package tm.services.district;
import static tm.services.district.GlobalServices.DATE_TIME_FORMAT;
import static tm.services.district.GlobalServices.IS_ALWAYS_REQUIRED_SCH_MAP;
import static tm.services.district.GlobalServices.IS_PRINT;
import static tm.services.district.GlobalServices.NOTIFICATION_MAP;
import static tm.services.district.GlobalServices.OFFER_READY_MAP;
import static tm.services.district.GlobalServices.println;
import static tm.services.teacher.AcceptOrDeclineAjax.getAllEmailId;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictPortfolioConfig;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.EligibilityStatusMaster;
import tm.bean.EligibilityVerificationHistroy;
import tm.bean.EmployeeMaster;
import tm.bean.FinalizeSpecificTemplates;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.MessageToTeacher;
import tm.bean.SapCandidateDetails;
import tm.bean.SapCandidateDetailsHistory;
import tm.bean.SchoolInJobOrder;
import tm.bean.SchoolWiseCandidateStatus;
import tm.bean.StafferMaster;
import tm.bean.StatusSpecificEmailTemplates;
import tm.bean.StatusWiseAutoEmailSend;
import tm.bean.StatusWiseEmailSection;
import tm.bean.StatusWiseEmailSubSection;
import tm.bean.StrengthOpportunitySummary;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherAssessmentOption;
import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherElectronicReferencesHistory;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.cgreport.DistrictMaxFitScore;
import tm.bean.cgreport.JobCategoryWiseStatusPrivilege;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.cgreport.TeacherStatusNotesHistory;
import tm.bean.cgreport.TeacherStatusScores;
import tm.bean.cgreport.TeacherStatusScoresHistory;
import tm.bean.cgreport.UserMailSend;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.WorkFlowOptions;
import tm.bean.hqbranchesmaster.WorkFlowOptionsForStatus;
import tm.bean.i4.I4InterviewInvites;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificRefChkAnswers;
import tm.bean.master.DistrictSpecificRefChkOptions;
import tm.bean.master.DistrictSpecificRefChkQuestions;
import tm.bean.master.EligibilityMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.master.RaceMaster;
import tm.bean.master.SchoolKeyContact;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.SkillAttributesMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusPrivilegeForSchools;
import tm.bean.master.StatusSpecificQuestions;
import tm.bean.master.StatusSpecificScore;
import tm.bean.master.TimeZoneMaster;
import tm.bean.teacher.OnlineActivityQuestionSet;
import tm.bean.teacher.OnlineActivityStatus;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.controller.teacher.BasicController;
import tm.controller.teacher.UserDashboardController;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.EligibilityStatusMasterDAO;
import tm.dao.EligibilityVerificationHistroyDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.FinalizeSpecificTemplatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.SapCandidateDetailsDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.SchoolSelectedByCandidateDAO;
import tm.dao.SchoolWiseCandidateStatusDAO;
import tm.dao.StafferMasterDAO;
import tm.dao.StatusSpecificEmailTemplatesDAO;
import tm.dao.StatusWiseAutoEmailSendDAO;
import tm.dao.StatusWiseEmailSectionDAO;
import tm.dao.StatusWiseEmailSubSectionDAO;
import tm.dao.StrengthOpportunitySummaryDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAnswerDetailDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherElectronicReferencesHistoryDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.cgreport.DistrictMaxFitScoreDAO;
import tm.dao.cgreport.JobCategoryWiseStatusPrivilegeDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.cgreport.TeacherStatusNotesHistoryDAO;
import tm.dao.cgreport.TeacherStatusScoresDAO;
import tm.dao.cgreport.TeacherStatusScoresHistoryDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.hqbranchesmaster.WorkFlowOptionsDAO;
import tm.dao.hqbranchesmaster.WorkFlowOptionsForStatusDAO;
import tm.dao.i4.I4InterviewInvitesDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificRefChkAnswersDAO;
import tm.dao.master.DistrictSpecificRefChkOptionsDAO;
import tm.dao.master.DistrictSpecificRefChkQuestionsDAO;
import tm.dao.master.EligibilityMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.dao.master.PanelAttendeesDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.SchoolKeyContactDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.SkillAttributesMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.StatusPrivilegeForSchoolsDAO;
import tm.dao.master.StatusSpecificQuestionsDAO;
import tm.dao.master.StatusSpecificScoreDAO;
import tm.dao.master.UserEmailNotificationsDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.teacher.OnlineActivityAnswersDAO;
import tm.dao.teacher.OnlineActivityQuestionSetDAO;
import tm.dao.teacher.OnlineActivityStatusDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CommonService;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.services.MassStatusUpdateService;
import tm.services.PATSHiredPostThread;
import tm.services.PaginationAndSorting;
import tm.services.ScheduleMailThreadByListToBcc;
import tm.services.i4.ScheduleI4Thread;
import tm.services.report.CGInviteInterviewAjax;
import tm.services.report.CandidateGridService;
import tm.services.report.CandidateGridSubAjax;
import tm.services.teacher.AcceptOrDeclineAjax;
import tm.services.teacher.AcceptOrDeclineThread;
import tm.services.teacher.AdvOnBoardingThread;
import tm.services.teacher.MailSendEvlCompThread;
import tm.services.teacher.MailSendSchedulerThread;
import tm.services.teacher.OnlineActivityAjax;
import tm.services.teacher.SchoolSelectionAjax;
import tm.services.teacher.SchoolSelectionThread;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class ManageStatusAjax
{
	private static final int MYTHREADS = 1000;
	

    String locale = Utility.getValueOfPropByKey("locale");

	@Autowired
	private WorkFlowOptionsForStatusDAO flowOptionsForStatusDAO;
	
	@Autowired
	private WorkFlowOptionsDAO flowOptionsDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private CandidateGridSubAjax candidateGridSubAjax; 

	@Autowired
	private CommonService commonService;

	@Autowired
	private OnlineActivityAnswersDAO onlineActivityAnswersDAO;

	@Autowired
	private OnlineActivityQuestionSetDAO onlineActivityQuestionSetDAO;

	@Autowired
	private OnlineActivityStatusDAO onlineActivityStatusDAO; 
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private OnlineActivityAjax onlineActivityAjax;
	public void setOnlineActivityAjax(
			OnlineActivityAjax onlineActivityAjax) {
		this.onlineActivityAjax = onlineActivityAjax;
	}

	@Autowired
	private SchoolSelectedByCandidateDAO schoolSelectedByCandidateDAO;
	@Autowired
	private SchoolSelectionAjax schoolSelectionAjax;
	public void setSchoolSelectionAjax(
			SchoolSelectionAjax schoolSelectionAjax) {
		this.schoolSelectionAjax = schoolSelectionAjax;
	}

	@Autowired
	private CGInviteInterviewAjax cGInviteInterviewAjax;
	public void setcGInviteInterviewAjax(
			CGInviteInterviewAjax cGInviteInterviewAjax) {
		this.cGInviteInterviewAjax = cGInviteInterviewAjax;
	}

	@Autowired
	private I4InterviewInvitesDAO I4InterviewInvitesDAO;

	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;

	@Autowired
	private RoleMasterDAO roleMasterDAO;

	@Autowired
	private SchoolWiseCandidateStatusDAO schoolWiseCandidateStatusDAO;

	@Autowired
	private StafferMasterDAO stafferMasterDAO;

	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;

	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;

	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;

	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;

	@Autowired
	private StatusPrivilegeForSchoolsDAO statusPrivilegeForSchoolsDAO;

	@Autowired
	private UserEmailNotificationsDAO userEmailNotificationsDAO;

	@Autowired
	private EligibilityStatusMasterDAO eligibilityStatusMasterDAO;

	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}

	@Autowired
	private TeacherStatusScoresDAO teacherStatusScoresDAO;

	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;

	@Autowired
	private TeacherStatusNotesDAO teacherStatusNotesDAO;

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;

	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;

	@Autowired
	private StatusMasterDAO statusMasterDAO;

	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;

	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;

	@Autowired
	private JobOrderDAO jobOrderDAO;

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;

	@Autowired
	private DistrictMaxFitScoreDAO districtMaxFitScoreDAO;

	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;

	@Autowired
	private UserMasterDAO userMasterDAO;

	@Autowired
	private EligibilityMasterDAO eligibilityMasterDAO;

	@Autowired
	private EligibilityVerificationHistroyDAO eligibilityVerificationHistroyDAO;

	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@Autowired
	private StatusWiseEmailSectionDAO statusWiseEmailSectionDAO;
	@Autowired
	private StatusWiseEmailSubSectionDAO statusWiseEmailSubSectionDAO;
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	@Autowired
	private SkillAttributesMasterDAO skillAttributesMasterDAO;
	public void setSkillAttributesMasterDAO(
			SkillAttributesMasterDAO skillAttributesMasterDAO) {
		this.skillAttributesMasterDAO = skillAttributesMasterDAO;
	}

	@Autowired
	private StatusSpecificQuestionsDAO statusSpecificQuestionsDAO;
	public void setStatusSpecificQuestionsDAO(
			StatusSpecificQuestionsDAO statusSpecificQuestionsDAO) {
		this.statusSpecificQuestionsDAO = statusSpecificQuestionsDAO;
	}


	@Autowired
	private StatusSpecificScoreDAO statusSpecificScoreDAO;
	public void setStatusSpecificScoreDAO(
			StatusSpecificScoreDAO statusSpecificScoreDAO) {
		this.statusSpecificScoreDAO = statusSpecificScoreDAO;
	}

	@Autowired
	private JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO;
	public void setJobCategoryWiseStatusPrivilegeDAO(
			JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO) {
		this.jobCategoryWiseStatusPrivilegeDAO = jobCategoryWiseStatusPrivilegeDAO;
	}

	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}

	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) {
		this.districtKeyContactDAO = districtKeyContactDAO;
	}

	@Autowired
	private SchoolKeyContactDAO schoolKeyContactDAO;
	public void setSchoolKeyContactDAO(SchoolKeyContactDAO schoolKeyContactDAO) {
		this.schoolKeyContactDAO = schoolKeyContactDAO;
	}

	@Autowired
	private StatusSpecificEmailTemplatesDAO statusSpecificEmailTemplatesDAO;
	public void setStatusSpecificEmailTemplatesDAO(
			StatusSpecificEmailTemplatesDAO statusSpecificEmailTemplatesDAO) {
		this.statusSpecificEmailTemplatesDAO = statusSpecificEmailTemplatesDAO;
	}

	@Autowired
	private TeacherAnswerDetailDAO teacherAnswerDetailDAO;

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;

	@Autowired
	private PanelScheduleDAO panelScheduleDAO;
	@Autowired
	private PanelAttendeesDAO panelAttendeesDAO;

	@Autowired
	private JobWisePanelStatusDAO jobWisePanelStatusDAO;

	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;

	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;

	@Autowired
	private StatusWiseAutoEmailSendDAO statusWiseAutoEmailSendDAO;

	@Autowired
	private RaceMasterDAO raceMasterDAO;

	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;

	@Autowired
	private TeacherElectronicReferencesHistoryDAO teacherElectronicReferencesHistoryDAO;

	@Autowired
	private DistrictSpecificRefChkQuestionsDAO districtSpecificRefChkQuestionsDAO;

	@Autowired
	private DistrictSpecificRefChkAnswersDAO districtSpecificRefChkAnswersDAO;

	@Autowired
	private DistrictSpecificRefChkOptionsDAO districtSpecificRefChkOptionsDAO;
	
	@Autowired
	private SapCandidateDetailsDAO sapCandidateDetailsDAO;
	
	@Autowired
	private StrengthOpportunitySummaryDAO strengthOpportunitySummaryDAO;
	
	@Autowired
	TeacherStatusScoresHistoryDAO teacherStatusScoresHistoryDAO;
	
	@Autowired
	TeacherStatusNotesHistoryDAO teacherStatusNotesHistoryDAO;
	
	@Autowired
	private AcceptOrDeclineAjax acceptOrDeclineAjax;	
	
	@Autowired
	private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
	
	@Autowired
	private FinalizeSpecificTemplatesDAO finalizeSpecificTemplatesDAO;
	/*========= Sekhar :  Create Folder Method ======================*/
	public int statusCheck(int parentId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		int returnFlag=0;
		try 
		{
			SecondaryStatus secondaryStatusObj= secondaryStatusDAO.findById(parentId, false, false);
			if(secondaryStatusObj.getStatusNodeMaster()!=null || secondaryStatusObj.getStatusMaster()!=null){
				returnFlag=1;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return returnFlag;
	}


	/*========= Sekhar :  Create Folder Method ======================*/
	public SecondaryStatus createStatusFolder(int parentId,String districtId,Integer jobCategory)
	{
		//System.out.println(":::parentId::new::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		DistrictMaster districtMaster			=	null;
		SecondaryStatus secondaryStatus 			= 	new SecondaryStatus();
		JobCategoryMaster jobCategoryMaster=null;

		if(userSession.getEntityType()!=1)
			districtMaster=userSession.getDistrictId();
		else if(districtId!=null && !districtId.equals(""))
			districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId), false, false);

		if(jobCategory!=null && jobCategory>0)
			jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategory, false, false);

		try 
		{
			SecondaryStatus secondaryStatusObj= secondaryStatusDAO.findById(parentId, false, false);
			String secondaryStatusName=secondaryStatusDAO.findSecondaryStatusExist(districtMaster,"New Status");
			//System.out.println("secondaryStatusName::::"+secondaryStatusName);

			if(secondaryStatusObj.getStatusNodeMaster()!=null){
				secondaryStatus.setUsermaster(userSession);
				secondaryStatus.setDistrictMaster(districtMaster);
				if(userSession.getEntityType()==3 && userSession.getSchoolId()!=null)
					secondaryStatus.setSchoolMaster(userSession.getSchoolId());

				secondaryStatus.setSecondaryStatusName(secondaryStatusName);
				secondaryStatus.setOrderNumber(11);
				secondaryStatus.setStatus("A");
				secondaryStatus.setSecondaryStatus(secondaryStatusObj);
				secondaryStatus.setCreatedDateTime(new Date());

				if(jobCategoryMaster!=null)
					secondaryStatus.setJobCategoryMaster(jobCategoryMaster);

				secondaryStatusDAO.makePersistent(secondaryStatus);

				// Start ... Add data for child relationship
				Map<SecondaryStatus, JobCategoryMaster> mapjc=new HashMap<SecondaryStatus, JobCategoryMaster>();
				if(jobCategory==null || jobCategory==0)
				{
					mapjc=secondaryStatusDAO.findSSByDistrictPlusByMap(districtMaster,secondaryStatusObj);
					SessionFactory sessionFactory=secondaryStatusDAO.getSessionFactory();
					StatelessSession statelesSsession = sessionFactory.openStatelessSession();
					Transaction txOpen =statelesSsession.beginTransaction();
					if(mapjc.size()>0)
					{
						Iterator iter = mapjc.entrySet().iterator();
						while (iter.hasNext()) {
							Map.Entry mEntry = (Map.Entry) iter.next();

							SecondaryStatus ssp=null;
							JobCategoryMaster jobCategoryMaster2=null;
							if(mEntry.getKey()!=null)
								ssp=(SecondaryStatus)mEntry.getKey();

							if(mEntry.getValue()!=null)
								jobCategoryMaster2=(JobCategoryMaster)mEntry.getValue();

							SecondaryStatus secondaryStatus3=new SecondaryStatus();
							secondaryStatus3.setDistrictMaster(districtMaster);
							if(userSession.getEntityType()==3 && userSession.getSchoolId()!=null)
								secondaryStatus.setSchoolMaster(userSession.getSchoolId());
							secondaryStatus3.setJobCategoryMaster(jobCategoryMaster2);
							secondaryStatus3.setSecondaryStatusName(secondaryStatus.getSecondaryStatusName());
							secondaryStatus3.setOrderNumber(secondaryStatus.getOrderNumber());
							secondaryStatus3.setStatus(secondaryStatus.getStatus());
							secondaryStatus3.setSecondaryStatus(ssp);
							secondaryStatus3.setSecondaryStatus_copy(secondaryStatus);
							secondaryStatus3.setUsermaster(userSession);
							secondaryStatus3.setCreatedDateTime(secondaryStatus.getCreatedDateTime());
							statelesSsession.insert(secondaryStatus3);
						}
					}
					txOpen.commit();
					statelesSsession.close();
				}
				// End ... Add data for child relationship

			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return secondaryStatus;
	}

	public SecondaryStatus copyPasteFolder(int parentId, String title,String districtId,Integer jobCategory)
	{
		//System.out.println("parentId::::::::"+parentId+" Title  "+title);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userMaster	=	(UserMaster) session.getAttribute("userMaster");
		SecondaryStatus secondaryStatus 			= 	new SecondaryStatus();


		DistrictMaster districtMaster=null;
		JobCategoryMaster jobCategoryMaster=null;

		if(userMaster.getEntityType()!=1)
			districtMaster=userMaster.getDistrictId();
		else if(districtId!=null && !districtId.equals(""))
			districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId), false, false);

		if(jobCategory!=null && jobCategory>0)
			jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategory, false, false);

		try 
		{

			secondaryStatus.setUsermaster(userMaster);
			secondaryStatus.setDistrictMaster(userMaster.getDistrictId());
			if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null)
			{
				secondaryStatus.setSchoolMaster(userMaster.getSchoolId());
			}
			SecondaryStatus secondaryStatusObj= secondaryStatusDAO.findById(parentId, false, false);
			String secondaryStatusName=secondaryStatusDAO.findSecondaryStatusExist(districtMaster,"Copy of "+title);

			secondaryStatus.setSecondaryStatusName(secondaryStatusName);
			secondaryStatus.setOrderNumber(11);
			secondaryStatus.setStatus("A");
			secondaryStatus.setSecondaryStatus(secondaryStatusObj);
			secondaryStatus.setCreatedDateTime(new Date());

			if(districtMaster!=null)
				secondaryStatus.setDistrictMaster(districtMaster);
			if(jobCategoryMaster!=null)
				secondaryStatus.setJobCategoryMaster(jobCategoryMaster);

			secondaryStatusDAO.makePersistent(secondaryStatus);

			// Start ... Add data for child relationship
			Map<SecondaryStatus, JobCategoryMaster> mapjc=new HashMap<SecondaryStatus, JobCategoryMaster>();
			if(jobCategory==null || jobCategory==0)
			{
				mapjc=secondaryStatusDAO.findSSByDistrictPlusByMap(districtMaster,secondaryStatusObj);
				/**** Session dynamic ****/
				SessionFactory sessionFactory=secondaryStatusDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				Transaction txOpen =statelesSsession.beginTransaction();
				if(mapjc.size()>0)
				{
					Iterator iter = mapjc.entrySet().iterator();
					while (iter.hasNext()) {


						Map.Entry mEntry = (Map.Entry) iter.next();

						SecondaryStatus ssp=null;
						JobCategoryMaster jobCategoryMaster2=null;
						if(mEntry.getKey()!=null)
							ssp=(SecondaryStatus)mEntry.getKey();

						if(mEntry.getValue()!=null)
							jobCategoryMaster2=(JobCategoryMaster)mEntry.getValue();

						SecondaryStatus secondaryStatus3=new SecondaryStatus();

						secondaryStatus3.setDistrictMaster(districtMaster);
						secondaryStatus3.setJobCategoryMaster(jobCategoryMaster2);
						secondaryStatus3.setSecondaryStatusName(secondaryStatusName);
						secondaryStatus3.setOrderNumber(11);
						secondaryStatus3.setStatus("A");
						//secondaryStatus3.setSecondaryStatus(secondaryStatusObj);
						secondaryStatus3.setCreatedDateTime(new Date());
						secondaryStatus3.setSecondaryStatus(ssp);
						secondaryStatus3.setSecondaryStatus_copy(secondaryStatus);
						secondaryStatus3.setUsermaster(userMaster);
						if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null)
							secondaryStatus3.setSchoolMaster(userMaster.getSchoolId());

						statelesSsession.insert(secondaryStatus3);
					}
				}
				txOpen.commit();
				statelesSsession.close();
			}
			// End ... Add data for child relationship

		}catch (Exception e) {
			e.printStackTrace();
		}		
		return secondaryStatus;
	}
	/*============ Sekhar : Cut Folder Method ================*/
	@Transactional(readOnly=false)
	public SecondaryStatus cutPasteFolder(int parentId,int dictkey,String districtId,Integer jobCategory)
	{
		//System.out.println("cutPasteFolder   parentId::::::::"+parentId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		UserMaster userMaster	=	(UserMaster) session.getAttribute("userMaster");

		DistrictMaster districtMaster=null;
		if(userMaster.getEntityType()!=1)
			districtMaster=userMaster.getDistrictId();
		else if(districtId!=null && !districtId.equals(""))
			districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId), false, false);

		SecondaryStatus secondaryStatus = secondaryStatusDAO.findById(dictkey, false, false);
		String sourceParetnName=secondaryStatus.getSecondaryStatus().getSecondaryStatusName();
		try 
		{
			SecondaryStatus secondaryStatusObj= secondaryStatusDAO.findById(parentId, false, false);
			secondaryStatus.setSecondaryStatus(secondaryStatusObj);
			secondaryStatus.setCreatedDateTime(new Date());
			secondaryStatusDAO.makePersistent(secondaryStatus);

			// Start ... Add data for child relationship
			if(jobCategory==null || jobCategory==0)
				secondaryStatusDAO.cutAndPasteForJobCategory(secondaryStatusObj, secondaryStatus, jobCategory,districtMaster,sourceParetnName);
			// End ... Add data for child relationship

		}catch (Exception e) {
			e.printStackTrace();
		}

		return secondaryStatus;
	}


	/*========= Sekhar :  Rename Folder Method ======================*/
	@Transactional(readOnly=false)
	public int renameStatusFolder(int folderId,String folderName )
	{
		//System.out.println("folderId>>>>::::::::"+folderId+" == folderName =="+folderName);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		SecondaryStatus secondaryStatus = new SecondaryStatus();
		List<SecondaryStatus> secondaryStatusList= new ArrayList<SecondaryStatus>();
		try{
			secondaryStatus=secondaryStatusDAO.findById(folderId, false, false);
			if(secondaryStatus.getStatusNodeMaster()!=null || (secondaryStatus.getStatusMaster()!=null && secondaryStatus.getStatusMaster().getBanchmarkStatus()!=1)){
				return 0;
			}else{
				List<JobCategoryMaster> jobCategoryMasterList=new ArrayList<JobCategoryMaster>();
				Map<Integer,JobCategoryMaster> jCM= new HashMap<Integer, JobCategoryMaster>();

				List<JobCategoryWiseStatusPrivilege> jobCWSP=new ArrayList<JobCategoryWiseStatusPrivilege>();
				List<StatusSpecificQuestions> lstSSQuestions = new ArrayList<StatusSpecificQuestions>();

				if(secondaryStatus.getJobCategoryMaster()!=null){
					if(secondaryStatus.getStatusMaster()!=null){
						jobCWSP=jobCategoryWiseStatusPrivilegeDAO.getMaxJSIValueList(secondaryStatus.getDistrictMaster(),secondaryStatus.getJobCategoryMaster(),null,secondaryStatus.getStatusMaster());
						lstSSQuestions=statusSpecificQuestionsDAO.findQuestions(secondaryStatus.getDistrictMaster(),secondaryStatus.getJobCategoryMaster(),null,secondaryStatus.getStatusMaster());
					}else{
						jobCWSP=jobCategoryWiseStatusPrivilegeDAO.getMaxJSIValueList(secondaryStatus.getDistrictMaster(),secondaryStatus.getJobCategoryMaster(),secondaryStatus,null);
						lstSSQuestions=statusSpecificQuestionsDAO.findQuestions(secondaryStatus.getDistrictMaster(),secondaryStatus.getJobCategoryMaster(),secondaryStatus,null);
					}
					//System.out.println("lstSSQuestions::::"+lstSSQuestions.size());
					if(jobCWSP.size()>0 && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("jsi")){
						return 3;
					}else if(lstSSQuestions.size()>0 && !secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("jsi")){
						return 5;
					}
					jobCategoryMasterList=jobCategoryMasterDAO.findAssessmentDocumentByDistrictJobCategory(secondaryStatus.getDistrictMaster(),secondaryStatus.getJobCategoryMaster().getJobCategoryId());
				}else{
					jobCategoryMasterList=jobCategoryMasterDAO.findAssessmentDocumentByDistrictJobCategory(secondaryStatus.getDistrictMaster(),null);
				}
				if(jobCategoryMasterList.size()>0){
					for(JobCategoryMaster jc: jobCategoryMasterList){
						jCM.put(jc.getJobCategoryId(),jc);
					}
				}
				if(!folderName.equalsIgnoreCase("jsi") || (folderName.equalsIgnoreCase("jsi") && jobCategoryMasterList.size()>0)){
					Criterion criterion=Restrictions.eq("secondaryStatusName",folderName.trim());
					Criterion criterion1=Restrictions.eq("districtMaster",secondaryStatus.getDistrictMaster());
					Criterion criterion2=Restrictions.eq("status","A");
					Criterion criterion3=null;
					if(secondaryStatus.getJobCategoryMaster()!=null){
						criterion3=Restrictions.eq("jobCategoryMaster",secondaryStatus.getJobCategoryMaster());
					}else{
						criterion3=Restrictions.isNull("jobCategoryMaster");
					}
					secondaryStatusList=secondaryStatusDAO.findByCriteria(criterion,criterion1,criterion2,criterion3);
					//System.out.println("secondaryStatusList:::"+secondaryStatusList.size());
					if(secondaryStatusList.size()>0){
						return 1;
					}else{
						String strOldStatusName=secondaryStatus.getSecondaryStatusName();
						secondaryStatus.setSecondaryStatusName(folderName);
						secondaryStatusDAO.makePersistent(secondaryStatus);
						secondaryStatusDAO.updateChildNodes(jCM,secondaryStatus, folderName,strOldStatusName);

						return 2;
					}
				}else{
					return 4;
				}
			}

		}catch (Exception e) {
			e.printStackTrace();
		}		
		return 2;
	}

	/*========= Sekhar :  displayTreeStructure Method ======================*/
	public String displayTreeStructure()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		List<SecondaryStatus> lstTreeStructure=	new ArrayList<SecondaryStatus>();
		StringBuffer sb =	new StringBuffer();
		try 
		{
			try{
				if(userSession.getEntityType()!=1){
					lstTreeStructure =	secondaryStatusDAO.findSecondaryStatus(userSession.getDistrictId());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			sb.append(" <div id=\"tree\"><ul>");
			for(SecondaryStatus tree: lstTreeStructure)
			{
				if(tree.getSecondaryStatus()==null)
				{
					sb.append("<li id='"+tree.getSecondaryStatusId()+"' class=\"folder\"  >"+tree.getSecondaryStatusName()+" ");
					if(tree.getChildren().size()>0)
						sb.append(printChild(request, tree.getChildren(),tree));
				}
			}
			sb.append("</ul></div>");

		}catch (Exception e) {
			e.printStackTrace();
		}		
		return sb.toString();
	}
	public String printChild(HttpServletRequest request,List<SecondaryStatus> lstuserChildren,SecondaryStatus tree)
	{
		StringBuffer sbchild=new StringBuffer();
		sbchild.append("<ul>");
		int banchmarkStatus=0;

		for(SecondaryStatus subfL: lstuserChildren ){
			if(subfL.getStatusMaster()!=null){
				if(subfL.getStatusMaster().getBanchmarkStatus()!=null){
					banchmarkStatus=subfL.getStatusMaster().getBanchmarkStatus();
				}
			}

			if(banchmarkStatus==0 && subfL.getStatus().equalsIgnoreCase("A") && subfL.getJobCategoryMaster()==tree.getJobCategoryMaster()){	
				sbchild.append("<li id='"+subfL.getSecondaryStatusId()+"' class=\"folder\" >"+subfL.getSecondaryStatusName()+"");
				/*if(subfL.getChildren().size()>0){
					sbchild.append(printChild(request, subfL.getChildren(),subfL));
					sbchild.append("</li>");
				}*/
			}else if(subfL.getStatusMaster()==null && subfL.getStatus().equalsIgnoreCase("A") && subfL.getJobCategoryMaster()==tree.getJobCategoryMaster()){
				sbchild.append("<li id='"+subfL.getSecondaryStatusId()+"' class=\"folder\" >"+subfL.getSecondaryStatusName()+"");
			}
		}
		for(SecondaryStatus subfL: lstuserChildren){
			if(subfL.getStatusMaster()!=null){
				if((subfL.getStatusMaster().getStatusShortName().equals("hcomp")|| subfL.getStatusMaster().getStatusShortName().equals("vcomp")||subfL.getStatusMaster().getStatusShortName().equals("ecomp")||subfL.getStatusMaster().getStatusShortName().equals("scomp")) && subfL.getStatus().equalsIgnoreCase("A") && subfL.getJobCategoryMaster()==tree.getJobCategoryMaster()){
					sbchild.append("<li id='"+subfL.getSecondaryStatusId()+"' class=\"folder\" >"+subfL.getSecondaryStatusName()+"");
				} 
			}
		}
		sbchild.append("</ul>");
		return sbchild.toString();
	}
	/*========= Sekhar :  displayTreeStructure Method ======================*/
	public String displayStatusDashboard(Integer headQuarterId,Integer branchId,Integer districtId,Integer jobCategory, Boolean isFromManageNode)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer rdiv =	new StringBuffer();
		try 
		{
			DistrictMaster districtMaster=null;
			JobCategoryMaster jobCategoryMaster=null;
			BranchMaster branchMaster=null;
			isFromManageNode = isFromManageNode==null? false : isFromManageNode;
			HeadQuarterMaster headQuarterMaster=null;
			List<SecondaryStatus> lstTreeStructure=	new ArrayList<SecondaryStatus>();
			
			if(districtId!=null && districtId>0){
				districtMaster=districtMasterDAO.findById(districtId, false, false);
			}
			if(jobCategory!=null && jobCategory>0){
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategory, false, false);
			}
			if(branchId!=null && branchId>0){
				branchMaster=branchMasterDAO.findById(branchId, false, false);
			}
			if(headQuarterId!=null && headQuarterId>0){
				headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
			}
			//System.out.println("districtId::::"+districtId +" jobCategory:::"+jobCategory+" headQuarterId::::"+headQuarterId +" branchId:::::"+branchId);
			lstTreeStructure =	secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHead(headQuarterMaster,branchMaster,districtMaster,jobCategoryMaster);
			
			System.out.println("lstTreeStructure:::::::"+lstTreeStructure.size());
			
			//*********************** changed by Deepak *************************************8 
			rdiv.append("<style>.childdiv{text-align:center;float:left;width: 40px;word-break: break-word;}.leftrightwidth{width: 40px;}</style>");
			
			//rdiv.append("<div class='row'><div class='col-sm-12 col-md-12'>");
			//rdiv.append("<tr><td colspan=4><img src=\"images/headstatus.png\" /></td><td>&nbsp;</td></tr>");
			//rdiv.append("<tr>");
			//rdiv.append("<div class='row'><div class='col-sm-12 col-md-12'>");
			Integer FlagCounter=0;
			String parentValue="";
			String iconName="";
			rdiv.append("<style>.childdiv{text-align:center;float:left;width: 93px;word-break: break-word;}</style>");
			List maximum = new ArrayList();
			maximum.add(0, 1);
			System.out.println("                lstTreeStructure size ::::::"+lstTreeStructure.size());
			for(SecondaryStatus tree: lstTreeStructure)
			{
				if(tree.getSecondaryStatus()==null)
				{   
					if(tree.getChildren().size()>0){					
						if(tree.getChildren().size()>0){
							FlagCounter+=1;
							if(FlagCounter==1)
							{
								iconName="screening.png";
								parentValue=Utility.getLocaleValuePropByKey("lblScreening", locale);
								rdiv.append("<div class='row' style='width:102%;'><div class='col-sm-6 col-md-6' style='background:#40C4FF;padding-left: 7%;'><label style='color:white;margin-bottom: 0px;'><b>Step 1</b>&nbsp;|&nbsp"+parentValue+"</label></div><div class='col-sm-6 col-md-6' style='border-top: 1px solid black; '></div></div>");
							}
							if(FlagCounter==2)
							{
								iconName="interview.png";
								parentValue=Utility.getLocaleValuePropByKey("lblInterview", locale);
								rdiv.append("<div class='row' style='width:102%;'><div class='col-sm-6 col-md-6' style='background:#F06292;padding-left: 7%;'><label style='color:white;margin-bottom: 0px;'><b>Step 2</b>&nbsp;|&nbsp"+parentValue+"</label></div><div class='col-sm-6 col-md-6' style='border-top: 1px solid black; '></div></div>");
							}
							else if(FlagCounter==3)
							{
								iconName="vetting.png";
								parentValue=Utility.getLocaleValuePropByKey("lblVetting", locale);
								rdiv.append("<div class='row' style='width:102%;'><div class='col-sm-6 col-md-6' style='background:#8560A8;padding-left: 7%;'><label style='color:white;margin-bottom: 0px;'><b>Step 3</b>&nbsp;|&nbsp"+parentValue+"</label></div><div class='col-sm-6 col-md-6' style='border-top: 1px solid black; '></div></div>");
							}
						rdiv.append(printChildStatus(request, tree.getChildren(),tree,isFromManageNode,iconName,maximum,FlagCounter));
					}
					//rdiv.append("</div>");
				}
			 }
			}
			
               Integer no_hiring=0; 
				parentValue=Utility.getLocaleValuePropByKey("lblHiring", locale);
				rdiv.append("<div class='row' style='width:102%;'><div class='col-sm-6 col-md-6' style='background:#22C589;padding-left: 6%;'><label style='color:white;margin-bottom: 0px;'><b>Step 4</b>&nbsp;|&nbsp"+parentValue+"</label></div><div class='col-sm-6 col-md-6' style='border-top: 1px solid black; '></div></div>");
				rdiv.append("<div class='row parentalignment'>");
				rdiv.append("<div class='col-sm-1 col-md-1' style='padding-left: 0px;'><div><img src=\"images/hirings.png\" style='width:40px;background-color: white;'/></div></div>");
				rdiv.append("<div class='col-sm-10 col-md-10'>");
			if(headQuarterMaster==null){
				no_hiring+=2;
				rdiv.append("<div class='childdiv'>");
				//rdiv.append("<td style='vertical-align:top;'>");
				//rdiv.append("<table  border=0 bordercolor='green'>");
				//rdiv.append("<div class='childdiv'>");
				rdiv.append("<a href='#'><img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/></a><br/><label style='margin-top:6px;'>Hired</label>");
				rdiv.append("</div>");
				rdiv.append("<div class='childdiv'>");
				//rdiv.append("</table>");
				//rdiv.append("<table  border=0 bordercolor='green'>");
				//rdiv.append("<div class='childdiv'>");
				rdiv.append("<img  style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br/><label style='margin-top:6px;'>Declined</label>");
				rdiv.append("</div>");
				//rdiv.append("</table>");
			}else{
				no_hiring+=1;
				rdiv.append("<div class='childdiv'>");
				//rdiv.append("<td style='vertical-align:top;'>");
				//rdiv.append("<table  border=0 bordercolor='green'>");
				//rdiv.append("<div class='childdiv'>");
				rdiv.append("<a href='#'><img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/></a><br/><label style='margin-top:6px;'>Completed Onboarding Process</label>");
				rdiv.append("</div>");
				//rdiv.append("</table>");
			}
			//rdiv.append("<table  border=0 bordercolor='green'>");
			no_hiring+=1;
			//rdiv.append("</div>");
			rdiv.append("<div class='childdiv'>");
			rdiv.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br/><label style='margin-top:6px;'>Rejected</label>");
			rdiv.append("</div></div>");
			//rdiv.append("</table>");
			//rdiv.append("<table  border=0 bordercolor='green'>");
			rdiv.append("<div class='col-sm-1 col-md-1'><div>");
			rdiv.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br/><label style='margin-top:6px;'>Withdrawn</label>");
			rdiv.append("</div>");
			//rdiv.append("</table>");
			//rdiv.append("</td>");
			//rdiv.append("</tr>");
			rdiv.append("</div></div></div></div>");
				//rdiv.append("</div></div>");
			Integer max_value;
			 max_value=Integer.parseInt(maximum.get(0).toString());
			Integer max_width;
			Integer leftrightwidth=6;
			
			if(max_value>no_hiring)
				max_width=100/(max_value-1);
			else
				max_width=100/no_hiring;
			System.out.println(max_width+"<---------no_hiring------->"+no_hiring);
			if(max_width/3>8)
			{
				leftrightwidth=max_width/(max_value);
			}
			
			System.out.println(" No OF node :::::::::::::: "+max_value+" ********** Width *********"+max_width+" left Right width ::::"+leftrightwidth);
			
					
			rdiv.append("<style>.childdiv{text-align:center;float:left;width: "+max_width+"%;word-break: break-word;}.leftrightwidth{width: "+leftrightwidth+"%;}</style>");
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return rdiv.toString();
	}
	/*========= Sekhar :  Delete Folder Method ======================*/
	public String printChildStatus(HttpServletRequest request,List<SecondaryStatus> lstuserChildren,SecondaryStatus tree, Boolean isFromManageNode,String iconName, List maximum, Integer FlagCounter)
	{
		
		StringBuffer sbchild=new StringBuffer();
		Collections.sort(lstuserChildren,SecondaryStatus.secondaryStatusOrder);
		//sbchild.append("<table  border=0 bordercolor='green'>");
		sbchild.append("<div class='row parentalignment'><div class='col-sm-1 col-md-1 steplineslc' style='padding-left: 0px;'><div><img style='width:40px;background-color: white;' src=\"images/"+iconName+"\"/></div></div>");
		int banchmarkStatus=0;
		int counterFlag=0;
		int counterStore=0;
		sbchild.append("<div class='col-sm-10 col-md-10 steplineslc' style='height:40px;'>");
		//System.out.println("------------------------------------------------------------------------------ Size   "+lstuserChildren.size());
		for(SecondaryStatus subfL: lstuserChildren ){
			//counterFlag++;
			if(subfL.getStatusMaster()!=null){
				if(subfL.getStatusMaster().getBanchmarkStatus()!=null){
					banchmarkStatus=subfL.getStatusMaster().getBanchmarkStatus();
				}
			}
			System.out.println("<<<<<<<<<banchmarkStatus>>>>>>>>>>>>>"+banchmarkStatus);
			if(banchmarkStatus==0){
				if(subfL.getStatusMaster()!=null){
					if(subfL.getStatusMaster().getStatusShortName().equals("hird")){
						sbchild.append("<div class='childdiv'>");
						if(isFromManageNode!=null && isFromManageNode==true)
							sbchild.append("<a href='javascript:void(0);' onclick=\"showQuestionsGrid('"+subfL.getSecondaryStatusId()+"')\"><img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/></a><br/><label style='margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");
						else
							sbchild.append("<img style='width:40px;background-color: white;' src=\"images/hire.png\"/><br/><label style='margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");
						sbchild.append("</div>");
					}else if(subfL.getStatusMaster().getStatusShortName().equals("rem")){
						//sbchild.append("<img src=\"images/remove.png\"/><br/>"+subfL.getSecondaryStatusName());
					}else if(subfL.getStatusMaster().getStatusShortName().equals("widrw")){
						//sbchild.append("<img src=\"images/withdrew.png\"/><br/>"+subfL.getSecondaryStatusName());
					}else if(subfL.getStatusMaster().getStatusShortName().equals("dcln")){
						//sbchild.append("<img src=\"images/decline.png\"/><br/>"+subfL.getSecondaryStatusName());
					}else if(subfL.getStatusMaster().getStatusShortName().equals("apl")  && subfL.getJobCategoryMaster()==tree.getJobCategoryMaster()){
						sbchild.append("<div class='childdiv'>");
						if(isFromManageNode!=null && isFromManageNode==true)
							sbchild.append("<a href='javascript:void(0);' onclick=\"showQuestionsGrid('"+subfL.getSecondaryStatusId()+"')\"><img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/></a><br/><label style='margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");
						else
							sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br/><label style='margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");
						sbchild.append("</div>");
					}else if(subfL.getStatusMaster().getStatusShortName().equals("hcomp")|| subfL.getStatusMaster().getStatusShortName().equals("vcomp")||subfL.getStatusMaster().getStatusShortName().equals("ecomp")||subfL.getStatusMaster().getStatusShortName().equals("scomp")){
						//sbchild.append("<tr><td style='text-align:center;height:100px;'>");
						sbchild.append("<div class='childdiv'><label style='margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");
						sbchild.append("</div>");
					}
				}else{
					sbchild.append("<div class='childdiv'>");
					if(isFromManageNode!=null && isFromManageNode==true)
						sbchild.append("<a href='javascript:void(0);' onclick=\"showQuestionsGrid('"+subfL.getSecondaryStatusId()+"')\"><img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/></a><br/><label style='margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");
					else
						sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br/><label style='margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");
					sbchild.append("</div>");
				}
				counterFlag++;
			}else if(subfL.getStatusMaster()==null && subfL.getStatus().equalsIgnoreCase("A") && subfL.getJobCategoryMaster()==tree.getJobCategoryMaster()){
				sbchild.append("<div class='childdiv'>");
				if(isFromManageNode!=null && isFromManageNode==true)
					sbchild.append("<a href='javascript:void(0);' onclick=\"showQuestionsGrid('"+subfL.getSecondaryStatusId()+"')\"><img style='width:40px;background-color: white;'  src=\"images/unavailable.png\"/></a><br/><label style='margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");
				else
					sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br/><label style='margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");
				
				sbchild.append("</div>");
				counterFlag++;
			}
			
		}
		sbchild.append("</div>");
		counterStore=Integer.parseInt(maximum.get(0).toString());
		counterFlag+=1;
		System.out.println("counterStore::::::::"+counterStore+"     Counter Flag:::"+(counterFlag));
		if(counterStore<counterFlag)
		{
		maximum.add(0, counterFlag);
		}
		
		sbchild.append("<div class='col-sm-1 col-md-1 steplineslc'");
		for(SecondaryStatus subfL: lstuserChildren){
			if(subfL.getStatusMaster()!=null && subfL.getStatus().equalsIgnoreCase("A") && subfL.getJobCategoryMaster()==tree.getJobCategoryMaster()){
				if(subfL.getStatusMaster().getStatusShortName().equals("hcomp")|| subfL.getStatusMaster().getStatusShortName().equals("vcomp")||subfL.getStatusMaster().getStatusShortName().equals("ecomp")||subfL.getStatusMaster().getStatusShortName().equals("scomp")){
					//sbchild.append("<div align='center' style='height:100px;width:150px;'>");
					sbchild.append("<div class='childdiv'><img style='width:40px;background-color: white;' src='images/unavailable.png'><br/><label style='margin-left: -40px;margin-right: -76px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label></div>");
					sbchild.append("</div>");
				} 
			}
		}
		//sbchild.append("</div></div>");
		sbchild.append("</div>");
		return sbchild.toString();
	}
	/*========= Sekhar :  Delete Folder Method ======================*/
	@Transactional(readOnly=false)
	public int deleteStatusFolder(int folderId)
	{
		//System.out.println("delete Folder Method: folderId::::::::"+folderId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		SecondaryStatus secondaryStatus = new SecondaryStatus();
		try 
		{
			secondaryStatus	= secondaryStatusDAO.findById(folderId, false, false);
			secondaryStatus.setStatus("I");
			try{
				secondaryStatusDAO.updatePersistent(secondaryStatus);
				secondaryStatusDAO.deleteChildNodes(secondaryStatus);
				return 1;
			}
			catch (Exception e)
			{
				return 2;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return 1;
	}

	/*========= Sekhar :  Delete Folder Method ======================*/
	@Transactional(readOnly=false)
	public int checkFolderIsDeleted(int folderId)
	{
		//System.out.println("checkFolderIsDeleted Method: folderId::::::::"+folderId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		SecondaryStatus secondaryStatus = new SecondaryStatus();
		try 
		{
			secondaryStatus	= secondaryStatusDAO.findById(folderId, false, false);
			if(secondaryStatus.getStatusMaster()!=null || secondaryStatus.getStatusNodeMaster()!=null)
				return 3;
			else
			{
				if(secondaryStatusDAO.isFoundChildNodes(secondaryStatus))
					return 5;
				else
					return 4;
			}

		}catch (Exception e) {
			e.printStackTrace();
			return 3;
		}
	}

	public String displayStatusDashboardCG(String teacherId,String jobId,int clickFlag)
	{
    WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	UserMaster userSession=(UserMaster) session.getAttribute("userMaster");
	DistrictMaster districtMaster=null;
	SchoolMaster schoolMaster=null;
	int entityType=0;
	int role=0;
	try{
		if(userSession!=null){
			entityType=userSession.getEntityType();
			role=userSession.getRoleId().getRoleId();
			if(userSession.getDistrictId()!=null){
				districtMaster=userSession.getDistrictId();
			}
			if(userSession.getSchoolId()!=null){
				schoolMaster=userSession.getSchoolId();
			}
		}
	}catch(Exception e){}
	List<SecondaryStatus> lstTreeStructure=	new ArrayList<SecondaryStatus>();
	List<DistrictMaxFitScore> lstDistrictMaxFitScores= new ArrayList<DistrictMaxFitScore>();
	List<StatusMaster> lstStatusMaster= new ArrayList<StatusMaster>();
	List<TeacherStatusScores> lstTeacherStatusScores= new ArrayList<TeacherStatusScores>();
	List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob= new ArrayList<TeacherStatusHistoryForJob>();
	List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJobActive= new ArrayList<TeacherStatusHistoryForJob>();
	List<SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus= new ArrayList<SchoolWiseCandidateStatus>();

	List<StatusPrivilegeForSchools> lstStatusPrivilegeForSchools= new ArrayList<StatusPrivilegeForSchools>();
	List<JobCategoryWiseStatusPrivilege> lstJobCategoryWiseStatusPrivilege= new ArrayList<JobCategoryWiseStatusPrivilege>();

	List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
	List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();
	//List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
	List<TeacherStatusNotes> lstTeacherStatusNotes= new ArrayList<TeacherStatusNotes>();
	List<DistrictKeyContact> districtKeyContactList =new ArrayList<DistrictKeyContact>();
	SchoolInJobOrder schoolInJobOrder=null;
	StringBuffer rdiv =	new StringBuffer();
	Map<Integer,StatusPrivilegeForSchools> mapSschool = new HashMap<Integer, StatusPrivilegeForSchools>();
	Map<Integer,StatusPrivilegeForSchools> mapSecSchool = new HashMap<Integer, StatusPrivilegeForSchools>();
	Map<Integer,SchoolWiseCandidateStatus> swcsMap= new HashMap<Integer, SchoolWiseCandidateStatus>();
	Map<Integer,JobCategoryWiseStatusPrivilege> mapJobSecSchool = new HashMap<Integer,JobCategoryWiseStatusPrivilege>();
	Map<String,UserMaster> mapJFH = new HashMap<String,UserMaster>();
	Map<String,String> mapHistrory = new HashMap<String,String>();
	Map<String,String> mapNote = new HashMap<String,String>();
	Map<SchoolMaster,String> mapSIJO = new HashMap<SchoolMaster,String>();
	boolean isJobAssessment=false;
	TeacherDetail teacherDetail=null;
	StatusMaster statusMaster =null;
	JobForTeacher jftObj=null;
	SchoolMaster schoolObj=null;
	boolean isOffer=true;
	boolean showStatus=true;
	int hiredFlagForClickOnStatus=0;
	boolean outerJob=true;
	String schoolIds="";
	try 
	{
		teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId),false,false);
		JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId),false,false);
		Date hireDate=Utility.getDays(jobOrder.getJobStartDate(),new Date(),7);
		boolean isPhiladelphia = false;//added by 21-03-2015
		boolean isMiami = false;
		boolean isHireDate = false;
		//added by 21-03-2015
		try{
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(0000000))
				isPhiladelphia=true;
		}catch(Exception e){};
		try{
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1200390))
				isMiami=true;
		}catch(Exception e){};
		try{
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1201470)){
				if(hireDate!=null){
					isHireDate=true;
				}
			}
		}catch(Exception e){};
		StatusMaster statusMasterForDcln=null;
		try{

			List<String> posList=new ArrayList<String>();
			List<JobOrder> jobList=new ArrayList<JobOrder>();
			List<JobForTeacher> jftOffer=jobForTeacherDAO.findJobByTeacherForOffer(teacherDetail);
			int fullAndPartTimeJob=0;
			try{
				if(jftOffer.size()>0)
					for (JobForTeacher jFTeacher : jftOffer) {
						posList.add(jFTeacher.getRequisitionNumber());
						jobList.add(jFTeacher.getJobId());
					}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(posList.size()>0){
				List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(jobOrder.getDistrictMaster(),posList);
				try{
					if(districtRequisitionNumbers.size()>0){
						List<JobRequisitionNumbers> lstRequisitionNumbers =jobRequisitionNumbersDAO.findSchoolByJobsAndDRN(jobList, districtRequisitionNumbers);
						for (JobRequisitionNumbers jobRequisitionNumbers : lstRequisitionNumbers) {
							if(jobRequisitionNumbers.getDistrictRequisitionNumbers().getPosType()!=null){
								if(jobRequisitionNumbers.getDistrictRequisitionNumbers().getPosType().equalsIgnoreCase("F")){
									fullAndPartTimeJob=1;
									break;
								}else if(jobRequisitionNumbers.getDistrictRequisitionNumbers().getPosType().equalsIgnoreCase("P")){
									fullAndPartTimeJob=2;
								}
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			//System.out.println("fullAndPartTimeJob::::::::::::::::::::::::::::"+fullAndPartTimeJob); 
			if(fullAndPartTimeJob==1){
				isOffer=false;
			}
			//System.out.println(jftOffer.size()+":::::isOffer:::::::"+isOffer);
			try{
				List<JobForTeacher> jft=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail,jobOrder);
				if(jft!=null){
					jftObj=jft.get(0);
					if(isMiami){
						try{
							if(jftObj!=null && jftObj.getRequisitionNumber()!=null){
								List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobOrder,jftObj.getJobId().getDistrictMaster(),jftObj.getRequisitionNumber());
								if(jobReqList.size()>0){
									try{
										schoolObj=jobReqList.get(0).getSchoolMaster();
									}catch(Exception e){
										e.printStackTrace();
									}
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					if(jft.get(0).getStatus().getStatusShortName().equalsIgnoreCase("icomp") || jft.get(0).getStatus().getStatusShortName().equalsIgnoreCase("comp") || jft.get(0).getStatus().getStatusShortName().equalsIgnoreCase("vlt")){
						statusMaster=jft.get(0).getStatus();
					}else{
						try{
							if(jft.get(0).getStatus().getStatusShortName().equalsIgnoreCase("dcln")){
								statusMasterForDcln=jft.get(0).getStatus();
							}
						}catch(Exception e){}
						JobForTeacher jobForTeacher= new JobForTeacher();
						jobForTeacher.setJobId(jobOrder);
						jobForTeacher.setTeacherId(teacherDetail);
						int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
						try{
						statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
						}catch(Exception e){}
					}
					if(jft.get(0).getStatus().getStatusShortName().equalsIgnoreCase("hird") && jobOrder.getHeadQuarterMaster()==null){
						showStatus=true;
						hiredFlagForClickOnStatus=1;
					}else if(jft.get(0).getStatus().getStatusShortName().equalsIgnoreCase("dcln") || jft.get(0).getStatus().getStatusShortName().equalsIgnoreCase("rem")){
						showStatus=false;
					}
				}
			}catch(Exception e){}
			if(jobOrder.getIsJobAssessment()!=null){
				if(jobOrder.getIsJobAssessment()){
					isJobAssessment=true;
				}
			}
			if(jobOrder!=null){
				List<SecondaryStatus> lstSecondaryStatus=secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
				if(lstSecondaryStatus.size()==0){
					secondaryStatusDAO.copyDataFromDistrictToJobCategoryBanchAndHead(jobOrder.getHeadQuarterMaster(),jobOrder.getBranchMaster(),jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster(),userSession);
				}
				lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
				lstDistrictMaxFitScores=districtMaxFitScoreDAO.getFitStatusScoreByHeadBranchAndDistrict(jobOrder);
				if(entityType==3){
					lstStatusPrivilegeForSchools=statusPrivilegeForSchoolsDAO.findStatusPrivilegeByHeadBranchAndDistrict(jobOrder);
					
					lstJobCategoryWiseStatusPrivilege=jobCategoryWiseStatusPrivilegeDAO.findStatusPrivilegeSchoolByHeadBranchAndDistrict(jobOrder);
					if(lstJobCategoryWiseStatusPrivilege!=null)
						for(JobCategoryWiseStatusPrivilege jobSchool:lstJobCategoryWiseStatusPrivilege){
							if(jobSchool.getSecondaryStatus()!=null)
								mapJobSecSchool.put(jobSchool.getSecondaryStatus().getSecondaryStatusId(), jobSchool);
						}

					if(lstStatusPrivilegeForSchools!=null)
						for(StatusPrivilegeForSchools spFSchool: lstStatusPrivilegeForSchools ){
							if(spFSchool.getStatusMaster()!=null){
								mapSschool.put(spFSchool.getStatusMaster().getStatusId(), spFSchool);
							}else{
								if(spFSchool.getSecondaryStatus()!=null)
									mapSecSchool.put(spFSchool.getSecondaryStatus().getSecondaryStatusId(), spFSchool);
							}
						}	
				}
				lstStatusMaster=statusMasterDAO.findAllStatusMaster();
				lstTeacherStatusScores=teacherStatusScoresDAO.getStatusScoreAllList(teacherDetail,jobOrder);
				lstTeacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findByTeacherStatusHistorySelected(teacherDetail,jobOrder);
				lstTeacherStatusHistoryForJobActive=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryActive(teacherDetail,jobOrder);
				districtKeyContactList= districtKeyContactDAO.findByContactType(jobOrder.getDistrictMaster(),"Staffer");
				try{
					if(userSession.getEntityType()==3){
						schoolInJobOrder=schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder,userSession.getSchoolId());
						if(schoolInJobOrder!=null){
							outerJob=false;
						}
					}
				}catch(Exception e){}
				try{
					if(lstTeacherStatusHistoryForJob.size()>0)
						for (TeacherStatusHistoryForJob teacherStatusHistoryForJob :lstTeacherStatusHistoryForJob) {
							if(teacherStatusHistoryForJob.getStatusMaster()!=null){
								String statusM=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##"+teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0"; 
								mapJFH.put(statusM, teacherStatusHistoryForJob.getUserMaster());
							}else{
								String sStatus=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##0"+"##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId();
								mapJFH.put(sStatus, teacherStatusHistoryForJob.getUserMaster());
							}
						}
					List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findJobOrder(jobOrder);
					if(listSchoolInJobOrder!=null && listSchoolInJobOrder.size()>0)
						for (SchoolInJobOrder schoolInJobOrder2 : listSchoolInJobOrder){
							if(mapSIJO.get("||"+schoolInJobOrder2.getSchoolId().getSchoolId()+"||")!=null){
								schoolIds=mapSIJO.get("||"+schoolInJobOrder2.getSchoolId().getSchoolId()+"||");
								mapSIJO.put(schoolInJobOrder2.getSchoolId(),schoolIds+"||"+schoolInJobOrder2.getSchoolId().getSchoolId()+"||");
							}else{
								schoolIds=schoolIds+"||"+schoolInJobOrder2.getSchoolId().getSchoolId()+"||";
								mapSIJO.put(schoolInJobOrder2.getSchoolId(),schoolIds);
							}
						}
					lstTeacherStatusNotes=teacherStatusNotesDAO.getFinalizeStatusNote(teacherDetail, jobOrder);
					if(lstTeacherStatusNotes.size()>0)
						for (TeacherStatusNotes teacherStatusNotes :lstTeacherStatusNotes) {
							if(teacherStatusNotes.getStatusMaster()!=null){
								String statusM=teacherStatusNotes.getTeacherDetail().getTeacherId()+"##"+teacherStatusNotes.getJobOrder().getJobId()+"##"+teacherStatusNotes.getStatusMaster().getStatusId()+"##0"; 
								try{
									if((mapNote.get(statusM)!=null && !mapNote.get(statusM).equals("Y")) || mapNote.get(statusM)==null){
										if(mapNote.get(statusM)!=null && mapNote.get(statusM).equals("N")){
											if(teacherStatusNotes.getUserMaster().getEntityType()==2){
												mapNote.put(statusM,"Y");
											}else if(teacherStatusNotes.getUserMaster().getSchoolId().getSchoolId().equals(userSession.getSchoolId().getSchoolId())){
												mapNote.put(statusM,"Y" );	
											}
										}else{
											if(teacherStatusNotes.getUserMaster().getEntityType()==2){
												mapNote.put(statusM,"Y");
											}else if(teacherStatusNotes.getUserMaster().getSchoolId().getSchoolId().equals(userSession.getSchoolId().getSchoolId())){
												mapNote.put(statusM,"Y");	
											}else{
												mapNote.put(statusM,"N");	
											}
										}
									}
								}catch(Exception e){
									mapNote.put(statusM,"N");
								}
							}else{
								String sStatus=teacherStatusNotes.getTeacherDetail().getTeacherId()+"##"+teacherStatusNotes.getJobOrder().getJobId()+"##0"+"##"+teacherStatusNotes.getSecondaryStatus().getSecondaryStatusId();
								try{
									if((mapNote.get(sStatus)!=null && !mapNote.get(sStatus).equals("Y")) || mapNote.get(sStatus)==null){
										if(mapNote.get(sStatus)!=null && mapNote.get(sStatus).equals("N")){
											if(teacherStatusNotes.getUserMaster().getEntityType()==2){
												mapNote.put(sStatus,"Y");
											}else if(teacherStatusNotes.getUserMaster().getSchoolId().getSchoolId().equals(userSession.getSchoolId().getSchoolId())){
												mapNote.put(sStatus,"Y" );	
											}
										}else{
											if(teacherStatusNotes.getUserMaster().getEntityType()==2){
												mapNote.put(sStatus,"Y");
											}else if(teacherStatusNotes.getUserMaster().getSchoolId().getSchoolId().equals(userSession.getSchoolId().getSchoolId())){
												mapNote.put(sStatus,"Y");	
											}else{
												mapNote.put(sStatus,"N");	
											}
										}
									}
								}catch(Exception e){
									mapNote.put(sStatus,"N");
								}
							}
						}
				}catch(Exception e){}
				//*******************
				if(userSession!=null)
					if(userSession.getEntityType()==3){
						lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusHistoryForSchool(teacherDetail, jobOrder,userSession.getSchoolId());
					}else if(userSession.getEntityType()==2){
						lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusHistoryByUser(teacherDetail, jobOrder,userSession);
					}
				//System.out.println(":::::::::::lstSchoolWiseCandidateStatus::::::::::::::::::::::::::::"+lstSchoolWiseCandidateStatus.size());
				if(lstSchoolWiseCandidateStatus.size()>0)
					for (SchoolWiseCandidateStatus schoolWiseCandidateStatus : lstSchoolWiseCandidateStatus) {
						swcsMap.put(schoolWiseCandidateStatus.getStatusMaster().getStatusId(), schoolWiseCandidateStatus);
						showStatus=false;
					}
				//System.out.println("showStatus:::::"+showStatus);
				//jobWisePanelStatusList=jobWisePanelStatusDAO.getPanelByJobAndSSID(jobOrder,lstTreeStructure);
				jobWisePanelStatusList=jobWisePanelStatusDAO.getPanelByJob(jobOrder);

				debugPrintln("", "jobWisePanelStatusList "+jobWisePanelStatusList.size());

				//panelScheduleList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusList(jobWisePanelStatusList);
				if(jobWisePanelStatusList.size() > 0)
					panelScheduleList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWisePanelStatusList,teacherDetail);

				//panelScheduleList=panelScheduleDAO.findPanelSchedules(teacherDetail, jobOrder);
				debugPrintln("", "panelScheduleList "+panelScheduleList.size());

			}
		}catch(Exception e){
			e.printStackTrace();
		}
		String teacherDetails=null;

		if(teacherDetail!=null){
			if(teacherDetail.getFirstName()!=null){
				teacherDetails=teacherDetail.getFirstName();
			}
			if(teacherDetail.getLastName()!=null){
				teacherDetails+=" "+teacherDetail.getLastName();
			}
			try{
				teacherDetails=teacherDetails.replace("'","\\'");
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		String onlineActivityStatusName="Pending";
		boolean onlineFlag=false;
		try{
			List<OnlineActivityStatus> onlineActivityStatusList=onlineActivityStatusDAO.findOnlineActivityStatusList(teacherDetail,districtMaster,jobOrder.getJobCategoryMaster());
			List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSetList(districtMaster,jobOrder.getJobCategoryMaster());
			Map<Integer,OnlineActivityStatus> onlineMap=new HashMap<Integer, OnlineActivityStatus>();
			for (OnlineActivityStatus onlineActivityStatus : onlineActivityStatusList) {
				onlineMap.put(onlineActivityStatus.getOnlineActivityQuestionSet().getQuestionSetId(),onlineActivityStatus);
			}
			for (OnlineActivityQuestionSet onlineActivityQuestionSet : onlineActivityQuestionSetList){
				onlineFlag=true;
				OnlineActivityStatus onlineActivityStatus =onlineMap.get(onlineActivityQuestionSet.getQuestionSetId());
				if(onlineActivityStatus!=null && onlineActivityStatus.getOnlineActivityQuestionSet().getQuestionSetId().equals(onlineActivityQuestionSet.getQuestionSetId())){
					if(onlineActivityStatus.getStatus()!=null && onlineActivityStatus.getStatus().equalsIgnoreCase("S")){
						if(!onlineActivityStatusName.equalsIgnoreCase("InComplete")){
							onlineActivityStatusName="Link Sent";
						}
					}else if(onlineActivityStatus.getStatus()!=null && onlineActivityStatus.getStatus().equalsIgnoreCase("I")){
							onlineActivityStatusName="InComplete";
					}else if(onlineActivityStatus.getStatus()!=null && onlineActivityStatus.getStatus().equalsIgnoreCase("C")){
						onlineActivityStatusName="Completed";
						break;
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		if(onlineFlag==false){
			onlineActivityStatusName=null;
		}
		System.out.println(onlineFlag+":::onlineActivityStatusName::::"+onlineActivityStatusName);
		//rdiv.append("<table border=0 bordercolor='red' width='765px'>");
		//rdiv.append("<tr>");
		//rdiv.append("<td style='vertical-align:bottom;'> ");
		rdiv.append("<div class='row'><div class='col-sm-12 col-md-12'>");
		//rdiv.append("<div class='row' style='height: 5px;'><div class='col-sm-4 col-md-4'></div><div class='col-sm-4 col-md-4' style='margin-top:20px;'><canvas id='chart-area'></canvas></div><div class='col-sm-4 col-md-4'></div></div>");
		rdiv.append("<div class='row' style='height: 5px;'><div class='col-sm-12 col-md-12'></div></div>");
		//   Adding By Deepak ******************
		Integer FlagCounter=0;
		String parentValue="";
		String iconName="";
		rdiv.append("<style>.childdiv{text-align:center;float:left;width: 93px;word-break: break-word;}</style>");
		List maximum = new ArrayList();
		maximum.add(0, 0);
		for(SecondaryStatus tree: lstTreeStructure)
		{ 
			if(tree.getSecondaryStatus()==null)
			{					
				
				rdiv.append("<input type='hidden' name='hiredFlagForClickOnStatus' value=\""+hiredFlagForClickOnStatus+"\" />");					
				if(tree.getChildren().size()>0){
					FlagCounter+=1;
					if(FlagCounter==1)
					{
						iconName="screening.png";
						parentValue=Utility.getLocaleValuePropByKey("lblScreening", locale);
						rdiv.append("<div class='row'><div class='col-sm-6 col-md-6' style='background:#40C4FF;padding-left: 7%;'><label style='color:white;margin-bottom: 0px;'><b>Step 1</b>&nbsp;|&nbsp"+parentValue+"</label></div><div class='col-sm-6 col-md-6' style='border-top: 1px solid black; '></div></div>");
					}
					if(FlagCounter==2)
					{
						iconName="interview.png";
						parentValue=Utility.getLocaleValuePropByKey("lblInterview", locale);
						rdiv.append("<div class='row'><div class='col-sm-6 col-md-6' style='background:#F06292;padding-left: 7%;'><label style='color:white;margin-bottom: 0px;'><b>Step 2</b>&nbsp;|&nbsp"+parentValue+"</label></div><div class='col-sm-6 col-md-6' style='border-top: 1px solid black; '></div></div>");
					}
					else if(FlagCounter==3)
					{
						iconName="vetting.png";
						parentValue=Utility.getLocaleValuePropByKey("lblVetting", locale);
						rdiv.append("<div class='row'><div class='col-sm-6 col-md-6' style='background:#8560A8;padding-left: 7%;'><label style='color:white;margin-bottom: 0px;'><b>Step 3</b>&nbsp;|&nbsp"+parentValue+"</label></div><div class='col-sm-6 col-md-6' style='border-top: 1px solid black; '></div></div>");
					}
									
					rdiv.append(printChildStatusCG(schoolIds,mapNote,mapJFH,mapHistrory,outerJob,showStatus,isOffer,statusMaster,teacherDetails,request,isJobAssessment, tree.getChildren(),tree,lstDistrictMaxFitScores,lstTeacherStatusHistoryForJob,lstTeacherStatusScores,mapSschool,mapSecSchool,mapJobSecSchool,jobWisePanelStatusList,userSession,lstTeacherStatusNotes,panelScheduleList,jftObj,schoolObj,clickFlag,teacherId,districtKeyContactList,onlineActivityStatusName,iconName,maximum,FlagCounter));
				}
				
			}
		}
		//rdiv.append("</td>");
		
		StatusMaster statusHired=findStatusByShortName(lstStatusMaster,"hird");
		StatusMaster statusDeclined=findStatusByShortName(lstStatusMaster,"dcln");
		StatusMaster statusRemoved=findStatusByShortName(lstStatusMaster,"rem");
		int fitScore=getMaxFitInStatusScore(lstTeacherStatusScores,statusHired,null);
		if(fitScore==0){
			fitScore=getMaxFitScore(lstDistrictMaxFitScores,statusHired,null);
		}
		boolean hiredFlag=false,declineFlag=false,rejectFlag=false;
		if(entityType==3){
			StatusPrivilegeForSchools sPFSObj=null;
			if(mapSschool.size()>0)
				sPFSObj=mapSschool.get(statusHired.getStatusId());
			if(sPFSObj!=null && sPFSObj.getCanSchoolSetStatus()!=null && sPFSObj.getCanSchoolSetStatus()==0){
				hiredFlag=true;
			}
			if(mapSschool.size()>0)
				sPFSObj=mapSschool.get(statusDeclined.getStatusId());
			if(sPFSObj!=null && sPFSObj.getCanSchoolSetStatus()!=null && sPFSObj.getCanSchoolSetStatus()==0){
				declineFlag=true;
			}
			if(mapSschool.size()>0)
				sPFSObj=mapSschool.get(statusRemoved.getStatusId());
			if(sPFSObj!=null && sPFSObj.getCanSchoolSetStatus()!=null && sPFSObj.getCanSchoolSetStatus()==0){
				rejectFlag=true;
			}
		}
		//rdiv.append("<td style='vertical-align:top;'>");
		Integer hiring_counter=0;
		parentValue=Utility.getLocaleValuePropByKey("lblHiring", locale);
		rdiv.append("<div class='row'><div class='col-sm-6 col-md-6' style='background:#22C589;padding-left: 6%;'><label style='color:white;margin-bottom: 0px;'><b>Step 4</b>&nbsp;|&nbsp"+parentValue+"</label></div><div class='col-sm-6 col-md-6' style='border-top: 1px solid black; '></div></div>");
		rdiv.append("<div class='row' style='width:94%;margin-left:50px;margin-right:0px;margin-top: 10px;margin-bottom: 25px;'>");
		rdiv.append("<div class='col-sm-1 col-md-1 leftrightwidth' style='padding-left: 0px;'><div><img src=\"images/hirings.png\" style='width:40px;background-color: white;'/></div></div>");
		rdiv.append("<div class='col-sm-10 col-md-10' style='width:715px;'><div class='childdiv'>");
        if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId()==1){
			if(!hiredFlag){
				rdiv.append("<a href='#' onclick=\" setStatusTilteFor2ndDiv('"+teacherDetails+"','Completed Onboarding Process','"+statusHired.getStatusShortName()+"'); jWTeacherStatusNotesOnOpen('"+fitScore+"','"+statusHired.getStatusId()+"',0);\">");
				String statusStr=teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+statusHired.getStatusId()+"##0";
				String waived=mapHistrory.get(statusStr);
				if(waived!=null && waived.equalsIgnoreCase("W")){
					hiring_counter++;
					rdiv.append("<img style='width:40px;background-color: white;' src=\"images/yellowRef.png\"/>");
				}else if(getStatusCheck(lstTeacherStatusHistoryForJobActive,statusHired,null)){
					hiring_counter++;
					rdiv.append("<img style='width:40px;background-color: white;' src=\"images/activate.png\"/>");
				}else{
					if(isHireDate){
						hiring_counter++;
						rdiv.append("<a href='#' onclick=\"offerReadyMsg(1,'The Job has posted on "+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobStartDate())+". Hiring for this job will start from "+Utility.convertDateAndTimeToUSformatOnlyDate(hireDate)+".');\"><img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/></a>");
					}else{
						hiring_counter++;
						rdiv.append("<img style='width:40px;background-color: white;' src=\"images/active_e.png\"/>");
					}
				}
				fitScore=getMaxFitInStatusScore(lstTeacherStatusScores,statusDeclined,null);
				if(fitScore==0){
					fitScore=getMaxFitScore(lstDistrictMaxFitScores,statusDeclined,null);
				}	
				rdiv.append("</a>");
			}else{
				String statusStr=teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+statusHired.getStatusId()+"##0";
				String waived=mapHistrory.get(statusStr);
				if(waived!=null && waived.equalsIgnoreCase("W")){
					hiring_counter++;
					rdiv.append("<img src=\"images/yellowRef.png\"/>");
				}else if(getStatusCheck(lstTeacherStatusHistoryForJobActive,statusHired,null)){
					hiring_counter++;
					rdiv.append("<img style='width:40px;background-color: white;' src=\"images/activate.png\"/>");
				}else{
					if(isHireDate){
						hiring_counter++;
						rdiv.append("<a href='#' onclick=\"offerReadyMsg(1,'The Job has posted on "+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobStartDate())+". Hiring for this job will start from "+Utility.convertDateAndTimeToUSformatOnlyDate(hireDate)+".');\"><img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/></a>");
					}else{
						hiring_counter++;
						rdiv.append("<img style='width:40px;background-color: white;' src=\"images/active_e.png\"/>");
					}
				}
			}
			rdiv.append("<br/>Completed Onboarding Process");
		}else{
			if((isMiami && role==7)||(isMiami==false && !hiredFlag)){
				rdiv.append("<a href='#' onclick=\" setStatusTilteFor2ndDiv('"+teacherDetails+"','"+statusHired.getStatus()+"','"+statusHired.getStatusShortName()+"'); jWTeacherStatusNotesOnOpen('"+fitScore+"','"+statusHired.getStatusId()+"',0);\">");
				if(getStatusCheck(lstTeacherStatusHistoryForJobActive,statusHired,null)){
					hiring_counter++;
					rdiv.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
				}else{
					if(isHireDate){
						hiring_counter++;
						rdiv.append("<a href='#' onclick=\"offerReadyMsg(1,'The Job has posted on "+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobStartDate())+". Hiring for this job will start from "+Utility.convertDateAndTimeToUSformatOnlyDate(hireDate)+".');\"><img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/></a>");
					}else{
						hiring_counter++;
						rdiv.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
					}
				}
				fitScore=getMaxFitInStatusScore(lstTeacherStatusScores,statusDeclined,null);
				if(fitScore==0){
					fitScore=getMaxFitScore(lstDistrictMaxFitScores,statusDeclined,null);
				}	
				rdiv.append("</a>");
			}else{
				if(getStatusCheck(lstTeacherStatusHistoryForJobActive,statusHired,null)){
					hiring_counter++;
					rdiv.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
				}else{
					if(isHireDate){
						hiring_counter++;
						rdiv.append("<a href='#' onclick=\"offerReadyMsg(1,'The Job has posted on "+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobStartDate())+". Hiring for this job will start from "+Utility.convertDateAndTimeToUSformatOnlyDate(hireDate)+".');\"><img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/></a>");
					}else{
						hiring_counter++;
							rdiv
									.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
					}
				}
			}
			rdiv.append("<br/><label style='margin-top:6px;'>"+Utility.getLocaleValuePropByKey("msgHired1", locale)+"</label>");
		}
			
		rdiv.append("</div>");

		//rdiv.append("</table>");                                      //By Deepak 
		if(jobOrder.getHeadQuarterMaster()==null){
			//rdiv.append("");
			rdiv.append("<div class='childdiv'>");
			if(!declineFlag){
				rdiv.append("<a href='#' onclick=\" setStatusTilteFor2ndDiv('"+teacherDetails+"','"+statusDeclined.getStatus()+"','"+statusDeclined.getStatusShortName()+"'); jWTeacherStatusNotesOnOpen('"+fitScore+"','"+statusDeclined.getStatusId()+"',0);\">");
				if(swcsMap.size()>0 && statusMasterForDcln==null){
					try{
						SchoolWiseCandidateStatus sWise=swcsMap.get(statusDeclined.getStatusId());
						if(sWise!=null && sWise.getUserMaster().getUserId().equals(userSession.getUserId())){
							hiring_counter++;
							rdiv.append("<img style='width:40px;background-color: white;' src=\"images/declined_d.png\"/>");
						}else{
							hiring_counter++;
							rdiv.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
						}
					}catch(Exception e){
						hiring_counter++;
						rdiv.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
					}
				}else{
					String statusStr=teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+statusHired.getStatusId()+"##0";
					String waived=mapHistrory.get(statusStr);
					if(waived!=null && waived.equalsIgnoreCase("W")){
						hiring_counter++;
						rdiv.append("<img style='width:40px;background-color: white;' src=\"images/yellowRef.png\"/>");
					}else if(getStatusCheck(lstTeacherStatusHistoryForJobActive,statusDeclined,null)){
						hiring_counter++;
						rdiv.append("<img style='width:40px;background-color: white;' src=\"images/declined_d.png\"/>");
					}else{
						hiring_counter++;
						rdiv.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
					}
				}
				fitScore=getMaxFitInStatusScore(lstTeacherStatusScores,statusRemoved,null);
				if(fitScore==0){
					fitScore=getMaxFitScore(lstDistrictMaxFitScores,statusRemoved,null);
				}
				rdiv.append("</a>");
			}else{
				if(swcsMap.size()>0 && statusMasterForDcln==null){
					try{
						SchoolWiseCandidateStatus sWise=swcsMap.get(statusDeclined.getStatusId());
						if(sWise!=null && sWise.getUserMaster().getUserId().equals(userSession.getUserId())){
							hiring_counter++;
							rdiv.append("<img style='width:40px;background-color: white;' src=\"images/declined_d.png\"/>");
						}else{
							hiring_counter++;
							rdiv.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
						}
					}catch(Exception e){
						hiring_counter++;
						rdiv.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
					}
				}else{
					String statusStr=teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+statusDeclined.getStatusId()+"##0";
					String waived=mapHistrory.get(statusStr);
					if(waived!=null && waived.equalsIgnoreCase("W")){
						hiring_counter++;
						rdiv.append("<img style='width:40px;background-color: white;' src=\"images/yellowRef.png\"/>");
					}else if(getStatusCheck(lstTeacherStatusHistoryForJobActive,statusDeclined,null)){
						hiring_counter++;
						rdiv.append("<img style='width:40px;background-color: white;' src=\"images/declined_d.png\"/>");
					}else{
						hiring_counter++;
						rdiv.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
					}
				}
			}
			rdiv.append("<br/><label style='margin-top:6px;'>"+Utility.getLocaleValuePropByKey("lblDeclined", locale)+"</label>");
			rdiv.append("</div>");
			//rdiv.append("</table>");
		}
		//rdiv.append("</td></tr></table></div>");
		
		//rdiv.append("<div class='col-sm-1 col-md-1'><img src=\"images/decline.png\"/>fghgfgfhgfhfghfg</div></div>");
		//rdiv.append("<div class='col-sm-2 col-md-2'>");
		//rdiv.append("<table  border=0 bordercolor='green' width='100%'><tr>");
		rdiv.append("<div class='childdiv'>");
		if(!rejectFlag){
			rdiv.append("<a href='#' onclick=\"setStatusTilteFor2ndDiv('"+teacherDetails+"','"+statusRemoved.getStatus()+"','"+statusRemoved.getStatusShortName()+"'); jWTeacherStatusNotesOnOpen('"+fitScore+"','"+statusRemoved.getStatusId()+"',0);\">");
			if(swcsMap.get(statusRemoved.getStatusId())!=null){
				hiring_counter++;
				rdiv.append("<img  style='width:40px;background-color: white;' src=\"images/removed_d.png\"/>");
			}else{
				String statusStr=teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+statusRemoved.getStatusId()+"##0";
				String waived=mapHistrory.get(statusStr);
				if(waived!=null && waived.equalsIgnoreCase("W")){
					hiring_counter++;
					rdiv.append("<img style='width:40px;background-color: white;' src=\"images/yellowRef.png\"/>");
				}else if(getStatusCheck(lstTeacherStatusHistoryForJobActive,statusRemoved,null)){
					hiring_counter++;
					rdiv.append("<img style='width:40px;background-color: white;' src=\"images/removed_d.png\"/>");
				}else{
					hiring_counter++;
					rdiv.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
				}
			}
			rdiv.append("</a>");
		}else{

			if(swcsMap.get(statusRemoved.getStatusId())!=null){
				hiring_counter++;
				rdiv.append("<img style='width:40px;background-color: white;' src=\"images/removed_d.png\"/>");
			}else{
				String statusStr=teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+statusRemoved.getStatusId()+"##0";
				String waived=mapHistrory.get(statusStr);
				if(waived!=null && waived.equalsIgnoreCase("W")){
					hiring_counter++;
					rdiv.append("<img style='width:40px;background-color: white;' src=\"images/yellowRef.png\"/>");
				}else if(getStatusCheck(lstTeacherStatusHistoryForJobActive,statusRemoved,null)){
					hiring_counter++;
					rdiv.append("<img style='width:40px;background-color: white;' src=\"images/removed_d.png\"/>");
				}else{
					hiring_counter++;
					rdiv.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
				}
			}

		}
		//****************************** Hiring Counter *******************************************************
		System.out.println("Hiring Counter ::::::: "+hiring_counter);		
		/*Integer max_value=Integer.parseInt(maximum.get(0).toString());
		Integer max_width;
		Integer leftrightwidth;
		if(max_value>hiring_counter)
			max_width=715/(max_value-1);
		else
			max_width=715/hiring_counter;
		
		max_width-=(max_value*3);
		
		if(max_value>5)
		{
		      leftrightwidth=max_width/(max_value/3);
		}
		else
		{
			leftrightwidth=max_width/4;
		}
		*/
		
		Integer max_value;
		 max_value=Integer.parseInt(maximum.get(0).toString());
		Integer max_width;
		Integer leftrightwidth=6;
		
		if(max_value>hiring_counter)
			max_width=100/(max_value-1);
		else
			max_width=100/hiring_counter;
		System.out.println(max_width+"<---------no_hiring------->"+hiring_counter);
		if(max_width/3>8)
		{
			leftrightwidth=max_width/(max_value);
		}
		
		System.out.println(" No OF node :::::::::::::: "+max_value+" ********** Width *********"+max_width+" left Right width ::::"+leftrightwidth);
		rdiv.append("<style>.childdiv{text-align:center;float:left;width: "+max_width+"%;word-break: break-word;}.leftrightwidth{width: "+leftrightwidth+"%;}</style>");
				
		//rdiv.append("<style>.childdiv{text-align:center;float:left;width: "+max_width+"px;word-break: break-word;}.leftrightwidth{width: "+leftrightwidth+"px;}</style>");
		
		
		//*****************************************************************************************************
		rdiv.append("<br/><label style='margin-top:6px;'>"+statusRemoved.getStatus()+"</label>");
		//rdiv.append("</td>");
		//rdiv.append("</tr></table>");
		rdiv.append("</div></div>");
		rdiv.append("<div class='col-sm-1 col-md-1 leftrightwidth' align='right' style='padding-right:0px;'>");
		//rdiv.append("<table  border=0 bordercolor='red'><tr>");
		//rdiv.append("<div class='childdiv'>");
		rdiv.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br/><label style='margin-top:6px;margin-right:-9px;'>"+Utility.getLocaleValuePropByKey("optWithdrawn", locale)+"</label>");	
		rdiv.append("</div></div>");
		//rdiv.append("</table>");
		//rdiv.append("</td>");
		//rdiv.append("</tr>");
		//rdiv.append("</table></td></tr></table>");
	}catch (Exception e) {
		e.printStackTrace();
	}		
	//rdiv.append("<script>$('img').load(function(){ alert('Image loaded.');chart_done(50);alert('done!!!!!!!!!!');});</script>");
	return rdiv.toString();
}
	public boolean getStatusCheck(List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		boolean status=false;
		try {
			if(lstTeacherStatusHistoryForJob!=null)
				for(TeacherStatusHistoryForJob tSH: lstTeacherStatusHistoryForJob){
					if(statusMaster!=null){
						if(tSH.getStatusMaster()!=null){
							if(!tSH.getStatus().equalsIgnoreCase("W") && tSH.getStatusMaster().getStatusId().equals(statusMaster.getStatusId())){
								status=true;
								break;
							}
						}
					}else{
						if(tSH.getSecondaryStatus()!=null){
							if(!tSH.getStatus().equalsIgnoreCase("W") && tSH.getSecondaryStatus().getSecondaryStatusId().equals(secondaryStatus.getSecondaryStatusId())){
								status=true;
								break;
							}
						}
					}

				}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public String getPrimaryStatus(List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob)
	{
		String status=null;
		try {
			if(lstTeacherStatusHistoryForJob!=null)
				for(TeacherStatusHistoryForJob tSH: lstTeacherStatusHistoryForJob){
					if(tSH.getStatusMaster()!=null){
						if(tSH.getStatus().equalsIgnoreCase("A") && tSH.getStatusMaster().getStatusShortName().equalsIgnoreCase("hird")){
							status="EH";
						}else if(tSH.getStatus().equalsIgnoreCase("A") && tSH.getStatusMaster().getStatusShortName().equalsIgnoreCase("dcln")){
							status="ED";
						}else if(tSH.getStatus().equalsIgnoreCase("A") && tSH.getStatusMaster().getStatusShortName().equalsIgnoreCase("rem")){
							status="ER";
						}
					}
				}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	/*========= Sekhar :  print Child Status CG ======================*/
	@Transactional(readOnly=true)
	public String printChildStatusCG(String schoolIds,Map<String,String> mapNote, Map<String,UserMaster> mapJFH,Map<String,String> mapHistrory,boolean outerJob,boolean showStatus,boolean isOffer,StatusMaster statusMaster,String teacherDetails,HttpServletRequest request,boolean isJobAssessment,List<SecondaryStatus> lstuserChildren,SecondaryStatus tree,List<DistrictMaxFitScore> lstDistrictMaxFitScores,List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob,List<TeacherStatusScores> lstTeacherStatusScores,Map<Integer,StatusPrivilegeForSchools> mapSschool,Map<Integer,StatusPrivilegeForSchools> mapSecSchool,Map<Integer,JobCategoryWiseStatusPrivilege> mapJobSecSchool,List<JobWisePanelStatus> jobWisePanelStatusList,UserMaster userMaster,List<TeacherStatusNotes> lstTeacherStatusNotes,List<PanelSchedule> panelScheduleList,JobForTeacher jobForTeacher,SchoolMaster schoolObj,int clickFlag,String teacherId,List<DistrictKeyContact> districtKeyContactList,String onlineActivityStatusName,String iconName,List maximum,int FlagCounter)
	{
		//System.out.println(":::::::::::::::::: teacherId ::::::::::::::::::::: Testing>>>>>>>"+teacherId);
		
		List<PanelAttendees> panelAttendeeList=new ArrayList<PanelAttendees>();
		StringBuffer sbchild=new StringBuffer();
		Collections.sort(lstuserChildren,SecondaryStatus.secondaryStatusOrder);
		sbchild.append("<div class='row' style='width:94%;margin-left:50px;margin-right:0px;margin-top: 10px;margin-bottom: 25px;'><div class='col-sm-1 col-md-1 steplineslc leftrightwidth' style='padding-left: 0px;'><div><img style='width:40px;background-color: white;' src=\"images/"+iconName+"\"/></div></div>");
		//sbchild.append("<div class='col-sm-10 col-md-10 steplineslc'><table  border=0 bordercolor='green' width='100%'><tr>");
		sbchild.append("<div class='col-sm-10 col-md-10 steplineslc' style='width:715px;'>");
		int banchmarkStatus=0;
		boolean priviFlag=false;
		boolean  isPanelExist=false;
		boolean offerReadyFlag=false;
		boolean isMiami = false;
		int counterFlag=0;
		int counterStore=0;
		JobWisePanelStatus jobWisePanelStatusObjCheck=null;
		CandidateGridService cgService=new CandidateGridService();	
	     // sbchild.append("<td style='text-align:center;height:100px;'><img src=\"images/green.png\"/><br/>"+parentValue+"</td>");
		for(SecondaryStatus subfL: lstuserChildren ){
			counterFlag++;		
			//if((isJobAssessment && subfL.getSecondaryStatusName().equalsIgnoreCase("JSI")) || !subfL.getSecondaryStatusName().equalsIgnoreCase("JSI")){
			if(subfL.getStatus().equalsIgnoreCase("A")){
				if(subfL.getStatusMaster()!=null){
					if(subfL.getStatusMaster().getBanchmarkStatus()!=null){
						banchmarkStatus=subfL.getStatusMaster().getBanchmarkStatus();
					}
				}
				if(banchmarkStatus==0){
					if(subfL.getStatusMaster()!=null){
						if(subfL.getStatusMaster().getStatusShortName().equals("apl")){
							if(statusMaster!=null && statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
								//sbchild.append("<td style='text-align:center;'>");
								sbchild.append("<div class='childdiv'>");
								sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/><br/><label style='margin-top:6px;'>"+Utility.getLocaleValuePropByKey("optIncomlete", locale)+"</label>");
								sbchild.append("</div>");
							}else if(statusMaster!=null && statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
								sbchild.append("<div class='childdiv'>");
								sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/><br/><label style='margin-top:6px;'>"+Utility.getLocaleValuePropByKey("optTOut", locale)+"</label>");
								sbchild.append("</div>");
							}else{
								sbchild.append("<div class='childdiv'>");
								sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/><br/><label style='margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");
								sbchild.append("</div>");
							}
						}
					}
				}else if(subfL.getStatusMaster()==null)
				{
					if((isJobAssessment && subfL.getSecondaryStatusName().equalsIgnoreCase("JSI")) || !subfL.getSecondaryStatusName().equalsIgnoreCase("JSI")){
						int fitScore=getMaxFitInStatusScore(lstTeacherStatusScores,null,subfL);
						if(fitScore==0){
							fitScore=getMaxFitScore(lstDistrictMaxFitScores,null,subfL);
						}
						SecondaryStatus subfLTmp=new SecondaryStatus();
						if(subfL.getSecondaryStatus_copy()!=null){
							subfLTmp=subfL.getSecondaryStatus_copy();
						}else{
							subfLTmp=subfL;
						}
						StatusPrivilegeForSchools sPFSObj=mapSecSchool.get(subfLTmp.getSecondaryStatusId());
						priviFlag=false;
						JobCategoryWiseStatusPrivilege jobSchool=mapJobSecSchool.get(subfL.getSecondaryStatusId());
						if(jobSchool!=null && !jobSchool.isStatusPrivilegeForSchools()){
							priviFlag=true;
						}else if(jobSchool==null && sPFSObj!=null && sPFSObj.getCanSchoolSetStatus()!=null && sPFSObj.getCanSchoolSetStatus()==0){
							priviFlag=true;
						}

						isMiami = false;
						try{
							if(jobForTeacher!=null && jobForTeacher.getJobId().getDistrictMaster().getDistrictId().equals(1200390))
								isMiami=true;
						}catch(Exception e){}
						boolean isSubstituteInstructionalJob = false;
						try{
							if(isMiami && jobForTeacher!=null && jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Substitute Instructional Positions"))
								isSubstituteInstructionalJob=true;
						}catch(Exception e){}
						sbchild.append("<div class='childdiv'>");
						int schoolId=0;
						String offerReady="";
						try{
							if(isMiami && subfL.getSecondaryStatusName()!=null && subfL.getSecondaryStatusName().equals("Offer Ready")){
								offerReady="Offer Ready";
								offerReadyFlag=true;
							}
						}catch(Exception e){}
						try{
							if(offerReady!=null && !offerReady.equals("")){
								schoolId=Integer.parseInt(schoolObj.getSchoolId()+"");
							}
						}catch(Exception e){}

						String reqNumber="";
						try{
							if(jobForTeacher!=null){
								if(jobForTeacher.getRequisitionNumber()!=null){
									reqNumber=jobForTeacher.getRequisitionNumber();
								}
							}
						}catch(Exception e){}
						Map<Integer,UserMaster> panelAttMap=new HashMap<Integer, UserMaster>();
						JobWisePanelStatus jobWisePanelStatusObj=null;
						jobWisePanelStatusObjCheck=null;
						isPanelExist=false;
						if(!priviFlag)
						{
							//Default,Green,GreenWithGreyDot,GreyWithGreenDot,Grey
							if(jobWisePanelStatusList.size()>0)
							{
								panelAttendeeList=new ArrayList<PanelAttendees>();
								for(JobWisePanelStatus jobWisePanelStatus:jobWisePanelStatusList)
								{
									if(jobWisePanelStatus!=null && jobWisePanelStatus.getSecondaryStatus()!=null && jobWisePanelStatus.getSecondaryStatus().getSecondaryStatusId().equals(subfL.getSecondaryStatusId()) && jobWisePanelStatus.getPanelStatus())
									{
										jobWisePanelStatusObj=jobWisePanelStatus;
										jobWisePanelStatusObjCheck=jobWisePanelStatus;
										if(panelScheduleList.size() > 0)
										{
											for(PanelSchedule panelSchedule:panelScheduleList)
											{
												if(panelSchedule!=null && panelSchedule.getJobWisePanelStatus().getJobPanelStatusId().equals(jobWisePanelStatus.getJobPanelStatusId()))
												{
													isPanelExist=true;
													panelAttendeeList=new ArrayList<PanelAttendees>();
													panelAttendeeList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
													debugPrintln("", "panelAttendees "+panelAttendeeList.size());
												}
											}
										}
									}
								}
							}
							///////////////Start Offer Ready as per Change /////////////////
							int offerReadyValue=2;
							try{
								if(jobForTeacher.getOfferReady()!=null){
									if(jobForTeacher.getOfferReady()){
										offerReadyValue=1;
									}else{
										offerReadyValue=0;
									}
								}
							}catch(Exception e){}
							DistrictMaster districtMaster=null;
							SchoolMaster schoolMaster=null;
							List<StafferMaster> sList=new ArrayList<StafferMaster>();
							List<PanelSchedule> pSList= new ArrayList<PanelSchedule>();

							try{
								if(jobForTeacher.getOfferReady()!=null){
									isOffer=true;	
								}
							}catch(Exception e){}
							//Panel is created
							if(isMiami && isSubstituteInstructionalJob==false && userMaster!=null && jobWisePanelStatusObj!=null && offerReady!=null && !offerReady.equals("")){
								try{
									for(PanelAttendees panelAttendees: panelAttendeeList){
										panelAttMap.put(panelAttendees.getPanelInviteeId().getUserId(),panelAttendees.getPanelInviteeId());									
									}
								}catch(Exception e){}
								pSList=panelScheduleDAO.findPanelSchedules(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
								if(userMaster.getEntityType()==3){
									districtMaster=userMaster.getDistrictId();
									schoolMaster=userMaster.getSchoolId();
									sList=stafferMasterDAO.findStafferMasterList(districtMaster,schoolMaster);
								}
							}

							////////////////////////// End Offer Ready as per Change///////////////////////////////
							if(showStatus){
								if(isMiami && offerReady!=null && !offerReady.equals("")){

									if(isOffer){

										List<TeacherStatusHistoryForJob> lstTSHJob = teacherStatusHistoryForJobDAO.findByTeacherStatusHistorySelected(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
										List<SecondaryStatus> listSecondaryStatus = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithOutStatusAndHB(jobForTeacher.getJobId());
										Map<String,Integer> cmpMap = new HashMap<String, Integer>();
										//System.out.println("lstTSHJob:::"+lstTSHJob.size());
										for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : lstTSHJob) {
											if(teacherStatusHistoryForJob.getSecondaryStatus()!=null)
											{
												//System.out.println("SecondaryStatus: "+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName());
												cmpMap.put(teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName(),1);
											}
											if(teacherStatusHistoryForJob.getStatusMaster()!=null && !teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equalsIgnoreCase("vcomp"))
											{
												//System.out.println("Status: "+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName());
												cmpMap.put(teacherStatusHistoryForJob.getStatusMaster().getStatusShortName(),1);
											}
										}

										Map<String,Integer> allStsMap = new HashMap<String, Integer>();
										for (SecondaryStatus secondaryStatus : listSecondaryStatus) {
											if(secondaryStatus.getStatusMaster()!=null)
											{
												//System.out.println("Stauts: "+secondaryStatus.getStatusMaster().getStatusShortName());
												allStsMap.put(secondaryStatus.getStatusMaster().getStatusShortName(), 1);
											}
											else 
											{
												//System.out.println("SecondaryStatus: "+secondaryStatus.getSecondaryStatusName());
												allStsMap.put(secondaryStatus.getSecondaryStatusName(), 1);
											}
										}
										if(allStsMap.size()>0){
											allStsMap.remove("vcomp");
											allStsMap.remove("apl");
											allStsMap.remove("Offer Ready");
											allStsMap.remove("icomp");
											allStsMap.remove("vlt");
											allStsMap.remove("hird");
											allStsMap.remove("dcln");
											allStsMap.remove("rem");
											allStsMap.remove("widrw");
											cmpMap.remove("Offer Ready");
											cmpMap.remove("hird");
											cmpMap.remove("dcln");
											cmpMap.remove("rem");
											cmpMap.remove("widrw");
											cmpMap.remove("vcomp");
											cmpMap.remove("icomp");
											cmpMap.remove("vlt");
											cmpMap.remove("apl");
										}
										//System.out.println("Comp: "+cmpMap.size());
										//System.out.println("All: "+allStsMap.size());

										if(cmpMap.size()!=allStsMap.size())
										{
											sbchild.append("<a href='#' onclick=\"offerReadyMsg(0,'"+Utility.getLocaleValuePropByKey("msgManageAjax3", locale)+"');\">");
										}else
										{
											if(isSubstituteInstructionalJob)
											{
												//sbchild.append("<a href='#' onclick=\"offerReadyMsg(0,'This is Substitute Instructional Position Job');\">");
												sbchild.append("<a href='#' onclick=\"setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth'); checkReqNumber('"+schoolId+"','"+offerReady+"','"+reqNumber+"'); jWTeacherStatusNotesOnOpen('"+fitScore+"',0,'"+subfL.getSecondaryStatusId()+"');\">");
											}else{

												if(districtMaster!=null && schoolMaster!=null && jobWisePanelStatusObj!=null){
													if(outerJob){
														sbchild.append("<a href='#' onclick=\"offerReadyMsg(0,'"+Utility.getLocaleValuePropByKey("lblPara", locale) +"');\">");
													}else{
														List<PanelSchedule> pSSList=panelScheduleDAO.findPanelSchedulesByTJSAndStatus(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),districtMaster,null,jobWisePanelStatusObj);
														//System.out.println("pSSList Size:::>>"+pSSList.size());
														int isCExistForPanel=0;
														for (PanelSchedule panelSchedule : pSSList) {
															isCExistForPanel=1;
															if(panelSchedule.getSchoolId()!=null){
																if(panelSchedule.getSchoolId().equals(schoolMaster)){
																	isCExistForPanel=2;
																	break;
																}
															}
														}
														if(isCExistForPanel==0 && sList.size()==0 && districtKeyContactList.size()==0){ // No Staffer
															sbchild.append("<a href='#' onclick=\"offerReadyMsg(0,'"+Utility.getLocaleValuePropByKey("msgManageAjax4", locale)+"clientservices@teachermatch.net.');\">");
														}else if(isCExistForPanel!=1){
															sbchild.append("<a href='#' onclick=\" setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth'); checkReqNumber('"+schoolId+"','"+offerReady+"','"+reqNumber+"'); jWTeacherStatusNotesOnOpen('"+fitScore+"',0,'"+subfL.getSecondaryStatusId()+"');\">");
														}else  {// Another School Created
															sbchild.append("<a href='#' onclick=\"offerReadyMsg(0,'"+Utility.getLocaleValuePropByKey("msgManageAjax5", locale)+"');\">");
														}
													}
												}else{ 
													if(userMaster.getEntityType()==2){
														if(panelAttMap.get(userMaster.getUserId())!=null && offerReadyValue!=2){
															sbchild.append("<a href='#' onclick=\" setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth');checkReqNumber('"+schoolId+"','"+offerReady+"','"+reqNumber+"'); jWTeacherStatusNotesOnOpen('"+fitScore+"',0,'"+subfL.getSecondaryStatusId()+"');\">");
														}else  if(offerReadyFlag && isPanelExist==false && jobWisePanelStatusObjCheck!=null){//if(pSList.size()==0){ //Panel is not created
															sbchild.append("<a href='#' onclick=\"offerReadyMsg(0,'"+Utility.getLocaleValuePropByKey("msgManageAjax6", locale)+"');\">");
														}else if(pSList.size()!=0){//Panel made but school not offer ready
															//sbchild.append("<a href='#' onclick=\"offerReadyMsg(0,'The Principal has not yet made an offer to this candidate and hence you can not finalize this status.');\">");
															sbchild.append("<a href='#' onclick=\"finalizeOrNoForOfferReady(0);setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth'); checkReqNumber('"+schoolId+"','"+offerReady+"','"+reqNumber+"'); jWTeacherStatusNotesOnOpen('"+fitScore+"',0,'"+subfL.getSecondaryStatusId()+"');\">");
														}else{
															sbchild.append("<a href='#' onclick=\"setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth'); checkReqNumber('"+schoolId+"','"+offerReady+"','"+reqNumber+"'); jWTeacherStatusNotesOnOpen('"+fitScore+"',0,'"+subfL.getSecondaryStatusId()+"');\">");
														}
													}else{
														sbchild.append("<a href='#' onclick=\"setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth'); checkReqNumber('"+schoolId+"','"+offerReady+"','"+reqNumber+"'); jWTeacherStatusNotesOnOpen('"+fitScore+"',0,'"+subfL.getSecondaryStatusId()+"');\">");
													}
												}
											}
										}
									}else{

										sbchild.append("<a href='#' onclick=\"offerReadyMsg(0,'"+Utility.getLocaleValuePropByKey("headCand", locale)+" "+teacherDetails+" "+Utility.getLocaleValuePropByKey("msgManageAjax7", locale)+"');\">");
									}
								}else{
									if(offerReadyFlag==false && isPanelExist==false && jobWisePanelStatusObj!=null){
										sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'"+Utility.getLocaleValuePropByKey("msgManageAjax6", locale)+"');\">");
									}else{
										
										boolean isBeforeAllNotGreen=AcceptOrDeclineAjax.checkAllGreenBeforeCurrentStatus( cgService, jobForTeacher, jobForTeacher.getJobId(),secondaryStatusDAO,teacherStatusHistoryForJobDAO,subfL.getSecondaryStatusName());
										boolean isAlreadyOffered=AcceptOrDeclineAjax.checkAlreadyOfferOrNot(jobForTeacherDAO, jobForTeacher, userMaster,subfL.getSecondaryStatusName());
										if((isBeforeAllNotGreen))
										{
											sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'Please finalize all the other statuses before finalizing the �"+subfL.getSecondaryStatusName()+"�&nbsp;&nbsp;status');\">");
										}
										else if(isAlreadyOffered){
											sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'"+Utility.getLocaleValuePropByKey("headCand", locale)+" "+teacherDetails+" "+Utility.getLocaleValuePropByKey("msgManageAjax9", locale)+" �"+subfL.getSecondaryStatusName()+"�"+Utility.getLocaleValuePropByKey("msgManageAjax10", locale)+"');\">");
										}
										else{
												if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().toString().equalsIgnoreCase("1201470") && subfL.getSecondaryStatusName().equalsIgnoreCase("Offer Made") && userMaster.getEntityType()==3){
													boolean checkStatusClick=AcceptOrDeclineAjax.checkStatusGreen(jobForTeacher, teacherStatusHistoryForJobDAO, secondaryStatusDAO, new String[]{"Credential Review"});
													if(checkStatusClick){
														sbchild.append("<a href='#' onclick=\"setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth'); jWTeacherStatusNotesOnOpen('"+fitScore+"',0,'"+subfL.getSecondaryStatusId()+"');\">");
													}else{
														sbchild.append("");
													}
												}else
												sbchild.append("<a href='#' onclick=\"setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth'); jWTeacherStatusNotesOnOpen('"+fitScore+"',0,'"+subfL.getSecondaryStatusId()+"');\">");											
										}
									}
								}
							}

							String strStatusColor=getStatusColor(lstTeacherStatusHistoryForJob, jobWisePanelStatusList, panelAttendeeList, null,subfL, userMaster,lstTeacherStatusScores, lstTeacherStatusNotes,panelScheduleList);

							if(subfL.getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref")){
								/*	Set Color For Transcript */
								TeacherDetail teacherDetail 	=	teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
								List<TeacherAcademics> teacherAcademicsList				=	null;
								List<EligibilityVerificationHistroy> checkHistoryRecord = 	null;
								Criterion criteria1 = null;
								String verifiedStatus	=	null;
								boolean statusFailed	=	false;
								boolean statusWaived	=	false;
								boolean statusVerified	=	false;
								boolean statusPennding	=	false;
								boolean statusNull		=	false;
								criteria1 				= 	Restrictions.eq("teacherId", teacherDetail);
								teacherAcademicsList  	=  	teacherAcademicsDAO.findByCriteria(criteria1);
								EligibilityMaster eligibilityMaster		= 	eligibilityMasterDAO.findById(5, false, false);
								try{
									checkHistoryRecord 	= 	eligibilityVerificationHistroyDAO.findAllHistoryByJobIdAndTrans(jobForTeacher.getJobId(),teacherDetail,eligibilityMaster);
								}catch(Exception e){}
								if(teacherAcademicsList.size() > 0 && checkHistoryRecord.size() > 0){

									for(TeacherAcademics teacherRecord:teacherAcademicsList){
										verifiedStatus = teacherRecord.getStatus()==null?"":teacherRecord.getStatus();
										if(verifiedStatus.equals("F")){statusFailed=true;}
										if(verifiedStatus.equals("W")){statusWaived=true;}
										if(verifiedStatus.equals("V")){statusVerified=true;}
										if(verifiedStatus.equals("P")){statusPennding=true;}
										if(verifiedStatus.equals("")){statusNull=true;}
									}

									if(statusNull){

										if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("Green")){
											try{
												String statusStr=jobForTeacher.getTeacherId().getTeacherId()+"##"+jobForTeacher.getJobId().getJobId()+"##0"+"##"+subfL.getSecondaryStatusId();
												UserMaster userObj=mapJFH.get(statusStr);
												if((userObj!=null && userObj.getSchoolId()!=null && userObj.getSchoolId().getSchoolId().equals(userMaster.getSchoolId().getSchoolId())&& userObj.getEntityType()==3) || userObj.getEntityType()==2){
													sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
												}else{
													String noteVal=mapNote.get(statusStr);
													if(noteVal!=null && noteVal.equals("Y")){
														sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
													}else{	
														if(schoolIds.indexOf("||"+userMaster.getSchoolId().getSchoolId()+"||")==-1){
															sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
														}else{
															sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greygreenb_b.png\"/>");
														}
													}
												}
											}catch(Exception e){
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
											}
										}else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("GreenWithGreyDot"))
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greengrey_y.png\"/>");
										else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("GreyWithGreenDot"))
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greygreen_n.png\"/>");
										else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("Grey"))
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
										else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("Default"))
										{
											if(getStatusCheck(lstTeacherStatusHistoryForJob,null,subfL)){
												try{
													String statusStr=jobForTeacher.getTeacherId().getTeacherId()+"##"+jobForTeacher.getJobId().getJobId()+"##0"+"##"+subfL.getSecondaryStatusId();
													UserMaster userObj=mapJFH.get(statusStr);
													if((userObj!=null && userObj.getSchoolId()!=null && userObj.getSchoolId().getSchoolId().equals(userMaster.getSchoolId().getSchoolId())&& userObj.getEntityType()==3) || userObj.getEntityType()==2){
														sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
													}else{
														String noteVal=mapNote.get(statusStr);
														if(noteVal!=null && noteVal.equals("Y")){
															sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
														}else{	
															if(schoolIds.indexOf("||"+userMaster.getSchoolId().getSchoolId()+"||")==-1){
																sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
															}else{
																sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greygreenb_b.png\"/>");
															}
														}
													}
												}catch(Exception e){
													sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
												}
											}else{
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
											}
										}
									} else {
										/*  Status Set According to PNR */
										if(statusFailed){
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
										}else if(statusPennding==false && statusWaived==false && statusFailed==false && statusVerified){
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
										}else if(statusPennding==false && statusWaived && statusFailed==false && statusVerified==false){
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
										}else if(statusPennding==false && statusWaived && statusFailed==false && statusVerified){//both 
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
										}else{
											if(statusPennding){ 
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>"); //pendinf
											}
										}	
									}

								} else {
									if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("Green")){
										try{
											String statusStr=jobForTeacher.getTeacherId().getTeacherId()+"##"+jobForTeacher.getJobId().getJobId()+"##0"+"##"+subfL.getSecondaryStatusId();
											String waived=mapHistrory.get(statusStr);
											if(waived!=null && waived.equalsIgnoreCase("W")){
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/yellowRef.png\"/>");
											}else{
											UserMaster userObj=mapJFH.get(statusStr);
											if((userObj!=null && userObj.getSchoolId()!=null && userObj.getSchoolId().getSchoolId().equals(userMaster.getSchoolId().getSchoolId())&& userObj.getEntityType()==3) || userObj.getEntityType()==2){
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
											}else{
												String noteVal=mapNote.get(statusStr);
												if(noteVal!=null && noteVal.equals("Y")){
													sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
												}else{	
													if(schoolIds.indexOf("||"+userMaster.getSchoolId().getSchoolId()+"||")==-1){
														sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
													}else{
														sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greygreenb_b.png\"/>");
													}
												}
											}
										  }
										}catch(Exception e){
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
										}
									}else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("GreenWithGreyDot"))
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greengrey_y.png\"/>");
									else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("GreyWithGreenDot"))
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greygreen_n.png\"/>");
									else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("Grey"))
										sbchild.append("<img style='width:40px;background-color: white;'  src=\"images/unavailable.png\"/>");
									else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("Default"))
									{
										if(getStatusCheck(lstTeacherStatusHistoryForJob,null,subfL)){
											try{
												String statusStr=jobForTeacher.getTeacherId().getTeacherId()+"##"+jobForTeacher.getJobId().getJobId()+"##0"+"##"+subfL.getSecondaryStatusId();
												String waived=mapHistrory.get(statusStr);
												if(waived!=null && waived.equalsIgnoreCase("W")){
													sbchild.append("<img style='width:40px;background-color: white;' src=\"images/yellowRef.png\"/>");
												}else{
													UserMaster userObj=mapJFH.get(statusStr);
													if((userObj!=null && userObj.getSchoolId()!=null && userObj.getSchoolId().getSchoolId().equals(userMaster.getSchoolId().getSchoolId())&& userObj.getEntityType()==3) || userObj.getEntityType()==2){
														sbchild.append("<img style='width:40px;background-color: white;' src=\"images/green.png\"/>");
													}else{
													String noteVal=mapNote.get(statusStr);
													if(noteVal!=null && noteVal.equals("Y")){
														sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
													}else{	
														if(schoolIds.indexOf("||"+userMaster.getSchoolId().getSchoolId()+"||")==-1){
															sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
														}else{
															sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greygreenb_b.png\"/>");
														}
													}
												 }
											   }
											}catch(Exception e){
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
											}
										}else{
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
										}
									}
								}

								/*	End Section */

							} else if(subfL.getSecondaryStatusName().equalsIgnoreCase("e-References")){
								Integer contactStatus 				= 	0;
								Integer replyStatus					=	0;
								Integer totalRefHistory				=	0;
								Integer numberOftecherElectronic	=	0;
								Integer totalActiveRefChkQuestion 	= 	0;

								contactStatus 		= teacherElectronicReferencesHistoryDAO.findTotalReferenceContactHistory(jobForTeacher.getJobId().getDistrictMaster(), jobForTeacher.getTeacherId());
								replyStatus 		= teacherElectronicReferencesHistoryDAO.findTotalReferenceReplyHistory(jobForTeacher.getJobId().getDistrictMaster(), jobForTeacher.getTeacherId());
								totalRefHistory 	= teacherElectronicReferencesHistoryDAO.findAllReferenceHistory(jobForTeacher.getJobId().getDistrictMaster(), jobForTeacher.getTeacherId());
								numberOftecherElectronic =	teacherElectronicReferencesDAO.findTotalReference(jobForTeacher.getTeacherId());	
								List<DistrictPortfolioConfig> districtPortfolioConfig 	= 	null;
								DistrictPortfolioConfig portConfig 						= 	null;
								districtPortfolioConfig = 	districtPortfolioConfigDAO.getPortfolioConfigsByDistrictAndCandidate(jobForTeacher.getJobId().getDistrictMaster(),"I");
								Integer requiredReference = 0;
								try{
									if(districtPortfolioConfig!=null && districtPortfolioConfig.size()>0){
										portConfig = districtPortfolioConfig.get(0);
										requiredReference = portConfig.getReference();
									}
								}catch(Exception exception){
									exception.printStackTrace();
								}
								
								if(totalRefHistory == 0){
									// Default Colour ( Red )
									sbchild.append("<img style='width:40px;background-color: white;' src=\"images/redRef.png\"/>");               //redRefChk50.png
								} else {
									if(replyStatus >= requiredReference) {
										// Green Color Status
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
									} else {
										if(contactStatus > 0 && contactStatus < requiredReference){
											// Partial Invitation : Colour ( Yellow )
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/yellowRef.png\"/>");

										} else if(contactStatus >= requiredReference){
											// Invitation Sent All Response Recieved : Colour ( Blue )
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/blueRef.png\"/>");
										} else {
											// No Response Come : Color ( Gray )
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
										}
									} 
								}

							}else if(subfL.getSecondaryStatusName().equalsIgnoreCase("Online Activity") && onlineActivityStatusName!=null){
								//System.out.println("::::::::::::::::::::::::::::::: Online Activity =:::::::::::::::::::::::::::::::::::"+onlineActivityStatusName);
								boolean statusOnLine=false;
								if(getStatusCheck(lstTeacherStatusHistoryForJob,null,subfL)){
									try{
										String statusStr=jobForTeacher.getTeacherId().getTeacherId()+"##"+jobForTeacher.getJobId().getJobId()+"##0"+"##"+subfL.getSecondaryStatusId();
										UserMaster userObj=mapJFH.get(statusStr);
										if((userObj!=null && userMaster.getSchoolId()!=null &&  userObj.getSchoolId()!=null &&  userObj.getSchoolId().getSchoolId().equals(userMaster.getSchoolId().getSchoolId())&& userObj.getEntityType()==3) || userObj!=null && userObj.getEntityType()==2){
											statusOnLine=true;
										}else{
											String noteVal=mapNote.get(statusStr);
											if(noteVal!=null && noteVal.equals("Y")){
												statusOnLine=true;
											}else{	
												if(userMaster.getSchoolId()!=null){
													if(schoolIds.indexOf("||"+userMaster.getSchoolId().getSchoolId()+"||")==-1){
														statusOnLine=true;
													}
												}
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
								}
								if(statusOnLine){
									sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
								}else{
									if(onlineActivityStatusName.equalsIgnoreCase("Pending")){
										sbchild.append("<img  style='width:40px;background-color: white;'src=\"images/unavailable.png\"/>");
									}else if(onlineActivityStatusName.equalsIgnoreCase("Link Sent")){
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
									}else if(onlineActivityStatusName.equalsIgnoreCase("InComplete")){
										sbchild.append("<img  style='width:40px;background-color: white;' src=\"images/greyred_d.png\"/>");
									}else if(onlineActivityStatusName.equalsIgnoreCase("Completed")){
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greygreen_n.png\"/>");
									}else{
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
									}
								}
							} else {
								String statusStr=jobForTeacher.getTeacherId().getTeacherId()+"##"+jobForTeacher.getJobId().getJobId()+"##0"+"##"+subfL.getSecondaryStatusId();
								String waived=mapHistrory.get(statusStr);
								if(waived!=null && waived.equalsIgnoreCase("W")){
									sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
								}else{
								if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("Green")){
									try{
										UserMaster userObj=mapJFH.get(statusStr);
										if((userObj!=null && userObj.getSchoolId()!=null && userObj.getSchoolId().getSchoolId().equals(userMaster.getSchoolId().getSchoolId())&& userObj.getEntityType()==3) || userObj.getEntityType()==2){
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
										}else{
											String noteVal=mapNote.get(statusStr);
											if(noteVal!=null && noteVal.equals("Y")){
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
											}else{	
												if(schoolIds.indexOf("||"+userMaster.getSchoolId().getSchoolId()+"||")==-1){
													sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
												}else{
													sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greygreenb_b.png\"/>");
												}
											}
										}
									}catch(Exception e){
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
									}
								}else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("GreenWithGreyDot"))
									sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greengrey_y.png\"/>");
								else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("GreyWithGreenDot"))
									sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greygreen_n.png\"/>");
								else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("Grey"))
									sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
								else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("Default"))
								{
									if(getStatusCheck(lstTeacherStatusHistoryForJob,null,subfL)){
										try{
											UserMaster userObj=mapJFH.get(statusStr);
											if((userObj!=null && userObj.getSchoolId()!=null && userObj.getSchoolId().getSchoolId().equals(userMaster.getSchoolId().getSchoolId())&& userObj.getEntityType()==3) || userObj.getEntityType()==2){
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
											}else{
												String noteVal=mapNote.get(statusStr);
												if(noteVal!=null && noteVal.equals("Y")){
													sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
												}else{	
													if(schoolIds.indexOf("||"+userMaster.getSchoolId().getSchoolId()+"||")==-1){
														sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
													}else{
														sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greygreenb_b.png\"/>");
													}
												}
											}
										}catch(Exception e){
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
										}
									}else{
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
									}
								}
							  }
							}
							if(showStatus){
								sbchild.append("</a>");
							}
						}
						else
						{
							String statusStr=jobForTeacher.getTeacherId().getTeacherId()+"##"+jobForTeacher.getJobId().getJobId()+"##0"+"##"+subfL.getSecondaryStatusId();
							String waived=mapHistrory.get(statusStr);
							if(waived!=null && waived.equalsIgnoreCase("W")){
								sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
							}else{
							if(getStatusCheck(lstTeacherStatusHistoryForJob,null,subfL)){
								try{
									UserMaster userObj=mapJFH.get(statusStr);
									if((userObj!=null && userObj.getSchoolId()!=null && userObj.getSchoolId().getSchoolId().equals(userMaster.getSchoolId().getSchoolId())&& userObj.getEntityType()==3) || userObj.getEntityType()==2){
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
									}else{
										String noteVal=mapNote.get(statusStr);
										if(noteVal!=null && noteVal.equals("Y")){
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
										}else{	
											if(schoolIds.indexOf("||"+userMaster.getSchoolId().getSchoolId()+"||")==-1){
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
											}else{
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/greygreenb_b.png\"/>");
											}
										}
									}
								}catch(Exception e){
									sbchild.append("<img style='width:40px;background-color: white;' src=\"images/available.png\"/>");
								}
							}else{
								sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/>");
						        }
					        }
					     }
						sbchild.append("<br/><label style='margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");
						sbchild.append("</div>");
					}///////
				}
			}
			//}

		}
		//sbchild.append("</tr></table></div>");
		sbchild.append("</div>");
		counterStore=Integer.parseInt(maximum.get(0).toString());
		System.out.println("counterStore::::::::"+counterStore+"     Counter Flag:::"+counterFlag);
		if(counterStore<counterFlag)
		{
		maximum.add(0, counterFlag);
		}
		//System.out.println("Maximum Counter <<<<<<<<<<<<<<:::::::::>>>>>>>>>>>>>>>> "+counterFlag+"  Maximun Value :::::::::"+counterStore);
		JobOrder jobOrder = jobForTeacher.getJobId();
		TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
		for(SecondaryStatus subfL: lstuserChildren){
			String offerReady="";
			int schoolId=0;
			boolean isPhiladelphia=false;
			if(jobForTeacher.getJobId().getDistrictMaster()!=null && jobForTeacher.getJobId().getDistrictMaster().getDistrictId().equals(0000000)){
				isPhiladelphia=true;
			}
			
			if(isPhiladelphia && subfL.getSecondaryStatusName()!=null && subfL.getSecondaryStatusName().equals("No Evaluation Complete")){
				offerReadyFlag=true;
				offerReady="No Evaluation Complete";
			}
			try{
				if(offerReady!=null && !offerReady.equals("")){
					schoolId=Integer.parseInt(schoolObj.getSchoolId()+"");
				}
			}catch(Exception e){}
			
			//if((isJobAssessment && subfL.getSecondaryStatusName().equalsIgnoreCase("JSI")) || !subfL.getSecondaryStatusName().equalsIgnoreCase("JSI")){
			if(subfL.getStatus().equalsIgnoreCase("A"))
				if(subfL.getStatusMaster()!=null){
					if(subfL.getStatusMaster().getStatusShortName().equals("vcomp")||subfL.getStatusMaster().getStatusShortName().equals("ecomp")||subfL.getStatusMaster().getStatusShortName().equals("scomp"))
					{
						//System.out.println("sec: "+subfL.getSecondaryStatusName());
						/////////////////////////////////////////////////////////////////////////////
						String secondaryStatusNames = "";
						StatusMaster statusMasterNew = subfL.getStatusMaster(); 
						if(jobOrder.getDistrictMaster()!=null && statusMasterNew!=null){
							if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()!=null){
								if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()){
									String accessDPoints="";
									if(jobOrder.getDistrictMaster().getAccessDPoints()!=null)
										accessDPoints=jobOrder.getDistrictMaster().getAccessDPoints();

									if(accessDPoints.contains(""+statusMasterNew.getStatusId())){
										List<TeacherStatusHistoryForJob> lstTSHJob =teacherStatusHistoryForJobDAO.findByTeacherStatusHistorySelected(teacherDetail,jobOrder);
										List<SecondaryStatus> listSecondaryStatus = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithStatus(jobOrder,statusMasterNew);

										Map<Integer,SecondaryStatus> secMap=new HashMap<Integer, SecondaryStatus>();
										for(TeacherStatusHistoryForJob tSHObj : lstTSHJob){
											if(tSHObj.getSecondaryStatus()!=null)
												secMap.put(tSHObj.getSecondaryStatus().getSecondaryStatusId(),tSHObj.getSecondaryStatus());
										}
										if(listSecondaryStatus.size()>0){
											int counter=0;
											for(SecondaryStatus  obj : listSecondaryStatus.get(0).getChildren()){
												if(obj.getStatus().equalsIgnoreCase("A") && obj.getStatusMaster()==null){
													SecondaryStatus sec=secMap.get(obj.getSecondaryStatusId());
													if(sec==null){
														boolean isJobAssessment1=false;
														if(jobOrder.getIsJobAssessment()!=null){
															if(jobOrder.getIsJobAssessment()){
																isJobAssessment1=true;
															}
														}
														if((isJobAssessment1 && obj.getSecondaryStatusName().equalsIgnoreCase("JSI"))|| !obj.getSecondaryStatusName().equalsIgnoreCase("JSI")){
															if(counter==1){
																secondaryStatusNames+=", ";
															}
															counter=0;
															secondaryStatusNames+=obj.getSecondaryStatusName();
															counter++;
														}
													}
												}
											}
										}

									}
								}
							}
						}

						int offerAccepted=1;
						try{
							if(jobForTeacher!=null){
								if(jobForTeacher.getOfferAccepted()!=null){
									if(!jobForTeacher.getOfferAccepted()){
										offerAccepted=0;
									}
								}
							}
						}catch(Exception e){}
						int fitScore=getMaxFitInStatusScore(lstTeacherStatusScores,subfL.getStatusMaster(),null);
						if(fitScore==0){
							fitScore=getMaxFitScore(lstDistrictMaxFitScores,subfL.getStatusMaster(),null);
						}
						priviFlag=false;
						StatusPrivilegeForSchools sPFSObj=mapSschool.get(subfL.getStatusMaster().getStatusId());

						JobCategoryWiseStatusPrivilege jobSchool=mapJobSecSchool.get(subfL.getSecondaryStatusId());//jobSchool=mapJobSschool.get(subfL.getStatusMaster().getStatusId());
						if(jobSchool!=null && !jobSchool.isStatusPrivilegeForSchools()){
							priviFlag=true;
						}else if(sPFSObj!=null && sPFSObj.getCanSchoolSetStatus()!=null && sPFSObj.getCanSchoolSetStatus()==0){
							priviFlag=true;
						}	
						sbchild.append("<div class='col-sm-1 col-md-1 steplineslc leftrightwidth' align='right' style='padding-right:0px;'>");
						if(!priviFlag){	
							Map<Integer,UserMaster> panelAttMap=new HashMap<Integer, UserMaster>();
							JobWisePanelStatus jobWisePanelStatusObj=null;
							jobWisePanelStatusObjCheck=null;
							isPanelExist=false;
							if(jobWisePanelStatusList.size()>0)
							{
								panelAttendeeList=new ArrayList<PanelAttendees>();
								for(JobWisePanelStatus jobWisePanelStatus:jobWisePanelStatusList)
								{
									if(jobWisePanelStatus!=null && jobWisePanelStatus.getSecondaryStatus()!=null && jobWisePanelStatus.getSecondaryStatus().getSecondaryStatusId().equals(subfL.getSecondaryStatusId()) && jobWisePanelStatus.getPanelStatus())
									{
										jobWisePanelStatusObj=jobWisePanelStatus;
										jobWisePanelStatusObjCheck=jobWisePanelStatus;
										if(panelScheduleList.size() > 0)
										{
											for(PanelSchedule panelSchedule:panelScheduleList)
											{
												if(panelSchedule!=null && panelSchedule.getJobWisePanelStatus().getJobPanelStatusId().equals(jobWisePanelStatus.getJobPanelStatusId()))
												{
													isPanelExist=true;
													panelAttendeeList=new ArrayList<PanelAttendees>();
													panelAttendeeList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
													debugPrintln("", "panelAttendees "+panelAttendeeList.size());
												}
											}
										}
									}
								}
							}
							///////////////Start Offer Ready as per Change /////////////////
							int offerReadyValue=2;
							try{
								if(jobForTeacher.getOfferReady()!=null){
									if(jobForTeacher.getOfferReady()){
										offerReadyValue=1;
									}else{
										offerReadyValue=0;
									}
								}
							}catch(Exception e){}
							DistrictMaster districtMaster=null;
							SchoolMaster schoolMaster=null;
							List<StafferMaster> sList=new ArrayList<StafferMaster>();
							List<PanelSchedule> pSList= new ArrayList<PanelSchedule>();
							
							try{
								//System.out.println("isOffer===="+isOffer);
								if(jobForTeacher.getOfferReady()!=null){
									isOffer=true;	
								}
								//System.out.println("jobForTeacher.getOfferReady()===="+jobForTeacher.getOfferReady());
							}catch(Exception e){}
							//Panel is created
							if(isPhiladelphia && userMaster!=null && jobWisePanelStatusObj!=null && offerReady!=null && !offerReady.equals("")){
								try{
									for(PanelAttendees panelAttendees: panelAttendeeList){
										panelAttMap.put(panelAttendees.getPanelInviteeId().getUserId(),panelAttendees.getPanelInviteeId());									
									}
								}catch(Exception e){}
								pSList=panelScheduleDAO.findPanelSchedules(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
								if(userMaster.getEntityType()==3){
									districtMaster=userMaster.getDistrictId();
									schoolMaster=userMaster.getSchoolId();
									sList=stafferMasterDAO.findStafferMasterList(districtMaster,schoolMaster);
								}
							}

							////////////////////////// End Offer Ready as per Change///////////////////////////////
							//System.out.println("userMaster==="+userMaster);
							//System.out.println("districtMaster=="+districtMaster+" schoolMaster=="+schoolMaster+"  jobWisePanelStatusObj=="+jobWisePanelStatusObj);
							//System.out.println("showStatus=="+showStatus+"  offerREady==="+offerReady+" isOffer=="+isOffer);
							System.out.println("showStatus :::::::::::::::"+showStatus);
							if(showStatus){
								if(isPhiladelphia && offerReady!=null && !offerReady.equals("")){

									if(isOffer){																				
										//System.out.println("============================================================================================");
										Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
										Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
										List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
										Map<String,Integer> cmpMap = new HashMap<String, Integer>();
										Map<String,Integer> allStsMap = new HashMap<String, Integer>();
										for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
											if(teacherStatusHistoryForJob.getSecondaryStatus()!=null)
											{
												//System.out.println("SecondaryStatus: "+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName());
												cmpMap.put(teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName(),teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
											}
											if(teacherStatusHistoryForJob.getStatusMaster()!=null && !teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equalsIgnoreCase("vcomp"))
											{
												//System.out.println("Status: "+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName());
												cmpMap.put(teacherStatusHistoryForJob.getStatusMaster().getStatus(),teacherStatusHistoryForJob.getStatusMaster().getStatusId());
											}
										}

										List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
										LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
										Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
										try{
											int counter=0; 
											for(SecondaryStatus tree1: lstTreeStructure)
											{
												if(tree1.getSecondaryStatus()==null)
												{
													if(tree1.getChildren().size()>0){
														counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree1.getChildren(),statusHistMap,statusNameMap,counter);
													}
												}
											}												
										//System.out.println("============================================================================================");
											for(Map.Entry<String,String> entry:statusNameMap.entrySet()){												
												String id[]=entry.getKey().toString().split("##");
												if(entry.getValue().toString().equalsIgnoreCase("No Evaluation Complete") )
													break;
												if(!entry.getValue().toString().equalsIgnoreCase("Available")){
												//System.out.println("Key======"+entry.getKey()+"   Value==="+entry.getValue());
												allStsMap.put(entry.getValue().toString(), Integer.parseInt((id[0].equals("0")?id[1]:id[0])));
												}
												
											}
										}catch(Exception e){}
										
										for(Map.Entry<String,Integer> entry:cmpMap.entrySet()){											
											allStsMap.remove(entry.getKey());
										}
									
										//System.out.println("Comp: "+cmpMap.size());
										//System.out.println("All: "+allStsMap.size());
										
										if(jobWisePanelStatusObj!=null && allStsMap.size()!=0)
										{
											sbchild.append("<a href='#' onclick=\"offerReadyMsg(0,'"+Utility.getLocaleValuePropByKey("msgManageAjax11", locale)+"');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
										}else
										{
											//added By 21-03-2015 Ram nath
											boolean flagForOnlyOneEvaluation=false;
											//List<JobForTeacher> lstJFT=jobForTeacherDAO.findByCriteria(Restrictions.eq("teacherId", jobForTeacher.getTeacherId()),Restrictions.isNotNull("offerReady"));
											List<JobForTeacher> lstJFT=jobForTeacherDAO.findAllTeacherByHBD(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());											
											if(lstJFT!=null && lstJFT.size()>0)
											{
												for(JobForTeacher jft:lstJFT){
													if(!jft.getJobForTeacherId().toString().equals(jobForTeacher.getJobForTeacherId().toString()))
														flagForOnlyOneEvaluation=true;
													else if(jft.getJobForTeacherId().toString().equals(jobForTeacher.getJobForTeacherId().toString()) && panelAttMap.get(userMaster.getUserId())==null)
														flagForOnlyOneEvaluation=true;
												}
											}
											//ended By 21-03-2015 Ram nath
																						
											if(districtMaster!=null && schoolMaster!=null && jobWisePanelStatusObj!=null){												
												if(outerJob){
													sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'"+Utility.getLocaleValuePropByKey("msgManageAjax12", locale)+"');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
												}else{
													List<PanelSchedule> pSSList=panelScheduleDAO.findPanelSchedulesByTJSAndStatus(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),districtMaster,null,jobWisePanelStatusObj);
													//System.out.println("pSSList Size:::>>"+pSSList.size());
													int isCExistForPanel=0;
													for (PanelSchedule panelSchedule : pSSList) {
														isCExistForPanel=1;
														if(panelSchedule.getSchoolId()!=null){
															if(panelSchedule.getSchoolId().equals(schoolMaster)){
																isCExistForPanel=2;
																break;
															}
														}
													}
													
													
													if(isCExistForPanel==0 && sList.size()==0 && districtKeyContactList.size()==0){ // No Staffer
														sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'"+Utility.getLocaleValuePropByKey("msgManageAjax4", locale)+" clientservices@teachermatch.net.');\">");
													}else if(flagForOnlyOneEvaluation){
														sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'"+Utility.getLocaleValuePropByKey("headCand", locale)+" "+teacherDetails+" "+Utility.getLocaleValuePropByKey("msgManageAjax13", locale)+"');\"><img style='width:40px;background-color: white;margin-right:-26px;' src='images/unavailable.png'>");	
													}
													else if(panelAttMap.get(userMaster.getUserId())==null && isCExistForPanel!=1 && jobForTeacher!=null && jobForTeacher.getOfferReady()!=null){
														sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'"+Utility.getLocaleValuePropByKey("headCand", locale)+" "+teacherDetails+" "+Utility.getLocaleValuePropByKey("msgManageAjax13", locale)+"');\"><img style='width:40px;background-color: white;margin-right:-26px;' src='images/unavailable.png'>");	
													}else if(isCExistForPanel!=1){
														sbchild.append("<a href='#' onclick=\" setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth');jWTeacherStatusNotesOnOpen('"+fitScore+"','"+subfL.getStatusMaster().getStatusId()+"','"+subfL.getSecondaryStatusId()+"');\"><img style='width:40px;background-color: white;margin-right:-26px;' src='images/unavailable.png'>");
													}else  {// Another School Created
														sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'"+Utility.getLocaleValuePropByKey("msgManageAjax14", locale)+"');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
													}
												}
											}else{ 
												if(userMaster.getEntityType()==2){													
													if(panelAttMap.get(userMaster.getUserId())==null && offerReadyValue!=2){
														sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'Candidate "+teacherDetails+" "+Utility.getLocaleValuePropByKey("msgManageAjax13", locale)+"');\"><img style='width:40px;background-color: white;margin-right:-26px;' src='images/unavailable.png'>");
													}
													else if(panelAttMap.get(userMaster.getUserId())!=null && offerReadyValue!=2){
														sbchild.append("<a href='#' onclick=\" setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth');jWTeacherStatusNotesOnOpen('"+fitScore+"','"+subfL.getStatusMaster().getStatusId()+"','"+subfL.getSecondaryStatusId()+"');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
													}else  if(offerReadyFlag && isPanelExist==false && jobWisePanelStatusObjCheck!=null){//if(pSList.size()==0){ //Panel is not created
														sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'"+Utility.getLocaleValuePropByKey("msgManageAjax6", locale)+"');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
													}else if(pSList.size()!=0){//Panel made but school not offer ready
														//sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'The Principal has not yet made an offer to this candidate and hence you can not finalize this status.');\">");
														sbchild.append("<a href='#' onclick=\"finalizeOrNoForOfferReady(0);setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth');jWTeacherStatusNotesOnOpen('"+fitScore+"','"+subfL.getStatusMaster().getStatusId()+"','"+subfL.getSecondaryStatusId()+"');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
													}else{
														sbchild.append("<a href='#' onclick=\"setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth');jWTeacherStatusNotesOnOpen('"+fitScore+"','"+subfL.getStatusMaster().getStatusId()+"','"+subfL.getSecondaryStatusId()+"');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
													}
												}else{
													if(flagForOnlyOneEvaluation){
														sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'Candidate "+teacherDetails+" "+Utility.getLocaleValuePropByKey("msgManageAjax13", locale)+"');\">");	
													}else{
													sbchild.append("<a href='#' onclick=\"setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth'); jWTeacherStatusNotesOnOpen('"+fitScore+"','"+subfL.getStatusMaster().getStatusId()+"','"+subfL.getSecondaryStatusId()+"');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
													}
												}
											}
										}
									}else{

										sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'Candidate "+teacherDetails+" "+Utility.getLocaleValuePropByKey("msgManageAjax13", locale)+"');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
									}
								}else{
									if(offerReadyFlag==false && isPanelExist==false && jobWisePanelStatusObj!=null){
										sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'"+Utility.getLocaleValuePropByKey("msgManageAjax6", locale)+"');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
									}else{
										boolean isBeforeAllNotGreen=AcceptOrDeclineAjax.checkAllGreenBeforeCurrentStatus( cgService, jobForTeacher, jobForTeacher.getJobId(),secondaryStatusDAO,teacherStatusHistoryForJobDAO,subfL.getStatusMaster().getStatus());
										boolean isAlreadyOffered=AcceptOrDeclineAjax.checkAlreadyOfferOrNot(jobForTeacherDAO, jobForTeacher, userMaster,subfL.getStatusMaster().getStatus());
										if((isBeforeAllNotGreen))
										{
											sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'"+Utility.getLocaleValuePropByKey("msgManageAjax8", locale)+" �"+subfL.getStatusMaster().getStatus()+"�&nbsp;&nbsp;status');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
										}
										else if(isAlreadyOffered){
											sbchild.append("<a href='#' onclick=\"offerReadyMsg(1,'"+Utility.getLocaleValuePropByKey("headCand", locale)+" "+teacherDetails+" "+Utility.getLocaleValuePropByKey("msgManageAjax9", locale)+" �"+subfL.getStatusMaster().getStatus()+"� cannot be extended at this point.');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
										}
										else{										
												sbchild.append("<a href='#' onclick=\"setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth'); jWTeacherStatusNotesOnOpen('"+fitScore+"','"+subfL.getStatusMaster().getStatusId()+"','"+subfL.getSecondaryStatusId()+"');\"><img style='width:40px;background-color: white;' src='images/unavailable.png'>");
										}
									}
								}
							}
							//System.out.println(">>>>>>>>>>>>>>"+lstTeacherStatusHistoryForJob+" | "+jobWisePanelStatusList.size()+" |j "+panelAttendeeList.size()+" |s "+subfL.getStatusMaster().getStatusShortName()+" |n  |u "+userMaster.getUserId()+" |li "+lstTeacherStatusScores.size()+" | "+lstTeacherStatusNotes.size()+" | "+panelScheduleList.size());
							String strStatusColor=getStatusColor(lstTeacherStatusHistoryForJob, jobWisePanelStatusList, panelAttendeeList, subfL.getStatusMaster(),subfL, userMaster,lstTeacherStatusScores, lstTeacherStatusNotes,panelScheduleList);
							
							//System.out.println(subfL.getSecondaryStatusName()+"::::::::::::strStatusColor:====================================:::"+strStatusColor);
							if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("Green"))
							{
								try{
									String statusStr=jobForTeacher.getTeacherId().getTeacherId()+"##"+jobForTeacher.getJobId().getJobId()+"##"+subfL.getStatusMaster().getStatusId()+"##0";
									String waived=mapHistrory.get(statusStr);
									if(waived!=null && waived.equalsIgnoreCase("W")){
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");  //txtyellowbgroundstatus
									}else{
									UserMaster userObj=mapJFH.get(statusStr);
									if((userObj!=null && userObj.getSchoolId()!=null && userObj.getSchoolId().getSchoolId().equals(userMaster.getSchoolId().getSchoolId()) && userObj.getEntityType()==3) || userObj.getEntityType()==2){
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>"); //txtgreenbgroundstatus
									}else{
										String noteVal=mapNote.get(statusStr);
										if(noteVal!=null && noteVal.equals("Y")){
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
										}else{	
											if(schoolIds.indexOf("||"+userMaster.getSchoolId().getSchoolId()+"||")==-1){
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
											}else{
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreygreenbgroundstatus replace for temp
											}
										}
									}
							      }
								}catch(Exception e){
									sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
								}
							}
							else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("GreenWithGreyDot"))
							{
								/*sbchild.append("<table><tr><td class='txtgreenbgroundstatus_p'>");
								sbchild.append("<table><tr><td class='bgroundstatusGey'>"+subfL.getSecondaryStatusName()+"</td></tr></table>");
								sbchild.append("</td></tr></table>");*/
								//sbchild.append("<table style='margin-left: -43px;width: 150px;'><tr><td class='txtbgroundstatus philadelphiaParent' style='background-color:#71C51C;'>");
								sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");// philadelphiaParent
								//sbchild.append("</td></tr></table>");

							}
							else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("GreyWithGreenDot"))
							{
								/*sbchild.append("<table><tr><td class='txtbgroundstatus_p'>");
								sbchild.append("<table><tr><td class='bgroundstatusGreen'>"+subfL.getSecondaryStatusName()+"</td></tr></table>");
								sbchild.append("</td></tr></table>");*/
								//sbchild.append("<table style='margin-left: -43px;width: 150px;'><tr><td class='txtbgroundstatus philadelphiaParent'>");
								sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//philadelphiaMidGreen
								//sbchild.append("</td></tr></table>");
							}
							else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("Grey"))
							{
								sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtbgroundstatus
							}
							else if(strStatusColor!=null && strStatusColor.equalsIgnoreCase("Default"))
							{
								String statusStr=jobForTeacher.getTeacherId().getTeacherId()+"##"+jobForTeacher.getJobId().getJobId()+"##"+subfL.getStatusMaster().getStatusId()+"##0";
								UserMaster userObj=mapJFH.get(statusStr);
								String waived=mapHistrory.get(statusStr);
								if(waived!=null && waived.equalsIgnoreCase("W")){
									sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtyellowbgroundstatus
								}else if(getStatusCheck(lstTeacherStatusHistoryForJob,subfL.getStatusMaster(),null)){
									try{
										
										if(waived!=null && waived.equalsIgnoreCase("W")){
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtyellowbgroundstatus
										}else{
										if((userObj!=null && userObj.getSchoolId()!=null && userObj.getSchoolId().getSchoolId().equals(userMaster.getSchoolId().getSchoolId())&& userObj.getEntityType()==3) || userObj.getEntityType()==2){
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
										}else{
											String noteVal=mapNote.get(statusStr);
											if(noteVal!=null && noteVal.equals("Y")){
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
											}else{
												if(schoolIds.indexOf("||"+userMaster.getSchoolId().getSchoolId()+"||")==-1){
													sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
												}else{
													sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
												}
											}
										}
									  }
									}catch(Exception e){
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
									}
								}else{
									sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtbgroundstatus
								}
							}
							if(showStatus){
								sbchild.append("</a>");
							}
						}else{
							if(getStatusCheck(lstTeacherStatusHistoryForJob,subfL.getStatusMaster(),null)){
								try{
									String statusStr=jobForTeacher.getTeacherId().getTeacherId()+"##"+jobForTeacher.getJobId().getJobId()+"##"+subfL.getStatusMaster().getStatusId()+"##0";
									String waived=mapHistrory.get(statusStr);
									if(waived!=null && waived.equalsIgnoreCase("W")){
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtyellowbgroundstatus
										//sbchild.append("<img src=\"images/yellowRef.png\"/>");
									}else{
									UserMaster userObj=mapJFH.get(statusStr);
									if((userObj!=null && userObj.getSchoolId()!=null && userObj.getSchoolId().getSchoolId().equals(userMaster.getSchoolId().getSchoolId()) && userObj.getEntityType()==3) || userObj.getEntityType()==2){
										sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
									}else{
										String noteVal=mapNote.get(statusStr);
										if(noteVal!=null && noteVal.equals("Y")){
											sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
										}else{
											if(schoolIds.indexOf("||"+userMaster.getSchoolId().getSchoolId()+"||")==-1){
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
											}else{
												sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
											}
										}
									}
								  }
								}catch(Exception e){
									sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtgreenbgroundstatus
								}
							}else{
								sbchild.append("<img style='width:40px;background-color: white;' src=\"images/unavailable.png\"/><br><label style='margin-left: -44px;margin-right: -33px;margin-top:6px;'>"+subfL.getSecondaryStatusName()+"</label>");//txtbgroundstatus
							}
						}
						//sbchild.append("</td></tr>");
					} 
				}
			//}
		//}
		}
		sbchild.append("</div></div></div></div>");
		return sbchild.toString();
	}
	public int getMaxFitInStatusScore(List<TeacherStatusScores> lstTeacherStatusScores,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		int fitScore=0;
		try {
			if(lstTeacherStatusScores!=null)
				for(TeacherStatusScores teacherStatusScoresObj: lstTeacherStatusScores){
					if(statusMaster!=null){
						if(teacherStatusScoresObj.getStatusMaster()!=null)
							if(teacherStatusScoresObj.getStatusMaster().getStatusId().equals(statusMaster.getStatusId()) && teacherStatusScoresObj.isFinalizeStatus()){
								fitScore=teacherStatusScoresObj.getMaxScore();
								break;
							}
					}else if(teacherStatusScoresObj.getSecondaryStatus()!=null){
						if(teacherStatusScoresObj.getSecondaryStatus().getSecondaryStatusId().equals(secondaryStatus.getSecondaryStatusId()) && teacherStatusScoresObj.isFinalizeStatus()){
							fitScore=teacherStatusScoresObj.getMaxScore();
							break;
						}
					}
				}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fitScore;
	}


	public boolean getStatus(List<TeacherStatusNotes> teacherStatusNotesList,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		boolean status=false;
		try {
			if(teacherStatusNotesList!=null)
				for(TeacherStatusNotes teacherStatusNotes: teacherStatusNotesList){
					if(statusMaster!=null){
						if(teacherStatusNotes.getStatusMaster()!=null){
							if(teacherStatusNotes.getStatusMaster().getStatusId().equals(statusMaster.getStatusId()) && teacherStatusNotes.isFinalizeStatus()){
								status=true;
								break;
							}
						}
					}else{
						if(teacherStatusNotes.getSecondaryStatus()!=null){
							if(teacherStatusNotes.getSecondaryStatus().getSecondaryStatusId().equals(secondaryStatus.getSecondaryStatusId()) && teacherStatusNotes.isFinalizeStatus()){
								status=true;
								break;
							}
						}
					}

				}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	public boolean getScore(List<TeacherStatusScores> teacherStatusScoresList,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		boolean score=false;
		try {
			if(teacherStatusScoresList!=null)
				for(TeacherStatusScores teacherStatusScores: teacherStatusScoresList){
					if(statusMaster!=null){
						if(teacherStatusScores.getStatusMaster()!=null){
							if(teacherStatusScores.getStatusMaster().getStatusId().equals(statusMaster.getStatusId()) && teacherStatusScores.isFinalizeStatus()){
								score=true;
								break;
							}
						}
					}else{
						if(teacherStatusScores.getSecondaryStatus()!=null){
							if(teacherStatusScores.getSecondaryStatus().getSecondaryStatusId().equals(secondaryStatus.getSecondaryStatusId()) && teacherStatusScores.isFinalizeStatus()){
								score=true;
								break;
							}
						}
					}

				}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return score;
	}


	@Transactional(readOnly=false)
	public String isOverrideFinalizeStatus(String jobForTeacherId,String fitScore,String teacherStatusNoteId,String teacherId,String jobId,String statusId,String secondaryStatusId,String statusNotes,String statusNoteFileName,String emailSentToD,String emailSentToS,String scoreProvided,String finalizeStatus,boolean bEmailSendFor_DA_SA,boolean bEmailSendFor_CA,Integer[] answerId_array,Integer[] score_array )//String teacherId,int jobForTeacherId,String statusPrev,String statusTxt,String statusValue)
	{
		//System.out.println(":::::::isOverrideFinalizeStatus::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();

		String  isOverrideFinalizeStatus="no";

		try {
			HttpSession session = request.getSession();
			UserMaster userMaster =null;
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}else{
				userMaster = (UserMaster) session.getAttribute("userMaster");
			}

			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			JobOrder jobOrder =jobOrderDAO.findById(new Integer(jobId), false, false);

			StatusMaster statusMaster=null;
			SecondaryStatus secondaryStatus=null;

			if(statusId!=null && !statusId.equals("0")){
				statusMaster=statusMasterDAO.findById(Integer.parseInt(statusId),false,false);
			}
			if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
				secondaryStatus=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
			}


			List<TeacherStatusScores> teacherStatusScoresList=new ArrayList<TeacherStatusScores>();
			try
			{
				teacherStatusScoresList= teacherStatusScoresDAO.getStatusScore(teacherDetail,jobOrder,statusMaster,secondaryStatus,userMaster);
				TeacherStatusScores statusScores = new TeacherStatusScores();
				if(teacherStatusScoresList.size()==1)
				{
					statusScores =teacherStatusScoresList.get(0);
					if(statusScores.isFinalizeStatus())
						isOverrideFinalizeStatus="yes";;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isOverrideFinalizeStatus;

	}

	public String statusWiseSectionList(Integer userId,String statusName){
		try {
			
			UserMaster userMaster=userMasterDAO.findById(userId, false, false);
			List<StatusWiseEmailSection> allSectionTemplatesList = statusWiseEmailSectionDAO.findStatusWiseEmailSectionByHBD(userMaster);
			if(allSectionTemplatesList.size()>0){
				String allSectionTemplatesListUi="<option value='0'>"+Utility.getLocaleValuePropByKey("sltTemplate", locale)+"</option>";
				for (StatusWiseEmailSection statusWiseEmailSection : allSectionTemplatesList) {
					if(statusName!=null && statusName.equalsIgnoreCase(statusWiseEmailSection.getSectionName())){
						allSectionTemplatesListUi+="<option selected value="+statusWiseEmailSection.getEmailSectionId()+">"+statusWiseEmailSection.getSectionName()+"</option>";
					}else{
						allSectionTemplatesListUi+="<option value="+statusWiseEmailSection.getEmailSectionId()+">"+statusWiseEmailSection.getSectionName()+"</option>";
					}
				}
				return allSectionTemplatesListUi;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*======= Gagan :It is used to get Status Wise Email Templates For Admin ===============  */
	public StatusSpecificEmailTemplates getStatusWiseEmailForAdmin(String statusName,String statusShortName,Long jftIdForSNote)
	{
		UserMaster userMaster =null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		try{

			JobForTeacher jbforteacher	=	jobForTeacherDAO.findById(jftIdForSNote, false, false);
			String[] arrHrDetail = getHrDetailToTeacher(request,jbforteacher.getJobId());
			StringBuffer sb 					= 	new StringBuffer();
			String hrContactFirstName = arrHrDetail[0];
			String hrContactLastName = arrHrDetail[1];
			String hrContactEmailAddress = arrHrDetail[2];
			String phoneNumber			 = arrHrDetail[3];
			String title		 = arrHrDetail[4];
			int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
			String schoolDistrictName =	arrHrDetail[6];
			String dmName 			 = arrHrDetail[7];
			String dmEmailAddress = arrHrDetail[8];
			String dmPhoneNumber	 = arrHrDetail[9];

			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>");
			if(isHrContactExist==0)
				sb.append( Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal11", locale)+"<br/>"+schoolDistrictName +Utility.getLocaleValuePropByKey("msgRecruitmentSelectionTeam", locale)+" <br/><br/>");
			else 
				if(isHrContactExist==1)
				{
					DistrictMaster districtmaster =  jbforteacher.getJobId().getDistrictMaster();

					if(districtmaster.getDistrictId()!=null && districtmaster.getDistrictId()!=0 && districtmaster.getDistrictId()==3628590)
					{
						sb.append(Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal11", locale)+"<br/>");
						sb.append( Utility.getLocaleValuePropByKey("msgRecrtAnsSelect", locale)+"<br/>");
						sb.append(Utility.getLocaleValuePropByKey("msgSyracuseCitySchoolDistrict", locale)+"<br/>");
						sb.append("315-435-4171<br/><br/>");
					}else
					{
						sb.append( Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal11", locale)+"<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");

						if(title!="")
							sb.append(title+"<br/>");
						if(phoneNumber!="")
							sb.append(phoneNumber+"<br/>");
						sb.append(schoolDistrictName+"<br/><br/>");
					}
					

				}
				else
				{
					if(isHrContactExist==2)
					{
						DistrictMaster districtmaster =  jbforteacher.getJobId().getDistrictMaster();

						if(districtmaster.getDistrictId()!=null && districtmaster.getDistrictId()!=0 && districtmaster.getDistrictId()==3628590)
						{
							sb.append( Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal11", locale)+"<br/>");
							sb.append( Utility.getLocaleValuePropByKey("msgRecrtAnsSelect", locale)+"<br/>");
							sb.append("Syracuse City School District<br/>");
							sb.append("315-435-4171<br/><br/>");
						}else
						{
							sb.append( Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal11", locale)+"<br/>"+dmName+"<br/>");
							if(dmPhoneNumber!="")
								sb.append(dmPhoneNumber+"<br/>");
							sb.append(schoolDistrictName+"<br/><br/>");
						}
						
						
					}
				}
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

			String url = Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jbforteacher.getJobId().getJobId()+"&JobOrderType="+jbforteacher.getJobId().getCreatedForEntity();
			//System.out.println(" \n CG Url "+url+"\n statusName : "+statusName);

			Criterion criterion2				=	Restrictions.eq("statusShortName",statusShortName);
			List<StatusSpecificEmailTemplates> lstStatusSpecificEmailTemplates	=	statusSpecificEmailTemplatesDAO.findByCriteria(criterion2);

			String mailBody	="";

			if(lstStatusSpecificEmailTemplates.size()>0)
			{
				mailBody	=	lstStatusSpecificEmailTemplates.get(0).getTemplateBody();
				if(mailBody.contains("&lt; Teacher Name &gt;") || mailBody.contains("&lt; User Name &gt;") || mailBody.contains("&lt; Job Title &gt;")|| mailBody.contains("&lt; Status Name &gt;"))
				{
					//System.out.println("\n\n =================================================================================================");
					mailBody	=	mailBody.replaceAll("&lt; Teacher Name &gt;",jbforteacher.getTeacherId().getFirstName()+" "+jbforteacher.getTeacherId().getLastName());
					mailBody	=	mailBody.replaceAll("&lt; User Name &gt;",userMaster.getFirstName()+" "+userMaster.getLastName());
					mailBody	=	mailBody.replaceAll("&lt; Job Title &gt;",jbforteacher.getJobId().getJobTitle());
					mailBody	=	mailBody.replaceAll("&lt; Status Name &gt;",statusName);
					mailBody	=	mailBody.replaceAll("&lt; URL &gt;",url);
					mailBody	=	mailBody.replaceAll("&lt; Hr Contact &gt;",sb.toString());
					mailBody	=	mailBody.replaceAll("&lt; User EmailAddress &gt;",userMaster.getEmailAddress());
				}
				lstStatusSpecificEmailTemplates.get(0).setTemplateBody(mailBody);	
				return lstStatusSpecificEmailTemplates.get(0);
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;

	}

	/*======= Gourav :It is used to get Status Wise Templates List ===============  */
	public String getSectionWiseTemaplteslist(Integer sectionEmailId){
		try{
			StatusWiseEmailSection statusWiseEmailSection = statusWiseEmailSectionDAO.findById(sectionEmailId, false, false);
			Criterion criteria				=	Restrictions.eq("statusWiseEmailSection",statusWiseEmailSection);
			List<StatusWiseEmailSubSection> sectionWiseTemaplteslist = statusWiseEmailSubSectionDAO.findByCriteria(Order.asc("subSectionName"),criteria);

			String statuswisesectiontemplateslist="<select class='form-control' id='statuswisesectiontemplateslist' onchange='setTemplateByLIstId(this.value)'><option value='0'>"+Utility.getLocaleValuePropByKey("msgSelectSubTemplate", locale)+"</option>";

			for (StatusWiseEmailSubSection statusWiseEmailSubSection : sectionWiseTemaplteslist) {
				statuswisesectiontemplateslist+="<option value="+statusWiseEmailSubSection.getEmailSubSectionId()+">"+statusWiseEmailSubSection.getSubSectionName()+"</option>";
				/*if(statusWiseEmailSubSection.getSubSectionName().length()>55){
				String templateName =   statusWiseEmailSubSection.getSubSectionName().substring(0, 54);
				statuswisesectiontemplateslist+="<a id='iconpophover4' rel='tooltip'><option value='"+statusWiseEmailSubSection.getEmailSubSectionId()+"' title='"+statusWiseEmailSubSection.getSubSectionName()+"'>"+templateName+"....</option></a>";
			}else{
				statuswisesectiontemplateslist+="<option value="+statusWiseEmailSubSection.getEmailSubSectionId()+">"+statusWiseEmailSubSection.getSubSectionName()+"</option>";
			}*/

			}

			statuswisesectiontemplateslist+="</select>";
			return statuswisesectiontemplateslist;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	} 

	public String getTemplateByLIst(Integer subSectionEmailId){
		try {
			StatusWiseEmailSubSection statusWiseEmailSubSection = statusWiseEmailSubSectionDAO.findById(subSectionEmailId, false, false);
			String templateBody = statusWiseEmailSubSection.getTemplateBody();
			return templateBody;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/*======= Gagan :It is used to get Status Wise Email Templates For Admin ===============  */
	public StatusSpecificEmailTemplates getStatusWiseEmailForTeacher(String statusName,String statusShortName,Long jftIdForSNote)
	{
		UserMaster userMaster =null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		try{
			JobForTeacher jbforteacher	=	jobForTeacherDAO.findById(jftIdForSNote, false, false);
			String[] arrHrDetail = getHrDetailToTeacher(request,jbforteacher.getJobId());
			String fromUserName	=	userMaster.getFirstName()+" "+userMaster.getLastName();
			String teacherName	=	jbforteacher.getTeacherId().getFirstName();
			String jobTitle		=	jbforteacher.getJobId().getJobTitle();

			StatusSpecificEmailTemplates sstObj	=	new StatusSpecificEmailTemplates();
			String url = Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jbforteacher.getJobId().getJobId()+"&JobOrderType="+jbforteacher.getJobId().getCreatedForEntity();
			//System.out.println(" \n CG Url "+url+"\n statusName : "+statusName);

			String mailBody	=MailText.statusNoteSendToCandidate(userMaster,url,fromUserName,statusShortName,statusName,teacherName,jobTitle,arrHrDetail);

			sstObj.setSubjectLine("Status Changed for "+jbforteacher.getJobId().getJobTitle()+" Position You Had Applied For" );
			sstObj.setTemplateBody(mailBody);

			return sstObj;


		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		//return districtMaster;
		return null;

	}
	public String  saveStatusNote(String jobForTeacherId,String fitScore,String teacherStatusNoteId,String teacherId,String jobId,String statusId,String secondaryStatusId,String setHiredDate,String statusNotes,String statusNoteFileName,String emailSentToD,String emailSentToS,String scoreProvided,String finalizeStatus,boolean bEmailSendFor_DA_SA,boolean bEmailSendFor_CA,Integer[] answerId_array,Integer[] score_array,int isEmailTemplateChanged, String hiddenStatusShortName,String msgSubject,String mailBody,Integer txtschoolCount,Long schoolIdInputFromUser,int isEmailTemplateChangedTeacher,String msgSubjectTeacher,String mailBodyTeacher,boolean chkOverridetSatus,String requisitionNumberId,String qqNoteIdsvalue_str,String questionAssessmentIds,int jobCategoryFlagValue,String rdReqSel,String txtNewReqNo, String[] strength,String[] growthOpp,String recomreason,String remquestion,Integer minOneScoreSliderSetOrNotFlag,Integer superAdminClickOnEditNote,String flowOptionIds){
		String returnStr="";
		try{
			returnStr=saveStatusNoteMain(jobForTeacherId,fitScore,teacherStatusNoteId,teacherId,jobId,statusId,secondaryStatusId,setHiredDate,statusNotes,statusNoteFileName,emailSentToD,emailSentToS,scoreProvided,finalizeStatus,bEmailSendFor_DA_SA,bEmailSendFor_CA,answerId_array,score_array,isEmailTemplateChanged,hiddenStatusShortName,msgSubject,mailBody,txtschoolCount,schoolIdInputFromUser,isEmailTemplateChangedTeacher,msgSubjectTeacher,mailBodyTeacher,chkOverridetSatus,requisitionNumberId,qqNoteIdsvalue_str,questionAssessmentIds,jobCategoryFlagValue,rdReqSel,txtNewReqNo,strength,growthOpp,recomreason,remquestion,minOneScoreSliderSetOrNotFlag,superAdminClickOnEditNote,flowOptionIds);
			//System.out.println(":::::::::After Save Status:::::"+minOneScoreSliderSetOrNotFlag+":::::"+returnStr);
		}catch(Exception e){
			e.printStackTrace();
		}
		return returnStr;
	}

	@Transactional(readOnly=false)
	public String  saveStatusNoteMain(String jobForTeacherId,String fitScore,String teacherStatusNoteId,String teacherId,String jobId,String statusId,String secondaryStatusId,String setHiredDate,String statusNotes,String statusNoteFileName,String emailSentToD,String emailSentToS,String scoreProvided,String finalizeStatus,boolean bEmailSendFor_DA_SA,boolean bEmailSendFor_CA,Integer[] answerId_array,Integer[] score_array,int isEmailTemplateChanged, String hiddenStatusShortName,String msgSubject,String mailBody,Integer txtschoolCount,Long schoolIdInputFromUser,int isEmailTemplateChangedTeacher,String msgSubjectTeacher,String mailBodyTeacher,boolean chkOverridetSatus,String requisitionNumberId,String qqNoteIdsvalue_str,String questionAssessmentIds,int jobCategoryFlagValue,String rdReqSel,String txtNewReqNo, String[] strength,String[] growthOpp,String recomreason,String remquestion,Integer minOneScoreSliderSetOrNotFlag,Integer superAdminClickOnEditNote,String flowOptionIds)
	{
		//System.out.println(fitScore+" :::::"+minOneScoreSliderSetOrNotFlag+"::>>>>>>>>>>>>saveStatusNote::::::"+qqNoteIdsvalue_str+" ....... questionAssessmentIds :: "+questionAssessmentIds+" secondaryStatusId "+secondaryStatusId+"       superAdminClickOnEditNote:::::::"+superAdminClickOnEditNote);
		StringBuffer sb = new StringBuffer("");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		boolean canHire = true;
		boolean canUnhire=true;
		boolean mailSend=false;
		boolean overrideAll=false;
		String errorFlag="1";
		int multiJobReturn=0;
		String secondaryStatusNames="";
		String statusUpdateHDR=null;	
		TeacherDetail teacherDetail =null;
		DistrictMaster districtMaster=null;
		HeadQuarterMaster headQuarterMaster=null;
		JobCategoryMaster jobCategoryMaster=null;
		SecondaryStatus secondaryStatus=null;
		boolean hourlyCategory=false;
		SecondaryStatus secondaryStatusVVI=null;
		StatusMaster statusMasterVVI=null;
		SecondaryStatus secondaryStatusCC=null;
		StatusMaster statusMasterCC=null;
		String statusMasterSecondaryName=null;
		String statusName = "";
		boolean isSuperAdminUser=false;// added by 04-04-2015
		List<TeacherDetail> lstTDetails=new ArrayList<TeacherDetail>();
		BranchMaster branchMaster=null;
		boolean statusWaived=false;
		try {
			HttpSession session = request.getSession();
			UserMaster userMaster =null;
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}else{
				userMaster = (UserMaster) session.getAttribute("userMaster");
			}
			//added by 03-04-2015
			List<DistrictKeyContact> notesKeys=districtKeyContactDAO.findByContactType(userMaster, "Super Administrator"); 
			if(notesKeys.size()>0)
				isSuperAdminUser=true;
			//ended by 03-04-2015
			int roleId=0;
			int entityType=0;
			SchoolMaster userSchoolMaster=null; 
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
				entityType=userMaster.getEntityType();
			}
			if(entityType==3){
				if(userMaster.getSchoolId()!=null){
					userSchoolMaster=userMaster.getSchoolId();
				}
			}
			List<StatusMaster> lstStatusMaster=null;
			String roleAccess=null;
			try{
				lstStatusMaster=statusMasterDAO.findAllStatusMaster();
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			List<Long> qAnssementQuesIds = new ArrayList<Long>();
			String qqNoteIdsvalue_str_array[]=null;
			String qAssessmentIds_array[]=null;
			try{
				if(questionAssessmentIds!=null && !questionAssessmentIds.equals("")){
					qqNoteIdsvalue_str_array= qqNoteIdsvalue_str.split("#");
					qAssessmentIds_array= questionAssessmentIds.split("#");
					for(int i=0;i<qAssessmentIds_array.length;i++)
					{
						qAnssementQuesIds.add(Long.parseLong(qAssessmentIds_array[i]));
					}
				}
			}catch(Exception e){
				//e.printStackTrace();
			}

			CandidateGridService cgService=new CandidateGridService();
			teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			JobOrder jobOrder =jobOrderDAO.findById(new Integer(jobId), false, false);


			//Start ... New Requistion

			if(finalizeStatus!=null && finalizeStatus.equals("1") && rdReqSel!=null && rdReqSel.equals("2") && txtNewReqNo!=null && !txtNewReqNo.equals(""))
			{
				DistrictMaster dmTemp=jobOrder.getDistrictMaster();
				SchoolMaster schoolMaster=null;
				DistrictRequisitionNumbers districtRequisitionNumber=districtRequisitionNumbersDAO.getDistrictRequisitionNumber(dmTemp, txtNewReqNo);
				if( districtRequisitionNumber==null || (districtRequisitionNumber!=null && !districtRequisitionNumber.getIsUsed()))
				{

					if(txtschoolCount>0)
						schoolMaster=schoolMasterDAO.findById(schoolIdInputFromUser, false, false);

					SchoolInJobOrder schoolInJobOrder=null;
					List<SchoolInJobOrder> lstSchoolInJobOrder= new ArrayList<SchoolInJobOrder>();
					lstSchoolInJobOrder=schoolInJobOrderDAO.getSIJO(jobOrder, schoolMaster);
					if(lstSchoolInJobOrder!=null && lstSchoolInJobOrder.size()==1)
						schoolInJobOrder=lstSchoolInJobOrder.get(0);

					if(txtschoolCount==0 && schoolInJobOrder!=null)
						schoolMaster=schoolInJobOrder.getSchoolId();


					if(districtRequisitionNumber==null)
					{
						districtRequisitionNumber=new DistrictRequisitionNumbers();
						districtRequisitionNumber.setCreatedDateTime(new Date());
						districtRequisitionNumber.setDistrictMaster(dmTemp);
						districtRequisitionNumber.setIsUsed(true);
						districtRequisitionNumber.setUserMaster(userMaster);

						if(jobOrder.getJobType()!=null && !jobOrder.getJobType().equals("") && jobOrder.getJobType().equals("P"))
							districtRequisitionNumber.setPosType("P");
						else
							districtRequisitionNumber.setPosType("F");

						districtRequisitionNumber.setRequisitionNumber(txtNewReqNo);
						districtRequisitionNumbersDAO.makePersistent(districtRequisitionNumber);
					}

					JobRequisitionNumbers jobRequisitionNumbers=new JobRequisitionNumbers();
					jobRequisitionNumbers.setJobOrder(jobOrder);
					jobRequisitionNumbers.setDistrictRequisitionNumbers(districtRequisitionNumber);
					jobRequisitionNumbers.setSchoolMaster(schoolMaster);
					jobRequisitionNumbers.setStatus(0);
					jobRequisitionNumbersDAO.makePersistent(jobRequisitionNumbers);

					if(schoolInJobOrder!=null)
					{
						int noOfSchoolExpHires=schoolInJobOrder.getNoOfSchoolExpHires();
						noOfSchoolExpHires=noOfSchoolExpHires+1;
						schoolInJobOrder.setNoOfSchoolExpHires(noOfSchoolExpHires);
						schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
					}
					else
					{
						int noOfExpHires=0;
						if(jobOrder.getNoOfExpHires()!=null )
							noOfExpHires=jobOrder.getNoOfExpHires();
						noOfExpHires=noOfExpHires+1;
						jobOrder.setNoOfExpHires(noOfExpHires);
						jobOrderDAO.makePersistent(jobOrder);
					}

					requisitionNumberId=jobRequisitionNumbers.getJobRequisitionId().toString();

				}
				else
				{
					errorFlag="99";
					return errorFlag+"##0##0##0";
				}
			}

			//End ... New Requistion


			jobCategoryMaster=jobOrder.getJobCategoryMaster();
			try{
				if(jobCategoryMaster!=null && jobCategoryMaster.getJobCategoryName().contains(Utility.getLocaleValuePropByKey("msgHourlyTeacher", locale))){
					hourlyCategory=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			//System.out.println("hourlyCategory:::::::::::::::::::::::::::::>"+hourlyCategory);
			JobForTeacher jobForTeacherObj =new JobForTeacher();
			if(jobForTeacherId!=null){
				JobForTeacher jobForTeacherForResend=jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
				if(finalizeStatus.equalsIgnoreCase("6")){
					jobForTeacherForResend.setOfferReady(false);
					jobForTeacherForResend.setOfferMadeDate(null);
					jobForTeacherDAO.updatePersistent(jobForTeacherForResend);
					finalizeStatus="1";				
				}
				jobForTeacherObj=jobForTeacherForResend;
			}
			if(finalizeStatus!=null && finalizeStatus.equalsIgnoreCase("7")){
				finalizeStatus="1";	
				statusWaived=true;
			}
			SchoolInJobOrder schoolInJobOrder = null;
			boolean outerJob=true;
			if(userMaster.getEntityType()==3){
				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, userMaster.getSchoolId());
				if(schoolInJobOrder!=null){
					outerJob=false;
				}
			}else{
				outerJob=false;
			}
			boolean isUpdateStatus=cgService.isUpdateStatus(jobForTeacherObj,userMaster,schoolInJobOrder);

			System.out.println("isUpdateStatus::::::::::::::::::::::::::::::::::::>>>"+isUpdateStatus);

			if(jobOrder.getCreatedForEntity()!=1){
				try{
					districtMaster=jobOrder.getDistrictMaster();
				}catch(Exception e){}
			}
			if(jobOrder.getCreatedForEntity()==5 || jobOrder.getCreatedForEntity()==6 || jobOrder.getHeadQuarterMaster()!=null){
				try{
					headQuarterMaster=jobOrder.getHeadQuarterMaster();
				}catch(Exception e){}
			}
			
			boolean isMiami = false;
			boolean isPhiladelphia = false;//added by 21-03-2015
			try{
				//added by 21-03-2015
				if(districtMaster!=null && districtMaster.getDistrictId().equals(00000000))
					isPhiladelphia=true;
				//end
				if(districtMaster!=null && districtMaster.getDistrictId().equals(1200390))
					isMiami=true;
			}catch(Exception e){}

			List<I4InterviewInvites> I4InterviewInvites=new ArrayList<I4InterviewInvites>();
			boolean vviPrivilege=false;
			try{
				if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview())
				{
					if(jobOrder.getStatusMaster()!=null && jobOrder.getSecondaryStatus()==null){
						statusMasterVVI=jobOrder.getStatusMaster();
						vviPrivilege=true;
					}else if(jobOrder.getStatusMaster()==null && jobOrder.getSecondaryStatus()!=null){
						secondaryStatusVVI=jobOrder.getSecondaryStatus();
						vviPrivilege=true;
					}
				}else if(jobOrder.getJobCategoryMaster().getOfferVirtualVideoInterview()!=null && jobOrder.getJobCategoryMaster().getOfferVirtualVideoInterview()){

					if(jobOrder.getJobCategoryMaster().getStatusIdForAutoVVILink()!=null && jobOrder.getJobCategoryMaster().getSecondaryStatusIdForAutoVVILink()==null){
						statusMasterVVI= statusMasterDAO.findById(jobOrder.getJobCategoryMaster().getStatusIdForAutoVVILink(), false, false);//jobOrder.getJobCategoryMaster().getStatusMaster();
						vviPrivilege=true;
					}else if(jobOrder.getJobCategoryMaster().getStatusIdForAutoVVILink()==null && jobOrder.getJobCategoryMaster().getSecondaryStatusIdForAutoVVILink()!=null){
						secondaryStatusVVI= secondaryStatusDAO.findById(jobOrder.getJobCategoryMaster().getSecondaryStatusIdForAutoVVILink(), false, false);
						vviPrivilege=true;
					}
				}else if(districtMaster!=null && districtMaster.getOfferVirtualVideoInterview()!=null && districtMaster.getOfferVirtualVideoInterview()){
					if(districtMaster.getStatusMasterForVVI()!=null && districtMaster.getSecondaryStatusForVVI()==null){
						statusMasterVVI=districtMaster.getStatusMasterForVVI();
						vviPrivilege=true;
					}else if(districtMaster.getStatusMasterForVVI()==null && districtMaster.getSecondaryStatusForVVI()!=null){
						secondaryStatusVVI=districtMaster.getSecondaryStatusForVVI();
						vviPrivilege=true;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(vviPrivilege){
				I4InterviewInvites=I4InterviewInvitesDAO.getListbyTeacherAndjobId(jobOrder,teacherDetail);
			}
			boolean ccPrivilege=false;
			try{
				if(districtMaster!=null && districtMaster.getStatusMasterForCC()!=null && districtMaster.getSecondaryStatusForCC()==null){
					statusMasterCC=districtMaster.getStatusMasterForCC();
					ccPrivilege=true;
				}else if(districtMaster!=null && districtMaster.getStatusMasterForCC()==null && districtMaster.getSecondaryStatusForCC()!=null){
					secondaryStatusCC=districtMaster.getSecondaryStatusForCC();
					ccPrivilege=true;

				}
			}catch(Exception e){
				e.printStackTrace();
			}

			SchoolMaster schoolMaster =null;
			if(jobOrder.getCreatedForEntity()==3){
				try{
					schoolMaster=jobOrder.getSchool().get(0);
				}catch(Exception e){}
			}
			
			System.out.println("statusId::::::------------------------:::::"+statusId+"     "+secondaryStatusId);
			StatusMaster statusMaster=null;
			SecondaryStatus secondaryStatusForPanel=null;
			String statusShortName="soth";
			if(statusId!=null && !statusId.equals("0")){
				statusMaster=statusMasterDAO.findById(Integer.parseInt(statusId),false,false);
			}


			if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
				secondaryStatus=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
				secondaryStatusForPanel=secondaryStatus;
			}
			if(statusMaster!=null)
			{
				statusName = statusMaster.getStatus();	
			}
			if(secondaryStatus!=null)
			{
				statusName = secondaryStatus.getSecondaryStatusName();
			}
			try{
				String secondaryStatusName = secondaryStatus.getSecondaryStatusName();
				if(districtMaster!=null && districtMaster.getDistrictId() == 5510470 && secondaryStatusName.equalsIgnoreCase("conditional offer")){
					emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Conditional Offer",MailText.getFPRejectMail(request, teacherDetail));
				}
			}catch(Exception e){}

			SecondaryStatus secondaryStatusForJobs=secondaryStatus;
			StatusMaster statusMasterForJobs=statusMaster;


			boolean selectedstatus=false;
			boolean selectedSecondaryStatus=false;
			try{
				if(jobForTeacherObj.getStatus()!=null && statusMaster!=null){
					selectedstatus=cgService.selectedStatusCheck(statusMaster,jobForTeacherObj.getStatus());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			try{
				if(secondaryStatus!=null && jobForTeacherObj.getStatus()!=null &&  statusMaster!=null){// && (jobForTeacherObj.getStatus().getStatusShortName().equals("scomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("ecomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("vcomp"))
					if(jobForTeacherObj.getSecondaryStatus()!=null){
						selectedSecondaryStatus=cgService.selectedPriSecondaryStatusCheck(secondaryStatus,jobForTeacherObj.getSecondaryStatus());
						if(selectedSecondaryStatus==false){
							selectedstatus=false;
						}
					}
				}else if(jobForTeacherObj.getSecondaryStatus()!=null && secondaryStatus!=null && statusMaster==null){
					if(jobForTeacherObj.getStatus()!=null){
						if(cgService.priSecondaryStatusCheck(jobForTeacherObj.getStatus(),secondaryStatus)){
							selectedSecondaryStatus=cgService.selectedNotPriSecondaryStatusCheck(secondaryStatus,jobForTeacherObj.getSecondaryStatus());
						}
					}
				}else if(jobForTeacherObj.getSecondaryStatus()==null && secondaryStatus!=null && statusMaster==null){
					if(jobForTeacherObj.getStatus()!=null){
						if(cgService.priSecondaryStatusCheck(jobForTeacherObj.getStatus(),secondaryStatus)){
							selectedSecondaryStatus=true;
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("::::::selectedstatus::::::::;;>>>>>>>>>>>>>>>>>>>>>>>>>>>;>>"+selectedstatus);
			System.out.println(":::::::;;selectedSecondaryStatus::::::::;;;>>"+selectedSecondaryStatus);
 
			if(statusId!=null && !statusId.equals("0")){
				secondaryStatusId="0";
				secondaryStatus=null;
			}
			List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryActive(teacherDetail,jobOrder);
			TeacherStatusHistoryForJob statusHistory=teacherStatusHistoryForJobDAO.findByTeacherStatus(teacherDetail,jobOrder,statusMaster,secondaryStatus);
			boolean  finalizeStatusFlag=false;
			try{
				
				if(headQuarterMaster!=null && headQuarterMaster.getStatusNotes()!=null){
					if(headQuarterMaster.getStatusNotes()==false){
						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								finalizeStatusFlag=true;
							}
						}
					}
				}else if(districtMaster!=null && districtMaster.getStatusNotes()!=null){
					if(districtMaster.getStatusNotes()==false){
						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								finalizeStatusFlag=true;
							}
						}
					}
				}
				if(statusName.equalsIgnoreCase("Asst Supe Interview"))
				{
					finalizeStatusFlag=true;
				}
			}catch(Exception e){}
			System.out.println("statusName::::::::::"+statusName);
			System.out.println("finalizeStatusFlag::::::::::"+finalizeStatusFlag);
			try{

				if(teacherStatusNoteId.equals("0"))
				{

					if((statusNoteFileName!=null && !statusNoteFileName.equals("")) || (statusNotes!=null && !statusNotes.equals("")))
					{
						TeacherStatusNotes tsDetailsObj = new TeacherStatusNotes();
						if(teacherStatusNoteId!=null){
							if(!teacherStatusNoteId.equals("0")){
								tsDetailsObj =teacherStatusNotesDAO.findById(Integer.parseInt(teacherStatusNoteId),false,false);
							}
						}
						tsDetailsObj.setTeacherDetail(teacherDetail);
						tsDetailsObj.setUserMaster(userMaster);
						tsDetailsObj.setJobOrder(jobOrder);
						if(districtMaster!=null){
							tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
						}
						if(statusId!=null && !statusId.equals("0")){
							tsDetailsObj.setStatusMaster(statusMaster);
						}
						if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
							tsDetailsObj.setSecondaryStatus(secondaryStatus);
						}
						if(statusNotes!=null){
							tsDetailsObj.setStatusNotes(statusNotes);
						}
						if(statusNoteFileName!=null){
							if(tsDetailsObj.getStatusNoteFileName()!=null){
								try{
									File file = new File(Utility.getValueOfPropByKey("communicationRootPath")+"statusNote/"+tsDetailsObj.getTeacherDetail().getTeacherId()+"/"+tsDetailsObj.getStatusNoteFileName());
									if(file.delete()){
										System.out.println(file.getName() + " is deleted!");
									}else{
										System.out.println("Delete operation is failed.");
									}
								}catch(Exception e){}
							}
							tsDetailsObj.setStatusNoteFileName(statusNoteFileName);
						}
						if(schoolMaster!=null){
							tsDetailsObj.setSchoolId(schoolMaster.getSchoolId());	
						}
						int emailSentTo=0;
						if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
							emailSentTo=2;
						}
						if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
							emailSentTo=1;
						}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
							emailSentTo=3;
						}
						tsDetailsObj.setEmailSentTo(emailSentTo);

						tsDetailsObj.setTeacherAssessmentQuestionId(null);

						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								tsDetailsObj.setFinalizeStatus(true);
								finalizeStatusFlag=true;
							}
						}
						teacherStatusNotesDAO.makePersistent(tsDetailsObj);
					}

					// Set ReferenceCheckHistory Table

					List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistoryList = null;
					teacherElectronicReferencesHistoryList	=	teacherElectronicReferencesHistoryDAO.countContactStatusRefChkByHBD(jobOrder, jobForTeacherObj.getTeacherId());;	
					Integer elerefHAutoId = null;
					try{
						if(teacherElectronicReferencesHistoryList != null){
							for(TeacherElectronicReferencesHistory objRefHistory : teacherElectronicReferencesHistoryList){
								elerefHAutoId	=	objRefHistory.getElerefHAutoId();
								TeacherElectronicReferencesHistory objEl = teacherElectronicReferencesHistoryDAO.findById(elerefHAutoId, false, false);	
								objEl.setReplyStatus(1);
								teacherElectronicReferencesHistoryDAO.makePersistent(objEl);
							}
						}
					} catch(Exception e){}

					//mukesh
					if((statusNotes==null || statusNotes.equals("")) && fitScore.equals("0")){
						if(districtMaster!=null && districtMaster.getStatusNotes()==false){
							if(finalizeStatus!=null){
								if(!finalizeStatus.equals("0")){
									finalizeStatusFlag=true;
								}
							}
						}
						/*if(statusName.equalsIgnoreCase("Asst Supe Interview"))
						{
							finalizeStatusFlag=true;
						}*/
					}
					TeacherStatusNotes checkForFinalize = teacherStatusNotesDAO.chkQuestionNotesStatusByUserMasterAndHBD(teacherDetail, jobOrder,qAnssementQuesIds,userMaster);

					if(checkForFinalize!=null && checkForFinalize.isFinalizeStatus()){
						System.out.println(" Data is finalize.......");
					}else{
						// For Question Status Notes
						if(qAssessmentIds_array!=null)
							for(int i=0;i<qAssessmentIds_array.length;i++)
							{
								TeacherStatusNotes tsDetailsObj = teacherStatusNotesDAO.getQuestionNoteObjByUserMasterAndHBD(teacherDetail, jobOrder,Long.parseLong(qAssessmentIds_array[i]),userMaster);
								if(tsDetailsObj==null)
								{
									tsDetailsObj = new TeacherStatusNotes();
								}


								tsDetailsObj.setTeacherDetail(teacherDetail);
								tsDetailsObj.setUserMaster(userMaster);
								tsDetailsObj.setJobOrder(jobOrder);
								if(districtMaster!=null){
									tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
								}
								if(statusId!=null && !statusId.equals("0")){
									tsDetailsObj.setStatusMaster(statusMaster);
								}
								if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
									tsDetailsObj.setSecondaryStatus(secondaryStatus);
								}
								tsDetailsObj.setStatusNoteFileName(null);
								if(schoolMaster!=null){
									tsDetailsObj.setSchoolId(schoolMaster.getSchoolId());
								}
								int emailSentTo=0;
								if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
									emailSentTo=2;
								}
								if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
									emailSentTo=1;
								}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
									emailSentTo=3;
								}
								tsDetailsObj.setEmailSentTo(emailSentTo);

								if(qAssessmentIds_array[i]!=null && !qAssessmentIds_array[i].equals(""))
									tsDetailsObj.setTeacherAssessmentQuestionId(Long.parseLong(qAssessmentIds_array[i]));
								else
									tsDetailsObj.setTeacherAssessmentQuestionId(null);

								boolean noQuestion=false;
								if(qqNoteIdsvalue_str_array!=null && qqNoteIdsvalue_str_array.length>i){
									if(qqNoteIdsvalue_str_array[i]!=null && !qqNoteIdsvalue_str_array[i].equals("")){
										tsDetailsObj.setStatusNotes(qqNoteIdsvalue_str_array[i]);
										noQuestion=true;
									}
								}
								if(finalizeStatus!=null){
									if(!finalizeStatus.equals("0")){
										tsDetailsObj.setFinalizeStatus(true);
										finalizeStatusFlag=true;
									}
								}
								if(noQuestion){
									teacherStatusNotesDAO.makePersistent(tsDetailsObj);
								}
							}
					}
				}
				else
				{
					if((statusNoteFileName!=null && !statusNoteFileName.equals("")) || (statusNotes!=null && !statusNotes.equals("")))
					{
						TeacherStatusNotes tsDetailsObj = new TeacherStatusNotes();
						boolean updatedbyuser=true;
						if(teacherStatusNoteId!=null){
							if(!teacherStatusNoteId.equals("0")){
								tsDetailsObj =teacherStatusNotesDAO.findById(Integer.parseInt(teacherStatusNoteId),false,false);
								//added by 01-04-2015		yyyyyyyyyyyyyyyyy						
								TeacherStatusNotesHistory copyjTSNH=new TeacherStatusNotesHistory();
		 			 			 try {
		 							BeanUtils.copyProperties(copyjTSNH, tsDetailsObj);
		 							copyjTSNH.setTeacherStatusNoteId(null);
		 							copyjTSNH.setUpdatedByUserId(userMaster.getUserId());
		 							copyjTSNH.setStatusUpdatedNotes(statusNotes);
		 							teacherStatusNotesHistoryDAO.makePersistent(copyjTSNH);
		 							tsDetailsObj.setUserMaster(copyjTSNH.getUserMaster());
		 							if(isSuperAdminUser)
		 							updatedbyuser=false;
		 						} catch (IllegalAccessException e) {
		 							e.printStackTrace();
		 						} catch (InvocationTargetException e) {
		 							e.printStackTrace();
		 						}catch(Exception e){
		 							e.printStackTrace();
		 						}
		 						//ended by 01-04-2015
							}
						}
						tsDetailsObj.setTeacherDetail(teacherDetail);
						
						if(updatedbyuser)//added by 04-04-2015
						tsDetailsObj.setUserMaster(userMaster);
						tsDetailsObj.setJobOrder(jobOrder);
						if(districtMaster!=null){
							tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
						}
						if(statusId!=null && !statusId.equals("0")){
							tsDetailsObj.setStatusMaster(statusMaster);
						}
						if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
							tsDetailsObj.setSecondaryStatus(secondaryStatus);
						}
						if(statusNotes!=null){
							tsDetailsObj.setStatusNotes(statusNotes);
						}
						if(statusNoteFileName!=null){
							if(tsDetailsObj.getStatusNoteFileName()!=null){
								try{
									File file = new File(Utility.getValueOfPropByKey("communicationRootPath")+"statusNote/"+tsDetailsObj.getTeacherDetail().getTeacherId()+"/"+tsDetailsObj.getStatusNoteFileName());
									if(file.delete()){
										System.out.println(file.getName() + " is deleted!");
									}else{
										System.out.println("Delete operation is failed.");
									}
								}catch(Exception e){}
							}
							tsDetailsObj.setStatusNoteFileName(statusNoteFileName);
						}
						if(schoolMaster!=null){
							tsDetailsObj.setSchoolId(schoolMaster.getSchoolId());
						}
						int emailSentTo=0;
						if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
							emailSentTo=2;
						}
						if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
							emailSentTo=1;
						}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
							emailSentTo=3;
						}
						tsDetailsObj.setEmailSentTo(emailSentTo);

						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								tsDetailsObj.setFinalizeStatus(true);
								finalizeStatusFlag=true;
							}
						}
						teacherStatusNotesDAO.makePersistent(tsDetailsObj);
					}

					TeacherStatusNotes checkForFinalize = teacherStatusNotesDAO.chkQuestionNotesStatusByUserMasterAndHBD(teacherDetail, jobOrder, qAnssementQuesIds,userMaster);

					if(checkForFinalize!=null && checkForFinalize.isFinalizeStatus()){
						System.out.println(" Data is finalize.......");
					}else{
						// For Question Status Notes in Edit Mode
						if(qAssessmentIds_array!=null)
							for(int i=0;i<qAssessmentIds_array.length;i++)
							{
								TeacherStatusNotes tsDetailsObj = teacherStatusNotesDAO.getQuestionNoteObjByUserMasterAndHBD(teacherDetail, jobOrder,Long.parseLong(qAssessmentIds_array[i]),userMaster);
								if(tsDetailsObj==null)
								{
									tsDetailsObj = new TeacherStatusNotes();
								}


								tsDetailsObj.setTeacherDetail(teacherDetail);
								tsDetailsObj.setUserMaster(userMaster);
								tsDetailsObj.setJobOrder(jobOrder);
								if(districtMaster!=null){
									tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
								}
								if(statusId!=null && !statusId.equals("0")){
									tsDetailsObj.setStatusMaster(statusMaster);
								}
								if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
									tsDetailsObj.setSecondaryStatus(secondaryStatus);
								}
								tsDetailsObj.setStatusNoteFileName(null);
								if(schoolMaster!=null){
									tsDetailsObj.setSchoolId(schoolMaster.getSchoolId());
								}
								int emailSentTo=0;
								if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
									emailSentTo=2;
								}
								if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
									emailSentTo=1;
								}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
									emailSentTo=3;
								}
								tsDetailsObj.setEmailSentTo(emailSentTo);

								if(qAssessmentIds_array[i]!=null && !qAssessmentIds_array[i].equals(""))
									tsDetailsObj.setTeacherAssessmentQuestionId(Long.parseLong(qAssessmentIds_array[i]));
								else
									tsDetailsObj.setTeacherAssessmentQuestionId(null);

								boolean noQuestion=false;
								if(qqNoteIdsvalue_str_array!=null && qqNoteIdsvalue_str_array.length>i){
									if(qqNoteIdsvalue_str_array[i]!=null && !qqNoteIdsvalue_str_array[i].equals("")){
										tsDetailsObj.setStatusNotes(qqNoteIdsvalue_str_array[i]);
										noQuestion=true;
									}
								}

								if(finalizeStatus!=null){
									if(!finalizeStatus.equals("0")){
										tsDetailsObj.setFinalizeStatus(true);
										finalizeStatusFlag=true;
									}
								}
								if(noQuestion){
									teacherStatusNotesDAO.makePersistent(tsDetailsObj);
								}
							}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}	

			//Override
			if(statusHistory!=null && finalizeStatusFlag)
			{
				if(chkOverridetSatus){
					statusHistory.setOverride(chkOverridetSatus);
					statusHistory.setOverrideBy(userMaster);
					debugPrintln("Override more ... not entry first time");
				}
				try {
					teacherStatusHistoryForJobDAO.makePersistent(statusHistory);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			try{
				TeacherStatusHistoryForJob statusHistoryForWaived=teacherStatusHistoryForJobDAO.findWaivedStatusByTeacherAndJob(teacherDetail,jobOrder,statusMaster,secondaryStatus);
				System.out.println(statusWaived+":::::::::::statusHistoryForWaived:::::::::::::::"+statusHistoryForWaived);
				if(statusHistoryForWaived!=null && statusWaived==false && finalizeStatusFlag){
					statusHistoryForWaived.setStatus("S");
					statusHistoryForWaived.setUserMaster(userMaster);
					statusHistoryForWaived.setUpdatedDateTime(new Date());
					teacherStatusHistoryForJobDAO.updatePersistent(statusHistoryForWaived);
				}
				//return null;
			}catch(Exception e){
				e.printStackTrace();
			}
			//add by Ram Nath for JSI Score
			String jsiStatusName="";
			if(statusMasterForJobs!=null)
			jsiStatusName=statusMasterForJobs.getStatus();
			else if(secondaryStatusForJobs!=null)
			jsiStatusName=secondaryStatusForJobs.getSecondaryStatusName();
			//System.out.println("status==="+jsiStatusName);				
			//end by Ram Nath
			
			// Start Update Answer Score
			int iReturnValue=0;
			int inputSumAnswerScore=0;
			int inputSumAnswerMaxScore=0; 
			if(answerId_array!=null && answerId_array.length>0)
			{
				List<StatusSpecificScore> StatusSpecificScoreList=statusSpecificScoreDAO.getStatusSpecificScoreList(answerId_array);
				Map<Integer,StatusSpecificScore> sSFSMap= new HashMap<Integer, StatusSpecificScore>();
				for(StatusSpecificScore sssOBJ : StatusSpecificScoreList ){
					sSFSMap.put(sssOBJ.getAnswerId(),sssOBJ);
				}

				for(int i=0; i<answerId_array.length;i++)	
				{
					try {
						StatusSpecificScore statusSpecificScore=null;
						if(sSFSMap!=null){
							statusSpecificScore=sSFSMap.get(answerId_array[i]);
						}
						if(statusSpecificScore!=null)
						{
							//start add by ram nath for JSI Score
							if(jsiStatusName.equalsIgnoreCase("jsi") || minOneScoreSliderSetOrNotFlag==1){
								statusSpecificScore.setFinalizeStatus(Integer.parseInt(finalizeStatus));
								statusSpecificScore.setScoreProvided(score_array[i]);
								statusSpecificScoreDAO.makePersistent(statusSpecificScore);
								iReturnValue++;
								inputSumAnswerScore=inputSumAnswerScore+statusSpecificScore.getScoreProvided();
								inputSumAnswerMaxScore=inputSumAnswerMaxScore+statusSpecificScore.getMaxScore();
							}else//end by ram nath	
							if(score_array[i]!=0){
								statusSpecificScore.setFinalizeStatus(Integer.parseInt(finalizeStatus));
								statusSpecificScore.setScoreProvided(score_array[i]);
								statusSpecificScoreDAO.makePersistent(statusSpecificScore);
								iReturnValue++;
								inputSumAnswerScore=inputSumAnswerScore+statusSpecificScore.getScoreProvided();
								inputSumAnswerMaxScore=inputSumAnswerMaxScore+statusSpecificScore.getMaxScore();
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			// End Update Answer Score

			boolean  jobWiseScoreFlag=false;
			List<TeacherStatusScores> teacherStatusScoresList=new ArrayList<TeacherStatusScores>();
			List<TeacherStatusNotes> teacherStatusNotesList=new ArrayList<TeacherStatusNotes>();
			try{
				//added by 10-04-2015
				//System.out.println("minOneScoreSliderSetOrNotFlag==="+minOneScoreSliderSetOrNotFlag+" inputSumAnswerScore==="+inputSumAnswerScore+"outerJob============="+outerJob);
				boolean isInsertZeroScore=false;
				if(minOneScoreSliderSetOrNotFlag==1)
				isInsertZeroScore=true;
				//ended by 10-04-2015
				if(outerJob==false && (( scoreProvided!=null && !scoreProvided.equals("0") && !scoreProvided.equals("")) || (inputSumAnswerScore>0) || isInsertZeroScore)){ //added by 10-04-2015 || isInsertZeroScore
					int fStatus=0;
					teacherStatusScoresList= teacherStatusScoresDAO.getStatusScore(teacherDetail,jobOrder,statusMaster,secondaryStatus,userMaster);
					TeacherStatusScores statusScores = new TeacherStatusScores();
					if(teacherStatusScoresList!=null && teacherStatusScoresList.size()> 0)
					{
						statusScores =teacherStatusScoresList.get(0);
						if(statusScores.isFinalizeStatus())
							fStatus=1;
					}
					if(fStatus==0){
						statusScores.setTeacherDetail(teacherDetail);
						statusScores.setUserMaster(userMaster);
						statusScores.setJobOrder(jobOrder);
						if(districtMaster!=null){
							statusScores.setDistrictId(districtMaster.getDistrictId());
						}
						if(statusId!=null && !statusId.equals("0")){
							statusScores.setStatusMaster(statusMaster);
						}
						if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
							statusScores.setSecondaryStatus(secondaryStatus);
						}

						if(schoolMaster!=null){
							statusScores.setSchoolId(schoolMaster.getSchoolId());	
						}
						int emailSentTo=0;
						if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
							emailSentTo=2;
						}
						if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
							emailSentTo=1;
						}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
							emailSentTo=3;
						}
						statusScores.setEmailSentTo(emailSentTo);

						if(inputSumAnswerScore>0)
						{
							statusScores.setScoreProvided(inputSumAnswerScore);
						}
						else{
							if(scoreProvided!=null && !scoreProvided.trim().equals("")){
								statusScores.setScoreProvided(Integer.parseInt(scoreProvided));
							}
						}

						if(inputSumAnswerScore>0)
						{
							statusScores.setMaxScore(inputSumAnswerMaxScore);
						}
						else
						{
							if(fitScore!=null){
								statusScores.setMaxScore(Integer.parseInt(fitScore));
							}
						}

						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								statusScores.setFinalizeStatus(true);
								jobWiseScoreFlag=true;
							}
						}
						teacherStatusScoresDAO.makePersistent(statusScores);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			///////////////////////Auto Panel Creation//////////////////////

			try{
				if(entityType==3 && userSchoolMaster!=null){
					JobWisePanelStatus jobWisePanelStatusObj=null;
					if(isPhiladelphia)
					jobWisePanelStatusObj =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacherObj.getJobId(),"No Evaluation Complete");	//added by ram nath
					else
					jobWisePanelStatusObj =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacherObj.getJobId(),"Offer Ready");
					if(jobWisePanelStatusObj!=null){
						List<PanelSchedule> pSList=panelScheduleDAO.findPanelSchedules(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId());
						List<StafferMaster>	sList=stafferMasterDAO.findStafferMasterList(districtMaster,userSchoolMaster);
						List<DistrictKeyContact> dKCList=new ArrayList<DistrictKeyContact>();
						List<UserMaster> userMasters=new ArrayList<UserMaster>();
						UserMaster stafferUser=null;
						if(sList.size()==0){
							try{
								dKCList=districtKeyContactDAO.findByContactType(districtMaster,"Staffer");	
								if(dKCList.size()>0){
									userMasters=userMasterDAO.findByEmailAndDate(dKCList.get(0).getKeyContactEmailAddress());
									if(userMasters.size()>0){
										for (UserMaster userMasterObj : userMasters) {
											stafferUser=userMasterObj;
											break;
										}
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
						//added by 21-03-2015
						if(((isPhiladelphia && secondaryStatusForPanel.getSecondaryStatusName().equals("No Evaluation Complete"))||(isMiami && secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready"))) && districtMaster!=null && userSchoolMaster!=null && pSList.size()==0 && (sList.size()>0 || stafferUser!=null ) ){
							UserMaster[] userMasterArray=new UserMaster[2];
							try{
								userMasterArray[0]=userMaster;
								if(sList.size()>0){
									userMasterArray[1]=sList.get(0).getUserMaster();
								}else{
									userMasterArray[1]=stafferUser;
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							if(outerJob==false){
								savePanelByCG(request,userMaster,jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId(),userMasterArray,jobWisePanelStatusObj,isPhiladelphia);
								/*
								 * 	Purpose	: 	Send Mail For JobCategory "Adult/Vocational" for Miami District.
								 *  By		:	Hanzala Subhani
								 *  Dated	:	23 April 2015
								 * 
								 */
								List<String> ccEmailAddress		=	new ArrayList<String>();
						    	List<String> bccEmailAddress	=	new ArrayList<String>();
						    	if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1200390) && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Adult Education (Vocational)") &&  userMaster.getEntityType().equals(3)){
						    		//System.out.println(":::::::::::: Mail Sent Afted Creation OF Pannel ::::::::::");
						    		// Claude Archer, Certification (Arlene Diaz and Julio Montiel), principal and applicant (see slide 3)
									ccEmailAddress.add("CARCHER@DADESCHOOLS.NET");
									ccEmailAddress.add("ADIAZ4@DADESCHOOLS.NET");
									ccEmailAddress.add("JMONTIEL@DADESCHOOLS.NET");
									bccEmailAddress.add("hanzala@netsutra.com");
									for(int k=0;k<userMasterArray.length;k++){
										if(userMasterArray[k].getEntityType().equals(3)){
											bccEmailAddress.add(userMasterArray[k].getEmailAddress());
										}
									}
									String mailContent	=	MailText.getOfferReadyByPrincipal(request, jobForTeacherObj);
									String mailSubject	=	jobForTeacherObj.getJobId().getJobTitle()+" Offer Letter";
							    	String mailTo 		= 	jobForTeacherObj.getTeacherId().getEmailAddress();
							    	System.out.println(mailSubject);
							    	System.out.println(mailContent);
								    	if(ccEmailAddress!=null && ccEmailAddress.size() > 0){
								    		emailerService.sendMailAsHTMLByListToCc(mailTo,bccEmailAddress,ccEmailAddress,mailSubject,mailContent);
								    	}
								    }
							}
						}
					}
				}
			}catch(Exception e){}
			////////////////////Auto Panel Ceration End///////////////////////////////////

			//System.out.println(jobOrder.getDistrictMaster()+"!=null && "+statusMaster+"!=null && ("+jobWiseScoreFlag+" || "+finalizeStatusFlag+" ) && ("+statusMaster.getStatusShortName()+" jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()="+jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints());
			if(jobOrder.getDistrictMaster()!=null && statusMaster!=null && (jobWiseScoreFlag || finalizeStatusFlag ) && (statusMaster.getStatusShortName().equalsIgnoreCase("scomp") || statusMaster.getStatusShortName().equalsIgnoreCase("ecomp") || statusMaster.getStatusShortName().equalsIgnoreCase("vcomp"))){
				if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()!=null){
					if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()){
						String accessDPoints="";
						if(jobOrder.getDistrictMaster().getAccessDPoints()!=null)
							accessDPoints=jobOrder.getDistrictMaster().getAccessDPoints();

						if(accessDPoints.contains(""+statusMaster.getStatusId())){
							List<TeacherStatusHistoryForJob> lstTSHJob =teacherStatusHistoryForJobDAO.findByTeacherStatusHistorySelected(teacherDetail,jobOrder);
							List<SecondaryStatus> listSecondaryStatus = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithStatusAndHBD(jobOrder,statusMaster);
							int isInternalCandidate=jobForTeacherObj.getIsAffilated()==0?3:2;
							Map<Integer,SecondaryStatus> secMap=new HashMap<Integer, SecondaryStatus>();
							for(TeacherStatusHistoryForJob tSHObj : lstTSHJob){
								if(tSHObj.getSecondaryStatus()!=null)
									secMap.put(tSHObj.getSecondaryStatus().getSecondaryStatusId(),tSHObj.getSecondaryStatus());
							}
							if(listSecondaryStatus.size()>0){
								List<SecondaryStatus> lstSecondaryStatus=listSecondaryStatus.get(0).getChildren();
								List<JobCategoryWiseStatusPrivilege> lstJCWSP= jobCategoryWiseStatusPrivilegeDAO.getStatus(jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster(),lstSecondaryStatus);
								List<SecondaryStatus> lstSS=new ArrayList<SecondaryStatus> ();
								for(JobCategoryWiseStatusPrivilege jcwspObj:lstJCWSP){
									if(jcwspObj.getInternalExternalOrBothCandidate()!=null){
									if(jcwspObj.getInternalExternalOrBothCandidate()==2 && isInternalCandidate!=2)
										lstSS.add(jcwspObj.getSecondaryStatus());
									else if(jcwspObj.getInternalExternalOrBothCandidate()==3 && isInternalCandidate!=3)
										lstSS.add(jcwspObj.getSecondaryStatus());
									}else{
										lstSS.add(jcwspObj.getSecondaryStatus());
									}
								}
								lstSecondaryStatus.removeAll(lstSS);
								int counter=0;
								for(SecondaryStatus  obj : listSecondaryStatus.get(0).getChildren()){
									if(obj.getStatus().equalsIgnoreCase("A") && obj.getStatusMaster()==null){
										SecondaryStatus sec=secMap.get(obj.getSecondaryStatusId());
										if(sec==null){
											boolean isJobAssessment=false;
											if(jobOrder.getIsJobAssessment()!=null){
												if(jobOrder.getIsJobAssessment()){
													isJobAssessment=true;
												}
											}
											if((isJobAssessment && obj.getSecondaryStatusName().equalsIgnoreCase("JSI"))|| !obj.getSecondaryStatusName().equalsIgnoreCase("JSI")){
												if(counter==1){
													secondaryStatusNames+=", ";
												}
												counter=0;
												secondaryStatusNames+=obj.getSecondaryStatusName();
												counter++;
											}
										}
									}
								}
							}
							if(!secondaryStatusNames.equals("")){
								errorFlag="5";
							}
							//********************************************End TPL 2024 (Internal And External Candidate) ***************************************************//
		
						}
					}
				}
			}
			
			if((headQuarterMaster!=null || branchMaster!=null || districtMaster!=null) && jobCategoryMaster!=null && secondaryStatusForJobs!=null && finalizeStatus!=null && finalizeStatus.equals("1"))
            {
                List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivilegeList = jobCategoryWiseStatusPrivilegeDAO.getStatusByHBD(jobOrder, secondaryStatusForJobs);
                if(jobCategoryWiseStatusPrivilegeList.size()>0)
                {
                        if(jobCategoryWiseStatusPrivilegeList.get(0).isOverrideUserSettings())
                             overrideAll=true;
                }
            }

			
			
			
			////////////////////////Start multiplejob Add one time //////////////////////
			if(jobCategoryFlagValue!=2 && entityType==2 && secondaryStatusNames.equals("")){
				List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=jobCategoryWiseStatusPrivilegeDAO.getFilterStatusByHBD(jobOrder, secondaryStatusForJobs);
				System.out.println("jobCategoryWiseStatusPrivileges:::::::::::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+jobCategoryWiseStatusPrivileges.size());
				int jobCategoryFlag=3;
				if(superAdminClickOnEditNote!=1)//added by 03-04-2015
				try{
					if(jobCategoryWiseStatusPrivileges.size()>0){
						if(jobCategoryWiseStatusPrivileges.get(0).isAutoUpdateStatus()){
							if(jobCategoryWiseStatusPrivileges.get(0).getUpdateStatusOption()!=null){
								if(jobCategoryWiseStatusPrivileges.get(0).getUpdateStatusOption()==0){
									jobCategoryFlag=0;
								}else if(jobCategoryWiseStatusPrivileges.get(0).getUpdateStatusOption()==1){
									jobCategoryFlag=1;
								}else if(jobOrder.getGeoZoneMaster()!=null){
									jobCategoryFlag=2;	
								}
							}
						}
					//	if(jobCategoryWiseStatusPrivileges.get(0).isOverrideUserSettings())
					//		overrideAll=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				Map<Integer,SecondaryStatus> sStatusMap= new HashMap<Integer, SecondaryStatus>();
				//System.out.println("::::::::::::::::::::jobCategoryFlag:::::::::::::::::::::::"+jobCategoryFlag);
				try{
					if(statusMasterForJobs!=null)
						if(statusMasterForJobs.getStatusShortName().equalsIgnoreCase("widrw") || statusMasterForJobs.getStatusShortName().equalsIgnoreCase("hird")|| statusMasterForJobs.getStatusShortName().equalsIgnoreCase("dcln")|| statusMasterForJobs.getStatusShortName().equalsIgnoreCase("rem")){
							jobCategoryFlag=3;
						}
				}catch(Exception e){}
				try{
					if(jobCategoryFlag!=3){
						List<JobCategoryMaster> jobCategoryMasters= new ArrayList<JobCategoryMaster>();
						List<JobForTeacher> jobForTeachers=new ArrayList<JobForTeacher>();
						List<SecondaryStatus> secondaryStatusList= new ArrayList<SecondaryStatus>();
						if(jobCategoryFlag==0){
							jobForTeachers= jobForTeacherDAO.findJobByTeacherAndJobCategoryHBD(teacherDetail,jobOrder,jobForTeacherId);
							jobCategoryMasters.add(jobCategoryMaster);
						}else if(jobCategoryFlag==1){
							jobForTeachers= jobForTeacherDAO.findJobByTeacherAndDistrictHBD(teacherDetail,jobOrder,jobForTeacherId);
							if(jobForTeachers.size()>0)
								for (JobForTeacher jobForTeacher : jobForTeachers) {
									jobCategoryMasters.add(jobForTeacher.getJobId().getJobCategoryMaster());
								}
							secondaryStatusList=secondaryStatusDAO.findSecondaryStatusByJobCategoryAndDistrictHBD(jobCategoryMasters,jobOrder,secondaryStatusForJobs);

							if(secondaryStatusList.size()>0)
								for (SecondaryStatus secondaryStatus2 : secondaryStatusList) {
									sStatusMap.put(secondaryStatus2.getJobCategoryMaster().getJobCategoryId(),secondaryStatus2);
								}
						}else if(jobCategoryFlag==2){

							String jobTitle=jobOrder.getJobTitle();
							if(jobTitle.indexOf("(")>0){
								jobTitle=jobTitle.substring(0,jobTitle.lastIndexOf("(")).trim();
								//System.out.println("jobTitle:::::::::"+jobTitle);
							}
							//System.out.println("jobTitle::::::"+jobTitle);
							jobForTeachers= jobForTeacherDAO.findJobByTeacherAndJobTitleHBD(teacherDetail,jobOrder,jobForTeacherId,jobTitle);
							jobCategoryMasters.add(jobCategoryMaster);
						}
						System.out.println("jobForTeachers::::::"+jobForTeachers.size());
						List<JobOrder> jobOrdersLst=new ArrayList<JobOrder>();
						for (JobForTeacher jobForTeacher : jobForTeachers) {
							jobOrdersLst.add(jobForTeacher.getJobId());
						}
						List<StatusSpecificQuestions> lstStatusSpecificQuestions = new ArrayList<StatusSpecificQuestions>();
						lstStatusSpecificQuestions=statusSpecificQuestionsDAO.findQuestions_msuByHBD(jobOrder, secondaryStatusForJobs, statusMasterForJobs);
						MassStatusUpdateService msusObj= new MassStatusUpdateService();
						List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
						Map<JobOrder,List<StatusSpecificScore>> mapStatusScoreDetailsByJobOrder= new HashMap<JobOrder,List<StatusSpecificScore>>();
						Map<String,StatusSpecificScore> ssfMap=new HashMap<String, StatusSpecificScore>();
						Map<String,StatusSpecificScore> ssfMapfoCategory=new HashMap<String, StatusSpecificScore>();
						Map<String,StatusSpecificScore> questionTxtMap= new HashMap<String, StatusSpecificScore>();
						Map<Integer,StatusSpecificScore> questionMap= new HashMap<Integer, StatusSpecificScore>();

						//System.out.println("lstStatusSpecificQuestions.size()::::"+lstStatusSpecificQuestions.size());
						String recentStatusName=null;
						try{
							if(secondaryStatusForJobs!=null){
								recentStatusName=secondaryStatusForJobs.getSecondaryStatusName();
							}else{
								recentStatusName=statusMasterForJobs.getStatus();
							}
						}catch(Exception e){}
						if(iReturnValue!=0){
							try{
								if(lstStatusSpecificQuestions.size() > 0 && jobOrdersLst.size()>0)
								{
									lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList_msuByHBD(jobOrder,jobOrdersLst,statusMasterForJobs,secondaryStatusForJobs,teacherDetail,userMaster);

									if(lstStatusSpecificScore.size() == 0)
									{
										if(jobOrder.getJobCategoryMaster()!=null)
											statusSpecificScoreDAO.copyQuestionAnswersForAllJobCategoryLst_msuByHBD(jobCategoryMasters,jobOrdersLst, statusMasterForJobs,secondaryStatusForJobs, secondaryStatusList, userMaster, teacherDetail);
									}
									else{
										mapStatusScoreDetailsByJobOrder=msusObj.getStatusScoreDetailsByJobOrder(lstStatusSpecificScore);

										List<JobOrder> jobOrdersLst_Partial=new ArrayList<JobOrder>();
										jobOrdersLst_Partial=msusObj.getJobOrderListWithNoQuestion(jobOrdersLst, mapStatusScoreDetailsByJobOrder);

										if(jobOrdersLst_Partial.size() > 0)
										{
											if(jobOrder.getJobCategoryMaster()!=null)
												statusSpecificScoreDAO.copyQuestionAnswersForAllJobCategoryLst_msuByHBD(jobCategoryMasters,jobOrdersLst_Partial, statusMasterForJobs,secondaryStatusForJobs, secondaryStatusList, userMaster, teacherDetail);
										}
									}

								}
							}catch(Exception e){}
							List<StatusSpecificScore> statusSpecificScoreList=statusSpecificScoreDAO.getStatusSpecificScoreList(answerId_array);

							for(StatusSpecificScore sssOBJ : statusSpecificScoreList ){
								if(sssOBJ.getStatusSpecificQuestions()!=null){
									if(sssOBJ.getSkillAttributesMaster()!=null){
										questionTxtMap.put(sssOBJ.getStatusSpecificQuestions().getQuestion()+"##"+sssOBJ.getSkillAttributesMaster().getSkillAttributeId(),sssOBJ);
									}else{
										questionTxtMap.put(sssOBJ.getStatusSpecificQuestions().getQuestion(),sssOBJ);
									}
									questionMap.put(sssOBJ.getStatusSpecificQuestions().getQuestionId(),sssOBJ);
								}else{
									questionMap.put(sssOBJ.getTeacherAnswerDetail().getAnswerId(),sssOBJ);
								}
							}
							if(jobOrdersLst.size()>0)
								lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionAllJobCategoryList_msuBYHBD(jobOrder,jobCategoryMasters,jobOrdersLst,statusMasterForJobs,secondaryStatusForJobs,secondaryStatusList,teacherDetail,userMaster);
							for (StatusSpecificScore statusSpecificScore : lstStatusSpecificScore) {
								if(statusSpecificScore.getStatusSpecificQuestions()!=null){
									if(statusSpecificScore.getJobCategoryId().equals(jobCategoryMaster.getJobCategoryId())){
										String ssfValue=statusSpecificScore.getStatusSpecificQuestions().getQuestionId()+"##"+statusSpecificScore.getJobOrder().getJobId();
										ssfMap.put(ssfValue,statusSpecificScore);	
									}else{
										String ssfCValue=null;
										if(statusSpecificScore.getSkillAttributesMaster()!=null){
											ssfCValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+statusSpecificScore.getSkillAttributesMaster().getSkillAttributeId()+"##"+statusSpecificScore.getJobOrder().getJobId();
										}else{
											ssfCValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+statusSpecificScore.getJobOrder().getJobId();
										}
										ssfMapfoCategory.put(ssfCValue,statusSpecificScore);	
									}
								}else{
									if(statusSpecificScore.getJobCategoryId().equals(jobCategoryMaster.getJobCategoryId())){
										String ssfValue=statusSpecificScore.getTeacherAnswerDetail().getAnswerId()+"##"+statusSpecificScore.getJobOrder().getJobId();
										ssfMap.put(ssfValue,statusSpecificScore);
									}else{
										String ssfCValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getTeacherAnswerDetail().getAnswerId()+"##"+statusSpecificScore.getJobOrder().getJobId();
										ssfMapfoCategory.put(ssfCValue,statusSpecificScore);	
									}
								}
							}
						}
						String saveMultiReturnVal="";
						for (JobForTeacher jobForTeacher : jobForTeachers){
							if(recentStatusName!=null && !recentStatusName.equalsIgnoreCase("jsi")){
								if(jobCategoryFlag==1){
									if(sStatusMap.get(jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryId())!=null){
										secondaryStatusForJobs=sStatusMap.get(jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryId());
										saveMultiReturnVal=saveStatusNoteForJobs(questionTxtMap,jobCategoryMaster,ssfMapfoCategory,questionMap,ssfMap,jobForTeacher,fitScore,teacherStatusNoteId,userMaster,jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterForJobs,secondaryStatusForJobs,statusNotes,statusNoteFileName,emailSentToD,emailSentToS,scoreProvided,finalizeStatus,bEmailSendFor_DA_SA,bEmailSendFor_CA,answerId_array,score_array,isEmailTemplateChanged,hiddenStatusShortName,msgSubject,mailBody,txtschoolCount,schoolIdInputFromUser,isEmailTemplateChangedTeacher,msgSubjectTeacher,mailBodyTeacher,chkOverridetSatus,requisitionNumberId,qqNoteIdsvalue_str,questionAssessmentIds,jobCategoryFlagValue);
									}
								}else{
									saveMultiReturnVal=saveStatusNoteForJobs(questionTxtMap,jobCategoryMaster,ssfMapfoCategory,questionMap,ssfMap,jobForTeacher,fitScore,teacherStatusNoteId,userMaster,jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterForJobs,secondaryStatusForJobs,statusNotes,statusNoteFileName,emailSentToD,emailSentToS,scoreProvided,finalizeStatus,bEmailSendFor_DA_SA,bEmailSendFor_CA,answerId_array,score_array,isEmailTemplateChanged,hiddenStatusShortName,msgSubject,mailBody,txtschoolCount,schoolIdInputFromUser,isEmailTemplateChangedTeacher,msgSubjectTeacher,mailBodyTeacher,chkOverridetSatus,requisitionNumberId,qqNoteIdsvalue_str,questionAssessmentIds,jobCategoryFlagValue);
								}
								if(saveMultiReturnVal!=null && !saveMultiReturnVal.equals("")){
									multiJobReturn=1;
								}
							}
						}
					}
				}catch(Exception e){}
			}
			//System.out.println("Final multiJobReturn::::::"+multiJobReturn);
			//////////////////////End multiplejob Add one time //////////////////////

			System.out.println("------------------------------------------End Multiple Jobs-------------------------------------------------------------------------------");
			String hireText="";
			if(headQuarterMaster!=null){
				hireText="Active";
			}else{
				hireText="Hire";
			}


			if(statusMaster!=null && (jobWiseScoreFlag || finalizeStatusFlag )){
				if(statusMaster.getStatusShortName().equalsIgnoreCase("hird")){
					if(getPrimaryStatus(lstTeacherStatusHistoryForJob)!=null && !getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("EH")){
						if(getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("ED")){
							statusUpdateHDR="Please remove the Declined status of the Candidate first and then Hire";
						}else if(getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("ER")){
							statusUpdateHDR="Please remove the Rejected status of the Candidate first and then "+hireText;
						}
					}
				}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln")){
					if(getPrimaryStatus(lstTeacherStatusHistoryForJob)!=null && !getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("ED")){
						if(getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("EH")){
							statusUpdateHDR="Please remove the Hired status of the Candidate first and then Decline";
						}else if(getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("ER")){
							statusUpdateHDR="Please remove the Rejected status of the Candidate first and then Decline";
						}
					}
				}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
					if(getPrimaryStatus(lstTeacherStatusHistoryForJob)!=null && !getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("ER")){
						if(getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("ED")){
							statusUpdateHDR="Please remove the Declined status of the Candidate first and then Rejected";
						}else if(getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("EH")){
							statusUpdateHDR="Please remove the Hired status of the Candidate first and then Rejected";
						}
					}
				}
			}
			//System.out.println("statusUpdateHDR:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::>>>"+statusUpdateHDR);
			String schoolLocation="";
			String locationCode="";
			if(isMiami || isPhiladelphia){ //added by 21-03-2015
				JobForTeacher jobForTeacherForisMiami = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);


				List<SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus= new ArrayList<SchoolWiseCandidateStatus>();
				if(userMaster!=null)
					if(userMaster.getEntityType()==3){	
						lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusHistoryForSchoolByHBD(jobForTeacherForisMiami.getTeacherId(),jobForTeacherForisMiami.getJobId(),userMaster.getSchoolId());
					}else if(userMaster.getEntityType()==2){
						lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusHistoryByUserAndHBD(jobForTeacherForisMiami.getTeacherId(), jobForTeacherForisMiami.getJobId(),userMaster);
					}

				//System.out.println("lstSchoolWiseCandidateStatus:::::::::::::::::::::>>>>>>>>>"+lstSchoolWiseCandidateStatus.size());
				if(lstSchoolWiseCandidateStatus.size()>0){
					SchoolWiseCandidateStatus schoolWiseCandidateStatus=lstSchoolWiseCandidateStatus.get(0);
					schoolWiseCandidateStatusDAO.makeTransient(schoolWiseCandidateStatus);
					jobForTeacherForisMiami.setOfferAcceptedDate(null);
					jobForTeacherForisMiami.setOfferAccepted(null);
					jobForTeacherDAO.updatePersistent(jobForTeacherForisMiami);
					try{
						TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
						StatusMaster statusMasterObj  = findStatusByShortName(lstStatusMaster,"dcln");
						tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacherForisMiami.getTeacherId(),jobForTeacherForisMiami.getJobId(),statusMasterObj,"A");
						if(tSHJ!=null){
							tSHJ.setStatus("I");
							tSHJ.setHiredByDate(null);
							tSHJ.setUpdatedDateTime(new Date());
							tSHJ.setUserMaster(userMaster);
							teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}

				try{
					SchoolMaster schoolObj=null;
					if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
						try{
							schoolObj=jobRequisitionNumbersDAO.findSchoolByJobReqId(jobOrder,requisitionNumberId);
						}catch(Exception e){}
					}
					if(jobForTeacherForisMiami!=null && jobForTeacherForisMiami.getRequisitionNumber()!=null){
						List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobOrder,jobForTeacherForisMiami.getJobId().getDistrictMaster(),jobForTeacherForisMiami.getRequisitionNumber());
						if(jobReqList.size()>0){
							try{
								schoolObj=jobReqList.get(0).getSchoolMaster();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					if(schoolObj!=null){	
						schoolLocation=schoolObj.getSchoolName();
						locationCode=schoolObj.getLocationCode();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}

			// Start ... Panel
			List<UserMaster> userMasters_PanelAttendees=new ArrayList<UserMaster>();
			List<UserMaster> panelAttendees_ForEmail=new ArrayList<UserMaster>();
			List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
			Map<Integer,UserMaster> panelMemberMap=new HashMap<Integer, UserMaster>();

			SecondaryStatus secondaryStatus_temp=null;
			StatusMaster statusMaster_temp=null;
			List<SecondaryStatus> lstSecondaryStatus_temp=new ArrayList<SecondaryStatus>();

			if(statusMaster!=null)
			{
				lstSecondaryStatus_temp=secondaryStatusDAO.getSecondaryStatusForPanelByHBD(jobOrder, statusMaster);
				if(lstSecondaryStatus_temp.size() ==1)
				{
					statusMasterSecondaryName=lstSecondaryStatus_temp.get(0).getSecondaryStatusName();//added
					secondaryStatus_temp=lstSecondaryStatus_temp.get(0);
					statusMaster_temp=null;
				}
				else
				{
					statusMaster_temp=statusMaster;
				}
			}
			else

			{
				secondaryStatus_temp=secondaryStatus;
			}
			try{
				if(statusMasterSecondaryName==null){
					statusMasterSecondaryName=statusName;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("statusMasterSecondaryName::::::-:>>>>>:"+statusMasterSecondaryName);
			jobWisePanelStatusList=jobWisePanelStatusDAO.getPanel(jobOrder, secondaryStatus_temp, statusMaster_temp);
			boolean smartPractices=false;
			if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
					smartPractices=true;
				}
			}
			//System.out.println("flowOptionIds::::::::::::::::::::::::::::::::::::::>>>>>>>>>>>>>>>>"+flowOptionIds);
			if(smartPractices){
				List<WorkFlowOptions> flowOptions=flowOptionsDAO.getFlowOptions();
				Map<Integer,String> flowMap=new HashMap<Integer, String>();
				for (WorkFlowOptions flowOptionsObj : flowOptions) {
					flowMap.put(flowOptionsObj.getFlowOptionId(),flowOptionsObj.getFlowOptionName());
				}
				//System.out.println("flowMap::::Size::::::::::::::::::::::::::::::"+flowMap.size());
				WorkFlowOptionsForStatus flowOptionsForStatus=flowOptionsForStatusDAO.findOptionsByTeacherStatus(teacherDetail, jobOrder, statusMaster, secondaryStatus);
				//System.out.println("flowOptionsForStatus::::::::::::::::::::::::"+flowOptionsForStatus);
				String flowOptionsName="";
				try{
					String flowOptionsId[]=null;
					try{
						if(flowOptionIds!=null && !flowOptionIds.equals("")){
							flowOptionsId= flowOptionIds.split("#");
							int countFlow=0;
							for(int i=1;i<flowOptionsId.length;i++)
							{
								boolean existFlowOptions=false;
								if(flowOptionsForStatus!=null){
									if(flowOptionsForStatus.getFlowOptionIds().contains("#"+flowOptionsId[i]+"#")){
										existFlowOptions=true;
									}
								}
								
								if(existFlowOptions==false){
									if(countFlow!=0){
										flowOptionsName	 =	flowOptionsName+"<br/>"+flowMap.get(Integer.parseInt(flowOptionsId[i]));
									}else{
										flowOptionsName	 =	flowOptionsName+flowMap.get(Integer.parseInt(flowOptionsId[i]));
									}
									countFlow++;
								}
							}
						}
					}catch(Exception e){
						//e.printStackTrace();
					}
					//System.out.println("flowOptionsName:===============>===============:::::::"+flowOptionsName);
					if(flowOptionIds!=null && !flowOptionIds.equals("") && flowOptionsName!=null && !flowOptionsName.equals("")){
						if(flowOptionsForStatus==null){
							flowOptionsForStatus=new WorkFlowOptionsForStatus();
						}
						flowOptionsForStatus.setFlowOptionIds(flowOptionIds);
						flowOptionsForStatus.setJobId(jobOrder.getJobId());
						flowOptionsForStatus.setTeacherId(teacherDetail.getTeacherId());
						if(statusMaster!=null){
							flowOptionsForStatus.setStatusId(statusMaster.getStatusId());
							flowOptionsForStatus.setSecondaryStatusId(null);
						}else{
							flowOptionsForStatus.setStatusId(null);
							flowOptionsForStatus.setSecondaryStatusId(secondaryStatus.getSecondaryStatusId());
						}
						flowOptionsForStatus.setCreatedBy(userMaster.getUserId());
						flowOptionsForStatus.setStatus("A");
						flowOptionsForStatus.setCreatedDateTime(new Date());
						flowOptionsForStatusDAO.makePersistent(flowOptionsForStatus);
						
						statusWiseFlow(jobOrder,teacherDetail,statusMaster,secondaryStatus,statusMasterSecondaryName,userMaster,flowOptionsName);
					}
				}catch(Exception  e){
					e.printStackTrace();
				}
			}
			boolean bPanel=false;
			int iPanelAttendeeCount=0;
			StatusMaster statusMasterPanel=null;
			SecondaryStatus secondaryStatusPanel=null;
			statusMasterPanel=statusMaster;
			secondaryStatusPanel=secondaryStatus;
			boolean flagforpanelAttendees=true;//added by 21-03-2015

			List<PanelSchedule> panelScheduleList=new ArrayList<PanelSchedule>();
			if(jobWisePanelStatusList.size() == 1)
			{

				bPanel=true;
				panelScheduleList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWisePanelStatusList,teacherDetail);
				if(panelScheduleList.size()==1)
				{
					PanelSchedule panelSchedule=null;
					if(panelScheduleList.get(0)!=null)
					{
						panelSchedule=panelScheduleList.get(0);
						List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
						panelAttendees=panelAttendeesDAO.getPanelAttendees(panelSchedule);
						iPanelAttendeeCount=panelAttendees.size();

						if(panelAttendees.size() > 0)
						{
							for(PanelAttendees attendees:panelAttendees)
							{
								if(attendees!=null)
								{
									if(isPhiladelphia && userMaster.getEntityType()==3 && statusMaster!=null && statusMaster.getStatus().equals("No Evaluation Complete")) //added by 21-03-2015
									flagforpanelAttendees=false;
									userMasters_PanelAttendees.add(attendees.getPanelInviteeId());
									if(!attendees.getPanelInviteeId().getUserId().equals(userMaster.getUserId()))
									{
										panelAttendees_ForEmail.add(attendees.getPanelInviteeId());
									}
									try{
										if(attendees.getPanelInviteeId()!=null){
											panelMemberMap.put(attendees.getPanelInviteeId().getUserId(),attendees.getPanelInviteeId());
										}
									}catch(Exception e){							
									}
								}
							}
						}

					}
				}
			}
			// End ... Panel

			if(!errorFlag.equals("5")){

				teacherStatusScoresList= teacherStatusScoresDAO.getFinalizeStatusScore(teacherDetail,jobOrder,statusMaster,secondaryStatus);
				teacherStatusNotesList= teacherStatusNotesDAO.getFinalizeStatusScore(teacherDetail,jobOrder,statusMaster,secondaryStatus);

				Map<Integer,Integer> noteScoreMap= new HashMap<Integer, Integer>();
				try{
					for (TeacherStatusScores teacherStatusScores: teacherStatusScoresList) {
						int count =0;
						try{
							count=noteScoreMap.get(teacherStatusScores.getUserMaster().getUserId());
						}catch(Exception e){

						}
						if(count==0){
							noteScoreMap.put(teacherStatusScores.getUserMaster().getUserId(), 1);
						}else{
							noteScoreMap.put(teacherStatusScores.getUserMaster().getUserId(), 1+count);
						}
					}
					for (TeacherStatusNotes teacherStatusNotes: teacherStatusNotesList) {
						int count =0;
						try{
							count=noteScoreMap.get(teacherStatusNotes.getUserMaster().getUserId());
						}catch(Exception e){

						}
						if(count==0){
							noteScoreMap.put(teacherStatusNotes.getUserMaster().getUserId(), 1);
						}else{
							noteScoreMap.put(teacherStatusNotes.getUserMaster().getUserId(), 1+count);
						}

					}
				}catch(Exception e){}

				TeacherStatusHistoryForJob canNotHireObj= new TeacherStatusHistoryForJob();
				boolean noInsert=true;
				if(statusMaster!=null && (statusMaster.getStatusShortName().equals("hird")|| statusMaster.getStatusShortName().equals("rem")|| statusMaster.getStatusShortName().equals("dcln"))){
					noInsert=false;
				}
				//System.out.println(outerJob+" "+noInsert+" && ("+selectedstatus+"|| "+selectedSecondaryStatus+") && ("+jobWiseScoreFlag+" || "+finalizeStatusFlag);
				//System.out.println(outerJob+"==false && "+statusHistory+"==null && "+noInsert+" && ("+selectedstatus+"==false|| "+selectedSecondaryStatus+"==false) && ("+jobWiseScoreFlag+" || "+finalizeStatusFlag+")");
				if(outerJob==false && noInsert && (selectedstatus|| selectedSecondaryStatus) && (jobWiseScoreFlag || finalizeStatusFlag)){//Ramnnath  && flagforpanelAttendees
					boolean histCheck=false;
					try{

						TeacherStatusHistoryForJob statusHistoryObj=teacherStatusHistoryForJobDAO.findByTeacherStatusAndUserMaster(teacherDetail,jobOrder,statusMaster,secondaryStatus,userMaster);
						if(statusHistoryObj==null){
							histCheck=true;
							Calendar c = Calendar.getInstance();
							c.add(Calendar.SECOND,01);
							Date date = c.getTime();
							TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
							tSHJ.setTeacherDetail(teacherDetail);
							tSHJ.setJobOrder(jobOrder);
							if(statusId!=null && !statusId.equals("0")){
								tSHJ.setStatusMaster(statusMaster);
							}
							if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
								tSHJ.setSecondaryStatus(secondaryStatus);
								SendEmailOfficialTranscripts(secondaryStatus,teacherDetail,jobOrder);
							}
							if(statusWaived){
								tSHJ.setStatus("W");
							}else{
								tSHJ.setStatus("S");
							}
							tSHJ.setCreatedDateTime(date);
							tSHJ.setUserMaster(userMaster);
							if(chkOverridetSatus){
								tSHJ.setOverride(chkOverridetSatus);
								tSHJ.setOverrideBy(userMaster);
								debugPrintln("Override entry first time");
							}
							teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
						}

					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
						if(jobForTeacher!=null && secondaryStatusId!=null && !secondaryStatusId.equals("0")){
							if(!bPanel)
							{
								//System.out.println("::::::::interviewInvites::::::::1");
								interviewInvites(I4InterviewInvites,null,null,secondaryStatus,secondaryStatusVVI,jobOrder,teacherDetail,userMaster,request);
								lstTDetails.clear();lstTDetails.add(teacherDetail);
								statusWise(jobOrder,lstTDetails,null,secondaryStatus,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
								jobForTeacher.setSecondaryStatus(secondaryStatus);
								jobForTeacher.setStatusMaster(null);
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
								jobForTeacher.setUpdatedDate(new Date());
								jobForTeacher.setLastActivity(secondaryStatus.getSecondaryStatusName());
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);
								jobForTeacherDAO.makePersistent(jobForTeacher);
								if(ccPrivilege){
									mailToTeacher(jobForTeacher,null,null,secondaryStatus,secondaryStatusCC,jobOrder,teacherDetail,userMaster,request);
								}
							}
						}else{
							try{
								if(jobForTeacher!=null && histCheck && statusId!=null && !statusId.equals("0") && selectedstatus && flagforpanelAttendees ){//added 21-03-2015
									if(statusMaster!=null && (statusMaster.getStatusShortName().equalsIgnoreCase("scomp") || statusMaster.getStatusShortName().equalsIgnoreCase("ecomp") || statusMaster.getStatusShortName().equalsIgnoreCase("vcomp"))){
										jobForTeacher.setStatus(statusMaster);
										if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
											jobForTeacher.setStatusMaster(null);
										}else{
											jobForTeacher.setStatusMaster(statusMaster);
										}
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMaster.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);
										jobForTeacherDAO.makePersistent(jobForTeacher);
										//System.out.println(":::::::::::::::Interview Check :::::::::::");
										mailSend = true;
										if(ccPrivilege){
											mailToTeacher(jobForTeacher,statusMaster,statusMasterCC,null,null,jobOrder,teacherDetail,userMaster,request);
										}
										interviewInvites(I4InterviewInvites,statusMaster,statusMasterVVI,null,null,jobOrder,teacherDetail,userMaster,request);
										lstTDetails.clear();lstTDetails.add(teacherDetail);
										statusWise(jobOrder,lstTDetails,statusMaster,null,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(outerJob==false && statusHistory==null && noInsert && (selectedstatus==false|| selectedSecondaryStatus==false) && (jobWiseScoreFlag || finalizeStatusFlag)){
					try{
						System.out.println(" Status Check :::::::::::::::::::::: 8");
						TeacherStatusHistoryForJob statusHistoryObj=teacherStatusHistoryForJobDAO.findByTeacherStatusAndUserMaster(teacherDetail,jobOrder,statusMaster,secondaryStatus,userMaster);
						if(statusHistoryObj==null){
							Calendar c = Calendar.getInstance();
							c.add(Calendar.SECOND,01);
							Date date = c.getTime();
							TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
							tSHJ.setTeacherDetail(teacherDetail);
							tSHJ.setJobOrder(jobOrder);
							if(statusId!=null && !statusId.equals("0")){
								tSHJ.setStatusMaster(statusMaster);
							}
							if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
								tSHJ.setSecondaryStatus(secondaryStatus);
								SendEmailOfficialTranscripts(secondaryStatus,teacherDetail,jobOrder);
							}
							if(statusWaived){
								tSHJ.setStatus("W");
							}else{
								tSHJ.setStatus("S");
							}
							tSHJ.setCreatedDateTime(date);
							tSHJ.setUserMaster(userMaster);
							if(chkOverridetSatus){
								tSHJ.setOverride(chkOverridetSatus);
								tSHJ.setOverrideBy(userMaster);
								debugPrintln("Override entry first time");
							}
							teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
						}

						if(secondaryStatus!=null){
							if(jobForTeacherObj.getSecondaryStatus()!=null){
								if(cgService.selectedNotPriSecondaryStatusCheck(secondaryStatus,jobForTeacherObj.getSecondaryStatus())){
									try{
										JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
										if(jobForTeacher!=null && secondaryStatusId!=null && !secondaryStatusId.equals("0")){
											if(!bPanel)
											{
												interviewInvites(I4InterviewInvites,null,null,secondaryStatus,secondaryStatusVVI,jobOrder,teacherDetail,userMaster,request);
												lstTDetails.clear();lstTDetails.add(teacherDetail);
												statusWise(jobOrder,lstTDetails,null,secondaryStatus,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
												jobForTeacher.setSecondaryStatus(secondaryStatus);
												jobForTeacher.setUpdatedBy(userMaster);
												jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
												jobForTeacher.setUpdatedDate(new Date());
												jobForTeacher.setLastActivity(secondaryStatus.getSecondaryStatusName());
												jobForTeacher.setLastActivityDate(new Date());
												jobForTeacher.setUserMaster(userMaster);
												jobForTeacherDAO.makePersistent(jobForTeacher);
												//System.out.println("::::::::interviewInvites::::::::2");
												if(ccPrivilege){
													mailToTeacher(jobForTeacher,null,null,secondaryStatus,secondaryStatusCC,jobOrder,teacherDetail,userMaster,request);
												}
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
								}
							}else{
								try{
									JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
									if(jobForTeacher!=null && secondaryStatusId!=null && !secondaryStatusId.equals("0")){
										if(!bPanel)
										{
											interviewInvites(I4InterviewInvites,null,null,secondaryStatus,secondaryStatusVVI,jobOrder,teacherDetail,userMaster,request);
											lstTDetails.clear();lstTDetails.add(teacherDetail);
											statusWise(jobOrder,lstTDetails,null,secondaryStatus,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
											jobForTeacher.setSecondaryStatus(secondaryStatus);
											jobForTeacher.setUpdatedBy(userMaster);
											jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
											jobForTeacher.setUpdatedDate(new Date());
											jobForTeacher.setLastActivity(secondaryStatus.getSecondaryStatusName());
											jobForTeacher.setLastActivityDate(new Date());
											jobForTeacher.setUserMaster(userMaster);
											jobForTeacherDAO.makePersistent(jobForTeacher);
											//System.out.println("::::::::interviewInvites::::::::3");
											if(ccPrivilege){
												mailToTeacher(jobForTeacher,null,null,secondaryStatus,secondaryStatusCC,jobOrder,teacherDetail,userMaster,request);
											}
										}
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}

				if(inputSumAnswerScore>0){
					scoreProvided=""+inputSumAnswerScore;
					fitScore=""+inputSumAnswerMaxScore;
				}

				if(outerJob==false && jobWiseScoreFlag ){
					int cScore=0;
					int maxScore=0;
					JobWiseConsolidatedTeacherScore jwScoreObj=null;
					jwScoreObj=jobWiseConsolidatedTeacherScoreDAO.getJWCTScore(teacherDetail, jobOrder);
					if(jwScoreObj==null){
						cScore=Integer.parseInt(scoreProvided);
						maxScore=Integer.parseInt(fitScore);
					}else{
						if(teacherStatusScoresList.size()>1){
							try{
								Double[] teacherStatusScoresAvg= teacherStatusScoresDAO.getAllFinalizeStatusScoreAvg(teacherDetail,jobOrder);
								cScore=Integer.parseInt(Math.round(teacherStatusScoresAvg[0])+"");
								maxScore=Integer.parseInt(Math.round(teacherStatusScoresAvg[1])+"");
							}catch(Exception e){ e.printStackTrace();}
						}else{
							cScore=Integer.parseInt(scoreProvided)+jwScoreObj.getJobWiseConsolidatedScore();	
							maxScore=Integer.parseInt(fitScore)+jwScoreObj.getJobWiseMaxScore();
						}
					}
					JobWiseConsolidatedTeacherScore jWScore= new JobWiseConsolidatedTeacherScore();
					if(jwScoreObj!=null){
						jWScore=jwScoreObj;
					}
					jWScore.setTeacherDetail(teacherDetail);
					jWScore.setJobOrder(jobOrder);
					jWScore.setJobWiseConsolidatedScore(cScore);
					if(maxScore!=0){
						jWScore.setJobWiseMaxScore(maxScore);
					}
					jobWiseConsolidatedTeacherScoreDAO.makePersistent(jWScore);
				}
				List<SchoolMaster> lstSchoolMasters = null;

				if(statusUpdateHDR==null && (jobWiseScoreFlag || finalizeStatusFlag)){
					errorFlag="3";
				}
				List<TeacherStatusNotes> statusNoteList=new ArrayList<TeacherStatusNotes>();
				try{
					statusNoteList=teacherStatusNotesDAO.getStatusNoteAllList(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId());
				}catch(Exception e){
					e.printStackTrace();
				}

				/******* Update Status *******/
				if(finalizeStatus!=null)
					if(outerJob==false && statusMaster!=null && statusMaster.getStatusShortName().equals("hird") && finalizeStatus.equals("2")){
						TeacherStatusHistoryForJob lastSelectedObj=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryLastSelected(teacherDetail,jobOrder);
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
						canUnhire=isUpdateStatus;

						if(canUnhire){
							int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
							statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);

							if(lastSelectedObj!=null && !lastSelectedObj.getStatusMaster().getStatusShortName().equals("hird")){
								statusMaster=lastSelectedObj.getStatusMaster();
							}
							if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
								try{
									if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null){
										List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobForTeacher.getJobId(),jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
										if(jobReqList.size()>0){
											boolean updateFlag=false;
											for (JobRequisitionNumbers jobReqObj : jobReqList) {
												try{
													if(jobForTeacher.getSchoolMaster()!=null && jobReqObj.getSchoolMaster().getSchoolId().equals(jobForTeacher.getSchoolMaster().getSchoolId()) && jobReqObj.getStatus()==1){
														updateFlag=true;
														jobReqObj.setStatus(0);
														jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
														break;
													}
												}catch(Exception e){
													e.printStackTrace();
												}
											}
											if(updateFlag==false){
												for (JobRequisitionNumbers jobReqObj : jobReqList){
													try{
														if(jobReqObj.getStatus()==1){
															updateFlag=true;
															jobReqObj.setStatus(0);
															jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
															break;
														}
													}catch(Exception e){
														e.printStackTrace();
													}
												}
											}
										}
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								errorFlag="3";
								jobForTeacher.setStatus(statusMaster);
								if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
									jobForTeacher.setStatusMaster(null);
								}else{
									jobForTeacher.setStatusMaster(statusMaster);
								}
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());	
								jobForTeacher.setLastActivity("UnHired");
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);
								jobForTeacher.setUpdatedDate(new Date());
								jobForTeacher.setSchoolMaster(null);
								jobForTeacher.setRequisitionNumber(null);
								jobForTeacherDAO.makePersistent(jobForTeacher);
								//System.out.println("W 04");

								try{
									TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
									StatusMaster statusMasterObj  = statusMasterDAO.findStatusByShortName("hird");
									tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterObj,"A");
									tSHJ.setJobOrder(jobOrder);
									if(tSHJ!=null){
										tSHJ.setStatus("I");
										tSHJ.setHiredByDate(null);
										tSHJ.setUpdatedDateTime(new Date());
										tSHJ.setUserMaster(userMaster);
										teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}else{
								canUnhire=true;
							}
						}
						if(canUnhire==false){
							errorFlag="4";
						}
					}else if(outerJob==false && isUpdateStatus && statusMaster!=null && statusMaster.getStatusShortName().equals("rem") && finalizeStatus.equals("4")){
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
						TeacherStatusHistoryForJob lastSelectedObj=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryLastSelected(teacherDetail,jobOrder);

						int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
						statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);

						if(lastSelectedObj!=null){
							statusMaster=lastSelectedObj.getStatusMaster();
						}
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
							errorFlag="3";
							try{
								TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
								StatusMaster statusMasterObj  = statusMasterDAO.findStatusByShortName("rem");
								tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterObj,"A");
								if(tSHJ!=null){
									tSHJ.setStatus("I");
									tSHJ.setUpdatedDateTime(new Date());
									tSHJ.setUserMaster(userMaster);
									teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							jobForTeacher.setStatus(statusMaster);
							if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
								jobForTeacher.setStatusMaster(null);
							}else{
								jobForTeacher.setStatusMaster(statusMaster);
							}
							jobForTeacher.setUpdatedBy(userMaster);
							jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
							jobForTeacher.setUpdatedDate(new Date());
							jobForTeacher.setLastActivity("UnRejected");
							jobForTeacher.setLastActivityDate(new Date());
							jobForTeacher.setUserMaster(userMaster);
							jobForTeacherDAO.makePersistent(jobForTeacher);
						}
					}else if(outerJob==false && isUpdateStatus && statusMaster!=null && statusMaster.getStatusShortName().equals("dcln") && finalizeStatus.equals("3")){
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
						TeacherStatusHistoryForJob lastSelectedObj=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryLastSelected(teacherDetail,jobOrder);
						int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
						statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);

						if(lastSelectedObj!=null){
							statusMaster=lastSelectedObj.getStatusMaster();
						}
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
							errorFlag="3";
							try{
								TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
								StatusMaster statusMasterObj  = statusMasterDAO.findStatusByShortName("dcln");
								tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterObj,"A");
								if(tSHJ!=null){
									tSHJ.setStatus("I");
									tSHJ.setHiredByDate(null);
									tSHJ.setUpdatedDateTime(new Date());
									tSHJ.setUserMaster(userMaster);
									teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							jobForTeacher.setStatus(statusMaster);
							if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
								jobForTeacher.setStatusMaster(null);
							}else{
								jobForTeacher.setStatusMaster(statusMaster);
							}
							jobForTeacher.setUpdatedBy(userMaster);
							jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
							jobForTeacher.setUpdatedDate(new Date());
							jobForTeacher.setLastActivity("UnDecline");
							jobForTeacher.setLastActivityDate(new Date());
							jobForTeacher.setOfferAccepted(null);
							jobForTeacher.setOfferAcceptedDate(null);
							jobForTeacher.setUserMaster(userMaster);
							jobForTeacherDAO.makePersistent(jobForTeacher);

						}
					}else if(outerJob==false && isUpdateStatus && finalizeStatus.equals("5")){ /*********Undo Status ************/
						//System.out.println("::::::::::::::::=Undo status:::::::::::");
						StatusMaster masterForDPoint=null;
						try{
							if(statusMaster!=null && (statusMaster.getStatusShortName().equalsIgnoreCase("scomp") || statusMaster.getStatusShortName().equalsIgnoreCase("ecomp") || statusMaster.getStatusShortName().equalsIgnoreCase("vcomp"))){
								masterForDPoint=statusMaster;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
						errorFlag="3";
						SchoolMaster panelSchool=null;
						boolean panelFlag=false;

						try{
							JobWisePanelStatus jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId());
							if(jobWisePanelStatus!=null){
								if(secondaryStatusForPanel!=null && secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready")){ 
									PanelSchedule panelSchedule=panelScheduleDAO.findPanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
									if(panelSchedule!=null){
										List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
										if(panelAttendeesList.size()>0){
											for(PanelAttendees panelAttendees :panelAttendeesList){
												if(panelAttendees.getPanelInviteeId()!=null){
													UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
													if(panelUserMaster.getEntityType()==3){
														panelSchool=panelUserMaster.getSchoolId();
														panelFlag=true;
													}
												}
											}
										}
										candidateGridSubAjax.cancelPanelEvent(panelSchedule.getPanelId());
									}
								}
							}else{
								//System.out.println(":::::::::::::Else::::::::::::");
								List<JobWisePanelStatus> jobWisePanelStatusWiseList = jobWisePanelStatusDAO.getPanel(jobForTeacher.getJobId(),secondaryStatus,masterForDPoint);
								//System.out.println("::::::::::::::::::::jobWisePanelStatusWiseList:::::::::"+jobWisePanelStatusWiseList.size());
								List <PanelSchedule> panelScheduleStatusList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWisePanelStatusWiseList,jobForTeacher.getTeacherId());
								//System.out.println("::::::::::::::::::::panelScheduleStatusList:::::::::"+panelScheduleStatusList.size());
								try{
									if(panelScheduleStatusList.size()==1){
										if(panelScheduleStatusList.get(0)!=null){
											List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelScheduleStatusList.get(0));
											//System.out.println("panelAttendeesList::::::"+panelAttendeesList.size());
											if(panelAttendeesList.size()>0){
												for(PanelAttendees panelAttendees :panelAttendeesList){
													if(panelAttendees.getPanelInviteeId()!=null){
														UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
														if(panelUserMaster.getUserId().equals(userMaster.getUserId())){
															panelFlag=true;
														}
													}
												}
											}
										}

									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}catch(Exception e){e.printStackTrace();}

						//System.out.println(panelFlag+":::panelFlag panelSchool::::::::"+panelSchool);

						List<TeacherStatusScores> teacherStatusScoresForUndoList= teacherStatusScoresDAO.getFinalizeStatusScore(teacherDetail,jobOrder,masterForDPoint,secondaryStatus);
						//System.out.println("teacherStatusScoresForUndoList:::::::::"+teacherStatusScoresForUndoList.size());
						try{
							if(teacherStatusScoresForUndoList!=null && teacherStatusScoresForUndoList.size()>0){
								for (TeacherStatusScores teacherStatusScores : teacherStatusScoresForUndoList) {
									teacherStatusScoresDAO.makeTransient(teacherStatusScores);
								}
								JobWiseConsolidatedTeacherScore jwScoreObj=jobWiseConsolidatedTeacherScoreDAO.getJWCTScore(teacherDetail, jobOrder);
								if(jwScoreObj!=null){
									try{
										Double[] teacherStatusScoresAvg= teacherStatusScoresDAO.getAllFinalizeStatusScoreAvg(teacherDetail,jobOrder);
										jwScoreObj.setJobWiseConsolidatedScore(Integer.parseInt(Math.round(teacherStatusScoresAvg[0])+""));
										jwScoreObj.setJobWiseMaxScore(Integer.parseInt(Math.round(teacherStatusScoresAvg[1])+""));
									}catch(Exception e){ e.printStackTrace();}
									jobWiseConsolidatedTeacherScoreDAO.updatePersistent(jwScoreObj);
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						if(panelFlag){
							try{
								List<TeacherStatusNotes> statusNoteUndoList=new ArrayList<TeacherStatusNotes>();
								try{
									statusNoteUndoList=teacherStatusNotesDAO.getFinalizeStatusScore(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId(),masterForDPoint,secondaryStatus);
								}catch(Exception e){
									e.printStackTrace();
								}
								//System.out.println("statusNoteUndoList:::::::::"+statusNoteUndoList.size());
								if(statusNoteUndoList.size()>0)
									for (TeacherStatusNotes teacherStatusNotes : statusNoteUndoList) {
										teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
									}
								jobForTeacher.setOfferReady(null);
								jobForTeacher.setOfferMadeDate(null);
								jobForTeacher.setRequisitionNumber(null);
								jobForTeacher.setOfferAcceptedDate(null);
								jobForTeacher.setOfferAccepted(null);
							}catch(Exception e){
								e.printStackTrace();
							}
							try{
								if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null){
									List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobForTeacher.getJobId(),jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
									if(jobReqList.size()>0){
										for (JobRequisitionNumbers jobReqObj : jobReqList) {
											try{
												if(panelSchool!=null && jobReqObj.getSchoolMaster().getSchoolId().equals(panelSchool.getSchoolId()) && jobReqObj.getStatus()==1){
													jobReqObj.setStatus(0);
													jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
													break;
												}
											}catch(Exception e){
												e.printStackTrace();
											}
										}
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
						try{
							List<TeacherStatusHistoryForJob>  statusHistoryObjList=teacherStatusHistoryForJobDAO.findByTeacherStatusForUndo(teacherDetail,jobOrder,masterForDPoint,secondaryStatus);
							//System.out.println("statusHistoryObjList::::::::"+statusHistoryObjList.size());
							for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : statusHistoryObjList) {
								teacherStatusHistoryForJob.setStatus("I");
								teacherStatusHistoryForJob.setHiredByDate(null);
								teacherStatusHistoryForJob.setUpdatedDateTime(new Date());
								teacherStatusHistoryForJob.setUserMaster(userMaster);
								teacherStatusHistoryForJobDAO.updatePersistent(teacherStatusHistoryForJob);
							}

						}catch(Exception e){
							e.printStackTrace();
						}
						commonService.undoJobStatus(jobForTeacher);
					}
				/*********End Update Status ************/
				if(outerJob==false && statusUpdateHDR==null && isUpdateStatus && selectedstatus)
					if(statusMaster!=null && statusMaster.getStatusShortName().equals("hird") && (jobWiseScoreFlag || finalizeStatusFlag)){
						int noOfHire = 0;
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
							if(userMaster.getEntityType()==2){//District - 2 user login type
								noOfHire = (jobForTeacherDAO.findHireByJobForDistrict(jobOrder, userMaster.getDistrictId())).size();
								canHire = isUpdateStatus;;
								if(jobOrder.getWritePrivilegeToSchool()!=null && jobOrder.getWritePrivilegeToSchool()){
									List<SchoolMaster> schoolMasterList=new ArrayList<SchoolMaster>();
									schoolMasterList = jobOrder.getSchool();
									if(schoolMasterList.size()>0){
										try{
											SchoolMaster schoolMasterForJFT=null;
											if(txtschoolCount > 1){
												if(schoolIdInputFromUser!=null && schoolIdInputFromUser!=0){
													schoolMasterForJFT=schoolMasterDAO.findById(schoolIdInputFromUser, false, false);
													noOfHire = (jobForTeacherDAO.findHireBySchool(jobOrder, schoolMasterForJFT)).size();
													schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMasterForJFT);
												}
											}else if(jobOrder.getCreatedForEntity()==2 && jobOrder.getNoSchoolAttach()==2){
												schoolMasterForJFT=jobOrder.getSchool().get(0);
												noOfHire = (jobForTeacherDAO.findHireBySchool(jobOrder, schoolMasterForJFT)).size();
												schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMasterForJFT);
											}
										}catch(Exception e){
											e.printStackTrace();
										}
									}
									if(schoolInJobOrder==null || schoolInJobOrder.getNoOfSchoolExpHires()==null || noOfHire>=schoolInJobOrder.getNoOfSchoolExpHires())
									{	
										canHire = false;
									}
								}else{
									if(jobOrder.getNoOfExpHires()!=null && jobOrder.getNoOfExpHires()<=noOfHire){	
										canHire = false;
									}
								}
							}else{
								if(isUpdateStatus){
									if(userMaster.getSchoolId()!=null){
										schoolMaster = userMaster.getSchoolId();				
										statusMaster= statusMasterDAO.findStatusByShortName("hird");
										noOfHire = (jobForTeacherDAO.findHireBySchool(jobOrder, schoolMaster)).size();
										lstSchoolMasters = jobOrder.getSchool();
	
										schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMaster);
	
										if(schoolInJobOrder==null || schoolInJobOrder.getNoOfSchoolExpHires()==null || noOfHire>=schoolInJobOrder.getNoOfSchoolExpHires())
										{	
											canHire = false;
										}
									}
								}else{
									canHire = false;
								}
							}
							if(canHire){
								//Start ... update for hiredBySchool
								SchoolMaster schoolMasterForJFT=null;
								try{
									if(txtschoolCount > 1){
										if(schoolIdInputFromUser!=null && schoolIdInputFromUser!=0)
											schoolMasterForJFT=schoolMasterDAO.findById(schoolIdInputFromUser, false, false);
									}else{
										if(jobOrder.getCreatedForEntity()==2 && jobOrder.getNoSchoolAttach()==2){
											schoolMasterForJFT=jobOrder.getSchool().get(0);
										}else{
											if(schoolMaster!=null){
												schoolMasterForJFT=schoolMaster;
											}
										}
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								//End ... update for hiredBySchool
								String requisitionNumber=null;
								if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
									try{
										if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
											JobRequisitionNumbers jRNOb=jobRequisitionNumbersDAO.findById(Integer.parseInt(requisitionNumberId),false,false);
											if(jRNOb!=null){
												requisitionNumber=jRNOb.getDistrictRequisitionNumbers().getRequisitionNumber();
												List<JobRequisitionNumbers> jobReqNumbers=jobRequisitionNumbersDAO.findJobReqNumbers(jRNOb.getDistrictRequisitionNumbers());
												if(jobReqNumbers.size()==1){
													boolean multiHired=false;
													try{
														if(jRNOb.getJobOrder().getIsExpHireNotEqualToReqNo()){
															multiHired=true;
														}	
													}catch(Exception e){
														e.printStackTrace();
													}
													if(multiHired==false){
														jRNOb.setStatus(1);
														jobRequisitionNumbersDAO.updatePersistent(jRNOb);
													}
												}
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
								}
								////System.out.println("requisitionNumber::::"+requisitionNumber);
								//System.out.println(":::::::::::::::::setHiredDate::::::::::::::::::"+setHiredDate);

								errorFlag="3";
								mailSend=true;
								statusShortName=statusMaster.getStatusShortName();
								jobForTeacher.setStatus(statusMaster);
								jobForTeacher.setStatusMaster(statusMaster);
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
								jobForTeacher.setUpdatedDate(new Date());
								jobForTeacher.setLastActivity("Hired");
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);
								if(requisitionNumber!=null){
									jobForTeacher.setRequisitionNumber(requisitionNumber);
								}
								if(schoolMasterForJFT!=null)
									jobForTeacher.setSchoolMaster(schoolMasterForJFT);
								jobForTeacherDAO.makePersistent(jobForTeacher);
								//System.out.println("::::::::interviewInvites::::::::4");

								interviewInvites(I4InterviewInvites,statusMaster,statusMasterVVI,null,null,jobOrder,teacherDetail,userMaster,request);
								lstTDetails.clear();lstTDetails.add(teacherDetail);
								statusWise(jobOrder,lstTDetails,statusMaster,null,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
								if(ccPrivilege){
									mailToTeacher(jobForTeacher,statusMaster,statusMasterCC,null,null,jobOrder,teacherDetail,userMaster,request);
								}
								int live=Integer.parseInt(Utility.getValueOfPropByKey("isAPILive"));
								if(districtMaster!=null &&  districtMaster.getDistrictId().equals(3628590) && live==1)
								{
									//// Vishwanath (For Hired Status SCSD candidates)
									try {
										TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(jobForTeacher.getTeacherId().getTeacherId(), false, false);

										List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>();
										String requisitionNumber1 = jobForTeacher.getRequisitionNumber();
										
										Criterion criteria5 = Restrictions.eq("requisitionNumber", requisitionNumber1);
										lstDistrictRequisitionNumbers = districtRequisitionNumbersDAO.findByCriteria(criteria5);
										
										//System.out.println("lstDistrictRequisitionNumbers**inside ManageStatusAjax*******************: "+lstDistrictRequisitionNumbers.size());
										
										String postingNo = "";
										if(lstDistrictRequisitionNumbers.size()>0)
										{
											postingNo = lstDistrictRequisitionNumbers.get(0).getPostingNo();
										}
										
										List<RaceMaster> lstRace= null;
										lstRace = raceMasterDAO.findAllRaceByOrder();
										Map<String,RaceMaster> raceMap = new HashMap<String, RaceMaster>();
										for (RaceMaster raceMaster : lstRace) {
											raceMap.put(""+raceMaster.getRaceId(), raceMaster);
										}

										if(teacherPersonalInfo!=null)
										{
											PATSHiredPostThread phpt = new PATSHiredPostThread();
											phpt.setRaceMap(raceMap);
											phpt.setJobForTeacher(jobForTeacher);
											phpt.setTeacherPersonalInfo(teacherPersonalInfo);
											phpt.setPostingNo(postingNo);
											//TMCommonUtil.updateSDSCHiredCandidate(jobForTeacher, teacherPersonalInfo, raceMap);
											phpt.start();
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}
						}else{
							canHire=true;
						}
						if(canHire==false){
							errorFlag="2";
						}
					}else if(outerJob==false && selectedstatus && statusMaster!=null && statusMaster.getStatusShortName().equals("rem") && (jobWiseScoreFlag || finalizeStatusFlag)){
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){

							boolean staffer=false;
							SchoolMaster panelSchool=null;
							if(isMiami || isPhiladelphia){ //added by 21-03-2015
								try{
									JobWisePanelStatus jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId());
									if(jobWisePanelStatus!=null){
										//System.out.println("jobWisePanelStatus:::"+jobWisePanelStatus.getJobPanelStatusId());
										PanelSchedule panelSchedule=panelScheduleDAO.findPanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
										if(panelSchedule!=null){
											List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
											if(panelAttendeesList.size()>0)
												for(PanelAttendees panelAttendees :panelAttendeesList){
													if(panelAttendees.getPanelInviteeId()!=null){
														UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
														if(panelUserMaster.getEntityType()==3){
															panelSchool=panelUserMaster.getSchoolId();
														}
														if(panelUserMaster.getUserId().equals(userMaster.getUserId())){
															staffer=true;
														}
													}
												}
										}
									}
								}catch(Exception e){e.printStackTrace();}
							}
							////////////////////////////////////////////////////////////////////////////////
							try{
								Map<Integer,List<String>> statusHistoryMap=new HashMap<Integer, List<String>>();
								Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
								Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
								List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId());

								List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
								LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
								Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
								if(staffer){
									try{
										int counter=0; 
										for(SecondaryStatus tree: lstTreeStructure)
										{
											if(tree.getSecondaryStatus()==null)
											{
												if(tree.getChildren().size()>0){
													counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
												}
											}
										}
									}catch(Exception e){}

									if(statusNoteList.size()>0)
										for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
											if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready"))){
												teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
											}
										}
									boolean vComp=false;
									if(historyList.size()>0)
										for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
											if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready"))){
												if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp")){
													vComp=true;	
												}
												teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
											}

										}

									try{		
										historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId());
										//System.out.println("historyList::::::::::"+historyList.size());

										List<String> statusList=new ArrayList<String>();
										if(historyList.size()>0)
											for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
												if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
													statusList=new ArrayList<String>();
													if(teacherStatusHistoryForJob.getStatusMaster()!=null){
														statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
													}else{
														statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
													}
													statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
												}else{
													statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
													if(teacherStatusHistoryForJob.getStatusMaster()!=null){
														statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
													}else{
														statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
													}
													statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
												}
											}
									}catch(Exception e){}
									//System.out.println("statusHistoryMap:::::::::::"+statusHistoryMap.size());
									List<String> statusIds=statusHistoryMap.get(jobForTeacherObj.getTeacherId().getTeacherId());
									String schoolStatusId=cgService.getSchoolStatusId(statusIds, statusHistMap, statusNameMap);
									SecondaryStatus secondaryStatusObj=null;
									StatusMaster statusMasterObj=null;
									try{
										String statusIdMapValue[]=schoolStatusId.split("#");
										if(statusIdMapValue[0].equals("0")){
											secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
											jobForTeacher.setSecondaryStatus(secondaryStatusObj);
											if(vComp){
												jobForTeacher.setStatus(findStatusByShortName(lstStatusMaster,"ecomp"));
												jobForTeacher.setStatusMaster(null);
											}
										}else{
											statusMasterObj=statusMasterMap.get(schoolStatusId);
										}
									}catch(Exception e){e.printStackTrace();}

									try{
										if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null){
											List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobForTeacher.getJobId(),jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
											if(jobReqList.size()>0){
												for (JobRequisitionNumbers jobReqObj : jobReqList) {
													try{
														if(panelSchool!=null && jobReqObj.getSchoolMaster().getSchoolId().equals(panelSchool.getSchoolId()) && jobReqObj.getStatus()==1){
															jobReqObj.setStatus(0);
															jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
															break;
														}
													}catch(Exception e){
														e.printStackTrace();
													}
												}
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									jobForTeacher.setOfferReady(null);
									jobForTeacher.setOfferMadeDate(new Date());
									jobForTeacher.setRequisitionNumber(null);
									jobForTeacher.setOfferAcceptedDate(new Date());
									jobForTeacher.setOfferAccepted(null);
									jobForTeacherDAO.updatePersistent(jobForTeacher);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							if(entityType==3 && (isMiami || isPhiladelphia)){ //added by 21-03-2015
								try{
									//System.out.println("<<<<<<<<<::::::::::::: Inside School Reject :::::::::::::::>>>>>>>>>>>");
									SchoolWiseCandidateStatus swcsObj= new SchoolWiseCandidateStatus();
									swcsObj.setStatusMaster(statusMaster);
									swcsObj.setJobOrder(jobForTeacher.getJobId());
									swcsObj.setDistrictId(userMaster.getDistrictId().getDistrictId());
									swcsObj.setSchoolId(userSchoolMaster.getSchoolId());
									swcsObj.setUserMaster(userMaster);		
									swcsObj.setCreatedDateTime(new Date());
									swcsObj.setTeacherDetail(jobForTeacher.getTeacherId());
									swcsObj.setStatus("A");
									swcsObj.setJobForTeacher(jobForTeacher);
									schoolWiseCandidateStatusDAO.makePersistent(swcsObj);
									errorFlag="3";
									mailSend=true;
									statusShortName=statusMaster.getStatusShortName();
								}catch(Exception e){e.printStackTrace();}
							}else{
								//////////////Add Zone Wise Rejected By Sekhar////////////
								if(entityType==2 && staffer==false && (isMiami)){
									//System.out.println("::::::::::::::::::::::::::::No Staffer Rejected:::::::::::");
									try{
										String jobTitle=jobOrder.getJobTitle();
										if(jobTitle.indexOf("(")>0){
											jobTitle=jobTitle.substring(0,jobTitle.lastIndexOf("(")).trim();
										}
								
										List<JobForTeacher> jobForTeachers= jobForTeacherDAO.findJobByTeacherAndJobTitle(teacherDetail,districtMaster,jobCategoryMaster,jobForTeacherId,jobTitle);

										List<JobOrder> joblist=new ArrayList<JobOrder>();
										List<TeacherDetail> teacherList =new ArrayList<TeacherDetail>();
										try{
											for (JobForTeacher jobObj : jobForTeachers) {
												joblist.add(jobObj.getJobId());
												teacherList.add(jobObj.getTeacherId());
											}
										}catch(Exception e){
											e.printStackTrace();
										}
										List<TeacherStatusHistoryForJob> forJobs=teacherStatusHistoryForJobDAO.findByTeachersAndJobsStatus(teacherList, joblist);

										Map<String,TeacherStatusHistoryForJob> mapTSHFJ=new HashMap<String, TeacherStatusHistoryForJob>();
										if(forJobs!=null && forJobs.size() > 0)
										{
											for(TeacherStatusHistoryForJob tSHObj : forJobs){
												mapTSHFJ.put(tSHObj.getTeacherDetail().getTeacherId()+"#"+tSHObj.getJobOrder().getJobId(),tSHObj);
											}
										}
										TeacherStatusHistoryForJob teacherHistoryJobs=teacherStatusHistoryForJobDAO.findByTeacherStatusHistory(teacherDetail, jobOrder, statusMasterPanel, secondaryStatusPanel);
										TeacherStatusNotes noteObj=teacherStatusNotesDAO.getFinalizeNotesByOrder(teacherDetail, jobOrder, statusMaster, secondaryStatus, userMaster);
										for (JobForTeacher jobForObj : jobForTeachers) {
											TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
											String sTID_JID=jobForObj.getTeacherId().getTeacherId()+"#"+jobForObj.getJobId().getJobId();
											try{
												if(mapTSHFJ.get(sTID_JID)!=null){
													teacherStatusHistoryForJob=mapTSHFJ.get(sTID_JID);
												}
												if(teacherStatusHistoryForJob!=null)
												{
													teacherStatusHistoryForJob.setStatus("I");
													teacherStatusHistoryForJob.setHiredByDate(null);
													teacherStatusHistoryForJob.setUpdatedDateTime(new Date());
													teacherStatusHistoryForJob.setUserMaster(userMaster);
													teacherStatusHistoryForJobDAO.makePersistent(teacherStatusHistoryForJob);
												}
											}catch(Exception e){
												e.printStackTrace();
											}
											if(teacherHistoryJobs!=null){
												try{
													if(teacherHistoryJobs!=null){
														TeacherStatusHistoryForJob forJob=new TeacherStatusHistoryForJob();
														forJob.setTeacherDetail(teacherHistoryJobs.getTeacherDetail());
														forJob.setJobOrder(jobForObj.getJobId());
														if(teacherHistoryJobs.getStatusMaster()!=null)
															forJob.setStatusMaster(teacherHistoryJobs.getStatusMaster());
														if(teacherHistoryJobs.getSecondaryStatus()!=null){
															forJob.setSecondaryStatus(teacherHistoryJobs.getSecondaryStatus());
														}
														forJob.setStatus(teacherHistoryJobs.getStatus());
														forJob.setUserMaster(teacherHistoryJobs.getUserMaster());
														forJob.setCreatedDateTime(teacherHistoryJobs.getCreatedDateTime());
														teacherStatusHistoryForJobDAO.makePersistent(forJob);
													}
												}catch(Exception e){
													e.printStackTrace();
												}
											}else{
												try{
													TeacherStatusHistoryForJob forJob=new TeacherStatusHistoryForJob();
													forJob.setTeacherDetail(jobForObj.getTeacherId());
													forJob.setJobOrder(jobForObj.getJobId());
													forJob.setStatusMaster(statusMaster);
													forJob.setStatus("A");
													if(statusMaster.getStatusShortName().equals("hird")){
														try{
															forJob.setHiredByDate(Utility.getCurrentDateFormart(setHiredDate));
														}catch(Exception e){
															e.printStackTrace();
														}

													}

													forJob.setUserMaster(userMaster);
													forJob.setCreatedDateTime(new Date());
													teacherStatusHistoryForJobDAO.makePersistent(forJob);
												}catch(Exception e){
													e.printStackTrace();
												}	
											}
											if(noteObj!=null){
												try{
													TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
													teacherStatusNoteObj.setTeacherDetail(noteObj.getTeacherDetail());
													teacherStatusNoteObj.setJobOrder(jobForObj.getJobId());
													if(noteObj.getStatusMaster()!=null)
														teacherStatusNoteObj.setStatusMaster(noteObj.getStatusMaster());
													if(noteObj.getSecondaryStatus()!=null){
														teacherStatusNoteObj.setSecondaryStatus(noteObj.getSecondaryStatus());
													}
													teacherStatusNoteObj.setUserMaster(noteObj.getUserMaster());
													teacherStatusNoteObj.setDistrictId(districtMaster.getDistrictId());
													teacherStatusNoteObj.setStatusNotes(noteObj.getStatusNotes());
													teacherStatusNoteObj.setStatusNoteFileName(noteObj.getStatusNoteFileName());
													teacherStatusNoteObj.setEmailSentTo(noteObj.getEmailSentTo());
													teacherStatusNoteObj.setFinalizeStatus(noteObj.isFinalizeStatus());
													teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
													//System.out.println(teacherStatusNoteObj.getTeacherStatusNoteId()+":::::::::AfterInsert Note::::::::"+noteObj.getTeacherStatusNoteId());
												}catch(Exception ee){
													ee.printStackTrace();
												}
											}else{
												try{
													TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
													teacherStatusNoteObj.setTeacherDetail(jobForObj.getTeacherId());
													teacherStatusNoteObj.setJobOrder(jobForObj.getJobId());
													teacherStatusNoteObj.setStatusMaster(statusMaster);
													teacherStatusNoteObj.setUserMaster(userMaster);
													teacherStatusNoteObj.setDistrictId(districtMaster.getDistrictId());
													teacherStatusNoteObj.setStatusNotes("Rejected");
													teacherStatusNoteObj.setEmailSentTo(1);
													teacherStatusNoteObj.setFinalizeStatus(true);
													teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
												}catch(Exception ee){
													ee.printStackTrace();
												}
											}
											jobForObj.setOfferReady(null);
											jobForObj.setOfferAccepted(null);
											jobForObj.setRequisitionNumber(null);
											jobForObj.setStatus(statusMaster);
											jobForObj.setStatusMaster(statusMaster);
											jobForObj.setUpdatedBy(userMaster);
											jobForObj.setUpdatedByEntity(userMaster.getEntityType());		
											jobForObj.setUpdatedDate(new Date());
											jobForObj.setLastActivity("Rejected");
											jobForObj.setLastActivityDate(new Date());
											jobForObj.setUserMaster(userMaster);
											jobForTeacherDAO.makePersistent(jobForObj);
										}
										jobForTeacher.setStatus(statusMaster);
										jobForTeacher.setStatusMaster(statusMaster);
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity("Rejected");
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);
										jobForTeacherDAO.makePersistent(jobForTeacher);
										errorFlag="3";
										mailSend=true;
										statusShortName=statusMaster.getStatusShortName();
										interviewInvites(I4InterviewInvites,statusMaster,statusMasterVVI,null,null,jobOrder,teacherDetail,userMaster,request);
										lstTDetails.clear();lstTDetails.add(teacherDetail);
										statusWise(jobOrder,lstTDetails,statusMaster,null,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
										if(ccPrivilege){
											mailToTeacher(jobForTeacher,statusMaster,statusMasterCC,null,null,jobOrder,teacherDetail,userMaster,request);
										}
										//System.out.println("::::::::interviewInvites::::::::5");
									}catch(Exception e){
										e.printStackTrace();
									}
								}else{ /////////End Zone Wise Rejected/////////////////////
									//System.out.println("::::::::Rejected Else::::::::::::::::::::");
									jobForTeacher.setOfferReady(null);
									jobForTeacher.setOfferAccepted(null);
									jobForTeacher.setRequisitionNumber(null);
									jobForTeacher.setStatus(statusMaster);
									jobForTeacher.setStatusMaster(statusMaster);
									jobForTeacher.setUpdatedBy(userMaster);
									jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
									jobForTeacher.setUpdatedDate(new Date());
									jobForTeacher.setLastActivity("Rejected");
									jobForTeacher.setLastActivityDate(new Date());
									jobForTeacher.setUserMaster(userMaster);
									jobForTeacherDAO.makePersistent(jobForTeacher);
									errorFlag="3";
									mailSend=true;
									statusShortName=statusMaster.getStatusShortName();
									interviewInvites(I4InterviewInvites,statusMaster,statusMasterVVI,null,null,jobOrder,teacherDetail,userMaster,request);
									lstTDetails.clear();lstTDetails.add(teacherDetail);
									statusWise(jobOrder,lstTDetails,statusMaster,null,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
									if(ccPrivilege){
										mailToTeacher(jobForTeacher,statusMaster,statusMasterCC,null,null,jobOrder,teacherDetail,userMaster,request);
									}
									//System.out.println("::::::::interviewInvites::::::::6");
								}
							}
						}
					}else if(outerJob==false && selectedstatus && statusMaster!=null && statusMaster.getStatusShortName().equals("dcln") && (jobWiseScoreFlag || finalizeStatusFlag)){

						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){

							boolean staffer=false;
							SchoolMaster panelSchool=null;
							if(isMiami || isPhiladelphia){ //added by 21-03-2015
								try{
									JobWisePanelStatus jobWisePanelStatus =null;
									if(isPhiladelphia)//added by 21-03-2015
										jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"No Evaluation Complete");
									else							
										jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"Offer Ready");
									if(jobWisePanelStatus!=null){
										//System.out.println("jobWisePanelStatus:::"+jobWisePanelStatus.getJobPanelStatusId());
										PanelSchedule panelSchedule=panelScheduleDAO.findPanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
										if(panelSchedule!=null){
											List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
											if(panelAttendeesList.size()>0)
												for(PanelAttendees panelAttendees :panelAttendeesList){
													if(panelAttendees.getPanelInviteeId()!=null){
														UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
														if(panelUserMaster.getEntityType()==3){
															panelSchool=panelUserMaster.getSchoolId();
														}
														if(panelUserMaster.getUserId().equals(userMaster.getUserId())){
															staffer=true;
														}
													}
												}
										}
									}
								}catch(Exception e){e.printStackTrace();}
							}

							/////////////////////////////////////QQQ///////////////////////////////////////////////

							try{
								Map<Integer,List<String>> statusHistoryMap=new HashMap<Integer, List<String>>();
								Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
								Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
								List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId());
																							
								List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
								LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
								Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
								if(staffer){
									try{
										int counter=0; 
										for(SecondaryStatus tree: lstTreeStructure)
										{
											if(tree.getSecondaryStatus()==null)
											{
												if(tree.getChildren().size()>0){
													counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
												}
											}
										}
									}catch(Exception e){}

									if(statusNoteList.size()>0)
										for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
											if(isPhiladelphia){ //added by 21-03-2015
												if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("ecomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("No Evaluation Complete"))){
												teacherStatusNotesDAO.makeTransient(teacherStatusNotes);	
												}
											}
											else{//ended by 21-03-2015
											if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready"))){
											teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
											}
											}
										}
									boolean vComp=false;
									if(historyList.size()>0)
										for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
											
											if(isPhiladelphia){ //added by 21-03-2015
												if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("ecomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("No Evaluation Complete"))){
													if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp")){
														vComp=true;	
													}
													teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
												}
											}
											else{//ended by 21-03-2015
											if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready"))){
												if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp")){
													vComp=true;	
												}
												teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
											}
											}

										}

									try{		
										historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId());
										//System.out.println("historyList::::::::::"+historyList.size());

										List<String> statusList=new ArrayList<String>();
										if(historyList.size()>0)
											for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
												if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
													statusList=new ArrayList<String>();
													if(teacherStatusHistoryForJob.getStatusMaster()!=null){
														statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
													}else{
														statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
													}
													statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
												}else{
													statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
													if(teacherStatusHistoryForJob.getStatusMaster()!=null){
														statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
													}else{
														statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
													}
													statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
												}
											}
									}catch(Exception e){}
									//System.out.println("statusHistoryMap:::::::::::"+statusHistoryMap.size());
									List<String> statusIds=statusHistoryMap.get(jobForTeacherObj.getTeacherId().getTeacherId());
									String schoolStatusId=cgService.getSchoolStatusId(statusIds, statusHistMap, statusNameMap);
									SecondaryStatus secondaryStatusObj=null;
									StatusMaster statusMasterObj=null;
									
									//added by 21-03-2015
									if(isPhiladelphia){
									try{
									int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
				                    StatusMaster statusMasterMain = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
				                    //System.out.println("statusMasterMain=="+statusMasterMain.getStatus());
				                    String statusIdMapValue[]=schoolStatusId.split("#");
									//System.out.println("statusIdMapValue[]==="+statusIdMapValue);
									if(statusIdMapValue[0].equals("0") && statusIdMapValue[1].equals("0")){
											jobForTeacher.setStatusMaster(statusMasterMain);												
											jobForTeacher.setStatus(statusMasterMain);
											jobForTeacher.setSecondaryStatus(null);
									}else if(statusIdMapValue[0].equals("0")){
											secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
											jobForTeacher.setSecondaryStatus(secondaryStatusObj);												
											jobForTeacher.setStatus(statusMasterMain);
											jobForTeacher.setStatusMaster(null);
									}else if(!statusIdMapValue[0].equals("0")){
											statusMasterObj=statusMasterMap.get(schoolStatusId);
											jobForTeacher.setStatusMaster(statusMasterObj);												
											jobForTeacher.setStatus(statusMasterObj);
											jobForTeacher.setSecondaryStatus(null);																								
									}
									}catch(Exception e){e.printStackTrace();}
									}else{
									//ended by 21-03-2015
									
									try{
										String statusIdMapValue[]=schoolStatusId.split("#");
										if(statusIdMapValue[0].equals("0")){
											secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
											jobForTeacher.setSecondaryStatus(secondaryStatusObj);
											if(vComp){
												jobForTeacher.setStatus(findStatusByShortName(lstStatusMaster,"ecomp"));
												jobForTeacher.setStatusMaster(null);
											}
										}else{
											statusMasterObj=statusMasterMap.get(schoolStatusId);
										}
									}catch(Exception e){e.printStackTrace();}
									}

									try{
										if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null){
											List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobForTeacher.getJobId(),jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
											if(jobReqList.size()>0){
												for (JobRequisitionNumbers jobReqObj : jobReqList) {
													try{
														if(panelSchool!=null && jobReqObj.getSchoolMaster().getSchoolId().equals(panelSchool.getSchoolId()) && jobReqObj.getStatus()==1){
															jobReqObj.setStatus(0);
															jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
															break;
														}
													}catch(Exception e){
														e.printStackTrace();
													}
												}
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									jobForTeacher.setOfferReady(null);
									jobForTeacher.setOfferMadeDate(new Date());
									jobForTeacher.setRequisitionNumber(null);
									jobForTeacher.setOfferAcceptedDate(new Date());
									jobForTeacher.setOfferAccepted(false);
									jobForTeacherDAO.updatePersistent(jobForTeacher);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							if(entityType==3 && (isMiami || isPhiladelphia)){//added by 21-03-2015
								try{
									//System.out.println("<<<<<<<<<::::::::::::: Inside School Decline :::::::::::::::>>>>>>>>>>>");
									SchoolWiseCandidateStatus swcsObj= new SchoolWiseCandidateStatus();
									swcsObj.setStatusMaster(statusMaster);
									swcsObj.setJobOrder(jobForTeacher.getJobId());
									swcsObj.setDistrictId(userMaster.getDistrictId().getDistrictId());
									swcsObj.setSchoolId(userSchoolMaster.getSchoolId());
									swcsObj.setUserMaster(userMaster);		
									swcsObj.setCreatedDateTime(new Date());
									swcsObj.setTeacherDetail(jobForTeacher.getTeacherId());
									swcsObj.setStatus("A");
									swcsObj.setJobForTeacher(jobForTeacher);
									schoolWiseCandidateStatusDAO.makePersistent(swcsObj);
									errorFlag="3";
									mailSend=true;
									statusShortName="soth";
								}catch(Exception e){e.printStackTrace();}

							}else{
								//System.out.println("Else::::::::::::::::::::");
								jobForTeacher.setStatus(statusMaster);
								jobForTeacher.setStatusMaster(statusMaster);
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
								jobForTeacher.setUpdatedDate(new Date());
								jobForTeacher.setLastActivity("Declined");
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);
								jobForTeacherDAO.makePersistent(jobForTeacher);
								errorFlag="3";
								mailSend=true;
								statusShortName="soth";
								interviewInvites(I4InterviewInvites,statusMaster,statusMasterVVI,null,null,jobOrder,teacherDetail,userMaster,request);
								lstTDetails.clear();lstTDetails.add(teacherDetail);
								statusWise(jobOrder,lstTDetails,statusMaster,null,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
								if(ccPrivilege){
									mailToTeacher(jobForTeacher,statusMaster,statusMasterCC,null,null,jobOrder,teacherDetail,userMaster,request);
								}
								//System.out.println("::::::::interviewInvites::::::::7");
							}
						}
					}else{
						if(outerJob==false && selectedstatus ){
							if(statusMaster!=null && statusMaster.getStatusShortName().equals("scomp")&& (jobWiseScoreFlag || finalizeStatusFlag)){
								String statuss ="|hird|dcln|rem|vcomp|ecomp|";
								JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
								if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())&& !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
									if(!bPanel)
									{
										jobForTeacher.setStatus(statusMaster);
										jobForTeacher.setStatusMaster(statusMaster);
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMaster.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);
										jobForTeacherDAO.makePersistent(jobForTeacher);
										interviewInvites(I4InterviewInvites,statusMaster,statusMasterVVI,null,null,jobOrder,teacherDetail,userMaster,request);
										lstTDetails.clear();lstTDetails.add(teacherDetail);
										statusWise(jobOrder,lstTDetails,statusMaster,null,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
										if(ccPrivilege){
											mailToTeacher(jobForTeacher,statusMaster,statusMasterCC,null,null,jobOrder,teacherDetail,userMaster,request);
										}
										//System.out.println("::::::::interviewInvites::::::::8");
									}
									errorFlag="3";
									mailSend=true;
									statusShortName="soth";
								}
							}else if(statusMaster!=null && statusMaster.getStatusShortName().equals("ecomp")&& (jobWiseScoreFlag || finalizeStatusFlag)){
								String statuss = "|hird|dcln|rem|vcomp|";

								JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
								if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())&& !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
									if(!bPanel)
									{
										jobForTeacher.setStatus(statusMaster);
										jobForTeacher.setStatusMaster(statusMaster);
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMaster.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);
										jobForTeacherDAO.makePersistent(jobForTeacher);
										interviewInvites(I4InterviewInvites,statusMaster,statusMasterVVI,null,null,jobOrder,teacherDetail,userMaster,request);
										lstTDetails.clear();lstTDetails.add(teacherDetail);
										statusWise(jobOrder,lstTDetails,statusMaster,null,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
										if(ccPrivilege){
											mailToTeacher(jobForTeacher,statusMaster,statusMasterCC,null,null,jobOrder,teacherDetail,userMaster,request);
										}
										//System.out.println("::::::::interviewInvites::::::::9");
									}
									errorFlag="3";
									mailSend=true;
									statusShortName="soth";
								}
							}else if(statusMaster!=null && statusMaster.getStatusShortName().equals("vcomp")&& (jobWiseScoreFlag || finalizeStatusFlag)){
								//System.out.println("::::::::::::::::::::::VCOM::::::::::::::::::::::::");
								JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
								String statuss = "|hird|dcln|rem|";
								if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId()) && !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
									if(!bPanel)
									{
										jobForTeacher.setStatus(statusMaster);
										jobForTeacher.setStatusMaster(statusMaster);
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMaster.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);

										jobForTeacherDAO.makePersistent(jobForTeacher);
										//System.out.println("vcomp 01");
										interviewInvites(I4InterviewInvites,statusMaster,statusMasterVVI,null,null,jobOrder,teacherDetail,userMaster,request);
										lstTDetails.clear();lstTDetails.add(teacherDetail);
										statusWise(jobOrder,lstTDetails,statusMaster,null,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
										if(ccPrivilege){
											mailToTeacher(jobForTeacher,statusMaster,statusMasterCC,null,null,jobOrder,teacherDetail,userMaster,request);
										}
										//System.out.println("::::::::interviewInvites::::::::10");
									}
									errorFlag="3";
									mailSend=true;
									statusShortName="soth";
								}
							}
						}
					}
				if(outerJob==false && isMiami && statusMasterPanel!=null && statusMasterPanel.getStatusShortName().equals("vcomp")&& (jobWiseScoreFlag || finalizeStatusFlag)){ 
					//System.out.println("::::::::::::::::::::::VCOM 3rdndddddd::::::::::::::::::::::::");
					JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
					UserMaster schoolUser=null;
					try{
						List<JobWisePanelStatus> jobWiseList=new ArrayList<JobWisePanelStatus>();
						SecondaryStatus  secondaryStatusObj =secondaryStatusDAO.findSecondaryStatusByJobOrder(jobForTeacher.getJobId(),"Offer Ready");
						jobWiseList=jobWisePanelStatusDAO.getPanel(jobOrder,secondaryStatusObj,null);

						//System.out.println("jobWiseList:::::>>>>>>>>>>>>>>>>>:"+jobWiseList.size());
						List<PanelSchedule> panelSList= new ArrayList<PanelSchedule>();
						if(jobWiseList.size() == 1){
							panelSList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWiseList,teacherDetail);
							if(panelSList.size()==1){
								PanelSchedule panelSchedule=null;
								if(panelSList.get(0)!=null){
									panelSchedule=panelSList.get(0);
									List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
									panelAttendees=panelAttendeesDAO.getPanelAttendees(panelSchedule);
									if(panelAttendees.size() > 0){
										for(PanelAttendees attendees:panelAttendees){
											if(attendees!=null){
												if(attendees.getPanelInviteeId().getEntityType()==3){
													schoolUser=attendees.getPanelInviteeId();
												}
											}
										}
									}
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}								int offerAccepted=1;
					//System.out.println("userMaster:::"+userMaster.getUserId());

					try{
						if(jobForTeacher!=null){
							if(jobForTeacher.getOfferAccepted()!=null){
								if(!jobForTeacher.getOfferAccepted()){
									offerAccepted=0;
								}
							}
						}
					}catch(Exception e){}
					//System.out.println("offerAccepted:::"+offerAccepted);

					int offerReady=2;
					try{
						if(jobForTeacher.getOfferReady()!=null){
							if(jobForTeacher.getOfferReady()){
								offerReady=1;
							}else{
								offerReady=0;
							}
						}
					}catch(Exception e){}

					try{
						String statuss = "|hird|dcln|rem|";
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMasterPanel.getStatusId()) && !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
							if(!bPanel)
							{
								jobForTeacher.setStatus(statusMasterPanel);
								jobForTeacher.setStatusMaster(statusMasterPanel);
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
								jobForTeacher.setUpdatedDate(new Date());
								jobForTeacher.setLastActivity(statusMaster.getStatus());
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);

								jobForTeacherDAO.makePersistent(jobForTeacher);
								interviewInvites(I4InterviewInvites,statusMaster,statusMasterVVI,null,null,jobOrder,teacherDetail,userMaster,request);
								lstTDetails.clear();lstTDetails.add(teacherDetail);
								statusWise(jobOrder,lstTDetails,statusMaster,null,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
								if(ccPrivilege){
									mailToTeacher(jobForTeacher,statusMaster,statusMasterCC,null,null,jobOrder,teacherDetail,userMaster,request);
								}
								//System.out.println("::::::::interviewInvites::::::::11");
							}
						}
					}catch(Exception e){

					}
					if(offerReady==1){
						try{
							List<UserMailSend> userMailSendListFotTeacher =new ArrayList<UserMailSend>();
							UserMailSend userMailSendForTeacher= new UserMailSend();
							userMailSendForTeacher.setEmail(teacherDetail.getEmailAddress());
							userMailSendForTeacher.setTeacherDetail(teacherDetail);

							String bccAndSchoolUser[]=new String[1];
							try{
								if(schoolUser!=null){
									bccAndSchoolUser[0]=schoolUser.getEmailAddress();
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							boolean isSubstituteInstructionalJob = false;
							try{
								if(jobForTeacher!=null && jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Substitute Instructional Positions"))
									isSubstituteInstructionalJob=true;
							}catch(Exception e){}
							try{
								if(isSubstituteInstructionalJob){
									if(userMaster!=null && userMaster.getDistrictId()!=null){
										bccAndSchoolUser[0]=userMaster.getEmailAddress();
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							userMailSendForTeacher.setBccEmail(bccAndSchoolUser);
							userMailSendForTeacher.setSubject("Offer Accepted");
							userMailSendForTeacher.setIsUserOrTeacherFlag(6);
							userMailSendForTeacher.setPanelExist("1");
							try{
								if(jobForTeacher.getIsAffilated()!=null){
									userMailSendForTeacher.setInternal(jobForTeacher.getIsAffilated());
								}
							}catch(Exception e){}
							userMailSendForTeacher.setSchoolLocation(schoolLocation);
							userMailSendForTeacher.setLocationCode(locationCode);// Set school location code

							userMailSendForTeacher.setJobTitle(jobOrder.getJobTitle());
							userMailSendForTeacher.setUserMaster(userMaster);
							userMailSendListFotTeacher.add(userMailSendForTeacher);

							Integer teacherId1 = teacherDetail.getTeacherId();
							TeacherPersonalInfo teacherPersonalInfos = teacherPersonalInfoDAO.findById(teacherId1, false, false);							
							TeacherDetail teacherPersonalInfoTemp = teacherDetail;							
							teacherPersonalInfoTemp.setFgtPwdDateTime(teacherPersonalInfos.getDob());//Dob		

							String teacherAddress="";
							try{
								if(teacherPersonalInfos.getAddressLine1()!=null){
									teacherAddress=teacherPersonalInfos.getAddressLine1().trim();
								}
								if(teacherPersonalInfos.getAddressLine2()!=null && !teacherPersonalInfos.getAddressLine2().equals("")){
									teacherAddress+=" "+teacherPersonalInfos.getAddressLine2().trim();
								}
								if(teacherPersonalInfos.getCityId()!=null){
									teacherAddress+=", "+teacherPersonalInfos.getCityId().getCityName();
								}
								if(teacherPersonalInfos.getStateId()!=null){
									teacherAddress+=", "+teacherPersonalInfos.getStateId().getStateName();
								}
								if(teacherPersonalInfos.getCountryId()!=null){
									teacherAddress+=", "+teacherPersonalInfos.getCountryId().getName();
								}
								if(teacherPersonalInfos.getZipCode()!=null){
									teacherAddress+=", "+teacherPersonalInfos.getZipCode();
								}
							}catch(Exception e){}
							//System.out.println("teacherAddress::::::::::>>>>"+teacherAddress);
							teacherPersonalInfoTemp.setAuthenticationCode(teacherAddress);//Address
							teacherPersonalInfoTemp.setPhoneNumber(teacherPersonalInfos.getPhoneNumber());//Phone

							String emailAddresses="";

							List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findOfferAccepted(districtMaster);
							if(districtKeyContactList!=null){
								//System.out.println("districtKeyContactList::::::::::::::::"+districtKeyContactList.size());
								int count=0;
								for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
									if(count==1){
										emailAddresses+=",";
										count=0;
									}
									emailAddresses+=districtKeyContact.getKeyContactEmailAddress();
									count++;
								}
								//System.out.println("emailAddresses:::::::->>>>>"+emailAddresses);
								UserMailSend userMailSendForTeacher3= new UserMailSend();
								userMailSendForTeacher3.setEmail(emailAddresses);
								userMailSendForTeacher3.setTeacherDetail(teacherPersonalInfoTemp);
								userMailSendForTeacher3.setSubject(Utility.getLocaleValuePropByKey("msgNotificationOfferAccepted", locale));
								userMailSendForTeacher3.setIsUserOrTeacherFlag(7);
								userMailSendForTeacher.setPanelExist("1");
								userMailSendForTeacher3.setSchoolLocation(schoolLocation);
								userMailSendForTeacher3.setLocationCode(locationCode); //Set school  location code
								userMailSendForTeacher3.setJobTitle(jobOrder.getJobTitle());
								userMailSendListFotTeacher.add(userMailSendForTeacher3);
							}
							mailSendToUserByThread(userMailSendListFotTeacher,null,null);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				//System.out.println("RJ>>>>>>>>>>>>>>>"+outerJob+" "+selectedstatus+" "+isUpdateStatus+"  "+statusUpdateHDR+" "+errorFlag);
				if(outerJob==false && selectedstatus && isUpdateStatus && isUpdateStatus && statusUpdateHDR==null && !errorFlag.equals("2") && !errorFlag.equals("4"))
					if(statusMaster!=null && (statusMaster.getStatusShortName().equals("hird")|| statusMaster.getStatusShortName().equals("rem")||statusMaster.getStatusShortName().equals("dcln"))){
						//System.out.println("RJ>>>>>>>>>>>>>>>1");
						if((jobWiseScoreFlag || finalizeStatusFlag) && lstTeacherStatusHistoryForJob.size()==0){
							JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
							boolean staffer=false;
							SchoolMaster panelSchool=null;
							if(isMiami || isPhiladelphia){//added by 21-03-2015
								try{
									JobWisePanelStatus jobWisePanelStatus =null;
									if(isPhiladelphia)//added by 21-03-2015
										jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"No Evaluation Complete");
									else
										jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"Offer Ready");
									if(jobWisePanelStatus!=null){
										//System.out.println("jobWisePanelStatus:::"+jobWisePanelStatus.getJobPanelStatusId());
										PanelSchedule panelSchedule=panelScheduleDAO.findPanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
										if(panelSchedule!=null){
											List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
											if(panelAttendeesList.size()>0)
												for(PanelAttendees panelAttendees :panelAttendeesList){
													if(panelAttendees.getPanelInviteeId()!=null){
														UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
														if(panelUserMaster.getEntityType()==3){
															panelSchool=panelUserMaster.getSchoolId();
														}
														if(panelUserMaster.getUserId().equals(userMaster.getUserId())){
															staffer=true;
														}
													}
												}
										}
									}
								}catch(Exception e){e.printStackTrace();}
							}
							if(staffer && !statusMaster.getStatusShortName().equals("hird")){
								try{
									JobWisePanelStatus jobWisePanelStatus =null;
									if(isPhiladelphia) //added by 21-03-2015
									jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"No Evaluation Complete");
									else
									jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"Offer Ready");
									if(jobWisePanelStatus!=null){
										panelScheduleDAO.updatePanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
									}
								}catch(Exception e){e.printStackTrace();}
							}
							//System.out.println("isMiami::;"+isMiami+" isPhiladelphia :::"+isPhiladelphia +" staffer :::"+staffer+" "+statusMaster.getStatusShortName());
							if(((isMiami || isPhiladelphia) && staffer==false && entityType==2) ||( (isMiami || isPhiladelphia) && staffer && (statusMaster.getStatusShortName().equals("hird")|| (statusMaster.getStatusShortName().equals("dcln")&& entityType==2) ||(statusMaster.getStatusShortName().equals("rem")&& entityType==2))) || (isMiami==false && isPhiladelphia==false)){//added by 21-03-2015
								
								//System.out.println(" :::::::::::::::::::::: Status Update :::::::::::::::::::::");
								try{
									Calendar c = Calendar.getInstance();
									c.add(Calendar.SECOND,01);
									Date date = c.getTime();
									TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
									tSHJ.setTeacherDetail(teacherDetail);
									tSHJ.setJobOrder(jobOrder);
									if(statusId!=null && !statusId.equals("0")){
										tSHJ.setStatusMaster(statusMaster);
									}
									if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
										tSHJ.setSecondaryStatus(secondaryStatus);
										SendEmailOfficialTranscripts(secondaryStatus,teacherDetail,jobOrder);
									}
									tSHJ.setStatus("A");

									try{
										if(statusMaster.getStatusShortName().equals("hird")){
											tSHJ.setHiredByDate(Utility.getCurrentDateFormart(setHiredDate));
										}
									} catch(Exception e){

									}

									tSHJ.setCreatedDateTime(date);
									tSHJ.setUserMaster(userMaster);
									teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
									canNotHireObj=tSHJ;
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}else if((jobWiseScoreFlag || finalizeStatusFlag) && jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId()==1){
							//System.out.println("::::::::::::::::::::Status Else RJ::::::::::::::::");
								try{
									Calendar c = Calendar.getInstance();
									c.add(Calendar.SECOND,01);
									Date date = c.getTime();
									TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
									tSHJ.setTeacherDetail(teacherDetail);
									tSHJ.setJobOrder(jobOrder);
									if(statusId!=null && !statusId.equals("0")){
										tSHJ.setStatusMaster(statusMaster);
									}
									if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
										tSHJ.setSecondaryStatus(secondaryStatus);
										SendEmailOfficialTranscripts(secondaryStatus,teacherDetail,jobOrder);
									}
									tSHJ.setStatus("A");

									try{
										if(statusMaster.getStatusShortName().equals("hird")){
											tSHJ.setHiredByDate(Utility.getCurrentDateFormart(setHiredDate));
										}
									} catch(Exception e){

									}

									tSHJ.setCreatedDateTime(date);
									tSHJ.setUserMaster(userMaster);
									teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
									canNotHireObj=tSHJ;
								}catch(Exception e){
									e.printStackTrace();
								}
						}
					}
				// Update 
				int offerReadyMailFlag=0;
				if(outerJob==false && bPanel && !finalizeStatus.equals("5")){
					JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
					boolean isPanelMember=false;
					boolean panelMailSend=false;
					//System.out.println("panelMemberMap::"+panelMemberMap.size());
					try{
						if(panelMemberMap.get(userMaster.getUserId())!=null){
							isPanelMember=true;
						}
					}catch(Exception e){}


					try{
						if(jobForTeacher.getOfferReady()!=null){
							if(!jobForTeacher.getOfferReady()){
								panelMailSend=true;
							}
						}else{
							panelMailSend=true;
						}
					}catch(Exception e){}
					//System.out.println("panelMailSend:::"+panelMailSend);
					//Is Already Override
					TeacherStatusHistoryForJob teacherStatusHistoryForJob= teacherStatusHistoryForJobDAO.getOverride(teacherDetail, jobOrder,statusMasterPanel,secondaryStatusPanel);
					boolean isAlreadyOverride=false;
					if(teacherStatusHistoryForJob!=null && teacherStatusHistoryForJob.getOverride().equals(new Boolean(true)))
					{
						isAlreadyOverride=true;
						if(teacherStatusHistoryForJob.getStatusMaster()!=null)
							statusMasterPanel=teacherStatusHistoryForJob.getStatusMaster();
						else if(teacherStatusHistoryForJob.getSecondaryStatus()!=null)
							secondaryStatusPanel=teacherStatusHistoryForJob.getSecondaryStatus();

					}
					String sStatusName="";
					if(statusMasterPanel!=null)
						sStatusName=statusMasterPanel.getStatus();
					else if(secondaryStatusPanel!=null){
						sStatusName=secondaryStatusPanel.getSecondaryStatusName();
					}
					/////////////////////////////////////
					int offerReady=2;
					try{
						if(jobForTeacher.getOfferReady()!=null){
							if(jobForTeacher.getOfferReady()){
								offerReady=1;
							}else{
								offerReady=0;
							}
						}
					}catch(Exception e){}

					SchoolMaster panelSchool=null;
					try{
						JobWisePanelStatus jobWisePanelStatus =null;
						if(isPhiladelphia)//added by 21-03-2015
							jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"No Evaluation Complete");
						else							
							jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"Offer Ready");
						if(jobWisePanelStatus!=null){
							//System.out.println("jobWisePanelStatus:::"+jobWisePanelStatus.getJobPanelStatusId());
							PanelSchedule panelSchedule=panelScheduleDAO.findPanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
							if(panelSchedule!=null){
								List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
								if(panelAttendeesList.size()>0)
									for(PanelAttendees panelAttendees :panelAttendeesList){
										if(panelAttendees.getPanelInviteeId()!=null){
											UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
											if(panelUserMaster.getEntityType()==3){
												panelSchool=panelUserMaster.getSchoolId();
											}
										}
									}
							}
						}
					}catch(Exception e){e.printStackTrace();}

					boolean positonNumberFlag=false;
					String requisitionNumber=null;
					try{
							JobRequisitionNumbers jRNsObj=null;
							if(!isPhiladelphia)//added by 21-03-2015
							jRNsObj=jobRequisitionNumbersDAO.findById(Integer.parseInt(requisitionNumberId),false,false);
							if(jRNsObj!=null){
							if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null && jobForTeacher.getRequisitionNumber().equals(jRNsObj.getDistrictRequisitionNumbers().getRequisitionNumber()) && offerReady==0 && entityType==2){
								requisitionNumber=jRNsObj.getDistrictRequisitionNumbers().getRequisitionNumber();
								positonNumberFlag=true;
							}
						}
						//System.out.println("positonNumberFlag:::::::::"+positonNumberFlag);
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(positonNumberFlag==false && jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null && offerReady==0 && entityType==2){
							List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobForTeacher.getJobId(),jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
							if(jobReqList.size()>0){
								for (JobRequisitionNumbers jobReqObj : jobReqList) {
									try{
										if(panelSchool!=null && jobReqObj.getSchoolMaster().getSchoolId().equals(panelSchool.getSchoolId()) && jobReqObj.getStatus()==1){
											jobReqObj.setStatus(0);
											jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
											break;
										}
									}catch(Exception e){
										e.printStackTrace();
									}
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(isMiami && positonNumberFlag==false && (isPanelMember)){
							
							try{
								if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
									JobRequisitionNumbers jRNOb=jobRequisitionNumbersDAO.findById(Integer.parseInt(requisitionNumberId),false,false);
									if(jRNOb!=null){
										requisitionNumber=jRNOb.getDistrictRequisitionNumbers().getRequisitionNumber();
										List<JobRequisitionNumbers> jobReqNumbers=jobRequisitionNumbersDAO.findJobReqNumbers(jRNOb.getDistrictRequisitionNumbers());
										if(jobReqNumbers.size()==1){
											boolean multiHired=false;
											try{
												if(jRNOb.getJobOrder().getIsExpHireNotEqualToReqNo()){
													multiHired=true;
												}	
											}catch(Exception e){
												e.printStackTrace();
											}
											if(multiHired==false){
												jRNOb.setStatus(1);
												jobRequisitionNumbersDAO.updatePersistent(jRNOb);
											}
										}
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					//	//System.out.println("requisitionNumber:::><::::::::::::"+requisitionNumber +" secon==="+secondaryStatusForPanel+" sta=="+statusMaster);
						if((requisitionNumber!=null || isPhiladelphia) && isPanelMember && offerReady==2 && entityType==3){//added by 21-03-2015 (isPhiladelphia)
							if(secondaryStatusForPanel!=null && !secondaryStatusForPanel.getSecondaryStatusName().equals("") && (secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready") || secondaryStatusForPanel.getSecondaryStatusName().equals("No Evaluation Complete"))){
								offerReadyMailFlag=1;
								jobForTeacher.setRequisitionNumber(requisitionNumber);
								jobForTeacher.setOfferReady(false);
								jobForTeacher.setOfferMadeDate(new Date());
								try {
									jobForTeacherDAO.updatePersistent(jobForTeacher);
								} catch (Exception e) {
									e.printStackTrace();
								}	
							}
						}else if((isMiami || isPhiladelphia) && isPanelMember && offerReady==0 && entityType==2 && finalizeStatus!=null && finalizeStatus.equals("1")){ //added by 21-03-2015
							if(secondaryStatusForPanel!=null && !secondaryStatusForPanel.getSecondaryStatusName().equals("") && (secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready")||secondaryStatusForPanel.getSecondaryStatusName().equals("No Evaluation Complete") )){//added by 21-03-2015
								offerReadyMailFlag=2;
								jobForTeacher.setOfferReady(true);
								jobForTeacher.setOfferMadeDate(new Date());
								if(requisitionNumber!=null){
									jobForTeacher.setRequisitionNumber(requisitionNumber);
								}
								try {
									jobForTeacherDAO.updatePersistent(jobForTeacher);
								} catch (Exception e) {
									e.printStackTrace();
								}	
							}
						}else if(isMiami  && offerReady==2 && entityType==2){ /// Special case Maimi (Substitute position)
							if(secondaryStatusForPanel!=null && !secondaryStatusForPanel.getSecondaryStatusName().equals("") && secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready")){ 
								offerReadyMailFlag=2;
								jobForTeacher.setOfferReady(false);
								jobForTeacher.setOfferMadeDate(new Date());
								if(requisitionNumber!=null){
									jobForTeacher.setRequisitionNumber(requisitionNumber);
								}
								try {
									jobForTeacherDAO.updatePersistent(jobForTeacher);
								} catch (Exception e) {
									e.printStackTrace();
								}	
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}

					int offerReadyForInProgress=0;
					try{
						if(statusNoteList.size()>0){
							for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
								try{
									if((teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready")) && teacherStatusNotes.isFinalizeStatus() && finalizeStatus!=null && finalizeStatus.equals("1") && secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready")){
										offerReadyForInProgress++;
									}
									//added by 21-03-2015
									if((teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("No Evaluation Complete")) && teacherStatusNotes.isFinalizeStatus() && finalizeStatus!=null && finalizeStatus.equals("1") && secondaryStatusForPanel.getSecondaryStatusName().equals("No Evaluation Complete")){
										offerReadyForInProgress++;
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					//System.out.println("offerReadyForInProgress=="+offerReadyForInProgress+" offerReady=="+offerReady);
					if(offerReadyForInProgress==1 && offerReady==0){
						offerReadyMailFlag=1;
					}
					//System.out.println("offerReadyForInProgress:::::::"+offerReadyForInProgress);

					//System.out.println("::::::::::::::::offerReadyMailFlag:::::::::::::::::::::::"+offerReadyMailFlag);
					/////////////////////////////////////////////
					List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobs=new ArrayList<TeacherStatusHistoryForJob>();
					if(userMasters_PanelAttendees.size() > 0)
					{
						teacherStatusHistoryForJobs=teacherStatusHistoryForJobDAO.findByTID_JID_Status(teacherDetail, jobOrder, statusMaster, secondaryStatus,userMasters_PanelAttendees);
					}
					if( ( iPanelAttendeeCount > 0 && iPanelAttendeeCount==teacherStatusHistoryForJobs.size() ) || isAlreadyOverride)
					{

						if(jobForTeacher!=null)
						{
							if(secondaryStatusPanel!=null)
							{
								jobForTeacher.setSecondaryStatus(secondaryStatusPanel);
								jobForTeacher.setStatusMaster(null);

							}

							if(statusMasterPanel!=null)
							{
								jobForTeacher.setStatus(statusMasterPanel);
								jobForTeacher.setStatusMaster(statusMasterPanel);
							}

							jobForTeacher.setUpdatedBy(userMaster);
							jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
							jobForTeacher.setUpdatedDate(new Date());
							if(sStatusName!=null && !sStatusName.equalsIgnoreCase(""))
								jobForTeacher.setLastActivity(sStatusName);
							jobForTeacher.setLastActivityDate(new Date());
							jobForTeacher.setUserMaster(userMaster);
							try {
								jobForTeacherDAO.updatePersistent(jobForTeacher);
							} catch (Exception e) {
								System.out.println("00.00 Error in jobForTeacherDAO");
								e.printStackTrace();
							}	
						}

						//System.out.println("4.06 Update jobForTeacherId "+jobForTeacherId);
					}
					else
					{
						System.out.println("4.06.01 Not Update jobForTeacherId "+jobForTeacherId);
					}

					
					//Start ... Email for panel Member
					if(finalizeStatus!=null && finalizeStatus.equals("1"))
					{
						///////////////////////Start Send To Teacher///////////////////////
						//System.out.println("=========================================offerReadyMailFlag:: "+offerReadyMailFlag);

						if((offerReadyMailFlag==2 && (isPanelMember) && (isMiami || isPhiladelphia) && panelMailSend)){//added 21-03-2015

							try{
								if(isPhiladelphia){ //added 21-03-2015
									//System.out.println("===================Offer Letter send to teacher ========================entityType========="+entityType);
									String statusIds="";
									try{
										if(statusMaster_temp!=null){
											statusIds="s##"+statusMaster_temp.getStatusId();
										}else{
											statusIds="ss##"+secondaryStatus_temp.getSecondaryStatusId();
										}
									}catch(Exception e){}
									//System.out.println("statusIds==Offer Letter send to teacher==="+statusIds);
									philadelphiaMail(jobOrder, teacherDetail, statusMaster, secondaryStatus,userMaster,entityType,statusIds);
								}
								else{
								List<UserMailSend> userMailSendListFotTeacher =new ArrayList<UserMailSend>();
								UserMailSend userMailSendForTeacher= new UserMailSend();
								userMailSendForTeacher.setEmail(teacherDetail.getEmailAddress());
								userMailSendForTeacher.setTeacherDetail(teacherDetail);

								userMailSendForTeacher.setSubject(Utility.getLocaleValuePropByKey("msgOfferLetter", locale));
								userMailSendForTeacher.setIsUserOrTeacherFlag(5);
								userMailSendForTeacher.setPanelExist("1");
								String statusIds="";
								try{
									if(statusMaster_temp!=null){
										statusIds="s##"+statusMaster_temp.getStatusId();
									}else{
										statusIds="ss##"+secondaryStatus_temp.getSecondaryStatusId();
									}
								}catch(Exception e){}

								int userId=0;
								try{
									if(userMaster!=null){
										userId=userMaster.getUserId();
									}
								}catch(Exception e){}

								String decline = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+"0##"+statusIds+"##"+userId);
								String accept = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+"1##"+statusIds+"##"+userId);
								userMailSendForTeacher.setOfferAcceptURL(Utility.getValueOfPropByKey("basePath")+"/verifyoffer.do?key="+accept);
								userMailSendForTeacher.setOfferDeclineURL(Utility.getValueOfPropByKey("basePath")+"/verifyoffer.do?key="+decline);

								userMailSendForTeacher.setSchoolLocation(schoolLocation);
								userMailSendForTeacher.setLocationCode(locationCode); // set school location code

								userMailSendForTeacher.setJobTitle(jobOrder.getJobTitle());
								userMailSendForTeacher.setUserMaster(userMaster);
								userMailSendForTeacher.setJobOrder(jobOrder);
								userMailSendListFotTeacher.add(userMailSendForTeacher);
								System.out.println("===================Offer Letter send to teacher =================================");
								mailSendToUserByThread(userMailSendListFotTeacher,null,null);
								//System.out.println("===================Offer Letter mailed =================================");
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
						//////////////////End Send To Teacher////////////////////


						if(offerReadyMailFlag==1){ // || offerReadyMailFlag==0 
							//System.out.println("===================Offer Letter send to District nnnnnnnnnnnn ========================entityType========="+entityType);
							if(isPhiladelphia){ //added by 21-2015
								//System.out.println("===================Offer Letter send to District nnnnnnnnnnnn ========================entityType========="+entityType);
								String statusIds="";
								try{
									if(statusMaster_temp!=null){
										statusIds="s##"+statusMaster_temp.getStatusId();
									}else{
										statusIds="ss##"+secondaryStatus_temp.getSecondaryStatusId();
									}
								}catch(Exception e){}
								//System.out.println("statusIds==Offer Letter send to District==="+statusIds);
								philadelphiaMail(jobOrder, teacherDetail, statusMaster, secondaryStatus,userMaster,entityType,statusIds);
							}
							else{
							List<UserMailSend> userMailSendListForPanelMember =new ArrayList<UserMailSend>();
							for(UserMaster userMaster2 : panelAttendees_ForEmail)
							{
								String sPanelUserName="";
								if(userMaster.getLastName()!=null)
									sPanelUserName=userMaster.getFirstName()+" "+userMaster.getLastName();	 
								else
									sPanelUserName=userMaster.getFirstName();

								String sPanelUserNameTo="";
								if(userMaster2.getLastName()!=null)
									sPanelUserNameTo=userMaster2.getFirstName()+" "+userMaster2.getLastName();	 
								else
									sPanelUserNameTo=userMaster2.getFirstName();
								String sTeacheName="";
								if(teacherDetail.getLastName()!=null)
									sTeacheName=teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
								else
									sTeacheName=teacherDetail.getFirstName();

								UserMailSend userMailSendForPanelMember= new UserMailSend();
								try{
									if(jobForTeacher.getRequisitionNumber()!=null){
										userMailSendForPanelMember.setRequisitionNumber(jobForTeacher.getRequisitionNumber());
									}
								}catch(Exception e){}

								try{
									if(userMaster.getEntityType()!=null && userMaster.getEntityType()==3){
										userMailSendForPanelMember.setSchoolLocation(userMaster.getSchoolId().getSchoolName());
										userMailSendForPanelMember.setLocationCode(locationCode); //Set school location code
									}else if(schoolLocation!=null){
										userMailSendForPanelMember.setSchoolLocation(schoolLocation);
										userMailSendForPanelMember.setLocationCode(locationCode);
									}
								}catch(Exception e){}
								userMailSendForPanelMember.setSubject(Utility.getLocaleValuePropByKey("msgOfferReadyNotice", locale));
								userMailSendForPanelMember.setEmail(userMaster2.getEmailAddress());
								userMailSendForPanelMember.setFromUserName(sPanelUserName);
								userMailSendForPanelMember.setToUserName(sPanelUserNameTo);
								userMailSendForPanelMember.setTeacherName(sTeacheName);
								userMailSendForPanelMember.setJobTitle(jobOrder.getJobTitle());
								userMailSendForPanelMember.setStatusName(sStatusName);
								userMailSendForPanelMember.setUserMaster(userMaster);
								userMailSendForPanelMember.setIsUserOrTeacherFlag(offerReadyMailFlag);
								userMailSendForPanelMember.setPanelExist("1");
								userMailSendListForPanelMember.add(userMailSendForPanelMember);
							}
							mailSendToPanelUserByThread(userMailSendListForPanelMember);
							}//added by 21-2015 one line
						}
					}
					//End
				}else if(!finalizeStatus.equals("5")){ //Start Without Panel
					////System.out.println("::::::::::::::Without Panel:::::::::");
					JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);

					TeacherStatusHistoryForJob teacherStatusHistoryForJob= teacherStatusHistoryForJobDAO.getOverride(teacherDetail, jobOrder,statusMasterPanel,secondaryStatusPanel);
					if(teacherStatusHistoryForJob!=null && teacherStatusHistoryForJob.getOverride().equals(new Boolean(true)))
					{
						if(teacherStatusHistoryForJob.getStatusMaster()!=null)
							statusMasterPanel=teacherStatusHistoryForJob.getStatusMaster();
						else if(teacherStatusHistoryForJob.getSecondaryStatus()!=null)
							secondaryStatusPanel=teacherStatusHistoryForJob.getSecondaryStatus();

					}
					String sStatusName="";
					if(statusMasterPanel!=null)
						sStatusName=statusMasterPanel.getStatus();
					else if(secondaryStatusPanel!=null){
						sStatusName=secondaryStatusPanel.getSecondaryStatusName();
					}
					//System.out.println("::::sStatusName:::: "+sStatusName);
					if(sStatusName!=null && sStatusName.equals("Offer Ready")){
						String requisitionNumber=null;
						try{
							if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
								JobRequisitionNumbers jRNOb=jobRequisitionNumbersDAO.findById(Integer.parseInt(requisitionNumberId),false,false);
								if(jRNOb!=null){
									requisitionNumber=jRNOb.getDistrictRequisitionNumbers().getRequisitionNumber();
									List<JobRequisitionNumbers> jobReqNumbers=jobRequisitionNumbersDAO.findJobReqNumbers(jRNOb.getDistrictRequisitionNumbers());
									if(jobReqNumbers.size()==1){
										boolean multiHired=false;
										try{
											if(jRNOb.getJobOrder().getIsExpHireNotEqualToReqNo()){
												multiHired=true;
											}	
										}catch(Exception e){
											e.printStackTrace();
										}
										if(multiHired==false){
											jRNOb.setStatus(1);
											jobRequisitionNumbersDAO.updatePersistent(jRNOb);
										}
									}
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						try{	
							//System.out.println("requisitionNumber:::::::::::::::"+requisitionNumber);
							if(finalizeStatus!=null && finalizeStatus.equals("1")){
								if(secondaryStatusForPanel!=null && !secondaryStatusForPanel.getSecondaryStatusName().equals("") && secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready")){
									jobForTeacher.setOfferReady(true);
									jobForTeacher.setOfferMadeDate(new Date());
									if(requisitionNumber!=null){
										jobForTeacher.setRequisitionNumber(requisitionNumber);
									}
									try {
										jobForTeacherDAO.updatePersistent(jobForTeacher);
									} catch (Exception e) {
										e.printStackTrace();
									}	
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						try{
							//System.out.println(" jobForTeacher >>>>>>> "+jobForTeacher);
							if(jobForTeacher!=null){
								int statusFlag=0;
								if(secondaryStatusPanel!=null && jobForTeacher.getSecondaryStatus()!=null && !secondaryStatusPanel.getSecondaryStatusName().equalsIgnoreCase(jobForTeacher.getSecondaryStatus().getSecondaryStatusName())){
									jobForTeacher.setSecondaryStatus(secondaryStatusPanel);
									if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
										jobForTeacher.setStatusMaster(null);
									}else{
										jobForTeacher.setStatusMaster(statusMaster);
										interviewInvites(I4InterviewInvites,statusMaster,statusMasterVVI,null,null,jobOrder,teacherDetail,userMaster,request);
										lstTDetails.clear();lstTDetails.add(teacherDetail);
										statusWise(jobOrder,lstTDetails,statusMaster,null,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
										statusFlag=1;
										//System.out.println("::::::::interviewInvites::::::::15");
									}
								}
								//System.out.println(" statusMasterPanel ::::: >>>>> "+statusMasterPanel);
								if(statusMasterPanel!=null){
									jobForTeacher.setStatus(statusMasterPanel);
									jobForTeacher.setStatusMaster(statusMasterPanel);
									//System.out.println(" >>>> interview method call >>>>");
									interviewInvites(I4InterviewInvites,statusMasterPanel,statusMasterVVI,null,null,jobOrder,teacherDetail,userMaster,request);
									lstTDetails.clear();lstTDetails.add(teacherDetail);
									statusWise(jobOrder,lstTDetails,statusMasterPanel,null,statusMasterSecondaryName,userMaster,jobForTeacherId,schoolIdInputFromUser);//added by ram nath
									statusFlag=2;
									//System.out.println("::::::::interviewInvites::::::::16");
								}
								//System.out.println("::::::::interviewInvites::::::::Both");
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
								jobForTeacher.setUpdatedDate(new Date());
								if(sStatusName!=null && !sStatusName.equalsIgnoreCase(""))
									jobForTeacher.setLastActivity(sStatusName);
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);
								try {
									jobForTeacherDAO.updatePersistent(jobForTeacher);
								} catch (Exception e) {
									System.out.println("Error in jobForTeacherDAO");
									e.printStackTrace();
								}	
								if(statusFlag==1){
									if(ccPrivilege){
										mailToTeacher(jobForTeacher,statusMaster,statusMasterCC,null,null,jobOrder,teacherDetail,userMaster,request);
									}
								}else if(statusFlag==2){
									if(ccPrivilege){
										mailToTeacher(jobForTeacher,statusMaster,statusMasterCC,null,null,jobOrder,teacherDetail,userMaster,request);
									}
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						if(finalizeStatus!=null && finalizeStatus.equals("1")){
							try{
								List<UserMailSend> userMailSendListFotTeacher =new ArrayList<UserMailSend>();
								UserMailSend userMailSendForTeacher= new UserMailSend();
								userMailSendForTeacher.setEmail(teacherDetail.getEmailAddress());
								userMailSendForTeacher.setTeacherDetail(teacherDetail);
								userMailSendForTeacher.setSubject(Utility.getLocaleValuePropByKey("msgSubstituteOfferLetter", locale));
								userMailSendForTeacher.setIsUserOrTeacherFlag(5);
								userMailSendForTeacher.setPanelExist("0");
								String statusIds="";
								try{
									if(statusMaster_temp!=null){
										statusIds="s##"+statusMaster_temp.getStatusId();
									}else{
										statusIds="ss##"+secondaryStatus_temp.getSecondaryStatusId();
									}
								}catch(Exception e){}

								int userId=0;
								try{
									if(userMaster!=null){
										userId=userMaster.getUserId();
									}
								}catch(Exception e){}

								String decline = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+"0##"+statusIds+"##"+userId);
								String accept = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+"1##"+statusIds+"##"+userId);
								userMailSendForTeacher.setOfferAcceptURL(Utility.getValueOfPropByKey("basePath")+"/verifyoffer.do?key="+accept);
								userMailSendForTeacher.setOfferDeclineURL(Utility.getValueOfPropByKey("basePath")+"/verifyoffer.do?key="+decline);

								userMailSendForTeacher.setSchoolLocation(schoolLocation);
								userMailSendForTeacher.setLocationCode(locationCode); // set school location code

								userMailSendForTeacher.setJobTitle(jobOrder.getJobTitle());
								userMailSendForTeacher.setUserMaster(userMaster);
								userMailSendForTeacher.setJobOrder(jobOrder);
								userMailSendListFotTeacher.add(userMailSendForTeacher);
								System.out.println("===================Offer Letter send to teacher =================================");
								mailSendToUserByThread(userMailSendListFotTeacher,null,null);
								//System.out.println("===================Offer Letter mailed =================================");
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}//End Offer Ready Mail
				}//End  Without Panel
				/*
				 * outerJob: false, bEmailSendFor_DA_SA: false
				   bEmailSendFor_CA: false, offerReadyMailFlag: 0
				 */


				try
				{
					//System.out.println("4.20 ************ { End Panel }  *************");
					System.out.println("========================================================== ");
					System.out.println("outerJob: "+selectedstatus+", bEmailSendFor_DA_SA: "+bEmailSendFor_DA_SA);
					System.out.println("bEmailSendFor_CA: "+bEmailSendFor_CA+", offerReadyMailFlag: "+offerReadyMailFlag);
					System.out.println("========================================================== ");
				}catch(Exception e){e.printStackTrace();}

				if(outerJob==false && (bEmailSendFor_DA_SA || bEmailSendFor_CA)&& offerReadyMailFlag==0) // Email Send only for Selected CkeckBox for All DA/SA or a Teacher
				{
					try
					{
						System.out.println("bEmailSendFor_DA_SA  Or bEmailSendFor_CA");
						System.out.println(" selectedstatus : "+selectedstatus+" \n isUpdateStatus : "+isUpdateStatus+" \n mailSend : "+mailSend+"\n statusUpdateHDR "+statusUpdateHDR+"\n errorFlag :"+errorFlag+"\n statusHistory :"+statusHistory+"\n teacherDetail :"+teacherDetail.getEmailAddress()+" \n jobOrder "+jobOrder.getJobTitle()+" \n jobWiseScoreFlag "+jobWiseScoreFlag+"\n finalizeStatusFlag "+finalizeStatusFlag+"\n\n" );
					}catch(Exception e){e.printStackTrace();}

					if(selectedSecondaryStatus)
						mailSend=true;
					try
					{
						System.out.println("========================================================== ");
						System.out.println("selectedstatus: "+selectedstatus+", selectedSecondaryStatus: "+selectedSecondaryStatus);
						System.out.println("isUpdateStatus: "+isUpdateStatus+", mailSend: "+mailSend);
						System.out.println("statusUpdateHDR: "+statusUpdateHDR+", errorFlag: "+errorFlag);
						System.out.println("statusHistory: "+statusHistory+", teacherDetail: "+teacherDetail);
						System.out.println("jobWiseScoreFlag: "+jobWiseScoreFlag+", finalizeStatusFlag: "+finalizeStatusFlag);
						System.out.println("jobOrder: "+jobOrder+", mailSend: "+mailSend);
						System.out.println("========================================================== ");

					}catch(Exception e)
					{
						e.printStackTrace();
					}


					if((selectedstatus|| selectedSecondaryStatus)  && isUpdateStatus && mailSend && statusUpdateHDR==null && !errorFlag.equals("2") && !errorFlag.equals("4") &&  statusHistory==null && teacherDetail!=null && jobOrder!=null && (jobWiseScoreFlag || finalizeStatusFlag )){
						System.out.println(":::::>mail Send Now<:::::");
						String url = Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+jobOrder.getCreatedForEntity();
						List<SecondaryStatus> lstSecondaryStatus =	secondaryStatusDAO.findSecondaryStatus(jobOrder.getDistrictMaster());
						Map<Integer,SecondaryStatus> mapSecStatus = new HashMap<Integer, SecondaryStatus>();
						for(SecondaryStatus sec:lstSecondaryStatus ){
							if(sec.getStatusMaster()!=null)
								mapSecStatus.put(sec.getStatusMaster().getStatusId(),sec);
						}
						List<UserMaster> userMasters=new ArrayList<UserMaster>();
						List<UserMaster> userMastersEmailNotification=new ArrayList<UserMaster>();
						List<UserMaster> AllDAuserMasters=new ArrayList<UserMaster>();

						List<RoleMaster> roleMasters=new ArrayList<RoleMaster>();
						RoleMaster roleMaster=new RoleMaster();
						roleMaster.setRoleId(2);
						roleMasters.add(roleMaster);
						roleMaster=new RoleMaster();
						roleMaster.setRoleId(3);
						roleMasters.add(roleMaster);

							if(statusMasterPanel!=null && (statusMasterPanel.getStatusShortName().equalsIgnoreCase("hird") || statusMasterPanel.getStatusShortName().equalsIgnoreCase("rem") || statusMasterPanel.getStatusShortName().equalsIgnoreCase("dcln"))){
								try{
									boolean isSchool=false;
									if(jobOrder.getSelectedSchoolsInDistrict()!=null){
										if(jobOrder.getSelectedSchoolsInDistrict()==1){
											isSchool=true;
										}
									}
									if(jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2 && isSchool==false){
										userMasters=userEmailNotificationsDAO.getUserByDistrict_slcByHBD(jobOrder,statusShortName,roleMasters);
									}else if(jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2 && isSchool){
										List<SchoolMaster>  schoolMasters= new ArrayList<SchoolMaster>();
										if(jobOrder.getSchool()!=null){
											if(jobOrder.getSchool().size()!=0){
												for(SchoolMaster sMaster : jobOrder.getSchool()){
													schoolMasters.add(sMaster);
												}
											}
										}
										userMasters=userEmailNotificationsDAO.getUserByDistrictAndSchoolList_slc(districtMaster, schoolMasters,jobOrder,statusShortName,roleMasters);
									}else if(jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==3 && isSchool){
										userMasters=userEmailNotificationsDAO.getUserByDistrictListOnlySchool_slc(districtMaster,userMaster.getSchoolId(),jobOrder,statusShortName,roleMasters);
									}else if(jobOrder.getCreatedForEntity()==3 && userMaster.getEntityType()==3){
										userMasters=userEmailNotificationsDAO.getUserByOnlySchool_slc(userMaster.getSchoolId(),statusShortName,roleMasters);
									}
									
									System.out.println(" userMasters >>>>>>>>>>>> <<<<<<<<<<<<<<<<<<< "+userMasters.size());
									
								}catch(Exception e){
									e.printStackTrace();
								}
						}else{
							try{
								//Get Selected District----------------------------------------------------
								List<JobCategoryWiseStatusPrivilege> jobCateWSPList = new ArrayList<JobCategoryWiseStatusPrivilege>(); 
								try{
									//System.out.println(" >>>> jobOrder "+jobOrder);
									//System.out.println(" >>>> secondaryStatusPanel "+secondaryStatusPanel);
									//System.out.println(" >>>> statusMasterPanel "+statusMasterPanel);
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
								System.out.println(" Condiation 2");
								
								//For Selected DAs/SAa
								jobCateWSPList = jobCategoryWiseStatusPrivilegeDAO.getFilterStatusWithDPoint(jobOrder.getDistrictMaster(), jobOrder.getJobCategoryMaster(), secondaryStatusPanel,statusMasterPanel);

								if(jobCateWSPList!=null && jobCateWSPList.size()>0)
								{
									System.out.println(" jobCateWSPList ::::::::: >>>>>>>>> "+jobCateWSPList.get(0).getJobCategoryStatusPrivilegeId()+" userMasters "+userMasters);
								}
								String userDaIdsStr = ""; 
								boolean allSchoolFlag = false;

								if(jobCateWSPList.size()>0)
								{
									userDaIdsStr = jobCateWSPList.get(0).getEmailNotificationToSelectedDistrictAdmins();
									allSchoolFlag = jobCateWSPList.get(0).isEmailNotificationToAllSchoolAdmins();

									//System.out.println(" userDaIdsStr "+userDaIdsStr+" allSchoolFlag :: "+allSchoolFlag);

									if(userDaIdsStr==null || userDaIdsStr.equals(""))
									{
										AllDAuserMasters = userMasterDAO.getActiveDAUserByDistrict(districtMaster);
									}


									if((userDaIdsStr!=null && !userDaIdsStr.equals("")) || allSchoolFlag)
									{
										System.out.println(" Condiation 1");
										List<Integer> daIdList = new ArrayList<Integer>();
										if(userDaIdsStr!=null && !userDaIdsStr.equals(""))
										{
											String[] userDaIds = userDaIdsStr.split("#");

											if((userDaIdsStr!=null && !userDaIdsStr.equals("")))
											{
												for(int i=0;i<userDaIds.length;i++)
												{
													daIdList.add(Integer.parseInt(userDaIds[i]));
												}
											}
										}
										//get Schools
										if(allSchoolFlag)
										{
											if(userDaIdsStr==null || userDaIdsStr.equals(""))
											{
												AllDAuserMasters = userMasterDAO.getActiveDAUserByDistrictHBD(jobOrder);
											}

											System.out.println(" jobOrder.getCreatedForEntity() "+jobOrder.getCreatedForEntity()+" userMaster.getEntityType() "+userMaster.getEntityType());


											if(jobOrder.getCreatedForEntity()==2 && (userMaster.getEntityType()==2 || userMaster.getEntityType()==3)){
												List<SchoolMaster>  schoolMasters= new ArrayList<SchoolMaster>();
												if(jobOrder.getSchool()!=null){
													if(jobOrder.getSchool().size()!=0){
														for(SchoolMaster sMaster : jobOrder.getSchool()){
															schoolMasters.add(sMaster);
														}
													}
												}
												if(schoolMasters!=null && schoolMasters.size()>0)
												{
													if(daIdList!=null && daIdList.size()>0)
														userMasters	= userMasterDAO.getUserIDSBySchoolsAndDistrict(schoolMasters,daIdList);
													else
														userMasters	= userMasterDAO.getUserIDSBySchools(schoolMasters);

													if(AllDAuserMasters.size()>0)
														userMasters.addAll(AllDAuserMasters);
												}
												else
												{
													if(userDaIdsStr!=null && !userDaIdsStr.equals(""))
														userMasters = userMasterDAO.getUsersByUserIds(daIdList);
													else
													{
														if(AllDAuserMasters.size()>0)
															userMasters.addAll(AllDAuserMasters);
													}
												}
											}
										}else{
											userMasters = userMasterDAO.getUsersByUserIds(daIdList);
										}

										if(userMasters!=null && userMasters.size()>0)
											System.out.println(" >>>>>>>>>>> final user list :: "+userMasters.size());
										else
											System.out.println(" >>>>>>>>>>> final user list is 0 ");
									}
									else
									{
										System.out.println(" Condidation 1.1 ");
										System.out.println("AllDAuserMasters size :: "+AllDAuserMasters.size());
										if(AllDAuserMasters.size()>0)
											userMasters.addAll(AllDAuserMasters);
										System.out.println(" userMasters size :::::: "+userMasters.size());
									}
									System.out.println(" overrideAll "+overrideAll);
									//For All selected users which have opted notification on
									if(overrideAll==false)
									{
										if(userMasters.size()>0)
											userMasters = userEmailNotificationsDAO.getUserByNotificationOn_slc(userMasters, statusShortName, roleMasters);
									}
									System.out.println(" userMasters final>>>>>>>>>>>>>>>>>>>>>>>size :::::"+userMasters.size());
								}
								
								
							}catch(Exception e)
							{
								e.printStackTrace();
							}
						}
						
						
						/*========= Gagan :  Calling this method for getting Hr Detail available in District Set up ===========*/
						//String[] arrHrDetail = getHrDetailToTeacher(request,jobOrder);
						/*========= Gagan :  Calling this method for getting Hr Detail available in District Set up ===========*/
						String[] arrHrDetail = getHrDetailToTeacher(request,jobOrder);
						String hrContactFirstName	= arrHrDetail[0];
						String hrContactLastName	= arrHrDetail[1];
						String phoneNumber			= arrHrDetail[3];
						String title		 		= arrHrDetail[4];
						int isHrContactExist 		= Integer.parseInt(arrHrDetail[5]);
						String schoolDistrictName 	= arrHrDetail[6];
						String dmName 			 	= arrHrDetail[7];
						boolean isOSCEOLA=false;
						String currentStatusName="";
							if(statusMaster!=null){
						    	if(statusMasterSecondaryName!=null){
						    		currentStatusName=statusMasterSecondaryName.trim();
						    	}else{
						    		currentStatusName=statusMaster.getStatus().trim();
						    	}
						    }else if(secondaryStatus!=null){
						    	currentStatusName=secondaryStatus.getSecondaryStatusName().trim();
						    }
						System.out.println("currentStatusName=============yyy=============="+currentStatusName);
						if((currentStatusName.trim().equalsIgnoreCase("Intent to Hire") || currentStatusName.trim().equalsIgnoreCase("Credential Review")) && userMaster.getDistrictId().getDistrictId().equals(1201470)){
							isOSCEOLA=true;
						}
						
						if(userMaster.getDistrictId().getDistrictId().equals(1201470)){
							dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
						}
						String dmPhoneNumber	 	= arrHrDetail[9];
						Criterion criterion2				=	Restrictions.eq("statusShortName",hiddenStatusShortName);

						List<StatusSpecificEmailTemplates> lstStatusSpecificEmailTemplates	=	statusSpecificEmailTemplatesDAO.findByCriteria(criterion2);
						if(bEmailSendFor_DA_SA)// Email Send only for Selected CkeckBox for All DA/SA
						{
							String sEmailToUserName="";
							if(userMaster!=null)
							{
								if(userMaster.getFirstName()!=null && !userMaster.getFirstName().equalsIgnoreCase(""))
									sEmailToUserName=userMaster.getFirstName();
								if(userMaster.getLastName()!=null && !userMaster.getLastName().equalsIgnoreCase(""))
									sEmailToUserName=sEmailToUserName+" "+userMaster.getLastName();
							}
							List<UserMailSend> userMailSendList =new ArrayList<UserMailSend>();
							System.out.println(userMasters.size());
							for(UserMaster userMasterObj : userMasters)
							{
								UserMailSend userMailSend= new UserMailSend();

								////System.out.println("Add E-mail in List for Sending Email for Only DA and SA(Role ID 5 and 6 is not allowed). Email Id "+userMasterObj.getEmailAddress() +" Role ID "+userMasterObj.getRoleId().getRoleId()); 

								// System.out.println(" User  [ Subject message] : "+userMailSend.getSubject()+" \n Message Body : "+userMailSend.getMailBody());

								userMailSend.setEmail(userMasterObj.getEmailAddress());
								if(userMaster.getLastName()!=null){
									userMailSend.setFromUserName(userMaster.getFirstName()+" "+userMaster.getLastName());	 
								}else{
									userMailSend.setFromUserName(userMaster.getFirstName());
								}
								userMailSend.setIsUserOrTeacherFlag(1);
								userMailSend.setPanelExist("1");


								userMailSend.setToUserName(userMasterObj.getFirstName());
								if(teacherDetail.getLastName()!=null){
									userMailSend.setTeacherName(teacherDetail.getFirstName()+" "+teacherDetail.getLastName());
								}else{
									userMailSend.setTeacherName(teacherDetail.getFirstName());
								}
								userMailSend.setJobTitle(jobOrder.getJobTitle());
								userMailSend.setStatusNote(statusNotes);
								userMailSend.setGridURL(url);
								userMailSend.setUserMaster(userMaster);
								if(statusId!=null && !statusId.equals("0")){
									if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp"))	{
										SecondaryStatus sec=mapSecStatus.get(statusMaster.getStatusId());
										if(sec!=null)
											userMailSend.setStatusName(sec.getSecondaryStatusName());
									}else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
										SecondaryStatus sec=mapSecStatus.get(statusMaster.getStatusId());
										if(sec!=null)
											userMailSend.setStatusName(sec.getSecondaryStatusName());
									}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
										SecondaryStatus sec=mapSecStatus.get(statusMaster.getStatusId());
										if(sec!=null)
											userMailSend.setStatusName(sec.getSecondaryStatusName());
									}else{
										userMailSend.setStatusName(statusMaster.getStatus());
									}
								}else if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){

									userMailSend.setStatusName(secondaryStatus.getSecondaryStatusName());
								}
								if(isEmailTemplateChanged==0 && lstStatusSpecificEmailTemplates.size()>0)
								{
									userMailSend.setSubject(lstStatusSpecificEmailTemplates.get(0).getSubjectLine());
									/*if(isOSCEOLA){
										userMailSend.setSubject("Candidate Status Changed (Job "+jobOrder.getJobId()+"- "+jobOrder.getJobTitle()+")");
									}*/
									if(isOSCEOLA){
										String subjectKeyForOSCEOLA=GlobalServices.getConvertedString(Utility.getLocaleValuePropByKey("intentToHireOrCredentialReviewSubject", locale),"#brk#",new String[]{jobOrder.getJobId().toString(),jobOrder.getJobTitle()});
										userMailSend.setSubject(subjectKeyForOSCEOLA);
										//userMailSend.setSubject("Candidate Status Changed (Job "+jobOrder.getJobId()+"- "+jobOrder.getJobTitle()+")");
									}
									mailBody	=	lstStatusSpecificEmailTemplates.get(0).getTemplateBody();
									if(mailBody.contains("&lt; Teacher Name &gt;") || mailBody.contains("&lt; User Name &gt;") || mailBody.contains("&lt; Job Title &gt;")|| mailBody.contains("&lt; Status Name &gt;"))
									{
										StringBuffer hrSb 					= 	new StringBuffer();
										hrSb.append("<table>");
										hrSb.append("<tr>");
										hrSb.append("<td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>");
										if(isHrContactExist==0)
											hrSb.append(""+Utility.getLocaleValuePropByKey("  mailAcceotOrDeclineMailHtmal11", locale)+"<br/>"+schoolDistrictName+" "+Utility.getLocaleValuePropByKey("  msgRecrtAnsSelect", locale)+" <br/><br/>");
										else 
											if(isHrContactExist==1)
											{
												hrSb.append(""+Utility.getLocaleValuePropByKey("  mailAcceotOrDeclineMailHtmal11", locale)+"<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
												if(title!="")
													hrSb.append(title+"<br/>");
												if(phoneNumber!="")
													sb.append(phoneNumber+"<br/>");
												hrSb.append(schoolDistrictName+"<br/><br/>");
											}
											else
											{
												if(isHrContactExist==2)
												{
													hrSb.append(""+Utility.getLocaleValuePropByKey("  mailAcceotOrDeclineMailHtmal11", locale)+"<br/>"+dmName+"<br/>");
													if(dmPhoneNumber!="")
														sb.append(dmPhoneNumber+"<br/>");
													hrSb.append(schoolDistrictName+"<br/><br/>");
												}
											}
										hrSb.append("</td>");
										hrSb.append("</tr>");
										hrSb.append("</table>");

										////System.out.println("\n\n =================================================================================================");
										mailBody	=	mailBody.replaceAll("&lt; Teacher Name &gt;",userMailSend.getTeacherName());
										mailBody	=	mailBody.replaceAll("&lt; User Name &gt;",sEmailToUserName);
										mailBody	=	mailBody.replaceAll("&lt; Job Title &gt;",jobOrder.getJobTitle());
										mailBody	=	mailBody.replaceAll("&lt; Status Name &gt;",userMailSend.getStatusName());
										mailBody	=	mailBody.replaceAll("&lt; URL &gt;",url);
										mailBody	=	mailBody.replaceAll("&lt; Hr Contact &gt;",hrSb.toString());
										mailBody	=	mailBody.replaceAll("&lt; User EmailAddress &gt;",userMaster.getEmailAddress());
									}
									userMailSend.setMailBody(mailBody);
								}
								else
								{
									if(isEmailTemplateChanged==1)
									{
										System.out.println(" Else  block [ User]");
										userMailSend.setSubject(msgSubject);
										/*if(isOSCEOLA){
											userMailSend.setSubject("Candidate Status Changed (Job "+jobOrder.getJobId()+"- "+jobOrder.getJobTitle()+")");
										}*/
										if(isOSCEOLA){
											String subjectKeyForOSCEOLA=GlobalServices.getConvertedString(Utility.getLocaleValuePropByKey("intentToHireOrCredentialReviewSubject", locale),"#brk#",new String[]{jobOrder.getJobId().toString(),jobOrder.getJobTitle()});
											userMailSend.setSubject(subjectKeyForOSCEOLA);
											//userMailSend.setSubject("Candidate Status Changed (Job "+jobOrder.getJobId()+"- "+jobOrder.getJobTitle()+")");
										}
										userMailSend.setMailBody(mailBody);
									}
								}

								userMailSendList.add(userMailSend);
							}
							mailSendToUserByThread(userMailSendList,arrHrDetail,null);
						}
						System.out.println(" bEmailSendFor_CA  "+bEmailSendFor_CA);
						if(outerJob==false && bEmailSendFor_CA) // Email Send only for Selected CkeckBox for a Teacher
						{
							if(teacherDetail!=null)
							{ 
								System.out.println(" \n Gagan : In case of send mail to Teacher ");
								List<UserMailSend> userMailSendListFotTeacher =new ArrayList<UserMailSend>();

								TeacherDetail teacherDetail2=teacherDetail;

								UserMailSend userMailSendForTeacher= new UserMailSend();


								userMailSendForTeacher.setEmail(teacherDetail2.getEmailAddress());

								if(userMaster.getLastName()!=null){
									userMailSendForTeacher.setFromUserName(userMaster.getFirstName()+" "+userMaster.getLastName());	 
								}else{
									userMailSendForTeacher.setFromUserName(userMaster.getFirstName());
								}

								userMailSendForTeacher.setTeacherDetail(teacherDetail);
								userMailSendForTeacher.setIsUserOrTeacherFlag(4);
								userMailSendForTeacher.setPanelExist("1");
								userMailSendForTeacher.setIsEmailTemplateChangedTeacher(isEmailTemplateChangedTeacher);
								if(isEmailTemplateChangedTeacher==1)
								{
									userMailSendForTeacher.setSubject(msgSubjectTeacher);
									userMailSendForTeacher.setMailBody(mailBodyTeacher);
								}
								else
								{
									if(isEmailTemplateChangedTeacher==0)
										userMailSendForTeacher.setSubject(""+Utility.getLocaleValuePropByKey("  msgStatusChangedfor", locale)+" "+jobOrder.getJobTitle()+" "+Utility.getLocaleValuePropByKey("  msgPositionAppliedFor", locale)+"");

								}
								userMailSendForTeacher.setToUserName(teacherDetail2.getFirstName());
								if(teacherDetail.getFirstName()!=null){
									userMailSendForTeacher.setTeacherName(teacherDetail.getFirstName());
								}
								userMailSendForTeacher.setJobOrder(jobOrder);
								userMailSendForTeacher.setJobTitle(jobOrder.getJobTitle());
								userMailSendForTeacher.setStatusNote(statusNotes);
								userMailSendForTeacher.setGridURL(url);
								userMailSendForTeacher.setUserMaster(userMaster);
								if(statusId!=null && !statusId.equals("0")){
									if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp"))	{
										SecondaryStatus sec=mapSecStatus.get(statusMaster.getStatusId());
										if(sec!=null)
										{
											userMailSendForTeacher.setStatusShortName(statusMaster.getStatusShortName());
											userMailSendForTeacher.setStatusName(sec.getSecondaryStatusName());
										}
									}else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
										SecondaryStatus sec=mapSecStatus.get(statusMaster.getStatusId());
										if(sec!=null)
										{
											userMailSendForTeacher.setStatusShortName(statusMaster.getStatusShortName());
											userMailSendForTeacher.setStatusName(sec.getSecondaryStatusName());
										}
									}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
										SecondaryStatus sec=mapSecStatus.get(statusMaster.getStatusId());
										if(sec!=null)
										{
											userMailSendForTeacher.setStatusShortName(statusMaster.getStatusShortName());
											userMailSendForTeacher.setStatusName(sec.getSecondaryStatusName());
										}
									}else{
										userMailSendForTeacher.setStatusName(statusMaster.getStatus());
										userMailSendForTeacher.setStatusShortName(statusMaster.getStatusShortName());
									}
								}else if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
									userMailSendForTeacher.setStatusShortName("oth");
									userMailSendForTeacher.setStatusName(secondaryStatus.getSecondaryStatusName());
								}

								userMailSendListFotTeacher.add(userMailSendForTeacher);
								mailSendToUserByThread(userMailSendListFotTeacher,arrHrDetail,null);

								System.out.println(" ------- Sending Email for Teacher on Changing Status "+userMailSendForTeacher.getEmail()+" status short Name "+userMailSendForTeacher.getStatusShortName());
							}
						}
					}
					else
					{
						System.out.println("Could not Send email to DA/SA/Teacher");
					}
				}
				else
				{
					System.out.println(" No bEmailSendFor_DA_SA  Or bEmailSendFor_CA");
				}
			}
			
			/*
			 * 	Purpose	:	Remove dtata from table "sapcandidatedetails" and copy to "sapcandidatedetailshistory" 
			 * 	By		:	Hanzala Subhani
			 *  Date	:	23 Feb 2015
			 * 
			 */
			
			//if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
			
			SessionFactory sessionFactory=jobForTeacherDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			Transaction txOpen = statelesSsession.beginTransaction();
			List <SapCandidateDetails> sapCandidateDetailsList = sapCandidateDetailsDAO.checkSAPdataByTeacherAndJob(teacherDetail,jobOrder);
			if(statusMaster!=null && statusMaster.getStatusShortName()!=null){
				//System.out.println(":::::::::::: SapCandidate Detail ::::::::::::::");
				if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln") || statusMaster.getStatusShortName().equalsIgnoreCase("rem")) {
					if(getPrimaryStatus(lstTeacherStatusHistoryForJob)!=null && !getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("ED")){
						// Status Will not Changed
						System.out.println("Status Will not Change");
					} else if(getPrimaryStatus(lstTeacherStatusHistoryForJob)!=null && !getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("ER")){
						// Status Will not Changed
						System.out.println("Status Will not Change.");
					} else {
						if(sapCandidateDetailsList!=null || sapCandidateDetailsList.size()>0){
							for(SapCandidateDetails sapCandidateDetails : sapCandidateDetailsList){
								SapCandidateDetailsHistory copySapHistory = new SapCandidateDetailsHistory();
								//copySapHistory.se
								try{
									BeanUtils.copyProperties(copySapHistory, sapCandidateDetails);
									copySapHistory.setSapCandidateId(null);
									copySapHistory.setHistoryDate(new Date());
								} catch (IllegalAccessException e) {
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									e.printStackTrace();
								}
								// Copy Record
								try {
									 txOpen =statelesSsession.beginTransaction();
									 statelesSsession.insert(copySapHistory);
									 txOpen.commit();
								} catch (HibernateException e) {
									e.printStackTrace();
								}
								// Delete Record
								try {
									txOpen =statelesSsession.beginTransaction();
									statelesSsession.delete(sapCandidateDetails);
									txOpen.commit();
								} catch (HibernateException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			}
			////Ankit//////////
			List<StrengthOpportunitySummary> strengthOpportunitySummaries = strengthOpportunitySummaryDAO.findStrengthOpportunityByTeacherIdandJobId(teacherDetail,jobOrder);
			List<String> competencies = new ArrayList<String>();
			competencies = strengthOpportunitySummaryDAO.getAllCompetencies();
			SessionFactory sessionSummary = strengthOpportunitySummaryDAO.getSessionFactory();
			StatelessSession statelessSummary = sessionSummary.openStatelessSession();
			Transaction trans = statelessSummary.beginTransaction();
			int size = 0;
			if(finalizeStatus.equals("1") && strength!=null && growthOpp!=null && growthOpp.length>0 && strength.length>0)
			{
				try {
					schoolMaster=schoolMasterDAO.findById(schoolIdInputFromUser, false, false);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				if(strengthOpportunitySummaries!=null && strengthOpportunitySummaries.size()>0)
				{
					int count = 0;
					for(StrengthOpportunitySummary sos : strengthOpportunitySummaries)
					{
						size = strength[count].split("@@@").length;
						if(size>1)
						{
							if(strength[count].split("@@@")[1]!=null && strength[count].split("@@@")[1].length()>0)
								sos.setStrength(strength[count].split("@@@")[1]);
						}

						size = growthOpp[count].split("@@@").length;
						if(size>1)
						{
							if(growthOpp[count].split("@@@")[1]!=null && growthOpp[count].split("@@@")[1].length()>0)
								sos.setGrowthOpportunity(growthOpp[count].split("@@@")[1]);
						}
						sos.setCompetency(competencies.get(count));
						//sos.setSchoolMaster(schoolMaster);
						sos.setRecommendSchoolReason(recomreason);
						sos.setQuestionForInterview(remquestion);
						count++;
						statelessSummary.update(sos);
					}
					trans.commit();
				}
				else
				{
					for(int i=0;i<6;i++)
					{
						StrengthOpportunitySummary sos = new StrengthOpportunitySummary();
						size = strength[i].split("@@@").length;
						if(size>1)
						{
							if(strength[i].split("@@@")[1]!=null && strength[i].split("@@@")[1].length()>0 && !strength[i].split("@@@")[1].equalsIgnoreCase("undefined"))
							{
								sos.setStrength(strength[i].split("@@@")[1]);
							}
						}

						size = growthOpp[i].split("@@@").length;
						if(size>1)
						{
							if(growthOpp[i].split("@@@")[1]!=null && growthOpp[i].split("@@@")[1].length()>0 && !growthOpp[i].split("@@@")[1].equalsIgnoreCase("undefined"))
							{
								sos.setGrowthOpportunity(growthOpp[i].split("@@@")[1]);
							}
						}
					
						sos.setTeacherDetail(teacherDetail);
						sos.setJobOrder(jobOrder);
						sos.setCompetency(competencies.get(i));
						sos.setSchoolMaster(schoolMaster);
						sos.setRecommendSchoolReason(recomreason);
						sos.setQuestionForInterview(remquestion);
						statelessSummary.insert(sos);
					}
					trans.commit();
				}
			}
			///////End Ankit /////////////
		}catch (Exception e) {
			e.printStackTrace();
		}catch (Throwable ee) {
			ee.printStackTrace();
		}
		/*
		 * 		Send e_Reference
		 * 		By Hanzala Subhani
		 * 		Dated : 05 May 2015
		 * 
		 * */
		Integer referenceStatusId 		= 	null;
		Integer referenceSecStatusId	=	null;
		String referenceSecStatusName	=	"";
		String secondaryStatusName		=	"";
		try{
			if(districtMaster!=null && districtMaster.getStatusMasterForReference()!=null && districtMaster.getStatusMasterForReference().getStatusId() !=null){
				referenceStatusId = districtMaster.getStatusMasterForReference().getStatusId();
			}
			
			if(districtMaster!=null && districtMaster.getSecondaryStatusForReference() != null && districtMaster.getSecondaryStatusForReference().getSecondaryStatusId() != null ){
				referenceSecStatusId 	=	districtMaster.getSecondaryStatusForReference().getSecondaryStatusId();
				referenceSecStatusName	=	districtMaster.getSecondaryStatusForReference().getSecondaryStatusName();
			}
			if(secondaryStatus!=null)
				secondaryStatusName			=	secondaryStatus.getSecondaryStatusName();	
			
			
			if((referenceStatusId !=null && referenceStatusId!=0) || (referenceSecStatusName!=null && referenceSecStatusName!="")){
				if(statusId.equals(referenceStatusId) || secondaryStatusName.equalsIgnoreCase(referenceSecStatusName)){					
					System.out.println("e-Reference Sent Success");
					sendReferenceMailOnFinalize(Integer.parseInt(teacherId),Integer.parseInt(jobId));
				}
			}
			
		} catch(Exception exception){
			exception.printStackTrace();
		}
		
		/* End Reference Check */
		System.out.println("errorFlag::::::::::::::::::::;:"+errorFlag);
		return errorFlag+"##"+statusUpdateHDR+"##"+secondaryStatusNames+"##"+multiJobReturn;

	}	

	public String  saveStatusNoteForJobs(Map<String,StatusSpecificScore> questionTxtMap,JobCategoryMaster jobCategoryMaster,Map<String,StatusSpecificScore> ssfMapfoCategory,Map<Integer,StatusSpecificScore> questionMap,Map<String,StatusSpecificScore> ssfMap,JobForTeacher jobForTeacher,String fitScore,String teacherStatusNoteId,UserMaster userMaster,TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,String statusNotes,String statusNoteFileName,String emailSentToD,String emailSentToS,String scoreProvided,String finalizeStatus,boolean bEmailSendFor_DA_SA,boolean bEmailSendFor_CA,Integer[] answerId_array,Integer[] score_array,int isEmailTemplateChanged, String hiddenStatusShortName,String msgSubject,String mailBody,Integer txtschoolCount,Long schoolIdInputFromUser,int isEmailTemplateChangedTeacher,String msgSubjectTeacher,String mailBodyTeacher,boolean chkOverridetSatus,String requisitionNumberId,String qqNoteIdsvalue_str,String questionAssessmentIds,int jobCategoryFlagValue)
	{
		//System.out.println(jobCategoryFlagValue+" ==============================================================================saveStatusNoteForJobs=============================================================================================");
		//System.out.println("requisitionNumberId::::"+requisitionNumberId);
		StringBuffer sb = new StringBuffer("");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		String errorFlag="1";
		String secondaryStatusNames=""; 
		String statusUpdateHDR=null;
		try {

			int roleId=0;
			int entityType=0;
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
				entityType=userMaster.getEntityType();
			}
			List<Long> qAnssementQuesIds = new ArrayList<Long>();
			String qqNoteIdsvalue_str_array[]=null;
			String qAssessmentIds_array[]=null;
			try{
				if(questionAssessmentIds!=null && !questionAssessmentIds.equals("")){
					qqNoteIdsvalue_str_array= qqNoteIdsvalue_str.split("#");
					qAssessmentIds_array= questionAssessmentIds.split("#");
					for(int i=0;i<qAssessmentIds_array.length;i++)
					{
						qAnssementQuesIds.add(Long.parseLong(qAssessmentIds_array[i]));
					}
				}
			}catch(Exception e){
				//e.printStackTrace();
			}


			CandidateGridService cgService=new CandidateGridService();
			JobForTeacher jobForTeacherObj =new JobForTeacher();
			jobForTeacherObj=jobForTeacher;
			SchoolInJobOrder schoolInJobOrder = null;
			boolean outerJob=true;
			if(userMaster.getEntityType()==3){
				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, userMaster.getSchoolId());
				if(schoolInJobOrder!=null){
					outerJob=false;
				}
			}else{
				outerJob=false;
			}
			boolean isUpdateStatus=cgService.isUpdateStatus(jobForTeacherObj,userMaster,schoolInJobOrder);

			//System.out.println("outerJob::::::::::::::::::::::::::::::::::::>>>"+outerJob);

			DistrictMaster districtMaster =null;
			if(jobOrder.getCreatedForEntity()!=1){
				try{
					districtMaster=jobOrder.getDistrictMaster();
				}catch(Exception e){}
			}

			boolean isMiami = false;
			try{
				if(districtMaster!=null && districtMaster.getDistrictId().equals(1200390))
					isMiami=true;
			}catch(Exception e){}


			SchoolMaster schoolMaster =null;
			if(jobOrder.getCreatedForEntity()==3){
				try{
					schoolMaster=jobOrder.getSchool().get(0);
				}catch(Exception e){}
			}
			SecondaryStatus secondaryStatusForPanel=null;
			String statusShortName="soth";
			secondaryStatusForPanel=secondaryStatus;
			boolean selectedstatus=false;
			boolean selectedSecondaryStatus=false;
			try{
				if(jobForTeacherObj.getStatus()!=null && statusMaster!=null){
					selectedstatus=cgService.selectedStatusCheck(statusMaster,jobForTeacherObj.getStatus());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			try{
				if(secondaryStatus!=null && jobForTeacherObj.getStatus()!=null &&  statusMaster!=null){// && (jobForTeacherObj.getStatus().getStatusShortName().equals("scomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("ecomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("vcomp"))
					if(jobForTeacherObj.getSecondaryStatus()!=null){
						selectedSecondaryStatus=cgService.selectedPriSecondaryStatusCheck(secondaryStatus,jobForTeacherObj.getSecondaryStatus());
						if(selectedSecondaryStatus==false){
							selectedstatus=false;
						}
					}
				}else if(jobForTeacherObj.getSecondaryStatus()!=null && secondaryStatus!=null && statusMaster==null){
					if(jobForTeacherObj.getStatus()!=null){
						if(cgService.priSecondaryStatusCheck(jobForTeacherObj.getStatus(),secondaryStatus)){
							selectedSecondaryStatus=cgService.selectedNotPriSecondaryStatusCheck(secondaryStatus,jobForTeacherObj.getSecondaryStatus());
						}
					}
				}else if(jobForTeacherObj.getSecondaryStatus()==null && secondaryStatus!=null && statusMaster==null){
					if(jobForTeacherObj.getStatus()!=null){
						if(cgService.priSecondaryStatusCheck(jobForTeacherObj.getStatus(),secondaryStatus)){
							selectedSecondaryStatus=true;
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			//System.out.println("::::::selectedstatus::::::::;;>>>>>>>>>>>>>>>>>>>>>>>>>>>;>>"+selectedstatus);
			//System.out.println(":::::::;;selectedSecondaryStatus::::::::;;;>>"+selectedSecondaryStatus);

			if(statusMaster!=null){
				secondaryStatus=null;
			}
			TeacherStatusHistoryForJob statusHistory=teacherStatusHistoryForJobDAO.findByTeacherStatus(teacherDetail,jobOrder,statusMaster,secondaryStatus);
			boolean  finalizeStatusFlag=false;
			try{
				if(districtMaster.getStatusNotes()!=null){
					if(districtMaster.getStatusNotes()==false){
						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								finalizeStatusFlag=true;
							}
						}
					}
				}
			}catch(Exception e){}
			try{

				if(teacherStatusNoteId.equals("0"))
				{

					if((statusNoteFileName!=null && !statusNoteFileName.equals("")) || (statusNotes!=null && !statusNotes.equals("")))
					{
						TeacherStatusNotes tsDetailsObj = new TeacherStatusNotes();
						if(teacherStatusNoteId!=null){
							if(!teacherStatusNoteId.equals("0")){
								tsDetailsObj =teacherStatusNotesDAO.findById(Integer.parseInt(teacherStatusNoteId),false,false);
							}
						}
						tsDetailsObj.setTeacherDetail(teacherDetail);
						tsDetailsObj.setUserMaster(userMaster);
						tsDetailsObj.setJobOrder(jobOrder);
						if(districtMaster!=null){
							tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
						}
						if(statusMaster!=null){
							tsDetailsObj.setStatusMaster(statusMaster);
						}
						if(secondaryStatus!=null){
							tsDetailsObj.setSecondaryStatus(secondaryStatus);
						}
						if(statusNotes!=null){
							tsDetailsObj.setStatusNotes(statusNotes);
						}
						if(statusNoteFileName!=null){
							if(tsDetailsObj.getStatusNoteFileName()!=null){
								try{
									File file = new File(Utility.getValueOfPropByKey("communicationRootPath")+"statusNote/"+tsDetailsObj.getTeacherDetail().getTeacherId()+"/"+tsDetailsObj.getStatusNoteFileName());
									if(file.delete()){
										System.out.println(file.getName() + " is deleted!");
									}else{
										System.out.println("Delete operation is failed.");
									}
								}catch(Exception e){}
							}
							tsDetailsObj.setStatusNoteFileName(statusNoteFileName);
						}
						if(schoolMaster!=null){
							tsDetailsObj.setSchoolId(schoolMaster.getSchoolId());	
						}
						int emailSentTo=0;
						if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
							emailSentTo=2;
						}
						if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
							emailSentTo=1;
						}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
							emailSentTo=3;
						}
						tsDetailsObj.setEmailSentTo(emailSentTo);

						tsDetailsObj.setTeacherAssessmentQuestionId(null);

						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								tsDetailsObj.setFinalizeStatus(true);
								finalizeStatusFlag=true;
							}
						}
						teacherStatusNotesDAO.makePersistent(tsDetailsObj);
					}

					//mukesh
					if((statusNotes==null || statusNotes.equals("")) && fitScore.equals("0")){
						if(districtMaster.getStatusNotes()){
							if(finalizeStatus!=null){
								if(!finalizeStatus.equals("0")){
									finalizeStatusFlag=true;
								}
							}
						}
					}
					TeacherStatusNotes checkForFinalize = teacherStatusNotesDAO.chkQuestionNotesStatusByUserMasterAndHBD(teacherDetail, jobOrder,qAnssementQuesIds,userMaster);

					if(checkForFinalize!=null && checkForFinalize.isFinalizeStatus()){
						System.out.println(" Data is finalize.......");
					}else{
						int ii=0;
						// For Question Status Notes
						if(qAssessmentIds_array!=null)
							for (Entry<Integer,StatusSpecificScore> entry : questionMap.entrySet()) {
								StatusSpecificQuestions statusSpecificQuestions=null;
								StatusSpecificScore statusSpecificScore=entry.getValue();
								String ssfValue="";
								if(jobCategoryMaster.equals(jobOrder.getJobCategoryMaster())){
									if(statusSpecificScore.getStatusSpecificQuestions()!=null){
										ssfValue=statusSpecificScore.getStatusSpecificQuestions().getQuestionId()+"##"+jobOrder.getJobId();
									}else{
										ssfValue=statusSpecificScore.getTeacherAnswerDetail().getAnswerId()+"##"+jobOrder.getJobId();
									}
									//System.out.println("ssfValue:::::::::::::::::::::::::::::22222222222::::::::::::::"+ssfValue);
									if(ssfMap.get(ssfValue)!=null){
										StatusSpecificScore statusSpecificScoreObj=ssfMap.get(ssfValue);
										statusSpecificQuestions=statusSpecificScoreObj.getStatusSpecificQuestions();
									}
								}else{
									if(statusSpecificScore.getStatusSpecificQuestions()!=null){
										if(statusSpecificScore.getSkillAttributesMaster()!=null){
											ssfValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+statusSpecificScore.getSkillAttributesMaster().getSkillAttributeId()+"##"+jobOrder.getJobId();
										}else{
											ssfValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+jobOrder.getJobId();
										}
									}else{
										ssfValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getTeacherAnswerDetail().getAnswerId()+"##"+jobOrder.getJobId();
									}
									//System.out.println("ssfCValue:::::::::::::::::::::::::::::1111::::::::::::::"+ssfValue);
									if(ssfMapfoCategory.get(ssfValue)!=null){
										StatusSpecificScore statusSpecificScoreObj=ssfMapfoCategory.get(ssfValue);
										statusSpecificQuestions=statusSpecificScoreObj.getStatusSpecificQuestions();
									}
								}
								int questionId=0;
								try{
									if(statusSpecificQuestions!=null){
										//System.out.println("questionId:::::::::::"+statusSpecificQuestions.getQuestionId());
										questionId=statusSpecificQuestions.getQuestionId();
									}
								}catch(Exception e){}



								TeacherStatusNotes tsDetailsObj = teacherStatusNotesDAO.getQuestionNoteObjByUserMasterAndHBD(teacherDetail, jobOrder,Long.parseLong(questionId+""),userMaster);
								if(tsDetailsObj==null)
								{
									tsDetailsObj = new TeacherStatusNotes();
								}


								tsDetailsObj.setTeacherDetail(teacherDetail);
								tsDetailsObj.setUserMaster(userMaster);
								tsDetailsObj.setJobOrder(jobOrder);
								if(districtMaster!=null){
									tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
								}
								if(statusMaster!=null){
									tsDetailsObj.setStatusMaster(statusMaster);
								}
								if(secondaryStatus!=null){
									tsDetailsObj.setSecondaryStatus(secondaryStatus);
								}
								tsDetailsObj.setStatusNoteFileName(null);
								if(schoolMaster!=null){
									tsDetailsObj.setSchoolId(schoolMaster.getSchoolId());	
								}
								int emailSentTo=0;
								if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
									emailSentTo=2;
								}
								if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
									emailSentTo=1;
								}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
									emailSentTo=3;
								}
								tsDetailsObj.setEmailSentTo(emailSentTo);

								if(statusSpecificQuestions!=null)
									tsDetailsObj.setTeacherAssessmentQuestionId(Long.parseLong(questionId+""));
								else
									tsDetailsObj.setTeacherAssessmentQuestionId(null);

								boolean noQuestion=false;
								if(qqNoteIdsvalue_str_array!=null && qqNoteIdsvalue_str_array.length>ii){
									if(qqNoteIdsvalue_str_array[ii]!=null && !qqNoteIdsvalue_str_array[ii].equals("")){
										tsDetailsObj.setStatusNotes(qqNoteIdsvalue_str_array[ii]);
										noQuestion=true;
									}
								}
								if(finalizeStatus!=null){
									if(!finalizeStatus.equals("0")){
										tsDetailsObj.setFinalizeStatus(true);
										finalizeStatusFlag=true;
									}
								}
								if(noQuestion){
									teacherStatusNotesDAO.makePersistent(tsDetailsObj);
								}
								ii++;
							}/////////////
					}
				}
				else
				{
					if((statusNoteFileName!=null && !statusNoteFileName.equals("")) || (statusNotes!=null && !statusNotes.equals("")))
					{
						TeacherStatusNotes tsDetailsObj = new TeacherStatusNotes();
						if(teacherStatusNoteId!=null){
							if(!teacherStatusNoteId.equals("0")){
								tsDetailsObj =teacherStatusNotesDAO.findById(Integer.parseInt(teacherStatusNoteId),false,false);
							}
						}
						tsDetailsObj.setTeacherDetail(teacherDetail);
						tsDetailsObj.setUserMaster(userMaster);
						tsDetailsObj.setJobOrder(jobOrder);
						tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
						if(statusMaster!=null){
							tsDetailsObj.setStatusMaster(statusMaster);
						}
						if(secondaryStatus!=null){
							tsDetailsObj.setSecondaryStatus(secondaryStatus);
						}
						if(statusNotes!=null){
							tsDetailsObj.setStatusNotes(statusNotes);
						}
						if(statusNoteFileName!=null){
							if(tsDetailsObj.getStatusNoteFileName()!=null){
								try{
									File file = new File(Utility.getValueOfPropByKey("communicationRootPath")+"statusNote/"+tsDetailsObj.getTeacherDetail().getTeacherId()+"/"+tsDetailsObj.getStatusNoteFileName());
									if(file.delete()){
										System.out.println(file.getName() + " is deleted!");
									}else{
										System.out.println("Delete operation is failed.");
									}
								}catch(Exception e){}
							}
							tsDetailsObj.setStatusNoteFileName(statusNoteFileName);
						}
						if(schoolMaster!=null){
							tsDetailsObj.setSchoolId(schoolMaster.getSchoolId());	
						}
						int emailSentTo=0;
						if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
							emailSentTo=2;
						}
						if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
							emailSentTo=1;
						}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
							emailSentTo=3;
						}
						tsDetailsObj.setEmailSentTo(emailSentTo);

						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								tsDetailsObj.setFinalizeStatus(true);
								finalizeStatusFlag=true;
							}
						}
						teacherStatusNotesDAO.makePersistent(tsDetailsObj);
					}

					TeacherStatusNotes checkForFinalize = teacherStatusNotesDAO.chkQuestionNotesStatusByUserMasterAndHBD(teacherDetail, jobOrder,qAnssementQuesIds,userMaster);

					if(checkForFinalize!=null && checkForFinalize.isFinalizeStatus()){
						System.out.println(" Data is finalize.......");
					}else{
						int j=0;
						// For Question Status Notes in Edit Mode
						if(qAssessmentIds_array!=null)
							for (Entry<Integer,StatusSpecificScore> entry : questionMap.entrySet()) {
								StatusSpecificQuestions statusSpecificQuestions=null;
								StatusSpecificScore statusSpecificScore=entry.getValue();
								String ssfValue="";
								if(jobCategoryMaster.equals(jobOrder.getJobCategoryMaster())){
									if(statusSpecificScore.getStatusSpecificQuestions()!=null){
										ssfValue=statusSpecificScore.getStatusSpecificQuestions().getQuestionId()+"##"+jobOrder.getJobId();
									}else{
										ssfValue=statusSpecificScore.getTeacherAnswerDetail().getAnswerId()+"##"+jobOrder.getJobId();
									}
									//System.out.println("ssfValue:::::::::::::::::::::::::::::22222222222::::::::::::::"+ssfValue);
									if(ssfMap.get(ssfValue)!=null){
										StatusSpecificScore statusSpecificScoreObj=ssfMap.get(ssfValue);
										statusSpecificQuestions=statusSpecificScoreObj.getStatusSpecificQuestions();
									}
								}else{
									if(statusSpecificScore.getStatusSpecificQuestions()!=null){
										if(statusSpecificScore.getSkillAttributesMaster()!=null){
											ssfValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+statusSpecificScore.getSkillAttributesMaster().getSkillAttributeId()+"##"+jobOrder.getJobId();
										}else{
											ssfValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+jobOrder.getJobId();
										}
									}else{
										ssfValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getTeacherAnswerDetail().getAnswerId()+"##"+jobOrder.getJobId();
									}
									//System.out.println("ssfCValue:::::::::::::::::::::::::::::1111::::::::::::::"+ssfValue);
									if(ssfMapfoCategory.get(ssfValue)!=null){
										StatusSpecificScore statusSpecificScoreObj=ssfMapfoCategory.get(ssfValue);
										statusSpecificQuestions=statusSpecificScoreObj.getStatusSpecificQuestions();
									}
								}
								int questionId=0;
								try{
									if(statusSpecificQuestions!=null){
										//System.out.println("questionId:::::::::::"+statusSpecificQuestions.getQuestionId());
										questionId=statusSpecificQuestions.getQuestionId();
									}
								}catch(Exception e){}
								TeacherStatusNotes tsDetailsObj = teacherStatusNotesDAO.getQuestionNoteObjByUserMasterAndHBD(teacherDetail, jobOrder, Long.parseLong(questionId+""),userMaster);
								if(tsDetailsObj==null)
								{
									tsDetailsObj = new TeacherStatusNotes();
								}


								tsDetailsObj.setTeacherDetail(teacherDetail);
								tsDetailsObj.setUserMaster(userMaster);
								tsDetailsObj.setJobOrder(jobOrder);
								if(districtMaster!=null){
									tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
								}
								if(statusMaster!=null){
									tsDetailsObj.setStatusMaster(statusMaster);
								}
								if(secondaryStatus!=null){
									tsDetailsObj.setSecondaryStatus(secondaryStatus);
								}
								tsDetailsObj.setStatusNoteFileName(null);
								if(schoolMaster!=null){
									tsDetailsObj.setSchoolId(schoolMaster.getSchoolId());
								}
								int emailSentTo=0;
								if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
									emailSentTo=2;
								}
								if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
									emailSentTo=1;
								}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
									emailSentTo=3;
								}
								tsDetailsObj.setEmailSentTo(emailSentTo);

								if(statusSpecificQuestions!=null)
									tsDetailsObj.setTeacherAssessmentQuestionId(Long.parseLong(questionId+""));
								else
									tsDetailsObj.setTeacherAssessmentQuestionId(null);

								boolean noQuestion=false;
								if(qqNoteIdsvalue_str_array!=null && qqNoteIdsvalue_str_array.length>j){
									if(qqNoteIdsvalue_str_array[j]!=null && !qqNoteIdsvalue_str_array[j].equals("")){
										tsDetailsObj.setStatusNotes(qqNoteIdsvalue_str_array[j]);
										noQuestion=true;
									}
								}

								if(finalizeStatus!=null){
									if(!finalizeStatus.equals("0")){
										tsDetailsObj.setFinalizeStatus(true);
										finalizeStatusFlag=true;
									}
								}
								if(noQuestion){
									teacherStatusNotesDAO.makePersistent(tsDetailsObj);
								}
								//}
								j++;
							}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}	

			List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
			SecondaryStatus secondaryStatus_temp=null;
			StatusMaster statusMaster_temp=null;
			List<SecondaryStatus> lstSecondaryStatus_temp=new ArrayList<SecondaryStatus>();

			if(statusMaster!=null)
			{
				lstSecondaryStatus_temp=secondaryStatusDAO.getSecondaryStatusForPanelByHBD(jobOrder, statusMaster);
				if(lstSecondaryStatus_temp.size() ==1)
				{
					secondaryStatus_temp=lstSecondaryStatus_temp.get(0);
					statusMaster_temp=null;
				}
				else
				{
					statusMaster_temp=statusMaster;
				}
			}
			else

			{
				secondaryStatus_temp=secondaryStatus;
			}

			jobWisePanelStatusList=jobWisePanelStatusDAO.getPanel(jobOrder, secondaryStatus_temp, statusMaster_temp);

			// Start ... Panel
			boolean bPanel=false;
			StatusMaster statusMasterPanel=null;
			statusMasterPanel=statusMaster;
			System.out.println("jobWisePanelStatusList"+jobWisePanelStatusList.size());
			if(jobWisePanelStatusList.size() == 1)
			{

				bPanel=true;
			}


			String schoolLocation="";
			if(isMiami){
				JobForTeacher jobForTeacherForisMiami = jobForTeacher;


				List<SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus= new ArrayList<SchoolWiseCandidateStatus>();
				if(userMaster!=null)
					if(userMaster.getEntityType()==3){	
						lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusHistoryForSchoolByHBD(jobForTeacherForisMiami.getTeacherId(),jobForTeacherForisMiami.getJobId(),userMaster.getSchoolId());
					}else if(userMaster.getEntityType()==2){
						lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusHistoryByUserAndHBD(jobForTeacherForisMiami.getTeacherId(), jobForTeacherForisMiami.getJobId(),userMaster);
					}

				//System.out.println("lstSchoolWiseCandidateStatus:::::::::::::::::::::>>>>>>>>>"+lstSchoolWiseCandidateStatus.size());
				if(lstSchoolWiseCandidateStatus.size()>0){
					SchoolWiseCandidateStatus schoolWiseCandidateStatus=lstSchoolWiseCandidateStatus.get(0);
					schoolWiseCandidateStatusDAO.makeTransient(schoolWiseCandidateStatus);
				}

				try{
					SchoolMaster schoolObj=null;
					if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
						try{
							schoolObj=jobRequisitionNumbersDAO.findSchoolByJobReqId(jobOrder,requisitionNumberId);
						}catch(Exception e){}
					}
					if(jobForTeacherForisMiami!=null && jobForTeacherForisMiami.getRequisitionNumber()!=null){
						List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobOrder,jobForTeacherForisMiami.getJobId().getDistrictMaster(),jobForTeacherForisMiami.getRequisitionNumber());
						if(jobReqList.size()>0){
							try{
								schoolObj=jobReqList.get(0).getSchoolMaster();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					if(schoolObj!=null){	
						schoolLocation=schoolObj.getSchoolName();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}

			try{
				if(isMiami && jobOrder!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Substitute Instructional Positions"))
				{
					bPanel=true;
				}
			}catch(Exception e){}


			// Start Update Answer Score
			boolean sameQuestion=true;
			int inputSumAnswerScore=0;
			int inputSumAnswerMaxScore=0; 

			if(answerId_array!=null && answerId_array.length>0 && !bPanel)
			{
				for (Entry<Integer,StatusSpecificScore> entry : questionMap.entrySet()) {
					if(jobCategoryMaster.equals(jobOrder.getJobCategoryMaster())){
						StatusSpecificScore statusSpecificScore=entry.getValue();
						String ssfValue="";
						if(statusSpecificScore.getStatusSpecificQuestions()!=null){
							ssfValue=statusSpecificScore.getStatusSpecificQuestions().getQuestionId()+"##"+jobOrder.getJobId();
						}else{
							ssfValue=statusSpecificScore.getTeacherAnswerDetail().getAnswerId()+"##"+jobOrder.getJobId();
						}
						//System.out.println("ssfValue:::::::::::::::::::::::::::::><"+ssfValue);
						if(ssfMap.get(ssfValue)!=null){
							inputSumAnswerScore=inputSumAnswerScore+statusSpecificScore.getScoreProvided();
							inputSumAnswerMaxScore=inputSumAnswerMaxScore+statusSpecificScore.getMaxScore();
						}else{
							sameQuestion=false;
						}
					}else{
						StatusSpecificScore statusSpecificScore=entry.getValue();
						String ssfValue="";
						if(statusSpecificScore.getStatusSpecificQuestions()!=null){
							if(statusSpecificScore.getSkillAttributesMaster()!=null){
								ssfValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+statusSpecificScore.getSkillAttributesMaster().getSkillAttributeId()+"##"+jobOrder.getJobId();
							}else{
								ssfValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+jobOrder.getJobId();
							}
						}else{
							ssfValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getTeacherAnswerDetail().getAnswerId()+"##"+jobOrder.getJobId();
						}
						//System.out.println("ssfCValue:::::::::::::::::::::::::::::><:"+ssfValue);
						if(ssfMapfoCategory.get(ssfValue)!=null){
							inputSumAnswerScore=inputSumAnswerScore+statusSpecificScore.getScoreProvided();
							inputSumAnswerMaxScore=inputSumAnswerMaxScore+statusSpecificScore.getMaxScore();
						}else{
							sameQuestion=false;
						}
					}
				}
				//System.out.println("sameQuestion::::::::::1>>>>>>"+sameQuestion);
				if(sameQuestion){
					if(jobCategoryMaster.equals(jobOrder.getJobCategoryMaster())){
						for (Entry<String,StatusSpecificScore> entry : ssfMap.entrySet()) {
							StatusSpecificScore statusSpecificScore=entry.getValue();
							StatusSpecificScore score=null;
							if(statusSpecificScore.getStatusSpecificQuestions()!=null){
								score= questionMap.get(statusSpecificScore.getStatusSpecificQuestions().getQuestionId());
							}else{
								score= questionMap.get(statusSpecificScore.getTeacherAnswerDetail().getAnswerId());
							}
							if(statusSpecificScore.getJobOrder().getJobId().equals(jobOrder.getJobId())){
								if(score!=null){
									String ssfValue="";
									if(score.getStatusSpecificQuestions()!=null){
										ssfValue=score.getStatusSpecificQuestions().getQuestionId()+"##"+jobOrder.getJobId();
									}else{
										ssfValue=score.getTeacherAnswerDetail().getAnswerId()+"##"+jobOrder.getJobId();
									}
									//System.out.println("ssfValue::::>>>>>>>>>>>>2nd:::::::::::::::::::::::::><"+ssfValue);
									if(ssfMap.get(ssfValue)!=null){
										//inputSumAnswerScore=inputSumAnswerScore+statusSpecificScore.getScoreProvided();
										//inputSumAnswerMaxScore=inputSumAnswerMaxScore+statusSpecificScore.getMaxScore();
									}else{
										sameQuestion=false;
									}
								}else{
									sameQuestion=false; 
								}
							}

						}
					}else{
						for (Entry<String,StatusSpecificScore> entry : ssfMapfoCategory.entrySet()) {
							StatusSpecificScore statusSpecificScore=entry.getValue();
							StatusSpecificScore score=null;
							if(statusSpecificScore.getStatusSpecificQuestions()!=null){
								if(statusSpecificScore.getSkillAttributesMaster()!=null){
									score= questionTxtMap.get(statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+statusSpecificScore.getSkillAttributesMaster().getSkillAttributeId());
								}else{
									score= questionTxtMap.get(statusSpecificScore.getStatusSpecificQuestions().getQuestion());
								}
							}else{
								score= questionMap.get(statusSpecificScore.getTeacherAnswerDetail().getAnswerId());
							}
							if(statusSpecificScore.getJobOrder().getJobId().equals(jobOrder.getJobId())){
								if(score!=null){
									String ssfValue="";
									if(score.getStatusSpecificQuestions()!=null){
										if(score.getSkillAttributesMaster()!=null){
											ssfValue=score.getMaxScore()+"##"+score.getStatusSpecificQuestions().getQuestion()+"##"+score.getSkillAttributesMaster().getSkillAttributeId()+"##"+jobOrder.getJobId();
										}else{
											ssfValue=score.getMaxScore()+"##"+score.getStatusSpecificQuestions().getQuestion()+"##"+jobOrder.getJobId();
										}
									}else{
										ssfValue=score.getMaxScore()+"##"+score.getTeacherAnswerDetail().getAnswerId()+"##"+jobOrder.getJobId();
									}
									//System.out.println("ssfCValue::::>>>>>>>>>>>>5th:::::::::::::::::::::::::><"+ssfValue);
									if(ssfMapfoCategory.get(ssfValue)!=null){
										//inputSumAnswerScore=inputSumAnswerScore+statusSpecificScore.getScoreProvided();
										//inputSumAnswerMaxScore=inputSumAnswerMaxScore+statusSpecificScore.getMaxScore();
									}else{
										sameQuestion=false;
									}
								}else{
									sameQuestion=false; 
								}
							}

						}
					}
				}
				//System.out.println("sameQuestion::::::::::2>>>>>>"+sameQuestion);
				if(sameQuestion){

					//Override
					if(statusHistory!=null && finalizeStatusFlag)
					{
						if(chkOverridetSatus){
							statusHistory.setOverride(chkOverridetSatus);
							statusHistory.setOverrideBy(userMaster);
							debugPrintln("Override more ... not entry first time");
						}
						try {
							teacherStatusHistoryForJobDAO.makePersistent(statusHistory);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					for (Entry<Integer,StatusSpecificScore> entry : questionMap.entrySet()) {
						StatusSpecificScore statusSpecificScore=entry.getValue();
						String ssfValue="";
						if(jobCategoryMaster.equals(jobOrder.getJobCategoryMaster())){
							if(statusSpecificScore.getStatusSpecificQuestions()!=null){
								ssfValue=statusSpecificScore.getStatusSpecificQuestions().getQuestionId()+"##"+jobOrder.getJobId();
							}else{
								ssfValue=statusSpecificScore.getTeacherAnswerDetail().getAnswerId()+"##"+jobOrder.getJobId();
							}
							//System.out.println("ssfValue:::::::::::::::::::::::::::::3rd::::::::::::::"+ssfValue);
							if(ssfMap.get(ssfValue)!=null){
								StatusSpecificScore statusSpecificScoreObj=ssfMap.get(ssfValue);
								statusSpecificScoreObj.setFinalizeStatus(Integer.parseInt(finalizeStatus));
								statusSpecificScoreObj.setScoreProvided(statusSpecificScore.getScoreProvided());
								statusSpecificScoreDAO.makePersistent(statusSpecificScoreObj);
								//statusSpecificScoreDAO.flush();
								//statusSpecificScoreDAO.clear();
							}
						}else{
							if(statusSpecificScore.getStatusSpecificQuestions()!=null){
								if(statusSpecificScore.getSkillAttributesMaster()!=null){
									ssfValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+statusSpecificScore.getSkillAttributesMaster().getSkillAttributeId()+"##"+jobOrder.getJobId();
								}else{
									ssfValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+jobOrder.getJobId();
								}
							}else{
								ssfValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getTeacherAnswerDetail().getAnswerId()+"##"+jobOrder.getJobId();
							}
							//System.out.println("ssfCValue:::::::::::::::::::::::::::::6th::::::::::::::"+ssfValue);
							if(ssfMapfoCategory.get(ssfValue)!=null){
								StatusSpecificScore statusSpecificScoreObj=ssfMapfoCategory.get(ssfValue);
								statusSpecificScoreObj.setFinalizeStatus(Integer.parseInt(finalizeStatus));
								statusSpecificScoreObj.setScoreProvided(statusSpecificScore.getScoreProvided());
								statusSpecificScoreDAO.makePersistent(statusSpecificScoreObj);
								//statusSpecificScoreDAO.flush();
								//statusSpecificScoreDAO.clear();
							}
						}
					}
				}
			}
			// End Update Answer Score
			//System.out.println(jobOrder.getJobId()+":::::::::::::::sameQuestion:::::"+sameQuestion);


			if(sameQuestion){

				boolean  jobWiseScoreFlag=false;
				List<TeacherStatusScores> teacherStatusScoresList=new ArrayList<TeacherStatusScores>();
				List<TeacherStatusNotes> teacherStatusNotesList=new ArrayList<TeacherStatusNotes>();
				try{
					if(sameQuestion && !bPanel)
						if(outerJob==false && ( scoreProvided!=null && !scoreProvided.equals("0") && !scoreProvided.equals("")) || (inputSumAnswerScore>0)){
							int fStatus=0;
							teacherStatusScoresList= teacherStatusScoresDAO.getStatusScore(teacherDetail,jobOrder,statusMaster,secondaryStatus,userMaster);
							TeacherStatusScores statusScores = new TeacherStatusScores();
							if(teacherStatusScoresList!=null && teacherStatusScoresList.size()> 0)
							{
								statusScores =teacherStatusScoresList.get(0);
								if(statusScores.isFinalizeStatus())
									fStatus=1;
							}
							if(fStatus==0 || jobCategoryFlagValue==1){
								statusScores.setTeacherDetail(teacherDetail);
								statusScores.setUserMaster(userMaster);
								statusScores.setJobOrder(jobOrder);
								if(districtMaster!=null){
									statusScores.setDistrictId(districtMaster.getDistrictId());
								}
								if(statusMaster!=null){
									statusScores.setStatusMaster(statusMaster);
								}
								if(secondaryStatus!=null){
									statusScores.setSecondaryStatus(secondaryStatus);
								}

								if(schoolMaster!=null){
									statusScores.setSchoolId(schoolMaster.getSchoolId());	
								}
								int emailSentTo=0;
								if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
									emailSentTo=2;
								}
								if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
									emailSentTo=1;
								}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
									emailSentTo=3;
								}
								statusScores.setEmailSentTo(emailSentTo);

								if(inputSumAnswerScore>0)
								{
									statusScores.setScoreProvided(inputSumAnswerScore);
								}
								else{
									if(scoreProvided!=null){
										statusScores.setScoreProvided(Integer.parseInt(scoreProvided));
									}
								}

								if(inputSumAnswerScore>0)
								{
									statusScores.setMaxScore(inputSumAnswerMaxScore);
								}
								else
								{
									if(fitScore!=null){
										statusScores.setMaxScore(Integer.parseInt(fitScore));
									}
								}

								if(finalizeStatus!=null){
									if(!finalizeStatus.equals("0")){
										statusScores.setFinalizeStatus(true);
										jobWiseScoreFlag=true;
									}
								}
								teacherStatusScoresDAO.makePersistent(statusScores);
							}
						}
				}catch(Exception e){
					e.printStackTrace();
				}
				// Start ... Panel

				if(!bPanel){
					if(jobOrder.getDistrictMaster()!=null && statusMaster!=null && (jobWiseScoreFlag || finalizeStatusFlag ) && (statusMaster.getStatusShortName().equalsIgnoreCase("scomp") || statusMaster.getStatusShortName().equalsIgnoreCase("ecomp") || statusMaster.getStatusShortName().equalsIgnoreCase("vcomp"))){
						if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()!=null){
							if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()){
								String accessDPoints="";
								if(jobOrder.getDistrictMaster().getAccessDPoints()!=null)
									accessDPoints=jobOrder.getDistrictMaster().getAccessDPoints();

								if(accessDPoints.contains(""+statusMaster.getStatusId())){
									List<TeacherStatusHistoryForJob> lstTSHJob =teacherStatusHistoryForJobDAO.findByTeacherStatusHistorySelectedStatus(teacherDetail,jobOrder);
									List<SecondaryStatus> listSecondaryStatus = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithStatusAndHBD(jobOrder,statusMaster);
									int isInternalCandidate=jobForTeacherObj.getIsAffilated()==0?3:2;
									Map<Integer,SecondaryStatus> secMap=new HashMap<Integer, SecondaryStatus>();
									for(TeacherStatusHistoryForJob tSHObj : lstTSHJob){
										if(tSHObj.getSecondaryStatus()!=null)
											secMap.put(tSHObj.getSecondaryStatus().getSecondaryStatusId(),tSHObj.getSecondaryStatus());
									}
									if(listSecondaryStatus.size()>0){
										List<SecondaryStatus> lstSecondaryStatus=listSecondaryStatus.get(0).getChildren();
										List<JobCategoryWiseStatusPrivilege> lstJCWSP= jobCategoryWiseStatusPrivilegeDAO.getStatus(jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster(),lstSecondaryStatus);
										List<SecondaryStatus> lstSS=new ArrayList<SecondaryStatus> ();
										for(JobCategoryWiseStatusPrivilege jcwspObj:lstJCWSP){
											if(jcwspObj.getInternalExternalOrBothCandidate()!=null){
											if(jcwspObj.getInternalExternalOrBothCandidate()==2 && isInternalCandidate!=2)
												lstSS.add(jcwspObj.getSecondaryStatus());
											else if(jcwspObj.getInternalExternalOrBothCandidate()==3 && isInternalCandidate!=3)
												lstSS.add(jcwspObj.getSecondaryStatus());
											}else{
												lstSS.add(jcwspObj.getSecondaryStatus());
											}
										}
										lstSecondaryStatus.removeAll(lstSS);
										int counter=0;
										for(SecondaryStatus  obj : listSecondaryStatus.get(0).getChildren()){
											if(obj.getStatus().equalsIgnoreCase("A") && obj.getStatusMaster()==null){
												SecondaryStatus sec=secMap.get(obj.getSecondaryStatusId());
												if(sec==null){
													boolean isJobAssessment=false;
													if(jobOrder.getIsJobAssessment()!=null){
														if(jobOrder.getIsJobAssessment()){
															isJobAssessment=true;
														}
													}
													if((isJobAssessment && obj.getSecondaryStatusName().equalsIgnoreCase("JSI"))|| !obj.getSecondaryStatusName().equalsIgnoreCase("JSI")){
														if(counter==1){
															secondaryStatusNames+=", ";
														}
														counter=0;
														secondaryStatusNames+=obj.getSecondaryStatusName();
														counter++;
													}
												}
											}
										}
									}
									if(!secondaryStatusNames.equals("")){
										errorFlag="5";
									}
					//********************************************End TPL 2024 (Internal And External Candidate) ***************************************************//
								}
							}
						}
					}

					if(!errorFlag.equals("5")){

						teacherStatusScoresList= teacherStatusScoresDAO.getFinalizeStatusScore(teacherDetail,jobOrder,statusMaster,secondaryStatus);
						teacherStatusNotesList= teacherStatusNotesDAO.getFinalizeStatusScore(teacherDetail,jobOrder,statusMaster,secondaryStatus);

						Map<Integer,Integer> noteScoreMap= new HashMap<Integer, Integer>();
						try{
							for (TeacherStatusScores teacherStatusScores: teacherStatusScoresList) {
								int count =0;
								try{
									count=noteScoreMap.get(teacherStatusScores.getUserMaster().getUserId());
								}catch(Exception e){

								}
								if(count==0){
									noteScoreMap.put(teacherStatusScores.getUserMaster().getUserId(), 1);
								}else{
									noteScoreMap.put(teacherStatusScores.getUserMaster().getUserId(), 1+count);
								}
							}
							for (TeacherStatusNotes teacherStatusNotes: teacherStatusNotesList) {
								int count =0;
								try{
									count=noteScoreMap.get(teacherStatusNotes.getUserMaster().getUserId());
								}catch(Exception e){

								}
								if(count==0){
									noteScoreMap.put(teacherStatusNotes.getUserMaster().getUserId(), 1);
								}else{
									noteScoreMap.put(teacherStatusNotes.getUserMaster().getUserId(), 1+count);
								}

							}
						}catch(Exception e){}

						TeacherStatusHistoryForJob canNotHireObj= new TeacherStatusHistoryForJob();
						boolean noInsert=true;
						if(statusMaster!=null && (statusMaster.getStatusShortName().equals("hird")|| statusMaster.getStatusShortName().equals("rem")|| statusMaster.getStatusShortName().equals("dcln"))){
							noInsert=false;
						}
						if(outerJob==false && noInsert && (selectedstatus|| selectedSecondaryStatus) && (jobWiseScoreFlag || finalizeStatusFlag)){
							try{
								Calendar c = Calendar.getInstance();
								c.add(Calendar.SECOND,01);
								Date date = c.getTime();
								TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
								tSHJ.setTeacherDetail(teacherDetail);
								tSHJ.setJobOrder(jobOrder);
								if(statusMaster!=null){
									tSHJ.setStatusMaster(statusMaster);
								}
								if(secondaryStatus!=null){
									tSHJ.setSecondaryStatus(secondaryStatus);
									SendEmailOfficialTranscripts(secondaryStatus,teacherDetail,jobOrder);
								}
								tSHJ.setStatus("S");
								tSHJ.setCreatedDateTime(date);
								tSHJ.setUserMaster(userMaster);
								if(chkOverridetSatus){
									tSHJ.setOverride(chkOverridetSatus);
									tSHJ.setOverrideBy(userMaster);
									debugPrintln("Override entry first time");
								}
								teacherStatusHistoryForJobDAO.makePersistent(tSHJ);

							}catch(Exception e){
								e.printStackTrace();
							}
							try{
								if(jobForTeacherObj!=null &&secondaryStatus!=null){
									if(!bPanel)
									{
										jobForTeacher.setSecondaryStatus(secondaryStatus);
										jobForTeacher.setStatusMaster(null);
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(secondaryStatus.getSecondaryStatusName());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);
										jobForTeacherDAO.makePersistent(jobForTeacher);
										//System.out.println("W 01");
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}else if(outerJob==false && statusHistory==null && noInsert && (selectedstatus==false|| selectedSecondaryStatus==false) && (jobWiseScoreFlag || finalizeStatusFlag)){
							try{
								Calendar c = Calendar.getInstance();
								c.add(Calendar.SECOND,01);
								Date date = c.getTime();
								TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
								tSHJ.setTeacherDetail(teacherDetail);
								tSHJ.setJobOrder(jobOrder);
								if(statusMaster!=null){
									tSHJ.setStatusMaster(statusMaster);
								}
								if(secondaryStatus!=null){
									tSHJ.setSecondaryStatus(secondaryStatus);
									SendEmailOfficialTranscripts(secondaryStatus,teacherDetail,jobOrder);
								}
								tSHJ.setStatus("S");
								tSHJ.setCreatedDateTime(date);
								tSHJ.setUserMaster(userMaster);
								if(chkOverridetSatus){
									tSHJ.setOverride(chkOverridetSatus);
									tSHJ.setOverrideBy(userMaster);
									debugPrintln("Override entry first time");
								}
								teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
								if(secondaryStatus!=null){
									if(jobForTeacherObj.getSecondaryStatus()!=null){
										if(cgService.selectedNotPriSecondaryStatusCheck(secondaryStatus,jobForTeacherObj.getSecondaryStatus())){
											try{
												if(jobForTeacher!=null && secondaryStatus!=null){
													if(!bPanel)
													{
														jobForTeacher.setSecondaryStatus(secondaryStatus);
														jobForTeacher.setUpdatedBy(userMaster);
														jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
														jobForTeacher.setUpdatedDate(new Date());
														jobForTeacher.setLastActivity(secondaryStatus.getSecondaryStatusName());
														jobForTeacher.setLastActivityDate(new Date());
														jobForTeacher.setUserMaster(userMaster);
														jobForTeacherDAO.makePersistent(jobForTeacher);
														//System.out.println("W 02");
													}
												}
											}catch(Exception e){
												e.printStackTrace();
											}
										}
									}else{
										try{
											if(jobForTeacher!=null && secondaryStatus!=null){
												if(!bPanel)
												{
													jobForTeacher.setSecondaryStatus(secondaryStatus);
													jobForTeacher.setUpdatedBy(userMaster);
													jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
													jobForTeacher.setUpdatedDate(new Date());
													jobForTeacher.setLastActivity(secondaryStatus.getSecondaryStatusName());
													jobForTeacher.setLastActivityDate(new Date());
													jobForTeacher.setUserMaster(userMaster);
													jobForTeacherDAO.makePersistent(jobForTeacher);
													//System.out.println("W 03");
												}
											}
										}catch(Exception e){
											e.printStackTrace();
										}
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}

						if(inputSumAnswerScore>0){
							scoreProvided=""+inputSumAnswerScore;
							fitScore=""+inputSumAnswerMaxScore;
						}

						if(outerJob==false && jobWiseScoreFlag ){
							int jobWiseConsolidatedScore=0;
							int jobWiseMaxScore=0;

							try
							{
								Double[] teacherStatusScoresAvg= teacherStatusScoresDAO.getAllFinalizeStatusScoreAvg(teacherDetail,jobOrder);
								jobWiseConsolidatedScore=Integer.parseInt(Math.round(teacherStatusScoresAvg[0])+"");
								jobWiseMaxScore=Integer.parseInt(Math.round(teacherStatusScoresAvg[1])+"");
							}
							catch(Exception e)
							{ 
								e.printStackTrace();
							}

							//System.out.println("JID "+jobOrder.getJobId()+" TID "+teacherDetail.getTeacherId() +" Score "+jobWiseConsolidatedScore +" Max "+jobWiseMaxScore);
							try
							{
								if(jobWiseConsolidatedScore > 0 && jobWiseMaxScore > 0)
								{
									JobWiseConsolidatedTeacherScore jWScore=jobWiseConsolidatedTeacherScoreDAO.getJWCTScore(teacherDetail, jobOrder);
									if(jWScore==null)
									{
										jWScore=new JobWiseConsolidatedTeacherScore();
										jWScore.setTeacherDetail(teacherDetail);
										jWScore.setJobOrder(jobOrder);
										jWScore.setJobWiseConsolidatedScore(jobWiseConsolidatedScore);
										jWScore.setJobWiseMaxScore(jobWiseMaxScore);
										jobWiseConsolidatedTeacherScoreDAO.makePersistent(jWScore);
									}
									else
									{
										jWScore.setJobWiseConsolidatedScore(jobWiseConsolidatedScore);
										jWScore.setJobWiseMaxScore(jobWiseMaxScore);
										jobWiseConsolidatedTeacherScoreDAO.makePersistent(jWScore);
									}
								}
							}
							catch(Exception e)
							{ 
								e.printStackTrace();
							}
						}
						List<SchoolMaster> lstSchoolMasters = null;

						if(statusUpdateHDR==null && (jobWiseScoreFlag || finalizeStatusFlag)){
							errorFlag="3";
						}
						/*********End Update Status ************/
						if(outerJob==false && statusUpdateHDR==null && isUpdateStatus && selectedstatus){
							if(outerJob==false && selectedstatus ){
								if(statusMaster!=null && statusMaster.getStatusShortName().equals("scomp")&& (jobWiseScoreFlag || finalizeStatusFlag)){
									String statuss ="|hird|dcln|rem|vcomp|ecomp|";
									if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())&& !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
										if(!bPanel)
										{
											jobForTeacher.setStatus(statusMaster);
											jobForTeacher.setStatusMaster(statusMaster);
											jobForTeacher.setUpdatedBy(userMaster);
											jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
											jobForTeacher.setUpdatedDate(new Date());
											jobForTeacher.setLastActivity(statusMaster.getStatus());
											jobForTeacher.setLastActivityDate(new Date());
											jobForTeacher.setUserMaster(userMaster);
											jobForTeacherDAO.makePersistent(jobForTeacher);
											//System.out.println("scomp 01");
										}
										errorFlag="3";
										statusShortName="soth";
									}
								}else if(statusMaster!=null && statusMaster.getStatusShortName().equals("ecomp")&& (jobWiseScoreFlag || finalizeStatusFlag)){
									String statuss = "|hird|dcln|rem|vcomp|";

									if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())&& !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
										if(!bPanel)
										{
											jobForTeacher.setStatus(statusMaster);
											jobForTeacher.setStatusMaster(statusMaster);
											jobForTeacher.setUpdatedBy(userMaster);
											jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
											jobForTeacher.setUpdatedDate(new Date());
											jobForTeacher.setLastActivity(statusMaster.getStatus());
											jobForTeacher.setLastActivityDate(new Date());
											jobForTeacher.setUserMaster(userMaster);
											jobForTeacherDAO.makePersistent(jobForTeacher);
											//System.out.println("ecomp 01");
										}
										errorFlag="3";
										statusShortName="soth";
									}
								}else if(statusMaster!=null && statusMaster.getStatusShortName().equals("vcomp")&& (jobWiseScoreFlag || finalizeStatusFlag)){
									//System.out.println("::::::::::::::::::::::VCOM::::::::::::::::::::::::");
									String statuss = "|hird|dcln|rem|";
									if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId()) && !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
										if(!bPanel)
										{
											jobForTeacher.setStatus(statusMaster);
											jobForTeacher.setStatusMaster(statusMaster);
											jobForTeacher.setUpdatedBy(userMaster);
											jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
											jobForTeacher.setUpdatedDate(new Date());
											jobForTeacher.setLastActivity(statusMaster.getStatus());
											jobForTeacher.setLastActivityDate(new Date());
											jobForTeacher.setUserMaster(userMaster);

											jobForTeacherDAO.makePersistent(jobForTeacher);
											//System.out.println("vcomp 01");
										}
										errorFlag="3";
										statusShortName="soth";
									}
								}
							}
						}
						if(outerJob==false && isMiami && statusMasterPanel!=null && statusMasterPanel.getStatusShortName().equals("vcomp")&& (jobWiseScoreFlag || finalizeStatusFlag)){
							//System.out.println("::::::::::::::::::::::VCOM 3rdndddddd::::::::::::::::::::::::");
							UserMaster schoolUser=null;
							try{
								List<JobWisePanelStatus> jobWiseList=new ArrayList<JobWisePanelStatus>();
								SecondaryStatus  secondaryStatusObj =secondaryStatusDAO.findSecondaryStatusByJobOrder(jobForTeacher.getJobId(),"Offer Ready");
								jobWiseList=jobWisePanelStatusDAO.getPanel(jobOrder,secondaryStatusObj,null);

								//System.out.println("jobWiseList:::::>>>>>>>>>>>>>>>>>:"+jobWiseList.size());
								List<PanelSchedule> panelSList= new ArrayList<PanelSchedule>();
								if(jobWiseList.size() == 1){
									panelSList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWiseList,teacherDetail);
									if(panelSList.size()==1){
										PanelSchedule panelSchedule=null;
										if(panelSList.get(0)!=null){
											panelSchedule=panelSList.get(0);
											List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
											panelAttendees=panelAttendeesDAO.getPanelAttendees(panelSchedule);
											if(panelAttendees.size() > 0){
												for(PanelAttendees attendees:panelAttendees){
													if(attendees!=null){
														if(attendees.getPanelInviteeId().getEntityType()==3){
															schoolUser=attendees.getPanelInviteeId();
														}
													}
												}
											}

										}
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}								
							try{
								String statuss = "|hird|dcln|rem|";
								if(!jobForTeacher.getStatus().getStatusId().equals(statusMasterPanel.getStatusId()) && !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
									if(!bPanel)
									{
										jobForTeacher.setStatus(statusMasterPanel);
										jobForTeacher.setStatusMaster(statusMasterPanel);
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMaster.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);

										jobForTeacherDAO.makePersistent(jobForTeacher);
									}
								}
							}catch(Exception e){

							}
						}
					}
				}
			}/////True Case

		} catch (Exception e) {
			e.printStackTrace();
		}
		return secondaryStatusNames;
	}	

	public void mailSendToUserByThread(List<UserMailSend> userList,String[] arrHrDetail,HttpServletRequest request){

		if(request==null)
		{
			WebContext context;
			context = WebContextFactory.get();
			request = context.getHttpServletRequest();
		}

		MailSendToUser mst =new MailSendToUser(userList,arrHrDetail,IPAddressUtility.getIpAddress(request));
		Thread currentThread = new Thread(mst);
		currentThread.start(); 
	}

	public class MailSendToUser  extends Thread{

		List<UserMailSend> userListTrdVal=new ArrayList<UserMailSend>();
		String[] arrHrDetailTrdVal = new String[15];
		String ipAddress;
		int count=1;

		public MailSendToUser(List<UserMailSend> userList,String[] arrHrDetail,String ipAddress){
			userListTrdVal=userList;
			arrHrDetailTrdVal=arrHrDetail;
			this.ipAddress=ipAddress;
		}
		public void run() {
			try {
				Thread.sleep(1000);
				int i =0;
				System.out.println("::Trd size::"+userListTrdVal.size());
				for(UserMailSend userMailSend :userListTrdVal){
					int internal=0;
					String emailTrdVal="";
					String fromUserNameTrdVal=""; 
					String statusShortNameTrdVal="";  
					String statusNameTrdVal="";     
					String teacherNameTrdVal="";
					String statusNoteTrdVal="";
					String jobTitleTrdVal="";
					String gridURLTrdVal="";
					String offerDeclineURLTrdVal="";
					String offerAcceptURLTrdVal="";
					String schoolLocation="";
					String locationCode="";
					String schoolEmailTrdVal=null;
					String bccEmailTrdVal[]=null;
					UserMaster userMasterTrdVal=null;
					EmailerService emailerServiceOther = null;
					boolean panelNotExist=false;

					try
					{
						if(userMailSend.getLocationCode()!=null && !userMailSend.getLocationCode().equals(""))
						{
							locationCode = " ("+userMailSend.getLocationCode()+")";
						}
					}catch(Exception e)
					{
						e.printStackTrace();
					}
					try{
						if(userMailSend.getPanelExist()!=null && userMailSend.getPanelExist().equals("0")){
							panelNotExist=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}


					if(userMailSend.getEmail()!=null){
						emailTrdVal=userMailSend.getEmail();
					}
					if(userMailSend.getFromUserName()!=null){
						fromUserNameTrdVal=userMailSend.getFromUserName();
					}
					if(userMailSend.getStatusNote()!=null){
						statusNoteTrdVal=userMailSend.getStatusNote();
					}
					if(userMailSend.getTeacherName()!=null){
						teacherNameTrdVal=userMailSend.getTeacherName();
					}
					if(userMailSend.getJobTitle()!=null){
						jobTitleTrdVal=userMailSend.getJobTitle();
					}
					if(userMailSend.getStatusShortName()!=null){
						statusShortNameTrdVal=userMailSend.getStatusShortName();
					}
					if(userMailSend.getStatusName()!=null){
						statusNameTrdVal=userMailSend.getStatusName();
					}
					if(userMailSend.getGridURL()!=null){
						gridURLTrdVal=userMailSend.getGridURL();
					}
					if(userMailSend.getUserMaster()!=null){
						userMasterTrdVal=userMailSend.getUserMaster();
					}

					if(userMailSend.getOfferAcceptURL()!=null){
						offerAcceptURLTrdVal=userMailSend.getOfferAcceptURL();
					}
					if(userMailSend.getOfferDeclineURL()!=null){
						offerDeclineURLTrdVal=userMailSend.getOfferDeclineURL();
					}
					if(userMailSend.getSchoolLocation()!=null){
						schoolLocation=userMailSend.getSchoolLocation()+locationCode;
					}
					try{
						if(userMailSend.getBccEmail()!=null){
							bccEmailTrdVal=userMailSend.getBccEmail();
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(userMailSend.getInternal()!=null){
							internal=userMailSend.getInternal();
						}
					}catch(Exception e){
						e.printStackTrace();
					}



					String emailBodyText="";
					if(userMailSend.getIsUserOrTeacherFlag()==1){ // DA SA
						emailBodyText=userMailSend.getMailBody();
						System.out.println("Mail subject: "+ userMailSend.getSubject());
						System.out.println("Mail send: "+emailTrdVal);
						System.out.println("Mail body: "+emailBodyText);
						i++;
						emailerService.sendMailAsHTMLText(emailTrdVal, userMailSend.getSubject(),emailBodyText);
						/* ====== Saving  Message to Teacher during Mail send with Resource from where it come =================*/
						MessageToTeacher  messageToTeacher= new MessageToTeacher();
						messageToTeacher.setTeacherId(userMailSend.getTeacherDetail());
						messageToTeacher.setJobId(userMailSend.getJobOrder());
						messageToTeacher.setTeacherEmailAddress(emailTrdVal);
						messageToTeacher.setMessageSubject(userMailSend.getSubject());
						messageToTeacher.setMessageSend(emailBodyText);
						messageToTeacher.setMessageSource("cgstatus");
						messageToTeacher.setSenderId(userMailSend.getUserMaster());
						messageToTeacher.setSenderEmailAddress(userMailSend.getUserMaster().getEmailAddress());
						messageToTeacher.setEntityType(userMailSend.getUserMaster().getEntityType());
						messageToTeacher.setIpAddress(ipAddress);
						messageToTeacherDAO.makePersistent(messageToTeacher);
					}else if(userMailSend.getIsUserOrTeacherFlag()==4){ // For Teacher Email 
						System.out.println(":::::::::::::::::::Mail Send Teacher::::::::::::::::::"); 
						if(userMailSend.getIsEmailTemplateChangedTeacher()==1)
							emailBodyText=userMailSend.getMailBody();
						else
							emailBodyText=MailText.statusNoteSendToCandidate(userMasterTrdVal,gridURLTrdVal,fromUserNameTrdVal,statusShortNameTrdVal,statusNameTrdVal,teacherNameTrdVal,jobTitleTrdVal,arrHrDetailTrdVal);

						//System.out.println("vishwanath : ::::::::::: emailBodyText:: "+emailBodyText);

						emailerService.sendMailAsHTMLText(emailTrdVal, userMailSend.getSubject(),emailBodyText);
						/* ====== Saving  Message to Teacher during Mail send with Resource from where it come =================*/
						MessageToTeacher  messageToTeacher= new MessageToTeacher();
						messageToTeacher.setTeacherId(userMailSend.getTeacherDetail());
						messageToTeacher.setJobId(userMailSend.getJobOrder());
						messageToTeacher.setTeacherEmailAddress(emailTrdVal);
						messageToTeacher.setMessageSubject(userMailSend.getSubject());
						messageToTeacher.setMessageSend(emailBodyText);
						messageToTeacher.setMessageSource("cgstatus");
						messageToTeacher.setSenderId(userMailSend.getUserMaster());
						messageToTeacher.setSenderEmailAddress(userMailSend.getUserMaster().getEmailAddress());
						messageToTeacher.setEntityType(userMailSend.getUserMaster().getEntityType());
						messageToTeacher.setIpAddress(ipAddress);
						messageToTeacherDAO.makePersistent(messageToTeacher);
					}else if(userMailSend.getIsUserOrTeacherFlag()==5){
						System.out.println(":::::::::::::::::::::::::Offer Letter Send ::::::::::::::::::"+panelNotExist);
						if(userMailSend.getJobOrder().getDistrictMaster()!=null && userMailSend.getJobOrder().getDistrictMaster().getDistrictId().equals(1200390) && userMailSend.getJobOrder().getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Adult Education (Vocational)")){
							  
							  System.out.println("Offer Ready Finalize For Miami Success");
							  
							  List<String> ccEmailAddress	=	new ArrayList<String>();
							  List<String> bccEmailAddress	=	new ArrayList<String>();
							  ccEmailAddress.add("DCARMONA1@DADESCHOOLS.NET");
							  ccEmailAddress.add("ADIAZ4@DADESCHOOLS.NET");
							  bccEmailAddress.add("hanzala@netsutra.com");
							  /*
							   * 	Find Principal
							   * */
							  List<PanelSchedule> panelScheduleList		= 	new ArrayList<PanelSchedule>();
							  List<PanelAttendees> PanelAttendeesList	= 	new ArrayList<PanelAttendees>();
							  try{
								  Criterion criterion = Restrictions.eq("teacherDetail",userMailSend.getTeacherDetail());
								  Criterion criterion1 = Restrictions.eq("jobOrder", userMailSend.getJobOrder());
								  Criterion criterion2 = Restrictions.eq("districtId", userMailSend.getJobOrder().getDistrictMaster().getDistrictId());
								  Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
								  
								  panelScheduleList 	= panelScheduleDAO.findByCriteria(criterion,criterion1,criterion2,criterion3);
								  if(panelScheduleList.size()>0 && panelScheduleList!=null){
									  
									  Integer panelId = panelScheduleList.get(0).getPanelId();
									  PanelSchedule panelSchedule = panelScheduleDAO.findById(panelId, false, false); 
									  Criterion criterion4 = Restrictions.eq("panelSchedule", panelSchedule);
									  PanelAttendeesList = panelAttendeesDAO.findByCriteria(criterion4);
									  
									  for(PanelAttendees panelAtten : PanelAttendeesList){
										  if(panelAtten.getPanelInviteeId().getEntityType().equals(3)){
											  ccEmailAddress.add(panelAtten.getPanelInviteeId().getEmailAddress());
										  }
									  }
								  }
							  }catch(Exception exception){
								  exception.printStackTrace();
							  }
							  String mailContent	=	MailText.getOfferReadyByStaffer(userMailSend.getTeacherDetail(),jobTitleTrdVal,schoolLocation);
							 //String mailSubject	=	userMailSend.getSubject();
							  String mailSubject	=	jobTitleTrdVal+" Offer Letter";
							  String mailTo 		= 	emailTrdVal;
							  System.out.println(mailSubject);
							  System.out.println(mailContent);
							  if(ccEmailAddress!=null && ccEmailAddress.size() > 0){
									emailerService.sendMailAsHTMLByListToCc(mailTo,bccEmailAddress,ccEmailAddress,mailSubject,mailContent);
								}
						  } else {
							JobOrder jobOrder = userMailSend.getJobOrder();
							emailBodyText=MailText.getSecondOfferLetter(userMailSend.getTeacherDetail(),jobTitleTrdVal,schoolLocation,offerAcceptURLTrdVal,offerDeclineURLTrdVal,panelNotExist);
							System.out.println("emailBodyText::"+emailBodyText);
							System.out.println("emailTrdVal::"+emailTrdVal);
							System.out.println("subject:"+userMailSend.getSubject());
							emailerService.sendMailAsHTMLTextWithBCC(emailTrdVal, userMailSend.getSubject(),emailBodyText,null,"Offer"); 
						  }
					}else if(userMailSend.getIsUserOrTeacherFlag()==6){
						System.out.println(internal+":::::::::::::::::::::::::Offer Letter Next Step Send ::::::::::::::::::");
						if(internal==1){
							emailBodyText=MailText.getThirdOfferNextStepForInternal(internal,userMailSend.getTeacherDetail(),jobTitleTrdVal,schoolLocation);
						}else{
							emailBodyText=MailText.getThirdOfferNextStep(userMailSend.getTeacherDetail(),jobTitleTrdVal,schoolLocation);
						}
						System.out.println("emailTrdVal::"+emailTrdVal);
						System.out.println("emailBodyText: "+emailBodyText);
						System.out.println("emailerService : "+emailerService);
						System.out.println("bccEmailTrdVal : "+bccEmailTrdVal);

						EmailerService  emailerService1 =userMailSend.getEmailService();
						if(emailerService1!=null)
							emailerService = emailerService1;

						emailerService.sendMailAsHTMLTextWithBCC(emailTrdVal, userMailSend.getSubject(),emailBodyText,bccEmailTrdVal,null);
					}else if(userMailSend.getIsUserOrTeacherFlag()==7){
						System.out.println(":::::::::::::::::::::::::Offer Accept Mail Send To Dennis And Julio ::::::::::::::::::");
						TeacherDetail teacherDetail = userMailSend.getTeacherDetail();
						emailBodyText=MailText.offerAcceptMailToKeyContact(teacherDetail, schoolLocation, jobTitleTrdVal);

						System.out.println("emailBodyText: "+emailBodyText);
						System.out.println("emailerService : "+emailerService);						

						EmailerService  emailerService1 =userMailSend.getEmailService();
						if(emailerService1!=null)
							emailerService = emailerService1;

						try{
							String[] breakEmail = emailTrdVal.split(",");
							for (String stringEmail : breakEmail) {
								System.out.println("stringEmail:::::::::"+stringEmail);
								emailerService.sendMailAsHTMLText(stringEmail, userMailSend.getSubject(), emailBodyText);
							}
						}catch(Exception e){

						}

					}
					//System.out.println("\n @@@@@@@@@@@@@@@@@			mailSendToUserByThread			@@@@@@@@@@@@@@@@@@@		count == "+count);
					++count;
				}
			}catch (NullPointerException en) {
				en.printStackTrace();
			}catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	// Email for Panel Member

	public void mailSendToPanelUserByThread(List<UserMailSend> userList){
		MailSendToPanelUser mst =new MailSendToPanelUser(userList);
		Thread currentThread = new Thread(mst);
		currentThread.start(); 
	}

	public class MailSendToPanelUser  extends Thread{

		List<UserMailSend> userPanelList=new ArrayList<UserMailSend>();
		public MailSendToPanelUser(List<UserMailSend> userList)
		{
			userPanelList=userList;
		}
		public void run() 
		{
			try 
			{
				Thread.sleep(1000);
				System.out.println("UserPanelList Size :: "+userPanelList.size());
				int iCount=0;
				int iTotalCount=userPanelList.size();
				for(UserMailSend userMailSend :userPanelList)
				{
					iCount++;
					String toEmail="";
					String sEmailSubject="";
					String sPanelUserName="";
					String sPanelUserNameTo="";
					String sTeacheName="";
					String sJobTitle="";
					String sStatusName="";
					String requisitionNumber="";
					String locationCode="";

					int offerReadyMailFlag=0;
					int flag=0;
					String schoolLocation="";
					UserMaster userMaster=null;

					try
					{
						//System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>>>>> userMailSend.getLocationCode() <<<<<<<<<<<<<<<< "+userMailSend.getLocationCode());

						if(userMailSend.getLocationCode()!=null && !userMailSend.getLocationCode().equals(""))
						{
							locationCode = " ("+userMailSend.getLocationCode()+")";
						}
					}catch(Exception e)
					{
						e.printStackTrace();
					}


					if(userMailSend.getEmail()!=null){
						toEmail=userMailSend.getEmail();
					}

					if(userMailSend.getSubject()!=null){
						sEmailSubject=userMailSend.getSubject();
					}

					if(userMailSend.getFromUserName()!=null){
						sPanelUserName=userMailSend.getFromUserName();
					}

					if(userMailSend.getToUserName()!=null){
						sPanelUserNameTo=userMailSend.getToUserName();
					}

					if(userMailSend.getTeacherName()!=null){
						sTeacheName=userMailSend.getTeacherName();
					}

					if(userMailSend.getJobTitle()!=null){
						sJobTitle=userMailSend.getJobTitle();
					}

					if(userMailSend.getStatusName()!=null){
						sStatusName=userMailSend.getStatusName();
					}

					if(userMailSend.getUserMaster()!=null){
						userMaster=userMailSend.getUserMaster();
					}

					if(userMailSend.getIsUserOrTeacherFlag()!=0){
						offerReadyMailFlag=userMailSend.getIsUserOrTeacherFlag();
					}
					if(userMailSend.getSchoolLocation()!=null){
						schoolLocation=userMailSend.getSchoolLocation()+locationCode;
					}

					if(userMailSend.getFlag()!=null){
						flag=userMailSend.getFlag();
					}
					if(userMailSend.getRequisitionNumber()!=null){
						requisitionNumber=userMailSend.getRequisitionNumber();
					}
					String emailBodyText="";

					/* if(userMailSend.getIsUserOrTeacherFlag()==7){
							 try{
		                       	//System.out.println(":::::::::::::::::::::::::Offer Decline And Accepted ::::::::::::::::::"+userMailSend.getSubject());
	                            emailBodyText=MailText.getFourthAcceptOrDecline(sPanelUserName,userMailSend.getTeacherDetail(),sJobTitle,schoolLocation,flag);
	                            //System.out.println("toEmail:::"+toEmail);
	                            emailerService.sendMailAsHTMLText(toEmail, userMailSend.getSubject(),emailBodyText);
							 }catch(Exception e){}
	                     }else*/ 
					if(offerReadyMailFlag==0){
						String sPanelEmailBodyText=MailText.getPanelEmailBody(toEmail, sEmailSubject, sPanelUserName, sPanelUserNameTo, sTeacheName, sJobTitle, sStatusName, userMaster);
						try{
							//System.out.println("Sending E-mail "+iCount +" of "+iTotalCount);
							//System.out.println("Panel Email ... Sublect "+sEmailSubject +" To "+sPanelUserNameTo +" Email "+toEmail+" PanelEmailBodyText "+sPanelEmailBodyText);
							// emailerService.sendMailAsHTMLText(toEmail, sEmailSubject,sPanelEmailBodyText);	
						}catch (Exception e){}
					}else  if(offerReadyMailFlag==1){
						try{
							String firstEmailBodyText=MailText.getFirstPanelMember(sJobTitle,sTeacheName,schoolLocation,requisitionNumber);
							//System.out.println("1 Sending E-mail "+iCount +" of "+iTotalCount);
							//System.out.println("1 Panel Email ... Sublect "+sEmailSubject +" To "+sPanelUserNameTo +" Email "+toEmail+" firstEmailBodyText "+firstEmailBodyText);
							emailerService.sendMailAsHTMLTextWithBCC(toEmail, sEmailSubject,firstEmailBodyText,null,"offer");	
						}catch (Exception e){}
					}

				}
			}catch (NullPointerException en) {
				en.printStackTrace();
			}catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public String getStatusNote(String jobForTeacherId,String fitScore,String teacherId,String jobId,String statusId,String secondaryStatusId, String pageNo, String noOfRow,String sortOrder,String sortOrderType,int doNotShowPanelinst)
	{
		boolean flag=false;
		StringBuffer subs = new StringBuffer();
		StringBuffer sb = new StringBuffer("");
		String schoolName = "";
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		String statusHDRFlag=null;
		String statusJSIFlag=null;
		String statusHDRReq=null;
		String statusResend=null;
		String statusWaived=null;
		Date hiredByDate = null;
		int jobCategoryFlag=2;
		boolean isSuperAdminUser=false; //added by 04-04-2015
		try {
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			//added by 03-04-2015
			List<DistrictKeyContact> notesKeys=districtKeyContactDAO.findByContactType(userMaster, "Super Administrator"); //added by 01-04-2015
			if(notesKeys.size()>0)
				isSuperAdminUser=true;
			//ended by 03-04-2015
			int roleId=0;
			int entityType=0;
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
				entityType=userMaster.getEntityType();
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			DistrictMaster districtMaster = userMaster.getDistrictId();

			//-- set start and end position			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord = 0;


			String qqNote = ""; //@Ashish :: for Get Qualification Question Note Ids
			//	String questionDetails = "";
			String questionAssessmentIds = "";


			String sortOrderFieldName	=	"createdDateTime";
			Order  sortOrderStrVal		=	null;

			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"1";
				sortOrderStrVal			=	Order.desc(sortOrderFieldName);
			}
			//------------------------------------
			CandidateGridService cgService=new CandidateGridService();
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			JobOrder jobOrder =jobOrderDAO.findById(new Integer(jobId), false, false);
			JobForTeacher jobForTeacherObj =new JobForTeacher();
			if(jobForTeacherId!=null){
				jobForTeacherObj=jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
				System.out.println("jobForTeacherObj Id::"+jobForTeacherObj.getJobForTeacherId());
			}
			boolean isMiami = false;
			try{
				if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1200390))
					isMiami=true;
			}catch(Exception e){}

			try{
				if(jobForTeacherObj!=null){
					if(jobForTeacherObj.getRequisitionNumber()!=null){
						statusHDRReq=jobForTeacherObj.getRequisitionNumber();	
					}
				}
			}catch(Exception e){}


			SchoolInJobOrder schoolInJobOrder = null;
			if(userMaster.getEntityType()==3){
				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, userMaster.getSchoolId());
			}
			boolean isUpdateStatus=cgService.isUpdateStatus(jobForTeacherObj,userMaster,schoolInJobOrder);
			String recentStatusName="";
			boolean isJobAssessment =false;
			if(jobOrder.getIsJobAssessment()!=null){
				if(jobOrder.getIsJobAssessment()){
					isJobAssessment=true;
				}
			}

			List<TeacherStatusNotes> lstTeacherStatusNotes = null;
			List<TeacherStatusNotes> lstTeacherStatusNotesAll = null;
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3=null; 
			StatusMaster statusMaster=null;
			SecondaryStatus secondaryStatus=null;
			SecondaryStatus secondaryStatusForScore=null;
			if(statusId!=null && !statusId.equals("0")){
				statusMaster=statusMasterDAO.findById(Integer.parseInt(statusId),false,false);
				recentStatusName=statusMaster.getStatus();
				criterion3 = Restrictions.eq("statusMaster", statusMaster);
				if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
					SecondaryStatus secondaryStatusObj=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
					secondaryStatusForScore=secondaryStatusObj;
					recentStatusName=secondaryStatusObj.getSecondaryStatusName();
					if(secondaryStatusObj.getSecondaryStatusName().equalsIgnoreCase("jsi")){
						recentStatusName="JSI";
					}else if(secondaryStatusObj.getSecondaryStatusName().equalsIgnoreCase("Offer Ready") && isMiami){
						statusHDRFlag="Offer Ready";
					}
				}
			}else if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
				secondaryStatus=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
				
				/*//added by 21-03-2015
				if(jobOrder.getDistrictMaster().getDistrictId().equals(4218990) && secondaryStatus.getSecondaryStatusName().equals("No Evaluation Complete")) //added for philadelphia
				{
					criterion3=Restrictions.eq("statusMaster", secondaryStatus.getStatusMaster());
				}else//end by 21-03-2015					
*/				criterion3 = Restrictions.eq("secondaryStatus", secondaryStatus);
				secondaryStatusForScore=secondaryStatus;
				recentStatusName=secondaryStatus.getSecondaryStatusName();
				if(secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("jsi")){
					recentStatusName="JSI";
				}else if(secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("Offer Ready") && isMiami){
					statusHDRFlag="Offer Ready";
				}else if(secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("No Evaluation Complete") && jobOrder.getDistrictMaster().getDistrictId().equals(4218990)){
					statusHDRFlag="No Evaluation Complete";
				}
			}
			try{
				System.out.println("::::::::::::::::::::; jobOrder.getJobCategoryMaster()=="+jobOrder.getJobCategoryMaster());
				System.out.println(" secondaryStatusForScore::"+secondaryStatusForScore.getSecondaryStatusId());
			}catch(Exception e){}
			List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivilegeList=jobCategoryWiseStatusPrivilegeDAO.getStatus(districtMaster,jobOrder.getJobCategoryMaster(), secondaryStatusForScore); 
			try{
				if(jobCategoryWiseStatusPrivilegeList.size()>0 && entityType==2){
					if(jobCategoryWiseStatusPrivilegeList.get(0).isAutoUpdateStatus()){
						if(jobCategoryWiseStatusPrivilegeList.get(0).getUpdateStatusOption()!=null){
							if(jobCategoryWiseStatusPrivilegeList.get(0).getUpdateStatusOption()==0){
								jobCategoryFlag=0;
							}else{
								jobCategoryFlag=1;	
							}
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			//System.out.println("recentStatusName:=:::::::::"+recentStatusName);
			//System.out.println("isJobAssessment::::::::::"+isJobAssessment);
			Map<String,String> mapHistrory = new HashMap<String,String>();
			if((isJobAssessment && recentStatusName.equalsIgnoreCase("JSI"))|| !recentStatusName.equalsIgnoreCase("JSI")){

				if(userMaster.getEntityType()==1){
					Criterion criterion4 = Restrictions.eq("finalizeStatus",true);
					lstTeacherStatusNotes=teacherStatusNotesDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion1,criterion2,criterion3,criterion4);
					lstTeacherStatusNotesAll=teacherStatusNotesDAO.findWithAll(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4);
				}else{
					Criterion criterion4 = Restrictions.eq("finalizeStatus",false);
					Criterion criterion5 = Restrictions.eq("userMaster",userMaster);
					Criterion criterion6=Restrictions.and(criterion4, criterion5); 
					Criterion criterion7 = Restrictions.eq("finalizeStatus",true);
					Criterion criterion8=Restrictions.or(criterion6, criterion7); 
					lstTeacherStatusNotes=teacherStatusNotesDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion1,criterion2,criterion3,criterion8);
					lstTeacherStatusNotesAll=teacherStatusNotesDAO.findWithAll(sortOrderStrVal,criterion1,criterion2,criterion3,criterion8);
				}
				List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findByTeacherAndJob(teacherDetail,jobOrder);
				try{
					for(TeacherStatusHistoryForJob teacherStatusHistoryForJob : lstTeacherStatusHistoryForJob){
						
						if(teacherStatusHistoryForJob.getStatusMaster()!=null){
							String statusM=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##"+teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0"; 
							mapHistrory.put(statusM, teacherStatusHistoryForJob.getStatus());
						}else{
							String sStatus=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##0"+"##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId();
							mapHistrory.put(sStatus, teacherStatusHistoryForJob.getStatus());
						}
						if(teacherStatusHistoryForJob.getStatusMaster() != null && teacherStatusHistoryForJob.getStatus().equalsIgnoreCase("A") && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("hird")){
							hiredByDate = teacherStatusHistoryForJob.getHiredByDate();
						}
					}
				}catch(Exception exception){
					exception.printStackTrace();
				}

				//System.out.println(":::::::::::: hiredByDate1 ::::::::::::::::::::"+hiredByDate);

				boolean isStatus=getStatusCheck(lstTeacherStatusHistoryForJob,statusMaster,secondaryStatus);
				try{
					if(statusMaster!=null)
						if(isMiami && (statusMaster.getStatusShortName().equalsIgnoreCase("rem")|| statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))){
							List<SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus=new ArrayList<SchoolWiseCandidateStatus>();
							if(userMaster!=null)
								if(userMaster.getEntityType()==3){
									lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusHistoryForSchool(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId(),userMaster.getSchoolId());
								}else if(userMaster.getEntityType()==2){
									lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusHistoryByUser(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId(),userMaster);
								}
							if(lstSchoolWiseCandidateStatus.size()>0){
								if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln") && lstSchoolWiseCandidateStatus.get(0).getStatusMaster().getStatusShortName().equalsIgnoreCase("dcln")){
									statusHDRFlag="D";
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem") && lstSchoolWiseCandidateStatus.get(0).getStatusMaster().getStatusShortName().equalsIgnoreCase("rem")){
									statusHDRFlag="R";
								}
							}
						}
				}catch(Exception e){
					e.printStackTrace();
				}
				/****************Undo Status*******************/
				try{
					if(userMaster.getEntityType()==2){
						if(jobForTeacherObj!=null && jobForTeacherObj.getSecondaryStatus()!=null &&  jobForTeacherObj.getSecondaryStatus().getSecondaryStatusName().equalsIgnoreCase("Offer Ready")){
							statusResend="resend";
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				try{
					if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==1){
						String statusStr=null;
						if(statusMaster!=null){
							statusStr=teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+statusMaster.getStatusId()+"##0";
						}else if(secondaryStatus!=null){
							statusStr=teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##0##"+secondaryStatus.getSecondaryStatusId();
						}
						String waived=mapHistrory.get(statusStr);
						System.out.println(statusStr+":::::waived::::::"+waived);
						if(waived==null){ 
							statusWaived="waived";	
						}
						System.out.println("statusWaived ::::>>>>>>>>>>>>>>>>>>>>>>>>>>: "+statusWaived);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				boolean undoForStatus=false;
				try{
					if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()!=null && jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints() && secondaryStatus!=null && userMaster.getEntityType()==2){
						if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()){
							String accessDPoints="";
							if(jobOrder.getDistrictMaster().getAccessDPoints()!=null)
								accessDPoints=jobOrder.getDistrictMaster().getAccessDPoints();

							List <StatusMaster> statusMasterList=statusMasterDAO.findAllStatusMaster();
							Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
							for (StatusMaster statusMasterObj : statusMasterList) {
								mapStatus.put(statusMasterObj.getStatusShortName(),statusMasterObj);
							}

							if(accessDPoints!=null && (accessDPoints.contains("16")|| accessDPoints.contains("17") || accessDPoints.contains("18"))){
								List<SecondaryStatus> listSecondaryStatus = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithStatuss(jobOrder,mapStatus);

								Map<Integer,SecondaryStatus> secMap=new HashMap<Integer, SecondaryStatus>();
								boolean nodeScomp=false;
								boolean nodeEcomp=false;
								boolean nodeVcomp=false;
								for(TeacherStatusHistoryForJob tSHObj : lstTeacherStatusHistoryForJob){
									if(tSHObj.getStatusMaster()!=null){
										if(tSHObj.getStatusMaster().getStatusShortName().equals("scomp")){
											nodeScomp=true;
										}else if(tSHObj.getStatusMaster().getStatusShortName().equals("ecomp")){
											nodeEcomp=true;
										}else if(tSHObj.getStatusMaster().getStatusShortName().equals("vcomp")){
											nodeVcomp=true;
										}
									}
									if(tSHObj.getSecondaryStatus()!=null)
										secMap.put(tSHObj.getSecondaryStatus().getSecondaryStatusId(),tSHObj.getSecondaryStatus());
								}
								//System.out.println("nodescomp::::::"+nodeScomp);
								//System.out.println("nodeecomp::::::"+nodeEcomp);
								//System.out.println("nodeVcomp::::::"+nodeVcomp);
								//System.out.println("listSecondaryStatus::::"+listSecondaryStatus.size());
								if(nodeScomp || nodeEcomp || nodeVcomp){
									if(listSecondaryStatus.size()>0){
										for(SecondaryStatus secondaryStatusObj :listSecondaryStatus){
											for(SecondaryStatus  obj : secondaryStatusObj.getChildren()){
												if(obj.getStatus().equalsIgnoreCase("A") && obj.getStatusMaster()==null){

													SecondaryStatus sec=secMap.get(obj.getSecondaryStatusId());
													if(sec!=null){
														////System.out.println("StatusNodeId:::::"+secondaryStatusObj.getStatusNodeMaster().getStatusNodeId());
														////System.out.println("loop Id::::"+sec.getSecondaryStatusId()+":::::Status Id::::"+secondaryStatus.getSecondaryStatusId());
														if(nodeScomp && secondaryStatusObj.getStatusNodeMaster().getStatusNodeId()==1){
															if(secondaryStatus.getSecondaryStatusId().equals(sec.getSecondaryStatusId())){
																undoForStatus=true;
																break;
															}
														}else if(nodeEcomp && secondaryStatusObj.getStatusNodeMaster().getStatusNodeId()==2){
															if(secondaryStatus.getSecondaryStatusId().equals(sec.getSecondaryStatusId())){
																undoForStatus=true;
																break;
															}
														}else if(nodeVcomp && secondaryStatusObj.getStatusNodeMaster().getStatusNodeId()==3){
															if(secondaryStatus.getSecondaryStatusId().equals(sec.getSecondaryStatusId())){
																undoForStatus=true;
																break;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}else if(userMaster.getEntityType()!=2){
						undoForStatus=true;
					}

					if(userMaster.getEntityType()==2 || userMaster.getRoleId().getRoleId()==10 || userMaster.getRoleId().getRoleId()==11){
						undoForStatus=false;
						/*int panelFlag=0;
						try{
							List<JobWisePanelStatus> jobWisePanelStatusWiseList = jobWisePanelStatusDAO.getPanel(jobOrder,secondaryStatus,statusMaster);
							//System.out.println(":::::::::Get:::::::::::jobWisePanelStatusWiseList:::::::::"+jobWisePanelStatusWiseList.size());
							List <PanelSchedule> panelScheduleStatusList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWisePanelStatusWiseList,teacherDetail);
							//System.out.println("::::::::::::::::::::panelScheduleStatusList:::::::::"+panelScheduleStatusList.size());
							if(panelScheduleStatusList.size()==1){
								panelFlag=1;
								if(panelScheduleStatusList.get(0)!=null){
									List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelScheduleStatusList.get(0));
									//System.out.println("panelAttendeesList::::::"+panelAttendeesList.size());
									if(panelAttendeesList.size()>0){
										for(PanelAttendees panelAttendees :panelAttendeesList){
											if(panelAttendees.getPanelInviteeId()!=null){
												UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
												if(panelUserMaster.getUserId().equals(userMaster.getUserId()) && userMaster.getEntityType()==2){
													panelFlag=2;
												}
											}
										}
									}
								}

							}
						}catch(Exception e){
							e.printStackTrace();
						}
						if(panelFlag==1){
							undoForStatus=true;
						}*/
						/////System.out.println("panelFlag:::::"+panelFlag);
					}
				}catch(Exception e){
					e.printStackTrace();
				}

				System.out.println("undoForStatus=::::::::>>>>>>::"+undoForStatus);
				/*********End Undo Status**************/
				if(statusMaster!=null && isStatus){
					if(statusMaster.getStatusShortName().equalsIgnoreCase("hird")){
						if(!isMiami){
							statusHDRFlag="H";
						}
					}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln")){
						statusHDRFlag="D";
					}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
						statusHDRFlag="R";
					}else{
						if(!undoForStatus){
							statusHDRFlag="S";
						}
					}
				}else if(isStatus && secondaryStatus!=null){
					String statusStr=teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##0"+"##"+secondaryStatus.getSecondaryStatusId();
					String waived=mapHistrory.get(statusStr);
					if(waived!=null && waived.equalsIgnoreCase("W")){
						
					}else{
						if(!undoForStatus){
							statusHDRFlag="S";
						}
					}
				}
				//System.out.println("statusHDRFlag::::>>>>>>:::::::::"+statusHDRFlag);
				totaRecord=lstTeacherStatusNotesAll.size();
				int topSliderScore_isFinalizeStatus=0;
				int scoreProvided=0;
				int scoreMaxSccore=0;
				int dslider=1;
				List<TeacherStatusScores> teacherStatusScoresList= new ArrayList<TeacherStatusScores>();
				try{
					teacherStatusScoresList= teacherStatusScoresDAO.getStatusScoreAll(teacherDetail,jobOrder,statusMaster,secondaryStatus);
					if(teacherStatusScoresList.size()!=0){
						for(TeacherStatusScores teacherStatusScores : teacherStatusScoresList){
							if(userMaster.getUserId().equals(teacherStatusScores.getUserMaster().getUserId()))
							{
								scoreProvided=teacherStatusScores.getScoreProvided();
								scoreMaxSccore=teacherStatusScores.getMaxScore();

								if(teacherStatusScores.isFinalizeStatus())
								{
									dslider=0;
									topSliderScore_isFinalizeStatus=1;
								}
								else{
									//scoreInProgress=true;
								}
								break;
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}

				boolean isQuestionEnable=true;
				List<TeacherStatusScores> teacherStatusScore_InProcess= new ArrayList<TeacherStatusScores>();
				teacherStatusScore_InProcess=teacherStatusScoresDAO.getStatusScore(teacherDetail, jobOrder, statusMaster, secondaryStatus, userMaster);
				if(teacherStatusScore_InProcess.size()==0)
					isQuestionEnable=true;
				else if(teacherStatusScore_InProcess.size()==1)
				{
					if(teacherStatusScore_InProcess.get(0).isFinalizeStatus())
						isQuestionEnable=false;
				}

				List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
				jobCategoryWiseStatusPrivileges=jobCategoryWiseStatusPrivilegeDAO.getJobCWSP(jobOrder, secondaryStatus,statusMaster);

				TeacherAssessmentStatus teacherAssessmentStatus = null;
				TeacherAssessmentdetail teacherAssessmentdetail = null;
				java.util.List<TeacherAnswerDetail> teacherAnswerDetails =null;
				java.util.List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
				TeacherAssessmentQuestion teacherAssessmentQuestion = null;
				java.util.List<TeacherAssessmentOption> teacherQuestionOptionsList = null;

				if(recentStatusName.equalsIgnoreCase("jsi"))
				{
					JobOrder jobOrderForJSI=null;
					AssessmentJobRelation assessmentJobRelation=null;
					assessmentJobRelation=assessmentJobRelationDAO.getAssessmentJobRelationByJobOrder(jobOrder);
					if(assessmentJobRelation!=null)
					{
						AssessmentDetail assessmentDetailForJSI=null;
						if(assessmentJobRelation.getAssessmentId()!=null)
						{
							//teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail, jobOrder);
							assessmentDetailForJSI=assessmentJobRelation.getAssessmentId();
							teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentByTeacher(teacherDetail, assessmentDetailForJSI);
							if(teacherAssessmentStatusList.size() == 1){
								teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
								if(teacherAssessmentStatusList.get(0).getJobOrder()!=null)
									jobOrderForJSI=teacherAssessmentStatusList.get(0).getJobOrder();
							}
						}
					}


					String status = "";
					if(teacherAssessmentStatus==null)
					{
						return "1";
					}
					else
					{

						status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();
						teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();
						if(status.equalsIgnoreCase("icomp")){
							return "1";
						}else if(status.equalsIgnoreCase("vlt")){
							return "2";
						}
					}
					Criterion criterion = Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail);
					Criterion criterionTech = Restrictions.eq("teacherDetail", teacherDetail);
					//Criterion criterionZJob = Restrictions.eq("jobOrder", jobOrder);
					Criterion criterionZJob = Restrictions.eq("jobOrder", jobOrderForJSI);
					teacherAnswerDetails = teacherAnswerDetailDAO.findByCriteria(criterion,criterionTech,criterionZJob);
				}

				// Start ... Copy Question in statusspecificscore table
				int iScoreSum=0;
				int iScoreMaxSum=0;
				int answerSliderCount=0;
				List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
				Map<Integer,StatusSpecificScore> stausSCore= new HashMap<Integer, StatusSpecificScore>();
				if(userMaster.getEntityType()!=1)
				{
					if((secondaryStatusId!=null && !secondaryStatusId.equals("0")) || (statusId!=null && !statusId.equals("0")) )
					{
						boolean flagForSuperUser=false;
						if((isSuperAdminUser || userMaster.getEntityType()==2) && !recentStatusName.equalsIgnoreCase("JSI")){
							lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList(jobOrder,statusMaster,secondaryStatus,teacherDetail,userMaster);
							if(lstStatusSpecificScore.size()==0)flagForSuperUser=true;
							lstStatusSpecificScore.clear();
							lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionListForSuperAdministration(jobOrder,statusMaster,secondaryStatus,teacherDetail);
						}else{
							lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList(jobOrder,statusMaster,secondaryStatus,teacherDetail,userMaster);
						}	
						if(lstStatusSpecificScore.size()==0 || flagForSuperUser){
							//System.out.println("==============================================enter----------------------------------------------------------------");
							if(recentStatusName.equalsIgnoreCase("jsi")){
								/**** Session dynamic ****/
								SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
								StatelessSession statelesSsession = sessionFactory.openStatelessSession();
								Transaction txOpen =statelesSsession.beginTransaction();
								for(TeacherAnswerDetail teacherAnswerDetail : teacherAnswerDetails) 
								{
									StatusSpecificScore statusSpecificScore=new StatusSpecificScore();
									statusSpecificScore.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());

									statusSpecificScore.setJobCategoryId(jobOrder.getJobCategoryMaster().getJobCategoryId());
									statusSpecificScore.setJobOrder(jobOrder);

									if(statusMaster!=null)
										statusSpecificScore.setStatusId(statusMaster.getStatusId());
									if(secondaryStatus!=null)
										statusSpecificScore.setSecondaryStatusId(secondaryStatus.getSecondaryStatusId());

									statusSpecificScore.setTeacherAnswerDetail(teacherAnswerDetail);

									statusSpecificScore.setScoreProvided(0);
									if(jobCategoryWiseStatusPrivileges.size()>0){
										statusSpecificScore.setMaxScore(jobCategoryWiseStatusPrivileges.get(0).getMaxValuePerJSIQuestion());
									}
									statusSpecificScore.setUserMaster(userMaster);
									statusSpecificScore.setCreatedDateTime(new Date());

									statusSpecificScore.setTeacherDetail(teacherDetail);
									statusSpecificScore.setFinalizeStatus(0);

									if(userMaster.getSchoolId()!=null)
										statusSpecificScore.setSchoolId(userMaster.getSchoolId().getSchoolId());

									statelesSsession.insert(statusSpecificScore);
								}
								statusSpecificScoreDAO.copyQuestionAnswer(jobOrder, statusMaster,secondaryStatus,userMaster,teacherDetail);//add by Ram Nath for Jsi question
								txOpen.commit();
								statelesSsession.close();
							}else{
								statusSpecificScoreDAO.copyQuestionAnswer(jobOrder, statusMaster,secondaryStatus,userMaster,teacherDetail);
							}
							if((isSuperAdminUser || userMaster.getEntityType()==2) && !recentStatusName.equalsIgnoreCase("JSI")){
							lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionListForSuperAdministration(jobOrder,statusMaster,secondaryStatus,teacherDetail);
							}else{
							lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList(jobOrder,statusMaster,secondaryStatus,teacherDetail,userMaster);
							}
						}
						for(StatusSpecificScore pojo:lstStatusSpecificScore)
						{
							if(userMaster.getUserId().toString().equalsIgnoreCase(pojo.getUserMaster().getUserId().toString()))
							if(pojo!=null)
							{
								if(pojo.getTeacherAnswerDetail()!=null)
									stausSCore.put(pojo.getTeacherAnswerDetail().getAnswerId(),pojo);
								if(pojo.getScoreProvided()!=null){
									iScoreSum=iScoreSum+pojo.getScoreProvided();
								}
								if(pojo.getMaxScore()!=null && pojo.getMaxScore()>0){
									iScoreMaxSum=iScoreMaxSum+pojo.getMaxScore();
									answerSliderCount++;
								}

							}
						}
					}
				}
				// End ... Copy Question in statusspecificscore table

				// Start ... ShowInstructionAttacheFileName 
				boolean bShowInstructionAttacheFileName=false;
				String strShowInstructionAttacheFileName="";
				//if(lstStatusSpecificScore.size()>0)
				//{
				if(jobCategoryWiseStatusPrivileges.size()>0){
					strShowInstructionAttacheFileName=jobCategoryWiseStatusPrivileges.get(0).getInstructionAttachFileName();
					if(userMaster.getEntityType()!=1 && strShowInstructionAttacheFileName!=null && !strShowInstructionAttacheFileName.equalsIgnoreCase(""))
						bShowInstructionAttacheFileName=true;
				}

				//}
				// End ... ShowInstructionAttacheFileName

				//Start ... Panel
				debugPrintln("Panel Process ","Start panel ... Teacher Id "+teacherDetail.getTeacherId());
				debugPrintln("Panel Process ","Panel Job Category "+jobOrder.getJobCategoryMaster().getJobCategoryId());

				//Is Already Override
				TeacherStatusHistoryForJob teacherStatusHistoryForJob= teacherStatusHistoryForJobDAO.getOverride(teacherDetail, jobOrder,statusMaster,secondaryStatus);
				boolean isAlreadyOverride=false;
				if(teacherStatusHistoryForJob!=null && teacherStatusHistoryForJob.getOverride().equals(new Boolean(true)))
					isAlreadyOverride=true;
				debugPrintln("Is Already Override "+isAlreadyOverride);

				//Active District User
				Map<String , Integer> mapUser=new HashMap<String, Integer>();
				Map<String , Boolean> mapHR=new HashMap<String, Boolean>();
				List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
				lstUserMaster=userMasterDAO.getActiveUserByDistrict(userMaster.getDistrictId());
				if(lstUserMaster.size() > 0)
				{
					for(UserMaster pojo:lstUserMaster)
						mapUser.put(pojo.getEmailAddress(), pojo.getEntityType());
				}
				//HR List
				boolean bActiveHR=false;
				boolean bOverride=false;
				List<DistrictKeyContact> districtKeyContacts=new ArrayList<DistrictKeyContact>();
				ContactTypeMaster contactTypeMaster=contactTypeMasterDAO.getActiveMaster(1);
				if(contactTypeMaster!=null && userMaster.getEntityType()!=1)
				{
					if(userMaster.getDistrictId()!=null)
						districtKeyContacts=districtKeyContactDAO.getHRList(contactTypeMaster, userMaster.getDistrictId().getDistrictId());
					if(districtKeyContacts.size() > 0)
						for(DistrictKeyContact pojo:districtKeyContacts)
							if(mapUser.get(pojo.getKeyContactEmailAddress())!=null)
							{
								bActiveHR=true;
								mapHR.put(pojo.getKeyContactEmailAddress(), new Boolean(true));
							}
							else
								mapHR.put(pojo.getKeyContactEmailAddress(), new Boolean(false));
				}

				if(bActiveHR)
				{
					if(mapHR.get(userMaster.getEmailAddress())!=null && mapHR.get(userMaster.getEmailAddress()).equals(new Boolean(true)))
						bOverride=true;
				}
				else
				{
					if(userMaster.getEntityType()==2)
						bOverride=true;
				}
				debugPrintln("", "bActiveHR "+bActiveHR +" bOverride "+bOverride);

				List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();

				SecondaryStatus secondaryStatus_temp=null;
				StatusMaster statusMaster_temp=null;
				List<SecondaryStatus> lstSecondaryStatus_temp=new ArrayList<SecondaryStatus>();

				if(statusMaster!=null)
				{
					lstSecondaryStatus_temp=secondaryStatusDAO.getSecondaryStatusForPanel(jobOrder.getDistrictMaster(), jobOrder.getJobCategoryMaster(), statusMaster);
					if(lstSecondaryStatus_temp.size() ==1)
					{
						secondaryStatus_temp=lstSecondaryStatus_temp.get(0);
						statusMaster_temp=null;
					}
					else
					{
						statusMaster_temp=statusMaster;
					}
				}
				else
				{
					secondaryStatus_temp=secondaryStatus;
				}

				jobWisePanelStatusList=jobWisePanelStatusDAO.getPanel(jobOrder,secondaryStatus_temp,statusMaster_temp);
				
				boolean bPanelForCG=false;
				boolean bPanelMember=false;
				boolean bLoggedInUserPanelAttendees=false;
				List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();

				if(jobWisePanelStatusList.size()==1)
				{
					panelScheduleList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWisePanelStatusList,teacherDetail);
					if(jobWisePanelStatusList.get(0).getPanelStatus())
					{
						/*if(panelScheduleList.size()==1)
							if(panelScheduleList.get(0).getTeacherDetail().getTeacherId().equals(teacherDetail.getTeacherId()))*/
						bPanelForCG=true;
					}

				}
				debugPrintln("", "bPanelForCG "+bPanelForCG);
				Map<Integer, UserMaster> mapUserScoreOrNote=new HashMap<Integer, UserMaster>();	
				List<UserMaster> lstUserMasterForScoresAndNotes = new ArrayList<UserMaster>();
				List<TeacherStatusNotes> lstTeacherStatusNotesAllUsers = new ArrayList<TeacherStatusNotes>();
				lstTeacherStatusNotesAllUsers=teacherStatusNotesDAO.findWithAll(sortOrderStrVal,criterion1,criterion2,criterion3);
				if(lstTeacherStatusNotesAllUsers.size()>0){
					for(TeacherStatusNotes notes:lstTeacherStatusNotesAllUsers)
						if(mapUserScoreOrNote.get(notes.getUserMaster().getUserId())==null && notes.isFinalizeStatus()){
							mapUserScoreOrNote.put(notes.getUserMaster().getUserId(), notes.getUserMaster());
							lstUserMasterForScoresAndNotes.add(notes.getUserMaster());
						}
				}

				if(teacherStatusScoresList.size()>0){
					for(TeacherStatusScores teacherStatusScores : teacherStatusScoresList)
						if(mapUserScoreOrNote.get(teacherStatusScores.getUserMaster().getUserId())==null && teacherStatusScores.isFinalizeStatus()){
							mapUserScoreOrNote.put(teacherStatusScores.getUserMaster().getUserId(), teacherStatusScores.getUserMaster());
							lstUserMasterForScoresAndNotes.add(teacherStatusScores.getUserMaster());
						}
				}				
				List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
				if(bPanelForCG)
				{
					//panelScheduleList=panelScheduleDAO.findPanelSchedules(teacherDetail, jobOrder);
					//panelScheduleList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusList(jobWisePanelStatusList);

					if(panelScheduleList.size()==1 && doNotShowPanelinst==0)
					{
						PanelSchedule panelSchedule= panelScheduleList.get(0);
						panelAttendees=panelAttendeesDAO.getPanelAttendees(panelSchedule);
						if(panelAttendees.size()>0)
						{
							bPanelMember=true;
							Collections.sort(panelAttendees,PanelAttendees.panelAttendeesUserNameAsc);
							sb.append("<div class='row mt10' style='margin-left:0px;'>");
							int i=0;
							for(PanelAttendees pojo:panelAttendees)
							{
								if(pojo.getPanelInviteeId().getUserId().equals(userMaster.getUserId()))
									bLoggedInUserPanelAttendees=true;

								i++;
								String sName="";
								if(pojo.getPanelInviteeId().getFirstName()!=null)
									sName=pojo.getPanelInviteeId().getFirstName();
								if(pojo.getPanelInviteeId().getLastName()!=null)
									sName=sName+" "+pojo.getPanelInviteeId().getLastName();
								sName.trim();

								String userAddress="";
								try{
									UserDashboardController udc=new UserDashboardController();
									BasicController bc= new BasicController();
									if(pojo.getPanelInviteeId()!=null){
										if(pojo.getPanelInviteeId().getEntityType()==3){
											userAddress=pojo.getPanelInviteeId().getSchoolId().getSchoolName()+"&#13;"+udc.getSchoolAddress(jobOrder);
										}else{
											userAddress=pojo.getPanelInviteeId().getDistrictId().getDistrictName()+"&#13;"+bc.getDistrictFullAddress(jobOrder);
										}
									}
									if(userAddress.equals("")){
										userAddress="Unknown";
									}
								}catch(Exception e){}


								if(mapUserScoreOrNote.get(pojo.getPanelInviteeId().getUserId())==null)									
									sb.append("<div class='row col-sm-4' style='padding-left:20px;'><div class='span_20'>&nbsp;<span class='fa-circle icon-large iconcolorRed' title='"+userAddress+"' style='cursor:default'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+sName+"</span></div></div>");
								else
									sb.append("<div class='row col-sm-4' style='padding-left:20px;'><div class='span_20'>&nbsp;<span class='fa-circle icon-large iconcolorGreen' title='"+userAddress+"' style='cursor:default'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+sName+"</span></div></div>");

								if(i%3==0)
									sb.append("<div class='row mt10' style='margin-left:-35px;'>&nbsp;</div>");							}
							sb.append("</div>");

							sb.append("<div class='row' style='margin-left:-35px;'>&nbsp;</div>");
						}
					}
				}
				else
				{
					if(lstUserMasterForScoresAndNotes.size()>0){
						Collections.sort(lstUserMasterForScoresAndNotes,UserMaster.userNameAsc);
						sb.append("<div class='row mt10' style='margin-left:-38px;'>");
						for(UserMaster userMaster2:lstUserMasterForScoresAndNotes)
						{
							if(userMaster2!=null)
							{
								String sName="";
								if(userMaster2.getFirstName()!=null)
									sName=userMaster2.getFirstName();
								if(userMaster2.getLastName()!=null)
									sName=sName+" "+userMaster2.getLastName();
								sName.trim();

								String userAddress="";
								try{
									UserDashboardController udc=new UserDashboardController();
									BasicController bc= new BasicController();
									if(userMaster2!=null){
										if(userMaster2.getEntityType()==3){
											userAddress=userMaster2.getSchoolId().getSchoolName()+"&#13;"+udc.getSchoolAddress(jobOrder);
										}else{
											userAddress=userMaster2.getDistrictId().getDistrictName()+"&#13;"+bc.getDistrictFullAddress(jobOrder);
										}
									}
									if(userAddress.equals("")){
										userAddress="Unknown";
									}
								}catch(Exception e){}
								sb.append("<div class='span3'><div class='status-notes-image'><span class='fa-circle icon-large iconcolorGreen' title='"+userAddress+"'></span></div><div class='status-notes-text' style='cursor:default' title='"+userAddress+"'>"+sName+"</div></div>");
							}
						}
						sb.append("</div>");
						sb.append("<div class='row' style='margin-left:-35px;'>&nbsp;</div>");
					}
				}
				debugPrintln("", "bLoggedInUserPanelAttendees "+bLoggedInUserPanelAttendees);

				List<TeacherStatusNotes> lstTeacherStatusNotesForPanel =new ArrayList<TeacherStatusNotes>();
				lstTeacherStatusNotesForPanel=teacherStatusNotesDAO.getFinalizeNotes(teacherDetail, jobOrder, statusMaster, secondaryStatus, userMaster); 

				boolean isFilaliseForPanelArray[]=new boolean[2];
				isFilaliseForPanelArray=isVisiableScoreSliderAndIcons(lstTeacherStatusHistoryForJob, jobWisePanelStatusList, panelAttendees, statusMaster, secondaryStatus, userMaster, teacherStatusScore_InProcess,lstTeacherStatusNotesForPanel,bPanelForCG,bLoggedInUserPanelAttendees,bOverride);
				debugPrintln("isFilaliseForPanel Size "+isFilaliseForPanelArray.length +" [ 1 ] "+isFilaliseForPanelArray[0] +" [ 2 ] "+isFilaliseForPanelArray[1]);
				boolean isFilaliseForPanel=isFilaliseForPanelArray[0];
				boolean isViewScoreIcon=isFilaliseForPanelArray[1];
				//End ... Panel



				if(userMaster.getEntityType()==3 && isUpdateStatus==false){
					dslider=0;
				}

				// Info message for If Panel have and Does not have panel member
				/*if(bPanelForCG && !bPanelMember)
				{
					sb.append("<div style='color: red;' id='noPanelMember'>No Panel Member is attached with panel schedule.</div>");
					sb.append("<input type='hidden' name='txtPanelMember' id='txtPanelMember'  value='0'/>");
				}
				else
				{
					sb.append("<input type='hidden' name='txtPanelMember' id='txtPanelMember'  value='1'/>");
				}*/
				if(true)
				{
				sb.append("<div class='row left10' id='candidateDetails' style='display:none;'><div class='control-group span16'>");
				sb.append("<label><strong>"+Utility.getLocaleValuePropByKey("lblCandidateName", locale)+":</strong></label>&nbsp;&nbsp;&nbsp;&nbsp;<span>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"</span><br/>");
				sb.append("<label><strong>"+Utility.getLocaleValuePropByKey("lblCandidatePosition", locale)+":</strong></label><span>&nbsp;&nbsp;"+jobOrder.getJobTitle()+"</span><br/></div></div>");
				}
				
				sb.append("<table border='0' style='width:670px;'>");
				if(fitScore!=null && !fitScore.isEmpty() && !fitScore.equals("0"))
				{
					/*if(isFilaliseForPanel)
					{*/
					sb.append("<tr><td align=left id='sliderStatusNote'><input type='hidden' name='noteDivChk' id='noteDivChk'  value='10'/>");
					sb.append("<table border=0><tr><td style='vertical-align:top;height:33px;padding-left:8px;'><label >"+Utility.getLocaleValuePropByKey("lblScore", locale)+" </label></td>");
					if(fitScore.equals("0")){
						dslider=0;
					}

					dslider=sliderDisplayStatus(dslider, bPanelForCG, bLoggedInUserPanelAttendees, bOverride);
					/*if(bPanelForCG)
						{
							debugPrintln("Main Slider", ""+dslider);

							if(bLoggedInUserPanelAttendees)
								dslider=1;
							else
								dslider=0;
							debugPrintln("Main Slider", ""+dslider);
						}*/

					if(answerSliderCount>0)
						dslider=0;

					if(!isQuestionEnable)
						dslider=0;

					/*if(!isFilaliseForPanel)
							dslider=0;*/


					int topMaxScore=0;
					if(scoreMaxSccore>0)
						topMaxScore=scoreMaxSccore;
					else
						topMaxScore=Integer.parseInt(fitScore);

					if(scoreProvided==0 && iScoreMaxSum>0)
						topMaxScore=iScoreMaxSum;

					int interval=10;
					if(topMaxScore%10==0)
						interval=10;
					else if(topMaxScore%5==0)
						interval=5;
					else if(topMaxScore%3==0)
						interval=3;
					else if(topMaxScore%2==0)
						interval=2;

					/*int scoreProvided_new=0;
						if(topSliderScore_isFinalizeStatus==1 || answerSliderCount==0)
							scoreProvided_new=scoreProvided;
						sb.append("<td style='padding-left:5px;vertical-align:bottom;height:33px;'>&nbsp;<iframe id=\"ifrmStatusNote\"  src=\"slideract.do?name=statusNoteFrm&tickInterval="+interval+"&max="+topMaxScore+"&swidth=536&dslider="+dslider+"&svalue="+scoreProvided_new+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:580px;margin-top:-11px;\"></iframe></td>");
					 */

					sb.append("<td style='padding:5px;vertical-align:bottom;height:33px;'>&nbsp;<iframe id=\"ifrmStatusNote\"  src=\"slideract.do?name=statusNoteFrm&tickInterval="+interval+"&max="+topMaxScore+"&swidth=536&dslider="+dslider+"&svalue="+scoreProvided+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:580px;margin-top:-11px;\"></iframe></td>");

					int sliderVal=0;
					if(userMaster.getEntityType()==3 && isUpdateStatus==false){
						sliderVal=-2;
					}else if(dslider==0){
						sliderVal=-1;
					}else{
						sliderVal=scoreProvided;
					}
					sb.append("<td valign=top style='padding-top:4px; padding-left: 17px;'><input type='hidden' name='dsliderchk' id='dsliderchk'  value='"+sliderVal+"'/>");
					if(getScore(teacherStatusScoresList, statusMaster, secondaryStatus)){
						if(isViewScoreIcon)
							sb.append("<a class='statusscore' data-placement='above' href='javascript:void(0);' id='statusscoreYes'><span class='fa-crosshairs icon-large iconcolor'></span></a>");
					}else{
						if(fitScore.equals("0")){
							sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblPlzAskDistrictAdmin", locale)+"' rel='tooltip' id='noFitScore' href='javascript:void(0);' ><img src=\"images/qua-icon.png\" width=\"15\" height=\"15\"></a>");
						}else{
							sb.append("&nbsp;");
						}
					}

					if(bShowInstructionAttacheFileName)
					{
						String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
						String fileName=strShowInstructionAttacheFileName;
						String filePath=""+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusId();
						sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewInst", locale)+"' rel='tooltip' id='attachInstructionFileName' href='javascript:void(0);' onclick=\"downloadAttachInstructionFileName('"+filePath+"','"+fileName+"','"+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusName()+"');"+windowFunc+"\"><span class='icon-info-sign icon-large iconcolor'></span></a>");
					}

					sb.append("</td></tr>");
					sb.append("</table>");
					sb.append("</td></tr>");
					/*}
					else
					{
						sb.append(OnlyShowInstructionAttacheFileNameWithOutSlider(bShowInstructionAttacheFileName, strShowInstructionAttacheFileName, jobCategoryWiseStatusPrivileges));
					}*/
				}
				else
				{
					sb.append(OnlyShowInstructionAttacheFileNameWithOutSlider(bShowInstructionAttacheFileName, strShowInstructionAttacheFileName, jobCategoryWiseStatusPrivileges));
				}

				int iCountScoreSlider=0;
				/*******Start JSI*********/
				sb.append("<tr><td align=left>");
				int iQdslider=0;
				if(isQuestionEnable)
					iQdslider=1;

				else if(userMaster.getEntityType()==1)
					iQdslider=0;
				if(userMaster.getEntityType()==3 && isUpdateStatus==false)
					iQdslider=0;

				iQdslider=sliderDisplayStatus(iQdslider, bPanelForCG, bLoggedInUserPanelAttendees, bOverride);
				int defauntQuestionNote=0;
				String clickable="class='notclickable'";				
				if(recentStatusName.equalsIgnoreCase("JSI")&& userMaster.getEntityType()!=1){
					
					//iQdslider,iCountScoreSlider,qqNote,defauntQuestionNote,questionAssessmentIds,html  //added date 30-04-2015
					/****************************call new method********************************/
					//array return value :iQdslider,iCountScoreSlider,qqNote,defauntQuestionNote,html
					String[] allValue=callOnlyForJSIJQuestionAndAnswer(teacherDetail, jobOrder, statusMaster_temp, secondaryStatus_temp, userMaster, teacherAssessmentdetail, stausSCore, teacherAnswerDetails, teacherAssessmentQuestion, teacherQuestionOptionsList, questionAssessmentIds, iQdslider, iCountScoreSlider, qqNote, defauntQuestionNote,isSuperAdminUser,clickable);
					iQdslider=Integer.parseInt(allValue[0]);
					iCountScoreSlider=Integer.parseInt(allValue[1]);
					qqNote=allValue[2];
					defauntQuestionNote=Integer.parseInt(allValue[3]);
					questionAssessmentIds=allValue[4];
					sb.append(allValue[5]);
					/****************************end call new method********************************/
				}
				sb.append("</td></tr>");
				/*******End JSI*********/

				//System.out.println(" >>>>***************** qqNote ******************* >>>>>>> "+qqNote);

				//************* Start ... Display Question and List
				int requiredNotesForUno=0; //added by 04-04-2015
				int isSchoolOnJobId=0;
				List<SchoolInJobOrder> listSIJO=new ArrayList<SchoolInJobOrder>();
				Boolean checkSchoolOnJobIdSetOrNot=true;
				if(jobOrder.getDistrictMaster()!=null){
					checkSchoolOnJobIdSetOrNot=OFFER_READY_MAP.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+recentStatusName.trim());
				}
				if(checkSchoolOnJobIdSetOrNot!=null && checkSchoolOnJobIdSetOrNot){
				try{
					listSIJO=schoolInJobOrderDAO.findByCriteria(Restrictions.eq("jobId", jobOrder));
					if(listSIJO.size()>0)
						isSchoolOnJobId=1;					
				}catch(Exception e){e.printStackTrace();}	
				}
				
				Map<Integer,String> alreadyShowQuestion=new TreeMap<Integer, String>();
				Map<Integer,String> alreadyShowAttribute=new TreeMap<Integer, String>();
				if(userMaster.getEntityType()!=1 && userMaster.getEntityType()!=5 && userMaster.getEntityType()!=6 && !recentStatusName.equalsIgnoreCase("JSI"))
				{		
					//added by 04-04-2015
					if(userMaster.getDistrictId().getDistrictId().equals(7800040) || userMaster.getDistrictId().getDistrictId().equals(614730)){
						requiredNotesForUno=1;
					}
					//ended by 04-04-2015
					iCountScoreSlider=0;
					if(lstStatusSpecificScore.size()>0)
					{
						int scoreWidth=525,scoreWidth1=565;
						if(userMaster.getEntityType()==3){
							scoreWidth=575;scoreWidth1=615;
						}
						Map<String,String> getScoreHtmlTableMap= getAllStatusSpecificScoreByQuestionIdAndAnswerId(teacherDetail, jobOrder, statusMaster, secondaryStatus);
						println("========================================================================================================");
						Map<String,StatusSpecificScore> mapSSS=new LinkedHashMap<String,StatusSpecificScore>();
						for(StatusSpecificScore score:lstStatusSpecificScore){
							if(score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString()) && score.getQuestion()!=null && !score.getQuestion().equals("")){
								mapSSS.put(score.getUserMaster().getUserId().toString()+"#"+score.getStatusSpecificQuestions().getQuestionId(),score);
							}else if(score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString()) && score.getSkillAttributesMaster()!=null)	{
								mapSSS.put(score.getUserMaster().getUserId().toString()+"#"+score.getStatusSpecificQuestions().getQuestionId(),score);
							}
							}							
						for(StatusSpecificScore score:lstStatusSpecificScore){println("--user id======"+score.getUserMaster().getUserId()+" question ===="+score.getQuestion());}
						
						Map<UserMaster,List<TeacherStatusNotes>> getAllNotesByUserMap=getAllNotesByUserMap(lstStatusSpecificScore);
						
						println("========================================================================================================");
						if(isSuperAdminUser){
							alreadyShowQuestion.clear();
							/*****************************************************************YYYYYYYYYYYYYY******************************************************/														
													sb.append("<tr><td align=left>");
														int questionNumber=1;
														int iQuestionCount=0;	
														boolean questionShowOrNot=false;
														boolean flagforOneTimeShow=false;
														//Map<Integer, Boolean> attMap = new HashMap<Integer, Boolean>();
														for(StatusSpecificScore score:lstStatusSpecificScore)
														{
															boolean firtQustionSliderShow=false;
															
															boolean thisUser=score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString());
															if(score.getQuestion()!=null && !score.getQuestion().equalsIgnoreCase("") && alreadyShowQuestion.get(score.getStatusSpecificQuestions().getQuestionId())==null)
															{
																questionShowOrNot=showOrNot(lstStatusSpecificScore,userMaster,score.getStatusSpecificQuestions(),getAllNotesByUserMap);
																if(questionShowOrNot){
																iQuestionCount++;
																sb.append("<div class='row question_padding_left '><span><label class='labletxt'><B>"+Utility.getLocaleValuePropByKey("lblQues", locale)+" "+iQuestionCount+":</B> "+score.getQuestion()+"</label></span></div>");
																if(score.getQuestion()!=null && !score.getQuestion().equals("") && score.getSkillAttributesMaster()!=null){
																	sb.append("<div class='row question_padding_left'><span><label  class='lableAtxt'><B>Attribute:</B> "+score.getSkillAttributesMaster().getSkillName()+"</label></span></div>");
																	alreadyShowAttribute.put(score.getStatusSpecificQuestions().getQuestionId(), score.getSkillAttributesMaster().getSkillName());
																}
																alreadyShowQuestion.put(score.getStatusSpecificQuestions().getQuestionId(), score.getQuestion());																
																firtQustionSliderShow=true;
																flagforOneTimeShow=true;
																}
															}
															
															
															int queId=0;
															
															if(score.getQuestion()!=null && !score.getQuestion().equals("") && score.getStatusSpecificQuestions()!=null){
																queId=score.getStatusSpecificQuestions().getQuestionId();
															}
															boolean scoreShowOrNot=false;
															try{
																println("thisUser=================yyyyyyyyyyy============================="+thisUser);
															if(!thisUser){
															List<TeacherStatusNotes> listTSN=teacherStatusNotesDAO.getQuestionNoteObjBySuperUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), Long.parseLong(queId+""),score.getUserMaster());
															println("listTSN==========hhhhhhhhhhhhhhhhhhhhh======:::::::::"+listTSN);
															if(listTSN!=null && listTSN.size()>0)
																scoreShowOrNot=listTSN.get(0).isFinalizeStatus();
															}
															}catch(Exception e){e.printStackTrace();}
															//boolean showOrHide=alreadyShowQuestion.get(score.getQuestion())!=null?true:false;															
															int iQuestionMaxScore=0;
															if(score.getMaxScore()!=null){
																iQuestionMaxScore=score.getMaxScore();
															}
															int tickInterval=1;
							
															if(iQuestionMaxScore%10==0)
																tickInterval=10;
															else if(iQuestionMaxScore%5==0)
																tickInterval=5;
															else if(iQuestionMaxScore%3==0)
																tickInterval=3;
															else if(iQuestionMaxScore%2==0)
																tickInterval=2;
																
																if(alreadyShowAttribute.get(score.getStatusSpecificQuestions().getQuestionId())==null && (score.getQuestion()==null || score.getQuestion().equals("")) && score.getSkillAttributesMaster()!=null && thisUser ){
																	sb.append("<div class='row question_padding_left'><span><label  class='lableAtxt'><B>"+Utility.getLocaleValuePropByKey("lnkAtt", locale)+":</B> "+score.getSkillAttributesMaster().getSkillName()+"</label></span></div>");
																	firtQustionSliderShow=true;																	
																}
																														
															if(firtQustionSliderShow){
																StatusSpecificScore sss=mapSSS.get(userMaster.getUserId().toString()+"#"+score.getStatusSpecificQuestions().getQuestionId());
																//System.out.println("teacherDetail=="+teacherDetail.getTeacherId()+" jobOrder==="+jobOrder.getJobId()+"  jobOrder.getDistrictMaster()====="+jobOrder.getDistrictMaster().getDistrictId()+"  userMaster===="+score.getUserMaster().getUserId() +"   Long.parseLong(queId===="+Long.parseLong(queId+""));
																//System.out.println("sss-------"+sss.getMaxScore()+"-------------------------"+sss.getScoreProvided()+"----------------------==="+sss.getUserMaster().getUserId()+"    "+sss.getQuestion());
															if(sss!=null && sss.getMaxScore()!=null && sss.getMaxScore()>0){
																iCountScoreSlider++;
																if(sss.getFinalizeStatus()==1)
																	iQdslider=0;
																else
																	iQdslider=1;
																sb.append("<div class='row mt10' style='padding-left: 22px;'>");
																sb.append("<div class='span5 slider_question' style='float:left;'><iframe id=\"ifrmQuestion_"+iCountScoreSlider+"\" src=\"slideract.do?name=questionFrm&tickInterval="+tickInterval+"&max="+sss.getMaxScore()+"&dslider="+iQdslider+"&swidth="+scoreWidth+"&step=1&answerId="+sss.getAnswerId()+"&svalue="+sss.getScoreProvided()+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:"+scoreWidth1+"px;margin-top:-11px;\"></iframe></div>");
																if(userMaster.getEntityType()==2){
																	sb.append("<div style='float:right;'><a class='questionScore' id='question"+score.getStatusSpecificQuestions().getQuestionId()+"' title='' data-original-title='' data-placement='above' href='javascript:void(0);' onclick='clickOnQuestionScore(this);'><span  class='fa-crosshairs icon-large iconcolor' ></span></a></div>");
																	//for all User Score
																	sb.append("<div id='scoreTablequestion"+score.getStatusSpecificQuestions().getQuestionId()+"' style='display:none;'>");
																	////System.out.println("teacherDetail=="+teacherDetail+" jobOrder=="+jobOrder+" statusMaster=="+statusMaster+" secondaryStatus=="+secondaryStatus);
																	println("qId=--------------------------------==="+score.getStatusSpecificQuestions().getQuestionId()+"    value===---------------------=="+getScoreHtmlTableMap.get(String.valueOf(score.getStatusSpecificQuestions().getQuestionId())));
																	sb.append(getScoreHtmlTableMap.get(String.valueOf(score.getStatusSpecificQuestions().getQuestionId())));
																	sb.append("</div>");
																	//end all User Score
																	}
																sb.append("</div>");
															}
															}
															queId=0;
							
															println(" >>>>>>>>>>>>>>> Question >>>>>>>>>>>>>>>>> "+score.getQuestion());
															boolean flagforfilledQuestionOrNot=false;
															if(score.getQuestion()!=null && !score.getQuestion().equals(""))
															{
																List<TeacherStatusNotes> listTeacherStatusNotes = new ArrayList<TeacherStatusNotes>();
																
																String statusNote = "";
																//int queId=0;
																if(score.getStatusSpecificQuestions()!=null){
																	queId=score.getStatusSpecificQuestions().getQuestionId();
																}
																println("hhhhh====================================================="+Long.parseLong(0+""));
																//teacherStatusNotes = teacherStatusNotesDAO.getQuestionNoteObjByUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), Long.parseLong(queId+""),userMaster);
																listTeacherStatusNotes = teacherStatusNotesDAO.getQuestionNoteObjBySuperUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), Long.parseLong(queId+""),score.getUserMaster());
							
																//@Ashish :: add Note for every question 
																if(thisUser || scoreShowOrNot){
																sb.append("<br/><div class='row mt5'>");
																sb.append("<div class='inner_disable_jqte' style='margin-left:22px;' id='questionNotes"+queId+"' >");																
																sb.append("<label >Note: </label>");
																}
																if(listTeacherStatusNotes!=null)
																println("listTeacherStatusNotes====================="+listTeacherStatusNotes.size());
																if(listTeacherStatusNotes!=null && listTeacherStatusNotes.size()>0){
																for(TeacherStatusNotes teacherStatusNotes:listTeacherStatusNotes){
																if(teacherStatusNotes!=null)
																	statusNote = teacherStatusNotes.getStatusNotes();
																sb.append("<br/><label>("+score.getUserMaster().getFirstName()+" "+score.getUserMaster().getLastName()+","+DATE_TIME_FORMAT.format(teacherStatusNotes.getCreatedDateTime())+")</label>");
																sb.append("<div>");								
																	if(teacherStatusNotes.isFinalizeStatus()){
																		sb.append("<textarea class='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='1' style='pointer-event:none;'>"+statusNote+"</textarea>");
																		if(thisUser){flagforfilledQuestionOrNot=true;}
																	}else
																		sb.append("<textarea class='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='1' style='pointer-event:none;'>"+statusNote+"</textarea>");									
																sb.append("<div id='main"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'>");
																sb.append("<a "+clickable+" class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"' rel='tooltip' href='javascript:void(0);' id='edit_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' onclick='editTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/edit-notes.png' width='25px' height='30px;'/></a>");
																sb.append("<br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("lblSaveNote", locale)+"' rel='tooltip' href='javascript:void(0);' id='save_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='saveTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/save-notes.png' width='25px' height='30px;'/></a>");
																sb.append("<br/><br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("btnClr", locale)+"' rel='tooltip' href='javascript:void(0);' id='cancel_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='cancelTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/cancel-notes.jpg' width='25px' height='30px;'/></a><br/>");
																sb.append("</div>");
																sb.append("<br/></div>");
																}
																}
																if(thisUser){																	
																int isJSI=0;
																String allInfoAboutNote="'"+teacherDetail.getTeacherId()+","+jobOrder.getJobId()+","+statusId+","+secondaryStatusId+","+userMaster.getUserId()+","+jobOrder.getDistrictMaster().getDistrictId()+","+ queId+","+isJSI+"'";
																sb.append("<label id='label"+queId+"' style='width:100%;'>("+score.getUserMaster().getFirstName()+" "+score.getUserMaster().getLastName()+")</label>");
																sb.append("<div class='inner_enable_jqte'>");
																sb.append("<textarea id='questionNotes"+queId+"' name='questionNotes"+queId+"' class='span6' rows='2'></textarea>");									
																sb.append("<div style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'><a "+clickable+" class='tooltipforicon' data-original-title='Add' rel='tooltip' href='javascript:void(0);' onclick=\"addTeacherStatusNotes(this,"+queId+","+allInfoAboutNote+");\"><img src='images/add_notes.png' width='25px' height='30px;'/></a></div>");
																sb.append("<br/></div>");
																}																
																sb.append("</div>");
																sb.append("</div>");
																if(thisUser){
																if(!flagforfilledQuestionOrNot)
																if(!qqNote.equals(""))
																	qqNote = qqNote+"#"+"questionNotes"+queId;
																else
																	qqNote = "questionNotes"+queId;
																}
															}
															if(thisUser){
															if(!flagforfilledQuestionOrNot && !(""+queId).equals("0"))
															if(questionAssessmentIds!=null && !questionAssessmentIds.equals(""))
																questionAssessmentIds = questionAssessmentIds+"#"+queId+"";
															else
																questionAssessmentIds = queId+"";
							
															questionNumber++;	
															}
														}
														
														if(questionNumber>1)
														{
															sb.append("<div class='row mt10'></div>");
														}
														sb.append("</td></tr>");
					/*****************************************************************YYYYYYYYYYYYYY******************************************************/								
						}else{
							alreadyShowQuestion.clear();
							sb.append("<tr><td align=left>");
							int questionNumber=1;
							int iQuestionCount=0;		
							boolean questionShowOrNot=false;
							for(StatusSpecificScore score:lstStatusSpecificScore)
							{
								boolean firtQustionSliderShow=false;								
								boolean thisUser=score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString());
								if(score.getQuestion()!=null && !score.getQuestion().equalsIgnoreCase("") && alreadyShowQuestion.get(score.getStatusSpecificQuestions().getQuestionId())==null)
								{
									questionShowOrNot=showOrNot(lstStatusSpecificScore,userMaster,score.getStatusSpecificQuestions(),getAllNotesByUserMap);
									if(questionShowOrNot){
									iQuestionCount++;
									sb.append("<div class='row question_padding_left '><span><label class='labletxt'><B>"+Utility.getLocaleValuePropByKey("headQues", locale)+" "+iQuestionCount+":</B> "+score.getQuestion()+"</label></span></div>");
									if(score.getQuestion()!=null && !score.getQuestion().equals("") && score.getSkillAttributesMaster()!=null){
										sb.append("<div class='row question_padding_left'><span><label  class='lableAtxt'><B>"+Utility.getLocaleValuePropByKey("lnkAtt", locale)+":</B> "+score.getSkillAttributesMaster().getSkillName()+"</label></span></div>");
										alreadyShowAttribute.put(score.getStatusSpecificQuestions().getQuestionId(), score.getSkillAttributesMaster().getSkillName());
									}
									alreadyShowQuestion.put(score.getStatusSpecificQuestions().getQuestionId(), score.getQuestion());
									firtQustionSliderShow=true;
									}									
								}
								println("--------------------------------------------------"+questionShowOrNot+"---------------------------------------------");
								int iQuestionMaxScore=0;
								if(score.getMaxScore()!=null){
									iQuestionMaxScore=score.getMaxScore();
								}
								int tickInterval=1;

								if(iQuestionMaxScore%10==0)
									tickInterval=10;
								else if(iQuestionMaxScore%5==0)
									tickInterval=5;
								else if(iQuestionMaxScore%3==0)
									tickInterval=3;
								else if(iQuestionMaxScore%2==0)
									tickInterval=2;
								
								
								if(alreadyShowAttribute.get(score.getStatusSpecificQuestions().getQuestionId())==null && (score.getQuestion()==null || score.getQuestion().equals("")) && score.getSkillAttributesMaster()!=null && thisUser){
									sb.append("<div class='row question_padding_left'><span><label  class='lableAtxt'><B>"+Utility.getLocaleValuePropByKey("lblAttribute1", locale)+":</B> "+score.getSkillAttributesMaster().getSkillName()+"</label></span></div>");
									firtQustionSliderShow=true;
								}
								
								
								int queId=0;
								if(score.getQuestion()!=null && !score.getQuestion().equals("") && score.getStatusSpecificQuestions()!=null){
									queId=score.getStatusSpecificQuestions().getQuestionId();
								}
								
								boolean scoreShowOrNot=false;
								try{
								if(!thisUser){
								println("teacherDetail=="+teacherDetail.getTeacherId()+" jobOrder==="+jobOrder.getJobId()+"  jobOrder.getDistrictMaster()====="+jobOrder.getDistrictMaster().getDistrictId()+"  userMaster===="+score.getUserMaster().getUserId() +"   Long.parseLong(queId===="+Long.parseLong(queId+""));
								List<TeacherStatusNotes> listTSN=teacherStatusNotesDAO.getQuestionNoteObjBySuperUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), Long.parseLong(queId+""),score.getUserMaster());
								println("listTSN==========hhhhhhhhhhhhhhhhhhhhh======:::::::::"+listTSN);
								if(listTSN!=null && listTSN.size()>0){
									println("listTSN==========hhhhhhhhhhhhhhhhhhhhh======:::::::::"+listTSN.size());
									scoreShowOrNot=listTSN.get(0).isFinalizeStatus();
								}
								}
								}catch(Exception e){e.printStackTrace();}
								
								if(firtQustionSliderShow){
									StatusSpecificScore sss=mapSSS.get(userMaster.getUserId().toString()+"#"+score.getStatusSpecificQuestions().getQuestionId());
									//System.out.println("teacherDetail=="+teacherDetail.getTeacherId()+" jobOrder==="+jobOrder.getJobId()+"  jobOrder.getDistrictMaster()====="+jobOrder.getDistrictMaster().getDistrictId()+"  userMaster===="+score.getUserMaster().getUserId() +"   Long.parseLong(queId===="+Long.parseLong(queId+""));
									//System.out.println("sss-------"+sss.getMaxScore()+"-------------------------"+sss.getScoreProvided()+"----------------------==="+sss.getUserMaster().getUserId()+"    "+sss.getQuestion());
								if(sss!=null && sss.getMaxScore()!=null && sss.getMaxScore()>0){
									iCountScoreSlider++;
									if(sss.getFinalizeStatus()==1)
										iQdslider=0;
									else
										iQdslider=1;									
									sb.append("<div class='row mt10' style='padding-left: 22px;'>");
									sb.append("<div class='span5 slider_question' style='float:left;'><iframe id=\"ifrmQuestion_"+iCountScoreSlider+"\" src=\"slideract.do?name=questionFrm&tickInterval="+tickInterval+"&max="+sss.getMaxScore()+"&dslider="+iQdslider+"&swidth="+scoreWidth+"&step=1&answerId="+sss.getAnswerId()+"&svalue="+sss.getScoreProvided()+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:"+scoreWidth1+"px;margin-top:-11px;\"></iframe></div>");
									if(userMaster.getEntityType()==2){
										sb.append("<div style='float:right;'><a class='questionScore' id='question"+score.getStatusSpecificQuestions().getQuestionId()+"' title='' data-original-title='' data-placement='above' href='javascript:void(0);' onclick='clickOnQuestionScore(this);'><span  class='fa-crosshairs icon-large iconcolor' ></span></a></div>");
										//for all User Score
										sb.append("<div id='scoreTablequestion"+score.getStatusSpecificQuestions().getQuestionId()+"' style='display:none;'>");
										//System.out.println("teacherDetail=="+teacherDetail+" jobOrder=="+jobOrder+" statusMaster=="+statusMaster+" secondaryStatus=="+secondaryStatus);
										println("qId=--------------------------------==="+score.getStatusSpecificQuestions().getQuestionId()+"    value===---------------------=="+getScoreHtmlTableMap.get(String.valueOf(score.getStatusSpecificQuestions().getQuestionId())));
										sb.append(getScoreHtmlTableMap.get(String.valueOf(score.getStatusSpecificQuestions().getQuestionId())));
										sb.append("</div>");
										//end all User Score
										}
									sb.append("</div>");									
								}
								}
								 queId=0;

								println(" >>>>>>>>>>>>>>> Question >>>>>>>>>>>>>>>>> "+score.getQuestion());
								boolean flagforfilledQuestionOrNot=false;
								if(score.getQuestion()!=null && !score.getQuestion().equals(""))
								{
									println("Entry ----------------->>>>>>>> score==="+score.getQuestion());
									//TeacherStatusNotes teacherStatusNotes = new TeacherStatusNotes();
									List<TeacherStatusNotes> listTeacherStatusNotes = null;

									String statusNote = "";
									//int queId=0;
									if(score.getStatusSpecificQuestions()!=null){
										queId=score.getStatusSpecificQuestions().getQuestionId();
									}									
									println("teacherDetail=="+teacherDetail.getTeacherId()+" jobOrder==="+jobOrder.getJobId()+"  jobOrder.getDistrictMaster()====="+jobOrder.getDistrictMaster().getDistrictId()+"  userMaster===="+score.getUserMaster().getUserId() +"   Long.parseLong(queId===="+Long.parseLong(queId+""));
									//teacherStatusNotes = teacherStatusNotesDAO.getQuestionNoteObjByUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), Long.parseLong(queId+""),score.getUserMaster());
									listTeacherStatusNotes = teacherStatusNotesDAO.getQuestionNoteObjBySuperUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), Long.parseLong(queId+""),score.getUserMaster());
									
									//@Ashish :: add Note for every question 
									sb.append("<div class='row mt5'>");
									sb.append("<div style='margin-left:22px;' id='questionNotes"+queId+"' >");
									if(questionShowOrNot)
									if(thisUser || scoreShowOrNot){
									sb.append("<label >"+Utility.getLocaleValuePropByKey("blNote", locale)+"</label>");
									}
									
									if(listTeacherStatusNotes!=null){
									for(TeacherStatusNotes teacherStatusNotes:listTeacherStatusNotes){
									if(teacherStatusNotes!=null)
									println(teacherStatusNotes.isFinalizeStatus()+"teacherStatusNotes===yyyyyyyyyyyyyyyyyyyyyyyyyy==="+teacherStatusNotes.getStatusNotes());
									if(teacherStatusNotes!=null)
										statusNote = teacherStatusNotes.getStatusNotes();
									sb.append("<br/><label>("+score.getUserMaster().getFirstName()+" "+score.getUserMaster().getLastName()+","+DATE_TIME_FORMAT.format(teacherStatusNotes.getCreatedDateTime())+")</label>");
									println("finialae=================================="+teacherStatusNotes.isFinalizeStatus());
									if(teacherStatusNotes.isFinalizeStatus()){
										sb.append("<div class='' style='background-color:#EEEEEE; width:585px; height:50px; border:1px solid #CCCCCC; overflow:scroll; '>"+statusNote+"</div>");
										if(thisUser){flagforfilledQuestionOrNot=true;}
									}else
										if(thisUser){
											sb.append("<textarea id='questionNotes"+queId+"' name='questionNotes"+queId+"' class='span6' rows='2'>"+statusNote+"</textarea>");
											}																
									}
									}
									else{
										if(thisUser){
										sb.append("<br/><label>("+score.getUserMaster().getFirstName()+" "+score.getUserMaster().getLastName()+")</label>");
										sb.append("<textarea id='questionNotes"+queId+"' name='questionNotes"+queId+"' class='span6' rows='2'>"+statusNote+"</textarea>");
										}
									}
									

									sb.append("</div>");
									sb.append("</div>");
									if(thisUser){
									if(!flagforfilledQuestionOrNot)
									if(!qqNote.equals(""))
										qqNote = qqNote+"#"+"questionNotes"+queId;
									else
										qqNote = "questionNotes"+queId;
									}
								}
								//System.out.println("queId=========          "+queId);
								if(thisUser){
								if(!flagforfilledQuestionOrNot && !(""+queId).equals("0"))								
								if(questionAssessmentIds!=null && !questionAssessmentIds.equals(""))									
									questionAssessmentIds = questionAssessmentIds+"#"+queId+"";
								else									
									questionAssessmentIds = queId+"";

								questionNumber++;
								}
							}
							if(questionNumber>1)
							{
								sb.append("<div class='row mt10'></div>");
							}
							sb.append("</td></tr>");
							
						}					
					}
				}
				int isRequiredSchoolIdCheck=0;
				int alreadyExistsSchoolInStatus=0;
				String schoolNameValue="";
				String schoolIdValue="";
				println("listSIJO size========================================================================="+listSIJO.size());
				Boolean checkFlagForOFFER_READY=null;
				if(jobOrder.getDistrictMaster()!=null){
					checkFlagForOFFER_READY=OFFER_READY_MAP.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+recentStatusName.trim());
				}
				if(checkFlagForOFFER_READY!=null && checkFlagForOFFER_READY){
					/*Boolean schoolInputBoxHideOnStatus=SCH_INPUTBOX_HIDE.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+recentStatusName.trim());
					if(schoolInputBoxHideOnStatus!=null && schoolInputBoxHideOnStatus){
						isRequiredSchoolIdCheck=0;
						}else*/
					isRequiredSchoolIdCheck=1;
					println("Enter================");
					if(listSIJO!=null && listSIJO.size()==1){
						alreadyExistsSchoolInStatus=1;
						SchoolInJobOrder sijo=listSIJO.get(0);
						schoolNameValue = sijo.getSchoolId().getSchoolName().replace(" ", "!!");
						schoolIdValue=sijo.getSchoolId().getSchoolId().toString();
						println(schoolIdValue+"================nnnnnnn============================="+schoolNameValue);
					}
					if(jobForTeacherObj.getSchoolMaster()!=null){
						alreadyExistsSchoolInStatus=1;
						schoolNameValue = jobForTeacherObj.getSchoolMaster().getSchoolName().replace(" ", "!!");
						schoolIdValue=jobForTeacherObj.getSchoolMaster().getSchoolId().toString();
						println(schoolIdValue+"===================yyyyyyy=========================="+schoolNameValue);
					}
				}
				Boolean isAlwaysRequiredSchool=null;
				if(jobOrder.getDistrictMaster()!=null){
					isAlwaysRequiredSchool=IS_ALWAYS_REQUIRED_SCH_MAP.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+recentStatusName.trim());
				}
				if(isAlwaysRequiredSchool!=null && isAlwaysRequiredSchool){
				isSchoolOnJobId=1;
				}
				
				sb.append("<input type='hidden' id='qqNoteIds' value='"+qqNote+"'>");
				//sb.append("<input type='hidden' id='questionDetails' value='"+questionDetails+"'>");
				sb.append("<input type='hidden' id='questionAssessmentIds' value='"+questionAssessmentIds+"'>");
				sb.append("<input type='hidden' name='iQdslider' id='iQdslider' value="+iQdslider+">");

				sb.append("<input type='hidden' name='iCountScoreSlider' id='iCountScoreSlider' value="+iCountScoreSlider+">");
				sb.append("<input type='hidden' name='isQuestionEnable' id='isQuestionEnable' value="+isQuestionEnable+">");
				sb.append("<input type='hidden' name='defauntQuestionNote' id='defauntQuestionNote' value="+defauntQuestionNote+">");
				sb.append("<input type='hidden' name='requiredNotesForUno' id='requiredNotesForUno' value="+requiredNotesForUno+">");
				sb.append("<input type='hidden' name='isSchoolOnJobId' id='isSchoolOnJobId' value="+isSchoolOnJobId+">");
				
				sb.append("<input type='hidden' name='isRequiredSchoolIdCheck' id='isRequiredSchoolIdCheck' value="+isRequiredSchoolIdCheck+">");
				sb.append("<input type='hidden' name='alreadyExistsSchoolInStatus' id='alreadyExistsSchoolInStatus' value="+alreadyExistsSchoolInStatus+">");
				sb.append("<input type='hidden' name='schoolNameValue' id='schoolNameValue' value="+schoolNameValue+">");
				sb.append("<input type='hidden' name='schoolIdValue' id='schoolIdValue' value="+schoolIdValue+">");
				sb.append("<input type='hidden' name='evaluationCompleteDiv' id='evaluationCompleteDiv' value="+(jobForTeacherObj.getOfferReady()!=null?1:0)+">");
				sb.append("<input type='hidden' name='isSuperAdministration' value="+(isSuperAdminUser?1:0)+">");
				
				
				
				//System.out.println("isRequiredSchoolIdCheck==="+isSchoolOnJobId+"====yyyyyyyyyyyyyyyyy========"+isRequiredSchoolIdCheck);
				// End ... Display Question list from statusSpecificScore

				/*
				 * 		Dated	:	28-07-2014
				 * 		Purpose	:	Add Transcript Portion.
				 * 		By		:	Hanzala Subhani
				 * 		Version	:	1.0
				 * 
				 * */

				Integer tId = null;
				Integer eId = null;
				Integer jId = null;
				if(jobForTeacherObj!=null){
					tId		=	jobForTeacherObj.getTeacherId().getTeacherId();
					eId		=	5;
					jId		=	jobForTeacherObj.getJobId().getJobId();
				}
				boolean smartPractices=false;
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
					if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
						smartPractices=true;
					}
				}
				if(smartPractices && recentStatusName != null && recentStatusName.equalsIgnoreCase("Restart Workflow")){
						System.out.println("::::::::::::::::::::::::::Restart Workflow::::::::::::::::::::::::");
						WorkFlowOptionsForStatus flowOptionsForStatus=flowOptionsForStatusDAO.findOptionsByTeacherStatus(teacherDetail, jobOrder, statusMaster, secondaryStatus);
						String flowOptionIds="";
						if(flowOptionsForStatus!=null){
							flowOptionIds=flowOptionsForStatus.getFlowOptionIds();
						}
						
						sb.append("<tr><td>");
						sb.append("<div id='restartWorkflowGrid'>");
						sb.append("<table border='0' id='restartWorkflowTable' cellpadding=5>");
						sb.append("<tr>");
						sb.append("<th colspan=2 valign='top'>Which items which you like to restart?</th>");
						sb.append("</tr>");
						
						List<WorkFlowOptions> flowOptionsList=flowOptionsDAO.getFlowOptions();
							for (WorkFlowOptions flowOptions : flowOptionsList){
								boolean optionShow=false;
								if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferJSI()==0){
									if(flowOptions.getFlowOptionName().equalsIgnoreCase("Prescreen")){
										optionShow=false;	
									}else{
										optionShow=true;
									}
								}else{
									optionShow=true;
								}
								if(optionShow){
									String selectedFlow="";
									if(flowOptionIds!=null && flowOptionIds.contains("#"+flowOptions.getFlowOptionId()+"#")){
										selectedFlow="disabled checked";
									}
									sb.append("<tr>");
									sb.append("<td align=center valign=top>");
									sb.append("<input "+selectedFlow+" type=\"checkbox\" name=\"restartWorkflow[]\" value="+flowOptions.getFlowOptionId()+">");	
									sb.append("</td>");
									sb.append("<td valign=middle>"+flowOptions.getFlowOptionName());
									sb.append("</td>");
									sb.append("</tr>");
								}
							}
						sb.append("</table>");
						sb.append("</div>");
						sb.append("</td>");
						sb.append("</tr>");
				}else if(secondaryStatus != null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref") && isMiami){

					String StatusMaster3			=	"";
					String teacherAcademicsStatus 	= 	"";

					try{
						List<EligibilityStatusMaster> eligibilityStatusMasterList = null;
						Criterion criteria = null;
						if(eId.equals(5)){
							criteria= Restrictions.eq("formCode", "TR");
						}

						eligibilityStatusMasterList 	= 	eligibilityStatusMasterDAO.findByCriteria(criteria);
						for(EligibilityStatusMaster allEligibility:eligibilityStatusMasterList){
							StatusMaster3	=	StatusMaster3+"#$#"+allEligibility.getValidateStatus();
						}
					} catch(Exception e){
						e.printStackTrace();
					}

					try{

						/*============= For Sorting ===========*/
						int totalRecord		= 	0;
						//------------------------------------
						/*====== set default sorting fieldName ====== **/
						String sortOrderNoField		=	"attendedInYear";

						/**Start set dynamic sorting fieldName **/
						if(sortOrder!=null){
							if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("degree") && !sortOrder.equals("fieldOfStudy") && !sortOrder.equals("university")){
								sortOrderFieldName=sortOrder;
								sortOrderNoField=sortOrder;
							}
							if(sortOrder.equals("degree")){
								sortOrderNoField="degree";
							}
							if(sortOrder.equals("fieldOfStudy")){
								sortOrderNoField="fieldOfStudy";
							}
							if(sortOrder.equals("university")){
								sortOrderNoField="university";
							}
						}
						if(!sortOrderType.equals("") && !sortOrderType.equals(null))
						{
							if(sortOrderType.equals("0"))
							{
								sortOrderStrVal		=	Order.asc(sortOrderFieldName);
							}
							else
							{
								sortOrderTypeVal	=	"1";
								sortOrderStrVal		=	Order.desc(sortOrderFieldName);
							}
						}
						else
						{
							sortOrderTypeVal		=	"0";
							sortOrderStrVal			=	Order.asc(sortOrderFieldName);
						}
						/*=============== End ======================*/	

						TeacherDetail teacherDetailTrans = teacherDetailDAO.findById(tId, false, false);

						List<TeacherAcademics> listTeacherAcademics = null;
						listTeacherAcademics = teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetailTrans);
						listTeacherAcademics = teacherAcademicsDAO.findSortedAcadamicDetailByTeacher(sortOrderStrVal,teacherDetailTrans);

						List<TeacherAcademics> sortedlistTeacherAcademics	=	new ArrayList<TeacherAcademics>();

						SortedMap<String,TeacherAcademics>	sortedMap = new TreeMap<String,TeacherAcademics>();
						if(sortOrderNoField.equals("degree"))
						{
							sortOrderFieldName	=	"degree";
						}
						if(sortOrderNoField.equals("fieldOfStudy"))
						{
							sortOrderFieldName	=	"fieldOfStudy";
						}
						if(sortOrderNoField.equals("university"))
						{
							sortOrderFieldName	=	"university";
						}
						int mapFlag=2;
						for (TeacherAcademics trAcademics : listTeacherAcademics){
							String orderFieldName=trAcademics.getAttendedInYear()+"";
							if(sortOrderFieldName.equals("degree")){
								orderFieldName=trAcademics.getDegreeId().getDegreeName()+"||"+trAcademics.getAcademicId();
								sortedMap.put(orderFieldName+"||",trAcademics);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
							if(sortOrderFieldName.equals("fieldOfStudy")){
								orderFieldName=trAcademics.getFieldId().getFieldName()+"||"+trAcademics.getAcademicId();
								sortedMap.put(orderFieldName+"||",trAcademics);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
							if(sortOrderFieldName.equals("university")){
								orderFieldName=trAcademics.getUniversityId().getUniversityName()+"||"+trAcademics.getAcademicId();
								sortedMap.put(orderFieldName+"||",trAcademics);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
						}
						if(mapFlag==1){
							NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
							for (Iterator iter=navig.iterator();iter.hasNext();) {  
								Object key = iter.next(); 
								sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
							} 
						}else if(mapFlag==0){
							Iterator iterator = sortedMap.keySet().iterator();
							while (iterator.hasNext()) {
								Object key = iterator.next();
								sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
							}
						}else{
							sortedlistTeacherAcademics=listTeacherAcademics;
						}

						totalRecord = sortedlistTeacherAcademics.size();

						if(totalRecord<end)
							end=totalRecord;
						List<TeacherAcademics> listsortedTeacherCertificates	=	sortedlistTeacherAcademics.subList(start,end);

						sb.append("<tr><td>");
						sb.append("<div id='academicGridAcademicGrid'>");
						sb.append("<table border='0' id='academicGridAcademic' style=\"margin-right:0px;\">");
						sb.append("<thead class='bg'>");

						sb.append("<tr>");
						sb.append("<th width='20%' valign='top'>&nbsp;</th>");

						sb.append("<th width='20%' valign='top'>"+Utility.getLocaleValuePropByKey("lblSchool", locale)+"</th>");

						sb.append("<th width='14%' valign='top'>"+Utility.getLocaleValuePropByKey("lblDegreeConferredDate", locale)+"</th>"); 

						sb.append("<th width='15%' valign='top'>"+Utility.getLocaleValuePropByKey("lblDegree", locale)+"</th>");

						sb.append("<th width='12%' valign='top'>"+Utility.getLocaleValuePropByKey("lblFieldOfStudy", locale)+"</th>");

						sb.append("<th width='10%' class='net-header-text' valign='top'>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");

						sb.append("</tr>");

						sb.append("</thead>");
						if(listTeacherAcademics!=null)
							for(TeacherAcademics ta:listsortedTeacherCertificates)
							{

								Long academicId	=	ta.getAcademicId();
								sb.append("<tr>");

								sb.append("<td>");
								sb.append("<input "+clickable+" type=\"checkbox\" name=\"transcript_data[]\" onclick='return showAcademicQues()' value="+academicId+">");	
								sb.append("</td>");			

								sb.append("<td>");
								if(ta.getUniversityId() != null){
									sb.append(ta.getUniversityId().getUniversityName());
								} else {
									sb.append("");
								}
								sb.append("</td>");

								sb.append("<td>");
								String status	=	ta.getStatus()==null?"":ta.getStatus();
								String dateShow	=	Utility.convertDateAndTimeToDatabaseformatOnlyDate(ta.getDegreeConferredDate());
								dateShow		=	dateShow==null?"":dateShow;
								if(ta.getDegreeConferredDate() != null){

									if(status.equals("V") && ta.getDegreeConferredDate() != null){
										sb.append("<input style=\"width:82px;\" type=\"text\" placeholder='mm-dd-yyyy' id=\"graduationDate_"+ta.getAcademicId()+"\" name=\"graduationDate_"+ta.getAcademicId()+"\"  maxlength=\"10\" value="+dateShow+" readonly=\"readonly\" class=\"help-inline form-control_1\">");
									} else {
										sb.append("<input style=\"width:82px;\" type=\"text\" placeholder='mm-dd-yyyy' id=\"graduationDate_"+ta.getAcademicId()+"\" name=\"graduationDate_"+ta.getAcademicId()+"\"  maxlength=\"10\" value="+dateShow+" class=\"help-inline form-control_1\">");
									}
								} else {
									if(status.equals("V") && ta.getDegreeConferredDate() != null){
										sb.append("<input style=\"width:82px;\" type=\"text\" placeholder='mm-dd-yyyy' id=\"graduationDate_"+ta.getAcademicId()+"\" name=\"graduationDate_"+ta.getAcademicId()+"\"  maxlength=\"10\" readonly=\"readonly\" class=\"help-inline form-control_1\">");
									} else {
										sb.append("<input style=\"width:82px;\" type=\"text\" placeholder='mm-dd-yyyy' id=\"graduationDate_"+ta.getAcademicId()+"\" name=\"graduationDate_"+ta.getAcademicId()+"\"  maxlength=\"10\" class=\"help-inline form-control_1\">");
									}
								}
								if(!status.equals("V")){
									sb.append("&nbsp;&nbsp;<a "+clickable+" href=\"#\" onClick=\"canleGraduationDate("+ta.getAcademicId()+")\"><img src=\"images/red_cancel.png\"></a>");
								}

								if(!status.equals("V") || ta.getDegreeConferredDate() == null){
								}
								sb.append("</td>");

								sb.append("<td>");
								sb.append(ta.getDegreeId().getDegreeName());
								sb.append("</td>");

								sb.append("<td>");
								sb.append(ta.getFieldId().getFieldName());	
								sb.append("</td>");

								sb.append("<td>");
								teacherAcademicsStatus	=	teacherAcademicsStatus+","+status;
								if(status == null || status.equals("") || status.equals("P")){
									status = Utility.getLocaleValuePropByKey("lblPending", locale);
								} else if(status.equals("V")){
									status = Utility.getLocaleValuePropByKey("lblVerified", locale);
								} else if(status.equals("F")){
									status = Utility.getLocaleValuePropByKey("lblFailed", locale);
								} else if(status.equals("W")){
									status = Utility.getLocaleValuePropByKey("lblWaived", locale);
								}
								sb.append(status);
								//System.out.println(" ::::: H ::::: "+teacherAcademicsStatus);
								sb.append("</td>");
								sb.append("</tr>");
							}
						if(listTeacherAcademics==null || listTeacherAcademics.size()==0) {
							sb.append("<tr>");
							sb.append("<td colspan='7'>");
							sb.append(""+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"");
							sb.append("</td>");
							sb.append("</tr>");
						}
						sb.append("</table>");
						sb.append("</div>");

						String teacherFName			= 	jobForTeacherObj.getTeacherId().getFirstName()==null?"":jobForTeacherObj.getTeacherId().getFirstName();
						String teacherLName 		= 	jobForTeacherObj.getTeacherId().getLastName()==null?"":jobForTeacherObj.getTeacherId().getLastName();
						String secondaryStatusName	=	secondaryStatus.getSecondaryStatusName()==null?"":secondaryStatus.getSecondaryStatusName();


						System.out.println("::: fitScore :::::"+fitScore);
						String teacherName	=	teacherFName+teacherLName;
						teacherName			=	teacherName.replace("'","\\'");
						secondaryStatusName	=	secondaryStatusName.replace("'","\\'");

						sb.append("<table border='0' id='academicGridAcademicNotes' cellspacing='3' cellpadding='3' style='display:none;'>");
						sb.append("<tr><td colspan=\"2\">&nbsp;</td></tr>");

						sb.append("<input type=\"hidden\" id=\"jsiFileNameTrans\" name=\"jsiFileNameTrans\" value=\"\" />");
						sb.append("<input type=\"hidden\" id=\"teacherIdTrans\" name=\"teacherIdTrans\" value='"+tId+"' />");
						sb.append("<input type=\"hidden\" id=\"eligibilityIdTrans\" name=\"eligibilityIdTrans\" value='"+eId+"' />");
						sb.append("<input type=\"hidden\" id=\"jobIdTrans\" name=\"jobIdTrans\" value='"+jId+"'/>");
						sb.append("<input type=\"hidden\" id=\"formId\" name=\"formId\" value=\"\" />");
						sb.append("<input type=\"hidden\" name=\"StatusMaster3\" id=\"StatusMaster3\" value="+StatusMaster3+">");
						sb.append("<input type=\"hidden\" name=\"teacherFName\" id=\"teacherFName\" value="+teacherFName+">");
						sb.append("<input type=\"hidden\" name=\"teacherLName\" id=\"teacherLName\" value="+teacherLName+">");
						sb.append("<input type=\"hidden\" name=\"secondaryStatus\" id=\"secondaryStatus\" value="+secondaryStatusName+">");
						sb.append("<input type=\"hidden\" name=\"fitScore\" id=\"fitScore\" value="+fitScore+">");
						sb.append("<input type=\"hidden\" name=\"statusId\" id=\"statusId\" value="+statusId+">");
						sb.append("<input type=\"hidden\" name=\"secondaryStatusId\" id=\"secondaryStatusId\" value="+secondaryStatusId+">");

						sb.append("<input type=\"hidden\" name=\"teacherAcademicsStatus\" id=\"teacherAcademicsStatus\" value="+teacherAcademicsStatus+">");

						sb.append("<tr>");

						sb.append("<td>&nbsp;</td>");
						sb.append("<td>");

						sb.append("<button class='btn btn-primary' type='button' onclick='return saveTransAcademic(22)'>"+Utility.getLocaleValuePropByKey("lblVerified", locale)+"</button>&nbsp;&nbsp;");
						sb.append("<button class='btn btn-primary' type='button' onclick='return saveTransAcademic(23)'>"+Utility.getLocaleValuePropByKey("lblFailed", locale)+"</button>&nbsp;&nbsp;");
						sb.append("<button class='btn btn-primary' type='button' onclick='return saveTransAcademic(24)'>"+Utility.getLocaleValuePropByKey("lblWaived", locale)+"</button>&nbsp;&nbsp;");
						sb.append("</td>");

						sb.append("</tr>");

						sb.append("</table>");

					}
					catch (Exception e){
						e.printStackTrace();
					}
					/*	End Transcript Section */
				} else if(secondaryStatus != null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("e-References")){
					// Reference Check Grid
					try{

						districtMaster	=	districtMaster == null?jobForTeacherObj.getJobId().getDistrictMaster():districtMaster;
						List<DistrictSpecificRefChkQuestions> districtSpecificQuestionList	=  null;
						Integer numberOfQuestion = null;
						try{
							districtSpecificQuestionList	=	districtSpecificRefChkQuestionsDAO.getDistrictRefChkQuestionBYDistrict(districtMaster);
							numberOfQuestion 				=	districtSpecificQuestionList.size();
						} catch(Exception exception){
							exception.printStackTrace();
						}

							
						List<DistrictPortfolioConfig> districtPortfolioConfig 	= 	null;
						DistrictPortfolioConfig portConfig 						= 	null;
						districtPortfolioConfig = 	districtPortfolioConfigDAO.getPortfolioConfigsByDistrictAndCandidate(districtMaster,"I");
						Integer requiredReference = 0;
						try{
							if(districtPortfolioConfig!=null && districtPortfolioConfig.size()>0){
								portConfig = districtPortfolioConfig.get(0);
								requiredReference = portConfig.getReference();
							}
						}catch(Exception exception){
							exception.printStackTrace();
						}
						
						
						/*============= For Sorting ===========*/
						int totalRecord		= 	0;
						int topMaxScore		=	0;
						//------------------------------------
						/*====== set default sorting fieldName ====== **/
						String sortOrderNoField		=	"createdDateTime";
						String sortOrderFieldName1	=	"createdDateTime";

						/**Start set dynamic sorting fieldName **/
						if(sortOrder!=null){
							if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("firstName") && !sortOrder.equals("email") && !sortOrder.equals("contactnumber")){
								sortOrderFieldName1=sortOrder;
								sortOrderNoField=sortOrder;
							}
							if(sortOrder.equals("firstName")){
								sortOrderNoField="firstName";
							}
							if(sortOrder.equals("email")){
								sortOrderNoField="email";
							}
							if(sortOrder.equals("contactnumber")){
								sortOrderNoField="contactnumber";
							}
						}
						if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
							if(sortOrderType.equals("0")) {
								sortOrderStrVal		=	Order.asc(sortOrderFieldName1);
							} else {
								sortOrderTypeVal	=	"1";
								sortOrderStrVal		=	Order.desc(sortOrderFieldName1);
							}
						} else {
							sortOrderTypeVal		=	"0";
							sortOrderStrVal			=	Order.asc(sortOrderFieldName1);
						}
						/*=============== End ======================*/	

						Criterion criteria= Restrictions.eq("teacherDetail", jobForTeacherObj.getTeacherId());
						//List<TeacherElectronicReferences> teacherElectronicReferenceList = teacherElectronicReferencesDAO.findByCriteria(criteria);
						//List<TeacherElectronicReferences> teacherElectronicReferenceList = teacherElectronicReferencesDAO.findByTeacher(jobForTeacherObj.getTeacherId());
						List<TeacherElectronicReferences> teacherElectronicReferenceList = teacherElectronicReferencesDAO.findActiveContactedReferencesByTeacher(teacherDetail);
						totalRecord =teacherElectronicReferenceList.size();
						if(totalRecord<end)
							end=totalRecord;

						List<TeacherElectronicReferences> teacherElectronicReferenceListMain = teacherElectronicReferenceList.subList(start,end);
						Map<String,TeacherElectronicReferencesHistory> mapRefHistory = new HashMap<String, TeacherElectronicReferencesHistory>();
						// Create Map
						List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistoryList = null;
						List<TeacherElectronicReferencesHistory> countContactStatusRefChkCount 			= null;
						List<TeacherElectronicReferencesHistory> countReplyStatusRefChkCount 			= null;
						Integer refChkSize 	= 	0;
						Integer contactSize	=	0;
						Integer replySize	=	0;

						try{
							teacherElectronicReferencesHistoryList	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByTeacherId(districtMaster, jobForTeacherObj.getTeacherId());
							countContactStatusRefChkCount			=	teacherElectronicReferencesHistoryDAO.countContactStatusRefChk(districtMaster, jobForTeacherObj.getTeacherId());
							countReplyStatusRefChkCount				=	teacherElectronicReferencesHistoryDAO.countReplyStatusRefChk(districtMaster, jobForTeacherObj.getTeacherId());

							if(teacherElectronicReferencesHistoryList != null){
								refChkSize	=	teacherElectronicReferencesHistoryList.size();
								for (TeacherElectronicReferencesHistory refRecord : teacherElectronicReferencesHistoryList) {
									String key = refRecord.getTeacherElectronicReferences().getElerefAutoId()+"";
									mapRefHistory.put(key, refRecord);
								}
							}

							if(countContactStatusRefChkCount != null){
								contactSize = 	countContactStatusRefChkCount.size();
							}

							if(countReplyStatusRefChkCount != null){
								replySize	=	countReplyStatusRefChkCount.size();
							}

						} catch(Exception e){}
						int actionAdd=0;
						String responseText = "";
						sb.append("<tr><td>");
						sb.append("<div id='referenceCheckGrid'>");
						sb.append("<table border='0' id='referenceCheckGrid' style=\"margin-right:0px;\">");
						sb.append("<thead class='bg'>");

						sb.append("<tr>");

						sb.append("<th valign='top'>&nbsp;</th>");
						//responseText=PaginationAndSorting.responseSortingLink("Name",sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo);

						sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblName", locale)+"</th>");
						sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblEmail", locale)+"</th>");
						sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblContNum", locale)+"</th>");
						sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
						sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblScore", locale)+"</th>");
						sb.append("<th valign='top'>Created Date Time</th>");
						if(userMaster.getEntityType()==2 || userMaster.getEntityType()==3){
							sb.append("<th valign='top'>Action</th>");
							actionAdd=1;
						}
						sb.append("</tr>");
						sb.append("</thead>");

						Map<String,TeacherElectronicReferencesHistory> mapPosNumber = new HashMap<String, TeacherElectronicReferencesHistory>();
						Integer elerefAutoId 	= 0;
						Integer questionMaxScore= 0;	
						String techerFullName	=	null;
						String candidateFullName = "";
						if(totalRecord > 0) {

							for(TeacherElectronicReferences referenceDetail:teacherElectronicReferenceListMain) {
								Integer contactStatus 	= 0;
								elerefAutoId = referenceDetail.getElerefAutoId();
								techerFullName = referenceDetail.getFirstName()+" "+referenceDetail.getLastName();
								candidateFullName = teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
								try{
									techerFullName=techerFullName.replace("'","\\'");
									candidateFullName=candidateFullName.replace("'","\\'");
								}catch(Exception e){
									e.printStackTrace();
								}

								TeacherElectronicReferencesHistory referenceHistory = 	mapRefHistory.get(elerefAutoId.toString());
								try{
									if(referenceHistory != null){
										contactStatus	=	referenceHistory.getContactStatus();
										questionMaxScore=	referenceHistory.getQuestionMaxScore();
									}
								}catch(Exception exception){
									exception.printStackTrace();
								}

								sb.append("<tr>");

								sb.append("<td class='td1'>");
								if(contactStatus != null){
									if(contactStatus == 1){
										sb.append("<input "+clickable+" disabled type=\"checkbox\" name=\"referenceData[]\" value="+elerefAutoId+">");
									} else {
										sb.append("<input "+clickable+" type=\"checkbox\" name=\"referenceData[]\" onclick='return showReferenceCheckSLC()' value="+elerefAutoId+">");
									}
								} else {
										sb.append("<input "+clickable+" type=\"checkbox\" name=\"referenceData[]\" onclick='return showReferenceCheckSLC()' value="+elerefAutoId+">");
								}
								sb.append("</td>");

								sb.append("<td class='td2'>");
								sb.append(referenceDetail.getFirstName()+" "+referenceDetail.getLastName());
								sb.append("</td>");

								sb.append("<td class='td3'>");
								sb.append(referenceDetail.getEmail());
								sb.append("</td>");

								sb.append("<td class='td4'>");
								sb.append(referenceDetail.getContactnumber());
								sb.append("</td>");

								if(contactStatus != null){
									if(contactStatus == 1){
										sb.append("<td class='td5'>" +
												"<a "+clickable+" href=\"#\" onclick=\"showReferenceDetailSLC('"+candidateFullName+"','"+techerFullName+"',"+elerefAutoId+","+districtMaster.getDistrictId()+","+jobId+","+teacherId+")\">" +
												"<span class=\"icon-download-alt icon-large iconcolorBlue\"></span>" +
												"</a>" +
										"</td>");
									} else {
										sb.append("<td class='td5'></td>");
									}
								} else {
									sb.append("<td class='td6'>"+questionMaxScore+"</td>");
								}

								if(questionMaxScore != null){
									sb.append("<td class='td6'>"+questionMaxScore+"</td>");
								} else {
									sb.append("<td class='td6'></td>");
								}
								
								
								

								sb.append("<td class='td7'>");
								sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(referenceDetail.getCreatedDateTime()));
								sb.append("</td>");
								
								if(actionAdd == 1){
								String elerefAutoId1="",eId1="",jId1="",tId1="",uId="",urlString="";
								{									
								eId1		=	referenceDetail.getElerefAutoId().toString();																
								tId1		=	referenceDetail.getTeacherDetail().getTeacherId().toString();
								uId			=	userMaster.getUserId().toString();
								urlString 		= 	eId1+","+tId1+","+uId;
								}	
								
								sb.append("<td class='td8' style='text-align:center;'>");
								if( contactStatus != 1){//referenceHistory != null &&
									sb.append("<a "+clickable.substring(0,(clickable.length()-1))+" districtTooltip'"+" href='javascript:void(0)' data-original-title='District Entry' rel='tooltip' onclick=\"openQuestionEReference(\'"+urlString+"\');\"><img class='addDistrictEntry' src='images/add_notes.png' width='17px' height='20px;'/></a>");
								}else{
									sb.append("&nbsp;");
								}
								sb.append("</td>");								
								}
								
								sb.append("</tr>");
								
							}
						} else {
							sb.append("<tr>");
							sb.append("<td colspan=\"5\"><B>");
							sb.append(Utility.getLocaleValuePropByKey("lblNoRecord", locale));
							sb.append("</B></td>");
							sb.append("</tr>");
						}
						sb.append("</table>");
						sb.append(PaginationAndSorting.getPaginationTrippleGrid(request,totalRecord,noOfRow, pageNo));

						sb.append("</div>");

						Integer referenceTeacherId	=	jobForTeacherObj.getTeacherId().getTeacherId();
						Integer referenceJobId		=	jobForTeacherObj.getJobId().getJobId();
						String secondaryStatusName	=	secondaryStatus.getSecondaryStatusName()==null?"":secondaryStatus.getSecondaryStatusName();
						secondaryStatusName			=	secondaryStatusName.replace("'","\\'");
						sb.append("<table border='0' id='referenceGridSendEmail' cellspacing='3' cellpadding='3' style='display:none;'>");
						sb.append("<tr><td colspan=\"2\">&nbsp;</td></tr>");
						sb.append("<input type=\"hidden\" id=\"referenceTeacherId\" name=\"referenceTeacherId\" value='"+referenceTeacherId+"'/>");
						sb.append("<input type=\"hidden\" id=\"referenceJobId\" name=\"referenceJobId\" value='"+referenceJobId+"'/>");
						sb.append("<input type=\"hidden\" id=\"secondaryStatus\" name=\"secondaryStatus\" value="+secondaryStatusName+">");
						sb.append("<input type=\"hidden\" id=\"fitScore\" name=\"fitScore\" value="+fitScore+">");
						sb.append("<input type=\"hidden\" id=\"statusId\" name=\"statusId\" value="+statusId+">");
						sb.append("<input type=\"hidden\" id=\"secondaryStatusId\" name=\"secondaryStatusId\" value="+secondaryStatusId+">");
						sb.append("<input type=\"hidden\" id=\"referenceListSize\" name=\"referenceListSize\" value="+totalRecord+">");
						sb.append("<input type=\"hidden\" id=\"contactSize\" name=\"contactSize\" value="+contactSize+">");
						sb.append("<input type=\"hidden\" id=\"contactSize\" name=\"contactSize\" value="+refChkSize+">");
						sb.append("<input type=\"hidden\" id=\"numberOfQuestion\" name=\"numberOfQuestion\" value="+numberOfQuestion+">");
						sb.append("<input type=\"hidden\" id=\"topMaxScore\" name=\"topMaxScore\" value="+topMaxScore+">");
						sb.append("<input type=\"hidden\" id=\"requiredReference\" name=\"requiredReference\" value="+requiredReference+">");
						sb.append("<input type=\"hidden\" id=\"actionAdd\" name=\"actionAdd\" value="+actionAdd+">");
						sb.append("<tr>");

						sb.append("<td>&nbsp;</td>");
						sb.append("<td>");

						if(replySize != totalRecord){
							if(refChkSize != 0 && (totalRecord == refChkSize)){
								sb.append("<button class='btn btn-primary' type='button' onclick='return sendReferenceMail()'>"+Utility.getLocaleValuePropByKey("btnResend", locale)+"</button>&nbsp;&nbsp;");
							} else {
								sb.append("<button class='btn btn-primary' type='button' onclick='return sendReferenceMail()'>"+Utility.getLocaleValuePropByKey("btnSend", locale)+"</button>&nbsp;&nbsp;");
							}
							sb.append("</td>");
							sb.append("</tr>");
							sb.append("</table>");
						}
					} catch (Exception e){
						e.printStackTrace();
					}
				}		

				sb.append("</td></tr>");
				sb.append("<tr><td align=right style='padding-right:15px;'>");
				sb.append("<div id=\"divTransAcademic\" style=\"margin-top: 10px;\"></div>");
				sb.append("</td></tr>");

				/********************************* Online Activity ****************************************/
				List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSetList(districtMaster,jobOrder.getJobCategoryMaster());
				if(recentStatusName.equalsIgnoreCase("Online Activity")&& userMaster.getEntityType()!=1 && onlineActivityQuestionSetList.size()>0){
					sb.append("<div id=\"divOnlineActivityForCG\" style=\"margin-top: 10px;\"></div>");
				}
				/*********************************End Online Activity ****************************************/

				if(userMaster.getEntityType()!=1){
					sb.append("<tr><td align=right>");
					sb.append("<a data-original-title='Export Notes in PDF' rel='tooltip' id='expNotePdf' href='javascript:void(0);'  onclick='downloadSLCReport();'><span class='icon-print icon-large iconcolor'></span></a>&nbsp;&nbsp;");
					sb.append("<span id=\"statusAddNote\"><a "+clickable+" href=\"#\" onclick=\"openNoteDiv("+userMaster.getUserId()+")\">"+Utility.getLocaleValuePropByKey("lnkAddNote", locale)+"</a></span>");
					sb.append("</td></tr>");
				}
				sb.append("<tr><td>");
				try{
					sb.append("<div class='hide'id=\"scoreList\">");
					sb.append("<table border=0 class='table' >");
					sb.append("<thead class='bg'>");
					sb.append("<tr><th width=290>Name</td><th  width=30>Score</td><th  width=80>"+Utility.getLocaleValuePropByKey("lblCreateddate", locale)+"</th>");
					if(isSuperAdminUser){sb.append("<th>"+Utility.getLocaleValuePropByKey("lblActions", locale)+"</th>");	}//added by 04-04-2015
					sb.append("</tr>");
					sb.append("</thead>");
					//added by 04-04-2015
					String bothStausString="";
					if(statusMaster!=null && secondaryStatus!=null)
						bothStausString="sm#"+statusMaster.getStatusId()+"##ss#"+secondaryStatus.getSecondaryStatusId();
					else if(statusMaster!=null)
						bothStausString="sm#"+statusMaster.getStatusId();
					else if(secondaryStatus!=null)
						bothStausString="ss#"+secondaryStatus.getSecondaryStatusId();
					String teacherStatusScoresInfo=","+teacherDetail.getTeacherId()+","+jobOrder.getJobId()+","+bothStausString;
					//ended by 04-04-2015
					for(TeacherStatusScores teacherStatusScores : teacherStatusScoresList){
						if(teacherStatusScores.isFinalizeStatus()){
							sb.append("<tr><td>"+teacherStatusScores.getUserMaster().getFirstName()+" "+teacherStatusScores.getUserMaster().getLastName()+"</td>");
							sb.append("<td>"+teacherStatusScores.getScoreProvided()+"</td>");
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(teacherStatusScores.getCreatedDateTime())+"</td>");
							String allInfo="'"+teacherStatusScores.getTeacherStatusScoreId()+","+teacherStatusScores.getMaxScore()+","+teacherStatusScores.getScoreProvided()+teacherStatusScoresInfo+"'";
							////System.out.println("allInfo========="+allInfo);
							if(isSuperAdminUser){
							sb.append("<td><a "+clickable+" href='javascript:void(0)'  onclick=\"editteacherStatusScores("+allInfo+");\">"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a></td>");
							}
							sb.append("</tr>"); //added by 04-04-2015
						}
					}
					sb.append("</table>");
					sb.append("<table id='openEditTeacherStatusScore' style='display:none;width:100%;'></table>"); //added by 04-04-2015
					sb.append("</div>");
				}catch(Exception e){
					e.printStackTrace();
				}
				String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				sb.append("<table border='0' class='table' id='tblStatusNote'>");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				sb.append("<th valign=top>"+PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblNotes", locale),sortOrderFieldName,"statusNotes",sortOrderTypeVal,pgNo)+"</th>");
				sb.append("<th valign=top>"+Utility.getLocaleValuePropByKey("hdAttachment", locale)+"</th>");
				sb.append("<th valign=top>"+PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCreatedBy", locale),sortOrderFieldName,"userMaster",sortOrderTypeVal,pgNo)+"</th>");
				sb.append("<th valign=top>"+PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCreatedDate", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo)+"</th>");
				sb.append("<th valign=top>"+Utility.getLocaleValuePropByKey("lblActions", locale)+"</th>");
				sb.append("</tr>");
				sb.append("</thead>");
				int counter=0,countSave=0;
				if(lstTeacherStatusNotes !=null)
				{
					for(TeacherStatusNotes tsDetails : lstTeacherStatusNotes){
						sb.append("<tr>");
						sb.append("<td>");
						String statusNote=tsDetails.getStatusNotes();
						if(statusNote!=null){
							String statusNotes=statusNote.replaceAll("\\<[^>]*>","");
							int iTurncateIndex=90;
							if(statusNotes.length()>iTurncateIndex)
							{
								int sSpaceIndex=statusNotes.indexOf(" ", iTurncateIndex);
								if(sSpaceIndex >0)
								{
									statusNotes=statusNotes.substring(0,sSpaceIndex);
									sb.append("<a href='javascript:void(0);' id='teacherNotesDetails"+tsDetails.getTeacherStatusNoteId()+"' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgClickviewdetails", locale)+"' onclick='viewCompleteTeacherStatusNotes("+tsDetails.getTeacherStatusNoteId()+");'>"+statusNotes+"</a>");
									sb.append("<script>$('#teacherNotesDetails"+tsDetails.getTeacherStatusNoteId()+"').tooltip({placement : 'right'});</script>");
								}
								else
									sb.append(statusNotes);
							}
							else
								sb.append(statusNotes);
						}
						sb.append("</td>");
						sb.append("<td>");
						String fileName=tsDetails.getStatusNoteFileName();
						String filePath="statusNote/"+teacherDetail.getTeacherId();
						if(fileName!=null && fileName!=""){
							counter++;
							sb.append("<a href='javascript:void(0);' id='commfile"+counter+"' onclick=\"downloadCommunication('"+filePath+"','"+fileName+"','commfile"+counter+"');"+windowFunc+"\"><span class='icon-paper-clip icon-large iconcolor'></span></a>");
						}
						sb.append("</td>");
						String name="";
						if(tsDetails.getUserMaster()!=null){
							UserMaster um=tsDetails.getUserMaster();
							if(um.getMiddleName()!=null){
								name=um.getFirstName()+" "+um.getMiddleName()+" "+um.getLastName();
							}else{
								name=um.getFirstName()+" "+um.getLastName();
							}
						}
						sb.append("<td>"+name+"</td>");
						sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(tsDetails.getCreatedDateTime())+"</td>");
						sb.append("<td>");
						//if(!tsDetails.isFinalizeStatus()&& userMaster.getEntityType()!=1){
						if((!tsDetails.isFinalizeStatus()|| isSuperAdminUser)&& userMaster.getEntityType()!=1){
							countSave++;
							if(tsDetails.getTeacherAssessmentQuestionId()!=null){
								if(isSuperAdminUser)
								sb.append("<a "+clickable+" href='javascript:void(0);' onclick=\"return editShowStatusNote('"+tsDetails.getTeacherStatusNoteId()+"')\" >"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a> | <a "+clickable+" href='javascript:void(0);' onclick=\"return deleteStatusNoteChk('"+tsDetails.getTeacherStatusNoteId()+"','note')\" >"+Utility.getLocaleValuePropByKey("lnkDlt", locale)+"</a>");
								else
								sb.append("<a "+clickable+" href='javascript:void(0);' onclick=\"return deleteStatusNoteChk('"+tsDetails.getTeacherStatusNoteId()+"','note')\" >"+Utility.getLocaleValuePropByKey("lnkDlt", locale)+"</a>");
							}else
								sb.append("<a "+clickable+" href='javascript:void(0);' onclick=\"return editShowStatusNote('"+tsDetails.getTeacherStatusNoteId()+"')\" >"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a> | <a "+clickable+" href='javascript:void(0);' onclick=\"return deleteStatusNoteChk('"+tsDetails.getTeacherStatusNoteId()+"','note')\" >"+Utility.getLocaleValuePropByKey("lnkDlt", locale)+"</a>");

						}else{
							sb.append("&nbsp");
						}
						sb.append("</td>");
						sb.append("</tr>");
					}
				}

				if(lstTeacherStatusNotes == null || lstTeacherStatusNotes.size()==0){
					sb.append("<tr>");				
					sb.append("<td colspan=4>"+Utility.getLocaleValuePropByKey("lblNoNoteAdd", locale)+"</td>");
					sb.append("</tr>");
				}
				sb.append("</table>");
				sb.append(PaginationAndSorting.getPaginationTrippleGrid(request,totaRecord,noOfRow, pageNo));
				sb.append("</td></tr></table>");


				if(bOverride && bPanelForCG && !isAlreadyOverride)
				{
					//sb.append("<div class='row mt10' style='margin-left:-20px;'>");
					//sb.append("<div class='span10'><label class='checkbox inline'><input type='checkbox' name='chkOverride' id='chkOverride' value='1'/>Override the Status</label></div>");
					//sb.append("</div>");
					sb.append("<input type='hidden' id='txtOverride' value='1'/>");
				}
				else
				{
					sb.append("<input type='hidden' id='txtOverride' value='0'/>");
				}

				if(bPanelForCG)
					sb.append("<input type='hidden' id='txtPanel' value='1'/>");
				else
					sb.append("<input type='hidden' id='txtPanel' value='0'/>");

				if(bLoggedInUserPanelAttendees)
					sb.append("<input type='hidden' id='txtLoggedInUserPanelAttendees' value='1'/>");
				else
					sb.append("<input type='hidden' id='txtLoggedInUserPanelAttendees' value='0'/>");


				// Start ... Read / Write privilege for school
				if(userMaster.getEntityType()!=3 && jobOrder!=null && ((statusMaster!=null && statusMaster.getStatusShortName().equalsIgnoreCase("hird")) || (secondaryStatus!=null && secondaryStatus.getSecondaryStatusName().equals("Offer Ready"))))
				{
					List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findJobOrder(jobOrder);
					if(listSchoolInJobOrder!=null && listSchoolInJobOrder.size() > 1)
						sb.append("<input type='hidden' id='txtschoolCount' value='"+listSchoolInJobOrder.size()+"'/>");
				}
				// End ... Read / Write privilege for school

			}else{
				statusJSIFlag="JSI";
				sb.append("<table>");	
				sb.append("<tr>");				
				sb.append("<td colspan=6>"+Utility.getLocaleValuePropByKey("lblNoAttachJSI", locale)+"</td>");
				sb.append("</tr>");
				sb.append("<table>");	
			}
			
			List<StrengthOpportunitySummary> strengthOpportunitySummaries = strengthOpportunitySummaryDAO.findStrengthOpportunityByTeacherIdandJobId(teacherDetail, jobOrder);
			List<String> competencyList = strengthOpportunitySummaryDAO.getAllCompetencies();
			
			subs.append("<div id='summaryDiv' style='margin-top:10px;margin:auto;padding-top:15px;'>");
			
			subs.append("<table style='border:1px solid black;text-align:center;width:670px;'>");
			subs.append("<th style='border:1px solid black;text-align:center;background-color:#AEAEAE;'><b>"+Utility.getLocaleValuePropByKey("lblCompetencies", locale)+"</b></th>");
			subs.append("<th style='border:1px solid black;text-align:center;background-color:#AEAEAE;'><b>"+Utility.getLocaleValuePropByKey("strengths", locale)+"</b></th>");
			subs.append("<th style='border:1px solid black;text-align:center;background-color:#AEAEAE;'><b>"+Utility.getLocaleValuePropByKey("lblGrowthOpportunities", locale)+"</b></th>");
			
			int count = 0;
			String strength = "strength";
			String growth = "growth";
			String recommendReason = "";
			String remainingQuestions = "";
			if(strengthOpportunitySummaries!=null && strengthOpportunitySummaries.size()>0)
			{
				recommendReason = strengthOpportunitySummaries.get(0).getRecommendSchoolReason();
				remainingQuestions = strengthOpportunitySummaries.get(0).getQuestionForInterview();
				if(strengthOpportunitySummaries.get(0).getSchoolMaster()!=null)
				{
				//	subs.append("<script type='text/javascript'>document.getElementById('schoolIdCG').value='"+strengthOpportunitySummaries.get(0).getSchoolMaster().getSchoolName()+"'; $('#schoolIdCG').css('disabled','true');</script>");
					schoolName = strengthOpportunitySummaries.get(0).getSchoolMaster().getSchoolName()+"@@@"+strengthOpportunitySummaries.get(0).getSchoolMaster().getSchoolId();
				}
				
				for(String s : competencyList)
				{
					strength = strengthOpportunitySummaries.get(count).getStrength()!=null?strengthOpportunitySummaries.get(count).getStrength():"";
					growth = strengthOpportunitySummaries.get(count).getGrowthOpportunity()!=null?strengthOpportunitySummaries.get(count).getGrowthOpportunity():"";
					subs.append("<tr style='border:1px solid black;'>");
					subs.append("<td><b>"+s+"</b></td>");
					if(strength.length()>0)
					{
						//subs.append("<td id='td"+count+"'><div id='strength"+count+"' style='width:240px;overflow:auto;height:120px;text-align:center;'>"+strength+"</div></td>");
						subs.append("<td id='td"+count+"'><textarea name='strength"+count+"' id='strength"+count+"' class='form-control' maxlength='100' rows='5' style='width:240px;resize: none;' readonly>"+strength+"</textarea></td>");
					}
					else
					{
						subs.append("<td id='td"+count+"'><textarea name='strength"+count+"' id='strength"+count+"' class='form-control' maxlength='100' rows='5' style='width:240px;resize: none;'>"+strength+"</textarea></td>");
					}
					
					if(growth.length()>0)
					{
						//subs.append("<td id='td"+count+"'><div id='growth"+count+"' style='width:240px;overflow:auto;height:120px;text-align:center;'>"+growth+"</div></td>");
						subs.append("<td id='td"+count+"'><textarea name='growth"+count+"' id='growth"+count+"' class='form-control' maxlength='100' rows='5' style='width:240px;resize: none;' readonly>"+growth+"</textarea></td>");
					}
					else
					{
						subs.append("<td id='td"+count+"'><textarea name='growth"+count+"' id='growth"+count+"' class='form-control' maxlength='100' rows='5' style='width:240px;resize: none;'>"+growth+"</textarea></td>");
					}
					subs.append("</tr>");
					count++;
				}
			}
			else
			{
				count = 0;
				for(String s : competencyList)
				{
					subs.append("<tr style='border:1px solid black;'>");
					subs.append("<td><b>"+s+"</b></td>");
					subs.append("<td id='td"+count+"'><textarea name='strength"+count+"' id='strength"+count+"' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
					subs.append("<td id='td"+count+"'><textarea name='growth"+count+"' id='growth"+count+"' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
					subs.append("</tr>");
					count++;
				}
			}
			
			
			/*subs.append("<tr style='border:1px solid black;'>");
			subs.append("<td><b>Instructional Leadership</b></td>");
			subs.append("<td id='td3'><textarea name='strength1' id='strength1' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("<td id='td4'><textarea name='growth1' id='growth1' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("</tr>");
			
			subs.append("<tr style='border:1px solid black;'>");
			subs.append("<td><b>Personal Leadership</b></td>");
			subs.append("<td id='td5'><textarea name='strength2' id='strength2' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("<td id='td6'><textarea name='growth2' id='growth2' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("</tr>");
			
			subs.append("<tr style='border:1px solid black;'>");
			subs.append("<td><b>Cultural and  Community Leadership</b></td>");
			subs.append("<td id='td7'><textarea name='strength3' id='strength3' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("<td id='td8'><textarea name='growth3' id='growth3' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("</tr>");
			
			subs.append("<tr style='border:1px solid black;'>");
			subs.append("<td><b>Operational Management<b></td>");
			subs.append("<td id='td9'><textarea name='strength4' id='strength4' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("<td id='td10'><textarea name='growth4' id='growth4' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("</tr>");
			
			subs.append("<tr style='border:1px solid black;'>");
			subs.append("<td><b>Results Driven<b></td>");
			subs.append("<td id='td11'><textarea name='strength5' id='strength5' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("<td id='td12'><textarea name='growth5' id='growth5' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("</tr>");
*/			
			
			subs.append("</table>");
			
			
			subs.append("<table><tr>");
			subs.append("<td>");
			
		 
			// Recommended for <INSERT SCHOOL> for the following key reasons:
			subs.append("<div id='recommendSchool' style='margin-top:10px;width:670px;'>");
			sb.append("<div id='recomDiv' class='control-group' style='width:670px;margin-top:10px;margin-left:1px;'>");
			sb.append("<label><strong>"+Utility.getLocaleValuePropByKey("lblRecommendedForSchool", locale)+":</strong></label><br/>");
			sb.append("<style>#recomreason .jqte{width:670px !important;}</style>");
			sb.append("<style>#remquestion .jqte{width:670px !important;}</style>");
			if(isSuperAdminUser) sb.append("<style>#jWTeacherStatusNotesDiv .jqte{height:150px !important;width:80% !important;float:left;padding-top:2px;padding-bottom:10px;} #statusNotes .jqte{width:96% !important;height:100% !important;padding-top:0px;padding-bottom:0px;}</style>");
			if(recommendReason.length()>0)
			{
			sb.append("<div id='recomreason'  style='pointer-events: none;width:670px;'><textarea name='text1' id='text1' class='form-control' maxlength='500' rows='5' style='width:670px;resize: none;' readonly>"+recommendReason+"</textarea></div></div>");
			}
			else
			{
			sb.append("<div id='recomreason' style='width:670px;'><textarea name='text1' id='text1' class='form-control' maxlength='500' rows='5' style='width:670px;resize: none;'>"+recommendReason+"</textarea></div></div>");
			}
			
			sb.append("<div id='remQuesDiv' class='control-group' style='width:670px;margin-top:10px;margin-left:1px;'>");
			sb.append("<label><strong>"+Utility.getLocaleValuePropByKey("lblRemainingQuestion", locale)+":</strong></label><br/>");
			if(remainingQuestions.length()>0)
			{
			sb.append("<div id='remquestion'  style='pointer-events: none;width:670px;'><textarea name='text2' id='text2' class='form-control' maxlength='500' rows='5' style='width:670px;resize: none;' readonly>"+remainingQuestions+"</textarea></div></div>");
			}
			else
			{
				sb.append("<div id='remquestion' style='width:670px;'><textarea name='text2' id='text2' class='form-control' maxlength='500' rows='5' style='width:670px;resize: none;'>"+remainingQuestions+"</textarea></div></div>");
			}
			//sb.append("<style> .jqte{width:670px !important;}</style>");
			subs.append("</div>");
			subs.append("</td>");
			subs.append("</tr></table>");
			
			subs.append("</div>");
			
			Integer autoEmailTemp=null;

			if(!statusId.equals("0") ){
				//System.out.println("**** status ID****** 1111 :: "+statusId);
				autoEmailTemp       =  Integer.parseInt(statusId);
				StatusWiseAutoEmailSend s =  statusWiseAutoEmailSendDAO.findAutoEmailStatusByStatusIdDistrict(autoEmailTemp, districtMaster);

				if(s!=null){
					flag=true;
				}
			}

			if(!secondaryStatusId.equals("0")){
				//System.out.println("**********secondaryStatus**********222 "+secondaryStatusId);
				autoEmailTemp      =  Integer.parseInt(secondaryStatusId);
				
				List<JobCategoryWiseStatusPrivilege> jcwpsList = new ArrayList<JobCategoryWiseStatusPrivilege>();
				SecondaryStatus secondaryStatus2 = secondaryStatusDAO.findById(autoEmailTemp, false, false);
				
				jcwpsList = jobCategoryWiseStatusPrivilegeDAO.getFilterStatusByName(districtMaster, jobOrder.getJobCategoryMaster(), secondaryStatus2);

				if(jcwpsList!=null && jcwpsList.size()>0){
					//System.out.println("01"+jcwpsList.get(0).getSecondaryStatus().getSecondaryStatusId());
					if(jcwpsList.get(0).getAutoNotifyAll()!=null){
						if(jcwpsList.get(0).getAutoNotifyAll())
							flag = true;
					}else{
						System.out.println("<<<<<<<<<<< AutoNotifyAll is NULL >>>>>>>>>>>>>>>>");
					}
				}
				
				
				/*List<Integer>  secodryIds      =  secondaryStatusDAO.findBySecondryStatusName(districtMaster, autoEmailTemp);      
				if(secodryIds.size()>0){
					if(flag!=true)
						flag = statusWiseAutoEmailSendDAO.findBySSIDs(secodryIds);
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//System.out.println(" flag :>>>>>>>>>>>>>>>>>>>: "+flag);
		
		return sb.toString()+"##"+statusHDRFlag+"##"+statusHDRReq+"##"+flag+"##"+jobCategoryFlag+"##"+hiredByDate+"##"+statusJSIFlag+"##"+statusResend+"##"+subs.toString()+"##"+schoolName+"##"+statusWaived;
		}	

	public StatusMaster findStatusByShortName(List <StatusMaster> statusMasterList,String statusShortName)
	{
		StatusMaster status= null;
		try 
		{   if(statusMasterList!=null)
			for(StatusMaster statusMaster: statusMasterList){
				if(statusMaster.getStatusShortName().equalsIgnoreCase(statusShortName)){
					status=statusMaster;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(status==null)
			return null;
		else
			return status;
	}	

	public int getMaxFitScore(List<DistrictMaxFitScore> districtMaxFitScoreList,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		int fitScore=0;
		try {
			if(districtMaxFitScoreList!=null)
				for(DistrictMaxFitScore districtMaxFitScoreObj: districtMaxFitScoreList){
					if(statusMaster!=null){
						if(districtMaxFitScoreObj.getStatusMaster()!=null)
							if(districtMaxFitScoreObj.getStatusMaster().getStatusId().equals(statusMaster.getStatusId())){
								fitScore=districtMaxFitScoreObj.getMaxFitScore();
							}
					}else{
						if(districtMaxFitScoreObj.getSecondaryStatus()!=null)
							if(secondaryStatus.getSecondaryStatus_copy()!=null){
								if(districtMaxFitScoreObj.getSecondaryStatus().getSecondaryStatusId().equals(secondaryStatus.getSecondaryStatus_copy().getSecondaryStatusId())){
									fitScore=districtMaxFitScoreObj.getMaxFitScore();
								}
							}else{
								if(districtMaxFitScoreObj.getSecondaryStatus().getSecondaryStatusId().equals(secondaryStatus.getSecondaryStatusId())){
									fitScore=districtMaxFitScoreObj.getMaxFitScore();
								}
							}
					}

				}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fitScore;
	}

	/*============ Sekhar : Copy  Folder Method ================*/
	public TeacherStatusNotes editShowStatusNote(String teacherStatusNoteId)
	{
		//System.out.println("editShowStatusNote::::::::"+teacherStatusNoteId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userMaster	=	(UserMaster) session.getAttribute("userMaster");
		TeacherStatusNotes teacherStatusNotes 			= 	new TeacherStatusNotes();
		try{
			teacherStatusNotes= teacherStatusNotesDAO.findById(Integer.parseInt(teacherStatusNoteId), false, false);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return teacherStatusNotes;
	}

	/*============ Sekhar : Delete Status Note ================*/
	public TeacherStatusNotes deleteStatusNote(String teacherStatusNoteId,String deleteType)
	{
		//System.out.println(deleteType+":::::::::deleteStatusNote::::::::"+teacherStatusNoteId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		TeacherStatusNotes teacherStatusNotes 			= 	new TeacherStatusNotes();
		try 
		{
			try{
				teacherStatusNotes= teacherStatusNotesDAO.findById(Integer.parseInt(teacherStatusNoteId), false, false);
			}catch (Exception e) {
				e.printStackTrace();
			}	
			if(deleteType!=null){
				try{
					File file = new File(Utility.getValueOfPropByKey("communicationRootPath")+"statusNote/"+teacherStatusNotes.getTeacherDetail().getTeacherId()+"/"+teacherStatusNotes.getStatusNoteFileName());
					if(file.delete()){
						System.out.println(file.getName() + " is deleted!");
					}else{
						System.out.println("Delete operation is failed.");
					}
				}catch(Exception e){}	
				if(deleteType.equals("attach")){
					teacherStatusNotes.setStatusNoteFileName("");
					teacherStatusNotesDAO.updatePersistent(teacherStatusNotes);
				}else{
					teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherStatusNotes;
	}

	// Start ... Add Question

	@Transactional(readOnly=true)
	public String displayJobCategoryByDistrict(Integer districtID,String txtjobCategory)
	{
		StringBuffer buffer=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		Integer jobCategory=0;
		if(txtjobCategory!=null && !txtjobCategory.equals(""))
			jobCategory=Integer.parseInt(txtjobCategory);

		if(districtID>0)
		{
			DistrictMaster districtMaster=districtMasterDAO.findById(districtID, false, false);
			List<JobCategoryMaster> lstjobCategoryMasters=jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
			if(lstjobCategoryMasters.size()>0)
			{
				buffer.append("<select class='form-control' name='jobCategory' id='jobCategory' onchange='resetJobCategory();'>");
				buffer.append("<option selected='selected' value=''>"+Utility.getLocaleValuePropByKey("optAllCat", locale)+"</option>");
				for(JobCategoryMaster pojo:lstjobCategoryMasters)
					if(pojo!=null)
					{
						if(pojo.getJobCategoryId().equals(jobCategory))
							buffer.append("<option value='"+pojo.getJobCategoryId()+"' selected='selected'>"+pojo.getJobCategoryName()+"</option>");
						else
							buffer.append("<option value='"+pojo.getJobCategoryId()+"'>"+pojo.getJobCategoryName()+"</option>");
					}
				buffer.append("</select>");
			}
		}
		else
		{
			buffer.append("<select class='form-control' name='jobCategory' id='jobCategory' disabled='disabled' onchange='resetJobCategory();'>");
			buffer.append("<option selected='selected' value=''>"+Utility.getLocaleValuePropByKey("optAllCat", locale)+"</option>");
			buffer.append("</select>");
		}

		return buffer.toString();
	}

	@Transactional(readOnly=true)
	public String displaySecondaryStatusByDistrictOrJobCategory(Integer headQuarterId,Integer branchId,Integer districtId,Integer jobCategory)
	{
		//System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<:::::::::::::::::new::::::displaySecondaryStatusByDistrictOrJobCategory::::::::::::::::::::>>>>>>>>>>>>>>>>>>>");
		StringBuffer sb=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<SecondaryStatus> lstTreeStructure=	new ArrayList<SecondaryStatus>();
		
		DistrictMaster districtMaster=null;
		JobCategoryMaster jobCategoryMaster=null;
		BranchMaster branchMaster=null;
		HeadQuarterMaster headQuarterMaster=null;
		
		if(districtId!=null && districtId>0){
			districtMaster=districtMasterDAO.findById(districtId, false, false);
		}
		if(jobCategory!=null && jobCategory>0){
			jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategory, false, false);
		}
		if(branchId!=null && branchId>0){
			branchMaster=branchMasterDAO.findById(branchId, false, false);
		}
		if(headQuarterId!=null && headQuarterId>0){
			headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
		}
		
		if(districtId!=null && districtId>0 && jobCategory!=null && jobCategory>0 && branchId==null && headQuarterId==null){
			lstTreeStructure =	secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategory(districtMaster,jobCategoryMaster);
		}else if(districtId!=null && districtId>0 && branchId==null && headQuarterId==null){
			lstTreeStructure =	secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategory(districtMaster,null);
		}else if(districtId!=null && districtId>0 && jobCategory!=null && jobCategory>0 && branchId!=null){
			lstTreeStructure =	secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHead(headQuarterMaster,branchMaster,districtMaster,jobCategoryMaster);
		}else if(districtId!=null && districtId>0 && jobCategory!=null && jobCategory>0 && headQuarterId!=null){
			lstTreeStructure =	secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHead(headQuarterMaster,null,districtMaster,jobCategoryMaster);
		}else if(districtId!=null && districtId>0 && branchId!=null){
			lstTreeStructure =	secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHead(headQuarterMaster,branchMaster,districtMaster,null);
		}else if(districtId!=null && districtId>0 && headQuarterId!=null){
			lstTreeStructure =	secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHead(headQuarterMaster,null,districtMaster,null);
		}else if(branchId!=null && branchId>0 && jobCategoryMaster==null){
			lstTreeStructure =	secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHead(headQuarterMaster,branchMaster,null,null);
		}else if(branchId!=null && branchId>0 && jobCategoryMaster!=null){
			lstTreeStructure =	secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHead(headQuarterMaster,branchMaster,null,jobCategoryMaster);
		}else if(headQuarterId!=null && headQuarterId>0 && jobCategoryMaster==null){
			HeadQuarterMaster HeadQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
			lstTreeStructure =	secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHead(HeadQuarterMaster,null,null,null);
		}
		else if(jobCategoryMaster!=null && headQuarterId!=null && headQuarterId>0){
			lstTreeStructure =	secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHead(headQuarterMaster,null,null,jobCategoryMaster);
		}
		if(lstTreeStructure!=null && lstTreeStructure.size()>0)
		{
			sb.append("<ul>");
			if(lstTreeStructure!=null)
				for(SecondaryStatus tree: lstTreeStructure)
				{
					if(tree.getSecondaryStatus()==null)
					{
						sb.append("<li id='"+tree.getSecondaryStatusId()+"' class=\"folder\"  >"+tree.getSecondaryStatusName()+" ");
						if(tree.getChildren().size()>0)
						{
							sb.append(printChild(request, tree.getChildren(),tree));
						}
					}
				}
			sb.append("</ul>");
		}

		return sb.toString();
	}
	@Transactional(readOnly=false)
	public String saveOrUpdateQuestion(Integer questionId,Integer districtId,Integer jobCategoryId,Integer secondaryStatusId,String question,Integer skillAttributeId,Integer maxScore,Integer maxJSIScore,boolean isChecked,boolean isJSIChecked,String source,String strFileName,boolean isPanelChecked,boolean isPanelCheckedJSI,boolean isAutoUpdateStatusChecked, Integer checked_site_radio,String statusUpdateExpiryDate,String daUsersIds,boolean allSchoolAdm, boolean overrideAdm, boolean chkNotifyAll, Integer internalExternalOrBothCandidate)
	{
		//System.out.println("internalExternalOrBothCandidate======"+internalExternalOrBothCandidate);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
		int entityType=0;
		if(userMaster!=null){
			entityType=userMaster.getEntityType();
		}
		try{
			internalExternalOrBothCandidate=internalExternalOrBothCandidate!=0?internalExternalOrBothCandidate:null;
		}catch(Exception e){e.printStackTrace();}
		
		try{
			DistrictMaster districtMaster=null;
			JobCategoryMaster jobCategoryMaster=null;
			SecondaryStatus secondaryStatus=null;
			SkillAttributesMaster skillAttributesMaster=null;

			if(districtId!=null)
				districtMaster=districtMasterDAO.findById(districtId, false, false);

			if(jobCategoryId!=null)
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);

			if(secondaryStatusId!=null)
				secondaryStatus=secondaryStatusDAO.findById(secondaryStatusId, false, false);


			if(source!=null)
				if(!source.equals("2")){
					if(skillAttributeId!=null && skillAttributeId>0)
						skillAttributesMaster=skillAttributesMasterDAO.findById(skillAttributeId, false, false);

					if((source.equals("1") && ( skillAttributesMaster!=null || (question!=null && !question.equals("")))) || source.equals("0") ){
						StatusSpecificQuestions statusSpecificQuestions=new StatusSpecificQuestions();
						if(questionId!=null && questionId>0){
							statusSpecificQuestions=statusSpecificQuestionsDAO.findById(questionId, false, false);
						}else{
							statusSpecificQuestions.setDistrictId(districtMaster.getDistrictId());
							statusSpecificQuestions.setJobCategoryId(jobCategoryMaster.getJobCategoryId());

							statusSpecificQuestions.setSecondaryStatusId(secondaryStatus.getSecondaryStatusId());

							if(secondaryStatus!=null && secondaryStatus.getStatusMaster()!=null)
								statusSpecificQuestions.setStatusId(secondaryStatus.getStatusMaster().getStatusId());
							else
								statusSpecificQuestions.setStatusId(null);

							statusSpecificQuestions.setUserMaster(userMaster);
							statusSpecificQuestions.setCreatedDateTime(new Date());
							statusSpecificQuestions.setStatus("A");
						}

						statusSpecificQuestions.setQuestion(question);

						if(skillAttributesMaster!=null)
							statusSpecificQuestions.setSkillAttributesMaster(skillAttributesMaster);
						else
							statusSpecificQuestions.setSkillAttributesMaster(null);

						statusSpecificQuestions.setMaxScore(maxScore);
						statusSpecificQuestionsDAO.makePersistent(statusSpecificQuestions);
					}
					if(source.equals("1")){

						List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=jobCategoryWiseStatusPrivilegeDAO.getStatus(districtMaster, jobCategoryMaster, secondaryStatus);
						if(jobCategoryWiseStatusPrivileges.size()==1)
						{
							try 
							{
								JobCategoryWiseStatusPrivilege pojo=jobCategoryWiseStatusPrivileges.get(0);
								if(pojo!=null)
								{
									//System.out.println("pojo >>>>>>>>>>>>>>>>>>1>>>>>>>"+pojo.getJobCategoryStatusPrivilegeId());
									
									pojo.setStatusPrivilegeForSchools(isChecked);
									pojo.setPanel(isPanelChecked);
									pojo.setAutoUpdateStatus(isAutoUpdateStatusChecked);
									pojo.setUpdateStatusOption(checked_site_radio);

									if(daUsersIds!=null && !daUsersIds.equals(""))
										pojo.setEmailNotificationToSelectedDistrictAdmins(daUsersIds);
									else
										pojo.setEmailNotificationToSelectedDistrictAdmins(null);

									pojo.setEmailNotificationToAllSchoolAdmins(allSchoolAdm);
									
									pojo.setOverrideUserSettings(overrideAdm);
									
									pojo.setAutoNotifyAll(chkNotifyAll);

									if(strFileName!=null && !strFileName.equalsIgnoreCase(""))
										pojo.setInstructionAttachFileName(strFileName);
									if(statusUpdateExpiryDate!=null && !statusUpdateExpiryDate.equalsIgnoreCase("") && isAutoUpdateStatusChecked){
										try {
											pojo.setStatusUpdateExpiryDate(Utility.getCurrentDateFormart(statusUpdateExpiryDate));	
										} catch (Exception e) {
											// TODO: handle exception
										}
									}else{

										pojo.setStatusUpdateExpiryDate(null);
									}
									pojo.setInternalExternalOrBothCandidate(internalExternalOrBothCandidate);
									jobCategoryWiseStatusPrivilegeDAO.makePersistent(pojo);
									//System.out.println("pojoid=========="+pojo.getJobCategoryStatusPrivilegeId());
								}

							} catch (Exception e) 
							{
								e.printStackTrace();
							}
						}
					}
				}else if(source.equals("2")){
					List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=jobCategoryWiseStatusPrivilegeDAO.getStatus(districtMaster, jobCategoryMaster, secondaryStatus);
					if(jobCategoryWiseStatusPrivileges.size()==1)
					{
						try 
						{
							JobCategoryWiseStatusPrivilege pojo=jobCategoryWiseStatusPrivileges.get(0);
							if(pojo!=null)
							{
								//System.out.println("pojo >>>>>>>>>>>>>>>>>>2>>>>>>>"+pojo.getJobCategoryStatusPrivilegeId());
								
								pojo.setStatusPrivilegeForSchools(isJSIChecked);
								pojo.setPanel(isPanelCheckedJSI);
								pojo.setAutoUpdateStatus(isAutoUpdateStatusChecked);
								pojo.setUpdateStatusOption(checked_site_radio);
								pojo.setMaxValuePerJSIQuestion(maxJSIScore);
								if(daUsersIds!=null && !daUsersIds.equals(""))
									pojo.setEmailNotificationToSelectedDistrictAdmins(daUsersIds);
								else
									pojo.setEmailNotificationToSelectedDistrictAdmins(null);

								pojo.setEmailNotificationToAllSchoolAdmins(allSchoolAdm);
								
								pojo.setOverrideUserSettings(overrideAdm);

								pojo.setAutoNotifyAll(chkNotifyAll);
								
								if(statusUpdateExpiryDate!=null && !statusUpdateExpiryDate.equalsIgnoreCase("") && isAutoUpdateStatusChecked){
									try {
										pojo.setStatusUpdateExpiryDate(Utility.getCurrentDateFormart(statusUpdateExpiryDate));	
									} catch (Exception e) {
										// TODO: handle exception
									}
								}else{

									pojo.setStatusUpdateExpiryDate(null);
								}
								pojo.setInternalExternalOrBothCandidate(internalExternalOrBothCandidate);
								jobCategoryWiseStatusPrivilegeDAO.makePersistent(pojo);
								//System.out.println("pojoid=========="+pojo.getJobCategoryStatusPrivilegeId());
							}

						} catch (Exception e) 
						{
							e.printStackTrace();
						}
					}
				}
		}catch (Exception e) {
			// TODO: handle exception
			source="";
		}
		finally
		{
			return source;
		}
	}



	@Transactional(readOnly=true)
	public String getQuestionListByDistrictAndJobCategory(Integer districtId,Integer jobCategoryId,Integer secondaryStatusId,String noOfRow, String pageNo,String sortOrder,String sortOrderType, Boolean isFromManageNode)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		userMaster = (UserMaster)session.getAttribute("userMaster");
		StringBuffer sb =	new StringBuffer();
		List<Integer> jobCateList = new ArrayList<Integer>();
		
		try{

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------

			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("skillAttributeId")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("skillAttributeId")){
					sortOrderNoField="skillAttributeId";
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}

			DistrictMaster districtMaster=null;
			JobCategoryMaster jobCategoryMaster=null;
			SecondaryStatus secondaryStatus=null;

			if(districtId!=null && districtId>0)
				districtMaster=districtMasterDAO.findById(districtId, false, false);

			if(jobCategoryId!=null && jobCategoryId>0){
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				jobCateList.add(jobCategoryMaster.getJobCategoryId());
				if(jobCategoryMaster.getParentJobCategoryId()!=null && jobCategoryMaster.getParentJobCategoryId()!=null && jobCategoryMaster.getParentJobCategoryId().getJobCategoryId()>0){
					jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryMaster.getParentJobCategoryId().getJobCategoryId(), false, false);
					jobCateList.add(jobCategoryMaster.getJobCategoryId());
				}
			}

			if(secondaryStatusId!=null && secondaryStatusId>0)
				secondaryStatus=secondaryStatusDAO.findById(secondaryStatusId, false, false);

			Criterion criterion_district= Restrictions.eq("districtId",districtMaster.getDistrictId());
			Criterion criterion_jobCategory= Restrictions.in("jobCategoryId",jobCateList);
			Criterion criterion_secondaryStatus= Restrictions.eq("secondaryStatusId",secondaryStatus.getSecondaryStatusId());
			totalRecord = statusSpecificQuestionsDAO.getRowCount(criterion_district,criterion_jobCategory,criterion_secondaryStatus);
			List<StatusSpecificQuestions> lststatusSpecificQuestions=statusSpecificQuestionsDAO.findByDistAndJobOrder(sortOrderStrVal,start,noOfRowInPage,criterion_district,criterion_jobCategory,criterion_secondaryStatus);

			// Sorting for Reference data

			List<StatusSpecificQuestions> sortedStatusSpecificQuestions		=	new ArrayList<StatusSpecificQuestions>();

			SortedMap<String,StatusSpecificQuestions>	sortedMap = new TreeMap<String,StatusSpecificQuestions>();
			if(sortOrderNoField.equals("skillAttributeId"))
				sortOrderFieldName	=	"skillAttributeId";

			int mapFlag=2;
			for (StatusSpecificQuestions pojo : lststatusSpecificQuestions){
				String orderFieldName=""+pojo.getQuestionId();
				if(sortOrderFieldName.equals("skillAttributeId")){
					if(pojo.getSkillAttributesMaster()==null)
						orderFieldName=""+"||"+pojo.getQuestionId();
					else
						orderFieldName=pojo.getSkillAttributesMaster().getSkillName()+"||"+pojo.getQuestionId();
					sortedMap.put(orderFieldName+"||",pojo);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedStatusSpecificQuestions.add((StatusSpecificQuestions) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedStatusSpecificQuestions.add((StatusSpecificQuestions) sortedMap.get(key));
				}
			}else{
				sortedStatusSpecificQuestions=lststatusSpecificQuestions;
			}

			if(totalRecord<end)
				end=totalRecord;

			// End sorting for reference data

			String responseText="";

			String tablePadding="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:30px;text-align:left;'";
			sb.append("<table border='0' width='785' class='table-bordered'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingMLink_1(Utility.getLocaleValuePropByKey("lblQues", locale),sortOrderFieldName,"question",sortOrderTypeVal,pgNo);
			sb.append("<th align=\"left\" width='200' "+tablePadding+" >"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLink_1(Utility.getLocaleValuePropByKey("lblAttribute1", locale),sortOrderFieldName,"skillAttributeId",sortOrderTypeVal,pgNo);
			sb.append("<th align=\"left\" width='300' "+tablePadding+" >"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLink_1(Utility.getLocaleValuePropByKey("msgMaxScore", locale),sortOrderFieldName,"maxScore",sortOrderTypeVal,pgNo);
			sb.append("<th align=\"left\" width='80' "+tablePadding+" >"+responseText+"</th>");

			sb.append("<th align=\"left\" width='110' "+tablePadding+" >"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");

			sb.append("</tr>");
			sb.append("</thead>");

			if(lststatusSpecificQuestions.size()==0)
				sb.append("<tr><td colspan='4' align='left' style='padding-left:5px;'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );

			int iRecordCount=1;
			String tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
			String tablePadding_Data_right="style='padding-right:10px;vertical-align:top;text-align:right;font-weight: normal;line-height:30px;'";
			//sb.append("<tbody>");
			sb.append("<tbody><tr><td colspan='4'>");
			sb.append("<div class='scrollit'><table border='0' width='775' class='table_bordered_question'>");
			
			if(isFromManageNode!=null && isFromManageNode)
			{
				Boolean isEditableBySchoolAdmin=false;
				for (StatusSpecificQuestions pojo : sortedStatusSpecificQuestions) 
				{
					if(pojo.getUserMaster().getEntityType()!=3 || userMaster.getUserId().equals(pojo.getUserMaster().getUserId()) )
					{
						iRecordCount++;
						
						String gridColor="";
						if(iRecordCount%2==0)
							gridColor="class='bggrid'";
						
						if(pojo.getUserMaster().getEntityType()==3)
							isEditableBySchoolAdmin = true;
						else
							isEditableBySchoolAdmin = false;
						
						sb.append("<tr "+gridColor+">");
						
						sb.append("<td "+tablePadding_Data+" width='200'>"+pojo.getQuestion()+"</td>");
						
						if(pojo.getSkillAttributesMaster()!=null)
							sb.append("<td "+tablePadding_Data+" width='300'>"+pojo.getSkillAttributesMaster().getSkillName()+"</td>");
						else
							sb.append("<td "+tablePadding_Data+" width='300'>&nbsp;</td>");

						sb.append("<td "+tablePadding_Data_right+" width='80'>"+pojo.getMaxScore()+"</td>");
						
						sb.append("<td "+tablePadding_Data+" width='110'>");
						sb.append("<a href='#' onclick=\"return editQuestion('"+pojo.getQuestionId()+"','"+isEditableBySchoolAdmin+"')\" >Edit</a>");
						
						sb.append(" | ");
						if(pojo.getStatus().equalsIgnoreCase("A"))
							sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateQuestion("+pojo.getQuestionId()+",'I','"+isEditableBySchoolAdmin+"')\" >Deactivate");
						else
							sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateQuestion("+pojo.getQuestionId()+",'A','"+isEditableBySchoolAdmin+"')\" >Activate");
						
						sb.append("</td>");
						sb.append("</tr>" );
					}
				}
			}
			else
			{
			for (StatusSpecificQuestions pojo : sortedStatusSpecificQuestions) 
			{
				iRecordCount++;

				String gridColor="";
				if(iRecordCount%2==0)
					gridColor="class='bggrid'";

				sb.append("<tr "+gridColor+">");
				sb.append("<td "+tablePadding_Data+" width='200'>"+pojo.getQuestion()+"</td>");
				if(pojo.getSkillAttributesMaster()!=null)
					sb.append("<td "+tablePadding_Data+" width='300'>"+pojo.getSkillAttributesMaster().getSkillName()+"</td>");
				else
					sb.append("<td "+tablePadding_Data+" width='300'>&nbsp;</td>");

				sb.append("<td "+tablePadding_Data_right+" width='80'>"+pojo.getMaxScore()+"</td>");

				sb.append("<td "+tablePadding_Data+" width='110'>");
				sb.append("<a href='#' onclick=\"return editQuestion('"+pojo.getQuestionId()+"')\">Edit</a>");
				sb.append(" | ");
				if(pojo.getStatus().equalsIgnoreCase("A"))
					sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateQuestion("+pojo.getQuestionId()+",'I')\">Deactivate");
				else
					sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateQuestion("+pojo.getQuestionId()+",'A')\">Activate");
				sb.append("</td>");

				sb.append("</tr>" );
			}
			}
			
			sb.append("</table></div>");
			sb.append("</td></tr></tbody>");

			if(totalRecord>10){
				sb.append("<tr><td colspan='4' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;line-height:30px;'>");
				sb.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow,pageNo));
				sb.append("</td></tr>");
			}
			sb.append("</table>");

		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return sb.toString();
		}

	}


	@Transactional(readOnly=false)
	public StatusSpecificQuestions editQuestion(Integer questionId)
	{
		StringBuffer sb=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		StatusSpecificQuestions statusSpecificQuestions=new StatusSpecificQuestions();
		if(questionId>0)
			statusSpecificQuestions=statusSpecificQuestionsDAO.findById(questionId, false, false);

		return statusSpecificQuestions;
	}


	@Transactional(readOnly=false)
	public String activateDeactivateQuestion(Integer questionId,String status)
	{
		StringBuffer sb=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		StatusSpecificQuestions statusSpecificQuestions=new StatusSpecificQuestions();
		if(questionId!=null && questionId>0)
		{
			statusSpecificQuestions=statusSpecificQuestionsDAO.findById(questionId, false, false);
			statusSpecificQuestions.setStatus(status);
			statusSpecificQuestionsDAO.makePersistent(statusSpecificQuestions);
		}
		return sb.toString();
	}

	@Transactional(readOnly=false)
	public String copyDataFromDistrictToJobCategory(Integer headQuarterId,Integer branchId,Integer districtId,Integer jobCategory)
	{
		//System.out.println("::::::::::::::::::::::copyDataFromDistrictToJobCategory:::::::::::::::::::::::::::::::");
		String sReturnValue="";
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");

		DistrictMaster districtMaster=null;
		JobCategoryMaster jobCategoryMaster=null;
		BranchMaster branchMaster=null;
		HeadQuarterMaster headQuarterMaster=null;
		
		if(districtId!=null && districtId>0){
			districtMaster=districtMasterDAO.findById(districtId, false, false);
		}
		if(jobCategory!=null && jobCategory>0){
			jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategory, false, false);
		}
		if(branchId!=null && branchId>0){
			branchMaster=branchMasterDAO.findById(branchId, false, false);
		}
		if(headQuarterId!=null && headQuarterId>0){
			headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
		}
		List<SecondaryStatus> lstSecondaryStatus=secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHead(headQuarterMaster,branchMaster,districtMaster,jobCategoryMaster);
		//System.out.println("lstSecondaryStatus::::::::Copy:::::::"+lstSecondaryStatus.size());
		if(lstSecondaryStatus.size()==0){
		//	secondaryStatusDAO.copyDataFromDistrictToJobCategory(districtMaster,jobCategoryMaster,userMaster);
			secondaryStatusDAO.copyDataFromDistrictToJobCategoryBanchAndHead(headQuarterMaster,branchMaster,districtMaster,jobCategoryMaster,userMaster);
			sReturnValue="copy";
		}
		else
		{
			//System.out.println("No need to copy ...");
		}
		return sReturnValue;
	}


	@Transactional(readOnly=false)
	public int updateStatusSpecificScore(Integer[] answerId_array,Integer[] score_array,Integer finalizeStatus)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		// Start Update Answer Score
		int iReturnValue=0;
		if(answerId_array!=null && answerId_array.length>0)
		{
			for(int i=0; i<answerId_array.length;i++)
			{
				try {
					StatusSpecificScore statusSpecificScore=statusSpecificScoreDAO.findById(answerId_array[i], false, false);
					if(statusSpecificScore!=null)
					{
						if(statusSpecificScore.getFinalizeStatus()==0)
						{
							statusSpecificScore.setFinalizeStatus(finalizeStatus);
							statusSpecificScore.setScoreProvided(score_array[i]);
							statusSpecificScoreDAO.makePersistent(statusSpecificScore);
							statusSpecificScoreDAO.flush();
							statusSpecificScoreDAO.clear();
							iReturnValue++;
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		// End Update Answer Score
		return iReturnValue;
	}

	@Transactional(readOnly=false)
	public JobCategoryWiseStatusPrivilege getJobCategoryWiseStatusPrivilege(Integer districtId,Integer jobCategoryId,Integer secondaryStatusId)
	{
		boolean isChecked=false;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
		JobCategoryWiseStatusPrivilege jobCWSP=new JobCategoryWiseStatusPrivilege();

		DistrictMaster districtMaster=null;
		JobCategoryMaster jobCategoryMaster=null;
		SecondaryStatus secondaryStatus=null;
		List<JobCategoryMaster> jobCateList = new ArrayList<JobCategoryMaster>();
		
		if(districtId!=null)
			districtMaster=districtMasterDAO.findById(districtId, false, false);

		if(jobCategoryId!=null)
			jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);

		if(secondaryStatusId!=null)
			secondaryStatus=secondaryStatusDAO.findById(secondaryStatusId, false, false);

		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=jobCategoryWiseStatusPrivilegeDAO.getStatus(districtMaster, jobCategoryMaster, secondaryStatus);
		if(jobCategoryWiseStatusPrivileges.size()==0){
			List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_ss=Restrictions.eq("secondaryStatus",secondaryStatus.getSecondaryStatus_copy());
			Criterion criterion_status=Restrictions.eq("statusMaster",secondaryStatus.getStatusMaster());

			if(secondaryStatus.getStatusMaster()!=null)
				lstStatusPForSchool = statusPrivilegeForSchoolsDAO.findByCriteria(criterion,criterion_status);
			else
				lstStatusPForSchool = statusPrivilegeForSchoolsDAO.findByCriteria(criterion,criterion_ss);

			if(lstStatusPForSchool.size()==1){
				if(lstStatusPForSchool.get(0).getCanSchoolSetStatus()==1)
					isChecked=true;
				else
					isChecked=false;
			}else{
				isChecked=true;
			}

			try {
				//	JobCategoryWiseStatusPrivilege pojo=new JobCategoryWiseStatusPrivilege();
				jobCWSP.setDistrictMaster(districtMaster);
				jobCWSP.setJobCategoryMaster(jobCategoryMaster);
				if(secondaryStatus.getStatusMaster()!=null)
					jobCWSP.setStatusMaster(secondaryStatus.getStatusMaster());
				if(secondaryStatus!=null)
					jobCWSP.setSecondaryStatus(secondaryStatus);
				jobCWSP.setStatusPrivilegeForSchools(isChecked);
				jobCWSP.setUserMaster(userMaster);
				jobCWSP.setCreatedDateTime(new Date());
				jobCategoryWiseStatusPrivilegeDAO.makePersistent(jobCWSP);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			//isChecked=jobCategoryWiseStatusPrivileges.get(0).isStatusPrivilegeForSchools();
			jobCWSP=jobCategoryWiseStatusPrivileges.get(0);
		}

		return jobCWSP;	

	}



	public boolean isQuestionAdd(Integer secondaryStatusId)
	{
		boolean isQuestionAdd=true;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try 
		{
			SecondaryStatus secondaryStatus= secondaryStatusDAO.findById(secondaryStatusId, false, false);
			if(secondaryStatus!=null)
			{
				if(secondaryStatus.getStatusMaster()!=null)
					if(secondaryStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("apl"))
						isQuestionAdd=false;

				if(secondaryStatus.getStatusNodeMaster()!=null)
					isQuestionAdd=false;
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			return isQuestionAdd;
		}
	}

	// End ... Add Question 

	public String downloadAttachInstructionFile(String filePath,String fileName)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String path="";

		try 
		{
			String source = Utility.getValueOfPropByKey("statusInstructionRootPath")+filePath+"/"+fileName;
			String target = context.getServletContext().getRealPath("/")+"/statusInstruction/"+filePath+"/";
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+"/statusInstruction/"+filePath+"/"+sourceFile.getName();
		} 
		catch (Exception e) 
		{
			System.out.println("Exception raise in downloadAttachInstructionFile method and Class Name is ManageStatusAjax and Error is "+e.getMessage());
			System.out.println(new Date() +" filePath "+filePath +" fileName "+fileName);
			path="";
		}
		return path;
	}

	@Transactional(readOnly=false)
	public JobCategoryWiseStatusPrivilege removeInstructionFile(Integer jobCategoryStatusPrivilegeId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		JobCategoryWiseStatusPrivilege jobCategoryWiseStatusPrivilege=new JobCategoryWiseStatusPrivilege();
		if(jobCategoryStatusPrivilegeId!=null && jobCategoryStatusPrivilegeId>0)
		{
			jobCategoryWiseStatusPrivilege=jobCategoryWiseStatusPrivilegeDAO.findById(jobCategoryStatusPrivilegeId, false, false);
			if(jobCategoryWiseStatusPrivilege!=null)
			{
				jobCategoryWiseStatusPrivilege.setInstructionAttachFileName(null);
				jobCategoryWiseStatusPrivilegeDAO.makePersistent(jobCategoryWiseStatusPrivilege);

				String filePath=Utility.getValueOfPropByKey("statusInstructionRootPath")+"/"+jobCategoryWiseStatusPrivilege.getSecondaryStatus().getSecondaryStatusId()+"/";
				File f=new File(filePath);
				if(f.exists())
				{
					String[] oldFilePaths;
					oldFilePaths = f.list();
					for(String oldFileName:oldFilePaths)
					{
						File fileOld=new File(filePath+"/"+oldFileName);
						if(fileOld.exists()){
							fileOld.delete();
						}
					}	
				}
			}
		}
		return jobCategoryWiseStatusPrivilege;	
	}

	/* @Author: Gagan 
	 * @Discription: It is used to get Hr information for Users that will used in Forgot Password email footer.
	 */
	public String[] getHrDetailToTeacher(HttpServletRequest request,JobOrder jobOrder)
	{
		//System.out.println("\n Method getHrDetailToTeacher in Manage Status Ajax ");
		//HttpSession session = request.getSession(false);
		String schoolDistrictName =	"";
		int isHrContactExist = 0;
		String hrContactFirstName = "";
		String hrContactLastName = "";
		String hrContactEmailAddress = "";
		String title 			 = "";
		String phoneNumber		 = "";
		String dmName 			 = "";
		String dmEmailAddress = "";
		String dmPhoneNumber	 = "";
		String[] arrHrDetail	= new String[15]; 

		List<DistrictKeyContact> dkclist = new ArrayList<DistrictKeyContact>();
		List<SchoolKeyContact> skclist = new ArrayList<SchoolKeyContact>();

		ContactTypeMaster ctm 	= new ContactTypeMaster();

		try{
			ctm	=	contactTypeMasterDAO.findById(1, false, false);
			Criterion criterion1 = Restrictions.eq("keyContactTypeId", ctm);
			Criterion criterion2 = null;


			if(jobOrder.getCreatedForEntity()==2)
			{
				schoolDistrictName		=	jobOrder.getDistrictMaster().getDistrictName()==null?"":jobOrder.getDistrictMaster().getDistrictName();
				criterion2				=	Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
				dkclist					=	districtKeyContactDAO.findByCriteria(Order.asc("keyContactId"),criterion1,criterion2);

				if(dkclist.size()>0)
				{
					isHrContactExist = 1;
					if(dkclist.get(0).getKeyContactFirstName()!=null)
						hrContactFirstName = dkclist.get(0).getKeyContactFirstName();

					if(dkclist.get(0).getKeyContactLastName()!=null)
						hrContactLastName = dkclist.get(0).getKeyContactLastName();

					if(dkclist.get(0).getKeyContactEmailAddress()!=null)
						hrContactEmailAddress = dkclist.get(0).getKeyContactEmailAddress();

					if(dkclist.get(0).getKeyContactPhoneNumber()!=null)
						phoneNumber	= dkclist.get(0).getKeyContactPhoneNumber();

					if(dkclist.get(0).getKeyContactTitle()!=null)
						title = dkclist.get(0).getKeyContactTitle();

				}
				else
				{
					//System.out.println(" [ ---- Manage Status Ajax ---- ] DA Final Decision Maker Information [ If Hr is not Available ]");
					if(jobOrder.getDistrictMaster().getDmName()!=null)
					{
						isHrContactExist=2;
						dmName	=	jobOrder.getDistrictMaster().getDmName();

						if(jobOrder.getDistrictMaster().getDmEmailAddress()!=null)
							dmEmailAddress	=	jobOrder.getDistrictMaster().getDmEmailAddress();

						if(jobOrder.getDistrictMaster().getDmPhoneNumber()!=null)
							dmPhoneNumber	=	jobOrder.getDistrictMaster().getDmPhoneNumber();
					}
				}
			}
			else
			{
				if(jobOrder.getCreatedForEntity()==3)
				{
					schoolDistrictName		=	jobOrder.getSchool().get(0).getSchoolName()==null?"":jobOrder.getSchool().get(0).getSchoolName();
					criterion2				=	Restrictions.eq("schoolId", jobOrder.getSchool().get(0).getSchoolId());
					skclist					=	schoolKeyContactDAO.findByCriteria(Order.asc("keyContactId"),criterion1,criterion2);

					if(skclist.size()>0)
					{
						isHrContactExist = 1;
						if(skclist.get(0).getKeyContactFirstName()!=null)
							hrContactFirstName = skclist.get(0).getKeyContactFirstName();

						if(skclist.get(0).getKeyContactLastName()!=null)
							hrContactLastName = skclist.get(0).getKeyContactLastName();

						if(dkclist.get(0).getKeyContactEmailAddress()!=null)
							hrContactEmailAddress = dkclist.get(0).getKeyContactEmailAddress();

						if(skclist.get(0).getKeyContactPhoneNumber()!=null)
							phoneNumber	= skclist.get(0).getKeyContactPhoneNumber();

						if(skclist.get(0).getKeyContactTitle()!=null)
							title = skclist.get(0).getKeyContactTitle();
					}
					else
					{
						//System.out.println(" [Manage Status Ajax  SA Final Decision Maker Information [ If Hr is not Available ]");
						if(jobOrder.getSchool().get(0).getDmName()!=null)
						{
							isHrContactExist=2;
							dmName	=	jobOrder.getSchool().get(0).getDmName();

							if(jobOrder.getSchool().get(0).getDmEmailAddress()!=null)
								dmEmailAddress	=	jobOrder.getSchool().get(0).getDmEmailAddress();

							if(jobOrder.getSchool().get(0).getDmPhoneNumber()!=null)
								dmPhoneNumber	=	jobOrder.getSchool().get(0).getDmPhoneNumber();
						}
					}
				}
			}

			arrHrDetail[0]	= hrContactFirstName;
			arrHrDetail[1]	= hrContactLastName;
			arrHrDetail[2]	= hrContactEmailAddress;	
			arrHrDetail[3]	= phoneNumber;
			arrHrDetail[4]	= title;
			arrHrDetail[5]	= ""+isHrContactExist;
			arrHrDetail[6]	= schoolDistrictName;
			arrHrDetail[7]	= dmName;
			arrHrDetail[8]	= dmEmailAddress;
			arrHrDetail[9]	= dmPhoneNumber;
			String JobBoardURL="";
			try{
				JobBoardURL=Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?districtId="+Utility.encryptNo(jobOrder.getDistrictMaster().getDistrictId()));
			}catch (Exception e) {
				// TODO: handle exception
			}
			arrHrDetail[12]	= JobBoardURL;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return arrHrDetail;
	}

	public List<SchoolInJobOrder> getSchoolInJobOrder(String jobOrderId,String SchoolName)
	{
		List<SchoolInJobOrder> schoolInJobOrders=new ArrayList<SchoolInJobOrder>();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			Integer iJobOrderId=Integer.parseInt(jobOrderId);
			if(iJobOrderId > 0)
				schoolInJobOrders=schoolInJobOrderDAO.getSchoolInJobOrder(iJobOrderId,SchoolName,0,25);
		}catch (Exception e){
			e.printStackTrace();
		}
		finally{
			return schoolInJobOrders;
		}
	}
	public List<SchoolInJobOrder> getSchoolInJobOrderForHired(int jobId,String SchoolName,int teacherId)
	{
		//System.out.println("::::::::::getSchoolInJobOrderForHired::::::::::");
		//System.out.println("teacherId:::::"+teacherId);
		List<SchoolInJobOrder> schoolInJobOrders=new ArrayList<SchoolInJobOrder>();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			if(jobId > 0)
				schoolInJobOrders=schoolInJobOrderDAO.getSchoolInJobOrder(jobId,SchoolName,0,25);
		}catch (Exception e){
			e.printStackTrace();
		}
		finally{
			return schoolInJobOrders;
		}
	}
	public static void debugPrintln(String strInput)
	{
		PrintOnConsole.debugPrintln("",strInput);
	}

	public static void debugPrintln(String strModuleName,String strInput)
	{
		PrintOnConsole.debugPrintln(strModuleName, strInput);
	}

	public static int sliderDisplayStatus(int dslider,boolean bPanelForCG,boolean bLoggedInUserPanelAttendees,boolean bOverride)
	{
		debugPrintln("Initial Values ","dslider "+dslider+" bPanelForCG "+bPanelForCG+" bLoggedInUserPanelAttendees "+bLoggedInUserPanelAttendees +" bOverride "+bOverride);
		if(bPanelForCG)
		{
			if(bLoggedInUserPanelAttendees)
				dslider=1;
			else
				dslider=0;
		}

		if(bOverride)
			dslider=1;

		debugPrintln("After Slider ", ""+dslider);

		return dslider;
	}


	public static String getStatusColor(List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob,List<JobWisePanelStatus> jobWisePanelStatusList,List<PanelAttendees> panelAttendees,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster,List<TeacherStatusScores> lstTeacherStatusScores,List<TeacherStatusNotes> lstTeacherStatusNotes,List<PanelSchedule> panelScheduleList)
	{
		//System.out.println(":::::::::::getStatusColor::::::::::");
		Map<Integer, Boolean> mapUserScoreOrNote=new HashMap<Integer, Boolean>();

		String sReturnValue="Default";
		boolean bPanelAttached=false;
		boolean bFinalizeAllPanelMember=false;
		boolean bFinalizePartialPanelMember=false;
		boolean bFinalizeLoggedInPanelMember=false;
		boolean bPanelMemberIsAttached=false;
		boolean bOverrideByHROrDA=false;

		int iPanelAttendeesCount=panelAttendees.size();
		if(panelAttendees.size() > 0)
			for(PanelAttendees panelAttendees2:panelAttendees)
				if(panelAttendees2!=null)
					if(panelAttendees2.getPanelInviteeId()!=null && mapUserScoreOrNote.get(panelAttendees2.getPanelInviteeId().getUserId())==null)
						mapUserScoreOrNote.put(panelAttendees2.getPanelInviteeId().getUserId(), new Boolean(false));
		//if(statusMaster!=null)
			////System.out.println("statusMaster:::-----------------------------:>"+statusMaster.getStatusId());
		if(panelScheduleList.size() > 0)
		{
			if(jobWisePanelStatusList.size() > 0)
			{
				for(JobWisePanelStatus pojo:jobWisePanelStatusList)
				{
					////System.out.println("pojo-------------------------------::::"+pojo.getJobPanelStatusId());
					if(statusMaster!=null)
					{
						if(pojo.getSecondaryStatus()!=null && pojo.getSecondaryStatus().getSecondaryStatusId().equals(secondaryStatus.getSecondaryStatusId()) && pojo.getPanelStatus())
						{
							bPanelAttached=true;
							if(panelAttendees.size() > 0)
							{
								for(PanelAttendees pojo1:panelAttendees)
								{
									if(lstTeacherStatusHistoryForJob.size() > 0)
									{
										for(TeacherStatusHistoryForJob pojo2:lstTeacherStatusHistoryForJob)
										{
											if(pojo2.getStatusMaster()!=null && pojo2.getStatusMaster().getStatusId().equals(statusMaster.getStatusId()))
											{
												if(pojo2.getOverride()!=null && pojo2.getOverride().equals(true))
													bOverrideByHROrDA=true;

												for(TeacherStatusScores teacherStatusScores :lstTeacherStatusScores)
													if(teacherStatusScores!=null && teacherStatusScores.getStatusMaster()!=null && teacherStatusScores.getStatusMaster().getStatusId().equals(statusMaster.getStatusId()) && mapUserScoreOrNote.get(pojo1.getPanelInviteeId().getUserId())!=null && teacherStatusScores.getUserMaster().getUserId().equals(pojo1.getPanelInviteeId().getUserId()) && teacherStatusScores.isFinalizeStatus())
														mapUserScoreOrNote.put(pojo1.getPanelInviteeId().getUserId(), new Boolean(true));

												for(TeacherStatusNotes teacherStatusNotes :lstTeacherStatusNotes)
													if(teacherStatusNotes!=null && teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusId().equals(statusMaster.getStatusId()) && mapUserScoreOrNote.get(pojo1.getPanelInviteeId().getUserId())!=null && teacherStatusNotes.getUserMaster().getUserId().equals(pojo1.getPanelInviteeId().getUserId()) && teacherStatusNotes.isFinalizeStatus())
														mapUserScoreOrNote.put(pojo1.getPanelInviteeId().getUserId(), new Boolean(true));

											}
										}
									}
								}// for
							}
						}// IF
					}
					else
					{
						if(pojo.getSecondaryStatus()!=null && pojo.getSecondaryStatus().getSecondaryStatusId().equals(secondaryStatus.getSecondaryStatusId()) && pojo.getPanelStatus())
						{
							bPanelAttached=true;
							if(panelAttendees.size() > 0)
							{
								for(PanelAttendees pojo1:panelAttendees)
								{
									if(lstTeacherStatusHistoryForJob.size() > 0)
									{
										for(TeacherStatusHistoryForJob pojo2:lstTeacherStatusHistoryForJob)
										{
											if(pojo2.getSecondaryStatus()!=null && pojo2.getSecondaryStatus().getSecondaryStatusId().equals(secondaryStatus.getSecondaryStatusId()))
											{
												if(pojo2.getOverride()!=null && pojo2.getOverride().equals(true))
													bOverrideByHROrDA=true;

												for(TeacherStatusScores teacherStatusScores :lstTeacherStatusScores)
													if(teacherStatusScores!=null && teacherStatusScores.getSecondaryStatus()!=null && teacherStatusScores.getSecondaryStatus().getSecondaryStatusId().equals(secondaryStatus.getSecondaryStatusId()) && mapUserScoreOrNote.get(pojo1.getPanelInviteeId().getUserId())!=null && teacherStatusScores.getUserMaster().getUserId().equals(pojo1.getPanelInviteeId().getUserId())){
														mapUserScoreOrNote.put(pojo1.getPanelInviteeId().getUserId(), new Boolean(true));
													}
												for(TeacherStatusNotes teacherStatusNotes :lstTeacherStatusNotes)
													if(teacherStatusNotes!=null && teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusId().equals(secondaryStatus.getSecondaryStatusId()) && mapUserScoreOrNote.get(pojo1.getPanelInviteeId().getUserId())!=null && teacherStatusNotes.getUserMaster().getUserId().equals(pojo1.getPanelInviteeId().getUserId())){
														mapUserScoreOrNote.put(pojo1.getPanelInviteeId().getUserId(), new Boolean(true));
													}

											}
										}
									}
								}// for
							}
						}// IF
					}
				}
			}
		}

		int iFinalizeAllPanelMember=0;

		Iterator iterUserScoreOrNote = mapUserScoreOrNote.entrySet().iterator();
		while (iterUserScoreOrNote.hasNext()) {
			Map.Entry mEntryUserScoreOrNote = (Map.Entry) iterUserScoreOrNote.next();

			if(mEntryUserScoreOrNote.getValue().equals(new Boolean(true)))
			{
				//bFinalizeAllPanelMember=true;
				//bFinalizePartialPanelMember=true;
				iFinalizeAllPanelMember++;
			}
			/*else
				bFinalizeAllPanelMember=false;*/

			if(iPanelAttendeesCount > 0 && iPanelAttendeesCount==iFinalizeAllPanelMember)
				bFinalizeAllPanelMember=true;
			else if(iPanelAttendeesCount > 0 && iFinalizeAllPanelMember > 0)
				bFinalizePartialPanelMember=true;

			if(mEntryUserScoreOrNote.getKey()!=null && mEntryUserScoreOrNote.getKey().equals(userMaster.getUserId()))
				bPanelMemberIsAttached=true;

			if(mEntryUserScoreOrNote.getKey()!=null && mEntryUserScoreOrNote.getKey().equals(userMaster.getUserId()) && mEntryUserScoreOrNote.getValue().equals(new Boolean(true)))
				bFinalizeLoggedInPanelMember=true;
		}
		//System.out.println(bPanelMemberIsAttached+" "+bFinalizeAllPanelMember+" "+bFinalizeAllPanelMember);
		if(bOverrideByHROrDA)
			sReturnValue="Green";
		else if(bPanelAttached)
		{
			if( (bPanelMemberIsAttached && bFinalizeAllPanelMember ) || bFinalizeAllPanelMember)
				sReturnValue="Green";
			else if(bPanelMemberIsAttached && bFinalizePartialPanelMember && bFinalizeLoggedInPanelMember)
				sReturnValue="GreenWithGreyDot";
			else if(bPanelMemberIsAttached && bFinalizePartialPanelMember && !bFinalizeLoggedInPanelMember)
				sReturnValue="GreyWithGreenDot";
			else
				sReturnValue="Grey";
		}
		//System.out.println("Method getStatusColor bOverrideByHROrDA "+bOverrideByHROrDA+" bPanelAttached "+bPanelAttached+" bPanelMemberIsAttached "+bPanelMemberIsAttached +" bFinalizeAllPanelMember "+bFinalizeAllPanelMember +" bFinalizePartialPanelMember "+bFinalizePartialPanelMember+" bFinalizeLoggedInPanelMember "+bFinalizeLoggedInPanelMember +" sReturnValue "+sReturnValue);
		return sReturnValue;
	}

	public static boolean[] isVisiableScoreSliderAndIcons(List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob,List<JobWisePanelStatus> jobWisePanelStatusList,List<PanelAttendees> panelAttendees,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster,List<TeacherStatusScores> teacherStatusScore,List<TeacherStatusNotes> lstTeacherStatusNotes,boolean panel,boolean login_panelMember,boolean bOverride)
	{
	
		boolean bReturnValueArray[]=new boolean[2];
		boolean alreadyFinalise=false;

		if(teacherStatusScore.size()==1 && teacherStatusScore.get(0).isFinalizeStatus()){
			alreadyFinalise=true;
			debugPrintln("isVisiableScoreSliderAndIcons"," TeacherStatusScore alreadyFinalise ^^^^^ 0 ^^^^^");
		}
		debugPrintln("isVisiableScoreSliderAndIcons ", "bOverride "+bOverride+" panel "+panel+" login_panelMember "+login_panelMember);
		if(!panel)
		{
			bReturnValueArray[0]=true;
			debugPrintln("isVisiableScoreSliderAndIcons"," ^^^^^ 2 ^^^^^");
		}
		if(bOverride)
		{
			bReturnValueArray[0]=true;
			debugPrintln("isVisiableScoreSliderAndIcons"," ^^^^^ 3 ^^^^^");
		}
		else if(panel && login_panelMember)
		{
			bReturnValueArray[0]=true;
			debugPrintln("isVisiableScoreSliderAndIcons"," ^^^^^ 4 ^^^^^");
		}
		else
		{
			bReturnValueArray[0]=false;
			debugPrintln("isVisiableScoreSliderAndIcons"," ^^^^^ 5 ^^^^^");
		}

		if(!panel)
		{
			bReturnValueArray[1]=true;
			debugPrintln("isVisiableScoreSliderAndIcons"," ^^^^^ 6 ^^^^^");
		}
		else if(panel && alreadyFinalise)
		{
			bReturnValueArray[1]=true;
			debugPrintln("isVisiableScoreSliderAndIcons"," ^^^^^ 7 ^^^^^");
		}
		else if(panel && login_panelMember && alreadyFinalise)
		{
			bReturnValueArray[1]=true;
			debugPrintln("isVisiableScoreSliderAndIcons"," ^^^^^ 8 ^^^^^");
		}
		else
		{
			bReturnValueArray[1]=false;
			debugPrintln("isVisiableScoreSliderAndIcons"," ^^^^^ 9 ^^^^^");
		}

		return bReturnValueArray;
	}

	public String OnlyShowInstructionAttacheFileNameWithOutSlider(boolean bShowInstructionAttacheFileName,String strShowInstructionAttacheFileName,List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges)
	{
		StringBuffer sb=new StringBuffer();
		if(bShowInstructionAttacheFileName)
		{
			sb.append("<tr><td align=left bgcolor=white>");
			sb.append("<table border=0><tr><td style='vertical-align:top;width:580px;'>&nbsp;</td>");
			sb.append("<td style='padding-left:5px;vertical-align:bottom;width:42px;'>&nbsp;</td>");
			sb.append("<td valign=top style='padding-top:4px;' align='right'>");
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			String fileName=strShowInstructionAttacheFileName;
			String filePath=""+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusId();
			sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewInst", locale)+"' rel='tooltip' id='attachInstructionFileName' href='javascript:void(0);' onclick=\"downloadAttachInstructionFileName('"+filePath+"','"+fileName+"','"+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusName()+"');"+windowFunc+"\"><span class='icon-info-sign icon-large iconcolor'></span></a>");
			sb.append("</td></tr>");
			sb.append("</table>");
			sb.append("</td></tr>");
		}
		else
		{
			sb.append("&nbsp;");
		}
		return sb.toString();
	}
	@Transactional(readOnly=false)
	public String requisitionNumbers(String jobId,String schoolId,String offerReady,String reqNumber,String teacherId)
	{
		//System.out.println("jobId>>>>::::::::"+jobId+" ==  schoolId =="+ schoolId + "==== "+offerReady );
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userMaster=null;
		if(session.getAttribute("userMaster")!=null){
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		TeacherDetail teacherDetail=null;
		if(teacherId!=null){
			teacherDetail=teacherDetailDAO.findById(Integer.parseInt(teacherId+""), false, false);
		}
		SchoolMaster schoolMaster=null;
		if(schoolId!=null && !schoolId.equals("") && !schoolId.equals("0")){
			schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId), false, false);
		}else if(userMaster!=null){
			if(userMaster.getEntityType()==3)
				schoolMaster=userMaster.getSchoolId();
		}
		JobOrder jobOrder=null;

		StringBuffer buffer= new  StringBuffer();
		try{
			jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);

			JobForTeacher jobForTeacher =null;
			String requisitionNumber=null;
			try{
				if(teacherDetail!=null){
					jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail,jobOrder);
				}

				if(jobForTeacher!=null){
					if(jobForTeacher.getRequisitionNumber()!=null){
						requisitionNumber=jobForTeacher.getRequisitionNumber();
					}
				}
			}catch(Exception e){}

		//	buffer.append("<select style='width:350px;' class='form-control' id=\"requisitionNumber\" name=\"requisitionNumber\">");
		//	buffer.append("<option value=\"0\">Select Position #/Requisition #</option>");

			List<JobRequisitionNumbers> jRNumbersList =jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobWithSchool(jobOrder,schoolMaster,offerReady);
			if(jRNumbersList.size()>0){
				buffer.append("<select style='width:350px;' class='form-control' id=\"requisitionNumber\" name=\"requisitionNumber\">");
				buffer.append("<option value=\"0\">Select Position #/Requisition #</option>");
				for (JobRequisitionNumbers jobRequisitionNumbers : jRNumbersList) {
					int status=jobRequisitionNumbers.getStatus();
					boolean reqNumberShow=false;
					try{
						if((requisitionNumber!=null && requisitionNumber.equalsIgnoreCase(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber()) && status==1 )|| status==0){
							reqNumberShow=true;
						}
					}catch(Exception e){}
					if(reqNumberShow){
						String jobTitle="";
						try{
							if(jobRequisitionNumbers.getDistrictRequisitionNumbers()!=null)
								if(jobRequisitionNumbers.getDistrictRequisitionNumbers().getJobTitle()!=null){
									jobTitle=" ( "+jobRequisitionNumbers.getDistrictRequisitionNumbers().getJobTitle()+" )";
								}
						}catch(Exception e){}
						if(reqNumber!=null && reqNumber.equals(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber())){
							buffer.append("<option selected value=\""+jobRequisitionNumbers.getJobRequisitionId()+"\">"+jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber()+jobTitle+"</option>");
						}else{
							buffer.append("<option value=\""+jobRequisitionNumbers.getJobRequisitionId()+"\">"+jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber()+jobTitle+"</option>");
						}
					}
				}
				buffer.append("</select>");
			}

			//buffer.append("</select>");

		}catch (Exception e) {
			e.printStackTrace();
		}		
		return buffer.toString();
	}

	public String[] getStatusForPanel(Integer panelsId,Integer userId){

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String[] members = new String[10]; 
		//UserMaster userMaster = userMasterDAO.findById(userId, false, false);
		PanelSchedule panelSchedule = panelScheduleDAO.findById(panelsId, false, false);
		JobWisePanelStatus jobWisePanelStatus = panelSchedule.getJobWisePanelStatus();

		Integer statusId=0;
		Integer secStatusId=0;

		StatusMaster statusMaster = jobWisePanelStatus.getStatusMaster();
		SecondaryStatus secondaryStatus = jobWisePanelStatus.getSecondaryStatus();

		if(statusMaster!=null)
			statusId = statusMaster.getStatusId();

		if(secondaryStatus!=null)
			secStatusId = secondaryStatus.getSecondaryStatusId();
		//System.out.println("==========...............");

		TeacherDetail teacherDetail = panelSchedule.getTeacherDetail();
		JobOrder jobOrder = panelSchedule.getJobOrder();

		UserMaster userSession=(UserMaster) session.getAttribute("userMaster");
		int entityType=0;
		if(userSession!=null){
			entityType=userSession.getEntityType();
		}
		List<DistrictMaxFitScore> lstDistrictMaxFitScores= new ArrayList<DistrictMaxFitScore>();
		List<TeacherStatusScores> lstTeacherStatusScores= new ArrayList<TeacherStatusScores>();

		boolean isJobAssessment=false;

		try 
		{
			try{

				if(jobOrder.getIsJobAssessment()!=null){
					if(jobOrder.getIsJobAssessment()){
						isJobAssessment=true;
					}
				}
				if(jobOrder!=null){
					lstDistrictMaxFitScores=districtMaxFitScoreDAO.getFitStatusScore(jobOrder.getDistrictMaster());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			String teacherDetails=null;

			if(teacherDetail!=null){
				if(teacherDetail.getFirstName()!=null){
					teacherDetails=teacherDetail.getFirstName();
				}
				if(teacherDetail.getLastName()!=null){
					teacherDetails+=" "+teacherDetail.getLastName();
				}
			}

			JobForTeacher jobForTeacher = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);

			int fitScore=0;
			//sbchild.append("<a href='#' onclick=\"setStatusTilteFor2ndDiv('"+teacherDetails+"','"+subfL.getSecondaryStatusName()+"','oth'); jWTeacherStatusNotesOnOpen('"+fitScore+"',0,'"+subfL.getSecondaryStatusId()+"');\">");
			members[0]=teacherDetails;
			if(statusMaster!=null)
			{
				members[1]=statusMaster.getStatus();
				members[2]=statusMaster.getStatusShortName();
				fitScore=getMaxFitInStatusScore(lstTeacherStatusScores,statusMaster,null);
				if(fitScore==0){
					fitScore=getMaxFitScore(lstDistrictMaxFitScores,statusMaster,null);
				}
				members[3]=""+fitScore;
				members[4]=""+statusMaster.getStatusId();
				members[5]="0";
			}
			if(secondaryStatus!=null)
			{
				members[1]=secondaryStatus.getSecondaryStatusName();
				members[2]="oth";
				fitScore=getMaxFitInStatusScore(lstTeacherStatusScores,secondaryStatus.getStatusMaster(),null);
				if(fitScore==0){
					fitScore=getMaxFitScore(lstDistrictMaxFitScores,secondaryStatus.getStatusMaster(),null);
				}

				members[3]=""+fitScore;
				members[4]="0";
				members[5]=""+secStatusId;
			}

			members[6]=""+jobForTeacher.getJobForTeacherId();

		}catch (Exception e) {
			e.printStackTrace();
		}		

		return members;
	}
	public void savePanelByCG(HttpServletRequest request,UserMaster userMaster,TeacherDetail teacherDetail,JobOrder jobOrder,UserMaster[] userMasterArray,JobWisePanelStatus jobWisePanelStatus,boolean isPhiladelphia){ //added By Ram nath for isPhiladelphia
		//System.out.println("::::::::::::::::::savePanelByCG::::::::::::::"); 
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;

		if(userMaster.getDistrictId()!=null){
			districtMaster=userMaster.getDistrictId();
		}
		if(userMaster.getSchoolId()!=null){
			schoolMaster =userMaster.getSchoolId();
		}
		BasicController bc=new BasicController();
		String mailContent = "";
		PanelSchedule panelSchedule=new PanelSchedule();
		if(districtMaster!=null){
			panelSchedule.setDistrictId(districtMaster.getDistrictId());
		}
		if(schoolMaster!=null){
			panelSchedule.setSchoolId(schoolMaster);
		}
		panelSchedule.setTeacherDetail(teacherDetail);
		panelSchedule.setJobOrder(jobOrder);
		panelSchedule.setCreatedDateTime(new Date());
		TimeZoneMaster timeZoneMaster= new TimeZoneMaster();
		timeZoneMaster.setTimeZoneId(2);


		panelSchedule.setPanelDate(new Date());
		panelSchedule.setPanelTime("12:30");
		panelSchedule.setTimeFormat("PM");
		panelSchedule.setTimeZoneMaster(timeZoneMaster);
		try{
			panelSchedule.setPanelAddress(bc.getDistrictFullAddress(jobOrder));
		}catch(Exception e){
			panelSchedule.setPanelAddress("Address");
		}
		panelSchedule.setPanelDescription("");
		panelSchedule.setCreatedBy(userMaster);
		panelSchedule.setPanelStatus("Scheduled");
		panelSchedule.setJobWisePanelStatus(jobWisePanelStatus);

		try{
			panelScheduleDAO.makePersistent(panelSchedule);
		}catch(Exception e){e.printStackTrace();}

		String distributionEmail=null;

		if(districtMaster.getDistributionEmail()!=null){
			distributionEmail=districtMaster.getDistributionEmail();
		}

		panelSchedule = panelScheduleDAO.findById(panelSchedule.getPanelId(), false, false);

		boolean isDistributionMail = true;
		if(distributionEmail==null || distributionEmail.equals(""))
			isDistributionMail = false;
		String baseURL=Utility.getBaseURL(request);
		String emailto = "vishwanath@netsutra.com";

		//System.out.println("userMasterArray::::::::::::::::::::::::::::::>"+userMasterArray.length);
		int userId = userMasterDAO.getUserId("CARCHER@DADESCHOOLS.NET");
		UserMaster userMaster1 = userMasterDAO.findById(userId, false, false);
		if(userMasterArray!=null && userMasterArray.length>0){
			for(int i=0;i<userMasterArray.length;i++){	
				PanelAttendees paneAttendees = new PanelAttendees();
				paneAttendees.setPanelDate(panelSchedule.getCreatedDateTime());
				paneAttendees.setPanelSchedule(panelSchedule);
				paneAttendees.setPanelStatus("Scheduled");
				paneAttendees.setHostId(userMaster);
				if(jobOrder.getDistrictMaster().getDistrictId()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1200390) && (jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Adult Education (Vocational)") || jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Adult Education (Degreed)")) &&  userMasterArray[i].getEntityType().equals(2)){
					paneAttendees.setPanelInviteeId(userMaster1);
				} else {
					paneAttendees.setPanelInviteeId(userMasterArray[i]);
				}
				panelAttendeesDAO.makePersistent(paneAttendees);
			}
			boolean mailSend=true;
			try{
				if(districtMaster.getDistrictId()==1200390 || isPhiladelphia){
					mailSend=false;
				}
			}catch(Exception e){}
			if(mailSend){
				//Mailing start to new candidates
				List<PanelAttendees> paneAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
				for (PanelAttendees paneAttendees : paneAttendeesList) {
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					String forMated = null;
					if(paneAttendees.getPanelInviteeId().getIsExternal()!=null && paneAttendees.getPanelInviteeId().getIsExternal()==true){
						//send a link
						String pnlId=Utility.encryptNo(panelSchedule.getPanelId());
						String usrId=Utility.encryptNo(paneAttendees.getPanelInviteeId().getUserId());
						String linkIds= pnlId+"###"+usrId;
						try {
							forMated = Utility.encodeInBase64(linkIds);
							forMated = baseURL+"panelscore.do?id="+forMated;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					mailContent = MailText.getNewPanelAttendeeMailText(request,panelSchedule,paneAttendees,paneAttendeesList,true,forMated);
					emailto = paneAttendees.getPanelInviteeId().getEmailAddress();

					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(emailto);
					dsmt.setMailsubject(Utility.getLocaleValuePropByKey("msgPanelScheduled", locale));
					dsmt.setMailcontent(mailContent);
					dsmt.start();
					if(isDistributionMail){
						dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("vishwanath@netsutra.com");
						dsmt.setMailto(distributionEmail);
						dsmt.setMailsubject(Utility.getLocaleValuePropByKey("msgPanelScheduled", locale));
						dsmt.setMailcontent(mailContent);
						dsmt.start();
					}
				}
			}
		}
		//System.out.println(Utility.getLocaleValuePropByKey("msgPanelScheduled", locale));
	}


	public void SendEmailOfficialTranscripts(SecondaryStatus secondaryStatus,TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		//System.out.println(":::::::::::::::::::::::::::::::::Copies of your official transcripts::::::::::::::::::::::");
		if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390)
		{
			String sDistrictName=jobOrder.getDistrictMaster().getDistrictName();
			if(secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref"))
			{
				if(teacherDetail!=null)
				{
					List<TeacherAcademics> lstTeacherAcademics=teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
					//System.out.println("Non Verify lstTeacherAcademics "+lstTeacherAcademics.size());
					if(lstTeacherAcademics!=null && lstTeacherAcademics.size()>0)
					{
						for(TeacherAcademics academics:lstTeacherAcademics)
						{
							if(academics!=null)
							{
								academics.setStatus("V");
								teacherAcademicsDAO.makePersistent(academics);
								//System.out.println("TeacherAcademics is updated Status V for TAIV "+academics.getAcademicId() +" TID "+academics.getTeacherId().getTeacherId());
							}
						}
					}

					String sTo=teacherDetail.getEmailAddress();
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					String mailContent = MailText.OfficialTranscripts(teacherDetail,sDistrictName);
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("cockroach@netsutra.com");
					dsmt.setMailto(sTo);
					dsmt.setMailsubject(Utility.getLocaleValuePropByKey("lblCopiesOfficialTran", locale));
					dsmt.setMailcontent(mailContent);

					System.out.println("*************** SendEmailOfficialTranscripts ************");
					System.out.println("Try to send email for Email "+sTo +" Subject Copies of your official transcripts");
					System.out.println("mailContent "+mailContent);

					dsmt.start();
				}
			}
		}

	}

	/*	
	 * 		Date	:	07-08-2014
	 * 		By		:	Hanzala Subhani
	 * 		Purpose	:	Save Status into teacherAcademic
	 */

	public boolean saveNotesTrans(String status,Integer teacherIdTrans,Integer eligibilityIdTrans,Integer jobIdTrans,String AcademicIDs) {
		UserMaster userMaster 			= 	null;
		DistrictMaster districtMaster 	= 	null;
		SchoolMaster schoolMaster 		= 	null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session1 = request.getSession(false);
		if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		if (session1 == null || session1.getAttribute("userMaster") == null) {

		} else {

			userMaster=(UserMaster)session1.getAttribute("userMaster");

			if(userMaster.getDistrictId()!=null){

				districtMaster=userMaster.getDistrictId();

			}

			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			} 
		}
		Integer districtId 	= districtMaster.getDistrictId();
		Integer userId 		= userMaster.getUserId();

		String arr[]=AcademicIDs.split("##");

		try{
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherIdTrans, false, false);
			List<TeacherAcademics> listTeacherAcademics = null;
			listTeacherAcademics = teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
			String  academicStatus	=	"";
			String setAcademicStatus=	"";
			String  setStatus		=	"";

			Integer FailedStatus	=	0;
			Integer PendingdStatus	=	0;
			Integer WaivedStatus	=	0;
			Integer VerifiedStatus	=	0;
			Integer ArraySize		=	0;

			if(listTeacherAcademics!=null && listTeacherAcademics.size()>0){
				ArraySize=listTeacherAcademics.size();
				for (TeacherAcademics trAcademics : listTeacherAcademics){
					try{
						if(trAcademics.getStatus()!=null){
							academicStatus = trAcademics.getStatus();
						}else{
							academicStatus = "P";
						}
					}catch(Exception e){
						academicStatus = "P";
					}
				}
			}

			if(status!=null){
				if(status.equals("22") && eligibilityIdTrans == 5){
					setAcademicStatus = "V";
				} else if(status.equals("23") && eligibilityIdTrans == 5){
					setAcademicStatus = "F";
				} else if(status.equals("24") && eligibilityIdTrans == 5){
					setAcademicStatus = "W";
				}
			}

			if(arr.length>0) {
				for(int i=1;i<arr.length;i++) {
					TeacherAcademics academicDetail = teacherAcademicsDAO.findById(Long.valueOf(arr[i]), false, false);
					academicDetail.setStatus(setAcademicStatus);
					teacherAcademicsDAO.makePersistent(academicDetail);
				}
			}

			/*try{
		    	TeacherDetail teachDetail= teacherDetailDAO.findById(teacherIdTrans, false, false); 
		    	Criterion criteria= Restrictions.eq("teacherId", teachDetail);
		    	List<TeacherAcademics> teacherAcademicsRec 	= teacherAcademicsDAO.findByCriteria(criteria);

		    	String academicStatusSet;
		    	Long academicId;
		    	for(TeacherAcademics teacherNullRecord:teacherAcademicsRec){
		    		academicId	 		=	teacherNullRecord.getAcademicId();
		    		academicStatusSet	=	teacherNullRecord.getStatus();	
		    		if(academicStatusSet == null || academicStatusSet == ""){
		    		try{
		    			TeacherAcademics teacherAcademics 	= 	teacherAcademicsDAO.findById(academicId, false, false);
    			    	teacherAcademics.setStatus("P");
    			    	teacherAcademicsDAO.makePersistent(teacherAcademics);
    			    }catch(Exception e){
    			    	e.printStackTrace();
    			    }
		    	  }
		    	}

		    	}catch(Exception e){
		    		e.printStackTrace();
		    	}*/


		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	/*	
	 * 		Date	:	24-11-2014
	 * 		By		:	Hanzala Subhani
	 * 		Purpose	:	Send Mail To Reference
	 */

	public boolean sendReferenceMail(Integer referenceTeacherId,Integer referenceJobId,String elerefAutoIds) {

		System.out.println("::::::::::: sendReferenceMail ::::::::::::::::");

		UserMaster userMaster 			= 	null;
		DistrictMaster districtMaster 	= 	null;
		SchoolMaster schoolMaster 		= 	null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session1 = request.getSession(false);
		if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		if (session1 == null || session1.getAttribute("userMaster") == null) {

		} else {

			userMaster=(UserMaster)session1.getAttribute("userMaster");

			if(userMaster.getDistrictId()!=null){

				districtMaster=userMaster.getDistrictId();

			}

			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			} 
		}

		JobOrder jobOrder			=	null;
		TeacherDetail teacherDetail = 	null;
		try{
			teacherDetail = 	teacherDetailDAO.findById(referenceTeacherId, false, false);
			if(referenceJobId != null){
				jobOrder	 =		jobOrderDAO.findById(referenceJobId, false, false);
			}
		} catch (Exception exception){
			exception.printStackTrace();
		}

		districtMaster	=	districtMaster == null?jobOrder.getDistrictMaster():districtMaster;

		/*	URL */
		String path = request.getContextPath();
		int portNo = request.getServerPort();
		String showPortNo = null;
		if(portNo==80)
		{
			showPortNo="";
		}
		else
		{
			//showPortNo=":"+portNo;
			showPortNo="";
		}

		String baseUrl = request.getScheme()+"://"+request.getServerName()+""+showPortNo+path+"/";

		Integer districtId 		= 	districtMaster.getDistrictId();
		Integer jobId 			= 	0;
		Integer distId 			= 	0;
		Integer teacherId 		= 	0;
		Integer elerefAutoId	=	0;

		String urlString		=	null;
		String mainURL			=	null;
		String mainURLEncode	=	null;
		String urlEncode		=	null;
		String mailContent		=	null;

		String eId	=	null;
		String dId	=	null;
		String jId	=	null;
		String tId	=	null;

		String arr[]=elerefAutoIds.split("##");

		StringBuffer sbEmail=new StringBuffer("");
		/*
			List<String> lstStafferEmailAddress	 =	new ArrayList<String>();
			lstStafferEmailAddress.add("hanzala@netsutra.com");
			List<String> mailToReference = new ArrayList<String>();
			String mailToReference = "";
		*/
		try{

			ScheduleMailThreadByListToBcc smt = new ScheduleMailThreadByListToBcc();

			if(arr.length>0) {
				for(int i=1;i<arr.length;i++) {
					String mailToReference = "";
					TeacherElectronicReferences teacherElectronicReferences = teacherElectronicReferencesDAO.findById(Integer.valueOf(arr[i]), false, false);
					mailToReference	= teacherElectronicReferences.getEmail();
					// Check Availability Of Record

					List<TeacherElectronicReferencesHistory> teacherElectronicHistory = null;
					//teacherElectronicHistory	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefId(teacherElectronicReferences, districtMaster, jobOrder, teacherDetail);
					teacherElectronicHistory	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefIdAndDist(teacherElectronicReferences, districtMaster, teacherDetail);

					elerefAutoId 	= 	Integer.valueOf(arr[i]);

					eId		=	Utility.encryptNo(elerefAutoId);
					dId		=	Utility.encryptNo(districtId);
					if(referenceJobId!=null){
						jId		=	Utility.encryptNo(referenceJobId);
					}
					tId		=	Utility.encryptNo(referenceTeacherId);

					urlString 		= 	"eId="+eId+"&dId="+dId+"&jId="+jId+"&tId="+tId+"";
					mainURLEncode	=	baseUrl+"referencecheck.do?p="+Utility.encodeInBase64(urlString);

					//System.out.println(":::::::::::::::::  teacherElectronicHistory.size() ::::::::::::::::::::"+teacherElectronicHistory.size());

					if(teacherElectronicHistory.size() == 0){
						System.out.println(":::::::::::::::: Record Save Successfully ::::::::::::::::");
						TeacherElectronicReferencesHistory teacherElectronicReferencesHistory = new  TeacherElectronicReferencesHistory();
						teacherElectronicReferencesHistory.setTeacherElectronicReferences(teacherElectronicReferences);
						teacherElectronicReferencesHistory.setDistrictMaster(districtMaster);
						if(jobOrder!=null){
							teacherElectronicReferencesHistory.setJobOrder(jobOrder);
						}
						teacherElectronicReferencesHistory.setTeacherDetail(teacherElectronicReferences.getTeacherDetail());
						teacherElectronicReferencesHistory.setEmail(teacherElectronicReferences.getEmail());
						teacherElectronicReferencesHistory.setContactStatus(0);
						teacherElectronicReferencesHistory.setReplyStatus(0);
						teacherElectronicReferencesHistory.setCreatedDateTime(new Date());
						teacherElectronicReferencesHistoryDAO.makePersistent(teacherElectronicReferencesHistory);
					}
					
					mailContent = MailText.ReferenceCheckMail(teacherElectronicReferences, jobOrder, teacherDetail, districtMaster, mainURLEncode);
					System.out.println(mailContent);
					emailerService.sendMailAsHTMLText(mailToReference,Utility.getLocaleValuePropByKey("msgReferenceMail", locale), mailContent);
					/*//System.out.println(mailContent);
					smt.setEmailerService(emailerService);
					smt.setLstMailTo(mailToReference);
					smt.setLstMailBcc(lstStafferEmailAddress);
					smt.setMailSubject("Reference Mail");
					smt.setMailContent(mailContent);
					smt.start();*/
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	public boolean sendReferenceMailPool(Integer referenceTeacherId,String elerefAutoIds) {

		UserMaster userMaster 			= 	null;
		DistrictMaster districtMaster 	= 	null;
		SchoolMaster schoolMaster 		= 	null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session1 = request.getSession(false);
		if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		if (session1 == null || session1.getAttribute("userMaster") == null) {

		} else {

			userMaster=(UserMaster)session1.getAttribute("userMaster");

			if(userMaster.getDistrictId()!=null){

				districtMaster=userMaster.getDistrictId();

			}

			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			} 
		}

		/*	URL */
		String path = request.getContextPath();
		int portNo = request.getServerPort();
		String showPortNo = null;
		if(portNo==80)
		{
			showPortNo="";
		}
		else
		{
			//showPortNo=":"+portNo;
			showPortNo="";
		}

		String baseUrl = request.getScheme()+"://"+request.getServerName()+""+showPortNo+path+"/";

		Integer districtId 		= 	districtMaster.getDistrictId();

		Integer jobId 			= 	0;
		Integer distId 			= 	0;
		Integer teacherId 		= 	0;
		Integer elerefAutoId	=	0;

		String urlString		=	null;
		String mainURL			=	null;
		String mainURLEncode	=	null;
		String urlEncode		=	null;
		String mailContent		=	null;

		String eId	=	null;
		String dId	=	null;
		String jId	=	null;
		String tId	=	null;

		String arr[]=elerefAutoIds.split("##");
		StringBuffer sbEmail=new StringBuffer("");
		List<String> lstStafferEmailAddress	 =	new ArrayList<String>();
		lstStafferEmailAddress.add("hanzala@netsutra.com");
		lstStafferEmailAddress.add("hanzala@netsutra.com");

		//List<String> mailToReference=new ArrayList<String>();
		JobOrder jobOrder	=	null;
		try{
			TeacherDetail teacherDetail = 	teacherDetailDAO.findById(referenceTeacherId, false, false);

			ScheduleMailThreadByListToBcc smt = new ScheduleMailThreadByListToBcc();

			if(arr.length>0) { 

				for(int i=1;i<arr.length;i++) {
					String mailToReference = "";
					TeacherElectronicReferences teacherElectronicReferences = teacherElectronicReferencesDAO.findById(Integer.valueOf(arr[i]), false, false);
					//mailToReference.add(teacherElectronicReferences.getEmail());
					mailToReference	= teacherElectronicReferences.getEmail();
					// Check Availability Of Record

					List<TeacherElectronicReferencesHistory> teacherElectronicHistory = null;
					//teacherElectronicHistory	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefId(teacherElectronicReferences, districtMaster, jobOrder, teacherDetail);
					teacherElectronicHistory	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefIdAndDist(teacherElectronicReferences, districtMaster, teacherDetail);

					elerefAutoId 	= 	Integer.valueOf(arr[i]);

					eId		=	Utility.encryptNo(elerefAutoId);
					dId		=	Utility.encryptNo(districtId);
					tId		=	Utility.encryptNo(referenceTeacherId);

					urlString 		= 	"eId="+eId+"&dId="+dId+"&jId="+jId+"&tId="+tId+"";
					mainURLEncode	=	baseUrl+"referencecheckpool.do?p="+Utility.encodeInBase64(urlString);
					if(teacherElectronicHistory.size() == 0){
						TeacherElectronicReferencesHistory teacherElectronicReferencesHistory = new  TeacherElectronicReferencesHistory();
						teacherElectronicReferencesHistory.setTeacherElectronicReferences(teacherElectronicReferences);
						teacherElectronicReferencesHistory.setDistrictMaster(districtMaster);
						if(jobOrder!=null){
							teacherElectronicReferencesHistory.setJobOrder(jobOrder);
						}
						teacherElectronicReferencesHistory.setTeacherDetail(teacherElectronicReferences.getTeacherDetail());
						teacherElectronicReferencesHistory.setEmail(teacherElectronicReferences.getEmail());
						teacherElectronicReferencesHistory.setContactStatus(0);
						teacherElectronicReferencesHistory.setReplyStatus(0);
						teacherElectronicReferencesHistory.setCreatedDateTime(new Date());
						teacherElectronicReferencesHistoryDAO.makePersistent(teacherElectronicReferencesHistory);
					}
					mailContent = MailText.ReferenceCheckMail(teacherElectronicReferences, jobOrder, teacherDetail, districtMaster, mainURLEncode);
					//System.out.println(mailContent);
					emailerService.sendMailAsHTMLText(mailToReference,Utility.getLocaleValuePropByKey("msgReferenceMail", locale), mailContent);
					/*//System.out.println(mailContent);
					smt.setEmailerService(emailerService);
					smt.setLstMailTo(mailToReference);
					smt.setLstMailBcc(lstStafferEmailAddress);
					smt.setMailSubject("Reference Mail");
					smt.setMailContent(mailContent);
					smt.start();*/
					
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	/*
	 * 		Update MaxScore for User From DA
	 * 		By	:	Hanzala Subhani
	 * 
	 */

	public String getSectionWiseTemaplteslistTPGolu(Integer sectionEmailId){
		try{
			StatusWiseEmailSection statusWiseEmailSection = statusWiseEmailSectionDAO.findById(sectionEmailId, false, false);
			Criterion criteria				=	Restrictions.eq("statusWiseEmailSection",statusWiseEmailSection);
			List<StatusWiseEmailSubSection> sectionWiseTemaplteslist = statusWiseEmailSubSectionDAO.findByCriteria(Order.asc("subSectionName"),criteria);

			String statuswisesectiontemplateslist="<select class='form-control' id='statuswisesectiontemplateslistTP' onchange='setTemplateByLIstIdTPGolu(this.value)'><option value='0'>"+Utility.getLocaleValuePropByKey("lblSelectSubTemplate1", locale)+"</option>";

			for (StatusWiseEmailSubSection statusWiseEmailSubSection : sectionWiseTemaplteslist) {
				statuswisesectiontemplateslist+="<option value="+statusWiseEmailSubSection.getEmailSubSectionId()+">"+statusWiseEmailSubSection.getSubSectionName()+"</option>";
				/*if(statusWiseEmailSubSection.getSubSectionName().length()>55){
				String templateName =   statusWiseEmailSubSection.getSubSectionName().substring(0, 54);
				statuswisesectiontemplateslist+="<a id='iconpophover4' rel='tooltip'><option value='"+statusWiseEmailSubSection.getEmailSubSectionId()+"' title='"+statusWiseEmailSubSection.getSubSectionName()+"'>"+templateName+"....</option></a>";
			}else{
				statuswisesectiontemplateslist+="<option value="+statusWiseEmailSubSection.getEmailSubSectionId()+">"+statusWiseEmailSubSection.getSubSectionName()+"</option>";
			}*/

			}

			statuswisesectiontemplateslist+="</select>";
			return statuswisesectiontemplateslist;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	} 
	public String checkOCPCStatus(String jobForTeacherId,String requisitionNumberId){

		//System.out.println("jobForTeacherId:: "+jobForTeacherId+" requisitionNumberId:: "+requisitionNumberId);
		JobOrder jobOrder = null;
		TeacherDetail teacherDetail = null;
		TeacherPersonalInfo teacherPersonalInfo = null;
		JobForTeacher jobForTeacherObj =new JobForTeacher();
		String oc = "";
		String pc = "";

		if(jobForTeacherId!=null){
			jobForTeacherObj=jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
			jobOrder = jobForTeacherObj.getJobId();
			teacherDetail = jobForTeacherObj.getTeacherId();
			teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
			String ssn = teacherPersonalInfo.getSSN()==null?"":teacherPersonalInfo.getSSN();
			String empNo = teacherPersonalInfo.getEmployeeNumber()==null?"":teacherPersonalInfo.getEmployeeNumber();
			EmployeeMaster employeeMaster = null;
			//System.out.println("empNo::::::: "+empNo);
			if(empNo!=null && !empNo.trim().equals(""))
			{
				employeeMaster = employeeMasterDAO.checkEmployeeByEmployeeCode(empNo,jobOrder.getDistrictMaster());
				//System.out.println("employeemaster::: "+employeeMaster);
			}else
			{
				try {
					if(!ssn.equals(""))
					{
						ssn = ssn.trim();
						ssn = Utility.decodeBase64(ssn);
						if(ssn.length()>4)
							ssn = ssn.substring(ssn.length() - 4, ssn.length());
						List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
						//System.out.println("==============================: "+ssn+" "+teacherPersonalInfo.getFirstName().trim()+" "+teacherPersonalInfo.getLastName().trim());
						employeeMasters =	employeeMasterDAO.checkEmployeeBySSN(ssn,teacherPersonalInfo.getFirstName().trim(),teacherPersonalInfo.getLastName().trim(),jobOrder.getDistrictMaster());
						if(employeeMaster!=null)
							employeeMaster = employeeMasters.get(0);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(employeeMaster!=null){	
				try{
					if(employeeMaster.getOCCode()!=null)
						oc = employeeMaster.getOCCode().trim();

					if(employeeMaster.getPCCode()!=null)
						pc = employeeMaster.getPCCode().trim();
				}catch(Exception e){ e.printStackTrace();}
			}
		}
		String posType = "";
		if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
			try{
				DistrictRequisitionNumbers districtRequisitionNumbers=null;
				districtRequisitionNumbers = jobRequisitionNumbersDAO.findDistrictReqIdByJobReqId(jobOrder,requisitionNumberId);

				if(districtRequisitionNumbers!=null)
				{
					posType = districtRequisitionNumbers.getPosType()==null?"":districtRequisitionNumbers.getPosType();
				}
			}catch(Exception e){}
		}
		System.out.println("pc ::: "+pc+"oc:::::::"+oc +" posType:::"+posType);

		if(pc.equalsIgnoreCase("y")){
			return "pc";
		}else if(oc.equalsIgnoreCase("y") && posType.equalsIgnoreCase("p")){
			return "oc";
		}else
			return "oc:"+oc+" pc:"+pc;

	}

	public String getReferenceCheck(Integer teacherId,Integer jobId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{

		//System.out.println(":::::::"+noOfRow+":::::::::::::"+sortOrder+":::::::::::"+sortOrderType);

		UserMaster userMaster 			= 	null;
		DistrictMaster districtMaster 	= 	null;
		SchoolMaster schoolMaster 		= 	null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session1 = request.getSession(false);
		if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		if (session1 == null || session1.getAttribute("userMaster") == null) {

		} else {

			userMaster=(UserMaster)session1.getAttribute("userMaster");

			if(userMaster.getDistrictId()!=null){

				districtMaster=userMaster.getDistrictId();

			}

			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			} 
		}

		JobOrder jobOrder = null;
		if(jobId!=null){
			try{
				jobOrder	=	jobOrderDAO.findById(jobId, false, false);
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		if(districtMaster == null)
			districtMaster = jobOrder.getDistrictMaster();

		/* 	Calculate Total Number of Question for district form "districtspecificrefchkquestions"
		 * 
		 * */
		List<DistrictSpecificRefChkQuestions> districtSpecificQuestionList	=  null;
		Integer numberOfQuestion = null;
		try{
			districtSpecificQuestionList	=	districtSpecificRefChkQuestionsDAO.getDistrictRefChkQuestionBYDistrict(districtMaster);
			numberOfQuestion 				=	districtSpecificQuestionList.size();
		} catch(Exception exception){
			exception.printStackTrace();
		}

		//System.out.println(":::::::::::::::: numberOfQuestion :::::::::::::::::::::"+numberOfQuestion);

		/*============= For Sorting ===========*/
		int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
		int pgNo 			= 	Integer.parseInt(pageNo);
		int start 			= 	((pgNo-1)*noOfRowInPage);
		int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord		= 	0;
		int topMaxScore 	= 	0;
		Integer totalContactNumber = 0;
		StringBuffer sb = new StringBuffer();
		// Reference Check Grid
		try{
			/*============= For Sorting ===========*/
			/*====== set default sorting fieldName ====== **/
			String sortOrderNoField		=	"createdDateTime";
			String sortOrderFieldName1	=	"createdDateTime";
			Order  sortOrderStrVal		=	null;
			String sortOrderTypeVal = 	"0";
			/**Start set dynamic sorting fieldName **/
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("firstName") && !sortOrder.equals("email") && !sortOrder.equals("contactnumber")){
					sortOrderFieldName1=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("firstName")){
					sortOrderNoField="firstName";
				}
				if(sortOrder.equals("email")){
					sortOrderNoField="email";
				}
				if(sortOrder.equals("contactnumber")){
					sortOrderNoField="contactnumber";
				}
			}
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")) {
					sortOrderStrVal		=	Order.asc(sortOrderFieldName1);
				} else {
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName1);
				}
			} else {
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName1);
			}
			/*=============== End ======================*/	

			TeacherDetail teacherDetail	=	teacherDetailDAO.findById(teacherId, false, false);
			

			//List<TeacherElectronicReferences> teacherElectronicReferenceList = teacherElectronicReferencesDAO.findByTeacher(teacherDetail);
			List<TeacherElectronicReferences> teacherElectronicReferenceList = teacherElectronicReferencesDAO.findActiveContactedReferencesByTeacher(teacherDetail);
			totalRecord =teacherElectronicReferenceList.size();
			if(totalRecord<end)
				end=totalRecord;

			List<TeacherElectronicReferences> teacherElectronicReferenceListMain = teacherElectronicReferenceList.subList(start,end);

			// Create Map
			List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistoryList = null;
			List<TeacherElectronicReferencesHistory> countContactStatusRefChkCount 			= null;
			List<TeacherElectronicReferencesHistory> countReplyStatusRefChkCount 			= null;
			Map<Integer,TeacherElectronicReferencesHistory> mapRefHistory = new HashMap<Integer, TeacherElectronicReferencesHistory>();
			Integer refChkSize 	= 	0;
			Integer contactSize	=	0;
			Integer replySize	=	0;

			try{
				teacherElectronicReferencesHistoryList	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByTeacherId(districtMaster, teacherDetail);
				countContactStatusRefChkCount			=	teacherElectronicReferencesHistoryDAO.countContactStatusRefChk(districtMaster, teacherDetail);
				countReplyStatusRefChkCount				=	teacherElectronicReferencesHistoryDAO.countReplyStatusRefChk(districtMaster, teacherDetail);

				if(teacherElectronicReferencesHistoryList != null){
					refChkSize	=	teacherElectronicReferencesHistoryList.size();
					for (TeacherElectronicReferencesHistory refRecord : teacherElectronicReferencesHistoryList) {
						Integer key = refRecord.getTeacherElectronicReferences().getElerefAutoId();
						mapRefHistory.put(key, refRecord);
					}
				}

				if(countContactStatusRefChkCount != null){
					contactSize = 	countContactStatusRefChkCount.size();
				}

				if(countReplyStatusRefChkCount != null){
					replySize	=	countReplyStatusRefChkCount.size();
				}

			} catch(Exception e){}
			int actionAdd=0;
			String responseText = "";
			sb.append("<tr><td>");
			sb.append("<div id='referenceCheckGrid'>");
			sb.append("<table border='0' id='referenceCheckGridCG' style=\"margin-right:0px;\">");
			sb.append("<thead class='bg'>");

			sb.append("<tr>");

			sb.append("<th valign='top'>&nbsp;</th>");
			//responseText=PaginationAndSorting.responseSortingLink("Name",sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo);
			sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblNm", locale)+"</th>");
			sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblEmail", locale)+"</th>");
			sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblContNum", locale)+"</th>");
			sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
			sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblScr", locale)+"</th>");
			sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgCreatedDateTime", locale)+"</th>");
			if(userMaster.getEntityType()==2 || userMaster.getEntityType()==3){
				sb.append("<th valign='top'>Action</th>");
				actionAdd=1;
			}
			sb.append("</tr>");
			sb.append("</thead>");

			Map<String,TeacherElectronicReferencesHistory> mapPosNumber = new HashMap<String, TeacherElectronicReferencesHistory>();
			Integer elerefAutoId 	= 0;
			Integer questionMaxScore= 0;
			String techerFullName	=	"";
			String candidateFullName=	"";
			if(totalRecord > 0) {

				for(TeacherElectronicReferences referenceDetail:teacherElectronicReferenceListMain) {
					Integer contactStatus 	= 0;
					elerefAutoId = referenceDetail.getElerefAutoId();
					techerFullName = referenceDetail.getFirstName()+" "+referenceDetail.getLastName();
					candidateFullName = teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
					try{
						techerFullName=techerFullName.replace("'","\\'");
						candidateFullName=candidateFullName.replace("'","\\'");
					}catch(Exception e){
						e.printStackTrace();
					}

					TeacherElectronicReferencesHistory referenceHistory = 	mapRefHistory.get(elerefAutoId);
					try{
						if(referenceHistory != null){
							contactStatus	=	referenceHistory.getContactStatus();
							questionMaxScore=	referenceHistory.getQuestionMaxScore();
							totalContactNumber = totalContactNumber+contactStatus;
						}
					}catch(Exception exception){
						exception.printStackTrace();
					}

					sb.append("<tr>");
					sb.append("<td class='td1'>");
					if(contactStatus != null){
						if(contactStatus == 1){
							sb.append("<input disabled type=\"checkbox\" name=\"referenceData[]\" value="+elerefAutoId+">");
							sb.append("<input style=\"display:none;\" disabled type=\"checkbox\" name=\"referenceDataForPrint[]\" value="+elerefAutoId+">");
						} else {
							sb.append("<input type=\"checkbox\" name=\"referenceData[]\" onclick='return showReferenceCheck()' value="+elerefAutoId+">");
						}
					} else {
							sb.append("<input type=\"checkbox\" name=\"referenceData[]\" onclick='return showReferenceCheck()' value="+elerefAutoId+">");
					}
					sb.append("</td>");
					

					sb.append("<td class='td2'>");
					sb.append(referenceDetail.getFirstName()+" "+referenceDetail.getLastName());
					sb.append("</td>");

					sb.append("<td class='td3'>");
					sb.append(referenceDetail.getEmail());
					sb.append("</td>");

					sb.append("<td class='td4'>");
					sb.append(referenceDetail.getContactnumber());
					sb.append("</td>");

					if(contactStatus != null){
						if(contactStatus == 1){
							sb.append("<td class='td5'>" +
									"<a href=\"#\" onclick=\"showReferenceDetail('"+candidateFullName+"','"+techerFullName+"',"+elerefAutoId+","+districtMaster.getDistrictId()+","+jobId+","+teacherId+")\">" +
									"<span class=\"icon-download-alt icon-large iconcolorBlue\"></span>" +
									"</a>" +
							"</td>");
						} else {
							sb.append("<td class='td5'>&nbsp;</td>");
						}
					} else {
						sb.append("<td class='td6'>&nbsp;</td>");
					}

					if(questionMaxScore != null){
						sb.append("<td class='td6'>");
						sb.append(questionMaxScore);
						sb.append("</td>");
					} else {
						sb.append("<td class='td6'>&nbsp;</td>");
					}


					sb.append("<td class='td7'>");
					sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(referenceDetail.getCreatedDateTime()));
					sb.append("</td>");
					
						if(actionAdd == 1){
						String elerefAutoId1="",eId1="",jId1="",tId1="",uId="",urlString="";
						{									
						eId1		=	referenceDetail.getElerefAutoId().toString();																
						tId1		=	referenceDetail.getTeacherDetail().getTeacherId().toString();
						uId			=	userMaster.getUserId().toString();
						urlString 		= 	eId1+","+tId1+","+uId;
						}	
						
						sb.append("<td class='td8' style='text-align:center;'>");
						if( contactStatus != 1){//referenceHistory != null &&
							sb.append("<a class='districtTooltip' href='javascript:void(0)' data-original-title='District Entry' rel='tooltip' onclick=\"openQuestionEReference(\'"+urlString+"\');\"><img class='addDistrictEntry' src='images/add_notes.png' width='17px' height='20px;'/></a>");
						}else{
							sb.append("&nbsp;");
						}
						sb.append("</td>");								
						}
					sb.append("</tr>");
				}
			} else {
				sb.append("<tr>");
				sb.append("<td colspan=\"5\"><B>");
				sb.append(""+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"");
				sb.append("</B></td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationTrippleGrid(request,totalRecord,noOfRow, pageNo));

			sb.append("</div>");

			Integer referenceTeacherId	=	teacherId;
			Integer referenceJobId		=	jobId;
			sb.append("<table border='0' id='referenceGridSendEmail' cellspacing='3' cellpadding='3' style='display:none;'>");
			sb.append("<tr><td colspan=\"2\">&nbsp;</td></tr>");
			sb.append("<input type=\"hidden\" id=\"referenceTeacherId\" name=\"referenceTeacherId\" value='"+referenceTeacherId+"'/>");
			sb.append("<input type=\"hidden\" id=\"referenceListSize\" name=\"referenceListSize\" value="+totalRecord+">");
			sb.append("<input type=\"hidden\" id=\"referenceJobId\" name=\"referenceJobId\" value='"+referenceJobId+"'/>");
			sb.append("<input type=\"hidden\" id=\"contactSize\" name=\"contactSize\" value="+contactSize+">");
			sb.append("<input type=\"hidden\" id=\"refChkSize\" name=\"refChkSize\" value="+refChkSize+">");
			sb.append("<input type=\"hidden\" id=\"numberOfQuestion\" name=\"numberOfQuestion\" value="+numberOfQuestion+">");
			sb.append("<input type=\"hidden\" id=\"topMaxScore\" name=\"topMaxScore\" value="+topMaxScore+">");
			sb.append("<input disabled type='hidden' id='totalContactNumber' name='totalContactNumber' value="+totalContactNumber+">");
			sb.append("<input type=\"hidden\" id=\"actionAdd\" name=\"actionAdd\" value="+actionAdd+">");
			sb.append("<input type=\"hidden\" id=\"cg\" name=\"cg\" value='1'>");
			sb.append("<input type=\"hidden\" id=\"teacherDetails\" name=\"teacherDetails\" value='"+(teacherDetail.getFirstName()+" "+teacherDetail.getLastName())+"'>");
			sb.append("<input type=\"hidden\" id=\"teacherId\" name=\"teacherId\" value='"+teacherId+"'>");
			
			sb.append("<tr>");

			sb.append("<td>&nbsp;</td>");
			sb.append("<td>");

			if(replySize != totalRecord){
				if(refChkSize != 0 && (totalRecord == refChkSize)){
					sb.append("<button class='btn btn-primary' type='button' onclick='return sendReferenceMailFromCG()'>"+Utility.getLocaleValuePropByKey("btnResend", locale)+"</button>&nbsp;&nbsp;");
				} else {
					sb.append("<button class='btn btn-primary' type='button' onclick='return sendReferenceMailFromCG()'>"+Utility.getLocaleValuePropByKey("btnSend", locale)+"</button>&nbsp;&nbsp;");
				}
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("</table>");
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		return sb.toString()+"##"+totalContactNumber;
	}


	public String showReferenceDetail(Integer elerefAutoId,Integer districtId,Integer jobId,Integer teacherId,String fromWhere){

		UserMaster userMaster 			= 	null;
		DistrictMaster districtMaster 	= 	null;
		SchoolMaster schoolMaster 		= 	null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session1 = request.getSession(false);
		if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		if (session1 == null || session1.getAttribute("userMaster") == null) {

		} else {

			userMaster=(UserMaster)session1.getAttribute("userMaster");

			if(userMaster.getDistrictId()!=null){

				districtMaster=userMaster.getDistrictId();

			}

			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			} 
		}

		if(districtMaster == null)
			districtMaster = districtMasterDAO.findById(districtId, false, false);

		Integer counter 	= 1;
		int topMaxScore 	= 0;
		int interval 		= 10;
		int dslider 		= 1;
		int svalue 			= 0;
		int scoreProvided 	= 0;

		if(topMaxScore%10==0)
			interval=10;
		else if(topMaxScore%5==0)
			interval=5;
		else if(topMaxScore%3==0)
			interval=3;
		else if(topMaxScore%2==0)
			interval=2;


		StringBuffer sb = new StringBuffer();
		try{
			TeacherDetail teacherDetail	=	teacherDetailDAO.findById(teacherId, false, false);	
			List<TeacherElectronicReferences> teacherElectronicReferenceList = teacherElectronicReferencesDAO.findByTeacher(teacherDetail);

			List<DistrictSpecificRefChkAnswers> districtSpecificRefChkAnswersList = null;

			districtSpecificRefChkAnswersList = districtSpecificRefChkAnswersDAO.findTeacherAnswersByDistrictAndRefChk(districtMaster, elerefAutoId);

			//System.out.println(":::::::::::: districtSpecificRefChkAnswersList :::::::::::::::::"+districtSpecificRefChkAnswersList.size());

			/* 	
			 * 	Purpose		:	Create Map for Option
			 * 	By 			:	Hanzala Subhani
			 * 
			 */
			List<DistrictSpecificRefChkOptions> districtSpecificRefChkOptionsList	=	districtSpecificRefChkOptionsDAO.findAll();	

			Map<Integer, DistrictSpecificRefChkOptions> mapReckOption	=	new HashMap<Integer, DistrictSpecificRefChkOptions>();
			for(DistrictSpecificRefChkOptions refCheckOption : districtSpecificRefChkOptionsList){
				Integer key = refCheckOption.getOptionId();
				mapReckOption.put(key, refCheckOption);
			}

			if(topMaxScore > 0){
				sb.append("<div  style=\"height:50px;\">");
				sb.append("<table border=0><tr><td style='vertical-align:top;height:33px;padding-left:8px;'><label><B>"+Utility.getLocaleValuePropByKey("lblScr", locale)+":</B></label></td>");
				sb.append("<td style='padding:5px;vertical-align:bottom;height:33px;'>&nbsp;");

				sb.append("<iframe id='ifrmDaAdminScore"+fromWhere+"' src='slideract.do?name=statusNoteFrm&amp;tickInterval="+interval+"&amp;max="+topMaxScore+"&amp;swidth=536&amp;dslider="+dslider+"&amp;svalue="+scoreProvided+"' scrolling='no' frameborder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:580px;margin-top:-11px;'></iframe>");						

				sb.append("</td>");
				sb.append("<td valign=top style='padding-top:4px; padding-left: 17px;'><input type='hidden' name='dsliderchk' id='dsliderchk'  value="+dslider+">");
				sb.append("</td></tr>");
				sb.append("</table>");
				sb.append("</div>");
			}
			sb.append("<div  style=\"height:450px; overflow-y:scroll;\">");
			sb.append("<table width='100%' border='0'>");

			DistrictSpecificRefChkOptions referenceChkOptionVal	=	null;
			if(districtSpecificRefChkAnswersList.size() != 0){

				for(DistrictSpecificRefChkAnswers answerRecord : districtSpecificRefChkAnswersList){
					sb.append("<tr>");
					sb.append("<td>");
					sb.append("<B>Question: "+counter+"<BR>");
					sb.append(answerRecord.getDistrictSpecificRefChkQuestions().getQuestion());
					sb.append("</B></td>");
					sb.append("</tr>");

					if(answerRecord.getQuestionOption() != null){
						sb.append("<tr>");
						sb.append("<td>");
						sb.append("<B>"+Utility.getLocaleValuePropByKey("msgAnswer", locale)+" </B><BR>");
						sb.append(answerRecord.getQuestionOption());
						sb.append("</td>");
						sb.append("</tr>");
					}

					if(answerRecord.getQuestionMaxScore() != null){
						
						if(answerRecord.getQuestionMaxScore().equals(999)){
							sb.append("<tr>");
							sb.append("<td>");
							sb.append(Utility.getLocaleValuePropByKey("msgNoBasisjudgement", locale));
							sb.append("</td>");
							sb.append("</tr>");
						} else {
							sb.append("<tr>");
							sb.append("<td>");
							sb.append("<B>"+Utility.getLocaleValuePropByKey("lblScr", locale)+": </B>");
							sb.append(answerRecord.getQuestionMaxScore());
							sb.append("</td>");
							sb.append("</tr>");
						}
					}

					if(answerRecord.getQuestionType().equals("ml") || answerRecord.getQuestionType().equals("sloet")){
						sb.append("<tr>");
						sb.append("<td>");
						sb.append(answerRecord.getInsertedText());
						sb.append("</td>");
						sb.append("</tr>");
					}

					if(answerRecord.getQuestionType().equals("SLD")){
						sb.append("<tr>");
						sb.append("<td>");
						sb.append("<B>"+Utility.getLocaleValuePropByKey("msgAdditionalComments", locale)+": </B><BR>");
						sb.append(answerRecord.getInsertedText());
						sb.append("</td>");
						sb.append("</tr>");
					}

					if(answerRecord.getQuestionType().equals("mlsel")){
						sb.append("<tr>");
						sb.append("<td>");
						sb.append("<B>"+Utility.getLocaleValuePropByKey("msgAnswer", locale)+" </B><BR>");
						sb.append("</td>");
						sb.append("</tr>");

						String questionOption 	= 	"";
						Integer optionId		=	null;
						questionOption			=	answerRecord.getSelectedOptions();
						String	OptionText		=	"";
						Integer	count			=	1;
						try{
							questionOption	=	questionOption.replace('|', ',');
							String jobCategoryIdStr[] =	questionOption.split(",");
							Integer oIds[] = new Integer[jobCategoryIdStr.length];

							for(int i=0;i < oIds.length;i++){
								oIds[i]=Integer.parseInt(jobCategoryIdStr[i]);
								referenceChkOptionVal = mapReckOption.get(oIds[i]);
								OptionText	=	"Option "+count+" : "+referenceChkOptionVal.getQuestionOption();

								sb.append("<tr>");
								sb.append("<td>");
								sb.append("<B>Option "+count+": </B>");
								sb.append(referenceChkOptionVal.getQuestionOption());
								sb.append("</td>");
								sb.append("</tr>");

								count++;
							}
						} catch (Exception exception){ }
					}
					if(answerRecord.getQuestionType().equals("mloet")){
						sb.append("<tr>");
						sb.append("<td>");
						sb.append("<B>"+Utility.getLocaleValuePropByKey("msgAnswer", locale)+"</B><BR>");
						sb.append("</td>");
						sb.append("</tr>");

						String questionOption 	= 	"";
						Integer optionId		=	null;
						questionOption			=	answerRecord.getSelectedOptions();
						String	OptionText		=	"";
						Integer	count			=	1;

						try{
							questionOption	=	questionOption.replace('|', ',');
							String jobCategoryIdStr[] =	questionOption.split(",");
							Integer oIds[] = new Integer[jobCategoryIdStr.length];

							for(int i=0;i < oIds.length;i++){
								oIds[i]=Integer.parseInt(jobCategoryIdStr[i]);
								referenceChkOptionVal = mapReckOption.get(oIds[i]);
								OptionText	=	""+Utility.getLocaleValuePropByKey("  lblOp", locale)+" "+count+" : "+referenceChkOptionVal.getQuestionOption();

								sb.append("<tr>");
								sb.append("<td>");
								sb.append("<B>"+Utility.getLocaleValuePropByKey("  lblOp", locale)+" "+count+": </B>");
								sb.append(referenceChkOptionVal.getQuestionOption());
								sb.append("</td>");
								sb.append("</tr>");
								count++;
							}
						} catch (Exception exception){ }

						/*sb.append("<tr>");
						sb.append("<td>");
							sb.append("<B>Additional comments: </B><BR>");
							sb.append(answerRecord.getInsertedText());
						sb.append("</td>");
						sb.append("</tr>");*/
					}

					sb.append("<tr>");
					sb.append("<td>&nbsp;</td>");
					sb.append("</tr>");

					counter++;
				}

			} else {
				sb.append(""+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"");
			}
			sb.append("</table>");
			sb.append("<input type='hidden' name='elerefAutoId' id='elerefAutoId'  value="+elerefAutoId+">");
			sb.append("<input type='hidden' name='districtId' id='districtId'  value="+districtId+">");
			sb.append("<input type='hidden' name='teacherId' id='teacherId'  value="+teacherId+">");
			sb.append("<input type='hidden' name='teacherIdForPool' id='teacherIdForPool'  value="+teacherId+">");
			sb.append("<input type='hidden' name='jobId' id='jobId'  value="+jobId+">");
			sb.append("</div>");
		}catch(Exception exception){
			exception.printStackTrace();
		}
		return sb.toString();
	}


	public boolean saveMaxScore(Integer elerefAutoId, Integer districtId, Integer jobId, Integer teacherId,Integer maxScore){

		//System.out.println("::::::::::::::::::: saveMaxScore :::::::::::::::::::::"+maxScore);

		UserMaster userMaster 			= 	null;
		DistrictMaster districtMaster 	= 	null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session1 = request.getSession(false);
		if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		if (session1 == null || session1.getAttribute("userMaster") == null) {

		} else {
			userMaster=(UserMaster)session1.getAttribute("userMaster");
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();

			}

		}
		TeacherElectronicReferences teacherElectronicReferences = null;
		TeacherDetail teacherDetail		=	null;
		JobOrder jobOrder				=	null;

		teacherElectronicReferences = 	teacherElectronicReferencesDAO.findById(elerefAutoId, false, false);
		districtMaster				=	districtMasterDAO.findById(districtId, false, false);
		teacherDetail				=	teacherDetailDAO.findById(teacherId, false, false);
		jobOrder					=	jobOrderDAO.findById(jobId, false, false);
		List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistory = null;
		try{
			//teacherElectronicReferencesHistory	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefId(teacherElectronicReferences, districtMaster, jobOrder, teacherDetail);
			teacherElectronicReferencesHistory	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefIdAndDist(teacherElectronicReferences, districtMaster, teacherDetail);
			if(teacherElectronicReferencesHistory!=null){
				TeacherElectronicReferencesHistory updateRecord = teacherElectronicReferencesHistoryDAO.findById(teacherElectronicReferencesHistory.get(0).getElerefHAutoId(), false, false);
				updateRecord.setQuestionMaxScore(maxScore);
				teacherElectronicReferencesHistoryDAO.makePersistent(updateRecord);
			}
		} catch (Exception exception){
			exception.printStackTrace();
		}

		return false;
	}

	public boolean saveMaxScoreForPool(Integer elerefAutoId, Integer districtId, Integer teacherId,Integer maxScore){

		//System.out.println("::::::::::::::::::: saveMaxScoreForPool :::::::::::::::::::::"+maxScore);

		UserMaster userMaster 			= 	null;
		DistrictMaster districtMaster 	= 	null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session1 = request.getSession(false);
		if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		if (session1 == null || session1.getAttribute("userMaster") == null) {

		} else {
			userMaster=(UserMaster)session1.getAttribute("userMaster");
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();

			}

		}
		TeacherElectronicReferences teacherElectronicReferences = null;
		TeacherDetail teacherDetail		=	null;

		teacherElectronicReferences = 	teacherElectronicReferencesDAO.findById(elerefAutoId, false, false);
		districtMaster				=	districtMasterDAO.findById(districtId, false, false);
		teacherDetail				=	teacherDetailDAO.findById(teacherId, false, false);
		List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistory = null;
		try{
			teacherElectronicReferencesHistory	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefIdAndDist(teacherElectronicReferences, districtMaster, teacherDetail);
			if(teacherElectronicReferencesHistory!=null){
				TeacherElectronicReferencesHistory updateRecord = teacherElectronicReferencesHistoryDAO.findById(teacherElectronicReferencesHistory.get(0).getElerefHAutoId(), false, false);
				updateRecord.setQuestionMaxScore(maxScore);
				teacherElectronicReferencesHistoryDAO.makePersistent(updateRecord);
			}
		} catch (Exception exception){
			exception.printStackTrace();
		}

		return false;
	}

	public void interviewInvites(List<I4InterviewInvites> I4InterviewInvitesList,StatusMaster statusMaster,StatusMaster statusMasterVVI,SecondaryStatus secondaryStatus,SecondaryStatus secondaryStatusVVI,JobOrder jobOrder,TeacherDetail teacherDetail,UserMaster userMaster,HttpServletRequest request){
		//System.out.println(I4InterviewInvitesList.size()+":::::::::::::=interviewInvites ::::::::::::::::::::::::::");
		//System.out.println(":::"+statusMaster+" :"+statusMasterVVI+"::"+secondaryStatus+":::"+secondaryStatusVVI);
		boolean schoolPrivilegeForI4=false;
		if(I4InterviewInvitesList.size()==0){
			if(statusMasterVVI!=null){
				//System.out.println("::::::::::::statusMasterVVI:::"+statusMasterVVI.getStatusId());
				//System.out.println("::::::::::::statusMaster:::"+statusMaster.getStatusId());
				if(statusMasterVVI.getStatusId().equals(statusMaster.getStatusId())){
					schoolPrivilegeForI4=true;;
				}
			}else if(secondaryStatusVVI!=null){
				//System.out.println("secondaryStatusVVI:::::"+secondaryStatusVVI.getSecondaryStatusName());
				//System.out.println("secondaryStatus:::::"+secondaryStatus.getSecondaryStatusName());
				if(secondaryStatusVVI.getSecondaryStatusName().equalsIgnoreCase(secondaryStatus.getSecondaryStatusName())){
					schoolPrivilegeForI4=true;;
				}
			}
		}
		System.out.println("schoolPrivilegeForI4:::::::::::::::::::::::::::::::::::"+schoolPrivilegeForI4);
		if(schoolPrivilegeForI4){
			I4QuestionSets i4QuestionSets=null;
			try{
				if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()  &&  jobOrder.getSendAutoVVILink()!=null && jobOrder.getSendAutoVVILink()){
					if(jobOrder.getI4QuestionSets()!=null)
						i4QuestionSets = jobOrder.getI4QuestionSets();

				} else if(jobOrder.getJobCategoryMaster().getOfferVirtualVideoInterview()!=null && jobOrder.getJobCategoryMaster().getOfferVirtualVideoInterview() && jobOrder.getJobCategoryMaster().getSendAutoVVILink()!=null && jobOrder.getJobCategoryMaster().getSendAutoVVILink()){
					if(jobOrder.getJobCategoryMaster().getI4QuestionSets()!=null)
						i4QuestionSets = jobOrder.getJobCategoryMaster().getI4QuestionSets();
				}else if(jobOrder.getDistrictMaster().getOfferVirtualVideoInterview()!=null && jobOrder.getDistrictMaster().getOfferVirtualVideoInterview() && jobOrder.getDistrictMaster().getSendAutoVVILink()!=null && jobOrder.getDistrictMaster().getSendAutoVVILink()){
					if(jobOrder.getDistrictMaster().getI4QuestionSets()!=null)
						i4QuestionSets = jobOrder.getDistrictMaster().getI4QuestionSets();
				}
				else {
					// no VVI is setup at any level
					System.out.println("  No question Set attached.");
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			//System.out.println("i4QuestionSets:::::::::::::::::::::::::::::"+i4QuestionSets);
			if(i4QuestionSets!=null){
				ScheduleI4Thread dsmt = new ScheduleI4Thread();
				dsmt.setCGInviteInterviewAjax(cGInviteInterviewAjax);
				dsmt.setTeacherDetail(teacherDetail);
				dsmt.setJobOrder(jobOrder);
				dsmt.setI4QuestionSets(i4QuestionSets);
				dsmt.setUserMaster(userMaster);
				dsmt.setRequest(request);
				try {
					dsmt.start();	
				} catch (Exception e) {}
			}
		}
	}

	public void mailToTeacher(JobForTeacher jobForTeacher,StatusMaster statusMaster,StatusMaster statusMasterCC,SecondaryStatus secondaryStatus,SecondaryStatus secondaryStatusCC,JobOrder jobOrder,TeacherDetail teacherDetail,UserMaster userMaster,HttpServletRequest request){
		
		System.out.println("------------------------------------------------------------------------------mailToTeacher322------------------------------------------------------------------------------");
		
		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::mailToTeacher322 ::::::::::::::::::::::::::::::::::::::::");
		System.out.println(":::"+statusMaster+" :"+statusMasterCC+"::"+secondaryStatus+":::"+secondaryStatusCC);
		boolean mailSend=false;
		SecondaryStatus  secondaryStatusObj =secondaryStatusDAO.findSecondaryStatusByJobOrder(jobForTeacher.getJobId(),"School Selection");
		if(secondaryStatusObj!=null){
			if(statusMasterCC!=null){
				System.out.println("::::::::::::statusMasterCC:::"+statusMasterCC.getStatusId());
				System.out.println("::::::::::::statusMaster:::"+statusMaster.getStatusId());
				if(statusMasterCC.getStatusId().equals(statusMaster.getStatusId())){
					mailSend=true;;
				}
			}else if(secondaryStatusCC!=null){
				System.out.println("secondaryStatusCC:::::"+secondaryStatusCC.getSecondaryStatusName());
				System.out.println("secondaryStatus:::::"+secondaryStatus.getSecondaryStatusName());
				if(secondaryStatusCC.getSecondaryStatusName().equalsIgnoreCase(secondaryStatus.getSecondaryStatusName())){
					mailSend=true;;
				}
			}
		}
		
		//System.out.println("Before mailSend:::::::::::::::::::::::::::::::::::"+mailSend);
		if(mailSend  && jobOrder!=null && jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getSchoolSelection()!=null )
			if(!jobOrder.getJobCategoryMaster().getSchoolSelection())
				mailSend=false;
		
		System.out.println("After mailSend:::::::::::::::::::::::::::::::::::"+mailSend);
		if(mailSend){
			try{
				jobForTeacher.setCandidateConsideration(false);
				jobForTeacherDAO.updatePersistent(jobForTeacher);
			}catch(Exception e){
				e.printStackTrace();
			}

			SchoolSelectionThread dsmt = new SchoolSelectionThread();
			dsmt.setSchoolSelectionAjax(schoolSelectionAjax);
			dsmt.setTeacherDetail(teacherDetail);
			dsmt.setJobOrder(jobOrder);
			dsmt.setUserMaster(userMaster);
			dsmt.setRequest(request);
			try {
				dsmt.start();	
			} catch (Exception e) {}
		}
	}
	public void sendOnlineActivityLink(Integer teacherId,Integer jobId,Integer questionSetId,Integer flag){
		//System.out.println(flag+":::::::::::::=sendOnlineActivityLink::::::"+questionSetId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session1 = request.getSession(false);
		if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
		if(flag!=null && flag!=0){
			OnlineActivityQuestionSet onlineActivityQuestionSet=onlineActivityQuestionSetDAO.findById(questionSetId, false,false);
			OnlineActivityStatus status=new OnlineActivityStatus();
			status.setTeacherDetail(teacherDetail);
			status.setDistrictMaster(jobOrder.getDistrictMaster());
			status.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
			status.setOnlineActivityQuestionSet(onlineActivityQuestionSet);
			status.setStatus("S");
			status.setActivityStartTime(new Date());
			status.setActivityDate(new Date());
			onlineActivityStatusDAO.makePersistent(status);
		}

		MailSendSchedulerThread dsmt = new MailSendSchedulerThread();
		dsmt.setQuestionSetId(questionSetId);
		dsmt.setOnlineActivityAjax(onlineActivityAjax);
		dsmt.setRequest(request);
		dsmt.setJobId(jobId);
		dsmt.setOnlineActivityFlag(0);
		dsmt.setTeacherId(teacherId);
		dsmt.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
		try {
			dsmt.start();	
		} catch (Exception e) {}
	}
	public String displayAllAndSelecedDist(String districtId,String jobCategoryId,String secondaryStatusId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		StringBuffer sbAttachSchool=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		boolean selectedDAS = false;
		boolean allAdmin = false;
		boolean overrideAdm = false;
		boolean notifyAll = false;
		try
		{
			DistrictMaster districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(Integer.parseInt(jobCategoryId), false, false);
			SecondaryStatus secondaryStatus = secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId), false, false);

			
			
			List<UserMaster> allActiveUserList = userMasterDAO.getActiveDAUserByDistrict(districtMaster);
			List<JobCategoryWiseStatusPrivilege> jcwpsList = new ArrayList<JobCategoryWiseStatusPrivilege>();

			jcwpsList = jobCategoryWiseStatusPrivilegeDAO.getFilterStatusByName(districtMaster, jobCategoryMaster, secondaryStatus);

			try{
				if(jcwpsList.get(0).getAutoNotifyAll()!=null){
					if(jcwpsList.get(0).getAutoNotifyAll())
						notifyAll = true;
					else
						notifyAll = false;
				}else{
					List<StatusWiseAutoEmailSend> listStatusWiseAutoEmailSends =new ArrayList<StatusWiseAutoEmailSend>(); 
					listStatusWiseAutoEmailSends = 	statusWiseAutoEmailSendDAO.findAllRecords(districtMaster);	
					
					//List<Integer> statusIDs = new ArrayList<Integer>();
					List<Integer> sStatusID = new ArrayList<Integer>();
					List<SecondaryStatus> seconList = new ArrayList<SecondaryStatus>();
					List<String> ssNameList = new ArrayList<String>();
					
					if(listStatusWiseAutoEmailSends!=null && listStatusWiseAutoEmailSends.size()>0){
						 for(StatusWiseAutoEmailSend swes : listStatusWiseAutoEmailSends){
							 if(swes.getSecondaryStatusId()!=null)
							 {
							  sStatusID.add( swes.getSecondaryStatusId());
							 }
						 }
					}
					
					seconList = secondaryStatusDAO.findSecStatusByStatusIdList(sStatusID);
					
					if(seconList!=null && seconList.size()>0){
						for(SecondaryStatus ss:seconList){
							ssNameList.add(ss.getSecondaryStatusName());
						}
					}
					
					
					//System.out.println("  district SS NAme ::::::::::::: "+ssNameList+" :::::::::: SLC SSNAme :::::::::::: "+secondaryStatus.getSecondaryStatusName());
					if(ssNameList.contains(secondaryStatus.getSecondaryStatusName())){
						notifyAll = true;
					}
				}
			}catch(Exception e){
				e.printStackTrace();	
			}

			String daIds = jcwpsList.get(0).getEmailNotificationToSelectedDistrictAdmins();

			//System.out.println(" jcwpsList.get(0).getEmailNotificationToSelectedDistrictAdmins() "+jcwpsList.get(0).getEmailNotificationToSelectedDistrictAdmins());
			//System.out.println(" jcwpsList.get(0).getJobCategoryStatusPrivilegeId() "+jcwpsList.get(0).getJobCategoryStatusPrivilegeId());

			String arr[]=null;
			List multiDAIds = new ArrayList();



			if(daIds!=null && !daIds.equals(""))
			{
				if(daIds!="" && daIds.length() > 0)
				{
					arr=daIds.split("#");
				}

				if(arr!=null && arr.length > 0)
				{
					for(int i=0;i<arr.length;i++)
					{
						multiDAIds.add(Integer.parseInt((arr[i])));	
					}
				}
			}

			List<UserMaster> selectedUserList = userMasterDAO.getUserlistByUserIdArray(multiDAIds);
			allActiveUserList.removeAll(selectedUserList);

			//Unselected DA List
			sb.append("<select multiple id=\"lstDistrictAdmins\" name=\"lstDistrictAdmins\" class=\"form-control\" style=\"height: 150px;\" >");
			if(allActiveUserList.size()>0)
			{
				for (UserMaster selectedDA : allActiveUserList) {
					sb.append("<option value="+selectedDA.getUserId()+">"+getUserName(selectedDA)+"("+selectedDA.getEmailAddress()+")</option>");
				}
			}
			sb.append("</select>");

			//Selected DA List
			sbAttachSchool.append("<select multiple class=\"form-control\" id=\"attachedDAList\" name=\"attachedDAList\" style=\"height: 150px;\">");
			if(selectedUserList.size()>0)
			{
				for (UserMaster selectdDA : selectedUserList) {
					sbAttachSchool.append("<option value="+selectdDA.getUserId()+">"+getUserName(selectdDA)+"("+selectdDA.getEmailAddress()+"</option>");
				}
				selectedDAS = true;
			}
			sbAttachSchool.append("</select>");

			allAdmin = jcwpsList.get(0).isEmailNotificationToAllSchoolAdmins();
			
			overrideAdm = jcwpsList.get(0).isOverrideUserSettings();

		}catch(Exception e)
		{
			e.printStackTrace();
		}

		//System.out.println(" sb.toString() :: "+sb.toString());
		//System.out.println(" sbAttachSchool.toString() :: "+sbAttachSchool.toString());
		//System.out.println(" selectedDAS :: "+selectedDAS);
		//System.out.println(" allAdmin :: "+allAdmin);//System.out.println(" overrideAdm :>>>>>: "+overrideAdm+" notifyAll :: "+notifyAll);
		
		return sb.toString()+"||"+sbAttachSchool.toString()+"||"+selectedDAS+"||"+allAdmin+"||"+overrideAdm+"||"+notifyAll;
	}
	public static String getUserName(UserMaster userMaster){
		String userName="";
		try{
			if(userMaster.getFirstName()!=null){
				userName=userMaster.getFirstName();
			}
			if(userMaster.getFirstName()!=null && userMaster.getLastName()!=null){
				userName=userMaster.getFirstName()+" "+userMaster.getLastName();
			}
		}catch(Exception e){	e.printStackTrace(); }
		return userName;
	}
	
	//added By Ram Nath
	public String statusWise(JobOrder jobOrder,List<TeacherDetail> lstTeacherDetail,StatusMaster statusMaster,SecondaryStatus secondaryStatus,String statusMasterSecondaryName,UserMaster userMaster,String jobForTeacherId,Long schoolId){
		System.out.println(">>>>>>>>>>>>::::::statusWise ::::::::yyy::::::<<<<<<<<<<<<<<<<<< StatusMaster=="+statusMaster+"  secondaryStatus=="+secondaryStatus);
		WebContext context;
	    context = WebContextFactory.get();
	    HttpServletRequest request = context.getHttpServletRequest();
	    HttpSession session1 = request.getSession(false);
	    if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
	          throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
	    
	    //only for COLUMBUS District   //1200390 Miami       //3904380 Columbus
	    if(jobOrder!=null && jobOrder.getDistrictMaster()!=null &&  jobOrder.getDistrictMaster().getDistrictId().toString().trim().equals("3904380")){
		    boolean messageFlag=false;
		    if(statusMaster!=null){
		    	if(statusMasterSecondaryName!=null && statusMasterSecondaryName.trim().equals("Advance to Onboarding"))
		    		messageFlag=true;
		    }else if(secondaryStatus!=null){
		    	if(secondaryStatus.getSecondaryStatusName().trim().equals("Advance to Onboarding"))
		    		messageFlag=true;
		    }
		    System.out.println("messageFlag::::"+messageFlag);
		    try{
			    if(messageFlag){
		    	TeacherDetail teacherId=lstTeacherDetail.get(0);
			    AdvOnBoardingThread loct = new AdvOnBoardingThread();
					loct.setOnlineActivityAjax(onlineActivityAjax);
					loct.setRequest(request);
					loct.setJobId(jobOrder);
					loct.setTeacherId(teacherId);
					loct.setUserMaster(userMaster);
					loct.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
					try {
						loct.start();	
					} catch (Exception e) {}
				}
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		 }
	    if(jobOrder.getDistrictMaster()!=null){	
		    Map<Integer, JobForTeacher> jobForTeachersMap = new LinkedHashMap<Integer, JobForTeacher>();
		    if(jobForTeacherId!=null){
		    	JobForTeacher jobForTeacher=jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
		    	jobForTeachersMap.put(lstTeacherDetail.get(0).getTeacherId(), jobForTeacher);
		    	}else{
				try {
					//System.out.println(" teacherDetailLst :: "+lstTeacherDetail.size()+" jobOrder :: "+jobOrder.getJobId());
					for (JobForTeacher jft : jobForTeacherDAO.getJFTByTIDsAndJID(lstTeacherDetail, jobOrder))
						jobForTeachersMap.put(jft.getTeacherId().getTeacherId(), jft);  
				}catch(Exception e){}
		     }
		    Session statelessSession=null;
			try{
				statelessSession = sessionFactory.openSession();
			    for(TeacherDetail teacherId1: lstTeacherDetail)
			    {
				    JobForTeacher jobForTeacher=jobForTeachersMap.get(teacherId1.getTeacherId());
					Boolean isMailSend=null;
					String callingControllerText="";
					String subject="";
					if(jobOrder!=null && jobForTeacher!=null && (jobForTeacher.getOfferReady()==null || !jobForTeacher.getOfferReady())){	
					    if(statusMaster!=null){
					    	if(statusMasterSecondaryName!=null){
					    		isMailSend=OFFER_READY_MAP.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+statusMasterSecondaryName.trim());
					    		callingControllerText=statusMasterSecondaryName.trim();
					    	}else{
					    		isMailSend=OFFER_READY_MAP.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+statusMaster.getStatus().trim());
					    		callingControllerText=statusMaster.getStatus().trim();
					    	}
					    }else if(secondaryStatus!=null){
					    		isMailSend=OFFER_READY_MAP.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+secondaryStatus.getSecondaryStatusName().trim());
					    		callingControllerText=secondaryStatus.getSecondaryStatusName().trim();
					    }
					    subject=callingControllerText;
					}
					
					
					Boolean notificaitonMail=null;
					try{
						notificaitonMail=NOTIFICATION_MAP.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+callingControllerText.trim());
					}catch (Exception e) {
						e.printStackTrace();
					}
					if(notificaitonMail!=null && notificaitonMail){
						if(jobOrder.getDistrictMaster().getDistrictId().equals(804800) && callingControllerText.equalsIgnoreCase("Offer Made")){
							SchoolMaster schoolMasterObj=null;
							if(callingControllerText.equalsIgnoreCase("Offer Made") && userMaster.getEntityType()==3){
								schoolMasterObj=userMaster.getSchoolId();
							}
							String allEmailId="";
							subject="Notification of \""+callingControllerText+"\"";
							callingControllerText="JEFFCO_NF_"+callingControllerText;
							if(jobOrder.getEmploymentServicesTechnician().getPrimaryEmailAddress()!=null && !jobOrder.getEmploymentServicesTechnician().getPrimaryEmailAddress().trim().equals(""))
								allEmailId=jobOrder.getEmploymentServicesTechnician().getPrimaryEmailAddress();
							if(jobOrder.getEmploymentServicesTechnician().getBackupEmailAddress()!=null && !jobOrder.getEmploymentServicesTechnician().getBackupEmailAddress().trim().equals(""))
								if(allEmailId.trim().equals(""))
									allEmailId=jobOrder.getEmploymentServicesTechnician().getBackupEmailAddress();
								else
									allEmailId+=","+jobOrder.getEmploymentServicesTechnician().getBackupEmailAddress();
								if(!allEmailId.trim().equals(""))
								sendNotificationMailToAllEmailIds(schoolMasterObj, jobOrder,  teacherId1,  userMaster, callingControllerText,  null,  allEmailId, subject, statelessSession);
						}
					}
					//System.out.println("HR Review/Offer==Checking Mail Send Or nOt=="+isMailSend+"         "+callingControllerText);
					if(isMailSend!=null && isMailSend){
							String statusFlagAndStatusId="";
							try{
								if(statusMaster!=null){
									statusFlagAndStatusId="s@@"+statusMaster.getStatusId();					
								}else{
									statusFlagAndStatusId="ss@@"+secondaryStatus.getSecondaryStatusId();					
								}
							}catch(Exception e){}
						
								String allSchoolUserEmailId="";
								SchoolMaster schoolMaster=null;
								if(schoolId>0){
									schoolMaster=schoolMasterDAO.findById(schoolId, false, false);				
								}
								if(callingControllerText.equalsIgnoreCase("Offer Made") && userMaster.getEntityType()==3){
									schoolMaster=userMaster.getSchoolId();
								}
								if(callingControllerText.equalsIgnoreCase("HR Review/Offer")){
									List<UserMaster> umList=new LinkedList<UserMaster>();											
									if(schoolMaster!=null){
										umList=userMasterDAO.getAllUserByDistrictAndSchool(jobOrder.getDistrictMaster(), schoolMaster);
									}
									allSchoolUserEmailId=getAllEmailId(umList);							
								}
								if(callingControllerText.equalsIgnoreCase("Offer Made")){
									subject=GlobalServices.getConvertedString(Utility.getLocaleValuePropByKey("offerMadeSubject", locale),"#brk#",new String[]{jobOrder.getJobId().toString(),jobOrder.getJobTitle()});
									//subject=subject+" (Job "+jobForTeacher.getJobId().getJobId()+"- "+jobForTeacher.getJobId().getJobTitle()+")";
								}
						
								AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
								loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
								loct.setSchoolId(schoolMaster);
								loct.setCallbytemplete(0);
								loct.setJobId(jobOrder);
								loct.setTeacherId(teacherId1);
								loct.setUserMaster(userMaster);
								loct.setEmailId(teacherId1.getEmailAddress());
								loct.setCallingControllerText(callingControllerText);
								loct.setDistrictId(jobOrder.getDistrictMaster());
								loct.setStatusFlagAndStatusId(statusFlagAndStatusId);
								loct.setSubject(subject);	
								loct.setStatelessSession(statelessSession);
								try {
									new Thread(loct).start();
								} catch (Exception e) {}
								
								if(callingControllerText.trim().equalsIgnoreCase("HR Review/Offer") && allSchoolUserEmailId!=null && !allSchoolUserEmailId.trim().equalsIgnoreCase("")){					
									sendNotificationMailToAllEmailIds(schoolMaster, jobOrder,  teacherId1,  userMaster, "allSchoolUserNotificationMail",  statusFlagAndStatusId,  allSchoolUserEmailId, "Notification of "+callingControllerText, statelessSession);
								}
						}
		    		}
				}catch(Exception e) {e.printStackTrace();}
	    }
	    
		return null;
	}
	
	private boolean sendNotificationMailToAllEmailIds(SchoolMaster schoolMaster, JobOrder jobOrder, TeacherDetail teacherId, UserMaster userMaster,String callingControllerText, String statusFlagAndStatusId, String allEmailId,String mailSubject, Session statelessSession){
		AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
		loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
		loct.setSchoolId(schoolMaster);
		loct.setCallbytemplete(0);
		loct.setJobId(jobOrder);
		loct.setTeacherId(teacherId);
		loct.setUserMaster(userMaster);
		loct.setEmailId(allEmailId);
		loct.setCallingControllerText(callingControllerText);
		loct.setDistrictId(jobOrder.getDistrictMaster());
		loct.setStatusFlagAndStatusId(statusFlagAndStatusId);
		loct.setStatelessSession(statelessSession);
		loct.setSubject(mailSubject);
			try {				
				new Thread(loct).start();	
			} catch (Exception e) { e.printStackTrace();return false;}
			return true;
	}
	//added By Ram Nath
	private String statusWiseFlow(JobOrder jobOrder,TeacherDetail teacherId,StatusMaster statusMaster,SecondaryStatus secondaryStatus,String statusMasterSecondaryName,UserMaster userMaster,String flowOptionsName){
		System.out.println(statusMasterSecondaryName+">>>>>>>>>>>>::::::statusWiseFlow ::::::::::::::<<<<<<<<<<<<<<<<<< StatusMaster=="+statusMaster+"  secondaryStatus=="+secondaryStatus);
		WebContext context;
	    context = WebContextFactory.get();
	    HttpServletRequest request = context.getHttpServletRequest();
	    HttpSession session1 = request.getSession(false);
	    if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
	          throw new IllegalStateException("Your session has expired!");
	    }
		Boolean isMailSend=null;
		String callingControllerText="";
		IS_PRINT=true;
	    if(statusMaster!=null){
	    	if(statusMasterSecondaryName!=null){
	    		isMailSend=OFFER_READY_MAP.get("Head##"+userMaster.getEntityType()+"##"+statusMasterSecondaryName.trim());
	    		callingControllerText=statusMasterSecondaryName.trim();
	    	}else{
	    		isMailSend=OFFER_READY_MAP.get("Head##"+userMaster.getEntityType()+"##"+statusMaster.getStatus().trim());
	    		callingControllerText=statusMaster.getStatus().trim();
	    	}
	    }else if(secondaryStatus!=null){
	    		isMailSend=OFFER_READY_MAP.get("Head##"+userMaster.getEntityType()+"##"+secondaryStatus.getSecondaryStatusName().trim());
	    		callingControllerText=secondaryStatus.getSecondaryStatusName().trim();
	    }
	    System.out.println("isMailSend:::::::>::::::"+isMailSend);
		if(isMailSend!=null && isMailSend){
			String statusFlagAndStatusId="";			
			try{
				if(statusMaster!=null){
					statusFlagAndStatusId="s@@"+statusMaster.getStatusId();
				}else{
					statusFlagAndStatusId="ss@@"+secondaryStatus.getSecondaryStatusId();
				}
			}catch(Exception e){}
			AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
			loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
			loct.setCallbytemplete(0);
			loct.setJobId(jobOrder);
			loct.setTeacherId(teacherId);
			loct.setUserMaster(userMaster);
			loct.setEmailId(teacherId.getEmailAddress());
			loct.setCallingControllerText(callingControllerText);
			if(jobOrder.getHeadQuarterMaster()==null){
				loct.setDistrictId(jobOrder.getDistrictMaster());
			}
			loct.setFlowOptionsName(flowOptionsName);
			loct.setStatusFlagAndStatusId(statusFlagAndStatusId);
			loct.setSubject(callingControllerText);
			loct.setStatelessSession(sessionFactory.openSession());
			try {
				/*Runnable worker = loct;
				executor.execute(worker);*/
				new Thread(loct).start();
			} catch (Exception e) {}
		}
		return null;
	}
	//ended By Ram Nath
	
	//added by Ram Nath for staffer philadephia
	public boolean philadelphiaMail(JobOrder jobOrder,TeacherDetail teacherId,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster,int entityType, String statusFlagAndStatusId){
		//System.out.println(">>>>>>>>>>>>::::::philadelphiaMail ::::::::::::::<<<<<<<<<<<<<<<<<< StatusMaster=="+statusMaster+"  secondaryStatus=="+secondaryStatus);
		WebContext context;
		boolean isSendMail=false;
	    context = WebContextFactory.get();
	    HttpServletRequest request = context.getHttpServletRequest();
	    HttpSession session1 = request.getSession(false);
	    if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
	          throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
	    if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(000000) && statusMaster!=null && statusMaster.getStatus().equals("No Evaluation Complete"))
	    	isSendMail=true;
	    else if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990) && secondaryStatus!=null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("No Evaluation Complete"))
	    	isSendMail=true;

	    if(isSendMail){
			try{
				MailSendEvlCompThread loct = new MailSendEvlCompThread();
					loct.setOnlineActivityAjax(onlineActivityAjax);
					if(entityType==3){
						loct.setSchoolUser(userMaster);
					loct.setTemp(0);
					}if(entityType==2){
						loct.setSchoolUser(getSchoolUser(teacherId,jobOrder,statusFlagAndStatusId));
					loct.setTemp(1);
					}
					loct.setStatusFlagAndStatusId(statusFlagAndStatusId);
					loct.setJobId(jobOrder);
					loct.setTeacherId(teacherId);
					loct.setUserMaster(userMaster);
					loct.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
					try {
						loct.start();	
						return loct.isMailSend();
					} catch (Exception e) {}					
		    }catch(Exception e){
		    	e.printStackTrace();
		    	return false;
		    }			
		}
		return false;
	}	
	
	 private UserMaster getSchoolUser(TeacherDetail teacherDetail, JobOrder jobOrder, String statusFlagAndStatusId){
			String statusFlag=statusFlagAndStatusId.split("##")[0];
			String statusId=statusFlagAndStatusId.split("##")[1];
			SecondaryStatus secondaryStatus_temp=null;
			StatusMaster statusMaster_temp=null;
			if(statusFlag.equals("ss")){
				secondaryStatus_temp=secondaryStatusDAO.findById(Integer.parseInt(statusId), false,false);
			}else{
				statusMaster_temp=statusMasterDAO.findById(Integer.parseInt(statusId), false,false);
			}
			List<JobWisePanelStatus> jobWisePanelStatusList=jobWisePanelStatusDAO.getPanel(jobOrder,secondaryStatus_temp,statusMaster_temp);
			List<UserMaster> panelAttendees_ForEmail=new ArrayList<UserMaster>();
			List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();
			UserMaster schoolUser=null;
			PanelSchedule panelSchedule=null;
			if(jobWisePanelStatusList.size() == 1){
				panelScheduleList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWisePanelStatusList,teacherDetail);
				if(panelScheduleList.size()==1){
					
					if(panelScheduleList.get(0)!=null){
						panelSchedule=panelScheduleList.get(0);
						List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
						panelAttendees=panelAttendeesDAO.getPanelAttendees(panelSchedule);
						if(panelAttendees.size() > 0){
							for(PanelAttendees attendees:panelAttendees){
								if(attendees!=null){
									panelAttendees_ForEmail.add(attendees.getPanelInviteeId());
									if(attendees.getPanelInviteeId().getEntityType()==3){
										schoolUser=attendees.getPanelInviteeId();
									}
								}
							}
						}
						
					}
				}
			}
			if(schoolUser!=null)
			return schoolUser;
			else 
				return null;
		}
	//ended By Ram Nath
	 
	 /*
	  * 	Print Reference Data : 
	  * 	By Hanzala Subhani
	  * 	Date : 31 March 2015
	  * 
	  */
	 public String printReferenceData(Integer teacherId,Integer referenceJobId,String elerefAutoIds){
		 
			UserMaster userMaster 			= 	null;
			DistrictMaster districtMaster 	= 	null;
			SchoolMaster schoolMaster 		= 	null;
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session1 = request.getSession(false);
			if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}

			if (session1 == null || session1.getAttribute("userMaster") == null) {

			} else {

				userMaster=(UserMaster)session1.getAttribute("userMaster");

				if(userMaster.getDistrictId()!=null){

					districtMaster = userMaster.getDistrictId();

				}

				if(userMaster.getSchoolId()!=null){
					schoolMaster =userMaster.getSchoolId();
				} 
			}

			Integer counter 	= 1;
			String arr[]=elerefAutoIds.split("##");
			StringBuffer sb = new StringBuffer();
			
			try{
				
				JobOrder jobOrder = jobOrderDAO.findById(referenceJobId, false, false); 
				TeacherDetail teacherDetail	=	teacherDetailDAO.findById(teacherId, false, false);
				if(districtMaster == null)
					districtMaster = jobOrder.getDistrictMaster();
				List<TeacherElectronicReferences> teacherElectronicReferenceList = teacherElectronicReferencesDAO.findByTeacher(teacherDetail);
				List<DistrictSpecificRefChkAnswers> districtSpecificRefChkAnswersList = null;
				
				List<DistrictSpecificRefChkOptions> districtSpecificRefChkOptionsList	=	districtSpecificRefChkOptionsDAO.findAll();	

				Map<Integer, DistrictSpecificRefChkOptions> mapReckOption	=	new HashMap<Integer, DistrictSpecificRefChkOptions>();
				for(DistrictSpecificRefChkOptions refCheckOption : districtSpecificRefChkOptionsList){
					Integer key = refCheckOption.getOptionId();
					mapReckOption.put(key, refCheckOption);
				}
		if(arr.length>0) {
			sb.append("<div style='padding-bottom:20px;' align='center'><img align='center' src='http://23.253.165.119/images/Logo%20with%20Beta300.png'></div>");
			for(int i=1;i<arr.length;i++) {
				TeacherElectronicReferences tElRef = teacherElectronicReferencesDAO.findById(Integer.parseInt(arr[i]), false, false);
				districtSpecificRefChkAnswersList = districtSpecificRefChkAnswersDAO.findTeacherAnswersByDistrictAndRefChk(districtMaster, Integer.parseInt(arr[i]));
				
				List<TeacherElectronicReferencesHistory> tERefH = null;
				tERefH	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefIdAndDist(tElRef, districtMaster, teacherDetail);
				
				sb.append("<table width='100%' border='0'>");
				DistrictSpecificRefChkOptions referenceChkOptionVal	= null;
				if(districtSpecificRefChkAnswersList.size() != 0){
					sb.append("<tr>");
					sb.append("<td align=\"center\" colspan=\"2\"><H3><B>");
					sb.append(Utility.getLocaleValuePropByKey("  msgReferencefor", locale));
					sb.append(teacherDetail.getFirstName()+" "+teacherDetail.getLastName());
					sb.append(Utility.getLocaleValuePropByKey("  msgprovidedby", locale));
					sb.append(tElRef.getFirstName()+" "+tElRef.getLastName());
					sb.append(" for ");
					sb.append(districtMaster.getDistrictName());
					sb.append("</B></H3></td></tr>");
					sb.append("<tr><td>&nbsp;</td></tr>");
					if(districtSpecificRefChkAnswersList!=null && districtSpecificRefChkAnswersList.size()>0){
						sb.append("<tr>");
							sb.append("<td width=\"20%\"><B>Date Provided:</B></td>");
							sb.append("<td><B>");
							sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(districtSpecificRefChkAnswersList.get(0).getCreatedDateTime()));
							sb.append("</B></td>");
						sb.append("</tr>");
					}
					if(tElRef.getDesignation()!=null){
						sb.append("<tr>");
							sb.append("<td width=\"20%\"><B>Role:</B></td>");
							sb.append("<td><B>");
							sb.append(tElRef.getDesignation());
							sb.append("</B></td>");
						sb.append("</tr>");
					}
					if(tElRef.getEmail()!=null){
						sb.append("<tr>");
							sb.append("<td width=\"20%\"><B>Email:</B></td>");
							sb.append("<td><B>");
							sb.append(tElRef.getEmail());
							sb.append("</B></td>");
						sb.append("</tr>");
					}
					if(tElRef.getContactnumber()!=null){
						sb.append("<tr>");
							sb.append("<td width=\"20%\"><B>Phone:</B></td>");
							sb.append("<td><B>");
							sb.append(tElRef.getContactnumber());
							sb.append("</B></td>");
						sb.append("</tr>");
					}
					
					sb.append("<tr><td>&nbsp;</td></tr>");
					for(DistrictSpecificRefChkAnswers answerRecord : districtSpecificRefChkAnswersList){
						sb.append("<tr>");
						sb.append("<td colspan=\"2\">");
						sb.append("<B>"+Utility.getLocaleValuePropByKey("  lblQues", locale)+": "+counter+"<BR>");
						sb.append(answerRecord.getDistrictSpecificRefChkQuestions().getQuestion());
						sb.append("</B></td>");
						sb.append("</tr>");

						if(answerRecord.getQuestionOption() != null){
							sb.append("<tr>");
							sb.append("<td colspan=\"2\">");
							sb.append("<B>"+Utility.getLocaleValuePropByKey("  msgAnswer", locale)+" </B><BR>");
							sb.append(answerRecord.getQuestionOption());
							sb.append("</td>");
							sb.append("</tr>");
						}

						if(answerRecord.getQuestionMaxScore() != null){
							
							if(answerRecord.getQuestionMaxScore().equals(999)){
								sb.append("<tr>");
								sb.append("<td colspan=\"2\">");
								sb.append(Utility.getLocaleValuePropByKey("msgNoBasisjudgement", locale));
								sb.append("</td>");
								sb.append("</tr>");
							} else {
								sb.append("<tr>");
								sb.append("<td colspan=\"2\">");
								sb.append("<B>"+Utility.getLocaleValuePropByKey("lblScr", locale)+": </B>");
								sb.append(answerRecord.getQuestionMaxScore());
								sb.append("</td>");
								sb.append("</tr>");
							}
						}

						if(answerRecord.getQuestionType().equals("ml") || answerRecord.getQuestionType().equals("sloet")){
							sb.append("<tr>");
							sb.append("<td colspan=\"2\">");
							sb.append(answerRecord.getInsertedText());
							sb.append("</td>");
							sb.append("</tr>");
						}

						if(answerRecord.getQuestionType().equals("SLD")){
							sb.append("<tr>");
							sb.append("<td colspan=\"2\">");
							sb.append("<B>"+Utility.getLocaleValuePropByKey("  msgAdditionalComments", locale)+": </B><BR>");
							sb.append(answerRecord.getInsertedText());
							sb.append("</td>");
							sb.append("</tr>");
						}

						if(answerRecord.getQuestionType().equals("mlsel")){
							sb.append("<tr>");
							sb.append("<td colspan=\"2\">");
							sb.append("<B>"+Utility.getLocaleValuePropByKey("  msgAnswer", locale)+" </B><BR>");
							sb.append("</td>");
							sb.append("</tr>");

							String questionOption 	= 	"";
							Integer optionId		=	null;
							questionOption			=	answerRecord.getSelectedOptions();
							String	OptionText		=	"";
							Integer	count			=	1;
							try{
								questionOption	=	questionOption.replace('|', ',');
								String jobCategoryIdStr[] =	questionOption.split(",");
								Integer oIds[] = new Integer[jobCategoryIdStr.length];

								for(int j=0;j < oIds.length;j++){
									oIds[j]=Integer.parseInt(jobCategoryIdStr[j]);
									referenceChkOptionVal = mapReckOption.get(oIds[j]);
									OptionText	=	""+Utility.getLocaleValuePropByKey("  lblOpt", locale)+" "+count+" : "+referenceChkOptionVal.getQuestionOption();

									sb.append("<tr>");
									sb.append("<td colspan=\"2\">");
									sb.append("<B>"+Utility.getLocaleValuePropByKey("  lblOpt", locale)+" "+count+": </B>");
									sb.append(referenceChkOptionVal.getQuestionOption());
									sb.append("</td>");
									sb.append("</tr>");

									count++;
								}
							} catch (Exception exception){ }
						}
						if(answerRecord.getQuestionType().equals("mloet")){
							sb.append("<tr>");
							sb.append("<td colspan=\"2\">");
							sb.append("<B>"+Utility.getLocaleValuePropByKey("  msgAnswer", locale)+" </B><BR>");
							sb.append("</td>");
							sb.append("</tr>");

							String questionOption 	= 	"";
							Integer optionId		=	null;
							questionOption			=	answerRecord.getSelectedOptions();
							String	OptionText		=	"";
							Integer	count			=	1;

							try{
								questionOption	=	questionOption.replace('|', ',');
								String jobCategoryIdStr[] =	questionOption.split(",");
								Integer oIds[] = new Integer[jobCategoryIdStr.length];

								for(int j=0;j < oIds.length;j++){
									oIds[j]=Integer.parseInt(jobCategoryIdStr[j]);
									referenceChkOptionVal = mapReckOption.get(oIds[j]);
									OptionText	=	""+Utility.getLocaleValuePropByKey("  lblOpt", locale)+" "+count+" : "+referenceChkOptionVal.getQuestionOption();

									sb.append("<tr>");
									sb.append("<td colspan=\"2\">");
									sb.append("<B>"+Utility.getLocaleValuePropByKey("  lblOpt", locale)+" "+count+": </B>");
									sb.append(referenceChkOptionVal.getQuestionOption());
									sb.append("</td>");
									sb.append("</tr>");
									count++;
								}
							} catch (Exception exception){ }
						}
						sb.append("<tr>");
						sb.append("<td colspan=\"2\">&nbsp;</td>");
						sb.append("</tr>");

						counter++;
					}
				}
				sb.append("</table>");
				sb.append("<div style='page-break-before: always;'>");
				sb.append("</div>");
				}
			  }
			}catch(Exception exception){
				exception.printStackTrace();
			}
			return sb.toString();
	}
	 
	//added by 01-04-2015
	 @Transactional
	 public String setAndGetTeacherStatusSlider(String teacherStatusScoreId, String teacherId, String jobId,String statusMasterId, String secondaryStatusId, String score){
		 UserMaster userMaster 			= 	null;
			DistrictMaster districtMaster 	= 	null;
			SchoolMaster schoolMaster 		= 	null;
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session1 = request.getSession(false);
			if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			if (session1 != null || session1.getAttribute("userMaster") != null) {
				userMaster=(UserMaster)session1.getAttribute("userMaster");
			}
		 StringBuffer sb=new StringBuffer();
		 //System.out.println(">>>>>>>>>>>>>>>>>>>>>:::::::::::::MousteOverDivEdit::::::::::::::<<<<<<<<<<<<<<<<<<<<<<<<status=="+statusMasterId+" secondaryStatusId== "+secondaryStatusId);
		 List<TeacherStatusScores> teacherStatusScoresList= new ArrayList<TeacherStatusScores>();
			try{
				TeacherDetail teacherDetail=teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
				JobOrder jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
				StatusMaster statusMaster=null;
				if(statusMasterId!=null && !statusMasterId.equals(""))
					statusMaster=statusMasterDAO.findById(Integer.parseInt(statusMasterId), false, false);
				SecondaryStatus secondaryStatus=null;
				if(secondaryStatusId!=null && !secondaryStatusId.equals(""))
					secondaryStatus=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId), false, false);
				
				TeacherStatusScores tss=teacherStatusScoresDAO.findById(Integer.parseInt(teacherStatusScoreId), false, false);
				
			//update fit score					
				 int minPrveScore=0;
		         int totalPrevScore=0;
	            try{
	            	Double[] teacherStatusScoresAvg=teacherStatusScoresDAO.getAllFinalizeStatusScoreAvg(teacherDetail, jobOrder);
	                if(teacherStatusScoresAvg.length>0){
	                        minPrveScore=Integer.parseInt(Math.round(teacherStatusScoresAvg[0])+"");
	                        totalPrevScore=Integer.parseInt(Math.round(teacherStatusScoresAvg[1])+"");
	                }
	            }catch(Exception e){ e.printStackTrace();}
	            //System.out.println("minPrveScore:::::::"+minPrveScore);
	            //System.out.println("totalPrevScore:::::::"+totalPrevScore);

			
	          //insert copy  history score
				TeacherStatusScoresHistory copyTSSH=new TeacherStatusScoresHistory();
	 			 try {
					BeanUtils.copyProperties(copyTSSH, tss);
					copyTSSH.setTeacherStatusScoreId(null);
					copyTSSH.setUpdatedByUserId(userMaster.getUserId());
					copyTSSH.setUpdateScoreProvided(Integer.parseInt(score));
					teacherStatusScoresHistoryDAO.makePersistent(copyTSSH);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}catch (Exception e) {
					e.printStackTrace();
				}
				//ended copy history score
				//update Score
				tss.setScoreProvided(Integer.parseInt(score));
				teacherStatusScoresDAO.updatePersistent(tss);
				//ended update score
				
			 	int minAfterScore=0;
	            int totalAfterScore=0;
	            try{
                	Double[] teacherStatusScoresAvg=teacherStatusScoresDAO.getAllFinalizeStatusScoreAvg(teacherDetail, jobOrder);
                    minAfterScore=Integer.parseInt(Math.round(teacherStatusScoresAvg[0])+"");
                    totalAfterScore=Integer.parseInt(Math.round(teacherStatusScoresAvg[1])+"");
                }catch(Exception e){ e.printStackTrace();}
                
                //System.out.println("minAfterScore:::::::"+minAfterScore);
                //System.out.println("totalAfterScore:::::::"+totalAfterScore);
	            
	            //show all teacher note list
				teacherStatusScoresList= teacherStatusScoresDAO.getStatusScoreAll(teacherDetail,jobOrder,statusMaster,secondaryStatus);
				//ended show all teacher note list
			
				int cScore=0;
				int maxScore=0;
	            int minUpdateScore=minAfterScore-minPrveScore;
	 
                //System.out.println("cScore:::::::"+cScore);
                //System.out.println("maxScore:::::::"+maxScore);
                if(maxScore>-1 && cScore>-1){
                    try{
                        JobWiseConsolidatedTeacherScore jwScoreObj=null;
                        jwScoreObj=jobWiseConsolidatedTeacherScoreDAO.getJWCTScore(teacherDetail,jobOrder);
                         cScore=minUpdateScore+jwScoreObj.getJobWiseConsolidatedScore();    
                        //System.out.println("cScore-----------:::::::"+cScore+" jwScoreObj.getJobWiseConsolidatedScore()=="+jwScoreObj.getJobWiseConsolidatedScore()+" jwScoreObj.getJobWiseMaxScore=="+jwScoreObj.getJobWiseMaxScore());
                        //System.out.println("maxScore:::::::"+maxScore);
                        jwScoreObj.setJobWiseConsolidatedScore(cScore);
                         jobWiseConsolidatedTeacherScoreDAO.makePersistent(jwScoreObj);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
		        //ended update fit score		   
		                
				//System.out.println("teacherDetail=="+teacherDetail+" jobOrder=="+jobOrder+" statusMaster=="+statusMaster+" secondaryStatus=="+secondaryStatus);			
				sb.append("<table border=0 class='table' >");
				sb.append("<thead class='bg'>");
				sb.append("<tr><th width=290>"+Utility.getLocaleValuePropByKey("  lblNm", locale)+"</td><th  width=30>"+Utility.getLocaleValuePropByKey("lblScr", locale)+"</td><th  width=80>"+Utility.getLocaleValuePropByKey("  lblcretedDate", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("  lblAct", locale)+"</th></tr>");//added by 01-04-2015
				sb.append("</thead>");
				//added by 01-04-2015
				String bothStausString="";
				if(statusMaster!=null && secondaryStatus!=null)
					bothStausString="sm#"+statusMaster.getStatusId()+"##ss#"+secondaryStatus.getSecondaryStatusId();
				else if(statusMaster!=null)
					bothStausString="sm#"+statusMaster.getStatusId();
				else if(secondaryStatus!=null)
					bothStausString="ss#"+secondaryStatus.getSecondaryStatusId();
				String teacherStatusScoresInfo=","+teacherDetail.getTeacherId()+","+jobOrder.getJobId()+","+bothStausString;
				//ended by 01-04-2015
				//System.out.println("Size================"+teacherStatusScoresList.size());
				for(TeacherStatusScores teacherStatusScores : teacherStatusScoresList){
					if(teacherStatusScores.isFinalizeStatus()){
						
						sb.append("<tr><td>"+teacherStatusScores.getUserMaster().getFirstName()+" "+teacherStatusScores.getUserMaster().getLastName()+"</td>");
						sb.append("<td>"+teacherStatusScores.getScoreProvided()+"</td>");
						sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(teacherStatusScores.getCreatedDateTime())+"</td>");
						String allInfo="'"+teacherStatusScores.getTeacherStatusScoreId()+","+teacherStatusScores.getMaxScore()+","+teacherStatusScores.getScoreProvided()+teacherStatusScoresInfo+"'";
						////System.out.println("allInfo========="+allInfo);
						sb.append("<td><a href='javascript:void(0)'  onclick=\"editteacherStatusScores("+allInfo+");\">"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a></td></tr>"); //added by 01-04-2015
					}
				}
				sb.append("</table>");
				sb.append("<table id='openEditTeacherStatusScore' style='display:none;width:100%;'></table>"); //added by 01-04-2015
			
				
				
			}catch(Exception e){
				e.printStackTrace();
			}
			////System.out.println("sb.toString()=="+sb.toString());
		 return sb.toString();
	 }
	 //ended by 01-04-2015
	 //added by 21-04-2015
	 @Transactional
	 public String editAndGetTeacherStatusNote(String teacherStatusNoteId, String editNote){
		 println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>::::::::::::::::::editAndGetTeacherStatusNote:::::::::::::::::<<<<<<<<<<<<<<<<<");
		 println("teacherStatusNoteId==============="+teacherStatusNoteId+"          editNote==========="+editNote);
		 String resMessage="no";
		 UserMaster userMaster 			= 	null;
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session1 = request.getSession(false);
			if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			if (session1 != null || session1.getAttribute("userMaster") != null) {
				userMaster=(UserMaster)session1.getAttribute("userMaster");
			}
		 try{
			 	TeacherStatusNotes tsn=teacherStatusNotesDAO.findById(Integer.parseInt(teacherStatusNoteId), false, false);
			 	//insert copy  history score
				TeacherStatusNotesHistory copyTSNH=new TeacherStatusNotesHistory();
	 			 try {
					BeanUtils.copyProperties(copyTSNH, tsn);
					copyTSNH.setTeacherStatusNoteId(null);
					copyTSNH.setUpdatedByUserId(userMaster.getUserId());
					copyTSNH.setStatusUpdatedNotes(editNote);
					teacherStatusNotesHistoryDAO.makePersistent(copyTSNH);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}catch (Exception e) {
					e.printStackTrace();
				}
				//ended copy history notes
				//update notes
				tsn.setStatusNotes(editNote);
				teacherStatusNotesDAO.updatePersistent(tsn);
				//ended update notes
				resMessage= "yes";
		 }catch(Exception e){
			 e.printStackTrace();
			 resMessage= "no";
		 }	
		 //System.out.println("resMessage===="+resMessage);
		 return resMessage;
	 }
	 @Transactional
	 public String addTeacherStatusNote(Integer teacherId, Integer jobId,Integer statusMasterId, Integer secondaryStatusId,Integer userId, Integer districtId,Long questionId, String note, int isJsi){
		 println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>::::::::::::::::::editAndGetTeacherStatusNote:::::::::::::::::<<<<<<<<<<<<<<<<<");
		 println("allInto=="+teacherId+","+jobId+","+statusMasterId+","+secondaryStatusId+","+userId+","+districtId+","+questionId+","+note);
		 String responseMessage="no###";
		 StringBuffer sb=new StringBuffer();
		 try{
		TeacherDetail td=teacherDetailDAO.findById(teacherId, false, false);
		JobOrder jo=jobOrderDAO.findById(jobId, false, false);
		StatusMaster sm=null;
		SecondaryStatus ss=null;
		if(statusMasterId!=null && statusMasterId>0){
			sm=statusMasterDAO.findById(statusMasterId, false, false);
		}else{
			ss=secondaryStatusDAO.findById(secondaryStatusId, false, false);
		}
		UserMaster um=userMasterDAO.findById(userId, false, false);
		DistrictMaster dm=districtMasterDAO.findById(districtId, false, false);
		 TeacherStatusNotes teacherStatusNotes=new TeacherStatusNotes();
		 teacherStatusNotes.setTeacherDetail(td);
		 teacherStatusNotes.setJobOrder(jo);
		 teacherStatusNotes.setStatusMaster(sm);		 
		 teacherStatusNotes.setSecondaryStatus(ss);
		 teacherStatusNotes.setUserMaster(um);		 
		 teacherStatusNotes.setDistrictId(dm.getDistrictId());
		 teacherStatusNotes.setStatusNotes(note);	
		 teacherStatusNotes.setTeacherAssessmentQuestionId(questionId);
		 teacherStatusNotes.setFinalizeStatus(true);
		 teacherStatusNotes.setEmailSentTo(0);
		 teacherStatusNotes.setCreatedDateTime(new Date());
		 teacherStatusNotesDAO.makePersistent(teacherStatusNotes);
		 //teacherStatusNotes=teacherStatusNotesDAO.findById(teacherStatusNotes.getTeacherStatusNoteId(), false, false);
		 println("teacherStatusNote============="+teacherStatusNotes.getTeacherStatusNoteId()+" teacherStatusNotes===="+teacherStatusNotes.getCreatedDateTime());
		 	sb.append("<div>");	
		 	if(isJsi==1)
			 	sb.append("<br/><label>"+Utility.getLocaleValuePropByKey("  blNote", locale)+"&nbsp;&nbsp;&nbsp;&nbsp;("+um.getFirstName()+" "+um.getLastName()+","+DATE_TIME_FORMAT.format(teacherStatusNotes.getCreatedDateTime())+")</label>");
			 	else if(isJsi==0)
			 	sb.append("<br/><label>("+um.getFirstName()+" "+um.getLastName()+","+DATE_TIME_FORMAT.format(teacherStatusNotes.getCreatedDateTime())+")</label>");
		 			 	
			if(teacherStatusNotes.isFinalizeStatus()){
				sb.append("<textarea class='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='1' style='pointer-event:none;'>"+teacherStatusNotes.getStatusNotes()+"</textarea>");				
			}else{
				sb.append("<textarea class='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='1' style='pointer-event:none;'>"+teacherStatusNotes.getStatusNotes()+"</textarea>");
			}
				sb.append("<div id='main"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'>");
				sb.append("<a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"' rel='tooltip' href='javascript:void(0);' id='edit_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' onclick='editTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/edit-notes.png' width='25px' height='30px;'/></a>");
				sb.append("<br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("  lblSaveNote", locale)+"' rel='tooltip' href='javascript:void(0);' id='save_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='saveTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/save-notes.png' width='25px' height='30px;'/></a>");
				sb.append("<br/><br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("  btnClr", locale)+"' rel='tooltip' href='javascript:void(0);' id='cancel_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='cancelTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/cancel-notes.jpg' width='25px' height='30px;'/></a><br/>");
				sb.append("</div>");
				sb.append("<br/></div>");
				responseMessage="yes###"+teacherStatusNotes.getTeacherStatusNoteId()+"###"+sb.toString();
		 }catch(Exception e){
			 e.printStackTrace();
			 responseMessage= "no###"+sb.toString();
		 }
		 return responseMessage;
	 }
	 public boolean showOrNot(List<StatusSpecificScore> lstStatusSpecificScore,UserMaster userMaster, StatusSpecificQuestions ssq,Map<UserMaster,List<TeacherStatusNotes>> getAllNotesByUserMap){
		 boolean flag=false;				
//		 for(Map.Entry<UserMaster, List<TeacherStatusNotes>> entry:getAllNotesByUserMap.entrySet())
//			 println("userfId=="+entry.getKey());
		 
		 for(StatusSpecificScore score:lstStatusSpecificScore){
			 if(score.getStatusSpecificQuestions()!=null)
			 if(ssq.getQuestionId().toString().equalsIgnoreCase(score.getStatusSpecificQuestions().getQuestionId().toString())){
				 println("----------"+ssq.getQuestionId()+"----------::------"+score.getUserMaster().getUserId()+"----------------"+userMaster.getUserId());
			 	if(score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString())){
			 		flag=true;	
			 		break;
			 	}else{
			 		println("--------------------::----------------------"+score.getFinalizeStatus());
			 		if(score.getFinalizeStatus()==1){
			 			flag=true;
			 			break;
			 			}
			 		else if(score.getFinalizeStatus()==0){			 			
			 			List<TeacherStatusNotes> lstAllTSN=getAllNotesByUserMap.get(score.getUserMaster());	
			 			if(lstAllTSN!=null && lstAllTSN.size()>0)
			 			for(TeacherStatusNotes tsn:lstAllTSN){
			 				println("----------"+tsn.getTeacherAssessmentQuestionId()+"----------::--------"+ssq.getQuestionId()+"--------------"+tsn.getTeacherStatusNoteId());
			 				if(tsn.getTeacherAssessmentQuestionId().toString().equalsIgnoreCase(ssq.getQuestionId().toString())){
			 					flag=true;
					 			break;	
			 				}
			 			}
			 			
			 		}
			 	}
			 }
		 }
		 return flag;
		 }
	 private Map<UserMaster,List<TeacherStatusNotes>> getAllNotesByUserMap(List<StatusSpecificScore> lstStatusSpecificScore){
		 List<UserMaster> listUserMaster =new ArrayList<UserMaster>();
		 Map<UserMaster,List<TeacherStatusNotes>> getAllNotesByUserMap=new HashMap<UserMaster, List<TeacherStatusNotes>>();
		 for(StatusSpecificScore score:lstStatusSpecificScore){listUserMaster.add(score.getUserMaster());}
		 StatusSpecificScore sss=null;
		 if(lstStatusSpecificScore.size()>0){
			 sss=lstStatusSpecificScore.get(0);
		 }
		 List<TeacherStatusNotes> lstTSN=null;
		 if(sss!=null){
			 lstTSN=teacherStatusNotesDAO.findByCriteria(Restrictions.eq("teacherDetail", sss.getTeacherDetail()),Restrictions.eq("jobOrder", sss.getJobOrder()),Restrictions.in("userMaster", listUserMaster),Restrictions.eq("districtId", sss.getDistrictId()),Restrictions.isNotNull("teacherAssessmentQuestionId"));
			 for(TeacherStatusNotes tsn:lstTSN){
				 if(getAllNotesByUserMap.get(tsn.getUserMaster())==null) {
					 List<TeacherStatusNotes> lstAllTSN=new ArrayList<TeacherStatusNotes>();
					 lstAllTSN.add(tsn);
					 getAllNotesByUserMap.put(tsn.getUserMaster(),lstAllTSN );					 
				 }else{
					 getAllNotesByUserMap.get(tsn.getUserMaster()).add(tsn);
				 }
			 }
		 }
		 return getAllNotesByUserMap;
	 }		
	//ended by 21-04-2015
	//added by 25-04-2015
		private String[] callOnlyForJSIJQuestionAndAnswer(TeacherDetail teacherDetail, JobOrder jobOrder,StatusMaster statusMaster, SecondaryStatus secondaryStatus, UserMaster userMaster,TeacherAssessmentdetail teacherAssessmentdetail,Map<Integer,StatusSpecificScore> stausSCore, List<TeacherAnswerDetail> teacherAnswerDetails,TeacherAssessmentQuestion teacherAssessmentQuestion,List<TeacherAssessmentOption> teacherQuestionOptionsList,String questionAssessmentIds, int iQdslider,int iCountScoreSlider,String qqNote,int defauntQuestionNote, boolean isSuperAdminUser, String clickable)
		{
			IS_PRINT=false;
			StringBuffer sb=new StringBuffer();
			//set width of slider
			int scoreWidth=525,scoreWidth1=565;
			if(userMaster.getEntityType()==3){
				scoreWidth=575;scoreWidth1=615;
			}
			//for return all value in which value change in this method
			String[] allValue=new String[6];
			// for all user available on this district in StatusSpecifictScore
			Map<Integer,UserMaster>  mapUserMaster=new LinkedHashMap<Integer,UserMaster>();
			Map<Integer,UserMaster>  mapExtraUserMaster=new LinkedHashMap<Integer,UserMaster>();
			
			// for all user by district StatusSpecificScore AnswerDetailId
			Map<String,List<StatusSpecificScore>> getUserIdAndAnswerIdSSSMap=new HashMap<String,List<StatusSpecificScore>>();
			// for all user by district StatusSpecificScore QuestionId
			Map<String,List<StatusSpecificScore>> getUserIdAndQuestionIdSSSMap=new HashMap<String,List<StatusSpecificScore>>();
			
			// for all notes by district  teacherStatusNotes
			Map<String,List<TeacherStatusNotes>> getUserIdAndAnswerIdTSNMap=new HashMap<String,List<TeacherStatusNotes>>();
			Map<String,List<TeacherStatusNotes>> getExtraUserIdAndAnswerIdTSNMap=new HashMap<String,List<TeacherStatusNotes>>();
			
			// for all user by district StatusSpecificScore of this user
			Map<Integer,StatusSpecificScore> mapScoreFirstSSS=new LinkedHashMap<Integer,StatusSpecificScore>();
			Map<Integer,StatusSpecificScore> mapExtraScoreFirstSSS=new LinkedHashMap<Integer,StatusSpecificScore>();
			
			// for all extra Question for jsi
			Map<Integer,StatusSpecificQuestions> allExtraQuestionMap=new TreeMap<Integer,StatusSpecificQuestions>();			
			
			//for all assesment question
			Set<Long> teacherAssessmentQuestionLst=new TreeSet<Long>();
			Set<Long> extraStatusSpecificQuestionLst=new TreeSet<Long>();
			mapUserMaster.put(userMaster.getUserId(),userMaster);
			
			//get All Notes of all user in one district
			Map<UserMaster,List<TeacherStatusNotes>>  getAllNotesByUserMap=new HashMap<UserMaster, List<TeacherStatusNotes>>();
			
			for (TeacherAnswerDetail teacherAnswerDetail : teacherAnswerDetails) {
				teacherAssessmentQuestionLst.add(teacherAnswerDetail.getTeacherAssessmentQuestion().getTeacherAssessmentQuestionId());
			}
			
			List<StatusSpecificScore> lstStatusSpecificScore=null;
			if(userMaster.getEntityType()==2){			
				lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionListForSuperAdministration(jobOrder,statusMaster,secondaryStatus,teacherDetail);
			}else{
				lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList(jobOrder,statusMaster,secondaryStatus,teacherDetail,userMaster);
			}
			getAllNotesByUserMap=getAllNotesByUserMap(lstStatusSpecificScore);
			println("Size===============22222222================="+lstStatusSpecificScore.size());
			for(StatusSpecificScore score:lstStatusSpecificScore){
				if(score.getTeacherAnswerDetail()!=null){
					try{
						if(score.getUserMaster()!=null)
							mapUserMaster.put(score.getUserMaster().getUserId(),score.getUserMaster());
						}catch(Exception e){e.printStackTrace();}
				if(score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString())){
					mapScoreFirstSSS.put(score.getTeacherAnswerDetail().getAnswerId(),score);
				}
				/*else if(score.getSkillAttributesMaster()!=null){
					mapScoreFirstSSS.put(score.getStatusSpecificQuestions().getQuestionId(),score);
				}*/
				}
				if(score.getStatusSpecificQuestions()!=null){
					Long qId=Long.parseLong(""+score.getStatusSpecificQuestions().getQuestionId());
					extraStatusSpecificQuestionLst.add(qId);
					try{
						if(score.getUserMaster()!=null)
							mapExtraUserMaster.put(score.getUserMaster().getUserId(),score.getUserMaster());
						}catch(Exception e){e.printStackTrace();}
					if(score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString())){
						mapExtraScoreFirstSSS.put(score.getStatusSpecificQuestions().getQuestionId(),score);
						allExtraQuestionMap.put(score.getStatusSpecificQuestions().getQuestionId(),score.getStatusSpecificQuestions());
					}else if(showOrNot(lstStatusSpecificScore, userMaster, score.getStatusSpecificQuestions(), getAllNotesByUserMap)){
						mapExtraScoreFirstSSS.put(score.getStatusSpecificQuestions().getQuestionId(),score);
						allExtraQuestionMap.put(score.getStatusSpecificQuestions().getQuestionId(),score.getStatusSpecificQuestions());
					}
				}
			}
			
			println("mapScoreFirstSSS==="+mapScoreFirstSSS.size()+"     mapExtraScoreFirstSSS=="+mapExtraScoreFirstSSS.size());
			
			
			for(StatusSpecificScore sps:lstStatusSpecificScore){
				println("userId===hhhhhh==="+sps+"======="+sps.getUserMaster().getFirstName()+" "+sps.getUserMaster().getLastName());
				if(sps.getTeacherAnswerDetail()!=null){					
					if(getUserIdAndAnswerIdSSSMap.get(sps.getTeacherAnswerDetail().getAnswerId()+"#"+sps.getUserMaster().getUserId())==null){
						List<StatusSpecificScore> lst=new ArrayList<StatusSpecificScore>();
						lst.add(sps);
						getUserIdAndAnswerIdSSSMap.put(sps.getTeacherAnswerDetail().getAnswerId()+"#"+sps.getUserMaster().getUserId(), lst);						
					}else{
						List<StatusSpecificScore> lst=getUserIdAndAnswerIdSSSMap.get(sps.getTeacherAnswerDetail().getAnswerId()+"#"+sps.getUserMaster().getUserId());
						lst.add(sps);
						getUserIdAndAnswerIdSSSMap.put(sps.getTeacherAnswerDetail().getAnswerId()+"#"+sps.getUserMaster().getUserId(), lst);
					}					
				}
				if(sps.getStatusSpecificQuestions()!=null){					
					if(getUserIdAndQuestionIdSSSMap.get(sps.getStatusSpecificQuestions().getQuestionId()+"#"+sps.getUserMaster().getUserId())==null){
						List<StatusSpecificScore> lst=new ArrayList<StatusSpecificScore>();
						lst.add(sps);
						getUserIdAndQuestionIdSSSMap.put(sps.getStatusSpecificQuestions().getQuestionId()+"#"+sps.getUserMaster().getUserId(), lst);						
					}else{
						List<StatusSpecificScore> lst=getUserIdAndQuestionIdSSSMap.get(sps.getStatusSpecificQuestions().getQuestionId()+"#"+sps.getUserMaster().getUserId());
						lst.add(sps);
						getUserIdAndQuestionIdSSSMap.put(sps.getStatusSpecificQuestions().getQuestionId()+"#"+sps.getUserMaster().getUserId(), lst);
					}					
				}				
			}
			
			List<TeacherStatusNotes> lstAllUserTeacherStatusNotes=new ArrayList<TeacherStatusNotes>();
			List<TeacherStatusNotes> lstExtraAllUserTeacherStatusNotes=new ArrayList<TeacherStatusNotes>();
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			}else{
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			}
			if(userMaster.getEntityType()==2){
			lstAllUserTeacherStatusNotes=teacherStatusNotesDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherDetail),Restrictions.eq("jobOrder", jobOrder),Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()),Restrictions.in("teacherAssessmentQuestionId", teacherAssessmentQuestionLst),criterionStatus);
			if(extraStatusSpecificQuestionLst.size()>0)
			lstExtraAllUserTeacherStatusNotes=teacherStatusNotesDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherDetail),Restrictions.eq("jobOrder", jobOrder),Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()),Restrictions.in("teacherAssessmentQuestionId", extraStatusSpecificQuestionLst),criterionStatus);
			}else{
				lstAllUserTeacherStatusNotes=teacherStatusNotesDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherDetail),Restrictions.eq("jobOrder", jobOrder),Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()),Restrictions.in("teacherAssessmentQuestionId", teacherAssessmentQuestionLst),criterionStatus,Restrictions.eq("userMaster", userMaster));
				if(extraStatusSpecificQuestionLst.size()>0)
				lstExtraAllUserTeacherStatusNotes=teacherStatusNotesDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherDetail),Restrictions.eq("jobOrder", jobOrder),Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()),Restrictions.in("teacherAssessmentQuestionId", extraStatusSpecificQuestionLst),criterionStatus,Restrictions.eq("userMaster", userMaster));
			}
			
			println("size 9of notes======================="+lstAllUserTeacherStatusNotes.size());
			println("size extra 9of notes======================="+lstExtraAllUserTeacherStatusNotes.size());
			for(TeacherStatusNotes tsn:lstAllUserTeacherStatusNotes){
				if(getUserIdAndAnswerIdTSNMap.get(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId())==null){
					List<TeacherStatusNotes> lst=new ArrayList<TeacherStatusNotes>();
					lst.add(tsn);
					getUserIdAndAnswerIdTSNMap.put(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId(), lst);						
				}else{
					List<TeacherStatusNotes> lst=getUserIdAndAnswerIdTSNMap.get(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId());
					lst.add(tsn);
					getUserIdAndAnswerIdTSNMap.put(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId(), lst);
				}
			}
			//for exta question
			for(TeacherStatusNotes tsn:lstExtraAllUserTeacherStatusNotes){
				if(getExtraUserIdAndAnswerIdTSNMap.get(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId())==null){
					List<TeacherStatusNotes> lst=new ArrayList<TeacherStatusNotes>();
					lst.add(tsn);
					getExtraUserIdAndAnswerIdTSNMap.put(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId(), lst);						
				}else{
					List<TeacherStatusNotes> lst=getExtraUserIdAndAnswerIdTSNMap.get(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId());
					lst.add(tsn);
					getExtraUserIdAndAnswerIdTSNMap.put(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId(), lst);
				}
			}
			
			Map<String,String> getScoreHtmlTableMap= getAllStatusSpecificScoreByQuestionIdAndAnswerId(teacherDetail, jobOrder, statusMaster, secondaryStatus);
			
			//for status
			int statusId=0,secondaryStatusId=0;			
			if(statusMaster!=null){
				statusId=statusMaster.getStatusId();
			}else if(secondaryStatus!=null) {
				secondaryStatusId=secondaryStatus.getSecondaryStatusId();
			}				
			//end

			if(teacherAssessmentdetail!=null)
			{
				String selectedOptions = "",insertedRanks="";
				String questionTypeShortName = "";
				long optionId = 0;
				String answerText = "";
				int questionCounter = 0;
				sb.append("<table>");

				for(TeacherAnswerDetail teacherAnswerDetail : teacherAnswerDetails){
					++questionCounter;
					teacherAssessmentQuestion = teacherAnswerDetail.getTeacherAssessmentQuestion();
					selectedOptions = teacherAnswerDetail.getSelectedOptions();

					sb.append("<tr>");
					sb.append("<td width='5%' valign='top'><label  class='labletxt'><b>Q. "+questionCounter+"</b></label></td>");

					sb.append("<td width='95%'><label   class='labletxt'>"+teacherAssessmentQuestion.getQuestion());
					sb.append("</label></td>");
					sb.append("</tr>");
					int maxScore=0;
					int aScoreProvided=0;
					int scoreAnswerId=0;

					questionTypeShortName = teacherAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();
					teacherQuestionOptionsList = teacherAssessmentQuestion.getTeacherAssessmentOptions();
					if(questionTypeShortName.equalsIgnoreCase("tf") || questionTypeShortName.equalsIgnoreCase("slsel") || questionTypeShortName.equalsIgnoreCase("lkts"))
					{
						if(selectedOptions!=null && !selectedOptions.equals(""))
						{ 
							try{optionId = Long.parseLong(selectedOptions);}catch(Exception e){optionId = 0;}
						}
						else
							optionId = 0;

						for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {

							sb.append("<tr>");
							sb.append("<td width='5%' valign='top'>&nbsp;</td>");
							sb.append("<td width='95%'><label    class='labletxt'>"+teacherQuestionOption.getQuestionOption());
							sb.append("</label></td>");
							sb.append("</tr>");

							if(optionId==teacherQuestionOption.getTeacherAssessmentOptionId())
							{
								answerText=teacherQuestionOption.getQuestionOption();
							}
						}

						sb.append("<tr>");
						sb.append("<td width='5%' valign='top'><label><B>"+Utility.getLocaleValuePropByKey("  msgAnswer", locale)+"</B></label></td>");
						sb.append("<td width='95%'><label>"+answerText);
						sb.append("</label></td>");
						sb.append("</tr>");

						answerText="";
					}else if(questionTypeShortName.equalsIgnoreCase("rt")){

						for(TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {

							sb.append("<tr>");
							sb.append("<td width='5%' valign='top'>&nbsp;</td>");
							sb.append("<td width='95%'><label    class='labletxt'>"+teacherQuestionOption.getQuestionOption());
							sb.append("</label></td>");
							sb.append("</tr>");
						}
						answerText=insertedRanks;
						insertedRanks="";

						insertedRanks = teacherAnswerDetail.getInsertedRanks()==null?"":teacherAnswerDetail.getInsertedRanks();
						selectedOptions = selectedOptions==null?"":selectedOptions;
						String[] ranks = insertedRanks.split("\\|");

						String ans = "";
						int rank = 0;
						sb.append("<table>");
						for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {

							try{ans = ranks[rank];}catch(Exception e){ans = "";}
							sb.append("<tr>");
							sb.append("<td><label>");
							sb.append(ans);
							sb.append("</label></td>");

							sb.append("<td><label class='labletxt'>");
							sb.append(teacherQuestionOption.getQuestionOption());
							sb.append("</label></td>");
							sb.append("</tr>");
							rank++;
						}
						sb.append("<table>");

						sb.append("<tr>");
						sb.append("<td width='5%' valign='top'><b>"+Utility.getLocaleValuePropByKey("  msgAnswer", locale)+"</></td>");
						sb.append("<td width='95%'><label>"+answerText);
						sb.append("</label></td>");
						sb.append("</tr>");

						answerText="";
					}else if(questionTypeShortName.equalsIgnoreCase("sl")){
						sb.append("<tr>");
						sb.append("<td width='5%' valign='top'><b>"+Utility.getLocaleValuePropByKey("  msgAnswer", locale)+"</b></td>");
						sb.append("<td width='95%'><label>"+teacherAnswerDetail.getInsertedText());
						sb.append("</label></td>");
						sb.append("</tr>");
					}
					else if(questionTypeShortName.equalsIgnoreCase("ml")){

						sb.append("<tr>");
						sb.append("<td width='5%' valign='top'><label><b>"+Utility.getLocaleValuePropByKey("  msgAnswer", locale)+"</b></label></td>");
						sb.append("<td width='95%'><label>"+teacherAnswerDetail.getInsertedText());
						sb.append("</label></td>");
						sb.append("</tr>");
					}
					
					boolean flagforScoreShow=true;	
					boolean increaseOrNot=false;
					for(Map.Entry<Integer, UserMaster> entry:mapUserMaster.entrySet()){	
						UserMaster um=entry.getValue();
						println("key---->::"+teacherAnswerDetail.getAnswerId()+"#"+um.getUserId());
					//StatusSpecificScore SSObj=stausSCore.get(teacherAnswerDetail.getAnswerId());
					List<StatusSpecificScore> lstSSS=getUserIdAndAnswerIdSSSMap.get(teacherAnswerDetail.getAnswerId()+"#"+um.getUserId());
					for(StatusSpecificScore SSObj:lstSSS){
					boolean thisUser=SSObj.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString());
					int tickInterval=1;
					if(flagforScoreShow){
						StatusSpecificScore SSObj1=mapScoreFirstSSS.get(SSObj.getTeacherAnswerDetail().getAnswerId());						
					if(SSObj1!=null){
						if(SSObj1.getMaxScore()!=null){
							maxScore=SSObj1.getMaxScore();
						}
						if(SSObj1.getScoreProvided()!=null){
							aScoreProvided=SSObj1.getScoreProvided();
						}
						if(SSObj1!=null)
							scoreAnswerId=SSObj1.getAnswerId();
					}
					
					if(maxScore%10==0)
						tickInterval=10;
					else if(maxScore%5==0)
						tickInterval=5;
					else if(maxScore%3==0)
						tickInterval=3;
					else if(maxScore%2==0)
						tickInterval=2;					
					}
					
					if(maxScore>0){
						if(flagforScoreShow)
						iCountScoreSlider++;

						//Add Code for JSI
						if(SSObj.getFinalizeStatus()==1)
							iQdslider=0;
						else
							iQdslider=1;

						sb.append("<tr>");
						sb.append("<td width='5%' valign='top'>&nbsp;</td>");
						sb.append("<td width='95%'>");		
						if(flagforScoreShow){
						sb.append("<div class='row ' style='padding-left: 12px;'>");
						sb.append("<div class='span5 slider_question' style='float:left;'><iframe id=\"ifrmQuestion_"+iCountScoreSlider+"\" src=\"slideract.do?name=questionFrm&tickInterval="+tickInterval+"&max="+maxScore+"&dslider="+iQdslider+"&swidth="+scoreWidth+"&step=1&answerId="+scoreAnswerId+"&svalue="+aScoreProvided+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:"+scoreWidth1+"px;margin-top:-11px;\"></iframe></div>");
						if(userMaster.getEntityType()==2){
						sb.append("<div style='float:right;'><a id='question"+SSObj.getTeacherAnswerDetail().getAnswerId()+"' class='questionScore' title='' data-original-title='' data-placement='above' href='javascript:void(0);' onclick='clickOnQuestionScore(this);'><span  class='fa-crosshairs icon-large iconcolor' ></span></a></div>");
						//for all User Score
						sb.append("<div id='scoreTablequestion"+SSObj.getTeacherAnswerDetail().getAnswerId()+"' style='display:none;'>");
						////System.out.println("teacherDetail=="+teacherDetail+" jobOrder=="+jobOrder+" statusMaster=="+statusMaster+" secondaryStatus=="+secondaryStatus);
						println("qId=--------------------------------==="+SSObj.getTeacherAnswerDetail().getAnswerId()+"    value===---------------------=="+getScoreHtmlTableMap.get(String.valueOf(SSObj.getTeacherAnswerDetail().getAnswerId())));
						sb.append(getScoreHtmlTableMap.get(String.valueOf(SSObj.getTeacherAnswerDetail().getAnswerId())));
						sb.append("</div>");
						//end all User Score
						}
						sb.append("</div>");
						flagforScoreShow=false;
						}
						sb.append("</div>");


						List<TeacherStatusNotes> lstTeacherStatusNotes = null;

						lstTeacherStatusNotes = getUserIdAndAnswerIdTSNMap.get(teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"#"+um.getUserId());
							//teacherStatusNotesDAO.getQuestionNoteObjByUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), teacherAssessmentQuestion.getTeacherAssessmentQuestionId(),userMaster);
						String statusNote = "";
						
						boolean flagforShowAddNote=false;
						if(lstTeacherStatusNotes!=null && lstTeacherStatusNotes.size()>0){
							for(TeacherStatusNotes teacherStatusNotes:lstTeacherStatusNotes){
								
						if(teacherStatusNotes!=null)
							statusNote = teacherStatusNotes.getStatusNotes();


						//@Ashish :: add Note for every question 
						sb.append("<div  class='row mt5'>");
						sb.append("<div class='left16 inner_disable_jqte' id='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' >");
						sb.append("<label >Note</label>&nbsp;&nbsp;&nbsp;&nbsp;");
						
							sb.append("<label>("+um.getFirstName()+" "+um.getLastName()+", "+DATE_TIME_FORMAT.format(teacherStatusNotes.getCreatedDateTime())+")</label>");
							if(isSuperAdminUser){
								sb.append("<div>");
								if(teacherStatusNotes.isFinalizeStatus()){
									sb.append("<textarea id='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='2'>"+statusNote+"</textarea>");
									increaseOrNot=false;
								}else{
									if(thisUser){
									sb.append("<textarea id='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='2'>"+statusNote+"</textarea>");
									increaseOrNot=false;
									}
								}
								sb.append("<div id='main"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'>");
								sb.append("<a "+clickable+" class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"' rel='tooltip' href='javascript:void(0);' id='edit_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' onclick='editTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/edit-notes.png' width='25px' height='30px;'/></a>");
								sb.append("<br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("  lblSaveNote", locale)+"' rel='tooltip' href='javascript:void(0);' id='save_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='saveTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/save-notes.png' width='25px' height='30px;'/></a>");
								sb.append("<br/><br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("  btnClr", locale)+"' rel='tooltip' href='javascript:void(0);' id='cancel_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='cancelTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/cancel-notes.jpg' width='25px' height='30px;'/></a><br/>");
								sb.append("</div>");
								sb.append("<br/></div>");
							}else{
							if(teacherStatusNotes.isFinalizeStatus()){
								sb.append("<div class='' style='background-color:#EEEEEE; width:585px; height:80px; border:1px solid #CCCCCC; overflow:scroll; '>"+statusNote+"</div>");
								increaseOrNot=false;
							}else{
								if(thisUser){
								sb.append("<textarea id='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' name='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' class='span6' rows='2'>"+statusNote+"</textarea>");
								increaseOrNot=true;
								}
							}
							}
						
						sb.append("</div>");
						sb.append("</div>");											
							}
						}
						if(thisUser){
							int isJSI=1;
							String allInfoAboutNote="'"+teacherDetail.getTeacherId()+","+jobOrder.getJobId()+","+statusId+","+secondaryStatusId+","+userMaster.getUserId()+","+jobOrder.getDistrictMaster().getDistrictId()+","+ teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+","+isJSI+"'";
							if(lstTeacherStatusNotes==null || lstTeacherStatusNotes.size()==0){
							sb.append("<div class='row mt5'>");
							sb.append("<div class='left16' id='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' >");
							sb.append("<label id='label"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' style='width:100%;'>Note&nbsp;&nbsp;&nbsp;&nbsp;");
							sb.append("("+um.getFirstName()+" "+um.getLastName()+")</label>");
							if(isSuperAdminUser){
							sb.append("<div class='inner_enable_jqte'>");
							}
							sb.append("<textarea id='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' name='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' class='span6' rows='2'></textarea>");
							if(isSuperAdminUser){
							sb.append("<div style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'><a "+clickable+" class='tooltipforicon' data-original-title='Add' rel='tooltip' href='javascript:void(0);' onclick=\"addTeacherStatusNotes(this,"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+","+allInfoAboutNote+");\"><img src='images/add_notes.png' width='25px' height='30px;'/></a></div>");
							sb.append("<br/></div>");
							}
							sb.append("</div>");
							sb.append("</div>");
							sb.append("</td>");
							sb.append("</tr>");					
							increaseOrNot=true;
							println("--------------------------------------------------------------------------------------");
							}
							if(isSuperAdminUser && !increaseOrNot){	
								sb.append("<div class='row mt5'>");
								sb.append("<div class='left16' id='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' >");
								sb.append("<label id='label"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' style='width:100%;'>Note&nbsp;&nbsp;&nbsp;&nbsp;");
								sb.append("("+um.getFirstName()+" "+um.getLastName()+")</label>");
								if(isSuperAdminUser){
								sb.append("<div class='inner_enable_jqte'>");
								}
								sb.append("<textarea id='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' name='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' class='span6' rows='2'></textarea>");
								if(isSuperAdminUser){
								sb.append("<div style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'><a "+clickable+" class='tooltipforicon' data-original-title='Add' rel='tooltip' href='javascript:void(0);' onclick=\"addTeacherStatusNotes(this,"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+","+allInfoAboutNote+");\"><img src='images/add_notes.png' width='25px' height='30px;'/></a></div>");
								sb.append("<br/></div>");
								}
								sb.append("</div>");
								sb.append("</div>");
								sb.append("</td>");
								sb.append("</tr>");																					
							}
						}

						if(increaseOrNot){
							println("user id=="+um.getUserId()+" "+teacherAssessmentQuestion.getTeacherAssessmentQuestionId());
						if(!qqNote.equals(""))
							qqNote = qqNote+"#"+"questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId();
						else
							qqNote = "questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId();
						}
					}
					}
						if(increaseOrNot){
						if(questionAssessmentIds!=null && !questionAssessmentIds.equals("")){
							questionAssessmentIds = questionAssessmentIds+"#"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId().toString();
							defauntQuestionNote++;
							increaseOrNot=false;
						}else{
							questionAssessmentIds = teacherAssessmentQuestion.getTeacherAssessmentQuestionId().toString();
							defauntQuestionNote++;
							increaseOrNot=false;
						}
						}
					}
				}
				
				
				
				
				// Second part of question
				
				//Extra Question 
				for(Map.Entry<Integer, StatusSpecificQuestions> entryQuestion:allExtraQuestionMap.entrySet()){
					Integer qId=entryQuestion.getKey();
					StatusSpecificQuestions question=entryQuestion.getValue();
					boolean flagForAttribute=false;
					if(question.getQuestion()!=null && !question.getQuestion().equalsIgnoreCase(""))
					{		
						sb.append("<tr>");
						sb.append("<td width='5%' valign='top'><label  class='labletxt'><b>Q. "+(++questionCounter)+"</b></label></td>");
						sb.append("<td width='95%'><label   class='labletxt'>"+teacherAssessmentQuestion.getQuestion());
						sb.append("</label></td>");
						sb.append("</tr>");											
						if(question.getQuestion()!=null && !question.getQuestion().equals("") && question.getSkillAttributesMaster()!=null){
							sb.append("<tr>");
							sb.append("<td width='5%' valign='top'><label  class='lableAtxt'><b>"+Utility.getLocaleValuePropByKey("  lblAttribute1", locale)+":</b></label></td>");
							sb.append("<td width='95%'><label   class='lableAtxt'>"+question.getSkillAttributesMaster().getSkillName());
							sb.append("</label></td>");
							sb.append("</tr>");														
						}						
					}else if((question.getQuestion()==null || question.getQuestion().equals("")) && question.getSkillAttributesMaster()!=null){
						{
							sb.append("<tr>");
							sb.append("<td width='5%' valign='top'><label  class='lableAtxt'><b>"+Utility.getLocaleValuePropByKey("  lblAttribute1", locale)+":</b></label></td>");
							sb.append("<td width='95%'><label   class='lableAtxt'>"+question.getSkillAttributesMaster().getSkillName());
							sb.append("</label></td>");
							sb.append("</tr>");	
							flagForAttribute=true;													
						}	
					}
					int maxScore=0;
					int aScoreProvided=0;
					int scoreQuestionId=0;
				boolean flagforScoreShow=true;	
				boolean increaseOrNot=false;
				boolean thisUser=false;
				for(Map.Entry<Integer, UserMaster> entry:mapUserMaster.entrySet())
				{	
					UserMaster um=entry.getValue();
					println("key--------extra-------->::"+qId+"#"+um.getUserId());				//
				List<StatusSpecificScore> lstSSS=getUserIdAndQuestionIdSSSMap.get(qId+"#"+um.getUserId());
				if(lstSSS!=null)
				for(StatusSpecificScore SSObj:lstSSS){
				boolean flagforDisableOrEnableScoreBecauseAnotherDAScore=true;
				int tickInterval=1;
				if(flagforScoreShow){
					StatusSpecificScore SSObj1=mapExtraScoreFirstSSS.get(SSObj.getStatusSpecificQuestions().getQuestionId());						
				if(SSObj1!=null){
					if(!SSObj1.getUserMaster().getUserId().toString().equals(userMaster.getUserId().toString()))
					flagforDisableOrEnableScoreBecauseAnotherDAScore=false;
					else{					
					if(SSObj1.getScoreProvided()!=null){
						aScoreProvided=SSObj1.getScoreProvided();
					}					
					}
					if(SSObj1.getMaxScore()!=null){
						maxScore=SSObj1.getMaxScore();
					}
					if(SSObj1!=null)
						scoreQuestionId=SSObj1.getAnswerId();
				}
				
				if(maxScore%10==0)
					tickInterval=10;
				else if(maxScore%5==0)
					tickInterval=5;
				else if(maxScore%3==0)
					tickInterval=3;
				else if(maxScore%2==0)
					tickInterval=2;					
				}

				if(maxScore>0){
					if(flagforScoreShow)
					iCountScoreSlider++;

					//Add Code for JSI
					if(SSObj.getFinalizeStatus()==1)
						iQdslider=0;
					else
						if(flagforDisableOrEnableScoreBecauseAnotherDAScore)
						iQdslider=1;
						else
							iQdslider=0;

					sb.append("<tr>");
					sb.append("<td width='5%' valign='top'>&nbsp;</td>");
					sb.append("<td width='95%'>");		
					if(flagforScoreShow){
					sb.append("<div class='row ' style='padding-left: 12px;'>");
					sb.append("<div style='float:left;' class='span5 slider_question'><iframe id=\"ifrmQuestion_"+iCountScoreSlider+"\" src=\"slideract.do?name=questionFrm&tickInterval="+tickInterval+"&max="+maxScore+"&dslider="+iQdslider+"&swidth="+scoreWidth+"&step=1&answerId="+scoreQuestionId+"&svalue="+aScoreProvided+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:"+scoreWidth1+"px;margin-top:-11px;\"></iframe></div>");
					if(userMaster.getEntityType()==2){
					sb.append("<div style='float:right;'><a class='questionScore' id='question"+qId+"' title='' data-original-title='' data-placement='above' href='javascript:void(0);' onclick='clickOnQuestionScore(this);'><span  class='fa-crosshairs icon-large iconcolor' ></span></a></div>");
					//for all User Score
					sb.append("<div id='scoreTablequestion"+qId+"' style='display:none;'>");
					//System.out.println("teacherDetail=="+teacherDetail+" jobOrder=="+jobOrder+" statusMaster=="+statusMaster+" secondaryStatus=="+secondaryStatus);
					println("qId=--------------------------------==="+qId+"    value===---------------------=="+getScoreHtmlTableMap.get(String.valueOf(qId)));
					sb.append(getScoreHtmlTableMap.get(String.valueOf(qId)));
					sb.append("</div>");
					//end all User Score
					}
					sb.append("</div>");
					flagforScoreShow=false;
					}
					sb.append("</div>");
					List<TeacherStatusNotes> lstTeacherStatusNotes = null;

					lstTeacherStatusNotes = getExtraUserIdAndAnswerIdTSNMap.get(qId+"#"+um.getUserId());					
					String statusNote = "";
					println("user check====out===="+um.getUserId() +"      ==="+userMaster.getUserId());
					thisUser=um.getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString());
					
					if(!flagForAttribute){
					if(lstTeacherStatusNotes!=null && lstTeacherStatusNotes.size()>0){
						for(TeacherStatusNotes teacherStatusNotes:lstTeacherStatusNotes){
							
					if(teacherStatusNotes!=null)
						statusNote = teacherStatusNotes.getStatusNotes();


					//add Note for every question 
					sb.append("<div class='row mt5'>");
					sb.append("<div class='left16 inner_disable_jqte' id='questionNotes"+qId+"' >");
					sb.append("<label >Note</label>&nbsp;&nbsp;&nbsp;&nbsp;");
					
						sb.append("<label>("+um.getFirstName()+" "+um.getLastName()+", "+DATE_TIME_FORMAT.format(teacherStatusNotes.getCreatedDateTime())+")</label>");
						if(isSuperAdminUser){
							sb.append("<div>");
							if(teacherStatusNotes.isFinalizeStatus()){
								sb.append("<textarea id='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='2'>"+statusNote+"</textarea>");
								increaseOrNot=false;
							}else{								
								sb.append("<textarea id='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='2'>"+statusNote+"</textarea>");
								increaseOrNot=false;								
							}
							sb.append("<div id='main"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'>");
							sb.append("<a "+clickable+" class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"' rel='tooltip' href='javascript:void(0);' id='edit_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' onclick='editTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/edit-notes.png' width='25px' height='30px;'/></a>");
							sb.append("<br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("  lblSaveNote", locale)+"' rel='tooltip' href='javascript:void(0);' id='save_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='saveTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/save-notes.png' width='25px' height='30px;'/></a>");
							sb.append("<br/><br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("  btnClr", locale)+"' rel='tooltip' href='javascript:void(0);' id='cancel_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='cancelTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/cancel-notes.jpg' width='25px' height='30px;'/></a><br/>");
							sb.append("</div>");
							sb.append("<br/></div>");
						}else{
						if(teacherStatusNotes.isFinalizeStatus()){
							sb.append("<div class='' style='background-color:#EEEEEE; width:585px; height:80px; border:1px solid #CCCCCC; overflow:scroll; '>"+statusNote+"</div>");
							increaseOrNot=false;
						}else{
							if(thisUser){
							sb.append("<textarea id='questionNotes"+qId+"' name='questionNotes"+qId+"' class='span6' rows='2'>"+statusNote+"</textarea>");
							increaseOrNot=true;
							}
						}
						}
					
						sb.append("</div>");
						sb.append("</div>");									
						}						
					}
					if(thisUser){
						int isJSI=1;
						String allInfoAboutNote="'"+teacherDetail.getTeacherId()+","+jobOrder.getJobId()+","+statusId+","+secondaryStatusId+","+userMaster.getUserId()+","+jobOrder.getDistrictMaster().getDistrictId()+","+ qId+","+isJSI+"'";
						if(flagforDisableOrEnableScoreBecauseAnotherDAScore && (lstTeacherStatusNotes==null || lstTeacherStatusNotes.size()==0)){											
						sb.append("<div class='row mt5'>");
						sb.append("<div class='left16' id='questionNotes"+qId+"' >");
						sb.append("<label id='label"+qId+"' style='width:100%;'>"+Utility.getLocaleValuePropByKey("  blNote", locale)+"&nbsp;&nbsp;&nbsp;&nbsp;");
						sb.append("("+um.getFirstName()+" "+um.getLastName()+")</label>");
						if(isSuperAdminUser){
							sb.append("<div class='inner_enable_jqte'>");
							}
						sb.append("<textarea id='questionNotes"+qId+"' name='questionNotes"+qId+"' class='span6' rows='2'></textarea>");
						if(isSuperAdminUser){
							sb.append("<div style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'><a "+clickable+" class='tooltipforicon' data-original-title='Add' rel='tooltip' href='javascript:void(0);' onclick=\"addTeacherStatusNotes(this,"+qId+","+allInfoAboutNote+");\"><img src='images/add_notes.png' width='25px' height='30px;'/></a></div>");
							sb.append("</div>");
							}
						sb.append("</div>");
						sb.append("</div>");
						sb.append("</td>");
						sb.append("</tr>");
						increaseOrNot=true;
						}
						if(flagforDisableOrEnableScoreBecauseAnotherDAScore && isSuperAdminUser && !increaseOrNot){							
							sb.append("<div class='row mt5'>");
							sb.append("<div class='left16' id='questionNotes"+qId+"' >");
							sb.append("<label id='label"+qId+"' style='width:100%;'>Note&nbsp;&nbsp;&nbsp;&nbsp;");
							sb.append("("+um.getFirstName()+" "+um.getLastName()+")</label>");
							if(isSuperAdminUser){
								sb.append("<div class='inner_enable_jqte'>");
								}
							sb.append("<textarea id='questionNotes"+qId+"' name='questionNotes"+qId+"' class='span6' rows='2'></textarea>");
							if(isSuperAdminUser){
								sb.append("<div style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'><a "+clickable+" class='tooltipforicon' data-original-title='Add' rel='tooltip' href='javascript:void(0);' onclick=\"addTeacherStatusNotes(this,"+qId+","+allInfoAboutNote+");\"><img src='images/add_notes.png' width='25px' height='30px;'/></a></div>");
								sb.append("</div>");
								}
							sb.append("</div>");
							sb.append("</div>");
							sb.append("</td>");
							sb.append("</tr>");															
						}
					}
					}

					if(increaseOrNot){
						println("user id=="+um.getUserId()+" "+qId);
					if(!qqNote.equals(""))
						qqNote = qqNote+"#"+"questionNotes"+qId;
					else
						qqNote = "questionNotes"+qId;
					}
				}else{	
					if(thisUser){
					defauntQuestionNote++;						
					}						
					}
				}
					if(increaseOrNot){
					if(questionAssessmentIds!=null && !questionAssessmentIds.equals("")){
						questionAssessmentIds = questionAssessmentIds+"#"+qId;
						increaseOrNot=false;
					}else{
						questionAssessmentIds = ""+qId;
						increaseOrNot=false;
					}
					}					
				}
				}
				sb.append("</table>");			
			}
			//iQdslider,iCountScoreSlider,qqNote,defauntQuestionNote,questionAssessmentIds,html
			allValue[0]=""+iQdslider;
			allValue[1]=""+iCountScoreSlider;
			allValue[2]=qqNote;
			allValue[3]=""+defauntQuestionNote;
			allValue[4]=questionAssessmentIds;
			allValue[5]=sb.toString();
			return allValue;
		}
		public Map<String,String> getAllStatusSpecificScoreByQuestionIdAndAnswerId(TeacherDetail teacherDetail, JobOrder jobOrder,StatusMaster statusMaster, SecondaryStatus secondaryStatus){
			 StringBuffer sb=new StringBuffer();
			 Map<String,List<StatusSpecificScore>> questionIdOrAnswerIdMap=new HashMap<String, List<StatusSpecificScore>>();
			 Map<String,String> getHtmlByQuestionIdOrAnswerIdMap=new HashMap<String, String>();
			 println(">>>>>>>>>>>>>>>>>>>>>::::::"+teacherDetail.getTeacherId()+":::::::call for Score::::::::"+jobOrder.getJobId()+"::::::<<<<<<<<<<<<<<<<<<<<<<<<status=="+statusMaster+" secondaryStatusId== "+secondaryStatus);
			 List<StatusSpecificScore> statusSpecificScoreList= new ArrayList<StatusSpecificScore>();
				try{
		            //show all teacher note list  teacherDetail,jobOrder,statusMaster,secondaryStatus
					Criterion criterionStatus=null;
					if(statusMaster!=null){
						criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
					}else{
						criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
					}					
					statusSpecificScoreList= statusSpecificScoreDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherDetail),Restrictions.eq("jobOrder", jobOrder),criterionStatus);
					println("Size================"+statusSpecificScoreList.size());
					//ended show all teacher note list
					
					for(StatusSpecificScore sps : statusSpecificScoreList){
						if(sps.getTeacherAnswerDetail()!=null){					
							if(questionIdOrAnswerIdMap.get(sps.getTeacherAnswerDetail().getAnswerId().toString())==null){
								List<StatusSpecificScore> lst=new ArrayList<StatusSpecificScore>();
								lst.add(sps);
								questionIdOrAnswerIdMap.put(sps.getTeacherAnswerDetail().getAnswerId().toString(), lst);						
							}else{
								List<StatusSpecificScore> lst=questionIdOrAnswerIdMap.get(sps.getTeacherAnswerDetail().getAnswerId().toString());
								lst.add(sps);
								questionIdOrAnswerIdMap.put(sps.getTeacherAnswerDetail().getAnswerId().toString(), lst);
							}					
						}
						if(sps.getStatusSpecificQuestions()!=null){					
							if(questionIdOrAnswerIdMap.get(sps.getStatusSpecificQuestions().getQuestionId().toString())==null){
								List<StatusSpecificScore> lst=new ArrayList<StatusSpecificScore>();
								lst.add(sps);
								questionIdOrAnswerIdMap.put(sps.getStatusSpecificQuestions().getQuestionId().toString(), lst);						
							}else{
								List<StatusSpecificScore> lst=questionIdOrAnswerIdMap.get(sps.getStatusSpecificQuestions().getQuestionId().toString());
								lst.add(sps);
								questionIdOrAnswerIdMap.put(sps.getStatusSpecificQuestions().getQuestionId().toString(), lst);
							}					
						}
					}
					//println("teacherDetail=="+teacherDetail+" jobOrder=="+jobOrder+" statusMaster=="+statusMaster+" secondaryStatus=="+secondaryStatus);
					for(Map.Entry<String, List<StatusSpecificScore>> entry:questionIdOrAnswerIdMap.entrySet()){
						List<StatusSpecificScore> sssList=entry.getValue();
						boolean isScoreFinalized=false;
						sb=new StringBuffer();
					sb.append("<table border=0 class='table' >");
					sb.append("<thead class='bg'>");
					sb.append("<tr><th width=290>"+Utility.getLocaleValuePropByKey("  lblNm", locale)+"</td><th  width=30>"+Utility.getLocaleValuePropByKey("lblScr", locale)+"</td><th  width=80>"+Utility.getLocaleValuePropByKey("  lblcretedDate", locale)+"</th></tr>");//added by 01-04-2015
					sb.append("</thead>");	
					if(sssList.size()>0){
					for(StatusSpecificScore statusSpecificScore : sssList){
						if(statusSpecificScore.getFinalizeStatus()==1){							
							sb.append("<tr><td>"+statusSpecificScore.getUserMaster().getFirstName()+" "+statusSpecificScore.getUserMaster().getLastName()+"</td>");
							sb.append("<td>"+statusSpecificScore.getScoreProvided()+"</td>");
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(statusSpecificScore.getCreatedDateTime())+"</td>");
							isScoreFinalized=true;
						}
					}
					}
					if(!isScoreFinalized){
						sb.append("<tr><td colspan='3'>"+Utility.getLocaleValuePropByKey("  msgNoScoreFinalizedYet", locale)+"</td></tr>");	
					}
					sb.append("</table>");						
					getHtmlByQuestionIdOrAnswerIdMap.put(entry.getKey(), sb.toString());
					//println("entry.getKey()=="+entry.getKey()+"   value=="+getHtmlByQuestionIdOrAnswerIdMap.get(entry.getKey()));
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				println("sb.toString()=="+sb.toString());
			 return getHtmlByQuestionIdOrAnswerIdMap;
		 }
		 //ended by 21-04-2015
		
		public boolean sendReferenceMailOnFinalize(Integer referenceTeacherId,Integer referenceJobId) {
			System.out.println("::::::::::: sendReferenceMailOnFinalize ::::::::::::::::");

			UserMaster userMaster 			= 	null;
			DistrictMaster districtMaster 	= 	null;
			SchoolMaster schoolMaster 		= 	null;
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session1 = request.getSession(false);
			if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}

			if (session1 == null || session1.getAttribute("userMaster") == null) {

			} else {

				userMaster=(UserMaster)session1.getAttribute("userMaster");

				if(userMaster.getDistrictId()!=null){

					districtMaster=userMaster.getDistrictId();

				}

				if(userMaster.getSchoolId()!=null){
					schoolMaster =userMaster.getSchoolId();
				} 
			}

			JobOrder jobOrder			=	null;
			TeacherDetail teacherDetail = 	null;
			try{
				teacherDetail = 	teacherDetailDAO.findById(referenceTeacherId, false, false);
				if(referenceJobId != null){
					jobOrder =		jobOrderDAO.findById(referenceJobId, false, false);
				}
			} catch (Exception exception){
				exception.printStackTrace();
			}

			districtMaster	=	districtMaster == null?jobOrder.getDistrictMaster():districtMaster;

			/*	URL */
			String path = request.getContextPath();
			int portNo = request.getServerPort();
			String showPortNo = null;
			if(portNo==80)
			{
				showPortNo="";
			}
			else
			{
				//showPortNo=":"+portNo;
				showPortNo="";
			}

			String baseUrl = request.getScheme()+"://"+request.getServerName()+""+showPortNo+path+"/";

			Integer districtId 		= 	districtMaster.getDistrictId();
			Integer jobId 			= 	0;
			Integer distId 			= 	0;
			Integer teacherId 		= 	0;
			Integer elerefAutoId	=	0;

			String urlString		=	null;
			String mainURL			=	null;
			String mainURLEncode	=	null;
			String urlEncode		=	null;
			String mailContent		=	null;

			String eId	=	null;
			String dId	=	null;
			String jId	=	null;
			String tId	=	null;

			StringBuffer sbEmail=new StringBuffer("");
			try{

				ScheduleMailThreadByListToBcc smt = new ScheduleMailThreadByListToBcc();
				List<TeacherElectronicReferences> tERList = teacherElectronicReferencesDAO.findActiveContactedReferencesByTeacher(teacherDetail);
				if(tERList.size()>0){
					for(TeacherElectronicReferences reference : tERList){
						String mailToReference = "";
						TeacherElectronicReferences teacherElectronicReferences = teacherElectronicReferencesDAO.findById(reference.getElerefAutoId(), false, false);
						List<TeacherElectronicReferencesHistory> teacherElectronicHistory = null;
						teacherElectronicHistory	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefIdAndDist(teacherElectronicReferences, districtMaster, teacherDetail);
						elerefAutoId 	= 	reference.getElerefAutoId();
						eId		=	Utility.encryptNo(elerefAutoId);
						dId		=	Utility.encryptNo(districtId);
						if(referenceJobId!=null){
							jId		=	Utility.encryptNo(referenceJobId);
						}
						tId		=	Utility.encryptNo(referenceTeacherId);
						urlString 		= 	"eId="+eId+"&dId="+dId+"&jId="+jId+"&tId="+tId+"";
						mainURLEncode	=	baseUrl+"referencecheck.do?p="+Utility.encodeInBase64(urlString);
						mailContent = MailText.ReferenceCheckMail(teacherElectronicReferences, jobOrder, teacherDetail, districtMaster, mainURLEncode);
						mailToReference	=	teacherElectronicReferences.getEmail();
						//System.out.println(":::::::::: teacherElectronicHistory ::::::::::"+teacherElectronicHistory.size());
						if(teacherElectronicHistory.size() == 0){
							System.out.println(":::::::::::::::: Record Save Successfully ::::::::::::::::");
							TeacherElectronicReferencesHistory teacherElectronicReferencesHistory = new  TeacherElectronicReferencesHistory();
							teacherElectronicReferencesHistory.setTeacherElectronicReferences(teacherElectronicReferences);
							teacherElectronicReferencesHistory.setDistrictMaster(districtMaster);
							if(jobOrder!=null){
								teacherElectronicReferencesHistory.setJobOrder(jobOrder);
							}
							teacherElectronicReferencesHistory.setTeacherDetail(teacherElectronicReferences.getTeacherDetail());
							teacherElectronicReferencesHistory.setEmail(teacherElectronicReferences.getEmail());
							teacherElectronicReferencesHistory.setContactStatus(0);
							teacherElectronicReferencesHistory.setReplyStatus(0);
							teacherElectronicReferencesHistory.setCreatedDateTime(new Date());
							teacherElectronicReferencesHistoryDAO.makePersistent(teacherElectronicReferencesHistory);
							System.out.println(mailContent);
							emailerService.sendMailAsHTMLText(mailToReference,"Reference Mail", mailContent);
						} else if(teacherElectronicHistory.size()>0){
							try{
								if(teacherElectronicHistory.get(0).getContactStatus().equals(0)){
									System.out.println(mailContent);
									emailerService.sendMailAsHTMLText(mailToReference,"Reference Mail", mailContent);
								}
							} catch(Exception exception){
								
							}
						}
						/*mailContent = MailText.ReferenceCheckMail(teacherElectronicReferences, jobOrder, teacherDetail, districtMaster, mainURLEncode);
						//System.out.println(mailContent);
						emailerService.sendMailAsHTMLText(mailToReference,Utility.getLocaleValuePropByKey("msgReferenceMail", locale), mailContent);*/
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		}
		
		public boolean sendPortfolioReminderMail(Integer teacherId) {
			UserMaster userMaster 			= 	null;
			DistrictMaster districtMaster 	= 	null;
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session1 = request.getSession(false);
			if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}

			if (session1 == null || session1.getAttribute("userMaster") == null) {

			} else {

				userMaster=(UserMaster)session1.getAttribute("userMaster");

				if(userMaster.getDistrictId()!=null){

					districtMaster=userMaster.getDistrictId();

				}
			}

			/*	URL */
			String path = request.getContextPath();
			int portNo = request.getServerPort();
			String showPortNo = null;
			if(portNo==80)
			{
				showPortNo="";
			}
			else
			{
				//showPortNo=":"+portNo;
				showPortNo="";
			}

			String baseUrl = request.getScheme()+"://"+request.getServerName()+""+showPortNo+path+"/";

			//Integer districtId 		= 	districtMaster.getDistrictId();


			String urlString		=	null;
			String mainURLEncode	=	null;
			String mailContent		=	null;
			String mailToReference =    "";
			
			if(userMaster.getDistrictId()!=null){

				districtMaster=userMaster.getDistrictId();

			}

			int disId   = districtMaster.getDistrictId();
			//int jbId    = 3159;
			String dId	=	null;
			String tId  =   null;
			//String jId	=	null;
			String messageSubject= ""+Utility.getLocaleValuePropByKey("  msgUpdateRequired", locale)+" "+districtMaster.getDistrictName()+"";

			
			//List<String> mailToReference=new ArrayList<String>();
			JobOrder   jobOrder =null;

//			if(jbId!=0){
//				jobOrder=jobOrderDAO.findById(jbId, false, false);
//			}
			try{
				TeacherDetail teacherDetail = 	teacherDetailDAO.findById(teacherId, false, false);

						mailToReference	= teacherDetail.getEmailAddress();
						String to = teacherDetail.getEmailAddress();
						
						dId=Utility.encryptNo(disId);
						tId=Utility.encryptNo(teacherId);
						//jId=Utility.encryptNo(jbId);
						
						urlString 		= 	"dId="+dId+"&tId="+tId+"";
						
						
						
						mainURLEncode	=	baseUrl+"personalinforeminder.do?p="+Utility.encodeInBase64(urlString);
						
						
						
						mailContent = MailText.PortfolioReminderMail(teacherDetail, districtMaster, mainURLEncode);
						System.out.println("mailContent-->"+mailContent);
						emailerService.sendMailAsHTMLText(mailToReference,messageSubject, mailContent);
						
						MessageToTeacher messageToTeacher = new MessageToTeacher();
						messageToTeacher.setTeacherId(teacherDetail);
						messageToTeacher.setJobId(jobOrder);
						messageToTeacher.setTeacherEmailAddress(to);
						messageToTeacher.setMessageSubject(messageSubject);
						messageToTeacher.setMessageSend(mailContent);
						messageToTeacher.setSenderId(userMaster);
						messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
						messageToTeacher.setEntityType(userMaster.getEntityType());
						messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
						messageToTeacher.setCreatedDateTime(new Date());
						messageToTeacherDAO.makePersistent(messageToTeacher);
						
						

			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
	}

	public boolean sendPortfolioReminderBulkMail(Integer[] teacherIds) {

	      System.out.println("sendPortfolioReminderBulkMail called");
			UserMaster userMaster 			= 	null;
			DistrictMaster districtMaster 	= 	null;
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session1 = request.getSession(false);
			if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}

			if (session1 == null || session1.getAttribute("userMaster") == null) {

			} else {

				userMaster=(UserMaster)session1.getAttribute("userMaster");

				if(userMaster.getDistrictId()!=null){

					districtMaster=userMaster.getDistrictId();

				}
			}

			/*	URL */
			String path = request.getContextPath();
			int portNo = request.getServerPort();
			String showPortNo = null;
			if(portNo==80)
			{
				showPortNo="";
			}
			else
			{
				//showPortNo=":"+portNo;
				showPortNo="";
			}

			String baseUrl = request.getScheme()+"://"+request.getServerName()+""+showPortNo+path+"/";

			//Integer districtId 		= 	districtMaster.getDistrictId();


			String urlString		=	null;
			String mainURLEncode	=	null;
			String mailContent		=	null;
			String mailToReference =    "";
			
			if(userMaster.getDistrictId()!=null){

				districtMaster=userMaster.getDistrictId();

			}

			int disId   = districtMaster.getDistrictId();
			//int jbId    = 3159;
			String dId	=	null;
			String tId  =   null;
			//String jId	=	null;
			String messageSubject= ""+Utility.getLocaleValuePropByKey("  msgUpdateRequired", locale)+" "+districtMaster.getDistrictName()+"";

			
			//List<String> mailToReference=new ArrayList<String>();
			JobOrder   jobOrder =null;

//			if(jbId!=0){
//				jobOrder=jobOrderDAO.findById(jbId, false, false);
//			}
			try{
				List<TeacherDetail> teacherDetailList=new ArrayList<TeacherDetail>();
				if(teacherIds.length>0){
					teacherDetailList=teacherDetailDAO.getTeachersHQL(teacherIds);
				}
				if(teacherDetailList.size()>0){
					for(TeacherDetail teacherDetail:teacherDetailList){
						if(teacherDetail!=null)
						mailToReference	= teacherDetail.getEmailAddress();
						String to = teacherDetail.getEmailAddress();
						
						dId=Utility.encryptNo(disId);
						tId=Utility.encryptNo(teacherDetail.getTeacherId());
						//jId=Utility.encryptNo(jbId);
						
						urlString 		= 	"dId="+dId+"&tId="+tId+"";
						
						mainURLEncode	=	baseUrl+"personalinforeminder.do?p="+Utility.encodeInBase64(urlString);
						
						mailContent = MailText.PortfolioReminderMail(teacherDetail, districtMaster, mainURLEncode);
						System.out.println("mailContent-->"+mailContent);
						emailerService.sendMailAsHTMLText(mailToReference,messageSubject, mailContent);
						
						MessageToTeacher messageToTeacher = new MessageToTeacher();
						messageToTeacher.setTeacherId(teacherDetail);
						messageToTeacher.setJobId(jobOrder);
						messageToTeacher.setTeacherEmailAddress(to);
						messageToTeacher.setMessageSubject(messageSubject);
						messageToTeacher.setMessageSend(mailContent);
						messageToTeacher.setSenderId(userMaster);
						messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
						messageToTeacher.setEntityType(userMaster.getEntityType());
						messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
						messageToTeacher.setCreatedDateTime(new Date());
						messageToTeacherDAO.makePersistent(messageToTeacher);
					}
			}
			}catch (Exception e) {
				e.printStackTrace();
			}
			return true;
	}
	
	

	public String getJobSubCateForSeach(Integer jobCateId,Integer districtId,Integer jobSubCateId)
	{
		StringBuffer sb	=	new StringBuffer();
		
		try
		{
			//String[] jobcateIdsArr = jobCateIds.split(",");
			List<Integer> jcateIds = new ArrayList<Integer>();
			
			jcateIds.add(jobCateId);
			JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(jobCateId, false, false);
			
			DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
			List<JobCategoryMaster> jobcateList = new ArrayList<JobCategoryMaster>();
			
			jobcateList.add(jobCategoryMaster);
			
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobSubCategoryByJobCateIds(jobcateList,districtMaster);
			String jcjsiName="";
			
			int jobSubId = 0;
			
			if(jobSubCateId!=null && !jobSubCateId.equals(""))
				jobSubId = jobSubCateId;
			
			System.out.println("  jobSubId :::::: "+jobSubId+" jobCateId :: "+jobCateId+" jobCategoryMasterlst size :: "+jobCategoryMasterlst.size());	
			
			if(jobCategoryMasterlst.size()>0)
			{
				sb.append("<select id='jobSubCategoryId' name='jobSubCategoryId' class='form-control' onchange='resetJobSubCategory();'>");
				sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("  optAll", locale)+"</option>");
				if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
					for(JobCategoryMaster jb: jobCategoryMasterlst)
					{
						if(jobSubId==jb.getJobCategoryId())
							sb.append("<option value='"+jb.getJobCategoryId()+"' selected='selected' >"+jb.getJobCategoryName()+"</option>");
						else
							sb.append("<option value='"+jb.getJobCategoryId()+"'>"+jb.getJobCategoryName()+"</option>");
					}
				}
				sb.append("</select>");
			}
			else
			{
				sb.append("<select id='jobSubCategoryId' name='jobSubCategoryId' class='form-control' disabled='disabled'>");
				sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("  optAll", locale)+"</option>");
				sb.append("</select>");
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return sb.toString();
	}
		
	
	public String sendFinelizeSpecificMailToTeacher(String statusName,String jftIdForSNote,String finelizeSpecificSubjectLine,String finelizeSpecificMessage)
	{
		UserMaster userMaster =null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		
		try
		{
			JobForTeacher jobForTeacher=null;
			try
			{
				Long jobForTeacherId = Long.parseLong(jftIdForSNote);
				if(jobForTeacherId!=null)
					jobForTeacher = jobForTeacherDAO.findById(jobForTeacherId, false, false);
			}
			catch(Exception e){}
			
			statusName = statusName==null? "" : statusName.trim();
			finelizeSpecificSubjectLine = finelizeSpecificSubjectLine==null? "" : finelizeSpecificSubjectLine.trim();
			finelizeSpecificMessage = finelizeSpecificMessage==null? "" : finelizeSpecificMessage.trim();
			
			if(jobForTeacher!=null && !statusName.equals("") && !finelizeSpecificSubjectLine.equals("") && !finelizeSpecificMessage.equals(""))
			{
				System.out.println("--------------------------------------Start Send finelizeSpecificailMail To Teacher-------------------------------------------");
				
				TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
				
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom("shadab.ansari@netsutra.com");
				dsmt.setMailto(teacherDetail.getEmailAddress());
				dsmt.setMailsubject(finelizeSpecificSubjectLine);
				dsmt.setMailcontent(finelizeSpecificMessage);
				
				System.out.println("teacher EmailId:- "+teacherDetail.getEmailAddress());
				System.out.println("finelizeSpecificSubjectLine:- "+finelizeSpecificSubjectLine);
				System.out.println("finelizeSpecificMessage:- "+finelizeSpecificMessage);
				
				try {
					dsmt.start();	
				} catch (Exception e) {}
			}
			return "true";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "";
	}
	
	public List<FinalizeSpecificTemplates> getFinalizeSpecificTemplateList(String statusName, Long jftIdForSNote, String templateId, String notifyUser)
	{
		UserMaster userMaster =null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		
		System.out.println("------------------------------------inside getFinalizeSpecificTemplateList-------------------------------------------");
		try
		{
			JobForTeacher jbforteacher=null;
			List<FinalizeSpecificTemplates> finalizeSpecificTemplateList = null;
			DistrictMaster districtMaster = userMaster.getDistrictId();
			if(jftIdForSNote!=null)
				jbforteacher = jobForTeacherDAO.findById(jftIdForSNote, false, false);
			
			JobOrder jobOrder = null;
			JobCategoryMaster jobCategoryMaster = null;
			TeacherDetail teacherDetail = null;
			
			if(jbforteacher!=null)
			{
				jobOrder = jbforteacher.getJobId()!=null? jbforteacher.getJobId() : new JobOrder();
				jobCategoryMaster = jobOrder!=null && jobOrder.getJobCategoryMaster()!=null? jobOrder.getJobCategoryMaster() : new JobCategoryMaster();
				teacherDetail = jbforteacher.getTeacherId()!=null? jbforteacher.getTeacherId() : new TeacherDetail();
			}
			
			
			System.out.println("statusName:- "+statusName+", jftIdForSNote:- "+jftIdForSNote+", templateId:- "+templateId+", jbforteacher:- "+jbforteacher+", notifyUser:- "+notifyUser);
			if(districtMaster!=null && jobCategoryMaster!=null && statusName!=null && notifyUser!=null)
			{
				finalizeSpecificTemplateList = finalizeSpecificTemplatesDAO.findByDistrictJobCategoryNodeName(districtMaster, jobCategoryMaster, statusName, notifyUser); 
			}
			
			Long tempId=null;
			try{tempId = Long.parseLong(templateId);}catch(Exception e){}
			
			System.out.println("tempId:- "+tempId+", finalizeSpecificTemplateList:- "+finalizeSpecificTemplateList+", jobOrder:- "+jobOrder+", jobCategoryMaster:- "+jobCategoryMaster+", teacherDetail:- "+teacherDetail);
			if(finalizeSpecificTemplateList!=null && finalizeSpecificTemplateList.size()>0)
			{
				for(FinalizeSpecificTemplates finalizeSpecificTemplates : finalizeSpecificTemplateList)
				{
					String SubjectLine = finalizeSpecificTemplates.getSubjectLine();
					String mailBody = finalizeSpecificTemplates.getTemplateBody();
					
					String teacherFirtName = teacherDetail.getFirstName()==null? "" : teacherDetail.getFirstName().trim();
					String teacherLastName = teacherDetail.getLastName()==null? "" : teacherDetail.getLastName().trim();
					String userFirstName = userMaster.getFirstName()==null? "" : userMaster.getFirstName().trim();
					String userLastName = userMaster.getLastName()==null? "" : userMaster.getLastName().trim();
					String jobTitle = jobOrder.getJobTitle()==null? "" : jobOrder.getJobTitle().trim();
					String districtName = districtMaster.getDistrictName()==null? "" : districtMaster.getDistrictName().trim();
					statusName = statusName==null? "" : statusName.trim();
					
					mailBody = mailBody.replaceAll("&lt; Teacher First Name &gt;",teacherFirtName);
					mailBody = mailBody.replaceAll("&lt; Teacher Last Name &gt;",teacherLastName);
					mailBody = mailBody.replaceAll("&lt; User First Name &gt;",userFirstName);
					mailBody = mailBody.replaceAll("&lt; User Last Name &gt;",userLastName);
					mailBody = mailBody.replaceAll("&lt; Job Title &gt;",jobTitle);
					mailBody = mailBody.replaceAll("&lt; District Name &gt;",districtName);
					mailBody = mailBody.replaceAll("&lt; Status Name &gt;",statusName);
					
					finalizeSpecificTemplates.setSubjectLine(SubjectLine);
					finalizeSpecificTemplates.setTemplateBody(mailBody);
					
					if(tempId!=null && finalizeSpecificTemplates.getFinalizeSpecificTemplatesId().equals(tempId))
						finalizeSpecificTemplates.setStatus("Selected");
				}
				
				if(tempId==null)
					finalizeSpecificTemplateList.get(0).setStatus("Selected");
						
				System.out.println("finalizeSpecificTemplateList:- "+finalizeSpecificTemplateList);
				
				return finalizeSpecificTemplateList;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}

}