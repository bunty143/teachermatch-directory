package tm.services;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.lang.ArrayUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictPortfolioConfig;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.PortfolioReminders;
import tm.bean.PortfolioRemindersDspq;
import tm.bean.PortfolioRemindersHistory;
import tm.bean.TeacherAchievementScore;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherPersonalInfo;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DegreeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.master.UniversityMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.PortfolioRemindersDAO;
import tm.dao.PortfolioRemindersDspqDAO;
import tm.dao.PortfolioRemindersHistoryDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAchievementScoreDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.TeacherSecondaryStatusDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DegreeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificPortfolioAnswersDAO;
import tm.dao.master.DistrictSpecificPortfolioQuestionsDAO;
import tm.dao.master.DistrictSpecificRefChkOptionsDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.SecondaryStatusMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TeacherAnswerDetailsForDistrictSpecificQuestionsDAO;
import tm.dao.master.UniversityMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.report.CGReportService;
import tm.services.report.CandidateGridService;
import tm.utility.IPAddressUtility;
import tm.utility.TeacherDetailComparatorLastNameAsc;
import tm.utility.TeacherDetailComparatorLastNameDesc;
import tm.utility.TestTool;
import tm.utility.Utility;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

public class PortfolioReminderAjax {

	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	@Autowired
	DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private DistrictSpecificRefChkOptionsDAO districtSpecificRefChkOptionsDAO;
	
	@Autowired
	private PortfolioRemindersDAO portfolioRemindersDAO;
	
	@Autowired
	private PortfolioRemindersHistoryDAO portfolioRemindersHistoryDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
	
	@Autowired
	private DistrictSpecificPortfolioQuestionsDAO districtSpecificPortfolioQuestionsDAO;
	
	@Autowired
	private PortfolioRemindersDspqDAO portfolioRemindersDspqDAO;
	
	@Autowired
	private EmailerService emailerService;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
	
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	
	@Autowired
	private DegreeMasterDAO degreeMasterDAO;
	
	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	
	@Autowired
	private UniversityMasterDAO universityMasterDAO;
	
	@Autowired
	private SecondaryStatusMasterDAO secondaryStatusMasterDAO;
	
	@Autowired
	private TeacherSecondaryStatusDAO teacherSecondaryStatusDAO;
	
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	
	@Autowired
	private TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	
	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	
	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private TeacherAchievementScoreDAO teacherAchievementScoreDAO;
	
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private DistrictSpecificPortfolioAnswersDAO districtSpecificPortfolioAnswersDAO;
	
	public String getPortfolioDetail(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		StringBuffer dmRecords = new StringBuffer();
		try{
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,22,"assessmentquestions.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			/*
			 * Get Record for DistrictPortFolioConfig
			 * 
			 * */
			List<PortfolioReminders> portfolioRemindersList 		= 	null;
			List<DistrictPortfolioConfig> districtPortfolioConfig 	= 	null;
			DistrictPortfolioConfig portConfig 						= 	null;
			PortfolioReminders portReminder 						= 	null;
			DistrictMaster districtMaster							=	null;
			Integer profileCheckCounter	= 0;
			Integer eeocCheckCounter	= 0;
			Integer dspqCheckCounter	= 0;

			districtMaster 			= 	districtMasterDAO.findById(districtId, false, false);
			districtPortfolioConfig = 	districtPortfolioConfigDAO.getPortfolioConfigsByDistrictAndCandidate(districtMaster,"I");
			portfolioRemindersList	= 	portfolioRemindersDAO.getRecordByDistrict(districtMaster);
			List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsList=districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestionBYDistrict(districtMaster);
			
			List<DistrictPortfolioConfig> distPortConfig =	null;
			distPortConfig	=	districtPortfolioConfigDAO.getPortfolioConfigs(districtMaster);
			
			int coverLetter=0,academicDspq=0,academicTranscript=0,certification=0,proofOfCertification=0,reference=0,referenceLettersOfRecommendation=0;
			int resume=0,tfaAffiliate=0,willingAsSubstituteTeacher=0,candidateType=0,address=0,expCertTeacherTraining=0,nationalBoardCert=0,affidavit=0;
			int phoneNumber=0,personalinfo=0,dateOfBirth=0,ssn=0,race=0,genderId=0,ethnicorigin=0,ethinicity=0,employment=0,formeremployee=0;
			int generalKnowledgeExam=0,subjectAreaExam=0,additionalDocuments=0,retirementnumber=0,veteran=0,videoLink=0,involvement=0,honors=0;
			System.out.println("::::::::::::::: distPortConfig SIZE ::::::::::::::::::"+distPortConfig.size());
			try{
				if(distPortConfig!=null && distPortConfig.size()>0){
					for(DistrictPortfolioConfig dspqRecord : distPortConfig){
						if(dspqRecord.getCoverLetter().equals(true)){coverLetter=coverLetter+1;}
						if(dspqRecord.getAcademic().equals(1)){academicDspq=academicDspq+1;}
						if(dspqRecord.getAcademicTranscript().equals(1)){academicTranscript=academicTranscript+1;}
						if(dspqRecord.getCertification().equals(1)){certification=certification+1;}
						if(dspqRecord.getProofOfCertification().equals(1)){proofOfCertification=proofOfCertification+1;}
						if(dspqRecord.getReference().equals(1)){reference=reference+1;}
						if(dspqRecord.getReferenceLettersOfRecommendation().equals(1)){referenceLettersOfRecommendation=referenceLettersOfRecommendation+1;}
						if(dspqRecord.getResume().equals(1)){resume=resume+1;}
						if(dspqRecord.getTfaAffiliate().equals(true)){tfaAffiliate=tfaAffiliate+1;}
						if(dspqRecord.getWillingAsSubstituteTeacher().equals(true)){willingAsSubstituteTeacher=willingAsSubstituteTeacher+1;}
						if(dspqRecord.getExpCertTeacherTraining().equals(true)){expCertTeacherTraining=expCertTeacherTraining+1;}
						if(dspqRecord.getNationalBoardCert().equals(true)){nationalBoardCert=nationalBoardCert+1;}
						if(dspqRecord.getAffidavit().equals(true)){affidavit=affidavit+1;}
						if(dspqRecord.getEmployment().equals(true)){employment=employment+1;}
						if(dspqRecord.getFormeremployee().equals(true)){formeremployee=formeremployee+1;}
						if(dspqRecord.getGeneralKnowledgeExam().equals(true)){generalKnowledgeExam=generalKnowledgeExam+1;}
						if(dspqRecord.getSubjectAreaExam().equals(true)){subjectAreaExam=subjectAreaExam+1;}
						if(dspqRecord.getAdditionalDocuments().equals(true)){additionalDocuments=additionalDocuments+1;}
						if(dspqRecord.getRetirementnumber().equals(true)){retirementnumber=retirementnumber+1;}
						if(dspqRecord.getVeteran().equals(true)){veteran=veteran+1;}
						if(dspqRecord.getVideoLink().equals(true)){videoLink=videoLink+1;}
						if(dspqRecord.getInvolvement().equals(true)){involvement=involvement+1;}
						if(dspqRecord.getHonors().equals(true)){honors=honors+1;}
					}
				}
				
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			try{
				if(districtPortfolioConfig!=null && districtPortfolioConfig.size()>0){
					portConfig = districtPortfolioConfig.get(0);
				}
				
				if(portfolioRemindersList != null && portfolioRemindersList.size()>0){
					portReminder = portfolioRemindersList.get(0);
				}
				
			} catch(Exception exception){
				exception.printStackTrace();
			}
			String checkedType = "";
			
			dmRecords.append("<div class='panel-group' id='accordion'>");
			
			dmRecords.append("<div class='panel panel-default'>");
				dmRecords.append("<div class='accordion-heading'> <a href='#collapseOne' data-parent='#accordion' data-toggle='collapse' class='accordion-toggle minus'> "+Utility.getLocaleValuePropByKey("msgTeacherMatchPortfolio", locale)+" </a> </div>");
				dmRecords.append("<div class='accordion-body in' id='collapseOne' style='height:auto;'>");
				dmRecords.append("<div class='accordion-inner'>");
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;' id='divProfileGrid'>");
				dmRecords.append("<div style='padding:5px 0px 0px 5px;'>");
				
				dmRecords.append("<div class='span12'>");
				dmRecords.append("<input id='persionalInfo' name='persionalInfo' style='vertical-align: middle' type='checkbox' onclick='checkAllPortfolio()'>");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("msgPersionalInfo", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("<div class=\"left15\">");
				
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				if(portReminder!=null && portReminder.getFirstName()!=null)
				if(portReminder.getFirstName().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='firstName' name='firstName' type='checkbox' candPortfolio='portDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblFname", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				if(portReminder!=null && portReminder.getLastName()!=null)
					if(portReminder.getLastName().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='lastName' name='lastName' type='checkbox' candPortfolio='portDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblLname", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
					if(portReminder!=null && portReminder.getAddressLine1()!=null)
						if(portReminder.getAddressLine1().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='addressLine1' name='addressLine1' type='checkbox' candPortfolio='portDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblAdd", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
					if(portReminder!=null && portReminder.getPhoneNumber()!=null)
						if(portReminder.getPhoneNumber().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='phoneNumber' name='phoneNumber' type='checkbox' candPortfolio='portDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblPhoneNo", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getMobileNumber()!=null)
						if(portReminder.getMobileNumber().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='mobileNumber' name='mobileNumber' type='checkbox' candPortfolio='portDivHidden' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblMobileNumber", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getDob()!=null)
						if(portReminder.getDob().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='dob' name='dob' type='checkbox' candPortfolio='portDivHidden' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblDOB", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
				
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;' id='divEEOCGrid'>");
				dmRecords.append("<div style='padding:5px 0px 0px 5px;'>");
				
				dmRecords.append("<div class='span12'>");
				dmRecords.append("<input id='eeoc' name='eeoc' style='vertical-align: middle' type='checkbox'  onclick='checkAllEEOC()'>");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("msgEEOC3", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("<div class=\"left15\">");
				
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
					if(portReminder!=null && portReminder.getGenderId()!=null)
						if(portReminder.getGenderId().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='genderId' name='genderId' type='checkbox' candEEOC='eeocDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblGend", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
					if(portReminder!=null && portReminder.getEthnicityId()!=null)
						if(portReminder.getEthnicityId().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='ethnicityId' name='ethnicityId' type='checkbox' candEEOC='eeocDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblEth", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getEthnicOriginId()!=null)
						if(portReminder.getEthnicOriginId().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='ethnicOriginId' name='ethnicOriginId' type='checkbox' candEEOC='eeocDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblEthOrig", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
					if(portReminder!=null && portReminder.getRaceId()!=null)
						if(portReminder.getRaceId().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='raceId' name='raceId' type='checkbox' candEEOC='eeocDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblRac", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
				
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getAcademics()!=null)
						if(portReminder.getAcademics().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='academics' name='academics' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("headAcad", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				checkedType = "";
				
				if(portReminder!=null && portReminder.getCertificationPortfolio()!=null)
					if(portReminder.getCertificationPortfolio().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='certificationPortfolio' name='certificationPortfolio' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lnkCerti/Lice", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				checkedType = "";
				
				if(portReminder!=null && portReminder.getHonors()!=null)
					if(portReminder.getHonors().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='honors' name='honors' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblHonors", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				
				checkedType = "";
					if(portReminder!=null && portReminder.getEmploymentHistory()!=null)
						if(portReminder.getEmploymentHistory().equals(1))
					checkedType = "checked";
					
				dmRecords.append("<input id='employmentHistory' name='employmentHistory' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblEmplHis", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				
				checkedType = "";
					if(portReminder!=null && portReminder.getAdditionalDocuments()!=null)
						if(portReminder.getAdditionalDocuments().equals(1))
					checkedType = "checked";
					
				dmRecords.append("<input id='additionalDocuments' name='additionalDocuments' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lnkAddDocu", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				
				checkedType = "";
					if(portReminder!=null && portReminder.getReferencesPortfolio()!=null)
						if(portReminder.getReferencesPortfolio().equals(1))
					checkedType = "checked";
					
				dmRecords.append("<input id='references' name='references' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				
				checkedType = "";
					if(portReminder!=null && portReminder.getTfaAffiliatePortfolio()!=null)
						if(portReminder.getTfaAffiliatePortfolio().equals(1))
					checkedType = "checked";
					
				dmRecords.append("<input id='tfaAffiliatePortfolio' name='tfaAffiliatePortfolio' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("msgTeachTFAAffiliate", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				
				checkedType = "";
					if(portReminder!=null && portReminder.getSubstituteTeacher()!=null)
						if(portReminder.getSubstituteTeacher().equals(1))
					checkedType = "checked";
					
				dmRecords.append("<input id='substituteTeacher' name='substituteTeacher' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblSubstituteTeacher", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getResume()!=null)
						if(portReminder.getResume().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='resume' name='resume' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblResume", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				/*dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getHighNeedSchool()!=null)
						if(portReminder.getHighNeedSchool().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='highNeedSchool' name='highNeedSchool' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>High Need School<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");*/
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getSSN()!=null)
						if(portReminder.getSSN().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='SSN' name='SSN' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblSSN", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getVeteran()!=null)
						if(portReminder.getVeteran().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='veteran' name='veteran' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblVeteran", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getRetirementnumber()!=null)
						if(portReminder.getRetirementnumber().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='retirementnumber' name='retirementnumber' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("msgRetirementNumber", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;'>");
				dmRecords.append("<div class='span12' style='padding:5px 0px 0px 5px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getVideoLink()!=null)
						if(portReminder.getVideoLink().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='videoLink' name='videoLink' style='vertical-align: middle' type='checkbox' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"<span>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
				
				
			dmRecords.append("<div class='panel panel-default'>");
			dmRecords.append("<div class='accordion-heading'> <a href='#collapseTwo' data-parent='#accordion' data-toggle='collapse' class='accordion-toggle plus'> "+Utility.getLocaleValuePropByKey("msgDistrictPortfolioProfile", locale)+" </a> </div>");
			dmRecords.append("<div class='accordion-body collapse' id='collapseTwo' style='height:auto;'>");
			dmRecords.append("<div class='accordion-inner'>");
			
			dmRecords.append("<div class='row' style='padding:5px 0px 0px 15px;' id='divDSPQGrid'>");
			dmRecords.append("<div style='padding:5px 0px 0px 5px;'>");
			
			/*dmRecords.append("<div class='span12'>");
			dmRecords.append("<span style='padding:5px 0px 0px 5px;'><span>");
			dmRecords.append("</div>");*/
			dmRecords.append("<div class=\"left15\">");
			//System.out.println(":::::::::::::::: coverLetter :::::::::::::::"+coverLetter);
			/*if(distPortConfig!=null && coverLetter>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				if(portReminder!=null && portReminder.getCoverLetter()!=null) 
				if(portReminder.getCoverLetter().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='coverLetter' name='coverLetter' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>Cover Letter</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}*/
            
			if(distPortConfig!=null && academicDspq>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getAcademicDspq()!=null) 
						if(portReminder.getAcademicDspq().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='academicDspq' name='academicDspq' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("headAcad", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			if(distPortConfig!=null && academicTranscript>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getAcademicTranscript()!=null) 
						if(portReminder.getAcademicTranscript().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='academicTranscript' name='academicTranscript' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("HeadTran", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			if(distPortConfig!=null && certification>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getCertification()!=null) 
						if(portReminder.getCertification().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='certification' name='certification' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblCertification", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			/*if(distPortConfig!=null && proofOfCertification>0){			
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getProofOfCertification()!=null) 
						if(portReminder.getProofOfCertification().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='proofOfCertification' name='proofOfCertification' type='checkbox' candDSPQ='dspqDivHidden' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>Proof Of Certification</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}*/
			
			if(distPortConfig!=null && reference>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getReferenceDspq()!=null) 
						if(portReminder.getReferenceDspq().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='referenceDspq' name='referenceDspq' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			if(distPortConfig!=null && referenceLettersOfRecommendation>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getReferenceLettersOfRecommendation()!=null) 
						if(portReminder.getReferenceLettersOfRecommendation().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='referenceLettersOfRecommendation' name='referenceLettersOfRecommendation' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("msgReferenceLetters", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			if(distPortConfig!=null && tfaAffiliate>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getTfaAffiliate()!=null) 
						if(portReminder.getTfaAffiliate().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='tfaAffiliate' name='tfaAffiliate' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("msgTFAAffiliate", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			if(distPortConfig!=null && willingAsSubstituteTeacher>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getWillingAsSubstituteTeacher()!=null) 
						if(portReminder.getWillingAsSubstituteTeacher().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='willingAsSubstituteTeacher' name='willingAsSubstituteTeacher' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("msgWillingAsSubstitute", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			if(distPortConfig!=null && expCertTeacherTraining>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getExpCertTeacherTraining()!=null) 
						if(portReminder.getExpCertTeacherTraining().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='expCertTeacherTraining' name='expCertTeacherTraining' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("msgTeachingExperience", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			if(distPortConfig!=null && nationalBoardCert>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getNationalBoardCert()!=null) 
						if(portReminder.getNationalBoardCert().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='nationalBoardCert' name='nationalBoardCert' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("msgNationalBoardCert", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			if(distPortConfig!=null && affidavit>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getAffidavit()!=null) 
						if(portReminder.getAffidavit().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='affidavit' name='affidavit' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("headAffidavit", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			if(distPortConfig!=null && employment>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getEmployment()!=null) 
						if(portReminder.getEmployment().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='employment' name='employment' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblEmplHis", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			if(distPortConfig!=null && formeremployee>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
			
					if(portReminder!=null && portReminder.getFormeremployee()!=null) 
						if(portReminder.getFormeremployee().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='formeremployee' name='formeremployee' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblFormerEmployee1", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			if(distPortConfig!=null && involvement>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getInvolvement()!=null) 
						if(portReminder.getInvolvement().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='involvement' name='involvement' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>"+Utility.getLocaleValuePropByKey("lblInvolvement", locale)+"</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			/*if(distPortConfig!=null && generalKnowledgeExam>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getGeneralKnowledgeExam()!=null) 
						if(portReminder.getGeneralKnowledgeExam().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='generalKnowledgeExam' name='generalKnowledgeExam' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>General Knowledge Exam</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}
			
			if(distPortConfig!=null && subjectAreaExam>0){
				dmRecords.append("<div class='col-sm-3 col-md-3 top5' style='padding-left: 20px;'>");
				dmRecords.append("<label class='' style='padding-left: 0px;'>");
				checkedType = "";
				
					if(portReminder!=null && portReminder.getSubjectAreaExam()!=null) 
						if(portReminder.getSubjectAreaExam().equals(1))
					checkedType = "checked";
				dmRecords.append("<input id='subjectAreaExam' name='subjectAreaExam' type='checkbox' candDSPQ='dspqDivHidden' style='vertical-align: middle' "+checkedType+">");
				dmRecords.append("<span style='padding:5px 0px 0px 5px;'>Subject Area Exam</span>");
				dmRecords.append("</label>");
				dmRecords.append("</div>");
			}*/

			dmRecords.append("</div>");
			dmRecords.append("</div>");
			dmRecords.append("</div>");
			
			dmRecords.append("</div>");
			dmRecords.append("</div>");
			dmRecords.append("</div>");
			
			dmRecords.append("<div class='panel panel-default'>");
			dmRecords.append("<div class='accordion-heading'> <a href='#collapseThree' data-parent='#accordion' data-toggle='collapse' class='accordion-toggle plus'> "+Utility.getLocaleValuePropByKey("msgDistrictPortfolioQuestion", locale)+" </a> </div>");
			dmRecords.append("<div class='accordion-body collapse' id='collapseThree' style='height:auto;'>");
			dmRecords.append("<div class='accordion-inner'>");
			dmRecords.append("<div>");
			
			int totalQuestions = districtSpecificPortfolioQuestionsList.size();
			int cnt = 0;
			int id = 1;
			/* Create Map For portfolioremindersdspq table*/
			
			Map<DistrictSpecificPortfolioQuestions,PortfolioRemindersDspq> dspqMap=new HashMap<DistrictSpecificPortfolioQuestions,PortfolioRemindersDspq>();
			List<PortfolioRemindersDspq> portfolioRemindersDspqList = null;
			List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsL = new ArrayList<DistrictSpecificPortfolioQuestions>();
			portfolioRemindersDspqList = portfolioRemindersDspqDAO.getRecordByDistrict(districtMaster);
			try{
				if(portfolioRemindersDspqList.size()>0){
					for (PortfolioRemindersDspq portfolioRemindersDspq : portfolioRemindersDspqList) {
						districtSpecificPortfolioQuestionsL.add(portfolioRemindersDspq.getDistrictSpecificPortfolioQuestions());
						//dspqMap.put(portfolioRemindersDspq.getDistrictSpecificPortfolioQuestions(), portfolioRemindersDspq);
					}
				}
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			if(districtSpecificPortfolioQuestionsList.size()>0){
				int i=0;
				int j = 1;
				for (DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions : districtSpecificPortfolioQuestionsList) 
				{
					Integer questionIdCheck = 0;
					checkedType = "";
					if(j<=districtSpecificPortfolioQuestionsList.size() && districtSpecificPortfolioQuestionsL.size()>0){
						if(i!=districtSpecificPortfolioQuestionsL.size())
							questionIdCheck = districtSpecificPortfolioQuestionsL.get(i).getQuestionId();
						if(questionIdCheck.equals(districtSpecificPortfolioQuestions.getQuestionId())){
							checkedType = "checked";
							i++;
						}
					}
					
					dmRecords.append("<div style='padding:5px 0px 0px 5px;'>");
					dmRecords.append("<div class='span12'>");
					dmRecords.append("<input style='vertical-align: middle;' type='checkbox' name='dspqPortfolioQuestion' id='dspqPortfolioQuestion_"+id+"' value="+(districtSpecificPortfolioQuestions.getQuestionId())+" "+checkedType+">");
					dmRecords.append("&nbsp;&nbsp;"+Utility.getUTFToHTML(districtSpecificPortfolioQuestions.getQuestion())+"");
					dmRecords.append("</div>");
					dmRecords.append("</div>");
					id++;
					j++;
				}									
			}
			dmRecords.append("</div>");
			
			dmRecords.append("</div>");
			dmRecords.append("</div>");
			dmRecords.append("</div>");
			
			dmRecords.append("<div>");
			
			/* DSPQ ENDS */
			
	}catch (Exception e) {
			e.printStackTrace();
		}
		return dmRecords.toString();
	}

	@Transactional(readOnly=false)
	//public String savePortfolioDetail(Integer districtId,Integer academics,Integer resume,Integer highNeedSchool,Integer SSN,Integer veteran,Integer retirementnumber,Integer videoLink,String arrProfile[],String arrEEOC[],String arrDSPQ[])
	public String savePortfolioDetail(Integer districtId,Integer academics,Integer resume,Integer highNeedSchool,Integer SSN,Integer veteran,Integer retirementnumber,Integer videoLink,Integer certificationPortfolioVal,Integer employmentHistoryVal,Integer additionalDocumentsVal,Integer referencesVal,Integer tfaAffiliatePortfolioVal,Integer substituteTeacherVal,String arrProfile[],String arrEEOC[],String coverLetter,String certification,String tfaAffiliate,String candidateType,String affidavit,String employment,String formeremployee,String subjectAreaExam,String involvement,String academicTranscript,String proofOfCertification,String referenceLettersOfRecommendation,String willingAsSubstituteTeacher,String expCertTeacherTraining,String nationalBoardCert,String generalKnowledgeExam,String dspQuestionArray,Integer honors,String academicDspq,String referenceDspq)
	{
		
		System.out.println("::::::::::::::::::::::::: savePortfolioDetail :::::::::::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		StringBuffer dmRecords = new StringBuffer();
		Integer status = 2;
		try{
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,22,"assessmentquestions.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(coverLetter!=null)
				coverLetter = coverLetter.equals("false")?"0":"1";
			if(academicTranscript!=null)
				academicTranscript = academicTranscript.equals("false")?"0":"1";
			if(certification!=null)
				certification = certification.equals("false")?"0":"1";
			if(proofOfCertification!=null)
				proofOfCertification = proofOfCertification.equals("false")?"0":"1";
			if(referenceLettersOfRecommendation!=null)
				referenceLettersOfRecommendation = 	referenceLettersOfRecommendation.equals("false")?"0":"1";
			if(tfaAffiliate!=null)
				tfaAffiliate = tfaAffiliate.equals("false")?"0":"1";
			if(willingAsSubstituteTeacher!=null)
				willingAsSubstituteTeacher 	= 	willingAsSubstituteTeacher.equals("false")?"0":"1";
			if(candidateType!=null)
				candidateType = candidateType.equals("false")?"0":"1";
			if(expCertTeacherTraining!=null)
				expCertTeacherTraining 	= 	expCertTeacherTraining.equals("false")?"0":"1";
			if(nationalBoardCert!=null)
				nationalBoardCert = nationalBoardCert.equals("false")?"0":"1";
			if(affidavit!=null)
				affidavit 	= 	affidavit.equals("false")?"0":"1";
			if(employment!=null)
				employment = employment.equals("false")?"0":"1";
			if(formeremployee!=null)
				formeremployee = 	formeremployee.equals("false")?"0":"1";
			if(generalKnowledgeExam!=null)
				generalKnowledgeExam = 	generalKnowledgeExam.equals("false")?"0":"1";
			if(subjectAreaExam!=null)
				subjectAreaExam = 	subjectAreaExam.equals("false")?"0":"1";
			if(involvement!=null)
				involvement = 	involvement.equals("false")?"0":"1";
			
			if(academicDspq!=null)
				academicDspq = academicDspq.equals("false")?"0":"1";
			if(referenceDspq!=null)
				referenceDspq = referenceDspq.equals("false")?"0":"1";
			
			DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			
			List<PortfolioReminders> portfolioRemindersList = null;
			List<PortfolioRemindersHistory> portfolioRemindersHistoryList = null;
			
			portfolioRemindersList	 		= 	portfolioRemindersDAO.getRecordByDistrict(districtMaster);
			portfolioRemindersHistoryList	=	portfolioRemindersHistoryDAO.getRecordByDistrict(districtMaster);
			SessionFactory sessionFactory	=	jobForTeacherDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			Transaction txOpen = statelesSsession.beginTransaction();
			try{
				if(portfolioRemindersList!=null && portfolioRemindersList.size()==0) {
					PortfolioReminders portfolioReminders = new PortfolioReminders();
					portfolioReminders.setDistrictMaster(districtMaster);
					portfolioReminders.setSalutation(1);
					portfolioReminders.setFirstName(Integer.parseInt(arrProfile[0]));
					portfolioReminders.setLastName(Integer.parseInt(arrProfile[1]));
					portfolioReminders.setAddressLine1(Integer.parseInt(arrProfile[2]));
					portfolioReminders.setPhoneNumber(Integer.parseInt(arrProfile[3]));
					portfolioReminders.setMobileNumber(Integer.parseInt(arrProfile[4]));
					portfolioReminders.setDob(Integer.parseInt(arrProfile[5]));
					
					portfolioReminders.setGenderId(Integer.parseInt(arrEEOC[0]));
					portfolioReminders.setEthnicityId(Integer.parseInt(arrEEOC[1]));
					portfolioReminders.setEthnicOriginId(Integer.parseInt(arrEEOC[2]));
					portfolioReminders.setRaceId(Integer.parseInt(arrEEOC[3]));
					
					portfolioReminders.setAcademics(academics);
					//portfolioReminders.setCertification(1);
					portfolioReminders.setHonors(honors);
					portfolioReminders.setResume(resume);
					portfolioReminders.setHighNeedSchool(highNeedSchool);
					portfolioReminders.setSSN(SSN);
					portfolioReminders.setVeteran(veteran);
					portfolioReminders.setRetirementnumber(retirementnumber);
					portfolioReminders.setVideoLink(videoLink);
					portfolioReminders.setCertificationPortfolio(certificationPortfolioVal);
					portfolioReminders.setEmploymentHistory(employmentHistoryVal);
					portfolioReminders.setAdditionalDocuments(additionalDocumentsVal);
					portfolioReminders.setReferencesPortfolio(referencesVal);
					portfolioReminders.setTfaAffiliatePortfolio(tfaAffiliatePortfolioVal);
					portfolioReminders.setSubstituteTeacher(substituteTeacherVal);
					
					if(coverLetter!=null)
						portfolioReminders.setCoverLetter(Integer.parseInt(coverLetter));
					if(academicTranscript!=null)
						portfolioReminders.setAcademicTranscript(Integer.parseInt(academicTranscript));
					if(certification!=null)
						portfolioReminders.setCertification(Integer.parseInt(certification));
					if(proofOfCertification!=null)
						portfolioReminders.setProofOfCertification(Integer.parseInt(proofOfCertification));
					if(referenceLettersOfRecommendation!=null)
						portfolioReminders.setReferenceLettersOfRecommendation(Integer.parseInt(referenceLettersOfRecommendation));
					if(tfaAffiliate!=null)
						portfolioReminders.setTfaAffiliate(Integer.parseInt(tfaAffiliate));
					if(willingAsSubstituteTeacher!=null)
						portfolioReminders.setWillingAsSubstituteTeacher(Integer.parseInt(willingAsSubstituteTeacher));
					if(candidateType!=null)
						portfolioReminders.setCandidateType(Integer.parseInt(candidateType));
					if(expCertTeacherTraining!=null)
						portfolioReminders.setExpCertTeacherTraining(Integer.parseInt(expCertTeacherTraining));
					if(nationalBoardCert!=null)
						portfolioReminders.setNationalBoardCert(Integer.parseInt(nationalBoardCert));
					if(affidavit!=null)
						portfolioReminders.setAffidavit(Integer.parseInt(affidavit));
					if(employment!=null)
						portfolioReminders.setEmployment(Integer.parseInt(employment));
					if(formeremployee!=null)
						portfolioReminders.setFormeremployee(Integer.parseInt(formeremployee));
					if(generalKnowledgeExam!=null)
						portfolioReminders.setGeneralKnowledgeExam(Integer.parseInt(generalKnowledgeExam));
					if(subjectAreaExam!=null)
						portfolioReminders.setSubjectAreaExam(Integer.parseInt(subjectAreaExam));
					if(involvement!=null)
						portfolioReminders.setInvolvement(Integer.parseInt(involvement));
					
					if(academicDspq!=null)
						portfolioReminders.setAcademicDspq(Integer.parseInt(academicDspq));
					if(referenceDspq!=null)
						portfolioReminders.setReferenceDspq(Integer.parseInt(referenceDspq));
					
					portfolioRemindersDAO.makePersistent(portfolioReminders);
					
					/* Copy To Portfolio History Table */
					
					PortfolioRemindersHistory portfolioRemindersHistory = new PortfolioRemindersHistory();
					portfolioRemindersHistory.setDistrictMaster(districtMaster);
					portfolioRemindersHistory.setSalutation(1);
					portfolioRemindersHistory.setFirstName(Integer.parseInt(arrProfile[0]));
					portfolioRemindersHistory.setLastName(Integer.parseInt(arrProfile[1]));
					portfolioRemindersHistory.setAddressLine1(Integer.parseInt(arrProfile[2]));
					portfolioRemindersHistory.setPhoneNumber(Integer.parseInt(arrProfile[3]));
					portfolioRemindersHistory.setMobileNumber(Integer.parseInt(arrProfile[4]));
					portfolioRemindersHistory.setDob(Integer.parseInt(arrProfile[5]));
					
					portfolioRemindersHistory.setGenderId(Integer.parseInt(arrEEOC[0]));
					portfolioRemindersHistory.setEthnicityId(Integer.parseInt(arrEEOC[1]));
					portfolioRemindersHistory.setEthnicOriginId(Integer.parseInt(arrEEOC[2]));
					portfolioRemindersHistory.setRaceId(Integer.parseInt(arrEEOC[3]));
					
					portfolioRemindersHistory.setAcademics(academics);
					//portfolioReminders.setCertification(1);
					portfolioRemindersHistory.setHonors(honors);
					portfolioRemindersHistory.setResume(resume);
					portfolioRemindersHistory.setHighNeedSchool(highNeedSchool);
					portfolioRemindersHistory.setSSN(SSN);
					portfolioRemindersHistory.setVeteran(veteran);
					portfolioRemindersHistory.setRetirementnumber(retirementnumber);
					portfolioRemindersHistory.setVideoLink(videoLink);
					portfolioRemindersHistory.setCertificationPortfolio(certificationPortfolioVal);
					portfolioRemindersHistory.setEmploymentHistory(employmentHistoryVal);
					portfolioRemindersHistory.setAdditionalDocuments(additionalDocumentsVal);
					portfolioRemindersHistory.setReferencesPortfolio(referencesVal);
					portfolioRemindersHistory.setTfaAffiliatePortfolio(tfaAffiliatePortfolioVal);
					portfolioRemindersHistory.setSubstituteTeacher(substituteTeacherVal);
					
					if(coverLetter!=null)
						portfolioRemindersHistory.setCoverLetter(Integer.parseInt(coverLetter));
					if(academicTranscript!=null)
						portfolioRemindersHistory.setAcademicTranscript(Integer.parseInt(academicTranscript));
					if(certification!=null)
						portfolioRemindersHistory.setCertification(Integer.parseInt(certification));
					if(proofOfCertification!=null)
						portfolioRemindersHistory.setProofOfCertification(Integer.parseInt(proofOfCertification));
					if(referenceLettersOfRecommendation!=null)
						portfolioRemindersHistory.setReferenceLettersOfRecommendation(Integer.parseInt(referenceLettersOfRecommendation));
					if(tfaAffiliate!=null)
						portfolioRemindersHistory.setTfaAffiliate(Integer.parseInt(tfaAffiliate));
					if(willingAsSubstituteTeacher!=null)
						portfolioRemindersHistory.setWillingAsSubstituteTeacher(Integer.parseInt(willingAsSubstituteTeacher));
					if(candidateType!=null)
						portfolioRemindersHistory.setCandidateType(Integer.parseInt(candidateType));
					if(expCertTeacherTraining!=null)
						portfolioRemindersHistory.setExpCertTeacherTraining(Integer.parseInt(expCertTeacherTraining));
					if(nationalBoardCert!=null)
						portfolioRemindersHistory.setNationalBoardCert(Integer.parseInt(nationalBoardCert));
					if(affidavit!=null)
						portfolioRemindersHistory.setAffidavit(Integer.parseInt(affidavit));
					if(employment!=null)
						portfolioRemindersHistory.setEmployment(Integer.parseInt(employment));
					if(formeremployee!=null)
						portfolioRemindersHistory.setFormeremployee(Integer.parseInt(formeremployee));
					if(generalKnowledgeExam!=null)
						portfolioRemindersHistory.setGeneralKnowledgeExam(Integer.parseInt(generalKnowledgeExam));
					if(subjectAreaExam!=null)
						portfolioRemindersHistory.setSubjectAreaExam(Integer.parseInt(subjectAreaExam));
					if(involvement!=null)
						portfolioRemindersHistory.setInvolvement(Integer.parseInt(involvement));
					
					if(academicDspq!=null)
						portfolioRemindersHistory.setAcademicDspq(Integer.parseInt(academicDspq));
					if(referenceDspq!=null)
						portfolioRemindersHistory.setReferenceDspq(Integer.parseInt(referenceDspq));

					portfolioRemindersHistoryDAO.makePersistent(portfolioRemindersHistory);
					/*  End Copy Record Into History Table */
					/*	Save Question ids */
					String arr[]=dspQuestionArray.split(",");
					List dspqQuestionList = new ArrayList();
					for(int i=0;i<arr.length;i++)
					{
						dspqQuestionList.add(Integer.parseInt(arr[i]));
					}
					List<DistrictSpecificPortfolioQuestions> dspqList = districtSpecificPortfolioQuestionsDAO.getQuestionList(dspqQuestionList);
					for(DistrictSpecificPortfolioQuestions dspq : dspqList){
						DistrictSpecificPortfolioQuestions dspqQuestions = districtSpecificPortfolioQuestionsDAO.findById(dspq.getQuestionId(), false, false);
						PortfolioRemindersDspq portfolioRemindersDspq = new PortfolioRemindersDspq();
						portfolioRemindersDspq.setDistrictSpecificPortfolioQuestions(dspqQuestions);
						portfolioRemindersDspq.setDistrictMaster(districtMaster);
						portfolioRemindersDspqDAO.makePersistent(portfolioRemindersDspq);
					}
					status = 1;
				} else {
					PortfolioReminders portfolioReminders1 		= 	portfolioRemindersDAO.findById(portfolioRemindersList.get(0).getPortfolioremindersId(), false, false);
					
					// Update Record
					portfolioReminders1.setSalutation(1);
					portfolioReminders1.setFirstName(Integer.parseInt(arrProfile[0]));
					portfolioReminders1.setLastName(Integer.parseInt(arrProfile[1]));
					portfolioReminders1.setAddressLine1(Integer.parseInt(arrProfile[2]));
					portfolioReminders1.setPhoneNumber(Integer.parseInt(arrProfile[3]));
					portfolioReminders1.setMobileNumber(Integer.parseInt(arrProfile[4]));
					portfolioReminders1.setDob(Integer.parseInt(arrProfile[5]));
					
					portfolioReminders1.setGenderId(Integer.parseInt(arrEEOC[0]));
					portfolioReminders1.setEthnicityId(Integer.parseInt(arrEEOC[1]));
					portfolioReminders1.setEthnicOriginId(Integer.parseInt(arrEEOC[2]));
					portfolioReminders1.setRaceId(Integer.parseInt(arrEEOC[3]));
					
					portfolioReminders1.setAcademics(academics);
					portfolioReminders1.setHonors(honors);
					portfolioReminders1.setResume(resume);
					portfolioReminders1.setHighNeedSchool(highNeedSchool);
					portfolioReminders1.setSSN(SSN);
					portfolioReminders1.setVeteran(veteran);
					portfolioReminders1.setRetirementnumber(retirementnumber);
					portfolioReminders1.setVideoLink(videoLink);
					portfolioReminders1.setCertificationPortfolio(certificationPortfolioVal);
					portfolioReminders1.setEmploymentHistory(employmentHistoryVal);
					portfolioReminders1.setAdditionalDocuments(additionalDocumentsVal);
					portfolioReminders1.setReferencesPortfolio(referencesVal);
					portfolioReminders1.setTfaAffiliatePortfolio(tfaAffiliatePortfolioVal);
					portfolioReminders1.setSubstituteTeacher(substituteTeacherVal);
					
					if(coverLetter!=null)
						portfolioReminders1.setCoverLetter(Integer.parseInt(coverLetter));
					if(academicTranscript!=null)
						portfolioReminders1.setAcademicTranscript(Integer.parseInt(academicTranscript));
					if(certification!=null)
						portfolioReminders1.setCertification(Integer.parseInt(certification));
					if(proofOfCertification!=null)
						portfolioReminders1.setProofOfCertification(Integer.parseInt(proofOfCertification));
					if(referenceLettersOfRecommendation!=null)
						portfolioReminders1.setReferenceLettersOfRecommendation(Integer.parseInt(referenceLettersOfRecommendation));
					if(tfaAffiliate!=null)
						portfolioReminders1.setTfaAffiliate(Integer.parseInt(tfaAffiliate));
					if(willingAsSubstituteTeacher!=null)
						portfolioReminders1.setWillingAsSubstituteTeacher(Integer.parseInt(willingAsSubstituteTeacher));
					if(candidateType!=null)
						portfolioReminders1.setCandidateType(Integer.parseInt(candidateType));
					if(expCertTeacherTraining!=null)
						portfolioReminders1.setExpCertTeacherTraining(Integer.parseInt(expCertTeacherTraining));
					if(nationalBoardCert!=null)
						portfolioReminders1.setNationalBoardCert(Integer.parseInt(nationalBoardCert));
					if(affidavit!=null)
						portfolioReminders1.setAffidavit(Integer.parseInt(affidavit));
					if(employment!=null)
						portfolioReminders1.setEmployment(Integer.parseInt(employment));
					if(formeremployee!=null)
						portfolioReminders1.setFormeremployee(Integer.parseInt(formeremployee));
					if(generalKnowledgeExam!=null)
						portfolioReminders1.setGeneralKnowledgeExam(Integer.parseInt(generalKnowledgeExam));
					if(subjectAreaExam!=null)
						portfolioReminders1.setSubjectAreaExam(Integer.parseInt(subjectAreaExam));
					if(involvement!=null)
						portfolioReminders1.setInvolvement(Integer.parseInt(involvement));
					if(academicDspq!=null)
						portfolioReminders1.setAcademicDspq(Integer.parseInt(academicDspq));
					if(referenceDspq!=null)
						portfolioReminders1.setReferenceDspq(Integer.parseInt(referenceDspq));
					portfolioRemindersDAO.makePersistent(portfolioReminders1);
					
					/* Delete Questions ID */
					List<PortfolioRemindersDspq> portfolioRemindersDspqList = portfolioRemindersDspqDAO.getRecordByDistrict(districtMaster);
					try{
						if(portfolioRemindersDspqList.size()>0){
							for (PortfolioRemindersDspq portfolioRemindersDspq : portfolioRemindersDspqList) {
								txOpen =statelesSsession.beginTransaction();
								statelesSsession.delete(portfolioRemindersDspq);
								txOpen.commit();
							}
						}
					}catch(Exception exception){
						exception.printStackTrace();
					}
					
					/*	Save Question ids */
					
					String arr[]=dspQuestionArray.split(",");
					List dspqQuestionList = new ArrayList();
					if(dspQuestionArray!=null && !dspQuestionArray.equals("")){
						for(int i=0;i<arr.length;i++)
						{
							dspqQuestionList.add(Integer.parseInt(arr[i]));	
						}
						System.out.println("::::::::::::::: dspqQuestionList :::::::::::::::::::"+dspqQuestionList.size());
						List<DistrictSpecificPortfolioQuestions> dspqList = districtSpecificPortfolioQuestionsDAO.getQuestionList(dspqQuestionList);
						try{
							for(DistrictSpecificPortfolioQuestions dspq : dspqList){
								DistrictSpecificPortfolioQuestions dspqQuestions = districtSpecificPortfolioQuestionsDAO.findById(dspq.getQuestionId(), false, false);
								PortfolioRemindersDspq portfolioRemindersDspq = new PortfolioRemindersDspq();
								portfolioRemindersDspq.setDistrictSpecificPortfolioQuestions(dspqQuestions);
								portfolioRemindersDspq.setDistrictMaster(districtMaster);
								portfolioRemindersDspqDAO.makePersistent(portfolioRemindersDspq);
							}
						} catch(Exception exception){
							exception.printStackTrace();
						}
					}
				}

			} catch(Exception exception){
				exception.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status.toString();
		//return districtSpecificQuestion;
	}
	
	public boolean sendPortfolioReminderMailToSelectedUser(Integer[] teacherIds, Integer districtId) {

	      	System.out.println("SendPortfolioReminderMailToSelectedUser called");
			UserMaster userMaster 			= 	null;
			DistrictMaster districtMaster 	= 	null;
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session1 = request.getSession(false);
			if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}

			if (session1 == null || session1.getAttribute("userMaster") == null) {

			} else {

				userMaster=(UserMaster)session1.getAttribute("userMaster");

				if(userMaster.getDistrictId()!=null){

					//districtMaster=userMaster.getDistrictId();

				}
			}
			
			districtMaster = districtMasterDAO.findById(districtId, false, false); 
			
			/*	URL */
			String path = request.getContextPath();
			int portNo = request.getServerPort();
			String showPortNo = null;
			if(portNo==80)
			{
				showPortNo="";
			}
			else
			{
				showPortNo="";
			}

			String baseUrl = request.getScheme()+"://"+request.getServerName()+""+showPortNo+path+"/";

			String urlString		=	null;
			String mainURLEncode	=	null;
			String mailContent		=	null;
			String mailToReference =    "";
			
			if(userMaster.getDistrictId()!=null){

				districtMaster=userMaster.getDistrictId();

			}

			int disId   = districtMaster.getDistrictId();
			//int jbId    = 3159;
			String dId	=	null;
			String tId  =   null;
			//String jId	=	null;
			String messageSubject= "Update Required: Your Application With "+districtMaster.getDistrictName()+"";
			
			JobOrder   jobOrder =null;

			try{
				List<TeacherDetail> teacherDetailList=new ArrayList<TeacherDetail>();
				if(teacherIds.length>0){
					teacherDetailList=teacherDetailDAO.getTeachersHQL(teacherIds);
				}
				
				SessionFactory factory = jobForTeacherDAO.getSessionFactory();
			    StatelessSession statelessSession=factory.openStatelessSession();
			    Transaction transaction = statelessSession.beginTransaction();
				
				if(teacherDetailList.size()>0){
					for(TeacherDetail teacherDetail:teacherDetailList){
						
						if(teacherDetail!=null)
							mailToReference	= 	teacherDetail.getEmailAddress();
							String to 		= 	teacherDetail.getEmailAddress();
							dId				=	Utility.encryptNo(disId);
							tId				=	Utility.encryptNo(teacherDetail.getTeacherId());
							
							urlString 		= 	"dId="+dId+"&tId="+tId+"";
							mainURLEncode	=	baseUrl+"personalinforeminder.do?p="+Utility.encodeInBase64(urlString);
							mailContent = MailText.PortfolioReminderMailByLink(teacherDetail, districtMaster, mainURLEncode);
							
							System.out.println(messageSubject);
							System.out.println(mailContent);
							emailerService.sendMailAsHTMLText(mailToReference,messageSubject, mailContent);
							
							MessageToTeacher messageToTeacher = new MessageToTeacher();
							messageToTeacher.setTeacherId(teacherDetail);
							messageToTeacher.setJobId(jobOrder);
							messageToTeacher.setTeacherEmailAddress(to);
							messageToTeacher.setMessageSubject(messageSubject);
							messageToTeacher.setMessageSend(mailContent);
							messageToTeacher.setSenderId(userMaster);
							messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
							messageToTeacher.setEntityType(userMaster.getEntityType());
							messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
							messageToTeacher.setCreatedDateTime(new Date());
							transaction = statelessSession.beginTransaction();
							statelessSession.insert(messageToTeacher);
							transaction.commit();
					}
			}
			}catch (Exception e) {
				e.printStackTrace();
			}
			return true;
	}

	public String sendPortfolioReminderMailToAllUser(String firstName,String lastName, String emailAddress,String regionId,String degreeId,String universityId,String normScoreSelectVal,String qualQues,String 
			normScoreVal,String CGPA,String CGPASelectVal,String candidateStatusStr,String stateId,String internalCandidate,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String districtId,String schoolId,String fromDate,String toDate,String certType,String references,String resume,String fitScoreSelectVal,String YOTESelectVal,String AScoreSelectVal,String LRScoreSelectVal,String canServeAsSubTeacherYes,String canServeAsSubTeacherNo,String canServeAsSubTeacherDA,String TFAA,String TFAC,String TFAN,String fitScoreVal,String YOTE,String AScore,String LRScore,String jobAppliedFromDate,String jobAppliedToDate,int daysVal,int [] subjects,boolean newcandidatesonly,String teacherSSN,String stateForcandidate,String certTypeForCandidate,String empNmbr,String jobCategoryIds,String tagsId,String stateId2,String zipCode,Integer[] certIds,String epiFromDate,String epiToDate)
	{
	      /* ========  For Session time Out Error =========*/
	      WebContext context;
	      context = WebContextFactory.get();
	      HttpServletRequest request = context.getHttpServletRequest();
	      HttpSession session = request.getSession(false);
	      if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	      {
	            throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	      }
	      //-- get no of record in grid,
	      //-- set start and end position

	      int noOfRowInPage = Integer.parseInt(noOfRow);
	      int pgNo = Integer.parseInt(pageNo);
	      int start = ((pgNo-1)*noOfRowInPage);
	      int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
	      int totalRecord = 0;
	      //------------------------------------
	      //StringBuffer tmRecords =    new StringBuffer();
	      CandidateGridService cgService=new CandidateGridService();
	      String fileName = null;
	      try{
	    	  boolean noEPIFlag=false;
	            List<TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();

	            /** set default sorting fieldName **/
	            String sortOrderFieldName     =     "lastName";
	            String sortOrderNoField       =     "lastName";

	            /**Start set dynamic sorting fieldName **/
	            Order  sortOrderStrVal=null;

	            if(sortOrder!=null){
	                  if(!sortOrder.equals("") && !sortOrder.equals(null)){
	                        sortOrderFieldName=sortOrder;
	                        sortOrderNoField=sortOrder;
	                  }
	            }

	            String sortOrderTypeVal="0";
	            if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
	                  if(sortOrderType.equals("0")){
	                        sortOrderStrVal=Order.asc(sortOrderFieldName);
	                  }else{
	                        sortOrderTypeVal="1";
	                        sortOrderStrVal=Order.desc(sortOrderFieldName);
	                  }
	            }else{
	                  sortOrderTypeVal="0";
	                  sortOrderStrVal=Order.asc(sortOrderFieldName);
	            }
	            sortOrderStrVal=Order.asc("lastName");
	            /**End ------------------------------------**/
	            int roleId=0;
	            int entityID=0;
	            boolean isMiami = false;
	            DistrictMaster districtMaster=null;
	            SchoolMaster schoolMaster=null;
	            UserMaster userMaster=null;
	            int entityTypeId=1;
	            int utype = 1;
	            boolean writePrivilegeToSchool =false;
	            boolean isManage = true;
	            if (session == null || session.getAttribute("userMaster") == null) {
	                  //return "false";
	            }else{
	                  userMaster=(UserMaster)session.getAttribute("userMaster");
	                  if(userMaster.getRoleId().getRoleId()!=null){
	                        roleId=userMaster.getRoleId().getRoleId();
	                  }
	                  if(userMaster.getDistrictId()!=null){
	                        districtMaster=userMaster.getDistrictId();
	                        if(districtMaster.getNoEPI()==null ||(districtMaster.getNoEPI()!=null && districtMaster.getNoEPI()==false)){
	                        	noEPIFlag=true;
	                        }
	                        if(districtMaster.getDistrictId().equals(1200390))
	                              isMiami = true;
	                        writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
	                  }
	                  if(userMaster.getSchoolId()!=null){
	                        schoolMaster =userMaster.getSchoolId();
	                  }     
	                  entityID=userMaster.getEntityType();
	                  utype = entityID;
	            }
	            if(entityID==1){
	            	noEPIFlag=true;
	            }
	            if(isMiami && entityID==3)
	                  isManage=false;

	            boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
	            if(entityID==1)
	            {
	                  achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
	            }else
	            {
	                  try{
	                        DistrictMaster districtMasterObj=districtMaster;
	                        if(districtMasterObj.getDisplayAchievementScore()){
	                              achievementScore=true;
	                        }
	                        if(districtMasterObj.getDisplayTFA()){
	                              tFA=true;
	                        }
	                        if(districtMasterObj.getDisplayDemoClass()){
	                              demoClass=true;
	                        }
	                        if(districtMasterObj.getDisplayJSI()){
	                              JSI=true;
	                        }
	                        if(districtMasterObj.getDisplayYearsTeaching()){
	                              teachingOfYear=true;
	                        }
	                        if(districtMasterObj.getDisplayExpectedSalary()){
	                              expectedSalary=true;
	                        }
	                        if(districtMasterObj.getDisplayFitScore()){
	                              fitScore=true;
	                        }
	                  }catch(Exception e){ e.printStackTrace();}
	            }

	            String roleAccess=null;
	            try{
	                  roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,43,"teacherinfo.do",1);
	            }catch(Exception e){
	                  e.printStackTrace();
	            }
	            Date fDate=null;
	            Date tDate=null;
	            Date appliedfDate=null;
	            Date appliedtDate=null;
	            boolean applieddateFlag=false;
	            boolean dateFlag=false;
	            try{
	                  if(!fromDate.equals("")){
	                        fDate=Utility.getCurrentDateFormart(fromDate);
	                        dateFlag=true;
	                  }
	                  if(!toDate.equals("")){
	                        Calendar cal2 = Calendar.getInstance();
	                        cal2.setTime(Utility.getCurrentDateFormart(toDate));
	                        cal2.add(Calendar.HOUR,23);
	                        cal2.add(Calendar.MINUTE,59);
	                        cal2.add(Calendar.SECOND,59);
	                        tDate=cal2.getTime();
	                        dateFlag=true;
	                  }

	                  // job Applied Filter
	                  if(jobAppliedFromDate!=null && !jobAppliedFromDate.equals("")){
	                        appliedfDate=Utility.getCurrentDateFormart(jobAppliedFromDate);
	                        applieddateFlag=true;
	                  }
	                  if(jobAppliedToDate!=null && !jobAppliedToDate.equals("")){
	                        Calendar cal2 = Calendar.getInstance();
	                        cal2.setTime(Utility.getCurrentDateFormart(jobAppliedToDate));
	                        cal2.add(Calendar.HOUR,23);
	                        cal2.add(Calendar.MINUTE,59);
	                        cal2.add(Calendar.SECOND,59);
	                        appliedtDate=cal2.getTime();
	                        applieddateFlag=true;
	                  }
	            }catch(Exception e){
	                  e.printStackTrace();
	            }
	            List<JobOrder> certJobOrderList       =   new ArrayList<JobOrder>();
	            List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();

	            boolean certTypeFlag=false;
	            StateMaster stateMaster=null;
	            List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
	            //System.out.println("certType::::: "+certType);
	            if(!certType.equals("0") && !certType.equals("")||!stateId.equals("0")){
	                  certTypeFlag=true;
	                  if(!stateId.equals("0")){
	                        stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
	                  }
	                  certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
	                  //System.out.println("certificateTypeMasterList:: "+certificateTypeMasterList.size());
	                  if(certificateTypeMasterList.size()>0)
	                        certJobOrderList=jobCertificationDAO.findCertificationByJobWithState(certificateTypeMasterList);
	                  //System.out.println("certJobOrderList:: "+certJobOrderList.size());
	                  if(certJobOrderList.size()>0){
	                        for(JobOrder job: certJobOrderList){
	                              jobOrderIds.add(job);
	                        }
	                  }
	                  if(jobOrderIds.size()==0){
	                        entityTypeId=0;
	                  }
	            }else if(certType.equals("")){
	                  entityTypeId=0;
	                  certTypeFlag=true;
	            }
	            
	            
	            List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
	            List<TeacherDetail> NbyATeacherList = new ArrayList<TeacherDetail>();
	            //List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
	            boolean teacherFlag=false;
	            
	            ////////////////////////////////// Resume //////////////////////////////////////////
	            List<TeacherDetail> nolstTeacherDetailAll= new ArrayList<TeacherDetail>();
	            List<TeacherDetail> resumeTeacherList = new ArrayList<TeacherDetail>();
	            boolean resumeFlag=false;
	            if(resume!=null && !resume.equals("") && !resume.equals("0")){
	                  resumeFlag=true;
	                  resumeTeacherList=teacherExperienceDAO.findTeachersByResume(true);
	            }
	      
	            if(resumeFlag && resumeTeacherList.size()>0){
	                  
	                  if(teacherFlag && resume.equals("1"))
	                  {
	                        filterTeacherList.retainAll(resumeTeacherList);
	                        teacherFlag=true;
	                  }
	                  else if(teacherFlag== false && resume.equals("1")){
	                        {
	                              filterTeacherList.addAll(resumeTeacherList);
	                              teacherFlag=true;
	                        }
	                  }else if(resume.equals("2"))
	                  {
	                        nolstTeacherDetailAll.addAll(resumeTeacherList);
	                        teacherFlag=false;
	                  }
	                  
	            }
	            
	            List<TeacherDetail> degreeTeacherDetailList = new ArrayList<TeacherDetail>();
	            boolean degreeIdFlag=false;
	            if(degreeId!=null && !degreeId.equals("") && !degreeId.equals("0")){
	                  degreeIdFlag=true;
	                  DegreeMaster degreeMaster=degreeMasterDAO.findById(Long.valueOf(degreeId), false,false);
	                  degreeTeacherDetailList=teacherAcademicsDAO.findTeacherListByDegree(degreeMaster);
	            }
	            if(degreeId.equals("")){
	                  degreeIdFlag=true;
	            }
	            if(degreeIdFlag && degreeTeacherDetailList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(degreeTeacherDetailList);
	                  }else{
	                        filterTeacherList.addAll(degreeTeacherDetailList);
	                  }
	            }
	            if(degreeIdFlag){
	                  teacherFlag=true;
	            }
	            if(!degreeTeacherDetailList.isEmpty())//clear List
	            degreeTeacherDetailList.clear();
	            
	            List<TeacherDetail> universityTeacherList = new ArrayList<TeacherDetail>();
	            boolean universityFlag=false;
	            if(universityId!=null && !universityId.equals("") && !universityId.equals("0")){
	                  universityFlag=true;
	                  UniversityMaster universityMaster=universityMasterDAO.findById(Integer.valueOf(universityId), false,false);
	                  universityTeacherList=teacherAcademicsDAO.findTeacherListByUniversity(universityMaster);
	            }
	            if(universityId.equals("")){
	                  universityFlag=true;
	            }
	            if(universityFlag && universityTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(universityTeacherList);
	                  }else{
	                        filterTeacherList.addAll(universityTeacherList);
	                  }
	            }
	            if(!universityTeacherList.isEmpty())//clear list
	            universityTeacherList.clear();
	            if(universityFlag){
	                  teacherFlag=true;
	            }
	//****************************tags filter*************************************************
	            List<TeacherDetail> tagsTeacherList = new ArrayList<TeacherDetail>();
	            boolean tagsFlag=false;
	            if(tagsId!=null && !tagsId.equals("") && !tagsId.equals("0") && entityID!=1){
	                  tagsFlag=true;
	                  SecondaryStatusMaster secondaryStatusMaster= secondaryStatusMasterDAO.findById(Integer.valueOf(tagsId), false,false);
	                  tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatus(secondaryStatusMaster,districtMaster);
	            }
	            
	            if(tagsFlag && tagsTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(tagsTeacherList);
	                  }else{
	                        filterTeacherList.addAll(tagsTeacherList);
	                  }
	            }
	            if(!tagsTeacherList.isEmpty())//clear List
	            tagsTeacherList.clear();
	            if(tagsFlag){
	                  teacherFlag=true;
	            }
	//**************************** End *************************************************

	            //multiple certificate filter
				boolean mCertFlag=false;
				List<Integer> certTypeIdList = new ArrayList<Integer>();
				List<Integer> teacherIdList = new ArrayList<Integer>();
				List<TeacherDetail> mCertTeacherDetailList = new ArrayList<TeacherDetail>();
				if(certIds!=null && !certIds.equals("") && certIds.length>0){
					for(int i=0;i<certIds.length;i++)
					{
						certTypeIdList.add(certIds[i]);
					}
					//System.out.println("================certTypeIdList==============="+certTypeIdList);
					teacherIdList = teacherCertificateDAO.findTeacherByCertificates(certTypeIdList);
					//System.out.println("================teacherIdList==============="+teacherIdList);
					if(teacherIdList!=null && !teacherIdList.equals("") && teacherIdList.size()>0){
						for(int i=0;i<teacherIdList.size();i++)
						{
							TeacherDetail teacherDetail = new TeacherDetail();
							teacherDetail.setTeacherId(teacherIdList.get(i));
							mCertTeacherDetailList.add(teacherDetail);
						}
					}
					mCertFlag=true;
				}
				if(mCertFlag && mCertTeacherDetailList.size()>0){
					filterTeacherList.addAll(mCertTeacherDetailList);
				}
				if(mCertFlag){
					teacherFlag=true;
				}
				
				//state filter
				List<TeacherDetail> stateTeacherDetailList = new ArrayList<TeacherDetail>();
				boolean stateFlag=false;
				if(stateId2!=null && !stateId2.equals("") && !stateId2.equals("0"))
				{
					stateFlag=true;
					StateMaster stateMaster1 = stateMasterDAO.findById(Long.valueOf(stateId2), false, false);
					List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByState(stateMaster1);
					for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
					{
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
						stateTeacherDetailList.add(teacherDetail);
					}
					System.out.println("stateId2=="+stateId2+",	stateFlag=="+stateFlag);
				}
				
				if(stateFlag && stateTeacherDetailList.size()>0)
				{
					if(teacherFlag)
						filterTeacherList.retainAll(stateTeacherDetailList);
					else
						filterTeacherList.addAll(stateTeacherDetailList);
				}
				if(stateFlag)
					teacherFlag=true;

				//zip code filter
				List<TeacherDetail> zipCodeTeacherdetaDetailList = new ArrayList<TeacherDetail>();
				boolean zipCodeFlag=false;
				if(zipCode!=null && !zipCode.equals("") && !zipCode.equals("0"))
				{
					zipCodeFlag=true;
					List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByZipCode(zipCode);
					for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
					{
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
						zipCodeTeacherdetaDetailList.add(teacherDetail);
					}
					System.out.println("zipCode=="+zipCode+",	zipCodeFlag=="+zipCodeFlag);
				}
				
				if(zipCodeFlag && zipCodeTeacherdetaDetailList.size()>0)
				{
					if(teacherFlag)
						filterTeacherList.retainAll(zipCodeTeacherdetaDetailList);
					else
						filterTeacherList.addAll(zipCodeTeacherdetaDetailList);
				}
				if(zipCodeFlag)
					teacherFlag=true;
				
				//epi date filter
				Date epifDate=null;
				Date epitDate=null;
				boolean epiDateFlag=false;
				List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
				try{
					if(!epiFromDate.equals("")){
						epifDate=Utility.getCurrentDateFormart(epiFromDate);
						epiDateFlag=true;
					}
					if(!epiToDate.equals("")){
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(Utility.getCurrentDateFormart(epiToDate));
						cal2.add(Calendar.HOUR,23);
						cal2.add(Calendar.MINUTE,59);
						cal2.add(Calendar.SECOND,59);
						epitDate=cal2.getTime();
						epiDateFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(epiDateFlag)
				{
					StatusMaster statusMaster = statusMasterDAO.findStatusByShortName("comp");
					List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getAssessmentCompletedDateTimeTeacherList(1, statusMaster, epifDate, epitDate);
					for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList)
					{
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(teacherAssessmentStatus.getTeacherDetail().getTeacherId());
						epiDateTeacherdetaDetailList.add(teacherDetail);
					}
					System.out.println("epifDate=="+epifDate+",	epitDate=="+epitDate+", epiDateFlag=="+epiDateFlag+", epiDateTeacherdetaDetailList.size()=="+epiDateTeacherdetaDetailList.size());
				}
				if(epiDateFlag && epiDateTeacherdetaDetailList.size()>0)
				{
					if(teacherFlag)
						filterTeacherList.retainAll(epiDateTeacherdetaDetailList);
					else
						filterTeacherList.addAll(epiDateTeacherdetaDetailList);
				}
				if(epiDateFlag)
					teacherFlag=true;
				
				//****************************Thumb filter by Ravindra*************************************************
				Map<Integer,TeacherDetail> qmap = new HashMap<Integer, TeacherDetail>();
				List<TeacherDetail> qqAttemptedTeachers1=null;
				List<TeacherDetail> invalidQQTeachers1 =null;
				List<TeacherDetail> blueThumbTecaherList=null;
				List<TeacherDetail> trueList=new ArrayList<TeacherDetail>();
				List<TeacherDetail> falseList=new ArrayList<TeacherDetail>();
				boolean qualQuesFlag=false;
				if(!qualQues.equals("")){
					if(!qualQues.equals("1")){	
						System.out.println("---->filterTeacherList "+filterTeacherList.size());
						invalidQQTeachers1 = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findQQStatusByTeacher(districtMaster,false);
						blueThumbTecaherList = jobForTeacherDAO.getAllQQFinalizeTeacherList(districtMaster);
						if(qualQues.equals("2"))
						{
							qqAttemptedTeachers1 =  teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findQQStatusByTeacher(districtMaster,true);
							for (TeacherDetail teacherDetail : qqAttemptedTeachers1)
								qmap.put(teacherDetail.getTeacherId(), teacherDetail);
							
							for (TeacherDetail teacherDetail : invalidQQTeachers1)
								qmap.remove(teacherDetail.getTeacherId());
							
							Set<Integer> set=qmap.keySet();
							List<TeacherDetail> validList=new ArrayList<TeacherDetail>();
							for(Integer i:set)
								validList.add(qmap.get(i)); 
							
							qualQuesFlag=true;
							if(teacherFlag){
								filterTeacherList.retainAll(validList);
							}else{
								filterTeacherList.addAll(validList);
							}
						}
						else if(qualQues.equals("3"))
						{
							for (TeacherDetail teacherDetail : invalidQQTeachers1)
								 qmap.put(teacherDetail.getTeacherId(),teacherDetail);
							
							Set<Integer> set1=qmap.keySet();
							List<TeacherDetail> invalidList=new ArrayList<TeacherDetail>();
							for(Integer i:set1)
								invalidList.add(qmap.get(i));
							
							invalidList.removeAll(blueThumbTecaherList);
							qualQuesFlag=true;
							if(teacherFlag){
								filterTeacherList.retainAll(invalidList);
							}else{
								filterTeacherList.addAll(invalidList);
							}
						}
						else if(qualQues.equals("4"))
						{
							qualQuesFlag=true;
							if(teacherFlag){
								filterTeacherList.retainAll(blueThumbTecaherList);
							}else{
								filterTeacherList.addAll(blueThumbTecaherList);
							}
						}
					}
				}
				System.out.println("------>> filterTeacherList "+filterTeacherList.size());
				
				if(qualQuesFlag){
					teacherFlag=true;
				} 
				//**************************** End Thumb filter*************************************************                 
	            
	            List<TeacherDetail> regionTeacherList = new ArrayList<TeacherDetail>();
	            boolean regionFlag=false;
	            if(regionId!=null && !regionId.equals("") && !regionId.equals("0")){
	                  regionFlag=true;
	                  regionTeacherList=teacherPreferenceDAO.findRegionByJobForTeacher(regionId);
	            }
	            if(regionId.equals("")){
	                  regionFlag=true;
	            }
	            if(regionFlag && regionTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(regionTeacherList);
	                  }else{
	                        filterTeacherList.addAll(regionTeacherList);
	                  }
	            }
	            if(regionFlag){
	                  teacherFlag=true;
	            }
	            /////////SSN//////////////////
	            boolean ssnFlag=false;
	            List<TeacherDetail> ssnTeacherList = new ArrayList<TeacherDetail>();
	            if(teacherSSN!=null && !teacherSSN.trim().equals(""))
	            {
	                  System.out.println("teacherSSN::: "+teacherSSN);
	                  teacherSSN = Utility.encodeInBase64(teacherSSN);
	                  System.out.println("teacherSSN decoded::: "+teacherSSN);
	                  ssnFlag = true;
	                  List<TeacherPersonalInfo> teacherPersonalInfosSSN = new ArrayList<TeacherPersonalInfo>();
	                  teacherPersonalInfosSSN = teacherPersonalInfoDAO.checkTeachersBySSN(teacherSSN);
	                  for (TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfosSSN) {
	                        TeacherDetail td = new TeacherDetail();
	                        td.setTeacherId(teacherPersonalInfo.getTeacherId());
	                        ssnTeacherList.add(td);
	                  }
	            }
	            if(ssnFlag && ssnTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(ssnTeacherList);
	                  }else{
	                        filterTeacherList.addAll(ssnTeacherList);
	                  }
	            }
	            if(ssnFlag){
	                  teacherFlag=true;
	            }
	            /////////////////////////
	            TestTool.getTraceSecond("2");
	            ////////////// Employee Number  ///////////////////////////////
	            try{
	                  boolean empNmbrFlag = false;
	                  List<TeacherDetail> empNmbrThrList = new ArrayList<TeacherDetail>();
	                  if(empNmbr!=null && !empNmbr.trim().equals(""))
	                  {
	                        System.out.println("empNmbr::: "+empNmbr);
	                        //teacherSSN = Utility.encodeInBase64(teacherSSN);
	                        //System.out.println("teacherSSN decoded::: "+teacherSSN);
	                        empNmbrFlag = true;
	                        List<TeacherPersonalInfo> teacherPerInfoEmpNbr = new ArrayList<TeacherPersonalInfo>();
	                        teacherPerInfoEmpNbr = teacherPersonalInfoDAO.checkTeachersByEmpNmbr(empNmbr);
	                        for (TeacherPersonalInfo teacherPersonalInfo : teacherPerInfoEmpNbr) {
	                              TeacherDetail td = new TeacherDetail();
	                              td.setTeacherId(teacherPersonalInfo.getTeacherId());
	                              empNmbrThrList.add(td);
	                        }
	                  }
	                  if(empNmbrFlag && empNmbrThrList.size()>0){
	                        if(teacherFlag){
	                              filterTeacherList.retainAll(empNmbrThrList);
	                        }else{
	                              filterTeacherList.addAll(empNmbrThrList);
	                        }
	                  }
	                  if(empNmbrFlag){
	                        teacherFlag=true;
	                  }
	            }catch(Exception e)
	            {
	                  e.printStackTrace();
	            }
	            ///////////////////////////////////////////////
	            
	            boolean nByAflag=false;
	            List<TeacherDetail> cgpaTeacherList = new ArrayList<TeacherDetail>();
	            boolean cgpaFlag=false;
	            if(!CGPASelectVal.equals("") && !CGPASelectVal.equals("0") & !CGPASelectVal.equals("6")){
	                  cgpaFlag=true;
	                  cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA(CGPA,CGPASelectVal);
	            }
	            if(cgpaFlag && cgpaTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(cgpaTeacherList);
	                  }else{
	                        filterTeacherList.addAll(cgpaTeacherList);
	                  }
	            }
	            if(cgpaFlag){
	                  teacherFlag=true;
	            }
	            /// N/A changes
	            boolean CGPANa=false;
	            if(!CGPASelectVal.equals("") && CGPASelectVal.equals("6")){
	                  CGPANa=true;
	                  cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA("0","5");
	                  
	                  if(CGPANa && cgpaTeacherList.size()>0){
	                        if(nByAflag){
	                              NbyATeacherList.retainAll(cgpaTeacherList);
	                        }else{
	                              NbyATeacherList.addAll(cgpaTeacherList);
	                        }
	                  }
	            }
	            if(CGPANa)
	                  nByAflag=true;
	            ///////// //////
	            
	            List<TeacherDetail> normTeacherList = new ArrayList<TeacherDetail>();
	            boolean normFlag=false;

	            if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") & !normScoreSelectVal.equals("6")){
	                  normFlag=true;
	                  normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore(normScoreVal,normScoreSelectVal);
	            }
	            if(normFlag && normTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(normTeacherList);
	                  }else{
	                        filterTeacherList.addAll(normTeacherList);
	                  }
	            }
	            if(normFlag){
	                  teacherFlag=true;
	            }
	            /// N/A changes
	            boolean normNa=false;
	            if(!normScoreSelectVal.equals("") && normScoreSelectVal.equals("6")){
	                  normNa=true;
	                  normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore("0","5");
	                  
	                  if(normNa && normTeacherList.size()>0){
	                        if(nByAflag){
	                              NbyATeacherList.retainAll(normTeacherList);
	                        }else{
	                              NbyATeacherList.addAll(normTeacherList);
	                        }
	                  }
	            }
	            if(normNa){
	                  nByAflag=true;
	            }
	            ///////// //////
	            /**** Add ***************/
	            TestTool.getTraceSecond("3");
	            List<TeacherDetail> referencesTeacherList = new ArrayList<TeacherDetail>();
	            boolean referFlag=false;
	            if(references!=null && !references.equals("") && !references.equals("0")){
	                  referFlag=true;
	                  referencesTeacherList=teacherElectronicReferencesDAO.findTeacherReferences();
	            }
	            if(references.equals("")){
	                  referFlag=true;
	            }
	            if(referFlag && referencesTeacherList.size()>0){
	                  if(teacherFlag){
	                        {
	                              filterTeacherList.retainAll(referencesTeacherList);
	                        }
	                  }else{
	                        if(references.equals("1"))
	                              filterTeacherList.addAll(referencesTeacherList);
	                        else
	                        {
	                              filterTeacherList.retainAll(referencesTeacherList);
	                        }
	                  }
	            }
	            if(referFlag){
	                  teacherFlag=true;
	            }
	            List<TeacherDetail> subjectTeacherList = new ArrayList<TeacherDetail>();
	            boolean subjectFlag=false;
	            if(subjects.length>0){
	                  subjectFlag=true;
	                  SubjectMaster subjectMaster = null;
	                  List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();

	                  for(Integer subjId:subjects)
	                  {
	                        subjectMaster = new SubjectMaster();
	                        subjectMaster.setSubjectId(subjId);
	                        subjectMasters.add(subjectMaster);
	                  }

	                  subjectTeacherList=jobForTeacherDAO.findTeachersBySubjectList(subjectMasters);
	            }
	            if(subjectFlag && subjectTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(subjectTeacherList);
	                  }else{
	                        filterTeacherList.addAll(subjectTeacherList);
	                  }
	            }
	            if(subjectFlag){
	                  teacherFlag=true;
	            }
	            
	            // Job category
				List<TeacherDetail> jobCateTeacherList = new ArrayList<TeacherDetail>();
				boolean jobCateFlag=false;
				if(!jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
				{
					List<Integer> jobcatIds = new ArrayList<Integer>();
					List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
					String jobCategoryIdStr[] =jobCategoryIds.split(",");
				  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
				  {		  
					for(String str : jobCategoryIdStr){
					 if(!str.equals(""))
						 jobcatIds.add(Integer.parseInt(str));
					 } 
					 if(jobcatIds.size()>0){
						 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,Integer.parseInt(districtId));
						 jobCateFlag=true;
					 }
					 if(listjJobOrders!=null && listjJobOrders.size()>0){
						 jobCateTeacherList = jobForTeacherDAO.getJobForTeacherByStatus(listjJobOrders);
						 if(jobCateFlag && jobCateTeacherList!=null && jobCateTeacherList.size()>0){
							if(teacherFlag){
								filterTeacherList.retainAll(jobCateTeacherList);
							}else{
								filterTeacherList.addAll(jobCateTeacherList);
							}
						 }
					 }
				  }
				}
				
				if(jobCateFlag){
					teacherFlag=true;
				}
				
	            List<TeacherDetail> expTeacherList = new ArrayList<TeacherDetail>();
	            boolean expFlag=false;
	            if(!YOTESelectVal.equals("") && !YOTESelectVal.equals("0") & !YOTESelectVal.equals("6")){
	                  expFlag=true;
	                  expTeacherList=teacherExperienceDAO.findTeachersByExperience(YOTE,YOTESelectVal);
	            }
	            if(expFlag && expTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(expTeacherList);
	                  }else{
	                        filterTeacherList.addAll(expTeacherList);
	                  }
	            }
	            if(expFlag){
	                  teacherFlag=true;
	            }
	            /// N/A changes
	            boolean YOTENa=false;
	            if(!YOTESelectVal.equals("") && YOTESelectVal.equals("6")){
	                  YOTENa=true;
	                  expTeacherList=teacherExperienceDAO.findTeachersByExperience("0","5");
	                  
	                  if(expTeacherList.size()>0){
	                        if(nByAflag){
	                              NbyATeacherList.retainAll(expTeacherList);
	                        }else{
	                              NbyATeacherList.addAll(expTeacherList);
	                        }
	                  }
	            }
	            if(YOTENa)
	                  nByAflag=true;
	            
	            List<TeacherDetail> acheivTeacherList = new ArrayList<TeacherDetail>();
	            boolean acheivFlag=false;
	            if(!AScoreSelectVal.equals("") && !AScoreSelectVal.equals("0") & !AScoreSelectVal.equals("6")){
	                  acheivFlag=true;
	                  acheivTeacherList=teacherAchievementScoreDAO.findTeachersByAscoreOrLScore(AScore,AScoreSelectVal,districtMaster,true);
	            }
	            if(acheivFlag && acheivTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(acheivTeacherList);
	                  }else{
	                        filterTeacherList.addAll(acheivTeacherList);
	                  }
	            }
	            if(acheivFlag){
	                  teacherFlag=true;
	            }
	            /// N/A changes
	            boolean acheivNa=false;
	            if(!CGPASelectVal.equals("") && CGPASelectVal.equals("6")){
	                  acheivNa=true;
	                  acheivTeacherList=teacherAchievementScoreDAO.findTeachersByAscoreOrLScore("0","5",districtMaster,true);
	                  
	                  if(cgpaTeacherList.size()>0){
	                        if(nByAflag){
	                              NbyATeacherList.retainAll(acheivTeacherList);
	                        }else{
	                              NbyATeacherList.addAll(acheivTeacherList);
	                        }
	                  }
	            }
	            if(acheivNa)
	                  nByAflag=true;
	            
	            List<TeacherDetail> LRScoreTeacherList = new ArrayList<TeacherDetail>();
	            boolean lRScoreFlag=false;
	            if(!LRScoreSelectVal.equals("") && !LRScoreSelectVal.equals("0") & !LRScoreSelectVal.equals("6") && districtMaster!=null){
	                  lRScoreFlag=true;
	                  LRScoreTeacherList=teacherAchievementScoreDAO.findTeachersByAscoreOrLScore(LRScore,LRScoreSelectVal,districtMaster,false);
	            }
	            if(lRScoreFlag && LRScoreTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(LRScoreTeacherList);
	                  }else{
	                        filterTeacherList.addAll(LRScoreTeacherList);
	                  }
	            }
	            if(lRScoreFlag){
	                  teacherFlag=true;
	            }

	            /// N/A changes
	            boolean LRScoreNa=false;
	            if(!CGPASelectVal.equals("") && CGPASelectVal.equals("6")){
	                  LRScoreNa=true;
	                  LRScoreTeacherList=teacherAchievementScoreDAO.findTeachersByAscoreOrLScore("0","5",districtMaster,false);
	                  
	                  if(LRScoreTeacherList.size()>0){
	                        if(nByAflag){
	                              NbyATeacherList.retainAll(LRScoreTeacherList);
	                        }else{
	                              NbyATeacherList.addAll(LRScoreTeacherList);
	                        }
	                  }
	            }
	            if(LRScoreNa)
	                  nByAflag=true;
	            List<TeacherDetail> substitudeTeacherList = new ArrayList<TeacherDetail>();
	            boolean substituteFlag=false;
	            if(!canServeAsSubTeacherYes.equals("false") || !canServeAsSubTeacherNo.equals("false") || !canServeAsSubTeacherDA.equals("false")){
	                  substituteFlag=true;
	                  substitudeTeacherList=teacherExperienceDAO.findTeachersBySubstitute(canServeAsSubTeacherYes,canServeAsSubTeacherNo,canServeAsSubTeacherDA);
	            }
	            if(substituteFlag && substitudeTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(substitudeTeacherList);
	                  }else{
	                        filterTeacherList.addAll(substitudeTeacherList);
	                  }
	            }
	            if(substituteFlag){
	                  teacherFlag=true;
	            }

	            /*
	             * TFAA 1
	                  TFAC 2
	                  TFAN 3
	             */
	            List<TeacherDetail> tfaTeacherList = new ArrayList<TeacherDetail>();
	            boolean tfaFlag=false;
	            if(!TFAA.equals("false") || !TFAC.equals("false") || !TFAN.equals("false")){
	                  tfaFlag=true;
	                  tfaTeacherList=teacherExperienceDAO.findTeachersByTFA(TFAA,TFAC,TFAN);
	            }
	            if(tfaFlag && tfaTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(tfaTeacherList);
	                  }else{
	                        filterTeacherList.addAll(tfaTeacherList);
	                  }
	            }
	            if(tfaFlag){
	                  teacherFlag=true;
	            }
	            TestTool.getTraceSecond("4");
	            /// Fit Score
	            List<TeacherDetail> fitscoreTeacherList = new ArrayList<TeacherDetail>();
	            boolean fitScoreFlag=false;
	            if(!fitScoreSelectVal.equals("") && !fitScoreSelectVal.equals("0") & !fitScoreSelectVal.equals("6")){
	                  fitScoreFlag=true;
	                  fitscoreTeacherList=jobWiseConsolidatedTeacherScoreDAO.findTeacherListByFitScore(fitScoreVal,fitScoreSelectVal);
	            }
	            if(fitScoreFlag && fitscoreTeacherList.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(fitscoreTeacherList);
	                  }else{
	                        filterTeacherList.addAll(fitscoreTeacherList);
	                  }
	            }
	            if(fitScoreFlag){
	                  teacherFlag=true;
	            }
	            
	            // Start /////////////////////////////// candidate list By certificate ///////////////////////////////////////
	            StateMaster stateMasterForCandidate=null;
	            List<CertificateTypeMaster> certTypeMasterListForCndt =new ArrayList<CertificateTypeMaster>();
	            List<TeacherDetail> tListByCertificateTypeId = new ArrayList<TeacherDetail>();
	            boolean tdataByCertTypeIdFlag = false;
	            
	            if(!certTypeForCandidate.equals("0") && !certTypeForCandidate.equals("")||!stateForcandidate.equals("0")){
	                  if(!stateForcandidate.equals("0")){
	                        stateMasterForCandidate=stateMasterDAO.findById(Long.valueOf(stateForcandidate), false,false);
	                  }
	                  certTypeMasterListForCndt=certificateTypeMasterDAO.findCertificationByJob(stateMasterForCandidate,certTypeForCandidate);
	                  tListByCertificateTypeId = teacherCertificateDAO.findCertificateByJobForTeacher(certTypeMasterListForCndt);
	                  tdataByCertTypeIdFlag=true;
	            }
	            if(tdataByCertTypeIdFlag && tListByCertificateTypeId.size()>0){
	                  if(teacherFlag){
	                        filterTeacherList.retainAll(tListByCertificateTypeId);
	                  }else{
	                        filterTeacherList.addAll(tListByCertificateTypeId);
	                  }
	            }
	            if(tdataByCertTypeIdFlag){
	                  teacherFlag=true;
	            }
	            // End /////////////////////////////// candidate list By certificate ///////////////////////////////////////
	            
	            
	            // N/A changes
	            boolean fitScoreNa=false;
	            if(!fitScoreSelectVal.equals("") && fitScoreSelectVal.equals("6")){
	                  fitScoreNa=true;
	                  fitscoreTeacherList=jobWiseConsolidatedTeacherScoreDAO.findTeacherListByFitScore("0","5");
	                  if(fitScoreNa && fitscoreTeacherList.size()>0){
	                        if(nByAflag){
	                              NbyATeacherList.retainAll(fitscoreTeacherList);
	                        }else{
	                              NbyATeacherList.addAll(fitscoreTeacherList);
	                        }
	                  }
	            }
	            if(fitScoreNa)
	                  nByAflag=true;
	            
	            /************* End **************/
	            
	            /*	
				 * 
				 * Multi Selection Status
				 * 
				 * */
				boolean multiSelectFlag = false; 
				String candidateStatus = "";
				try{
					if(candidateStatusStr!=null && !candidateStatusStr.equals("") && !candidateStatusStr.equals("0")){
						String candidateStatusString[] = candidateStatusStr.split("#%#");
						if(candidateStatusString.length==1){
							if(!ArrayUtils.contains(candidateStatusString, "0")){
								for(String strStatus1 : candidateStatusString){
									candidateStatus = strStatus1;
								}
							}
						}else if(candidateStatusString.length>1){
							if(!ArrayUtils.contains(candidateStatusString, "0")){
								multiSelectFlag = true;
							}
						}
					}
				}catch(Exception exception){
					exception.printStackTrace();
				}
				/* End Multi Selection */
	            
	            
	            StatusMaster statusMaster = new StatusMaster();
	            boolean status=false;
	            if(!candidateStatus.equals("") && !candidateStatus.equals("0")){
	                  statusMaster= statusMasterDAO.findStatusByShortName(candidateStatus);
	                  if(candidateStatus.equals("hird")|| candidateStatus.equals("rem") || candidateStatus.equals("widrw")){
	                        status=true;
	                  }else{
	                        List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
	                        try{
	                              if(candidateStatus.equals("icomp")){
	                                    String[] statuss = {"comp","vlt"};
	                                    lstStatusMasters = statusMasterDAO.findStatusByShortNames(statuss);
	                              }else{
	                                    String[] statuss = {candidateStatus};
	                                    lstStatusMasters = statusMasterDAO.findStatusByShortNames(statuss);
	                              }
	                        }catch (Exception e) {
	                              e.printStackTrace();
	                        }
	                        List<TeacherDetail> teacherStatusIcompList= new ArrayList<TeacherDetail>();
	                        List<TeacherDetail> teacherStatusList = new ArrayList<TeacherDetail>();
	                        if(candidateStatus.equals("icomp")){
	                              teacherStatusIcompList=teacherAssessmentStatusDAO.findBaseStatusByTeachers(lstStatusMasters);
	                              List teacherIds = new ArrayList();
	                              for(int e=0; e<teacherStatusIcompList.size(); e++) {
	                                    teacherIds.add(teacherStatusIcompList.get(e).getTeacherId());
	                              }
	                              teacherStatusList=teacherDetailDAO.findIcompTeacherDetails(teacherIds);
	                        }else{
	                              teacherStatusList=teacherAssessmentStatusDAO.findBaseStatusByTeachers(lstStatusMasters);
	                        }

	                        if(teacherStatusList.size()>0){
	                              if(teacherFlag){
	                                    filterTeacherList.retainAll(teacherStatusList);
	                              }else{
	                                    filterTeacherList.addAll(teacherStatusList);
	                              }
	                              teacherFlag=true;
	                        }
	                  }
	            }  else if(multiSelectFlag == true) {
					List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
					List<TeacherDetail> teacherStatusList = new ArrayList<TeacherDetail>();
					String statuss[] = candidateStatusStr.split("#%#");
					lstStatusMasters = statusMasterDAO.findStatusByShortNames(statuss);
					teacherStatusList= teacherAssessmentStatusDAO.findBaseStatusByTeachers(lstStatusMasters);
					if(teacherStatusList.size()>0){
						if(teacherFlag){
							filterTeacherList.retainAll(teacherStatusList);
						}else{
							filterTeacherList.addAll(teacherStatusList);
						}
						teacherFlag=true;
					}
	            }

	            int internalCandVal=0;
	            if(internalCandidate.equalsIgnoreCase("true")){
	                  internalCandVal=1;
	            }
	            SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
	            SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
	            SortedMap<Integer,Integer> jftMap1 = new TreeMap<Integer,Integer>();
	            SortedMap<Integer,Integer> jftMapForTm1 = new TreeMap<Integer,Integer>();
	            List<JobForTeacher> lstJobForTeacher =    new ArrayList<JobForTeacher>();
	            List<JobOrder> lstJobOrderList =new ArrayList<JobOrder>();
	            List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
	            if(schoolId.equals("") || districtId.equals("")){
	                  entityTypeId=0;
	            }
	            SchoolMaster sm = schoolMaster;
	            List<TeacherDetail> lstTeacherDetailAll= new ArrayList<TeacherDetail>();
	            if(entityTypeId!=0){
	                  if(entityID==3){
	                        // Nobal check
	                        if(!schoolId.equals("0") && writePrivilegeToSchool)
	                              schoolMaster.setSchoolId(Long.parseLong(schoolId));

	                        lstJobOrderSLList =schoolInJobOrderDAO.allJobOrderPostedBySchool(schoolMaster);
	                        entityTypeId=3;
	                  }
	                  if(!schoolId.equals("0") && entityID!=3){
	                        SchoolMaster smaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
	                        sm = smaster;
	                        lstJobOrderSLList =schoolInJobOrderDAO.allJobOrderPostedBySchool(smaster);
	                        entityTypeId=3;
	                  }
	                  if(entityTypeId==3 && lstJobOrderSLList.size()>0 && certTypeFlag){
	                        lstJobOrderList.addAll(getFilterJobOrder(lstJobOrderSLList,jobOrderIds));
	                  }else if(entityTypeId!=3 && lstJobOrderSLList.size()==0 && certTypeFlag){
	                        lstJobOrderList.addAll(jobOrderIds);
	                        entityTypeId=3;
	                  }else{
	                        lstJobOrderList.addAll(lstJobOrderSLList);
	                  }
	                  if(entityID==2){
	                        entityTypeId=2;
	                  }
	                  if(!districtId.equals("0")  && entityID==1){
	                        districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId),false,false);
	                        entityTypeId=2;
	                  }
	                  if(!districtId.equals("0") && (!schoolId.equals("0")|| certTypeFlag) && entityID!=3){
	                        entityTypeId=4;
	                  }
	                  TestTool.getTraceSecond("5");
	                  //filter.......
	                  //if(newcandidatesonly && (appliedfDate!=null || appliedtDate!=null))
	                  if(newcandidatesonly){
	                        lstTeacherDetailAll=jobForTeacherDAO.getTeacherDetailByDASARecordsBeforeDate(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,newcandidatesonly,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds);
	                  }
	                  if(resume.equals("2"))
	                        lstTeacherDetailAll.addAll(nolstTeacherDetailAll);
	                  TestTool.getTraceSecond("6");
	                  String stq="";
	                  if(districtMaster!=null  && utype==3)
	                  {
	                        SecondaryStatus smaster = districtMaster.getSecondaryStatus();
	                        if(smaster!=null)
	                        {
	                              String secStatus = smaster.getSecondaryStatusName();
	                              if(secStatus!=null && !secStatus.equals(""))
	                              {
	                                    //
	                                     List<SecondaryStatus> lstStatus = new ArrayList<SecondaryStatus>();
	                                     lstStatus = secondaryStatusDAO.findSecondaryStatusByName(districtMaster,secStatus);
	                                     System.out.println("lstStatus.size():::: "+lstStatus.size());
	                                     for (SecondaryStatus secondaryStatus : lstStatus) {
	                                           if(secondaryStatus.getJobCategoryMaster()!=null)
	                                           {
	                                                 stq+=" IF(secondarys3_.jobCategoryId="+secondaryStatus.getJobCategoryMaster().getJobCategoryId()+", ( secondaryStatusId >="+secondaryStatus.getSecondaryStatusId()+" ),false) or";
	                                           }
	                                    }
	                                     if(stq.length()>3)
	                                     {
	                                           stq = stq.substring(0, stq.length()-2);
	                                     }
	                              }
	                        }
	                  }
	                  TestTool.getTraceSecond("7");
	                  if(entityID==1 && entityTypeId==1 && dateFlag==false && applieddateFlag==false && status==false && internalCandVal==0){
	                        //>>>>>>>>>>>>>>>>>>>>> Job Applied change >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	                        lstTeacherDetail=jobForTeacherDAO.getTeacherDetailByDASA(sortOrderStrVal,start,noOfRowInPage,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm);
	                        lstTeacherDetailAll=jobForTeacherDAO.getTeacherDetailByDASARecords(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm);
	                        totalRecord=lstTeacherDetailAll.size();
	                  }else{
	                        //Noble Type check
	                	  TestTool.getTraceSecond("8");
	                        if(writePrivilegeToSchool && schoolId.equals("0"))
	                              entityTypeId = 2;
	                        if(!(entityTypeId==3 && lstJobOrderList.size()==0))
	                        {
	                              lstTeacherDetail=jobForTeacherDAO.getTeacherDetailByDASA(sortOrderStrVal,start,noOfRowInPage,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,schoolMaster);
	                              lstTeacherDetailAll=jobForTeacherDAO.getTeacherDetailByDASARecords(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,schoolMaster);
	                        }
	                        TestTool.getTraceSecond("9");
	                        totalRecord=lstTeacherDetailAll.size();
	                  }
	                  // sorting on normscore, ascore, lrscore
	                  if(!sortOrder.equals("tLastName"))
	                  {
	                        List<TeacherNormScore> teacherNormScoreList = teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetailAll);
	                        Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
	                        for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
	                              normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
	                        }
	                        Map<Integer,TeacherAchievementScore> mapAScore = null;

	                        if(entityID!=1 && achievementScore)
	                        {
	                              JobOrder jo=new JobOrder();
	                              jo.setDistrictMaster(districtMaster);
	                              List<TeacherAchievementScore> lstTeacherAchievementScore=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetailAll,jo);

	                              mapAScore = new HashMap<Integer, TeacherAchievementScore>();

	                              for(TeacherAchievementScore tAScore : lstTeacherAchievementScore){
	                                    mapAScore.put(tAScore.getTeacherDetail().getTeacherId(),tAScore);
	                              }
	                        }
	                        TeacherAchievementScore tAScore=null;

	                        TeacherNormScore teacherNormScore = null;
	                        for(TeacherDetail teacherDetail : lstTeacherDetailAll)
	                        {
	                              teacherNormScore = normMap.get(teacherDetail.getTeacherId());
	                              if(teacherNormScore==null)
	                                    teacherDetail.setNormScore(-0);
	                              else
	                                    teacherDetail.setNormScore(teacherNormScore.getTeacherNormScore());

	                              if(entityID!=1 && achievementScore)
	                              {
	                                    tAScore=mapAScore.get(teacherDetail.getTeacherId());
	                                    if(tAScore!=null){
	                                          if(tAScore.getAcademicAchievementScore()!=null)
	                                                teacherDetail.setaScore(tAScore.getAcademicAchievementScore());

	                                          if(tAScore.getLeadershipAchievementScore()!=null)
	                                                teacherDetail.setlRScore(tAScore.getLeadershipAchievementScore());
	                                    }else{
	                                          teacherDetail.setaScore(-1);
	                                          teacherDetail.setlRScore(-1);
	                                    }
	                              }
	                        }

	                        if(sortOrderFieldName.equals("normScore") && sortOrderType.equals("0")){
	                              Collections.sort(lstTeacherDetailAll,TeacherDetail.teacherDetailComparatorNormScoreDesc );
	                        }else if(sortOrderFieldName.equals("normScore") && sortOrderType.equals("1")){
	                              Collections.sort(lstTeacherDetailAll,TeacherDetail.teacherDetailComparatorNormScore);
	                        }else if(sortOrderFieldName.equals("aScore") && sortOrderType.equals("1")){                     
	                              Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorAScore );
	                        }else if(sortOrderFieldName.equals("aScore") && sortOrderType.equals("0")){                     
	                              Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorAScoreDesc );                        
	                        }else if(sortOrderFieldName.equals("lRScore") && sortOrderType.equals("1")){                     
	                              Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorLRScore );
	                        }else if(sortOrderFieldName.equals("lRScore") && sortOrderType.equals("0")){                     
	                              Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorLRScoreDesc );                      
	                        }
	                  }
	                  else
	                  {
	                        if(sortOrder.equals("tLastName")){
	                              for(TeacherDetail td:lstTeacherDetailAll)
	                              {
	                                          td.settLastName(td.getLastName());
	                              }
	                              
	                              if(sortOrderType.equals("0"))
	                                    Collections.sort(lstTeacherDetailAll,new TeacherDetailComparatorLastNameAsc());
	                              else
	                                    Collections.sort(lstTeacherDetailAll,new TeacherDetailComparatorLastNameDesc());
	                        }
	                  }

	                  TestTool.getTraceSecond("10");
	                  List<Integer> lstStatusMasters = new ArrayList<Integer>();
	                  String[] statuss = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
	                  try{
	                        lstStatusMasters = statusMasterDAO.findStatusIdsByStatusByShortNames(statuss);
	                  }catch (Exception e) {
	                        e.printStackTrace();
	                  }
	                  
	                  //comment by Ram nath
	                  if(totalRecord!=0){
	                	  	TestTool.getTraceSecond("11");
	                        //lstJobForTeacher=jobForTeacherDAO.getJobForTeacherByDSTeachersList(lstStatusMasters,lstTeacherDetailAll,lstJobOrderSLList,districtMaster,entityID);
	                        
	                        List<String[]> lstJobForTeachers =new ArrayList<String[]>();
	                        lstJobForTeachers=jobForTeacherDAO.getJobForTeacherByJobAndTeachersList(lstStatusMasters,lstTeacherDetailAll,lstJobOrderSLList,districtMaster,entityID);
	                        if(lstJobForTeachers.size()>0){
	                              for (Iterator it = lstJobForTeachers.iterator(); it.hasNext();) {
	                              Object[] row = (Object[]) it.next()       ;                                                                 
	                              jftMap1.put(Integer.parseInt(row[0].toString()),Integer.parseInt(row[1].toString()));
	                          }               
	                         }
	                        
	                        TestTool.getTraceSecond("12");
	                        
	                        /*for(JobForTeacher jobForTeacher : lstJobForTeacher){
	                              jftMap.put(jobForTeacher.getJobForTeacherId(),jobForTeacher);
	                        }*/
	                        //lstJobForTeacher=jobForTeacherDAO.getJobForTeacherByDSTeachersList(lstStatusMasters,lstTeacherDetailAll,lstJobOrderSLList,districtMaster,1);
	                        lstJobForTeachers=jobForTeacherDAO.getJobForTeacherByJobAndTeachersList(lstStatusMasters,lstTeacherDetailAll,lstJobOrderSLList,districtMaster,1);
	                        if(lstJobForTeachers.size()>0){
	                              for (Iterator it = lstJobForTeachers.iterator(); it.hasNext();) {
	                              Object[] row = (Object[]) it.next()       ;                                                                 
	                              jftMapForTm1.put(Integer.parseInt(row[0].toString()),Integer.parseInt(row[1].toString()));
	                          }               
	                         }
	                        TestTool.getTraceSecond("13");
	                        /*for(JobForTeacher jobForTeacher : lstJobForTeacher){
	                              jftMapForTm.put(jobForTeacher.getJobForTeacherId(),jobForTeacher);
	                        }*/
	                  }
	            }
	            List<TeacherNormScore> teacherNormScoreList=teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetailAll);
	            Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
	            for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
	                  normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
	            }

	            List<TeacherExperience> lstTeacherExperience=teacherExperienceDAO.findExperienceForTeachers(lstTeacherDetailAll);
	            TestTool.getTraceSecond("14");
	            //Excel Exporting
	                  String time = String.valueOf(System.currentTimeMillis()).substring(6);
	                  String basePath = request.getSession().getServletContext().getRealPath ("/")+"/applicantspool";
	                  fileName ="applicantspool"+time+".xls";
	                  
	                  File file = new File(basePath);
	                  if(!file.exists())
	                        file.mkdirs();

	                  Utility.deleteAllFileFromDir(basePath);

	                  file = new File(basePath+"/"+fileName);

	                  WorkbookSettings wbSettings = new WorkbookSettings();

	                  wbSettings.setLocale(new Locale("en", "EN"));

	                  WritableCellFormat timesBoldUnderline;
	                  WritableCellFormat header;
	                  WritableCellFormat headerBold;
	                  WritableCellFormat times;

	                  WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
	                  workbook.createSheet("applicantspool", 0);
	                  WritableSheet excelSheet = workbook.getSheet(0);

	                  WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
	                  // Define the cell format
	                  times = new WritableCellFormat(times10pt);
	                  // Lets automatically wrap the cells
	                  times.setWrap(true);

	                  // Create create a bold font with unterlines
	                  WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
	                  WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

	                  timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
	                  timesBoldUnderline.setAlignment(Alignment.CENTRE);
	                  // Lets automatically wrap the cells
	                  timesBoldUnderline.setWrap(true);

	                  header = new WritableCellFormat(times10ptBoldUnderline);
	                  headerBold = new WritableCellFormat(times10ptBoldUnderline);
	                  CellView cv = new CellView();
	                  cv.setFormat(times);
	                  cv.setFormat(timesBoldUnderline);
	                  cv.setAutosize(true);

	                  header.setBackground(Colour.GRAY_25);

	                  
	                  //Defining Number of Column 
	                  int numberOfRow=3;
	                  
	                  if(entityID!=1 && achievementScore){
	                        numberOfRow=numberOfRow+2;
	                  }
	                  if(tFA)
	                     numberOfRow=numberOfRow+1;
	                  if(teachingOfYear)
	                        numberOfRow=numberOfRow+1;
	                  if(expectedSalary)
	                        numberOfRow=numberOfRow+1;
	                  if(entityID==1)
	                        numberOfRow=numberOfRow+1;
	                  
	                 if(!noEPIFlag){
	                	 numberOfRow=numberOfRow-1; 
	                 }
	                  TestTool.getTraceSecond("16");
	                  // Write a few headers
	                  excelSheet.mergeCells(0, 0, numberOfRow-1, 1);
	                  Label label;
	                  label = new Label(0, 0, Utility.getLocaleValuePropByKey("headTeachApplicantPool", locale), timesBoldUnderline);
	                  excelSheet.addCell(label);
	                  excelSheet.mergeCells(0, 3, numberOfRow-1, 3);
	                  label = new Label(0, 3, "");
	                  excelSheet.addCell(label);
	                  excelSheet.getSettings().setDefaultColumnWidth(18);
	                  
	                  int k=4;
	                  int col=0;
	                  label = new Label(0, k, Utility.getLocaleValuePropByKey("lblNm", locale),header); 
	                  excelSheet.addCell(label);
	                  
	                  if(noEPIFlag){
	                	 label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblNormScore", locale),header); 
		                 excelSheet.addCell(label); 
	                  }
	                  
	                  if(entityID!=1 && achievementScore){
	                        label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblAScore", locale),header); 
	                        excelSheet.addCell(label);

	                        label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblL/RScore", locale),header); 
	                        excelSheet.addCell(label);
	                  }
	                  if(tFA)
	                  {
	                        label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblTFA", locale),header); 
	                        excelSheet.addCell(label);
	                  }
	                  label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblOfJobs", locale),header); 
	                  excelSheet.addCell(label);

	                  if(teachingOfYear){
	                        label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblOfYearsTeaching", locale),header); 
	                        excelSheet.addCell(label);
	                  }
	                  if(expectedSalary){
	                        label = new Label(++col, k, Utility.getLocaleValuePropByKey("ExptSalary", locale),header); 
	                        excelSheet.addCell(label);
	                  }

	                  if(entityID==1){
	                        label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblStatus", locale),header); 
	                        excelSheet.addCell(label);
	                  }
	                  
	                  
	                  k=k+1;
	                  if(lstTeacherDetailAll.size()==0)
	                  {     
	                        excelSheet.mergeCells(0, k, 8, k);
	                        label = new Label(0, k, Utility.getLocaleValuePropByKey("msgNorecordfound", locale)); 
	                        excelSheet.addCell(label);
	                  }
	            //****************************************************************************      
	                  TestTool.getTraceSecond("17");
	            String tFname="",tLname="";
	            List<TeacherPersonalInfo> lstTeacherPersonalInfo = new ArrayList<TeacherPersonalInfo>();
	            List<TeacherAchievementScore> lstTeacherAchievementScore = new ArrayList<TeacherAchievementScore>();                 
	            TeacherNormScore teacherNormScore = null;
	            
	            
	            List<Integer>teacherIdLst=new ArrayList<Integer>();
	            for (TeacherDetail teacherDetail : lstTeacherDetailAll) 
	            {
	                  teacherIdLst.add(teacherDetail.getTeacherId());
	            }
	            Criterion criterionTeacherPersonalInfo=Restrictions.in("teacherId", teacherIdLst);
	            List<TeacherPersonalInfo>teacherPersonalInfos=null;
	            HashMap<Integer, TeacherPersonalInfo> map=new HashMap<Integer, TeacherPersonalInfo>();
	            if(teacherIdLst!=null && teacherIdLst.size()>0){
	                  teacherPersonalInfos=teacherPersonalInfoDAO.findByCriteria(criterionTeacherPersonalInfo);
	                  for (TeacherPersonalInfo pojo : teacherPersonalInfos) 
	                  {
	                        map.put(pojo.getTeacherId(), pojo);
	                  }
	            }     
	                              
	            if(lstTeacherDetailAll.size()!=0)
	            {
	                  if(entityID!=1)
	                  {
	                        JobOrder jobOrder = new JobOrder();
	                        jobOrder.setDistrictMaster(districtMaster);
	                        lstTeacherAchievementScore=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetailAll,jobOrder);
	                  }
	                  lstTeacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPInfoForTeacher(lstTeacherDetailAll);
	            }
	      
	            TestTool.getTraceSecond("18");
	            
	            /*
	             * 		Generate URL
	             * 
	             * */
	            
				String path = request.getContextPath();
				int portNo = request.getServerPort();
				String showPortNo = null;
				if(portNo==80) {
					showPortNo="";
				} else {
					showPortNo="";
				}

				String baseUrl = request.getScheme()+"://"+request.getServerName()+""+showPortNo+path+"/";

				String urlString		=	null;
				String mainURLEncode	=	null;
				String mailContent		=	null;
				String mailToReference =    "";
				
				/*if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}*/

				int disId   = districtMaster.getDistrictId();
				String dId	=	null;
				String tId  =   null;
				String messageSubject= "Update Required: Your Application With "+districtMaster.getDistrictName()+"";
				JobOrder   jobOrder =null;
				SessionFactory factory = jobForTeacherDAO.getSessionFactory();
			    StatelessSession statelessSession=factory.openStatelessSession();
			    Transaction transaction = statelessSession.beginTransaction();
	            System.out.println("::::::::::::::::::::::: Total Record :::::::::::::::::"+lstTeacherDetailAll.size());
	            if(lstTeacherDetailAll.size()>0){
	            	for (TeacherDetail teacherDetail : lstTeacherDetailAll)
	            	{

						if(teacherDetail!=null)
							mailToReference	= 	teacherDetail.getEmailAddress();
							String to 		= 	teacherDetail.getEmailAddress();
							dId				=	Utility.encryptNo(disId);
							tId				=	Utility.encryptNo(teacherDetail.getTeacherId());
							
							urlString 		= 	"dId="+dId+"&tId="+tId+"";
							mainURLEncode	=	baseUrl+"personalinforeminder.do?p="+Utility.encodeInBase64(urlString);
							mailContent = MailText.PortfolioReminderMailByLink(teacherDetail, districtMaster, mainURLEncode);
							
							System.out.println(messageSubject);
							System.out.println(mailContent);
							emailerService.sendMailAsHTMLText(mailToReference,messageSubject, mailContent);
							
							MessageToTeacher messageToTeacher = new MessageToTeacher();
							messageToTeacher.setTeacherId(teacherDetail);
							messageToTeacher.setJobId(jobOrder);
							messageToTeacher.setTeacherEmailAddress(to);
							messageToTeacher.setMessageSubject(messageSubject);
							messageToTeacher.setMessageSend(mailContent);
							messageToTeacher.setSenderId(userMaster);
							messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
							messageToTeacher.setEntityType(userMaster.getEntityType());
							messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
							messageToTeacher.setCreatedDateTime(new Date());
							transaction = statelessSession.beginTransaction();
							statelessSession.insert(messageToTeacher);
							transaction.commit();
	            	}
	            }
	            TestTool.getTraceSecond("20");
	            lstTeacherDetailAll.clear();
	      }catch (Exception e) 
	      {
	            e.printStackTrace();
	      }
	      return fileName;
	}
	
	public List<JobOrder> getFilterJobOrder(List<JobOrder> jobOrderSList, List<JobOrder> jobOrderCertList)
	{
		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		String jobIds="";

		try{
			for(JobOrder jobOrder: jobOrderSList){
				jobIds+="||"+jobOrder.getJobId()+"||";
			}
		}catch (Exception e) { }
		try{
			for(JobOrder jobOrder: jobOrderCertList){
				if(jobIds.contains("||"+jobOrder.getJobId()+"||")){
					jobOrderList.add(jobOrder);
				}
			}
		}catch (Exception e) {}
		return jobOrderList;
	}
	
	public boolean startApplicantManagementReminder(){
		
		System.out.println(":::::::: startApplicantManagementReminder :::::::::");
		Integer districtId = 1200390;
		DistrictMaster districtMaster 	= 	null;
		UserMaster userMaster = null;
		
		/*WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();*/
		
		
		String userEmail = "HARSHBARGER@DADESCHOOLS.NET";
		
		List<UserMaster> lstUMaster = userMasterDAO.findByEmail(userEmail); 
		if(lstUMaster!=null && lstUMaster.size()>0){
			userMaster=lstUMaster.get(0);
		}
		
		districtMaster = districtMasterDAO.findById(districtId, false, false);
		System.out.println(districtMaster+"::: districtMaster111 :::::"+districtMaster.getDistrictId());
		/*	URL */
		/*String path = request.getContextPath();
		int portNo = request.getServerPort();
		String showPortNo = null;
		if(portNo==80) {
			showPortNo="";
		} else {
			showPortNo="";
		}*/
		//String baseUrl = request.getScheme()+"://"+request.getServerName()+""+showPortNo+path+"/";
		//String baseUrl = "https://platform.teachermatch.org/";
		String baseUrl = "http://localhost:8080/teachermatch/";
		
		String urlString		=	null;
		String mainURLEncode	=	null;
		String mailContent		=	null;
		String mailToReference 	=   "";
		
		int disId   = districtMaster.getDistrictId();
		String dId	=	null;
		String tId  =   null;
		String messageSubject= "Reminder -- Update Required for Your Application with "+districtMaster.getDistrictName()+"";
		
		JobOrder   jobOrder =null;

		try{
			List<TeacherDetail> teacherDetailList 		= 	null;
			List<TeacherDetail> teacherDetailListMain 	= 	null;
			List<StatusMaster>  statusMasterList		=	null;
			String arrStatusId = null;
			
			arrStatusId = districtMaster.getStatusSendMailPortfolio();
			String statusId[]=arrStatusId.split(",");
			List statusIdList = new ArrayList();
			for(int i=0;i<statusId.length;i++) {
				statusIdList.add(Integer.parseInt(statusId[i]));	
			}
			
			statusMasterList		=	statusMasterDAO.findStatusByStatusIdList(statusIdList);
			teacherDetailList 	  	= 	jobForTeacherDAO.getAllHiredTeacher(districtMaster);
			teacherDetailListMain 	= 	jobForTeacherDAO.getAllTeacherByStatus(statusMasterList,districtMaster,teacherDetailList);
			
			/*
			 * Create map for MessageToTeacher
			 * 
			 * */
			List<MessageToTeacher> messageToTeacherList = null;
			Criterion criteria = null;
			Map<Integer,MessageToTeacher> msgMap = new HashMap<Integer, MessageToTeacher>();
			
			try{
				criteria = Restrictions.eq("portfolioReminder", "Y");
				messageToTeacherList = messageToTeacherDAO.findByCriteria(criteria);
				
				for(MessageToTeacher msgObj:messageToTeacherList){
					msgMap.put(msgObj.getTeacherId().getTeacherId(), msgObj);
				}
				
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			/*
			 * 	END Map
			 * */
			SessionFactory factory = jobForTeacherDAO.getSessionFactory();
		    StatelessSession statelessSession=factory.openStatelessSession();
		    Transaction transaction = statelessSession.beginTransaction();
			
			int noOfReminderPortfolio = districtMaster.getNoOfReminderPortfolio()==null?0:districtMaster.getNoOfReminderPortfolio();
			int reminderFrequencyInDaysPortfolio = districtMaster.getReminderFrequencyInDaysPortfolio()==null?0:districtMaster.getReminderFrequencyInDaysPortfolio();
			int reminderOfFirstFrequencyInDaysPortfolio = districtMaster.getReminderOfFirstFrequencyInDaysPortfolio()==null?0:districtMaster.getReminderOfFirstFrequencyInDaysPortfolio();
			
			Date endDate 	= new Date();
			Date startDate 	= null;
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			
			
			String teacherEmail = "hanzala@netsutra.com";
			TeacherDetail teacherDetailTest = null;
			teacherDetailListMain = teacherDetailDAO.findByEmail(teacherEmail); 
			if(teacherDetailListMain!=null && teacherDetailListMain.size()>0){
				teacherDetailTest=teacherDetailListMain.get(0);
			}
			//System.out.println(teacherDetailListMain.size()+"::::::::::::::::::::::::::"+teacherDetailTest.getEmailAddress());
			
			if(teacherDetailListMain.size()>0 && teacherDetailListMain!=null){
				for(TeacherDetail teacherDetail  : teacherDetailListMain){
					
					boolean chkDup=false;
					int noOfReminderSent = 0;
					
					MessageToTeacher messageToTeacher = new MessageToTeacher();
					MessageToTeacher mSt = msgMap.get(teacherDetail.getTeacherId());
					
					if(mSt!=null && teacherDetail.getTeacherId().equals(mSt.getTeacherId().getTeacherId()))
						chkDup = true;
					
					if(mSt!=null && mSt.getNoOfReminderSent()!=null)
						noOfReminderSent = mSt.getNoOfReminderSent()==null?0:mSt.getNoOfReminderSent();
					
					
					if(noOfReminderSent==1)
						reminderFrequencyInDaysPortfolio = reminderOfFirstFrequencyInDaysPortfolio;
						
					if(noOfReminderSent < noOfReminderPortfolio){
						
						if(mSt==null)
						{
							startDate = teacherDetail.getCreatedDateTime();
							try {startDate = dateFormat.parse(dateFormat.format(startDate));
							} catch (ParseException e) {}
						} else {
							startDate = mSt.getCreatedDateTime();
						}
						int difInDays = (int) ((endDate.getTime() - startDate.getTime())/(1000*60*60*24));
						//System.out.println("difInDays::: "+difInDays);
						/*System.out.println(":: difInDays ::"+difInDays);
						System.out.println(":: reminderFrequencyInDaysPortfolio ::"+reminderFrequencyInDaysPortfolio);
						System.out.println(":: noOfReminderSent ::"+noOfReminderSent);
						System.out.println(":: noOfReminderPortfolio ::"+noOfReminderPortfolio);*/
						
						if((noOfReminderPortfolio>=noOfReminderSent && difInDays==reminderFrequencyInDaysPortfolio) || (noOfReminderSent==0 && difInDays>=reminderFrequencyInDaysPortfolio)){
							if(teacherDetail!=null){
								try{
									
									mailToReference	= teacherDetail.getEmailAddress();
									String to = teacherDetail.getEmailAddress();
									dId=Utility.encryptNo(disId);
									tId=Utility.encryptNo(teacherDetail.getTeacherId());
									urlString 		= 	"dId="+dId+"&tId="+tId+"";
									
									mainURLEncode	=	baseUrl+"personalinforeminder.do?p="+Utility.encodeInBase64(urlString);
									
									mailContent = MailText.PortfolioReminderMailByLink(teacherDetail, districtMaster, mainURLEncode);
									System.out.println(mainURLEncode);
									//emailerService.sendMailAsHTMLText(mailToReference,messageSubject, mailContent);
									
									messageToTeacher.setTeacherId(teacherDetail);
									messageToTeacher.setJobId(jobOrder);
									messageToTeacher.setTeacherEmailAddress(to);
									messageToTeacher.setMessageSubject(messageSubject);
									messageToTeacher.setMessageSend(mailContent);
									messageToTeacher.setSenderId(userMaster);
									messageToTeacher.setSenderEmailAddress("MDCPS");
									messageToTeacher.setEntityType(userMaster.getEntityType());
									messageToTeacher.setPortfolioReminder("Y");
									
									if(chkDup==true){
										mSt.setNoOfReminderSent(mSt.getNoOfReminderSent()+1);
										mSt.setCreatedDateTime(new Date());
									} else {
										messageToTeacher.setNoOfReminderSent(1);
									}
									
									//messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
									messageToTeacher.setCreatedDateTime(new Date());
									transaction = statelessSession.beginTransaction();
									if(chkDup==true){
										statelessSession.update(mSt);
									} else {
										statelessSession.insert(messageToTeacher);
									}

								} catch(Exception exception){
									exception.printStackTrace();
								}
							}
						}
					}
				}
			}
			transaction.commit();
			
		} catch(Exception exception){
			exception.printStackTrace();
		}
		
		return true;
	}
	
	public boolean startApplicantManagementNoResponse(){
		
		Integer districtId 				= 	1200390;
		DistrictMaster districtMaster 	= 	null;
		UserMaster userMaster 			= 	null;
		List<PortfolioReminders> portfolioRemindersList = 	null;
		PortfolioReminders portReminder = 	null;
		Integer statusIdReminderExpireActionPortfolio = 0;
		SessionFactory sessionFactory =  secondaryStatusDAO.getSessionFactory();
		StatelessSession statelessSession1 = sessionFactory.openStatelessSession();
		Transaction transaction = statelessSession1.beginTransaction();
		districtMaster = districtMasterDAO.findById(districtId, false, false);
		Integer noOfReminderPortfolio = 0;

		List<SecondaryStatus> secondaryStatusList = null;
		List<JobForTeacher> jobForTeacherList 	  = null;
		try{
			portfolioRemindersList	= 	portfolioRemindersDAO.getRecordByDistrict(districtMaster);
			if(portfolioRemindersList != null && portfolioRemindersList.size()>0){
				portReminder = portfolioRemindersList.get(0);
			}
			noOfReminderPortfolio = districtMaster.getNoOfReminderPortfolio();
			List<TeacherDetail> teacherDetailList = null;
			
			if(noOfReminderPortfolio!=0)
				teacherDetailList = messageToTeacherDAO.getPortfolioRecord(noOfReminderPortfolio);
			
			statusIdReminderExpireActionPortfolio = districtMaster.getStatusIdReminderExpireActionPortfolio();
			
			List<TeacherPersonalInfo> teacherPersonalInfoList = null;
			List<TeacherCertificate> teacherCertificateList   = null;
			List<PortfolioRemindersDspq> portfolioRemindersDspqList = null;
			List<DistrictSpecificPortfolioAnswers> districtSpecificPortfolioAnswersList = null;
			// findCertificateByTeacher
			if(teacherDetailList.size()>0){
				secondaryStatusList =  	secondaryStatusDAO.getOfferReadyByDistrict(districtMaster);
				jobForTeacherList	=	jobForTeacherDAO.getTeacherByStatusAndSecondaryStatus(teacherDetailList,districtMaster,secondaryStatusList);
				portfolioRemindersDspqList = portfolioRemindersDspqDAO.getRecordByDistrict(districtMaster);
				
				// Map For  TeacherPersonalinfo 
				Map<Integer,TeacherPersonalInfo> pInfoMap = new HashMap<Integer, TeacherPersonalInfo>();
				Map<Integer,TeacherCertificate> tCertiMap = new HashMap<Integer, TeacherCertificate>();
				//Map<Integer,List<DistrictSpecificPortfolioAnswers>> dspqAnsMap = new HashMap<Integer, List<DistrictSpecificPortfolioAnswers>>();
				Map<Integer,DistrictSpecificPortfolioAnswers> dspqAnsMap = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
				try{
					if(teacherDetailList.size()>0){
						teacherPersonalInfoList = teacherPersonalInfoDAO.findTeacherPInfoForTeacher(teacherDetailList);
						teacherCertificateList  = teacherCertificateDAO.findCertificateByTeacher(teacherDetailList);
						districtSpecificPortfolioAnswersList =  districtSpecificPortfolioAnswersDAO.getByDistrictAndTeacherList(teacherDetailList,districtMaster);
					}
					
					System.out.println("::: teacherCertificateList :::"+teacherCertificateList);
					
					for(TeacherPersonalInfo msgObj:teacherPersonalInfoList){
						pInfoMap.put(msgObj.getTeacherId(), msgObj);
					}
					
					for(TeacherCertificate tcObj:teacherCertificateList){
						tCertiMap.put(tcObj.getTeacherDetail().getTeacherId(), tcObj);
					}
					
					for(DistrictSpecificPortfolioAnswers dspqAns:districtSpecificPortfolioAnswersList){
						dspqAnsMap.put(dspqAns.getTeacherDetail().getTeacherId(), dspqAns);
					}
					
				} catch(Exception exception){
					exception.printStackTrace();
				}
				PreparedStatement  pstmt=null;
				CGReportService crs = new CGReportService();
				Connection con = crs.getConnection();
				Session session = sessionFactory.openSession();
				
				String teacherEmail = "hanzala@netsutra.com";
				TeacherDetail teacherDetailTest = null;
				List<TeacherDetail> teacherDetailListMain = teacherDetailDAO.findByEmail(teacherEmail); 
				if(teacherDetailListMain!=null && teacherDetailListMain.size()>0){
					teacherDetailTest=teacherDetailListMain.get(0);
				}
				System.out.println("::::::::: jobForTeacherList ::::::"+jobForTeacherList.size());
				jobForTeacherList = jobForTeacherDAO.findJobByTeacher(teacherDetailTest);
				System.out.println("::::::::: jobForTeacherList ::::::"+jobForTeacherList.size());
				try{
					for(JobForTeacher jft : jobForTeacherList){
						
						jft.getTeacherId().getPhoneNumber();
						Boolean chkPhone = false;
						Boolean chkCerti = false;
						Boolean chkDSPQ = false;
						
						TeacherPersonalInfo tps = pInfoMap.get(jft.getTeacherId().getTeacherId());
						TeacherCertificate  tcf = tCertiMap.get(jft.getTeacherId().getTeacherId());
						DistrictSpecificPortfolioAnswers dspqAns = dspqAnsMap.get(jft.getTeacherId().getTeacherId());
						
						System.out.println(jft.getTeacherId().getTeacherId()+":::::::: tcf :::::::::::"+tcf);
						// Condition Starts
						if(tps!=null){
							if(portReminder.getPhoneNumber()==1 && tps.getPhoneNumber()==null)
								chkPhone = true;
						}

						if(portReminder.getCertification()==1 && tcf==null)
							chkCerti = true;
						
						if(dspqAns!=null && portfolioRemindersDspqList==null)
							chkDSPQ = true;
						
						System.out.println(chkPhone+"::::::"+chkCerti+"::::::"+chkDSPQ);
						if(chkPhone==true || chkCerti==true || chkDSPQ==true){
							System.out.println(":::: UPDATE JFT :::::::::::::");
							try {
									pstmt = con.prepareStatement("update jobforteacher set status="+statusIdReminderExpireActionPortfolio+",displayStatusId="+statusIdReminderExpireActionPortfolio+" WHERE jobForTeacherId="+jft.getJobForTeacherId()+"");
									pstmt.executeUpdate();
							
							} catch (HibernateException e) {
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					
				}catch(Exception exception){
					exception.printStackTrace();
				}
			}
		} catch(Exception exception){
			exception.printStackTrace();
		}
		
		return true;
	}
	
}
