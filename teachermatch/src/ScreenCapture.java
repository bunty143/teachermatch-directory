import java.awt.AWTException;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

/**
 * Simple screen capture class which demonstrates how to get the size of the
 * default screen, how to capture a snaphot of it, and finally save it as an
 * image file.
 * 
 * @author Havard Rast Blok
 * 
 */
public class ScreenCapture {

  BufferedImage screen;

  public ScreenCapture() {

  }

  /**
   * Returns the size of the default screen.
   * 
   * @return the size of the current/default graphics device.
   */
  protected Rectangle getScreenSize() {
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice gd = ge.getDefaultScreenDevice();
    DisplayMode dm = gd.getDisplayMode();
    return new Rectangle(dm.getWidth(), dm.getHeight());
  }

  /**
   * Captures an image of the default screen.
   *
   */
  public void capture() {
    try {
      Robot robot = new Robot();
      Rectangle screenSize = getScreenSize();
      screen = robot.createScreenCapture(screenSize);
    } catch (AWTException e) {
      e.printStackTrace();
    }

  }
  
  /**
   * Save an already captured image to file, using the PNG image format.
   * 
   * @param fileName path/file to save to
   * @throws IllegalStateException if the capture() methods was not previously called.
   */
  public void save(String fileName) {
    if(screen == null) {
      throw new IllegalStateException("No screen captured yet.");
    }
    
    //set the image file format to use for frame saving
    Iterator ite = ImageIO.getImageWritersByFormatName("png");
    ImageWriter imgWriter = (ImageWriter) ite.next();
    
    try {
      //create a buffered file output stream
      BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(fileName));
      //and use it to create an image output stream
      ImageOutputStream ios = ImageIO.createImageOutputStream(bout);

      //assign it to the image writer, and write the image 
      imgWriter.setOutput(ios);
      imgWriter.write(screen);

      //always remember to close any streams and writers used 
      ios.close();
      bout.close();
      
      System.out.println("Saved screen to "+fileName);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * @param args N/A
   */
  public static void main(String[] args) {
    ScreenCapture sc = new ScreenCapture();
    sc.capture();
    sc.save("test.png");
  }

}