package org.tempuri.memberqualification;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="Member">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RegistrationId">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="20"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="FirstName">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="60"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Surname">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="60"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="ProfessionalDesignationEnglish" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="3"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="ProfessionalDesignationFrench" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="3"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="NTIPCompletionYear" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BasicQualification">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RegistrationId">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="20"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Division" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="60"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Subject" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="80"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Institution" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="200"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="SubjectCode" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="6"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="DivisionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AdditionalQualifications">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RegistrationId">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="20"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Subject" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="120"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Option" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="120"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Institution" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="200"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="SubjectCode" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="6"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="OptionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "memberOrBasicQualificationOrAdditionalQualifications" })
@XmlRootElement(name = "MemberQualification")
public class MemberQualification {

	@XmlElements( {
			@XmlElement(name = "AdditionalQualifications", type = MemberQualification.AdditionalQualifications.class),
			@XmlElement(name = "BasicQualification", type = MemberQualification.BasicQualification.class),
			@XmlElement(name = "Member", type = MemberQualification.Member.class) })
	protected List<Object> memberOrBasicQualificationOrAdditionalQualifications;

	/**
	 * Gets the value of the
	 * memberOrBasicQualificationOrAdditionalQualifications property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the
	 * memberOrBasicQualificationOrAdditionalQualifications property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getMemberOrBasicQualificationOrAdditionalQualifications().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link MemberQualification.AdditionalQualifications }
	 * {@link MemberQualification.BasicQualification }
	 * {@link MemberQualification.Member }
	 * 
	 * 
	 */
	public List<Object> getMemberOrBasicQualificationOrAdditionalQualifications() {
		if (memberOrBasicQualificationOrAdditionalQualifications == null) {
			memberOrBasicQualificationOrAdditionalQualifications = new ArrayList<Object>();
		}
		return this.memberOrBasicQualificationOrAdditionalQualifications;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="RegistrationId">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="20"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="Subject" minOccurs="0">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="120"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="Option" minOccurs="0">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="120"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="Institution" minOccurs="0">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="200"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
	 *         &lt;element name="SubjectCode" minOccurs="0">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="6"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="OptionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "registrationId", "subject", "option",
			"institution", "effectiveDate", "subjectCode", "optionCode" })
	public static class AdditionalQualifications {

		@XmlElement(name = "RegistrationId", required = true)
		protected String registrationId;
		@XmlElement(name = "Subject")
		protected String subject;
		@XmlElement(name = "Option")
		protected String option;
		@XmlElement(name = "Institution")
		protected String institution;
		@XmlElement(name = "EffectiveDate")
		@XmlSchemaType(name = "dateTime")
		protected XMLGregorianCalendar effectiveDate;
		@XmlElement(name = "SubjectCode")
		protected String subjectCode;
		@XmlElement(name = "OptionCode")
		protected String optionCode;

		/**
		 * Gets the value of the registrationId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getRegistrationId() {
			return registrationId;
		}

		/**
		 * Sets the value of the registrationId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setRegistrationId(String value) {
			this.registrationId = value;
		}

		/**
		 * Gets the value of the subject property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSubject() {
			return subject;
		}

		/**
		 * Sets the value of the subject property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSubject(String value) {
			this.subject = value;
		}

		/**
		 * Gets the value of the option property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getOption() {
			return option;
		}

		/**
		 * Sets the value of the option property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setOption(String value) {
			this.option = value;
		}

		/**
		 * Gets the value of the institution property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getInstitution() {
			return institution;
		}

		/**
		 * Sets the value of the institution property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setInstitution(String value) {
			this.institution = value;
		}

		/**
		 * Gets the value of the effectiveDate property.
		 * 
		 * @return possible object is {@link XMLGregorianCalendar }
		 * 
		 */
		public XMLGregorianCalendar getEffectiveDate() {
			return effectiveDate;
		}

		/**
		 * Sets the value of the effectiveDate property.
		 * 
		 * @param value
		 *            allowed object is {@link XMLGregorianCalendar }
		 * 
		 */
		public void setEffectiveDate(XMLGregorianCalendar value) {
			this.effectiveDate = value;
		}

		/**
		 * Gets the value of the subjectCode property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSubjectCode() {
			return subjectCode;
		}

		/**
		 * Sets the value of the subjectCode property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSubjectCode(String value) {
			this.subjectCode = value;
		}

		/**
		 * Gets the value of the optionCode property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getOptionCode() {
			return optionCode;
		}

		/**
		 * Sets the value of the optionCode property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setOptionCode(String value) {
			this.optionCode = value;
		}

	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="RegistrationId">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="20"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="Division" minOccurs="0">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="60"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="Subject" minOccurs="0">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="80"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="Institution" minOccurs="0">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="200"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
	 *         &lt;element name="SubjectCode" minOccurs="0">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="6"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="DivisionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "registrationId", "division", "subject",
			"institution", "effectiveDate", "subjectCode", "divisionCode" })
	public static class BasicQualification {

		@XmlElement(name = "RegistrationId", required = true)
		protected String registrationId;
		@XmlElement(name = "Division")
		protected String division;
		@XmlElement(name = "Subject")
		protected String subject;
		@XmlElement(name = "Institution")
		protected String institution;
		@XmlElement(name = "EffectiveDate")
		@XmlSchemaType(name = "dateTime")
		protected XMLGregorianCalendar effectiveDate;
		@XmlElement(name = "SubjectCode")
		protected String subjectCode;
		@XmlElement(name = "DivisionCode")
		protected String divisionCode;

		/**
		 * Gets the value of the registrationId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getRegistrationId() {
			return registrationId;
		}

		/**
		 * Sets the value of the registrationId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setRegistrationId(String value) {
			this.registrationId = value;
		}

		/**
		 * Gets the value of the division property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getDivision() {
			return division;
		}

		/**
		 * Sets the value of the division property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setDivision(String value) {
			this.division = value;
		}

		/**
		 * Gets the value of the subject property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSubject() {
			return subject;
		}

		/**
		 * Sets the value of the subject property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSubject(String value) {
			this.subject = value;
		}

		/**
		 * Gets the value of the institution property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getInstitution() {
			return institution;
		}

		/**
		 * Sets the value of the institution property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setInstitution(String value) {
			this.institution = value;
		}

		/**
		 * Gets the value of the effectiveDate property.
		 * 
		 * @return possible object is {@link XMLGregorianCalendar }
		 * 
		 */
		public XMLGregorianCalendar getEffectiveDate() {
			return effectiveDate;
		}

		/**
		 * Sets the value of the effectiveDate property.
		 * 
		 * @param value
		 *            allowed object is {@link XMLGregorianCalendar }
		 * 
		 */
		public void setEffectiveDate(XMLGregorianCalendar value) {
			this.effectiveDate = value;
		}

		/**
		 * Gets the value of the subjectCode property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSubjectCode() {
			return subjectCode;
		}

		/**
		 * Sets the value of the subjectCode property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSubjectCode(String value) {
			this.subjectCode = value;
		}

		/**
		 * Gets the value of the divisionCode property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getDivisionCode() {
			return divisionCode;
		}

		/**
		 * Sets the value of the divisionCode property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setDivisionCode(String value) {
			this.divisionCode = value;
		}

	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="RegistrationId">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="20"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="FirstName">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="60"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="Surname">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="60"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="ProfessionalDesignationEnglish" minOccurs="0">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="3"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="ProfessionalDesignationFrench" minOccurs="0">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="3"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="NTIPCompletionYear" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "registrationId", "firstName", "surname",
			"professionalDesignationEnglish", "professionalDesignationFrench",
			"ntipCompletionYear" })
	public static class Member {

		@XmlElement(name = "RegistrationId", required = true)
		protected String registrationId;
		@XmlElement(name = "FirstName", required = true)
		protected String firstName;
		@XmlElement(name = "Surname", required = true)
		protected String surname;
		@XmlElement(name = "ProfessionalDesignationEnglish")
		protected String professionalDesignationEnglish;
		@XmlElement(name = "ProfessionalDesignationFrench")
		protected String professionalDesignationFrench;
		@XmlElement(name = "NTIPCompletionYear")
		protected Integer ntipCompletionYear;

		/**
		 * Gets the value of the registrationId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getRegistrationId() {
			return registrationId;
		}

		/**
		 * Sets the value of the registrationId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setRegistrationId(String value) {
			this.registrationId = value;
		}

		/**
		 * Gets the value of the firstName property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getFirstName() {
			return firstName;
		}

		/**
		 * Sets the value of the firstName property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setFirstName(String value) {
			this.firstName = value;
		}

		/**
		 * Gets the value of the surname property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSurname() {
			return surname;
		}

		/**
		 * Sets the value of the surname property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSurname(String value) {
			this.surname = value;
		}

		/**
		 * Gets the value of the professionalDesignationEnglish property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getProfessionalDesignationEnglish() {
			return professionalDesignationEnglish;
		}

		/**
		 * Sets the value of the professionalDesignationEnglish property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setProfessionalDesignationEnglish(String value) {
			this.professionalDesignationEnglish = value;
		}

		/**
		 * Gets the value of the professionalDesignationFrench property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getProfessionalDesignationFrench() {
			return professionalDesignationFrench;
		}

		/**
		 * Sets the value of the professionalDesignationFrench property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setProfessionalDesignationFrench(String value) {
			this.professionalDesignationFrench = value;
		}

		/**
		 * Gets the value of the ntipCompletionYear property.
		 * 
		 * @return possible object is {@link Integer }
		 * 
		 */
		public Integer getNTIPCompletionYear() {
			return ntipCompletionYear;
		}

		/**
		 * Sets the value of the ntipCompletionYear property.
		 * 
		 * @param value
		 *            allowed object is {@link Integer }
		 * 
		 */
		public void setNTIPCompletionYear(Integer value) {
			this.ntipCompletionYear = value;
		}

	}

}
