package org.tempuri.memberqualification;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.sf.json.JSON;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.xml.XMLSerializer;

//import org.json.XML;
import org.tempuri.ApplyToEducationGetMemberQualificationsResponse.ApplyToEducationGetMemberQualificationsResult;
import org.tempuri.memberqualification.MemberQualification.AdditionalQualifications;
import org.tempuri.memberqualification.MemberQualification.BasicQualification;
import org.tempuri.memberqualification.MemberQualification.Member;
import org.tempuri.IOCTMemberPublicInformation;
import org.tempuri.OCTMemberPublicInformation;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.org.apache.xerces.internal.dom.ElementNSImpl;
public class MemberQualificationClient 
{
	int i=0;
	public void getText(NodeList list)
	{
		for(int i=0;i<list.getLength();i++)
		{
			if(list.getLength()>1 && i==0)
			{
				System.out.println("11--------"+list.item(i).getParentNode().getNodeName()+"--------");
			}

			if(list.item(i).hasChildNodes() )
			{

				getText(list.item(i).getChildNodes());
			}
			else 
			{
				if(i==0)
				{
					//show(list.item(i));
					System.out.println(list.item(i).getParentNode().getNodeName()+"\t"+list.item(i).getTextContent());
				}
			}


		}
	}
	public void show(Node node)
	{
		System.out.println(node.getParentNode().getNodeName()+"\t"+node.getTextContent());
	}

	/*public void getText(NodeList list)
	{
		for(int i=0;i<list.getLength();i++)
		{
			String parentNode = list.item(i).getParentNode().getNodeName();

			System.out.println("parentNode:"+parentNode);
			if(list.item(i).hasChildNodes() )
			{
				System.out.println(list.item(i).getParentNode().getNodeName()+"\t"+list.item(i).getTextContent());
			}
		}
	}*/

	/*public JSONObject getMemberQualification(String RegistrationId)
	{
		try
		{
			OCTMemberPublicInformation service = new OCTMemberPublicInformation();
			IOCTMemberPublicInformation client=service.getBasicHttpBindingIOCTMemberPublicInformation();
			ApplyToEducationGetMemberQualificationsResult MemberQualifications=client.applyToEducationGetMemberQualifications(RegistrationId);
			Node xmlNode = ((Node)MemberQualifications.getAny());
			String responseData = xmlNode.getTextContent();
			//System.out.println(responseData);
			if(!responseData.equals(""))
			{

				StringWriter writer = new StringWriter();
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
				transformer.transform(new DOMSource(xmlNode), new StreamResult(writer));
				String xml = writer.toString();
				//System.out.println(""+xml);

				org.json.JSONObject soapDatainJsonObject = XML.toJSONObject(xml);
				org.json.JSONObject diffgram = soapDatainJsonObject.getJSONObject("diffgr:diffgram");
				org.json.JSONObject memberQualification = diffgram.getJSONObject("MemberQualification");
				org.json.JSONObject member = memberQualification.getJSONObject("Member");
				System.out.println(member.get("RegistrationId"));

				JSONObject jsonRequest = null;
				XMLSerializer ss = new XMLSerializer();
				JSON json  = ss.read(xml);

				//System.out.println(g.toString());
				try {
					jsonRequest = (JSONObject) JSONSerializer.toJSON(json.toString());
				} catch (Exception e) {
					e.printStackTrace();
				} 

				//System.out.println(jsonRequest);
				return jsonRequest;

			}else
				return null;

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}*/

	public Object[] getMemberQualification(String RegistrationId)
	{
		try
		{
			OCTMemberPublicInformation service = new OCTMemberPublicInformation();
			IOCTMemberPublicInformation portType = service.getBasicHttpBindingIOCTMemberPublicInformation();
			ApplyToEducationGetMemberQualificationsResult result= portType.applyToEducationGetMemberQualifications(RegistrationId);
			//ApplyToEducationGetMemberQualificationsResult result= portType.applyToEducationGetMemberQualifications("1111111");
			Object[] returnObject = new Object[3];
			System.out.println((ElementNSImpl)result.getAny());
			Node node=(Node)result.getAny();
			String text = node.getTextContent();
			if(text!=null && !text.equals(""))
			{
				StringWriter writer = new StringWriter();
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
				transformer.transform(new DOMSource(node.getFirstChild()), new StreamResult(writer));
				String xml = writer.toString();
				System.out.println(xml);

				JAXBContext jaxbContext = JAXBContext.newInstance(MemberQualification.class,Member.class,BasicQualification.class,AdditionalQualifications.class);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

				StringReader reader = new StringReader(xml);
				MemberQualification memberQualification = (MemberQualification) unmarshaller.unmarshal(reader);
				System.out.println(memberQualification.getMemberOrBasicQualificationOrAdditionalQualifications());
				List<Object> list=memberQualification.getMemberOrBasicQualificationOrAdditionalQualifications();

				List<Member> memberList = new ArrayList<Member>();
				List<BasicQualification> basicQualificationList = new ArrayList<BasicQualification>();
				List<AdditionalQualifications> additionalQualificationList = new ArrayList<AdditionalQualifications>();
				for(Object object:list)
				{
					if(object instanceof Member)
					{
						//System.out.println("*************Member Qualification*************");
						//Member member=(Member)object;
						memberList.add((Member)object);
					}
					if(object instanceof BasicQualification)
					{
						//System.out.println("*************basic Qualification*************");
						//BasicQualification basicQualification=(BasicQualification)object;
						basicQualificationList.add((BasicQualification)object);
					}

					if(object instanceof AdditionalQualifications)
					{
						//System.out.println("*************Additional Qualification*************");
						//AdditionalQualifications additionalQualifications=(AdditionalQualifications)object;
						additionalQualificationList.add((AdditionalQualifications)object);
					}

				}

				System.out.println("basicQualificationList.size():: "+basicQualificationList.size());
				System.out.println("additionalQualificationList.size():: "+additionalQualificationList.size());
				
				returnObject[0]= memberList;
				returnObject[1]= basicQualificationList;
				returnObject[2]= additionalQualificationList;
				
				return returnObject;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}
	public static void main(String args[])
	{
		try
		{
			MemberQualificationClient client = new MemberQualificationClient();
			client.getMemberQualification("518784");			//System.out.println("jsonRequest:: "+jsonRequest);

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}
}
