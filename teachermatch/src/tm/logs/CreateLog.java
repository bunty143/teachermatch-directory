package tm.logs;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import antlr.collections.List;

import tm.bean.JobCertification;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.user.UserMaster;

 
public class CreateLog {

	public void logForJobOrder(HttpServletRequest request, final JobOrder jobOrder,final boolean isNew , final Integer jobId, final UserMaster user , final ArrayList<JobWisePanelStatus> listJobWise){
		
				Thread t = new Thread(new Runnable() {
			         public void run()
			         {
			        	 try {
								Thread.sleep(8000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
			        	 
			        	 try{new LogUtility().createLogForJobOrder(jobOrder,isNew,jobId, user , listJobWise);
			        	 }catch (Exception e) {
							System.out.println("########################## exception from logForJobOrder ###################"+e);
						e.printStackTrace();
			        	 }
			        	 
			         }
			});
				
				t.start();
				
				
	}
	
	
public void logForCertificate(HttpServletRequest request, final ArrayList<JobCertification> certList ,final JobOrder jobOrder , final UserMaster userMaster){
		
	 
				Thread t = new Thread(new Runnable() {
			         public void run()
			         {
			        	 try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
				                   e.printStackTrace();
							}
			        	 
			        	 try{new LogUtility().createCertificate(certList , jobOrder , userMaster);
			        	 }catch (Exception e) {
							System.out.println("########################## exception from logForJobOrder ###################"+e);
						e.printStackTrace();
			        	 }
			        	 
			         }
			});
				
				t.start();
				
				
	}
	
	

public void logSchoolInJobOrderLog(HttpServletRequest request, final ArrayList<SchoolInJobOrder> lstschoolInJobOrder  , final UserMaster userMaster , final JobOrder jobId){
	
			Thread t = new Thread(new Runnable() {
		         public void run()
		         {
		        	 try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
			                 e.printStackTrace();
						}
		        	 
		        	 try{new LogUtility().createSchoolInJobOrder(lstschoolInJobOrder , userMaster , jobId);
		        	 }catch (Exception e) {
						System.out.println("########################## exception from logForJobOrder ###################"+e);
					e.printStackTrace();
		        	 }
		        	 
		         }
		});
			
			t.start();
			
			
}


}
