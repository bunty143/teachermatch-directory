package tm.api;

import javax.servlet.http.HttpServletRequest;

import tm.utility.Utility;

public class PaginationAndSortingAPI 
{
	public static String getPaginationString(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{
		
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				//sb.append("<<");
				sb.append("First&nbsp;&nbsp;|");
				sb.append("</A>");
				
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				//sb.append("<");
				sb.append("&nbsp;&nbsp;<&lt;Previous&nbsp;&nbsp;|");
				sb.append("</A>");
			
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			//
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				//sb.append(">");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				//sb.append(">>");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px; height:30px; color: #555555;' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		
		//System.out.println(sb.toString());
		return sb.toString();
	}
	public static String responseSortingLink(String fieldLabel, String sortOrderFieldName,String fieldName,String sortOrderTypeVal,int pgNo){
		StringBuffer sbResponseText = new StringBuffer("");
		try{
			sbResponseText.append("<span style='cursor:pointer;'>");
			if( sortOrderTypeVal.equals("1") && sortOrderFieldName.equals(fieldName)){
				sbResponseText.append("<a  style='text-decoration: none;color:#444444;'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\">"+fieldLabel+"&nbsp;<img alt='' src='../images/Descorder.png' height='10px' width='10px'></a>");
			}else if( sortOrderTypeVal.equals("0") && sortOrderFieldName.equals(fieldName)){
				sbResponseText.append("<a  style='text-decoration: none;color:#444444;'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\">"+fieldLabel+"&nbsp;<img alt='' src='../images/AscOrder.png' height='10px' width='10px'></a>");
			}else{
				sbResponseText.append("<a style='color:#444444;'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\">"+fieldLabel+"</a>");
			}
			sbResponseText.append("</span>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sbResponseText+"";
	}
	
	public static String getPaginationDoubleString(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				//sb.append("<<");
				sb.append("First&nbsp;&nbsp;|");
				sb.append("</A>");
				
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				//sb.append("<");
				sb.append("&nbsp;&nbsp;<&lt;Previous&nbsp;&nbsp;|");
				sb.append("</A>");
			
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			//
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				//sb.append(">");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				//sb.append(">>");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize1' id='pageSize1' style='width: 60px; height:30px; color: #555555;' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		
		//System.out.println(sb.toString());
		return sb.toString();
	}
	
	
	public static String getPaginationStringPnr(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{
		
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingPnr('1') \">");
				//sb.append("<<");
				sb.append("First&nbsp;&nbsp;|");
				sb.append("</A>");
				
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingPnr('"+(pageNo-1)+"') \" >");
				//sb.append("<");
				sb.append("&nbsp;&nbsp;<&lt;Previous&nbsp;&nbsp;|");
				sb.append("</A>");
			
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			//
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPagingPnr('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingPnr('"+(pageNo+1)+"') \"  >");
				//sb.append(">");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingPnr('"+(noOfPage)+"') \"   >");
				//sb.append(">>");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px; height:30px; color: #555555;' onchange=\"getPagingPnr('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		
		//System.out.println(sb.toString());
		return sb.toString();
	}
	
}
