package tm.api.controller;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.engine.TypedValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import edu.emory.mathcs.backport.java.util.Arrays;

import tm.api.UtilityAPI;
import tm.api.services.ServiceUtility;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.EligibilityVerificationHistroy;
import tm.bean.EmployeeMaster;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.Jobrequisitionnumberstemp;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.GeoMapping;
import tm.bean.master.RaceMaster;
import tm.bean.master.RegionMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SchoolStandardMaster;
import tm.bean.master.SchoolTypeMaster;
import tm.bean.master.StateMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.EligibilityVerificationHistroyDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.JobRequisitionNumbersTempDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.GeoMappingDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.RegionMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SchoolStandardMasterDAO;
import tm.dao.master.SchoolTypeMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.es.ElasticSearchService;
import tm.services.teacher.AcceptOrDeclineMailHtml;
import static tm.services.district.GlobalServices.*;
import tm.utility.JDBCFunction;
import tm.utility.Utility;
import net.sf.json.JSONArray;

@Controller
public class JeffcoAPIController {

	@Autowired 
 	private SchoolStandardMasterDAO schoolStandardMasterDAO;
 	public void setStandardMasterDAO(SchoolStandardMasterDAO schoolStandardMasterDAO) {
		this.schoolStandardMasterDAO = schoolStandardMasterDAO;
	}
 	
 	@Autowired
    private RegionMasterDAO regionMasterDAO;
    public void setRegionMasterDAO(RegionMasterDAO regionMasterDAO) {
		this.regionMasterDAO = regionMasterDAO;
	}
    
    @Autowired
    private GeoMappingDAO geoMappingDAO;
    public void setGeoMappingDAO(GeoMappingDAO geoMappingDAO) {
		this.geoMappingDAO = geoMappingDAO;
	}
 	
 	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	public void setEmployeeMasterDAO(EmployeeMasterDAO employeeMasterDAO){
		this.employeeMasterDAO = employeeMasterDAO;
	}
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO){
		this.schoolMasterDAO = schoolMasterDAO;
	}
	
	@Autowired
	private RoleMasterDAO roleMasterDAO;
	public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO){
		this.roleMasterDAO = roleMasterDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	 public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
			this.districtMasterDAO = districtMasterDAO;
		}
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private SchoolTypeMasterDAO schoolTypeMasterDAO;
	public void setSchoolTypeMasterDAO(SchoolTypeMasterDAO schoolTypeMasterDAO) {
		this.schoolTypeMasterDAO = schoolTypeMasterDAO;
	}

	@Autowired
	private StateMasterDAO stateMasterDAO;
    public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}

	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	public void setDistrictRequisitionNumbersDAO(
			DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO) {
		this.districtRequisitionNumbersDAO = districtRequisitionNumbersDAO;
	}
	
	@Autowired
    private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	public void setJobRequisitionNumbersDAO(
			JobRequisitionNumbersDAO jobRequisitionNumbersDAO) {
		this.jobRequisitionNumbersDAO = jobRequisitionNumbersDAO;
	}
	
	@Autowired UserMasterDAO userMasterDAO;
    public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
    
    @Autowired SchoolInJobOrderDAO schoolInJobOrderDAO;
    public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
    
    @Autowired
    private DistrictSchoolsDAO districtSchoolsDAO;
    
    @Autowired
    private JobRequisitionNumbersTempDAO jobRequisitionNumbersTempDAO;
    
    @Autowired
	private EmailerService emailerService;
    
    @Autowired
    private JobForTeacherDAO jobForTeacherDAO;
    
    public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	public static String getEmployeeInfoByEmployeeNumber(String empId) throws Exception
    {      

    	String url = Utility.getValueOfPropByKey("productionUrlOrTest")+"/PSIGW/RESTListeningConnector/PSFT_HR/JC_TM_EMPINFO.v1/EMPLID="+empId;//production server
        //String url = "https://jcspatst.jeffco.k12.co.us/PSIGW/RESTListeningConnector/PSFT_HR/JC_TM_EMPINFO.v1/EMPLID="+empId;//136077//test Server
        URL obj = new URL(url); 
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection(); 
        //add request header 
	        String method = "GET"; 
	        con.setRequestMethod(method);
		        con.setRequestProperty("User-Agent", "");
		        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		        //con.setRequestProperty("Authorization", "Basic " + "VEVBQ0hNQVQ6QjNuJkozcnJ5cw=="); //test server
		        con.setRequestProperty("Authorization", "Basic " + Utility.getValueOfPropByKey("passwordAPIURL"));//production
			        int responseCode = con.getResponseCode();
			        System.out.println("\nSending '" + method + "' request to URL : " + url);
			        System.out.println("Response Code : " + responseCode); 
	        String response= printHTTPResults(con);
	        System.out.println("Response Table=="+response);
	        return response;
   }

   private static synchronized String printHTTPResults(HttpsURLConnection con) {
	        BufferedReader in = null; 
	        StringBuffer response = new StringBuffer();
	        StringBuffer sb = new StringBuffer();
	        	try
		        {
			        in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			        String inputLine;
			        while ((inputLine = in.readLine()) != null)
			        { response.append(inputLine); }
			        in.close();
			        //print result	
			        //Start make a table for response data
			        //Html table div
	        		 //System.out.println("entyer     66         o[poi[poi[");
			        	/*String response1="{"+
			        	 "\"JC_TM_EMPLINFO_RESP_DOC\":"+ 
			        	  "{\"Emplid\": \"136077\",\"NamePrefix\": \"Ms\",\"LastName\": \"Leal\",\"FirstName\": \"Deborah\",\"Sex\": \"F\","+
			        	   "\"Ethnicity\":"+
			        	        "[{\"EthnicityCd\": \"WHITE\"}],"+
			        	   "\"EmpInfoAddress\":"+
			        	        "[{\"AddressType\": \"HOME\",\"Address1\": \"8000 W Crestline Ave\",\"Address2\": \"Apt 1021\",\"City\": \"Littleton\",\"State\": \"CO\",\"Postal\": \"80123\",\"Country\": \"USA\"}],"+
			        	   "\"EmpInfoPhone\":"+
			        	       "[{\"PhoneType\": \"CELL\",\"Phone\": \"303725-9042\",\"Preferred\": \"N\"}],"+
			        	   "\"EmpInfoDegrees\":"+
			        	       "[{\"DateAcquired\": \"\",\"Degree\": \"\",\"DegreeDescr\": \"\",\"Major\": \"\",\"MajorDescr\": \"\",\"State\": \"\",\"StateDescr\": \"\",\"School\": \"\",\"SchoolDescr\": \"\",\"Graduated\": \"\"}],"+			        	  		        	       
			        	  "\"EmpInfoLicEnd\": "+
								"["+
								  "{\"LicenseEffdt\": \"2014-09-26\",\"License\": \"CCPI\",\"LicenseDescr\": \"CPI Non-Violent Crisis Interv\",\"State\": \"\",\"StateDescr\": \"\",\"ExpirationDate\": \"2016-09-26\",\"LicenseNumber\": \"\",\"IssuedBy\": \"\","+
								   "\"EmpInfoEnd\":"+
								     	"[{\"EndorsementEffdt\": \"\",\"Endorsement\": \"\",\"EndorsementDescr\": \"\"}]"+  //
								  	"},"+
								  	"{\"LicenseEffdt\": \"2012-05-24\",\"License\": \"CLTPF\",\"LicenseDescr\": \"Professional License, Teacher\",\"State\": \"CO\",\"StateDescr\": \"Colorado\",\"ExpirationDate\": \"2017-05-24\",\"LicenseNumber\": \"15301\",\"IssuedBy\": \"CDE\","+
								   "\"EmpInfoEnd\":"+
								    	"[{\"EndorsementEffdt\": \"2012-05-24\",\"Endorsement\": \"E180100\",\"EndorsementDescr\": \"Elementary Education\"},"+
										 "{\"EndorsementEffdt\": \"2012-05-24\",\"Endorsement\": \"E198110\",\"EndorsementDescr\": \"Moderate Needs\"},"+
										 "{\"EndorsementEffdt\": \"2012-05-24\",\"Endorsement\": \"E198130\",\"EndorsementDescr\": \"Severe Needs - Affective\"}"+ 
										"]"+
									 "}"+
								"],"+
			        	   "\"EmpInfoHighlyQualCol\":"+ 
			        	       	"[{\"ContentType\": \"\",\"Effdt\": \"\",\"ItemID\": \"\",\"Descr\": \"\"}],"+
			        	   	"\"EmpInfoSpedHighlyQualCol\":"+
			        	       	"[{\"ContentType\": \"\",\"Effdt\": \"\",\"ItemID\": \"\",\"Descr\": \"\"}],"+
			        	   	"\"EmpInfoSchoolEd\": "+
			        	       	"[{\"Effdt\": \"\",\"EducationLevel\": \"\",\"EducationLevelDescr\": \"\",\"State\": \"\",\"StateDescr\": \"\",\"SchoolType\": \"\",\"SchoolDescr\": \"\",\"Completed\": \"\"}]"+
			        	   	"}"+
			        	 "}";
			        	*/
			        	System.out.println("response1============="+response);
					        JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON(response.toString());
				        
			        		JSONObject allInfo = jsonRequest.getJSONObject("JC_TM_EMPLINFO_RESP_DOC");
				        	String Emplid = allInfo.get("Emplid")==null?"":UtilityAPI.parseString(allInfo.getString("Emplid"));
				        	String NamePrefix = allInfo.get("NamePrefix")==null?"":UtilityAPI.parseString(allInfo.getString("NamePrefix"));
				        	String FirstName = allInfo.get("FirstName")==null?"":UtilityAPI.parseString(allInfo.getString("FirstName"));
				        	String LastName = allInfo.get("LastName")==null?"":UtilityAPI.parseString(allInfo.getString("LastName"));
				        	String Sex = allInfo.get("Sex")==null?"":UtilityAPI.parseString(allInfo.getString("Sex"));
				        	
				        		JSONArray jsonArrayEthnicity =allInfo.getJSONArray("Ethnicity");
				        		JSONObject jsonEthnicity=(JSONObject) jsonArrayEthnicity.get(0);
				        		String EthnicityCd = jsonEthnicity.get("EthnicityCd")==null?"":UtilityAPI.parseString(jsonEthnicity.getString("EthnicityCd"));
				        		
						        	JSONArray jsonArrayEmpInfoAddress =allInfo.getJSONArray("EmpInfoAddress");
						        		JSONObject jsonEmpInfoAddress=(JSONObject) jsonArrayEmpInfoAddress.get(0);
						        		String AddressType = jsonEmpInfoAddress.get("AddressType")==null?"":UtilityAPI.parseString(jsonEmpInfoAddress.getString("AddressType"));
						        		String Address1 = jsonEmpInfoAddress.get("Address1")==null?"":UtilityAPI.parseString(jsonEmpInfoAddress.getString("Address1"));
						        		String Address2 = jsonEmpInfoAddress.get("Address2")==null?"":UtilityAPI.parseString(jsonEmpInfoAddress.getString("Address2"));
						        		String City = jsonEmpInfoAddress.get("City")==null?"":UtilityAPI.parseString(jsonEmpInfoAddress.getString("City"));
						        		String State = jsonEmpInfoAddress.get("State")==null?"":UtilityAPI.parseString(jsonEmpInfoAddress.getString("State"));
						        		String Postal = jsonEmpInfoAddress.get("Postal")==null?"":UtilityAPI.parseString(jsonEmpInfoAddress.getString("Postal"));
						        		String Country = jsonEmpInfoAddress.get("Country")==null?"":UtilityAPI.parseString(jsonEmpInfoAddress.getString("Country"));
						        	
							        	JSONArray jsonArrayEmpInfoPhone =allInfo.getJSONArray("EmpInfoPhone");
							        		JSONObject jsonEmpInfoPhone=(JSONObject) jsonArrayEmpInfoPhone.get(0);
							        		String PhoneType = jsonEmpInfoPhone.get("PhoneType")==null?"":UtilityAPI.parseString(jsonEmpInfoPhone.getString("PhoneType"));
							        		String Phone = jsonEmpInfoPhone.get("Phone")==null?"":UtilityAPI.parseString(jsonEmpInfoPhone.getString("Phone"));
							        		String Preferred = jsonEmpInfoPhone.get("Preferred")==null?"":UtilityAPI.parseString(jsonEmpInfoPhone.getString("Preferred"));
							        	
								        	JSONArray jsonArrayEmpInfoDegrees =allInfo.getJSONArray("EmpInfoDegrees");
									        	String[] DateAcquired = new String[jsonArrayEmpInfoDegrees.size()]; 
								        		String[] Degree = new String[jsonArrayEmpInfoDegrees.size()]; 
								        		String[] Major = new String[jsonArrayEmpInfoDegrees.size()]; 
								        		String[] MajorDescr = new String[jsonArrayEmpInfoDegrees.size()]; 
								        		String[] State1 = new String[jsonArrayEmpInfoDegrees.size()]; 
								        		String[] StateDescr = new String[jsonArrayEmpInfoDegrees.size()]; 
								        		String[] School = new String[jsonArrayEmpInfoDegrees.size()]; 
								        		String[] SchoolDescr = new String[jsonArrayEmpInfoDegrees.size()]; 
								        		String[] Graduated =new String[jsonArrayEmpInfoDegrees.size()]; 
									        	for(int i=0;i<jsonArrayEmpInfoDegrees.size();i++){
									        		JSONObject jsonEmpInfoDegrees=(JSONObject) jsonArrayEmpInfoDegrees.get(i);
									        		DateAcquired[i] = jsonEmpInfoDegrees.get("DateAcquired")==null?"":UtilityAPI.parseString(jsonEmpInfoDegrees.getString("DateAcquired"));
									        		Degree[i] = jsonEmpInfoDegrees.get("Degree")==null?"":UtilityAPI.parseString(jsonEmpInfoDegrees.getString("Degree"));
									        		Major[i] = jsonEmpInfoDegrees.get("Major")==null?"":UtilityAPI.parseString(jsonEmpInfoDegrees.getString("Major"));
									        		MajorDescr[i] = jsonEmpInfoDegrees.get("MajorDescr")==null?"":UtilityAPI.parseString(jsonEmpInfoDegrees.getString("MajorDescr"));
									        		State1[i] = jsonEmpInfoDegrees.get("State")==null?"":UtilityAPI.parseString(jsonEmpInfoDegrees.getString("State"));
									        		StateDescr[i] = jsonEmpInfoDegrees.get("StateDescr")==null?"":UtilityAPI.parseString(jsonEmpInfoDegrees.getString("StateDescr"));
									        		School[i] = jsonEmpInfoDegrees.get("School")==null?"":UtilityAPI.parseString(jsonEmpInfoDegrees.getString("School"));
									        		SchoolDescr[i] = jsonEmpInfoDegrees.get("SchoolDescr")==null?"":UtilityAPI.parseString(jsonEmpInfoDegrees.getString("SchoolDescr"));
									        		Graduated[i] = jsonEmpInfoDegrees.get("Graduated")==null?"":UtilityAPI.parseString(jsonEmpInfoDegrees.getString("Graduated"));
									        	}
								        	
									        	JSONArray jsonArrayEmpInfoLicEnd = allInfo.getJSONArray("EmpInfoLicEnd");
									        	String[] LicenseEffdt=new String[jsonArrayEmpInfoLicEnd.size()];
									        	String[] License=new String[jsonArrayEmpInfoLicEnd.size()];
									        	String[] LicenseDescr=new String[jsonArrayEmpInfoLicEnd.size()];
									        	String[] State2=new String[jsonArrayEmpInfoLicEnd.size()];
									        	String[] StateDescr1=new String[jsonArrayEmpInfoLicEnd.size()];
									        	String[] ExpirationDate=new String[jsonArrayEmpInfoLicEnd.size()];
									        	String[] LicenseNumber=new String[jsonArrayEmpInfoLicEnd.size()];
									        	String[] IssuedBy=new String[jsonArrayEmpInfoLicEnd.size()];
									        	Map<Integer,Map<Integer,List<String>>> lstEmpInfoEnd=new LinkedHashMap<Integer,Map<Integer,List<String>>>();
									        	Map<Integer,List<String>> map=new LinkedHashMap<Integer,List<String>>();
									        	List<String> lst=new ArrayList<String>();
									        	for(int i=0;i<jsonArrayEmpInfoLicEnd.size();i++){
								        			JSONObject jsonEmpInfoLicEnd=(JSONObject) jsonArrayEmpInfoLicEnd.get(i);
									        		LicenseEffdt[i] = jsonEmpInfoLicEnd.get("LicenseEffdt")==null?"":UtilityAPI.parseString(jsonEmpInfoLicEnd.getString("LicenseEffdt"));
									        		License[i] = jsonEmpInfoLicEnd.get("License")==null?"":UtilityAPI.parseString(jsonEmpInfoLicEnd.getString("License"));
									        		LicenseDescr[i] = jsonEmpInfoLicEnd.get("LicenseDescr")==null?"":UtilityAPI.parseString(jsonEmpInfoLicEnd.getString("LicenseDescr"));
									        		State2[i] = jsonEmpInfoLicEnd.get("State")==null?"":UtilityAPI.parseString(jsonEmpInfoLicEnd.getString("State"));
									        		StateDescr1[i] = jsonEmpInfoLicEnd.get("StateDescr")==null?"":UtilityAPI.parseString(jsonEmpInfoLicEnd.getString("StateDescr"));
									        		ExpirationDate[i] = jsonEmpInfoLicEnd.get("ExpirationDate")==null?"":UtilityAPI.parseString(jsonEmpInfoLicEnd.getString("ExpirationDate"));
									        		LicenseNumber[i] = jsonEmpInfoLicEnd.get("LicenseNumber")==null?"":UtilityAPI.parseString(jsonEmpInfoLicEnd.getString("LicenseNumber"));
									        		IssuedBy[i] = jsonEmpInfoLicEnd.get("IssuedBy")==null?"":UtilityAPI.parseString(jsonEmpInfoLicEnd.getString("IssuedBy"));
								        		
								        				String EmpInfoEnd = jsonEmpInfoLicEnd.get("EmpInfoEnd")==null?"":UtilityAPI.parseString(jsonEmpInfoLicEnd.getString("EmpInfoEnd"));
								        				JSONArray jsonArrayEmpInfoEnd =null;
								        				if(!EmpInfoEnd.equals(""))
								        					jsonArrayEmpInfoEnd =jsonEmpInfoLicEnd.getJSONArray("EmpInfoEnd");
								        				map=new LinkedHashMap<Integer,List<String>>();
								        				if(jsonArrayEmpInfoEnd!=null)
								        				for(int j=0;j<jsonArrayEmpInfoEnd.size();j++){
								        				lst=new ArrayList<String>();
										        		JSONObject jsonEmpInfoEnd=(JSONObject) jsonArrayEmpInfoEnd.get(j);
										        		lst.add(jsonEmpInfoEnd.get("EndorsementEffdt")==null?"":UtilityAPI.parseString(jsonEmpInfoEnd.getString("EndorsementEffdt")));
										        		lst.add(jsonEmpInfoEnd.get("Endorsement")==null?"":UtilityAPI.parseString(jsonEmpInfoEnd.getString("Endorsement")));
										        		lst.add(jsonEmpInfoEnd.get("EndorsementDescr")==null?"":UtilityAPI.parseString(jsonEmpInfoEnd.getString("EndorsementDescr")));
										        		map.put(j, lst);
								        				}
								        				lstEmpInfoEnd.put(i, map);
									        	}
								        				
								        				
												        	JSONArray jsonArrayEmpInfoHighlyQualCol = allInfo.getJSONArray("EmpInfoHighlyQualCol");
												        	String[] ContentType =new String[jsonArrayEmpInfoHighlyQualCol.size()]; 
											        		String[] Effdt = new String[jsonArrayEmpInfoHighlyQualCol.size()];
											        		String[] ItemID = new String[jsonArrayEmpInfoHighlyQualCol.size()];
											        		String[] Descr=new String[jsonArrayEmpInfoHighlyQualCol.size()];
												        	for(int i=0;i<jsonArrayEmpInfoHighlyQualCol.size();i++){
											        		JSONObject jsonEmpInfoHighlyQualCol=(JSONObject) jsonArrayEmpInfoHighlyQualCol.get(i);
											        		ContentType[i] = jsonEmpInfoHighlyQualCol.get("ContentType")==null?"":UtilityAPI.parseString(jsonEmpInfoHighlyQualCol.getString("ContentType"));
											        		Effdt[i] = jsonEmpInfoHighlyQualCol.get("Effdt")==null?"":UtilityAPI.parseString(jsonEmpInfoHighlyQualCol.getString("Effdt"));
											        		ItemID[i] = jsonEmpInfoHighlyQualCol.get("ItemID")==null?"":UtilityAPI.parseString(jsonEmpInfoHighlyQualCol.getString("ItemID"));
											        		Descr [i]= jsonEmpInfoHighlyQualCol.get("Descr")==null?"":UtilityAPI.parseString(jsonEmpInfoHighlyQualCol.getString("Descr"));
												        	}
												        	
													        	JSONArray jsonArrayEmpInfoSpedHighlyQualCol = allInfo.getJSONArray("EmpInfoSpedHighlyQualCol");
													        	String[] ContentType1 =new String[jsonArrayEmpInfoSpedHighlyQualCol.size()]; 
												        		String[] Effdt1 = new String[jsonArrayEmpInfoSpedHighlyQualCol.size()];
												        		String[] ItemID1= new String[jsonArrayEmpInfoSpedHighlyQualCol.size()];
												        		String[] Descr1=new String[jsonArrayEmpInfoSpedHighlyQualCol.size()];
													        	for(int i=0;i<jsonArrayEmpInfoSpedHighlyQualCol.size();i++){
													        		JSONObject jsonEmpInfoSpedHighlyQualCol=(JSONObject) jsonArrayEmpInfoSpedHighlyQualCol.get(i);
													        		ContentType1[i] = jsonEmpInfoSpedHighlyQualCol.get("ContentType")==null?"":UtilityAPI.parseString(jsonEmpInfoSpedHighlyQualCol.getString("ContentType"));
													        		Effdt1[i] = jsonEmpInfoSpedHighlyQualCol.get("Effdt")==null?"":UtilityAPI.parseString(jsonEmpInfoSpedHighlyQualCol.getString("Effdt"));
													        		ItemID1[i] = jsonEmpInfoSpedHighlyQualCol.get("ItemID")==null?"":UtilityAPI.parseString(jsonEmpInfoSpedHighlyQualCol.getString("ItemID"));
													        		Descr1[i] = jsonEmpInfoSpedHighlyQualCol.get("Descr")==null?"":UtilityAPI.parseString(jsonEmpInfoSpedHighlyQualCol.getString("Descr"));
													        	}
													        	
													        		JSONArray jsonArrayEmpInfoSchoolEd = allInfo.getJSONArray("EmpInfoSchoolEd");
													        		String[] Effdt2 = new String[jsonArrayEmpInfoSchoolEd.size()]; 
													        		String[] EducationLevel = new String[jsonArrayEmpInfoSchoolEd.size()]; 
													        		String[] EducationLevelDescr = new String[jsonArrayEmpInfoSchoolEd.size()]; 
													        		String[] State3 = new String[jsonArrayEmpInfoSchoolEd.size()]; 
													        		String[] StateDescr2 = new String[jsonArrayEmpInfoSchoolEd.size()]; 
													        		String[] SchoolType2 = new String[jsonArrayEmpInfoSchoolEd.size()]; 
													        		String[] SchoolDescr2 = new String[jsonArrayEmpInfoSchoolEd.size()]; 
													        		String[] Completed =new String[jsonArrayEmpInfoSchoolEd.size()]; 
													        		for(int i=0;i<jsonArrayEmpInfoSchoolEd.size();i++){	
														        		JSONObject jsonEmpInfoSchoolEd=(JSONObject) jsonArrayEmpInfoSchoolEd.get(i);
														        		 Effdt2[i] = jsonEmpInfoSchoolEd.get("Effdt")==null?"":UtilityAPI.parseString(jsonEmpInfoSchoolEd.getString("Effdt"));
														        		 EducationLevel[i] = jsonEmpInfoSchoolEd.get("EducationLevel")==null?"":UtilityAPI.parseString(jsonEmpInfoSchoolEd.getString("EducationLevel"));
														        		EducationLevelDescr[i] = jsonEmpInfoSchoolEd.get("EducationLevelDescr")==null?"":UtilityAPI.parseString(jsonEmpInfoSchoolEd.getString("EducationLevelDescr"));
														        		State3[i] = jsonEmpInfoSchoolEd.get("State")==null?"":UtilityAPI.parseString(jsonEmpInfoSchoolEd.getString("State"));
														        		StateDescr2[i] = jsonEmpInfoSchoolEd.get("StateDescr")==null?"":UtilityAPI.parseString(jsonEmpInfoSchoolEd.getString("StateDescr"));
														        		SchoolType2[i] = jsonEmpInfoSchoolEd.get("SchoolType")==null?"":UtilityAPI.parseString(jsonEmpInfoSchoolEd.getString("SchoolType"));
														        		SchoolDescr2[i] = jsonEmpInfoSchoolEd.get("SchoolDescr")==null?"":UtilityAPI.parseString(jsonEmpInfoSchoolEd.getString("SchoolDescr"));
														        		Completed[i] = jsonEmpInfoSchoolEd.get("Completed")==null?"":UtilityAPI.parseString(jsonEmpInfoSchoolEd.getString("Completed"));											        		
													        		}
		                List<String> content=null;
		                String TMCSS_FONT_SIZE = "font-family: Century Gothic,Open Sans, sans-serif;font-size: 14px;width:95%;border:1px solid #007FB2;";
		                sb.append("<style>.leftAlign{text-align:left;} .tableCSS tr td{border:1px solid #007FB2;background-color:white;font-weight: normal;}.tableCSS tr th{border:1px solid white;}</style>");
		                //sb.append("<script>$(function(){try{setTimeout(function(){ alert('enter');$('#jsonTable').parents().parents('.modal-content').parents('.modal-dialog').css('width','850px');},100);	}catch(e){}});</script>");
		                //System.out.println("sb==============="+sb.toString());
			        	sb.append(getTMTableStartWithId("jsonTable", TMCSS_FONT_SIZE));
			        	//System.out.println("JC_TM_EMPLINFO_RESP_DOC======"+JC_TM_EMPLINFO_RESP_DOC);
			        	sb.append(getTrHeaderHTML("Employee Information", 3, "", "", "background-color:#007FB2;color:white;"));
			        	
			        	//System.out.println("Emplid: "+Emplid+"\nName: "+NamePrefix+" "+FirstName+" "+LastName+"\nSex: "+Sex);
			        	content=new ArrayList<String>();content.add("Employee Number");content.add(":");content.add(Emplid);
			        	sb.append(getMultipleTdOfTrWithCssHTML(content,"leftAlign","",""));
			        	
			        	content=new ArrayList<String>();content.add("Name");content.add(":");content.add(NamePrefix+" "+FirstName+" "+LastName);
			        	sb.append(getMultipleTdOfTrWithCssHTML(content,"leftAlign","",""));
			        	
			        	//TPL-1800 (Do NOT Display:�	Sex      	�	Ethnicity )
			        	/*content=new ArrayList<String>();content.add("Sex");content.add(":");content.add(Sex);
			        	sb.append(getMultipleTdOfTrWithCssHTML(content,"leftAlign","",""));
			        	
			        	content=new ArrayList<String>();content.add("Ethnicity");content.add(":");content.add(EthnicityCd);
			        	sb.append(getMultipleTdOfTrWithCssHTML(content,"leftAlign","",""));*/
			        	//System.out.println("EthnicityCd: "+EthnicityCd);
			        	
			        	
			        	//System.out.println("AddressType: "+AddressType+"\nAddress1: "+Address1+"\nAddress2: "+Address2+"\nCity: "+City+"\nState: "+State+"\nPostal: "+Postal+"\nCountry: "+Country);//+"\n"+
			        	
			        	sb.append(getTrHeaderHTML("Employee Address", 3, "leftAlign", "", "background-color:#007FB2;color:white;"));
			        	
			        	
			        	content=new ArrayList<String>();content.add("Address 1");content.add(":");content.add(Address1);
			        	sb.append(getMultipleTdOfTrWithCssHTML(content,"leftAlign jeffCoApiAdd1","",""));
			        	
			        	content=new ArrayList<String>();content.add("Address 2");content.add(":");content.add(Address2);
			        	sb.append(getMultipleTdOfTrWithCssHTML(content,"leftAlign jeffCoApiAdd2","",""));
			        	
			        	content=new ArrayList<String>();content.add("City");content.add(":");content.add(City);
			        	sb.append(getMultipleTdOfTrWithCssHTML(content,"leftAlign jeffCoApiCity","",""));
			        	
			        	content=new ArrayList<String>();content.add("State");content.add(":");content.add(State);
			        	sb.append(getMultipleTdOfTrWithCssHTML(content,"leftAlign jeffCoApiState","",""));
			        	
			        	content=new ArrayList<String>();content.add("Postal");content.add(":");content.add(Postal);
			        	sb.append(getMultipleTdOfTrWithCssHTML(content,"leftAlign jeffCoApiPostal","",""));
			        	
			        	content=new ArrayList<String>();content.add("Country");content.add(":");content.add(Country);
			        	sb.append(getMultipleTdOfTrWithCssHTML(content,"leftAlign jeffCoApiCountry","",""));
			        	
			        	
			        	//System.out.println("PhoneType: "+PhoneType+"\nPhone: "+Phone+"\nPreferred: "+Preferred);//+"\n"+	
			        	
			        	sb.append(getTrHeaderHTML("Employee Phone", 3, "leftAlign", "", "background-color:#007FB2;color:white;"));
			        	
			        	
			        	content=new ArrayList<String>();content.add("Phone");content.add(":");content.add(Phone);
			        	sb.append(getMultipleTdOfTrWithCssHTML(content,"leftAlign","",""));
			        	
			        	content=new ArrayList<String>();content.add("Preferred");content.add(":");content.add(Preferred);
			        	sb.append(getMultipleTdOfTrWithCssHTML(content,"leftAlign","",""));
			        	
			        	
			        	//System.out.println("DateAcquired: "+DateAcquired+"\nDegree: "+Degree+"\nMajor: "+Major+"\nMajorDescr: "+MajorDescr+"\nState1: "+State1+"\nStateDescr: "+StateDescr+"\nSchool: "+School+"\nSchoolDescr: "+SchoolDescr+"\nGraduated: "+Graduated);//+"\n"+
			        	//Do NOT Display: 	�	Major (number)	�	School (Number) 
			        	sb.append(getTrHeaderHTML("Employee Degrees", 3, "leftAlign", "", "background-color:#007FB2;color:white;"));
			        	
			        	StringBuffer sbDegree=new StringBuffer();
			        	sbDegree.append(getTMTableStartWithClassAndId("table tableCSS", "tblEmployeeDegree", ""));
			        	sbDegree.append("<thead class='bg'>");
			        	List<String> jsonData=new ArrayList<String>();
			        	jsonData.add("Date Acquired");
			        	jsonData.add("Degree");
			        	jsonData.add("Major Description");
			        	jsonData.add("State");
			        	jsonData.add("State Description");
			        	jsonData.add("School  Description");
			        	jsonData.add("Graduated");			        	
			        	sbDegree.append(getMultipleThOfTrWithCssHTML(jsonData, "background-color:#007FB2;color:white;", "", ""));
			        	if(DateAcquired.length>0){
			        	for(int i=0;i<DateAcquired.length;i++){
			        		jsonData=new ArrayList<String>();
				        	jsonData.add(DateAcquired[i]);
				        	jsonData.add(Degree[i]);
				        	jsonData.add(MajorDescr[i]);
				        	jsonData.add(State1[i]);
				        	jsonData.add(StateDescr[i]);
				        	jsonData.add(SchoolDescr[i]);
				        	jsonData.add(Graduated[i]);
				        	sbDegree.append(getMultipleTdOfTrWithCssHTML(jsonData, "", "", ""));
			        	}}else{
			        		sbDegree.append(getTdOfTrHTML("No Record Found",7,"","",""));
				        }
			        	sbDegree.append(getTMTableEnd());
			        	sb.append(getTdOfTrHTML(sbDegree.toString(),3,"","","padding:10px;"));
			        	
			        	//EmpInfoLicEnd
			        	//System.out.println("LicenseEffdt: "+LicenseEffdt+"\nLicense: "+License+"\nLicenseDescr: "+LicenseDescr+"\nState2: "+State2+"\nStateDescr1: "+StateDescr1+"\nExpirationDate: "+ExpirationDate+"\nLicenseNumber:"+LicenseNumber+"\nIssuedBy: "+IssuedBy);//+"\n"+
			        	//Do NOT Display: �	Endorsement(number)
			        	sb.append(getTrHeaderHTML("Employee License/Certifications", 3, "leftAlign", "", "background-color:#007FB2;color:white;"));
			        	
			        	StringBuffer sbLicense=new StringBuffer();
			        	sbLicense.append(getTMTableStartWithClassAndId("table tableCSS", "tblEmployeeLicense", ""));
			        	sbLicense.append("<thead class='bg'>");
			        	jsonData=new ArrayList<String>();
			        	jsonData.add("License Effect Date");
			        	jsonData.add("License");
			        	jsonData.add("License Description");
			        	jsonData.add("State");
			        	jsonData.add("State Description");
			        	jsonData.add("Expiration Date");
			        	jsonData.add("License Number");
			        	jsonData.add("Issued By");
			        	jsonData.add("Employee Inforamation End");
			        	sbLicense.append(getMultipleThOfTrWithCssHTML(jsonData, "background-color:#007FB2;color:white;", "", ""));
			        	sbLicense.append("</thead>");
			        	if(LicenseEffdt.length>0){
				        for(int i=0;i<LicenseEffdt.length;i++){
				        	jsonData=new ArrayList<String>();
				        	jsonData.add(LicenseEffdt[i]);
				        	jsonData.add(License[i]);
				        	jsonData.add(LicenseDescr[i]);
				        	jsonData.add(State2[i]);
				        	jsonData.add(StateDescr1[i]);
				        	jsonData.add(ExpirationDate[i]);
				        	jsonData.add(LicenseNumber[i]);
				        	jsonData.add(IssuedBy[i]);
				        	
				        	//System.out.println("EndorsementEffdt: "+EndorsementEffdt+"\nEndorsement: "+Endorsement+"\nEndorsementDescr: "+EndorsementDescr);//+"\n"+
				        	StringBuffer sbChildTable=new StringBuffer();
				        	Map<Integer,List<String>> map1=lstEmpInfoEnd.get(i);
				        	sbChildTable.append(getTMTableStartWithId(""+i, "border:1px solid;border-color:#007FB2;"));
			        		List<String> jsonData1=new ArrayList<String>();
				        	jsonData1.add("Endorsement Effect Date");
				        	//jsonData1.add("Endorsement");
				        	jsonData1.add("Endorsement Description");
				        	sbChildTable.append(getMultipleThOfTrWithCssHTML(jsonData1, "background-color:#007FB2;color:white;", "", ""));
				        	
				        	StringBuffer sbChildTd=new StringBuffer();
				        	String clickableView="";
				        	String anchorLabel="";
				        	if(map1!=null && map1.size()>0){
				        		for(Entry<Integer, List<String>> entry:map1.entrySet()){
				        			List<String> lst1=entry.getValue();
				        			jsonData1=new ArrayList<String>();
				        			jsonData1.add(lst1.get(0));
				        			//jsonData1.add(lst1.get(1));
				        			jsonData1.add(lst1.get(2));
				        			sbChildTd.append(getMultipleTdOfTrWithCssHTML(jsonData1, "", "", ""));
				        		}
				        	}
				        	if(!sbChildTd.toString().trim().equalsIgnoreCase("")){
				        		sbChildTable.append(sbChildTd.toString());
				        		clickableView="onclick='openEmpInfoEnd(\"id"+i+"\");'";
				        		anchorLabel="View";
				        	}else{
				        		sbChildTable.append(getTdOfTrHTML("No Record Found",2,"","",""));
				        	}
				        	
				        	sbChildTable.append(getTMTableEnd());
				        		
				        		String modal=getModal("id"+i, "Employee Information End", "Ok", sbChildTable.toString());
				        		jsonData.add(modal+getAnchorTag(clickableView,"javascript:void(0)",anchorLabel));
					        	sbLicense.append(getMultipleTdOfTrWithCssHTML(jsonData, "", "", ""));
				        }
			        	}else{
			        		sbLicense.append(getTdOfTrHTML("No Record Found",9,"","",""));
				        }
			        	sbLicense.append("<script>function openEmpInfoEnd(openModalId){$('#'+openModalId).css('margin-top','150px');$('#'+openModalId).show();}</script>");
			        sbLicense.append(getTMTableEnd());
			        sb.append(getTdOfTrHTML(sbLicense.toString(),3,"","","padding:10px;"));
			        	
			        
			        	//System.out.println("ContentType: "+ContentType+"\nEffdt: "+Effdt+"\nItemID: "+ItemID+"\nDescr: "+Descr);//+"\n"+
			        	//EmpInfoHighlyQualCol
			        	//Do NOT Display: �	Content Type �	Item ID
			        	sb.append(getTrHeaderHTML("Employee Highly Qualified Information", 3, "leftAlign", "", "background-color:#007FB2;color:white;"));
			        	StringBuffer sbEHQI=new StringBuffer();
			        	sbEHQI.append(getTMTableStartWithClassAndId("table tableCSS", "tblEmployeeEHQI", ""));
			        	sbEHQI.append("<thead class='bg'>");
			        	jsonData=new ArrayList<String>();
			        	jsonData.add("Effect Date");
			        	jsonData.add("Description");
			        	sbEHQI.append(getMultipleThOfTrWithCssHTML(jsonData, "background-color:#007FB2;color:white;", "", ""));
			        	sbEHQI.append("</thead>");
			        	for(int i=0;i<ContentType.length;i++){
			        		jsonData=new ArrayList<String>();
			        		jsonData.add(Effdt[i]);
			        		jsonData.add(Descr[i]);
			        		sbEHQI.append(getMultipleTdOfTrWithCssHTML(jsonData,"","",""));
			        	}
			        	sbEHQI.append(getTMTableEnd());
				        sb.append(getTdOfTrHTML(sbEHQI.toString(),3,"","","padding:10px;"));
			        	
			        	//System.out.println("ContentType1: "+ContentType1+"\nEffdt1: "+Effdt1+"\nItemID1: "+ItemID1+"\nDescr1: "+Descr1);//+"\n"+
			        	//EmpInfoHighlyQualCol
			        	//Do NOT Display: �	 Content Type �	Item ID
			        	sb.append(getTrHeaderHTML("Employee Special Education Highly Qualified Information", 3, "leftAlign", "", "background-color:#007FB2;color:white;"));
			        	StringBuffer sbESEHQI=new StringBuffer();
			        	sbESEHQI.append(getTMTableStartWithClassAndId("table tableCSS", "tblEmployeeESEHQI", ""));
			        	sbESEHQI.append("<thead class='bg'>");
			        	jsonData=new ArrayList<String>();
			        	jsonData.add("Effect Date");
			        	jsonData.add("Description");
			        	sbESEHQI.append(getMultipleThOfTrWithCssHTML(jsonData, "background-color:#007FB2;color:white;", "", ""));
			        	if(ContentType1.length>0){
			        	for(int i=0;i<ContentType1.length;i++){
			        		jsonData=new ArrayList<String>();
			        		jsonData.add(Effdt1[i]);
			        		jsonData.add(Descr1[i]);
			        		sbESEHQI.append(getMultipleTdOfTrWithCssHTML(jsonData,"","",""));
			        	}}else{
			        		sbESEHQI.append(getTdOfTrHTML("No Record Found",2,"","",""));
				        }
			        	sbESEHQI.append(getTMTableEnd());
				        sb.append(getTdOfTrHTML(sbESEHQI.toString(),3,"","","padding:10px;"));
			        	
			        	
			        	//System.out.println("Effdt2: "+Effdt2+"\nEducationLevel: "+EducationLevel+"\nEducationLevelDescr: "+EducationLevelDescr+"\nState3: "+State3+"\nStateDescr2: "+StateDescr2+"\nSchoolType2: "+SchoolType2+"\nSchoolDescr2: "+SchoolDescr2+"\nCompleted: "+Completed);//+"\n"+
			        	//EmpInfoSchoolEd
			        	//Do NOT Display: �	Ed Level 
			        	sb.append(getTrHeaderHTML("Employee Other Educational Information", 3, "leftAlign", "", "background-color:#007FB2;color:white;"));
			        	StringBuffer sbEOEI=new StringBuffer();
			        	sbEOEI.append(getTMTableStartWithClassAndId("table tableCSS", "tblEmployeeEOEI", ""));
			        	sbEOEI.append("<thead class='bg'>");
			        	jsonData=new ArrayList<String>();
			        	jsonData.add("Effect Date");
			        	jsonData.add("Education Level Description");
			        	jsonData.add("State");
			        	jsonData.add("State Description");
			        	jsonData.add("School Type");
			        	jsonData.add("School Description");
			        	jsonData.add("Completed");
			        	sbEOEI.append(getMultipleThOfTrWithCssHTML(jsonData, "background-color:#007FB2;color:white;", "", ""));
			        	if(Effdt2.length>0){
			        	for(int i=0;i<Effdt2.length;i++){
			        		jsonData=new ArrayList<String>();
			        		jsonData.add(Effdt2[i]);
			        		jsonData.add(EducationLevelDescr[i]);
			        		jsonData.add(State3[i]);
			        		jsonData.add(StateDescr2[i]);
			        		jsonData.add(SchoolType2[i]);
			        		jsonData.add(SchoolDescr2[i]);
			        		jsonData.add(Completed[i]);
			        		sbEOEI.append(getMultipleTdOfTrWithCssHTML(jsonData,"","",""));
			        	}}else{
			        		sbEOEI.append(getTdOfTrHTML("No Record Found",7,"","",""));
				        }
			        	sbEOEI.append(getTMTableEnd());
				        sb.append(getTdOfTrHTML(sbEOEI.toString(),3,"","","padding:10px;"));
			        	sb.append(getTMTableEnd());	        	
			        	//System.out.println(sb.toString());
			              
		        }
		    	catch (IOException eIO){
		    		//eIO.printStackTrace();
		    		sb=new StringBuffer();
		    		sb.append("1####No information found on the Employee Number.");
		    	}
		   
		    System.out.println("Ouput: [" + response.toString() + "]");
		   
		    response = new StringBuffer();
		    try
		    {
		        in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		        String inputLine;
		        while ((inputLine = in.readLine()) != null)
		        { response.append(inputLine); }
		        in.close();
		        }
		    catch (Exception e)
		    {
		    	//e.printStackTrace();
		    }
		    System.out.println("Error: [" + response.toString() + "]");
		    return sb.toString();
    }

    
    public static String[] jcTMHireCandidate(JobForTeacher jft,SessionFactory sessionFactory,TeacherStatusHistoryForJob tshj,EligibilityVerificationHistroyDAO eligibilityVerificationHistroyDAO, TeacherNormScoreDAO teacherNormScoreDAO){  
    	java.security.Security.insertProviderAt (new org.bouncycastle.jce.provider.BouncyCastleProvider(), 1);
   	 	System.out.println("/**********::**********Jeffco: Hire Candidate Information*************::*************/");
			String retrunValue = "";		
			String strURL=Utility.getValueOfPropByKey("productionUrlOrTest")+"/PSIGW/RESTListeningConnector/PSFT_HR/JC_TM_HIRE_RQST.v1/hire";
			//String strURL="https://jcspatst.jeffco.k12.co.us/PSIGW/RESTListeningConnector/PSFT_HR/JC_TM_HIRE_RQST.v1/hire";
			//http://qb1x-psdevweb2.jeffco.k12.co.us:27700/PSIGW/RESTListeningConnector/PSFT_HR/JC_TM_HIRE_RQST.v1/hire";
			//http://qb1x-psdevweb2.jeffco.k12.co.us:27700/PSIGW/RESTListeningConnector/PSFT_HR/JC_TM_HIRE_RQST.v1/hire";
			//String strURL="http://localhost:8080/teachermatch/service/provisionUpdate.do";
					/*String string="{\""+
					"\"emplid\": \"NEW\",\"firstName\": \"May\",\"lastName\": \"Template115\",\"middleName\": \"Test\",\"action\": \"HIR\",\"actionReason\": \"NEW\",\"effdt\": \"2015-05-22\",\"positionNbr\": \"00000022\",\"ethnicGroup\": \"UNKNOWN\", \"contractType\": \"TBD\", \"payGroup\": \"LPR\", \"comments\": \"test comments\","+
					"\"HireAddress\": [{\"addressType\": \"HOME\",\"address1\": \"8 High St\",\"address2\": \"Apt 8\",\"city\": \"Lakewood\",\"state\": \"CO\",\"postal\": \"80546\"}],"+
					"\"Phone\": [{\"phoneType\": \"CELL\",\"phone\": \"303-343-8888\"}],"+
					"\"HireEmail\": [{\"emailType\": \"HOME\",\"emailAddress\": \"martin8@123.com\"}]"+
					"}";*/
		//PostMethod post = new PostMethod(strURL);
   	 	try{
	   	 	URL obj = new URL(strURL);
			HttpURLConnection urlConnection = (HttpURLConnection) obj.openConnection();
			String method = "POST";
			urlConnection.setRequestMethod(method); 
			urlConnection.setRequestProperty("User-Agent", "");
			urlConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			urlConnection.setRequestProperty("Content-type", "application/json");
			urlConnection.setRequestProperty("Authorization", "Basic " + Utility.getValueOfPropByKey("passwordAPIURL")); //test server
			//urlConnection.setRequestProperty("Authorization", "Basic " + "VEVBQ0hNQVQ6QzBsZFN0MG4z");//production
		
   	 		SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd");
   	 		//Get Status by Node
	   	 	List<EligibilityVerificationHistroy>  evhList=eligibilityVerificationHistroyDAO.findByLastEntryEVHistoryByJobOrderAndTeacherDetail(tshj.getTeacherDetail(), tshj.getJobOrder());
			Map<String,EligibilityVerificationHistroy> statusNodeMap=new LinkedHashMap<String,EligibilityVerificationHistroy>();
			System.out.println("Size===="+evhList.size());
				for(EligibilityVerificationHistroy evh:evhList){
					if(!evh.getEligibilityMaster().getEligibilityName().trim().equalsIgnoreCase("BISI")){
						statusNodeMap.put(evh.getEligibilityMaster().getEligibilityName(),evh);
						System.out.println("evh=="+evh.getEligibilityMaster().getEligibilityName());
					}
				}
				
			EligibilityVerificationHistroy salaryNode=statusNodeMap.get("Salary");
			EligibilityVerificationHistroy empNode=statusNodeMap.get("Employee ID/BISI");
			//Ended
			
			List<TeacherDetail> listTD=new ArrayList<TeacherDetail>();
			listTD.add(tshj.getTeacherDetail());
			List<TeacherNormScore> TNSList=teacherNormScoreDAO.findNormScoreByTeacherList(listTD);
			
   	 		Session session=sessionFactory.openSession();
   	 		TeacherPersonalInfo teacherPersonalInfo=(TeacherPersonalInfo) session.load(TeacherPersonalInfo.class,jft.getTeacherId().getTeacherId());
   	 		/*******************************************Details of Candidate Race ********************************************/
   	 			/*String candidateRace = "";
   	 			try{
	   	 			List<RaceMaster> lstRace= null;		   	 
					lstRace = session.createCriteria(RaceMaster.class).add(Restrictions.ne("raceId",0)).add(Restrictions.eq("status","A")).addOrder(Order.asc("orderBy")).list();
					Map<String,RaceMaster> raceMap = new HashMap<String, RaceMaster>();
					for (RaceMaster raceMaster : lstRace) {
						raceMap.put(""+raceMaster.getRaceId(), raceMaster);
					}					
					String raceIds[] = new String[10];
					if(teacherPersonalInfo.getRaceId()!=null)
					{
						raceIds = teacherPersonalInfo.getRaceId().split(",");
					}
					if(raceIds.length>0)
					{
						for (int i = 0; i < raceIds.length; i++) {
							RaceMaster raceMaster = raceMap.get(""+raceIds[i]);
							if(raceMaster!=null)
							candidateRace+=raceMaster.getRaceName()+";";
						}

						if(candidateRace.length()>0)
						candidateRace = candidateRace.substring(0,candidateRace.length()-1);
					}
   	 			}catch(Exception e){e.printStackTrace();}*/
   	 		/*******************************************end Candidate Race ********************************************/
		   		Map<Integer,String> jobApplicationStatusMap=new LinkedHashMap<Integer,String>();
		   		jobApplicationStatusMap.put(0,"");
		   		jobApplicationStatusMap.put(1,"Ongoing");
		   		jobApplicationStatusMap.put(2,"Temporary");
		   		Map<Integer,String> tempTypeMap=new LinkedHashMap<Integer,String>();
		   		tempTypeMap.put(0,"");
		   		tempTypeMap.put(1,"Funding");
		   		tempTypeMap.put(2,"Late Posting");
		   		tempTypeMap.put(3,"Protected");
		   		System.out.println("teacherPersonalInfo:::: "+teacherPersonalInfo.getEmailAddress());
				JSONObject jsonInput = new JSONObject(); 				
				JSONObject hireAddress=new JSONObject();
				JSONObject phone=new JSONObject();	
				JSONObject emailAddress=new JSONObject();
				jsonInput.put("emplid", empNode.getEmployeeId());
				jsonInput.put("action", "HIR");
				jsonInput.put("actionReason", "NEW");
				if(teacherPersonalInfo!=null){
						/*if(teacherPersonalInfo.getFirstName()!=null){
							jsonInput.put("firstName", teacherPersonalInfo.getFirstName());
						}else{
							jsonInput.put("firstName", "");
						}
							if(teacherPersonalInfo.getLastName()!=null){
								jsonInput.put("lastName", teacherPersonalInfo.getLastName());
							}else{
								jsonInput.put("lastName", "");
							}
								if(teacherPersonalInfo.getMiddleName()!=null){
									jsonInput.put("middleName", teacherPersonalInfo.getMiddleName());
								}else{
									jsonInput.put("middleName", "");
								}*/
					
								//Comment on 21-09-2015
								/*jsonInput.put("HIRE_DT",salaryNode.getStartDate()!=null?sdf.format(salaryNode.getStartDate()):"");	
								jsonInput.put("POSITION_NBR",tshj.getJobOrder().getRequisitionNumber()!=null?tshj.getJobOrder().getRequisitionNumber():"");
								JSONArray contract=new JSONArray();
								contract.add(tshj.getJobOrder().getJobApplicationStatus()!=null?jobApplicationStatusMap.get(tshj.getJobOrder().getJobApplicationStatus()):"");
								jsonInput.put("CONTRACT",contract);
								jsonInput.put("CONTRACT_TYPE",tshj.getJobOrder().getTempType()!=null?(tshj.getJobOrder().getJobApplicationStatus()!=null?(jobApplicationStatusMap.get(tshj.getJobOrder().getJobApplicationStatus()).equalsIgnoreCase("Temporary")?(tempTypeMap.get(tshj.getJobOrder().getTempType())):""):""):"");
								jsonInput.put("MONTHLY_RT",salaryNode.getSalary()!=null?salaryNode.getSalary().trim():"");
								jsonInput.put("STEP",salaryNode.getStep()!=null?salaryNode.getStep().trim():"");
								jsonInput.put("CONTRACT_END_DT",salaryNode.getEndDate()!=null?sdf.format(salaryNode.getEndDate()):"");
								jsonInput.put("JPM_DECIMAL_1",TNSList.size()>0?(TNSList.get(0).getTeacherNormScore()):"");
								jsonInput.put("JPM_Date", TNSList.size()>0?(TNSList.get(0).getCreatedDateTime()!=null?sdf.format(TNSList.get(0).getCreatedDateTime()):""):"");*/
								
								jsonInput.put("effdt",salaryNode.getStartDate()!=null?sdf.format(salaryNode.getStartDate()):"");
								jsonInput.put("positionNbr",tshj.getJobOrder().getRequisitionNumber()!=null?tshj.getJobOrder().getRequisitionNumber():"");
								/*JSONArray contract=new JSONArray();
								contract.add(tshj.getJobOrder().getJobApplicationStatus()!=null?jobApplicationStatusMap.get(tshj.getJobOrder().getJobApplicationStatus()):"");
								jsonInput.put("contract",contract);
								jsonInput.put("contractType",tshj.getJobOrder().getTempType()!=null?(tshj.getJobOrder().getJobApplicationStatus()!=null?(jobApplicationStatusMap.get(tshj.getJobOrder().getJobApplicationStatus()).equalsIgnoreCase("Temporary")?(tempTypeMap.get(tshj.getJobOrder().getTempType())):""):""):"");*/
								jsonInput.put("monthlyRt",salaryNode.getSalary()!=null?salaryNode.getSalary().trim():"");
								jsonInput.put("salaryStep",salaryNode.getStep()!=null?salaryNode.getStep().trim():"");
								jsonInput.put("contractStartDt",salaryNode.getStartDate()!=null?sdf.format(salaryNode.getStartDate()):"");  
								jsonInput.put("contractEndDt",salaryNode.getEndDate()!=null?sdf.format(salaryNode.getEndDate()):"");
								jsonInput.put("epiScore",TNSList.size()>0?(TNSList.get(0).getTeacherNormScore().toString()):"");
								jsonInput.put("epiDate",TNSList.size()>0?(TNSList.get(0).getCreatedDateTime()!=null?sdf.format(TNSList.get(0).getCreatedDateTime()):""):"");
					
								
					hireAddress.put("addressType", "HOME");
					if(teacherPersonalInfo.getAddressLine1()!=null){
						hireAddress.put("address1", teacherPersonalInfo.getAddressLine1());
					}else{
						hireAddress.put("address1", "");
					}				
						if(teacherPersonalInfo.getAddressLine2()!=null){
							hireAddress.put("address2", teacherPersonalInfo.getAddressLine2());
						}else{
							hireAddress.put("address2", "");
						}					
							if(teacherPersonalInfo.getCityId()!=null && teacherPersonalInfo.getCityId().getCityName()!=null){
								hireAddress.put("city", teacherPersonalInfo.getCityId().getCityName());
							}else{
								hireAddress.put("city", "");
							}
								if(teacherPersonalInfo.getStateId()!=null && teacherPersonalInfo.getStateId().getStateName()!=null){
									hireAddress.put("state", teacherPersonalInfo.getStateId().getStateName());
								}else{
									hireAddress.put("state", "");
								}					
									if(teacherPersonalInfo.getPresentZipCode()!=null){
										hireAddress.put("postal", teacherPersonalInfo.getPresentZipCode());
									}else{
										hireAddress.put("postal", "");
									}
									
					phone.put("phoneType", "CELL");
						if(teacherPersonalInfo.getPhoneNumber()!=null){
							phone.put("phone", teacherPersonalInfo.getPhoneNumber());
						}else{
							phone.put("phone", "");
						}	
					
					/*if(teacherPersonalInfo.getEthnicityId()!=null && teacherPersonalInfo.getEthnicityId().getEthnicityName()!=null){
						jsonInput.put("ethnicGroup", teacherPersonalInfo.getEthnicityId().getEthnicityName());
					}else{
						jsonInput.put("ethnicGroup", "UNKNOWN");
					}		*/		    
					
				}
				/*else{											
					if(jft.getTeacherId().getFirstName()!=null){
						jsonInput.put("firstName", teacherPersonalInfo.getFirstName());
					}else{
						jsonInput.put("firstName", "");
					}
						if(jft.getTeacherId().getLastName()!=null){
							jsonInput.put("lastName", teacherPersonalInfo.getLastName());
						}else{
							jsonInput.put("lastName", "");
						}
						jsonInput.put("middleName", "");
							
					
						hireAddress.put("addressType", "HOME");
						hireAddress.put("address1", "");				
						hireAddress.put("address2", "");				
						hireAddress.put("city", "");
						hireAddress.put("state", "");
						hireAddress.put("postal", "");
						phone.put("phoneType", "CELL");
							if(jft.getTeacherId().getPhoneNumber()!=null){
								phone.put("phone", jft.getTeacherId().getPhoneNumber());
							}else{
								phone.put("phone", "");
							}						
					jsonInput.put("ethnicGroup", "UNKNOWN");
				}*/
				
				/*jsonInput.put("effdt",sdf.format(new Date()));
				if(jft.getRequisitionNumber()!=null){
					jsonInput.put("positionNbr", jft.getRequisitionNumber());
				}else{
					jsonInput.put("positionNbr", "");
				}				
				
				jsonInput.put("contractType", "TBD");
				jsonInput.put("payGroup", "LPR");
				jsonInput.put("comments", "");*/
				
				JSONArray hiredEmailArray=new JSONArray();
				JSONArray phoneArray=new JSONArray();
				JSONArray hireAddressArray=new JSONArray();
				emailAddress.put("emailType","HOME");
				emailAddress.put("emailAddress", jft.getTeacherId().getEmailAddress());
				
				
				hireAddressArray.add(hireAddress);
				//jsonInput.put("HireAddress",hireAddressArray);
				/*phoneArray.add(phone);
				jsonInput.put("Phone",phoneArray);
				hiredEmailArray.add(emailAddress);
				jsonInput.put("HireEmail",hiredEmailArray);*/
								
		   		String req=jsonInput.toString();
				System.out.println("req::: "+req);
				
				urlConnection.setDoOutput(true);
				DataOutputStream dos = new DataOutputStream(urlConnection.getOutputStream());
				dos.writeBytes(req);
				dos.flush();
				dos.close();
				
				int responseCode = urlConnection.getResponseCode();
				System.out.println("\nSending '" + method + "' request to URL : " + strURL);
				System.out.println("Post parameters : " + req);
				System.out.println("Response Code : " + responseCode);
				evhList.clear();
				statusNodeMap.clear();
				TNSList.clear();
				listTD.clear();
				jobApplicationStatusMap.clear();
				tempTypeMap.clear();
				return printHTTPResults(urlConnection);
   	 }catch (Exception e) {
		// TODO: handle exception
   		 e.printStackTrace();
	}
   	 return null;
    }
    
    private synchronized static String[] printHTTPResults(HttpURLConnection con) {
		BufferedReader in = null;
		String[] returnString =new String[2];
		StringBuffer responseSuccess = new StringBuffer();
		try {
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				responseSuccess.append(inputLine);
			}
			in.close();
			// print result
		} catch (IOException eIO) {
		}
		
		StringBuffer responseError = new StringBuffer();
		try {
			in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				responseError.append(inputLine);
			}
			in.close();
		} catch (Exception e) {
		}
		returnString[0]=responseSuccess.toString();
		returnString[1]=responseError.toString();
		return returnString;
	}
    
    private static boolean validateEmailAddress(String emailAddress) {
	      String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
	      return emailAddress.matches(EMAIL_REGEX);	      
	} 
    
    @RequestMapping(value="/service/provisionGet.do", method={RequestMethod.GET,RequestMethod.POST})
    public String provisionGet(HttpServletRequest request, HttpServletResponse response){
   	 System.out.println("/**********::**********Jeffco: Provision Update*************::*************/");
   	 String requestContent = null;
   	 String respondMessage=null;
   	 try{
   		PrintWriter out = response.getWriter();
   		HttpSession session =request.getSession(false);
   		if(session==null){
   			session=request.getSession(true);
   		}
   		Map<String,String> roleLst=new LinkedHashMap<String,String>();
   		roleLst.put("District Admin","DA");
   		roleLst.put("District Analyst","DT");
   		roleLst.put("School Admin","SA");
   		roleLst.put("School Analyst","ST");
   		
   		DistrictMaster districtMaster = null;
   		String authKey =null;
   		String EMAIL_LOGIN  =null;// Base 64 encoded email old/current email address
   		String EMPLOYEEID  =null; 
   		boolean responseStatus = false;
        String errorMsg ="Invalid Credentials";
        String errorCode = "10001";
	   	 try{
	    	 requestContent = request.getParameter("request");
	    	 
	    	 if(requestContent==null){
	    		 requestContent = ServiceUtility.getBody(request);
	    	 }
	    	 
	    	 System.out.println("requestContent: "+requestContent);
	    	 boolean isContailNull=requestContent.contains("\"EMAIL_LOGIN\":null");
	    	 
	    	 
	    	 //if(requestContent!=null && !requestContent.equalsIgnoreCase(""))
	    	 JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( requestContent ); 
				//districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
				
				authKey = jsonRequest.get("authKey")==null?"":UtilityAPI.parseString(jsonRequest.getString("authKey")).replace(' ', '+');							
		   		EMAIL_LOGIN = jsonRequest.get("EMAIL_LOGIN")==null?"":UtilityAPI.parseString(jsonRequest.getString("EMAIL_LOGIN")).replace(' ', '+');
		   		EMPLOYEEID  = jsonRequest.get("EMPLOYEEID")==null?"":UtilityAPI.parseString(jsonRequest.getString("EMPLOYEEID")).replace(' ', '+');
		   		println("authKey====>:::\t"+authKey);
		   		println("EMAIL_LOGIN====>:::\t"+EMAIL_LOGIN);
		   		println("EMPLOYEEID====>:::\t"+EMPLOYEEID);
				
				/*if(!EMAIL_LOGIN.trim().equals("")){
					EMAIL_LOGIN = Utility.decodeBase64(EMAIL_LOGIN);
				}*/
				println("Login email========"+EMAIL_LOGIN);
				//System.out.println("Encode EMAIL_LOGIN========="+Utility.encodeInBase64(EMAIL_LOGIN));
				
	    	 
	    	 if(authKey!=null){        		 
        		 districtMaster = districtMasterDAO.validateDistrictByAuthkey(authKey);
		        		 if(districtMaster!=null){
		        			 session.setAttribute("apiDistrictMaster", districtMaster);
		        			 responseStatus = true;
		        			 println("********District Validated********"+districtMaster.getDistrictName()+"   "+districtMaster.getDistrictId());
		        		 }else{
			        		 responseStatus = false;
			        		 errorCode = "10001";
			        		 errorMsg = "Invalid Credentials";
			        	 }
		        	 }else{
		        		 responseStatus = false;
		        		 errorCode = "10001";
		        		 errorMsg = "Invalid Credentials";
		        	 }
	    	 if(isContailNull){
	    		 EMAIL_LOGIN="";
	    	 }
	    	 if(!EMAIL_LOGIN.trim().equals("") && !validateEmailAddress(EMAIL_LOGIN.trim())){
	    		 	errorCode="10061";
			 		errorMsg="Invalid login email address";
			 		responseStatus=false;
    		 }
	    	 JSONObject json=new JSONObject();
	    	 EmployeeMaster em=null;
	    	 Criterion email_id=null;
	    	 Criterion employee_id=null;
	    	 int count=0;
	    	 List<Criterion> lstCriterion=new ArrayList<Criterion>();
	    	 if(responseStatus){
	    		 if(EMAIL_LOGIN.trim().equals("")){
	    			 errorCode="10058";
	    			 errorMsg="Login email address is required";
	    		 }else{
	    			 email_id=Restrictions.eq("emailAddress", EMAIL_LOGIN);
	    			 lstCriterion.add(email_id);
	    			 count=1;
	    		 }
	    		 if(EMPLOYEEID.trim().equals("")){
	    			 errorCode="10062";
	    			 errorMsg="Employee Id is required";
	    		 }else{
	    			 employee_id=Restrictions.eq("employeeCode", EMPLOYEEID.trim());
	    			 lstCriterion.add(employee_id);
	    			 count=2;
	    		 }
	    		 if(email_id==null && employee_id==null){
	    			 errorCode="10064";
	    			 errorMsg="Login email or Employee Id is required";
	    			 responseStatus = false;
	    		 }
	    		 
		    		 if(responseStatus){
		    			 lstCriterion.add(Restrictions.eq("districtMaster", districtMaster));
		    			 Criterion[] criterion=new Criterion[lstCriterion.size()];
		    			 System.out.println("cri length============="+criterion.length);
		    			 for(int i=0;i<criterion.length;i++){
		    				 System.out.println("cir  "+i+"   "+lstCriterion.get(i));
		    				 criterion[i]=lstCriterion.get(i);
		    			 }
		    			 println("cri length============="+criterion.length);
		    			 List<EmployeeMaster> lstEM=new ArrayList<EmployeeMaster>();
		    			 if(count!=1)
		    			 lstEM=employeeMasterDAO.findByCriteria(criterion);
		    			 if((lstEM.size()==0 && count==1) || lstEM.size()>0){   // if(lstEM.size()>0 || (lstEM.size()==0 && count==1)){
		    				 List<Criterion> userCri=new ArrayList<Criterion>();
		    				 userCri.add(Restrictions.eq("districtId", districtMaster));
		    				 String emaialAddress=EMAIL_LOGIN;
		    				 if(lstEM.size()>0){
		    					 List<UserMaster> lstUM=userMasterDAO.findByCriteria(Restrictions.in("employeeMaster", lstEM));
		    					 if(lstUM.isEmpty() || lstEM.size()==1){
		    						 emaialAddress=lstEM.get(0).getEmailAddress();
		    						 if(!lstUM.isEmpty())
		    						 userCri.add(Restrictions.eq("emailAddress",emaialAddress));
			    					 userCri.add(Restrictions.eq("employeeMaster",lstEM.get(0)));
		    					 }else{
		    						 emaialAddress=lstUM.get(0).getEmployeeMaster().getEmailAddress();
			    					 userCri.add(Restrictions.eq("employeeMaster",lstUM.get(0).getEmployeeMaster()));
		    					 }
		    				 }else{
		    					 userCri.add(Restrictions.eq("emailAddress",emaialAddress));
		    				 }
		    				 List<Criterion> userCri1=new ArrayList<Criterion>(userCri);
		    				 userCri1.add(Restrictions.eq("status","A"));
		    				 List<UserMaster> lstUM=userMasterDAO.findByCriteria(userCri1.toArray(new Criterion[userCri1.size()]));
		    				 if(lstUM.size()>0){
		    					 String role="";
		    					 JSONArray deptId=new JSONArray();
		    					 UserMaster umaster=null;
		    					 for(UserMaster umObj:lstUM){
		    							 umaster=umObj;
		    						 if(roleLst.get(umObj.getRoleId().getRoleName())!=null && (roleLst.get(umObj.getRoleId().getRoleName()).equals("SA") ||  roleLst.get(umObj.getRoleId().getRoleName()).equals("ST")));
		    						 if(umObj.getSchoolId()!=null)
		    						 deptId.add(umObj.getSchoolId().getLocationCode());
		    						 println("::::::::::::++"+umObj.getRoleId().getRoleName());
		    						 if(!role.contains(roleLst.get(umObj.getRoleId().getRoleName())))
		    						 role+=roleLst.get(umObj.getRoleId().getRoleName())+"|";
		    						 println("role============"+role);
		    						 
		    					 }
		    					 if(umaster!=null){
		    					 json.put("DEPTID", deptId);
		    					 json.put("ROLE", !role.trim().equals("")?role.substring(0,(role.length()-1)):"");
		    					 json.put("FIRST_NAME", umaster.getFirstName()!=null?umaster.getFirstName():"");
		    					 json.put("LAST_NAME", umaster.getLastName()!=null?umaster.getLastName():"");
		    					 json.put("EMAIL_LOGIN", Utility.encodeInBase64(umaster.getEmailAddress()));
		    					 json.put("TITLE", umaster.getTitle()!=null?umaster.getTitle():"");//.replaceAll("\\+", " ")
		    					 json.put("EMPLOYEEID", umaster.getEmployeeMaster()!=null?umaster.getEmployeeMaster().getEmployeeCode():"");
		    					 json.put("STATUS", umaster.getStatus()!=null?(umaster.getStatus().equals("A")?"Active":"Inactive"):"");
		    					 responseStatus = true;
		    					 }
		    				 }else{
		    					 List<Criterion> userCri2=new ArrayList<Criterion>(userCri);
			    				 userCri2.add(Restrictions.eq("status","I"));
			    				 System.out.println("userCri2==="+userCri2.size());
		    					 List<UserMaster> lstUM1=userMasterDAO.findByCriteria(userCri2.toArray(new Criterion[userCri2.size()]));
		    					 if(lstUM1.size()>0){
			    					 String role="";
			    					 JSONArray deptId=new JSONArray();
			    					 UserMaster umaster=null;
			    					 for(UserMaster umObj:lstUM1){
			    							 umaster=umObj;
			    						 if(roleLst.get(umObj.getRoleId().getRoleName())!=null && (roleLst.get(umObj.getRoleId().getRoleName()).equals("SA") ||  roleLst.get(umObj.getRoleId().getRoleName()).equals("ST")));
			    						 if(umObj.getSchoolId()!=null)
			    						 deptId.add(umObj.getSchoolId().getLocationCode());
			    						 println("::::::::::::++"+umObj.getRoleId().getRoleName());
			    						 if(!role.contains(roleLst.get(umObj.getRoleId().getRoleName())))
			    						 role+=roleLst.get(umObj.getRoleId().getRoleName())+"|";
			    						 println("role============"+role);
			    						 
			    					 }
			    					 if(umaster!=null){
			    					 json.put("DEPTID", deptId);
			    					 json.put("ROLE", !role.trim().equals("")?role.substring(0,(role.length()-1)):"");
			    					 json.put("FIRST_NAME", umaster.getFirstName()!=null?umaster.getFirstName():"");
			    					 json.put("LAST_NAME", umaster.getLastName()!=null?umaster.getLastName():"");
			    					 json.put("EMAIL_LOGIN", Utility.encodeInBase64(umaster.getEmailAddress()));
			    					 json.put("TITLE", umaster.getTitle()!=null?umaster.getTitle():"");//.replaceAll("\\+", " ")
			    					 json.put("EMPLOYEEID", umaster.getEmployeeMaster()!=null?umaster.getEmployeeMaster().getEmployeeCode():"");
			    					 json.put("STATUS", umaster.getStatus()!=null?(umaster.getStatus().equals("A")?"Active":"Inactive"):"");
			    					 responseStatus = true;
			    					 }
			    				 }else{
			    					errorCode="10004";
			    					errorMsg="User does not exist" ;
			    					responseStatus = false;
		    					 }
		    				 }
		    			 }else{
		    				 println("count========"+count);
		    				 if(criterion.length==3){
		    				 errorCode="10066";
			    			 errorMsg="Employee Id does not exist";
			    			 responseStatus = false; 
		    				 }else if(criterion.length<3){
		    					if(count==1){
		    						errorCode="10042";
		    						 errorMsg="Login email does not exist";
		    						 responseStatus = false; 
		    					}else if(count==2){
		    						errorCode="10066";
		    						 errorMsg="Employee Id does not exist";
		    						 responseStatus = false; 
		    					}
		    				 }
		    			 }
		    			 
		    		 }
	    	 }
	    	 JSONObject responseMSG=getResponseMessage(responseStatus,errorCode,errorMsg,json);
	    	 respondMessage=responseMSG.toString();
	    	 out.println(responseMSG);
	   	 }catch(Exception e){
	   		 e.printStackTrace();
	   		JSONObject responseMSG=getResponseMessage(responseStatus,errorCode,errorMsg,null);
	    	 respondMessage=responseMSG.toString();
	    	 out.println(responseMSG);
	   		 }
   	 }catch(Exception e){e.printStackTrace();}
   	 insertTrackingAPIHistory(userMasterDAO.getSessionFactory(), "/service/provisionGet.do", requestContent, respondMessage);
	return null;
    }
    
    @RequestMapping(value="/service/provisionUpdate.do", method=RequestMethod.POST)
    public void provisionUpdate(ModelMap map, HttpServletRequest request, HttpServletResponse response){
   	
   	 System.out.println("/**********::**********Jeffco: Provision Update*************::*************/");
   	 String requestContent = null;
   	 String respondMessage=null;
   	 try{
   		PrintWriter out = response.getWriter();
   		HttpSession session =request.getSession(false);
   		if(session==null){
   			session=request.getSession(true);
   		}
   		Map<String,String> roleLst=new LinkedHashMap<String,String>();
   		roleLst.put("DA", "District Admin");
   		roleLst.put("DT", "District Analyst");
   		roleLst.put("SA", "School Admin");
   		roleLst.put("ST", "School Analyst");
   		Map<String,String> roleLst1=new LinkedHashMap<String,String>();
   		roleLst1.put("District Admin","DA");
   		roleLst1.put("District Analyst","DT");
   		roleLst1.put("School Admin","SA");
   		roleLst1.put("School Analyst","ST");
   		
   		DistrictMaster districtMaster = null;
   		String authKey =null;
   		String action =null;
   		String updateAction =null;;// {addSchool, removeSchool, changeRole, changeInfo}
   		String DEPTID  =null;// location id in schoolMaster
   		String ROLE  =null;// DA, DT, SA, ST
   		String FIRST_NAME  =null;
   		String LAST_NAME  =null;
   		String EMAIL_LOGIN  =null;// Base 64 encoded email old/current email address
   		String EMAIL_ADDRESS  =null;// new email address
   		String EMPLOYEEID  =null;
   		String TITLE  =null; 
   		String STATUS =null;
   		boolean responseStatus = false;
        String errorMsg ="Invalid Credentials";
        String errorCode = "10001";
	   	 try{
	    	 requestContent = request.getParameter("request");
	    	 
	    	 if(requestContent==null){
	    		 requestContent = ServiceUtility.getBody(request);
	    	 }
	    	 
	    	 System.out.println("requestContent: "+requestContent);
	    	 //if(requestContent!=null && !requestContent.equalsIgnoreCase(""))
	    	 JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( requestContent ); 
				//districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
				boolean deptArrayflag=true;
				JSONArray jsonArrayDEPTID=null;
				List<String> locationList=new ArrayList<String> ();
				List<String> locationExistsList=new ArrayList<String> ();
				List<String> locationJSONList=new ArrayList<String> ();
				Map<String,Boolean> locationExistsMap=new LinkedHashMap<String,Boolean>();
				authKey = jsonRequest.get("authKey")==null?"":UtilityAPI.parseString(jsonRequest.getString("authKey")).replace(' ', '+');							
				action =jsonRequest.get("action")==null?"":UtilityAPI.parseString(jsonRequest.getString("action")).replace(' ', '+');
				updateAction =jsonRequest.get("updateAction")==null?"":UtilityAPI.parseString(jsonRequest.getString("updateAction")).replace(' ', '+');
		   		DEPTID  =jsonRequest.get("DEPTID")==null?"":UtilityAPI.parseString(jsonRequest.getString("DEPTID"));
		   		try{
		   		jsonArrayDEPTID = jsonRequest.getJSONArray("DEPTID");
		   		System.out.println("dept===================="+jsonArrayDEPTID.size());
		   		if(jsonArrayDEPTID.size()>0){
		   			deptArrayflag=true;
		   			for(Object locationCode:jsonArrayDEPTID){
		   				if(!locationList.contains((""+locationCode).trim()))
		   				{
		   					locationList.add((""+locationCode).trim());
		   					locationJSONList.add((""+locationCode).trim());
		   					locationExistsMap.put((""+locationCode).trim(), true);
		   				}
		   			}
		   		}
		   		}catch(Throwable t){
		   			deptArrayflag=false;
		   			DEPTID  =jsonRequest.get("DEPTID")==null?"":UtilityAPI.parseString(jsonRequest.getString("DEPTID"));
		   			locationJSONList.add(DEPTID.trim());
		   			locationExistsMap.put(DEPTID.trim(), true);
		   			//t.printStackTrace();
		   		}
		   		
		   		ROLE  =jsonRequest.get("ROLE")==null?"":UtilityAPI.parseString(jsonRequest.getString("ROLE"));
		   		FIRST_NAME  =jsonRequest.get("FIRST_NAME")==null?"":UtilityAPI.parseString(jsonRequest.getString("FIRST_NAME")).replace(' ', '+');
		   		LAST_NAME  =jsonRequest.get("LAST_NAME")==null?"":UtilityAPI.parseString(jsonRequest.getString("LAST_NAME")).replace(' ', '+');
		   		EMAIL_LOGIN = jsonRequest.get("EMAIL_LOGIN")==null?"":UtilityAPI.parseString(jsonRequest.getString("EMAIL_LOGIN")).replace(' ', '+');
		   		EMAIL_ADDRESS  =jsonRequest.get("EMAIL_ADDRESS")==null?"":UtilityAPI.parseString(jsonRequest.getString("EMAIL_ADDRESS")).replace(' ', '+');
		   		EMPLOYEEID  = jsonRequest.get("EMPLOYEEID")==null?"":UtilityAPI.parseString(jsonRequest.getString("EMPLOYEEID")).replace(' ', '+');
		   		TITLE  = jsonRequest.get("TITLE")==null?"":jsonRequest.getString("TITLE");//.replace(' ', '+');
		   		STATUS  = jsonRequest.get("STATUS")==null?"":UtilityAPI.parseString(jsonRequest.getString("STATUS")).replace(' ', '+');
		   		
		   		System.out.println("authKey====>:::\t"+authKey);
		   		System.out.println("action====>:::\t"+action);
		   		System.out.println("updateAction====>:::\t"+updateAction);
		   		System.out.println("DEPTID====>:::\t"+DEPTID);
		   		System.out.println("ROLE====>:::\t"+ROLE);
		   		System.out.println("FIRST_NAME====>:::\t"+FIRST_NAME);
		   		System.out.println("LAST_NAME====>:::\t"+LAST_NAME);
		   		System.out.println("EMAIL_LOGIN====>:::\t"+EMAIL_LOGIN);
		   		System.out.println("EMAIL_ADDRESS====>:::\t"+EMAIL_ADDRESS);
		   		System.out.println("EMPLOYEEID====>:::\t"+EMPLOYEEID);
		   		System.out.println("TITLE====>:::\t"+TITLE);
				
		   		System.out.println("--------------hhfgjhfgjfgjhfgjfgjhfgjfgjfjfjfgh--------------------------------------------------------------------");
		   		System.out.println("EMAIL_LOGIN:- "+EMAIL_LOGIN);
			    	 if(authKey!=null){        		 
		        		 districtMaster = districtMasterDAO.validateDistrictByAuthkey(authKey);
		        		 		System.out.println("districtMaster========"+districtMaster);
				        		 if(districtMaster!=null){
				        			 System.out.println("districtMaster========"+districtMaster.getDisplayName());
				        			 session.setAttribute("apiDistrictMaster", districtMaster);
				        			 responseStatus = true;
				        			 System.out.println("********District Validated********"+districtMaster.getDistrictName()+"   "+districtMaster.getDistrictId());
				        		 }else{
					        		 responseStatus = false;
					        		 errorCode = "10001";
					        		 errorMsg = "Invalid Credentials";
					        	 }
				        	 }else{
				        		 responseStatus = false;
				        		 errorCode = "10001";
				        		 errorMsg = "Invalid Credentials";
				        	 }
		   		if(responseStatus && validateEmailAddress(EMAIL_LOGIN))	//Shadab Ansari
		   		{
		   			EMAIL_LOGIN="";
		   			responseStatus = false;
		   			errorCode = "10068";
		   			errorMsg = "Email Id is not in Base64.";
		   			System.out.println("sdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfg");
		   		}
		   		
				if(!EMAIL_LOGIN.trim().equals("")){
					EMAIL_LOGIN = Utility.decodeBase64(EMAIL_LOGIN);
				}
				
				System.out.println("Login email========"+EMAIL_LOGIN);
				System.out.println("Encode EMAIL_ADDRESS========="+Utility.encodeInBase64(EMAIL_ADDRESS));
				
	    	 
	    	 if(responseStatus)
	    	 if(action.trim().equalsIgnoreCase("")){
    			 errorCode="10056";
    			 errorMsg="Action is required";    			 
    			 responseStatus=false;
    		 }
	    	 System.out.println("");
	    	 if(responseStatus){// TPL-5633    
	    		 if(action.equalsIgnoreCase("add")){
	    			 if(!EMPLOYEEID.trim().equalsIgnoreCase("") && !EMAIL_LOGIN.trim().equalsIgnoreCase("")){
	    				 List<UserMaster> users=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster));
	    			     if(users!=null && users.size()>0){
	    			    	 List<EmployeeMaster> employees=employeeMasterDAO.findByCriteria(Restrictions.eq("employeeCode",EMPLOYEEID.trim()), Restrictions.eq("districtMaster", districtMaster));
							if(employees==null || employees.size()==0){
								action="update";
							}
	    			     }
	    			 }
	    		 }
	    	 }
	    	 
	    	 if(responseStatus){
	    		 if(action.equalsIgnoreCase("add")){
		    		 System.out.println("/**********************ADD ACTIOIN***************************/");
		    		 boolean requiredflag=true;
		    		 if(FIRST_NAME.trim().equalsIgnoreCase("")){
		    			 errorCode="10044";
		    			 errorMsg="First Name is required";
		    			 requiredflag=false;
		    			 responseStatus=false;
		    		 }
		    		 if(LAST_NAME.trim().equalsIgnoreCase("")){
		    			 errorCode="10045";
		    			 errorMsg="Last Name is required";
		    			 requiredflag=false;
		    			 responseStatus=false;
		    		 }
		    		 if(ROLE.trim().equalsIgnoreCase("")){
		    			 errorCode="10046";
		    			 errorMsg="Role is required";
		    			 requiredflag=false;
		    			 responseStatus=false;
		    		 }
		    		 if(EMAIL_LOGIN.trim().equalsIgnoreCase("")){
		    			 errorCode="10058";
		    			 errorMsg="Login email address is required";
		    			 requiredflag=false;
		    			 responseStatus=false;
		    		 }
		    		 if(requiredflag){
		    			 if((deptArrayflag && locationList!=null && locationList.size()>0) || (!deptArrayflag && !DEPTID.trim().equalsIgnoreCase(""))){		    				
			    			 List<RoleMaster> lstRoleMaster=roleMasterDAO.findByCriteria(Restrictions.eq("roleName", roleLst.get(ROLE.trim())));
			    			 if(lstRoleMaster.size()>0){
		    					 if(validateEmailAddress(EMAIL_LOGIN.trim())){
		    						 Criterion locationCri=null;
		    						 if(deptArrayflag)
		    							 locationCri=Restrictions.in("locationCode", locationList);
		    						 else
		    							 locationCri=Restrictions.eq("locationCode", DEPTID.trim());
		    						 List<SchoolMaster> lstSchoolMaster=schoolMasterDAO.findByCriteria(Restrictions.eq("districtId", districtMaster),locationCri);
		    						 for(SchoolMaster sm:lstSchoolMaster)
		    							 locationExistsList.add(sm.getLocationCode());
		    						 if((!deptArrayflag && lstSchoolMaster.size()>0) || (deptArrayflag && locationList!=null && locationList.size()<=lstSchoolMaster.size()))
		    						 {
		    							 List<UserMaster> lstUserMaster=new ArrayList<UserMaster> ();	
		    							 if(lstSchoolMaster!=null && lstSchoolMaster.size()>0){
		    							 lstUserMaster=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster),Restrictions.in("schoolId",lstSchoolMaster));//,Restrictions.eq("roleId", lstRoleMaster.get(0)),Restrictions.eq("entityType", lstRoleMaster.get(0).getEntityType())
		    							 if(lstUserMaster==null || lstUserMaster.size()==0)
		    								 lstUserMaster=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster),Restrictions.isNull("schoolId"));//,Restrictions.eq("roleId", lstRoleMaster.get(0)),Restrictions.eq("entityType", lstRoleMaster.get(0).getEntityType())		    							 
		    							 }else
		    							 lstUserMaster=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster));//,Restrictions.eq("roleId", lstRoleMaster.get(0)),Restrictions.eq("entityType", lstRoleMaster.get(0).getEntityType())
				    					 if(lstUserMaster.size()>0){
				    						 List<String> dupDEPTId=new ArrayList<String>();
				    						 for(UserMaster um:lstUserMaster)
				    						 if(um.getSchoolId()!=null)	 
				    						 dupDEPTId.add(um.getSchoolId().getLocationCode());
				    						 errorCode="10043";
				    						 if(dupDEPTId.isEmpty())
				    						 errorMsg="Duplicate login email address"; 
				    						 else
							    			 errorMsg="Duplicate login email address on deptId"+dupDEPTId+"";
							    			 responseStatus=false;	
				    					 }else{	
				    						 	UserMaster um= new UserMaster();
					    						um.setStatus("A");
					    						um.setFirstName(FIRST_NAME);
					    						um.setLastName(LAST_NAME);
					    						um.setEmailAddress(EMAIL_LOGIN);
					    						um.setPassword(MD5Encryption.toMD5("teachermatch"));
					    						um.setRoleId(lstRoleMaster.get(0));
					    						um.setEntityType(lstRoleMaster.get(0).getEntityType());
					    						um.setDistrictId(districtMaster);
					    						um.setForgetCounter(0);
					    						um.setIsQuestCandidate(false);
					    						um.setCreatedDateTime(new Date());
					    						int verificationCode=(int) Math.round(Math.random() * 2000000);
					    						um.setVerificationCode(verificationCode+"");
					    						um.setAuthenticationCode(verificationCode+"");
					    						if(!TITLE.trim().equalsIgnoreCase(""))
					    							um.setTitle(TITLE);
					    						
				    						 	List<EmployeeMaster> lstEmployeeMasters=null;
					    						 ///if(!EMPLOYEEID.trim().equalsIgnoreCase("")){
					    							 try{
					    								 List<EmployeeMaster> lstEmployeeMasters1=new ArrayList<EmployeeMaster> ();
					    								 
					    								 if(!EMPLOYEEID.trim().equalsIgnoreCase(""))
					    									 lstEmployeeMasters1=employeeMasterDAO.findByCriteria(Restrictions.eq("employeeCode",EMPLOYEEID.trim()), Restrictions.eq("districtMaster", um.getDistrictId()));
					    								 if(lstEmployeeMasters1.size()==0)
					    									 lstEmployeeMasters1=employeeMasterDAO.findByCriteria(Restrictions.eq("emailAddress",EMAIL_LOGIN.trim()), Restrictions.eq("districtMaster", um.getDistrictId()));
					    								 
						    							 if(lstEmployeeMasters1.size()>0){
						    								 lstEmployeeMasters=createEmployeeMaster(um, EMPLOYEEID.trim(),"exists", lstSchoolMaster, lstEmployeeMasters1.get(0));				    							
						    							 }else{
						    								 if(!EMPLOYEEID.trim().equalsIgnoreCase(""))
						    								 lstEmployeeMasters=createEmployeeMaster(um, EMPLOYEEID.trim(),"new", lstSchoolMaster,null);
						    							 }					    							 
						    						}catch (NumberFormatException e) {
					    								 e.printStackTrace();														
													}catch (Exception e) {
														 e.printStackTrace();
														 responseStatus=false;
													}
					    							 
					    						/// }					    						 
					    						 if(lstEmployeeMasters ==null || lstEmployeeMasters.size()>0){
						    						if(lstEmployeeMasters!=null){
						    							EmployeeMaster em=lstEmployeeMasters.get(0);
						    							em.setEmailAddress(um.getEmailAddress());
						    							em.setFirstName(um.getFirstName());
						    							em.setLastName(um.getLastName());
						    							
						    						    um.setEmployeeMaster(em);
						    						}
						    						for(SchoolMaster sm:lstSchoolMaster){
						    							um.setUserId(null);
							    						um.setSchoolId(sm);
							    						userMasterDAO.makePersistent(um);
						    						}
						    						
						    						//Start 
						    						String mailDenyTemplate=AcceptOrDeclineMailHtml.sendAddUserByAPIMailNotification(um,districtMaster,request);
						    						System.out.println("mailDenyTemplate==========="+mailDenyTemplate);
						    						new Thread(new DemoScheduleMailThread(emailerService,null,um.getEmailAddress(),"ram.thakur@mailinator.com","I Have Added You As a User",mailDenyTemplate)).start();
						    						System.out.println("Ok====="+mailDenyTemplate);
						    						//End
						    						
						    						System.out.println("Add user admin successfully ******"+lstRoleMaster.get(0).getRoleName()+"****userId=="+um.getUserId()+"*** : in district "+districtMaster);
						    				 		responseStatus=true;
					    						 }else{
					    							 	errorCode="10051";
							    				 		errorMsg="Another user already exists on this employee Id.";
							    				 		responseStatus=false;
					    						 }
				    					 }
		    						 }else{
		    							 	locationJSONList.removeAll(locationExistsList);
		    							 	System.out.println("List========"+locationJSONList);
		    							 	errorCode="10047";
				    				 		errorMsg="Location Code does not exist"+locationJSONList;
				    				 		responseStatus=false; 
		    						 }
		    					 }else{
		    						 	errorCode="10061";
			    				 		errorMsg="Invalid login email address";
			    				 		responseStatus=false;		    						 
		    					 }
		    				 }else{
		    					 errorCode="10048";
				    			 errorMsg="Role does not exist";
				    			 responseStatus=false;
		    				 }
		    			 }else{	
		    				 if(ROLE.trim().equalsIgnoreCase("DA") || ROLE.trim().equalsIgnoreCase("DT")){
		    				 List<RoleMaster> lstRoleMaster=roleMasterDAO.findByCriteria(Restrictions.eq("roleName", roleLst.get(ROLE.trim())));
			    			 if(lstRoleMaster.size()>0){
		    					 if(validateEmailAddress(EMAIL_LOGIN.trim())){		    						
		    						 List<UserMaster> lstUserMaster=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster));//,Restrictions.eq("roleId", lstRoleMaster.get(0)),Restrictions.eq("entityType", lstRoleMaster.get(0).getEntityType())
				    					 if(lstUserMaster.size()>0){
				    						 errorCode="10043";
							    			 errorMsg="Duplicate login email address.";
							    			 responseStatus=false;	
				    					 }else{	
				    						 UserMaster um= new UserMaster();
					    						um.setStatus("A");
					    						um.setFirstName(FIRST_NAME);
					    						um.setLastName(LAST_NAME);
					    						um.setEmailAddress(EMAIL_LOGIN);
					    						um.setPassword(MD5Encryption.toMD5("teachermatch"));
					    						um.setRoleId(lstRoleMaster.get(0));
					    						um.setEntityType(lstRoleMaster.get(0).getEntityType());
					    						um.setDistrictId(districtMaster);
					    						um.setForgetCounter(0);
					    						um.setIsQuestCandidate(false);
					    						um.setCreatedDateTime(new Date());
					    						int verificationCode=(int) Math.round(Math.random() * 2000000);
					    						um.setVerificationCode(verificationCode+"");
					    						um.setAuthenticationCode(verificationCode+"");
					    						if(!TITLE.trim().equalsIgnoreCase(""))
					    							um.setTitle(TITLE);
				    						 	List<EmployeeMaster> lstEmployeeMasters=null;
					    						 //if(!EMPLOYEEID.trim().equalsIgnoreCase("")){
				    						 	 
			    								 
					    							 try{
					    								 List<EmployeeMaster> lstEmployeeMasters1=new ArrayList<EmployeeMaster> ();
					    								 if(!EMPLOYEEID.trim().equalsIgnoreCase(""))
					    									 lstEmployeeMasters1=employeeMasterDAO.findByCriteria(Restrictions.eq("employeeCode",EMPLOYEEID.trim()), Restrictions.eq("districtMaster", um.getDistrictId()));
					    								 if(lstEmployeeMasters1.size()==0)
					    									 lstEmployeeMasters1=employeeMasterDAO.findByCriteria(Restrictions.eq("emailAddress",EMAIL_LOGIN.trim()), Restrictions.eq("districtMaster", um.getDistrictId()));
						    							 if(lstEmployeeMasters1.size()>0){
						    								 lstEmployeeMasters=createEmployeeMaster(um, EMPLOYEEID.trim(),"exists", null, lstEmployeeMasters1.get(0));				    							
						    							 }else{
						    								 if(!EMPLOYEEID.trim().equalsIgnoreCase(""))
						    								 lstEmployeeMasters=createEmployeeMaster(um, EMPLOYEEID.trim(),"new", null,null);
						    							 }
					    							 }catch (NumberFormatException e) {
					    								 e.printStackTrace();														
													}catch (Exception e) {
														 e.printStackTrace();
														 responseStatus=false;
													}
					    							 
					    						// }					    						 
					    						 if(lstEmployeeMasters ==null || lstEmployeeMasters.size()>0){
					    							if(lstEmployeeMasters!=null){
					    								EmployeeMaster em=lstEmployeeMasters.get(0);
					    								em.setEmailAddress(um.getEmailAddress());
						    							em.setFirstName(um.getFirstName());
						    							em.setLastName(um.getLastName());
						    							um.setEmployeeMaster(em);	
					    							}
						    						userMasterDAO.makePersistent(um);
						    						
						    						//Start 
						    						String mailDenyTemplate=AcceptOrDeclineMailHtml.sendAddUserByAPIMailNotification(um,districtMaster,request);
						    						System.out.println("mailDenyTemplate==========="+mailDenyTemplate);
						    						new Thread(new DemoScheduleMailThread(emailerService,null,um.getEmailAddress(),"ram.thakur@mailinator.com","I Have Added You As a User",mailDenyTemplate)).start();
						    						System.out.println("Ok====="+mailDenyTemplate);
						    						//End
						    						
						    						System.out.println("Add user successfully ******"+lstRoleMaster.get(0).getRoleName()+"****userId=="+um.getUserId()+"*** : in district "+districtMaster);
						    				 		responseStatus=true;
					    						 }else{
					    							 	errorCode="10051";
							    				 		errorMsg="Another user already exists on this employee Id.";
							    				 		responseStatus=false;
					    						 }
				    					 }
		    					 }else{
		    						 	errorCode="10061";
			    				 		errorMsg="Invalid login email address";
			    				 		responseStatus=false;		    						 
		    					 }
		    				 }else{
		    					 errorCode="10048";
				    			 errorMsg="Role does not exist";
				    			 responseStatus=false;
		    				 }
		    				}else{
		    					errorCode="10052";
	    				 		errorMsg="Location code is required";
	    				 		responseStatus=false;
		    				}
		    			 }
		    		 }
		    	 }else if(action.equalsIgnoreCase("update")){
		    		 System.out.println("/**********************UPDATE ACTION***************************/");
		    		 List<SchoolMaster> lstSchoolMaster=new ArrayList<SchoolMaster>();
		    		 List<RoleMaster> lstRoleMaster=new ArrayList<RoleMaster>();
		    		 Map<String,SchoolMaster> smMap=new LinkedHashMap<String,SchoolMaster>();
		    		 
		    		 System.out.println("/***********************************************changeInfo***********************************************/");
					 System.out.println("Location Code Exists for schoolMaster322 : "+DEPTID);
		    		 if(ROLE.trim().equalsIgnoreCase("")){
		    			 errorCode="10046";
		    			 errorMsg="Role is required";
		    			 responseStatus=false;
		    		 }
		    		 if(EMAIL_LOGIN.trim().equalsIgnoreCase("") && EMPLOYEEID.trim().equalsIgnoreCase("")){
		    			 errorCode="10058";
		    			 errorMsg="Login email address or employee id is required";
		    			 responseStatus=false;
		    		 }
		    		 if(!STATUS.trim().equalsIgnoreCase(""))
					 {
		    			 String status = STATUS.trim();
		    			 if(!(status.equalsIgnoreCase("active") || status.equalsIgnoreCase("Inactive") || status.equalsIgnoreCase("")) )
		    			 //if(status.equalsIgnoreCase("") || status.equalsIgnoreCase(""))
		    			 {
		    				 errorCode="10071";
					 		 errorMsg="Invalid status";
					 		 responseStatus=false;
		    			 }
					 }else{
						 	errorCode="10075";
					 		errorMsg="Status is required";
					 		responseStatus=false;
					 }
		    		 if(!EMAIL_LOGIN.trim().equals("") && !validateEmailAddress(EMAIL_LOGIN.trim())){
			    		 	errorCode="10061";
					 		errorMsg="Invalid login email address";
					 		responseStatus=false;
		    		 }
		    		 if(!ROLE.trim().equalsIgnoreCase("") && !ROLE.trim().equalsIgnoreCase("SA") && !ROLE.trim().equalsIgnoreCase("ST") && !ROLE.trim().equalsIgnoreCase("DA") && !ROLE.trim().equalsIgnoreCase("DT")){
		    			 errorCode="10053";
		    			 errorMsg="Wrong Role for add school";
		    			 responseStatus=false;
		    		 }
		    		 if(LAST_NAME.trim().equalsIgnoreCase("")){
		    			 errorCode="10045";
		    			 errorMsg="Last Name is required";
		    			 responseStatus=false; 
		    		 }
		    		 if(FIRST_NAME.trim().equalsIgnoreCase("")){
		    			 errorCode="10044";
		    			 errorMsg="First Name is required";
		    			 responseStatus=false;
		    		 }
		    		 
		    		 if(!DEPTID.trim().equalsIgnoreCase("")){
		    			 lstSchoolMaster=schoolMasterDAO.findByCriteria(Restrictions.in("locationCode", locationJSONList),Restrictions.eq("districtId", districtMaster));
		    			 for(SchoolMaster sm:lstSchoolMaster){
		    				 locationExistsList.add(sm.getLocationCode());
		    				 smMap.put(sm.getLocationCode(), sm);
		    			 }
		    		 } 
		    		 if(lstSchoolMaster.size()>0 && lstSchoolMaster.size()<locationJSONList.size()){
		    			 	locationJSONList.removeAll(locationExistsList)	;
 							errorCode="10047";
		    				errorMsg="Location Code does not exist"+locationJSONList+"";
		    				responseStatus=false;
		    		 }else if(!DEPTID.trim().equalsIgnoreCase("") && (lstSchoolMaster==null || lstSchoolMaster.size()==0)){
		    			 	errorCode="10047";
		    				errorMsg="Location Code does not exist"+locationJSONList+"";
		    				responseStatus=false;
		    		 }
		    		 if(responseStatus)
		    		 {
		    			 
		    			 if(!ROLE.trim().equalsIgnoreCase("")){
		    			 	lstRoleMaster=roleMasterDAO.findByCriteria(Restrictions.eq("roleName", roleLst.get(ROLE.trim())));
		    			 }
		    			 List<UserMaster> lstUserMaster=new ArrayList<UserMaster>();
		    			 boolean schoolIdBlankWithDA=false;
		    			 /*if(DEPTID.trim().equalsIgnoreCase("") && (ROLE.trim().equalsIgnoreCase("DA") || ROLE.trim().equalsIgnoreCase("DT")))
		    			 lstUserMaster=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster),Restrictions.isNull("schoolId"));
		    			 else*/
		    			 if(!EMPLOYEEID.trim().equals(""))
		    			 {
		    				 List<EmployeeMaster> lstEmployeeMastersSearchList=employeeMasterDAO.findByCriteria(Restrictions.eq("employeeCode",EMPLOYEEID.trim()), Restrictions.eq("districtMaster", districtMaster));
		    				 if(lstEmployeeMastersSearchList!=null && lstEmployeeMastersSearchList.size()>0)
		    					 lstUserMaster=userMasterDAO.findByCriteria(Restrictions.eq("employeeMaster", lstEmployeeMastersSearchList.get(0)),Restrictions.eq("districtId", districtMaster));
		    				 else if(!EMAIL_LOGIN.trim().equalsIgnoreCase(""))
		    					 lstUserMaster=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster));
		    				if(lstUserMaster.isEmpty() && !EMAIL_LOGIN.trim().equalsIgnoreCase(""))
		    					 lstUserMaster=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster));
		    			 }else
		    				 lstUserMaster=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster));
		    			 
		    			 
		    			 boolean flagDAtoSA=false;
	    				 if(lstRoleMaster.size()>0 && lstUserMaster.size()>0 && lstUserMaster.get(0).getRoleId().getRoleName().trim().equalsIgnoreCase(roleLst.get("DA")) && lstRoleMaster.get(0).getRoleName().trim().equalsIgnoreCase(roleLst.get("SA"))){
	    					 flagDAtoSA=true;
	    				 }
	    				 if(lstSchoolMaster==null || lstSchoolMaster.size()==0){ schoolIdBlankWithDA=true;}
	    				 if(lstSchoolMaster.size()>0 && lstUserMaster.size()>0){ // && lstUserMaster.size()<locationJSONList.size()
		    				 locationExistsList.clear();
								for(UserMaster um:lstUserMaster){
									if(um.getSchoolId()!=null)
									locationExistsList.add(um.getSchoolId().getLocationCode());
									else
									schoolIdBlankWithDA=true;
								}
								locationJSONList.removeAll(locationExistsList)	;
								for(String loc:locationJSONList){
									UserMaster um=new UserMaster();
									BeanUtils.copyProperties(um, lstUserMaster.get(0));
									um.setUserId(null);
									um.setSchoolId(smMap.get(loc.trim()));
									boolean flagAdd=true;
									if(flagDAtoSA ){
										for(UserMaster obj:lstUserMaster){
											if(obj.getSchoolId()==null){
												obj.setSchoolId(um.getSchoolId());
												flagDAtoSA=false;
												flagAdd=false;
											}
										}
									}
									if(flagAdd)
									lstUserMaster.add(um);
								}
			    				//errorMsg="User does not exist on location code"+locationJSONList+"";
							}
		    			 List<EmployeeMaster> lstEmployeeMasters=null;
		    			 if(responseStatus && lstUserMaster.size()>0){
		    				 boolean flagupdate=true;
		    				 boolean isUpdateEmailAddress=false;
							 UserMaster um = lstUserMaster.get(0);
							 System.out.println("lstRoleMaster==="+lstRoleMaster.size());
							 System.out.println("um.getRoleId().getRoleName()==="+um.getRoleId().getRoleName()+"=="+roleLst.get("DA"));
							 System.out.println("lstRoleMaster.get(0).getRoleName()==="+lstRoleMaster.get(0).getRoleName()+"=="+roleLst.get("SA"));
							 System.out.println("locationJSONList.size()==="+locationJSONList.size());
							 System.out.println("schoolIdBlankWithDA==="+schoolIdBlankWithDA);
							 System.out.println("email:"+um.getEmailAddress());
							 String role="";
	    					 for(UserMaster umObj:lstUserMaster){
	    						 if(!role.contains(roleLst1.get(umObj.getRoleId().getRoleName())))
	    						 role+=roleLst1.get(umObj.getRoleId().getRoleName())+"|";
	    						 System.out.println("role====="+role);
	    					 }
							 	if(lstRoleMaster.size()>0 && um.getRoleId().getRoleName().trim().equalsIgnoreCase(roleLst.get("DA")) && lstRoleMaster.get(0).getRoleName().trim().equalsIgnoreCase(roleLst.get("SA")) && locationJSONList.size()==0 && schoolIdBlankWithDA && !(role.contains("SA") && role.contains("DA"))){
									errorCode="10052";
		    				 		errorMsg="Location code is required";
		    				 		responseStatus=false;
								}
							 	if(responseStatus){
										 //update the usermasterList
			    					 	 String firstName = !FIRST_NAME.trim().equalsIgnoreCase("")? FIRST_NAME.trim() : um.getFirstName();
										 String lastName =  !LAST_NAME.trim().equalsIgnoreCase("")? LAST_NAME.trim() : um.getLastName();
										 String status = STATUS.trim().equalsIgnoreCase("Active")?"A":(STATUS.trim().equalsIgnoreCase("Inactive")? "I" : "");
										 String title = TITLE.trim().equalsIgnoreCase("")? "" : TITLE.trim();
										 
										//remove all + operator from String
										 firstName = firstName.replaceAll("\\+", " ");
										 lastName = lastName.replaceAll("\\+", " ");
										 status = status.replaceAll("\\+", " ");
										 title = title.replaceAll("\\+", " ");
									 
										 for(UserMaster userMaster : lstUserMaster)	//Update the usermaster and employmaster.
											 {
												 userMaster.setFirstName(firstName);
												 userMaster.setLastName(lastName);
												 if(lstRoleMaster.size()>0){
												 userMaster.setRoleId(lstRoleMaster.get(0));
												 userMaster.setEntityType(lstRoleMaster.get(0).getEntityType());
												 }
												 if(!status.equalsIgnoreCase(""))
													 userMaster.setStatus(status);
												 //if(!title.equalsIgnoreCase(""))
													 if(title.trim().equalsIgnoreCase("null"))
														 userMaster.setTitle("");
													 else
														 userMaster.setTitle(title);
												 
													 
												 EmployeeMaster employeeMaster = userMaster.getEmployeeMaster();
												 if(employeeMaster!=null)
												 {
													 employeeMaster.setFirstName(firstName);
													 employeeMaster.setLastName(lastName);
													 if(!status.equalsIgnoreCase(""))
														 employeeMaster.setStatus(status);
												 }
												 userMaster.setEmployeeMaster(employeeMaster);
											 }
										 if(!EMAIL_LOGIN.trim().equalsIgnoreCase("") && !EMPLOYEEID.trim().equalsIgnoreCase(""))	//If emailAddress is exist then update the userMaster
										 {
											 boolean flagForEmployeeIdAssignAnotherUser=false;
											 List<UserMaster> lstUserMaster1=null;
											 lstUserMaster1=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster));
											 if(lstUserMaster1!=null && lstUserMaster1.size()>0){
												 if(lstUserMaster1.get(0).getEmployeeMaster()==null){
													 flagForEmployeeIdAssignAnotherUser=false;
												 }else if(um.getEmployeeMaster()!=null && !um.getEmployeeMaster().getEmployeeCode().trim().equals(lstUserMaster1.get(0).getEmployeeMaster().getEmployeeCode().trim()))
						    								flagForEmployeeIdAssignAnotherUser=true;
					    						}
											 System.out.println("lstUserMaster1:- "+lstUserMaster1);
											 
											 if(!flagForEmployeeIdAssignAnotherUser){	//If Employee does not exist the create new one other use previous employee.
												 isUpdateEmailAddress=true;
											 }else{
												 	errorCode="10081";
									    			errorMsg="Another user already exists on this login email address.";
									    			flagupdate=false;
									    			responseStatus=false;
											 }
										 }
										 if(!EMPLOYEEID.trim().equalsIgnoreCase(""))	//If Employ Code is exist then update the employMaster
											 {
												 boolean flagForEmployeeIdAssignAnotherUser=false;
												 List<EmployeeMaster> lstEmployeeMasters1=null;
												 lstEmployeeMasters1=employeeMasterDAO.findByCriteria(Restrictions.eq("employeeCode",EMPLOYEEID.trim()), Restrictions.eq("districtMaster", um.getDistrictId()));
												 if(lstEmployeeMasters1!=null && lstEmployeeMasters1.size()>0){
							    						if(!um.getEmailAddress().trim().equalsIgnoreCase(lstEmployeeMasters1.get(0).getEmailAddress().trim()))
							    								flagForEmployeeIdAssignAnotherUser=true;
						    						}
						    						
												 /*if(lstEmployeeMasters1==null || lstEmployeeMasters1.size()==0)
													 lstEmployeeMasters1=employeeMasterDAO.findByCriteria(Restrictions.eq("emailAddress",EMAIL_LOGIN.trim()), Restrictions.eq("districtMaster", um.getDistrictId()) );*/
												 
												 System.out.println("lstEmployeeMasters1:- "+lstEmployeeMasters1);
												 
												 if(lstEmployeeMasters1==null || lstEmployeeMasters1.size()==0)	//If Employee does not exist the create new one other use previous employee.
													 lstEmployeeMasters = createEmployeeMaster(um, EMPLOYEEID.trim(),"new", lstSchoolMaster,null);
												 else
													 lstEmployeeMasters = lstEmployeeMasters1;
												 
												 System.out.println("lstEmployeeMasters:- "+lstEmployeeMasters);
												 if(!flagForEmployeeIdAssignAnotherUser && lstEmployeeMasters!=null && lstEmployeeMasters.size()>0)
												 {
													 EmployeeMaster employeeMaster = lstEmployeeMasters.get(0);
					    							 
													 //update the EmployeeMaster
					    							 employeeMaster.setFirstName(firstName);
					    							 employeeMaster.setLastName(lastName);
					    							//vishwanath
					    							 employeeMaster.setEmailAddress(EMAIL_LOGIN.trim());
					    							 if(!status.equalsIgnoreCase(""))
														 employeeMaster.setStatus(status);
					    							 
					    							 System.out.println("employeeMaster\nfirstName:- "+employeeMaster.getFirstName()+",LastName:-  "+employeeMaster.getLastName()+", Id:- "+employeeMaster.getEmployeeId()+", code:- "+employeeMaster.getEmployeeCode());
					    							 //update UserMaster
					    							 for(UserMaster userMaster : lstUserMaster)
					    								 userMaster.setEmployeeMaster(employeeMaster);
												}
											    else
					    						{
					    							errorCode="10051";
									    			errorMsg="Another user already exists on this employee Id.";
									    			flagupdate=false;
									    			responseStatus=false;
							    				}
											 }
									 
											 System.out.println("flagupdate:- "+flagupdate+",responseStatus:- "+responseStatus);
											 if(flagupdate)
											 {
												 System.out.println("Employmaster is going to update");
												 if(lstEmployeeMasters!=null && lstEmployeeMasters.size()>0)
												 {
													 System.out.println(lstEmployeeMasters.get(0).getEmailAddress()+",Code:-  "+lstEmployeeMasters.get(0).getEmployeeCode()+", Id:- "+lstEmployeeMasters.get(0).getEmployeeId()+" is updated.");
													 EmployeeMaster employeeMasterupdate =  lstEmployeeMasters.get(0);
													 //vishwanath 
													 employeeMasterupdate.setEmailAddress(EMAIL_LOGIN.trim());
													 employeeMasterDAO.makePersistent(employeeMasterupdate);
												 }
												 
												 System.out.println("UserMaster is going to update");
												 if(lstUserMaster!=null && lstUserMaster.size()>0)
												 {
													 for(UserMaster userMaster : lstUserMaster)
													 {
														 //System.out.println(userMaster.getEmailAddress()+", "+userMaster.getUserId()+" is updated.");
														// System.out.println("userMaster.getSchoolId()=="+userMaster.getSchoolId().getLocationCode()+"   locationExistsMap=="+locationExistsMap.get(userMaster.getSchoolId().getLocationCode()));
														 if(userMaster.getSchoolId()!=null && locationExistsMap.get(userMaster.getSchoolId().getLocationCode())==null){
															 if(!userMaster.getRoleId().getRoleName().equals(roleLst.get("DA")) && !userMaster.getRoleId().getRoleName().equals(roleLst.get("DT")))
															 	userMaster.setStatus("I");
															 else
															 {
																if(smMap!=null && smMap.size()>0 && userMaster.getSchoolId()!=null){
																	if(smMap.get(userMaster.getSchoolId().getLocationCode())==null)
																		userMaster.setStatus("I");	
																}
															 }
														 }
														 if(isUpdateEmailAddress)userMaster.setEmailAddress(EMAIL_LOGIN.trim());
														 userMasterDAO.makePersistent(userMaster);
													 }
												 }
												 
												 responseStatus=true;
												 System.out.println("***************changeInfo********userId=="+um.getUserId()+"*********************");
											 }
									 }
		    			 }else if(responseStatus){
		    				 boolean flag =true;
		    				 {
		    					 	 if(lstEmployeeMasters!=null)lstEmployeeMasters.clear();
		    						 lstEmployeeMasters=employeeMasterDAO.findByCriteria(Restrictions.eq("employeeCode",EMPLOYEEID.trim()), Restrictions.eq("districtMaster", districtMaster));
			    						if(lstEmployeeMasters.size()>0){
			    							List<UserMaster> lstUserMasterEmail=null;
			    							 /*if(DEPTID.trim().equalsIgnoreCase("") && (ROLE.trim().equalsIgnoreCase("DA") || ROLE.trim().equalsIgnoreCase("DT")))
				    						 lstUserMasterEmail=userMasterDAO.findByCriteria(Restrictions.eq("employeeMaster", lstEmployeeMasters.get(0)),Restrictions.eq("districtId", districtMaster),Restrictions.isNull("schoolId"));
			    							 else*/
			    								lstUserMasterEmail=userMasterDAO.findByCriteria(Restrictions.eq("employeeMaster", lstEmployeeMasters.get(0)),Restrictions.eq("districtId", districtMaster));
			    								flagDAtoSA=false;
			    			    				 if(lstRoleMaster.size()>0 && lstUserMaster.size()>0 && lstUserMaster.get(0).getRoleId().getRoleName().trim().equalsIgnoreCase(roleLst.get("DA")) && lstRoleMaster.get(0).getRoleName().trim().equalsIgnoreCase(roleLst.get("SA"))){
			    			    					 flagDAtoSA=true;
			    			    				 }
			    			    				 if(lstSchoolMaster==null || lstSchoolMaster.size()==0){ schoolIdBlankWithDA=true;}
						    							if(lstSchoolMaster.size()>0 && lstUserMasterEmail.size()>0){ // && lstUserMasterEmail.size()<locationJSONList.size()
							    								locationExistsList.clear();
							    								for(UserMaster um:lstUserMasterEmail){
							    									if(um.getSchoolId()!=null)
							    									locationExistsList.add(um.getSchoolId().getLocationCode());
							    								}
							    								
							    								locationJSONList.removeAll(locationExistsList)	;
							    								for(String loc:locationJSONList){
							    									UserMaster um=new UserMaster();
							    									if(lstUserMaster.size()>0)
							    										BeanUtils.copyProperties(um, lstUserMaster.get(0));
							    									/*else
							    										BeanUtils.copyProperties(um, lstUserMasterEmail.get(0));*/
							    									
							    									um.setUserId(null);
							    									um.setSchoolId(smMap.get(loc.trim()));
							    									boolean flagAdd=true;
							    									if(flagDAtoSA ){
							    										for(UserMaster obj:lstUserMaster){
							    											if(obj.getSchoolId()==null){
							    												obj.setSchoolId(um.getSchoolId());
							    												flagDAtoSA=false;
							    												flagAdd=false;
							    											}
							    										}
							    									}
							    									if(flagAdd)
							    									lstUserMaster.add(um);
							    								}
							    			    				//errorMsg="User does not exist on location code"+locationJSONList+"";
							    			    				/*flag =false;
							    			    				responseStatus=false;*/
						    							}
			    							
					    								if(responseStatus && lstEmployeeMasters!=null && lstUserMasterEmail!=null && lstEmployeeMasters.size()>0 && lstUserMasterEmail.size()>0)
							    						{
							    							List<UserMaster> lstUserMasterCheck=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster));
							    							if(lstUserMasterCheck==null || lstUserMasterCheck.size()==0){
							    								
								    							flag=false;
								    							UserMaster userObject=lstUserMasterEmail.get(0);
									    							if(lstRoleMaster.size()>0 && userObject.getRoleId().getRoleName().trim().equalsIgnoreCase(roleLst.get("DA")) && lstRoleMaster.get(0).getRoleName().trim().equalsIgnoreCase(roleLst.get("SA")) && locationJSONList.size()==0){
								    									errorCode="10052";
								    		    				 		errorMsg="Location code is required";
								    		    				 		responseStatus=false;
								    								}
									    							if(responseStatus){
									    								for(UserMaster um:lstUserMasterEmail){	
										    							EmployeeMaster employeeMaster = lstEmployeeMasters.get(0);
										    							
										    							//update the usermasterList
										    							String firstName = !FIRST_NAME.trim().equalsIgnoreCase("")? FIRST_NAME.trim() : um.getFirstName();
										    							String lastName =  !LAST_NAME.trim().equalsIgnoreCase("")? LAST_NAME.trim() : um.getLastName();
										    							String status = STATUS.trim().equalsIgnoreCase("Active")?"A":(STATUS.trim().equalsIgnoreCase("Inactive")? "I" : "");
										    							String title = TITLE.trim().equalsIgnoreCase("")? "" : TITLE.trim();
										    							
										    							//remove all + operator from String
										    							firstName = firstName.replaceAll("\\+", " ");
										    							lastName = lastName.replaceAll("\\+", " ");
										    							status = status.replaceAll("\\+", " ");
										    							title = title.replaceAll("\\+", " ");
										    							
										    							//update the usermasterList
										    							um.setFirstName(firstName);
										    							um.setLastName(lastName);
										    							um.setEmailAddress(EMAIL_LOGIN.trim());
										    							if(lstRoleMaster.size()>0){
											    							um.setRoleId(lstRoleMaster.get(0));
											    							um.setEntityType(lstRoleMaster.get(0).getEntityType());
										    							}
										    							//if(!title.equalsIgnoreCase(""))
										    								if(title.trim().equalsIgnoreCase("null"))
									    										 um.setTitle("");
									    									 else
									    										 um.setTitle(title);
									    								if(!status.equalsIgnoreCase(""))
										    							{
								    										 employeeMaster.setStatus(status);
								    										 um.setStatus(status);
								    										 System.out.println("Status is updated02:- "+status);
										    							}
										    								
										    							 
										    							employeeMaster.setFirstName(firstName);
										    							employeeMaster.setLastName(lastName);
										    							employeeMaster.setEmailAddress(EMAIL_LOGIN.trim());
										    							
										    							um.setEmployeeMaster(lstEmployeeMasters.get(0));
									    							
											    						if(um.getEmployeeMaster()!=null)
											    						{
											    							employeeMasterDAO.updatePersistent(um.getEmployeeMaster());
											    							System.out.println("um.getEmployeeMaster().getStatus():- "+um.getEmployeeMaster().getStatus());
											    						}
											    						 //System.out.println("um.getSchoolId()=="+um.getSchoolId().getLocationCode()+"   locationExistsMap=="+locationExistsMap.get(um.getSchoolId().getLocationCode()));
																		 if(um.getSchoolId()!=null && locationExistsMap.get(um.getSchoolId().getLocationCode())==null)
																			 if(!um.getRoleId().getRoleName().equals(roleLst.get("DA")) && !um.getRoleId().getRoleName().equals(roleLst.get("DT")))
																				 um.setStatus("I");
																			 else
																			 {
																					if(smMap!=null && smMap.size()>0 && um.getSchoolId()!=null){
																						if(smMap.get(um.getSchoolId().getLocationCode())==null)
																							um.setStatus("I");	
																					}
																			  }
											    						userMasterDAO.makePersistent(um);
											    						responseStatus=true;
											    						System.out.println("***************changeInfo********userId=="+um.getUserId()+"*********************");
									    							}
									    							}
							    							}else{
							    								 flag=false;
								    							 errorCode="10070";
								    			    			 errorMsg="This email login is already assigned to another user";
								    			    			 responseStatus = false;
							    							}
							    						}
			    							 	
			    						}else{
		    							 flag=false;
		    							 errorCode="10066";
		    			    			 errorMsg="Employee does not exist";
		    			    			 responseStatus = false; 
		    						}
		    					}
		    				        if(flag){
	    							errorCode="10004";
						    		errorMsg="User does not exist";
						    		responseStatus=false;
	    							}
		    			 }
		    		 }
		    	 }//End of if
		    	 else if(action.equalsIgnoreCase("delete")){
		    		 System.out.println("/**********************DELETE ACTION***************************/");
		    		 if(!DEPTID.trim().equalsIgnoreCase("")){
		    			 if(!ROLE.trim().equalsIgnoreCase("")){
		    			 List<SchoolMaster> lstSchoolMaster=schoolMasterDAO.findByCriteria(Restrictions.in("locationCode", locationJSONList));
		    			 for(SchoolMaster sm:lstSchoolMaster)
							 locationExistsList.add(sm.getLocationCode());
		    			 List<RoleMaster> lstRoleMaster=roleMasterDAO.findByCriteria(Restrictions.eq("roleName", roleLst.get(ROLE.trim())));
		    			 if(lstSchoolMaster.size()>0 && lstSchoolMaster.size()==locationJSONList.size()){
		    				 if(lstRoleMaster.size()>0){
		    					 if(!EMAIL_LOGIN.trim().equalsIgnoreCase("")){
		    					 List<UserMaster> lstUserMaster=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster),Restrictions.eq("roleId", lstRoleMaster.get(0)),Restrictions.in("schoolId", lstSchoolMaster));
			    					 if(lstUserMaster.size()>0){
			    						 for(UserMaster um:lstUserMaster){
			    						//UserMaster um= lstUserMaster.get(0);
				    						um.setStatus("I");
				    						userMasterDAO.updatePersistent(um);
				    						System.out.println("Deactivate user admin successfully ********userId=="+um.getUserId()+"***** : in district "+districtMaster);
			    						 }
			    				 		responseStatus=true;	
			    					 }else{
			    						 errorCode="10004";
						    			 errorMsg="User does not exist";
						    			 responseStatus=false;
			    					 }
		    					 }else{
		    						 errorCode="10002";
					    			 errorMsg="Login email address is required";
					    			 responseStatus=false;		    						 
		    					 }
		    				 }else{
		    					 errorCode="10048";
				    			 errorMsg="Role does not exist";
				    			 responseStatus=false;
		    				 }		    				 
		    			 }else{
		    				 locationJSONList.removeAll(locationExistsList);
		    				 errorCode="10047";
			    			 errorMsg="Location Code does not exist"+locationJSONList+"";
			    			 responseStatus=false; 
		    			 }
		    			 }else{
		    				 errorCode="10045";
			    			 errorMsg="Role is required";
			    			 responseStatus=false;
		    			 }
		    		 }else{
		    			 if(!ROLE.trim().equalsIgnoreCase("")){		    			
		    			 List<RoleMaster> lstRoleMaster=roleMasterDAO.findByCriteria(Restrictions.eq("roleName", roleLst.get(ROLE.trim())));
		    				 if(lstRoleMaster.size()>0){
		    					 if(!EMAIL_LOGIN.trim().equalsIgnoreCase("")){
		    						 List<UserMaster> lstUserMaster=userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", EMAIL_LOGIN.trim()),Restrictions.eq("districtId", districtMaster),Restrictions.eq("roleId", lstRoleMaster.get(0)));
			    					 if(lstUserMaster.size()>0){
			    						for(UserMaster um:lstUserMaster){
			    						um.setStatus("I");
			    						userMasterDAO.updatePersistent(um);
			    						System.out.println("Deactivate user admin successfully *******userId=="+um.getUserId()+"****** : in district "+districtMaster);
			    				 		responseStatus=true;	
			    						}
			    					 }else{
			    						 errorCode="10004";
						    			 errorMsg="User does not exist";
						    			 responseStatus=false;
			    					 }
		    					 }else{
		    						 errorCode="10058";
					    			 errorMsg="Login email address is required";
					    			 responseStatus=false;		    						 
		    					 }
		    				 }else{
		    					 errorCode="10048";
				    			 errorMsg="Role does not exist";
				    			 responseStatus=false;
		    				 }		    				 		    			 
		    			 }else{
		    				 errorCode="10045";
			    			 errorMsg="Role is required";
			    			 responseStatus=false;
		    			 }
		    		 
		    		 }		    		 		    		 
		    	 }else{
		    		 errorCode="10057";
	    			 errorMsg="Action does not exist";
	    			 responseStatus=false;
		    	 }
	    	 }
	    	 JSONObject responseMSG=getResponseMessage(responseStatus,errorCode,errorMsg,null);
	    	 respondMessage=responseMSG.toString();
	    	 out.println(responseMSG);
	   	 }catch(Exception e){
	   		 e.printStackTrace();
	   		 JSONObject responseMSG=getResponseMessage(responseStatus,errorCode,errorMsg,null);
	    	 respondMessage=responseMSG.toString();
	    	 out.println(responseMSG);
	   		 }
    
   	 }catch(Exception e){
   		 e.printStackTrace();   		
   	 }
   	insertTrackingAPIHistory(userMasterDAO.getSessionFactory(), "/service/provisionUpdate.do", requestContent, respondMessage);
    }
    
    private JSONObject getResponseMessage(boolean responseStatus,String errorCode,String errorMsg,JSONObject json){
    	JSONObject jsonResponse = new JSONObject();
    	if(responseStatus){
    		if(json!=null){
    			jsonResponse=json;
    		}else{
	   		 jsonResponse.put("status",true);
	   		 jsonResponse.put("timeStamp",UtilityAPI.getCurrentTimeStamp());  		
    		}
    		System.out.println("Message :******************: "+"Success Response"+jsonResponse.toString());
   	 }else{
   		 jsonResponse.put("status",false);
   		 jsonResponse.put("timeStamp",UtilityAPI.getCurrentTimeStamp());
   		 jsonResponse.put("errorCode",errorCode);
   		 jsonResponse.put("errorMessage",errorMsg);   		        			 	        		
   		 System.out.println("Message :******************: "+"Failure Response"+jsonResponse.toString());
   	 }
    	return jsonResponse;
    }
    
    @Transactional(readOnly=false)
	private List<EmployeeMaster> createEmployeeMaster(UserMaster um, String employeeCode, String checkStatus, List<SchoolMaster> lstSchoolMaster, EmployeeMaster employeeMaster)throws Exception{
    	List<EmployeeMaster> listEmployeeMaster=null;
    		if(checkStatus.equals("new")){
    		EmployeeMaster em=new EmployeeMaster();
    		em.setDistrictMaster(um.getDistrictId());
    		em.setFirstName(um.getFirstName());
    		em.setLastName(um.getLastName());
    		em.setMiddleName(um.getMiddleName()!=null?um.getMiddleName():"");
    		em.setEmailAddress(um.getEmailAddress());
    		em.setEmployeeCode(employeeCode);
    		em.setSSN("");
    		em.setStatus(um.getStatus());
    		em.setCreatedDateTime(um.getCreatedDateTime());
    		employeeMasterDAO.getSessionFactory().openSession().saveOrUpdate(em);
    		listEmployeeMaster=new ArrayList<EmployeeMaster>();
    		listEmployeeMaster.add(em);
    		return listEmployeeMaster;
    		}else if(checkStatus.equals("exists")){
    			List<UserMaster> lstUserMaster=new ArrayList<UserMaster>();
    			List<Criterion> listRestriction=new ArrayList<Criterion>();
    			listRestriction.add(Restrictions.eq("districtId", um.getDistrictId()));
    			listRestriction.add(Restrictions.eq("employeeMaster", employeeMaster));
    			//listRestriction.add(Restrictions.eq("roleId", um.getRoleId()));
    			//listRestriction.add(Restrictions.eq("entityType", um.getEntityType()));
    			/*if(lstSchoolMaster!=null){
    			listRestriction.add(Restrictions.in("schoolId", lstSchoolMaster));*/
    			lstUserMaster=userMasterDAO.findByCriteria(listRestriction.toArray(new Criterion[listRestriction.size()]));
    			/*}else{
    				listRestriction.add(Restrictions.isNull("schoolId"));
    				lstUserMaster=userMasterDAO.findByCriteria(listRestriction.toArray(new Criterion[listRestriction.size()]));
    			}*/
    			if(lstUserMaster!=null && lstUserMaster.size()>0){
    				listEmployeeMaster=new ArrayList<EmployeeMaster>();
    				return listEmployeeMaster;
    			}else{
    				listEmployeeMaster=new ArrayList<EmployeeMaster>();
    				employeeMaster.setFirstName(um.getFirstName());
    				employeeMaster.setLastName(um.getLastName());
    				employeeMaster.setMiddleName(um.getMiddleName()!=null?um.getMiddleName():"");
    				employeeMaster.setEmailAddress(um.getEmailAddress());
    				employeeMasterDAO.updatePersistent(employeeMaster);
    				listEmployeeMaster.add(employeeMaster);
    				return listEmployeeMaster;
    			}
    		}
    		listEmployeeMaster=new ArrayList<EmployeeMaster>();
			return listEmployeeMaster;
    }
    
    @RequestMapping(value="/service/updatePositionNo.do", method=RequestMethod.POST)
    public String updatePostionNumberInTMByPOST(ModelMap map, HttpServletRequest request, HttpServletResponse response){
   	
   	 System.out.println("********************updatePostionNumberInTMByPOST**************************");
   	 try{
   		     PrintWriter out = response.getWriter();
   	         DistrictMaster districtMaster = null;
   	         UserMaster userMaster = null;
   	         JobOrder jobOrder = null;
   	         SchoolMaster schoolMstr = null;
   	         
   	         HttpSession session = request.getSession();
   	         
   	         JSONObject jsonResponse = new JSONObject();
   			 response.setContentType("application/json");
   	         
                List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = null;
                List<JobRequisitionNumbers> lstJobRequisitionNumbers = null; 
   	         List<UserMaster> lstUserMaster = null;
                
   	         String AUTH_KEY = null;
   	         String ACTION = null;
   	         String LOCATION_ID = null;
   	         String JOB_TITLE = null;
   	         String POSITION_NBR = null;
   	         
   	         
   	         boolean responseStatus = false;
   	         String errorMsg ="Invalid Credentials.";
   	         Integer errorCode = 10001;
   	         
   	         String requestContent ="";
   	         
   	         try{
   	        	 DataInputStream wr = new DataInputStream(request.getInputStream());
   	        	 int readByte=0;
   	    		 while((readByte=wr.read())!=-1){
   	    			 requestContent+=(char)readByte;
   	    		 }
   	        	 
   	        	 if(requestContent==null){
   	        		 requestContent = ServiceUtility.getBody(request);
   	        	 }
   	        	 
   	        	 System.out.println("requestContent:::::::::: "+requestContent);
   	        	 
   	        	DocumentBuilderFactory builderFactoryReq = null;
   	     	    DocumentBuilder docBuilderReq = null;
   	     	    org.w3c.dom.Document docReq = null;
   	     	    
   	     	    try {
   	     	            builderFactoryReq = DocumentBuilderFactory.newInstance();
   	     		     	docBuilderReq = builderFactoryReq.newDocumentBuilder();
   	     		     	
   	     		} catch (ParserConfigurationException e) {
   	     			e.printStackTrace();
   	     		}
   	    		  
   	    		 if (requestContent!=null)
   	    		 {
   	    			 docReq = docBuilderReq.parse(new InputSource(new StringReader(requestContent)));
   	    		     docReq.getDocumentElement().normalize();
   	    		     System.out.println("docReq "+docReq);
   	    		 
   	    		     NodeList jobCodeUpdateNode = docReq.getElementsByTagName("Transaction");
   	    		     
   	    		     System.out.println("jobCodeUpdateNode : "+jobCodeUpdateNode.getLength());
   	    		     if(jobCodeUpdateNode.getLength()<1){
   	    		    	 jobCodeUpdateNode = docReq.getElementsByTagName("jobCodeUpdate");
   	    		     }
   	    		     
   	    		     for(int i = 0;i<jobCodeUpdateNode.getLength();i++){
   	    		    	 
   	    		    	 Node nNode = jobCodeUpdateNode.item(i);
   	    			    	
   	    			    	if (nNode.getNodeType() == Node.ELEMENT_NODE) {
   	    			    		 
   	    						Element eElement = (Element) nNode;
   	    			 
   	    						if(eElement.getElementsByTagName("authKey").getLength()>0){
   	    							AUTH_KEY = eElement.getElementsByTagName("authKey").item(0).getTextContent();
   	    				    		}
   	    			
   	    						if(eElement.getElementsByTagName("JC_TM_ACTION").getLength()>0){
   	    							ACTION = eElement.getElementsByTagName("JC_TM_ACTION").item(0).getTextContent();
   	    				    		}
   	    						
   	    						if(eElement.getElementsByTagName("DEPTID").getLength()>0){
   	    							LOCATION_ID = eElement.getElementsByTagName("DEPTID").item(0).getTextContent();
   	    				    		}
   	    						
   	    						if(eElement.getElementsByTagName("DESCR").getLength()>0){
   	    							JOB_TITLE = eElement.getElementsByTagName("DESCR").item(0).getTextContent();
   	    				    		}
   	    		    	 
   	    						if(eElement.getElementsByTagName("Position_NBR").getLength()>0){
   	    							POSITION_NBR = eElement.getElementsByTagName("Position_NBR").item(0).getTextContent();
   	    				    	}
   	    			    	}
   	    		        }
   	    		     System.out.println("AUTHKEY:: "+AUTH_KEY);
   	    			 System.out.println("ACTION :"+ACTION);
   	    			 System.out.println("LOCATION_ID :"+LOCATION_ID);
   	    			 System.out.println("JOB_TITLE :"+JOB_TITLE);
   	    			 System.out.println("POSITION_NBR :"+POSITION_NBR);
   	    		 }
   	    		 
   	        	 if(AUTH_KEY!=null){
   	        		 
   	        		 districtMaster = districtMasterDAO.validateDistrictByAuthkey(AUTH_KEY);
   	        		 if(districtMaster!=null){
   	        			 session.setAttribute("apiDistrictMaster", districtMaster);
   	        			 responseStatus = true;
   	        			 System.out.println("********District Validated********"+districtMaster);
   	        		 }else{
   		        		 responseStatus = false;
   		        		 errorCode = 10001;
   		        		 errorMsg = "Invalid Credentials.";
   		        	 }
   	        	 }else{
   	        		 responseStatus = false;
   	        		 errorCode = 10001;
   	        		 errorMsg = "Invalid Credentials.";
   	        	 }
   	        	 
   	        	 if(responseStatus){
   	        		  System.out.println("Entering 1st responseStatus block************** ");
   	        		  
   	        		 Criterion cri1 = Restrictions.eq("locationCode", LOCATION_ID);
   					 Criterion cri2 = Restrictions.eq("districtId", districtMaster);
   					 List<SchoolMaster> lstSchoolMaster = schoolMasterDAO.findByCriteria(cri1,cri2);
   					 if(lstSchoolMaster!=null){
   						 if(lstSchoolMaster.size()>0){
   							schoolMstr = lstSchoolMaster.get(0); 
   						 }
   					 }
   	        		  
   	        		  if(POSITION_NBR!=null){
   	        		  
   	        			  Criterion criteria1 = Restrictions.eq("requisitionNumber", POSITION_NBR);
   	        			  Criterion criteria2 = Restrictions.eq("isUsed", true);
   	        		      lstDistrictRequisitionNumbers = districtRequisitionNumbersDAO.findByCriteria(criteria1,criteria2);
   	        		      
   	        		      System.out.println("lstDistrictRequisitionNumbers ************* size: "+lstDistrictRequisitionNumbers);
   	        		  }else{
   	        			     responseStatus = false;
   			        		 errorCode = 10025;
   			        		 errorMsg = "Position No. is required";
   	        		  }
   	        		
   	        		  if(lstDistrictRequisitionNumbers!=null && lstDistrictRequisitionNumbers.size()>0){
   	        			  Criterion criteria1 = Restrictions.in("districtRequisitionNumbers", lstDistrictRequisitionNumbers);
   	        			  Criterion criteria2 = Restrictions.eq("status", 0);
   	        			  lstJobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(criteria1,criteria2);
   	        			
   	        			  System.out.println("lstJobRequisitionNumbers ************* size: "+lstJobRequisitionNumbers.size());
   	        		  }
   	        			 if(responseStatus){
   	        			 
   	        				 if(ACTION.equalsIgnoreCase("add"))
   	        				 {
   	        					  System.out.println("INSIDE ADD ");
   	        					  List<JobRequisitionNumbers> lstJobRequisitionNumbers1=null;
   	        					  
   		        				  lstDistrictRequisitionNumbers = districtRequisitionNumbersDAO.findByCriteria(Restrictions.eq("requisitionNumber", POSITION_NBR));
   		        				  if(lstDistrictRequisitionNumbers!=null && lstDistrictRequisitionNumbers.size()>0)
     		        				  lstJobRequisitionNumbers1 = jobRequisitionNumbersDAO.findByCriteria(Restrictions.in("districtRequisitionNumbers", lstDistrictRequisitionNumbers));
   		        				  
   		        				  Map<Integer, JobRequisitionNumbers> jobMap = new HashMap<Integer, JobRequisitionNumbers>();
   		        				  DistrictRequisitionNumbers districtRequisitionNumbers=null;
   		        				  
   		        				  if(lstJobRequisitionNumbers1!=null && lstJobRequisitionNumbers1.size()>0)
   		        				  for(JobRequisitionNumbers jrn : lstJobRequisitionNumbers1)
   		        					  jobMap.put(jrn.getDistrictRequisitionNumbers().getDistrictRequisitionId(), jrn);
   		        				  
   		        				  if(lstDistrictRequisitionNumbers!=null && lstDistrictRequisitionNumbers.size()>0)
   		        				  for(DistrictRequisitionNumbers drn : lstDistrictRequisitionNumbers)
   		        				  {
   		        					  JobRequisitionNumbers jrn = jobMap.get(drn.getDistrictRequisitionId());
   		        					  if(jrn==null || jrn.getStatus()==0|| jrn.getStatus()==1)
   		        					  {
   		        						  if(jrn==null){
	   		        						  districtRequisitionNumbers = drn;
	   		        						  break;
   		        					      }
   		        					  }
   		        				  }
	        					  
   		        				  if(districtRequisitionNumbers==null)	//Add new DistrictRequisition Number
   		        				  {
   		        					  DistrictRequisitionNumbers districtRequisitionNumbers2 = new DistrictRequisitionNumbers();
   		        					  districtRequisitionNumbers2.setDistrictMaster(districtMaster);
   		        					  districtRequisitionNumbers2.setIsUsed(false);
   		        					  districtRequisitionNumbers2.setStatus("A");
   		        					  districtRequisitionNumbers2.setPosType("F");
   		        					  districtRequisitionNumbers2.setRequisitionNumber(POSITION_NBR);
   		        					  districtRequisitionNumbers2.setJobTitle(JOB_TITLE);
 		        					  
 		        					  if(schoolMstr!=null)
 		        						  districtRequisitionNumbers2.setSchoolMaster(schoolMstr);
 		        					  
 		        					  Criterion criteria3 = Restrictions.eq("districtId", districtMaster);
 		        					  lstUserMaster = userMasterDAO.findByCriteria(criteria3);
 		        					  
 		        					  if(lstUserMaster!=null)
 		        					  {
 		        						  userMaster = lstUserMaster.get(0);
 		        						  if(userMaster!=null)
 		        							  districtRequisitionNumbers2.setUserMaster(userMaster);
 		        					  }
 		        					  districtRequisitionNumbers2.setCreatedDateTime(new Date());
 		        					  
 		        					  districtRequisitionNumbersDAO.makePersistent(districtRequisitionNumbers2);
 		        					  System.out.println("Position Added Successfully in districtRequisitionNumbers table: "+POSITION_NBR);
 		        					  
 		        					  responseStatus = true;
 		        					  errorMsg = "Success.";
   		        				  }
   		        				  else
   		        				  if(districtRequisitionNumbers.getStatus().equalsIgnoreCase("I"))	//Set Active And Inactive Requisition Number
   		        				  {
   		        					  districtRequisitionNumbers.setStatus("A");
   		        					  
   		        					  SessionFactory sessionFact = districtRequisitionNumbersDAO.getSessionFactory();
   		        					  StatelessSession statLessSession = sessionFact.openStatelessSession();
   		        					  Transaction trans = statLessSession.beginTransaction();
   		        					  statLessSession.update(districtRequisitionNumbers);
   		        					  trans.commit();
   		        					  statLessSession.close();
   		        					  
   		        					  responseStatus = true;
   		        					  errorMsg = "Success.";
   		        					  System.out.println("Existing Position Activated *successfully***********");
   		        				  }
   		        				  else if(districtRequisitionNumbers.getIsUsed()==false)	//If districtrequistion number is not used
   		        				  {
	        						  responseStatus = true;
  		        					  errorMsg = "Success.";
  		        					  System.out.println("districtrequistionnumber is not used so response return *successfully***********");
   		        				  }
   		        				  else
   		        				  {
   		        					  responseStatus = false;
   		        					  errorCode = 10055;
   		        					  errorMsg = "Position nos, "+POSITION_NBR+" already exist.";
   		        				  }
   	        				 }
   	        				  
   	        				if(ACTION.equalsIgnoreCase("update")){
   	   	        				System.out.println("INSIDE UPDATE lstJobRequisitionNumbers "+lstJobRequisitionNumbers+" and lstDistrictRequisitionNumbers:"+lstDistrictRequisitionNumbers);
   	   	        				      Criterion criteria1 = Restrictions.eq("requisitionNumber", POSITION_NBR);
   			        				  lstDistrictRequisitionNumbers = districtRequisitionNumbersDAO.findByCriteria(criteria1);
   	   	        				
   			        				  if(lstDistrictRequisitionNumbers!=null){
   			        					  if(lstDistrictRequisitionNumbers.size()>0){
   			        						  
   			        						     DistrictRequisitionNumbers  districtRequisitionNumbers = lstDistrictRequisitionNumbers.get(0);
   			    	        					 System.out.println("districtRequisitionNumbers.getIsUsed(): "+districtRequisitionNumbers.getIsUsed());
   			        						     if(districtRequisitionNumbers.getIsUsed()==false){
   			        						    	
   			        						    	 districtRequisitionNumbers.setDistrictMaster(districtMaster);
   				    	        					
   			        						    	 if(JOB_TITLE!=null){
   			        						    	 districtRequisitionNumbers.setJobTitle(JOB_TITLE);
   			        						    	 }
   				    	        					 Criterion criteria3 = Restrictions.eq("districtId", districtMaster);
   				    	        					 lstUserMaster = userMasterDAO.findByCriteria(criteria3);
   				    	        					 
   				    	        					 if(lstUserMaster!=null){
   				    	        					 userMaster = lstUserMaster.get(0);
   				    	        					
   				    	        					 if(userMaster!=null){
   				    	        					 districtRequisitionNumbers.setUserMaster(userMaster);
   				    	        					 }
   				    	        					 
   				    	        					if(schoolMstr!=null){
   				    	        					    districtRequisitionNumbers.setSchoolMaster(schoolMstr);
   				    	        					}
   				    	        					 
   				    	        					 districtRequisitionNumbers.setCreatedDateTime(new Date());
   				    	        					}
   				    	        					 districtRequisitionNumbersDAO.makePersistent(districtRequisitionNumbers);
   				    	        					 System.out.println("Position Updated Successfully in districtRequisitionNumbers table: "+POSITION_NBR);
   				    	        					    
   				    	        					    responseStatus = true;
   				    	   	        				    errorMsg = "Success"; 
   				    	   	        				    
   			        						     }else  if(districtRequisitionNumbers.getIsUsed()==true){
   			        						    	 	    Criterion criterion12 = Restrictions.eq("districtRequisitionNumbers", districtRequisitionNumbers);
   			        						    	 	    lstJobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(criterion12);
   			        						    	 	    if(lstJobRequisitionNumbers!=null){
   			        						    	 	    	if(lstJobRequisitionNumbers.size()>0){
   			        						    	 	    		
   			        						    	 	    		JobRequisitionNumbers jobRea = lstJobRequisitionNumbers.get(0);
   			        						    	 	    		if(jobRea.getStatus()!=null && jobRea.getStatus()==0){
   			        						    	 	    			jobOrder =jobRea.getJobOrder();
   			        						    	 	    			
   			        						    	 	    			 if(LOCATION_ID!=null){
   			        					   			        				   
   			        					   			        					districtMaster.setLocationCode(LOCATION_ID);
   			        					   			        				    SessionFactory factory = districtMasterDAO.getSessionFactory();
   			        					   			        				    StatelessSession stateLessSession = factory.openStatelessSession();;
   			        					   			        				    Transaction txOpen = stateLessSession.beginTransaction();
   			        					   			        				    stateLessSession.update(districtMaster);
   			        					   			        				    txOpen.commit();
   			        					   			        				    stateLessSession.close();
   			        					   			        				    System.out.println("districtMaster Updated successfully ************* : "+districtMaster+" LOCATION_ID: "+LOCATION_ID);
   			        					   			        				  }
   			        					   			        				 
   			        					   			        				    if(JOB_TITLE!=null){
   			        					   			        				    jobOrder.setJobTitle(JOB_TITLE);
   			        					   			        				    
   			        					   			        				    SessionFactory factory = jobOrderDAO.getSessionFactory();
   			        					   			        				    StatelessSession stateLessSession = factory.openStatelessSession();
   			        					   			        				    Transaction txOpen = stateLessSession.beginTransaction();
   			        					   			        				    stateLessSession.update(jobOrder);
   			        					   			        				    txOpen.commit();
   			        					   			        				    stateLessSession.close();
   			        					   			        				    System.out.println("jobOrder Updated successfully ************* : "+" JOB_TITLE after: "+jobOrder.getJobTitle());
   			        					   			        				    
   			        					   			        				    }
   			        						    	 	    			
   			        					   			        				    responseStatus = true;
   			        					    	   	        				    errorMsg = "Success"; 
   			        					   			        				    
   			        						    	 	    		}else if(jobRea.getStatus()!=null && jobRea.getStatus()==1){
   			        						    	 	    			 responseStatus = false;
   			        				   					        		 errorCode = 10041;
   			        				   					        		 errorMsg = "Position nos, "+POSITION_NBR+" is already In used.";
   			        						    	 	    		}
   			        						    	 	    	}
   			        						    	 	    }
   			        						     }else{
   			        						    	 responseStatus = false;
   			   						        		 errorCode = 10042;
   			   						        		 errorMsg = "Position nos, "+POSITION_NBR+" does not exist.";
   			        						     }
   			        					  }else{
   			        						     responseStatus = false;
   		   						        		 errorCode = 10042;
   		   						        		 errorMsg = "Position nos, "+POSITION_NBR+" does not exist.";
   			        					  }
   			        				  }else{
   		        						     responseStatus = false;
   	   						        		 errorCode = 10042;
   	   						        		 errorMsg = "Position nos, "+POSITION_NBR+" does not exist.";
   		        					  }
   	   	        			  }
   	        				  
   	        		   if(ACTION.equalsIgnoreCase("delete")){
   	        			   
   	        			       System.out.println("IN DELETE lstJobRequisitionNumbers : "+lstJobRequisitionNumbers);
   	        			       boolean isJobRequitionExists=false;
   	        			       Criterion criteria6 = Restrictions.eq("requisitionNumber", POSITION_NBR);
   	        			       Criterion criteria_district = Restrictions.eq("districtMaster", districtMaster);
   	        			       List<DistrictRequisitionNumbers>  lstDistrictRequisitionNumbers1 = districtRequisitionNumbersDAO.findByCriteria(criteria6,criteria_district);
   	        				   System.out.println("lstDistrictRequisitionNumbers1((((((((((((((((( *************: "+lstDistrictRequisitionNumbers1);
   	        				   
   	        				   List<JobRequisitionNumbers>  lstJobRequisitionNumbers1 = null;
   	        				   if(lstDistrictRequisitionNumbers1!=null && lstDistrictRequisitionNumbers1.size()>0)//Code for removing the districtREquestionNumbers
   	        				   {
   	        					   Criterion criteria7 = Restrictions.in("districtRequisitionNumbers", lstDistrictRequisitionNumbers1);
   	        					   lstJobRequisitionNumbers1 = jobRequisitionNumbersDAO.findByCriteria(criteria7);
   	        					   
   	        					   DistrictRequisitionNumbers deletedDisReqObj=null;
   	        					   
   	        					   Map<Integer, JobRequisitionNumbers> jobMap = new HashMap<Integer, JobRequisitionNumbers>();
   	        					   if(lstJobRequisitionNumbers1!=null && lstJobRequisitionNumbers1.size()>0)
   	        					   for(JobRequisitionNumbers jobRequisitionNumbers : lstJobRequisitionNumbers1)
   	        						   jobMap.put(jobRequisitionNumbers.getDistrictRequisitionNumbers().getDistrictRequisitionId(), jobRequisitionNumbers);
   	        					   JobRequisitionNumbers jobRequisitionNumbers=null;
   	        					   for(DistrictRequisitionNumbers districtRequisitionNumbers : lstDistrictRequisitionNumbers1)
        						   {
        							   jobRequisitionNumbers = jobMap.get(districtRequisitionNumbers.getDistrictRequisitionId());
        							  // if(jobRequisitionNumbers==null || jobRequisitionNumbers.getStatus()==0)
        							   //{
        								   	   if(jobRequisitionNumbers!=null)
        									   isJobRequitionExists=true;
        								   deletedDisReqObj = districtRequisitionNumbers;
        								   break;
        							   //}
        						   }
   	        					   boolean nextFlag=true;
   	        					   if(deletedDisReqObj==null)
   	        					   {
   	        						   nextFlag=false;
   	        						   responseStatus = true;
   	        						   errorMsg = "Success.";
   	        						  //errorCode = 10041;
   	        						  //errorMsg = "Position nos, "+POSITION_NBR+" is already In used.";
   	        					   }
   	        					   
   	        					   System.out.println("deletedDisReqObj:- "+deletedDisReqObj);
   	        					   if(nextFlag && deletedDisReqObj!=null)
   	        					   {
   	        						   System.out.println("deletedDisReqObj.getStatus():- "+deletedDisReqObj.getStatus());
   	        						   if(deletedDisReqObj.getStatus()!=null && deletedDisReqObj.getStatus().equalsIgnoreCase("A") && !deletedDisReqObj.getIsUsed())
   	        						   {
   	        							   /*if(isJobRequitionExists){
   	        							   deletedDisReqObj.setStatus("I");
   	        							   SessionFactory sessionFact = districtRequisitionNumbersDAO.getSessionFactory();
   	        							   StatelessSession statLessSession = sessionFact.openStatelessSession();
   	        							   Transaction trans = statLessSession.beginTransaction();
   	        							   statLessSession.update(deletedDisReqObj);
   	        							   trans.commit();
   	        							   statLessSession.close();
   	        							   }else{
   	        								   List<Jobrequisitionnumberstemp> jobRequisitionNumbersTempList=jobRequisitionNumbersTempDAO.findByDRID(deletedDisReqObj);
   	        								   SessionFactory sessionFact = districtRequisitionNumbersDAO.getSessionFactory();
     	        							   StatelessSession statLessSession = sessionFact.openStatelessSession();
     	        							   Transaction trans = statLessSession.beginTransaction();
     	        							   if(jobRequisitionNumbersTempList.size()>0)
     	        								   for(Jobrequisitionnumberstemp drnt:jobRequisitionNumbersTempList)
     	        									  statLessSession.delete(drnt);
     	        							   statLessSession.delete(deletedDisReqObj);
     	        							   trans.commit();
     	        							   statLessSession.close();
   	        							   }*/
   	        							   
   	        							    if(isJobRequitionExists){
	    	        							 
    	        							   }else{
	    	        							   deletedDisReqObj.setStatus("I");
	    	        							   SessionFactory sessionFact = districtRequisitionNumbersDAO.getSessionFactory();
	    	        							   StatelessSession statLessSession = sessionFact.openStatelessSession();
	    	        							   Transaction trans = statLessSession.beginTransaction();
	    	        							   statLessSession.update(deletedDisReqObj);
	    	        							   trans.commit();
	    	        							   statLessSession.close();
    	        							   }
   	        							   
   	        							   responseStatus = true;
   	        							   errorMsg = "Success";
   	        						   }else if(deletedDisReqObj.getStatus()!=null && deletedDisReqObj.getStatus().equalsIgnoreCase("A") && deletedDisReqObj.getIsUsed()){
   	        							   
   	        							 if(isJobRequitionExists)
   	        							 {
   	        								//Check for hiring on Requisition Number or Not
   	        								JobOrder job=jobRequisitionNumbers.getJobOrder();
   	        								List<JobForTeacher> listJobForTeachers = jobForTeacherDAO.findByCriteria(Restrictions.eq("requisitionNumber",deletedDisReqObj.getRequisitionNumber()),Restrictions.eq("jobId",jobRequisitionNumbers.getJobOrder()));
   	        								if(listJobForTeachers!=null && listJobForTeachers.size()>0){//1)Candidate is hired on that Requisition Number...
   	        									responseStatus = true;
   	        							        errorMsg = "Success";
   	        							    }else{//2)Candidate is not hired on that Requisition Number...
   	        							    	if(job.getStatus().equalsIgnoreCase("I")){//Job is InActive
   	        							    		//Do Nothing
   	        							    	}else{//Job is Active or may be Expire_______//Setting Job to In-Active
   	        							    	    job.setStatus("I");
   	        							    	    SessionFactory sessionFectory = jobOrderDAO.getSessionFactory();
	    	        							    StatelessSession statlessSession = sessionFectory.openStatelessSession();
	    	        							    Transaction trans = statlessSession.beginTransaction();
	    	        							    statlessSession.update(job);
	    	        							    trans.commit();
	    	        							    statlessSession.close();
	    	        							    new ElasticSearchService().updateESSchoolJobOrderByJobId(job);
   	        							    	}
   	        							     responseStatus = true;
	        							     errorMsg = "Success";
   	        							    }
   	        							}
   	        						   }else if(deletedDisReqObj.getStatus()!=null && deletedDisReqObj.getStatus().equalsIgnoreCase("I"))
   	        							    {
	   	        								if(isJobRequitionExists){
	   	        								   responseStatus = true;
	   	        								   errorMsg = "Success";//"Position nos, "+POSITION_NBR+" already Deactivated.";
	   	        								}else{
	   	        								   List<Jobrequisitionnumberstemp> jobRequisitionNumbersTempList=jobRequisitionNumbersTempDAO.findByDRID(deletedDisReqObj);
	   	        								   SessionFactory sessionFact = districtRequisitionNumbersDAO.getSessionFactory();
	      	        							   StatelessSession statLessSession = sessionFact.openStatelessSession();
	      	        							   Transaction trans = statLessSession.beginTransaction();
	      	        							   if(jobRequisitionNumbersTempList.size()>0)
	     	        								   for(Jobrequisitionnumberstemp drnt:jobRequisitionNumbersTempList)
	     	        									  statLessSession.delete(drnt);
	      	        							   statLessSession.delete(deletedDisReqObj);
	      	        							   trans.commit();
	      	        							   statLessSession.close();
	      	        							   responseStatus = true;
	   	        								   errorMsg = "Success";
	   	        								}
   	        							   }
   	        					   }
   	        				   }
   	        				   else
   	        				   {
   	        					   responseStatus = false;
   	        					   errorCode = 10042;
   	        					   errorMsg = "Position nos, "+POSITION_NBR+" does not exist.";
   	        				   }
   	        		   }
   	        		}//end inside response status flag
   	        	 }
   	        	 if(responseStatus){
   	        		    jsonResponse = writeJSONReponses( true, 1, "Success: " +  UtilityAPI.getCurrentTimeStamp(), null);	
	
   						
   						System.out.println("jsonResponse :********IN valid case**********: "+jsonResponse);
   	        	 }else{
   	        		 jsonResponse = writeJSONReponses(false, errorCode, errorMsg, null);
   	        		 System.out.println("jsonResponse :******IN case of FAILD************: "+jsonResponse);
   	        	 }
   	         }catch (Exception e) {
   				 e.printStackTrace();
   				 jsonResponse = writeJSONReponses(false, 10017, "Server Error.", e);
   			}
   	         out.print(jsonResponse);
   	 }catch(Exception e){
   		 e.printStackTrace();
   	 }
   	 return null;
    }
    
    @RequestMapping(value="/service/xml/locationUpdate.do", method=RequestMethod.POST)
    public String locationUpdateByXMLPOST(ModelMap map, HttpServletRequest request,HttpServletResponse response){
   	 
   	 System.out.println("************************locationUpdateByXMLPOST***************************");
   	 try{
   	 	 PrintWriter out = response.getWriter();
   		 HttpSession session = request.getSession();
   		 
   		 JSONObject jsonResponse = new JSONObject();
   		 
   		 DistrictMaster districtMaster = null;
   		 SchoolMaster schoolMaster = null;
   		 StateMaster stateMaster = null;
   		 List<SchoolTypeMaster> schoolTypeMaster = null;
   		 SchoolTypeMaster schoolTypeMastr = null;
   		
   	     String AUTHKEY = "";
   		 String DEPTID = "";
   		 String DESCR = "";
   		 String JC_TM_SCHL_TYPE = "";
   		 String ADDRESS1 = "";
   		 String ADDRESS2 = "";
   		 String CITY = "";
   		 String STATE = "";
   		 String POSTAL = "";
   		 String exitURL = "";
   		 String JC_TM_ACTION = null;
   		 
   		 boolean responseStatus = true;
   		 Integer errorCode = 10001;
   		 String errorMsg = "Invalid Credentials.";
   		 
   		 String LOCATION_CODE = null;
   		 
   		 DataInputStream wr = new DataInputStream(request.getInputStream());
   		 
   		 try{
   		 String soapRequestMessage ="";
   		 int readByte=0;
   		 while((readByte=wr.read())!=-1){
   			 soapRequestMessage+=(char)readByte;
   		 }
   		 if(soapRequestMessage==null){
   			 try{
   				 soapRequestMessage = ServiceUtility.getBody(request); 
   			 }catch(Exception e){
   				 e.printStackTrace();
   			 }
   		 }
   		 System.out.println("soapRequestMessage :: "+soapRequestMessage);
   	
   		DocumentBuilderFactory builderFactoryReq = null;
   	    DocumentBuilder docBuilderReq = null;
   	    org.w3c.dom.Document docReq = null;
   	    
   	    try {
   	            builderFactoryReq = DocumentBuilderFactory.newInstance();
   		     	docBuilderReq = builderFactoryReq.newDocumentBuilder();
   		     	
   		} catch (ParserConfigurationException e) {
   			e.printStackTrace();
   		}
   		  
   		 if(soapRequestMessage!=null){
   			 docReq = docBuilderReq.parse(new InputSource(new StringReader(soapRequestMessage)));
   		     docReq.getDocumentElement().normalize();
   		
   		 NodeList RESULT = docReq.getElementsByTagName("JC_TM_LOC_RQST");

   		 System.out.println("docReq  ::::: "+docReq + "RESULT : "+RESULT);
   		 
   		 for(int i = 0;i<RESULT.getLength();i++){
   		    	
   		    	Node nNode = RESULT.item(i);
   		    	
   		    	if (nNode.getNodeType() == Node.ELEMENT_NODE) {
   		    		 
   					Element eElement = (Element) nNode;
   		 
   					if(eElement.getElementsByTagName("AUTH_TOKEN").getLength()>0){
   						AUTHKEY = eElement.getElementsByTagName("AUTH_TOKEN").item(0).getTextContent();
   			    		}
   			    		
   		    		if(eElement.getElementsByTagName("DEPTID").getLength()>0){
   		    			DEPTID = eElement.getElementsByTagName("DEPTID").item(0).getTextContent();
   		    		}
   		    		
   		    		if(eElement.getElementsByTagName("DESCR").getLength()>0){
   		    			DESCR = eElement.getElementsByTagName("DESCR").item(0).getTextContent();
   			    		}
   			    		
   		    		if(eElement.getElementsByTagName("JC_TM_SCHL_TYPE").getLength()>0){
   		    			JC_TM_SCHL_TYPE = eElement.getElementsByTagName("JC_TM_SCHL_TYPE").item(0).getTextContent();
   		    		}
   					
   		    		if(eElement.getElementsByTagName("ADDRESS1").getLength()>0){
   		    			ADDRESS1 = eElement.getElementsByTagName("ADDRESS1").item(0).getTextContent();
   			    		}
   			    		
   		    		if(eElement.getElementsByTagName("ADDRESS2").getLength()>0){
   		    			ADDRESS2 = eElement.getElementsByTagName("ADDRESS2").item(0).getTextContent();
   		    		}
   		    		
   		    		if(eElement.getElementsByTagName("CITY").getLength()>0){
   		    			CITY = eElement.getElementsByTagName("CITY").item(0).getTextContent();
   		    		}
   					
   		    		if(eElement.getElementsByTagName("STATE").getLength()>0){
   		    			STATE = eElement.getElementsByTagName("STATE").item(0).getTextContent();
   			    		}
   			    		
   		    		if(eElement.getElementsByTagName("POSTAL").getLength()>0){
   		    			POSTAL = eElement.getElementsByTagName("POSTAL").item(0).getTextContent();
   		    		}
   					
   		    		if(eElement.getElementsByTagName("URL").getLength()>0){
   		    			exitURL = eElement.getElementsByTagName("URL").item(0).getTextContent();
   		    		}
   		    		
   		    		if(eElement.getElementsByTagName("JC_TM_ACTION").getLength()>0){
   		    			JC_TM_ACTION = eElement.getElementsByTagName("JC_TM_ACTION").item(0).getTextContent();
   		    		}
   		    	}
   		    }
   			 
   		 /*System.out.println("AUTHKEY:: "+AUTHKEY);
   		 System.out.println("DEPTID :"+DEPTID);
   		 System.out.println("DESCR :"+DESCR);
   		 System.out.println("JC_TM_SCHL_TYPE :"+JC_TM_SCHL_TYPE);
   		 System.out.println("ADDRESS1 :"+ADDRESS1);
   		 System.out.println("ADDRESS2 :"+ADDRESS2);
   		 System.out.println("CITY :"+CITY);
   		 System.out.println("STATE :"+STATE);
   		 System.out.println("POSTAL :"+POSTAL);
   		 System.out.println("ExitURL :"+exitURL);*/
   		 System.out.println("JC_TM_ACTION :"+JC_TM_ACTION);
   		 }
   		 
   		 if(AUTHKEY!=null){
   			 districtMaster = districtMasterDAO.validateDistrictByAuthkey(AUTHKEY);
   			 if(districtMaster!=null){
   				 session.setAttribute("apiDistrictMaster", districtMaster);
   				 responseStatus = true;
   				 System.out.println("validating districtMaster :: "+districtMaster);
   			 }else{
   				    schoolMaster = schoolMasterDAO.validateSchoolByAuthkey(AUTHKEY);
   					 if(schoolMaster!=null){ 
   						 session.setAttribute("apiSchoolMaster", schoolMaster);
   						 session.setAttribute("apiDistrictMaster", schoolMaster.getDistrictId());
   						 responseStatus = true;
   						 System.out.println("validating schoolMaster :: "+schoolMaster);
   				      }else{
   				    	     responseStatus=false;
   							 errorCode=10001;
   							 errorMsg="Invalid Credentials. Supplied AUTH_KEY: " + AUTHKEY;
   				      }
   			 }
   			 System.out.println("districtMaster :: "+districtMaster+" schoolMaster:: "+schoolMaster); 
   			 if(districtMaster==null){
   				 responseStatus=false;
   				 errorCode=10001;
   				 errorMsg="Invalid Credentials. Supplied AUTH_KEY: " + AUTHKEY;
   			 }
   		 }else{
   			 responseStatus=false;
   			 errorCode=10001;
   			 errorMsg="Invalid Credentials. Supplied AUTH_KEY: " + AUTHKEY;
   		 }
   		
   		 List<SchoolMaster> lstSchoolMaster = null;
   		
   		 if(responseStatus){
   			 System.out.println("*************entering ResponseStatus if block************* ");
   		
   			 if(districtMaster!=null){
   				 
   				 Criterion criteria1 = Restrictions.eq("districtId", districtMaster);
   				 Criterion criteria2 = Restrictions.eq("locationCode", DEPTID);
   				 lstSchoolMaster = schoolMasterDAO.findByCriteria(criteria1,criteria2);
   				 System.out.println("lstSchoolMaster for districtMaster "+lstSchoolMaster.size());
   			 }else if(schoolMaster!=null){
   				 Criterion criteria1 = Restrictions.eq("schoolId", schoolMaster);
   				 Criterion criteria2 = Restrictions.eq("locationCode", DEPTID);
   				 lstSchoolMaster = schoolMasterDAO.findByCriteria(criteria1,criteria2);
   				 System.out.println("lstSchoolMaster for schoolMaster "+lstSchoolMaster.size());
   			 }
   			 if(JC_TM_ACTION.equalsIgnoreCase("change")){  //JC_TM_ACTION.equalsIgnoreCase("update") ||
   				 System.out.println("******IN update/change JC_TM_ACTION************::: "+JC_TM_ACTION+" lstSchoolMaster: "+lstSchoolMaster);
   			 if(lstSchoolMaster.size()>0){
   			       schoolMaster = lstSchoolMaster.get(0);
   			       
   			       System.out.println("Location Code Exist for schoolMaster : "+LOCATION_CODE);
   					 
   					 schoolMaster.setLocationCode(DEPTID);
   					 schoolMaster.setSchoolName(DESCR);
   					 
   					 String tmSchoolTypeName = matchJCToTMSchoolType(JC_TM_SCHL_TYPE);
   					 
   					  Criterion criterion1 = Restrictions.eq("schoolTypeName", tmSchoolTypeName);
   					  schoolTypeMaster = schoolTypeMasterDAO.findByCriteria(criterion1);
   					  System.out.println("schoolTypeMaster ****** : "+schoolTypeMaster);
   					  
   					  if(schoolTypeMaster!=null && schoolTypeMaster.size()>0){
   						  schoolTypeMastr = schoolTypeMaster.get(0); 
   						  schoolTypeMastr.setSchoolTypeName(tmSchoolTypeName);
   					      schoolMaster.setSchoolTypeId(schoolTypeMastr);
   					  }else{
   						  responseStatus = false;
   						  errorCode = 10038;
   						  errorMsg = "School type does not exist.";
   					  }
   					 
   					  schoolMaster.setAddress(ADDRESS1);
   					  schoolMaster.setCityName(CITY);
   					 
   					  stateMaster = stateMasterDAO.findByStateCode(STATE);
   						 if(stateMaster!=null){
   							 stateMaster.setStateShortName(STATE);
   							 schoolMaster.setStateMaster(stateMaster);
   						 }else{
   							  responseStatus = false;
   							  errorCode = 10039;
   							  errorMsg = "State does not exist.";
   						 }
   						
   						schoolMaster.setZip(POSTAL); 
   						schoolMaster.setExitURL(exitURL);
   						
   						schoolMasterDAO.makePersistent(schoolMaster);
   					    System.out.println("School Locaction updated successfully*****");
   			           
   			         }else{
   						  responseStatus = false;
   						  errorCode = 10043;
   						  errorMsg = "JC_TM_ACTION is not valid, Please use, JC_TM_ACTION as add";
   					 }
   			 }else if(JC_TM_ACTION.equalsIgnoreCase("add")){
   					 System.out.println("IN ADD JC_TM_ACTION Location Code does not Exist for schoolMaster  : "+LOCATION_CODE);
   					 if(lstSchoolMaster.size()<1){
   					 schoolMaster = new SchoolMaster();
   					 schoolMaster.setDistrictId(districtMaster);
   					 
   					 if(districtMaster.getDistrictName()!=null){
   					 schoolMaster.setDistrictName(districtMaster.getDistrictName());
   					 }
   					 schoolMaster.setLocationCode(DEPTID);
   					 schoolMaster.setSchoolName(DESCR);
   					 
   					  Criterion criterion1 = Restrictions.eq("schoolTypeName", matchJCToTMSchoolType(JC_TM_SCHL_TYPE));
   					  schoolTypeMaster = schoolTypeMasterDAO.findByCriteria(criterion1);
   					  System.out.println("schoolTypeMaster ****** : "+schoolTypeMaster);
   					  
   					  if(schoolTypeMaster!=null && schoolTypeMaster.size()>0){
   						  schoolTypeMastr = schoolTypeMaster.get(0); 
   						  schoolTypeMastr.setSchoolTypeName(matchJCToTMSchoolType(JC_TM_SCHL_TYPE));
   					      schoolMaster.setSchoolTypeId(schoolTypeMastr);
   					  }else{
   						  responseStatus = false;
   						  errorCode = 10038;
   						  errorMsg = "School type does not exist.";
   					  }
   					 
   					 schoolMaster.setAddress(ADDRESS1);
   					 schoolMaster.setCityName(CITY);
   					 
   					 StateMaster statmstr = new StateMaster();
   					 statmstr.setStateShortName(STATE);
   					 
   					 stateMaster = stateMasterDAO.findByStateCode(STATE);
   					 if(stateMaster!=null){
   						 stateMaster.setStateShortName(STATE);
   						 schoolMaster.setStateMaster(stateMaster);
   					 }else{
   						  responseStatus = false;
   						  errorCode = 10039;
   						  errorMsg = "State does not exist.";
   					 }
   					 schoolMaster.setZip(POSTAL);    
   					 schoolMaster.setStatus("A");
   					 
   					 GeoMapping geoMapping =geoMappingDAO.findById(0, false, false);
   					 schoolMaster.setGeoMapping(geoMapping);
   					 
   					 Integer regionId = districtMaster.getStateId().getRegionId().getRegionId();
   					 System.out.println("regionId *********: "+regionId);
   					 RegionMaster regionMaster = regionMasterDAO.findById(regionId, false, false); 
   					 schoolMaster.setRegionId(regionMaster);
   					 
   					 schoolMaster.setCanTMApproach(districtMaster.getCanTMApproach());
   					 schoolMaster.setIsPortfolioNeeded(districtMaster.getIsPortfolioNeeded());
   					 
   					 schoolMaster.setIsWeeklyCgReport(districtMaster.getIsWeeklyCgReport());
   					 
   					 SchoolStandardMaster schoolStandardMaster = schoolStandardMasterDAO.findById(0, false, false);
   					 schoolMaster.setSchoolStandardId(schoolStandardMaster);
   					 
   					 schoolMaster.setDmName(districtMaster.getDmName());
   					 schoolMaster.setAcName(districtMaster.getAcName());
   					 schoolMaster.setAmName(districtMaster.getAmName());
   					 
   					 schoolMaster.setPostingOnDistrictWall(districtMaster.getPostingOnDistrictWall());
   					 schoolMaster.setPostingOnTMWall(districtMaster.getPostingOnTMWall());
   					 schoolMaster.setPostingOnSchoolWall(2);
   					 
   					 schoolMaster.setExitURL(exitURL);
   					 schoolMaster.setCreatedDateTime(new Date());
   					 
   					 if(responseStatus){
   						 schoolMasterDAO.makePersistent(schoolMaster);
   						 
   						 DistrictSchools districtSchools=new DistrictSchools();
   						districtSchools.setDistrictMaster(schoolMaster.getDistrictId());
   						districtSchools.setSchoolMaster(schoolMaster);
   						districtSchools.setSchoolName(schoolMaster.getSchoolName());
   						districtSchools.setCreatedDateTime(new Date());
   						UserMaster userMaster=new UserMaster();
   						userMaster.setUserId(1);
   						districtSchools.setCreatedBy(userMaster);
   						districtSchoolsDAO.makePersistent(districtSchools);
   						 System.out.println("new Location added successfully*****");
   					 }
   			   }else{
   					  responseStatus = false;
   					  errorCode = 10044;
   					  errorMsg = "JC_TM_ACTION is not valid, Please use, JC_TM_ACTION as update";
   				 }
   			 }else if(JC_TM_ACTION.equalsIgnoreCase("delete")){
   				 System.out.println("******IN Delete JC_TM_ACTION************::: "+JC_TM_ACTION+" lstSchoolMaster: "+lstSchoolMaster);
   	   			 if(lstSchoolMaster.size()>0){
   	   			       schoolMaster = lstSchoolMaster.get(0);
   	   			       System.out.println("Location Code Exist for schoolMaster : "+LOCATION_CODE);
   	   					 schoolMaster.setStatus("I");
   	   						schoolMasterDAO.makePersistent(schoolMaster);
   	   					    System.out.println("School Locaction Inactive successfully*****");
   	   			         }else{
   	   						  responseStatus = false;
   	   						  errorCode = 10043;
   	   						  errorMsg = "JC_TM_ACTION is not valid, Please use, JC_TM_ACTION as add";
   	   					 }
   			 }else{
   					  responseStatus = false;
   					  errorCode = 10042;
   					  errorMsg = "JC_TM_ACTION is not valid.";
   				 }
   		 }
   		 if(responseStatus)
   		 {
   			jsonResponse = writeJSONReponses( true, 1, "Success: " +  UtilityAPI.getCurrentTimeStamp(), null);	
   			System.out.println("jsonResponse :********IN valid case**********: "+jsonResponse);
   	 
   		 }else{
   			 jsonResponse = writeJSONReponses(false, errorCode, errorMsg,null);
       		 System.out.println("jsonResponse :******IN case of FAILD************: "+jsonResponse);
   		 }
   		 
   	 	}catch(Exception e){
   			 e.printStackTrace();
   			 jsonResponse = writeJSONReponses(false, errorCode, errorMsg,e);
   			}
   	 	out.println(jsonResponse);
   	 }catch(Exception e){
   		 e.printStackTrace();
   	 }
   	 return null;
    }

   private String matchJCToTMSchoolType(String jC_TM_SCHL_TYPE) 
   {
	   	Map<String, String> jcToTMMap = new HashMap<String, String>();
	   	
	   	jcToTMMap.put("JC_TM_CHARTERSCHLS", "Charter");
	   	jcToTMMap.put("JC_TM_DISTRICTDEPTS", "Public");
	   	jcToTMMap.put("JC_TM_ELEMENTARYSCHLS", "Public");
	   	jcToTMMap.put("JC_TM_HIGHSCHLS", "Public");
	   	jcToTMMap.put("JC_TM_LOCTN_PUSH", "Public");
	   	jcToTMMap.put("JC_TM_MIDDLESCHLS", "Public");
	   	jcToTMMap.put("JC_TM_OPTIONSCHLS", "Public");
	   	jcToTMMap.put("JC_TM_OUTDOORLABSCHLS", "Public");
	   	jcToTMMap.put("JC_TM_POSTN_PUSH", "Public");
	   	jcToTMMap.put("JC_TM_PRESCHOOLPROGS", "Public");
	   	jcToTMMap.put("JC_TM_SAEPROGS", "Public");
	   	jcToTMMap.put("JC_TM_SPEC_PROG_CTE", "Public");
	   	jcToTMMap.put("JC_TM_SPEC_PROG_DROPOUT", "Public");
	   	jcToTMMap.put("JC_TM_SPEC_PROG_ESL", "Public");
	   	jcToTMMap.put("JC_TM_SPEC_PROG_GT", "Public");
	   	jcToTMMap.put("JC_TM_SPEC_PROG_INDIANED", "Public");
	   	jcToTMMap.put("JC_TM_SPEC_PROG_SPED", "Public");
	   	jcToTMMap.put("JC_TM_SPEC_PROG_TITLEI", "Public");
	   	jcToTMMap.put("JC_TM_SPEC_SCHLS_CONNECTIONS", "Public");
	   	jcToTMMap.put("JC_TM_SPEC_SCHLS_FLETCHER", "Public");
	   	jcToTMMap.put("JC_TM_SPEC_SCHLS_SOBESKY", "Public");
	
	   	if (null != jcToTMMap.get(jC_TM_SCHL_TYPE))
	   	   return jcToTMMap.get(jC_TM_SCHL_TYPE);
	   	
	   	return "Public";
   }

    
    private JSONObject writeJSONReponses(boolean success,
			                             Integer errorCode, 
			                             String errorMessage,
			                             Exception e)
	{

		JSONObject jsonOutput = new JSONObject();			
	    
		JSONObject responseObj = new JSONObject();			
		
		if (success)
		{
			responseObj.put("response", "success");
			responseObj.put("message", errorMessage);
		}
		else
		{
		   responseObj.put("response", "fail");

		   StringBuffer errorMsg = new StringBuffer("errorCode: [" + errorCode + "] ");
		   
		   errorMsg.append("errorMessage: [" + errorMessage + "] ");
		   
		   if (e != null && e.getMessage() != null)
			   errorMsg.append("excMessage: [" + e.getMessage() + "] ");
		
		   errorMsg.append("timestamp: [" + UtilityAPI.getCurrentTimeStamp() + "] ");

		   responseObj.put("message", errorMsg.toString());
		}
		
		jsonOutput.put("JC_TM_POS_STAT_RESP", responseObj);
		
		return jsonOutput;
	}
    
    @SuppressWarnings("unchecked")
	public static synchronized void insertTrackingAPIHistory(SessionFactory sessionFactory, String apiName, String requestContent, String respondMessage){
		try{
	 		 SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor)sessionFactory;
			 ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
			 Connection connection = connectionProvider.getConnection();
			 Map map1=new LinkedHashMap();
			 map1.put("apiName",apiName);
			 map1.put("requestParamenter",requestContent);
			 map1.put("apiCallingDateTime",new Date()); 
			 map1.put("respondMessage", respondMessage);
			 JDBCFunction.insertWithMap(map1, connection, "trackingrequestapi");
			 connection.close();
		 }catch (Exception e) {
			e.printStackTrace();
		}
    }
}
