package tm.api.controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import tm.api.UtilityAPI;
import tm.api.services.ServiceUtility;
import tm.api.services.TeacherUpdateThread;
import tm.api.services.applitrack.AppliTrackAPIService;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.master.ApplitrackDistricts;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.StatusMaster;
import tm.dao.ApplitrackDistrictsDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

@Controller
public class ApplyJobContoller {
	

    String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired 
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}
	
	@Autowired
	private ServiceUtility serviceUtility;
	public void setServiceUtility(ServiceUtility serviceUtility) {
		this.serviceUtility = serviceUtility;
	}
	
	@Autowired
	private ApplitrackDistrictsDAO applitrackDistrictsDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@RequestMapping(value="/service/json/applyCandidateToJob.do", method=RequestMethod.GET)
	public String doApplyJobJSONGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		String apiJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();		
		String redirectedFromURL = "";
		Map<String,String> mapTDetail = new HashMap<String, String>();
		mapTDetail  = serviceUtility.validateTeacher(request);
		
		TeacherDetail teacherDetail = null;
		JobOrder jobOrder = null;
		JobForTeacher jobForTeacher = null;
		
		Boolean tCreatedOrExist = false;
		Integer errorCode=0;
		String errorMsg="";
		
		PrintWriter out = null;
		JSONObject jsonResponse = new JSONObject();
		response.setContentType("application/json");
		StatusMaster statusMaster = null;
		HttpSession session = request.getSession();
		try {	
			out =  response.getWriter();
			Boolean isValidUser = false;						
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			
			if(!isValidUser){
				jsonResponse = UtilityAPI.writeJSONErrors(new Integer(mapDistSchDetail.get("errorCode")),mapDistSchDetail.get("errorMsg"), null);
			}
			else{				
				
				try {
					DistrictMaster districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
					jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
				}catch (Exception e) {
					jobOrder=null;
				}
				
				tCreatedOrExist = new Boolean(mapTDetail.get("tCreatedOrExist").trim());
				
				if(!tCreatedOrExist){					
					tCreatedOrExist=false;
					errorCode = new Integer(mapTDetail.get("errorCode").trim());
					errorMsg =  mapTDetail.get("errorMsg");
				}
				else if(jobOrder==null){
					tCreatedOrExist=false;
					errorCode = 10004;
					errorMsg = Utility.getLocaleValuePropByKey("msgJobNotExist", locale);
				}
				else if(!jobOrder.getStatus().equalsIgnoreCase("a")){
					tCreatedOrExist=false;
					errorCode = 10009;
					errorMsg = Utility.getLocaleValuePropByKey("msgJobInactive", locale);
				}
				else if(jobOrder.getJobStartDate().compareTo(new Date())>0){
					tCreatedOrExist=false;
					errorCode = 10009;
					errorMsg = Utility.getLocaleValuePropByKey("msgJobInactive", locale);
				}
				else if(jobOrder.getJobEndDate().compareTo(new Date())<0){
					tCreatedOrExist=false;
					errorCode = 10012;
					errorMsg = Utility.getLocaleValuePropByKey("msgJobExpiere", locale);
				}
				else{
					teacherDetail = (TeacherDetail) session.getAttribute("apiTeacherDetail");
					jobForTeacher = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);				
					if(jobForTeacher==null || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hide")){
						
						redirectedFromURL = request.getHeader("referer")==null?"":request.getHeader("referer");
						
						if(jobForTeacher==null){
							jobForTeacher = new JobForTeacher();
							jobForTeacher.setTeacherId(teacherDetail);
							jobForTeacher.setJobId(jobOrder);
							jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
							jobForTeacher.setRedirectedFromURL(redirectedFromURL);
							jobForTeacher.setJobBoardReferralURL(redirectedFromURL);
							jobForTeacher.setCoverLetter("");
							jobForTeacher.setIsAffilated(0);
							jobForTeacher.setCreatedDateTime(new Date());
						}
						else{
							jobForTeacher.setUpdatedDate(new Date());
						}
												
						statusMaster = WorkThreadServlet.statusMap.get("icomp");
						
						AssessmentDetail assessmentDetail = null;
						TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jobForTeacher.getTeacherId());
						if(!jobOrder.getJobCategoryMaster().getBaseStatus()){
							if(jobOrder.getIsJobAssessment()){
								List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
								if(lstAssessmentJobRelation.size()>0)
								{
									AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
									assessmentDetail = assessmentJobRelation.getAssessmentId();
									List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail, assessmentDetail);
									
									if(lstTeacherAssessmentStatus.size()>0)
									{						
										teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
										/*teacherAssessmentStatus.setTeacherAssessmentStatusId(null);
										teacherAssessmentStatus.setJobOrder(jobOrder);
										teacherAssessmentStatus.setCreatedDateTime(new Date());
										teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);		*/				
										statusMaster = teacherAssessmentStatus.getStatusMaster();						
									}
									else
									{
										statusMaster = WorkThreadServlet.statusMap.get("icomp");
									}					
								}
								else
								{
									statusMaster = WorkThreadServlet.statusMap.get("icomp");
								}
							}
							else{
								statusMaster = WorkThreadServlet.statusMap.get("comp");
							}
						}
						else if(jobOrder.getIsJobAssessment()){
							List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
							if(lstAssessmentJobRelation.size()>0)
							{
								AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
								assessmentDetail = assessmentJobRelation.getAssessmentId();
								List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail, assessmentDetail);
								
								if(lstTeacherAssessmentStatus.size()>0)
								{						
									teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
									/*teacherAssessmentStatus.setTeacherAssessmentStatusId(null);
									teacherAssessmentStatus.setJobOrder(jobOrder);
									teacherAssessmentStatus.setCreatedDateTime(new Date());
									teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);*/						
									statusMaster = teacherAssessmentStatus.getStatusMaster();						
								}
								else
								{
									statusMaster = WorkThreadServlet.statusMap.get("icomp");
								}					
							}
							else
							{
								statusMaster = WorkThreadServlet.statusMap.get("icomp");
							}
						}
						else{						
							List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
							JobOrder jOrder = new JobOrder();
							jOrder.setJobId(0);
							teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(),jOrder);
							if(teacherAssessmentStatusList.size()>0){
								teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
								statusMaster = teacherAssessmentStatus.getStatusMaster();
							}else{
								statusMaster = WorkThreadServlet.statusMap.get("icomp");
							}
						
						}
						jobForTeacher.setStatus(statusMaster);
						jobForTeacher.setStatusMaster(statusMaster);
						if(statusMaster!=null){
							jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
						}
						jobForTeacherDAO.makePersistent(jobForTeacher);
						tCreatedOrExist=true;
					}
					else{
						statusMaster = jobForTeacher.getStatus();
						tCreatedOrExist=true;
					}
				}
				
				if(tCreatedOrExist){
					session.setAttribute("apiTeacherDetail", teacherDetail);
					jsonResponse.put("status",true);				
					jsonResponse.put("jobStatus",statusMaster.getStatus());
					jsonResponse.put("timestamp",UtilityAPI.getCurrentTimeStamp());
				}
				else{
					jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg, null);
				}
			}
		}catch (Exception e){
			jsonResponse = UtilityAPI.writeJSONErrors(10017, "Server Error.", e);
			e.printStackTrace();
		}		
		out.print(jsonResponse);
		return null;
	
	}
	
	
	@RequestMapping(value="/service/xml/applyCandidateToJob.do", method=RequestMethod.GET)
	public String doAuthenticateCandidateXMLGET(ModelMap map,HttpServletRequest request, HttpServletResponse response){
		
		String apiJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();			
		response.setContentType("text/xml");
		
		Map<String,String> mapTDetail = new HashMap<String, String>();
		mapTDetail  = serviceUtility.validateTeacher(request);
		
		TeacherDetail teacherDetail = null;
		JobOrder jobOrder = null;
		JobForTeacher jobForTeacher = null;
		String redirectedFromURL = null;
		
		Boolean tCreatedOrExist = false;
		Integer errorCode=0;
		String errorMsg="";
		
		PrintWriter out = null;
		HttpSession session = request.getSession();
		try {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = null;
			Document doc = null;
			Element root = null;
			
			
			docBuilder = builderFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();
			root = doc.createElement("teachermatch");
			doc.appendChild(root);			
			
			StatusMaster statusMaster = null;
			try {			
				out =  response.getWriter();
				
				Boolean isValidUser = false;						
				Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
				DistrictMaster districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
				isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
				
				if(!isValidUser){					
					UtilityAPI.writeXMLErrors(new Integer(mapDistSchDetail.get("errorCode")),mapDistSchDetail.get("errorMsg"),null, doc, root);					
				}
				else{
					try {
						jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
					} catch (Exception e) {
						jobOrder=null;
					}
					tCreatedOrExist = new Boolean(mapTDetail.get("tCreatedOrExist").trim());
					if(!tCreatedOrExist){						
						tCreatedOrExist=false;
						errorCode = new Integer(mapTDetail.get("errorCode").trim());
						errorMsg =  mapTDetail.get("errorMsg");
					}
					else if(jobOrder==null){
						tCreatedOrExist=false;
						errorCode = 10004;
						errorMsg = Utility.getLocaleValuePropByKey("msgJobNotExist", locale);
					}
					else if(!jobOrder.getStatus().equalsIgnoreCase("a")){
						tCreatedOrExist=false;
						errorCode = 10009;
						errorMsg = Utility.getLocaleValuePropByKey("msgJobInactive", locale);
					}
					else if(jobOrder.getJobStartDate().compareTo(new Date())>0){
						tCreatedOrExist=false;
						errorCode = 10009;
						errorMsg = Utility.getLocaleValuePropByKey("msgJobInactive", locale);
					}
					else if(jobOrder.getJobEndDate().compareTo(new Date())<0){
						tCreatedOrExist=false;
						errorCode = 10012;
						errorMsg = Utility.getLocaleValuePropByKey("msgJobExpiere", locale);
					}
					else{
						teacherDetail = (TeacherDetail) session.getAttribute("apiTeacherDetail");					
						jobForTeacher = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
						
						if(jobForTeacher==null || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hide")){
							redirectedFromURL = request.getHeader("referer")==null?"":request.getHeader("referer");
							if(jobForTeacher==null){
								jobForTeacher = new JobForTeacher();
								jobForTeacher.setTeacherId(teacherDetail);
								jobForTeacher.setJobId(jobOrder);
								jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
								jobForTeacher.setRedirectedFromURL(redirectedFromURL);
								jobForTeacher.setJobBoardReferralURL(redirectedFromURL);
								jobForTeacher.setCoverLetter("");
								jobForTeacher.setIsAffilated(0);
								jobForTeacher.setCreatedDateTime(new Date());
							}
							else{
								jobForTeacher.setUpdatedDate(new Date());
							}							
							statusMaster = WorkThreadServlet.statusMap.get("icomp");
							
							AssessmentDetail assessmentDetail = null;
							TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jobForTeacher.getTeacherId());
							if(!jobOrder.getJobCategoryMaster().getBaseStatus()){
								if(jobOrder.getIsJobAssessment()){
									List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
									if(lstAssessmentJobRelation.size()>0)
									{
										AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
										assessmentDetail = assessmentJobRelation.getAssessmentId();
										List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail, assessmentDetail);
										
										if(lstTeacherAssessmentStatus.size()>0)
										{						
											teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
											/*teacherAssessmentStatus.setTeacherAssessmentStatusId(null);
											teacherAssessmentStatus.setJobOrder(jobOrder);
											teacherAssessmentStatus.setCreatedDateTime(new Date());
											teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);*/						
											statusMaster = teacherAssessmentStatus.getStatusMaster();						
										}
										else
										{
											statusMaster = WorkThreadServlet.statusMap.get("icomp");
										}					
									}
									else
									{
										statusMaster = WorkThreadServlet.statusMap.get("icomp");
									}
								}
								else{
									statusMaster = WorkThreadServlet.statusMap.get("comp");
								}
							}
							else if(jobOrder.getIsJobAssessment()){
								List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
								if(lstAssessmentJobRelation.size()>0)
								{
									AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
									assessmentDetail = assessmentJobRelation.getAssessmentId();
									List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail, assessmentDetail);
									
									if(lstTeacherAssessmentStatus.size()>0)
									{						
										teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
										/*teacherAssessmentStatus.setTeacherAssessmentStatusId(null);
										teacherAssessmentStatus.setJobOrder(jobOrder);
										teacherAssessmentStatus.setCreatedDateTime(new Date());
										teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);*/						
										statusMaster = teacherAssessmentStatus.getStatusMaster();						
									}
									else
									{
										statusMaster = WorkThreadServlet.statusMap.get("icomp");
									}					
								}
								else
								{
									statusMaster = WorkThreadServlet.statusMap.get("icomp");
								}
							}
							else{							
								List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
								JobOrder jOrder = new JobOrder();
								jOrder.setJobId(0);
								teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(),jOrder);
								if(teacherAssessmentStatusList.size()>0){
									teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
									statusMaster = teacherAssessmentStatus.getStatusMaster();
								}else{
									statusMaster = WorkThreadServlet.statusMap.get("icomp");
								}
							
							}
							jobForTeacher.setStatus(statusMaster);
							jobForTeacher.setStatusMaster(statusMaster);
							if(statusMaster!=null){
								jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
							}
							jobForTeacherDAO.makePersistent(jobForTeacher);
							tCreatedOrExist=true;
						}
						else{
							statusMaster = jobForTeacher.getStatus();
							tCreatedOrExist=true;
						}
					}
					
					if(tCreatedOrExist){			
						session.setAttribute("apiTeacherDetail", teacherDetail);
						
						Element eleStatus = doc.createElement("status");
						root.appendChild(eleStatus);
						Text textStatus = doc.createTextNode("true");
						eleStatus.appendChild(textStatus);
						
						Element eleJbStatus = doc.createElement("jobStatus");
						root.appendChild(eleJbStatus);
						Text jbStatusResult = doc.createTextNode(statusMaster.getStatus());
						eleJbStatus.appendChild(jbStatusResult);
						
						Element timeStamp = doc.createElement("timeStamp");
						root.appendChild(timeStamp);
						Text timeStampResult = doc.createTextNode(UtilityAPI.getCurrentTimeStamp());
						timeStamp.appendChild(timeStampResult);
					}
					else{
						UtilityAPI.writeXMLErrors(errorCode, errorMsg, null, doc, root);
					}
				}
				
				
			}catch (Exception e){
				UtilityAPI.writeXMLErrors(10001, Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale), e, doc, root);
				e.printStackTrace();
			}		
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			out.println(sw.toString());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;	
	
	}
	
	
	@RequestMapping(value="/service/applyJob.do", method=RequestMethod.GET)
	public String doAuthenticate(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		try{			
			HttpSession session = request.getSession();
			String redirectedFromURL = null;
			String jobBoardReferralURL = null;
			Boolean isValidUser = false;						
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			
			System.out.println("isValidUser:: "+isValidUser);
			
			boolean isPrevApply = false; 
			Boolean tCreatedOrExist = false;
			
			Map<String,String> mapTDetail = new HashMap<String, String>();

			//isValidUser = false;
			if(isValidUser){
				mapTDetail  = serviceUtility.validateTeacher(request);
				JobOrder jobOrder = null;
				String apiJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
				DistrictMaster districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
				System.out.println("districtMaster:: "+districtMaster.getDistrictId());
				ApplitrackDistricts applitrackDistricts = applitrackDistrictsDAO.findByDistrictId(districtMaster);
				System.out.println("applitrackDistricts:: "+applitrackDistricts);
				String auths[] = new String[5];
				if(applitrackDistricts!=null && districtMaster!=null && districtMaster.getIsApplitrackIntegration()!=null && districtMaster.getIsApplitrackIntegration())
				{
					auths[0] = applitrackDistricts.getUserName();
					auths[1] = applitrackDistricts.getPassword();
					auths[2] = applitrackDistricts.getApiURL();
					auths[3] = applitrackDistricts.getRealm();
					auths[4] = applitrackDistricts.getDefaultJobCategory();
					
					System.out.println("UserName: "+ auths[0]);
					//System.out.println("Password: "+ auths[1]);
					System.out.println("ApiURL: "+ auths[2]);
					System.out.println("Realm: "+ auths[3]);
					System.out.println("DefaultJobCategory: "+ auths[4]);
					
					AppliTrackAPIService appliService = new AppliTrackAPIService();
					jobOrder = appliService.getJob(request, response, districtMaster, apiJobId,auths);
					
					if(jobOrder==null)
					{
						jobOrder = jobOrderDAO.getListByTitle("Applitrack Integration",districtMaster);
						if(jobOrder==null)
						{
							jobOrder=new JobOrder();
							jobOrder.setNotificationToschool(true);
		        			     			
		        			jobOrder.setDistrictMaster(districtMaster);
							jobOrder.setJobTitle("Applitrack Integration");
							jobOrder.setJobStartDate(new Date());
							jobOrder.setJobEndDate(Utility.getCurrentDateFormart("12-25-2099"));
							JobCategoryMaster jobCategoryMaster=jobCategoryMasterDAO.findJobCategoryByNmaeAPI(auths[4], districtMaster);
							if(jobCategoryMaster==null)
							{
								jobCategoryMaster = jobCategoryMasterDAO.findById(1, false, false);
							}
							
							try{
								if(jobOrder.getDistrictMaster()==null){
									if(jobCategoryMaster.getDistrictMaster()!=null){
										jobOrder.setDistrictMaster(jobCategoryMaster.getDistrictMaster());
										System.out.println(" Inside District :::");
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							
							jobOrder.setJobCategoryMaster(jobCategoryMaster);
							jobOrder.setIsJobAssessment(false);
							jobOrder.setJobAssessmentStatus(0);
							jobOrder.setJobStatus("O");
							jobOrder.setStatus("A");
							jobOrder.setJobType("");
							jobOrder.setCreatedBy(null);
							jobOrder.setJobDescription("");
							jobOrder.setCreatedByEntity(new Integer(2));
							jobOrder.setCreatedForEntity(new Integer(2));
							jobOrder.setNoOfExpHires(99); // Need to ask for default
							jobOrder.setApiJobId(apiJobId);
							jobOrder.setFlagForURL(1);
							jobOrder.setFlagForMessage(0);
							jobOrder.setExitURL("https://platform.teachermatch.org");
							jobOrder.setExitMessage(null);
							jobOrder.setCreatedDateTime(new Date());
							jobOrder.setIpAddress(IPAddressUtility.getIpAddress(request));
							jobOrder.setIsPortfolioNeeded(false);
							jobOrder.setIsPoolJob(0);
							jobOrder.setWritePrivilegeToSchool(false);
							jobOrder.setSelectedSchoolsInDistrict(2);
							jobOrder.setCreatedBy(1);
							jobOrder.setOfferVirtualVideoInterview(false);
							jobOrder.setVVIExpiresInDays(0);
							jobOrder.setSendAutoVVILink(false);
							jobOrder.setApprovalBeforeGoLive(1);
							jobOrder.setNoSchoolAttach(2);
							jobOrder.setAssessmentDocument(null);
							jobOrder.setAttachNewPillar(2);
							jobOrder.setAttachDefaultDistrictPillar(2);
							jobOrder.setAttachDefaultSchoolPillar(2);
							jobOrder.setIsExpHireNotEqualToReqNo(false);
							jobOrder.setNotificationToschool(true);
							jobOrder.setIsInviteOnly(true);
							jobOrder.setHiddenJob(true);
							
							
							jobOrderDAO.makePersistent(jobOrder);
						}
					}
					
				}
				
			
				TeacherDetail teacherDetail = null;
				
				JobForTeacher jobForTeacher = null;
				
				Boolean responseStatus = false;
				Integer errorCode=0;
				String errorMsg="";
				
				StatusMaster statusMaster = null;
				try {			
					
					try {
						if(jobOrder==null)
							jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
						
					} catch (Exception e) {
						jobOrder=null;
					}
					teacherDetail = (TeacherDetail)session.getAttribute("apiTeacherDetail");
					try {
						TeacherUpdateThread teacherUpdateThread = new TeacherUpdateThread();
						teacherUpdateThread.setRequest(request);
						teacherUpdateThread.setResponse(response);
						teacherUpdateThread.setDistrictMaster(districtMaster);
						teacherUpdateThread.setTeacherDetail(teacherDetail);
						teacherUpdateThread.setAuths(auths);
						teacherUpdateThread.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					tCreatedOrExist = new Boolean(mapTDetail.get("tCreatedOrExist").trim());
					if(!tCreatedOrExist){						
						tCreatedOrExist=false;
						errorCode = new Integer(mapTDetail.get("errorCode").trim());
						errorMsg =  mapTDetail.get("errorMsg");
					}
					else if(jobOrder==null){
						tCreatedOrExist=false;
						errorCode = 10004;
						errorMsg = Utility.getLocaleValuePropByKey("msgJobNotExist", locale);
					}					
					else if(!jobOrder.getStatus().equalsIgnoreCase("a")){
						tCreatedOrExist=false;
						errorCode = 10009;
						errorMsg = Utility.getLocaleValuePropByKey("msgJobInactive", locale);
					}
					else if(jobOrder.getJobStartDate().compareTo(new Date())>0){
						tCreatedOrExist=false;
						errorCode = 10009;
						errorMsg = Utility.getLocaleValuePropByKey("msgJobInactive", locale);
					}
					else if(jobOrder.getJobEndDate().compareTo(new Date())<0){
						tCreatedOrExist=false;
						errorCode = 10012;
						errorMsg = Utility.getLocaleValuePropByKey("msgJobExpiere", locale);
					}
					else {
						System.out.println("5555555555555555555555555555555555555");
						jobForTeacher = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
						System.out.println("jobForTeacher:: "+jobForTeacher);
						redirectedFromURL = request.getHeader("referer")==null?"":request.getHeader("referer");
						jobBoardReferralURL = session.getAttribute("jobBoardReferralURL")==null?"":(String)session.getAttribute("jobBoardReferralURL");
						
						Integer epiJobId = null;
						if(jobOrder!=null && jobOrder.getJobId()!=null){
							epiJobId = jobOrder.getJobId();
						}
						if(epiJobId!=null)
							session.setAttribute("epiJobId", epiJobId);
						
						if(jobForTeacher==null || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hide")){
							
							try {
								jobForTeacher = jobForTeacherDAO.saveJobForTeacher(jobForTeacher, jobOrder, teacherDetail, redirectedFromURL, jobBoardReferralURL);
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							responseStatus=true;
						}
						else{							
							responseStatus=true;
							isPrevApply = true;
						}
					}
					
					if(responseStatus){						
						jobOrder = new JobOrder();
						jobOrder.setJobId(0);
						List<TeacherAssessmentStatus> lstAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(), jobOrder);
						TeacherAssessmentStatus assessmentStatus = null;
						StatusMaster stm = null;
						if(lstAssessmentStatus.size()>0){
							assessmentStatus = lstAssessmentStatus.get(0);
							stm = assessmentStatus.getStatusMaster();
						}
						
						map.addAttribute("jobForTeacher", jobForTeacher);
						map.addAttribute("teacherAssessmentStatus", assessmentStatus);
						
						if(jobForTeacher.getJobId().getJobCategoryMaster().getBaseStatus()){ // Base is needed							
							if((assessmentStatus!=null) &&( assessmentStatus.getStatusMaster().getStatusShortName().equals("comp") || assessmentStatus.getStatusMaster().getStatusShortName().equals("vlt") )){
								if(isPrevApply){
									map.addAttribute("msgFlag", "2"); // already apply this job									
								}
								else{
									map.addAttribute("msgFlag", "3"); // just apply this job
								}
							}
							else{								
								if(isPrevApply){
									return "redirect:startOrContinueEPI.do"; // already apply job but EPI is not completed							
								}
								else{
									map.addAttribute("msgFlag", "3"); //just apply this job
									map.addAttribute("baseStart", true);									
								}
							}							
						}
						else{ // Base is not needed
							if(isPrevApply){
								map.addAttribute("msgFlag", "2"); // already apply this job								
							}
							else{
								map.addAttribute("msgFlag", "3"); // just apply this job							
							}
						}												
						return "epi";
					}
					else{
						map.addAttribute("msgFlag", "0");
						map.addAttribute("errorCode", errorCode);
						map.addAttribute("errorMsg", errorMsg);
						return "epi";
					}
				}catch (Exception e){
					e.printStackTrace();
				}		
			}
			else{
				map.addAttribute("msgFlag", 0);
				map.addAttribute("errorCode", mapDistSchDetail.get("errorCode"));
				map.addAttribute("errorMsg", Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale));
				return "epi";
			}								
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/service/singlesignin.do",  method = { RequestMethod.GET, RequestMethod.POST })
	public String doAuthenticateSignIn(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		try {			
			HttpSession session = request.getSession(false);
			Boolean isValidUser = false;
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchoolUsingLocationCode(request);
			System.out.println("mapDistSchDetail"+mapDistSchDetail);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			System.out.println("isValidUser>"+isValidUser );
			if(!isValidUser){
				map.addAttribute("msgFlag", 0);
				map.addAttribute("errorCode", new Integer(mapDistSchDetail.get("errorCode")));
				map.addAttribute("errorMsg", mapDistSchDetail.get("errorMsg"));
				
				if(session != null)
					session.invalidate();
				
				return "singlesignin";
			}
			else{		
				Integer usrRole = new Integer(mapDistSchDetail.get("userRole"));
				System.out.println("usrRole:: "+usrRole);
				if(usrRole!=null && usrRole==7 || usrRole==8)
					return "redirect:../onboardingdashboard.do";
				else
					return "redirect:../tmdashboard.do";
			}
		}catch (Exception e){
			e.printStackTrace();
		}		
		
		return "singlesignin";
	}
	
	@RequestMapping(value="/service/authSingleSignOn.do",  method = { RequestMethod.GET, RequestMethod.POST })
	public String doAuthenticateSingleSignIn(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		try {			
			HttpSession session = request.getSession(false);
			Boolean isValidUser = false;
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchoolUsingEmail(request);
			
			System.out.println("mapDistSchDetail"+mapDistSchDetail);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			System.out.println("isValidUser>"+isValidUser );
			if(!isValidUser){
				map.addAttribute("msgFlag", 0);
				map.addAttribute("errorCode", new Integer(mapDistSchDetail.get("errorCode")));
				map.addAttribute("errorMsg", mapDistSchDetail.get("errorMsg"));
				
				if(session != null)
					session.invalidate();

				return "authSingleSignOn";
			}
			else{		
				Integer usrRole = new Integer(mapDistSchDetail.get("userRole"));
				System.out.println("usrRole:: "+usrRole);
				/*if(usrRole!=null && usrRole==7 || usrRole==8)
					return "redirect:../teacherinfo.do";
				else*/
					return "redirect:../tmdashboard.do";
			}
		}catch (Exception e){
			e.printStackTrace();
		}		
		
		return "authSingleSignOn";
	}
	
	//@RequestMapping(value="/philsinglesignin.do",  method = { RequestMethod.GET, RequestMethod.POST })
	@RequestMapping(value="/service/philsinglesignin.do",  method = { RequestMethod.GET, RequestMethod.POST })
	public String doAuthenticatePhilSignIn(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		try {			
			request.getSession();
			Boolean isValidUser = false;
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchoolUsingEmployeeCode(request);
			
			System.out.println("mapDistSchDetail"+mapDistSchDetail);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			System.out.println("isValidUser>"+isValidUser );
			if(!isValidUser){
				map.addAttribute("msgFlag", 0);
				map.addAttribute("errorCode", new Integer(mapDistSchDetail.get("errorCode")));
				map.addAttribute("errorMsg", mapDistSchDetail.get("errorMsg"));
				HttpSession session = request.getSession(false);
				if(session != null)
					session.invalidate();

				return "philsinglesignin";
			}
			else{		
				Integer usrRole = new Integer(mapDistSchDetail.get("userRole"));
				System.out.println("usrRole:: "+usrRole);
				if(usrRole!=null && usrRole==10)
					return "redirect:../userdashboard.do";
				else
					return "redirect:../tmdashboard.do";
			}
		}catch (Exception e){
			e.printStackTrace();
		}		
		
		return "philsinglesignin";
	}
}
