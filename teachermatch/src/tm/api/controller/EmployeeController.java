package tm.api.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.api.UtilityAPI;
import tm.api.services.ServiceUtility;
import tm.bean.EmployeeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.user.UserMaster;
import tm.dao.EmployeeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.Utility;

@Controller
public class EmployeeController {
	
    String locale = Utility.getValueOfPropByKey("locale");

	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;

	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;

	@Autowired
	private StateMasterDAO stateMasterDAO;

	@RequestMapping(value="/service/json/addEmployees.do", method=RequestMethod.GET)
	public String doManageEmployeesByJSONGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("========================================");
		return doManageEmployeesByJSONPOST(map, request, response);
	}

	@RequestMapping(value="/service/json/addEmployees.do", method=RequestMethod.POST)
	public String doManageEmployeesByJSONPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{

		try {			
			PrintWriter out =null;
			out = response.getWriter();
			System.out.println("Enter in addJobOrder.do JSON");

			JSONObject jsonResponse = new JSONObject();

			response.setContentType("application/json");
			HttpSession session  = request.getSession();
			DistrictMaster districtMaster = null;

			//JSONObject jsonRequest = new JSONObject();

			System.out.println("request.getParameter(request)"+request.getParameter("request"));
			Boolean responseStatus = false;
			Integer errorCode = 10001;
			String errorMsg = Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			SchoolMaster schoolMaster = null;

			try {
				String payloadRequest = request.getParameter("request");
				if(payloadRequest==null)
				{
						try {
							payloadRequest = ServiceUtility.getBody(request);
						} catch (IOException e) {
							e.printStackTrace();
						}
				}
				System.out.println("payloadRequest::= "+payloadRequest);
				
				
				JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( payloadRequest ); 
				//districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
				
				String authKey = jsonRequest.get("authKey")==null?"":UtilityAPI.parseString(jsonRequest.getString("authKey")).replace(' ', '+');
				String orgType = jsonRequest.get("orgType")==null?"":UtilityAPI.parseString(jsonRequest.getString("orgType")).trim();
				String userEmail = jsonRequest.get("userEmail")==null?"":UtilityAPI.parseString(jsonRequest.getString("userEmail")).replace(' ', '+');
				
				if(!userEmail.trim().equals("")){
					userEmail = Utility.decodeBase64(userEmail);
				}
				UserMaster userMaster = null;
				List<UserMaster> lstUMaster = userMasterDAO.findByEmail(userEmail); 
				if(lstUMaster!=null && lstUMaster.size()>0){
					userMaster=lstUMaster.get(0);
				}
				
				if(orgType.equalsIgnoreCase("D")){
					districtMaster =  districtMasterDAO.validateDistrictByAuthkey(authKey);				
					if(districtMaster!=null){
						session.setAttribute("apiDistrictMaster", districtMaster);						
						responseStatus = true;
					}
				}
				else if(orgType.equalsIgnoreCase("S")){
					schoolMaster = schoolMasterDAO.validateSchoolByAuthkey(authKey);					
					System.out.println("SchoolMaster IN"+schoolMaster);
					if(schoolMaster!=null){
						districtMaster =  schoolMaster.getDistrictId();
						session.setAttribute("apiSchoolMaster", schoolMaster);
						session.setAttribute("apiDistrictMaster", schoolMaster.getDistrictId());
						responseStatus = true;
					}
				}
				else{
					responseStatus = false;	
					errorCode=10001;
					errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
				}
				
				
				if(!responseStatus)
				{
					jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
				}else
				{
					JSONArray employees = new JSONArray();
					employees = jsonRequest.getJSONArray("Employee");
					List<String> stats = new ArrayList<String>(); 
					List<String> employeeCodes = new ArrayList<String>(); 
					/*
						firstName: String,
						middleName: String (Optional),
						lastName: String,
						employeeCode: String,
						address1: String,
						city: String,
						state: String,
						zipCode: String,
						birthDate: Date,
						phone: String,
						mobile: String (Optional)
					 */
					Map<String,String> empErrorMap = new HashMap<String, String>();
					for (Object object : employees) {
						JSONObject jsonItems = (JSONObject)object;
						String firstName = jsonItems.get("firstName")==null?"":UtilityAPI.parseString(jsonItems.getString("firstName")).trim();
						String lastName = jsonItems.get("lastName")==null?"":UtilityAPI.parseString(jsonItems.getString("lastName")).trim();
						String employeeCode = jsonItems.get("employeeCode")==null?"":UtilityAPI.parseString(jsonItems.getString("employeeCode")).trim();
						String address1 = jsonItems.get("address1")==null?"":UtilityAPI.parseString(jsonItems.getString("address1")).trim();
						String city = jsonItems.get("city")==null?"":UtilityAPI.parseString(jsonItems.getString("city")).trim();
						String state = jsonItems.get("state")==null?"":UtilityAPI.parseString(jsonItems.getString("state")).trim();
						String zipCode = jsonItems.get("zipCode")==null?"":UtilityAPI.parseString(jsonItems.getString("zipCode")).trim();
						String phone = jsonItems.get("phone")==null?"":UtilityAPI.parseString(jsonItems.getString("phone")).trim();
						String birthDate = jsonItems.get("birthDate")==null?"":UtilityAPI.parseString(jsonItems.getString("birthDate")).trim();
						String staffType = jsonItems.get("staffType")==null?"":UtilityAPI.parseString(jsonItems.getString("staffType")).trim();
						Date dob1 = null;
						try{
							dob1 = (Date)formatter.parse(birthDate);
							System.out.println(":::dob1::::::dob1:::::::dob1::::: "+dob1);
						}catch (Exception e) {						
							dob1 = null;
						}
						System.out.println(":::dob1::::::dob1:::::::dob1::::: "+dob1);
						
						stats.add(state);
						employeeCodes.add(employeeCode);
						
						System.out.println("firstName: "+firstName);
						System.out.println("middleName: "+jsonItems.getString("middleName"));
						System.out.println("lastName: "+lastName);
						System.out.println("employeeCode: "+employeeCode);
						System.out.println("address1: "+address1);
						System.out.println("city: "+city);
						System.out.println("state: "+state);
						System.out.println("zipCode: "+zipCode);
						System.out.println("birthDate: "+birthDate);
						System.out.println("phone: "+phone);
						System.out.println("mobile: "+jsonItems.getString("mobile"));
						System.out.println("staffType: "+staffType);
						System.out.println("=================================================");
						errorMsg="";
						if(employeeCode.equals("")){
							errorMsg += Utility.getLocaleValuePropByKey("msgEmpCodeReq", locale);
						}
						if(firstName.equals("")){
							errorMsg += Utility.getLocaleValuePropByKey("msgFirstNameReq", locale);
						}
						if(lastName.equals("")){
							errorMsg += Utility.getLocaleValuePropByKey("msgLastNameReq", locale);
						}					
						if(employeeCode.equals("")){
							errorMsg += "LastName is required.";
						}
						/*if(address1.equals("")){
							errorMsg += Utility.getLocaleValuePropByKey("msgAddressReq", locale);
						}
						if(city.equals("")){
							errorMsg += Utility.getLocaleValuePropByKey("msgCityReq", locale);
						}
						if(state.equals("")){
							errorMsg += Utility.getLocaleValuePropByKey("msgStateReq", locale);
						}
						if(zipCode.equals("")){
							errorMsg += Utility.getLocaleValuePropByKey("msgZipCodeReq", locale);
						}
						if(phone.equals("")){
							errorMsg += Utility.getLocaleValuePropByKey("msgPhoneReq", locale);
						}*/
						if(staffType.equals("")){
							errorMsg += Utility.getLocaleValuePropByKey("msgStaffTypeReq", locale);
						}
						if(dob1==null){
							errorMsg += Utility.getLocaleValuePropByKey("msgInvalideBDay", locale);
						}
						if(!errorMsg.equals(""))
						empErrorMap.put(employeeCode, "("+errorMsg+" "+Utility.getLocaleValuePropByKey("msgForEmployeeCode", locale)+" "+employeeCode+")");
					}

					List<StateMaster> stateList = stateMasterDAO.findByStateCodes(stats);
					Map<String,StateMaster> stateMap = new HashMap<String, StateMaster>();
					for (StateMaster stateMaster : stateList) {
						stateMap.put(stateMaster.getStateShortName(), stateMaster);
					}

					List<EmployeeMaster> empList = employeeMasterDAO.checkEmployeesByEmployeeCodes(employeeCodes,districtMaster);
					Map<String,EmployeeMaster> empMap = new HashMap<String, EmployeeMaster>();
					for (EmployeeMaster employeeMaster : empList) {
						empMap.put(employeeMaster.getEmployeeCode().trim(), employeeMaster);
					}
					StringBuffer sb = new StringBuffer();
					for (Object object : employees) {
						JSONObject jsonItems = (JSONObject)object;
						
						Date dob = null;
						try{
							dob = (Date)formatter.parse(jsonItems.getString("birthDate"));
							System.out.println(":::dob1::::::dob1:::::::dob1::::: "+dob);
						}catch (Exception e) {						
							dob = null;
						}
						EmployeeMaster employeeMaster = new EmployeeMaster();
						String employeeCode = jsonItems.getString("employeeCode").trim();
						employeeMaster.setDob(dob);
						employeeMaster.setFirstName(jsonItems.getString("firstName"));
						employeeMaster.setMiddleName(jsonItems.getString("middleName"));
						employeeMaster.setLastName(jsonItems.getString("lastName"));
						employeeMaster.setEmployeeCode(employeeCode);
						employeeMaster.setAddress1(jsonItems.getString("address1"));
						employeeMaster.setCityName(jsonItems.getString("city"));
						employeeMaster.setZipCode(jsonItems.getString("zipCode"));
						employeeMaster.setPhone(jsonItems.getString("phone"));
						employeeMaster.setMobile(jsonItems.getString("mobile"));
						employeeMaster.setStaffType(jsonItems.getString("staffType"));
						employeeMaster.setSSN("");
						employeeMaster.setDistrictMaster(districtMaster);
						employeeMaster.setStatus("A");
						employeeMaster.setCreatedDateTime(new Date());
						StateMaster statemaster = stateMap.get(jsonItems.getString("state").trim());
						employeeMaster.setStateMaster(statemaster);
						String error = empErrorMap.get(employeeCode);
						if(statemaster==null)
						{
							if(error==null)
								error=Utility.getLocaleValuePropByKey("msgInvalideState", locale) +" "+jsonItems.getString("state")+" "+Utility.getLocaleValuePropByKey("msgForEmployeeCode", locale)+" "+employeeCode+".";
							else
								error+=Utility.getLocaleValuePropByKey("msgInvalideState", locale) +" "+jsonItems.getString("state")+" "+Utility.getLocaleValuePropByKey("msgForEmployeeCode", locale)+" "+employeeCode+".";
						}
						if(error==null)
						{
							if(empMap.get(employeeCode)==null)
							{
								try {
									employeeMasterDAO.makePersistent(employeeMaster);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}else
							{
								sb.append(Utility.getLocaleValuePropByKey("msgDuplicateemployeeCode", locale)+" "+employeeCode+".");
							}
						}else
						{
							sb.append(error);
						}
					}
					String errors= sb.toString();
					
					jsonResponse = new JSONObject();
					if(errors.equals(""))
						jsonResponse.put("status", true);
					else
						jsonResponse.put("status", false);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
					if(!errors.equals(""))
						jsonResponse.put("errorMessage", errors);
					else
						jsonResponse.put("errorMessage", Utility.getLocaleValuePropByKey("msgEmpAddedSuccess", locale));

				}
			} 
			catch (Exception e) {
				e.printStackTrace();
				jsonResponse = UtilityAPI.writeJSONErrors(10017, Utility.getLocaleValuePropByKey("msgServerError", locale), e);

			}
			//out.print(">>>>"+jsonRequest);
			out.print(jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;	
	}
	
	@RequestMapping(value="/service/json/updateEmployees.do", method=RequestMethod.GET)
	public String doUpdateEmployeesByJSONGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("========================================");
		return doUpdateEmployeesByJSONPOST(map, request, response);
	}
	@RequestMapping(value="/service/json/updateEmployees.do", method=RequestMethod.POST)
	public String doUpdateEmployeesByJSONPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{

		try {			
			PrintWriter out =null;
			out = response.getWriter();
			System.out.println("Enter in updateEmployees.do JSON");

			JSONObject jsonResponse = new JSONObject();

			response.setContentType("application/json");
			HttpSession session  = request.getSession();
			DistrictMaster districtMaster = null;

			//JSONObject jsonRequest = new JSONObject();

			System.out.println("request.getParameter(request)"+request.getParameter("request"));

			Boolean responseStatus = false;
			Integer errorCode = 10001;
			String errorMsg = Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			SchoolMaster schoolMaster = null;

			try {
				String payloadRequest = request.getParameter("request");
				if(payloadRequest==null)
				{
						try {
							payloadRequest = ServiceUtility.getBody(request);
						} catch (IOException e) {
							e.printStackTrace();
						}
				}
				System.out.println("payloadRequest::= "+payloadRequest);
				JSONObject jsonRequest = null;
				jsonRequest = (JSONObject) JSONSerializer.toJSON( payloadRequest ); 
				String authKey = jsonRequest.get("authKey")==null?"":UtilityAPI.parseString(jsonRequest.getString("authKey")).replace(' ', '+');
				String orgType = jsonRequest.get("orgType")==null?"":UtilityAPI.parseString(jsonRequest.getString("orgType")).trim();
				String userEmail = jsonRequest.get("userEmail")==null?"":UtilityAPI.parseString(jsonRequest.getString("userEmail")).replace(' ', '+');
				
				if(!userEmail.trim().equals("")){
					userEmail = Utility.decodeBase64(userEmail);
				}
				UserMaster userMaster = null;
				List<UserMaster> lstUMaster = userMasterDAO.findByEmail(userEmail); 
				if(lstUMaster!=null && lstUMaster.size()>0){
					userMaster=lstUMaster.get(0);
				}
				
				if(orgType.equalsIgnoreCase("D")){
					districtMaster =  districtMasterDAO.validateDistrictByAuthkey(authKey);				
					if(districtMaster!=null){
						session.setAttribute("apiDistrictMaster", districtMaster);						
						responseStatus = true;
					}
				}
				else if(orgType.equalsIgnoreCase("S")){
					schoolMaster = schoolMasterDAO.validateSchoolByAuthkey(authKey);					
					System.out.println("SchoolMaster IN"+schoolMaster);
					if(schoolMaster!=null){
						districtMaster =  schoolMaster.getDistrictId();
						session.setAttribute("apiSchoolMaster", schoolMaster);
						session.setAttribute("apiDistrictMaster", schoolMaster.getDistrictId());
						responseStatus = true;
					}
				}
				else{
					responseStatus = false;	
					errorCode=10001;
					errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
				}
				
				
				if(!responseStatus)
				{
					jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
				}else
				{
					JSONArray employees = new JSONArray();
					employees = jsonRequest.getJSONArray("Employee");
					List<String> stats = new ArrayList<String>(); 
					List<String> employeeCodes = new ArrayList<String>(); 
					Map<String,String> empErrorMap = new HashMap<String, String>();
					for (Object object : employees) {
						JSONObject jsonItems = (JSONObject)object;
						String employeeCode = jsonItems.get("employeeCode")==null?"":UtilityAPI.parseString(jsonItems.getString("employeeCode")).trim();
						String state = jsonItems.get("state")==null?"":UtilityAPI.parseString(jsonItems.getString("state")).trim();

						stats.add(state);
						employeeCodes.add(employeeCode);
					}

					List<StateMaster> stateList = stateMasterDAO.findByStateCodes(stats);
					Map<String,StateMaster> stateMap = new HashMap<String, StateMaster>();
					for (StateMaster stateMaster : stateList) {
						stateMap.put(stateMaster.getStateShortName(), stateMaster);
					}

					List<EmployeeMaster> empList = employeeMasterDAO.checkEmployeesByEmployeeCodes(employeeCodes,districtMaster);
					Map<String,EmployeeMaster> empMap = new HashMap<String, EmployeeMaster>();
					for (EmployeeMaster employeeMaster : empList) {
						empMap.put(employeeMaster.getEmployeeCode().trim(), employeeMaster);
					}
					StringBuffer sb = new StringBuffer();
					for (Object object : employees) {
						JSONObject jsonItems = (JSONObject)object;
						
						String employeeCode = jsonItems.get("employeeCode")==null?"":UtilityAPI.parseString(jsonItems.getString("employeeCode")).trim();
						EmployeeMaster employeeMaster = empMap.get(employeeCode);
						String error = empErrorMap.get(employeeCode);
						if(employeeMaster!=null)
						{
							String firstName = jsonItems.get("firstName")==null?"":UtilityAPI.parseString(jsonItems.getString("firstName")).trim();
							String lastName = jsonItems.get("lastName")==null?"":UtilityAPI.parseString(jsonItems.getString("lastName")).trim();
							String middleName = jsonItems.get("middleName")==null?"":UtilityAPI.parseString(jsonItems.getString("middleName")).trim();
							String address1 = jsonItems.get("address1")==null?"":UtilityAPI.parseString(jsonItems.getString("address1")).trim();
							String city = jsonItems.get("city")==null?"":UtilityAPI.parseString(jsonItems.getString("city")).trim();
							String state = jsonItems.get("state")==null?"":UtilityAPI.parseString(jsonItems.getString("state")).trim();
							String zipCode = jsonItems.get("zipCode")==null?"":UtilityAPI.parseString(jsonItems.getString("zipCode")).trim();
							String phone = jsonItems.get("phone")==null?"":UtilityAPI.parseString(jsonItems.getString("phone")).trim();
							String mobile = jsonItems.get("mobile")==null?"":UtilityAPI.parseString(jsonItems.getString("mobile")).trim();
							String birthDate = jsonItems.get("birthDate")==null?"":UtilityAPI.parseString(jsonItems.getString("birthDate")).trim();
							String staffType = jsonItems.get("staffType")==null?"":UtilityAPI.parseString(jsonItems.getString("staffType")).trim();
							if(!birthDate.equals(""))
							{
								Date dob = null;
								try{
									dob = (Date)formatter.parse(birthDate);
								}catch (Exception e) {						
									dob = null;
								}
								if(dob!=null)
								employeeMaster.setDob(dob);
							}
							if(!firstName.equals(""))
								employeeMaster.setEmployeeCode(employeeCode);
							if(!firstName.equals(""))
								employeeMaster.setFirstName(firstName);
							if(!middleName.equals(""))
								employeeMaster.setMiddleName(middleName);
							if(!lastName.equals(""))
								employeeMaster.setLastName(lastName);
							if(!address1.equals(""))
								employeeMaster.setAddress1(address1);
							if(!city.equals(""))
								employeeMaster.setCityName(city);
							if(!zipCode.equals(""))
								employeeMaster.setZipCode(zipCode);
							if(!phone.equals(""))
								employeeMaster.setPhone(phone);
							if(!mobile.equals(""))
								employeeMaster.setMobile(mobile);
							if(!staffType.equals(""))
								employeeMaster.setStaffType(staffType);
							if(!state.equals(""))
							{
								StateMaster statemaster = stateMap.get(state);
								employeeMaster.setStateMaster(statemaster);
								
								if(statemaster==null)
								{
									if(error==null)
										error=Utility.getLocaleValuePropByKey("msgInvalideState", locale) +" "+jsonItems.getString("state")+" "+Utility.getLocaleValuePropByKey("msgForEmployeeCode", locale)+" "+employeeCode+".";
									else
										error+=Utility.getLocaleValuePropByKey("msgInvalideState", locale) +" "+jsonItems.getString("state")+" "+Utility.getLocaleValuePropByKey("msgForEmployeeCode", locale)+" "+employeeCode+".";
								}
							}
						}else
							error= Utility.getLocaleValuePropByKey("msgInvalidemployeeCode", locale)+" "+employeeCode+".";
						
						if(error==null && employeeMaster!=null)
						{
							try {
									employeeMasterDAO.makePersistent(employeeMaster);
									System.out.println("Done:::::::");
								} catch (Exception e) {
									e.printStackTrace();
								}
							
						}else
						{
							sb.append(error);
						}
					}
					String errors= sb.toString();
					
					jsonResponse = new JSONObject();
					if(errors.equals(""))
						jsonResponse.put("status", true);
					else
						jsonResponse.put("status", false);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
					if(!errors.equals(""))
						jsonResponse.put("errorMessage", errors);
					else
						jsonResponse.put("errorMessage", Utility.getLocaleValuePropByKey("msgEmployeeUpdateSucess", locale));

				}
			} 
			catch (Exception e) {
				e.printStackTrace();
				jsonResponse = UtilityAPI.writeJSONErrors(10017, Utility.getLocaleValuePropByKey("msgServerError", locale), e);
			}
			//System.out.print(">>>>"+jsonResponse);
			out.print(jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;	
	}

}
