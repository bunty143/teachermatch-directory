package tm.api.controller;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.commons.io.FileUtils;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import tm.api.UtilityAPI;
import tm.api.services.APIAssessmentCampaignAjax;
import tm.api.services.ServiceUtility;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.NetchemiaDistricts;
import tm.bean.TeacherAffidavit;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherAssessmentAttempt;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherStrikeLog;
import tm.bean.assessment.AssessmentCompetencyScore;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentDomainScore;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.bean.master.DistrictMaster;
import tm.bean.master.StatusMaster;
import tm.bean.teacher.SpInboundAPICallRecord;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.NetchemiaDistrictsDAO;
import tm.dao.TeacherAffidavitDAO;
import tm.dao.TeacherAnswerDetailDAO;
import tm.dao.TeacherAssessmentAttemptDAO;
import tm.dao.TeacherAssessmentDetailDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherStrikeLogDAO;
import tm.dao.assessment.AssessmentCompetencyScoreDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentDomainScoreDAO;
import tm.dao.assessment.AssessmentGroupDetailsDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.teacher.SpInboundAPICallRecordDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.AdminDashboardAjax;
import tm.services.CandidateReportService;
import tm.services.EmailerService;
import tm.utility.AESEncryption;
import tm.utility.TMCommonUtil;
import tm.utility.Utility;

/*
 * Manage Teacher related url
 * setting, changepassword,
 */
@Controller
public class EPIController 
{
	
    String locale = Utility.getValueOfPropByKey("locale");
    
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) 
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) 
	{
		this.userMasterDAO = userMasterDAO;
	}

	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	public void setAssessmentDetailDAO(AssessmentDetailDAO assessmentDetailDAO) {
		this.assessmentDetailDAO = assessmentDetailDAO;
	}

	@Autowired
	private TeacherAssessmentDetailDAO teacherAssessmentDetailDAO;
	public void setTeacherAssessmentDetailDAO(TeacherAssessmentDetailDAO teacherAssessmentDetailDAO) {
		this.teacherAssessmentDetailDAO = teacherAssessmentDetailDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private TeacherAssessmentAttemptDAO assessmentAttemptDAO;
	public void setAssessmentAttemptDAO(TeacherAssessmentAttemptDAO assessmentAttemptDAO) {
		this.assessmentAttemptDAO = assessmentAttemptDAO;
	}
	
	@Autowired
	private TeacherAnswerDetailDAO teacherAnswerDetailDAO;
	public void setTeacherAnswerDetailDAO(TeacherAnswerDetailDAO teacherAnswerDetailDAO) {
		this.teacherAnswerDetailDAO = teacherAnswerDetailDAO;
	}
	
	@Autowired
	private TeacherStrikeLogDAO teacherStrikeLogDAO;
	public void setTeacherStrikeLogDAO(TeacherStrikeLogDAO teacherStrikeLogDAO) {
		this.teacherStrikeLogDAO = teacherStrikeLogDAO;
	}
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	
	@Autowired
 	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	
	@Autowired
	private ServiceUtility serviceUtility;
	public void setServiceUtility(ServiceUtility serviceUtility) {
		this.serviceUtility = serviceUtility;
	}
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	public void setTeacherNormScoreDAO(TeacherNormScoreDAO teacherNormScoreDAO) {
		this.teacherNormScoreDAO = teacherNormScoreDAO;
	}
	
	@Autowired
	private AdminDashboardAjax adminDashboardAjax;
	
	@Autowired
	private AssessmentGroupDetailsDAO assessmentGroupDetailsDAO;
	
	@Autowired
	private APIAssessmentCampaignAjax apiAssessmentCampaignAjax;

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setAssessmentDetailDAO(DistrictMasterDAO districtMasterDAO){
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	
	@Autowired
	private AssessmentDomainScoreDAO assessmentDomainScoreDAO;

	@Autowired
	private AssessmentCompetencyScoreDAO assessmentCompetencyScoreDAO;
	
	@Autowired 
	private NetchemiaDistrictsDAO netchemiaDistrictsDAO;
	
	@Autowired
	private CandidateReportService candidateReportService;
	public void setCandidateReportService(CandidateReportService candidateReportService) {
		this.candidateReportService = candidateReportService;
	}
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	
	@Autowired
	private TeacherAffidavitDAO teacherAffidavitDAO;
	
	@Autowired
	private SpInboundAPICallRecordDAO spInboundAPICallRecordDAO;
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to run assessment.
	 */
	@RequestMapping(value="/service/apirunAssessment.do", method=RequestMethod.GET)
	public String runAssessment(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("apiTeacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}

		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("apiTeacherDetail");
		DistrictMaster districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
		session.setAttribute("teacherDetail", teacherDetail);
		/***  Session set for Menu List( Teacher ) populate ***/
		session.setAttribute("lstMenuMaster", roleAccessPermissionDAO.getMenuListForTeacher(request));
		map.addAttribute("teacherDetail", teacherDetail);
		
		String assessmentId = request.getParameter("assessmentId");
		String apijobId = request.getParameter("jobId");
		String newJobId = request.getParameter("newJobId");
		String epiJobId = request.getParameter("epiJobId");
		AssessmentDetail assessmentDetail = null;
		TeacherAssessmentdetail teacherAssessmentdetail = null;
		int remainingSessionTime = 0;
		JobOrder jobOrder = null;
		int attemptId = 0;
		int totalAttempted = 0;
		int assessmentType = 1;
		try 
		{
			if(assessmentId!=null && assessmentId.trim().length()>0)
			{
				assessmentDetail = assessmentDetailDAO.findById(Integer.parseInt(assessmentId), false, false);
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
				assessmentType = assessmentDetail.getAssessmentType();
				teacherAssessmentStatusList = teacherAssessmentStatusDAO.getTeacherAssessmentStatus(assessmentDetail,teacherDetail);
				if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()!=0)
					teacherAssessmentdetail = teacherAssessmentStatusList.get(0).getTeacherAssessmentdetail();
				
				int totalAssessmentSessionTime = teacherAssessmentdetail.getAssessmentSessionTime()==null?0:teacherAssessmentdetail.getAssessmentSessionTime();
				int consumedSessionTime = 0;
				TeacherAssessmentAttempt teacherAssessmentAttempt = null;
				List<TeacherAssessmentAttempt> teacherAssessmentAttempts = assessmentAttemptDAO.findNoOfAttempts(assessmentDetail, teacherDetail, teacherAssessmentdetail.getAssessmentTakenCount());
				
				if (!teacherAssessmentAttempts.isEmpty()) {
					{
						for (TeacherAssessmentAttempt teacherAssessmentAttempt1 : teacherAssessmentAttempts) {
							consumedSessionTime+=teacherAssessmentAttempt1.getAssessmentSessionTime();
							
							if(!teacherAssessmentAttempt1.getIsForced())
								totalAttempted++;
							
							//totalAttempted++;
						}
						teacherAssessmentAttempt = teacherAssessmentAttempts.get(teacherAssessmentAttempts.size()-1);
						attemptId = teacherAssessmentAttempt.getAttemptId();
						
						/// new changes /////////////
						/*if(teacherAssessmentAttempt.getIsForced())
							totalAttempted = totalAttempted - 1;*/
						///////////////////////////
					}
				}
				remainingSessionTime = totalAssessmentSessionTime-consumedSessionTime;
			}
			if(apijobId!=null && apijobId.trim().length()>0)
			{
				jobOrder = jobOrderDAO.findJobByApiJobId(apijobId,districtMaster);
			}

			List<TeacherAnswerDetail> teacherAnswerDetails = null;
			/*teacherAnswerDetails = teacherAnswerDetailDAO.findNoOfSkippedQuestion(assessmentDetail, teacherDetail, teacherAssessmentdetail);
			System.out.println(teacherAnswerDetails.size());*/
			
			List<TeacherStrikeLog> teacherStrikeLogList= null;
			teacherStrikeLogList = teacherStrikeLogDAO.getTotalStrikesByTeacher(teacherDetail, teacherAssessmentdetail);
			//System.out.println("teacherStrikeLogList.size()::::::::::::::: "+teacherAssessmentdetail.getTeacherAssessmentId());
			//System.out.println("teacherStrikeLogList.size()::::::::::::::: "+teacherDetail.getTeacherId());

			map.addAttribute("assessmentDetail", assessmentDetail);	
			map.addAttribute("teacherAssessmentdetail", teacherAssessmentdetail);
			map.addAttribute("jobOrder", jobOrder);
			map.addAttribute("remainingSessionTime", remainingSessionTime);
			map.addAttribute("attemptId", attemptId);
			map.addAttribute("totalAttempted",totalAttempted);
			//map.addAttribute("totalSkippedQuestions", teacherAnswerDetails.size());
			map.addAttribute("totalSkippedQuestions", 0);
			map.addAttribute("newJobId", newJobId);
			map.addAttribute("epiJobId", epiJobId);
			map.addAttribute("isCompleted", true);
			int teacherStrikeLogListSize=0;
			if(teacherStrikeLogList!=null){
				teacherStrikeLogListSize=teacherStrikeLogList.size();
			}
			map.addAttribute("totalStrikes",teacherStrikeLogListSize);
			String rURL = "startOrContinueEPI.do";
			boolean epiStandalone = teacherDetail.getIsPortfolioNeeded();
			System.out.println("epiStandalone:::: "+epiStandalone);
			if(!epiStandalone)
				rURL = "startOrContinueEPI.do";
			
			if(assessmentType==1)
			{
				List<JobForTeacher> lstJobForTeacher = jobForTeacherDAO.findMostRecentJobByTeacher(teacherDetail);
				if(lstJobForTeacher.size()>0)
				{
					boolean onDashboard = false;
					jobOrder = lstJobForTeacher.get(0).getJobId();
					int flagForURL = jobOrder.getFlagForURL();
					int flagForMessage  = jobOrder.getFlagForMessage();
					String exitMessage = jobOrder.getExitMessage();
					String exitURL = jobOrder.getExitURL();
					
					if(exitMessage!=null && !exitMessage.trim().equals("") && exitURL!=null && !exitURL.trim().equals(""))
					{
						onDashboard = true;
					}else if(flagForMessage==1)
					{
						onDashboard = false;
						rURL = "thankyoumessage.do?jobId="+jobOrder.getJobId();
						//sb.append("window.location.href='thankyoumessage.do?jobId="+jobOrder.getJobId()+"'");
					}else if(flagForURL==1)
					{
						if(!epiStandalone)
							rURL = "startOrContinueEPI.do?jobId="+jobOrder.getJobId()+"&es=1";
							//rURL=exitURL;
						else
							rURL = "startOrContinueEPI.do?jobId="+jobOrder.getJobId()+"&es=1";;
							//rURL=exitURL;
					}
					/*if(rURL.indexOf("http")==-1)
						rURL="http://"+rURL;*/
				}
			}
			System.out.println("rURL::: "+rURL);
			map.addAttribute("rURL",rURL);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "apirunAssessment";
	}
	
	
	@RequestMapping(value="/service/json/epiStatus.do", method=RequestMethod.GET)
	public String epiStatusJSONGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		PrintWriter out = null;
		JSONObject jsonResponse = new JSONObject();
		response.setContentType("application/json");
		String epiStatus = "Icomplete";
		boolean isError = true;
		Integer errorCode = 0;
		String errorMsg = "";
		try {			
			out =  response.getWriter();
			String candidateEmail = request.getParameter("candidateEmail")==null?"":request.getParameter("candidateEmail").trim().replace(' ', '+');
			if(!candidateEmail.trim().equals("")){
				candidateEmail = UtilityAPI.decodeBase64(candidateEmail);
			}
			
			Boolean isValidUser = false;
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			
			List<TeacherDetail> lstTDetails = teacherDetailDAO.findByEmail(candidateEmail);
			TeacherDetail teacherDetail = null;
			if(lstTDetails!=null && lstTDetails.size()>0){
				teacherDetail = lstTDetails.get(0);
			}			
			if(!isValidUser){
				errorCode=new Integer(mapDistSchDetail.get("errorCode"));
				errorMsg=mapDistSchDetail.get("errorMsg");
								
			}
			else if(candidateEmail.trim().equals("")){
				errorCode=10002;
				errorMsg= Utility.getLocaleValuePropByKey("msgCanEmailReq", locale);
			}	
			else if(!Utility.validEmailForTeacher(candidateEmail)){
				errorCode=10003;
				errorMsg=Utility.getLocaleValuePropByKey("msgInvalidemailaddres", locale);
			}			
			else if(teacherDetail==null){
				errorCode=10004;
				errorMsg=Utility.getLocaleValuePropByKey("msgCanNotExistInTM", locale);				
			}
			else{
				isError=false;
				TeacherAssessmentStatus taStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
				if(taStatus!=null){
					epiStatus = taStatus.getStatusMaster().getStatus();
				}				
				jsonResponse.put("status",true);				
				jsonResponse.put("epiStatus",epiStatus);
				jsonResponse.put("timestamp",UtilityAPI.getCurrentTimeStamp());
			}
			
			if(isError){
				jsonResponse = UtilityAPI.writeJSONErrors(errorCode,errorMsg, null);
			}
			
			
		}catch (Exception e){
			jsonResponse = UtilityAPI.writeJSONErrors(10017, "Server Error.", e);
			e.printStackTrace();
		}		
		out.print(jsonResponse);
		return null;
	}
	
	@RequestMapping(value="/service/xml/epiStatus.do", method=RequestMethod.GET)
	public String epiStatusXMLGET(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{	
		
		
		try {
			PrintWriter out = null;		
			response.setContentType("text/xml");
			String epiStatus = "Icomp";
			boolean isError = true;
			Integer errorCode = 0;
			String errorMsg = "";
			
			
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = null;
			Document doc = null;
			Element root = null;
			try {	
				
				docBuilder = builderFactory.newDocumentBuilder();
				doc = docBuilder.newDocument();
				root = doc.createElement("teachermatch");
				doc.appendChild(root);
				
				out =  response.getWriter();
				String candidateEmail = request.getParameter("candidateEmail")==null?"":request.getParameter("candidateEmail").trim().replace(' ', '+');
				if(!candidateEmail.trim().equals("")){
					candidateEmail = UtilityAPI.decodeBase64(candidateEmail);
				}
				
				Boolean isValidUser = false;
				Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
				isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
				
				List<TeacherDetail> lstTDetails = teacherDetailDAO.findByEmail(candidateEmail);
				TeacherDetail teacherDetail = null;
				if(lstTDetails!=null && lstTDetails.size()>0){
					teacherDetail = lstTDetails.get(0);
				}			
				if(!isValidUser){
					errorCode=new Integer(mapDistSchDetail.get("errorCode"));
					errorMsg=mapDistSchDetail.get("errorMsg");
									
				}
				else if(candidateEmail.trim().equals("")){
					errorCode=10002;
					errorMsg="Candidate email address is required.";
				}	
				else if(!Utility.validEmailForTeacher(candidateEmail)){
					errorCode=10003;
					errorMsg="Invalid email address.";
				}			
				else if(teacherDetail==null){
					errorCode=10004;
					errorMsg="Candidate does not exist in Teachermatch.";				
				}
				else{
					isError=false;
					TeacherAssessmentStatus taStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
					if(taStatus!=null){
						epiStatus = taStatus.getStatusMaster().getStatus();
					}				

					
					Element eleEpiStatus = doc.createElement("status");
					root.appendChild(eleEpiStatus);
					Text textEpiStatus = doc.createTextNode("true");
					eleEpiStatus.appendChild(textEpiStatus);
					
					Element eleStatus = doc.createElement("epiStatus");
					root.appendChild(eleStatus);
					Text textStatus = doc.createTextNode(epiStatus);
					eleStatus.appendChild(textStatus);
					
					Element timeStamp = doc.createElement("timeStamp");
					root.appendChild(timeStamp);
					Text timeStampResult = doc.createTextNode(UtilityAPI.getCurrentTimeStamp());
					timeStamp.appendChild(timeStampResult);

					
					
				}
				
				if(isError){
					UtilityAPI.writeXMLErrors(errorCode,errorMsg,null, doc, root);
				}
				
				
			}catch (Exception e){
				
				UtilityAPI.writeXMLErrors(errorCode,errorMsg,e, doc, root);
				e.printStackTrace();
			}	
			
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			out.println(sw.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to redirect to thankyou page.
	 */
	@RequestMapping(value="/service/apithankyoumessage.do", method=RequestMethod.GET)
	public String thankYouMsgGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("apiTeacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}

		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("apiTeacherDetail");
		DistrictMaster districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
		map.addAttribute("teacherDetail", teacherDetail);
		String apiJobId = request.getParameter("jobId");
		JobOrder jobOrder  = null;

		try 
		{
			if(apiJobId!=null && apiJobId.trim().length()>0)
				jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);				
			

			map.addAttribute("jobOrder", jobOrder);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "apithankyoumessage";
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to redirect to thank you page.
	 */
	@RequestMapping(value="/service/startOrContinueEPI.do", method=RequestMethod.GET)
	public String userboardGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println(" ::::::::::::::::::::::::::::::: /service/startOrContinueEPI.do ::::::::::::::::::::::::::::::: ");
		HttpSession session = request.getSession();
		String candidateEmail = request.getParameter("candidateEmail")==null?"":request.getParameter("candidateEmail").trim().replace(' ', '+');
		String teo = request.getParameter("teo")==null?"":request.getParameter("teo").trim().replace(' ', '+');
		System.out.println("candidateEmail : "+candidateEmail);
		boolean epIOnly = false;
		if(teo.equalsIgnoreCase("y"))
			epIOnly = true;
		Integer errorCode = 0;
		String errorMsg = "";
		
		Boolean isValidUser = false;		
		Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);		
		TeacherDetail teacherDetail = null;
		
		if(session.getAttribute("apiDistrictMaster")==null && session.getAttribute("apiSchoolMaster")==null ){
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
		}
		else{
			isValidUser = true;
		}
		System.out.println("isValidUser : "+isValidUser);
		
		Boolean tCreatedOrExist = false;
		Map<String,String> mapTDetail = new HashMap<String, String>();
		if(isValidUser)
		{
			if(session.getAttribute("apiTeacherDetail")!=null && candidateEmail.equals("")){
				tCreatedOrExist = true;
				teacherDetail = (TeacherDetail) session.getAttribute("apiTeacherDetail");
			}
			else{
				mapTDetail  = serviceUtility.validateTeacher(request);
				tCreatedOrExist = new Boolean(mapTDetail.get("tCreatedOrExist"));
				teacherDetail = (TeacherDetail) session.getAttribute("apiTeacherDetail");
			}
		}
		DistrictMaster districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
		map.addAttribute("teacherDetail", teacherDetail);
		String apiJobId = request.getParameter("jobId");
		JobOrder jobOrder  = null;
		Integer epiJobId = null;
		System.out.println("mapTDetail : "+mapTDetail);
		try 
		{
			if(!isValidUser){				 
				map.addAttribute("tCreatedOrExist", false);
				map.addAttribute("errorCode","10001" );
				map.addAttribute("errorMsg", "Invalid Credentials.");
			}
			else if(tCreatedOrExist==false){
				map.addAttribute("tCreatedOrExist", false);
				errorCode = new Integer(mapTDetail.get("errorCode").trim());
				errorMsg =  mapTDetail.get("errorMsg");
				map.addAttribute("errorCode",errorCode );
				map.addAttribute("errorMsg", errorMsg);	
			}
			else{
				map.addAttribute("tCreatedOrExist", true);
				session.setAttribute("apiTeacherDetail", teacherDetail);
				NetchemiaDistricts netchemiaDistricts = null;
				
				try {
					netchemiaDistricts = netchemiaDistrictsDAO.findByDistrictId(districtMaster,epIOnly);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				if(apiJobId!=null && apiJobId.trim().length()>0){
					try {
						jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
				
				if(apiJobId!=null && apiJobId.trim().length()>0 || netchemiaDistricts!=null)
				{
					if(jobOrder==null)
					{
						jobOrder = jobOrderDAO.getListByTitle("Netchemia Integration",districtMaster);
					}
					if(jobOrder!=null)
					{
						JobForTeacher jobForTeacher = null;
						jobForTeacher = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
						System.out.println("jobForTeacher : "+jobForTeacher);
						String redirectedFromURL = request.getHeader("referer")==null?"":request.getHeader("referer");
						String jobBoardReferralURL = session.getAttribute("jobBoardReferralURL")==null?"":(String)session.getAttribute("jobBoardReferralURL");
						try {
							jobForTeacher = jobForTeacherDAO.saveJobForTeacher(jobForTeacher, jobOrder, teacherDetail, redirectedFromURL, jobBoardReferralURL);
							jobOrder =null;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			
				map.addAttribute("jobOrder", jobOrder);
				epiJobId = session.getAttribute("epiJobId")==null?null:(Integer)session.getAttribute("epiJobId");
				if(epiJobId==null && jobOrder!=null && jobOrder.getJobId()!=null){
					epiJobId = jobOrder.getJobId();
				}
				if(epiJobId!=null)
					map.addAttribute("epiJobId", epiJobId);
				else if(epiJobId==null)
					map.addAttribute("epiJobId", "null");
				boolean baseInvStatus = false;
				
				TeacherAssessmentStatus teacherBaseAssessmentStatus = null;
				teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
				if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") )){
					baseInvStatus=true;
				}				
				if(baseInvStatus){
					System.out.println("inside Base epi complete");
					map.addAttribute("baseStatus", "comp");	
					if((districtMaster!=null))
					{
						System.out.println("inside netchemia::::::::");
					    List<TeacherNormScore> list=teacherNormScoreDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherDetail));
					    if((list!=null)&&(list.size()>0))
					    {
					    	if(netchemiaDistricts==null)
					    	netchemiaDistricts = netchemiaDistrictsDAO.findByDistrictId(districtMaster,epIOnly);
							
							if(netchemiaDistricts!=null && netchemiaDistricts.getApiURL()!=null)
							{
								String apiURL = netchemiaDistricts.getApiURL();
								if(!apiURL.equals(""))
								{
									//if(districtMaster.getDistrictId()==7800052)
									TMCommonUtil.sendComposteScoreJSON(list.get(0),apiURL);
								}
							}
					    }
					}
				}
				else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
					map.addAttribute("baseStatus", "vlt");	
				}
				else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")){
					map.addAttribute("baseStatus", "icomp");
					map.addAttribute("assessmentDetail", teacherBaseAssessmentStatus.getAssessmentDetail());
				}
				else{
					map.addAttribute("baseStatus", "resume");
					AssessmentDetail assessmentDetail = new AssessmentDetail();
					JobOrder jobOrder2 = new JobOrder();
					jobOrder2.setJobId(0);
					apiAssessmentCampaignAjax.setHttpServletRequest(request);
					assessmentDetail = apiAssessmentCampaignAjax.checkInventory(jobOrder2,epiJobId);
					map.addAttribute("assessmentDetail", assessmentDetail);
				}
			}	
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		
		boolean chkAffidavit = false;
		String affChk = "";
		String affDisable = "";
		
		try{
			TeacherPortfolioStatus teacherPortfolioStatus = null;
			TeacherAffidavit teacherAffidavit = null;
			if(teacherDetail!=null){
				teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				teacherAffidavit = teacherAffidavitDAO.findAffidavitByTeacher(teacherDetail);
			
				if(teacherPortfolioStatus!=null && teacherPortfolioStatus.getIsAffidavitCompleted()!=null && teacherPortfolioStatus.getIsAffidavitCompleted())
					chkAffidavit = true;
				
				if(teacherAffidavit!=null && teacherAffidavit.getIsDone()!=null && teacherAffidavit.getIsDone() && teacherAffidavit.getAffidavitAccepted()!=null && teacherAffidavit.getAffidavitAccepted())
					chkAffidavit = true;
				
			}
			
			if(chkAffidavit){
				affChk = "checked";
				affDisable = "disabled";
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		map.addAttribute("affChk", affChk);
		map.addAttribute("affDisable", affDisable);
		
		System.out.println(" affChk :: "+affChk+" affDisable :: "+affDisable);
		
		return "userboard";
	}

	@RequestMapping(value="/service/setEPIStatus.do", method=RequestMethod.GET)
	public String setEPIStatusJSON(ModelMap map, HttpServletRequest request, HttpServletResponse response){
		System.out.println("######Hello######");
		try {			
			response.setContentType("text/xml");
			PrintWriter out = response.getWriter();
			out.println("candidateEmail"+request.getParameter("candidateEmail"));
			out.println("epiStatus"+request.getParameter("epiStatus"));		
			//TeacherDetail teacherDetail = new TeacherDetail();
			/*if(teacherDetail!=null)
				adminDashboardAjax.makeEPIIncomplete(teacherDetail);*/
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}

	/**
	 * @author Amit Chaudhary
	 * @param map
	 * @param request
	 * @return inventory
	 * @date 08-May-15
	 */
	@RequestMapping(value="/service/inventory.do", method=RequestMethod.GET)
	public String ipiboardGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println(" ::::::::::::::::::::::::::::::: inventory.do ::::::::::::::::::::::::::::::: ");
		HttpSession session = request.getSession();
		String id = request.getParameter("id")==null?"":request.getParameter("id").replace(' ', '+');
		String unit = request.getParameter("unit")==null?"":request.getParameter("unit").trim();
		if(unit==null || unit.equals("")){
			unit = "01";
		}
		Integer errorCode = 10001;
		String errorMsg = "Invalid Credentials.";
		String decryptText = null;
		
		try
		{
			if((id==null || id.equals("")) && session != null && (session.getAttribute("teacherDetail")!=null ))
			{
				TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
				List<SpInboundAPICallRecord> spInboundAPICallRecordList = spInboundAPICallRecordDAO.getDetailsByTID(teacherDetail);
				SpInboundAPICallRecord spInboundAPICallRecord = new SpInboundAPICallRecord();
				if(spInboundAPICallRecordList!=null && spInboundAPICallRecordList.size()>0)
					spInboundAPICallRecord = spInboundAPICallRecordList.get(spInboundAPICallRecordList.size()-1);
				int currentLesson = 0;
				int totalLessons = 0;
				if(unit!=null && !unit.equals("")){
					if(unit.equals("01")){
						if(spInboundAPICallRecord.getCurrentLessonNo()!=null && spInboundAPICallRecord.getTotalLessons()!=null && spInboundAPICallRecord.getCurrentLessonNo()>0 && spInboundAPICallRecord.getTotalLessons()>0){
							currentLesson = spInboundAPICallRecord.getCurrentLessonNo();
							totalLessons = spInboundAPICallRecord.getTotalLessons();
						}
					}
					else if(unit.equals("03")){
						if(spInboundAPICallRecord.getUnit03currentLessonNo()!=null && spInboundAPICallRecord.getUnit03TotalLessons()!=null && spInboundAPICallRecord.getUnit03currentLessonNo()>0 && spInboundAPICallRecord.getUnit03TotalLessons()>0){
							currentLesson = spInboundAPICallRecord.getUnit03currentLessonNo();
							totalLessons = spInboundAPICallRecord.getUnit03TotalLessons();
						}
					}
					else if(unit.equals("04")){
						if(spInboundAPICallRecord.getUnit04currentLessonNo()!=null && spInboundAPICallRecord.getUnit04TotalLessons()!=null && spInboundAPICallRecord.getUnit04currentLessonNo()>0 && spInboundAPICallRecord.getUnit04TotalLessons()>0){
							currentLesson = spInboundAPICallRecord.getUnit04currentLessonNo();
							totalLessons = spInboundAPICallRecord.getUnit04TotalLessons();
						}
					}
				}
				
				if(currentLesson>0 && totalLessons>0 && (currentLesson==totalLessons))
				{
					String assessmentGroupName = null;
	       			String encryptedGrpName = null;
					List<AssessmentGroupDetails> assessmentGroupDetailsList = assessmentGroupDetailsDAO.getAllAssessmentGroupDetailsList(3, 1);
	       			if(assessmentGroupDetailsList!=null && assessmentGroupDetailsList.size() >0)
	       			{
	       				for(AssessmentGroupDetails assessmentGroupDetails : assessmentGroupDetailsList){
	       					if(unit!=null && !unit.equals("")){
								if(unit.equals("01")){
									if(assessmentGroupDetails.getGroupShortName()!=null && assessmentGroupDetails.getGroupShortName().equalsIgnoreCase("unit1")){
										assessmentGroupName = assessmentGroupDetails.getAssessmentGroupName();
										break;
									}
								}
								else if(unit.equals("03")){
									if(assessmentGroupDetails.getGroupShortName()!=null && assessmentGroupDetails.getGroupShortName().equalsIgnoreCase("unit3")){
										assessmentGroupName = assessmentGroupDetails.getAssessmentGroupName();
										break;
									}
								}
								else if(unit.equals("04")){
									if(assessmentGroupDetails.getGroupShortName()!=null && assessmentGroupDetails.getGroupShortName().equalsIgnoreCase("unit4")){
										assessmentGroupName = assessmentGroupDetails.getAssessmentGroupName();
										break;
									}
								}
							}
	       				}
	       				if(assessmentGroupName!=null)
	       					encryptedGrpName = Utility.encodeInBase64(assessmentGroupName);
	       			}
					if(encryptedGrpName!=null && !encryptedGrpName.equals(""))
					{
						String authKey = districtMasterDAO.findByCriteria(Restrictions.eq("districtId", 7800035)).get(0).getAuthKey();
						JSONObject jsonObject = new JSONObject();
		       			jsonObject.put("authKey", authKey);
		       			jsonObject.put("orgType", "D");
		       			jsonObject.put("assessmentType", "3");
		       			jsonObject.put("grpName", encryptedGrpName);
		       			jsonObject.put("candidateFirstName", teacherDetail.getFirstName());
		       			jsonObject.put("candidateLastName", teacherDetail.getLastName());
		       			jsonObject.put("candidateEmail", Utility.encodeInBase64(teacherDetail.getEmailAddress()));
		       			String encryptText = AESEncryption.encrypt(jsonObject.toString());
						System.out.println("encryptText : "+encryptText);
						id = encryptText;
					}
				}
				else{
					map.addAttribute("tCreatedOrExist", true);
					map.addAttribute("assessmentStatus", "inbound");
					return "inventory";
				}
			}	
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		try {
			if(id!=null && id!="")
				decryptText = AESEncryption.decrypt(id);
			else
			{
				map.addAttribute("errorCode",errorCode);
				map.addAttribute("errorMsg", errorMsg);
			}
			System.out.println("decryptText : "+decryptText);
		} catch (Exception e) {
			e.printStackTrace();
			if(e.getCause() instanceof javax.crypto.IllegalBlockSizeException){
				map.addAttribute("errorCode",errorCode);
				map.addAttribute("errorMsg", errorMsg);
			}
		}

		JSONObject jsonObject  = new JSONObject();
		if(decryptText!=null)
			jsonObject = (JSONObject) JSONSerializer.toJSON(decryptText);
		else
		{
			map.addAttribute("errorCode",errorCode);
			map.addAttribute("errorMsg", errorMsg);
		}
		
		String authKey=null, orgType=null, grpName=null, candidateFirstName=null, candidateLastName=null, candidateEmail=null, ipiReferralURL=null;
		Integer assessmentType=null;
		
		if(jsonObject.get("authKey")!=null)
			authKey = jsonObject.get("authKey").toString();
		
		if(jsonObject.get("orgType")!=null)
			orgType = jsonObject.get("orgType").toString();
		
		if(jsonObject.get("assessmentType")!=null)
			assessmentType = Integer.parseInt(jsonObject.get("assessmentType").toString());
		
		if(jsonObject.get("grpName")!=null)
			grpName = jsonObject.get("grpName").toString();
		
		if(jsonObject.get("candidateFirstName")!=null)
			candidateFirstName = jsonObject.get("candidateFirstName").toString();
		
		if(jsonObject.get("candidateLastName")!=null)
			candidateLastName = jsonObject.get("candidateLastName").toString();
		
		if(jsonObject.get("candidateEmail")!=null)
			candidateEmail = jsonObject.get("candidateEmail").toString();
		
		if(jsonObject.get("ipiReferralURL")!=null){
			ipiReferralURL = jsonObject.get("ipiReferralURL").toString();
			if(ipiReferralURL!=null && ipiReferralURL!=""){
				session.setAttribute("ipiReferralURL", ipiReferralURL);
			}
			System.out.println("ipiReferralURL : "+ipiReferralURL);
		}
		else{
			try 
			{
				String strURL = "";
				ipiReferralURL = request.getHeader("referer")==null?"":request.getHeader("referer");
				System.out.println("ipiReferralURL : "+ipiReferralURL);
				if(session.getAttribute("ipiReferralURL")==null || session.getAttribute("ipiReferralURL")==""){
					System.out.println("session.getAttribute('ipiReferralURL') : "+null);
					if(ipiReferralURL!="" && ipiReferralURL!=null){
						String[] referralURLArr = ipiReferralURL.split("/");
						strURL = referralURLArr[0]+"//"+referralURLArr[2]+"/service/publishInventoryScores";
						session.setAttribute("ipiReferralURL", strURL);
					}
				}
				else{
					System.out.println("session.getAttribute('ipiReferralURL') : "+session.getAttribute("ipiReferralURL"));
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("authKey : "+authKey);
		System.out.println("orgType : "+orgType);
		System.out.println("assessmentType : "+assessmentType);
		System.out.println("grpName : "+grpName);
		System.out.println("candidateFirstName : "+candidateFirstName);
		System.out.println("candidateLastName : "+candidateLastName);
		System.out.println("candidateEmail : "+candidateEmail);
		//String candidateEmail = request.getParameter("candidateEmail")==null?"":request.getParameter("candidateEmail").trim().replace(' ', '+');
		//System.out.println("candidateEmail == "+candidateEmail);
		
		Boolean isValidUser = false;		
		Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request, authKey, orgType);		
		TeacherDetail teacherDetail = null;
		/*Integer assessmentType = null;
		String grpName = null;
		if(request.getParameter("assessmentType").equals("") || request.getParameter("assessmentType").equals(null))
			assessmentType =null;
		else
			assessmentType = Integer.parseInt(request.getParameter("assessmentType").trim());
		if(request.getParameter("grpName").equals("") || request.getParameter("grpName").equals(null))
			grpName = null;
		else
			grpName = request.getParameter("grpName")==null?"":request.getParameter("grpName").trim();
		*/
		if(session.getAttribute("apiDistrictMaster")==null && session.getAttribute("apiSchoolMaster")==null)
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
		else
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
		
		System.out.println("isValidUser : "+isValidUser);
		//System.out.println("assessmentType == "+assessmentType);
		//System.out.println("encoded grpName == "+grpName);
		
		Boolean tCreatedOrExist = false;
		Map<String,String> mapTDetail = new HashMap<String, String>();
		if(session.getAttribute("apiTeacherDetail")!=null && candidateEmail.equals("")){
			tCreatedOrExist = true;
			teacherDetail = (TeacherDetail) session.getAttribute("apiTeacherDetail");
		}
		else{
			mapTDetail  = serviceUtility.validateTeacher(request, candidateFirstName, candidateLastName, candidateEmail, assessmentType);
			tCreatedOrExist = new Boolean(mapTDetail.get("tCreatedOrExist"));
			teacherDetail = (TeacherDetail) session.getAttribute("apiTeacherDetail");
		}
		map.addAttribute("teacherDetail", teacherDetail);
		//System.out.println("mapTDetail == "+mapTDetail);
		try
		{
			if(!isValidUser){
				map.addAttribute("tCreatedOrExist", false);
				map.addAttribute("errorCode","10001" );
				map.addAttribute("errorMsg", "Invalid Credentials.");
			}
			else if(tCreatedOrExist==false){
				map.addAttribute("tCreatedOrExist", false);
				errorCode = new Integer(mapTDetail.get("errorCode").trim());
				errorMsg =  mapTDetail.get("errorMsg");
				map.addAttribute("errorCode",errorCode );
				map.addAttribute("errorMsg", errorMsg);	
			}
			else if(assessmentType==null || (assessmentType!=null && assessmentType!=3) && (assessmentType!=null && assessmentType!=4)){
				map.addAttribute("tCreatedOrExist", false);
				map.addAttribute("errorCode","10006" );
				map.addAttribute("errorMsg", "Invalid Assessment Type.");
			}
			else if(grpName==null){
				map.addAttribute("tCreatedOrExist", false);
				map.addAttribute("errorCode","10007" );
				map.addAttribute("errorMsg", "Invalid Group Name.");
			}
			else if(assessmentType!=null && grpName!=null){
				grpName = UtilityAPI.decodeBase64(grpName);
				System.out.println("grpName : "+grpName);
				List<AssessmentGroupDetails> assessmentGroupDetailList = assessmentGroupDetailsDAO.getAssessmentGroupDetailsListByTypeAndName(assessmentType, grpName);
				if(assessmentGroupDetailList==null || assessmentGroupDetailList.size()==0){
					map.addAttribute("tCreatedOrExist", false);
					map.addAttribute("errorCode","10007" );
					map.addAttribute("errorMsg", "Invalid Group Name.");
				}
				else if(assessmentGroupDetailList!=null && assessmentGroupDetailList.size()>0){
					map.addAttribute("tCreatedOrExist", true);
					map.addAttribute("assessmentType", assessmentType);
					map.addAttribute("grpName", grpName);
					session.setAttribute("apiTeacherDetail", teacherDetail);
					session.setAttribute("encryptText", id);
					
//					boolean assessmentStatus = false;
					List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
					boolean isPass = false;
					JobOrder jobOrder = new JobOrder();
					if(assessmentType==3){
						jobOrder.setJobId(-3);
						teacherAssessmentStatusList = teacherAssessmentStatusDAO.findTeacherAssessmentStatusByAssessmentType(teacherDetail, assessmentType);
						if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0){
							for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList){
								if(teacherAssessmentStatus.getPass()!=null && teacherAssessmentStatus.getPass().equals("P"))
									isPass = true;
							}
						}
					}
					else if(assessmentType==4)
						jobOrder.setJobId(-4);
					
					if(isPass)
					{
						System.out.println("isPass : "+isPass);
						map.addAttribute("assessmentStatus", "pass");
					}
					else
					{
						AssessmentDetail assessmentDetail = apiAssessmentCampaignAjax.checkInventory(jobOrder, assessmentType, grpName, false, request);
						if(assessmentDetail.getAssessmentName()==null && assessmentDetail.getStatus().equals("1"))
						{
							map.addAttribute("assessmentStatus", "1");
						}
						else if(assessmentDetail.getAssessmentName()==null && assessmentDetail.getStatus().equals("8"))
						{
							map.addAttribute("assessmentStatus", "8");
						}
						else if(assessmentDetail.getStatus()!=null && assessmentDetail.getStatus().length()>1)
						{
							map.addAttribute("assessmentStatus", "icomp");
							map.addAttribute("assessmentDetail", assessmentDetail);
						}
						else
						{
							map.addAttribute("assessmentStatus", "resume");
							map.addAttribute("assessmentDetail", assessmentDetail);
						}
					}
					
					/*teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findTeacherAssessmentStatusByAssessmentType(teacherDetail, assessmentType);
					if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") )){
						assessmentStatus=true;
					}
					if(assessmentStatus){
						map.addAttribute("assessmentStatus", "comp");	
					}
					else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
						map.addAttribute("assessmentStatus", "vlt");	
					}
					else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")){
						map.addAttribute("assessmentStatus", "icomp");
					}
					else{
						map.addAttribute("assessmentStatus", "resume");
					}*/
				}
			}			
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return "inventory";
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param request
	 * @return inventorycomplete
	 * @date 08-May-15
	 */
	@RequestMapping(value="/service/inventorycomplete.do", method=RequestMethod.GET)
	public String logoutWithThanks(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println(":::::::::::::: inventorycomplete.do ::::::::::::::");
		String assessmentType = request.getParameter("assessmentType")==null?"":request.getParameter("assessmentType").trim();
		if(assessmentType!=null && assessmentType.equals("3"))
			map.addAttribute("assessmentType", "3");
		else if(assessmentType!=null && assessmentType.equals("4"))
			map.addAttribute("assessmentType", "4");
		
		return "inventorycomplete";
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param map
	 * @param request
	 * @param response
	 * @return {@link String}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/service/json/inventoryData.do", method=RequestMethod.GET)
	public String postInventoryData(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(":::::::::::::: /service/json/inventoryData.do ::::::::::::::");
		request.getSession();
		PrintWriter out = null;
		JSONObject jsonResponse = new JSONObject();
		response.setContentType("application/json");
		boolean isError = true;
		Integer errorCode = 10001;
		String errorMsg = "Invalid URL.";
		String decryptText = null;
		try {
			out =  response.getWriter();
			String id = request.getParameter("id")==null?"":request.getParameter("id").trim().replace(' ', '+');
			try {
				//System.out.println("id : "+id);
				if(id!=null && id!="")
					decryptText = AESEncryption.decrypt(id);
				else
				{
					jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
				}
				System.out.println("decryptText == "+decryptText);
			} catch (Exception e) {
				e.printStackTrace();
				jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
				if(e.getCause() instanceof javax.crypto.BadPaddingException){
					jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
				}
				if(e.getCause() instanceof javax.crypto.IllegalBlockSizeException){
					jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
				}
				return null;
			}
			
			JSONObject jsonObject  = new JSONObject();
			if(decryptText!=null)
				jsonObject = (JSONObject) JSONSerializer.toJSON(decryptText);
			else
			{
				jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
			}
			
			String authKey=null, orgType=null, grpName=null, candidateEmail=null;
			Integer assessmentType=null, assessmentAttemptNo = null;
			
			if(jsonObject.get("authKey")!=null)
				authKey = jsonObject.get("authKey").toString();
			
			if(jsonObject.get("orgType")!=null)
				orgType = jsonObject.get("orgType").toString();
			
			if(jsonObject.get("assessmentType")!=null)
				assessmentType = Integer.parseInt(jsonObject.get("assessmentType").toString());
			
			System.out.println("assessmentAttemptNo :::: "+jsonObject.get("assessmentAttemptNo"));
			if(!jsonObject.get("assessmentAttemptNo").equals("") && jsonObject.get("assessmentAttemptNo")!=null)
				assessmentAttemptNo = Integer.parseInt(jsonObject.get("assessmentAttemptNo").toString());
			
			if(jsonObject.get("grpName")!=null)
				grpName = jsonObject.get("grpName").toString();
			
			if(jsonObject.get("candidateEmail")!=null)
				candidateEmail = jsonObject.get("candidateEmail").toString();
			
			System.out.println("authKey : "+authKey);
			System.out.println("orgType : "+orgType);
			System.out.println("assessmentType : "+assessmentType);
			System.out.println("assessmentAttemptNo : "+assessmentAttemptNo);
			System.out.println("grpName : "+grpName);
			System.out.println("candidateEmail == "+candidateEmail);
			
			if(assessmentAttemptNo==null){
				assessmentAttemptNo=0;
			}
			if(candidateEmail!=null){
				candidateEmail = UtilityAPI.decodeBase64(candidateEmail);
				System.out.println("candidateEmail : "+candidateEmail);
			}
			
			Boolean isValidUser = false;
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request, authKey, orgType);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			
			List<TeacherDetail> lstTDetails = teacherDetailDAO.findByEmail(candidateEmail);
			TeacherDetail teacherDetail = null;
			if(lstTDetails!=null && lstTDetails.size()>0){
				teacherDetail = lstTDetails.get(0);
			}
			if(!isValidUser){
				errorCode=new Integer(mapDistSchDetail.get("errorCode"));
				errorMsg=mapDistSchDetail.get("errorMsg");
			}
			else if(candidateEmail.trim().equals("")){
				errorCode=10002;
				errorMsg="Candidate email address is required.";
			}
			else if(!Utility.validEmailForTeacher(candidateEmail)){
				errorCode=10003;
				errorMsg="Invalid email address.";
			}
			else if(teacherDetail==null){
				errorCode=10004;
				errorMsg="Candidate does not exist in Teachermatch.";
			}
			else if(assessmentType==null || (assessmentType!=null && assessmentType!=3) && (assessmentType!=null && assessmentType!=4)){
				errorCode=10006;
				errorMsg="Invalid Assessment Type.";
			}
			else if(grpName==null || grpName.equals("")){
				errorCode=10007;
				errorMsg="Invalid Group Name.";
			}
			else if(assessmentAttemptNo==null){
				errorCode=10008;
				errorMsg="Invalid Assessment Attempt Number.";
			}
			else if(assessmentType!=null && grpName!=null){
				grpName = UtilityAPI.decodeBase64(grpName);
				System.out.println("grpName == "+grpName);
				List<AssessmentGroupDetails> assessmentGroupDetailList = assessmentGroupDetailsDAO.getAssessmentGroupDetailsListByTypeAndName(assessmentType, grpName);
				if(assessmentGroupDetailList==null || assessmentGroupDetailList.size()==0){
					errorCode=10007;
					errorMsg="Invalid Group Name.";
				}
				else if(assessmentGroupDetailList!=null && assessmentGroupDetailList.size()>0){
					isError=false;
					List<StatusMaster> statusMasterList = Utility.getStaticMasters(new String[]{"comp","vlt"});
					System.out.println("teacherDetail.getTeacherId() : "+teacherDetail.getTeacherId());
					List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.findByTeacherAssessmentTypeAndStatus(teacherDetail, assessmentType, statusMasterList);
					TeacherAssessmentStatus teacherAssessmentStatus = null;
					if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0 && assessmentAttemptNo<=teacherAssessmentStatusList.size()){
						System.out.println("teacherAssessmentStatusList.size() : "+teacherAssessmentStatusList.size());
						if(assessmentAttemptNo==0){
							assessmentAttemptNo = teacherAssessmentStatusList.size();
							teacherAssessmentStatus = teacherAssessmentStatusList.get(teacherAssessmentStatusList.size()-1);
						}
						else
							teacherAssessmentStatus = teacherAssessmentStatusList.get(assessmentAttemptNo-1);
						if(teacherAssessmentStatus!=null){
							System.out.println("teacherAssessmentStatus.getTeacherAssessmentStatusId() : "+teacherAssessmentStatus.getTeacherAssessmentStatusId());
							List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
							teacherDetailList.add(teacherDetail);
							List assessmentDomainData = new ArrayList();
							assessmentDomainData = assessmentDomainScoreDAO.calculateCandidatesNormScore(teacherDetailList, teacherAssessmentStatus.getTeacherAssessmentdetail());
							Integer normscore = 0;
							for(Object oo: assessmentDomainData){
								Object obj[] = (Object[])oo;
								System.out.println("obj[0] : "+obj[0]);
								Double normscoreD = (Double)obj[0];
								normscore = Integer.valueOf((int) Math.round(normscoreD));
							}
							
							JSONArray jsonArrayDomain = new JSONArray();
							List<AssessmentDomainScore> assessmentDomainScoreList = assessmentDomainScoreDAO.getAssessmentDomainScore(teacherDetailList,teacherAssessmentStatus.getAssessmentDetail(),teacherAssessmentStatus.getAssessmentType(),teacherAssessmentStatus.getAssessmentTakenCount());
							for(AssessmentDomainScore assessmentDomainScore : assessmentDomainScoreList){
								JSONObject jsonObjectDomain = new JSONObject();
								jsonObjectDomain.put("domainId", assessmentDomainScore.getDomainMaster().getDomainId());
								jsonObjectDomain.put("domainName", assessmentDomainScore.getDomainMaster().getDomainName());
								jsonObjectDomain.put("score", assessmentDomainScore.getNormscore());
								jsonArrayDomain.add(jsonObjectDomain);
							}
							
							JSONArray jsonArrayCompetency = new JSONArray();
							List<AssessmentCompetencyScore> assessmentCompetencyScoreList = assessmentCompetencyScoreDAO.getAssessmentCompetencyScore(teacherDetailList,teacherAssessmentStatus.getAssessmentDetail(),teacherAssessmentStatus.getAssessmentType(),teacherAssessmentStatus.getAssessmentTakenCount());
							for(AssessmentCompetencyScore assessmentCompetencyScore : assessmentCompetencyScoreList){
								JSONObject jsonObjectCompetency = new JSONObject();
								jsonObjectCompetency.put("competencyId", assessmentCompetencyScore.getCompetencyMaster().getCompetencyId());
								jsonObjectCompetency.put("competencyName", assessmentCompetencyScore.getCompetencyMaster().getCompetencyName());
								jsonObjectCompetency.put("score", assessmentCompetencyScore.getNormscore());
								jsonArrayCompetency.add(jsonObjectCompetency);
							}
							
							jsonResponse.put("candidateEmail",Utility.encodeInBase64(candidateEmail));
							//jsonResponse.put("assessmentType",assessmentType.toString());
							jsonResponse.put("grpName",Utility.encodeInBase64(grpName));
							jsonResponse.put("assessmentAttemptNo",assessmentAttemptNo);
							jsonResponse.put("assessmentStatus",teacherAssessmentStatus.getStatusMaster().getStatus());
							jsonResponse.put("totalRawScore",normscore);
							jsonResponse.put("domainScores", jsonArrayDomain);
							jsonResponse.put("competencyScores", jsonArrayCompetency);
							String pdURL = null;
							//String invitationLink = Utility.getBaseURL(request)+"service/inventory.do?id="+encryptText;
							if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equals("comp")){
								pdURL = Utility.getBaseURL(request)+"service/pdf/inventoryPDP.do?id="+id;
							}
							jsonResponse.put("pdURL", pdURL);
							jsonResponse.put("assessmentCompletedDateTime", teacherAssessmentStatus.getAssessmentCompletedDateTime().toString());
							System.out.println("jsonResponse : "+jsonResponse.toString());
						}
					}
					else{
						errorCode=10008;
						errorMsg="Invalid Assessment Attempt Number.";
						jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
					}
				}
			}
			
			if(isError){
				jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
			}
			
		}catch (Exception e){
			jsonResponse = UtilityAPI.writeJSONError(10017, "Server Error.", e);
			e.printStackTrace();
		}
		finally{
			out.print(jsonResponse);
		}
		return null;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param map
	 * @param request
	 * @param response
	 * @return {@link String}
	 */
	@RequestMapping(value="/service/pdf/inventoryPDP.do", method=RequestMethod.GET)
	public String postIpiPdpReport(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(":::::::::::::: /service/pdf/inventoryPDP.do ::::::::::::::");
		request.getSession();
		ServletContext servletContext = request.getSession().getServletContext();
		
		String fileName = "", basePath = "", urlRootPath = "";
		
		//PrintWriter out = null;
		JSONObject jsonResponse = new JSONObject();
		response.setContentType("application/json");
		boolean isError = true;
		Integer errorCode = 10001;
		String errorMsg = "Invalid URL.";
		String decryptText = null;
		try {
			//out =  response.getWriter();
			String id = request.getParameter("id")==null?"":request.getParameter("id").trim().replace(' ', '+');
			try {
				//System.out.println("id : "+id);
				if(id!=null && id!="")
					decryptText = AESEncryption.decrypt(id);
				else
				{
					jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
				}
				System.out.println("decryptText == "+decryptText);
			} catch (Exception e) {
				e.printStackTrace();
				jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
				if(e.getCause() instanceof javax.crypto.BadPaddingException){
					jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
				}
				if(e.getCause() instanceof javax.crypto.IllegalBlockSizeException){
					jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
				}
				return null;
			}
			
			JSONObject jsonObject  = new JSONObject();
			if(decryptText!=null)
				jsonObject = (JSONObject) JSONSerializer.toJSON(decryptText);
			else
			{
				jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
			}
			
			String authKey=null, orgType=null, grpName=null, candidateEmail=null,pdpType = null;
			Integer assessmentType=null, assessmentAttemptNo = null;
			
			
			if(jsonObject.get("authKey")!=null)
				authKey = jsonObject.get("authKey").toString();
			
			if(jsonObject.get("orgType")!=null)
				orgType = jsonObject.get("orgType").toString();
			
			if(jsonObject.get("assessmentType")!=null)
				assessmentType = Integer.parseInt(jsonObject.get("assessmentType").toString());
			
			System.out.println("assessmentAttemptNo :::: "+jsonObject.get("assessmentAttemptNo"));
			if(!jsonObject.get("assessmentAttemptNo").equals("") && jsonObject.get("assessmentAttemptNo")!=null)
				assessmentAttemptNo = Integer.parseInt(jsonObject.get("assessmentAttemptNo").toString());
			
			if(jsonObject.get("grpName")!=null)
				grpName = jsonObject.get("grpName").toString();
			
			if(jsonObject.get("candidateEmail")!=null)
				candidateEmail = jsonObject.get("candidateEmail").toString();
			
			if(jsonObject.get("pdpType")!=null)
				pdpType =jsonObject.get("pdpType").toString(); 
			
			System.out.println("authKey : "+authKey);
			System.out.println("orgType : "+orgType);
			System.out.println("assessmentType : "+assessmentType);
			System.out.println("assessmentAttemptNo : "+assessmentAttemptNo);
			System.out.println("grpName : "+grpName);
			System.out.println("candidateEmail == "+candidateEmail);
			System.out.println("pdpType= "+pdpType);
			
			if(assessmentAttemptNo==null){
				assessmentAttemptNo=0;
			}
			
			if(candidateEmail!=null){
				candidateEmail = UtilityAPI.decodeBase64(candidateEmail);
				System.out.println("candidateEmail : "+candidateEmail);
			}
			
			Boolean isValidUser = false;
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request, authKey, orgType);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			
			List<TeacherDetail> lstTDetails = teacherDetailDAO.findByEmail(candidateEmail);
			TeacherDetail teacherDetail = null;
			if(lstTDetails!=null && lstTDetails.size()>0){
				teacherDetail = lstTDetails.get(0);
			}
			if(!isValidUser){
				errorCode=new Integer(mapDistSchDetail.get("errorCode"));
				errorMsg=mapDistSchDetail.get("errorMsg");
			}
			else if(candidateEmail.trim().equals("")){
				errorCode=10002;
				errorMsg="Candidate email address is required.";
			}
			else if(!Utility.validEmailForTeacher(candidateEmail)){
				errorCode=10003;
				errorMsg="Invalid email address.";
			}
			else if(teacherDetail==null){
				errorCode=10004;
				errorMsg="Candidate does not exist in Teachermatch.";
			}
			else if(assessmentType==null || (assessmentType!=null && assessmentType!=3) && (assessmentType!=null && assessmentType!=4) && (assessmentType!=null && assessmentType!=1)){
				errorCode=10006;
				errorMsg="Invalid Assessment Type.";
			}
			else if(grpName==null || grpName.equals("")){
				errorCode=10007;
				errorMsg="Invalid Group Name.";
			}
			else if(assessmentAttemptNo==null){
				errorCode=10008;
				errorMsg="Invalid Assessment Attempt Number.";
			}
			else if(pdpType!=null && !pdpType.equals("") && (pdpType.equalsIgnoreCase("IPI") || pdpType.equalsIgnoreCase("EPI") || pdpType.equalsIgnoreCase("SP"))){
				//valid pdpType
			}
			else if(pdpType!=null && !pdpType.equals("")){
				errorCode=10009;
				errorMsg="Invalid PDP Type.";
			}
			else if(assessmentType!=null && grpName!=null){
				grpName = UtilityAPI.decodeBase64(grpName);
				System.out.println("grpName == "+grpName);
				Integer pdptypecall=2;
				Boolean nogrouprequiredflag=true;
				List<AssessmentGroupDetails> assessmentGroupDetailList=null;
				if(pdpType!=null && !pdpType.equals("")){
					List<TeacherAssessmentStatus> teacherAssessmentIPIPdp = new ArrayList<TeacherAssessmentStatus>();
					List<TeacherAssessmentStatus> teacherAssessmentEPIPdp = new ArrayList<TeacherAssessmentStatus>();
					teacherAssessmentIPIPdp = teacherAssessmentStatusDAO.findLatestAssessmentTakenByTeachersIPI(teacherDetail);
					teacherAssessmentEPIPdp = teacherAssessmentStatusDAO.findLatestAssessmentTakenByTeachersEPI(teacherDetail);
					
					if((teacherAssessmentIPIPdp!=null && teacherAssessmentIPIPdp.size()>0)&&(teacherAssessmentEPIPdp!=null&&teacherAssessmentEPIPdp.size()==0))
					{
						System.out.println("Only IPI Assessment Given ***********************");
						if(pdpType.equalsIgnoreCase("IPI")|| pdpType.equalsIgnoreCase("SP"))
						{
							pdptypecall=2; // IPI or SP	
						}
						else if(pdpType!=null && !pdpType.equals("") && pdpType.equalsIgnoreCase("EPI"))
							pdptypecall=1; //EPI
						
					}
					else if((teacherAssessmentEPIPdp!=null&&teacherAssessmentEPIPdp.size()>0)&&(teacherAssessmentIPIPdp!=null && teacherAssessmentIPIPdp.size()==0))
					{	
						System.out.println("Only EPI Assessment Given ***********************");
						if(pdpType.equalsIgnoreCase("EPI")){
							pdptypecall=1;
							nogrouprequiredflag=false;
							assessmentType=1;
						}
						else if(pdpType.equalsIgnoreCase("IPI")|| pdpType.equalsIgnoreCase("SP")){
							pdptypecall=2; 
							assessmentType=1;
							nogrouprequiredflag=false;
						}
					}
					else if((teacherAssessmentEPIPdp!=null&&teacherAssessmentEPIPdp.size()>0)&&(teacherAssessmentIPIPdp!=null && teacherAssessmentIPIPdp.size()>0))
					{	
						System.out.println("Both EPI and IPI  Assessment Given ***********************");
						if(pdpType.equalsIgnoreCase("EPI"))
							pdptypecall=1;
						else if(pdpType.equalsIgnoreCase("IPI")|| pdpType.equalsIgnoreCase("SP"))
							pdptypecall=2;
						
					}
				}
				if(nogrouprequiredflag){
					assessmentGroupDetailList = assessmentGroupDetailsDAO.getAssessmentGroupDetailsListByTypeAndName(assessmentType, grpName);
				}
				if((assessmentGroupDetailList==null || assessmentGroupDetailList.size()==0)&& nogrouprequiredflag){
					errorCode=10007;
					errorMsg="Invalid Group Name.";
				}
				else if(!nogrouprequiredflag ||(assessmentGroupDetailList!=null && assessmentGroupDetailList.size()>0)){
					
					List<StatusMaster> statusMasterList = Utility.getStaticMasters(new String[]{"comp","vlt"});
					System.out.println("teacherDetail.getTeacherId() : "+teacherDetail.getTeacherId());
					
					List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.findByTeacherAssessmentTypeAndStatus(teacherDetail, assessmentType, statusMasterList);
					
					TeacherAssessmentStatus teacherAssessmentStatus = null;
					if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0 && assessmentAttemptNo<=teacherAssessmentStatusList.size()){
						System.out.println("teacherAssessmentStatusList.size() : "+teacherAssessmentStatusList.size());
						if(assessmentAttemptNo==0){
							assessmentAttemptNo = teacherAssessmentStatusList.size();
							teacherAssessmentStatus = teacherAssessmentStatusList.get(teacherAssessmentStatusList.size()-1);
						}
						else
						teacherAssessmentStatus = teacherAssessmentStatusList.get(assessmentAttemptNo-1);
						
						if(teacherAssessmentStatus!=null){
							System.out.println("teacherAssessmentStatus.getTeacherAssessmentStatusId() : "+teacherAssessmentStatus.getTeacherAssessmentStatusId());
							fileName = teacherDetail.getFirstName()+"-"+teacherDetail.getLastName()+"_"+Utility.convertDateAndTimeToDatabaseformatTime(new Date())+".pdf";
							basePath = servletContext.getRealPath("/")+"/candidatereports/"+teacherDetail.getTeacherId()+"/IPIPdReport/";
							urlRootPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
							System.out.println("fileName : "+fileName);
							System.out.println("basePath : "+basePath);
							System.out.println("urlRootPath : "+urlRootPath);
							File file = new File(basePath);
							if(!file.exists())
								 file.mkdirs();
							FileUtils.cleanDirectory(file);
							if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equals("comp")){
								if(pdptypecall==2)
									candidateReportService.generateIPIPDReport(request, teacherAssessmentStatus, basePath, fileName, urlRootPath);
								else
									candidateReportService.generatePDReport(request, teacherDetail, basePath, fileName, urlRootPath);	
								File file2 = new File(basePath+fileName);
								if(file2.exists()){
									isError=false;
									/*response.setContentType("application/octet-stream");
									response.setHeader("Content-Disposition", fileName);
									InputStream inputStream = new FileInputStream(basePath+fileName);
									OutputStream outputStream  = response.getOutputStream();
							        byte[] buffer = new byte[1024];
							        int length;
							        while((length=inputStream.read(buffer))>0){
							        	outputStream.write(buffer,0,length);
							        }
							        outputStream.flush();
							        outputStream.close();
							        inputStream.close();*/
							        
									//response.setContentType("text/html");
									//response.getWriter().print(urlRootPath+"candidatereports/"+teacherDetail.getTeacherId()+"/IPIPdReport/"+fileName);
									System.out.println("response.sendRedirect : "+urlRootPath+"candidatereports/"+teacherDetail.getTeacherId()+"/IPIPdReport/"+fileName);
									response.sendRedirect(urlRootPath+"candidatereports/"+teacherDetail.getTeacherId()+"/IPIPdReport/"+fileName);
							        System.out.println("Success");
								}
							}
							else{
								errorCode=10009;
								errorMsg="Assessment not completed.";
								jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
							}
						}
					}
					else{
						errorCode=10008;
						errorMsg="Invalid Assessment Attempt Number.";
						jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
					}
				}
			}
			
			if(isError){
				jsonResponse = UtilityAPI.writeJSONError(errorCode,errorMsg, null);
			}
			
		}catch (Exception e){
			jsonResponse = UtilityAPI.writeJSONError(10017, "Server Error.", e);
			e.printStackTrace();
		}
		finally{
			//out.print(jsonResponse);
			try {
				if(isError)
					response.getWriter().println(jsonResponse);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}

