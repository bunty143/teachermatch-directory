package tm.api;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import tm.api.UtilityAPI;

public class HireVueTestCase {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPostToHireVue() throws Exception 
	{
		CookieHandler.setDefault(new CookieManager());
		
		String url = "https://app.hirevue.com/api/v1/login/";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
 
		//add request header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", "");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("Content-Type", "application/json");
		 
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put("username", "tdisessa@teachermatch.org");
		jsonResponse.put("password","W4uk3$h4");
		jsonResponse.put("applicationToken", "test_public_token");
		jsonResponse.put("version", "1.2.0");
		
		System.out.println(jsonResponse.toString());
		
		String urlParameters = jsonResponse.toString();

		con.setDoOutput(true);
	    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
		
		System.out.println("Response Code : " + responseCode);
		
		printHTTPResults(con);
		
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		String csfrToken = "";
		// List<String> cookieList = null;
		Map<String, List<String>> headerFieldsMap = null;
		headerFieldsMap = con.getHeaderFields();
		
		for (String header : con.getHeaderFields().keySet())
		{
            if ("csrftoken".equals(header))
            {
				List<String> headerList = con.getHeaderFields().get(header);
				
				for (String headerField : headerList)
				{
					csfrToken = headerField;
				}
            }
//            if ("Set-Cookie".equals(header))
//            {
//            	cookieList = con.getHeaderFields().get(header);				
//            }
		}

// login_response 
		//--header "X-CSRFToken: $CSRF_TOKEN" 
		// --header "Content-Type: application/json
		// "https://app.hirevue.com/api/v1/positions/		
		url = "https://app.hirevue.com/api/v1/positions/";
	    obj = new URL(url);
		con = (HttpsURLConnection) obj.openConnection();
 
		//add request header
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", "");
		con.setRequestProperty("X-CSRFToken", csfrToken);
		
//		for (String header : headerFieldsMap.keySet())
//		{
//			List<String> headerList = headerFieldsMap.get(header);
//
//			for (String cookie : headerList) 
//			{
//				if (null != header)
//			       con.addRequestProperty("Cookie", cookie);
//			}
//		}

		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("Content-Type", "application/json");
		 		
		for (String header : con.getRequestProperties().keySet())
		{
			for (String cookie : con.getRequestProperties().get(header))
			{
				System.out.println("[" + header + "]:[" + cookie + "]");
			}
		}
		
		responseCode = con.getResponseCode();
		
		System.out.println("Response Code : " + responseCode);
		
		//printHTTPResults(con);
		
		
		
		
	}

	private String printHTTPResults(HttpsURLConnection con) {
		BufferedReader in = null; 
		StringBuffer response = new StringBuffer();

		try
		{
				in = new BufferedReader(new InputStreamReader(con.getInputStream()));

				String inputLine;
		 
				while ((inputLine = in.readLine()) != null) 
				{
					response.append(inputLine);
				}
				in.close();
				//print result
		}
		catch (IOException eIO)
		{
		}

		System.out.println("Output: [" + response.toString() + "]");
		
		String returnString = response.toString();
		
		response = new StringBuffer();

		try
		{
			in = new BufferedReader(new InputStreamReader(con.getErrorStream()));

			String inputLine;
	 
			while ((inputLine = in.readLine()) != null) 
			{
				response.append(inputLine);
			}
			in.close();
		}
		catch (Exception e)
		{
		}
		
		System.out.println("Error: [" + response.toString() + "]");
		
		for (String header : con.getHeaderFields().keySet())
		{
			List<String> headerList = con.getHeaderFields().get(header);
			
			System.out.println("header: [" + header + "]");

			for (String headerField : headerList)
			{
				System.out.println("headerField: [" + headerField + "]");
			}
			
		}
		
		
		
		return returnString;
	}
	

}
