package tm.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


public class UtilityAPI {
	
	final static private String charset = "[a-zA-Z0-9_.-]"; // separate this out for future fixes
	final static private String regex = charset + "+@" + charset + "+\\." + charset + "+";

	public static boolean validEmail(String email) {
	    return email.matches(regex);
	}
	public static void main(String[] args) {
			
		//System.out.println(encodeInBase64("saurabh@teachermatch.com"));
		//System.out.println(encodeInBase64("da@teachermatch.com"));
		System.out.println(encodeInBase64("t4@netsutra.com"));
		
	}
	
	public static String getCurrentTimeStamp()
	{
		//SimpleDateFormat smt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		SimpleDateFormat smt = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		return smt.format(new Date());
	}
	public static String getDateTime(Date date)
	{
		SimpleDateFormat smt = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		return smt.format(date);
	}
	private static String readAll(Reader rd) throws IOException 
	{
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}
	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException 
	{
		InputStream is = null;
		try{
			is = new URL(url).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json =(JSONObject) JSONSerializer.toJSON(jsonText);
			return json;
		}
		catch (Exception e){
			return null;
		}
		finally 
		{
			if(is!=null)
				is.close();
			is=null;
		}
	}

	public static JSONArray readJsonArrayFromUrl(String url) throws IOException, JSONException 
	{
		InputStream is = null;
		try 
		{
			is = new URL(url).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONArray jsonArray =(JSONArray) JSONSerializer.toJSON(jsonText);
			return jsonArray;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally 
		{
			if(is!=null)
				is.close();
			is=null;
		}
	}	


	public static String encodeInBase64(String str)
	{
		BASE64Encoder encoder = new BASE64Encoder();
		String encodedBytes = encoder.encodeBuffer(str.getBytes());
		//System.out.println("encodedBytes"+encodedBytes);
		return encodedBytes;
	}
	
	public static String decodeBase64(String str)throws IOException
	{
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] decodedBytes = decoder.decodeBuffer(str);
		return  new String(decodedBytes);
	}

	public static String sendRequestToURL(String urlTOcall){
		String url=urlTOcall;
		InputStream is = null;
		String responseText;
		try 
		{
			is = new URL(url).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			responseText = readAll(rd);
			System.out.println(responseText);

		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally 
		{
			try {
				if(is!=null)
					is.close();
				is=null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return responseText;

	}
	public static String encodeText(String str)
	{
		try {
			str=URLEncoder.encode(str,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return str;
	}
	public static JSONObject writeJSONErrors(Integer errorCode,String errorMessage,Exception e)
	{

		JSONObject jsonOutput=new JSONObject();						
		jsonOutput.put("status",false);
		jsonOutput.put("timestamp",getCurrentTimeStamp());
		jsonOutput.put("errorCode",errorCode);
		jsonOutput.put("errorMessage",errorMessage);
		if(e!=null && e.getMessage()!=null)
			jsonOutput.put("errorMessageDetails",e.getMessage());

		return jsonOutput;
	}
	
	public static JSONObject writeJSONError(Integer errorCode,String errorMessage,Exception e)
	{

		JSONObject jsonOutput=new JSONObject();						
		jsonOutput.put("errorCode",errorCode);
		jsonOutput.put("errorMessage",errorMessage);
		if(e!=null && e.getMessage()!=null)
			jsonOutput.put("errorMessageDetails",e.getMessage());

		return jsonOutput;
	}

	public static void writeXMLErrors(Integer errorCode,String errorMessage,Exception e, Document doc, Element root)
	{
		Element eleStatus = doc.createElement("status");
		root.appendChild(eleStatus);
		Text textStatus = doc.createTextNode("false");
		eleStatus.appendChild(textStatus);
		
		Element timeStamp = doc.createElement("timeStamp");
		root.appendChild(timeStamp);
		Text timeStampResult = doc.createTextNode(getCurrentTimeStamp());
		timeStamp.appendChild(timeStampResult);
		
		Element eleErrorCode= doc.createElement("errorCode");
		root.appendChild(eleErrorCode);
		Text textErrorCode= doc.createTextNode(""+errorCode);
		eleErrorCode.appendChild(textErrorCode);
		
		Element eleErrorMsg = doc.createElement("errorMessage");
		root.appendChild(eleErrorMsg);
		Text textErrorMsg = doc.createTextNode(""+errorMessage);
		eleErrorMsg.appendChild(textErrorMsg);
		
		if(e!=null && e.getMessage()!=null){
			Element eleExMsg = doc.createElement("errorMessageDetails");
			root.appendChild(eleExMsg);
			Text textExMsg = doc.createTextNode(""+e.getMessage());
			eleExMsg.appendChild(textExMsg);
		}			
	}
	
	public static String  parseString(String str){
		str = (str==null)?"":str;
		String str1="";		
		str1=str.replaceAll("'","''");		
		str1=str1.replaceAll("<","&lt;");
		str1=str1.replaceAll(">","&gt;");
		str1 = str1.replaceAll("[\\[\\](){}]","");
		str1=str1.replaceAll("\"","");
		//System.out.println(str1);
		return str1.trim();
	}
	public static String  parseString2(String str){
		str = (str==null)?"":str;
		String str1="";		
		str1=str.replaceAll("\n","<br>");		
		return str1.trim();
	}
	
	public static int parseString(int value){
		return value;
	}
	public static double parseString(double value){
		return value;
	}


}
