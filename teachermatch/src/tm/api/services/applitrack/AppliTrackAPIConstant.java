package tm.api.services.applitrack;

public class AppliTrackAPIConstant 
{
	public static String sAPPLITRACKAPI_HOST="www.applitrack.com";
	public static int sAPPLITRACKAPI_PORT=443;
	//public static String sAPPLITRACKAPI_REALM="devdallas AppliTrack API";
	public static String sAPPLITRACKAPI_REALM="dallasisd AppliTrack API";
	public static String sAPPLITRACKAPI_USERNAME="teachermatch";
	public static String sAPPLITRACKAPI_PASSWORD="T34ch3rm@tchAP1";
	
	//Dev URL
	//public static String sAPPLITRACKAPI_URL="https://www.applitrack.com/devdallas/api/applitrackapi.svc/";
	//Production URL
	//public static String sAPPLITRACKAPI_URL="https://www.applitrack.com/dallasisd/applitrackapi.svc/";
	public static String sAPPLITRACKAPI_URL="https://www.applitrack.com/dallasisd/api/applitrackapi.svc/";
	
	public static String sAPPLITRACKAPI_DEFAULTJOBCATEGORY="Teacher";
	
}
