package tm.api.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import tm.api.UtilityAPI;
import tm.bean.EmployeeMaster;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPreference;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.controller.teacher.BasicController;
import tm.dao.EmployeeMasterDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class ServiceUtility{
	
	String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
		this.teacherDetailDAO = teacherDetailDAO;
	}
//teacherPersonalInfoDAO
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	@Autowired 
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) {
		this.emailerService = emailerService;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}

	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;

	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;

	@Autowired
	private BasicController basicController;

	@Autowired
	private RoleMasterDAO roleMasterDAO;
	
	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;

	public Map<String, String> authDistOrSchool(HttpServletRequest request){

		Map<String, String> mapOrgDetail = new HashMap<String, String>();
		Integer errorCode=0;
		String errorMsg="";	
		boolean isValidUser = false;
		try {			
			HttpSession session = request.getSession();
			// intitilise here
			session.setAttribute("teacherDetail",null);
			session.setAttribute("userMaster",null);
			
			String authKey = request.getParameter("authKey")==null?"":request.getParameter("authKey").replace(' ', '+');
			String orgType = request.getParameter("orgType")==null?"":request.getParameter("orgType").trim();
			System.out.println("authKey>"+authKey);
			if(orgType.equalsIgnoreCase("D")){
				DistrictMaster districtMaster =  districtMasterDAO.validateDistrictByAuthkey(authKey);
				System.out.println(districtMaster);
				if(districtMaster == null){					
					isValidUser = false;
					errorCode=10001;
					errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
				}
				else if(districtMaster.getStatus().equalsIgnoreCase("I")){
					isValidUser = false;
					errorCode=10009;
					errorMsg=Utility.getLocaleValuePropByKey("msgInactivated3", locale);	
				}
				else {
					session.setAttribute("apiDistrictMaster", districtMaster);
					System.out.println("session.getId()::: "+session.getId());
					isValidUser = true;					
				}
			}
			else if(orgType.equalsIgnoreCase("S")){
				SchoolMaster schoolMaster = schoolMasterDAO.validateSchoolByAuthkey(authKey);

				if(schoolMaster == null){
					isValidUser = false;
					errorCode=10001;
					errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
				}
				else if(schoolMaster.getStatus().equalsIgnoreCase("I")){
					isValidUser = false;
					errorCode=10009;
					errorMsg=Utility.getLocaleValuePropByKey("msgInactivated3", locale);	
				}
				else{
					session.setAttribute("apiSchoolMaster", schoolMaster);
					session.setAttribute("apiDistrictMaster", schoolMaster.getDistrictId());
					isValidUser = true;			
				}
			}
			else{
				isValidUser = false;		
				errorCode = 10001;
				errorMsg = Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
			}

			if(isValidUser){
				mapOrgDetail.put("isValidUser", "true");
			}
			else{
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
			}
		} 
		catch (Exception e){			
			e.printStackTrace();

			mapOrgDetail.put("isValidUser", "false");
		}
		return mapOrgDetail;	
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param request
	 * @param authKey
	 * @param orgType
	 * @return {@link Map}
	 */
	public Map<String, String> authDistOrSchool(HttpServletRequest request, String authKey, String orgType){

		Map<String, String> mapOrgDetail = new HashMap<String, String>();
		Integer errorCode=0;
		String errorMsg="";
		boolean isValidUser = false;
		try {
			HttpSession session = request.getSession();

			if(authKey!=null && orgType!=null)
				if(orgType.equalsIgnoreCase("D")){
					DistrictMaster districtMaster = districtMasterDAO.validateDistrictByAuthkey(authKey);
					if(districtMaster == null){
						isValidUser = false;
						errorCode=10001;
						errorMsg="Invalid Credentials.";
					}
					else if(districtMaster.getStatus().equalsIgnoreCase("I")){
						isValidUser = false;
						errorCode=10009;
						errorMsg="District/School is inactivated.";
					}
					else {
						session.setAttribute("apiDistrictMaster", districtMaster);
						isValidUser = true;
					}
				}
				else if(orgType.equalsIgnoreCase("S")){
					SchoolMaster schoolMaster = schoolMasterDAO.validateSchoolByAuthkey(authKey);
					if(schoolMaster == null){
						isValidUser = false;
						errorCode=10001;
						errorMsg="Invalid Credentials.";
					}
					else if(schoolMaster.getStatus().equalsIgnoreCase("I")){
						isValidUser = false;
						errorCode=10009;
						errorMsg="District/School is inactivated.";
					}
					else{
						session.setAttribute("apiSchoolMaster", schoolMaster);
						session.setAttribute("apiDistrictMaster", schoolMaster.getDistrictId());
						isValidUser = true;
					}
				}
				else{
					isValidUser = false;
					errorCode = 10001;
					errorMsg = "Invalid Credentials.";
				}

			if(isValidUser){
				mapOrgDetail.put("isValidUser", "true");
			}
			else{
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
			}
		}
		catch (Exception e){			
			e.printStackTrace();

			mapOrgDetail.put("isValidUser", "false");
		}
		return mapOrgDetail;	
	}

	public Map<String, String> authUser(HttpServletRequest request){
		Map<String, String> mapUser = new HashMap<String, String>();
		try {
			HttpSession session  = request.getSession();
			UserMaster userMaster = null;
			Boolean responseStatus = false;
			Integer errorCode=0;
			String errorMsg="";	
			DistrictMaster dm = (DistrictMaster) session.getAttribute("apiDistrictMaster");
			String userEmail = request.getParameter("userEmail")==null?"":request.getParameter("userEmail").replace(' ', '+');;
			if(!userEmail.trim().equals("")){
				userEmail = Utility.decodeBase64(userEmail);
				List<UserMaster> lstUserMaster = userMasterDAO.findByEmail(userEmail);			
				System.out.println("lstUserMaster"+lstUserMaster);
				if (lstUserMaster!=null && lstUserMaster.size()>0){
					userMaster = lstUserMaster.get(0);	
					//System.out.println("userDistrict"+userMaster.getDistrictId().getDistrictId());
					//System.out.println("District"+dm.getDistrictId());
				}
			}


			if(userEmail.equals("")){
				responseStatus=false;
				errorCode = 10002;
				errorMsg = Utility.getLocaleValuePropByKey("msgUserEmailAddress", locale);
			}
			else if(!UtilityAPI.validEmail(userEmail)){
				responseStatus=false;
				errorCode = 10003;
				errorMsg = Utility.getLocaleValuePropByKey("msgInvalidUserEmailAddress", locale);
			}	
			else if(userMaster==null){
				responseStatus=false;
				errorCode = 10004;
				errorMsg = Utility.getLocaleValuePropByKey("msgUserNotExist", locale);
			}
			else if(!userMaster.getStatus().equals("A")){
				responseStatus=false;
				errorCode = 10009;
				errorMsg = Utility.getLocaleValuePropByKey("msgCandidateInactivated", locale);
			}
			else if(userMaster.getDistrictId()==null){
				System.out.println(">>");
				responseStatus=false;
				errorCode = 10010;
				errorMsg = Utility.getLocaleValuePropByKey("msgUserDistrictSchool", locale);
			}
			else if(!userMaster.getDistrictId().equals(dm)){
				System.out.println(">>>>>>>"+dm.getDistrictId());
				System.out.println(userMaster.getDistrictId().getDistrictId());
				responseStatus=false;
				errorCode = 10010;
				errorMsg = Utility.getLocaleValuePropByKey("msgUserDistrictSchool1", locale);
			}			
			else{
				session.setAttribute("userMasterAPI", userMaster);
				responseStatus=true;
			}

			if(responseStatus){
				mapUser.put("isSuccess","true");
			}
			else{
				mapUser.put("isSuccess","false");				
				mapUser.put("errorCode",""+errorCode);
				mapUser.put("errorMsg",errorMsg);
			}

		} 
		catch (Exception e) {
			System.out.println("**************");
			e.printStackTrace();
		}		
		return mapUser;
	}

	public Map<String, String> validateTeacher(HttpServletRequest request){
		Map<String, String> mapTeacher = new HashMap<String, String>();
		try {			
			String candidateFirstName = request.getParameter("candidateFirstName")==null?"":request.getParameter("candidateFirstName").trim();
			String candidateLastName = request.getParameter("candidateLastName")==null?"":request.getParameter("candidateLastName").trim();		
			String candidateEmail = request.getParameter("candidateEmail")==null?"":request.getParameter("candidateEmail").trim().replace(' ', '+');

			if(!candidateEmail.equals("")){
				candidateEmail = UtilityAPI.decodeBase64(candidateEmail);
			}

			System.out.println("candidateEmail+++"+candidateEmail);

			int authorizationkey=(int) Math.round(Math.random() * 2000000);
			TeacherDetail teacherDetail = null;

			Boolean responseStatus = false;
			Integer errorCode=0;
			String errorMsg="";	

			HttpSession session = request.getSession();

			List<UserMaster> lstUserMasters = userMasterDAO.findByEmail(candidateEmail);
			UserMaster userMaster = null;
			if(lstUserMasters!=null && lstUserMasters.size()>0){
				userMaster = lstUserMasters.get(0);
			}


			if(candidateFirstName.equals("")){
				responseStatus=false;
				errorCode = 10002;
				errorMsg = Utility.getLocaleValuePropByKey("msgCandidateFirstNameRequired", locale);
			}
			else if(candidateLastName.equals("")){
				responseStatus=false;
				errorCode = 10002;
				errorMsg = Utility.getLocaleValuePropByKey("msgCandidateLastNameRequired", locale);
			}
			else if(candidateEmail.equals("")){
				responseStatus=false;
				errorCode = 10002;
				errorMsg = Utility.getLocaleValuePropByKey("msgCanEmailReq", locale);
			}
			else if(!Utility.validEmailForTeacher(candidateEmail)){
				responseStatus=false;
				errorCode = 10003;
				errorMsg = Utility.getLocaleValuePropByKey("msgInvalidCandidateEmailAddress", locale);
			}
			/*else if(userMaster!=null){
				responseStatus=false;
				errorCode = 10005;
				errorMsg = "Candidate with this email address already exist.";
			}*/
			else{				
				List<TeacherDetail> lstTDetail = teacherDetailDAO.findByEmail(candidateEmail);				
				if(lstTDetail.size()>0){
					System.out.println("fatch existing teacher>>");
					teacherDetail = lstTDetail.get(0);
				}

				if(teacherDetail==null){
					responseStatus=true;
					System.out.println("Save teacher");
					teacherDetail = new TeacherDetail();
					teacherDetail.setFirstName(candidateFirstName);
					teacherDetail.setLastName(candidateLastName);
					teacherDetail.setEmailAddress(candidateEmail);
					teacherDetail.setUserType("R");
					teacherDetail.setPassword(MD5Encryption.toMD5("Te@cherMatch"));							
					teacherDetail.setFbUser(false);
					teacherDetail.setQuestCandidate(0);
					teacherDetail.setAuthenticationCode(""+authorizationkey);
					teacherDetail.setVerificationCode(""+authorizationkey);
					teacherDetail.setVerificationStatus(0);				
					teacherDetail.setNoOfLogin(0);
					teacherDetail.setIsPortfolioNeeded(true);
					teacherDetail.setIsResearchTeacher(false);
					teacherDetail.setForgetCounter(0);
					teacherDetail.setStatus("A");
					teacherDetail.setSendOpportunity(false);
					teacherDetail.setCreatedDateTime(new Date());
					teacherDetail.setInternalTransferCandidate(false);
					teacherDetailDAO.makePersistent(teacherDetail);	

					try {
						DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailcontent(MailText.getRegistrationMail(request,teacherDetail));
						dsmt.setMailto(teacherDetail.getEmailAddress());
						dsmt.setMailsubject(Utility.getLocaleValuePropByKey("msgWelcomeTeacherMatchAccount", locale));
						dsmt.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
					//emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Welcome to TeacherMatch, please confirm your account",MailText.getRegistrationMail(request,teacherDetail));


				}
				else if(!teacherDetail.getStatus().equals("A")){
					responseStatus=false;
					errorCode = 10009;
					errorMsg = Utility.getLocaleValuePropByKey("msgCandidateInactivated", locale);
				}				
				else{
					responseStatus=true;
				}
			}

			if(responseStatus){
				System.out.println("ttt");
				session.setAttribute("apiTeacherDetail", teacherDetail);
				mapTeacher.put("tCreatedOrExist","true");
			}
			else{
				mapTeacher.put("tCreatedOrExist","false");				
				mapTeacher.put("errorCode",""+errorCode);
				mapTeacher.put("errorMsg",errorMsg);
			}
		}
		catch (Exception e){			
			e.printStackTrace();
			mapTeacher.put("tCreatedOrExist","false");
		}		

		return mapTeacher;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param request
	 * @param candidateFirstName
	 * @param candidateLastName
	 * @param candidateEmail
	 * @return {@link Map}
	 */
	public Map<String, String> validateTeacher(HttpServletRequest request, String candidateFirstName, String candidateLastName, String candidateEmail, Integer assessmentType){
		Map<String, String> mapTeacher = new HashMap<String, String>();
		try {

			if(candidateEmail!=null && !candidateEmail.equals(""))
				candidateEmail = UtilityAPI.decodeBase64(candidateEmail);

			System.out.println("candidateEmail == "+candidateEmail);

			int authorizationkey=(int) Math.round(Math.random() * 2000000);
			TeacherDetail teacherDetail = null;

			Boolean responseStatus = false;
			Integer errorCode=0;
			String errorMsg="";	

			HttpSession session = request.getSession();

			/*List<UserMaster> lstUserMasters = userMasterDAO.findByEmail(candidateEmail);
			UserMaster userMaster = null;
			if(lstUserMasters!=null && lstUserMasters.size()>0){
				userMaster = lstUserMasters.get(0);
			}*/

			if(candidateFirstName==null || candidateFirstName.equals("")){
				responseStatus=false;
				errorCode = 10002;
				errorMsg = "Candidate First Name is required.";
			}
			else if(candidateLastName==null || candidateLastName.equals("")){
				responseStatus=false;
				errorCode = 10002;
				errorMsg = "Candidate Last Name is required.";
			}
			else if(candidateEmail==null || candidateEmail.equals("")){
				responseStatus=false;
				errorCode = 10002;
				errorMsg = "Candidate email address is required.";
			}
			else if(!Utility.validEmailForTeacher(candidateEmail)){
				responseStatus=false;
				errorCode = 10003;
				errorMsg = "Invalid Candidate email address.";
			}
			/*else if(userMaster!=null){
				responseStatus=false;
				errorCode = 10005;
				errorMsg = "Candidate with this email address already exist.";
			}*/
			else{				
				List<TeacherDetail> lstTDetail = teacherDetailDAO.findByEmail(candidateEmail);				
				if(lstTDetail.size()>0){
					System.out.println(" ::: fatch existing teacher ::: ");
					teacherDetail = lstTDetail.get(0);
				}

				if(teacherDetail==null){
					responseStatus=true;
					System.out.println(" ::: new teacher insert ::: ");
					teacherDetail = new TeacherDetail();
					teacherDetail.setFirstName(candidateFirstName);
					teacherDetail.setLastName(candidateLastName);
					teacherDetail.setEmailAddress(candidateEmail);
					teacherDetail.setUserType("R");
					teacherDetail.setPassword(MD5Encryption.toMD5("Te@cherMatch"));							
					teacherDetail.setFbUser(false);
					teacherDetail.setQuestCandidate(0);
					teacherDetail.setAuthenticationCode(""+authorizationkey);
					teacherDetail.setVerificationCode(""+authorizationkey);
					teacherDetail.setVerificationStatus(0);				
					teacherDetail.setNoOfLogin(0);
					teacherDetail.setIsPortfolioNeeded(true);
					teacherDetail.setIsResearchTeacher(false);
					teacherDetail.setForgetCounter(0);
					teacherDetail.setStatus("A");
					teacherDetail.setSendOpportunity(false);
					teacherDetail.setCreatedDateTime(new Date());
					teacherDetail.setInternalTransferCandidate(false);
					teacherDetailDAO.makePersistent(teacherDetail);	

					if(assessmentType!=null && assessmentType!=4){
						try {
							DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
							dsmt.setEmailerService(emailerService);
							dsmt.setMailcontent(MailText.getRegistrationMail(request,teacherDetail));
							dsmt.setMailto(teacherDetail.getEmailAddress());
							dsmt.setMailsubject("Welcome to TeacherMatch, please confirm your account");
							dsmt.start();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					//emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Welcome to TeacherMatch, please confirm your account",MailText.getRegistrationMail(request,teacherDetail));
				}
				else if(!teacherDetail.getStatus().equals("A")){
					responseStatus=false;
					errorCode = 10009;
					errorMsg = "Candidate is inactivated.";
				}				
				else{
					responseStatus=true;
				}
			}

			if(responseStatus){
				session.setAttribute("apiTeacherDetail", teacherDetail);
				mapTeacher.put("tCreatedOrExist","true");
			}
			else{
				mapTeacher.put("tCreatedOrExist","false");				
				mapTeacher.put("errorCode",""+errorCode);
				mapTeacher.put("errorMsg",errorMsg);
			}
		}
		catch (Exception e){			
			e.printStackTrace();
			mapTeacher.put("tCreatedOrExist","false");
		}		

		return mapTeacher;
	}

	public void setEpiStatus(HttpServletRequest request){

		try {
			String emailAddress = "";
			HttpSession session = request.getSession();
			SchoolMaster schoolMaster =  null;
			DistrictMaster districtMaster = null;
			String redirectURl = "";
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("apiTeacherDetail");
			TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);

			if(session.getAttribute("apiSchoolMaster")!=null){
				schoolMaster = (SchoolMaster) session.getAttribute("apiSchoolMaster");
				redirectURl = schoolMaster.getApiRedirectUrl(); 
			}
			else if(session.getAttribute("apiDistrictMaster")!=null){
				districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
				redirectURl = districtMaster.getApiRedirectUrl();
			}
			if(redirectURl!=null && (!redirectURl.trim().equals(""))){				
				emailAddress = teacherDetail.getEmailAddress().replaceAll("\\r\\n|\\r|\\n", "");
				emailAddress = emailAddress.substring(0,emailAddress.length()-1);
				redirectURl=redirectURl+"?candidateEmail="+Utility.encodeInBase64(emailAddress)+"&epiStatus="+teacherAssessmentStatus.getStatusMaster().getStatus();
				System.out.println(redirectURl);				
				Utility.sendRequestToURL(redirectURl);
			}			
		} 
		catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/*public Map<String, String> validateEmployee(HttpServletRequest request){
		Map<String, String> mapTeacher = new HashMap<String, String>();
		try {			
			String employeeCode = request.getParameter("employeeCode")==null?"":request.getParameter("employeeCode").trim();
			String locationCode = request.getParameter("locationCode")==null?"":request.getParameter("locationCode").trim();		

			System.out.println("locationCode+++"+locationCode);

			Boolean responseStatus = false;
			Integer errorCode=0;
			String errorMsg="";	

			if(employeeCode.equals("")){
				responseStatus=false;
				errorCode = 10002;
				errorMsg = "employeeCode is required.";
			}
			if(locationCode.equals("")){
				responseStatus=false;
				errorCode = 10002;
				errorMsg = "locationCode is required.";
			}

			HttpSession session = request.getSession();

			UserMaster userMaster = null;
			SchoolMaster schoolMaster =null;


			if(!locationCode.equals("")){
			boolean checkUser = false;	
				if(locationCode.equals("123456"))
					checkUser = true;	
				else
				{
					schoolMaster = schoolMasterDAO.checkSchoolByLocationCode(locationCode);
					if(schoolMaster==null)
					{
						responseStatus=false;
						errorCode = 10002;
						errorMsg = "Invalid locationCode.";
					}else
						checkUser = true;	
				}

				if(checkUser)
				{
					List<UserMaster> lstUserMasters = userMasterDAO.getUserByEmployeeCode(null,schoolMaster,employeeCode);
					if(lstUserMasters!=null && lstUserMasters.size()>0){
						userMaster = lstUserMasters.get(0);
					}
				}
			}

			if(userMaster!=null){
				responseStatus=false;
				errorCode = 10005;
				errorMsg = "Candidate with this email address already exist.";
			}
			else{				
				responseStatus=true;
			}

			if(responseStatus){
				System.out.println("ttt");
				session.setAttribute("apiTeacherDetail", null);
				mapTeacher.put("tCreatedOrExist","true");
			}
			else{
				mapTeacher.put("tCreatedOrExist","false");				
				mapTeacher.put("errorCode",""+errorCode);
				mapTeacher.put("errorMsg",errorMsg);
			}
		}
		catch (Exception e){			
			e.printStackTrace();
			mapTeacher.put("tCreatedOrExist","false");
		}		

		return mapTeacher;
	}*/

	public Map<String, String> authDistOrSchoolUsingLocationCode(HttpServletRequest request){

		Map<String, String> mapOrgDetail = new HashMap<String, String>();
		Integer errorCode=0;
		String errorMsg="";	
		boolean isValidUser = false;
		Integer roleId = 1;
		try {			
			HttpSession session = request.getSession();
			// intitilise here
			session.setAttribute("teacherDetail",null);
			session.setAttribute("userMaster",null);
			
			String authKey = request.getParameter("authKey")==null?"":request.getParameter("authKey").replace(' ', '+');
			String employeeCode = request.getParameter("employeeCode")==null?"":request.getParameter("employeeCode").trim();
			String locationCode = request.getParameter("locationCode")==null?"":request.getParameter("locationCode").trim();		

			System.out.println("employeeCode:: "+employeeCode);

			System.out.println("locationCode+++"+locationCode);
			System.out.println("authKey>"+authKey);
			if(authKey.equals("")){
				isValidUser = false;
				errorCode = 10002;
				errorMsg = Utility.getLocaleValuePropByKey("msgAuthKeyRequired", locale);
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
				return mapOrgDetail;
			}
			if(employeeCode.equals("")){
				isValidUser = false;
				errorCode = 10002;
				errorMsg = Utility.getLocaleValuePropByKey("msgEmployeeCodeRequired", locale);
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
				return mapOrgDetail;
			}
			if(locationCode.equals("")){
				isValidUser = false;
				errorCode = 10002;
				errorMsg = Utility.getLocaleValuePropByKey("msgLocationCodeRequired", locale);
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
				return mapOrgDetail;
			}

			UserMaster userMaster = null;
			SchoolMaster schoolMaster =null;

			if(!employeeCode.equals("")){
				DistrictMaster districtMaster =  districtMasterDAO.validateDistrictByAuthkey(authKey);
				System.out.println("districtMaster: "+districtMaster);
				if(districtMaster == null){					
					isValidUser = false;
					errorCode=10001;
					errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
				}
				else if(districtMaster.getStatus().equalsIgnoreCase("I")){
					isValidUser = false;
					errorCode=10009;
					//errorMsg=Utility.getLocaleValuePropByKey("msgInactivated3", locale);
					errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
				}
				else {
					boolean checkUser = false;	
					//String districtLoctionCode = districtMaster.getLocationCode()==null?"":districtMaster.getLocationCode();
					//if(locationCode.equalsIgnoreCase(districtLoctionCode))

					if(locationCode.equalsIgnoreCase("9999"))
						checkUser = true;	
					else
					{
						schoolMaster = schoolMasterDAO.checkSchoolByLocationCode(locationCode);
						System.out.println("schoolMaster: "+schoolMaster);
						if(schoolMaster==null)
						{
							isValidUser = false;
							errorCode = 10002;
							//errorMsg = "Invalid Location Code.";
							errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
						}else
							checkUser = true;	
					}
					System.out.println("checkUser:::: "+checkUser);
					//checkUser=true;
					if(checkUser)
					{
						EmployeeMaster employeeMaster =  employeeMasterDAO.checkEmployeeByEmployeeCode(employeeCode,districtMaster);
						//System.out.println("employeeMasterID::: "+employeeMaster.getEmployeeId());
						if(employeeMaster!=null)
						{
							//List<UserMaster> lstUserMasters = userMasterDAO.getUserByEmployeeCode(districtMaster,schoolMaster,employeeMaster);
							List<UserMaster> lstUserMasters = userMasterDAO.getUserByEmployeeCode(districtMaster,employeeMaster);
							List<UserMaster> lstUserMastersDA = new ArrayList<UserMaster>();
							System.out.println("lstUserMaster::::: "+lstUserMasters.size());
							if(lstUserMasters!=null && lstUserMasters.size()>0){
								//userMaster = lstUserMasters.get(0);
								boolean isSA=false;
								for (UserMaster userMaster2 : lstUserMasters) {
									schoolMaster = userMaster2.getSchoolId();
									if(schoolMaster!=null)
									{
										if(schoolMaster.getLocationCode().equalsIgnoreCase(locationCode))
										{
											userMaster = userMaster2;
											isSA=true;
										}
									}else
										lstUserMastersDA.add(userMaster2);
								}
								System.out.println("lstUserMastersDA::: "+lstUserMastersDA.size());
								if(isSA==false && lstUserMastersDA.size()==0)
								{
									errorCode = 10002;
									//errorMsg = "Invalid Employee.";
									errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
									isValidUser = false;
									userMaster=null;
								}else if(isSA==false && lstUserMastersDA.size()>0)
								{
									userMaster=null;
									if(locationCode.equalsIgnoreCase("9999"))
										userMaster = lstUserMastersDA.get(0);
								}
								if(userMaster!=null)
								{
									int entityType = userMaster.getEntityType();
									if(entityType==3 && locationCode.equalsIgnoreCase("9999"))
									{
										errorCode = 10002;
										//errorMsg = "Invalid Employee.";
										errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
										isValidUser = false;

									}else
									{
										if(userMaster.getEntityType()==2)
											userMaster.setSchoolId(null);
										roleId = userMaster.getRoleId().getRoleId();
										System.out.println("userMaster.getUserId()::: "+userMaster.getUserId());
										session.setAttribute("userMaster", userMaster);
										session.setAttribute("lstMenuMaster", roleAccessPermissionDAO.getMenuList(request,userMaster));

										if(userMaster.getEntityType()==2)//District
										{
											int responseFolder =	 basicController.createHomeFolder(request);
											if(userMaster.getEntityType()!=1)
												basicController.createDistrictStatusHome(request);
										}
										else if(userMaster.getEntityType()==3)//School
										{
											int responseFolder =	 basicController.createHomeFolder(request);
											if(userMaster.getEntityType()!=1)
												basicController.createDistrictStatusHome(request);
										}
										isValidUser = true;		
									}
								}else
								{
									errorCode = 10002;
									//errorMsg = "Invalid Employee.";
									errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
									isValidUser = false;
								}

							}else
							{
								errorCode = 10002;
								//errorMsg = "Invalid Employee.";
								errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
								isValidUser = false;
							}

						}else
						{
							isValidUser = false;
							errorCode = 10002;
							//errorMsg = "Invalid Employee.";
							errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
							isValidUser = false;		
						}

					}


				}
			}
			else{
				isValidUser = false;		
				errorCode = 10001;
				errorMsg = Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
			}

			if(isValidUser){
				mapOrgDetail.put("isValidUser", "true");
				mapOrgDetail.put("userRole", ""+roleId);
			}
			else{
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
			}
		} 
		catch (Exception e){			
			e.printStackTrace();

			mapOrgDetail.put("isValidUser", "false");
		}
		return mapOrgDetail;	
	}
	public static String getBody(HttpServletRequest request) throws IOException {

		String body = null;
		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader bufferedReader = null;

		try {
			InputStream inputStream = request.getInputStream();
			// BufferedReader r = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
			if (inputStream != null) {
				//  bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
				bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
				char[] charBuffer = new char[128];
				int bytesRead = -1;
				while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
					stringBuilder.append(charBuffer, 0, bytesRead);
				}
				/* String s ="";
	            while(bufferedReader.readLine()!=null)
	            {
	            	 stringBuilder.append(bufferedReader.readLine());
	            }*/
			} else {
				stringBuilder.append("");
			}
		} catch (IOException ex) {
			throw ex;
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException ex) {
					throw ex;
				}
			}
		}

		body = stringBuilder.toString();
		return body;
	}

	public Map<String, String> authDistOrSchoolUsingEmail(HttpServletRequest request){

		Map<String, String> mapOrgDetail = new HashMap<String, String>();
		Integer errorCode=0;
		String errorMsg="";	
		boolean isValidUser = false;
		Integer roleId = 1;
		try {			
			HttpSession session = request.getSession();
			String authKey = request.getParameter("authKey")==null?"":request.getParameter("authKey").replace(' ', '+');
			String firstName = request.getParameter("firstName")==null?"":request.getParameter("firstName").trim();
			String lastName = request.getParameter("lastName")==null?"":request.getParameter("lastName").trim();
			String email = request.getParameter("userEmail")==null?"":request.getParameter("userEmail").trim().replace(' ', '+');
			String role = request.getParameter("role")==null?"":request.getParameter("role").trim();

			System.out.println("email:: "+email);

			System.out.println("firstName+++"+firstName);
			System.out.println("authKey>"+authKey);
			if(authKey.equals("")){
				isValidUser = false;
				errorCode = 10002;
				errorMsg = Utility.getLocaleValuePropByKey("msgAuthKeyRequired", locale);
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
				return mapOrgDetail;
			}
			if(email.equals("")){
				isValidUser = false;
				errorCode = 10003;
				errorMsg = Utility.getLocaleValuePropByKey("msgEmailRequired", locale);
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
				return mapOrgDetail;
			}
			if(firstName.equals("")){
				isValidUser = false;
				errorCode = 10004;
				errorMsg = Utility.getLocaleValuePropByKey("msgFirstNameRequired", locale);
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
				return mapOrgDetail;
			}if(lastName.equals("")){
				isValidUser = false;
				errorCode = 10005;
				errorMsg = Utility.getLocaleValuePropByKey("msgLastNameRequired", locale);
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
				return mapOrgDetail;
			}if(role.equals("")){
				isValidUser = false;
				errorCode = 10006;
				errorMsg = Utility.getLocaleValuePropByKey("msgRoleRequired", locale);
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
				return mapOrgDetail;
			}if(!(role.equals("2") || role.equals("5") || role.equals("7") || role.equals("8"))){
				isValidUser = false;
				errorCode = 10007;
				errorMsg = Utility.getLocaleValuePropByKey("msgInvalidRole", locale);
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
				return mapOrgDetail;
			}


			UserMaster userMaster = null;

			if(!email.equals("")){
				DistrictMaster districtMaster =  districtMasterDAO.validateDistrictByAuthkey(authKey);
				System.out.println("districtMaster: "+districtMaster);
				if(districtMaster == null){					
					isValidUser = false;
					errorCode=10001;
					errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
				}
				else if(districtMaster.getStatus().equalsIgnoreCase("I")){
					isValidUser = false;
					errorCode=10009;
					//errorMsg=Utility.getLocaleValuePropByKey("msgInactivated3", locale);
					errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
				}
				else {
					List<UserMaster> lstUserMasters = new ArrayList<UserMaster>();

					if(!email.trim().equals("")){
						String useremail = Utility.decodeBase64(email);
						lstUserMasters = userMasterDAO.findByEmail(useremail);			
						System.out.println("lstUserMaster"+lstUserMasters);
						if (lstUserMasters!=null && lstUserMasters.size()>0){
							userMaster = lstUserMasters.get(0);	

							System.out.println("role::: "+role);

							//System.out.println("userDistrict"+userMaster.getDistrictId().getDistrictId());
							//System.out.println("District"+dm.getDistrictId());
							userMaster.setFirstName(firstName);
							userMaster.setLastName(lastName);
							userMaster.setForgetCounter(0);
							RoleMaster roleMaster = roleMasterDAO.findById(Integer.parseInt(role), false, false);
							userMaster.setEntityType(roleMaster.getEntityType());
							userMaster.setRoleId(roleMaster);

							userMasterDAO.makePersistent(userMaster);
						}
					}

					if(lstUserMasters.size()>0)
					{
						if(userMaster.getDistrictId().getDistrictId().equals(districtMaster.getDistrictId()))
						{
							if(userMaster.getStatus().equalsIgnoreCase("A"))
								isValidUser = true;
							else
								isValidUser = false;
						}
						else
						{
							isValidUser = false;		
							errorCode = 10008;
							errorMsg = Utility.getLocaleValuePropByKey("msgEmailRegistered", locale);	
							mapOrgDetail.put("isValidUser", "false");
							mapOrgDetail.put("errorCode", ""+errorCode);
							mapOrgDetail.put("errorMsg", errorMsg);
							return mapOrgDetail;
						}
					}else
					{
						if(!email.trim().equals(""))
							email = Utility.decodeBase64(email);

						List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(email);

						if(lstTeacherDetails.size()>0)
						{
							isValidUser = false;		
							errorCode = 10009;
							errorMsg = Utility.getLocaleValuePropByKey("msgEmailRegistered1", locale);	
							mapOrgDetail.put("isValidUser", "false");
							mapOrgDetail.put("errorCode", ""+errorCode);
							mapOrgDetail.put("errorMsg", errorMsg);
							return mapOrgDetail;
						}else
						{
							userMaster = new UserMaster();
							userMaster.setFirstName(firstName);
							userMaster.setLastName(lastName);
							userMaster.setEmailAddress(email);
							userMaster.setDistrictId(districtMaster);
							Utility utility= new Utility();
							String authenticationCode =	utility.getUniqueCodebyId((Integer.parseInt("1000")+1));
							int verificationCode=(int) Math.round(Math.random() * 2000000);

							userMaster.setAuthenticationCode(authenticationCode);
							userMaster.setSalutation(null);
							userMaster.setStatus("A");
							userMaster.setVerificationCode(""+verificationCode);
							userMaster.setIsExternal(false);
							try {
								userMaster.setPassword(MD5Encryption.toMD5("teachermatch"));
							} catch (NoSuchAlgorithmException e) {
								e.printStackTrace();
							} catch (UnsupportedEncodingException e) {
								e.printStackTrace();
							}

							RoleMaster roleMaster = roleMasterDAO.findById(Integer.parseInt(role), false, false);
							userMaster.setEntityType(roleMaster.getEntityType());
							userMaster.setRoleId(roleMaster);

							userMaster.setForgetCounter(0);
							userMaster.setCreatedDateTime(new Date());

							userMaster.setPhotoPathFile(null); 
							userMaster.setPhotoPath(null);
							userMaster.setIsQuestCandidate(false);
							userMasterDAO.makePersistent(userMaster);

							//emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), "I Have Added You As a User",MailText.createUserPwdMailToOtUser(request,userMaster));

							isValidUser = true;	
						}
					}

					if(isValidUser && userMaster!=null)
					{
						if(userMaster.getEntityType()==2)
							userMaster.setSchoolId(null);
						roleId = userMaster.getRoleId().getRoleId();
						System.out.println("userMaster.getUserId()::: "+userMaster.getUserId());
						session.setAttribute("userMaster", userMaster);
						session.setAttribute("lstMenuMaster", roleAccessPermissionDAO.getMenuList(request,userMaster));

						if(userMaster.getEntityType()==2)//District
						{
							int responseFolder =	 basicController.createHomeFolder(request);
							if(userMaster.getEntityType()!=1)
								basicController.createDistrictStatusHome(request);
						}
						else if(userMaster.getEntityType()==3)//School
						{
							int responseFolder =	 basicController.createHomeFolder(request);
							if(userMaster.getEntityType()!=1)
								basicController.createDistrictStatusHome(request);
						}

					}else{
						isValidUser = false;		
						errorCode = 10001;
						errorMsg = Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
					}

				}
			}
			else{
				isValidUser = false;		
				errorCode = 10001;
				errorMsg = Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
			}

			if(isValidUser){
				mapOrgDetail.put("isValidUser", "true");
				mapOrgDetail.put("userRole", ""+roleId);
			}
			else{
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
			}
		} 
		catch (Exception e){			
			e.printStackTrace();

			mapOrgDetail.put("isValidUser", "false");
		}
		return mapOrgDetail;	
	}

	public Map<String, String> authDistOrSchoolUsingEmployeeCode(HttpServletRequest request){

		Map<String, String> mapOrgDetail = new HashMap<String, String>();
		Integer errorCode=0;
		String errorMsg="";	
		boolean isValidUser = false;
		Integer roleId = 1;
		try {			
			HttpSession session = request.getSession();
			String authKey = "";
			System.out.println("queryString: "+request.getQueryString());
			//Demo
			//authKey = "TeacherMatch";
			//Platform
			authKey = "T3@ch3rM@tch";
			String applicationId = request.getParameter("applicationId")==null?"":request.getParameter("applicationId").trim();
			String forceLogout = request.getParameter("forceLogout")==null?"":request.getParameter("forceLogout").trim();
			String empId = request.getParameter("empId")==null?"":request.getParameter("empId").trim();
			String userId = request.getParameter("userId")==null?"":request.getParameter("userId").trim();
			String sdpUlcs = request.getParameter("sdpUlcs")==null?"":request.getParameter("sdpUlcs").trim();
			String sdpAgency = request.getParameter("sdpAgency")==null?"":request.getParameter("sdpAgency").trim();
			String appAgencies = request.getParameter("appAgencies")==null?"":request.getParameter("appAgencies").trim();
			String appRegions = request.getParameter("appRegions")==null?"":request.getParameter("appRegions").trim();
			String appRoles = request.getParameter("appRoles")==null?"":request.getParameter("appRoles").trim();
			String appLocations = request.getParameter("appLocations")==null?"":request.getParameter("appLocations").trim();
			String appAccessCnt = request.getParameter("appAccessCnt")==null?"":request.getParameter("appAccessCnt").trim();
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();


			System.out.println("authKey>"+authKey);
			if(key.equals("")){
				isValidUser = false;
				errorCode = 10002;
				errorMsg = Utility.getLocaleValuePropByKey("msgKeyRequired", locale);
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
				return mapOrgDetail;
			}
			if(empId.equals("")){
				isValidUser = false;
				errorCode = 10003;
				errorMsg = Utility.getLocaleValuePropByKey("msgEmpIdRequired", locale);
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
				return mapOrgDetail;
			}if(appRoles.equals("")){
				isValidUser = false;
				errorCode = 10003;
				errorMsg = Utility.getLocaleValuePropByKey("msgappRolesRequired", locale);
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
				return mapOrgDetail;
			}



			if(!key.equals("")){

				DistrictMaster districtMaster =  districtMasterDAO.validateDistrictByAuthkey(authKey);
				System.out.println("districtMaster: "+districtMaster);
				if(districtMaster == null){					
					isValidUser = false;
					errorCode=10001;
					errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
				}
				else if(districtMaster.getStatus().equalsIgnoreCase("I")){
					isValidUser = false;
					errorCode=10009;
					//errorMsg=Utility.getLocaleValuePropByKey("msgInactivated3", locale);
					errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
				}
				else {

					String mykey = empId + userId + sdpUlcs + sdpAgency + authKey + appAgencies + appRegions + appRoles + appLocations + appAccessCnt;
					System.out.println("mykey:: "+mykey);
					String generatedKey = Utility.md5(mykey);
					System.out.println("generatedKey: "+generatedKey);
					
					if(key.equals(generatedKey))
					{
						System.out.println("Key Validated");
						try{
							if(appRoles.contains(","))
							{
								String[] roles = appRoles.split(",");
								appRoles = roles[0];
							}
						}catch(Exception e1){}

						
						UserMaster userMaster = null;
						SchoolMaster schoolMaster =null;

						//appLocations
						EmployeeMaster employeeMaster =  employeeMasterDAO.checkEmployeeByEmployeeCode(empId,districtMaster);
						System.out.println("-------------------------------- "+employeeMaster);
						if(employeeMaster!=null)
						{
							String locationCode = "";
							//System.out.println("employeeMasterID::: "+employeeMaster.getEmployeeId());
							
							String role = "";
							if(appRoles.equals("20")) //Operations Administrator (OA) 
								role = "7";
							else if(appRoles.equals("21"))//Operations Analyst (OT)
								role = "8";
							else if(appRoles.equals("30"))// District Administrator (DA) 
								role = "2";
							else if(appRoles.equals("31")) //District Analyst (DT) 
								role = "5";
							else if(appRoles.equals("40"))// School Administrator (SA) 
								role = "3";
							else if(appRoles.equals("41")) // School Analyst (ST)
								role = "6";
							RoleMaster roleMaster = null;
							try {
								roleMaster = roleMasterDAO.findById(Integer.parseInt(role), false, false);
							} catch (Exception e) {
								//e.printStackTrace();
							}
							System.out.println("roleMaster:: "+roleMaster);
							
							if(roleMaster!=null) // Not a Candidate
							{
								//sdpUlcs
								if(sdpUlcs.equals("")){
									isValidUser = false;
									errorCode = 10002;
									errorMsg = Utility.getLocaleValuePropByKey("msgsdpUlcsRequired", locale);
									mapOrgDetail.put("isValidUser", "false");
									mapOrgDetail.put("errorCode", ""+errorCode);
									mapOrgDetail.put("errorMsg", errorMsg);
									return mapOrgDetail;
								}else
									locationCode = sdpUlcs;


								String email = "";
								email = employeeMaster.getEmailAddress();
								List<UserMaster> lstUserMasters = new ArrayList<UserMaster>();

								boolean checkUser = false;	
								//String districtLoctionCode = districtMaster.getLocationCode()==null?"":districtMaster.getLocationCode();
								//if(locationCode.equalsIgnoreCase(districtLoctionCode))

								if(locationCode.equalsIgnoreCase("6666"))
									checkUser = true;	
								else
								{
									schoolMaster = schoolMasterDAO.checkSchoolByLocationCode(locationCode);
									System.out.println("schoolMaster: "+schoolMaster);
									if(schoolMaster==null)
									{
										isValidUser = false;
										errorCode = 10002;
										//errorMsg = "Invalid Location Code.";
										errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
									}else
										checkUser = true;	
								}
								System.out.println("checkUser:::: "+checkUser);
								
								if(employeeMaster!=null){
									//List<UserMaster> lstUserMasters = userMasterDAO.getUserByEmployeeCode(districtMaster,schoolMaster,employeeMaster);
									lstUserMasters = userMasterDAO.getUserByEmployeeCode(districtMaster,employeeMaster);
									List<UserMaster> lstUserMastersDA = new ArrayList<UserMaster>();
									System.out.println("lstUserMaster::::: "+lstUserMasters.size());
									if(lstUserMasters!=null && lstUserMasters.size()>0){
										//userMaster = lstUserMasters.get(0);
										boolean isSA=false;
										for (UserMaster userMaster2 : lstUserMasters) {
											schoolMaster = userMaster2.getSchoolId();
											if(schoolMaster!=null)
											{
												//if(schoolMaster.getLocationCode().equalsIgnoreCase(locationCode) && userMaster2.getRoleId().getEntityType()==3)
												if(schoolMaster.getLocationCode().equalsIgnoreCase(locationCode))
												{
													userMaster = userMaster2;
													isSA=true;
												}
											}else
												lstUserMastersDA.add(userMaster2);
										}
										System.out.println("lstUserMastersDA::: "+lstUserMastersDA.size());
										System.out.println("isSA::: "+isSA);
										if(isSA==false && lstUserMastersDA.size()==0)
										{
											errorCode = 10002;
											//errorMsg = "Invalid Employee.";
											errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
											isValidUser = false;
											userMaster=null;
										}else if(isSA==false && lstUserMastersDA.size()>0)
										{
											userMaster=null;
											if(locationCode.equalsIgnoreCase("6666"))
												userMaster = lstUserMastersDA.get(0);
										}
										if(userMaster!=null)
										{
											int entityType = userMaster.getEntityType();
											if(entityType==3 && locationCode.equalsIgnoreCase("6666"))
											{
												errorCode = 10002;
												//errorMsg = "Invalid Employee.";
												errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
												isValidUser = false;

											}else
											{
												if(userMaster.getEntityType()==2)
													userMaster.setSchoolId(null);
												roleId = userMaster.getRoleId().getRoleId();
												System.out.println("userId: "+userMaster.getUserId());
												System.out.println("roleId "+ roleId +" "+roleMaster.getRoleId());
												if(userMaster.getRoleId().getRoleId().equals(roleMaster.getRoleId()))
												{
													System.out.println("userMaster.getUserId()::: "+userMaster.getUserId());
													session.setAttribute("userMaster", userMaster);
													session.setAttribute("lstMenuMaster", roleAccessPermissionDAO.getMenuList(request,userMaster));

													if(userMaster.getEntityType()==2)//District
													{
														int responseFolder =	 basicController.createHomeFolder(request);
														if(userMaster.getEntityType()!=1)
															basicController.createDistrictStatusHome(request);
													}
													else if(userMaster.getEntityType()==3)//School
													{
														int responseFolder =	 basicController.createHomeFolder(request);
														if(userMaster.getEntityType()!=1)
															basicController.createDistrictStatusHome(request);
													}
													isValidUser = true;		
												}else
												{
													errorCode = 10002;
													//errorMsg = "Invalid Employee.";
													errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
													isValidUser = false;
												}
											}
										}else
										{
											errorCode = 10002;
											//errorMsg = "Invalid Employee.";
											errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
											isValidUser = false;
										}

									}else
									{
										errorCode = 10002;
										//errorMsg = "Invalid Employee.";
										errorMsg=Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
										isValidUser = false;
									}


								}else
								{
									isValidUser = false;
								}
							}else if(appRoles.equals("10")) // Candidate logic 
							{
								// candidate Logic begins
								List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(employeeMaster.getEmailAddress());
								System.out.println("================= "+lstTeacherDetails.size());
								TeacherDetail teacherDetail = null;
								if(lstTeacherDetails== null || lstTeacherDetails.size()==0)
								{
									teacherDetail = new TeacherDetail();
									int authorizationkey=(int) Math.round(Math.random() * 2000000);
									
									teacherDetail.setFirstName(employeeMaster.getFirstName());
									teacherDetail.setLastName(employeeMaster.getLastName());
									teacherDetail.setEmailAddress(employeeMaster.getEmailAddress());
									try {
										teacherDetail.setPassword(MD5Encryption.toMD5("teachermatch"));
									} catch (NoSuchAlgorithmException e) {
										e.printStackTrace();
									} catch (UnsupportedEncodingException e) {
										e.printStackTrace();
									}
									teacherDetail.setUserType("R");
									teacherDetail.setFbUser(false);
									teacherDetail.setQuestCandidate(0);
									teacherDetail.setAuthenticationCode(""+authorizationkey);
									teacherDetail.setVerificationCode(""+authorizationkey);
									teacherDetail.setVerificationStatus(0);
									teacherDetail.setIsPortfolioNeeded(true);
									teacherDetail.setNoOfLogin(0);
									teacherDetail.setStatus("A");
									teacherDetail.setSendOpportunity(false);
									teacherDetail.setIsResearchTeacher(false);
									teacherDetail.setForgetCounter(0);
									teacherDetail.setCreatedDateTime(new Date());
									teacherDetail.setIpAddress(IPAddressUtility.getIpAddress(request));
									teacherDetail.setInternalTransferCandidate(false);
									teacherDetailDAO.makePersistent(teacherDetail);
									isValidUser = true;
									
									try {
										TeacherPersonalInfo teacherPersonalInfo = new TeacherPersonalInfo();
										teacherPersonalInfo.setTeacherId(teacherDetail.getTeacherId());
										teacherPersonalInfo.setFirstName(employeeMaster.getFirstName());
										teacherPersonalInfo.setLastName(employeeMaster.getLastName());
										teacherPersonalInfo.setMiddleName(employeeMaster.getMiddleName());
										teacherPersonalInfo.setSSN(Utility.encodeInBase64(employeeMaster.getSSN()));
										teacherPersonalInfo.setEmployeeNumber(employeeMaster.getEmployeeCode());
										teacherPersonalInfo.setIsDone(false);
										teacherPersonalInfo.setCreatedDateTime(new Date());
										
										if(employeeMaster.getFECode().equals("1") || employeeMaster.getFECode().equals("3")){
											teacherPersonalInfo.setEmployeeType(1);
										}else{
											teacherPersonalInfo.setEmployeeType(0);
										}
										teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}else
									teacherDetail = lstTeacherDetails.get(0);
								
								if(!teacherDetail.getStatus().equalsIgnoreCase("A"))
								{
									isValidUser = false;
								}else
								{
									isValidUser = true;
									//added 25-Mar-15
									TeacherPreference teacherPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
									if(teacherPreference==null)
									{
										teacherPreference = new TeacherPreference();
										teacherPreference.setTeacherId(teacherDetail);
										teacherPreference.setGeoId("21");
										teacherPreference.setRegionId("3");
										teacherPreference.setSchoolTypeId("2");
										teacherPreference.setCreatedDateTime(new Date());
										try{
											teacherPreferenceDAO.makePersistent(teacherPreference);
										}catch(Exception e){
											e.printStackTrace();
										}
									}
									
								}
								session.setAttribute("teacherDetail", teacherDetail);
								String fullName = "";

								String firstName = "";
								String lastName = "";
								firstName = teacherDetail.getFirstName();
								lastName = teacherDetail.getLastName();
								fullName = firstName + " " + lastName;
								//employeeMaster.getEmployeeCode();
								//System.out.println("userMasterDAO.checkTeacherAlsoSA(employeeMaster)   "+userMasterDAO.checkTeacherAlsoSA(employeeMaster));
								if(employeeMaster.getFECode().equals("1") || employeeMaster.getFECode().equals("3")){
									session.setAttribute("isAlsoSA",userMasterDAO.checkTeacherAlsoSA(employeeMaster));
								}else{
									session.setAttribute("isAlsoSA",false);
								}
								
								session.setAttribute("teacherName",	fullName);								
								session.setAttribute("lstMenuMaster", roleAccessPermissionDAO.getMenuListForTeacher(request));
								session.setAttribute("isImCandidate",	true);
								session.setAttribute("districtIdForImCandidate",employeeMaster.getDistrictMaster());
								/*session.setAttribute("classification",	employeeMaster.getClassification());
								if(employeeMaster.getEligibleToTransfer()!=null && !employeeMaster.getEligibleToTransfer().equalsIgnoreCase("") && employeeMaster.getEligibleToTransfer().equalsIgnoreCase("N")){
									isValidUser = false;		
									errorCode = 10001;
									errorMsg = "You are not eligible for Internal Movement Application.";
								}*/
								roleId = 10;
								System.out.println("isValidUser: "+isValidUser);
							}else
							{
								isValidUser = false;		
								errorCode = 10001;
								errorMsg = Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
							}

						}else
						{
							isValidUser = false;		
							errorCode = 10001;
							errorMsg = Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
						}
					}else
					{
						isValidUser = false;		
						errorCode = 10001;
						errorMsg = Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
					}


				}
			}
			else{
				isValidUser = false;		
				errorCode = 10001;
				errorMsg = Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale);
			}
			System.out.println("===================================================="+isValidUser );
			if(isValidUser){
				mapOrgDetail.put("isValidUser", "true");
				mapOrgDetail.put("userRole", ""+roleId);
			}
			else{
				mapOrgDetail.put("isValidUser", "false");
				mapOrgDetail.put("errorCode", ""+errorCode);
				mapOrgDetail.put("errorMsg", errorMsg);
			}
		} 
		catch (Exception e){			
			e.printStackTrace();

			mapOrgDetail.put("isValidUser", "false");
		}
		return mapOrgDetail;	
	}
}
