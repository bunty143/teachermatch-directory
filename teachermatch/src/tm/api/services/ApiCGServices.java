package tm.api.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.TeacherNotes;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherSecondaryStatus;
import tm.bean.cgreport.PercentileCalculation;
import tm.bean.cgreport.PercentileScoreByJob;
import tm.bean.cgreport.PercentileZscoreTscore;
import tm.bean.cgreport.RawDataForDomain;
import tm.bean.cgreport.TmpPercentileWiseZScore;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherNotesDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherSecondaryStatusDAO;
import tm.dao.cgreport.PercentileCalculationDAO;
import tm.dao.cgreport.PercentileScoreByJobDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.dao.cgreport.TmpPercentileWiseZScoreDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SecondaryStatusMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.services.CandidateReportService;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.services.PaginationAndSorting;
import tm.services.report.CGReportService;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class ApiCGServices 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private PercentileScoreByJobDAO percentileScoreByJobDAO;
	public void setPercentileScoreByJobDAO(	PercentileScoreByJobDAO percentileScoreByJobDAO) {
		this.percentileScoreByJobDAO = percentileScoreByJobDAO;
	}
	
	@Autowired
	private PercentileCalculationDAO percentileCalculationDAO;
	public void setPercentileCalculationDAO(
			PercentileCalculationDAO percentileCalculationDAO) {
		this.percentileCalculationDAO = percentileCalculationDAO;
	}
	
	@Autowired
	private CGReportService cGReportService;
	public void setcGReportService(CGReportService cGReportService) {
		this.cGReportService = cGReportService;
	}
	
	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;
	public void setRawDataForDomainDAO(RawDataForDomainDAO rawDataForDomainDAO) {
		this.rawDataForDomainDAO = rawDataForDomainDAO;
	}
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) 
	{
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) 
	{
		this.domainMasterDAO = domainMasterDAO;
	}
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	public void setTeacherCertificateDAO(TeacherCertificateDAO teacherCertificateDAO) 
	{
		this.teacherCertificateDAO = teacherCertificateDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
		
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) 
	{
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
		
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}
	
	@Autowired
	private CandidateReportService candidateReportService;
	public void setCandidateReportService(CandidateReportService candidateReportService) {
		this.candidateReportService = candidateReportService;
	}
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) 
	{
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO) 
	{
		this.teacherExperienceDAO = teacherExperienceDAO;
	}	

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}
	
	@Autowired
	private TeacherNotesDAO teacherNotesDAO;
	public void setTeacherNotesDAO(TeacherNotesDAO teacherNotesDAO) {
		this.teacherNotesDAO = teacherNotesDAO;
	}	
	@Autowired
	private SecondaryStatusMasterDAO secondaryStatusMasterDAO;
	public void setSecondaryStatusMasterDAO(SecondaryStatusMasterDAO secondaryStatusMasterDAO) {
		this.secondaryStatusMasterDAO = secondaryStatusMasterDAO;
	}
	
	@Autowired
	private TeacherSecondaryStatusDAO teacherSecondaryStatusDAO;
	public void setTeacherSecondaryStatusDAO(TeacherSecondaryStatusDAO teacherSecondaryStatusDAO) {
		this.teacherSecondaryStatusDAO = teacherSecondaryStatusDAO;
	}
	
	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;
	public void setMessageToTeacherDAO(MessageToTeacherDAO messageToTeacherDAO) {
		this.messageToTeacherDAO = messageToTeacherDAO;
	}

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	
	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	public void setTeacherAcademicsDAO(TeacherAcademicsDAO teacherAcademicsDAO) {
		this.teacherAcademicsDAO = teacherAcademicsDAO;
	}
	
	@Autowired
	private TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO;
	public void setTmpPercentileWiseZScoreDAO(
			TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO) {
		this.tmpPercentileWiseZScoreDAO = tmpPercentileWiseZScoreDAO;
	}
	
	Map<Integer,Map<Integer, Object>> mapDomainScore = null;
	double[] compositeScore = null;
	int max = 0;
	int min = 0;
	List<JobForTeacher> lstJobForTeacher = null;
	List<JobForTeacher> lstJobForTeacherVlt = null;
	public String getCandidateReportGrid(String apiJobId, String orderColumn, String sortingOrder)
	{
		System.out.println("#######################################################################################");

		
		Integer districtIdForSpecific = 3702970;
		//Integer districtIdForSpecific = 100002;
		Map<String,RawDataForDomain> mapRawDomainScore = null;
		double[] compositeTScoreMeanJob = null;
		double[] compositeTScoreMeanNational = null;
		List<JobForTeacher> lstJobForTeacher = null;
		List<JobForTeacher> lstJobForTeacherVlt = null;
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster = null;
		int roleId=0;
		if(session == null ||(session.getAttribute("userMasterAPI")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
			userMaster=	(UserMaster) session.getAttribute("userMasterAPI");
			districtMaster = userMaster.getDistrictId();
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}

		TeacherAssessmentStatus assessmentStatus = null;
		StatusMaster status = null;
		RawDataForDomain rawDataForDomain = null;
		
		PercentileScoreByJob pScoreByJob = null;
		String key = "";
		
		double domainTotatlScore = 0;
		double domainTotalNational = 0;
		//double displayScore = 0.0;
		double tScoreNW = 0.0;
		double tScoreJob= 0;
		StringBuffer sb = new StringBuffer();	
		double[] meanArray=null;
		int noOfRecordCheck = 0;
		double composite = 0;
		DecimalFormat oneDForm = new DecimalFormat("###,###.0");
		lstJobForTeacherVlt = new ArrayList<JobForTeacher>();
		try 
		{
			System.out.println(">>>><<<<<<<<<<<<<<<<<<<<<<<<<");
			
			String tFname = "";
			String tLname = "";
			String roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			meanArray = new double[lstDomain.size()];


			//JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			JobOrder jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId);
			TeacherAssessmentStatus teAssesStatusTempBase = null;
			TeacherAssessmentStatus teAssesStatusTempJSI = null;
			
			StatusMaster statusComp = WorkThreadServlet.statusMap.get("comp");
			StatusMaster statusIcomp = WorkThreadServlet.statusMap.get("icomp");
			StatusMaster statusHired = WorkThreadServlet.statusMap.get("hird");
			StatusMaster statusRemove = WorkThreadServlet.statusMap.get("rem");
			StatusMaster statusVlt = WorkThreadServlet.statusMap.get("vlt");

			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			lstStatusMasters.add(statusComp);
			lstStatusMasters.add(statusIcomp);
			lstStatusMasters.add(statusHired);
			lstStatusMasters.add(statusRemove);			

			lstJobForTeacher = jobForTeacherDAO.findJFTbyJobOrderForCG(jobOrder,lstStatusMasters);
			List<JobForTeacher> lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacher);

			boolean baseStatusFlag=false;
			if(jobCategoryMasterDAO.baseStatusChecked(jobOrder.getJobCategoryMaster().getJobCategoryId())){
				baseStatusFlag=true;
			}
			List<TmpPercentileWiseZScore> tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreDAO.findByCriteria(Order.asc("zScore"));
			List<PercentileZscoreTscore> pztList=cGReportService.populateZScoreMap(tmpPercentileWiseZScoreList);
			//--------------------------------------------------set global map start----------------------------------			
			Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();		
			Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
			List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
			List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
			for (JobForTeacher jbforteacher : lstJobForTeacher) 
				teacherDetails.add(jbforteacher.getTeacherId());
			if(teacherDetails!=null && teacherDetails.size()>0){
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetails);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
					if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
						isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
					}
				}
			}
			
			Map<String, PercentileCalculation> mapNationalPercentile = new HashMap<String, PercentileCalculation>();
			Map<String, PercentileScoreByJob> mapJobPercentile = new HashMap<String, PercentileScoreByJob>();
			
			List<PercentileCalculation> lstPercentileCalculation = percentileCalculationDAO.findAllInCurrenctYear();
			for(PercentileCalculation percentileCalculation : lstPercentileCalculation){
				mapNationalPercentile.put(percentileCalculation.getDomainMaster().getDomainId()+"##"+percentileCalculation.getScore(), percentileCalculation);
			}			
			
			List<PercentileScoreByJob> lstPercentileScoreByJobs = percentileScoreByJobDAO.findPercentileScoreByJob(jobOrder);
			for(PercentileScoreByJob percentileScoreByJob : lstPercentileScoreByJobs){
				mapJobPercentile.put(percentileScoreByJob.getDomainMaster().getDomainId()+"##"+percentileScoreByJob.getScore(), percentileScoreByJob);
			}			
			
			mapRawDomainScore = new HashMap<String, RawDataForDomain>();
			List<RawDataForDomain> lstRawDataForDomain = null;
			for(DomainMaster dm: lstDomain){				
				lstRawDataForDomain = rawDataForDomainDAO.findByDomainAndTeachers(dm, teacherDetails);
				for(RawDataForDomain rdm : lstRawDataForDomain){
					mapRawDomainScore.put(rdm.getDomainMaster().getDomainId()+"##"+rdm.getTeacherDetail().getTeacherId(), rdm);
				}
			}

			List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByJobAndSchool(jobOrder, userMaster.getSchoolId());
			Map<Integer,TeacherSecondaryStatus> mapTSecStatus = new HashMap<Integer, TeacherSecondaryStatus>();
			for(TeacherSecondaryStatus tSecondaryStatus: lstTeacherSecondaryStatus ){
				mapTSecStatus.put(tSecondaryStatus.getTeacherDetail().getTeacherId(), tSecondaryStatus);
			}	

			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI = teacherAssessmentStatusDAO.findAssessmentTaken(null, jobOrder);
			for(TeacherAssessmentStatus ts : lstJSI){
				mapJSI.put(ts.getTeacherDetail().getTeacherId(), ts);
			}
			//--------------------------------------------------set global map end----------------------------------
			for(JobForTeacher jft : lstJFTremove){				
				teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());				
				teAssesStatusTempJSI =mapJSI.get(jft.getTeacherId().getTeacherId()); 

				if((teAssesStatusTempBase==null && baseStatusFlag) || (baseStatusFlag && teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))){
					lstJobForTeacher.remove(jft);
				}
				if((teAssesStatusTempBase!=null && baseStatusFlag) && (teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))){	
					lstJobForTeacherVlt.add(jft);
					lstJobForTeacher.remove(jft);
				}
				else if(teAssesStatusTempJSI!=null && teAssesStatusTempJSI.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
					lstJobForTeacherVlt.add(jft);
					lstJobForTeacher.remove(jft);					
				}
			}

			//------------Check user can hire, unhire or remove start
			List<SchoolMaster> lstSchoolMaster =null;
			SchoolMaster schoolMaster = null;
			SchoolInJobOrder schoolInJobOrder = null;
		
			boolean doActivity = true;
			if(userMaster.getEntityType()==2)//District - 2 user login type
			{
				
				lstSchoolMaster = jobOrder.getSchool();

				if(lstSchoolMaster!=null && lstSchoolMaster.size()>0){										
					doActivity= false;
				}

				if(jobOrder.getCreatedForEntity().equals(3)){
					doActivity= false;
				}				
			}
			else{
				schoolMaster = userMaster.getSchoolId();				
				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMaster);
				if(schoolInJobOrder==null){					
					doActivity = false;
				}
			}

			int k=0;
			compositeTScoreMeanJob = new double[lstJobForTeacher.size()];
			compositeTScoreMeanNational = new double[lstJobForTeacher.size()];
			for(JobForTeacher jft:lstJobForTeacher){		
				domainTotatlScore=0.0;
				domainTotalNational=0.0;
				tScoreJob = 0.0;
				tScoreNW = 0.0;
				
				for(DomainMaster domain: lstDomain){					
					key = domain.getDomainId()+"##"+jft.getTeacherId().getTeacherId();
					rawDataForDomain =  mapRawDomainScore.get(key);
					key = domain.getDomainId()+"##"+Math.round(rawDataForDomain.getScore());
					
					if(mapNationalPercentile.get(key)!=null)
						tScoreNW = mapNationalPercentile.get(key).gettValue();						
					else						
						tScoreJob = 0.0;
					
					if(mapJobPercentile.get(key)!=null)
						tScoreJob =  mapJobPercentile.get(key).gettValue();
					else
						tScoreJob = 0.00;
			
					domainTotatlScore = domainTotatlScore+tScoreJob*domain.getMultiplier();
					domainTotalNational = domainTotalNational+tScoreNW*domain.getMultiplier();
				}
				
				jft.setCompositeScore(domainTotatlScore);
				jft.setNormScore(domainTotalNational);				
				compositeTScoreMeanJob[k]=domainTotatlScore;
				compositeTScoreMeanNational[k]=domainTotalNational;
				k++;
			}

			sb.append("<table border='0' cellspacing=0 cellpadding=0 width='100%'>");
			sb.append("<tr>");
			sb.append("<td style='vertical-align: middle; padding-bottom: 10px; ' width='50%'><b>Job Title: </b>"+jobOrder.getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td  id='totalCGRecordDiv' name='totalCGRecordDiv' style='vertical-align: middle; padding-bottom: 10px; ' width='50%'><b>Number of Candidates: </b>");
			sb.append("</td>");
			sb.append("<td style='vertical-align: top; padding-bottom: 10px;text-align: right;' width='50%'>");

			sb.append("<table border='0' cellspacing=0 cellpadding=0 width='100%'  >");			
			sb.append("<tr>");
			sb.append("<td style='text-align: right;'>");
			sb.append("<a data-original-title='PDF' rel='tooltip' href=\"javascript:void(0);\" id='hrefPDF' onclick=\"downloadCandidateGridReport();if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;\"><img src='../images/viewPDF.png' ></a>");

			sb.append("&nbsp;<a data-original-title='Search' rel='tooltip' id='tpSearch' href='javascript:void(0);' ><img src='../images/search.png' ></a>");

			sb.append("&nbsp;<a data-original-title='Legend' rel='tooltip' id='tpLegend' href='javascript:void(0);'><img src='../images/legend.png' ></a>");

			String divLegentHeight=null;
			if(baseStatusFlag){
				divLegentHeight="style='height: 700px;'";
			}else{
				divLegentHeight="style='height: 380px;'";
			}

			sb.append("<div id='divToggel' class='divLegent' "+divLegentHeight+" >");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style='vertical-align: top; text-align: left; padding-left: 10px;' width='30%'>");
			sb.append("<b>Competency Domain Definitions</b>");
			sb.append("</td>");
			sb.append("<td style='vertical-align: top; text-align: right; padding-right: 10px;' width='30%'>");
			sb.append("<b><button onclick=\"divToggelClose();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">x</button><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td colspan='2' style='text-align: left; padding-left: 10px;' width='30%'>");

			sb.append("<b>Attitudinal</b>");
			sb.append("<p>This score represents the attitudinal factors affecting a candidate�s ability to be a highly effective teacher. Competencies include persistence in the face of adversity, maintaining a positive attitude, and motivation to succeed.</p>");

			sb.append("<b>Cognitive Ability</b>");
			sb.append("<p>Candidate scores in the competencies of verbal ability, quantitative ability, and analytical reasoning.</p>");

			sb.append("<b>Teaching Skills</b>");
			sb.append("<p>Candidate scores in instructing, creating a learning environment, planning for successful outcomes, and analyzing and adjusting.</p>");

			if(baseStatusFlag){
				sb.append("<b>Key</b>");
				for(int i=10; i>=1; i--){ 
					if(i==1)
						sb.append("<p><img height='12px' width='9px' src='../images/d"+i+".png'>&nbsp;"+Utility.getValueOfPropByKey("decile"+i+"Min")+"</p>");
					else
						sb.append("<p><img height='12px' width='9px' src='../images/d"+i+".png'>&nbsp;"+Utility.getValueOfPropByKey("decile"+i+"Min")+" - "+Utility.getValueOfPropByKey("decile"+i+"Max")+"</p>");
					
				}						
			}
			if(!jobOrder.getDistrictMaster().getDistrictId().equals(districtIdForSpecific)){
				sb.append("<p><b>Removed</b> Indicates removal by an Administrator</p>");
				sb.append("<p><b>Available</b> Applied for position and is available for Hire</p>");
				sb.append("<p><b>Hired</b> Indicates hired for this position (Can be Unhired)</p>");
				sb.append("<p><b>Timed Out</b> Indicates a timeout violation; no scores displayed</p>");
			}
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</td>");						
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");

			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style='vertical-align: top;' width='100%' colspan='2'>");			
			sb.append("<table id='tblGrid' border='0' class='table table-bordered '>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");

			sb.append("<th >");		
			if(orderColumn.equals("candidate") && sortingOrder.equals("0")){
				sb.append("<span style='cursor:pointer;'>");			
				sb.append("<a  class='net-header-text'  href=\"javascript:orderingCG('candidate','1')\">Candidate<img alt='' src='../images/AscOrder.png' height='10px' width='10px'></a>");			
				sb.append("</span>");
			}
			else if(orderColumn.equals("candidate") && sortingOrder.equals("1")){
				sb.append("<span style='cursor:pointer;'>");			
				sb.append("<a  class='net-header-text'  href=\"javascript:orderingCG('candidate','0')\">Candidate<img alt='' src='../images/Descorder.png' height='10px' width='10px'></a>");			
				sb.append("</span>");
			}
			else{				
				sb.append("<a  class='net-header-text'  href=\"javascript:orderingCG('candidate','1')\">Candidate</a>");
			}
			sb.append("</th>");
			if(baseStatusFlag){
				for(DomainMaster domain: lstDomain)
				{	
					sb.append("<th width='50px;'>");
					sb.append(domain.getDomainName());
					sb.append("</th>");
				}
			}
			if(baseStatusFlag){
				sb.append("<th width='60px;'>");
				if(orderColumn.equals("candidateScore") && sortingOrder.equals("0")){
					sb.append("<span style='cursor:pointer;'>");			
					sb.append("<a  class='net-header-text'  href=\"javascript:orderingCG('candidateScore','1')\">Composite Score<img alt='' src='../images/AscOrder.png' height='10px' width='10px'></a>");			
					sb.append("</span>");
				}
				else if(orderColumn.equals("candidateScore") && sortingOrder.equals("1")){
					sb.append("<span style='cursor:pointer;'>");			
					sb.append("<a  class='net-header-text'  href=\"javascript:orderingCG('candidateScore','0')\">Composite Score<img alt='' src='../images/Descorder.png' height='10px' width='10px'></a>");			
					sb.append("</span>");
				}
				else{				
					sb.append("<a  class='net-header-text'  href=\"javascript:orderingCG('candidateScore','1')\">Composite Score</a>");
				}
				sb.append("</th>");
			}
			
			if(baseStatusFlag){
				sb.append("<th width='60px;'>");
				if(orderColumn.equals("normScore") && sortingOrder.equals("0")){
					sb.append("<span style='cursor:pointer;'>");			
					sb.append("<a  class='net-header-text'  href=\"javascript:orderingCG('normScore','1')\">Norm Score<img alt='' src='../images/AscOrder.png' height='10px' width='10px'></a>");			
					sb.append("</span>");
				}
				else if(orderColumn.equals("normScore") && sortingOrder.equals("1")){
					sb.append("<span style='cursor:pointer;'>");			
					sb.append("<a  class='net-header-text'  href=\"javascript:orderingCG('normScore','0')\">Norm Score<img alt='' src='../images/Descorder.png' height='10px' width='10px'></a>");			
					sb.append("</span>");
				}
				else{				
					sb.append("<a  class='net-header-text'  href=\"javascript:orderingCG('normScore','1')\">Norm Score</a>");
				}
				sb.append("</th>");
			}
			if(!jobOrder.getDistrictMaster().getDistrictId().equals(districtIdForSpecific)){
				sb.append("<th width='60px;'>");
				sb.append("Status");
				sb.append("</th>");
			}			
			sb.append("<th width='235px;'>");
			sb.append("Details");
			sb.append("</th>");
			
			sb.append("</tr>");
			sb.append("</thead>");

			

			//-- sorting CG view start
			List<JobForTeacher> lstjft = new ArrayList<JobForTeacher>(lstJobForTeacher);
			List<JobForTeacher> lstremoveJFT = new ArrayList<JobForTeacher>();
			for(JobForTeacher jft: lstjft){
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					lstJobForTeacher.remove(jft);
					lstremoveJFT.add(jft);
				}
			}
			
			if(orderColumn.equals("candidate") && sortingOrder.equals("0")){				
				Collections.sort(lstJobForTeacher,JobForTeacher.jobForTeacherComparatorTeacherDesc);				
			}
			else if(orderColumn.equals("candidate") && sortingOrder.equals("1")){				
				Collections.sort(lstJobForTeacher,JobForTeacher.jobForTeacherComparatorTeacher);
			}
			if(orderColumn.equals("normScore") && sortingOrder.equals("0")){				
				Collections.sort(lstJobForTeacher,JobForTeacher.jobForTeacherComparatorNormScoreDesc );				
			}
			else if(orderColumn.equals("normScore") && sortingOrder.equals("1")){				
				Collections.sort(lstJobForTeacher,JobForTeacher.jobForTeacherComparatorNormScore);
			}
			else if(orderColumn.equals("candidateScore") && sortingOrder.equals("0")){				
				Collections.sort(lstJobForTeacher,JobForTeacher.jobForTeacherComparatorDesc);
			}
			else{				
				Collections.sort(lstJobForTeacher,JobForTeacher.jobForTeacherComparator);				
			}
			Collections.sort(lstremoveJFT,JobForTeacher.jobForTeacherComparator);
			lstJobForTeacher.addAll(lstremoveJFT);
			//-- sorting CG view end

			TeacherSecondaryStatus teacherSecondaryStatus = null;
			String teacherSecStatus = "";
			String remCol="";



			int teacherAssessmentStatusId=0;


			List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();			
			for(JobForTeacher jft:lstJobForTeacher){
				lstTeacherDetails.add(jft.getTeacherId());
			}

			
			for(JobForTeacher jft:lstJobForTeacher)
			{	
				teacherSecondaryStatus = mapTSecStatus.get(jft.getTeacherId().getTeacherId());

				if((teacherSecondaryStatus!=null) && (!userMaster.getEntityType().equals(1))){
					teacherSecStatus = "&nbsp;<font style='color: #195386; font-weight: bold;'>"+teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusName()+"</font>";
				}
				else{
					teacherSecStatus="";
				}

				noOfRecordCheck++;
				if(jft.getTeacherId()!=null){
					tFname = jft.getTeacherId().getFirstName()==null?"":jft.getTeacherId().getFirstName();
					tLname = jft.getTeacherId().getLastName()==null?"":jft.getTeacherId().getLastName();
				}

				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					remCol="bgcolor='#e1e1e1'";
				}
				else{
					remCol="";
				}
				assessmentStatus = baseTakenMap.get(jft.getTeacherId().getTeacherId());
				sb.append("<tr>");
				sb.append("<td "+remCol+">");
				sb.append(tFname+" "+tLname+teacherSecStatus);
				sb.append("</td>");

				domainTotatlScore=0;
				int i=0;
				String colorName="";
				PercentileCalculation pCalculationObj=null;
				for(DomainMaster domain: lstDomain)	{	
					key = domain.getDomainId()+"##"+jft.getTeacherId().getTeacherId();
					rawDataForDomain =  mapRawDomainScore.get(key);
					if(rawDataForDomain==null){
						sb.append("<td style='text-align: center; vertical-align: middle;'>null");
											
						sb.append("</td>");
					}
										
					if(baseStatusFlag){
						key = domain.getDomainId()+"##"+Math.round(rawDataForDomain.getScore());						
						pScoreByJob = mapJobPercentile.get(key);
						pCalculationObj=mapNationalPercentile.get(key);
						if(pScoreByJob==null){
							pScoreByJob = new PercentileScoreByJob();
							pScoreByJob.setCumulativeFrequency(0);
							pScoreByJob.setFrequency(0);
							pScoreByJob.setPercentile(0.0);
						}
						sb.append("<td style='text-align: center; vertical-align: middle;'>");
						sb.append(Math.round(pScoreByJob.gettValue())+"");					
						sb.append("</td>");
						meanArray[i]=meanArray[i] + pScoreByJob.gettValue();
						i++;
					}
				}
				composite =  Double.valueOf(oneDForm.format(jft.getCompositeScore()));
				
				if(baseStatusFlag){	
					sb.append("<td bgcolor='"+colorName+"' style='text-align: center; vertical-align: middle;'>");
					sb.append(Math.round(composite));
					sb.append("</td>");
					double percentile=cGReportService.getPercentile(pztList,Double.parseDouble(oneDForm.format(jft.getNormScore())));
					colorName =cGReportService.getColorName(percentile);
					sb.append("<td bgcolor='"+colorName+"'  style='text-align: center; vertical-align: middle;'>");
					sb.append(""+(Math.round(jft.getNormScore()))+"");
					sb.append("</td>");
				}

				status = jft.getStatus();
				if(!jobOrder.getDistrictMaster().getDistrictId().equals(districtIdForSpecific)){
					sb.append("<td style='text-align: center; vertical-align: middle;'>");									
					if(status.getStatusShortName().equalsIgnoreCase("comp") || status.getStatusShortName().equalsIgnoreCase("icomp"))
					{
						sb.append("Available");
					}
					else if(status.getStatusShortName().equalsIgnoreCase("rem"))
					{
						sb.append("Removed");
					}
					else
					{
						sb.append("Hired");
					}	
					sb.append("</td>");
				}
				

				sb.append("<td style='vertical-align: middle;'>");


				sb.append("<table border=0 cellspacing=0 cellpadding=0 '>");
				sb.append("<tr>");				
				if(userMaster.getEntityType()!=1 && doActivity)
				{
					if(baseStatusFlag){
						teacherAssessmentStatusId=assessmentStatus.getTeacherAssessmentStatusId();
					}
					else{
						teacherAssessmentStatusId=0;
					}
					if(status.getStatusShortName().equalsIgnoreCase("hird"))
					{	
						if(userMaster.getEntityType().equals(3)){
							if(userMaster.getSchoolId().equals(jft.getUpdatedBy().getSchoolId())){
								sb.append("<td  style='padding: 0px; padding-left:5px; margin: 0px; border: 0px;'>");
								if(roleAccess.indexOf("|14|")!=-1){
									sb.append("<a data-original-title='Take Action' rel='tooltip' id='tpAct"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"showActDiv('"+status.getStatusShortName()+"','"+teacherAssessmentStatusId+"','"+jft.getJobForTeacherId()+"')\"><img src='../images/act.png' width='24px'></a>");
								}else{
									sb.append("&nbsp;");
								}
								sb.append("</td>");								
							}
						}
						else{
							sb.append("<td  style='padding: 0px; padding-left:5px; margin: 0px; border: 0px;'>");
							if(roleAccess.indexOf("|14|")!=-1){
								sb.append("<a data-original-title='Take Action' rel='tooltip' id='tpAct"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"showActDiv('"+status.getStatusShortName()+"','"+teacherAssessmentStatusId+"','"+jft.getJobForTeacherId()+"')\"><img src='../images/act.png' width='24px'></a>");					
							}else{
								sb.append("&nbsp;");
							}
						}						 
					}
					else if(!status.getStatusShortName().equalsIgnoreCase("rem"))
					{	
						sb.append("<td  style='padding: 0px; padding-left:5px; margin: 0px; border: 0px;'>");
						if(roleAccess.indexOf("|14|")!=-1){
							sb.append("<a data-original-title='Take Action' rel='tooltip' id='tpAct"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"showActDiv('"+status.getStatusShortName()+"','"+teacherAssessmentStatusId+"','"+jft.getJobForTeacherId()+"')\"><img src='../images/act.png' width='24px'></a>");
						}else{
							sb.append("&nbsp;");
						}
					}

					sb.append("</td>");
				}

				sb.append("<td  style='padding: 0px; padding-left:5px; margin: 0px; border: 0px;'>");
				if(roleAccess.indexOf("|4|")!=-1){
					sb.append("<a data-original-title='Send a Message' rel='tooltip' id='tpMsg"+noOfRecordCheck+"' href='javascript:void(0);'  onclick=\"getMessageDiv('"+jft.getTeacherId().getTeacherId()+"','"+jft.getTeacherId().getEmailAddress()+"','"+jft.getJobId().getJobId()+"');\"  ><img src='../images/message.png' width='24px'></a>");					
				}else{
					sb.append("&nbsp;");					
				}
				sb.append("</td>");

				sb.append("<td  style='padding: 0px; padding-left:5px; margin: 0px; border: 0px;'>");
				if(roleAccess.indexOf("|4|")!=-1){
					sb.append("<a data-original-title='View/Create Notes' rel='tooltip' id='tpNotes"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"getNotesDiv('"+jft.getTeacherId().getTeacherId()+"','"+jft.getJobId().getJobId()+"')\"><img src='../images/notes.png' width='24px'></a>");
				}else{
					sb.append("&nbsp;");
				}				
				sb.append("</td>");

				sb.append("</tr>");
				sb.append("</table>");

				sb.append("</td>");
				sb.append("</tr>");


			}
			if(baseStatusFlag){
				if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
				{
					sb.append("<tr>");				
					sb.append("<td>");
					sb.append("<b>Mean</b>");
					sb.append("</td>");

					for(double sum : meanArray)
					{					
						sb.append("<td style='text-align: center; vertical-align: middle;'>");
						sb.append("<b>"+Math.round(sum/lstJobForTeacher.size())+" </b>");
						sb.append("</td>");						
					}

					sb.append("<td style='text-align: center; vertical-align: middle;'>");
					double compositSumJob = 0.0;					
					for(double d : compositeTScoreMeanJob){
						compositSumJob=compositSumJob+d;
					}
					sb.append("<b>"+Math.round(compositSumJob/compositeTScoreMeanJob.length)+"</b>");
					sb.append("</td>");
					
					sb.append("<td style='text-align: center; vertical-align: middle;'>");
					double compositSumNational = 0.0;
					for(double d : compositeTScoreMeanNational){
						compositSumNational = compositSumNational+d;
					}
					sb.append("<b>"+Math.round(compositSumNational/compositeTScoreMeanNational.length)+"</b>");
					sb.append("</td>");
					sb.append("<td colspan='4'>");
					sb.append("");
					sb.append("</td>");
					sb.append("</tr>");
				}
			}
			lstStatusMasters=null;
			lstStatusMasters = new ArrayList<StatusMaster>();			
			lstStatusMasters.add(statusVlt);			
			lstJobForTeacherVlt.addAll(jobForTeacherDAO.findJFTbyJobOrderForCG(jobOrder,lstStatusMasters));
			System.out.println("11"+lstJobForTeacherVlt.size());
			//------------------map creation start-------

			if(teacherDetails!=null && teacherDetails.size()>0){
				for (JobForTeacher jbforteacher : lstJobForTeacherVlt) 
					teacherDetails.add(jbforteacher.getTeacherId());
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetails);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus){				
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
				}
			}


			//------------------map creation end--------
			lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacherVlt);			
			for(JobForTeacher jft : lstJFTremove)
			{				
				//assessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jft.getTeacherId());
				teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());

				if((teAssesStatusTempBase==null && baseStatusFlag) || (baseStatusFlag && teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")))
					//if(teAssesStatusTempBase==null  || teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
				{	
					lstJobForTeacherVlt.remove(jft);
				}				
			}

			//--Sorting CG report data -- start
			lstjft = new ArrayList<JobForTeacher>(lstJobForTeacherVlt);
			lstremoveJFT = new ArrayList<JobForTeacher>();
			for(JobForTeacher jft: lstjft){
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					lstJobForTeacherVlt.remove(jft);
					lstremoveJFT.add(jft);
				}
			}
			Collections.sort(lstJobForTeacherVlt,JobForTeacher.jobForTeacherComparatorTeacher);
			Collections.sort(lstremoveJFT,JobForTeacher.jobForTeacherComparatorTeacher);
			lstJobForTeacherVlt.addAll(lstremoveJFT);
			//--Sorting CG report data -- end


			lstTeacherDetails = new ArrayList<TeacherDetail>();

			for(JobForTeacher jft:lstJobForTeacherVlt){
				lstTeacherDetails.add(jft.getTeacherId());
			}

			
			
			System.out.println("lstJobForTeacherVlt"+lstJobForTeacherVlt.size());

			for(JobForTeacher jft:lstJobForTeacherVlt)
			{				
				assessmentStatus = baseTakenMap.get(jft.getTeacherId().getTeacherId());

				teacherSecondaryStatus = mapTSecStatus.get(jft.getTeacherId().getTeacherId());
				if((teacherSecondaryStatus!=null) && (!userMaster.getEntityType().equals(1))){
					teacherSecStatus = "&nbsp;<font style='color: #195386; font-weight: bold;'>"+teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusName()+"</font>";
				}
				else{
					teacherSecStatus="";
				}
				noOfRecordCheck++;
				if(jft.getTeacherId()!=null)
				{
					tFname = jft.getTeacherId().getFirstName()==null?"":jft.getTeacherId().getFirstName();
					tLname = jft.getTeacherId().getLastName()==null?"":jft.getTeacherId().getLastName();
				}
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					remCol="bgcolor='#e1e1e1'";
				}
				else{
					remCol="";
				}
				sb.append("<tr>");
				sb.append("<td "+remCol+">");
				sb.append(tFname+" "+tLname+teacherSecStatus);
				sb.append("</td>");

				domainTotatlScore=0;
				int i=0;
				if(baseStatusFlag){
					for(DomainMaster domain: lstDomain)	{					
						sb.append("<td>");
						sb.append("");					
						sb.append("</td>");					
					}
					
					sb.append("<td>");
					sb.append("");
					sb.append("</td>");
				}


				status = jft.getStatus();
				if(!jobOrder.getDistrictMaster().getDistrictId().equals(districtIdForSpecific)){
					sb.append("<td style='text-align: center; vertical-align: middle;'>");
					if(status.getStatusShortName().equalsIgnoreCase("comp") || status.getStatusShortName().equalsIgnoreCase("icomp") || status.getStatusShortName().equalsIgnoreCase("vlt") )
					{	
						sb.append("Timed Out");
					}
					else if(status.getStatusShortName().equalsIgnoreCase("rem"))
					{
						sb.append("Removed");
					}
					else
					{
						sb.append("Hired");
					}				
					sb.append("</td>");
				}
				sb.append("<td style='vertical-align: middle;'>");

				
				sb.append("</td>");		

				sb.append("<td style='vertical-align: middle;'>");


				sb.append("<table border=0 cellspacing=0 cellpadding=0 '>");
				sb.append("<tr>");	

				int assessmentId = 0;
				if(userMaster.getEntityType()!=1 && doActivity)
				{
					if(baseStatusFlag){
						teacherAssessmentStatusId=assessmentStatus.getTeacherAssessmentStatusId();
					}
					else{
						teacherAssessmentStatusId=0;
					}
					if(status.getStatusShortName().equalsIgnoreCase("hird"))
					{	

						if(userMaster.getEntityType().equals(3)){
							if(userMaster.getSchoolId().equals(jft.getUpdatedBy().getSchoolId())){
								sb.append("<td  style='padding: 0px; padding-left:5px; margin: 0px; border: 0px;'>");
								if(roleAccess.indexOf("|14|")!=-1){
									sb.append("<a data-original-title='Take Action' rel='tooltip' id='tpAct"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"showActDiv('"+status.getStatusShortName()+"','"+teacherAssessmentStatusId+"','"+jft.getJobForTeacherId()+"')\"><img src='../images/act.png' width='24px'></a>");
								}else{
									sb.append("&nbsp;");
								}
								sb.append("</td>");								
							}
						}
						else{
							sb.append("<td  style='padding: 0px; padding-left:5px; margin: 0px; border: 0px;'>");
							if(roleAccess.indexOf("|14|")!=-1){
								sb.append("<a data-original-title='Take Action' rel='tooltip' id='tpAct"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"showActDiv('"+status.getStatusShortName()+"','"+teacherAssessmentStatusId+"','"+jft.getJobForTeacherId()+"')\"><img src='../images/act.png' width='24px'></a>");					
							}else{
								sb.append("&nbsp;");
							}
						}						 
					}
					else if(!status.getStatusShortName().equalsIgnoreCase("rem"))
					{
						sb.append("<td  style='padding: 0px; padding-left:5px; margin: 0px; border: 0px;'>");

						if(assessmentStatus!=null && assessmentStatus.getTeacherAssessmentStatusId()==null){
							assessmentId = assessmentStatus.getTeacherAssessmentStatusId();
						}
						else{
							assessmentId=0;
						}

						if(roleAccess.indexOf("|14|")!=-1){
							sb.append("<a data-original-title='Take Action' rel='tooltip' id='tpAct"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"showActDiv('"+status.getStatusShortName()+"','"+assessmentId+"','"+jft.getJobForTeacherId()+"')\"><img src='../images/act.png' width='24px'></a>");					
						}else{
							sb.append("&nbsp;");
						}
					}
					sb.append("</td>");
				}

				sb.append("<td  style='padding: 0px; padding-left:5px; margin: 0px; border: 0px;'>");
				if(roleAccess.indexOf("|4|")!=-1){
					sb.append("<a data-original-title='Send a Message' rel='tooltip' id='tpMsg"+noOfRecordCheck+"' href='javascript:void(0);'  onclick=\"getMessageDiv('"+jft.getTeacherId().getTeacherId()+"','"+jft.getTeacherId().getEmailAddress()+"','"+jft.getJobId().getJobId()+"');\"  ><img src='../images/message.png' width='24px'></a>");					
				}else{
					sb.append("&nbsp;");					
				}
				sb.append("</td>");

				sb.append("<td  style='padding: 0px; padding-left:5px; margin: 0px; border: 0px;'>");
				if(roleAccess.indexOf("|4|")!=-1){
					sb.append("<a data-original-title='View/Create Notes' rel='tooltip' id='tpNotes"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"getNotesDiv('"+jft.getTeacherId().getTeacherId()+"','"+jft.getJobId().getJobId()+"')\"><img src='../images/notes.png' width='24px'></a>");
				}else{
					sb.append("&nbsp;");
				}				
				sb.append("</td>");

				sb.append("</tr>");
				sb.append("</table>");

				sb.append("</td>");
				sb.append("</tr>");

			}

			if(noOfRecordCheck==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='11'>");
				sb.append("No Record Found.");
				sb.append("</td>");
				sb.append("</tr>");
			}

			sb.append("</table>");
			sb.append("</td >");

			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td colspan='11'>");
			sb.append("<input type=\"hidden\" id=\"totalCGRecord\" name=\"totalCGRecord\" value='"+noOfRecordCheck+"'>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");




		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return sb.toString();	
	
	
	}
	

	public String showActDiv(String status,int teacherAssessmentStatusId,String jobForTeacherId)
	{	
		
		
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = (UserMaster) session.getAttribute("userMasterAPI");		
		
		StringBuffer sb = new StringBuffer("");
		StatusMaster statusMaster = WorkThreadServlet.statusMap.get(status);
		
		TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findById(new Integer(teacherAssessmentStatusId), false, false);
		JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
		int teacherAssessmentStatus_Id=0;
		if(teacherAssessmentStatus!=null){
			teacherAssessmentStatus_Id=teacherAssessmentStatus.getTeacherAssessmentStatusId();
		}
		String disableText="";
		
		
		
		sb.append("<table>");
		if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
		{	
			disableText = "disabled";
			sb.append("<tr>");
			sb.append("<td class='tdAct'>");
			sb.append("<input type='radio' style='vertical-align: middle;' name='rdoSecStatus'  value=\"unhird,"+teacherAssessmentStatus_Id+","+jobForTeacher.getJobForTeacherId()+"\">");
			sb.append("<td class='tdAct'>&nbsp;Unhire");
			sb.append("</td>");				
			sb.append("</tr>");
		}
		else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem"))
		{	
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("&nbsp;&nbsp;");
			sb.append("</td>");
			sb.append("</tr>");
		}
		else
		{	
			
			sb.append("<tr>");
			sb.append("<td class='tdAct'>");
			sb.append("<input type='radio' name='rdoSecStatus' value=\"hird,"+teacherAssessmentStatus_Id+","+jobForTeacher.getJobForTeacherId()+"\">");			
			sb.append("</td>");
			sb.append("<td class='tdAct'>&nbsp;Hire");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td class='tdAct'>");			
			sb.append("<input type='radio'  name='rdoSecStatus' value=\"remove,"+teacherAssessmentStatus_Id+","+jobForTeacher.getJobForTeacherId()+"\">");
			sb.append("</td>");
			
			sb.append("<td class='tdAct'>&nbsp;Remove");
			sb.append("</td>");
			
			sb.append("</tr>");
		}
		List<SecondaryStatusMaster>  lstSecondaryStatusMaster = secondaryStatusMasterDAO.findActiveSecStaus();
		
		TeacherSecondaryStatus teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(jobForTeacher.getTeacherId(), jobForTeacher.getJobId(), userMaster.getSchoolId());
		
		for(SecondaryStatusMaster secondaryStatusMaster : lstSecondaryStatusMaster){
			if(teacherSecondaryStatus!=null && secondaryStatusMaster.equals(teacherSecondaryStatus.getSecondaryStatusMaster())){
				sb.append("<tr>");
				sb.append("<td class='tdAct'>");
				sb.append("<input type='radio'  "+disableText+"  name='rdoSecStatus' checked value='"+jobForTeacher.getJobForTeacherId()+","+secondaryStatusMaster.getSecStatusId()+","+jobForTeacher.getTeacherId().getTeacherId()+"'>");
				sb.append("</td>");
				sb.append("<td class='tdAct'>&nbsp;"+secondaryStatusMaster.getSecStatusName());
				sb.append("</td>");
				sb.append("</tr>");
			}
			else{
				sb.append("<tr>");
				sb.append("<td class='tdAct' >");
				sb.append("<input type='radio'  "+disableText+" name='rdoSecStatus' value='"+jobForTeacher.getJobForTeacherId()+","+secondaryStatusMaster.getSecStatusId()+","+jobForTeacher.getTeacherId().getTeacherId()+"'>");
				sb.append("</td>");
				sb.append("<td class='tdAct' >&nbsp;"+secondaryStatusMaster.getSecStatusName());
				sb.append("</td>");				
				sb.append("</tr>");
			}			
		}
		sb.append("</table>");		
		return sb.toString();
	}

	public Boolean saveMessage(int teacherId,String messageSubject,String messageSend,int jobId){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMasterAPI");
	
		TeacherDetail   teacherdetail =  teacherDetailDAO.findById(teacherId, false, false);
		JobOrder   jobOrder =  jobOrderDAO.findById(jobId, false, false);
		String to=teacherdetail.getEmailAddress();
		try{
			UserMaster userMaster = (UserMaster)session.getAttribute("userMasterAPI");
			MessageToTeacher  messageToTeacher= new MessageToTeacher();
			messageToTeacher.setTeacherId(teacherdetail);
			messageToTeacher.setJobId(jobOrder);
			messageToTeacher.setTeacherEmailAddress(to);
			messageToTeacher.setMessageSubject(messageSubject);
			messageToTeacher.setMessageSend(messageSend);
			messageToTeacher.setSenderId(userMaster);
			messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
			messageToTeacher.setEntityType(userMaster.getEntityType());
			messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
			messageToTeacherDAO.makePersistent(messageToTeacher);
			emailerService.sendMailAsHTMLText(to,messageSubject,MailText.messageForDefaultFont(messageSend,userSession));
		}catch(Exception e){
			e.printStackTrace();
		}
	
		return true;
	}

	public Boolean hireTeacher(String assessmentStatusId, String jobForTeacherId)
	{	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		boolean canHire = true;
		try 
		{
			List<SchoolMaster> lstSchoolMasters = null;
			SchoolInJobOrder schoolInJobOrder = null;
			DistrictMaster districtMaster = null;
			SchoolMaster schoolMaster = null;
			StatusMaster statusMaster = null;
			int noOfHire = 0;
			
			UserMaster userMaster = (UserMaster)session.getAttribute("userMasterAPI");
			JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
			JobOrder jobOrder = jobForTeacher.getJobId();
			if(userMaster.getEntityType()==2)//District - 2 user login type
			{
				districtMaster = userMaster.getDistrictId();				
				statusMaster= WorkThreadServlet.statusMap.get("hird");
				noOfHire = (jobForTeacherDAO.findHireByJobForDistrict(jobOrder, districtMaster)).size();
				
				lstSchoolMasters = jobOrder.getSchool();
				
				if(lstSchoolMasters!=null && lstSchoolMasters.size()>0)
				{						
					canHire = false;
				}
				else
				{	
					if(jobOrder.getNoOfExpHires()<=noOfHire)
					{	
						canHire = false;
					}
				}							
			}
			else
			{
				schoolMaster = userMaster.getSchoolId();				
				statusMaster= WorkThreadServlet.statusMap.get("hird");
				noOfHire = (jobForTeacherDAO.findHireByJobForSchool(jobOrder, schoolMaster)).size();
				lstSchoolMasters = jobOrder.getSchool();
				
				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMaster);
				
				if(schoolInJobOrder==null || schoolInJobOrder.getNoOfSchoolExpHires()==null || noOfHire>=schoolInJobOrder.getNoOfSchoolExpHires())
				{	
					canHire = false;
				}
			}

			
			if(canHire)
			{	
				statusMaster  = WorkThreadServlet.statusMap.get("hird");
				jobForTeacher.setStatus(statusMaster);
				if(statusMaster!=null){
					jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
				}
				jobForTeacher.setUpdatedBy(userMaster);
				jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
				jobForTeacher.setUpdatedDate(new Date());
				jobForTeacherDAO.makePersistent(jobForTeacher);
				
				TeacherSecondaryStatus teacherSecondaryStatus = null;
				teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(jobForTeacher.getTeacherId(), jobForTeacher.getJobId(), schoolMaster);
				if(teacherSecondaryStatus!=null)
				teacherSecondaryStatusDAO.makeTransient(teacherSecondaryStatus);
				
				/*if(jobForTeacher.getJobId().getCreatedForEntity().equals(3)){
					teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(jobForTeacher.getTeacherId(), jobForTeacher.getJobId(), schoolMaster);
				}
				else{
					teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(jobForTeacher.getTeacherId(), jobForTeacher.getJobId(), schoolMaster);
				}*/
				//TeacherSecondaryStatus teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(jobForTeacher.getTeacherId(), jobForTeacher.getJobId(), schoolMaster)
				
			}			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			canHire = false;
		}
		return canHire;
	}
	
	public String getPhoneDetail(String teacherId){
		
		
		
		StringBuffer sb = new StringBuffer("");
		try {
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(new Integer(teacherId), false, false);
			
			String phone = teacherPersonalInfo.getPhoneNumber()==null?"":teacherPersonalInfo.getPhoneNumber();
			String mobile = teacherPersonalInfo.getMobileNumber()==null?"":teacherPersonalInfo.getMobileNumber();
			
			if(teacherPersonalInfo !=null && (!(phone.trim().equals("") && mobile.trim().equals("")))){			
				sb.append("<table>");
				
				if(!phone.trim().equals("")){
					sb.append("<tr>");
					sb.append("<td>");
					sb.append("Phone Number:");
					sb.append("</td>");				
					sb.append("<td>");
					sb.append(""+phone);
					sb.append("</td>");
					sb.append("</tr>");
				}
				
				if(!mobile.trim().equals("")){
					sb.append("<tr>");
					sb.append("<td>");
					sb.append("Mobile Number:");
					sb.append("</td>");				
					sb.append("<td>");
					sb.append(""+mobile);
					sb.append("</td>");
					sb.append("</tr>");
				}
				
				sb.append("</table>");	
			}
				
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		sb.append("");
		
		return sb.toString();
	}
public String getCertificationGrid(int teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType){
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"certType";
			String sortOrderNoField		=	"certType";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("state")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("state")){
					 sortOrderNoField="state";
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			
			
			TeacherDetail teacherDetail =  teacherDetailDAO.findById(teacherId, false, false);;
			
			List<TeacherCertificate> listTeacherCertificates = null;
			listTeacherCertificates = teacherCertificateDAO.findSortedCertificateByTeacher(sortOrderStrVal,teacherDetail);
			
			List<TeacherCertificate> sortedlistTeacherCertificates		=	new ArrayList<TeacherCertificate>();
			
			SortedMap<String,TeacherCertificate>	sortedMap = new TreeMap<String,TeacherCertificate>();
			if(sortOrderNoField.equals("state"))
			{
				sortOrderFieldName	=	"state";
			}
			int mapFlag=2;
			for (TeacherCertificate trCertificate : listTeacherCertificates){
				String orderFieldName=trCertificate.getCertType();
				if(sortOrderFieldName.equals("state")){
					orderFieldName=trCertificate.getStateMaster().getStateName()+"||"+trCertificate.getCertId();
					sortedMap.put(orderFieldName+"||",trCertificate);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherCertificates=listTeacherCertificates;
			}
			
			totalRecord =sortedlistTeacherCertificates.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherCertificate> listsortedTeacherCertificates		=	sortedlistTeacherCertificates.subList(start,end);
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			
			String nationalBoard="No";
			if(teacherExperience!=null){
				if(teacherExperience.getNationalBoardCert()){
					nationalBoard="Yes";
				}
				sb.append("<table border='0' width='100%'>");
				sb.append("<tr>");
				sb.append("<td>Years of Certified Teaching Experience : "+teacherExperience.getExpCertTeacherTraining()+"</td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td>National Board Certification: "+nationalBoard+"</td>");
				sb.append("</tr>");
			
				if(teacherExperience.getNationalBoardCert()){	
					sb.append("<tr>");
					sb.append("<td>National Board Certification Year: "+teacherExperience.getNationalBoardCertYear()+"</td>");
					sb.append("</tr>");
				}
			}else{
				sb.append("<tr>");
				sb.append("<td>No Certification provided.</td>");
				sb.append("</tr>");
			}
			sb.append("<tr>");
			sb.append("<td  width='100%'><table border='0' width='100%' class='table table-bordered table-striped mt30'  >");
			
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");
			
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("State",sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
		
			responseText=PaginationAndSorting.responseSortingLink("Year received",sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
		
			responseText=PaginationAndSorting.responseSortingLink("Type",sortOrderFieldName,"certType",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			sb.append("</tr>");
			sb.append("</thead>");
			if(teacherCertificateDAO!=null)
			for(TeacherCertificate tCertificate:listsortedTeacherCertificates)
			{
				sb.append("<tr>");
				sb.append("<td>");
				sb.append(tCertificate.getStateMaster().getStateName());
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append(tCertificate.getYearReceived());
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append(tCertificate.getCertType());
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			if(listTeacherCertificates==null || listTeacherCertificates.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append("No Record Found.");
				sb.append("</td>");
				sb.append("</tr>");
			}
						
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	
	}
	public String getTranscriptGrid(String teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType){
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"attendedInYear";
			String sortOrderNoField		=	"attendedInYear";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("degree") && !sortOrder.equals("fieldOfStudy") && !sortOrder.equals("university")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("degree")){
					 sortOrderNoField="degree";
				 }
				 if(sortOrder.equals("fieldOfStudy")){
					 sortOrderNoField="fieldOfStudy";
				 }
				 if(sortOrder.equals("university")){
					 sortOrderNoField="university";
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			
			
			
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			
			List<TeacherAcademics> listTeacherAcademics = null;
			//listTeacherAcademics = teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
			listTeacherAcademics = teacherAcademicsDAO.findSortedAcadamicDetailByTeacher(sortOrderStrVal,teacherDetail);
			
			
			List<TeacherAcademics> sortedlistTeacherAcademics =	new ArrayList<TeacherAcademics>();
			
			SortedMap<String,TeacherAcademics>	sortedMap = new TreeMap<String,TeacherAcademics>();
			if(sortOrderNoField.equals("degree"))
			{
				sortOrderFieldName	=	"degree";
			}
			if(sortOrderNoField.equals("fieldOfStudy"))
			{
				sortOrderFieldName	=	"fieldOfStudy";
			}
			if(sortOrderNoField.equals("university"))
			{
				sortOrderFieldName	=	"university";
			}
			int mapFlag=2;
			for (TeacherAcademics trAcademics : listTeacherAcademics){
				String orderFieldName=trAcademics.getAttendedInYear()+"";
				if(sortOrderFieldName.equals("degree")){
					orderFieldName=trAcademics.getDegreeId().getDegreeName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("fieldOfStudy")){
					orderFieldName=trAcademics.getFieldId().getFieldName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("university")){
					orderFieldName=trAcademics.getUniversityId().getUniversityName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherAcademics=listTeacherAcademics;
			}
			
			totalRecord =sortedlistTeacherAcademics.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherAcademics> listsortedTeacherCertificates	=	sortedlistTeacherAcademics.subList(start,end);
			
			sb.append("<table border='0' class='table table-striped mt30'   id='tblTrans'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");			
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("Degree",sortOrderFieldName,"degree",sortOrderTypeVal,pgNo);
			sb.append("<th width='15%' valign='top'>"+responseText+"</th>");			
			responseText=PaginationAndSorting.responseSortingLink("Field of Study",sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
			sb.append("<th width='12%' valign='top'>"+responseText+"</th>");			
			responseText=PaginationAndSorting.responseSortingLink("Dates Attended",sortOrderFieldName,"attendedInYear",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");			
			responseText=PaginationAndSorting.responseSortingLink("School",sortOrderFieldName,"university",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");			
			responseText=PaginationAndSorting.responseSortingLink("Transcript",sortOrderFieldName,"pathOfTranscript",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			sb.append("<th width='15%'>");
			sb.append("GPA");
			sb.append("</th>");						
			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherAcademics!=null)
			for(TeacherAcademics ta:listsortedTeacherCertificates)
			{
				sb.append("<tr>");
				sb.append("<td>");
				sb.append(ta.getDegreeId().getDegreeName());;
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append(ta.getFieldId().getFieldName());	
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append(ta.getAttendedInYear()+" to "+ta.getLeftInYear());
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append(ta.getUniversityId().getUniversityName());	
				sb.append("</td>");
				
				String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				sb.append("<td>");								
				sb.append(ta.getPathOfTranscript()==null?"":"<a href='javascript:void(0);' id='trans"+ta.getAcademicId()+"' onclick=\"downloadTranscript('"+ta.getAcademicId()+"');\">"+ta.getPathOfTranscript()+"</a>");
				sb.append("</td>");
				
				sb.append("<td>");
				if(ta.getDegreeId().getDegreeType().trim().equalsIgnoreCase("b"))
				{
					sb.append("Freshman: "+(ta.getGpaFreshmanYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaFreshmanYear()))+"<br> " +
						" Sophomore: "+(ta.getGpaSophomoreYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaSophomoreYear()))+"<br>" +
						" Junior: "+(ta.getGpaJuniorYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaJuniorYear()))+"<br>" +
						" Senior: "+(ta.getGpaSeniorYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaSeniorYear()))+"<br>" +
						" Cumulative: "+(ta.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaCumulative())));
				}
				else
				{
					sb.append("Cumulative:"+(ta.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaCumulative())));
				}
				sb.append("</td>");				
				sb.append("</tr>");
			}
			
			if(listTeacherAcademics==null || listTeacherAcademics.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append("No Record Found.");
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	
	}
	
	public String downloadTranscript(String academicId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }

		String path="";
		
		try 
		{
			
			
			TeacherAcademics teacherAcademics = teacherAcademicsDAO.findById(new Long(academicId), false, false);
			
			TeacherDetail teacherDetail = teacherAcademics.getTeacherId();
			
			String fileName= teacherAcademics.getPathOfTranscript();
			
	        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;
	        
	        
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
	        
	        
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        
	        FileUtils.copyFile(sourceFile, targetFile);
	        	        
	        
	        path = Utility.getBaseURL(request)+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	        
	        
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return path;
	
	}
	
	
	public Boolean unHireTeacher(String assessmentStatusId, String jobForTeacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		
		Boolean canUnhire = true;
		try 
		{
			DistrictMaster loginDistrict = null;
			SchoolMaster loginSchool = null;
			
			DistrictMaster hireDistrict = null;
			SchoolMaster hireSchool = null;
			
			UserMaster userMaster = (UserMaster)session.getAttribute("userMasterAPI");
			JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
			
			if(userMaster.getEntityType()==2)//District - 2 user login type
			{
				loginDistrict = userMaster.getDistrictId();	
				
				if(jobForTeacher.getUpdatedBy().getUserId().equals(userMaster.getUserId()))
				{
					if(!(loginDistrict.getDistrictId().equals(jobForTeacher.getUpdatedBy().getDistrictId().getDistrictId())))
					{
						
						canUnhire = false;
					}
				}
				else
				{
					
					canUnhire = false;
				}
			}
			else
			{
				loginSchool = userMaster.getSchoolId();
				if(!loginSchool.getSchoolId().equals(jobForTeacher.getUpdatedBy().getSchoolId().getSchoolId()))
				{
					
					canUnhire = false;
				}
			}
			
			if(canUnhire)
			{
				StatusMaster statusMaster  = null;
				boolean isAssesmetnStatus = jobForTeacher.getJobId().getIsJobAssessment();
				if(isAssesmetnStatus)
				{	
					List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(), jobForTeacher.getJobId());
					if(lstTeacherAssessmentStatus==null || lstTeacherAssessmentStatus.size()==0)
					{
						
						statusMaster  = WorkThreadServlet.statusMap.get("icomp");
					}
					else
					{
						
						TeacherAssessmentStatus teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
						statusMaster = teacherAssessmentStatus.getStatusMaster();						
					}
					
				}
				else
				{
					
					statusMaster  = WorkThreadServlet.statusMap.get("comp");
				}
				if(statusMaster!=null){
					jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
				}
				jobForTeacher.setStatus(statusMaster);
				jobForTeacher.setUpdatedBy(userMaster);
				jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
				jobForTeacher.setUpdatedDate(new Date());
				jobForTeacherDAO.makePersistent(jobForTeacher);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			canUnhire = false;
		}
		return canUnhire;
	}
	
	public String removeTeacher(String assessmentStatusId, String jobForTeacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try 
		{
			
			UserMaster userMaster = (UserMaster)session.getAttribute("userMasterAPI");
			
			JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
			StatusMaster statusMaster  = WorkThreadServlet.statusMap.get("rem");
			jobForTeacher.setStatus(statusMaster);
			if(statusMaster!=null){
				jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
			}
			jobForTeacher.setUpdatedBy(userMaster);
			jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
			jobForTeacher.setUpdatedDate(new Date());
			jobForTeacherDAO.makePersistent(jobForTeacher);
		
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public String downloadResume(String teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String path="";
		
		try 
		{
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			
			String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+teacherExperience.getResume();
	        if(teacherExperience==null)
	        	return "";
	        
	        String resume = teacherExperience.getResume();
	        if(resume==null || resume.trim().equals(""))
	        	return "";
	        
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
	        
	        
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        
	        FileUtils.copyFile(sourceFile, targetFile);
	        
	        
	        
	        path = Utility.getBaseURL(request)+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	        
	        
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			path="";
		}
		
		return path;
	}
	
	public String downloadCandidateGridReport(String newJobId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
	
		try
		{	
			Font font9 = null;
			Font font9bold = null;
			Font font11 = null;
			Font font11bold = null;

			try {
				String fontPath = request.getRealPath("/");
				BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

				font9 = new Font(tahoma, 9);
				font9bold = new Font(tahoma, 9, Font.BOLD);
				font11 = new Font(tahoma, 11);
				font11bold = new Font(tahoma, 11,Font.BOLD);


			} 
			catch (DocumentException e1) 
			{
				e1.printStackTrace();
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}
			DistrictMaster districtMaster = null;

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMasterAPI") == null) {
				//return "false";
			
			}else
			{
				userMaster = (UserMaster)session.getAttribute("userMasterAPI");
				districtMaster  = userMaster.getDistrictId();
			}

			int userId = userMaster.getUserId();

			JobOrder jobOrder = jobOrderDAO.findJobByApiJobId(newJobId,districtMaster);

			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			
			generateCandidateReport(userMaster, jobOrder, basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
			return "../user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}
	
	public String downloadPortfolioReport(String teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String path="";
		
		try 
		{
			
			
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			
			TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			
			if(!teacherPortfolioStatus.getIsAffidavitCompleted()){
				return "";
			}
			
			String sourceDir = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+""+teacherDetail.getTeacherId()+"/";
			File file = new File(sourceDir);
			
			if(!file.exists())
			{
				file.mkdirs();
			}
			
			
			path = candidateReportService.generatePortfolioReport(teacherDetail,request,sourceDir+"/"+"portfolio.pdf");
			
			return Utility.getBaseURL(request)+"teacher/"+""+teacherDetail.getTeacherId()+"/"+"portfolio.pdf";		
				        
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return path;
	}
	
	public String getCoverLetter(String teacherId, String apiJobId)
	{
		String coverLetter="";
		try 
		{
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			JobOrder jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId);			
			JobForTeacher jobForTeacher = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail,jobOrder);
			if(jobForTeacher.getCoverLetter()!=null)
				coverLetter=jobForTeacher.getCoverLetter();
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return coverLetter;
	}
	
	public String getTranscriptText(String teacherId)
	{	
		StringBuffer sb = new StringBuffer("");
		try {
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	public String getNotesDetail(String teacherId, String pageNo, String noOfRow,String sortOrder,String sortOrderType)
	{
		StringBuffer sb = new StringBuffer("");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
	
		try {			
			HttpSession session = request.getSession();
			UserMaster userMaster = (UserMaster) session.getAttribute("userMasterAPI");
			int roleId=0;
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			String roleAccess=null;
			try
			{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			//-- set start and end position			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord = 0;
			
			String sortOrderFieldName	=	"notesOrder";
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			//------------------------------------
			
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			List<TeacherNotes> lstTeacherNotes = null;
			
			if(userMaster.getEntityType().equals(1)){
				
				lstTeacherNotes = teacherNotesDAO.findByTeacher(teacherDetail,sortOrderStrVal,start,noOfRowInPage);
				totaRecord = teacherNotesDAO.getRowcountByTeacher(teacherDetail);	
			}
			else if(userMaster.getEntityType().equals(2)){//For District
				
				lstTeacherNotes = teacherNotesDAO.findByTeacherAndDistrict(teacherDetail,userMaster.getDistrictId(),sortOrderStrVal,start,noOfRowInPage);
				totaRecord = teacherNotesDAO.getRowcountByTeacherAndDistrict(teacherDetail,userMaster.getDistrictId());				
			}
			else if(userMaster.getEntityType().equals(3)){//For School
				
				lstTeacherNotes = teacherNotesDAO.findByTeacherAndSchool(teacherDetail,userMaster.getSchoolId(),sortOrderStrVal,start,noOfRowInPage);
				totaRecord = teacherNotesDAO.getRowcountByTeacherAndSchool(teacherDetail,userMaster.getSchoolId());
			}
			
			sb.append("<table border='0' class='table table-bordered table-striped'  id='tblNotes'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th>"+PaginationAndSorting.responseSortingLink("Date",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo)+"</th>");
			sb.append("<th>"+PaginationAndSorting.responseSortingLink("Submitted By",sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo)+"</th>");
			sb.append("<th>"+PaginationAndSorting.responseSortingLink("Notes",sortOrderFieldName,"note",sortOrderTypeVal,pgNo)+"</th>");
			String locale = Utility.getValueOfPropByKey("locale");
			sb.append("<th>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			String submitedBy = "";
			if(lstTeacherNotes !=null)
			for(TeacherNotes teacherNotes : lstTeacherNotes){
				submitedBy = teacherNotes.getNoteCreatedByUser().getFirstName() +" "+teacherNotes.getNoteCreatedByUser().getLastName();
				
				
				sb.append("<tr>");
				sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(teacherNotes.getCreatedDateTime())+"</td>");
				sb.append("<td>"+submitedBy+"</td>");
				sb.append("<td>"+teacherNotes.getNote()+"</td>");
				if(userMaster.getEntityType().equals(1)){
					
					sb.append("<td><a href='javascript:void(0);' onclick=\"return editNoted('"+teacherNotes.getNoteId()+"','view')\" >View</a></td>");
				}
				else if(userMaster.getEntityType().equals(2)){
					
					if(teacherNotes.getNoteCreatedByUser().getSchoolId()!=null){
						sb.append("<td><a href='javascript:void(0);' onclick=\"return editNoted('"+teacherNotes.getNoteId()+"','view')\" >View</a></td>");
					}
					else{
						if(roleAccess.indexOf("|2|")!=-1){
							sb.append("<td><a href='javascript:void(0);' onclick=\"return editNoted('"+teacherNotes.getNoteId()+"')\" >Edit</a></td>");
						}else{
							sb.append("<td><a href='javascript:void(0);' onclick=\"return editNoted('"+teacherNotes.getNoteId()+"','view')\" >View</a></td>");					
						}
					}
				}
				else if(userMaster.getEntityType().equals(3)){
					
					if(roleAccess.indexOf("|2|")!=-1){
						sb.append("<td><a href='javascript:void(0);' onclick=\"return editNoted('"+teacherNotes.getNoteId()+"')\" >Edit</a></td>");
					}else{
						sb.append("<td><a href='javascript:void(0);' onclick=\"return editNoted('"+teacherNotes.getNoteId()+"','view')\" >View</a></td>");					
					}
				}								
				sb.append("</tr>");
			}
			if(lstTeacherNotes == null || lstTeacherNotes.size()==0){
				sb.append("<tr>");				
				sb.append("<td colspan=4>No record found.</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationString(request,totaRecord,noOfRow, pageNo));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String saveNotes(String teacherId, String note, String noteId)
	{	
		
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userMaster = (UserMaster) session.getAttribute("userMasterAPI");
		try{
			
			TeacherNotes tNotes = null;
			try {
				tNotes = teacherNotesDAO.findById(new Integer(noteId), false, false);				
			} catch (Exception e) {
				
			}
			
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			if(tNotes==null){				
				TeacherNotes teacherNotes = new TeacherNotes();
				
				teacherNotes.setTeacherDetail(teacherDetail);
				teacherNotes.setNote(note);
				teacherNotes.setNotesOrder(0);
				teacherNotes.setNoteCreatedByUser(userMaster);
				teacherNotes.setNoteCreatedByEntity(userMaster.getEntityType());			
				teacherNotes.setStatus("A");
				teacherNotes.setIpaddress(IPAddressUtility.getIpAddress(request));
				teacherNotes.setCreatedDateTime(new Date());
				
				teacherNotesDAO.makePersistent(teacherNotes);
				teacherNotes.setNotesOrder(teacherNotes.getNoteId());
				teacherNotesDAO.makePersistent(teacherNotes);
			}
			else{
				if(!note.equals(tNotes.getNote())){
					
					tNotes.setStatus("I");
					teacherNotesDAO.makePersistent(tNotes);
					
					TeacherNotes teacherNotes = new TeacherNotes();
					
					teacherNotes.setTeacherDetail(teacherDetail);
					teacherNotes.setNote(note);
					teacherNotes.setNotesOrder(tNotes.getNotesOrder());
					teacherNotes.setParentNoteId(tNotes);
					teacherNotes.setNoteCreatedByUser(userMaster);
					teacherNotes.setNoteCreatedByEntity(userMaster.getEntityType());			
					teacherNotes.setStatus("A");
					teacherNotes.setIpaddress(IPAddressUtility.getIpAddress(request));
					teacherNotes.setCreatedDateTime(new Date());
					
					teacherNotesDAO.makePersistent(teacherNotes);
				}
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "Hello saveNotes";
	}
	
	public TeacherNotes showEditNotes(String notesId)
	{
		TeacherNotes teacherNotes = null;
		try {
			teacherNotes = teacherNotesDAO.findById(new Integer(notesId), false, false);
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherNotes;
	}
	
	public String saveOrUpdateTeacherSecStatus(String jobForTeacherId, String secStatusId, String teacherId)
	{
		
		
		JobForTeacher jobForTeacher = null;
		SecondaryStatusMaster secondaryStatusMaster = null;
		TeacherDetail teacherDetail = null;
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		if(session == null ||(session.getAttribute("userMasterAPI")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userMaster = (UserMaster) session.getAttribute("userMasterAPI");
		try {
			jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
			secondaryStatusMaster = secondaryStatusMasterDAO.findById(new Integer(secStatusId), false, false);
			teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			
			TeacherSecondaryStatus teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(teacherDetail, jobForTeacher.getJobId(),userMaster.getSchoolId());
			
			
			
			
			if(teacherSecondaryStatus==null){
				teacherSecondaryStatus = new TeacherSecondaryStatus();
				teacherSecondaryStatus.setTeacherDetail(teacherDetail);
				teacherSecondaryStatus.setJobOrder(jobForTeacher.getJobId());
				teacherSecondaryStatus.setSecondaryStatusMaster(secondaryStatusMaster);
				teacherSecondaryStatus.setCreatedByUser(userMaster);
				teacherSecondaryStatus.setCreatedByEntity(userMaster.getEntityType());
				teacherSecondaryStatus.setDistrictMaster(userMaster.getDistrictId());
				teacherSecondaryStatus.setSchoolMaster(userMaster.getSchoolId());
				teacherSecondaryStatus.setCreatedDateTime(new Date());
			}
			else{
				teacherSecondaryStatus.setSecondaryStatusMaster(secondaryStatusMaster);
			}
				
			
			
			
			teacherSecondaryStatusDAO.makePersistent(teacherSecondaryStatus);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "Hello";
	}
	
	
	
	
	
	
	public int getPercentScoreOfBasesAssement(DomainMaster domainMaster, TeacherDetail teacherDetail)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		double sum=0;
		double maxMark=0;
		double percent=0;
	
		try 
		{
			Object obj[] = (Object[]) mapDomainScore.get(domainMaster.getDomainId()).get(teacherDetail.getTeacherId());			
			sum = obj[1]==null?0:(Double)obj[1];				
			maxMark = obj[2]==null?0:(Double)obj[2];
			percent = sum/maxMark*100;		
			
			
		}		
		catch (Exception e) 
		{
			//System.out.println();
			//e.printStackTrace();
		}
		
		return (int) Math.round(percent);
	}
	
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to generate PDReport.
	 */
	public String generatePDReport(Integer teacher)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try{
			
	

			String path = request.getContextPath();
			String urlRootPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

			TeacherDetail teacherDetail = null;
			
			teacherDetail = teacherDetailDAO.findById(teacher, false, false);
			int teacherId = teacherDetail.getTeacherId();

			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/candidatereports/"+teacherId;
			
			String fileName =time+"PDReport.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);

			
			return urlRootPath+"candidatereports/"+teacherId+"/"+fileName;
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "Not available";
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to check Pilar Report.
	 */
	public String generatePilarReport(Integer teacher,JobOrder jobOrder)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try{
			
			TeacherDetail teacherDetail = null;
			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
			
			
			if(jobOrder!=null)
			{
				if(jobOrder.getIsJobAssessment()!=true)
				{
					return "0";
				}
			}
			
			teacherDetail = teacherDetailDAO.findById(teacher, false, false);

			TeacherAssessmentStatus teacherAssessmentStatus = null;
			java.util.List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;

			teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail, jobOrder);

			if(teacherAssessmentStatusList.size()>0)
				teacherAssessmentStatus = teacherAssessmentStatusList.get(0);

			
			String status = "";
			if(teacherAssessmentStatus==null)
			{
				return "1";
			}else
			{
				
				status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();
				
				if(status.equalsIgnoreCase("icomp"))
				{
					return "1";
				}else if(status.equalsIgnoreCase("vlt"))
				{
					return "2";
				}
			}
			
			return "3";
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "Not available";
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to generate JSI Report.
	 */
	public String generateJSAReport(Integer teacher,JobOrder jobOrder)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try{

			
			String path = request.getContextPath();
			String urlRootPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

			TeacherDetail teacherDetail = null;
			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
			
			
			
			teacherDetail = teacherDetailDAO.findById(teacher, false, false);
			
			int teacherId = teacherDetail.getTeacherId();

			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/candidatereports/"+teacherId;
			
			String fileName =time+"JSIReport.pdf";

			TeacherAssessmentStatus teacherAssessmentStatus = null;
			TeacherAssessmentdetail teacherAssessmentdetail = null;
			java.util.List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;

			teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail, jobOrder);

			if(teacherAssessmentStatusList.size()>0)
				teacherAssessmentStatus = teacherAssessmentStatusList.get(0);

			
			String status = "";
			if(teacherAssessmentStatus==null)
			{
				return "1";
			}else
			{
				
				status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();
				
				teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();
				if(status.equalsIgnoreCase("icomp"))
				{
					return "1";
				}else if(status.equalsIgnoreCase("vlt"))
				{
					return "2";
				}
			}
			
			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			
			if(teacherAssessmentdetail!=null)
				candidateReportService.generatePilarReport(request,jobOrder,teacherDetail,teacherAssessmentdetail, basePath,fileName,urlRootPath,teacherAssessmentStatus);

			
			return urlRootPath+"candidatereports/"+teacherId+"/"+fileName;
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "Not available";
	
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to checkBaseCompeleted.
	 */
	public String checkBaseCompeleted(Integer teacher)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try{

			TeacherDetail teacherDetail = null;

			teacherDetail = teacherDetailDAO.findById(teacher, false, false);
			List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
			TeacherAssessmentStatus teacherAssessmentStatus = null;
			JobOrder jobOrder = new JobOrder();
			jobOrder.setJobId(0);
			teacherAssessmentStatusList=teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jobOrder);
			
			if(teacherAssessmentStatusList.size()!=0)
			{
				teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
				if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
				{
					return "1";
				}else if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
				{
					return "3";
				}
			}



		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "2";
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to notify TM Admins.
	 */
	public String notifyTMAdmins(Integer teacher)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMasterAPI")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try{

			

			TeacherDetail teacherDetail = null;

			teacherDetail = teacherDetailDAO.findById(teacher, false, false);
			emailerService.sendMailAsHTMLText("clientservices@teachermatch.net", "Request for Professional Development (PD) report",MailText.getTeacherPDReportMailText(request,teacherDetail));
			//emailerService.sendMailAsHTMLText("vishwanath@netsutra.com", "Request for Professional Development (PD) report",MailText.getTeacherPDReportMailText(request,teacherDetail));

			return "1";

		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "0";
	}
	
	public boolean generateCandidateReport(UserMaster userMaster,JobOrder jobOrder, String path,String realPath)
	{
		System.out.println("---");
		
		Integer districtIdForSpecific = 3702970;
		//Integer districtIdForSpecific = 100002;
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		boolean baseStatusFlag=false;
		if(jobCategoryMasterDAO.baseStatusChecked(jobOrder.getJobCategoryMaster().getJobCategoryId())){
			baseStatusFlag=true;
		}
		
		
		Map<String,RawDataForDomain> mapRawDomainScore = null;
		double[] compositeTScoreMeanJob = null;
		double[] compositeTScoreMeanNational = null;
		List<JobForTeacher> lstJobForTeacher = null;
		List<JobForTeacher> lstJobForTeacherVlt = null;
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		int roleId=0;
		if(session.getAttribute("userMasterAPI")==null){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMasterAPI");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}

		TeacherAssessmentStatus assessmentStatus = null;
		StatusMaster status = null;
		RawDataForDomain rawDataForDomain = null;
		
		PercentileScoreByJob pScoreByJob = null;
		String key = "";
		
		double domainTotatlScore = 0;
		double domainTotalNational = 0;
		//double displayScore = 0.0;
		double tScoreNW = 0.0;
		double tScoreJob= 0;
		StringBuffer sb = new StringBuffer();	
		double[] meanArray=null;
		int noOfRecordCheck = 0;
		double composite = 0;
		DecimalFormat oneDForm = new DecimalFormat("###,###.0");
		lstJobForTeacherVlt = new ArrayList<JobForTeacher>();

		try
		{	
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;

			try {
				String fontPath = realPath;
				BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
				font8 = new Font(tahoma, 8);

				font8Green = new Font(tahoma, 8);
				font8Green.setColor(Color.BLUE);
				font8bold = new Font(tahoma, 8, Font.BOLD);
				
				font9 = new Font(tahoma, 9);
				font9bold = new Font(tahoma, 9, Font.BOLD);
				font10 = new Font(tahoma, 10);
				font10bold = new Font(tahoma, 10, Font.BOLD);
				font11 = new Font(tahoma, 11);
				font11bold = new Font(tahoma, 11,Font.BOLD);
				font20bold = new Font(tahoma, 20,Font.BOLD);
			} 
			catch (DocumentException e1) 
			{
				e1.printStackTrace();
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}
						
			//***************************************************************************************************************************************
			
			String tFname = "";
			String tLname = "";
			String roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			meanArray = new double[lstDomain.size()];


			TeacherAssessmentStatus teAssesStatusTempBase = null;
			TeacherAssessmentStatus teAssesStatusTempJSI = null;
			
			StatusMaster statusComp = WorkThreadServlet.statusMap.get("comp");
			StatusMaster statusIcomp = WorkThreadServlet.statusMap.get("icomp");
			StatusMaster statusHired = WorkThreadServlet.statusMap.get("hird");
			StatusMaster statusRemove = WorkThreadServlet.statusMap.get("rem");
			StatusMaster statusVlt = WorkThreadServlet.statusMap.get("vlt");

			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			lstStatusMasters.add(statusComp);
			lstStatusMasters.add(statusIcomp);
			lstStatusMasters.add(statusHired);
			lstStatusMasters.add(statusRemove);			

			
			lstJobForTeacher = jobForTeacherDAO.findJFTbyJobOrderForCG(jobOrder,lstStatusMasters);
			List<JobForTeacher> lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacher);
			List<TmpPercentileWiseZScore> tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreDAO.findByCriteria(Order.asc("zScore"));
			List<PercentileZscoreTscore> pztList=cGReportService.populateZScoreMap(tmpPercentileWiseZScoreList);

			//--------------------------------------------------set global map start----------------------------------			
			Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();		
			Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
			List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
			List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
			for (JobForTeacher jbforteacher : lstJobForTeacher) 
				teacherDetails.add(jbforteacher.getTeacherId());
			
			if(teacherDetails!=null && teacherDetails.size()>0){
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetails);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
					if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
						isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
					}
				}
			}
			
			Map<String, PercentileCalculation> mapNationalPercentile = new HashMap<String, PercentileCalculation>();
			Map<String, PercentileScoreByJob> mapJobPercentile = new HashMap<String, PercentileScoreByJob>();
			
			List<PercentileCalculation> lstPercentileCalculation = percentileCalculationDAO.findAllInCurrenctYear();
			for(PercentileCalculation percentileCalculation : lstPercentileCalculation){
				mapNationalPercentile.put(percentileCalculation.getDomainMaster().getDomainId()+"##"+percentileCalculation.getScore(), percentileCalculation);
			}			
			
			List<PercentileScoreByJob> lstPercentileScoreByJobs = percentileScoreByJobDAO.findPercentileScoreByJob(jobOrder);
			for(PercentileScoreByJob percentileScoreByJob : lstPercentileScoreByJobs){
				mapJobPercentile.put(percentileScoreByJob.getDomainMaster().getDomainId()+"##"+percentileScoreByJob.getScore(), percentileScoreByJob);
			}			
			
			mapRawDomainScore = new HashMap<String, RawDataForDomain>();
			List<RawDataForDomain> lstRawDataForDomain = null;
			for(DomainMaster dm: lstDomain){				
				lstRawDataForDomain = rawDataForDomainDAO.findByDomainAndTeachers(dm, teacherDetails);
				for(RawDataForDomain rdm : lstRawDataForDomain){
					mapRawDomainScore.put(rdm.getDomainMaster().getDomainId()+"##"+rdm.getTeacherDetail().getTeacherId(), rdm);
				}
			}

			List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByJobAndSchool(jobOrder, userMaster.getSchoolId());
			Map<Integer,TeacherSecondaryStatus> mapTSecStatus = new HashMap<Integer, TeacherSecondaryStatus>();
			for(TeacherSecondaryStatus tSecondaryStatus: lstTeacherSecondaryStatus ){
				mapTSecStatus.put(tSecondaryStatus.getTeacherDetail().getTeacherId(), tSecondaryStatus);
			}	

			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI = teacherAssessmentStatusDAO.findAssessmentTaken(null, jobOrder);
			for(TeacherAssessmentStatus ts : lstJSI){
				mapJSI.put(ts.getTeacherDetail().getTeacherId(), ts);
			}
			//--------------------------------------------------set global map end----------------------------------
			for(JobForTeacher jft : lstJFTremove){				
				teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());				
				teAssesStatusTempJSI =mapJSI.get(jft.getTeacherId().getTeacherId()); 

				if((teAssesStatusTempBase==null && baseStatusFlag) || (baseStatusFlag && teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))){
					lstJobForTeacher.remove(jft);
				}
				if((teAssesStatusTempBase!=null && baseStatusFlag) && (teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))){	
					lstJobForTeacherVlt.add(jft);
					lstJobForTeacher.remove(jft);
				}
				else if(teAssesStatusTempJSI!=null && teAssesStatusTempJSI.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
					lstJobForTeacherVlt.add(jft);
					lstJobForTeacher.remove(jft);					
				}
			}

			//------------Check user can hire, unhire or remove start

			DistrictMaster districtMaster = null;
			StatusMaster statusMaster = null;
			List<SchoolMaster> lstSchoolMaster =null;
			SchoolMaster schoolMaster = null;
			SchoolInJobOrder schoolInJobOrder = null;
			//userMaster = (UserMaster)session.getAttribute("userMasterAPI");
			boolean doActivity = true;
			if(userMaster.getEntityType()==2)//District - 2 user login type
			{
				districtMaster = userMaster.getDistrictId();
				lstSchoolMaster = jobOrder.getSchool();

				if(lstSchoolMaster!=null && lstSchoolMaster.size()>0){										
					doActivity= false;
				}

				if(jobOrder.getCreatedForEntity().equals(3)){
					doActivity= false;
				}				
			}
			else{
				schoolMaster = userMaster.getSchoolId();				
				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMaster);
				if(schoolInJobOrder==null){					
					doActivity = false;
				}
			}

			int k=0;
			compositeTScoreMeanJob = new double[lstJobForTeacher.size()];
			compositeTScoreMeanNational = new double[lstJobForTeacher.size()];
			for(JobForTeacher jft:lstJobForTeacher){		
				domainTotatlScore=0.0;
				domainTotalNational=0.0;
				tScoreJob = 0.0;
				tScoreNW = 0.0;
				
				for(DomainMaster domain: lstDomain){					
					key = domain.getDomainId()+"##"+jft.getTeacherId().getTeacherId();
					rawDataForDomain =  mapRawDomainScore.get(key);
					key = domain.getDomainId()+"##"+Math.round(rawDataForDomain.getScore());
					
					if(mapNationalPercentile.get(key)!=null)
						tScoreNW = mapNationalPercentile.get(key).gettValue();						
					else						
						tScoreJob = 0.0;
					
					if(mapJobPercentile.get(key)!=null)
						tScoreJob =  mapJobPercentile.get(key).gettValue();
					else
						tScoreJob = 0.00;
			
					domainTotatlScore = domainTotatlScore+tScoreJob*domain.getMultiplier();
					domainTotalNational = domainTotalNational+tScoreNW*domain.getMultiplier();
				}
				
				jft.setCompositeScore(domainTotatlScore);
				jft.setNormScore(domainTotalNational);				
				compositeTScoreMeanJob[k]=domainTotatlScore;
				compositeTScoreMeanNational[k]=domainTotalNational;
				k++;
			}
			
			//-- sorting CG view start
			List<JobForTeacher> lstjft = new ArrayList<JobForTeacher>(lstJobForTeacher);
			List<JobForTeacher> lstremoveJFT = new ArrayList<JobForTeacher>();
			for(JobForTeacher jft: lstjft){
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					lstJobForTeacher.remove(jft);
					lstremoveJFT.add(jft);
				}
			}
			Collections.sort(lstremoveJFT,JobForTeacher.jobForTeacherComparator);
			lstJobForTeacher.addAll(lstremoveJFT);
			//-- sorting CG view end

			TeacherSecondaryStatus teacherSecondaryStatus = null;
			String teacherSecStatus = "";
			String remCol="";



			int teacherAssessmentStatusId=0;


			List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();			
			for(JobForTeacher jft:lstJobForTeacher){
				lstTeacherDetails.add(jft.getTeacherId());
			}
			
			
			
			
			//***************************************************************************************************************************************
			
			



			document = new Document(PageSize.A4,10f,10f,10f,10f);		
			document.addAuthor("TeacherMatch");
			document.addCreator("TeacherMatch Inc.");
			document.addSubject("Candidate Grid Report");
			document.addCreationDate();
			document.addTitle("CANDIDATE GRID REPORT");

			fos = new FileOutputStream(path);
			PdfWriter.getInstance(document, fos);

			footerpara = new Paragraph("Generated by TeacherMatch: Created on - "+new Date(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 6,Font.BOLDITALIC,Color.black));
			headerFooter = new HeaderFooter(footerpara,false);
			headerFooter.setAlignment(HeaderFooter.ALIGN_RIGHT);
			headerFooter.setBorder(0);
			document.setFooter(headerFooter);
			document.open();

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			Paragraph [] para = null;
			PdfPCell [] cell = null;


			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20bold);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);


			Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/TMatch-Logo.png");
			logo.scalePercent(75);

			cell[1]= new PdfPCell(logo);
			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[1].setBorder(0);

			para[2] = new Paragraph("CANDIDATE GRID REPORT",font20bold);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);

			for(int i=0;i<2;i++)
				mainTable.addCell(cell[0]);

			mainTable.addCell(cell[1]);
			for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);
			mainTable.addCell(cell[2]);
			mainTable.addCell(cell[0]);

			if(baseStatusFlag){
				cellSize = lstDomain.size()+4;
			}else{
				cellSize=2;
			}
			
			if(jobOrder.getDistrictMaster().getDistrictId().equals(districtIdForSpecific)){
				cellSize=cellSize-1;
			}
			
			document.add(mainTable);

			float[] tblwidth={.07f,.83f};
			mainTable = new PdfPTable(tblwidth);
			mainTable.setWidthPercentage(90);
			para = new Paragraph[2];
			cell = new PdfPCell[2];

			para[0] = new Paragraph("Job Title:",font8bold);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].setBorder(0);
			mainTable.addCell(cell[0]);

			para[1] = new Paragraph(""+jobOrder.getJobTitle(),font8bold);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);
			mainTable.addCell(cell[1]);

			document.add(mainTable);

			int cellCount=0;

			mainTable = new PdfPTable(cellSize);
			mainTable.setWidthPercentage(90);
			para = new Paragraph[cellSize];
			cell = new PdfPCell[cellSize];

			para[cellCount] = new Paragraph("Candidate",font8bold);
			cell[cellCount] = new PdfPCell(para[cellCount]);
			cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[cellCount].enableBorderSide(1);
			mainTable.addCell(cell[cellCount]);
			cellCount++;

			if(baseStatusFlag)
				for(DomainMaster domain:lstDomain)
				{				
					para[cellCount] = new Paragraph(""+domain.getDomainName(),font8bold);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
					cell[cellCount].enableBorderSide(1);
					mainTable.addCell(cell[cellCount]);
					cellCount++;
				}

			if(baseStatusFlag){
				para[cellCount] = new Paragraph("Composite Score",font8bold);
				cell[cellCount] = new PdfPCell(para[cellCount]);
				cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[cellCount].enableBorderSide(1);
				mainTable.addCell(cell[cellCount]);
				cellCount++;
			}
			
			if(baseStatusFlag){
				para[cellCount] = new Paragraph("Norm Score",font8bold);
				cell[cellCount] = new PdfPCell(para[cellCount]);
				cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[cellCount].enableBorderSide(1);
				mainTable.addCell(cell[cellCount]);
				cellCount++;
			}

			
			if(!jobOrder.getDistrictMaster().getDistrictId().equals(districtIdForSpecific)){
				para[cellCount] = new Paragraph("Status",font8bold);
				cell[cellCount] = new PdfPCell(para[cellCount]);
				cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
	
				cell[cellCount].enableBorderSide(1);
				mainTable.addCell(cell[cellCount]);
				cellCount++;
			}

			TeacherAssessmentStatus teAssesStatusTemp = null;			


			cellCount = 0;
			double score = 0;
			String statusShow="";
			Color color = null;

			meanArray = new double[lstDomain.size()];
			
			Collections.sort(lstJobForTeacher,JobForTeacher.jobForTeacherComparator);
			for(JobForTeacher jft:lstJobForTeacher)
			{				
				assessmentStatus = baseTakenMap.get(jft.getTeacherId().getTeacherId());
				teacherSecondaryStatus = mapTSecStatus.get(jft.getTeacherId().getTeacherId());
				
				if((teacherSecondaryStatus!=null) && (!userMaster.getEntityType().equals(1))){
					teacherSecStatus = teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusName();
				}
				else{
					teacherSecStatus="";
				}

				cellCount=0;
				if(jft.getTeacherId()!=null)
				{
					tFname = jft.getTeacherId().getFirstName()==null?"":jft.getTeacherId().getFirstName();
					tLname = jft.getTeacherId().getLastName()==null?"":jft.getTeacherId().getLastName();
				}

				para[cellCount] = new Paragraph(""+tFname+" "+tLname+" ",font8);
				cell[cellCount] = new PdfPCell(para[cellCount]);
				cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					cell[cellCount].setBackgroundColor(Color.GRAY);
				}
				cell[cellCount].enableBorderSide(1);
				if(!teacherSecStatus.trim().equals("")){									
					Chunk chunk = new Chunk("\n"+teacherSecStatus, font8Green);					
					para[cellCount].add(chunk);
				}
				mainTable.addCell(cell[cellCount]);
				cellCount++;

				domainTotatlScore=0;
				int i = 0;
				String colorName = "";
				PercentileCalculation pCalculationObj=null;
				if(baseStatusFlag){
					for(DomainMaster domain: lstDomain)
					{	
						key = domain.getDomainId()+"##"+jft.getTeacherId().getTeacherId();
						rawDataForDomain =  mapRawDomainScore.get(key);
						key = domain.getDomainId()+"##"+Math.round(rawDataForDomain.getScore());						
						pScoreByJob = mapJobPercentile.get(key);
						pCalculationObj=mapNationalPercentile.get(key);
						para[cellCount] = new Paragraph(""+Math.round(pScoreByJob.gettValue())+"",font8);
						cell[cellCount] = new PdfPCell(para[cellCount]);
						cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell[cellCount].enableBorderSide(1);						
						mainTable.addCell(cell[cellCount]);
						cellCount++;

						meanArray[i]=meanArray[i]+pScoreByJob.gettValue();
						i++;
					}
					
					composite =  Double.valueOf(oneDForm.format(jft.getCompositeScore()));

					para[cellCount] = new Paragraph(""+Math.round(composite)+"",font8);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell[cellCount].enableBorderSide(1);
					cell[cellCount].setBackgroundColor(color);
					mainTable.addCell(cell[cellCount]);
					cellCount++;
					double percentile=cGReportService.getPercentile(pztList,Double.parseDouble(oneDForm.format(jft.getNormScore())));
					para[cellCount] = new Paragraph((Math.round(jft.getNormScore()))+"",font8); 
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell[cellCount].enableBorderSide(1);
					cell[cellCount].setBackgroundColor(new Color(Integer.parseInt(cGReportService.getColorName(percentile) ,16)));
					mainTable.addCell(cell[cellCount]);
					cellCount++;
				}
				status = jft.getStatus();

				if(status.getStatusShortName().equalsIgnoreCase("comp") || status.getStatusShortName().equalsIgnoreCase("icomp"))
				{
					statusShow="Available";
				}
				else if(status.getStatusShortName().equalsIgnoreCase("rem"))
				{
					statusShow="Removed";
				}
				else
				{
					statusShow="Hired";
				}
				if(!jobOrder.getDistrictMaster().getDistrictId().equals(districtIdForSpecific)){
					para[cellCount] = new Paragraph(""+statusShow,font8);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
					cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell[cellCount].enableBorderSide(1);
					mainTable.addCell(cell[cellCount]);
					cellCount++;
				}
			}

			if(baseStatusFlag)
				if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
				{
					cellCount = 0;

					para[cellCount] = new Paragraph("Mean",font8bold);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[cellCount].enableBorderSide(1);

					mainTable.addCell(cell[cellCount]);
					double compositMean = 0;
					for(double sum:meanArray)
					{

						para[cellCount] = new Paragraph(""+Math.round(sum/lstJobForTeacher.size())+"",font8bold);
						cell[cellCount] = new PdfPCell(para[cellCount]);
						cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell[cellCount].enableBorderSide(1);

						mainTable.addCell(cell[cellCount]);


						compositMean = compositMean+sum/lstJobForTeacher.size();
					}
					double compositSumJob = 0.0;					
					for(double d : compositeTScoreMeanJob){
						compositSumJob = compositSumJob+d;
					}
					para[cellCount] = new Paragraph(Math.round(compositSumJob/compositeTScoreMeanJob.length)+"",font8bold);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell[cellCount].enableBorderSide(1);
					mainTable.addCell(cell[cellCount]);

					double compositSumNational = 0.0;
					for(double d : compositeTScoreMeanNational){
						compositSumNational = compositSumNational+d;
					}

					para[cellCount] = new Paragraph(Math.round(compositSumNational/compositeTScoreMeanNational.length)+"",font8bold);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell[cellCount].enableBorderSide(1);
					mainTable.addCell(cell[cellCount]);

					cellCount++;
					
					if(!jobOrder.getDistrictMaster().getDistrictId().equals(districtIdForSpecific)){
						para[cellCount] = new Paragraph("",font8bold);
						cell[cellCount] = new PdfPCell(para[cellCount]);
						cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[cellCount].setHorizontalAlignment(Element.ALIGN_MIDDLE);
						cell[cellCount].enableBorderSide(1);
						mainTable.addCell(cell[cellCount]);
					}

					cellCount++;
				}
			
			
			lstStatusMasters=null;
			lstStatusMasters = new ArrayList<StatusMaster>();			
			lstStatusMasters.add(statusVlt);			
			lstJobForTeacherVlt.addAll(jobForTeacherDAO.findJFTbyJobOrderForCG(jobOrder,lstStatusMasters));
			
			//------------------map creation start-------

			if(teacherDetails!=null && teacherDetails.size()>0){
				for (JobForTeacher jbforteacher : lstJobForTeacherVlt) 
					teacherDetails.add(jbforteacher.getTeacherId());
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetails);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {				
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
				}
			}


			//------------------map creation end--------
			lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacherVlt);			
			for(JobForTeacher jft : lstJFTremove)
			{	
				teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());
				if((teAssesStatusTempBase==null && baseStatusFlag) || (baseStatusFlag && teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")))					
				{	
					lstJobForTeacherVlt.remove(jft);
				}				
			}

			//--Sorting CG report data -- start
			lstjft = new ArrayList<JobForTeacher>(lstJobForTeacherVlt);
			lstremoveJFT = new ArrayList<JobForTeacher>();
			for(JobForTeacher jft: lstjft){
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					lstJobForTeacherVlt.remove(jft);
					lstremoveJFT.add(jft);
				}
			}
			Collections.sort(lstJobForTeacherVlt,JobForTeacher.jobForTeacherComparatorTeacher);
			Collections.sort(lstremoveJFT,JobForTeacher.jobForTeacherComparatorTeacher);
			lstJobForTeacherVlt.addAll(lstremoveJFT);
			//--Sorting CG report data -- end


			lstTeacherDetails = new ArrayList<TeacherDetail>();

			for(JobForTeacher jft:lstJobForTeacherVlt){
				lstTeacherDetails.add(jft.getTeacherId());
			}			
			
			for(JobForTeacher jft:lstJobForTeacherVlt)
			{	
				assessmentStatus = baseTakenMap.get(jft.getTeacherId().getTeacherId());
				teacherSecondaryStatus = mapTSecStatus.get(jft.getTeacherId().getTeacherId());
				if((teacherSecondaryStatus!=null) && (!userMaster.getEntityType().equals(1))){
					teacherSecStatus = teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusName();
				}
				else{
					teacherSecStatus="";
				}

				cellCount=0;
				if(jft.getTeacherId()!=null)
				{
					tFname = jft.getTeacherId().getFirstName()==null?"":jft.getTeacherId().getFirstName();
					tLname = jft.getTeacherId().getLastName()==null?"":jft.getTeacherId().getLastName();
				}

				para[cellCount] = new Paragraph(""+tFname+" "+tLname+" ",font8);

				cell[cellCount] = new PdfPCell(para[cellCount]);
				cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					cell[cellCount].setBackgroundColor(Color.GRAY);
					
				}
				if(!teacherSecStatus.trim().equals("")){
					Chunk chunk = new Chunk("\n"+teacherSecStatus, font8Green);										
					para[cellCount].add(chunk);
				}
				cell[cellCount].enableBorderSide(1);
				mainTable.addCell(cell[cellCount]);
				cellCount++;
				font8.setColor(Color.black);
				domainTotatlScore=0;
				int i=0;
				String colorName="";
				if(baseStatusFlag)
					for(DomainMaster domain: lstDomain)
					{	
						//score = getPercentScoreOfBasesAssement(domain,jft.getTeacherId(),mapDomainScore); 
						domainTotatlScore = domainTotatlScore+score;
						para[cellCount] = new Paragraph("",font8);
						cell[cellCount] = new PdfPCell(para[cellCount]);
						cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[cellCount].enableBorderSide(1);
						mainTable.addCell(cell[cellCount]);
						cellCount++;


						meanArray[i]=meanArray[i]+score;
						i++;
					}

				status = jft.getStatus();
				if(status.getStatusShortName().equalsIgnoreCase("comp") || status.getStatusShortName().equalsIgnoreCase("icomp") || status.getStatusShortName().equalsIgnoreCase("vlt") )
				{
					statusShow="Timed Out";					
				}
				else if(status.getStatusShortName().equalsIgnoreCase("rem"))
				{
					statusShow="Removed";

				}
				else
				{
					statusShow="Hired";					
				}	
				if(baseStatusFlag){
					para[cellCount] = new Paragraph("",font8);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
					cell[cellCount].enableBorderSide(1);
					mainTable.addCell(cell[cellCount]);
					cellCount++;
					
					para[cellCount] = new Paragraph("",font8);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
					cell[cellCount].enableBorderSide(1);
					mainTable.addCell(cell[cellCount]);
					cellCount++;
				}
				
				if(!jobOrder.getDistrictMaster().getDistrictId().equals(districtIdForSpecific)){
					para[cellCount] = new Paragraph(statusShow,font8);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
					cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell[cellCount].enableBorderSide(1);
					mainTable.addCell(cell[cellCount]);
					cellCount++;
				}
			}

			int cellDomainSize=0;
			if(baseStatusFlag){
				cellDomainSize = lstDomain.size()+4;
			}else{
				cellDomainSize=2;
			}
			if(jobOrder.getDistrictMaster().getDistrictId().equals(districtIdForSpecific)){
				cellDomainSize=cellDomainSize-1;
			}

			for(int i=0;i<cellDomainSize;i++)
			{

				cellCount=0;
				para[i] = new Paragraph("",font8);
				cell[i] = new PdfPCell(para[i]);
				cell[i].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[i].setBorder(0);
				mainTable.addCell(cell[i]);				
			}

			PdfPTable tableStaticContent= new PdfPTable(1);
			tableStaticContent .setWidthPercentage(90);

			Paragraph [] paraStaticContent= null;
			PdfPCell [] cellStaticContent = null;

			paraStaticContent = new Paragraph[2];
			cellStaticContent = new PdfPCell[2];


			paraStaticContent[0] = new Paragraph("Competency Domain Definitions",font10bold);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);


			paraStaticContent[0] = new Paragraph("Attitudinal",font10bold);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);

			paraStaticContent[0] = new Paragraph("This score represents the attitudinal factors affecting a candidate�s ability to be a highly effective teacher. Competencies include persistence in the face of adversity, maintaining a positive attitude, and motivation to succeed.",font8);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);



			paraStaticContent[0] = new Paragraph("Cognitive Ability",font10bold);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);

			paraStaticContent[0] = new Paragraph("Candidate scores in the competencies of verbal ability, quantitative ability, and analytical reasoning.",font8);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);


			paraStaticContent[0] = new Paragraph("Teaching Skills",font10bold);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);

			paraStaticContent[0] = new Paragraph("Candidate scores in instructing, creating a learning environment, planning for successful outcomes, and analyzing and adjusting.",font8);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);

			float[] PartucularWidth={.05f,.9f};
			PdfPTable table = new PdfPTable(PartucularWidth); 

			Image image = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/greenbox5px.png");

			PdfPCell cell1 = new PdfPCell(image, false);
			PdfPCell cell2 = new PdfPCell(new Paragraph("Green is "+Math.round(0)+"% - 100% (Better than most on this job order)",font8));


			if(baseStatusFlag){
				paraStaticContent[0] = new Paragraph("Key",font10bold);
				cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);

				cell1.setPaddingTop(4.5f);
				cell1.setPaddingLeft(3);
				cell1.setBorder(0);
				cell2.setBorder(0);

				table.addCell(cell1);
			//	table.addCell(cell2);

				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);



				table = new PdfPTable(PartucularWidth); 
				image = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/yellowbox5px.png");            
				cell1 = new PdfPCell(image, false);
				cell1.setPaddingTop(4.5f);
				cell1.setPaddingLeft(3);
				cell1.setBorder(0);
				cell2 = new PdfPCell(new Paragraph("Yellow is "+Math.round(0)+"% - "+((Math.round(0)-1)<0?"0":""+(Math.round(0)-1))+"% (About the same as most on this job order)",font8));
				cell2.setBorder(0);            
				table.addCell(cell1);
				//table.addCell(cell2);
				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);

				
				table = new PdfPTable(PartucularWidth); 
				image = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/redbox5px.png");            
				cell1 = new PdfPCell(image, false);
				cell1.setPaddingTop(4.5f);
				cell1.setPaddingLeft(3);
				cell1.setBorder(0);
				cell2 = new PdfPCell(new Paragraph("Red is 0% - "+((Math.round(0)-1)<0?"0":""+(Math.round(0)-1))+"% (Worse than most on this job order)",font8));
				cell2.setBorder(0);            
				table.addCell(cell1);
				//table.addCell(cell2);
				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);
				
				for(int i=10;i>=1;i--){
					table = new PdfPTable(PartucularWidth); 
					image = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/d_5px"+i+".png");            
					cell1 = new PdfPCell(image, false);
					cell1.setPaddingTop(4.5f);
					cell1.setPaddingLeft(3);
					cell1.setBorder(0);
					
					if(i==1)
						cell2 = new PdfPCell(new Paragraph(""+Utility.getValueOfPropByKey("decile"+i+"Min"),font8));
					else
						cell2 = new PdfPCell(new Paragraph(""+Utility.getValueOfPropByKey("decile"+i+"Min")+" - "+Utility.getValueOfPropByKey("decile"+i+"Max"),font8));
			
					cell2.setBorder(0);            
					table.addCell(cell1);
					table.addCell(cell2);
					cellStaticContent[0] = new PdfPCell(table);
					cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
					cellStaticContent[0].setBorder(0);
					tableStaticContent.addCell(cellStaticContent[0]);
				}
			}

			if(!jobOrder.getDistrictMaster().getDistrictId().equals(districtIdForSpecific)){
				float[] PartucularWidth1={.21f,.74f};
				table = new PdfPTable(PartucularWidth1); 
	
				cell1 = new PdfPCell(new Paragraph("Removed",font8bold));
				cell1.setBorder(0);
				cell2 = new PdfPCell(new Paragraph("Indicates removal by an Administrator",font8));
				cell2.setBorder(0);            
				table.addCell(cell1);
				table.addCell(cell2);
				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);
	
	
				table = new PdfPTable(PartucularWidth1);
				cell1 = new PdfPCell(new Paragraph("Available",font8bold));
				cell1.setBorder(0);
				cell2 = new PdfPCell(new Paragraph("Applied for position and is available for Hire",font8));
				cell2.setBorder(0);            
				table.addCell(cell1);
				table.addCell(cell2);
				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);
	
	
				table = new PdfPTable(PartucularWidth1);            
				cell1 = new PdfPCell(new Paragraph("Hired",font8bold));
				cell1.setBorder(0);
				cell2 = new PdfPCell(new Paragraph("Indicates hired for this position (Can be Unhired)",font8));
				cell2.setBorder(0);            
				table.addCell(cell1);
				table.addCell(cell2);
				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);
	
				table = new PdfPTable(PartucularWidth1);            
				cell1 = new PdfPCell(new Paragraph("Timed Out",font8bold));
				cell1.setBorder(0);
				cell2 = new PdfPCell(new Paragraph("Indicates a timeout violation; no scores displayed",font8));
				cell2.setBorder(0);            
				table.addCell(cell1);
				table.addCell(cell2);
				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);
	
	
				paraStaticContent[0] = new Paragraph("",font8);
				cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);
			}

			float[] PartucularWidth0={.6f,.3f};
			PdfPTable tableContainer = new PdfPTable(PartucularWidth0);
			tableContainer .setWidthPercentage(90);


			PdfPCell [] cellContainer = null;


			cellContainer = new PdfPCell[2];



			cellContainer[0] = new PdfPCell(mainTable);
			cellContainer[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellContainer[0].setPaddingTop(5);
			cellContainer[0].setBorder(0);


			tableContainer.addCell(cellContainer[0]);

			cellContainer[1] = new PdfPCell(tableStaticContent);
			cellContainer[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellContainer[1].setBorder(0);
			tableContainer.addCell(cellContainer[1]);


			document.add(tableContainer);
			document.newPage();

		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(document != null && document.isOpen())
				document.close();
			if(writer!=null){
				writer.flush();
				writer.close();
			}
		}
		return true;		
	
	}

}