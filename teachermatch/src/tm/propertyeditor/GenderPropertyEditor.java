package tm.propertyeditor;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.CityMaster;
import tm.bean.master.GenderMaster;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.GenderMasterDAO;

public class GenderPropertyEditor extends PropertyEditorSupport
{
	@Autowired
	private GenderMasterDAO genderMasterDAO;
	public void setGenderMasterDAO(GenderMasterDAO genderMasterDAO) 
	{
		this.genderMasterDAO = genderMasterDAO;
	}
	
	@Override
	public void setAsText(String text) throws IllegalArgumentException 
	{

		GenderMaster genderMaster = null;
		try
		{	
			if(text!=null && text.trim().length()>0)
			{
				
				if(text!=null)
				{
					try
					{
						genderMaster=genderMasterDAO.findById(Integer.parseInt(text), false, true);
						setValue(genderMaster);
					}
					catch(Exception ex)
					{	
						
						genderMaster=new GenderMaster();
						genderMaster.setGenderName(text);
						setValue(genderMaster);	
					}		
				}						
			}
			else
			{
				setValue(null);
			}
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}	
	
	}
	

}
