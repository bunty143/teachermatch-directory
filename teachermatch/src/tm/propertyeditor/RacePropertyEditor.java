package tm.propertyeditor;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.CityMaster;
import tm.bean.master.RaceMaster;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.RaceMasterDAO;

public class RacePropertyEditor extends PropertyEditorSupport
{
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	public void setRaceMasterDAO(RaceMasterDAO raceMasterDAO) 
	{
		this.raceMasterDAO = raceMasterDAO;
	}
	
	@Override
	public void setAsText(String text) throws IllegalArgumentException 
	{

		RaceMaster raceMaster = null;
		try
		{	
			if(text!=null && text.trim().length()>0)
			{
				
				if(text!=null)
				{
					try
					{
						raceMaster=raceMasterDAO.findById(Integer.parseInt(text), false, true);
						setValue(raceMaster);
					}
					catch(Exception ex)
					{	
						
						raceMaster=new RaceMaster();
						raceMaster.setRaceName(text);
						setValue(raceMaster);	
					}		
				}						
			}
			else
			{
				setValue(null);
			}
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}	
	
	}
	

}
