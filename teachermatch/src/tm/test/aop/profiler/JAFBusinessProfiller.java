package tm.test.aop.profiler;
import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class JAFBusinessProfiller { 
	
	@Pointcut("execution(* tm.service.jobapplication.*.*(..))")
    public Object profile0(ProceedingJoinPoint pjp) throws Throwable {
    	return getCommonCode(pjp, "L0");
    }
    
    @Around("execution(* tm.dao.master.DspqFieldMasterDAO.*(..))")
    public Object profileL1(ProceedingJoinPoint pjp) throws Throwable {
    	return getCommonCode(pjp, "L1");
    }
    
    @Around("execution(* tm.dao.master.DspqGroupMasterDAO.*(..))")
    public Object profileL2(ProceedingJoinPoint pjp) throws Throwable {
    	return getCommonCode(pjp, "L2");
    }
    
    @Around("execution(* tm.dao.master.DspqJobWiseStatusDAO.*(..))")
    public Object profileL3(ProceedingJoinPoint pjp) throws Throwable {
    	return getCommonCode(pjp, "L3");
    }
    
    @Around("execution(* tm.dao.master.DspqPortfolioNameDAO.*(..))")
    public Object profileL4(ProceedingJoinPoint pjp) throws Throwable {
    	return getCommonCode(pjp, "L4");
    }
    
    @Around("execution(* tm.dao.master.DspqRouterDAO.*(..))")
    public Object profileL5(ProceedingJoinPoint pjp) throws Throwable {
    	return getCommonCode(pjp, "L5");
    }
    
    @Around("execution(* tm.dao.master.DspqSectionMasterDAO.*(..))")
    public Object profileL6(ProceedingJoinPoint pjp) throws Throwable {
    	return getCommonCode(pjp, "L6");
    }
    
    public static Object getCommonCode(ProceedingJoinPoint joinPoint,String sLevel)throws Throwable
    {
    	Object output = null;
		try {
			long start = System.currentTimeMillis();
			output = joinPoint.proceed();
			String sPrintTime="";
			long elapsedTime = System.currentTimeMillis() - start;
			long lSecond=0;

			if(elapsedTime>1000)
			{
				lSecond=elapsedTime/1000;
				long elapsedTimeL2=elapsedTime-(lSecond*1000);
				sPrintTime=lSecond+" Seconds "+elapsedTimeL2+" Milliseconds";
			}
			else
			{
				sPrintTime=elapsedTime+" Milliseconds";
			}
			System.out.println(sLevel+" Calling Method: ( "+joinPoint.getTarget().getClass().getName()+"->"+joinPoint.getSignature().getName()+" ) Execution Time [ " + sPrintTime + " ]");
			//System.out.println("Arguments : " + Arrays.toString(joinPoint.getArgs()));
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return output;
    }
    
   }