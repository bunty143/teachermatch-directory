package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import tm.bean.assessment.AssessmentGroupDetails;

@Entity(name="epitakenlist")
public class EpiTakenList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7862830475147522276L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer epiTakenId;
	private Boolean isResearchEPI;
	private Integer assessmentId;
	@ManyToOne
	@JoinColumn(name="assessmentGroupId",referencedColumnName="assessmentGroupId")
	private AssessmentGroupDetails assessmentGroupDetails;
	
	public Integer getEpiTakenId() {
		return epiTakenId;
	}
	public void setEpiTakenId(Integer epiTakenId) {
		this.epiTakenId = epiTakenId;
	}
	public Boolean getIsResearchEPI() {
		return isResearchEPI;
	}
	public void setIsResearchEPI(Boolean isResearchEPI) {
		this.isResearchEPI = isResearchEPI;
	}
	public Integer getAssessmentId() {
		return assessmentId;
	}
	public void setAssessmentId(Integer assessmentId) {
		this.assessmentId = assessmentId;
	}
	public AssessmentGroupDetails getAssessmentGroupDetails() {
		return assessmentGroupDetails;
	}
	public void setAssessmentGroupDetails(AssessmentGroupDetails assessmentGroupDetails) {
		this.assessmentGroupDetails = assessmentGroupDetails;
	}
}
