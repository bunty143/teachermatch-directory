package tm.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.user.UserMaster;


@Entity
@Table(name="nationalboardcertificationmaster")
public class NationalBoardCertificationMaster {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer customquestionid;
	
	
	public Integer getCustomquestionid() {
		return customquestionid;
	}
	public void setCustomquestionid(Integer customquestionid) {
		this.customquestionid = customquestionid;
	}
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster  headQuarterId;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtId;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryId;
	
	private String question;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBy;
	
	private String status;
	private Date createdDateTime;
	public HeadQuarterMaster getHeadQuarterId() {
		return headQuarterId;
	}
	public void setHeadQuarterId(HeadQuarterMaster headQuarterId) {
		this.headQuarterId = headQuarterId;
	}
	public BranchMaster getBranchId() {
		return branchId;
	}
	public void setBranchId(BranchMaster branchId) {
		this.branchId = branchId;
	}
	public DistrictMaster getDistrictId() {
		return districtId;
	}
	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}
	public JobCategoryMaster getJobCategoryId() {
		return jobCategoryId;
	}
	public void setJobCategoryId(JobCategoryMaster jobCategoryId) {
		this.jobCategoryId = jobCategoryId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public UserMaster getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(UserMaster createdBy) {
		this.createdBy = createdBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
}
