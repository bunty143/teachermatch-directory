package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="districtportfolioconfig")
public class DistrictPortfolioConfig implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7828488708354199502L;

	@Id	
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtPortfolioConfigId;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private  BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	private Boolean coverLetter;
	private Integer academic;
	private Integer academicTranscript;
	private Integer certification;
	private Integer proofOfCertification;
	private Integer reference;
	private Integer referenceLettersOfRecommendation;
	private Boolean resume;
	private Boolean tfaAffiliate;
	private Boolean willingAsSubstituteTeacher;
	private String candidateType;
	private Boolean phoneNumber;
	private Boolean address;
	private Boolean expCertTeacherTraining;
	private Boolean nationalBoardCert;
	private Boolean affidavit;
	private Boolean generalKnowledgeExam;
	private Boolean subjectAreaExam;
	private Boolean additionalDocuments;
	private Boolean personalinfo;
	private Boolean dateOfBirth;
	private Boolean ssn;
	private Boolean race;
	private Boolean formeremployee;
	private Boolean veteran;
	private Boolean ethnicorigin;
	private Integer dateOfBirthOptional;
	
	public Integer getDateOfBirthOptional() {
		return dateOfBirthOptional;
	}
	public void setDateOfBirthOptional(Integer dateOfBirthOptional) {
		this.dateOfBirthOptional = dateOfBirthOptional;
	}

	private Boolean ethinicity;
	private Boolean employment;
	private Boolean genderId;
	private Boolean retirementnumber;
	private Boolean videoLink;
	private Boolean involvement;
	private Boolean honors;
	private Boolean residency;	
	private String dspqName;
	@Transient
	private String jobCategoryName;
	
	@Transient
	private Boolean isSubstituteInstructionalForMiami;
	
	@Transient
	private Boolean districtSpecificPortfolioQuestions;
	
	@Transient
	private Boolean isInterventionistsForMiami;
	@Transient
	private Boolean isNonTeacher;
	@Transient
	private Boolean isSchoolSupportPhiladelphia;
	@Transient
	private Boolean isPrinciplePhiladelphia;
	@Transient
	private String jobTitle;
	private Boolean eEOCOptional;
	private Boolean certificationptional;
	private Boolean academicsOptional;
	private Boolean referenceOptional;
	private Boolean employeementOptional;
	private Boolean tfaOptional;
	private Boolean nationalBoardOptional;
	private Boolean certfiedTeachingExpOptional;
	private Boolean substituteOptional;
	private Boolean videoSecOptional;
	private Boolean gpaOptional;
	private Boolean empSecSalaryOptional;
	private Boolean empSecRoleOptional;
	private Integer empSecPrirOptional;
	private Integer empSecMscrOptional;
	private Boolean empSecReasonForLeavOptional;
	private Boolean coverLetterOptional;
	private Boolean ressumeOptional;
	private Boolean addressOptional;
	private Integer expectedSalary;
	private Boolean certificationUrl;
	private Boolean certificationDoeNumber;
	private Boolean ssnOptional;
	private Boolean academicsDatesOptional;
	private Boolean empDatesOptional;
	private Boolean certiDatesOptional;
	private Integer certiGrades;
	private Boolean transcriptUpload;
	private Boolean generalKnowledgeExamOptional;	
	private Boolean degreeOptional;
	private Boolean schoolOptional;
	private Boolean fieldOfStudyOptional;	
	private Boolean empOrganizationOptional;
	private Boolean empCityOptional;
	private Boolean empStateOptional;
	private Boolean empPositionOptional;
	private Boolean licenseLetterOptional;
	
	
	private Integer SINOptional;
	public Integer getSINOptional() {
		return SINOptional;
	}
	public void setSINOptional(Integer sINOptional) {
		SINOptional = sINOptional;
	}
	
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private Date createdDateTime;
	private Date lastUpdatedDateTime;
	
	public Boolean getDistrictSpecificPortfolioQuestions() {
		return districtSpecificPortfolioQuestions;
	}
	public void setDistrictSpecificPortfolioQuestions(
			Boolean districtSpecificPortfolioQuestions) {
		this.districtSpecificPortfolioQuestions = districtSpecificPortfolioQuestions;
	}
	public Boolean getGeneralKnowledgeExam() {
		return generalKnowledgeExam;
	}
	public void setGeneralKnowledgeExam(Boolean generalKnowledgeExam) {
		this.generalKnowledgeExam = generalKnowledgeExam;
	}
	public Boolean getAdditionalDocuments() {
		return additionalDocuments;
	}
	public void setAdditionalDocuments(Boolean additionalDocuments) {
		this.additionalDocuments = additionalDocuments;
	}
	
	public Boolean getSubjectAreaExam() {
		return subjectAreaExam;
	}
	public void setSubjectAreaExam(Boolean subjectAreaExam) {
		this.subjectAreaExam = subjectAreaExam;
	}
	public Integer getDistrictPortfolioConfigId() {
		return districtPortfolioConfigId;
	}
	public void setDistrictPortfolioConfigId(Integer districtPortfolioConfigId) {
		this.districtPortfolioConfigId = districtPortfolioConfigId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	
	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}
	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}
	public Boolean getCoverLetter() {
		return coverLetter;
	}
	public void setCoverLetter(Boolean coverLetter) {
		this.coverLetter = coverLetter;
	}
	public Integer getAcademic() {
		return academic;
	}
	public void setAcademic(Integer academic) {
		this.academic = academic;
	}
	public Integer getAcademicTranscript() {
		return academicTranscript;
	}
	public void setAcademicTranscript(Integer academicTranscript) {
		this.academicTranscript = academicTranscript;
	}
	public Integer getCertification() {
		return certification;
	}
	public void setCertification(Integer certification) {
		this.certification = certification;
	}
	public Integer getProofOfCertification() {
		return proofOfCertification;
	}
	public void setProofOfCertification(Integer proofOfCertification) {
		this.proofOfCertification = proofOfCertification;
	}
	public Integer getReference() {
		return reference;
	}
	public void setReference(Integer reference) {
		this.reference = reference;
	}
	public Integer getReferenceLettersOfRecommendation() {
		return referenceLettersOfRecommendation;
	}
	public void setReferenceLettersOfRecommendation(
			Integer referenceLettersOfRecommendation) {
		this.referenceLettersOfRecommendation = referenceLettersOfRecommendation;
	}
	public Boolean getResume() {
		return resume;
	}
	public void setResume(Boolean resume) {
		this.resume = resume;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Date getLastUpdatedDateTime() {
		return lastUpdatedDateTime;
	}
	public void setLastUpdatedDateTime(Date lastUpdatedDateTime) {
		this.lastUpdatedDateTime = lastUpdatedDateTime;
	}
	public Boolean getTfaAffiliate() {
		return tfaAffiliate;
	}
	public void setTfaAffiliate(Boolean tfaAffiliate) {
		this.tfaAffiliate = tfaAffiliate;
	}
	public Boolean getWillingAsSubstituteTeacher() {
		return willingAsSubstituteTeacher;
	}
	public void setWillingAsSubstituteTeacher(Boolean willingAsSubstituteTeacher) {
		this.willingAsSubstituteTeacher = willingAsSubstituteTeacher;
	}
	public String getCandidateType() {
		return candidateType;
	}
	public void setCandidateType(String candidateType) {
		this.candidateType = candidateType;
	}
	public Boolean getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(Boolean phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Boolean getAddress() {
		return address;
	}
	public void setAddress(Boolean address) {
		this.address = address;
	}
	public Boolean getExpCertTeacherTraining() {
		return expCertTeacherTraining;
	}
	public void setExpCertTeacherTraining(Boolean expCertTeacherTraining) {
		this.expCertTeacherTraining = expCertTeacherTraining;
	}
	public Boolean getNationalBoardCert() {
		return nationalBoardCert;
	}
	public void setNationalBoardCert(Boolean nationalBoardCert) {
		this.nationalBoardCert = nationalBoardCert;
	}
	public Boolean getAffidavit() {
		return affidavit;
	}
	public void setAffidavit(Boolean affidavit) {
		this.affidavit = affidavit;
	}
	public Boolean getPersonalinfo() {
		return personalinfo;
	}
	public void setPersonalinfo(Boolean personalinfo) {
		this.personalinfo = personalinfo;
	}
	
	public Boolean getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Boolean dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public Boolean getRace() {
		return race;
	}
	public void setRace(Boolean race) {
		this.race = race;
	}
	public Boolean getFormeremployee() {
		return formeremployee;
	}
	public void setFormeremployee(Boolean formeremployee) {
		this.formeremployee = formeremployee;
	}
	public Boolean getSsn() {
		return ssn;
	}
	public void setSsn(Boolean ssn) {
		this.ssn = ssn;
	}
	public Boolean getVeteran() {
		return veteran;
	}
	public void setVeteran(Boolean veteran) {
		this.veteran = veteran;
	}
	public Boolean getEthnicorigin() {
		return ethnicorigin;
	}
	public void setEthnicorigin(Boolean ethnicorigin) {
		this.ethnicorigin = ethnicorigin;
	}
	public Boolean getEthinicity() {
		return ethinicity;
	}
	public void setEthinicity(Boolean ethinicity) {
		this.ethinicity = ethinicity;
	}
	public Boolean getEmployment() {
		return employment;
	}
	public void setEmployment(Boolean employment) {
		this.employment = employment;
	}
	public Boolean getGenderId() {
		return genderId;
	}
	public void setGenderId(Boolean genderId) {
		this.genderId = genderId;
	}
	public Boolean getInvolvement() {
		return involvement;
	}
	public void setInvolvement(Boolean involvement) {
		this.involvement = involvement;
	}
	public Boolean getHonors() {
		return honors;
	}
	public void setHonors(Boolean honors) {
		this.honors = honors;
	}
	public Boolean getIsSubstituteInstructionalForMiami() {
		return isSubstituteInstructionalForMiami;
	}
	public void setIsSubstituteInstructionalForMiami(
			Boolean isSubstituteInstructionalForMiami) {
		this.isSubstituteInstructionalForMiami = isSubstituteInstructionalForMiami;
	}
	public Boolean getIsInterventionistsForMiami() {
		return isInterventionistsForMiami;
	}
	public void setIsInterventionistsForMiami(Boolean isInterventionistsForMiami) {
		
		System.out.println(" isInterventionistsForMiami "+isInterventionistsForMiami);
		
		this.isInterventionistsForMiami = isInterventionistsForMiami;
	}
	public Boolean getRetirementnumber() {
		return retirementnumber;
	}
	public void setRetirementnumber(Boolean retirementnumber) {
		this.retirementnumber = retirementnumber;
	}
	public Boolean getVideoLink() {
		return videoLink;
	}
	public void setVideoLink(Boolean videoLink) {
		this.videoLink = videoLink;
	}
	public Boolean getIsNonTeacher() {
		return isNonTeacher;
	}
	public void setIsNonTeacher(Boolean isNonTeacher) {
		this.isNonTeacher = isNonTeacher;
	}
	public Boolean getIsSchoolSupportPhiladelphia() {
		return isSchoolSupportPhiladelphia;
	}
	public void setIsSchoolSupportPhiladelphia(Boolean isSchoolSupportPhiladelphia) {
		this.isSchoolSupportPhiladelphia = isSchoolSupportPhiladelphia;
	}
	public Boolean getIsPrinciplePhiladelphia() {
		return isPrinciplePhiladelphia;
	}
	public void setIsPrinciplePhiladelphia(Boolean isPrinciplePhiladelphia) {
		this.isPrinciplePhiladelphia = isPrinciplePhiladelphia;
	}
	public String getJobCategoryName() {
		return jobCategoryName;
	}
	public void setJobCategoryName(String jobCategoryName) {
		this.jobCategoryName = jobCategoryName;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	public Boolean geteEOCOptional() {
		return eEOCOptional;
	}
	public void seteEOCOptional(Boolean eEOCOptional) {
		this.eEOCOptional = eEOCOptional;
	}
	public Boolean getCertificationptional() {
		return certificationptional;
	}
	public void setCertificationptional(Boolean certificationptional) {
		this.certificationptional = certificationptional;
	}
	public Boolean getAcademicsOptional() {
		return academicsOptional;
	}
	public void setAcademicsOptional(Boolean academicsOptional) {
		this.academicsOptional = academicsOptional;
	}
	public Boolean getReferenceOptional() {
		return referenceOptional;
	}
	public void setReferenceOptional(Boolean referenceOptional) {
		this.referenceOptional = referenceOptional;
	}
	public Boolean getEmployeementOptional() {
		return employeementOptional;
	}
	public void setEmployeementOptional(Boolean employeementOptional) {
		this.employeementOptional = employeementOptional;
	}
	public Boolean getTfaOptional() {
		return tfaOptional;
	}
	public void setTfaOptional(Boolean tfaOptional) {
		this.tfaOptional = tfaOptional;
	}
	public Boolean getNationalBoardOptional() {
		return nationalBoardOptional;
	}
	public void setNationalBoardOptional(Boolean nationalBoardOptional) {
		this.nationalBoardOptional = nationalBoardOptional;
	}
	public Boolean getCertfiedTeachingExpOptional() {
		return certfiedTeachingExpOptional;
	}
	public void setCertfiedTeachingExpOptional(Boolean certfiedTeachingExpOptional) {
		this.certfiedTeachingExpOptional = certfiedTeachingExpOptional;
	}
	public Boolean getSubstituteOptional() {
		return substituteOptional;
	}
	public void setSubstituteOptional(Boolean substituteOptional) {
		this.substituteOptional = substituteOptional;
	}
	public Boolean getVideoSecOptional() {
		return videoSecOptional;
	}
	public void setVideoSecOptional(Boolean videoSecOptional) {
		this.videoSecOptional = videoSecOptional;
	}
	public Boolean getGpaOptional() {
		return gpaOptional;
	}
	public void setGpaOptional(Boolean gpaOptional) {
		this.gpaOptional = gpaOptional;
	}
	public Boolean getEmpSecSalaryOptional() {
		return empSecSalaryOptional;
	}
	public void setEmpSecSalaryOptional(Boolean empSecSalaryOptional) {
		this.empSecSalaryOptional = empSecSalaryOptional;
	}
	public Boolean getEmpSecRoleOptional() {
		return empSecRoleOptional;
	}
	public void setEmpSecRoleOptional(Boolean empSecRoleOptional) {
		this.empSecRoleOptional = empSecRoleOptional;
	}
	public Integer getEmpSecPrirOptional() {
		return empSecPrirOptional;
	}
	public void setEmpSecPrirOptional(Integer empSecPrirOptional) {
		this.empSecPrirOptional = empSecPrirOptional;
	}
	public Integer getEmpSecMscrOptional() {
		return empSecMscrOptional;
	}
	public void setEmpSecMscrOptional(Integer empSecMscrOptional) {
		this.empSecMscrOptional = empSecMscrOptional;
	}
	public Boolean getCoverLetterOptional() {
		return coverLetterOptional;
	}
	public void setCoverLetterOptional(Boolean coverLetterOptional) {
		this.coverLetterOptional = coverLetterOptional;
	}
	public Boolean getRessumeOptional() {
		return ressumeOptional;
	}
	public void setRessumeOptional(Boolean ressumeOptional) {
		this.ressumeOptional = ressumeOptional;
	}
	public Boolean getAddressOptional() {
		return addressOptional;
	}
	public void setAddressOptional(Boolean addressOptional) {
		this.addressOptional = addressOptional;
	}
	public Integer getExpectedSalary() {
		return expectedSalary;
	}
	public void setExpectedSalary(Integer expectedSalary) {
		this.expectedSalary = expectedSalary;
	}
	public Boolean getCertificationUrl() {
		return certificationUrl;
	}
	public void setCertificationUrl(Boolean certificationUrl) {
		this.certificationUrl = certificationUrl;
	}
	public Boolean getCertificationDoeNumber() {
		return certificationDoeNumber;
	}
	public void setCertificationDoeNumber(Boolean certificationDoeNumber) {
		this.certificationDoeNumber = certificationDoeNumber;
	}
	public Boolean getSsnOptional() {
		return ssnOptional;
	}
	public void setSsnOptional(Boolean ssnOptional) {
		this.ssnOptional = ssnOptional;
	}
	public Boolean getAcademicsDatesOptional() {
		return academicsDatesOptional;
	}
	public void setAcademicsDatesOptional(Boolean academicsDatesOptional) {
		this.academicsDatesOptional = academicsDatesOptional;
	}
	public Boolean getEmpDatesOptional() {
		return empDatesOptional;
	}
	public void setEmpDatesOptional(Boolean empDatesOptional) {
		this.empDatesOptional = empDatesOptional;
	}
	public Boolean getCertiDatesOptional() {
		return certiDatesOptional;
	}
	public void setCertiDatesOptional(Boolean certiDatesOptional) {
		this.certiDatesOptional = certiDatesOptional;
	}
	public Integer getCertiGrades() {
		return certiGrades;
	}
	public void setCertiGrades(Integer certiGrades) {
		this.certiGrades = certiGrades;
	}
	public Boolean getResidency() {
		return residency;
	}
	public void setResidency(Boolean residency) {
		this.residency = residency;
	}
	public String getDspqName() {
		return dspqName;
	}
	public void setDspqName(String dspqName) {
		this.dspqName = dspqName;
	}
	public Boolean getEmpSecReasonForLeavOptional() {
		return empSecReasonForLeavOptional;
	}
	public void setEmpSecReasonForLeavOptional(Boolean empSecReasonForLeavOptional) {
		this.empSecReasonForLeavOptional = empSecReasonForLeavOptional;
	}
	public Boolean getTranscriptUpload() {
		return transcriptUpload;
	}
	public void setTranscriptUpload(Boolean transcriptUpload) {
		this.transcriptUpload = transcriptUpload;
	}
	public Boolean getGeneralKnowledgeExamOptional() {
		return generalKnowledgeExamOptional;
	}
	public void setGeneralKnowledgeExamOptional(Boolean generalKnowledgeExamOptional) {
		this.generalKnowledgeExamOptional = generalKnowledgeExamOptional;
	}
	public Boolean getDegreeOptional() {
		return degreeOptional;
	}
	public void setDegreeOptional(Boolean degreeOptional) {
		this.degreeOptional = degreeOptional;
	}
	public Boolean getSchoolOptional() {
		return schoolOptional;
	}
	public void setSchoolOptional(Boolean schoolOptional) {
		this.schoolOptional = schoolOptional;
	}
	public Boolean getFieldOfStudyOptional() {
		return fieldOfStudyOptional;
	}
	public void setFieldOfStudyOptional(Boolean fieldOfStudyOptional) {
		this.fieldOfStudyOptional = fieldOfStudyOptional;
	}		
	public Boolean getEmpPositionOptional() {
		return empPositionOptional;
	}
	public void setEmpPositionOptional(Boolean empPositionOptional) {
		this.empPositionOptional = empPositionOptional;
	}
	public Boolean getEmpOrganizationOptional() {
		return empOrganizationOptional;
	}
	public void setEmpOrganizationOptional(Boolean empOrganizationOptional) {
		this.empOrganizationOptional = empOrganizationOptional;
	}
	public Boolean getEmpCityOptional() {
		return empCityOptional;
	}
	public void setEmpCityOptional(Boolean empCityOptional) {
		this.empCityOptional = empCityOptional;
	}
	public Boolean getEmpStateOptional() {
		return empStateOptional;
	}
	public void setEmpStateOptional(Boolean empStateOptional) {
		this.empStateOptional = empStateOptional;
	}
	public Boolean getLicenseLetterOptional() {
		return licenseLetterOptional;
	}
	public void setLicenseLetterOptional(Boolean licenseLetterOptional) {
		this.licenseLetterOptional = licenseLetterOptional;
	}
	
	
}
