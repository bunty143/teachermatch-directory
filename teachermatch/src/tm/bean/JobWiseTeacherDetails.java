package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
@Entity
@Table(name="jobwiseteacherdetails")
public class JobWiseTeacherDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5749618920669172956L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobWiseTeacherDetailsId;  
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="userUpdatedBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private String seniorityNumber;
	//userUpdatedBy
	private Date createdDateTime;
	public Integer getJobWiseTeacherDetailsId() {
		return jobWiseTeacherDetailsId;
	}
	public void setJobWiseTeacherDetailsId(Integer jobWiseTeacherDetailsId) {
		this.jobWiseTeacherDetailsId = jobWiseTeacherDetailsId;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getSeniorityNumber() {
		return seniorityNumber;
	}
	public void setSeniorityNumber(String seniorityNumber) {
		this.seniorityNumber = seniorityNumber;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
