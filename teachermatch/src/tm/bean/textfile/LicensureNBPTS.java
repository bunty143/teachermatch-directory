package tm.bean.textfile;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

@Entity
@Table(name="licensurenbpts")
public class LicensureNBPTS {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer licensurenbptsId; 
	
	String SSN;
	String nbotsAreaCode;
	
	@Column 
	@Type(type="date")
	 Date effectiveDate;

	@Column 
	@Type(type="date")
	 Date expirationDate;
	
	private String status;
	
	
	@Column(name = "createdDate", columnDefinition="")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	private int createdBy;
	public Integer getLicensurenbptsId() {
		return licensurenbptsId;
	}

	
	
	public void setLicensurenbptsId(Integer licensurenbptsId) {
		this.licensurenbptsId = licensurenbptsId;
	}

	public String getSSN() {
		return SSN;
	}

	public void setSSN(String sSN) {
		SSN = sSN;
	}

	public String getNbotsAreaCode() {
		return nbotsAreaCode;
	}

	public void setNbotsAreaCode(String nbotsAreaCode) {
		this.nbotsAreaCode = nbotsAreaCode;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	
	
	
	
	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public Date getCreatedDate() {
		return createdDate;
	}



	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}



	public int getCreatedBy() {
		return createdBy;
	}



	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}



	@Override
	public String toString() {
		return "LicensureNBPTS [SSN=" + SSN + ", effectiveDate="
				+ effectiveDate + ", expirationDate=" + expirationDate
				+ ", licensurenbptsId=" + licensurenbptsId + ", nbotsAreaCode="
				+ nbotsAreaCode + "]";
	}
	
	
	
	

}
