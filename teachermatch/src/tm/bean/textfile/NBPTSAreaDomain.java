package tm.bean.textfile;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="nbptsareadomain")
public class NBPTSAreaDomain {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer nbptsAreaDomainId;
	
	private String nbptsAreaCode;
	
	private String nbptsAreaDescription;
	
	private String status;
	
	@Column(name = "createdDate", columnDefinition="")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	private int createdBy;

	public Integer getNbptsAreaDomainId() {
		return nbptsAreaDomainId;
	}

	public void setNbptsAreaDomainId(Integer nbptsAreaDomainId) {
		this.nbptsAreaDomainId = nbptsAreaDomainId;
	}

	public String getNbptsAreaCode() {
		return nbptsAreaCode;
	}

	public void setNbptsAreaCode(String nbptsAreaCode) {
		this.nbptsAreaCode = nbptsAreaCode;
	}

	public String getNbptsAreaDescription() {
		return nbptsAreaDescription;
	}

	public void setNbptsAreaDescription(String nbptsAreaDescription) {
		this.nbptsAreaDescription = nbptsAreaDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	 
	
	
	
}
