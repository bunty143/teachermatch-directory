package tm.bean.textfile;

import java.io.Serializable;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;
/* 
 * @Author: Sandeep Yadav
 */

@Entity
@Table(name="applicantlicenseareadomain")
public class LicenseAreaDomain implements Serializable{
	
	private static final long serialVersionUID = 2183204264206577556L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer licenseAreaDomainId; 
	private String licenseAreaCode;
	private String licenseAreaDescription;
	private String status;
	private Date  createdDate;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;

	public Integer getLicenseAreaDomainId() {
		return licenseAreaDomainId;
	}

	public void setLicenseAreaDomainId(Integer licenseAreaDomainId) {
		this.licenseAreaDomainId = licenseAreaDomainId;
	}

	public String getLicenseAreaCode() {
		return licenseAreaCode;
	}

	public void setLicenseAreaCode(String licenseAreaCode) {
		this.licenseAreaCode = licenseAreaCode;
	}

	public String getLicenseAreaDescription() {
		return licenseAreaDescription;
	}

	public void setLicenseAreaDescription(String licenseAreaDescription) {
		this.licenseAreaDescription = licenseAreaDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	
	
	
}
