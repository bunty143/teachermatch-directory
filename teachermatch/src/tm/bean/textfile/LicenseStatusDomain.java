package tm.bean.textfile;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

/* 
 * @Author: Sandeep Yadav
 */

@Entity
@Table(name="applicantlicensestatusdomain")
public class LicenseStatusDomain implements Serializable{
	
	private static final long serialVersionUID = 2183204264206577556L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer licenseStatusDomainId; 
	private String programStatusCode;
	private String programStatusDescription;
	private String status;
	private Date  createdDate;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;

	
	
	public Integer getLicenseStatusDomainId() {
		return licenseStatusDomainId;
	}

	public void setLicenseStatusDomainId(Integer licenseStatusDomainId) {
		this.licenseStatusDomainId = licenseStatusDomainId;
	}

	public String getProgramStatusCode() {
		return programStatusCode;
	}

	public void setProgramStatusCode(String programStatusCode) {
		this.programStatusCode = programStatusCode;
	}

	public String getProgramStatusDescription() {
		return programStatusDescription;
	}

	public void setProgramStatusDescription(String programStatusDescription) {
		this.programStatusDescription = programStatusDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}


}
