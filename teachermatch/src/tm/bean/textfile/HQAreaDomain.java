package tm.bean.textfile;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

/* 
 * @Author: Sandeep Yadav
 */

@Entity
@Table(name="applicanthqareadomain")
public class HQAreaDomain implements Serializable{
	
	private static final long serialVersionUID = 2183204264206577556L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer hQAreaDomainId; 
	private String hQAreaCode;
	private String hQAreaDescription;
	private String status;
	private Date  createdDate;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;

	public Integer gethQAreaDomainId() {
		return hQAreaDomainId;
	}

	public void sethQAreaDomainId(Integer hQAreaDomainId) {
		this.hQAreaDomainId = hQAreaDomainId;
	}

	public String gethQAreaCode() {
		return hQAreaCode;
	}

	public void sethQAreaCode(String hQAreaCode) {
		this.hQAreaCode = hQAreaCode;
	}

	public String gethQAreaDescription() {
		return hQAreaDescription;
	}

	public void sethQAreaDescription(String hQAreaDescription) {
		this.hQAreaDescription = hQAreaDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	
	
	
}
