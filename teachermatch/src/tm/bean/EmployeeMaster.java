package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import tm.bean.master.DistrictMaster;
import tm.bean.master.StateMaster;

@Entity(name="employeemaster")
public class EmployeeMaster implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6762107660437634613L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer employeeId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	private String firstName;
	private String middleName;
	private String lastName;
	private String emailAddress;
	private String SSN;
	private String employeeCode;
	private String staffType;
	private String FECode;
	private String RRCode;
	private String OCCode;
	private String PCCode;
	private String SAPId;
	private String status;
	private Date drugTestFailDate;
	private String address1;
	private String cityName;
	private String zipCode;
	private String phone;
	private String mobile;
	private Date dob;
	private String position;
	private String certificationCode;
	private String eligibleToTransfer;
	private String classification;
	
	private String OMN;
	private String CurrentPositionName;
	private String CurrentPositionNumber;
	private String FTE;
	private String seniority_number;
	private Date seniority_date;
	@ManyToOne
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private StateMaster stateMaster ;
	
	private Date createdDateTime;
	
	private String alum;
	
	private Date dismissedDate ;
	private String approveStatus;
	
	private Date   doNotHireDate  ;
	private String doNotHireComment ;
	private String doNotHireStatus  ;
	private String employmentStatus  ;
	@Column(name="DT",columnDefinition="Displaced Teachers")
	private String DT;
	private Date updatedDateTime;
	
	private String supervisorFirstName ;	
	private String supervisorLastName ;	
	private String supervisorEmail  ;
	private String supervisorPhone  ;	
	private String superVisorComment;
	private Date supervisorAvailDate;
	
	
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public String getSSN() {
		return SSN;
	}
	public void setSSN(String sSN) {
		SSN = sSN;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String getStaffType() {
		return staffType;
	}
	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}
	public void setFECode(String fECode) {
		FECode = fECode;
	}
	public String getFECode() {
		return FECode;
	}
	public String getRRCode() {
		return RRCode;
	}
	public void setRRCode(String rRCode) {
		RRCode = rRCode;
	}
	public String getOCCode() {
		return OCCode;
	}
	public void setOCCode(String oCCode) {
		OCCode = oCCode;
	}
	public String getPCCode() {
		return PCCode;
	}
	public void setPCCode(String pCCode) {
		PCCode = pCCode;
	}
	public String getSAPId() {
		return SAPId;
	}
	public void setSAPId(String sAPId) {
		SAPId = sAPId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getDrugTestFailDate() {
		return drugTestFailDate;
	}
	public void setDrugTestFailDate(Date drugTestFailDate) {
		this.drugTestFailDate = drugTestFailDate;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public StateMaster getStateMaster() {
		return stateMaster;
	}
	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getAlum() {
		return alum;
	}
	public void setAlum(String alum) {
		this.alum = alum;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getCertificationCode() {
		return certificationCode;
	}
	public void setCertificationCode(String certificationCode) {
		this.certificationCode = certificationCode;
	}
	public String getEligibleToTransfer() {
		return eligibleToTransfer;
	}
	public void setEligibleToTransfer(String eligibleToTransfer) {
		this.eligibleToTransfer = eligibleToTransfer;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	public Date getDismissedDate() {
		return dismissedDate;
	}
	public void setDismissedDate(Date dismissedDate) {
		this.dismissedDate = dismissedDate;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public Date getDoNotHireDate() {
		return doNotHireDate;
	}
	public void setDoNotHireDate(Date doNotHireDate) {
		this.doNotHireDate = doNotHireDate;
	}
	public String getDoNotHireComment() {
		return doNotHireComment;
	}
	public void setDoNotHireComment(String doNotHireComment) {
		this.doNotHireComment = doNotHireComment;
	}
	public String getDoNotHireStatus() {
		return doNotHireStatus;
	}
	public void setDoNotHireStatus(String doNotHireStatus) {
		this.doNotHireStatus = doNotHireStatus;
	}
	public String getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
	public String getOMN() {
		return OMN;
	}
	public void setOMN(String oMN) {
		OMN = oMN;
	}
	public String getCurrentPositionName() {
		return CurrentPositionName;
	}
	public void setCurrentPositionName(String currentPositionName) {
		CurrentPositionName = currentPositionName;
	}
	public String getCurrentPositionNumber() {
		return CurrentPositionNumber;
	}
	public void setCurrentPositionNumber(String currentPositionNumber) {
		CurrentPositionNumber = currentPositionNumber;
	}
	public String getFTE() {
		return FTE;
	}
	public void setFTE(String fTE) {
		FTE = fTE;
	}
	public String getSeniority_number() {
		return seniority_number;
	}
	public void setSeniority_number(String seniorityNumber) {
		seniority_number = seniorityNumber;
	}
	public Date getSeniority_date() {
		return seniority_date;
	}
	public void setSeniority_date(Date seniorityDate) {
		seniority_date = seniorityDate;
	}
	public String getDT() {
		return DT;
	}
	public void setDT(String dT) {
		DT = dT;
	}
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	public String getSupervisorFirstName() {
		return supervisorFirstName;
	}
	public void setSupervisorFirstName(String supervisorFirstName) {
		this.supervisorFirstName = supervisorFirstName;
	}
	public String getSupervisorLastName() {
		return supervisorLastName;
	}
	public void setSupervisorLastName(String supervisorLastName) {
		this.supervisorLastName = supervisorLastName;
	}
	public String getSupervisorEmail() {
		return supervisorEmail;
	}
	public void setSupervisorEmail(String supervisorEmail) {
		this.supervisorEmail = supervisorEmail;
	}
	public String getSupervisorPhone() {
		return supervisorPhone;
	}
	public void setSupervisorPhone(String supervisorPhone) {
		this.supervisorPhone = supervisorPhone;
	}
	public String getSuperVisorComment() {
		return superVisorComment;
	}
	public void setSuperVisorComment(String superVisorComment) {
		this.superVisorComment = superVisorComment;
	}
	public Date getSupervisorAvailDate() {
		return supervisorAvailDate;
	}
	public void setSupervisorAvailDate(Date supervisorAvailDate) {
		this.supervisorAvailDate = supervisorAvailDate;
	}
}
