package tm.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;

@Entity
@Table(name="schoolinjoborder")
public class SchoolInJobOrder implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1179362618926723062L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int jobOrderSchoolId;     
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobId;    
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster  schoolId;
	
	/*@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "schoolmaster", joinColumns = { @JoinColumn(name = "schoolId") }, inverseJoinColumns = { @JoinColumn(name = "districtId") })
	private List<DistrictMaster> district = new ArrayList<DistrictMaster>(0);
	
	public List<DistrictMaster> getDistrict() {
		return district;
	}


	public void setDistrict(List<DistrictMaster> district) {
		this.district = district;
	}*/



	private Integer noOfSchoolExpHires;
	
	private Date createdDateTime;
	
	public Date getCreatedDateTime() {
		return createdDateTime;
	}


	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}


	public int getJobOrderSchoolId() {
		return jobOrderSchoolId;
	}


	public void setJobOrderSchoolId(int jobOrderSchoolId) {
		this.jobOrderSchoolId = jobOrderSchoolId;
	}


	public JobOrder getJobId() {
		return jobId;
	}


	public void setJobId(JobOrder jobId) {
		this.jobId = jobId;
	}


	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}


	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}


	public SchoolMaster getSchoolId() {
		return schoolId;
	}


	public void setSchoolId(SchoolMaster schoolId) {
		this.schoolId = schoolId;
	}

	public Integer getNoOfSchoolExpHires() {
		return noOfSchoolExpHires;
	}


	public void setNoOfSchoolExpHires(Integer noOfSchoolExpHires) {
		this.noOfSchoolExpHires = noOfSchoolExpHires;
	}
	
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
