package tm.bean;

import java.io.Serializable;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="districtspecifictfaoptions")
public class DistrictSpecificTfaOptions implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3433520509926829289L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtspecifictfaoptionsId;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	private String tfaOptions;
	private String status;
	public Integer getDistrictspecifictfaoptionsId() {
		return districtspecifictfaoptionsId;
	}
	public void setDistrictspecifictfaoptionsId(Integer districtspecifictfaoptionsId) {
		this.districtspecifictfaoptionsId = districtspecifictfaoptionsId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getTfaOptions() {
		return tfaOptions;
	}
	public void setTfaOptions(String tfaOptions) {
		this.tfaOptions = tfaOptions;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
