package tm.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="districtspecificdocuments")
public class DistrictSpecificDocuments implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -2345073720731423335L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer documentId;

	@ManyToOne
	@JoinColumn(name="uploadedBy",referencedColumnName="userId")
	private UserMaster userMaster;

	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="documentFileId",referencedColumnName="documentFileId")
	private DocumentFileMaster documentFileMaster;

	private String documentName;
	private Date expireDate;
	private Boolean docType;
	private Date createdDateTime;
	private String status;
	private Boolean uploadFile;
	private String ipAddress;
	
	public Integer getJobDocsId() {
		return documentId;
	}
	public void setJobDocsId(Integer documentId) {
		this.documentId = documentId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	
	public Integer getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}
	public DocumentFileMaster getDocumentFileMaster() {
		return documentFileMaster;
	}
	public void setDocumentFileMaster(DocumentFileMaster documentFileMaster) {
		this.documentFileMaster = documentFileMaster;
	}
	public Date getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}
	public Boolean getDocType() {
		return docType;
	}
	public void setDocType(Boolean docType) {
		this.docType = docType;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}
	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}
	
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getUploadFile() {
		return uploadFile;
	}
	public void setUploadFile(Boolean uploadFile) {
		this.uploadFile = uploadFile;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	@Override
	public String toString() {
		return  documentId+"";
	}
	
}
