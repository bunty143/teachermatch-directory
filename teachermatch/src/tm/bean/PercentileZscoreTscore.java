package tm.bean;

import java.io.Serializable;

public class PercentileZscoreTscore implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6540264023552045500L;
	private Double percentile;
	private Double zScore;
	private Double tScore;
	
	public Double gettScore() {
		return tScore;
	}
	public void settScore(Double tScore) {
		this.tScore = tScore;
	}
	public Double getPercentile() {
		return percentile;
	}
	public void setPercentile(Double percentile) {
		this.percentile = percentile;
	}
	public Double getzScore() {
		return zScore;
	}
	public void setzScore(Double zScore) {
		this.zScore = zScore;
	}
	
	
	
		
}
