package tm.bean;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.DistrictMaster;
import tm.bean.master.EligibilityMaster;
import tm.bean.master.EthinicityMaster;
import tm.bean.master.EthnicOriginMaster;
import tm.bean.master.GenderMaster;
import tm.bean.user.UserMaster;


@Entity
@Table(name="sapeeocdetails")
public class SapEEOCDetails implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4270231327986754839L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer sapEEOCId;
	private String frsCode;
	//private Integer ethnicOriginCode;
	@ManyToOne
	@JoinColumn(name="ethnicOriginCode",referencedColumnName="ethnicOriginId")
	private EthnicOriginMaster ethnicOriginMaster ;
	@ManyToOne
	@JoinColumn(name="ethnicityCode",referencedColumnName="ethnicityId")
	private EthinicityMaster ethinicityMaster;
	//private Integer ethnicityCode;
	private String raceCode;
	@ManyToOne
	@JoinColumn(name="genderCode",referencedColumnName="genderId")
	private GenderMaster genderMaster;
	//private Integer genderCode;
	private Date createdDateTime;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;

	public Integer getSapEEOCId() {
		return sapEEOCId;
	}

	public void setSapEEOCId(Integer sapEEOCId) {
		this.sapEEOCId = sapEEOCId;
	}

	public String getFrsCode() {
		return frsCode;
	}

	public void setFrsCode(String frsCode) {
		this.frsCode = frsCode;
	}

	public EthnicOriginMaster getEthnicOriginMaster() {
		return ethnicOriginMaster;
	}

	public void setEthnicOriginMaster(EthnicOriginMaster ethnicOriginMaster) {
		this.ethnicOriginMaster = ethnicOriginMaster;
	}

	public EthinicityMaster getEthinicityMaster() {
		return ethinicityMaster;
	}

	public void setEthinicityMaster(EthinicityMaster ethinicityMaster) {
		this.ethinicityMaster = ethinicityMaster;
	}

	public String getRaceCode() {
		return raceCode;
	}

	public void setRaceCode(String raceCode) {
		this.raceCode = raceCode;
	}

	public GenderMaster getGenderMaster() {
		return genderMaster;
	}

	public void setGenderMaster(GenderMaster genderMaster) {
		this.genderMaster = genderMaster;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	
	
	
}
