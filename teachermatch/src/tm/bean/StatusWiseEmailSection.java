package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;


@Entity
@Table(name="statuswiseemailsection")
public class StatusWiseEmailSection implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3055883393047707765L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer emailSectionId;

	private Integer headQuarterId;
	private Integer branchId;
	private Integer districtId;
	
	private String sectionName;
	private String status;
	private Integer createdBy;
	private Date createdDateTime;
	

	public Integer getHeadQuarterId() {
		return headQuarterId;
	}
	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public Integer getEmailSectionId() {
		return emailSectionId;
	}
	public void setEmailSectionId(Integer emailSectionId) {
		this.emailSectionId = emailSectionId;
	}
	
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

}
