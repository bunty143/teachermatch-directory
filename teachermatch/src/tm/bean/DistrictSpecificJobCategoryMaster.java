package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.StateMaster;

@Entity(name="districtspecificjobcategory")
public class DistrictSpecificJobCategoryMaster implements Serializable{

	
	private static final long serialVersionUID = -8127768371638203841L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer djobcatId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	
	private String jobCode;
	private String desc;
	private String PATSJobFamily;
	private String tmJobCategory;
	private String status;
	private String subJobCategory;
	private Date effectiveDate;
	
	public Integer getDjobcatId() {
		return djobcatId;
	}
	public void setDjobcatId(Integer djobcatId) {
		this.djobcatId = djobcatId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}
	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}
	public String getJobCode() {
		return jobCode;
	}
	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getPATSJobFamily() {
		return PATSJobFamily;
	}
	public void setPATSJobFamily(String pATSJobFamily) {
		PATSJobFamily = pATSJobFamily;
	}
	public String getTmJobCategory() {
		return tmJobCategory;
	}
	public void setTmJobCategory(String tmJobCategory) {
		this.tmJobCategory = tmJobCategory;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubJobCategory() {
		return subJobCategory;
	}
	public void setSubJobCategory(String subJobCategory) {
		this.subJobCategory = subJobCategory;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	
}
