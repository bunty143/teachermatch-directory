package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;

@Entity
@Table(name="districtjobcode")
public class DistrictJobCode implements Serializable 
{

	private static final long serialVersionUID = 8732709207189450690L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Integer jobCodeId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
    private DistrictMaster districtMaster;
    private String jobCode;
    private String jobTitle;
    private Double hours;
    private String jobDescription;
    private String status;
    @ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
    private JobCategoryMaster jobCategoryMaster;
    private String ipAddress;
	public Integer getJobCodeId() {
		return jobCodeId;
	}
	public void setJobCodeId(Integer jobCodeId) {
		this.jobCodeId = jobCodeId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getJobCode() {
		return jobCode;
	}
	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public Double getHours() {
		return hours;
	}
	public void setHours(Double hours) {
		this.hours = hours;
	}
	public String getJobDescription() {
		return jobDescription;
	}
	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}
	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
    
    
	
}
