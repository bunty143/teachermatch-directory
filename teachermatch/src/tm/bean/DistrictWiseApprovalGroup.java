package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
/*
 * @Author : Ram Nath
 */

/*
 CREATE TABLE `districtwiseapprovalgroup` (
  `approvalId` int(11) NOT NULL auto_increment,
  `districtId` int(11) NOT NULL,
  `groupName` varchar(255) ,
  `groupShortName` varchar(255) NOT NULL ,
  `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `status` varchar(1) NOT NULL,
   PRIMARY KEY  (`approvalId`),
   KEY `districtId` (`districtId`),
   CONSTRAINT `districtId_ibfk_1`  FOREIGN KEY ( districtId ) REFERENCES districtmaster( districtId )
);
 */
@Entity
@Table(name="districtwiseapprovalgroup")
public class DistrictWiseApprovalGroup implements Serializable{
	@Column(name="approvalId")
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private Integer  approvalId;
	@JoinColumn(name="districtId", referencedColumnName="districtId")
	@OneToOne(fetch=FetchType.LAZY)
	private DistrictMaster districtId;
	@Column(name="groupName")
	private String groupName;
	@Column(name="groupShortName")
	private String groupShortName;
	@Column(name="createdDateTime")
	private Date createdDateTime=new Date();
	@Column(name="status")
	private String status;
	public Integer getApprovalId() {
		return approvalId;
	}
	public void setApprovalId(Integer approvalId) {
		this.approvalId = approvalId;
	}
	
	public DistrictMaster getDistrictId() {
		return districtId;
	}
	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupShortName() {
		return groupShortName;
	}
	public void setGroupShortName(String groupShortName) {
		this.groupShortName = groupShortName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((approvalId == null) ? 0 : approvalId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DistrictWiseApprovalGroup other = (DistrictWiseApprovalGroup) obj;
		if (approvalId == null) {
			if (other.approvalId != null)
				return false;
		} else if (!approvalId.equals(other.approvalId))
			return false;
		return true;
	}

}
