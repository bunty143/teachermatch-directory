package tm.bean.menu;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.RoleMaster;

@Entity
@Table(name="roleaccesspermission")
public class RoleAccessPermission implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2436689154509679411L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer accessId;
	
	@ManyToOne
	@JoinColumn(name="menuId", referencedColumnName = "menuId")
	private MenuMaster menuId;
	
	@ManyToOne
	@JoinColumn(name="roleId", referencedColumnName = "roleId")
	private RoleMaster roleId;
	
	@ManyToOne
	@JoinColumn(name="menuSectionId", referencedColumnName = "menuSectionId")
	private MenuSectionMaster menuSectionId;
	
	private String optionid;
	private String status;
	
	public Integer getAccessId() {
		return accessId;
	}
	public void setAccessId(Integer accessId) {
		this.accessId = accessId;
	}
	public MenuMaster getMenuId() {
		return menuId;
	}
	public void setMenuId(MenuMaster menuId) {
		this.menuId = menuId;
	}
	public RoleMaster getRoleId() {
		return roleId;
	}
	public void setRoleId(RoleMaster roleId) {
		this.roleId = roleId;
	}
	public MenuSectionMaster getMenuSectionId() {
		return menuSectionId;
	}
	public void setMenuSectionId(MenuSectionMaster menuSectionId) {
		this.menuSectionId = menuSectionId;
	}
	public String getOptionid() {
		return optionid;
	}
	public void setOptionid(String optionid) {
		this.optionid = optionid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
