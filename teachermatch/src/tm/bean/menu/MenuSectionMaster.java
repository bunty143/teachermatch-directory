package tm.bean.menu;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.RoleMaster;

@Entity
@Table(name="menusectionmaster")
public class MenuSectionMaster implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5984011798906232345L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer menuSectionId;
	
	private Integer menuId;
	private String menuSectionName;
	
	public Integer getMenuSectionId() {
		return menuSectionId;
	}
	public void setMenuSectionId(Integer menuSectionId) {
		this.menuSectionId = menuSectionId;
	}
	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	public String getMenuSectionName() {
		return menuSectionName;
	}
	public void setMenuSectionName(String menuSectionName) {
		this.menuSectionName = menuSectionName;
	}
}
