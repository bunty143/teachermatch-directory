package tm.bean.menu;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="menumaster")
public class MenuMaster  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4454199325069947406L;

	/**
	 * 
	 */
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer menuId;
	
	@ManyToOne
	@JoinColumn(name="parentMenuId", referencedColumnName = "menuId")
	private MenuMaster parentMenuId;
	
	
	@ManyToOne
	@JoinColumn(name="subMenuId", referencedColumnName = "menuId")
	private MenuMaster subMenuId;
	
	private String menuName;
	private String pageName;
	private String pageNameWithSubPage;
	private Integer orderBy;
	private String status;
	private String districtId;	
	private Integer menuArrow;
	private String description;
		
	 
	 
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getMenuArrow() {
		return menuArrow;
	}
	public void setMenuArrow(Integer menuArrow) {
		this.menuArrow = menuArrow;
	}
	public String getDistrictId() {
		return districtId;
	}
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}
	public MenuMaster getSubMenuId() {
		return subMenuId;
	}
	public void setSubMenuId(MenuMaster subMenuId) {
		this.subMenuId = subMenuId;
	}
	
	public String getPageNameWithSubPage() {
		return pageNameWithSubPage;
	}
	public void setPageNameWithSubPage(String pageNameWithSubPage) {
		this.pageNameWithSubPage = pageNameWithSubPage;
	}
	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	
	
	public Integer getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}
	public MenuMaster getParentMenuId() {
		return parentMenuId;
	}
	public void setParentMenuId(MenuMaster parentMenuId) {
		this.parentMenuId = parentMenuId;
	}
	
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
