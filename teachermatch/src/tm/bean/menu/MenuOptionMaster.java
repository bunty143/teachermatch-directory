package tm.bean.menu;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="menuoptionmaster")
public class MenuOptionMaster implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6073911022178172211L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer optionid;
		
	@ManyToOne
	@JoinColumn(name="menuid",referencedColumnName="menuid")
	
	private MenuMaster menuid; 
	private Integer menuSectionId;
	private Integer accessOptionId;
	private String status;
	
	public MenuMaster getMenuid() {
		return menuid;
	}
	public void setMenuid(MenuMaster menuid) {
		this.menuid = menuid;
	}
	public Integer getOptionid() {
		return optionid;
	}
	public void setOptionid(Integer optionid) {
		this.optionid = optionid;
	}
	public Integer getMenuSectionId() {
		return menuSectionId;
	}
	public void setMenuSectionId(Integer menuSectionId) {
		this.menuSectionId = menuSectionId;
	}
	public Integer getAccessOptionId() {
		return accessOptionId;
	}
	public void setAccessOptionId(Integer accessOptionId) {
		this.accessOptionId = accessOptionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
