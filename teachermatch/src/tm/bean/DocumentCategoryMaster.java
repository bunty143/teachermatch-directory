package tm.bean;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="documentcategorymaster")
public class DocumentCategoryMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5597248929549862823L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer documentCategoryId;
	
	private String documentCategory;

	public Integer getDocumentCategoryId() {
		return documentCategoryId;
	}

	public void setDocumentCategoryId(Integer documentCategoryId) {
		this.documentCategoryId = documentCategoryId;
	}

	public String getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}
	

}
