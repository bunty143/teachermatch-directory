package tm.bean.user;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import tm.bean.EmployeeMaster;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;


@Entity
@Table(name="usermaster")
public class UserMaster  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2183204264206577556L;
	/*
	  `entityType` int(1) NOT NULL COMMENT '"1" - TM, "2" - District, "3" - School ',
	  `schoolId` int(5) default NULL COMMENT '"Save schoolId if entityType is 3  
	  'districtId' Foreign Key to ""districtAutoId"" in table ""district" ',
	  `salutation` int(2) default NULL COMMENT '"1" - Mr., "2" - Ms., "3" - Mrs., "4" - Dr. ',
	  `status` varchar(1) NOT NULL COMMENT '"A" - Active, "I" - Inactive ',
	 */
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer userId; 
	private Integer entityType; 
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtId;     

	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolId;     
	
	
	private Integer salutation; 
	@ManyToOne
	@JoinColumn(name="employeeId",referencedColumnName="employeeId")
	private EmployeeMaster employeeMaster; 
	
	private String firstName; 
	private String middleName;
	private String lastName; 
	private String title; 
	private String emailAddress; 
	private String password; 
	private String phoneNumber; 
	private String mobileNumber;
	
	@ManyToOne
	@JoinColumn(name="roleId",referencedColumnName="roleId")
	private RoleMaster roleId;
	
	private String authenticationCode; 
	private String verificationCode;
	private String photoPath; 
	private String status;
	private Date fgtPwdDateTime;
	private Date createdDateTime;
	private CommonsMultipartFile 	photoPathFile;
	private Integer forgetCounter;
	private Boolean isExternal;
	private Boolean isQuestCandidate;
	private String tableauUserStatus;
	
	public String getTableauUserStatus() {
		return tableauUserStatus;
	}
	public void setTableauUserStatus(String tableauUserStatus) {
		this.tableauUserStatus = tableauUserStatus;
	}
	public CommonsMultipartFile getPhotoPathFile() {
		return photoPathFile;
	}
	public void setPhotoPathFile(CommonsMultipartFile photoPathFile) {
		this.photoPathFile = photoPathFile;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public DistrictMaster getDistrictId() {
		return districtId;
	}
	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}
	public SchoolMaster getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(SchoolMaster schoolId) {
		this.schoolId = schoolId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Integer getEntityType() {
		return entityType;
	}
	public void setEntityType(Integer entityType) {
		this.entityType = entityType;
	}

	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	public Integer getSalutation() {
		return salutation;
	}
	public void setSalutation(Integer salutation) {
		this.salutation = salutation;
	}
	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}
	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public RoleMaster getRoleId() {
		return roleId;
	}
	public void setRoleId(RoleMaster roleId) {
		this.roleId = roleId;
	}
	public String getAuthenticationCode() {
		return authenticationCode;
	}
	public void setAuthenticationCode(String authenticationCode) {
		this.authenticationCode = authenticationCode;
	}
	public String getVerificationCode() {
		return verificationCode;
	}
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	public String getPhotoPath() {
		return photoPath;
	}
	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getFgtPwdDateTime() {
		return fgtPwdDateTime;
	}
	public void setFgtPwdDateTime(Date fgtPwdDateTime) {
		this.fgtPwdDateTime = fgtPwdDateTime;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Integer getForgetCounter() {
		return forgetCounter;
	}
	public void setForgetCounter(Integer forgetCounter) {
		this.forgetCounter = forgetCounter;
	}
	public Boolean getIsExternal() {
		return isExternal;
	}
	public void setIsExternal(Boolean isExternal) {
		this.isExternal = isExternal;
	}
	
	public Boolean getIsQuestCandidate() {
		return isQuestCandidate;
	}
	public void setIsQuestCandidate(Boolean isQuestCandidate) {
		this.isQuestCandidate = isQuestCandidate;
	}
	@Override
	public String toString() 
	{
		// TODO Auto-generated method stub
		return userId.toString();
		
	}
	
	public static Comparator<UserMaster> userNameAsc  = new Comparator<UserMaster>(){
		public int compare(UserMaster userMaster1, UserMaster userMaster2) {
			
			String sFirstName1 = userMaster1.getFirstName().toUpperCase();
		    String sLastName1 = userMaster1.getLastName().toUpperCase();
		    String sName1="";
		    if(sFirstName1!=null && !sFirstName1.equals(""))
		    	sName1=sFirstName1.trim();
		    if(sLastName1!=null && !sLastName1.equals(""))
		    	sName1=sName1+" "+sLastName1.trim();
		    sName1.trim();
		    
		    String sFirstName2 = userMaster2.getFirstName().toUpperCase();
		    String sLastName2 = userMaster2.getLastName().toUpperCase();
		    String sName2="";
		    if(sFirstName2!=null && !sFirstName2.equals(""))
		    	sName2=sFirstName2.trim();
		    if(sLastName2!=null && !sLastName2.equals(""))
		    	sName2=sName2+" "+sLastName2.trim();
		    sName2.trim();
		    
		    //ascending order
		      return sName1.compareTo(sName2);
		      
		    //descending  order
		      //return sName2.compareTo(sName1);
			
		}		
	};

	@Override
	public boolean equals(Object object) {
		if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof UserMaster))return false;
	    UserMaster userMaster = (UserMaster)object;
	  
	    if(this.userId.equals(userMaster.getUserId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	@Override
	public int hashCode() {
		return new Integer(""+userId);
	}
	
	
 
	
	
}
