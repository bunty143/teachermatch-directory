package tm.bean.user;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import tm.bean.EmployeeMaster;
import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.PanelAttendees;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SchoolTypeMaster;


@Entity
@Table(name="tempusermaster")
public class UserMasterTemp  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -674728941077328764L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer tempuserId; 
	private Integer districtId;
	private Integer entityType; 
	private String role;
	private String location;
	private String firstName; 
	private String lastName; 
	private String emailAddress;
	private String sessionid; 
	private String errortext;
	
	private Date date;
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public String getErrortext() {
		return errortext;
	}
	public void setErrortext(String errortext) {
		this.errortext = errortext;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public Integer getTempuserId() {
		return tempuserId;
	}
	public void setTempuserId(Integer tempuserId) {
		this.tempuserId = tempuserId;
	}
	public Integer getEntityType() {
		return entityType;
	}
	public void setEntityType(Integer entityType) {
		this.entityType = entityType;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	} 
	
}
