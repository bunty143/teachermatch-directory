package tm.bean.user;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="rolemaster")
public class RoleMaster implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5414141646987745880L;

	/*
    `entityType` int(1) NOT NULL COMMENT '"1" - TM, "2" - District, "3" - School ',
    `status` varchar(1) NOT NULL COMMENT '"A" - Active, "I" - Inactive ',
	 */

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer roleId;	
	
	private String roleName;
	private Integer entityType;
	private String status;
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Integer getEntityType() {
		return entityType;
	}
	public void setEntityType(Integer entityType) {
		this.entityType = entityType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
