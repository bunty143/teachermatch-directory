package tm.bean.user;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="tableauusermaster")
public class TableauUserMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer tableauUserId;
	private Integer districtId;
	private String tableauUserName;
	private String description;
	private Integer createdBy;
	private String status;
	private Date createdDateTime;
	private Integer entityType;
	
	public Integer getEntityType() {
		return entityType;
	}
	public void setEntityType(Integer entityType) {
		this.entityType = entityType;
	}
	public Integer getTableauUserId() {
		return tableauUserId;
	}
	public void setTableauUserId(Integer tableauUserId) {
		this.tableauUserId = tableauUserId;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public String getTableauUserName() {
		return tableauUserName;
	}
	public void setTableauUserName(String tableauUserName) {
		this.tableauUserName = tableauUserName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
	

}
