package tm.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.ObjectiveMaster;
import tm.bean.master.QuestionTypeMaster;

@Entity
@Table(name="teacherassessmentquestions")
public class TeacherAssessmentQuestion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2016462028409257800L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long teacherAssessmentQuestionId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;   
	@ManyToOne
	@JoinColumn(name="teacherAssessmentId",referencedColumnName="teacherAssessmentId")
	private TeacherAssessmentdetail teacherAssessmentdetail;
	@ManyToOne
	@JoinColumn(name="teacherSectionId",referencedColumnName="teacherSectionId")
	private TeacherSectionDetail teacherSectionDetail;
	@ManyToOne
	@JoinColumn(name="domainId",referencedColumnName="domainId")
	private DomainMaster domainMaster;   
	@ManyToOne
	@JoinColumn(name="competencyId",referencedColumnName="competencyId")
	private CompetencyMaster competencyMaster;  
	@ManyToOne
	@JoinColumn(name="objectiveId",referencedColumnName="objectiveId")
	private ObjectiveMaster objectiveMaster;
	private String questionUId;
	private String question; 
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster questionTypeMaster; 
	private Double questionWeightage;
	private Double maxMarks;
	private String questionInstruction;
	private String validationType; 
	private Integer fieldLength;    
	private Boolean isMandatory;    
	private Integer questionPosition;
	private Date createdDateTime;
	private Boolean isAttempted;
	
	@OneToMany()
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="teacherAssessmentQuestionId",referencedColumnName="teacherAssessmentQuestionId",insertable=false,updatable=false)
	private List<TeacherAssessmentOption> teacherAssessmentOptions;
	public Long getTeacherAssessmentQuestionId() {
		return teacherAssessmentQuestionId;
	}
	public void setTeacherAssessmentQuestionId(Long teacherAssessmentQuestionId) {
		this.teacherAssessmentQuestionId = teacherAssessmentQuestionId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	
	public TeacherAssessmentdetail getTeacherAssessmentdetail() {
		return teacherAssessmentdetail;
	}
	public void setTeacherAssessmentdetail(
			TeacherAssessmentdetail teacherAssessmentdetail) {
		this.teacherAssessmentdetail = teacherAssessmentdetail;
	}
	public TeacherSectionDetail getTeacherSectionDetail() {
		return teacherSectionDetail;
	}
	public void setTeacherSectionDetail(TeacherSectionDetail teacherSectionDetail) {
		this.teacherSectionDetail = teacherSectionDetail;
	}
	public DomainMaster getDomainMaster() {
		return domainMaster;
	}
	public void setDomainMaster(DomainMaster domainMaster) {
		this.domainMaster = domainMaster;
	}
	public CompetencyMaster getCompetencyMaster() {
		return competencyMaster;
	}
	public void setCompetencyMaster(CompetencyMaster competencyMaster) {
		this.competencyMaster = competencyMaster;
	}
	public ObjectiveMaster getObjectiveMaster() {
		return objectiveMaster;
	}
	public void setObjectiveMaster(ObjectiveMaster objectiveMaster) {
		this.objectiveMaster = objectiveMaster;
	}
	
	public String getQuestionUId() {
		return questionUId;
	}
	public void setQuestionUId(String questionUId) {
		this.questionUId = questionUId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}
	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}
	public Double getQuestionWeightage() {
		return questionWeightage;
	}
	public void setQuestionWeightage(Double questionWeightage) {
		this.questionWeightage = questionWeightage;
	}
	public String getQuestionInstruction() {
		return questionInstruction;
	}
	public void setQuestionInstruction(String questionInstruction) {
		this.questionInstruction = questionInstruction;
	}
	public String getValidationType() {
		return validationType;
	}
	public void setValidationType(String validationType) {
		this.validationType = validationType;
	}
	public Integer getFieldLength() {
		return fieldLength;
	}
	public void setFieldLength(Integer fieldLength) {
		this.fieldLength = fieldLength;
	}
	public Boolean getIsMandatory() {
		return isMandatory;
	}
	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}
	public Integer getQuestionPosition() {
		return questionPosition;
	}
	public void setQuestionPosition(Integer questionPosition) {
		this.questionPosition = questionPosition;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Boolean getIsAttempted() {
		return isAttempted;
	}
	public void setIsAttempted(Boolean isAttempted) {
		this.isAttempted = isAttempted;
	}
	public List<TeacherAssessmentOption> getTeacherAssessmentOptions() {
		return teacherAssessmentOptions;
	}
	public void setTeacherAssessmentOptions(
			List<TeacherAssessmentOption> teacherAssessmentOptions) {
		this.teacherAssessmentOptions = teacherAssessmentOptions;
	}
	public Double getMaxMarks() {
		return maxMarks;
	}
	public void setMaxMarks(Double maxMarks) {
		this.maxMarks = maxMarks;
	}
	

	
}
