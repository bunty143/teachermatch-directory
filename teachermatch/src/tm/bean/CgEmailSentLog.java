package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cgemailsentlog")
public class CgEmailSentLog implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8629463200177498924L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer cgmailId;
	private String sentTos;
	private String districtName;
	private String schoolName;
	private String jobTitle;
	private Integer jobType;
	private Date createdDateTime;
	public Integer getCgmailId() {
		return cgmailId;
	}
	public void setCgmailId(Integer cgmailId) {
		this.cgmailId = cgmailId;
	}
	public String getSentTos() {
		return sentTos;
	}
	public void setSentTos(String sentTos) {
		this.sentTos = sentTos;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public Integer getJobType() {
		return jobType;
	}
	public void setJobType(Integer jobType) {
		this.jobType = jobType;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
}

