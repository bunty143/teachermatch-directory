package tm.bean.i4;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="questionsetquestions")
public class I4QuestionSetQuestions implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1837713016990875572L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer ID;
	
	@ManyToOne
	@JoinColumn(name="QuestionSetID",referencedColumnName="ID")
	private I4QuestionSets questionSets;
	
	@ManyToOne
	@JoinColumn(name="QuestionID",referencedColumnName="ID")
	private I4QuestionPool questionPool;

	private Integer QuestionSequence;
	
	public Integer getQuestionSequence() {
		return QuestionSequence;
	}

	public void setQuestionSequence(Integer questionSequence) {
		QuestionSequence = questionSequence;
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public I4QuestionSets getQuestionSets() {
		return questionSets;
	}

	public void setQuestionSets(I4QuestionSets questionSets) {
		this.questionSets = questionSets;
	}

	public I4QuestionPool getQuestionPool() {
		return questionPool;
	}

	public void setQuestionPool(I4QuestionPool questionPool) {
		this.questionPool = questionPool;
	}
	
	
	
}
