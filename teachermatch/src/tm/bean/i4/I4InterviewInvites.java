package tm.bean.i4;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;

@Entity
@Table(name="i4_interviewinvites")
public class I4InterviewInvites implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -631961748847111644L;

	/**
	 * 
	 */

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="jobid",referencedColumnName="jobid")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="candidateid",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="questionsetid",referencedColumnName="ID")
	private I4QuestionSets i4QuestionSets;
	
	private Integer status;
	private Date datetimeset;
	private Integer I4inviteid;
	private String interviewurl;
	private Date completedDateTime;
	private String userAssessmentId; 
	private Integer i4VideoInterviewScore; 
	private Integer i4VideoInterviewMaxScore; 
	
	
	private Integer districtId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public I4QuestionSets getI4QuestionSets() {
		return i4QuestionSets;
	}
	public void setI4QuestionSets(I4QuestionSets i4QuestionSets) {
		this.i4QuestionSets = i4QuestionSets;
	}
	
	public Date getDatetimeset() {
		return datetimeset;
	}
	public void setDatetimeset(Date datetimeset) {
		this.datetimeset = datetimeset;
	}
	public Integer getI4inviteid() {
		return I4inviteid;
	}
	public void setI4inviteid(Integer i4inviteid) {
		I4inviteid = i4inviteid;
	}
	public String getInterviewurl() {
		return interviewurl;
	}
	public void setInterviewurl(String interviewurl) {
		this.interviewurl = interviewurl;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getCompletedDateTime() {
		return completedDateTime;
	}
	public void setCompletedDateTime(Date completedDateTime) {
		this.completedDateTime = completedDateTime;
	}
	public Integer getI4VideoInterviewScore() {
		return i4VideoInterviewScore;
	}
	public void setI4VideoInterviewScore(Integer i4VideoInterviewScore) {
		this.i4VideoInterviewScore = i4VideoInterviewScore;
	}
	public Integer getI4VideoInterviewMaxScore() {
		return i4VideoInterviewMaxScore;
	}
	public void setI4VideoInterviewMaxScore(Integer i4VideoInterviewMaxScore) {
		this.i4VideoInterviewMaxScore = i4VideoInterviewMaxScore;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public String getUserAssessmentId() {
		return userAssessmentId;
	}
	public void setUserAssessmentId(String userAssessmentId) {
		this.userAssessmentId = userAssessmentId;
	}
}
