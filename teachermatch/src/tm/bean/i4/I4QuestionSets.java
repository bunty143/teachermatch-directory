package tm.bean.i4;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="questionsets")
public class I4QuestionSets implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3328682451482087064L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer ID;
	
	@ManyToOne
	@JoinColumn(name="DistrictID",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private Integer headQuarterId;
	
	private Integer branchId;
	
	
	private String QuestionSetText;
	private String I4QuestionSetID;
	private Date DateCreated;
	private String Status;
	
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	
	
	public Integer getHeadQuarterId() {
		return headQuarterId;
	}
	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public String getQuestionSetText() {
		return QuestionSetText;
	}
	public void setQuestionSetText(String questionSetText) {
		QuestionSetText = questionSetText;
	}
	public String getI4QuestionSetID() {
		return I4QuestionSetID;
	}
	public void setI4QuestionSetID(String i4QuestionSetID) {
		I4QuestionSetID = i4QuestionSetID;
	}
	public Date getDateCreated() {
		return DateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		DateCreated = dateCreated;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		I4QuestionSets other = (I4QuestionSets) obj;
		if (QuestionSetText == null) {
			if (other.QuestionSetText != null)
				return false;
		} else if (!QuestionSetText.equals(other.QuestionSetText))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((QuestionSetText == null) ? 0 : QuestionSetText.hashCode());
		return result;
	}
	
	@Override
	public String toString() {
		return this.QuestionSetText;
	}
	
	
	
}
