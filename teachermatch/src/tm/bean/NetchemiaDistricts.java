package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="netchemiadistricts")
public class NetchemiaDistricts implements Serializable 
{

	private static final long serialVersionUID = -2215229423453988404L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer netDistrictId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	private String apiURL;
	private String status;
	private Boolean epiOnly;
	
	public Integer getNetDistrictId() {
		return netDistrictId;
	}
	public void setNetDistrictId(Integer netDistrictId) {
		this.netDistrictId = netDistrictId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getApiURL() {
		return apiURL;
	}
	public void setApiURL(String apiURL) {
		this.apiURL = apiURL;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getEpiOnly() {
		return epiOnly;
	}
	public void setEpiOnly(Boolean epiOnly) {
		this.epiOnly = epiOnly;
	}
	
}
