package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
@Entity
@Table(name="eventfacilitatorslist")
public class EventFacilitatorsList implements Serializable{
private static final long serialVersionUID = 6880404967417954807L;
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private int eventFacilitatorId;
@ManyToOne
@JoinColumn(name="eventId",referencedColumnName="eventId")
private EventDetails eventDetails;
public EventDetails getEventDetails() {
	return eventDetails;
}
public void setEventDetails(EventDetails eventDetails) {
	this.eventDetails = eventDetails;
}
@ManyToOne
@JoinColumn(name="districtId",referencedColumnName="districtId")
private DistrictMaster districtMaster;

@ManyToOne
@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
private HeadQuarterMaster headQuarterMaster;

@ManyToOne
@JoinColumn(name="branchId",referencedColumnName="branchId")
private BranchMaster branchMaster;

private String facilitatorFirstName,facilitatorLastName,facilitatorEmailAddress,status;
@ManyToOne
@JoinColumn(name="schoolId",referencedColumnName="schoolId")
private SchoolMaster schoolId;  
private int invitationEmailSent;
@ManyToOne
@JoinColumn(name="createdBy",referencedColumnName="userId")
private UserMaster createdBY;
private Date createdDateTime;
public int getEventFacilitatorId() {
	return eventFacilitatorId;
}
public void setEventFacilitatorId(int eventFacilitatorId) {
	this.eventFacilitatorId = eventFacilitatorId;
}

public DistrictMaster getDistrictMaster() {
	return districtMaster;
}
public void setDistrictMaster(DistrictMaster districtMaster) {
	this.districtMaster = districtMaster;
}
public String getFacilitatorFirstName() {
	return facilitatorFirstName;
}
public void setFacilitatorFirstName(String facilitatorFirstName) {
	this.facilitatorFirstName = facilitatorFirstName;
}
public String getFacilitatorLastName() {
	return facilitatorLastName;
}
public void setFacilitatorLastName(String facilitatorLastName) {
	this.facilitatorLastName = facilitatorLastName;
}
public String getFacilitatorEmailAddress() {
	return facilitatorEmailAddress;
}
public void setFacilitatorEmailAddress(String facilitatorEmailAddress) {
	this.facilitatorEmailAddress = facilitatorEmailAddress;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public SchoolMaster getSchoolId() {
	return schoolId;
}
public void setSchoolId(SchoolMaster schoolId) {
	this.schoolId = schoolId;
}
public int getInvitationEmailSent() {
	return invitationEmailSent;
}
public void setInvitationEmailSent(int invitationEmailSent) {
	this.invitationEmailSent = invitationEmailSent;
}
public UserMaster getCreatedBY() {
	return createdBY;
}
public void setCreatedBY(UserMaster createdBY) {
	this.createdBY = createdBY;
}
public Date getCreatedDateTime() {
	return createdDateTime;
}
public void setCreatedDateTime(Date createdDateTime) {
	this.createdDateTime = createdDateTime;
}
public HeadQuarterMaster getHeadQuarterMaster() {
	return headQuarterMaster;
}
public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
	this.headQuarterMaster = headQuarterMaster;
}
public BranchMaster getBranchMaster() {
	return branchMaster;
}
public void setBranchMaster(BranchMaster branchMaster) {
	this.branchMaster = branchMaster;
}

}