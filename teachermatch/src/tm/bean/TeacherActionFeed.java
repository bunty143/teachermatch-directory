package tm.bean;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity(name="teacheractionfeed")
public class TeacherActionFeed implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1227156916285071478L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacheractionfeedId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	private Boolean isInternal;
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	private Date jobAppliedDate;
	private Integer teacherNormScore;
	private String lastActivityOnJob;
	private Date lastActivityDate;
	private Integer daysForLastActivity;
	@ManyToOne
	@JoinColumn(name="lastActivityDoneBy",referencedColumnName="userId")
	private UserMaster userMaster;
	private Date createdDateTime;
	public Integer getTeacheractionfeedId() {
		return teacheractionfeedId;
	}
	public void setTeacheractionfeedId(Integer teacheractionfeedId) {
		this.teacheractionfeedId = teacheractionfeedId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public Boolean getIsInternal() {
		return isInternal;
	}
	public void setIsInternal(Boolean isInternal) {
		this.isInternal = isInternal;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public Date getJobAppliedDate() {
		return jobAppliedDate;
	}
	public void setJobAppliedDate(Date jobAppliedDate) {
		this.jobAppliedDate = jobAppliedDate;
	}
	public Integer getTeacherNormScore() {
		return teacherNormScore;
	}
	public void setTeacherNormScore(Integer teacherNormScore) {
		this.teacherNormScore = teacherNormScore;
	}
	public String getLastActivityOnJob() {
		return lastActivityOnJob;
	}
	public void setLastActivityOnJob(String lastActivityOnJob) {
		this.lastActivityOnJob = lastActivityOnJob;
	}
	public Date getLastActivityDate() {
		return lastActivityDate;
	}
	public void setLastActivityDate(Date lastActivityDate) {
		this.lastActivityDate = lastActivityDate;
	}
	public Integer getDaysForLastActivity() {
		return daysForLastActivity;
	}
	public void setDaysForLastActivity(Integer daysForLastActivity) {
		this.daysForLastActivity = daysForLastActivity;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	
}
