package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.core.annotation.Order;

import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.user.UserMaster;
import tm.utility.Utility;

@Entity
@Table(name="teacherwiseassessmenttime")
public class TeacherWiseAssessmentTime implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2992443911091426852L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer assessmentTimeId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentId;
	private Integer assessmentType;
	private Integer questionSessionTime;
	private Integer assessmentSessionTime;
	private String assessmentFlag;
	@ManyToOne
	@JoinColumn(name="timeAllowedBy",referencedColumnName="userId")
	private UserMaster timeAllowedBy;	
	private String ipAddress;
	private Date createdDateTime;
	
	public Integer getAssessmentTimeId() {
		return assessmentTimeId;
	}
	public void setAssessmentTimeId(Integer assessmentTimeId) {
		this.assessmentTimeId = assessmentTimeId;
	}
	
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public AssessmentDetail getAssessmentId() {
		return assessmentId;
	}
	public void setAssessmentId(AssessmentDetail assessmentId) {
		this.assessmentId = assessmentId;
	}
	public Integer getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(Integer assessmentType) {
		this.assessmentType = assessmentType;
	}
	public Integer getQuestionSessionTime() {
		return questionSessionTime;
	}
	public void setQuestionSessionTime(Integer questionSessionTime) {
		this.questionSessionTime = questionSessionTime;
	}
	public Integer getAssessmentSessionTime() {
		return assessmentSessionTime;
	}
	public void setAssessmentSessionTime(Integer assessmentSessionTime) {
		this.assessmentSessionTime = assessmentSessionTime;
	}
	
	public String getAssessmentFlag() {
		return assessmentFlag;
	}
	public void setAssessmentFlag(String assessmentFlag) {
		this.assessmentFlag = assessmentFlag;
	}
	public UserMaster getTimeAllowedBy() {
		return timeAllowedBy;
	}
	public void setTimeAllowedBy(UserMaster timeAllowedBy) {
		this.timeAllowedBy = timeAllowedBy;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
	

