package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;
import tm.utility.Utility;

@Entity
@Table(name="questionoptions")

public class QuestionOptions implements Serializable{
	

	private static final long serialVersionUID = -8108985216199516483L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer optionId;
	
	@ManyToOne
	@JoinColumn(name="questionId",referencedColumnName="questionId")
	private QuestionsPool questionsPool;
	private String questionOption;
	private Integer questionOptionTag;
	private Integer rank;
	private Double score;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	private String status;
	private Date createdDateTime;
	private String ipaddress;
	public Integer getOptionId() {
		return optionId;
	}
	public void setOptionId(Integer optionId) {
		this.optionId = optionId;
	}
	public QuestionsPool getQuestionsPool() {
		return questionsPool;
	}
	public void setQuestionsPool(QuestionsPool questionsPool) {
		this.questionsPool = questionsPool;
	}
	
	public String getQuestionOption() {
		return questionOption;
	}
	public void setQuestionOption(String questionOption) {
		this.questionOption = Utility.trim(questionOption);
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	public Integer getQuestionOptionTag() {
		return questionOptionTag;
	}
	public void setQuestionOptionTag(Integer questionOptionTag) {
		this.questionOptionTag = questionOptionTag;
	}
}