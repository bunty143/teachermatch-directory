package tm.bean.assessment;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;
import tm.bean.cgreport.RawDataForCompetency;
import tm.bean.master.CompetencyMaster;

@Entity
@Table(name="assessmentcompetencyscore")
public class AssessmentCompetencyScore implements Serializable 
{

	private static final long serialVersionUID = -1819974646803722357L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer assessmentcompetencyscoreId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="rawDataId",referencedColumnName="rawDataId")
	private RawDataForCompetency rawDataForCompetency;
	@ManyToOne
	@JoinColumn(name="competencyId",referencedColumnName="competencyId")
	private CompetencyMaster competencyMaster;
	private Double percentile;
	private Double zscore;
	private Double tscore;
	private Double ncevalue;
	private Double normscore;
	private Double ritvalue;
	@ManyToOne
	@JoinColumn(name="lookupId",referencedColumnName="lookupId")
	private ScoreLookupMaster scoreLookupMaster;
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentDetail;
	private Integer assessmentType;
	private Integer assessmentTakenCount;
	private Date createdDateTime;
	private Date assessmentDateTime;
	private Date updateDateTime;
	
	public Integer getAssessmentcompetencyscoreId() {
		return assessmentcompetencyscoreId;
	}
	public void setAssessmentcompetencyscoreId(Integer assessmentcompetencyscoreId) {
		this.assessmentcompetencyscoreId = assessmentcompetencyscoreId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public CompetencyMaster getCompetencyMaster() {
		return competencyMaster;
	}
	public void setCompetencyMaster(CompetencyMaster competencyMaster) {
		this.competencyMaster = competencyMaster;
	}
	public Double getPercentile() {
		return percentile;
	}
	public void setPercentile(Double percentile) {
		this.percentile = percentile;
	}
	public Double getZscore() {
		return zscore;
	}
	public void setZscore(Double zscore) {
		this.zscore = zscore;
	}
	public Double getTscore() {
		return tscore;
	}
	public void setTscore(Double tscore) {
		this.tscore = tscore;
	}
	public Double getNcevalue() {
		return ncevalue;
	}
	public void setNcevalue(Double ncevalue) {
		this.ncevalue = ncevalue;
	}
	public Double getRitvalue() {
		return ritvalue;
	}
	public void setRitvalue(Double ritvalue) {
		this.ritvalue = ritvalue;
	}
	public ScoreLookupMaster getScoreLookupMaster() {
		return scoreLookupMaster;
	}
	public void setScoreLookupMaster(ScoreLookupMaster scoreLookupMaster) {
		this.scoreLookupMaster = scoreLookupMaster;
	}
	public Double getNormscore() {
		return normscore;
	}
	public void setNormscore(Double normscore) {
		this.normscore = normscore;
	}
	public AssessmentDetail getAssessmentDetail() {
		return assessmentDetail;
	}
	public void setAssessmentDetail(AssessmentDetail assessmentDetail) {
		this.assessmentDetail = assessmentDetail;
	}
	public Integer getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(Integer assessmentType) {
		this.assessmentType = assessmentType;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Integer getAssessmentTakenCount() {
		return assessmentTakenCount;
	}
	public void setAssessmentTakenCount(Integer assessmentTakenCount) {
		this.assessmentTakenCount = assessmentTakenCount;
	}
	public RawDataForCompetency getRawDataForCompetency() {
		return rawDataForCompetency;
	}
	public void setRawDataForCompetency(RawDataForCompetency rawDataForCompetency) {
		this.rawDataForCompetency = rawDataForCompetency;
	}
	public Date getAssessmentDateTime() {
		return assessmentDateTime;
	}
	public void setAssessmentDateTime(Date assessmentDateTime) {
		this.assessmentDateTime = assessmentDateTime;
	}
	public Date getUpdateDateTime() {
		return updateDateTime;
	}
	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
}
