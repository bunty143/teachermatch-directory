package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.utility.Utility;

@Entity
@Table(name="assessmentdetail")
public class AssessmentDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1773899817854250976L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer assessmentId;
	private Integer questionSessionTime;
	private Integer assessmentSessionTime;
	private Integer maxQuesToDisplay;
	private Integer assessmentType;
	private Integer jobOrderType;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private  BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	
	@ManyToOne
	@JoinColumn(name="lookupId",referencedColumnName="lookupId")
	private ScoreLookupMaster scoreLookupMaster;
	
	@ManyToOne ()
	@JoinColumn(name="assessmentGroupId",referencedColumnName="assessmentGroupId")
	private AssessmentGroupDetails assessmentGroupDetails;

	private Boolean isDefault;
	private Boolean isResearchEPI;
	private Boolean questionRandomization;
	private Boolean optionRandomization;
	private String assessmentName;
	private String assessmentDescription;
	private String status;
	private String ipaddress;
	
	private Integer pdpreporttype;
	
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	@Column(name="createdDateTime",updatable=false)
	private Date createdDateTime;

	@OneToMany()
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId",insertable=false,updatable=false)
	private List<AssessmentJobRelation> assessmentJobRelations;
	
	private Integer noOfExperimentQuestions;
	private String splashInstruction1;
	private String splashInstruction2;	
	
	public Integer getAssessmentId() {
		return assessmentId;
	}

	public void setAssessmentId(Integer assessmentId) {
		this.assessmentId = assessmentId;
	}

	public Integer getQuestionSessionTime() {
		return questionSessionTime;
	}

	public void setQuestionSessionTime(Integer questionSessionTime) {
		this.questionSessionTime = questionSessionTime;
	}

	public Integer getAssessmentSessionTime() {
		return assessmentSessionTime;
	}

	public void setAssessmentSessionTime(Integer assessmentSessionTime) {
		this.assessmentSessionTime = assessmentSessionTime;
	}

	public Integer getMaxQuesToDisplay() {
		return maxQuesToDisplay;
	}

	public void setMaxQuesToDisplay(Integer maxQuesToDisplay) {
		this.maxQuesToDisplay = maxQuesToDisplay;
	}

	public Boolean getQuestionRandomization() {
		return questionRandomization;
	}

	public void setQuestionRandomization(Boolean questionRandomization) {
		this.questionRandomization = questionRandomization;
	}

	public Boolean getOptionRandomization() {
		return optionRandomization;
	}

	public void setOptionRandomization(Boolean optionRandomization) {
		this.optionRandomization = optionRandomization;
	}

	public String getAssessmentName() {
		return assessmentName;
	}

	public void setAssessmentName(String assessmentName) {
		this.assessmentName = Utility.trim(assessmentName);
	}

	public String getAssessmentDescription() {
		return assessmentDescription;
	}

	public void setAssessmentDescription(String assessmentDescription) {
		this.assessmentDescription = Utility.trim(assessmentDescription);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}


	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public Integer getAssessmentType() {
		return assessmentType;
	}

	public void setAssessmentType(Integer assessmentType) {
		this.assessmentType = assessmentType;
	}

	public Integer getJobOrderType() {
		return jobOrderType;
	}

	public void setJobOrderType(Integer jobOrderType) {
		this.jobOrderType = jobOrderType;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}

	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	
	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}

	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Boolean getIsResearchEPI() {
		return isResearchEPI;
	}

	public void setIsResearchEPI(Boolean isResearchEPI) {
		this.isResearchEPI = isResearchEPI;
	}

	public List<AssessmentJobRelation> getAssessmentJobRelations() {
		return assessmentJobRelations;
	}

	public void setAssessmentJobRelations(
			List<AssessmentJobRelation> assessmentJobRelations) {
		this.assessmentJobRelations = assessmentJobRelations;
	}

	public Integer getNoOfExperimentQuestions() {
		return noOfExperimentQuestions;
	}
	
	public void setNoOfExperimentQuestions(Integer noOfExperimentQuestions) {
		this.noOfExperimentQuestions = noOfExperimentQuestions;
	}
	
	public ScoreLookupMaster getScoreLookupMaster() {
		return scoreLookupMaster;
	}
	public void setScoreLookupMaster(ScoreLookupMaster scoreLookupMaster) {
		this.scoreLookupMaster = scoreLookupMaster;
	}
	
	public void setAssessmentGroupDetails(
			AssessmentGroupDetails assessmentGroupDetails) {
		this.assessmentGroupDetails = assessmentGroupDetails;
	}
	public AssessmentGroupDetails getAssessmentGroupDetails() {
		return assessmentGroupDetails;
	}

	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}

	public String getSplashInstruction1() {
		return splashInstruction1;
	}

	public void setSplashInstruction1(String splashInstruction1) {
		this.splashInstruction1 = splashInstruction1;
	}

	public String getSplashInstruction2() {
		return splashInstruction2;
	}

	public void setSplashInstruction2(String splashInstruction2) {
		this.splashInstruction2 = splashInstruction2;
	}

	public Integer getPdpreporttype() {
		return pdpreporttype;
	}

	public void setPdpreporttype(Integer pdpreporttype) {
		this.pdpreporttype = pdpreporttype;
	}
	
	
}
