package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="templatewisestepmessage")
public class TemplateWiseStepMessage implements Serializable {

	private static final long serialVersionUID = -5433235163802022554L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer templateWiseStepMessageId;
	@ManyToOne
	@JoinColumn(name="assessmentTemplateId",referencedColumnName="assessmentTemplateId")
	private AssessmentTemplateMaster assessmentTemplateMaster;
	@ManyToOne
	@JoinColumn(name="assessmentStepId",referencedColumnName="assessmentStepId")
	private AssessmentStepMaster assessmentStepMaster;
	private String stepMessage;
	private String status;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster1;
	private Date createdDateTime;
	@ManyToOne
	@JoinColumn(name="updatedBy",referencedColumnName="userId")
	private UserMaster userMaster2;
	private Date updatedDateTime;
	private String ipaddress;
	
	public Integer getTemplateWiseStepMessageId() {
		return templateWiseStepMessageId;
	}
	public void setTemplateWiseStepMessageId(Integer templateWiseStepMessageId) {
		this.templateWiseStepMessageId = templateWiseStepMessageId;
	}
	public AssessmentTemplateMaster getAssessmentTemplateMaster() {
		return assessmentTemplateMaster;
	}
	public void setAssessmentTemplateMaster(
			AssessmentTemplateMaster assessmentTemplateMaster) {
		this.assessmentTemplateMaster = assessmentTemplateMaster;
	}
	public AssessmentStepMaster getAssessmentStepMaster() {
		return assessmentStepMaster;
	}
	public void setAssessmentStepMaster(AssessmentStepMaster assessmentStepMaster) {
		this.assessmentStepMaster = assessmentStepMaster;
	}
	public String getStepMessage() {
		return stepMessage;
	}
	public void setStepMessage(String stepMessage) {
		this.stepMessage = stepMessage;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public UserMaster getUserMaster1() {
		return userMaster1;
	}
	public void setUserMaster1(UserMaster userMaster) {
		this.userMaster1 = userMaster;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public UserMaster getUserMaster2() {
		return userMaster2;
	}
	public void setUserMaster2(UserMaster userMaster) {
		this.userMaster2 = userMaster;
	}
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
}
