package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="assessmenttemplatemaster")
public class AssessmentTemplateMaster implements Serializable {

	private static final long serialVersionUID = -4421139308213462822L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer assessmentTemplateId;
	private String templateName;
	private Integer assessmentType;
	private Boolean isDefault;
	private String status;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster1;
	private Date createdDateTime;
	@ManyToOne
	@JoinColumn(name="updatedBy",referencedColumnName="userId")
	private UserMaster userMaster2;
	private Date updatedDateTime;
	private String ipaddress;
	
	public Integer getAssessmentTemplateId() {
		return assessmentTemplateId;
	}
	public void setAssessmentTemplateId(Integer assessmentTemplateId) {
		this.assessmentTemplateId = assessmentTemplateId;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public Integer getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(Integer assessmentType) {
		this.assessmentType = assessmentType;
	}
	public Boolean getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public UserMaster getUserMaster1() {
		return userMaster1;
	}
	public void setUserMaster1(UserMaster userMaster) {
		this.userMaster1 = userMaster;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public UserMaster getUserMaster2() {
		return userMaster2;
	}
	public void setUserMaster2(UserMaster userMaster) {
		this.userMaster2 = userMaster;
	}
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
}
