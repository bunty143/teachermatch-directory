package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name = "assessmentgroupdetails")
public class AssessmentGroupDetails implements Serializable {
	

	private static final long serialVersionUID = 97800120523391719L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "assessmentGroupId")
	private int assessmentGroupId;

	@Column(name = "assessmentGroupName")
	private String assessmentGroupName;
	private String groupShortName;
	@Column(name = "assessmentType")
	private int assessmentType;

	@Column(name = "status")
	private String status;

	@Column(name = "createdDateTime")
	private Date createdDateTime;

	@Column(name = "ipaddress")
	private String ipaddress;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "createdBy", referencedColumnName = "userId")
	private UserMaster userMaster;

	private Boolean isDefault;
	
	public int getAssessmentGroupId() {
		return assessmentGroupId;
	}
	public void setAssessmentGroupId(int assessmentGroupId) {
		this.assessmentGroupId = assessmentGroupId;
	}
	public String getAssessmentGroupName() {
		return assessmentGroupName;
	}
	public void setAssessmentGroupName(String assessmentGroupName) {
		this.assessmentGroupName = assessmentGroupName;
	}
	public int getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(int assessmentType) {
		this.assessmentType = assessmentType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public Boolean getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}
	public String getGroupShortName() {
		return groupShortName;
	}
	public void setGroupShortName(String groupShortName) {
		this.groupShortName = groupShortName;
	}
}
