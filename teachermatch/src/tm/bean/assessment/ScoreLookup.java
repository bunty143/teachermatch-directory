package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="scorelookup")
public class ScoreLookup implements Serializable 
{

	private static final long serialVersionUID = -807447633792001427L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer scorelookupId;
	@ManyToOne
	@JoinColumn(name="lookupId",referencedColumnName="lookupId")
	private ScoreLookupMaster scoreLookupMaster;
	@ManyToOne
	@JoinColumn(name="domainId",referencedColumnName="domainId")
	private DomainMaster domainMaster;
	@ManyToOne
	@JoinColumn(name="competencyId",referencedColumnName="competencyId")
	private CompetencyMaster competencyMaster;
	private Double totalscore;
	private Double percentile;
	private Double zscore;
	private Double tscore;
	private Double normscore;
	private Double ncevalue;
	private Double ritvalue;
	private Double ritse;
	private String pass;
	private Date createdDateTime;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	public Integer getScorelookupId() {
		return scorelookupId;
	}
	public void setScorelookupId(Integer scorelookupId) {
		this.scorelookupId = scorelookupId;
	}
	public ScoreLookupMaster getScoreLookupMaster() {
		return scoreLookupMaster;
	}
	public void setScoreLookupMaster(ScoreLookupMaster scoreLookupMaster) {
		this.scoreLookupMaster = scoreLookupMaster;
	}
	public DomainMaster getDomainMaster() {
		return domainMaster;
	}
	public void setDomainMaster(DomainMaster domainMaster) {
		this.domainMaster = domainMaster;
	}
	public CompetencyMaster getCompetencyMaster() {
		return competencyMaster;
	}
	public void setCompetencyMaster(CompetencyMaster competencyMaster) {
		this.competencyMaster = competencyMaster;
	}
	public Double getTotalscore() {
		return totalscore;
	}
	public void setTotalscore(Double totalscore) {
		this.totalscore = totalscore;
	}
	public Double getPercentile() {
		return percentile;
	}
	public void setPercentile(Double percentile) {
		this.percentile = percentile;
	}
	public Double getZscore() {
		return zscore;
	}
	public void setZscore(Double zscore) {
		this.zscore = zscore;
	}
	public Double getTscore() {
		return tscore;
	}
	public void setTscore(Double tscore) {
		this.tscore = tscore;
	}
	public Double getNormscore() {
		return normscore;
	}
	public void setNormscore(Double normscore) {
		this.normscore = normscore;
	}
	public Double getRitvalue() {
		return ritvalue;
	}
	public void setRitvalue(Double ritvalue) {
		this.ritvalue = ritvalue;
	}
	public Double getRitse() {
		return ritse;
	}
	public void setRitse(Double ritse) {
		this.ritse = ritse;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public Double getNcevalue() {
		return ncevalue;
	}
	public void setNcevalue(Double ncevalue) {
		this.ncevalue = ncevalue;
	}
}
