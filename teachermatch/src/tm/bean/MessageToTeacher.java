package tm.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import tm.bean.user.UserMaster;

@Entity
@Table(name="messagetoteacher")
public class MessageToTeacher implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5765720805369185661L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer messageId;  
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherId; 
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobId;
	
	@ManyToOne
	@JoinColumn(name="senderId",referencedColumnName="userId")
	private UserMaster senderId; 
	private String teacherEmailAddress;     
	private String messageSubject; 
	private String messageSend; 
	private String messageSource;
	private String senderEmailAddress; 
	private Integer entityType; 
	private Date createdDateTime;     
	private String ipAddress;
	private String messageFileName;
	private String document;
	
	private Integer noOfReminderSent;
	private String portfolioReminder;
	
	private String importType;
	
	private Boolean peaplesoft;
	
	private String teacherEmailFlag;
	
	
	public String getTeacherEmailFlag() {
		return teacherEmailFlag;
	}
	public void setTeacherEmailFlag(String teacherEmailFlag) {
		this.teacherEmailFlag = teacherEmailFlag;
	}
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	public String getMessageSource() {
		return messageSource;
	}
	public void setMessageSource(String messageSource) {
		this.messageSource = messageSource;
	}
	public String getMessageFileName() {
		return messageFileName;
	}
	public void setMessageFileName(String messageFileName) {
		this.messageFileName = messageFileName;
	}
	public TeacherDetail getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}
	public JobOrder getJobId() {
		return jobId;
	}
	public void setJobId(JobOrder jobId) {
		this.jobId = jobId;
	}
	public UserMaster getSenderId() {
		return senderId;
	}
	public void setSenderId(UserMaster senderId) {
		this.senderId = senderId;
	}
	public String getMessageSend() {
		return messageSend;
	}
	public void setMessageSend(String messageSend) {
		this.messageSend = messageSend;
	}
	public Integer getMessageId() {
		return messageId;
	}
	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}
	
	public String getTeacherEmailAddress() {
		return teacherEmailAddress;
	}
	public void setTeacherEmailAddress(String teacherEmailAddress) {
		this.teacherEmailAddress = teacherEmailAddress;
	}
	public String getMessageSubject() {
		return messageSubject;
	}
	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}
	
	public String getSenderEmailAddress() {
		return senderEmailAddress;
	}
	public void setSenderEmailAddress(String senderEmailAddress) {
		this.senderEmailAddress = senderEmailAddress;
	}
	public Integer getEntityType() {
		return entityType;
	}
	public void setEntityType(Integer entityType) {
		this.entityType = entityType;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Integer getNoOfReminderSent() {
		return noOfReminderSent;
	}
	public void setNoOfReminderSent(Integer noOfReminderSent) {
		this.noOfReminderSent = noOfReminderSent;
	}
	public String getPortfolioReminder() {
		return portfolioReminder;
	}
	public void setPortfolioReminder(String portfolioReminder) {
		this.portfolioReminder = portfolioReminder;
	}
	public String getImportType() {
		return importType;
	}
	public void setImportType(String importType) {
		this.importType = importType;
	}
	public Boolean getPeaplesoft() {
		return peaplesoft;
	}
	public void setPeaplesoft(Boolean peaplesoft) {
		this.peaplesoft = peaplesoft;
	}
	
	
}
