package tm.bean;

import java.io.Serializable;

import tm.bean.user.UserMaster;
import tm.services.EmailerService;


public class InternalMailSend implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -267756240198542689L;
	private TeacherDetail teacherDetail;
	//private String jobTitle;
	private JobOrder jobOrder;
	private String loginURL;
	private String []arrHrDetail;
	private EmailerService emailerService;
	
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	/*public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}*/
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public String getLoginURL() {
		return loginURL;
	}
	public void setLoginURL(String loginURL) {
		this.loginURL = loginURL;
	}
	public String[] getArrHrDetail() {
		return arrHrDetail;
	}
	public void setArrHrDetail(String[] arrHrDetail) {
		this.arrHrDetail = arrHrDetail;
	}
	
	public void setEmailerService(EmailerService emailerService) {
		this.emailerService = emailerService;
	}
	
	public EmailerService getEmailerService() {
		return emailerService;
	}
     
	
		
}
