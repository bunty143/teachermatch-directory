package tm.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="jobshare")
public class JobShare implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6424109259713292310L;
	@Id	
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String senderName;
	private String senderEmail;
	private String recipientName;
	private String recipientEmail;
	private Boolean isUserLogin;						
	private String ipAddress;
	private Date createdDatetime;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getSenderEmail() {
		return senderEmail;
	}
	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}
	public String getRecipientName() {
		return recipientName;
	}
	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}
	public String getRecipientEmail() {
		return recipientEmail;
	}
	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;
	}
	public Boolean getIsUserLogin() {
		return isUserLogin;
	}
	public void setIsUserLogin(Boolean isUserLogin) {
		this.isUserLogin = isUserLogin;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}	
}
