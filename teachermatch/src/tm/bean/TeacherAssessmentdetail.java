package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.ScoreLookupMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacherassessmentdetails")
public class TeacherAssessmentdetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4433613700804975867L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherAssessmentId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentDetail;
	private String assessmentName; 
	private String assessmentDescription; 
	private Integer assessmentType; 
	private Integer assosiatedJobOrderType;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster; 
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	private Integer questionSessionTime; 
	private Integer assessmentSessionTime;
	private Boolean questionRandomization; 
	private Boolean optionRandomization; 
	private Integer maxQuesToDisplay;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	private String status; 
	private Boolean isDone;
	private Date createdDateTime; 
	private String ipaddress;
	private Integer assessmentTakenCount;
	
	@ManyToOne
	@JoinColumn(name="lookupId",referencedColumnName="lookupId")
	private ScoreLookupMaster scoreLookupMaster;
	
	public Integer getTeacherAssessmentId() {
		return teacherAssessmentId;
	}
	public void setTeacherAssessmentId(Integer teacherAssessmentId) {
		this.teacherAssessmentId = teacherAssessmentId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public AssessmentDetail getAssessmentDetail() {
		return assessmentDetail;
	}
	public void setAssessmentDetail(AssessmentDetail assessmentDetail) {
		this.assessmentDetail = assessmentDetail;
	}
	public String getAssessmentName() {
		return assessmentName;
	}
	public void setAssessmentName(String assessmentName) {
		this.assessmentName = assessmentName;
	}
	public String getAssessmentDescription() {
		return assessmentDescription;
	}
	public void setAssessmentDescription(String assessmentDescription) {
		this.assessmentDescription = assessmentDescription;
	}
	public Integer getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(Integer assessmentType) {
		this.assessmentType = assessmentType;
	}
	public Integer getAssosiatedJobOrderType() {
		return assosiatedJobOrderType;
	}
	public void setAssosiatedJobOrderType(Integer assosiatedJobOrderType) {
		this.assosiatedJobOrderType = assosiatedJobOrderType;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public Integer getQuestionSessionTime() {
		return questionSessionTime;
	}
	public void setQuestionSessionTime(Integer questionSessionTime) {
		this.questionSessionTime = questionSessionTime;
	}
	public Integer getAssessmentSessionTime() {
		return assessmentSessionTime;
	}
	public void setAssessmentSessionTime(Integer assessmentSessionTime) {
		this.assessmentSessionTime = assessmentSessionTime;
	}
	public Boolean getQuestionRandomization() {
		return questionRandomization;
	}
	public void setQuestionRandomization(Boolean questionRandomization) {
		this.questionRandomization = questionRandomization;
	}
	public Boolean getOptionRandomization() {
		return optionRandomization;
	}
	public void setOptionRandomization(Boolean optionRandomization) {
		this.optionRandomization = optionRandomization;
	}
	public Integer getMaxQuesToDisplay() {
		return maxQuesToDisplay;
	}
	public void setMaxQuesToDisplay(Integer maxQuesToDisplay) {
		this.maxQuesToDisplay = maxQuesToDisplay;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getIsDone() {
		return isDone;
	}
	public void setIsDone(Boolean isDone) {
		this.isDone = isDone;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	} 
	public ScoreLookupMaster getScoreLookupMaster() {
		return scoreLookupMaster;
	}
	public void setScoreLookupMaster(ScoreLookupMaster scoreLookupMaster) {
		this.scoreLookupMaster = scoreLookupMaster;
	}
	public Integer getAssessmentTakenCount() {
		return assessmentTakenCount;
	}
	public void setAssessmentTakenCount(Integer assessmentTakenCount) {
		this.assessmentTakenCount = assessmentTakenCount;
	}
	
}
