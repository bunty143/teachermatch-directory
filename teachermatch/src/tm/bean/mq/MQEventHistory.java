package tm.bean.mq;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;

	@Entity
	@Table(name="mqeventhistory")
	public class MQEventHistory implements Serializable{

		private static final long serialVersionUID = 8665030249384692431L;

		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Integer eventHistoryId;
		
		@ManyToOne
		@JoinColumn(name="eventId",referencedColumnName="eventId")
		private MQEvent mqEvent;
		
		private String xmlObject;
		private Date createdDateTime;
		@Column(name="updatedDateTime")
		private Date updateDateTime;
		private Integer createdBy;
		private String ipAddress;
		public Integer getEventHistoryId() {
			return eventHistoryId;
		}
		public void setEventHistoryId(Integer eventHistoryId) {
			this.eventHistoryId = eventHistoryId;
		}
		public MQEvent getMqEvent() {
			return mqEvent;
		}
		public void setMqEvent(MQEvent mqEvent) {
			this.mqEvent = mqEvent;
		}
		public String getXmlObject() {
			return xmlObject;
		}
		public void setXmlObject(String xmlObject) {
			this.xmlObject = xmlObject;
		}
		public Date getCreatedDateTime() {
			return createdDateTime;
		}
		public void setCreatedDateTime(Date createdDateTime) {
			this.createdDateTime = createdDateTime;
		}
		public Date getUpdateDateTime() {
			return updateDateTime;
		}
		public void setUpdateDateTime(Date updateDateTime) {
			this.updateDateTime = updateDateTime;
		}
		public Integer getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(Integer createdBy) {
			this.createdBy = createdBy;
		}
		public String getIpAddress() {
			return ipAddress;
		}
		public void setIpAddress(String ipAddress) {
			this.ipAddress = ipAddress;
		}
		
		
		
	
	
}
