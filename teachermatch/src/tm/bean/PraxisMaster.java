package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import tm.bean.master.StateMaster;

@Entity(name="praxismaster")
public class PraxisMaster implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8941891489504993576L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer testKeyId;
	private String stateCode;
	@ManyToOne
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private StateMaster stateMaster;
	private String praxisIReading;
	private Integer readingQualifyingScore; 
	private String praxisIWriting;
	private Integer writingQualifyingScore;
	private String praxisIMathematics;
	private Integer mathematicsQualifyingScore;
	
	public Integer getTestKeyId() {
		return testKeyId;
	}
	public void setTestKeyId(Integer testKeyId) {
		this.testKeyId = testKeyId;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public StateMaster getStateMaster() {
		return stateMaster;
	}
	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}
	public String getPraxisIReading() {
		return praxisIReading;
	}
	public void setPraxisIReading(String praxisIReading) {
		this.praxisIReading = praxisIReading;
	}
	public Integer getReadingQualifyingScore() {
		return readingQualifyingScore;
	}
	public void setReadingQualifyingScore(Integer readingQualifyingScore) {
		this.readingQualifyingScore = readingQualifyingScore;
	}
	public String getPraxisIWriting() {
		return praxisIWriting;
	}
	public void setPraxisIWriting(String praxisIWriting) {
		this.praxisIWriting = praxisIWriting;
	}
	public Integer getWritingQualifyingScore() {
		return writingQualifyingScore;
	}
	public void setWritingQualifyingScore(Integer writingQualifyingScore) {
		this.writingQualifyingScore = writingQualifyingScore;
	}
	public String getPraxisIMathematics() {
		return praxisIMathematics;
	}
	public void setPraxisIMathematics(String praxisIMathematics) {
		this.praxisIMathematics = praxisIMathematics;
	}
	public Integer getMathematicsQualifyingScore() {
		return mathematicsQualifyingScore;
	}
	public void setMathematicsQualifyingScore(Integer mathematicsQualifyingScore) {
		this.mathematicsQualifyingScore = mathematicsQualifyingScore;
	}

	

}
