package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.CurrencyMaster;
import tm.bean.master.EmpRoleTypeMaster;
import tm.bean.master.FieldMaster;
import tm.bean.master.FieldOfStudyMaster;
import tm.utility.Utility;

@Entity
@Table(name="teacherloginhistory")
public class TeacherLoginHistory implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7219351412827206316L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;     
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;	
	private String sessionId	;
	private Date loginTime;
	private Date logOutTime;
	private String timeSpent;     
	private String ipAddress;
	private String serverName;
	private String loginPasswordType;
	private String logType;
	
	
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	public String getLoginPasswordType() {
		return loginPasswordType;
	}
	public void setLoginPasswordType(String loginPasswordType) {
		this.loginPasswordType = loginPasswordType;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Date getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
	public Date getLogOutTime() {
		return logOutTime;
	}
	public void setLogOutTime(Date logOutTime) {
		this.logOutTime = logOutTime;
	}
	public String getTimeSpent() {
		return timeSpent;
	}
	public void setTimeSpent(String timeSpent) {
		this.timeSpent = timeSpent;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	/**
	 * @param logType the logType to set
	 */
	public void setLogType(String logType) {
		this.logType = logType;
	}
	/**
	 * @return the logType
	 */
	public String getLogType() {
		return logType;
	}	
	
}
