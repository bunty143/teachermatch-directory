package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="referencenotes")
public class ReferenceNote implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1836940185954533745L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer refNoteId;
	
	@ManyToOne
	@JoinColumn(name="elerefAutoId", referencedColumnName="elerefAutoId")
	private TeacherElectronicReferences electronicReferences; 
	
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobId;  
	
	private String refNoteDescription;
	private String refNoteAttachment;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster  schoolId;
	
	@ManyToOne
	@JoinColumn(name="noteCreatedBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private Date noteCreatedDate;

	public Integer getRefNoteId() {
		return refNoteId;
	}

	public void setRefNoteId(Integer refNoteId) {
		this.refNoteId = refNoteId;
	}

	public JobOrder getJobId() {
		return jobId;
	}

	public void setJobId(JobOrder jobId) {
		this.jobId = jobId;
	}

	public String getRefNoteDescription() {
		return refNoteDescription;
	}

	public void setRefNoteDescription(String refNoteDescription) {
		this.refNoteDescription = refNoteDescription;
	}

	public String getRefNoteAttachment() {
		return refNoteAttachment;
	}

	public void setRefNoteAttachment(String refNoteAttachment) {
		this.refNoteAttachment = refNoteAttachment;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public SchoolMaster getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(SchoolMaster schoolId) {
		this.schoolId = schoolId;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Date getNoteCreatedDate() {
		return noteCreatedDate;
	}

	public void setNoteCreatedDate(Date noteCreatedDate) {
		this.noteCreatedDate = noteCreatedDate;
	}

	public TeacherElectronicReferences getElectronicReferences() {
		return electronicReferences;
	}

	public void setElectronicReferences(
			TeacherElectronicReferences electronicReferences) {
		this.electronicReferences = electronicReferences;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	
	
	
	
		
}
