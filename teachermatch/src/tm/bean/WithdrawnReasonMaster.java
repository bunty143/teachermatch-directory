package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.EthinicityMaster;
import tm.bean.master.EthnicOriginMaster;
import tm.bean.master.GenderMaster;
import tm.bean.user.UserMaster;


@Entity
@Table(name="withdrawnreasonmaster")
public class WithdrawnReasonMaster implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1463494735207252604L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer withdrawnreasonmasterId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private String reason;
	
	private String status;
	
	private Date createdDateTime;

	public Integer getWithdrawnreasonmasterId() {
		return withdrawnreasonmasterId;
	}

	public void setWithdrawnreasonmasterId(Integer withdrawnreasonmasterId) {
		this.withdrawnreasonmasterId = withdrawnreasonmasterId;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
}
