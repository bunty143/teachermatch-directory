package tm.bean.onboarding;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="checkpointpermissionmaster")
public class CheckPointPermissionMaster {
	private static final long serialVersionUID = 6631718743665590819L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer cpPermissionId;
	String cpPermissionName;
	Integer cpReadonly;
	Integer cpReadWrite;
	Integer cpNoPermission;
	Date createdDate;
	String status;
	public Integer getCpPermissionId() {
		return cpPermissionId;
	}
	public void setCpPermissionId(Integer cpPermissionId) {
		this.cpPermissionId = cpPermissionId;
	}
	public String getCpPermissionName() {
		return cpPermissionName;
	}
	public void setCpPermissionName(String cpPermissionName) {
		this.cpPermissionName = cpPermissionName;
	}
	public Integer getCpReadonly() {
		return cpReadonly;
	}
	public void setCpReadonly(Integer cpReadonly) {
		this.cpReadonly = cpReadonly;
	}
	public Integer getCpReadWrite() {
		return cpReadWrite;
	}
	public void setCpReadWrite(Integer cpReadWrite) {
		this.cpReadWrite = cpReadWrite;
	}
	public Integer getCpNoPermission() {
		return cpNoPermission;
	}
	public void setCpNoPermission(Integer cpNoPermission) {
		this.cpNoPermission = cpNoPermission;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
