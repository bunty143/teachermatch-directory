package tm.bean.onboarding;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="onboardingcheckpointquestion")
public class OnboardCheckPointQuestion {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer checkPointQuestionId;
	private Integer onBoardingId;
	private Integer districtId;
	private Integer checkPointId;
	private Integer checkPointTypeId;
	private String question;
	private String documentFileName;
	public String getDocumentFileName() {
		return documentFileName;
	}
	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}
	private String candidateType;
	private Date createdDate;
	private Integer createdBy;
	private Date modifiedDate;
	private Integer modifiedBy;
	private String ipAddress;
	public Integer getCheckPointQuestionId() {
		return checkPointQuestionId;
	}
	public void setCheckPointQuestionId(Integer checkPointQuestionId) {
		this.checkPointQuestionId = checkPointQuestionId;
	}
	public Integer getOnBoardingId() {
		return onBoardingId;
	}
	public void setOnBoardingId(Integer onBoardingId) {
		this.onBoardingId = onBoardingId;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public Integer getCheckPointId() {
		return checkPointId;
	}
	public void setCheckPointId(Integer checkPointId) {
		this.checkPointId = checkPointId;
	}
	public Integer getCheckPointTypeId() {
		return checkPointTypeId;
	}
	public void setCheckPointTypeId(Integer checkPointTypeId) {
		this.checkPointTypeId = checkPointTypeId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	
	public String getCandidateType() {
		return candidateType;
	}
	public void setCandidateType(String candidateType) {
		this.candidateType = candidateType;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	private char status;
	
	

}
