package tm.bean.onboarding;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="vwonboardingdashboard")
public class EditOnboardDashboard {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Integer checkPointTransactionId;
	Integer checkPointMasterId;
	Integer onBoardingId;
	Integer districtId;
	String checkPointName;
	Integer conmmunicationRequired;
	String actionPerformedBy;
	String checkPointStatus;
	String createdByUser;
	Date createdDate;
	String modifiedByUser;
	Date modifiedDate;
	String ipAddress;
	Integer actionNeeded;
	String status;
	Integer checkPointQuestionId;
	Integer checkPointTypeId;
	Integer checkPointId;
	String candidateType;
	String question;
	String documentFileName;
	Integer checkPointOptionsId;
	String validOption;
	String options;
	public Integer getCheckPointTransactionId() {
		return checkPointTransactionId;
	}
	public void setCheckPointTransactionId(Integer checkPointTransactionId) {
		this.checkPointTransactionId = checkPointTransactionId;
	}
	public Integer getCheckPointMasterId() {
		return checkPointMasterId;
	}
	public void setCheckPointMasterId(Integer checkPointMasterId) {
		this.checkPointMasterId = checkPointMasterId;
	}
	public Integer getOnBoardingId() {
		return onBoardingId;
	}
	public void setOnBoardingId(Integer onBoardingId) {
		this.onBoardingId = onBoardingId;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public String getCheckPointName() {
		return checkPointName;
	}
	public void setCheckPointName(String checkPointName) {
		this.checkPointName = checkPointName;
	}
	public Integer getConmmunicationRequired() {
		return conmmunicationRequired;
	}
	public void setConmmunicationRequired(Integer conmmunicationRequired) {
		this.conmmunicationRequired = conmmunicationRequired;
	}
	public String getActionPerformedBy() {
		return actionPerformedBy;
	}
	public void setActionPerformedBy(String actionPerformedBy) {
		this.actionPerformedBy = actionPerformedBy;
	}
	public String getCheckPointStatus() {
		return checkPointStatus;
	}
	public void setCheckPointStatus(String checkPointStatus) {
		this.checkPointStatus = checkPointStatus;
	}
	public String getCreatedByUser() {
		return createdByUser;
	}
	public void setCreatedByUser(String createdByUser) {
		this.createdByUser = createdByUser;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedByUser() {
		return modifiedByUser;
	}
	public void setModifiedByUser(String modifiedByUser) {
		this.modifiedByUser = modifiedByUser;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Integer getActionNeeded() {
		return actionNeeded;
	}
	public void setActionNeeded(Integer actionNeeded) {
		this.actionNeeded = actionNeeded;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getCheckPointQuestionId() {
		return checkPointQuestionId;
	}
	public void setCheckPointQuestionId(Integer checkPointQuestionId) {
		this.checkPointQuestionId = checkPointQuestionId;
	}
	public Integer getCheckPointId() {
		return checkPointId;
	}
	public void setCheckPointId(Integer checkPointId) {
		this.checkPointId = checkPointId;
	}
	public String getCandidateType() {
		return candidateType;
	}
	public void setCandidateType(String candidateType) {
		this.candidateType = candidateType;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getDocumentFileName() {
		return documentFileName;
	}
	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}
	public Integer getCheckPointOptionsId() {
		return checkPointOptionsId;
	}
	public void setCheckPointOptionsId(Integer checkPointOptionsId) {
		this.checkPointOptionsId = checkPointOptionsId;
	}
	public String getValidOption() {
		return validOption;
	}
	public void setValidOption(String validOption) {
		this.validOption = validOption;
	}
	public String getOptions() {
		return options;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	public Integer getCheckPointTypeId() {
		return checkPointTypeId;
	}
	public void setCheckPointTypeId(Integer checkPointTypeId) {
		this.checkPointTypeId = checkPointTypeId;
	}
	
	
	

}
