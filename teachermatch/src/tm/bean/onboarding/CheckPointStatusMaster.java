package tm.bean.onboarding;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="checkpointstatusmaster")
public class CheckPointStatusMaster {
	private static final long serialVersionUID = 6631718743665590819L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer checkPointStatusId; 
	String checkPointStatusName;
	String decisionPoint;
	String color;
	String status;
	Date createdDate;
	public Integer getCheckPointStatusId() {
		return checkPointStatusId;
	}
	public void setCheckPointStatusId(Integer checkPointStatusId) {
		this.checkPointStatusId = checkPointStatusId;
	}
	public String getCheckPointStatusName() {
		return checkPointStatusName;
	}
	public void setCheckPointStatusName(String checkPointStatusName) {
		this.checkPointStatusName = checkPointStatusName;
	}
	public String getDecisionPoint() {
		return decisionPoint;
	}
	public void setDecisionPoint(String decisionPoint) {
		this.decisionPoint = decisionPoint;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
