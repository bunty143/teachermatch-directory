package tm.bean.onboarding;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="onboardingcheckpointoptions")
public class OnBoardingCheckPointOptions {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer checkPointOptionsId;
	
	public Integer getCheckPointOptionsId() {
		return checkPointOptionsId;
	}
	public void setCheckPointOptionsId(Integer checkPointOptionsId) {
		this.checkPointOptionsId = checkPointOptionsId;
	}
	private Integer checkPointQuestionId;
	
	private String validOption;
	private String options;
	private Date createdDate;
	private Integer createdBy;
	private Date modifiedDate;
	private Integer modifiedBy;
	public Integer getCheckPointQuestionId() {
		return checkPointQuestionId;
	}
	public void setCheckPointQuestionId(Integer checkPointQuestionId) {
		this.checkPointQuestionId = checkPointQuestionId;
	}
	public String getValidOption() {
		return validOption;
	}
	public void setValidOption(String validOption) {
		this.validOption = validOption;
	}
	public String getOptions() {
		return options;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	private String ipAddress;
	private char status;
	
	

}
