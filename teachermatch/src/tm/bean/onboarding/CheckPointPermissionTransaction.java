package tm.bean.onboarding;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="checkpointpermissiontransaction")
public class CheckPointPermissionTransaction {
	private static final long serialVersionUID = 6631718743665590819L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer permissionTransactionId;
	Integer permissionMasterId;
	Integer onBoardingId;
	Integer checkPointId;
	Integer districtId;
	String candidateType;
	String permissionName;
	Integer readOnly;
	Integer readWrite;
	Integer noPermission;
	Date createdDate;
	String createdByUser;
	String modifiedByUser;
	Date modifiedDate;
	String ipAddress;
	String status;
	public Integer getPermissionTransactionId() {
		return permissionTransactionId;
	}
	public void setPermissionTransactionId(Integer permissionTransactionId) {
		this.permissionTransactionId = permissionTransactionId;
	}
	public Integer getPermissionMasterId() {
		return permissionMasterId;
	}
	public void setPermissionMasterId(Integer permissionMasterId) {
		this.permissionMasterId = permissionMasterId;
	}
	public Integer getOnBoardingId() {
		return onBoardingId;
	}
	public void setOnBoardingId(Integer onBoardingId) {
		this.onBoardingId = onBoardingId;
	}
	public Integer getCheckPointId() {
		return checkPointId;
	}
	public void setCheckPointId(Integer checkPointId) {
		this.checkPointId = checkPointId;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public String getCandidateType() {
		return candidateType;
	}
	public void setCandidateType(String candidateType) {
		this.candidateType = candidateType;
	}
	public String getPermissionName() {
		return permissionName;
	}
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}
	public Integer getReadOnly() {
		return readOnly;
	}
	public void setReadOnly(Integer readOnly) {
		this.readOnly = readOnly;
	}
	public Integer getReadWrite() {
		return readWrite;
	}
	public void setReadWrite(Integer readWrite) {
		this.readWrite = readWrite;
	}
	public Integer getNoPermission() {
		return noPermission;
	}
	public void setNoPermission(Integer noPermission) {
		this.noPermission = noPermission;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedByUser() {
		return createdByUser;
	}
	public void setCreatedByUser(String createdByUser) {
		this.createdByUser = createdByUser;
	}
	public String getModifiedByUser() {
		return modifiedByUser;
	}
	public void setModifiedByUser(String modifiedByUser) {
		this.modifiedByUser = modifiedByUser;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
