package tm.bean.onboarding;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="checkpointtransaction")
public class CheckPointTransaction {
	private static final long serialVersionUID = 6631718743665590819L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer checkPointTransactionId;
	Integer checkPointMasterId;
	Integer onBoardingId;
	Integer districtId;
	String checkPointName;
	Integer actionNeeded;
	String checkPointStatus;
	Integer createdByUser;
	Date createdDate;
	Integer modifiedByUser;
	Date modifiedDate;
	String ipAddress;
	String status;
	public Integer getCheckPointTransactionId() {
		return checkPointTransactionId;
	}
	public void setCheckPointTransactionId(Integer checkPointTransactionId) {
		this.checkPointTransactionId = checkPointTransactionId;
	}
	public Integer getCheckPointMasterId() {
		return checkPointMasterId;
	}
	public void setCheckPointMasterId(Integer checkPointMasterId) {
		this.checkPointMasterId = checkPointMasterId;
	}
	public Integer getOnBoardingId() {
		return onBoardingId;
	}
	public void setOnBoardingId(Integer onBoardingId) {
		this.onBoardingId = onBoardingId;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public String getCheckPointName() {
		return checkPointName;
	}
	public void setCheckPointName(String checkPointName) {
		this.checkPointName = checkPointName;
	}
	public Integer getActionNeeded() {
		return actionNeeded;
	}
	public void setActionNeeded(Integer actionNeeded) {
		this.actionNeeded = actionNeeded;
	}
	public String getCheckPointStatus() {
		return checkPointStatus;
	}
	public void setCheckPointStatus(String checkPointStatus) {
		this.checkPointStatus = checkPointStatus;
	}
	public Integer getCreatedByUser() {
		return createdByUser;
	}
	public void setCreatedByUser(Integer createdByUser) {
		this.createdByUser = createdByUser;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getModifiedByUser() {
		return modifiedByUser;
	}
	public void setModifiedByUser(Integer modifiedByUser) {
		this.modifiedByUser = modifiedByUser;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
