package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacherprofilevisithistory")
public class TeacherProfileVisitHistory implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8683063951577915371L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer visitId;
	
	@ManyToOne
	@JoinColumn(name="userId", referencedColumnName="userId")
	private UserMaster userMaster; 
	
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherDetail; 
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtId;     

	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolId;
	
	
	private String visitLocation;
	private Date visitedDateTime;
	private String ipAddress;
	public Integer getVisitId() {
		return visitId;
	}
	public void setVisitId(Integer visitId) {
		this.visitId = visitId;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public String getVisitLocation() {
		return visitLocation;
	}
	public void setVisitLocation(String visitLocation) {
		this.visitLocation = visitLocation;
	}
	public Date getVisitedDateTime() {
		return visitedDateTime;
	}
	public void setVisitedDateTime(Date visitedDateTime) {
		this.visitedDateTime = visitedDateTime;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public DistrictMaster getDistrictId() {
		return districtId;
	}
	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}
	public SchoolMaster getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(SchoolMaster schoolId) {
		this.schoolId = schoolId;
	}
	
	
	
	
}
