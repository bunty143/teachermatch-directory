package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacherepichangedstatuslog")
public class TeacherEPIChangedStatusLog implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -1256853671479083691L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer changedStatusId;
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userMaster;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentDetail;
	private String assessmentName;
	private Integer assessmentType; 
	@ManyToOne
	@JoinColumn(name="teacherAssessmentStatusId",referencedColumnName="teacherAssessmentStatusId")
	private TeacherAssessmentStatus teacherAssessmentStatus;
	private String resetMessage;
	private Date createdDateTime;
	private String ipAddress;
	public Integer getChangedStatusId() {
		return changedStatusId;
	}
	public void setChangedStatusId(Integer changedStatusId) {
		this.changedStatusId = changedStatusId;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public AssessmentDetail getAssessmentDetail() {
		return assessmentDetail;
	}
	public void setAssessmentDetail(AssessmentDetail assessmentDetail) {
		this.assessmentDetail = assessmentDetail;
	}
	public String getAssessmentName() {
		return assessmentName;
	}
	public void setAssessmentName(String assessmentName) {
		this.assessmentName = assessmentName;
	}	
	public Integer getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(Integer assessmentType) {
		this.assessmentType = assessmentType;
	}
	public TeacherAssessmentStatus getTeacherAssessmentStatus() {
		return teacherAssessmentStatus;
	}
	public void setTeacherAssessmentStatus(
			TeacherAssessmentStatus teacherAssessmentStatus) {
		this.teacherAssessmentStatus = teacherAssessmentStatus;
	}
	public String getResetMessage() {
		return resetMessage;
	}
	public void setResetMessage(String resetMessage) {
		this.resetMessage = resetMessage;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
}
