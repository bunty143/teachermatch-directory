package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.CityMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.EthinicityMaster;
import tm.bean.master.EthnicOriginMaster;
import tm.bean.master.GenderMaster;
import tm.bean.master.StateMaster;
import tm.utility.Utility;

@Entity
@Table(name="teacherpersonalinfo")
public class TeacherPersonalInfo implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4306781569935890215L;
	@Id
	private Integer teacherId;     
	private Integer salutation;     
	private String firstName;     
	private String middleName;
	private String lastName;
	private String anotherName;
	private String districtEmail;
	
	private String raceId;
	
	@ManyToOne
	@JoinColumn(name="genderId",referencedColumnName="genderId")
	private GenderMaster genderId;
	
 	private String addressLine1;     
	private String addressLine2;     
	private String zipCode; 	
	@ManyToOne
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private StateMaster stateId;	
	
	@ManyToOne
	@JoinColumn(name="cityId",referencedColumnName="cityId")
	private CityMaster cityId;     
	private String phoneNumber;     
	private String mobileNumber;    
	
	private String otherCity;
	private String otherState;
	
	private Boolean isDone;     
	private Date createdDateTime;
	
	private Integer expectedSalary;
	private String noLongerEmployed;
	private Date dob;
	private Date retirementdate;
	private String drivingLicState;
	private String drivingLicNum;
	private Date moneywithdrawaldate;
	private Date retirementdatePERA;
	private String SSN;
	
	private String SIN;
	public String getSIN() {
		return SIN;
	}
	public void setSIN(String sIN) {
		SIN = sIN;
	}
	private Integer employeeType;
	private String  employeeNumber;
	private String lastPositionWhenEmployed;
	
	private String location;
	private String position;
	//private Integer isRetiredEmployee;
	private Integer isCurrentFullTimeTeacher;
	private Integer isVateran;
	private String retirementCode;
	private String retirementnumber;
	
	private String presentAddressLine1;     
	private String presentAddressLine2;     
	private String presentZipCode; 	
	@ManyToOne
	@JoinColumn(name="presentStateId",referencedColumnName="stateId")
	private StateMaster presentStateId;	

	@ManyToOne
	@JoinColumn(name="presentCityId",referencedColumnName="cityId")
	private CityMaster presentCityId; 
	private String persentOtherCity;
	private String persentOtherState;
	@ManyToOne
	@JoinColumn(name="presentCountryId",referencedColumnName="countryId")
	private CountryMaster presentCountryId;
	
	@ManyToOne
	@JoinColumn(name="retiredFromState",referencedColumnName="stateId")
	private StateMaster stateMaster;	
	
	@ManyToOne
	@JoinColumn(name="retiredFromDistrict",referencedColumnName="districtId")
	private DistrictMaster districtmaster; 
	
	
	@Transient
	private String emailAddress;
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	@ManyToOne
	@JoinColumn(name="ethnicityId",referencedColumnName="ethnicityId")
	private EthinicityMaster ethnicityId;
	
	@ManyToOne
	@JoinColumn(name="ethnicOriginId",referencedColumnName="ethnicOriginId")
	private EthnicOriginMaster ethnicOriginId;
	
	@ManyToOne
	@JoinColumn(name="countryId",referencedColumnName="countryId")
	private CountryMaster countryId;
	private String backgroundCheckDocument;
	private Date bgCheckUploadedDate;
	private Integer phoneTypeUSNonUS ;
	
	
	
	
	public Integer getPhoneTypeUSNonUS() {
		return phoneTypeUSNonUS;
	}
	public void setPhoneTypeUSNonUS(Integer phoneTypeUSNonUS) {
		this.phoneTypeUSNonUS = phoneTypeUSNonUS;
	}
	public EthinicityMaster getEthnicityId() {
		return ethnicityId;
	}
	public void setEthnicityId(EthinicityMaster ethnicityId) {
		this.ethnicityId = ethnicityId;
	}
	public EthnicOriginMaster getEthnicOriginId() {
		return ethnicOriginId;
	}
	public void setEthnicOriginId(EthnicOriginMaster ethnicOriginId) {
		this.ethnicOriginId = ethnicOriginId;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	public String getRaceId() {
		return raceId;
	}
	public void setRaceId(String raceId) {
		this.raceId = raceId;
	}
	public GenderMaster getGenderId() {
		return genderId;
	}
	public void setGenderId(GenderMaster genderId) {
		this.genderId = genderId;
	}
	public Integer getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}
	public Integer getSalutation() {
		return salutation;
	}
	public void setSalutation(Integer salutation) {
		this.salutation = salutation;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = Utility.trim(firstName);
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = Utility.trim(lastName);
	}
	
	public String getAnotherName() {
		return anotherName;
	}
	public void setAnotherName(String anotherName) {
		this.anotherName = anotherName;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = Utility.trim(addressLine1);
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = Utility.trim(zipCode);
	}
	public StateMaster getStateId() {
		return stateId;
	}
	public void setStateId(StateMaster stateId) {
		this.stateId = stateId;
	}
	public CityMaster getCityId() {
		return cityId;
	}
	public void setCityId(CityMaster cityId) {
		this.cityId = cityId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Boolean getIsDone() {
		return isDone;
	}
	public void setIsDone(Boolean isDone) {
		this.isDone = isDone;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Integer getExpectedSalary() {
		return expectedSalary;
	}
	public void setExpectedSalary(Integer expectedSalary) {
		this.expectedSalary = expectedSalary;
	}
	public String getNoLongerEmployed() {
		return noLongerEmployed;
	}
	public void setNoLongerEmployed(String noLongerEmployed) {
		this.noLongerEmployed = noLongerEmployed;
	}
	public CountryMaster getCountryId() {
		return countryId;
	}
	public void setCountryId(CountryMaster countryId) {
		this.countryId = countryId;
	}
	public String getOtherCity() {
		return otherCity;
	}
	public void setOtherCity(String otherCity) {
		this.otherCity = otherCity;
	}
	public String getOtherState() {
		return otherState;
	}
	public void setOtherState(String otherState) {
		this.otherState = otherState;
	}
	public String getSSN() {
		return SSN;
	}
	public void setSSN(String sSN) {
		SSN = sSN;
	}
	public Integer getEmployeeType() {
		return employeeType;
	}
	public void setEmployeeType(Integer employeeType) {
		this.employeeType = employeeType;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getLastPositionWhenEmployed() {
		return lastPositionWhenEmployed;
	}
	public void setLastPositionWhenEmployed(String lastPositionWhenEmployed) {
		this.lastPositionWhenEmployed = lastPositionWhenEmployed;
	}
	
	public Integer getIsCurrentFullTimeTeacher() {
		return isCurrentFullTimeTeacher;
	}
	public void setIsCurrentFullTimeTeacher(Integer isCurrentFullTimeTeacher) {
		this.isCurrentFullTimeTeacher = isCurrentFullTimeTeacher;
	}
	public Integer getIsVateran() {
		return isVateran;
	}
	public void setIsVateran(Integer isVateran) {
		this.isVateran = isVateran;
	}
	public Date getRetirementdate() {
		return retirementdate;
	}
	public void setRetirementdate(Date retirementdate) {
		this.retirementdate = retirementdate;
	}
	public Date getMoneywithdrawaldate() {
		return moneywithdrawaldate;
	}
	public void setMoneywithdrawaldate(Date moneywithdrawaldate) {
		this.moneywithdrawaldate = moneywithdrawaldate;
	}
	public String getRetirementCode() {
		return retirementCode;
	}
	public void setRetirementCode(String retirementCode) {
		this.retirementCode = retirementCode;
	}
	public String getRetirementnumber() {
		return retirementnumber;
	}
	public void setRetirementnumber(String retirementnumber) {
		this.retirementnumber = retirementnumber;
	}
	public StateMaster getStateMaster() {
		return stateMaster;
	}
	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}
	public DistrictMaster getDistrictmaster() {
		return districtmaster;
	}
	public void setDistrictmaster(DistrictMaster districtmaster) {
		this.districtmaster = districtmaster;
	}
	public String getDrivingLicState() {
		return drivingLicState;
	}
	public void setDrivingLicState(String drivingLicState) {
		this.drivingLicState = drivingLicState;
	}
	public String getDrivingLicNum() {
		return drivingLicNum;
	}
	public void setDrivingLicNum(String drivingLicNum) {
		this.drivingLicNum = drivingLicNum;
	}
	public String getPresentAddressLine1() {
		return presentAddressLine1;
	}
	public void setPresentAddressLine1(String presentAddressLine1) {
		this.presentAddressLine1 = presentAddressLine1;
	}
	public String getPresentAddressLine2() {
		return presentAddressLine2;
	}
	public void setPresentAddressLine2(String presentAddressLine2) {
		this.presentAddressLine2 = presentAddressLine2;
	}
	public String getPresentZipCode() {
		return presentZipCode;
	}
	public void setPresentZipCode(String presentZipCode) {
		this.presentZipCode = presentZipCode;
	}
	public StateMaster getPresentStateId() {
		return presentStateId;
	}
	public void setPresentStateId(StateMaster presentStateId) {
		this.presentStateId = presentStateId;
	}
	public CityMaster getPresentCityId() {
		return presentCityId;
	}
	public void setPresentCityId(CityMaster presentCityId) {
		this.presentCityId = presentCityId;
	}
	public String getPersentOtherCity() {
		return persentOtherCity;
	}
	public void setPersentOtherCity(String persentOtherCity) {
		this.persentOtherCity = persentOtherCity;
	}
	public String getPersentOtherState() {
		return persentOtherState;
	}
	public void setPersentOtherState(String persentOtherState) {
		this.persentOtherState = persentOtherState;
	}
	public CountryMaster getPresentCountryId() {
		return presentCountryId;
	}
	public void setPresentCountryId(CountryMaster presentCountryId) {
		this.presentCountryId = presentCountryId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public Date getRetirementdatePERA() {
		return retirementdatePERA;
	}
	public void setRetirementdatePERA(Date retirementdatePERA) {
		this.retirementdatePERA = retirementdatePERA;
	}
	public String getDistrictEmail() {
		return districtEmail;
	}
	public void setDistrictEmail(String districtEmail) {
		this.districtEmail = districtEmail;
	}
	public String getBackgroundCheckDocument() {
		return backgroundCheckDocument;
	}
	public void setBackgroundCheckDocument(String backgroundCheckDocument) {
		this.backgroundCheckDocument = backgroundCheckDocument;
	}
	public void setBgCheckUploadedDate(Date bgCheckUploadedDate) {
		this.bgCheckUploadedDate = bgCheckUploadedDate;
	}
	public Date getBgCheckUploadedDate() {
		return bgCheckUploadedDate;
	}
	
}
