package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="districtpanelmaster")
public class DistrictPanelMaster implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6870762310266894163L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer panelId;
	
	@ManyToOne
	@JoinColumn(name="districtId", referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private String panelName;
	private String status;
	private Date createdDateTime;
	
	public Integer getPanelId() {
		return panelId;
	}
	public void setPanelId(Integer panelId) {
		this.panelId = panelId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getPanelName() {
		return panelName;
	}
	public void setPanelName(String panelName) {
		this.panelName = panelName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
}
