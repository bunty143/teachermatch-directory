package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.master.*;

@Entity
@Table(name="teacheracademics")
public class TeacherAcademics implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7635282760023873957L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long academicId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherId;
	
	@ManyToOne
	@JoinColumn(name="degreeId",referencedColumnName="degreeId")
	private DegreeMaster degreeId; 
	
	@ManyToOne
	@JoinColumn(name="universityId",referencedColumnName="universityId")
	private UniversityMaster universityId;
	
	@ManyToOne
	@JoinColumn(name="fieldId",referencedColumnName="fieldId")
	private FieldOfStudyMaster fieldId;     
	private Long attendedInYear;
	
	@Column(name="LeftInYear")
	private Long leftInYear; 
	
	private Double gpaFreshmanYear;     
	private Double gpaJuniorYear;     
	private Double gpaSophomoreYear;     
	private Double gpaSeniorYear;     
	private Double gpaCumulative;     
	private String pathOfTranscript;     
	private Boolean isDone;     
	private Date createdDateTime;
	private String status;
	private Date degreeConferredDate;
	private boolean international;
	private String otherUniversityName;
	private String otherDegreeName;
	private String otherFieldName;
	
	public Long getAcademicId() {
		return academicId;
	}
	public void setAcademicId(Long academicId) {
		this.academicId = academicId;
	}
	public TeacherDetail getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}
	public DegreeMaster getDegreeId() {
		return degreeId;
	}
	public void setDegreeId(DegreeMaster degreeId) {
		this.degreeId = degreeId;
	}
	public UniversityMaster getUniversityId() {
		return universityId;
	}
	public void setUniversityId(UniversityMaster universityId) {
		this.universityId = universityId;
	}
	public FieldOfStudyMaster getFieldId() {
		return fieldId;
	}
	public void setFieldId(FieldOfStudyMaster fieldId) {
		this.fieldId = fieldId;
	}
	public Long getAttendedInYear() {
		return attendedInYear;
	}
	public void setAttendedInYear(Long attendedInYear) {
		this.attendedInYear = attendedInYear;
	}
	public Long getLeftInYear() {
		return leftInYear;
	}
	public void setLeftInYear(Long leftInYear) {
		this.leftInYear = leftInYear;
	}
	public Double getGpaFreshmanYear() {
		return gpaFreshmanYear;
	}
	public void setGpaFreshmanYear(Double gpaFreshmanYear) {
		this.gpaFreshmanYear = gpaFreshmanYear;
	}
	public Double getGpaJuniorYear() {
		return gpaJuniorYear;
	}
	public void setGpaJuniorYear(Double gpaJuniorYear) {
		this.gpaJuniorYear = gpaJuniorYear;
	}
	public Double getGpaSophomoreYear() {
		return gpaSophomoreYear;
	}
	public void setGpaSophomoreYear(Double gpaSophomoreYear) {
		this.gpaSophomoreYear = gpaSophomoreYear;
	}
	public Double getGpaSeniorYear() {
		return gpaSeniorYear;
	}
	public void setGpaSeniorYear(Double gpaSeniorYear) {
		this.gpaSeniorYear = gpaSeniorYear;
	}
	public Double getGpaCumulative() {
		return gpaCumulative;
	}
	public void setGpaCumulative(Double gpaCumulative) {
		this.gpaCumulative = gpaCumulative;
	}
	public String getPathOfTranscript() {
		return pathOfTranscript;
	}
	public void setPathOfTranscript(String pathOfTranscript) {
		this.pathOfTranscript = pathOfTranscript;
	}
	public Boolean getIsDone() {
		return isDone;
	}
	public void setIsDone(Boolean isDone) {
		this.isDone = isDone;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getDegreeConferredDate() {
		return degreeConferredDate;
	}
	public void setDegreeConferredDate(Date degreeConferredDate) {
		this.degreeConferredDate = degreeConferredDate;
	}
	public boolean isInternational() {
        return international;
  }
  public void setInternational(boolean international) {
        this.international = international;
  }
public String getOtherUniversityName() {
	return otherUniversityName;
}
public void setOtherUniversityName(String otherUniversityName) {
	this.otherUniversityName = otherUniversityName;
}
public String getOtherDegreeName() {
	return otherDegreeName;
}
public void setOtherDegreeName(String otherDegreeName) {
	this.otherDegreeName = otherDegreeName;
}
public String getOtherFieldName() {
	return otherFieldName;
}
public void setOtherFieldName(String otherFieldName) {
	this.otherFieldName = otherFieldName;
}


  

}
