package tm.bean.teacher;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;

@Entity
@Table(name="studentteacherexperience")
public class StudentTeacherExperience implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1795892568383719690L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer studentTeacherExperienceId;
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	private String schoolName;
	private String subject;
	private Boolean pkOffered;
	private Boolean kgOffered;
	private Boolean g01Offered;
	private Boolean g02Offered;
	private Boolean g03Offered;
	private Boolean g04Offered;
	private Boolean g05Offered;
	private Boolean g06Offered;
	private Boolean g07Offered;
	private Boolean g08Offered;
	private Boolean g09Offered;
	private Boolean g10Offered;
	private Boolean g11Offered;
	private Boolean g12Offered;
	private String fromDate;
	private String toDate;
	private Date createdDateTime;
	public Integer getStudentTeacherExperienceId() {
		return studentTeacherExperienceId;
	}
	public void setStudentTeacherExperienceId(Integer studentTeacherExperienceId) {
		this.studentTeacherExperienceId = studentTeacherExperienceId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public Boolean getPkOffered() {
		return pkOffered;
	}
	public void setPkOffered(Boolean pkOffered) {
		this.pkOffered = pkOffered;
	}
	public Boolean getKgOffered() {
		return kgOffered;
	}
	public void setKgOffered(Boolean kgOffered) {
		this.kgOffered = kgOffered;
	}
	public Boolean getG01Offered() {
		return g01Offered;
	}
	public void setG01Offered(Boolean g01Offered) {
		this.g01Offered = g01Offered;
	}
	public Boolean getG02Offered() {
		return g02Offered;
	}
	public void setG02Offered(Boolean g02Offered) {
		this.g02Offered = g02Offered;
	}
	public Boolean getG03Offered() {
		return g03Offered;
	}
	public void setG03Offered(Boolean g03Offered) {
		this.g03Offered = g03Offered;
	}
	public Boolean getG04Offered() {
		return g04Offered;
	}
	public void setG04Offered(Boolean g04Offered) {
		this.g04Offered = g04Offered;
	}
	public Boolean getG05Offered() {
		return g05Offered;
	}
	public void setG05Offered(Boolean g05Offered) {
		this.g05Offered = g05Offered;
	}
	public Boolean getG06Offered() {
		return g06Offered;
	}
	public void setG06Offered(Boolean g06Offered) {
		this.g06Offered = g06Offered;
	}
	public Boolean getG07Offered() {
		return g07Offered;
	}
	public void setG07Offered(Boolean g07Offered) {
		this.g07Offered = g07Offered;
	}
	public Boolean getG08Offered() {
		return g08Offered;
	}
	public void setG08Offered(Boolean g08Offered) {
		this.g08Offered = g08Offered;
	}
	public Boolean getG09Offered() {
		return g09Offered;
	}
	public void setG09Offered(Boolean g09Offered) {
		this.g09Offered = g09Offered;
	}
	public Boolean getG10Offered() {
		return g10Offered;
	}
	public void setG10Offered(Boolean g10Offered) {
		this.g10Offered = g10Offered;
	}
	public Boolean getG11Offered() {
		return g11Offered;
	}
	public void setG11Offered(Boolean g11Offered) {
		this.g11Offered = g11Offered;
	}
	public Boolean getG12Offered() {
		return g12Offered;
	}
	public void setG12Offered(Boolean g12Offered) {
		this.g12Offered = g12Offered;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	
}
