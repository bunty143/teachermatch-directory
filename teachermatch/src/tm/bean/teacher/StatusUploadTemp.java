package tm.bean.teacher;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import tm.bean.EmployeeMaster;
import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.PanelAttendees;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SchoolTypeMaster;


@Entity
@Table(name="statusuploadtemp")
public class StatusUploadTemp  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7148696450220677581L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer statustempId;
	private String branchId;
	private String talent_email;
	private String job_id;
	private String node_name; 
	private String status; 
	private String sessionid; 
	private String errortext;
	private Date date;
	
	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String jobId) {
		job_id = jobId;
	}
	public String getTalent_email() {
		return talent_email;
	}
	public void setTalent_email(String talentEmail) {
		talent_email = talentEmail;
	}
	
	public Integer getStatustempId() {
		return statustempId;
	}
	public void setStatustempId(Integer statustempId) {
		this.statustempId = statustempId;
	}
	public String getNode_name() {
		return node_name;
	}
	public void setNode_name(String nodeName) {
		node_name = nodeName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public String getErrortext() {
		return errortext;
	}
	public void setErrortext(String errortext) {
		this.errortext = errortext;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
