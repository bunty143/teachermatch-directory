package tm.bean.teacher;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.teacher.OnlineActivityQuestionSet;
import tm.bean.user.UserMaster;

@Entity
@Table(name="onlineactivitynotes")
public class OnlineActivityNotes {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer onlineActivityNoteId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;

	@ManyToOne
	@JoinColumn(name="questionSetId",referencedColumnName="questionSetId")
	private OnlineActivityQuestionSet questionSetId;
	
	private String onlineActivityNotes;
	
	private Date createdDateTime;

	public Integer getOnlineActivityNoteId() {
		return onlineActivityNoteId;
	}

	public void setOnlineActivityNoteId(Integer onlineActivityNoteId) {
		this.onlineActivityNoteId = onlineActivityNoteId;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public OnlineActivityQuestionSet getQuestionSetId() {
		return questionSetId;
	}

	public void setQuestionSetId(OnlineActivityQuestionSet questionSetId) {
		this.questionSetId = questionSetId;
	}

	public String getOnlineActivityNotes() {
		return onlineActivityNotes;
	}

	public void setOnlineActivityNotes(String onlineActivityNotes) {
		this.onlineActivityNotes = onlineActivityNotes;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
	

}
