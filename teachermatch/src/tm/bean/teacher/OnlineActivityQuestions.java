package tm.bean.teacher;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="onlineactivityquestions")
public class OnlineActivityQuestions implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3942047904452764182L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer questionId; 	
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster  jobCategoryMaster;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="questionSetId",referencedColumnName="questionSetId")
	private OnlineActivityQuestionSet onlineActivityQuestionSet;
	
	private String question;
	
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster  questionTypeMaster;
	
	private String questionInstructions;
	private int isRequired;
	private Integer maxScore;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	
	private String status;
	
	private Date createdDateTime;
	
	
	public Integer getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(Integer maxScore) {
		this.maxScore = maxScore;
	}

	public OnlineActivityQuestionSet getOnlineActivityQuestionSet() {
		return onlineActivityQuestionSet;
	}

	public void setOnlineActivityQuestionSet(
			OnlineActivityQuestionSet onlineActivityQuestionSet) {
		this.onlineActivityQuestionSet = onlineActivityQuestionSet;
	}

	public int getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(int isRequired) {
		this.isRequired = isRequired;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="questionId",referencedColumnName="questionId",insertable=false,updatable=false)
	private List<OnlineActivityOptions> questionOptions;
	

	public List<OnlineActivityOptions> getQuestionOptions() {
		return questionOptions;
	}

	public void setQuestionOptions(
			List<OnlineActivityOptions> questionOptions) {
		this.questionOptions = questionOptions;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}

	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}

	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}

	public String getQuestionInstructions() {
		return questionInstructions;
	}

	public void setQuestionInstructions(String questionInstructions) {
		this.questionInstructions = questionInstructions;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
