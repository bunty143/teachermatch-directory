package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="cmseventschedulemaster")
public class CmsEventScheduleMaster implements Serializable{
	private static final long serialVersionUID = 180177534230919337L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer eventScheduleId;
	private String eventScheduleName;
	@ManyToOne
	@JoinColumn(name="eventTypeId",referencedColumnName="eventTypeId")
	private  CmsEventTypeMaster cmsEventTypeMaster;
	
	public CmsEventTypeMaster getCmsEventTypeMaster() {
		return cmsEventTypeMaster;
	}
	public void setCmsEventTypeMaster(CmsEventTypeMaster cmsEventTypeMaster) {
		this.cmsEventTypeMaster = cmsEventTypeMaster;
	}
	public Integer getEventScheduleId() {
		return eventScheduleId;
	}
	public void setEventScheduleId(Integer eventScheduleId) {
		this.eventScheduleId = eventScheduleId;
	}
	public String getEventScheduleName() {
		return eventScheduleName;
	}
	public void setEventScheduleName(String eventScheduleName) {
		this.eventScheduleName = eventScheduleName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public UserMaster getCreatedBY() {
		return createdBY;
	}
	public void setCreatedBY(UserMaster createdBY) {
		this.createdBY = createdBY;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	private String status;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBY;
	private Date createdDateTime;

}
