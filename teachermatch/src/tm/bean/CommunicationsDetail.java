package tm.bean;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Transient;

import tm.bean.mq.MQEvent;

public class CommunicationsDetail implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1706626696837962823L;
	private Date createdDateTime; 
	private String txtMessage;
	private String commType;
	private String fileName;
	private String userFirstName;
	private String userLastName;
	private String msgSubject;
	private String jobTitle;
	
	private String schoolName;
	private String districtName;
	
	private JobOrder jobOrder;
	
	private TeacherDetail teacherDetail;
	
	private String withdrawnreason;
	
	public String getWithdrawnreason() {
		return withdrawnreason;
	}
	public void setWithdrawnreason(String withdrawnreason) {
		this.withdrawnreason = withdrawnreason;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	private String messageFlag;
	
	public String getMessageFlag() {
		return messageFlag;
	}
	public void setMessageFlag(String messageFlag) {
		this.messageFlag = messageFlag;
	}

	@Transient
	private String qqFlag;
	
	public String getQqFlag() {
		return qqFlag;
	}
	public void setQqFlag(String qqFlag) {
		this.qqFlag = qqFlag;
	}
	
	private String prevColor;
	private String currentColor;
	private MQEvent mqEvent;
	private Date updateDateTime;
	private String activityType;
	private String logColumn;
	private Date lastUpdateDateTime;
	
	public String getMsgSubject() {
		return msgSubject;
	}
	public void setMsgSubject(String msgSubject) {
		this.msgSubject = msgSubject;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getCommType() {
		return commType;
	}
	public void setCommType(String commType) {
		this.commType = commType;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getTxtMessage() {
		return txtMessage;
	}
	public void setTxtMessage(String txtMessage) {
		this.txtMessage = txtMessage;
	}
	
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public static Comparator<CommunicationsDetail> createdDate= new Comparator<CommunicationsDetail>(){
		 public int compare(CommunicationsDetail m1, CommunicationsDetail m2) {
		        return m2.getCreatedDateTime().compareTo(m1.getCreatedDateTime());
		  }		
	};
	public String getPrevColor() {
		return prevColor;
	}
	public void setPrevColor(String prevColor) {
		this.prevColor = prevColor;
	}
	public String getCurrentColor() {
		return currentColor;
	}
	public void setCurrentColor(String currentColor) {
		this.currentColor = currentColor;
	}
	public MQEvent getMqEvent() {
		return mqEvent;
	}
	public void setMqEvent(MQEvent mqEvent) {
		this.mqEvent = mqEvent;
	}
	public Date getUpdateDateTime() {
		return updateDateTime;
	}
	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	public String getLogColumn() {
		return logColumn;
	}
	public void setLogColumn(String logColumn) {
		this.logColumn = logColumn;
	}
	public Date getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}
	public void setLastUpdateDateTime(Date lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}
	
	
		
}
