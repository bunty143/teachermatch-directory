package tm.bean;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;

public class TeacherAssessmentStatusDetail implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 852805875994235709L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherAssessmentStatusId;  
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;   
	
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentDetail;     
	
	@ManyToOne
	@JoinColumn(name="teacherAssessmentId",referencedColumnName="teacherAssessmentId")
	private TeacherAssessmentdetail teacherAssessmentdetail;  
	
	private Integer assessmentType;     
	
	@ManyToOne
	@JoinColumn(name="statusId",referencedColumnName="statusId")
	private StatusMaster statusMaster;   
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="updatedBy",referencedColumnName="userId")
	private UserMaster updatedBy;
	
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userId; 
	
	private Integer updatedByEntity; 
	private Date updatedDate; 
	private Boolean cgUpdated;
	private Date createdDateTime;
	private Date assessmentCompletedDateTime;
	private int changedStatusId;
	private String assessmentName;
	public Integer getTeacherAssessmentStatusId() {
		return teacherAssessmentStatusId;
	}
	public void setTeacherAssessmentStatusId(Integer teacherAssessmentStatusId) {
		this.teacherAssessmentStatusId = teacherAssessmentStatusId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public AssessmentDetail getAssessmentDetail() {
		return assessmentDetail;
	}
	public void setAssessmentDetail(AssessmentDetail assessmentDetail) {
		this.assessmentDetail = assessmentDetail;
	}
	public TeacherAssessmentdetail getTeacherAssessmentdetail() {
		return teacherAssessmentdetail;
	}
	public void setTeacherAssessmentdetail(
			TeacherAssessmentdetail teacherAssessmentdetail) {
		this.teacherAssessmentdetail = teacherAssessmentdetail;
	}
	public Integer getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(Integer assessmentType) {
		this.assessmentType = assessmentType;
	}
	public StatusMaster getStatusMaster() {
		return statusMaster;
	}
	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public UserMaster getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(UserMaster updatedBy) {
		this.updatedBy = updatedBy;
	}
	public UserMaster getUserId() {
		return userId;
	}
	public void setUserId(UserMaster userId) {
		this.userId = userId;
	}
	public Integer getUpdatedByEntity() {
		return updatedByEntity;
	}
	public void setUpdatedByEntity(Integer updatedByEntity) {
		this.updatedByEntity = updatedByEntity;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Boolean getCgUpdated() {
		return cgUpdated;
	}
	public void setCgUpdated(Boolean cgUpdated) {
		this.cgUpdated = cgUpdated;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public int getChangedStatusId() {
		return changedStatusId;
	}
	public void setChangedStatusId(int changedStatusId) {
		this.changedStatusId = changedStatusId;
	}
	public String getAssessmentName() {
		return assessmentName;
	}
	public void setAssessmentName(String assessmentName) {
		this.assessmentName = assessmentName;
	}
	public Date getAssessmentCompletedDateTime() {
        return assessmentCompletedDateTime;
	}
	public void setAssessmentCompletedDateTime(Date assessmentCompletedDateTime) {
        this.assessmentCompletedDateTime = assessmentCompletedDateTime;
	}
	public static Comparator<TeacherAssessmentStatusDetail> createdDate= new Comparator<TeacherAssessmentStatusDetail>(){
		 public int compare(TeacherAssessmentStatusDetail m1, TeacherAssessmentStatusDetail m2) {
		        return m2.getCreatedDateTime().compareTo(m1.getCreatedDateTime());
		  }		
	};
}

