package tm.bean;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SubjectMaster;
import tm.utility.Utility;

@Entity
@Table(name="jobcategoryforinternalcandidate")
public class JobCategoryForInternalCandidate implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1696251873847142311L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer internaljobCategoryId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="internalCandidateId",referencedColumnName="internalCandidateId")
	private InternalTransferCandidates internalTransferCandidates;     

	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	
	
	@ManyToOne
	@JoinColumn(name="subjectId",referencedColumnName="subjectId")
	private SubjectMaster subjectMaster;
	
	@Transient
	String [] chkStatusMaster;

	public String[] getChkStatusMaster() {
		return chkStatusMaster;
	}


	public void setChkStatusMaster(String[] chkStatusMaster) {
		this.chkStatusMaster = chkStatusMaster;
	}


	public Integer getInternaljobCategoryId() {
		return internaljobCategoryId;
	}


	public void setInternaljobCategoryId(Integer internaljobCategoryId) {
		this.internaljobCategoryId = internaljobCategoryId;
	}


	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}


	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}


	public InternalTransferCandidates getInternalTransferCandidates() {
		return internalTransferCandidates;
	}


	public void setInternalTransferCandidates(
			InternalTransferCandidates internalTransferCandidates) {
		this.internalTransferCandidates = internalTransferCandidates;
	}


	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}


	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}


	public SubjectMaster getSubjectMaster() {
		return subjectMaster;
	}


	public void setSubjectMaster(SubjectMaster subjectMaster) {
		this.subjectMaster = subjectMaster;
	}
	
}
