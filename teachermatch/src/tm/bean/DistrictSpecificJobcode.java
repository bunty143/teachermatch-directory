package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;

@Entity
@Table(name="districtspecificjobcode")
public class DistrictSpecificJobcode implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer distjobcodeId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryId;
	
	private String jobCode;
	private String jobTitle;
	private Double hours;
	private String jobDescription;
	    
	private String status;
	private Date effectiveDate;
	private Date createdDateTime;
	private String ipAddress;
	
	private String salaryAdminPlan;
	private String step;
	private String gradePay;
	
	
	public Integer getDistjobcodeId() {
		return distjobcodeId;
	}
	public void setDistjobcodeId(Integer distjobcodeId) {
		this.distjobcodeId = distjobcodeId;
	}
	public DistrictMaster getDistricMaster() {
		return districtMaster;
	}
	public void setDistricMaster(DistrictMaster districMaster) {
		this.districtMaster = districMaster;
	}
	public JobCategoryMaster getJobCategoryId() {
		return jobCategoryId;
	}
	public void setJobCategoryId(JobCategoryMaster jobCategoryId) {
		this.jobCategoryId = jobCategoryId;
	}
	public String getJobCode() {
		return jobCode;
	}
	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public Double getHours() {
		return hours;
	}
	public void setHours(Double hours) {
		this.hours = hours;
	}
	public String getJobDescription() {
		return jobDescription;
	}
	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}
	
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getSalaryAdminPlan() {
		return salaryAdminPlan;
	}
	public void setSalaryAdminPlan(String salaryAdminPlan) {
		this.salaryAdminPlan = salaryAdminPlan;
	}
	public String getStep() {
		return step;
	}
	public void setStep(String step) {
		this.step = step;
	}
	public String getGradePay() {
		return gradePay;
	}
	public void setGradePay(String gradePay) {
		this.gradePay = gradePay;
	}
}
