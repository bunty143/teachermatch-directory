package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;

@Entity
@Table(name="teacherelectronicreferencehistory")
public class TeacherElectronicReferencesHistory implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8445329778613819072L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer elerefHAutoId;
	
	@ManyToOne
	@JoinColumn(name="elerefAutoId",referencedColumnName="elerefAutoId")
	private TeacherElectronicReferences teacherElectronicReferences;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;

	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;

	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	private String email;
	private Integer contactStatus;
	private Integer replyStatus;
	private Integer questionMaxScore;
	
	public Integer getQuestionMaxScore() {
		return questionMaxScore;
	}
	public void setQuestionMaxScore(Integer questionMaxScore) {
		this.questionMaxScore = questionMaxScore;
	}
	private Date createdDateTime;
	
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	public Integer getElerefHAutoId() {
		return elerefHAutoId;
	}
	public void setElerefHAutoId(Integer elerefHAutoId) {
		this.elerefHAutoId = elerefHAutoId;
	}
	public TeacherElectronicReferences getTeacherElectronicReferences() {
		return teacherElectronicReferences;
	}
	public void setTeacherElectronicReferences(
			TeacherElectronicReferences teacherElectronicReferences) {
		this.teacherElectronicReferences = teacherElectronicReferences;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Integer getContactStatus() {
		return contactStatus;
	}
	public void setContactStatus(Integer contactStatus) {
		this.contactStatus = contactStatus;
	}
	public Integer getReplyStatus() {
		return replyStatus;
	}
	public void setReplyStatus(Integer replyStatus) {
		this.replyStatus = replyStatus;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
}