package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="octdetails")
public class OctDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7926317523304289917L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer octId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherId;
	
	
	
	private Integer octOption;
	private String octNumber;
	private String octFileName;
	private String octText;
	private Date createdDate;
	
	public Integer getOctId() {
		return octId;
	}
	public void setOctId(Integer octId) {
		this.octId = octId;
	}
	
	public TeacherDetail getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}
	public Integer getOctOption() {
		return octOption;
	}
	public void setOctOption(Integer octOption) {
		this.octOption = octOption;
	}
	public String getOctNumber() {
		return octNumber;
	}
	public void setOctNumber(String octNumber) {
		this.octNumber = octNumber;
	}
	public String getOctFileName() {
		return octFileName;
	}
	public void setOctFileName(String octFileName) {
		this.octFileName = octFileName;
	}
	public String getOctText() {
		return octText;
	}
	public void setOctText(String octText) {
		this.octText = octText;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}	
}
