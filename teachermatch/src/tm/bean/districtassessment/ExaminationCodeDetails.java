package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.JobCategoryMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="examinationcodedetails")
public class ExaminationCodeDetails implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1279790305227990147L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer codeId;
	
	@ManyToOne
	@JoinColumn(name="districtAssessmentId",referencedColumnName="districtAssessmentId")
	private DistrictAssessmentDetail districtAssessmentDetail;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;  
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	
	private String examinationCode;
	
	private Date examDate;
	private String examStartTime;
	private String examEndTime;
	
	private Boolean approved;
	
	@ManyToOne
	@JoinColumn(name="approvedBy",referencedColumnName="userId")
	private UserMaster approvedBy;
	
	public DistrictAssessmentDetail getDistrictAssessmentDetail() {
		return districtAssessmentDetail;
	}
	public void setDistrictAssessmentDetail(
			DistrictAssessmentDetail districtAssessmentDetail) {
		this.districtAssessmentDetail = districtAssessmentDetail;
	}
	public String getExamStartTime() {
		return examStartTime;
	}
	public void setExamStartTime(String examStartTime) {
		this.examStartTime = examStartTime;
	}
	public String getExamEndTime() {
		return examEndTime;
	}
	public void setExamEndTime(String examEndTime) {
		this.examEndTime = examEndTime;
	}
	public Date getExamDate() {
		return examDate;
	}
	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}
	public Boolean getApproved() {
		return approved;
	}
	public void setApproved(Boolean approved) {
		this.approved = approved;
	}
	public UserMaster getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(UserMaster approvedBy) {
		this.approvedBy = approvedBy;
	}
	public Date getApprovedDate() {
		return approvedDate;
	}
	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}
	private Date approvedDate;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private String inviteStatus;
	
	private String status;
	
	private java.util.Date createdDateTime;
	private String ipaddress;
	public Integer getCodeId() {
		return codeId;
	}
	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public String getExaminationCode() {
		return examinationCode;
	}
	public void setExaminationCode(String examinationCode) {
		this.examinationCode = examinationCode;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getInviteStatus() {
		return inviteStatus;
	}
	public void setInviteStatus(String inviteStatus) {
		this.inviteStatus = inviteStatus;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public java.util.Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(java.util.Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}
	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}


}
