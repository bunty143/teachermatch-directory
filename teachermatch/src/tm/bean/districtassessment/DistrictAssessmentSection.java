package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.user.UserMaster;

@Entity
@Table(name="districtassessmentsections")
public class DistrictAssessmentSection implements Serializable 
{

	private static final long serialVersionUID = -7487899849443267850L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtSectionId;
	
	@ManyToOne
	@JoinColumn(name="districtAssessmentId",referencedColumnName="districtAssessmentId")
	private DistrictAssessmentDetail districtAssessmentDetail;
	private String sectionName;
	private String sectionDescription;
	private String sectionInstructions;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	private String status;
	private String ipaddress;
	@Column(updatable=false)
	private Date createdDateTime;
	
	public Integer getDistrictSectionId() {
		return districtSectionId;
	}
	public void setDistrictSectionId(Integer districtSectionId) {
		this.districtSectionId = districtSectionId;
	}
	public DistrictAssessmentDetail getDistrictAssessmentDetail() {
		return districtAssessmentDetail;
	}
	public void setDistrictAssessmentDetail(
			DistrictAssessmentDetail districtAssessmentDetail) {
		this.districtAssessmentDetail = districtAssessmentDetail;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getSectionDescription() {
		return sectionDescription;
	}
	public void setSectionDescription(String sectionDescription) {
		this.sectionDescription = sectionDescription;
	}
	public String getSectionInstructions() {
		return sectionInstructions;
	}
	public void setSectionInstructions(String sectionInstructions) {
		this.sectionInstructions = sectionInstructions;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
