package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;

@Entity
@Table(name="teacherdistrictassessmentoptions")
public class TeacherDistrictAssessmentOption implements Serializable 
{

	private static final long serialVersionUID = 3708268685106726992L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long teacherDistrictAssessmentOptionId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="teacherDistrictAssessmentQuestionId",referencedColumnName="teacherDistrictAssessmentQuestionId")
	private TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion;   
	
	private String questionOption;                    
	private Integer rank;                      
	private Double score;     
	private Date createdDateTime;
	public Long getTeacherDistrictAssessmentOptionId() {
		return teacherDistrictAssessmentOptionId;
	}
	public void setTeacherDistrictAssessmentOptionId(
			Long teacherDistrictAssessmentOptionId) {
		this.teacherDistrictAssessmentOptionId = teacherDistrictAssessmentOptionId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public TeacherDistrictAssessmentQuestion getTeacherDistrictAssessmentQuestion() {
		return teacherDistrictAssessmentQuestion;
	}
	public void setTeacherDistrictAssessmentQuestion(
			TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion) {
		this.teacherDistrictAssessmentQuestion = teacherDistrictAssessmentQuestion;
	}
	public String getQuestionOption() {
		return questionOption;
	}
	public void setQuestionOption(String questionOption) {
		this.questionOption = questionOption;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
