package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.TimeZoneMaster;

@Entity
@Table(name="centerschedulemaster")
public class CenterScheduleMaster implements Serializable 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2808485427423723358L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer centreScheduleId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="centerId",referencedColumnName="centerId")
	private CenterMaster centerMaster;
	
	@ManyToOne
	@JoinColumn(name="timeZoneId",referencedColumnName="timeZoneId")
	private TimeZoneMaster timeZoneMaster;
	
	private Integer availability;
	
	private String csTime;
	
	private Date csDate;
	private Integer noOfApplicantsScheduled;
	
	@ManyToOne
	@JoinColumn(name="reservedForJobCategory",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	private Integer scheduleCreatedBy;
	private Integer timeOrder;
	
	private Date scheduleCreatedDateTime;
	
	public Integer getTimeOrder() {
		return timeOrder;
	}

	public void setTimeOrder(Integer timeOrder) {
		this.timeOrder = timeOrder;
	}

	public String getCsTime() {
		return csTime;
	}

	public void setCsTime(String csTime) {
		this.csTime = csTime;
	}

	public Date getCsDate() {
		return csDate;
	}

	public void setCsDate(Date csDate) {
		this.csDate = csDate;
	}

	public Integer getCentreScheduleId() {
		return centreScheduleId;
	}

	public void setCentreScheduleId(Integer centreScheduleId) {
		this.centreScheduleId = centreScheduleId;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public CenterMaster getCenterMaster() {
		return centerMaster;
	}

	public void setCenterMaster(CenterMaster centerMaster) {
		this.centerMaster = centerMaster;
	}

	public TimeZoneMaster getTimeZoneMaster() {
		return timeZoneMaster;
	}

	public void setTimeZoneMaster(TimeZoneMaster timeZoneMaster) {
		this.timeZoneMaster = timeZoneMaster;
	}

	public Integer getAvailability() {
		return availability;
	}

	public void setAvailability(Integer availability) {
		this.availability = availability;
	}

	public Integer getNoOfApplicantsScheduled() {
		return noOfApplicantsScheduled;
	}

	public void setNoOfApplicantsScheduled(Integer noOfApplicantsScheduled) {
		this.noOfApplicantsScheduled = noOfApplicantsScheduled;
	}

	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}

	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}

	public Integer getScheduleCreatedBy() {
		return scheduleCreatedBy;
	}

	public void setScheduleCreatedBy(Integer scheduleCreatedBy) {
		this.scheduleCreatedBy = scheduleCreatedBy;
	}

	public Date getScheduleCreatedDateTime() {
		return scheduleCreatedDateTime;
	}

	public void setScheduleCreatedDateTime(Date scheduleCreatedDateTime) {
		this.scheduleCreatedDateTime = scheduleCreatedDateTime;
	}
	
	
}
