package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.StatusMaster;

@Entity
@Table(name="teacherdistrictassessmentstatus")
public class TeacherDistrictAssessmentStatus implements Serializable
{

	private static final long serialVersionUID = -4889977038610340928L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherDistrictAssessmentStatusId;  
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;   
	
	@ManyToOne
	@JoinColumn(name="districtAssessmentId",referencedColumnName="districtAssessmentId")
	private DistrictAssessmentDetail districtAssessmentDetail;     
	
	@ManyToOne
	@JoinColumn(name="teacherDistrictAssessmentId",referencedColumnName="teacherDistrictAssessmentId")
	private TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail;  
	
	@ManyToOne
	@JoinColumn(name="statusId",referencedColumnName="statusId")
	private StatusMaster statusMaster;   
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	private Date updatedDate; 
	private Date createdDateTime;
	private Double marksObtained;
	private Double totalMarks;
	
	public Integer getTeacherDistrictAssessmentStatusId() {
		return teacherDistrictAssessmentStatusId;
	}
	public void setTeacherDistrictAssessmentStatusId(
			Integer teacherDistrictAssessmentStatusId) {
		this.teacherDistrictAssessmentStatusId = teacherDistrictAssessmentStatusId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public DistrictAssessmentDetail getDistrictAssessmentDetail() {
		return districtAssessmentDetail;
	}
	public void setDistrictAssessmentDetail(
			DistrictAssessmentDetail districtAssessmentDetail) {
		this.districtAssessmentDetail = districtAssessmentDetail;
	}
	public TeacherDistrictAssessmentDetail getTeacherDistrictAssessmentDetail() {
		return teacherDistrictAssessmentDetail;
	}
	public void setTeacherDistrictAssessmentDetail(
			TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail) {
		this.teacherDistrictAssessmentDetail = teacherDistrictAssessmentDetail;
	}
	public StatusMaster getStatusMaster() {
		return statusMaster;
	}
	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Double getMarksObtained() {
		return marksObtained;
	}
	public void setMarksObtained(Double marksObtained) {
		this.marksObtained = marksObtained;
	}
	public void setTotalMarks(Double totalMarks) {
		this.totalMarks = totalMarks;
	}
	public Double getTotalMarks() {
		return totalMarks;
	}
}
