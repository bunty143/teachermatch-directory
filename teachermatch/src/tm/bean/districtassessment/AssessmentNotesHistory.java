package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;

@Entity
@Table(name="assessmentnoteshistory")
public class AssessmentNotesHistory implements Serializable 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -685057905701100594L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer assessmentNotesId;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	@ManyToOne
	@JoinColumn(name="districtAssessmentId",referencedColumnName="districtAssessmentId")
	private DistrictAssessmentDetail districtAssessmentDetail;
	
	@ManyToOne
	@JoinColumn(name="teacherDistrictAssessmentQuestionId",referencedColumnName="teacherDistrictAssessmentQuestionId")
	private TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	private String assessmentNotes;
	
	private Date createdDateTime;

	public Integer getAssessmentNotesId() {
		return assessmentNotesId;
	}

	public void setAssessmentNotesId(Integer assessmentNotesId) {
		this.assessmentNotesId = assessmentNotesId;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public DistrictAssessmentDetail getDistrictAssessmentDetail() {
		return districtAssessmentDetail;
	}

	public void setDistrictAssessmentDetail(
			DistrictAssessmentDetail districtAssessmentDetail) {
		this.districtAssessmentDetail = districtAssessmentDetail;
	}

	public TeacherDistrictAssessmentQuestion getTeacherDistrictAssessmentQuestion() {
		return teacherDistrictAssessmentQuestion;
	}

	public void setTeacherDistrictAssessmentQuestion(
			TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion) {
		this.teacherDistrictAssessmentQuestion = teacherDistrictAssessmentQuestion;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public String getAssessmentNotes() {
		return assessmentNotes;
	}

	public void setAssessmentNotes(String assessmentNotes) {
		this.assessmentNotes = assessmentNotes;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
}
