package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;

@Entity
@Table(name="teacherdistrictstrikelog")
public class TeacherDistrictStrikeLog implements Serializable 
{

	private static final long serialVersionUID = -2373767036190335772L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer strikeId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="teacherDistrictAssessmentId",referencedColumnName="teacherDistrictAssessmentId")
	private TeacherDistrictAssessmentDetail teacherDistrictAssessmentdetail;
	@ManyToOne
	@JoinColumn(name="teacherDistrictAssessmentQuestionId",referencedColumnName="teacherDistrictAssessmentQuestionId")
	private TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion;
	private String ipAddress;
	private Date createdDateTime;
	public Integer getStrikeId() {
		return strikeId;
	}
	public void setStrikeId(Integer strikeId) {
		this.strikeId = strikeId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public TeacherDistrictAssessmentDetail getTeacherDistrictAssessmentdetail() {
		return teacherDistrictAssessmentdetail;
	}
	public void setTeacherDistrictAssessmentdetail(
			TeacherDistrictAssessmentDetail teacherDistrictAssessmentdetail) {
		this.teacherDistrictAssessmentdetail = teacherDistrictAssessmentdetail;
	}
	public TeacherDistrictAssessmentQuestion getTeacherDistrictAssessmentQuestion() {
		return teacherDistrictAssessmentQuestion;
	}
	public void setTeacherDistrictAssessmentQuestion(
			TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion) {
		this.teacherDistrictAssessmentQuestion = teacherDistrictAssessmentQuestion;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
	
}
