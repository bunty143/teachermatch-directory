package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.assessment.AssessmentSections;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="districtassessmentquestions")
public class DistrictAssessmentQuestions implements Serializable
{

	private static final long serialVersionUID = -7285867604634051710L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtAssessmentQuestionId;
	@ManyToOne
	@JoinColumn(name="districtAssessmentId",referencedColumnName="districtAssessmentId")
	private DistrictAssessmentDetail districtAssessmentDetail;
	
	@ManyToOne
	@JoinColumn(name="districtSectionId",referencedColumnName="districtSectionId")
	private DistrictAssessmentSection districtAssessmentSection;
	
	private String question;
	
	private String questionImage;
	
	private Double questionWeightage;
	private Double maxMarks;
	private String questionInstruction;
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster questionTypeMaster;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	private String status;
	private Date createdDateTime;
	
	@OneToMany()
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="districtAssessmentQuestionId",referencedColumnName="districtAssessmentQuestionId",insertable=false,updatable=false)
	private List<DistrictAssessmentQuestionOptions> districtAssessmentQuestionOptions;
	
	public Integer getDistrictAssessmentQuestionId() {
		return districtAssessmentQuestionId;
	}
	public void setDistrictAssessmentQuestionId(Integer districtAssessmentQuestionId) {
		this.districtAssessmentQuestionId = districtAssessmentQuestionId;
	}
	public DistrictAssessmentDetail getDistrictAssessmentDetail() {
		return districtAssessmentDetail;
	}
	public void setDistrictAssessmentDetail(
			DistrictAssessmentDetail districtAssessmentDetail) {
		this.districtAssessmentDetail = districtAssessmentDetail;
	}
	public void setDistrictAssessmentSection(
			DistrictAssessmentSection districtAssessmentSection) {
		this.districtAssessmentSection = districtAssessmentSection;
	}
	public DistrictAssessmentSection getDistrictAssessmentSection() {
		return districtAssessmentSection;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public Double getQuestionWeightage() {
		return questionWeightage;
	}
	public void setQuestionWeightage(Double questionWeightage) {
		this.questionWeightage = questionWeightage;
	}
	public Double getMaxMarks() {
		return maxMarks;
	}
	public void setMaxMarks(Double maxMarks) {
		this.maxMarks = maxMarks;
	}
	public String getQuestionInstruction() {
		return questionInstruction;
	}
	public void setQuestionInstruction(String questionInstruction) {
		this.questionInstruction = questionInstruction;
	}
	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}
	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public List<DistrictAssessmentQuestionOptions> getDistrictAssessmentQuestionOptions() {
		return districtAssessmentQuestionOptions;
	}
	public void setDistrictAssessmentQuestionOptions(
			List<DistrictAssessmentQuestionOptions> districtAssessmentQuestionOptions) {
		this.districtAssessmentQuestionOptions = districtAssessmentQuestionOptions;
	}
	public String getQuestionImage() {
		return questionImage;
	}
	public void setQuestionImage(String questionImage) {
		this.questionImage = questionImage;
	}
	
}
