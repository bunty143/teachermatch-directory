package tm.bean;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.DistrictMaster;
import tm.bean.master.EligibilityMaster;
import tm.bean.user.UserMaster;


@Entity
@Table(name="eligibilityverificationhistroy")
public class EligibilityVerificationHistroy implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4560740482225344351L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer eligibilityVerificationId;
	private Date createdDateTime;
	private Date eligibilityExaminationDate;
	private Date eligibilityClearExaminationDate;
	
	private Date failedDateFp;
	private Date reprintDateFp;
	
	private String notes;
	private String activity;
	private String attachment; 
	
	private String employeeId;
	private String salary;
	private String grade;
	private String step;
	private Date startDate ;
	private Date endDate ;
	private Boolean yearRound ;
	private String actualDays ;
	private String totalDays ;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;

	@ManyToOne
	@JoinColumn(name="eligibilityId",referencedColumnName="eligibilityId")
	private EligibilityMaster eligibilityMaster;
	
	@ManyToOne
	@JoinColumn(name="activityDoneBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	@ManyToOne
	@JoinColumn(name="eligibilityStatusId",referencedColumnName="eligibilityStatusId")
	private EligibilityStatusMaster eligibilityStatusMaster;
	
	@ManyToOne
	@JoinColumn(name="eligibilityProficiencyId",referencedColumnName="eligibilityProficiencyId")
	private EligibilityProficiencyMaster eligibilityProficiencyMaster;

	
	public Date getFailedDateFp() {
		return failedDateFp;
	}

	public void setFailedDateFp(Date failedDateFp) {
		this.failedDateFp = failedDateFp;
	}

	public Date getReprintDateFp() {
		return reprintDateFp;
	}

	public void setReprintDateFp(Date reprintDateFp) {
		this.reprintDateFp = reprintDateFp;
	}

	public Integer getEligibilityVerificationId() {
		return eligibilityVerificationId;
	}

	public void setEligibilityVerificationId(Integer eligibilityVerificationId) {
		this.eligibilityVerificationId = eligibilityVerificationId;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public Date getEligibilityExaminationDate() {
		return eligibilityExaminationDate;
	}

	public void setEligibilityExaminationDate(Date eligibilityExaminationDate) {
		this.eligibilityExaminationDate = eligibilityExaminationDate;
	}

	public Date getEligibilityClearExaminationDate() {
		return eligibilityClearExaminationDate;
	}

	public void setEligibilityClearExaminationDate(
			Date eligibilityClearExaminationDate) {
		this.eligibilityClearExaminationDate = eligibilityClearExaminationDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public EligibilityMaster getEligibilityMaster() {
		return eligibilityMaster;
	}

	public void setEligibilityMaster(EligibilityMaster eligibilityMaster) {
		this.eligibilityMaster = eligibilityMaster;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public EligibilityStatusMaster getEligibilityStatusMaster() {
		return eligibilityStatusMaster;
	}

	public void setEligibilityStatusMaster(
			EligibilityStatusMaster eligibilityStatusMaster) {
		this.eligibilityStatusMaster = eligibilityStatusMaster;
	}

	public EligibilityProficiencyMaster getEligibilityProficiencyMaster() {
		return eligibilityProficiencyMaster;
	}

	public void setEligibilityProficiencyMaster(
			EligibilityProficiencyMaster eligibilityProficiencyMaster) {
		this.eligibilityProficiencyMaster = eligibilityProficiencyMaster;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getStep() {
		return step;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Boolean getYearRound() {
		return yearRound;
	}

	public void setYearRound(Boolean yearRound) {
		this.yearRound = yearRound;
	}

	public String getActualDays() {
		return actualDays;
	}

	public void setActualDays(String actualDays) {
		this.actualDays = actualDays;
	}

	public String getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(String totalDays) {
		this.totalDays = totalDays;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	
	
	
	
}
