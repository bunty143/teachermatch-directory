package tm.bean;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.master.*;

@Entity
@Table(name="teacheradditionaldocuments")
public class TeacherAdditionalDocuments implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3823056833439075135L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer additionDocumentId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	private String  documentName;
	
	private String  uploadedDocument;
	
	private Date createdDateTime;
	

	public Integer getAdditionDocumentId() {
		return additionDocumentId;
	}

	public void setAdditionDocumentId(Integer additionDocumentId) {
		this.additionDocumentId = additionDocumentId;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getUploadedDocument() {
		return uploadedDocument;
	}

	public void setUploadedDocument(String uploadedDocument) {
		this.uploadedDocument = uploadedDocument;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	} 
	
	
}
