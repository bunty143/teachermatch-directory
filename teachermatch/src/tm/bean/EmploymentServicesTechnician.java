package tm.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * 
 * alter table employmentservicestechnician ADD `employeeId` int(10) NOT NULL;
   alter table employmentservicestechnician MODIFY eSBackupId int(10) UNIQUE NOT NULL;


CREATE TABLE `employmentservicestechnician` (
  `estechnicianId` int(10) NOT NULL auto_increment,
  `employeeId` int(10) NOT NULL,
  `primaryESTech` text NOT NULL COMMENT 'Primary Employement Service Technician',
  `eSBackupId` int(10) UNIQUE NOT NULL,
  `backupESTech` text NOT NULL COMMENT 'Backup Employement Service Technician',
   PRIMARY KEY  (`estechnicianId`)
);

ALTER TABLE  `joborder` ADD  `positionStartDate` DATE NULL DEFAULT NULL AFTER  `jobEndDate` ,
ADD  `positionEndDate` DATE NULL DEFAULT NULL AFTER  `positionStartDate` ,
ADD  `tempType` TINYINT NULL DEFAULT NULL AFTER  `positionEndDate` ,
ADD  `jobApplicationStatus` TINYINT NULL DEFAULT NULL AFTER  `tempType` ;

alter table joborder add salaryRange text default NULL ;
alter table joborder add additionalDocumentPath text default NULL ;
alter table joborder add estechnicianId int(10) default NULL ;
ALTER TABLE `joborder` ADD FOREIGN KEY ( estechnicianId ) REFERENCES employmentservicestechnician( estechnicianId );
*/
@Entity
@Table(name="employmentservicestechnician")
public class EmploymentServicesTechnician {
	
	@Column(name="estechnicianId")
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private Integer employmentservicestechnicianId;
	@Column(name="primaryESTech" ,columnDefinition="Primary Employement Service Technician")
	private String  primaryESTech;
	@Column(name="eSBackupId",length=10,unique=true)
	private Integer esBackupId;
	@Column(name="backupESTech",columnDefinition="Backup Employement Service Technician")
	private String backupESTech;
	@Column(name="employeeId",length=10)
	private Integer employeeId;
	@Column(name="primaryEmailAddress")
	private String primaryEmailAddress;
	@Column(name="backupEmailAddress")
	private String backupEmailAddress;
	
	public String getPrimaryEmailAddress() {
		return primaryEmailAddress;
	}
	public void setPrimaryEmailAddress(String primaryEmailAddress) {
		this.primaryEmailAddress = primaryEmailAddress;
	}
	public String getBackupEmailAddress() {
		return backupEmailAddress;
	}
	public void setBackupEmailAddress(String backupEmailAddress) {
		this.backupEmailAddress = backupEmailAddress;
	}
	public Integer getEmploymentservicestechnicianId() {
		return employmentservicestechnicianId;
	}
	public void setEmploymentservicestechnicianId(
			Integer employmentservicestechnicianId) {
		this.employmentservicestechnicianId = employmentservicestechnicianId;
	}
	public String getPrimaryESTech() {
		return primaryESTech;
	}
	public void setPrimaryESTech(String primaryESTech) {
		this.primaryESTech = primaryESTech;
	}
	public Integer getEsBackupId() {
		return esBackupId;
	}
	public void setEsBackupId(Integer esBackupId) {
		this.esBackupId = esBackupId;
	}
	public String getBackupESTech() {
		return backupESTech;
	}
	public void setBackupESTech(String backupESTech) {
		this.backupESTech = backupESTech;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	@Override
	public String toString() {
		return ""+employeeId;
	}
	
	
	
	
}
