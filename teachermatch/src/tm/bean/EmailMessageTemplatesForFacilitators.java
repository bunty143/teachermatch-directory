package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
@Entity
@Table(name="emailmessagetemplatesforfacilitators")
public class EmailMessageTemplatesForFacilitators implements Serializable
 {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5289291697287980900L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Integer emailMessageTemplatesForFacilitatorsId;
	
	private	String templateName;
	private String  status;
	private String  templateBody;
	private String subjectLine;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBY;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster distritMaster;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;

	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	private Date createdDateTime;

	public Integer getEmailMessageTemplatesForFacilitatorsId() {
		return emailMessageTemplatesForFacilitatorsId;
	}

	public void setEmailMessageTemplatesForFacilitatorsId(
			Integer emailMessageTemplatesForFacilitatorsId) {
		this.emailMessageTemplatesForFacilitatorsId = emailMessageTemplatesForFacilitatorsId;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTemplateBody() {
		return templateBody;
	}

	public void setTemplateBody(String templateBody) {
		this.templateBody = templateBody;
	}

	public String getSubjectLine() {
		return subjectLine;
	}

	public void setSubjectLine(String subjectLine) {
		this.subjectLine = subjectLine;
	}

	public UserMaster getCreatedBY() {
		return createdBY;
	}

	public void setCreatedBY(UserMaster createdBY) {
		this.createdBY = createdBY;
	}

	public DistrictMaster getDistritMaster() {
		return distritMaster;
	}

	public void setDistritMaster(DistrictMaster distritMaster) {
		this.distritMaster = distritMaster;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	
}
