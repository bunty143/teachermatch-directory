package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="usersupport")
public class UserSupport implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 834156015748538366L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer supportId;
	private String userFirstName;
	private String userLastName;
	private String userEmail;
	private String userContact;
	private String userType;
	private String userRole;
	private String subject;
	private String message;
	private String uploadedFileURL;
	private String visitedPageTitle;
	private String visitedPageURL;
	private String ipAddress;
	private Date createdDateTime;
	public Integer getSupportId() {
		return supportId;
	}
	public void setSupportId(Integer supportId) {
		this.supportId = supportId;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUploadedFileURL() {
		return uploadedFileURL;
	}
	public void setUploadedFileURL(String uploadedFileURL) {
		this.uploadedFileURL = uploadedFileURL;
	}
	public String getVisitedPageURL() {
		return visitedPageURL;
	}
	public void setVisitedPageURL(String visitedPageURL) {
		this.visitedPageURL = visitedPageURL;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getUserContact() {
		return userContact;
	}
	public void setUserContact(String userContact) {
		this.userContact = userContact;
	}
	public String getVisitedPageTitle() {
		return visitedPageTitle;
	}
	public void setVisitedPageTitle(String visitedPageTitle) {
		this.visitedPageTitle = visitedPageTitle;
	}
	
	
}
