package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="eventparticipantstemp")
public class EventParticipantsTemp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4413823141364046473L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer eventParticipantTempId;
	
	private String participantFirstName;
	private String participantLastName;
	private String participantEmailAddress;
	private String tempId;
	private String error;
	
	
	public Integer getEventParticipantTempId() {
		return eventParticipantTempId;
	}

	public void setEventParticipantTempId(Integer eventParticipantTempId) {
		this.eventParticipantTempId = eventParticipantTempId;
	}

	public String getParticipantFirstName() {
		return participantFirstName;
	}

	public void setParticipantFirstName(String participantFirstName) {
		this.participantFirstName = participantFirstName;
	}

	public String getParticipantLastName() {
		return participantLastName;
	}

	public void setParticipantLastName(String participantLastName) {
		this.participantLastName = participantLastName;
	}

	public String getParticipantEmailAddress() {
		return participantEmailAddress;
	}

	public void setParticipantEmailAddress(String participantEmailAddress) {
		this.participantEmailAddress = participantEmailAddress;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
}
