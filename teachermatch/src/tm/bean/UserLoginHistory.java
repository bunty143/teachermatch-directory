package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CurrencyMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.EmpRoleTypeMaster;
import tm.bean.master.FieldMaster;
import tm.bean.master.FieldOfStudyMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.utility.Utility;

@Entity
@Table(name="userloginhistory")
public class UserLoginHistory implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2183059643315932679L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;     
	
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userMaster;	
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtId;     

	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolId;  
	
	private String sessionId;
	private String logType;
	private Date logTime;
	private String ipAddress;
	private String serverName;
	private String loginPasswordType;
	
	public DistrictMaster getDistrictId() {
		return districtId;
	}
	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}
	public SchoolMaster getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(SchoolMaster schoolId) {
		this.schoolId = schoolId;
	}
	public Date getLogTime() {
		return logTime;
	}
	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	public String getLogType() {
		return logType;
	}
	public void setLogType(String logType) {
		this.logType = logType;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	public String getServerName() {
		return serverName;
	}
	/**
	 * @param loginPasswordType the loginPasswordType to set
	 */
	public void setLoginPasswordType(String loginPasswordType) {
		this.loginPasswordType = loginPasswordType;
	}
	/**
	 * @return the loginPasswordType
	 */
	public String getLoginPasswordType() {
		return loginPasswordType;
	}	
	
}
