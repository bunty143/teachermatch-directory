package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="adminactivitydetail")
public class AdminActivityDetail implements Serializable{

	/**
	 * Author : Anurag Kumar
	 * Date : 21/9/15
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer adminactivitydetail_id;
	
	@Column(name="headquarter_id")
	private Integer headQuarterId;
	
	@Column(name="branch_id")
	private Integer branchId;
	
	@Column(name="district_id")
	private Integer districtId;
	
/*	@Column(name="user_id")
	private Integer userId;
*/	
	@ManyToOne
	@JoinColumn(name="user_id",referencedColumnName="userId")
	private UserMaster userMaster;
	
	
	@Column(name="entyty_id")
	private Integer entityId;
	
	@Column(name="created_dt")
	private Date createdDate;
	
	@Column(name="entitytype")
	private String entityType;
	
	@Column(name="entitytable")
	private String entityTable;
	
	@Column(name="activitytype")
	private String activityType;
	
	private String description;

	public Integer getHeadQuarterId() {
		return headQuarterId;
	}

	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}


	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getEntityTable() {
		return entityTable;
	}

	public void setEntityTable(String entityTable) {
		this.entityTable = entityTable;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
