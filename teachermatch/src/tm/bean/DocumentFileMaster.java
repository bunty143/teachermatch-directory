package tm.bean;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="documentfilemaster")
public class DocumentFileMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2114672526169052198L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer documentFileId;
	
	@ManyToOne
	@JoinColumn(name="documentCategoryId",referencedColumnName="documentCategoryId")
	private DocumentCategoryMaster documentCategoryMaster;
	
	private String fileName;

	public Integer getDocumentFileId() {
		return documentFileId;
	}

	public void setDocumentFileId(Integer documentFileId) {
		this.documentFileId = documentFileId;
	}

	public DocumentCategoryMaster getDocumentCategoryMaster() {
		return documentCategoryMaster;
	}

	public void setDocumentCategoryMaster(
			DocumentCategoryMaster documentCategoryMaster) {
		this.documentCategoryMaster = documentCategoryMaster;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	

}
