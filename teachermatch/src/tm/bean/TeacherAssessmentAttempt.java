package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import tm.bean.assessment.AssessmentDetail;

@Entity(name="teacherassessmentattempt")
public class TeacherAssessmentAttempt implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8038421523867982309L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer attemptId;

	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentDetail;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	private Date assessmentStartTime;
	private Date assessmentEndTime;
	private Integer assessmentSessionTime;
	private Boolean isForced;
	private String ipAddress;
	private Integer assessmentTakenCount;
	@ManyToOne
	@JoinColumn(name="teacherAssessmentId",referencedColumnName="teacherAssessmentId")
	private TeacherAssessmentdetail teacherAssessmentdetail;
	
	public Integer getAttemptId() {
		return attemptId;
	}
	public void setAttemptId(Integer attemptId) {
		this.attemptId = attemptId;
	}
	
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	
	public AssessmentDetail getAssessmentDetail() {
		return assessmentDetail;
	}
	public void setAssessmentDetail(AssessmentDetail assessmentDetail) {
		this.assessmentDetail = assessmentDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public Date getAssessmentStartTime() {
		return assessmentStartTime;
	}
	public void setAssessmentStartTime(Date assessmentStartTime) {
		this.assessmentStartTime = assessmentStartTime;
	}
	public Date getAssessmentEndTime() {
		return assessmentEndTime;
	}
	public void setAssessmentEndTime(Date assessmentEndTime) {
		this.assessmentEndTime = assessmentEndTime;
	}
	public Integer getAssessmentSessionTime() {
		return assessmentSessionTime;
	}
	public void setAssessmentSessionTime(Integer assessmentSessionTime) {
		this.assessmentSessionTime = assessmentSessionTime;
	}
	public Boolean getIsForced() {
		return isForced;
	}
	public void setIsForced(Boolean isForced) {
		this.isForced = isForced;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Integer getAssessmentTakenCount() {
		return assessmentTakenCount;
	}
	public void setAssessmentTakenCount(Integer assessmentTakenCount) {
		this.assessmentTakenCount = assessmentTakenCount;
	}
	public TeacherAssessmentdetail getTeacherAssessmentdetail() {
		return teacherAssessmentdetail;
	}
	public void setTeacherAssessmentdetail(TeacherAssessmentdetail teacherAssessmentdetail) {
		this.teacherAssessmentdetail = teacherAssessmentdetail;
	}
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof TeacherAssessmentAttempt))return false;
	    TeacherAssessmentAttempt teacherAssessmentAttempt = (TeacherAssessmentAttempt)object;
	  
	    if(this.attemptId.equals(teacherAssessmentAttempt.getAttemptId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+attemptId);
	}
	
}
