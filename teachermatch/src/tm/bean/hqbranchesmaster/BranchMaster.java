package tm.bean.hqbranchesmaster;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.StateMaster;
import tm.bean.master.TimeZoneMaster;

@Entity
@Table(name="branchmaster")
public class BranchMaster implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7571770660905673150L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer branchId;
	private String branchName;
	private String branchBusinessName;
	private String branchDisplayName;
	private String branchEmailAddress;
	@ManyToOne
	@JoinColumn(name="timeZoneId",referencedColumnName="timeZoneId")
	private TimeZoneMaster timeZoneMaster;
	private String branchCode;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	@ManyToOne
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private StateMaster stateMaster;
	private String cityName;
	private String authKey;
	private String exitURL;
	private String websiteUrl;
	private String apiRedirectUrl;
	private String locationCode;
	private String phoneNumber;
	private String contactNumber1;
	private String contactNumber2;
	private String faxNumber;
	private String address; 
	private String zipCode; 
	private String type;                                             
	private String latcod;     
	private String loncod;
	
	private int jobApplicationCriteriaForProspects;
	private Boolean displayTMDefaultJobCategory;
	private Boolean areAllDistrictsInContract;
	private Boolean writePrivilegeToDistrict;
	private Boolean approvalBeforeGoLive;
	private Boolean autoNotifyCandidateOnAttachingWithJob;
	private Integer postingOnHQWall;
	private Integer postingOnTMWall;
	private String assessmentUploadURL;
	private Integer canTMApproach;
	private Integer allowMessageTeacher;
	private String hiringAuthority;
	private Boolean hQApproval;
	private Boolean isPortfolioNeeded;
	private Integer isWeeklyCgReport;
	private String emailForTeacher;
	
	private Integer totalNoOfDistricts;	
	private Integer districtsUnderContract;
	private Integer noDistrictUnderContract;    
	private Integer allDistrictsUnderContract;
	private Integer selectedDistrictUnderContract;
	
	private Date contractStartDate;     
	private Date contractEndDate;
	private Double annualSubsciptionAmount;
	
	private String logoPath;
	private CommonsMultipartFile 	logoPathFile;
	private CommonsMultipartFile 	assessmentUploadURLFile;
	
	private Integer candidateFeedNormScore;
	private String distributionEmail;
	private Integer candidateFeedDaysOfNoActivity;
	private Boolean canBranchOverrideCandidateFeed;	
	private Boolean statusPrivilegeForBranches;	
	
	private Integer jobFeedCriticalJobActiveDays;
	private Integer jobFeedAttentionJobActiveDays;
	private Integer jobFeedCriticalCandidateRatio;
	private Boolean jobFeedAttentionJobNotFilled;
	
	/*@ManyToOne
	@JoinColumn(name="statusIdForBranchPrivilege",referencedColumnName="statusId")
	private StatusMaster statusMaster;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusIdForBranchPrivilege",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;*/
	
	private Integer communicationsAccess;
	
	
	private Boolean displayCGPA;
	private Boolean displayAchievementScore;
	private Boolean displayTFA;
	private Boolean displayDemoClass;
	private Boolean displayJSI;
	private Boolean displayYearsTeaching;
	private Boolean displayExpectedSalary;
	private Boolean displayFitScore;
	private Boolean displayPhoneInterview;
	
	private String textForDistrictSpecificQuestions;
	
	private Boolean offerDistrictSpecificItems;
	private Boolean offerQualificationItems;
	private Boolean offerEPI;
	private Boolean offerJSI;
	private Boolean offerPortfolioNeeded;
	private Boolean offerVirtualVideoInterview;
	private Boolean offerAssessmentInviteOnly;
	
	@ManyToOne
	@JoinColumn(name="vviQuestionSet",referencedColumnName="ID")
	private I4QuestionSets i4QuestionSets;
	private Integer maxScoreForVVI;
	private Boolean sendAutoVVILink;
	private Integer timeAllowedPerQuestion;
	private Integer VVIExpiresInDays;
	private String completionMessage;
	
	/*@ManyToOne
	@JoinColumn(name="statusIdForAutoVVILink",referencedColumnName="statusId")
	private StatusMaster statusMasterForVVI;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusIdForAutoVVILink",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatusForVVI;
	
	@ManyToOne
	@JoinColumn(name="candidateConsiderationStatusId",referencedColumnName="statusId")
	private StatusMaster statusMasterForCC;
	
	@ManyToOne
	@JoinColumn(name="candidateConsiderationSecondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatusForCC;*/
	
	private Boolean isTeacherPoolOnLoad;
	private Boolean noEPI;
	private String status;
	private Date createdDateTime;
	
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public StateMaster getStateMaster() {
		return stateMaster;
	}
	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchBusinessName() {
		return branchBusinessName;
	}
	public void setBranchBusinessName(String branchBusinessName) {
		this.branchBusinessName = branchBusinessName;
	}
	public String getBranchDisplayName() {
		return branchDisplayName;
	}
	public void setBranchDisplayName(String branchDisplayName) {
		this.branchDisplayName = branchDisplayName;
	}
	public String getBranchEmailAddress() {
		return branchEmailAddress;
	}
	public void setBranchEmailAddress(String branchEmailAddress) {
		this.branchEmailAddress = branchEmailAddress;
	}
	public TimeZoneMaster getTimeZoneMaster() {
		return timeZoneMaster;
	}
	public void setTimeZoneMaster(TimeZoneMaster timeZoneMaster) {
		this.timeZoneMaster = timeZoneMaster;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	public String getExitURL() {
		return exitURL;
	}
	public void setExitURL(String exitURL) {
		this.exitURL = exitURL;
	}
	public String getWebsiteUrl() {
		return websiteUrl;
	}
	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}
	public String getApiRedirectUrl() {
		return apiRedirectUrl;
	}
	public void setApiRedirectUrl(String apiRedirectUrl) {
		this.apiRedirectUrl = apiRedirectUrl;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getContactNumber1() {
		return contactNumber1;
	}
	public void setContactNumber1(String contactNumber1) {
		this.contactNumber1 = contactNumber1;
	}
	public String getContactNumber2() {
		return contactNumber2;
	}
	public void setContactNumber2(String contactNumber2) {
		this.contactNumber2 = contactNumber2;
	}
	
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLatcod() {
		return latcod;
	}
	public void setLatcod(String latcod) {
		this.latcod = latcod;
	}
	public String getLoncod() {
		return loncod;
	}
	public void setLoncod(String loncod) {
		this.loncod = loncod;
	}
	public int getJobApplicationCriteriaForProspects() {
		return jobApplicationCriteriaForProspects;
	}
	public void setJobApplicationCriteriaForProspects(
			int jobApplicationCriteriaForProspects) {
		this.jobApplicationCriteriaForProspects = jobApplicationCriteriaForProspects;
	}
	public Boolean getDisplayTMDefaultJobCategory() {
		return displayTMDefaultJobCategory;
	}
	public void setDisplayTMDefaultJobCategory(Boolean displayTMDefaultJobCategory) {
		this.displayTMDefaultJobCategory = displayTMDefaultJobCategory;
	}
	public Boolean getAreAllDistrictsInContract() {
		return areAllDistrictsInContract;
	}
	public void setAreAllDistrictsInContract(Boolean areAllDistrictsInContract) {
		this.areAllDistrictsInContract = areAllDistrictsInContract;
	}
	public Boolean getWritePrivilegeToDistrict() {
		return writePrivilegeToDistrict;
	}
	public void setWritePrivilegeToDistrict(Boolean writePrivilegeToDistrict) {
		this.writePrivilegeToDistrict = writePrivilegeToDistrict;
	}
	public Boolean getApprovalBeforeGoLive() {
		return approvalBeforeGoLive;
	}
	public void setApprovalBeforeGoLive(Boolean approvalBeforeGoLive) {
		this.approvalBeforeGoLive = approvalBeforeGoLive;
	}
	public Boolean getAutoNotifyCandidateOnAttachingWithJob() {
		return autoNotifyCandidateOnAttachingWithJob;
	}
	public void setAutoNotifyCandidateOnAttachingWithJob(
			Boolean autoNotifyCandidateOnAttachingWithJob) {
		this.autoNotifyCandidateOnAttachingWithJob = autoNotifyCandidateOnAttachingWithJob;
	}
	public Integer getPostingOnHQWall() {
		return postingOnHQWall;
	}
	public void setPostingOnHQWall(Integer postingOnHQWall) {
		this.postingOnHQWall = postingOnHQWall;
	}
	public Integer getPostingOnTMWall() {
		return postingOnTMWall;
	}
	public void setPostingOnTMWall(Integer postingOnTMWall) {
		this.postingOnTMWall = postingOnTMWall;
	}
	public String getAssessmentUploadURL() {
		return assessmentUploadURL;
	}
	public void setAssessmentUploadURL(String assessmentUploadURL) {
		this.assessmentUploadURL = assessmentUploadURL;
	}
	public Integer getCanTMApproach() {
		return canTMApproach;
	}
	public void setCanTMApproach(Integer canTMApproach) {
		this.canTMApproach = canTMApproach;
	}
	public Integer getAllowMessageTeacher() {
		return allowMessageTeacher;
	}
	public void setAllowMessageTeacher(Integer allowMessageTeacher) {
		this.allowMessageTeacher = allowMessageTeacher;
	}
	public String getHiringAuthority() {
		return hiringAuthority;
	}
	public void setHiringAuthority(String hiringAuthority) {
		this.hiringAuthority = hiringAuthority;
	}
	
	public Boolean gethQApproval() {
		return hQApproval;
	}
	public void sethQApproval(Boolean hQApproval) {
		this.hQApproval = hQApproval;
	}
	public Boolean getIsPortfolioNeeded() {
		return isPortfolioNeeded;
	}
	public void setIsPortfolioNeeded(Boolean isPortfolioNeeded) {
		this.isPortfolioNeeded = isPortfolioNeeded;
	}
	public Integer getIsWeeklyCgReport() {
		return isWeeklyCgReport;
	}
	public void setIsWeeklyCgReport(Integer isWeeklyCgReport) {
		this.isWeeklyCgReport = isWeeklyCgReport;
	}
	public String getEmailForTeacher() {
		return emailForTeacher;
	}
	public void setEmailForTeacher(String emailForTeacher) {
		this.emailForTeacher = emailForTeacher;
	}
	public Integer getTotalNoOfDistricts() {
		return totalNoOfDistricts;
	}
	public void setTotalNoOfDistricts(Integer totalNoOfDistricts) {
		this.totalNoOfDistricts = totalNoOfDistricts;
	}
	public Integer getDistrictsUnderContract() {
		return districtsUnderContract;
	}
	public void setDistrictsUnderContract(Integer districtsUnderContract) {
		this.districtsUnderContract = districtsUnderContract;
	}
	public Integer getNoDistrictUnderContract() {
		return noDistrictUnderContract;
	}
	public void setNoDistrictUnderContract(Integer noDistrictUnderContract) {
		this.noDistrictUnderContract = noDistrictUnderContract;
	}
	public Integer getAllDistrictsUnderContract() {
		return allDistrictsUnderContract;
	}
	public void setAllDistrictsUnderContract(Integer allDistrictsUnderContract) {
		this.allDistrictsUnderContract = allDistrictsUnderContract;
	}
	public Integer getSelectedDistrictUnderContract() {
		return selectedDistrictUnderContract;
	}
	public void setSelectedDistrictUnderContract(
			Integer selectedDistrictUnderContract) {
		this.selectedDistrictUnderContract = selectedDistrictUnderContract;
	}
	public Date getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public Date getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public Double getAnnualSubsciptionAmount() {
		return annualSubsciptionAmount;
	}
	public void setAnnualSubsciptionAmount(Double annualSubsciptionAmount) {
		this.annualSubsciptionAmount = annualSubsciptionAmount;
	}
	public String getLogoPath() {
		return logoPath;
	}
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}
	public CommonsMultipartFile getLogoPathFile() {
		return logoPathFile;
	}
	public void setLogoPathFile(CommonsMultipartFile logoPathFile) {
		this.logoPathFile = logoPathFile;
	}
	public CommonsMultipartFile getAssessmentUploadURLFile() {
		return assessmentUploadURLFile;
	}
	public void setAssessmentUploadURLFile(
			CommonsMultipartFile assessmentUploadURLFile) {
		this.assessmentUploadURLFile = assessmentUploadURLFile;
	}
	public Integer getCandidateFeedNormScore() {
		return candidateFeedNormScore;
	}
	public void setCandidateFeedNormScore(Integer candidateFeedNormScore) {
		this.candidateFeedNormScore = candidateFeedNormScore;
	}
	public String getDistributionEmail() {
		return distributionEmail;
	}
	public void setDistributionEmail(String distributionEmail) {
		this.distributionEmail = distributionEmail;
	}
	public Integer getCandidateFeedDaysOfNoActivity() {
		return candidateFeedDaysOfNoActivity;
	}
	public void setCandidateFeedDaysOfNoActivity(
			Integer candidateFeedDaysOfNoActivity) {
		this.candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity;
	}
	public Boolean getCanBranchOverrideCandidateFeed() {
		return canBranchOverrideCandidateFeed;
	}
	public void setCanBranchOverrideCandidateFeed(
			Boolean canBranchOverrideCandidateFeed) {
		this.canBranchOverrideCandidateFeed = canBranchOverrideCandidateFeed;
	}
	public Boolean getStatusPrivilegeForBranches() {
		return statusPrivilegeForBranches;
	}
	public void setStatusPrivilegeForBranches(Boolean statusPrivilegeForBranches) {
		this.statusPrivilegeForBranches = statusPrivilegeForBranches;
	}
	public Integer getCommunicationsAccess() {
		return communicationsAccess;
	}
	public void setCommunicationsAccess(Integer communicationsAccess) {
		this.communicationsAccess = communicationsAccess;
	}
	public Boolean getDisplayCGPA() {
		return displayCGPA;
	}
	public void setDisplayCGPA(Boolean displayCGPA) {
		this.displayCGPA = displayCGPA;
	}
	public Boolean getDisplayAchievementScore() {
		return displayAchievementScore;
	}
	public void setDisplayAchievementScore(Boolean displayAchievementScore) {
		this.displayAchievementScore = displayAchievementScore;
	}
	public Boolean getDisplayTFA() {
		return displayTFA;
	}
	public void setDisplayTFA(Boolean displayTFA) {
		this.displayTFA = displayTFA;
	}
	public Boolean getDisplayDemoClass() {
		return displayDemoClass;
	}
	public void setDisplayDemoClass(Boolean displayDemoClass) {
		this.displayDemoClass = displayDemoClass;
	}
	public Boolean getDisplayJSI() {
		return displayJSI;
	}
	public void setDisplayJSI(Boolean displayJSI) {
		this.displayJSI = displayJSI;
	}
	public Boolean getDisplayYearsTeaching() {
		return displayYearsTeaching;
	}
	public void setDisplayYearsTeaching(Boolean displayYearsTeaching) {
		this.displayYearsTeaching = displayYearsTeaching;
	}
	public Boolean getDisplayExpectedSalary() {
		return displayExpectedSalary;
	}
	public void setDisplayExpectedSalary(Boolean displayExpectedSalary) {
		this.displayExpectedSalary = displayExpectedSalary;
	}
	public Boolean getDisplayFitScore() {
		return displayFitScore;
	}
	public void setDisplayFitScore(Boolean displayFitScore) {
		this.displayFitScore = displayFitScore;
	}
	public Boolean getDisplayPhoneInterview() {
		return displayPhoneInterview;
	}
	public void setDisplayPhoneInterview(Boolean displayPhoneInterview) {
		this.displayPhoneInterview = displayPhoneInterview;
	}
	public String getTextForDistrictSpecificQuestions() {
		return textForDistrictSpecificQuestions;
	}
	public void setTextForDistrictSpecificQuestions(
			String textForDistrictSpecificQuestions) {
		this.textForDistrictSpecificQuestions = textForDistrictSpecificQuestions;
	}
	public Boolean getOfferDistrictSpecificItems() {
		return offerDistrictSpecificItems;
	}
	public void setOfferDistrictSpecificItems(Boolean offerDistrictSpecificItems) {
		this.offerDistrictSpecificItems = offerDistrictSpecificItems;
	}
	public Boolean getOfferQualificationItems() {
		return offerQualificationItems;
	}
	public void setOfferQualificationItems(Boolean offerQualificationItems) {
		this.offerQualificationItems = offerQualificationItems;
	}
	public Boolean getOfferEPI() {
		return offerEPI;
	}
	public void setOfferEPI(Boolean offerEPI) {
		this.offerEPI = offerEPI;
	}
	public Boolean getOfferJSI() {
		return offerJSI;
	}
	public void setOfferJSI(Boolean offerJSI) {
		this.offerJSI = offerJSI;
	}
	public Boolean getOfferPortfolioNeeded() {
		return offerPortfolioNeeded;
	}
	public void setOfferPortfolioNeeded(Boolean offerPortfolioNeeded) {
		this.offerPortfolioNeeded = offerPortfolioNeeded;
	}
	public Boolean getOfferVirtualVideoInterview() {
		return offerVirtualVideoInterview;
	}
	public void setOfferVirtualVideoInterview(Boolean offerVirtualVideoInterview) {
		this.offerVirtualVideoInterview = offerVirtualVideoInterview;
	}
	public Boolean getOfferAssessmentInviteOnly() {
		return offerAssessmentInviteOnly;
	}
	public void setOfferAssessmentInviteOnly(Boolean offerAssessmentInviteOnly) {
		this.offerAssessmentInviteOnly = offerAssessmentInviteOnly;
	}
	public I4QuestionSets getI4QuestionSets() {
		return i4QuestionSets;
	}
	public void setI4QuestionSets(I4QuestionSets i4QuestionSets) {
		this.i4QuestionSets = i4QuestionSets;
	}
	public Integer getMaxScoreForVVI() {
		return maxScoreForVVI;
	}
	public void setMaxScoreForVVI(Integer maxScoreForVVI) {
		this.maxScoreForVVI = maxScoreForVVI;
	}
	public Boolean getSendAutoVVILink() {
		return sendAutoVVILink;
	}
	public void setSendAutoVVILink(Boolean sendAutoVVILink) {
		this.sendAutoVVILink = sendAutoVVILink;
	}
	public Integer getTimeAllowedPerQuestion() {
		return timeAllowedPerQuestion;
	}
	public void setTimeAllowedPerQuestion(Integer timeAllowedPerQuestion) {
		this.timeAllowedPerQuestion = timeAllowedPerQuestion;
	}
	public Integer getVVIExpiresInDays() {
		return VVIExpiresInDays;
	}
	public void setVVIExpiresInDays(Integer vVIExpiresInDays) {
		VVIExpiresInDays = vVIExpiresInDays;
	}
	/*public StatusMaster getStatusMasterForVVI() {
		return statusMasterForVVI;
	}
	public void setStatusMasterForVVI(StatusMaster statusMasterForVVI) {
		this.statusMasterForVVI = statusMasterForVVI;
	}
	public SecondaryStatus getSecondaryStatusForVVI() {
		return secondaryStatusForVVI;
	}
	public void setSecondaryStatusForVVI(SecondaryStatus secondaryStatusForVVI) {
		this.secondaryStatusForVVI = secondaryStatusForVVI;
	}
	public StatusMaster getStatusMasterForCC() {
		return statusMasterForCC;
	}
	public void setStatusMasterForCC(StatusMaster statusMasterForCC) {
		this.statusMasterForCC = statusMasterForCC;
	}
	public SecondaryStatus getSecondaryStatusForCC() {
		return secondaryStatusForCC;
	}
	public void setSecondaryStatusForCC(SecondaryStatus secondaryStatusForCC) {
		this.secondaryStatusForCC = secondaryStatusForCC;
	}*/
	public Boolean getIsTeacherPoolOnLoad() {
		return isTeacherPoolOnLoad;
	}
	public void setIsTeacherPoolOnLoad(Boolean isTeacherPoolOnLoad) {
		this.isTeacherPoolOnLoad = isTeacherPoolOnLoad;
	}
	public Boolean getNoEPI() {
		return noEPI;
	}
	public void setNoEPI(Boolean noEPI) {
		this.noEPI = noEPI;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}	
	public Integer getJobFeedCriticalJobActiveDays() {
		return jobFeedCriticalJobActiveDays;
	}
	public void setJobFeedCriticalJobActiveDays(Integer jobFeedCriticalJobActiveDays) {
		this.jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays;
	}
	public Integer getJobFeedAttentionJobActiveDays() {
		return jobFeedAttentionJobActiveDays;
	}
	public void setJobFeedAttentionJobActiveDays(
			Integer jobFeedAttentionJobActiveDays) {
		this.jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays;
	}
	public Integer getJobFeedCriticalCandidateRatio() {
		return jobFeedCriticalCandidateRatio;
	}
	public void setJobFeedCriticalCandidateRatio(
			Integer jobFeedCriticalCandidateRatio) {
		this.jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio;
	}
	public Boolean getJobFeedAttentionJobNotFilled() {
		return jobFeedAttentionJobNotFilled;
	}
	public void setJobFeedAttentionJobNotFilled(Boolean jobFeedAttentionJobNotFilled) {
		this.jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled;
	}
	
	public String getCompletionMessage() {
		return completionMessage;
	}
	public void setCompletionMessage(String completionMessage) {
		this.completionMessage = completionMessage;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj==null)return false;
		if(obj==this)return true;
		if(!(obj instanceof BranchMaster)) return false;
		BranchMaster branchMaster = (BranchMaster) obj;
		if(this.branchId.equals(branchMaster.branchId))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return this.branchId;
	}
	
	@Override
	public String toString() {
		//return "Branch Id :: "+this.branchId+" BranchName :: "+this.branchName;
		return this.branchName;
	}
	
	
	public static Comparator<BranchMaster> branchMasterComparatorAsc  = new Comparator<BranchMaster>(){
		public int compare(BranchMaster b1, BranchMaster b2) {			
			int c = b1.getBranchName().compareTo(b2.getBranchName());
			return c;
		}		
	};	 
}
