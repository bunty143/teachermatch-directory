package tm.bean.hqbranchesmaster;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="workflowstatus")
public class WorkFlowStatus implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static String DISCONNECT = "DISCONNECT";
	public static String UPDATE = "UPDATE";
	public static String INITIALIZE = "INITIALIZE";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer workFlowStatusId;
	
	private String workFlowStatus;
	private String status;
	private Date updatedDateTime;
	private Date createdDateTime;
	public String getWorkFlowStatus() {
		return workFlowStatus;
	}
	public void setWorkFlowStatus(String workFlowStatus) {
		this.workFlowStatus = workFlowStatus;
	}
	public Integer getWorkFlowStatusId() {
		return workFlowStatusId;
	}
	public void setWorkFlowStatusId(Integer workFlowStatusId) {
		this.workFlowStatusId = workFlowStatusId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
	
}
