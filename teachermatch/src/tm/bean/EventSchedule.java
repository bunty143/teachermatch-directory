package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.TimeZoneMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name = "eventschedule")
public class EventSchedule implements Serializable {
	private static final long serialVersionUID = 1121370875535282677L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer eventScheduleId;
	@ManyToOne
	@JoinColumn(name = "eventId", referencedColumnName = "eventId")
	private EventDetails eventDetails;
	private Date eventDateTime;
	private String eventStartTime;
	private String eventStartTimeFormat;
	private String eventEndTime;
	private String eventEndTimeFormat;
	private String status, location;
	private Integer validity;

	@ManyToOne
	@JoinColumn(name = "createdBy", referencedColumnName = "userId")
	private UserMaster createdBY;
	
	private Date createdDateTime;

	@ManyToOne
	@JoinColumn(name = "timeZoneId", referencedColumnName = "timeZoneId")
	private TimeZoneMaster timeZoneMaster;

	private Boolean singlebookingonly;
	
	private Integer noOfCandidatePerSlot;
	
	public Integer getValidity() {
		return validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getEventScheduleId() {
		return eventScheduleId;
	}

	public void setEventScheduleId(Integer eventScheduleId) {
		this.eventScheduleId = eventScheduleId;
	}

	public EventDetails getEventDetails() {
		return eventDetails;
	}

	public void setEventDetails(EventDetails eventDetails) {
		this.eventDetails = eventDetails;
	}

	public Date getEventDateTime() {
		return eventDateTime;
	}

	public void setEventDateTime(Date eventDateTime) {
		this.eventDateTime = eventDateTime;
	}

	public String getEventStartTime() {
		return eventStartTime;
	}

	public void setEventStartTime(String eventStartTime) {
		this.eventStartTime = eventStartTime;
	}

	public String getEventStartTimeFormat() {
		return eventStartTimeFormat;
	}

	public void setEventStartTimeFormat(String eventStartTimeFormat) {
		this.eventStartTimeFormat = eventStartTimeFormat;
	}

	public String getEventEndTime() {
		return eventEndTime;
	}

	public void setEventEndTime(String eventEndTime) {
		this.eventEndTime = eventEndTime;
	}

	public String getEventEndTimeFormat() {
		return eventEndTimeFormat;
	}

	public void setEventEndTimeFormat(String eventEndTimeFormat) {
		this.eventEndTimeFormat = eventEndTimeFormat;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public UserMaster getCreatedBY() {
		return createdBY;
	}

	public void setCreatedBY(UserMaster createdBY) {
		this.createdBY = createdBY;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public TimeZoneMaster getTimeZoneMaster() {
		return timeZoneMaster;
	}

	public void setTimeZoneMaster(TimeZoneMaster timeZoneMaster) {
		this.timeZoneMaster = timeZoneMaster;
	}

	public Boolean getSinglebookingonly() {
		return singlebookingonly;
	}

	public void setSinglebookingonly(Boolean singlebookingonly) {
		this.singlebookingonly = singlebookingonly;
	}

	public Integer getNoOfCandidatePerSlot() {
		return noOfCandidatePerSlot;
	}

	public void setNoOfCandidatePerSlot(Integer noOfCandidatePerSlot) {
		this.noOfCandidatePerSlot = noOfCandidatePerSlot;
	}

}
