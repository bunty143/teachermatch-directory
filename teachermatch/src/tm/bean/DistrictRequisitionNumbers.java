package tm.bean;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="districtrequisitionnumbers")
public class DistrictRequisitionNumbers implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6546427182556149175L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtRequisitionId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private String requisitionNumber;
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	private String postingNo;
	
	private String jobCode;
	private String jobTitle;
	private String posType;	
	private String status;
	private Boolean isUsed;
	
	private String deleteFlag;
	private String uploadFrom;
	@Transient
	private String isHired;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private Date createdDateTime;
	
	private String requisitionDescription;
	private String continuingOrTemporaryCode;
	private Date startDate;
	private Date endDate;
	private String payMethod;
	private String postionStatus;
	private String commDriverLicence;
	private Float postionTerm;
	private Float positionPercentFunded;
	private Float postionHoursWeekFunded;
	private Float standardHoursWeek;
	private String updateTimestamp;
	
	public String getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(String updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	private String requisitionTypeCode;
		
	public String getRequisitionTypeCode() {
		return requisitionTypeCode;
	}

	public void setRequisitionTypeCode(String requisitionTypeCode) {
		this.requisitionTypeCode = requisitionTypeCode;
	}

	public String getRequisitionDescription() {
		return requisitionDescription;
	}

	public void setRequisitionDescription(String requisitionDescription) {
		this.requisitionDescription = requisitionDescription;
	}

	public String getContinuingOrTemporaryCode() {
		return continuingOrTemporaryCode;
	}

	public void setContinuingOrTemporaryCode(String continuingOrTemporaryCode) {
		this.continuingOrTemporaryCode = continuingOrTemporaryCode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public String getPostionStatus() {
		return postionStatus;
	}

	public void setPostionStatus(String postionStatus) {
		this.postionStatus = postionStatus;
	}

	public String getCommDriverLicence() {
		return commDriverLicence;
	}

	public void setCommDriverLicence(String commDriverLicence) {
		this.commDriverLicence = commDriverLicence;
	}

	public Float getPostionTerm() {
		return postionTerm;
	}

	public void setPostionTerm(Float postionTerm) {
		this.postionTerm = postionTerm;
	}

	public Float getPositionPercentFunded() {
		return positionPercentFunded;
	}

	public void setPositionPercentFunded(Float positionPercentFunded) {
		this.positionPercentFunded = positionPercentFunded;
	}

	public Float getPostionHoursWeekFunded() {
		return postionHoursWeekFunded;
	}

	public void setPostionHoursWeekFunded(Float postionHoursWeekFunded) {
		this.postionHoursWeekFunded = postionHoursWeekFunded;
	}

	public Float getStandardHoursWeek() {
		return standardHoursWeek;
	}

	public void setStandardHoursWeek(Float standardHoursWeek) {
		this.standardHoursWeek = standardHoursWeek;
	}

	
 
	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public Integer getDistrictRequisitionId() {
		return districtRequisitionId;
	}

	public void setDistrictRequisitionId(Integer districtRequisitionId) {
		this.districtRequisitionId = districtRequisitionId;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public String getRequisitionNumber() {
		return requisitionNumber;
	}

	public void setRequisitionNumber(String requisitionNumber) {
		this.requisitionNumber = requisitionNumber;
	}

	public String getPostingNo() {
		return postingNo;
	}
	public void setPostingNo(String postingNo) {
		this.postingNo = postingNo;
	}
	
	public Boolean getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Boolean isUsed) {
		this.isUsed = isUsed;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getUploadFrom() {
		return uploadFrom;
	}

	public void setUploadFrom(String uploadFrom) {
		this.uploadFrom = uploadFrom;
	}

	public String getPosType() {
		return posType;
	}

	public void setPosType(String posType) {
		this.posType = posType;
	}

	public String getIsHired() {
		return isHired;
	}

	public void setIsHired(String isHired) {
		this.isHired = isHired;
	}

	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}

	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
