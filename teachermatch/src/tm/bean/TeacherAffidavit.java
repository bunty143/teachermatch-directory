package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="teacheraffidavit")
public class TeacherAffidavit implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 166040937841685740L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer affidavitId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherId;
	private Boolean affidavitAccepted;
	private Boolean isDone;
	private Date CreatedDateTime;
	
	public Integer getAffidavitId() {
		return affidavitId;
	}
	public void setAffidavitId(Integer affidavitId) {
		this.affidavitId = affidavitId;
	}
	public TeacherDetail getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}
	public Boolean getAffidavitAccepted() {
		return affidavitAccepted;
	}
	public void setAffidavitAccepted(Boolean affidavitAccepted) {
		this.affidavitAccepted = affidavitAccepted;
	}
	public Boolean getIsDone() {
		return isDone;
	}
	public void setIsDone(Boolean isDone) {
		this.isDone = isDone;
	}
	public Date getCreatedDateTime() {
		return CreatedDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		CreatedDateTime = createdDateTime;
	}
	
}
