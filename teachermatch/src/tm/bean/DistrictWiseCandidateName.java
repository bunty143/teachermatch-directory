package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="districtwisecandidatename")
public class DistrictWiseCandidateName implements Serializable
{
	/**
	 **/
	private static final long serialVersionUID = 4306781569935890215L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer nameId; 
	
	

	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtId;  
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherId;
	
	private String status;
	private Date createdDateTime;
	
	private String currentempjobtitle;
	private String currentemplocation;
	private String currentempyearinlocation;
	private String currentempsupervisor;
	
	private String formalempjobtitle;
	private Date DatesofEmployment;
	private String nameWhileEmployed;
		
	private Date  DateofRetirement;
	
	private String currentsubstitutejobtitle;
	private Integer employeeType;
	
	private String isApplicantHasBDegreeOrHigh;
	private Date updatedDateTime;

	
	public Integer getNameId() {
		return nameId;
	}
	public void setNameId(Integer nameId) {
		this.nameId = nameId;
	}
	public DistrictMaster getDistrictId() {
		return districtId;
	}
	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}
	public TeacherDetail getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getNameWhileEmployed() {
		return nameWhileEmployed;
	}
	public void setNameWhileEmployed(String nameWhileEmployed) {
		this.nameWhileEmployed = nameWhileEmployed;
	}
	public void setCurrentempjobtitle(String currentempjobtitle) {
		this.currentempjobtitle = currentempjobtitle;
	}
	public String getCurrentempjobtitle() {
		return currentempjobtitle;
	}
	public void setCurrentemplocation(String currentemplocation) {
		this.currentemplocation = currentemplocation;
	}
	public String getCurrentemplocation() {
		return currentemplocation;
	}
	public void setCurrentempyearinlocation(String currentempyearinlocation) {
		this.currentempyearinlocation = currentempyearinlocation;
	}
	public String getCurrentempyearinlocation() {
		return currentempyearinlocation;
	}
	public String getCurrentempsupervisor() {
		return currentempsupervisor;
	}
	public void setCurrentempsupervisor(String currentempsupervisor) {
		this.currentempsupervisor = currentempsupervisor;
	}
	
	public String getCurrentsubstitutejobtitle() {
		return currentsubstitutejobtitle;
	}
	public void setCurrentsubstitutejobtitle(String currentsubstitutejobtitle) {
		this.currentsubstitutejobtitle = currentsubstitutejobtitle;
	}
	
	public void setFormalempjobtitle(String formalempjobtitle) {
		this.formalempjobtitle = formalempjobtitle;
	}
	public String getFormalempjobtitle() {
		return formalempjobtitle;
	}
	public void setDatesofEmployment(Date datesofEmployment) {
		DatesofEmployment = datesofEmployment;
	}
	public Date getDatesofEmployment() {
		return DatesofEmployment;
	}
	public void setDateofRetirement(Date dateofRetirement) {
		DateofRetirement = dateofRetirement;
	}
	public Date getDateofRetirement() {
		return DateofRetirement;
	}
	public void setEmployeeType(Integer employeeType) {
		this.employeeType = employeeType;
	}
	public Integer getEmployeeType() {
		return employeeType;
	}
	public String getIsApplicantHasBDegreeOrHigh() {
		return isApplicantHasBDegreeOrHigh;
	}
	public void setIsApplicantHasBDegreeOrHigh(String isApplicantHasBDegreeOrHigh) {
		this.isApplicantHasBDegreeOrHigh = isApplicantHasBDegreeOrHigh;
	}
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
}
