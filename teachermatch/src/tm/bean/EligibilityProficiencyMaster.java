package tm.bean;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.DistrictMaster;
import tm.bean.master.EligibilityMaster;


@Entity
@Table(name="eligibilityproficiencymaster")
public class EligibilityProficiencyMaster implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4632400556872433389L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int eligibilityProficiencyId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster district_id;
	
	@ManyToOne
	@JoinColumn(name="eligibilityId",referencedColumnName="eligibilityId")
	private EligibilityMaster eligibility_id;

	public int getEligibilityProficiencyId() {
		return eligibilityProficiencyId;
	}

	public void setEligibilityProficiencyId(int eligibilityProficiencyId) {
		this.eligibilityProficiencyId = eligibilityProficiencyId;
	}

	public DistrictMaster getDistrict_id() {
		return district_id;
	}

	public void setDistrict_id(DistrictMaster districtId) {
		district_id = districtId;
	}

	public EligibilityMaster getEligibility_id() {
		return eligibility_id;
	}

	public void setEligibility_id(EligibilityMaster eligibilityId) {
		eligibility_id = eligibilityId;
	}

}
