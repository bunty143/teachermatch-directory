package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="candidateeventdetails")
public class CandidateEventDetails implements Serializable
 {
	private static final long serialVersionUID = 6022927839312836391L;
	 
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Integer candidateeventdetailsId;
	
	@ManyToOne
	@JoinColumn(name="eventId",referencedColumnName="eventId")
	private EventDetails eventDetails;
	
	private Integer districtId;
	private Integer headQuartetId;
	private Integer branchId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	private String firstName;
	
	private String lastName;
	
	private String emailAddress;

	private String ipAddress;
	
	private Date createdDateTime;
	
	public Integer getCandidateeventdetailsId() {
		return candidateeventdetailsId;
	}

	public void setCandidateeventdetailsId(Integer candidateeventdetailsId) {
		this.candidateeventdetailsId = candidateeventdetailsId;
	}

	public EventDetails getEventDetails() {
		return eventDetails;
	}

	public void setEventDetails(EventDetails eventDetails) {
		this.eventDetails = eventDetails;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getHeadQuartetId() {
		return headQuartetId;
	}

	public void setHeadQuartetId(Integer headQuartetId) {
		this.headQuartetId = headQuartetId;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	
}
