package tm.bean;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="jobforteacher")
public class JobForTeacher implements Comparable<JobForTeacher>,Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8706871327486982833L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long jobForTeacherId;  
	
	private Integer districtId;

	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherId;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobId;     
	private String redirectedFromURL;
	private String jobBoardReferralURL;
	
	@ManyToOne
	@JoinColumn(name="status",referencedColumnName="statusId")
	private StatusMaster status; 
	
	@OneToOne(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name="jftId",referencedColumnName="jobForTeacherId")
	private JFTWiseDetails jFTWiseDetails;
	
	
	private Boolean cgUpdated;	
	private String coverLetter;
	private String coverLetterFileName;
	private Integer isAffilated;
	private String staffType;
	private Boolean flagForDistrictSpecificQuestions;
	private Boolean flagForDistrictSpecificQuestionsONB;
	private String noteForDistrictSpecificQuestionsONB;
	private Boolean isDistrictSpecificNoteFinalizeONB;
	private Date districtSpecificNoteFinalizeONBDate;
	@ManyToOne
	@JoinColumn(name="districtSpecificNoteFinalizeONBBy",referencedColumnName="userId")
	private UserMaster districtSpecificNoteFinalizeONBBy;

	@ManyToOne
	@JoinColumn(name="updatedBy",referencedColumnName="userId")
	private UserMaster updatedBy; 
	private Integer updatedByEntity; 
	private Date updatedDate; 
	private Date createdDateTime;
	private String lastActivity;
	private Date lastActivityDate;
	private String noteForDistrictSpecificQuestions;
	private Boolean isDistrictSpecificNoteFinalize;
	private Date districtSpecificNoteFinalizeDate;
	private Boolean flagForDspq;
	private Integer noOfReminderSent;
	private Date reminderSentDate;
	private Boolean isImCandidate;
	private Date jobCompleteDate;
	private Integer noofTries;
	private Date hiredByDate;
	private Date startDate;
	private Boolean branchcontact;
	private Boolean vviMailSendFlag;
	private Boolean notReviewedFlag;
	private Boolean sendMail;
	private String isApplicantHasBDegreeOrHigh;
	private Integer applicationStatus; 
	//Added by kumar avinash
 
	@Transient
	private String firstName="";	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name="withdrawnDateTime")
	private Date withdrwanDateTime;
	
	public Date getWithdrwanDateTime() {
		return withdrwanDateTime;
	}
	public void setWithdrwanDateTime(Date withdrwanDateTime) {
		this.withdrwanDateTime = withdrwanDateTime;
	}
	public Boolean getBranchcontact() {
		return branchcontact;
	}
	public void setBranchcontact(Boolean branchcontact) {
		this.branchcontact = branchcontact;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getHiredByDate() {
		return hiredByDate;
	}
	public void setHiredByDate(Date hiredByDate) {
		this.hiredByDate = hiredByDate;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	@ManyToOne
	@JoinColumn(name="districtSpecificNoteFinalizeBy",referencedColumnName="userId")
	private UserMaster districtSpecificNoteFinalizeBy;
		
	public UserMaster getDistrictSpecificNoteFinalizeBy() {
		return districtSpecificNoteFinalizeBy;
	}
	public void setDistrictSpecificNoteFinalizeBy(
			UserMaster districtSpecificNoteFinalizeBy) {
		this.districtSpecificNoteFinalizeBy = districtSpecificNoteFinalizeBy;
	}

	@ManyToOne
	@JoinColumn(name="lastActivityDoneBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	@Transient
	private TeacherAssessmentStatus teacherAssessmentStatus;
	
	@Transient
	private String assessmentStatus;
	
	@Transient
	private Double compositeScore = -0.0;
	
	@Transient
	private Double normScore = -0.0;
	
	@Transient
	private Double CGPA= -0.0;
	
	@Transient
	String schoolName;
	
	@Transient
	private Integer aScore;
	
	@Transient
	private Integer assScore1;
	
	@Transient
	private Integer assScore2;
	
	@Transient
	private Integer assScore3;
	
	@Transient
	private Integer assScore4;
	
	@Transient
	private Integer assScore5;
		
	@Transient
	private Integer lRScore;
	
	@Transient
	private Double fitScore;
	
	@Transient
	private String  distName;
	
	@Transient
	private String  stafferName;
	
	@Transient
	private Integer  NF;
	
	@Transient
	private Integer  MA;
	
	@Transient
	private String  HNS;
	
	@Transient
	private Integer  VVI;
	
	
	@ManyToOne
	@JoinColumn(name="displayStatusId",referencedColumnName="statusId")
	private StatusMaster statusMaster;
	
	@ManyToOne
	@JoinColumn(name="displaySecondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;
	
	@ManyToOne
	@JoinColumn(name="internalStatus",referencedColumnName="statusId")
	private StatusMaster internalStatus; 
	
	@ManyToOne
	@JoinColumn(name="internalSecondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus internalSecondaryStatus;
	
	@ManyToOne
	@JoinColumn(name="hiredBySchool",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	private String  requisitionNumber;
	
	@Transient
	private String sapStatus;
	
	@Transient
	private String lastName;
	
	@Transient
	private String jobTitle;
	
	@Transient
	private Integer totalCandidate;
	
	@Transient
	private Integer avlCandidate;
	
	private Boolean offerReady;
	
	private Date offerMadeDate;
	
	private Boolean offerAccepted;
	
	private Date offerAcceptedDate;
	
	private Boolean noResponseEmail;
	
	private Boolean candidateConsideration;
	
	@Transient
	private Date postDate;
	
	@Transient
	private Date endDate;
	
	@Transient
	private Date appsDate;
	
	@Transient
	private boolean statusChange=false;
	
	@Transient
	private boolean statusMasterChange=false;
		
	@Transient
	private Date hiredDate;
	
	@Transient
	private Double yote;
	
	@Transient
	private Integer onlineActivityScore1;
	
	@Transient
	private Integer onlineActivityScore2;
	
	@Transient
	private Integer spStatus;
	
	@Transient
	private Integer noOfRecord;
	
	@Transient
	private Integer epiNormScoreAvg;
	
	public Integer getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(Integer applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public Integer getEpiNormScoreAvg() {
		return epiNormScoreAvg;
	}
	public void setEpiNormScoreAvg(Integer epiNormScoreAvg) {
		this.epiNormScoreAvg = epiNormScoreAvg;
	}
	public Integer getNoOfRecord() {
		return noOfRecord;
	}
	public void setNoOfRecord(Integer noOfRecord) {
		this.noOfRecord = noOfRecord;
	}
	public Integer getSpStatus() {
		return spStatus;
	}
	public void setSpStatus(Integer spStatus) {
		this.spStatus = spStatus;
	}
	
	public Date getJobCompleteDate() {
		return jobCompleteDate;
	}
	public void setJobCompleteDate(Date jobCompleteDate) {
		this.jobCompleteDate = jobCompleteDate;
	}
	public Integer getOnlineActivityScore1() {
		return onlineActivityScore1;
	}
	public void setOnlineActivityScore1(Integer onlineActivityScore1) {
		this.onlineActivityScore1 = onlineActivityScore1;
	}
	public Integer getOnlineActivityScore2() {
		return onlineActivityScore2;
	}
	public void setOnlineActivityScore2(Integer onlineActivityScore2) {
		this.onlineActivityScore2 = onlineActivityScore2;
	}
	public Double getyote() {
		return yote;
	}
	public void setyote(Double yote) {
		this.yote = yote;
	}
	
	public Boolean getIsImCandidate() {
		return isImCandidate;
	}
	public void setIsImCandidate(Boolean isImCandidate) {
		this.isImCandidate = isImCandidate;
	}
	public Integer getAssScore1() {
		return assScore1;
	}
	public void setAssScore1(Integer assScore1) {
		this.assScore1 = assScore1;
	}
	public Integer getAssScore2() {
		return assScore2;
	}
	public void setAssScore2(Integer assScore2) {
		this.assScore2 = assScore2;
	}
	public Integer getAssScore3() {
		return assScore3;
	}
	public void setAssScore3(Integer assScore3) {
		this.assScore3 = assScore3;
	}
	public Integer getAssScore4() {
		return assScore4;
	}
	public void setAssScore4(Integer assScore4) {
		this.assScore4 = assScore4;
	}
	public Integer getAssScore5() {
		return assScore5;
	}
	public void setAssScore5(Integer assScore5) {
		this.assScore5 = assScore5;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Transient
	private String emailId;
	
	@Transient
	private String ssn;
	
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public Boolean getCandidateConsideration() {
		return candidateConsideration;
	}
	public void setCandidateConsideration(Boolean candidateConsideration) {
		this.candidateConsideration = candidateConsideration;
	}
	public Integer getVVI() {
		return VVI;
	}
	public void setVVI(Integer vVI) {
		VVI = vVI;
	}
	public Integer getNF() {
		return NF;
	}
	public void setNF(Integer nF) {
		NF = nF;
	}
	public Integer getMA() {
		return MA;
	}
	public void setMA(Integer mA) {
		MA = mA;
	}
	public String getHNS() {
		return HNS;
	}
	public void setHNS(String hNS) {
		HNS = hNS;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public Integer getTotalCandidate() {
		return totalCandidate;
	}
	public void setTotalCandidate(Integer totalCandidate) {
		this.totalCandidate = totalCandidate;
	}
	public Integer getAvlCandidate() {
		return avlCandidate;
	}
	public void setAvlCandidate(Integer avlCandidate) {
		this.avlCandidate = avlCandidate;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Boolean getNoResponseEmail() {
		return noResponseEmail;
	}
	public void setNoResponseEmail(Boolean noResponseEmail) {
		this.noResponseEmail = noResponseEmail;
	}
	public Date getOfferMadeDate() {
		return offerMadeDate;
	}
	public void setOfferMadeDate(Date offerMadeDate) {
		this.offerMadeDate = offerMadeDate;
	}
	public Boolean getOfferReady() {
		return offerReady;
	}
	public void setOfferReady(Boolean offerReady) {
		this.offerReady = offerReady;
	}
	public String getSapStatus() {
		return sapStatus;
	}
	public void setSapStatus(String sapStatus) {
		this.sapStatus = sapStatus;
	}
	public Date getOfferAcceptedDate() {
		return offerAcceptedDate;
	}
	public void setOfferAcceptedDate(Date offerAcceptedDate) {
		this.offerAcceptedDate = offerAcceptedDate;
	}
	public Boolean getOfferAccepted() {
		return offerAccepted;
	}
	public void setOfferAccepted(Boolean offerAccepted) {
		this.offerAccepted = offerAccepted;
	}
	public SecondaryStatus getInternalSecondaryStatus() {
		return internalSecondaryStatus;
	}
	public void setInternalSecondaryStatus(SecondaryStatus internalSecondaryStatus) {
		this.internalSecondaryStatus = internalSecondaryStatus;
	}
	public StatusMaster getInternalStatus() {
		return internalStatus;
	}
	public void setInternalStatus(StatusMaster internalStatus) {
		this.internalStatus = internalStatus;
	}
	public String getRequisitionNumber() {
		return requisitionNumber;
	}
	public void setRequisitionNumber(String requisitionNumber) {
		this.requisitionNumber = requisitionNumber;
	}
	public StatusMaster getStatusMaster() {
		return statusMaster;
	}
	public void setStatusMaster(StatusMaster statusMaster) {		
		if(statusMaster!=null && statusMaster.getStatusId()!=null)
			statusMasterChange = (this.statusMaster==null || this.statusMaster.getStatusId()==null )? true : (this.statusMaster.getStatusId().toString().trim().equalsIgnoreCase(statusMaster.getStatusId().toString().trim()))? false : true;
		this.statusMaster = statusMaster;	
	}
	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}
	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}
	public Double getFitScore() {
		return fitScore;
	}
	public void setFitScore(Double fitScore) {
		this.fitScore = fitScore;
	}
	public String getDistName() {
		return distName;
	}
	public void setDistName(String distName) {
		this.distName = distName;
	}
	
	public Date getPostDate() {
		return postDate;
	}
	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getAppsDate() {
		return appsDate;
	}
	public void setAppsDate(Date appsDate) {
		this.appsDate = appsDate;
	}
	public Integer getaScore() {
		return aScore;
	}
	public void setaScore(Integer aScore) {
		this.aScore = aScore;
	}
	public Integer getlRScore() {
		return lRScore;
	}
	public void setlRScore(Integer lRScore) {
		this.lRScore = lRScore;
	}
	
	public String getAssessmentStatus() {
		return assessmentStatus;
	}
	public void setAssessmentStatus(String assessmentStatus) {
		this.assessmentStatus = assessmentStatus;
	}
	public Long getJobForTeacherId() {
		return jobForTeacherId;
	}
	public void setJobForTeacherId(Long jobForTeacherId) {
		this.jobForTeacherId = jobForTeacherId;
	}
	public TeacherDetail getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}
	public JobOrder getJobId() {
		return jobId;
	}
	public void setJobId(JobOrder jobId) {
		this.jobId = jobId;
	}
	public String getRedirectedFromURL() {
		return redirectedFromURL;
	}
	public void setRedirectedFromURL(String redirectedFromURL) {
		this.redirectedFromURL = redirectedFromURL;
	}
	public String getJobBoardReferralURL() {
		return jobBoardReferralURL;
	}
	public void setJobBoardReferralURL(String jobBoardReferralURL) {
		this.jobBoardReferralURL = jobBoardReferralURL;
	}
	public StatusMaster getStatus() {
		return status;
	}
	public void setStatus(StatusMaster status) {
		if(status!=null && status.getStatusId()!=null)
			statusChange = (this.status==null || this.status.getStatusId()==null )? true : (this.status.getStatusId().toString().trim().equalsIgnoreCase(status.getStatusId().toString().trim()))? false : true;
		this.status = status;		
	}
	public boolean isStatusChange()
	{
		return statusChange;
	}	
	public boolean isStatusMasterChange() {
		return statusMasterChange;
	}
	public Boolean getCgUpdated() {
		return cgUpdated;
	}
	public void setCgUpdated(Boolean cgUpdated) {
		this.cgUpdated = cgUpdated;
	}
	public String getCoverLetter() {
		return coverLetter;
	}
	public void setCoverLetter(String coverLetter) {
		this.coverLetter = coverLetter;
	}
	public Integer getIsAffilated() {
		return isAffilated;
	}
	public void setIsAffilated(Integer isAffilated) {
		this.isAffilated = isAffilated;
	}
	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}
	public String getStaffType() {
		return staffType;
	}
	public Boolean getFlagForDistrictSpecificQuestions() {
		return flagForDistrictSpecificQuestions;
	}
	public void setFlagForDistrictSpecificQuestions(
			Boolean flagForDistrictSpecificQuestions) {
		this.flagForDistrictSpecificQuestions = flagForDistrictSpecificQuestions;
	}
	public UserMaster getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(UserMaster updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getUpdatedByEntity() {
		return updatedByEntity;
	}
	public void setUpdatedByEntity(Integer updatedByEntity) {
		this.updatedByEntity = updatedByEntity;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public String getLastActivity() {
		return lastActivity;
	}
	public void setLastActivity(String lastActivity) {
		this.lastActivity = lastActivity;
	}
	public Date getLastActivityDate() {
		return lastActivityDate;
	}
	public void setLastActivityDate(Date lastActivityDate) {
		this.lastActivityDate = lastActivityDate;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	public TeacherAssessmentStatus getTeacherAssessmentStatus() {
		return teacherAssessmentStatus;
	}
	public void setTeacherAssessmentStatus(
			TeacherAssessmentStatus teacherAssessmentStatus) {
		this.teacherAssessmentStatus = teacherAssessmentStatus;
	}
	public Double getCompositeScore() {
		return compositeScore;
	}
	public void setCompositeScore(Double compositeScore) {
		this.compositeScore = compositeScore;
	}
	public Double getNormScore() {
		return normScore;
	}
	public void setNormScore(Double normScore) {
		this.normScore = normScore;
	}
	
	public String getNoteForDistrictSpecificQuestions() {
		return noteForDistrictSpecificQuestions;
	}
	public void setNoteForDistrictSpecificQuestions(
			String noteForDistrictSpecificQuestions) {
		this.noteForDistrictSpecificQuestions = noteForDistrictSpecificQuestions;
	}
	public Boolean getIsDistrictSpecificNoteFinalize() {
		return isDistrictSpecificNoteFinalize;
	}
	public void setIsDistrictSpecificNoteFinalize(
			Boolean isDistrictSpecificNoteFinalize) {
		this.isDistrictSpecificNoteFinalize = isDistrictSpecificNoteFinalize;
	}
	
	public Date getDistrictSpecificNoteFinalizeDate() {
		return districtSpecificNoteFinalizeDate;
	}
	public void setDistrictSpecificNoteFinalizeDate(
			Date districtSpecificNoteFinalizeDate) {
		this.districtSpecificNoteFinalizeDate = districtSpecificNoteFinalizeDate;
	}
	public Boolean getFlagForDspq() {
		return flagForDspq;
	}
	public void setFlagForDspq(Boolean flagForDspq) {
		this.flagForDspq = flagForDspq;
	}
	public String getStafferName() {
		return stafferName;
	}
	public void setStafferName(String stafferName) {
		this.stafferName = stafferName;
	}
	public Date getHiredDate() {
		return hiredDate;
	}
	public void setHiredDate(Date hiredDate) {
		this.hiredDate = hiredDate;
	}
	
	public Integer getNoOfReminderSent() {
		return noOfReminderSent;
	}
	public void setNoOfReminderSent(Integer noOfReminderSent) {
		this.noOfReminderSent = noOfReminderSent;
	}
	public Date getReminderSentDate() {
		return reminderSentDate;
	}
	public void setReminderSentDate(Date reminderSentDate) {
		this.reminderSentDate = reminderSentDate;
	}
	public Integer getNoofTries() {
		return noofTries;
	}
	public void setNoofTries(Integer noofTries) {
		this.noofTries = noofTries;
	}
	
	
	
	public Boolean getFlagForDistrictSpecificQuestionsONB() {
		return flagForDistrictSpecificQuestionsONB;
	}
	public void setFlagForDistrictSpecificQuestionsONB(
			Boolean flagForDistrictSpecificQuestionsONB) {
		this.flagForDistrictSpecificQuestionsONB = flagForDistrictSpecificQuestionsONB;
	}
	public String getNoteForDistrictSpecificQuestionsONB() {
		return noteForDistrictSpecificQuestionsONB;
	}
	public void setNoteForDistrictSpecificQuestionsONB(
			String noteForDistrictSpecificQuestionsONB) {
		this.noteForDistrictSpecificQuestionsONB = noteForDistrictSpecificQuestionsONB;
	}
	public Boolean getIsDistrictSpecificNoteFinalizeONB() {
		return isDistrictSpecificNoteFinalizeONB;
	}
	public void setIsDistrictSpecificNoteFinalizeONB(
			Boolean isDistrictSpecificNoteFinalizeONB) {
		this.isDistrictSpecificNoteFinalizeONB = isDistrictSpecificNoteFinalizeONB;
	}
	public Date getDistrictSpecificNoteFinalizeONBDate() {
		return districtSpecificNoteFinalizeONBDate;
	}
	public void setDistrictSpecificNoteFinalizeONBDate(
			Date districtSpecificNoteFinalizeONBDate) {
		this.districtSpecificNoteFinalizeONBDate = districtSpecificNoteFinalizeONBDate;
	}
	public UserMaster getDistrictSpecificNoteFinalizeONBBy() {
		return districtSpecificNoteFinalizeONBBy;
	}
	public void setDistrictSpecificNoteFinalizeONBBy(
			UserMaster districtSpecificNoteFinalizeONBBy) {
		this.districtSpecificNoteFinalizeONBBy = districtSpecificNoteFinalizeONBBy;
	}
	
	public String getCoverLetterFileName() {
		return coverLetterFileName;
	}
	public void setCoverLetterFileName(String coverLetterFileName) {
		this.coverLetterFileName = coverLetterFileName;
	}
	public Boolean getVviMailSendFlag() {
		return vviMailSendFlag;
	}
	public void setVviMailSendFlag(Boolean vviMailSendFlag) {
		this.vviMailSendFlag = vviMailSendFlag;
	}
	
	public Boolean getNotReviewedFlag() {
		return notReviewedFlag;
	}
	public void setNotReviewedFlag(Boolean notReviewedFlag) {
		this.notReviewedFlag = notReviewedFlag;
	}
	
	public String getIsApplicantHasBDegreeOrHigh() {
		return isApplicantHasBDegreeOrHigh;
	}
	public void setIsApplicantHasBDegreeOrHigh(String isApplicantHasBDegreeOrHigh) {
		this.isApplicantHasBDegreeOrHigh = isApplicantHasBDegreeOrHigh;
	}
		
	public JFTWiseDetails getjFTWiseDetails() {
		return jFTWiseDetails;
	}
	public void setjFTWiseDetails(JFTWiseDetails jFTWiseDetails) {
		this.jFTWiseDetails = jFTWiseDetails;
	}
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof JobForTeacher))return false;
	    JobForTeacher jobForTeacher = (JobForTeacher)object;
	  
	    if(this.jobForTeacherId.equals(jobForTeacher.getJobForTeacherId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	public int compareTo(JobForTeacher otherJFT) 
	{   
		return this.compositeScore.compareTo(otherJFT.compositeScore);
		
		/*if (this.compositeScore == otherJFT.getCompositeScore())
			return 0;
		else if (this.compositeScore < otherJFT.getCompositeScore())
			return 1;
		else
			return -1;
        */
    }
	
	public static Comparator<JobForTeacher> jobForTeacherComparator = new Comparator<JobForTeacher>() {
		
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
		Long cScore1 = Math.round(jft1.getCompositeScore());
		Long cScore2 = Math.round(jft2.getCompositeScore());		
		int c = cScore2.compareTo(cScore1);
		if(c==0){
			return  jft1.getTeacherId().getFirstName().compareTo(jft2.getTeacherId().getFirstName());
		}
		else{
			return c;
		}		
	}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorDesc = new Comparator<JobForTeacher>() {
		
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
		
		Long cScore1 = Math.round(jft1.getCompositeScore());
		Long cScore2 = Math.round(jft2.getCompositeScore());		
		int c = cScore1.compareTo(cScore2);
		if(c==0){
			return  jft1.getTeacherId().getLastName().compareTo(jft2.getTeacherId().getLastName());
		}
		else{
			return c;
		}		
	}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorTeacher  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			return jft1.getTeacherId().getLastName().toUpperCase().compareTo(jft2.getTeacherId().getLastName().toUpperCase());
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorTeacherDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			int c = jft2.getTeacherId().getLastName().toUpperCase().compareTo(jft1.getTeacherId().getLastName().toUpperCase());
			/*if(c==0){
				return jft2.getTeacherId().getLastName().toUpperCase().compareTo(jft1.getTeacherId().getLastName().toUpperCase());
			}*/
			return c;
			
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorNormScore = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Double cScore1 =jft1.getNormScore();
			Double cScore2 =jft2.getNormScore();		
			int c = cScore1.compareTo(cScore2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorNormScoreDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Double cScore1 = jft1.getNormScore();
			Double cScore2 = jft2.getNormScore();		
			int c = cScore2.compareTo(cScore1);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorAssessmentStatus  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			return jft1.getAssessmentStatus().toUpperCase().compareTo(jft2.getAssessmentStatus().toUpperCase());
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorAssessmentStatusDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			return jft2.getAssessmentStatus().toUpperCase().compareTo(jft1.getAssessmentStatus().toUpperCase());
		}		
	};
	
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorAScore = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer aScore1 = jft1.getaScore();
			Integer aScore2=jft2.getaScore();		
			int c =aScore1.compareTo(aScore2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorAScoreDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer aScore1 = jft1.getaScore();
			Integer aScore2=jft2.getaScore();		
			int c =aScore2.compareTo(aScore1);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorLRScore = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer lRScore1 = jft1.getlRScore();
			Integer lRScore2=jft2.getlRScore();		
			int c =lRScore1.compareTo(lRScore2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorLRScoreDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer lRScore1 = jft1.getlRScore();
			Integer lRScore2=jft2.getlRScore();		
			int c =lRScore2.compareTo(lRScore1);
			return c;
		}		
	};
	
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorTeacherId = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer lRScore1 = jft1.getTeacherId().getTeacherId();
			Integer lRScore2=jft2.getTeacherId().getTeacherId();		
			int c =lRScore1.compareTo(lRScore2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorTeacherIdDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer lRScore1 = jft1.getTeacherId().getTeacherId();
			Integer lRScore2=jft2.getTeacherId().getTeacherId();	
			int c =lRScore2.compareTo(lRScore1);
			return c;
		}		
	};
	
	
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorNF = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer NF1 = jft1.getNF();
			Integer NF2=jft2.getNF();		
			int c =NF1.compareTo(NF2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorNFDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer NF1 = jft1.getNF();
			Integer NF2=jft2.getNF();			
			int c =NF2.compareTo(NF1);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorFitScore = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Double fitScore1 = jft1.getFitScore();
			Double fitScore2=jft2.getFitScore();		
			int c =fitScore1.compareTo(fitScore2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorFitScoreDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Double fitScore1 = jft1.getFitScore();
			Double fitScore2=jft2.getFitScore();		
			int c =fitScore2.compareTo(fitScore1);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorMA = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer MA1 = jft1.getMA();
			Integer MA2 = jft2.getMA();	
			int c =MA1.compareTo(MA2);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorMADesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer MA1 = jft1.getMA();
			Integer MA2 = jft2.getMA();	
			int c =MA2.compareTo(MA1);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorHNS = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			return jft1.getHNS().toUpperCase().compareTo(jft2.getHNS().toUpperCase());
		}	
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorHNSDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			return jft2.getHNS().toUpperCase().compareTo(jft1.getHNS().toUpperCase());
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorCGPADesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Double CGPA1 = jft1.getCGPA();
			Double CGPA2 = jft2.getCGPA();		
			int c = CGPA2.compareTo(CGPA1);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorCGPA = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Double CGPA1 = jft1.getCGPA();
			Double CGPA2 = jft2.getCGPA();		
			int c = CGPA1.compareTo(CGPA2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorDate  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			return jft1.getCreatedDateTime().compareTo(jft2.getCreatedDateTime());
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorDateDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			return jft2.getCreatedDateTime().compareTo(jft1.getCreatedDateTime());
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorCompletionDate  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {	
			int x =5;
			System.out.println("HEL********************************");
			if(jft1.getJobCompleteDate()!=null && jft2.getJobCompleteDate()!=null){
				x =jft1.getJobCompleteDate().compareTo(jft2.getJobCompleteDate());
			}
			return x;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorCompletionDateDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			int x =5;
//			System.out.println("^^^^^^inside jobForTeacherComparatorCompletionDateDesc ??????????????????????"+jft1.getJobCompleteDate());
	//	    System.out.println(jft2.getJobCompleteDate());
			if(jft1.getJobCompleteDate()!=null && jft2.getJobCompleteDate()!=null){
				x =jft2.getJobCompleteDate().compareTo(jft1.getJobCompleteDate());
			}
			return x;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorVVI = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer VVI1 = jft1.getVVI();
			Integer VVI2 = jft2.getVVI();	
			int c =VVI1.compareTo(VVI2);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorVVIDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer VVI1 = jft1.getVVI();
			Integer VVI2 = jft2.getVVI();	
			int c =VVI2.compareTo(VVI1);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorYote = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Double yote1 = jft1.getyote();
			Double yote2 = jft2.getyote();	
			int c =yote1.compareTo(yote2);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorassYoteDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Double yote1 = jft1.getyote();
			Double yote2 = jft2.getyote();	
			int c =yote2.compareTo(yote1);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorassScore1 = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer ass1 = jft1.getAssScore1();
			Integer ass2 = jft2.getAssScore1();	
			int c =ass1.compareTo(ass2);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorassScore1Desc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer ass1 = jft1.getAssScore1();
			Integer ass2 = jft2.getAssScore1();	
			int c =ass2.compareTo(ass1);
			return c;
		}		
	};
	
	///
	public static Comparator<JobForTeacher> jobForTeacherComparatorassScore2 = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer ass1 = jft1.getAssScore2();
			Integer ass2 = jft2.getAssScore2();	
			int c =ass1.compareTo(ass2);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorassScore2Desc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer ass1 = jft1.getAssScore2();
			Integer ass2 = jft2.getAssScore2();	
			int c =ass2.compareTo(ass1);
			return c;
		}		
	};
	/////
	public static Comparator<JobForTeacher> jobForTeacherComparatorassScore3 = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer ass1 = jft1.getAssScore3();
			Integer ass2 = jft2.getAssScore3();	
			int c =ass1.compareTo(ass2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorassScore3Desc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer ass1 = jft1.getAssScore3();
			Integer ass2 = jft2.getAssScore3();	
			int c =ass2.compareTo(ass1);
			return c;
		}		
	};
	///
	public static Comparator<JobForTeacher> jobForTeacherComparatorassScore4 = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer ass1 = jft1.getAssScore4();
			Integer ass2 = jft2.getAssScore4();	
			int c =ass1.compareTo(ass2);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorassScore4Desc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer ass1 = jft1.getAssScore4();
			Integer ass2 = jft2.getAssScore4();	
			int c =ass2.compareTo(ass1);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorassScore5 = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer ass1 = jft1.getAssScore5();
			Integer ass2 = jft2.getAssScore5();	
			int c =ass1.compareTo(ass2);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorassScore5Desc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer ass1 = jft1.getAssScore5();
			Integer ass2 = jft2.getAssScore5();	
			int c =ass2.compareTo(ass1);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorOaScore1 = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer oa1 = jft1.getOnlineActivityScore1();
			Integer oa2 = jft2.getOnlineActivityScore1();	
			int c =oa1.compareTo(oa2);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorOaScore1Desc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer oa1 = jft1.getOnlineActivityScore1();
			Integer oa2 = jft2.getOnlineActivityScore1();	
			int c =oa2.compareTo(oa1);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorOaScore2 = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer oa1 = jft1.getOnlineActivityScore2();
			Integer oa2 = jft2.getOnlineActivityScore2();	
			int c =oa1.compareTo(oa2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorId = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer oa1 = jft1.getJobId().getJobId();
			Integer oa2 = jft2.getJobId().getJobId();	
			int c =oa1.compareTo(oa2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorIdDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer cScore1 = jft1.getJobId().getJobId();
			Integer cScore2 = jft2.getJobId().getJobId();		
			int c = cScore2.compareTo(cScore1);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorOaScore2Desc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer oa1 = jft1.getOnlineActivityScore2();
			Integer oa2 = jft2.getOnlineActivityScore2();	
			int c =oa2.compareTo(oa1);
			return c;
		}		
	};
	@Override
	public String toString() 
	{
		return super.toString();
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public Double getCGPA() {
		return CGPA;
	}
	public void setCGPA(Double cGPA) {
		CGPA = cGPA;
	}
	public static Comparator<JobForTeacher> jobForTeacherComparatorSpStatus = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer spStatus1 = jft1.getSpStatus();
			Integer spStatus2=jft2.getSpStatus();		
			int c =spStatus1.compareTo(spStatus2);
			return c;
		}		
	};
	public static Comparator<JobForTeacher> jobForTeacherComparatorSpStatusDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer spStatus1 = jft1.getSpStatus();
			Integer spStatus2=jft2.getSpStatus();	
			int c =spStatus2.compareTo(spStatus1);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorNoOfRecord = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer Record1 = jft1.getNoOfRecord();
			Integer Record2=jft2.getNoOfRecord();	
			if(Record1==null)
			{
				Record1=-1;
			}
			if(Record2==null)
			{
				Record2=-1;
			}
			int c =Record1.compareTo(Record2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorNoOfRecordDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer Record1 = jft1.getNoOfRecord();
			Integer Record2=jft2.getNoOfRecord();	
			if(Record1==null)
			{
				Record1=-1;
			}
			if(Record2==null)
			{
				Record2=-1;
			}
			int c =Record2.compareTo(Record1);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorEPINormScore = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer avgEPINorm = jft1.getEpiNormScoreAvg();
			Integer avgEPINorm1=jft2.getEpiNormScoreAvg();	
			if(avgEPINorm==null)
			{
				avgEPINorm=-1;
			}
			if(avgEPINorm1==null)
			{
				avgEPINorm1=-1;
			}
			int c =avgEPINorm.compareTo(avgEPINorm1);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherComparatorEPINormScoreDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer avgEPINorm = jft1.getEpiNormScoreAvg();
			Integer avgEPINorm1=jft2.getEpiNormScoreAvg();	
			if(avgEPINorm==null)
			{
				avgEPINorm=-1;
			}
			if(avgEPINorm1==null)
			{
				avgEPINorm1=-1;
			}
			int c =avgEPINorm1.compareTo(avgEPINorm);
			return c;
		}		
	};

	public Boolean getSendMail() {
		return sendMail;
	}
	public void setSendMail(Boolean sendMail) {
		this.sendMail = sendMail;
	}	
	
	public static Comparator<JobForTeacher> jobForTeacherStatusComparatorId = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			Integer oa1 = jft1.getStatus().getStatusId();
			Integer oa2 = jft2.getStatus().getStatusId();	
			int c =oa1.compareTo(oa2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherStatusComparatorIdDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			Integer cScore1 = jft1.getStatus().getStatusId();
			Integer cScore2 = jft2.getStatus().getStatusId();		
			int c = cScore2.compareTo(cScore1);
			return c;
		}		
	};
	//For TIMING
	public static Comparator<JobForTeacher> jobForTeacherTimingComparatorId = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			String oa1 = jft1.getDistName();
			String oa2 = jft2.getDistName();	
			int c =oa1.compareTo(oa2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherTimingComparatorIdDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			String cScore1 = jft1.getDistName();
			String cScore2 = jft2.getDistName();		
			int c = cScore2.compareTo(cScore1);
			return c;
		}		
	};
	//FOR REASON
	public static Comparator<JobForTeacher> jobForTeacherReasonComparatorId = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {
			String oa1 = jft1.getStafferName();
			String oa2 = jft2.getStafferName();	
			int c =oa1.compareTo(oa2);
			return c;
		}		
	};
	
	public static Comparator<JobForTeacher> jobForTeacherReasonComparatorIdDesc  = new Comparator<JobForTeacher>(){
		public int compare(JobForTeacher jft1, JobForTeacher jft2) {			
			String cScore1 = jft1.getStafferName();
			String cScore2 = jft2.getStafferName();		
			int c = cScore2.compareTo(cScore1);
			return c;
		}		
	};
	
	
	
}
