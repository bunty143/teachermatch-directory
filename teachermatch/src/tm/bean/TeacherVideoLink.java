package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teachervideolink")
public class TeacherVideoLink implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2943126532820228047L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer videolinkAutoId;
	
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherDetail; 
	
	private String videourl;     
	private Date createdDate;
	private String video;	
	
	@ManyToOne
	@JoinColumn(name="videoAddedByDistrict", referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="videoAddedByUser", referencedColumnName="userId")
	private UserMaster userMaster;
	
	public Integer getVideolinkAutoId() {
		return videolinkAutoId;
	}
	public void setVideolinkAutoId(Integer videolinkAutoId) {
		this.videolinkAutoId = videolinkAutoId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public String getVideourl() {
		return videourl;
	}
	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getVideo() {
		return video;
	}
	public void setVideo(String video) {
		this.video = video;
	}
	
	

	
	
}
