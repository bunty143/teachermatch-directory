package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.StatusWiseEmailSection;


@Entity
@Table(name="statuswiseemailsubsection")
public class StatusWiseEmailSubSection implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5453486019810402979L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer emailSubSectionId;

	@ManyToOne
	@JoinColumn(name="emailSectionId",referencedColumnName="emailSectionId")
	private StatusWiseEmailSection statusWiseEmailSection;
	
	private String subSectionName;
	private String templateBody;
	private String status;
	private Integer createdBy;
	private Date createdDateTime;


	public Integer getEmailSubSectionId() {
		return emailSubSectionId;
	}
	public void setEmailSubSectionId(Integer emailSubSectionId) {
		this.emailSubSectionId = emailSubSectionId;
	}
	public StatusWiseEmailSection getStatusWiseEmailSection() {
		return statusWiseEmailSection;
	}
	public void setStatusWiseEmailSection(
			StatusWiseEmailSection statusWiseEmailSection) {
		this.statusWiseEmailSection = statusWiseEmailSection;
	}
	public String getSubSectionName() {
		return subSectionName;
	}
	public void setSubSectionName(String subSectionName) {
		this.subSectionName = subSectionName;
	}
	
	public String getTemplateBody() {
		return templateBody;
	}
	public void setTemplateBody(String templateBody) {
		this.templateBody = templateBody;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
