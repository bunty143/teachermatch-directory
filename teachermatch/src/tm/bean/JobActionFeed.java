package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;

@Entity(name="jobactionfeed")
public class JobActionFeed implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3685569448497358151L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobactionfeedId;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	private Integer jobActiveDays;
	private Integer noOfHiredCandidates;
	private Integer expectedHires;
	private Integer totalApplicants; 
	private Integer candidateVsOpeningRatio;
	private String lastActivityOnJob;
	private Date lastActivityDate;
	private String activityDoneBy;
	private Integer criticalCandidatesWithHigherNormScore;
	private Integer criticalNormScoreVsOpeningRatio;
	private Integer attentionCandidatesWithHigherNormScore;
	private Integer attentionNormScoreVsOpeningRatio;
	
	private Date createdDateTime;
	
	public Integer getJobactionfeedId() {
		return jobactionfeedId;
	}
	public void setJobactionfeedId(Integer jobactionfeedId) {
		this.jobactionfeedId = jobactionfeedId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public Integer getJobActiveDays() {
		return jobActiveDays;
	}
	public void setJobActiveDays(Integer jobActiveDays) {
		this.jobActiveDays = jobActiveDays;
	}
	public Integer getNoOfHiredCandidates() {
		return noOfHiredCandidates;
	}
	public void setNoOfHiredCandidates(Integer noOfHiredCandidates) {
		this.noOfHiredCandidates = noOfHiredCandidates;
	}
	public Integer getCandidateVsOpeningRatio() {
		return candidateVsOpeningRatio;
	}
	public Integer getTotalApplicants() {
		return totalApplicants;
	}
	public void setTotalApplicants(Integer totalApplicants) {
		this.totalApplicants = totalApplicants;
	}
	public Integer getExpectedHires() {
		return expectedHires;
	}
	public void setExpectedHires(Integer expectedHires) {
		this.expectedHires = expectedHires;
	}
	public void setCandidateVsOpeningRatio(Integer candidateVsOpeningRatio) {
		this.candidateVsOpeningRatio = candidateVsOpeningRatio;
	}
	public String getLastActivityOnJob() {
		return lastActivityOnJob;
	}
	public void setLastActivityOnJob(String lastActivityOnJob) {
		this.lastActivityOnJob = lastActivityOnJob;
	}
	public Date getLastActivityDate() {
		return lastActivityDate;
	}
	public void setLastActivityDate(Date lastActivityDate) {
		this.lastActivityDate = lastActivityDate;
	}
	public String getActivityDoneBy() {
		return activityDoneBy;
	}
	public void setActivityDoneBy(String activityDoneBy) {
		this.activityDoneBy = activityDoneBy;
	}
	public Integer getCriticalCandidatesWithHigherNormScore() {
		return criticalCandidatesWithHigherNormScore;
	}
	public void setCriticalCandidatesWithHigherNormScore(
			Integer criticalCandidatesWithHigherNormScore) {
		this.criticalCandidatesWithHigherNormScore = criticalCandidatesWithHigherNormScore;
	}
	public Integer getCriticalNormScoreVsOpeningRatio() {
		return criticalNormScoreVsOpeningRatio;
	}
	public void setCriticalNormScoreVsOpeningRatio(
			Integer criticalNormScoreVsOpeningRatio) {
		this.criticalNormScoreVsOpeningRatio = criticalNormScoreVsOpeningRatio;
	}
	public Integer getAttentionCandidatesWithHigherNormScore() {
		return attentionCandidatesWithHigherNormScore;
	}
	public void setAttentionCandidatesWithHigherNormScore(
			Integer attentionCandidatesWithHigherNormScore) {
		this.attentionCandidatesWithHigherNormScore = attentionCandidatesWithHigherNormScore;
	}
	public Integer getAttentionNormScoreVsOpeningRatio() {
		return attentionNormScoreVsOpeningRatio;
	}
	public void setAttentionNormScoreVsOpeningRatio(
			Integer attentionNormScoreVsOpeningRatio) {
		this.attentionNormScoreVsOpeningRatio = attentionNormScoreVsOpeningRatio;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	
	
	
}
