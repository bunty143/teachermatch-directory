package tm.bean;

/**********************************
 ***********************
 *  @Author Ram Nath  *
**********************
********************************/

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="districtcandidate",uniqueConstraints = { @UniqueConstraint( columnNames = { "teacherId", "districtId" } ) } )
public class DistrictCandidates {
		/*
		 * @Author Ram Nath
		 * 
		 * CREATE TABLE `districtcandidate` (
				  `districtCandidateId` int(10) NOT NULL auto_increment COMMENT 'when a candidate applies a job in a district for the first time. Do not create a record if the job is a branch job without a districtId (one and only one record per candidate per districtId)',
				  `teacherId` int(10) NOT NULL COMMENT 'Foreign Key to "teacherId" in table "teacherdetail"',
				  `districtId` int(10) NOT NULL COMMENT 'Foreign Key to "districtId" in table "districtMaster"',
				  `numberOfapplications` int(10) NOT NULL default '1' COMMENT 'number of applicants',
				  `firstApplicationDate` datetime NOT NULL COMMENT 'the date part of the createddatetime of the first application in the district',
				  `mostRecentApplicationDate` datetime NOT NULL COMMENT 'default value is same as the firstApplicationDate',
				  `hiringStatusInDistrict` tinyint(1) NOT NULL default '0' COMMENT 'default 0',
				  `hiringStatusInDistrictUpdatedDateTime` datetime default NULL COMMENT '',
				  `positionsHiredForInDistrict` int(10) NOT NULL default '0' COMMENT 'default is 0',
				  `statusInDistrict` varchar(1) NOT NULL default 'A' COMMENT 'default active',
				  `statusInDistrictUpdatedDateTime` datetime default NULL COMMENT 'default null',
				  `createdDateTime` datetime NOT NULL COMMENT 'date and time when the record is created',
				  PRIMARY KEY  (`districtCandidateId`),
				  UNIQUE KEY `districtcandidate_unquie_1` (`districtId`,`teacherId`),
				  KEY `districtId` (`districtId`),
				  KEY `teacherId` (`teacherId`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
				
					ALTER TABLE `districtcandidate`
					  ADD CONSTRAINT `districtcandidate_fk_key_1` FOREIGN KEY (`districtId`) REFERENCES `districtmaster` (`districtId`),
					  ADD CONSTRAINT `districtcandidate_fk_key_2` FOREIGN KEY (`teacherId`) REFERENCES `teacherdetail` (`teacherId`);
		*
		*/
		@Column(name="districtCandidateId" ,columnDefinition="when a candidate applies a job in a district for the first time. Do not create a record if the job is a branch job without a districtId (one and only one record per candidate per districtId)")
		@GeneratedValue(strategy=GenerationType.AUTO)
		@Id
		private Long districtCandidateId;
		
		@ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name="teacherId",referencedColumnName="teacherId",columnDefinition="Foreign Key to 'teacherId' in table 'teacherdetail'")
		private TeacherDetail teacherId;
		
		@ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name="districtId",referencedColumnName="districtId",columnDefinition="Foreign Key to 'districtId' in table 'districtMaster'")
		private DistrictMaster districtId;
		
		@Column(name="numberOfapplications",length=10, columnDefinition="number of applicants")
		private Integer numberOfapplications;
		
		@Column(name="firstApplicationDate",columnDefinition="the date part of the createddatetime of the first application in the district")
		private Date firstApplicationDate;
		
		@Column(name="mostRecentApplicationDate",columnDefinition="default value is same as the firstApplicationDate")
		private Date mostRecentApplicationDate;
	
		@Column(name="hiringStatusInDistrict" ,columnDefinition="default 0")
		private Boolean hiringStatusInDistrict;
		
		@Column(name="hiringStatusInDistrictUpdatedDateTime" ,columnDefinition="")
		private Date hiringStatusInDistrictUpdatedDateTime;
		
		@Column(name="positionsHiredForInDistrict" ,columnDefinition="default is 0")
		private Integer positionsHiredForInDistrict;
		
		@Column(name="statusInDistrict",length=1 ,columnDefinition="default active")
		private String statusInDistrict;
		
		@Column(name="statusInDistrictUpdatedDateTime" ,columnDefinition="default null")
		private Date statusInDistrictUpdatedDateTime;
		
		@Column(name="createdDateTime", columnDefinition="date and time when the record is created")
		private Date createdDateTime;

		public Long getDistrictCandidateId() {
			return districtCandidateId;
		}

		public void setDistrictCandidateId(Long districtCandidateId) {
			this.districtCandidateId = districtCandidateId;
		}

		public TeacherDetail getTeacherId() {
			return teacherId;
		}

		public void setTeacherId(TeacherDetail teacherId) {
			this.teacherId = teacherId;
		}

		public DistrictMaster getDistrictId() {
			return districtId;
		}

		public void setDistrictId(DistrictMaster districtId) {
			this.districtId = districtId;
		}

		public Integer getNumberOfapplications() {
			return numberOfapplications;
		}

		public void setNumberOfapplications(Integer numberOfapplications) {
			this.numberOfapplications = numberOfapplications;
		}

		public Date getFirstApplicationDate() {
			return firstApplicationDate;
		}

		public void setFirstApplicationDate(Date firstApplicationDate) {
			this.firstApplicationDate = firstApplicationDate;
		}

		public Date getMostRecentApplicationDate() {
			return mostRecentApplicationDate;
		}

		public void setMostRecentApplicationDate(Date mostRecentApplicationDate) {
			this.mostRecentApplicationDate = mostRecentApplicationDate;
		}

		public Boolean getHiringStatusInDistrict() {
			return hiringStatusInDistrict;
		}

		public void setHiringStatusInDistrict(Boolean hiringStatusInDistrict) {
			this.hiringStatusInDistrict = hiringStatusInDistrict;
		}

		public Date getHiringStatusInDistrictUpdatedDateTime() {
			return hiringStatusInDistrictUpdatedDateTime;
		}

		public void setHiringStatusInDistrictUpdatedDateTime(
				Date hiringStatusInDistrictUpdatedDateTime) {
			this.hiringStatusInDistrictUpdatedDateTime = hiringStatusInDistrictUpdatedDateTime;
		}

		public Integer getPositionsHiredForInDistrict() {
			return positionsHiredForInDistrict;
		}

		public void setPositionsHiredForInDistrict(Integer positionsHiredForInDistrict) {
			this.positionsHiredForInDistrict = positionsHiredForInDistrict;
		}

		public String getStatusInDistrict() {
			return statusInDistrict;
		}

		public void setStatusInDistrict(String statusInDistrict) {
			this.statusInDistrict = statusInDistrict;
		}

		public Date getStatusInDistrictUpdatedDateTime() {
			return statusInDistrictUpdatedDateTime;
		}

		public void setStatusInDistrictUpdatedDateTime(
				Date statusInDistrictUpdatedDateTime) {
			this.statusInDistrictUpdatedDateTime = statusInDistrictUpdatedDateTime;
		}

		public Date getCreatedDateTime() {
			return createdDateTime;
		}

		public void setCreatedDateTime(Date createdDateTime) {
			this.createdDateTime = createdDateTime;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime
					* result
					+ ((districtCandidateId == null) ? 0 : districtCandidateId
							.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DistrictCandidates other = (DistrictCandidates) obj;
			if (districtCandidateId == null) {
				if (other.districtCandidateId != null)
					return false;
			} else if (!districtCandidateId.equals(other.districtCandidateId))
				return false;
			return true;
		}
}


