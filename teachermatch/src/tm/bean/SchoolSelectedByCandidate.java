package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;

@Entity
@Table(name="schoolselectedbycandidate")
public class SchoolSelectedByCandidate implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6040386975342348104L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer selectedSchoolId;  
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	private String letterOfIntent;
	
	private Date schoolSelectionDate;

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public String getLetterOfIntent() {
		return letterOfIntent;
	}

	public void setLetterOfIntent(String letterOfIntent) {
		this.letterOfIntent = letterOfIntent;
	}

	public Integer getSelectedSchoolId() {
		return selectedSchoolId;
	}

	public void setSelectedSchoolId(Integer selectedSchoolId) {
		this.selectedSchoolId = selectedSchoolId;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}

	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}

	public Date getSchoolSelectionDate() {
		return schoolSelectionDate;
	}

	public void setSchoolSelectionDate(Date schoolSelectionDate) {
		this.schoolSelectionDate = schoolSelectionDate;
	}     
}
