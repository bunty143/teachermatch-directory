package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="jobremovedfromfeed")
public class JobRemovedFromFeed implements Serializable{

	private static final long serialVersionUID = -416722278845750414L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobRemFromFeedId;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	@ManyToOne
	@JoinColumn(name="RemovedByUserId",referencedColumnName="userId")
	private UserMaster userMaster;
	private Integer userEntityTypeId;
	@ManyToOne
	@JoinColumn(name="userHeadQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	@ManyToOne
	@JoinColumn(name="userBranchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	@ManyToOne
	@JoinColumn(name="userDistrictId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	@ManyToOne
	@JoinColumn(name="userSchoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	private Date deletedDateTime;
	
	public Integer getJobRemFromFeedId() {
		return jobRemFromFeedId;
	}
	public void setJobRemFromFeedId(Integer jobRemFromFeedId) {
		this.jobRemFromFeedId = jobRemFromFeedId;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public Integer getUserEntityTypeId() {
		return userEntityTypeId;
	}
	public void setUserEntityTypeId(Integer userEntityTypeId) {
		this.userEntityTypeId = userEntityTypeId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public Date getDeletedDateTime() {
		return deletedDateTime;
	}
	public void setDeletedDateTime(Date deletedDateTime) {
		this.deletedDateTime = deletedDateTime;
	}
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	
	
}
