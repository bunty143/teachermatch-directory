package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.CertificateNameMaster;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.CertificationStatusMaster;
import tm.bean.master.CertificationTypeMaster;
import tm.bean.master.StateMaster;
import tm.utility.Utility;

@Entity
@Table(name="teachercertificate")
public class TeacherCertificate implements Serializable
{	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4868971255574567800L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer certId;
	
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="stateId", referencedColumnName="stateId")
	private StateMaster stateMaster;
	private Integer yearReceived;
	private Integer yearExpired;
	private String doeNumber;

	private String certType;  
	private String certUrl;  
	
	@ManyToOne
	@JoinColumn(name="certTypeId",referencedColumnName="certTypeId")
	private CertificateTypeMaster certificateTypeMaster;
	
	@ManyToOne
	@JoinColumn(name="certificationTypeMasterId", referencedColumnName="certificationTypeMasterId")
	private CertificationTypeMaster certificationTypeMaster;
	
	@ManyToOne
	@JoinColumn(name="certificationStatusId", referencedColumnName="certificationStatusId")
	private CertificationStatusMaster certificationStatusMaster;
	
	private Boolean pkOffered;
	private Boolean kgOffered;
	private Boolean g01Offered;
	private Boolean g02Offered;
	private Boolean g03Offered;
	private Boolean g04Offered;
	private Boolean g05Offered;
	private Boolean g06Offered;
	private Boolean g07Offered;
	private Boolean g08Offered;
	private Boolean g09Offered;
	private Boolean g10Offered;
	private Boolean g11Offered;
	private Boolean g12Offered;
	private Integer readingQualifyingScore;
	private Integer writingQualifyingScore;
	private Integer mathematicsQualifyingScore;
	private String certText;
	private Date createdDateTime;
	private String ieinNumber;
	
	public String getDoeNumber() {
		return doeNumber;
	}
	public void setDoeNumber(String doeNumber) {
		this.doeNumber = doeNumber;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	private String pathOfCertification;
	
	private String certiExpiration;
	public CertificationTypeMaster getCertificationTypeMaster() {
		return certificationTypeMaster;
	}
	public void setCertificationTypeMaster(
			CertificationTypeMaster certificationTypeMaster) {
		this.certificationTypeMaster = certificationTypeMaster;
	}
	public Integer getYearExpired() {
		return yearExpired;
	}
	public void setYearExpired(Integer yearExpired) {
		this.yearExpired = yearExpired;
	}
	
	public String getPathOfCertification() {
		return pathOfCertification;
	}
	public void setPathOfCertification(String pathOfCertification) {
		this.pathOfCertification = pathOfCertification;
	}
	public Integer getCertId() {
		return certId;
	}
	public void setCertId(Integer certId) {
		this.certId = certId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public StateMaster getStateMaster() {
		return stateMaster;
	}
	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}
	public Integer getYearReceived() {
		return yearReceived;
	}
	public void setYearReceived(Integer yearReceived) {
		this.yearReceived = yearReceived;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = Utility.trim(certType);
	}
	public String getCertUrl() {
		return certUrl;
	}
	public void setCertUrl(String certUrl) {
		this.certUrl = certUrl;
	}
	public CertificateTypeMaster getCertificateTypeMaster() {
		return certificateTypeMaster;
	}
	public void setCertificateTypeMaster(
			CertificateTypeMaster certificateTypeMaster) {
		this.certificateTypeMaster = certificateTypeMaster;
	}
	public Boolean getPkOffered() {
		return pkOffered;
	}
	public void setPkOffered(Boolean pkOffered) {
		this.pkOffered = pkOffered;
	}
	public Boolean getKgOffered() {
		return kgOffered;
	}
	public void setKgOffered(Boolean kgOffered) {
		this.kgOffered = kgOffered;
	}
	public Boolean getG01Offered() {
		return g01Offered;
	}
	public void setG01Offered(Boolean g01Offered) {
		this.g01Offered = g01Offered;
	}
	public Boolean getG02Offered() {
		return g02Offered;
	}
	public void setG02Offered(Boolean g02Offered) {
		this.g02Offered = g02Offered;
	}
	public Boolean getG03Offered() {
		return g03Offered;
	}
	public void setG03Offered(Boolean g03Offered) {
		this.g03Offered = g03Offered;
	}
	public Boolean getG04Offered() {
		return g04Offered;
	}
	public void setG04Offered(Boolean g04Offered) {
		this.g04Offered = g04Offered;
	}
	public Boolean getG05Offered() {
		return g05Offered;
	}
	public void setG05Offered(Boolean g05Offered) {
		this.g05Offered = g05Offered;
	}
	public Boolean getG06Offered() {
		return g06Offered;
	}
	public void setG06Offered(Boolean g06Offered) {
		this.g06Offered = g06Offered;
	}
	public Boolean getG07Offered() {
		return g07Offered;
	}
	public void setG07Offered(Boolean g07Offered) {
		this.g07Offered = g07Offered;
	}
	public Boolean getG08Offered() {
		return g08Offered;
	}
	public void setG08Offered(Boolean g08Offered) {
		this.g08Offered = g08Offered;
	}
	public Boolean getG09Offered() {
		return g09Offered;
	}
	public void setG09Offered(Boolean g09Offered) {
		this.g09Offered = g09Offered;
	}
	public Boolean getG10Offered() {
		return g10Offered;
	}
	public void setG10Offered(Boolean g10Offered) {
		this.g10Offered = g10Offered;
	}
	public Boolean getG11Offered() {
		return g11Offered;
	}
	public void setG11Offered(Boolean g11Offered) {
		this.g11Offered = g11Offered;
	}
	public Boolean getG12Offered() {
		return g12Offered;
	}
	public void setG12Offered(Boolean g12Offered) {
		this.g12Offered = g12Offered;
	}
	public Integer getWritingQualifyingScore() {
		return writingQualifyingScore;
	}
	public void setWritingQualifyingScore(Integer writingQualifyingScore) {
		this.writingQualifyingScore = writingQualifyingScore;
	}
	public Integer getReadingQualifyingScore() {
		return readingQualifyingScore;
	}
	public void setReadingQualifyingScore(Integer readingQualifyingScore) {
		this.readingQualifyingScore = readingQualifyingScore;
	}
	public Integer getMathematicsQualifyingScore() {
		return mathematicsQualifyingScore;
	}
	public void setMathematicsQualifyingScore(Integer mathematicsQualifyingScore) {
		this.mathematicsQualifyingScore = mathematicsQualifyingScore;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public CertificationStatusMaster getCertificationStatusMaster() {
		return certificationStatusMaster;
	}
	public void setCertificationStatusMaster(
			CertificationStatusMaster certificationStatusMaster) {
		this.certificationStatusMaster = certificationStatusMaster;
	}
	public String getCertText() {
		return certText;
	}
	public void setCertText(String certText) {
		this.certText = certText;
	}
	public String getIeinNumber() {
		return ieinNumber;
	}
	public void setIeinNumber(String ieinNumber) {
		this.ieinNumber = ieinNumber;
	}
	public String getCertiExpiration() {
		return certiExpiration;
	}
	public void setCertiExpiration(String certiExpiration) {
		this.certiExpiration = certiExpiration;
	}
	
}
