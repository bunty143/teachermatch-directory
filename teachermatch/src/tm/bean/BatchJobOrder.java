package tm.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;

@Entity
@Table(name="batchjoborder")
public class BatchJobOrder implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7474308421697581047L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int batchJobId; 
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
 	private String jobTitle;
 	private Date jobStartDate;     
	private Date jobEndDate;   
	private String jobDescription; 
	private String jobQualification; 
	private Integer noOfExpHires;  
	private String jobStatus;     
	private String status;     
	private Integer createdBy;	
	private Integer createdByEntity;
	private Date createdDateTime;     
	private String ipAddress;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "schoolinbatchjob", joinColumns = { @JoinColumn(name = "batchJobId") }, inverseJoinColumns = { @JoinColumn(name = "schoolId") })
	private List<SchoolMaster> school = new ArrayList<SchoolMaster>(0);
	
	
	private Integer allSchoolsInDistrict;
	private Integer allGrades;
	private Integer selectedSchoolsInDistrict;
	private Integer pkOffered;
	private Integer kgOffered;
	private Integer g01Offered;
	private Integer g02Offered;
	private Integer g03Offered;
	private Integer g04Offered;
	private Integer g05Offered;
	private Integer g06Offered;
	private Integer g07Offered;
	private Integer g08Offered;
	private Integer g09Offered;
	private Integer g10Offered;
	private Integer g11Offered;
	private Integer g12Offered;
	private Integer attachDefaultDistrictPillar;
	private Integer attachDefaultSchoolPillar;
	private Integer attachNewPillar;
	private String exitURL;   
	private String exitMessage; 
	private Integer flagForURL; 
	private Integer flagForMessage; 
	private Boolean isJobAssessment;
	private String assessmentDocument;
	private Integer createdForEntity;
	@Transient
	private String jobDescriptionHTML;
	
	@Transient
	private String jobQualificationHTML;
	
	public List<SchoolMaster> getSchool() {
		return school;
	}


	public void setSchool(List<SchoolMaster> school) {
		this.school = school;
	}


	public Integer getCreatedForEntity() {
		return createdForEntity;
	}


	public void setCreatedForEntity(Integer createdForEntity) {
		this.createdForEntity = createdForEntity;
	}

	public String getExitURL() {
		return exitURL;
	}


	public void setExitURL(String exitURL) {
		this.exitURL = exitURL;
	}


	public String getExitMessage() {
		return exitMessage;
	}


	public void setExitMessage(String exitMessage) {
		this.exitMessage = exitMessage;
	}


	public Integer getFlagForURL() {
		return flagForURL;
	}


	public void setFlagForURL(Integer flagForURL) {
		this.flagForURL = flagForURL;
	}


	public Integer getFlagForMessage() {
		return flagForMessage;
	}


	public void setFlagForMessage(Integer flagForMessage) {
		this.flagForMessage = flagForMessage;
	}


	public Boolean getIsJobAssessment() {
		return isJobAssessment;
	}


	public void setIsJobAssessment(Boolean isJobAssessment) {
		this.isJobAssessment = isJobAssessment;
	}


	public String getAssessmentDocument() {
		return assessmentDocument;
	}


	public void setAssessmentDocument(String assessmentDocument) {
		this.assessmentDocument = assessmentDocument;
	}


	public Integer getAllSchoolsInDistrict() {
		return allSchoolsInDistrict;
	}


	public void setAllSchoolsInDistrict(Integer allSchoolsInDistrict) {
		this.allSchoolsInDistrict = allSchoolsInDistrict;
	}


	public Integer getAllGrades() {
		return allGrades;
	}


	public void setAllGrades(Integer allGrades) {
		this.allGrades = allGrades;
	}


	public Integer getSelectedSchoolsInDistrict() {
		return selectedSchoolsInDistrict;
	}


	public void setSelectedSchoolsInDistrict(Integer selectedSchoolsInDistrict) {
		this.selectedSchoolsInDistrict = selectedSchoolsInDistrict;
	}


	public Integer getPkOffered() {
		return pkOffered;
	}


	public void setPkOffered(Integer pkOffered) {
		this.pkOffered = pkOffered;
	}


	public Integer getKgOffered() {
		return kgOffered;
	}


	public void setKgOffered(Integer kgOffered) {
		this.kgOffered = kgOffered;
	}


	public Integer getG01Offered() {
		return g01Offered;
	}


	public void setG01Offered(Integer g01Offered) {
		this.g01Offered = g01Offered;
	}


	public Integer getG02Offered() {
		return g02Offered;
	}


	public void setG02Offered(Integer g02Offered) {
		this.g02Offered = g02Offered;
	}


	public Integer getG03Offered() {
		return g03Offered;
	}


	public void setG03Offered(Integer g03Offered) {
		this.g03Offered = g03Offered;
	}


	public Integer getG04Offered() {
		return g04Offered;
	}


	public void setG04Offered(Integer g04Offered) {
		this.g04Offered = g04Offered;
	}


	public Integer getG05Offered() {
		return g05Offered;
	}


	public void setG05Offered(Integer g05Offered) {
		this.g05Offered = g05Offered;
	}


	public Integer getG06Offered() {
		return g06Offered;
	}


	public void setG06Offered(Integer g06Offered) {
		this.g06Offered = g06Offered;
	}


	public Integer getG07Offered() {
		return g07Offered;
	}


	public void setG07Offered(Integer g07Offered) {
		this.g07Offered = g07Offered;
	}


	public Integer getG08Offered() {
		return g08Offered;
	}


	public void setG08Offered(Integer g08Offered) {
		this.g08Offered = g08Offered;
	}


	public Integer getG09Offered() {
		return g09Offered;
	}


	public void setG09Offered(Integer g09Offered) {
		this.g09Offered = g09Offered;
	}


	public Integer getG10Offered() {
		return g10Offered;
	}


	public void setG10Offered(Integer g10Offered) {
		this.g10Offered = g10Offered;
	}


	public Integer getG11Offered() {
		return g11Offered;
	}


	public void setG11Offered(Integer g11Offered) {
		this.g11Offered = g11Offered;
	}


	public Integer getG12Offered() {
		return g12Offered;
	}


	public void setG12Offered(Integer g12Offered) {
		this.g12Offered = g12Offered;
	}


	public Integer getAttachDefaultDistrictPillar() {
		return attachDefaultDistrictPillar;
	}


	public void setAttachDefaultDistrictPillar(Integer attachDefaultDistrictPillar) {
		this.attachDefaultDistrictPillar = attachDefaultDistrictPillar;
	}


	public Integer getAttachDefaultSchoolPillar() {
		return attachDefaultSchoolPillar;
	}


	public void setAttachDefaultSchoolPillar(Integer attachDefaultSchoolPillar) {
		this.attachDefaultSchoolPillar = attachDefaultSchoolPillar;
	}


	public Integer getAttachNewPillar() {
		return attachNewPillar;
	}


	public void setAttachNewPillar(Integer attachNewPillar) {
		this.attachNewPillar = attachNewPillar;
	}

	public int getBatchJobId() {
		return batchJobId;
	}


	public void setBatchJobId(int batchJobId) {
		this.batchJobId = batchJobId;
	}


	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}


	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}


	public String getJobTitle() {
		return jobTitle;
	}


	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}


	public Date getJobStartDate() {
		return jobStartDate;
	}


	public void setJobStartDate(Date jobStartDate) {
		this.jobStartDate = jobStartDate;
	}


	public Date getJobEndDate() {
		return jobEndDate;
	}


	public void setJobEndDate(Date jobEndDate) {
		this.jobEndDate = jobEndDate;
	}


	public String getJobDescription() {
		return jobDescription;
	}


	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}


	public Integer getNoOfExpHires() {
		return noOfExpHires;
	}


	public void setNoOfExpHires(Integer noOfExpHires) {
		this.noOfExpHires = noOfExpHires;
	}


	public String getJobStatus() {
		return jobStatus;
	}


	public String getJobQualification() {
		return jobQualification;
	}


	public void setJobQualification(String jobQualification) {
		this.jobQualification = jobQualification;
	}


	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Integer getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}


	public Integer getCreatedByEntity() {
		return createdByEntity;
	}


	public void setCreatedByEntity(Integer createdByEntity) {
		this.createdByEntity = createdByEntity;
	}


	public Date getCreatedDateTime() {
		return createdDateTime;
	}


	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}


	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public String getJobDescriptionHTML() 
	{

		StringBuffer sb = new StringBuffer("");
		try 
		{
			byte[] b= jobDescription.getBytes("UTF-8");			
			char c;
			for(byte a:b)
			{
				System.out.println(a);
				if(a==10)
				{
					sb.append("<br>");
				}				
				else
				{
					c=(char)a;
					sb.append(c);
				}
			}
		}
		catch (Exception e) 
		{
			return null;
		}
		jobDescriptionHTML=sb.toString();
		return jobDescriptionHTML;
	
	}
	
	
	public String getJobQualificationHTML() 
	{

		StringBuffer sb = new StringBuffer("");
		try 
		{
			byte[] b= jobQualification.getBytes("UTF-8");			
			char c;
			for(byte a:b)
			{
				System.out.println(a);
				if(a==10)
				{
					sb.append("<br>");
				}				
				else
				{
					c=(char)a;
					sb.append(c);
				}
			}
		}
		catch (Exception e) 
		{
			return null;
		}
		jobQualificationHTML=sb.toString();
		return jobQualificationHTML;
	
	}
	

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
