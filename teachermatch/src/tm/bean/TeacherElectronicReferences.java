package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacherelectronicreference")
public class TeacherElectronicReferences implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3344735829105976381L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer elerefAutoId;
	
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherDetail; 
	
	private Integer salutation;     
	private String firstName;     
	private String lastName;
	
	private String designation;
	private String organization;
	private String email;
	private String contactnumber;
	
	private Date createdDateTime;
	
	private Boolean rdcontacted;
	private Boolean canContOnOffer;
	private String referenceDetails;
	
	private String longHaveYouKnow;
	private Integer status;
	
	private String pathOfReference;
	
	@ManyToOne
	@JoinColumn(name="refAddedByDistrict", referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="refAddedByUser", referencedColumnName="userId")
	private UserMaster userMaster;
	
	public String getPathOfReference() {
		return pathOfReference;
	}
	public void setPathOfReference(String pathOfReference) {
		this.pathOfReference = pathOfReference;
	}
	
	public Integer getElerefAutoId() {
		return elerefAutoId;
	}
	public void setElerefAutoId(Integer elerefAutoId) {
		this.elerefAutoId = elerefAutoId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public Integer getSalutation() {
		return salutation;
	}
	public void setSalutation(Integer salutation) {
		this.salutation = salutation;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactnumber() {
		return contactnumber;
	}
	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Boolean getRdcontacted() {
		return rdcontacted;
	}
	public void setRdcontacted(Boolean rdcontacted) {
		this.rdcontacted = rdcontacted;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getReferenceDetails() {
		return referenceDetails;
	}
	public void setReferenceDetails(String referenceDetails) {
		this.referenceDetails = referenceDetails;
	}
	
	public String getLongHaveYouKnow() {
		return longHaveYouKnow;
	}
	public void setLongHaveYouKnow(String longHaveYouKnow) {
		this.longHaveYouKnow = longHaveYouKnow;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public Boolean getCanContOnOffer() {
		return canContOnOffer;
	}
	public void setCanContOnOffer(Boolean canContOnOffer) {
		this.canContOnOffer = canContOnOffer;
	}
	
	
}
