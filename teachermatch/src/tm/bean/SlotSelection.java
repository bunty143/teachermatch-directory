package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="slotselection")
public class SlotSelection implements Serializable
{
	private static final long serialVersionUID = 4381576666870517280L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer slotselectionId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="eventScheduleId",referencedColumnName="eventScheduleId")
	private EventSchedule eventSchedule;
	
	public Integer getSlotselectionId() {
		return slotselectionId;
	}
	public void setSlotselectionId(Integer slotselectionId) {
		this.slotselectionId = slotselectionId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public EventSchedule getEventSchedule() {
		return eventSchedule;
	}
	public void setEventSchedule(EventSchedule eventSchedule) {
		this.eventSchedule = eventSchedule;
	}

	
}
