package tm.bean.cgreport;

import java.io.Serializable;
import java.util.Comparator;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.user.UserMaster;

@Entity
@Table(name="jobwiseconsolidatedteacherscore")
public class JobWiseConsolidatedTeacherScore implements Serializable
{
	private static final long serialVersionUID = 7069891725830529679L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherConsolidatedScoreId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	private Double jobWiseConsolidatedScore;
	private Double jobWiseMaxScore;
	private Date updatedDateTime;

	public Double getJobWiseMaxScore() {
		return jobWiseMaxScore;
	}

	public void setJobWiseMaxScore(Double jobWiseMaxScore) {
		this.jobWiseMaxScore = jobWiseMaxScore;
	}

	public Integer getTeacherConsolidatedScoreId() {
		return teacherConsolidatedScoreId;
	}

	public void setTeacherConsolidatedScoreId(Integer teacherConsolidatedScoreId) {
		this.teacherConsolidatedScoreId = teacherConsolidatedScoreId;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}


	public Double getJobWiseConsolidatedScore() {
		return jobWiseConsolidatedScore;
	}

	public void setJobWiseConsolidatedScore(Double jobWiseConsolidatedScore) {
		this.jobWiseConsolidatedScore = jobWiseConsolidatedScore;
	}

	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	
	
}
