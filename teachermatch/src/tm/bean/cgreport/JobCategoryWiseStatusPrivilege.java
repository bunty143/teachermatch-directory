package tm.bean.cgreport;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="jobcategorywisestatusprivilege")
public class JobCategoryWiseStatusPrivilege implements Serializable
{
	private static final long serialVersionUID = -5133720197566941861L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobCategoryStatusPrivilegeId;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;

	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	
	@ManyToOne
	@JoinColumn(name="statusId",referencedColumnName="statusId")
	private StatusMaster statusMaster;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;
	
	private boolean statusPrivilegeForSchools; 
	
	private String instructionAttachFileName; 
	private Double maxValuePerJSIQuestion;
	
	private boolean panel;
	private boolean autoUpdateStatus;
	
	private Integer	updateStatusOption;
	private Date statusUpdateExpiryDate;
	private String emailNotificationToSelectedDistrictAdmins;
	private boolean emailNotificationToAllSchoolAdmins;
	private boolean overrideUserSettings;
	private Boolean autoNotifyAll;
	
	private Boolean statusPrivilegeForHqBranch;
	
	private String emailNotificationToSelectedHqBranchAdmins;
	
	private Integer emailNotificationToAllHqBranchAdmins;
	
	private Integer statusForFirstTimeJobApply;
	
	private Integer finalizeForEvent;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private Date createdDateTime;
	
	@Column(name="internalExternalOrBothCandidate", columnDefinition="(Required Node on candidate: 0- no required for both internal and external candidate, 1- requied for both external and internal candidate, 2-requied for internal candidate, 3-requied for external candidate)")
	private Integer internalExternalOrBothCandidate=1;

	public Integer getFinalizeForEvent() {
		return finalizeForEvent;
	}

	public void setFinalizeForEvent(Integer finalizeForEvent) {
		this.finalizeForEvent = finalizeForEvent;
	}

	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}

	
	public Double getMaxValuePerJSIQuestion() {
		return maxValuePerJSIQuestion;
	}

	public void setMaxValuePerJSIQuestion(Double maxValuePerJSIQuestion) {
		this.maxValuePerJSIQuestion = maxValuePerJSIQuestion;
	}

	public Integer getJobCategoryStatusPrivilegeId() {
		return jobCategoryStatusPrivilegeId;
	}

	public void setJobCategoryStatusPrivilegeId(Integer jobCategoryStatusPrivilegeId) {
		this.jobCategoryStatusPrivilegeId = jobCategoryStatusPrivilegeId;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}

	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}

	public StatusMaster getStatusMaster() {
		return statusMaster;
	}

	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}

	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}

	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}

	public boolean isStatusPrivilegeForSchools() {
		return statusPrivilegeForSchools;
	}

	public void setStatusPrivilegeForSchools(boolean statusPrivilegeForSchools) {
		this.statusPrivilegeForSchools = statusPrivilegeForSchools;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getInstructionAttachFileName() {
		return instructionAttachFileName;
	}

	public void setInstructionAttachFileName(String instructionAttachFileName) {
		this.instructionAttachFileName = instructionAttachFileName;
	}

	public boolean isPanel() {
		return panel;
	}

	public void setPanel(boolean panel) {
		this.panel = panel;
	}

	public boolean isAutoUpdateStatus() {
		return autoUpdateStatus;
	}

	public void setAutoUpdateStatus(boolean autoUpdateStatus) {
		this.autoUpdateStatus = autoUpdateStatus;
	}
	
	public Integer getUpdateStatusOption() {
		return updateStatusOption;
	}

	public void setUpdateStatusOption(Integer updateStatusOption) {
		this.updateStatusOption = updateStatusOption;
	}

	public Date getStatusUpdateExpiryDate() {
		return statusUpdateExpiryDate;
	}

	public void setStatusUpdateExpiryDate(Date statusUpdateExpiryDate) {
		this.statusUpdateExpiryDate = statusUpdateExpiryDate;
	}

	public String getEmailNotificationToSelectedDistrictAdmins() {
		return emailNotificationToSelectedDistrictAdmins;
	}

	public void setEmailNotificationToSelectedDistrictAdmins(
			String emailNotificationToSelectedDistrictAdmins) {
		this.emailNotificationToSelectedDistrictAdmins = emailNotificationToSelectedDistrictAdmins;
	}

	public boolean isEmailNotificationToAllSchoolAdmins() {
		return emailNotificationToAllSchoolAdmins;
	}

	public void setEmailNotificationToAllSchoolAdmins(
			boolean emailNotificationToAllSchoolAdmins) {
		this.emailNotificationToAllSchoolAdmins = emailNotificationToAllSchoolAdmins;
	}
	
	public boolean isOverrideUserSettings() {
		return overrideUserSettings;
	}
	
	public void setOverrideUserSettings(boolean overrideUserSettings) {
		this.overrideUserSettings = overrideUserSettings;
	}

	public Boolean getAutoNotifyAll() {
		return autoNotifyAll;
	}

	public void setAutoNotifyAll(Boolean autoNotifyAll) {
		this.autoNotifyAll = autoNotifyAll;
	}

	public Integer getInternalExternalOrBothCandidate() {
		return internalExternalOrBothCandidate;
	}

	public void setInternalExternalOrBothCandidate(
			Integer internalExternalOrBothCandidate) {
		this.internalExternalOrBothCandidate = internalExternalOrBothCandidate;
	}

	public Boolean getStatusPrivilegeForHqBranch() {
		return statusPrivilegeForHqBranch;
	}

	public void setStatusPrivilegeForHqBranch(Boolean statusPrivilegeForHqBranch) {
		this.statusPrivilegeForHqBranch = statusPrivilegeForHqBranch;
	}

	public String getEmailNotificationToSelectedHqBranchAdmins() {
		return emailNotificationToSelectedHqBranchAdmins;
	}

	public void setEmailNotificationToSelectedHqBranchAdmins(
			String emailNotificationToSelectedHqBranchAdmins) {
		this.emailNotificationToSelectedHqBranchAdmins = emailNotificationToSelectedHqBranchAdmins;
	}

	public Integer getEmailNotificationToAllHqBranchAdmins() {
		return emailNotificationToAllHqBranchAdmins;
	}

	public void setEmailNotificationToAllHqBranchAdmins(
			Integer emailNotificationToAllHqBranchAdmins) {
		this.emailNotificationToAllHqBranchAdmins = emailNotificationToAllHqBranchAdmins;
	}

	public Integer getStatusForFirstTimeJobApply() {
		return statusForFirstTimeJobApply;
	}

	public void setStatusForFirstTimeJobApply(Integer statusForFirstTimeJobApply) {
		this.statusForFirstTimeJobApply = statusForFirstTimeJobApply;
	}
	
}
