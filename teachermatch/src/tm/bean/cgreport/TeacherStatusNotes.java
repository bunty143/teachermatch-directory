package tm.bean.cgreport;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusNodeMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacherstatusnotes")
public class TeacherStatusNotes implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2909906768615055099L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherStatusNoteId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="statusId",referencedColumnName="statusId")
	private StatusMaster statusMaster;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;
	
	
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private Integer  districtId;
	
	private Long  schoolId;
	
	private String statusNotes;
	
	private Long teacherAssessmentQuestionId;

	private String statusNoteFileName;
	
	private boolean offeraccepbycandidate;
	
	public boolean isOfferaccepbycandidate() {
		return offeraccepbycandidate;
	}

	public void setOfferaccepbycandidate(boolean offeraccepbycandidate) {
		this.offeraccepbycandidate = offeraccepbycandidate;
	}

	private Integer emailSentTo;
	
	private boolean finalizeStatus; 
	
	private Date createdDateTime;
	
	
	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getTeacherStatusNoteId() {
		return teacherStatusNoteId;
	}

	public void setTeacherStatusNoteId(Integer teacherStatusNoteId) {
		this.teacherStatusNoteId = teacherStatusNoteId;
	}

	@Transient
	private Integer scoreProvided;
	

	public Integer getScoreProvided() {
		return scoreProvided;
	}

	public void setScoreProvided(Integer scoreProvided) {
		this.scoreProvided = scoreProvided;
	}

	public String getStatusNoteFileName() {
		return statusNoteFileName;
	}

	public void setStatusNoteFileName(String statusNoteFileName) {
		this.statusNoteFileName = statusNoteFileName;
	}

	public boolean isFinalizeStatus() {
		return finalizeStatus;
	}

	public void setFinalizeStatus(boolean finalizeStatus) {
		this.finalizeStatus = finalizeStatus;
	}

	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}

	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}

	public StatusMaster getStatusMaster() {
		return statusMaster;
	}

	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public String getStatusNotes() {
		return statusNotes;
	}

	public void setStatusNotes(String statusNotes) {
		this.statusNotes = statusNotes;
	}

	public Integer getEmailSentTo() {
		return emailSentTo;
	}

	public void setEmailSentTo(Integer emailSentTo) {
		this.emailSentTo = emailSentTo;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public Long getTeacherAssessmentQuestionId() {
		return teacherAssessmentQuestionId;
	}

	public void setTeacherAssessmentQuestionId(Long teacherAssessmentQuestionId) {
		this.teacherAssessmentQuestionId = teacherAssessmentQuestionId;
	}
}
