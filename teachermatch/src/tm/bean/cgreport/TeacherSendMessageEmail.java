package tm.bean.cgreport;

import java.io.Serializable;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;


public class TeacherSendMessageEmail implements Serializable 
{

	private static final long serialVersionUID = 5478558924525571164L;
	
	private String toEmailId;
	private String messageSubject;   
	private String fromEmailId;     
	private String messageSendBody;
	private String email_filePath;
	private String fileName;
	UserMaster userMaster;
	/* Gagan : for distrcict wise template [Start] */
	private TeacherDetail teacherDetail;
	private JobOrder jobOrder;
	/* Gagan : for distrcict wise template [END] */
	public String getToEmailId() {
		return toEmailId;
	}
	public void setToEmailId(String toEmailId) {
		this.toEmailId = toEmailId;
	}
	public String getMessageSubject() {
		return messageSubject;
	}
	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}
	public String getFromEmailId() {
		return fromEmailId;
	}
	public void setFromEmailId(String fromEmailId) {
		this.fromEmailId = fromEmailId;
	}
	public String getMessageSendBody() {
		return messageSendBody;
	}
	public void setMessageSendBody(String messageSendBody) {
		this.messageSendBody = messageSendBody;
	}
	public String getEmail_filePath() {
		return email_filePath;
	}
	public void setEmail_filePath(String emailFilePath) {
		email_filePath = emailFilePath;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	
}
