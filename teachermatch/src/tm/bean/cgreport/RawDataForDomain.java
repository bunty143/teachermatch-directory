package tm.bean.cgreport;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.DomainMaster;

/**
 * @author Saurabh Kumar
 *
 */
@Entity
@Table(name="rawdatafordomain")
public class RawDataForDomain implements Serializable{
	
	private static final long serialVersionUID = -5328871832187533855L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Integer rawDataId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")	
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="teacherAssessmentId",referencedColumnName="teacherAssessmentId")
	private TeacherAssessmentdetail teacherAssessmentdetail;
	@ManyToOne
	@JoinColumn(name="domainId",referencedColumnName="domainId")	
	private DomainMaster domainMaster;
	private Double score; 
	private Double maxMarks;
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentDetail;
	private Integer assessmentType;
	private Integer assessmentTakenCount;
	private Date createdDateTime;
	private Date assessmentDateTime;
	private Date updateDateTime;
	
	public Integer getRawDataId() {
		return rawDataId;
	}
	public void setRawDataId(Integer rawDataId) {
		this.rawDataId = rawDataId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public DomainMaster getDomainMaster() {
		return domainMaster;
	}
	public void setDomainMaster(DomainMaster domainMaster) {
		this.domainMaster = domainMaster;
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	public Double getMaxMarks() {
		return maxMarks;
	}
	public void setMaxMarks(Double maxMarks) {
		this.maxMarks = maxMarks;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}	
	public AssessmentDetail getAssessmentDetail() {
		return assessmentDetail;
	}
	public void setAssessmentDetail(AssessmentDetail assessmentDetail) {
		this.assessmentDetail = assessmentDetail;
	}
	public Integer getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(Integer assessmentType) {
		this.assessmentType = assessmentType;
	}
	public Integer getAssessmentTakenCount() {
		return assessmentTakenCount;
	}
	public void setAssessmentTakenCount(Integer assessmentTakenCount) {
		this.assessmentTakenCount = assessmentTakenCount;
	}
	public TeacherAssessmentdetail getTeacherAssessmentdetail() {
		return teacherAssessmentdetail;
	}
	public void setTeacherAssessmentdetail(
			TeacherAssessmentdetail teacherAssessmentdetail) {
		this.teacherAssessmentdetail = teacherAssessmentdetail;
	}
	public Date getAssessmentDateTime() {
		return assessmentDateTime;
	}
	public void setAssessmentDateTime(Date assessmentDateTime) {
		this.assessmentDateTime = assessmentDateTime;
	}
	public Date getUpdateDateTime() {
		return updateDateTime;
	}
	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
	public static Comparator<RawDataForDomain> comparatorRawDataForDomainByTeacherId = new Comparator<RawDataForDomain>(){
		public int compare(RawDataForDomain data1, RawDataForDomain data2) {			
			return data1.getTeacherDetail().getTeacherId().compareTo(data2.getTeacherDetail().getTeacherId());
		}		
	};
	@Override
	public String toString() {
		return "RawDataForDomain [domainMaster=" + domainMaster
				+ ", rawDataId=" + rawDataId + ", score=" + score
				+ ", teacherDetail=" + teacherDetail + "]";
	}
}
