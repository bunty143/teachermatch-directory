package tm.bean.cgreport;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacherstatusscores")
public class TeacherStatusScores implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 839700830058268543L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherStatusScoreId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="statusId",referencedColumnName="statusId")
	private StatusMaster statusMaster;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;
	
	
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private Integer  districtId;

	private Long  schoolId;
	
	private Integer emailSentTo;
	
	private Double scoreProvided;
	
	private Double maxScore;
	
	private boolean finalizeStatus; 
	
	private Date createdDateTime;
	
	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getTeacherStatusScoreId() {
		return teacherStatusScoreId;
	}

	public void setTeacherStatusScoreId(Integer teacherStatusScoreId) {
		this.teacherStatusScoreId = teacherStatusScoreId;
	}

	public boolean isFinalizeStatus() {
		return finalizeStatus;
	}

	public void setFinalizeStatus(boolean finalizeStatus) {
		this.finalizeStatus = finalizeStatus;
	}

	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}

	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}

	public StatusMaster getStatusMaster() {
		return statusMaster;
	}

	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Integer getEmailSentTo() {
		return emailSentTo;
	}

	public void setEmailSentTo(Integer emailSentTo) {
		this.emailSentTo = emailSentTo;
	}

	public Double getScoreProvided() {
		return scoreProvided;
	}

	public void setScoreProvided(Double scoreProvided) {
		this.scoreProvided = scoreProvided;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public Double getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(Double maxScore) {
		this.maxScore = maxScore;
	}
	
	
}
