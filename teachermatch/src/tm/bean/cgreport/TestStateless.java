package tm.bean.cgreport;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="teststateless")
public class TestStateless implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1896462818480773326L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Integer id;	
	Integer no;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getNo() {
		return no;
	}
	public void setNo(Integer no) {
		this.no = no;
	}
	
	
}
