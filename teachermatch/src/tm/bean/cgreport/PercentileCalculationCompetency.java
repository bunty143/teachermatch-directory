package tm.bean.cgreport;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;

@Entity
@Table(name="percentilecalculationcompetency")
public class PercentileCalculationCompetency implements Comparable<PercentileCalculationCompetency>,Serializable {
	
	private static final long serialVersionUID = -6172683161238804773L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer percentileCalcId;	
	@ManyToOne
	@JoinColumn(name="competencyId",referencedColumnName="competencyId")
	private CompetencyMaster competencyMaster;
	private Integer score;
	private Integer frequency;
	private Integer cumulativeFrequency;
	private Double percentile;
	private Double zValue;
	private Double tValue;
	private String status;
	private Date createdDateTime;
	
	public Integer getPercentileCalcId() {
		return percentileCalcId;
	}
	public void setPercentileCalcId(Integer percentileCalcId) {
		this.percentileCalcId = percentileCalcId;
	}

	public CompetencyMaster getCompetencyMaster() {
		return competencyMaster;
	}
	public void setCompetencyMaster(CompetencyMaster competencyMaster) {
		this.competencyMaster = competencyMaster;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}
	public Integer getCumulativeFrequency() {
		return cumulativeFrequency;
	}
	public void setCumulativeFrequency(Integer cumulativeFrequency) {
		this.cumulativeFrequency = cumulativeFrequency;
	}
	public Double getPercentile() {
		return percentile;
	}
	public void setPercentile(Double percentile) {
		this.percentile = percentile;
	}
	public Double getzValue() {
		return zValue;
	}
	public void setzValue(Double zValue) {
		this.zValue = zValue;
	}
	public Double gettValue() {
		return tValue;
	}
	public void settValue(Double tValue) {
		this.tValue = tValue;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public int compareTo(PercentileCalculationCompetency pcal) {
		return this.score.compareTo(pcal.score);		
	}
}
