package tm.bean.cgreport;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.master.DomainMaster;

@Entity
@Table(name="percentilescorebyjob")
public class PercentileScoreByJob implements Comparable<PercentileScoreByJob>,Serializable {
	
	private static final long serialVersionUID = 729195087532395628L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer percentileId;	
	
	@ManyToOne
	@JoinColumn(name="domainId",referencedColumnName="domainId")
	private DomainMaster domainMaster;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
 	private Integer score;
	private Integer frequency;
	private Integer cumulativeFrequency;
	private Double percentile;
	private Double zValue;
	private Double tValue;
	private String status;
	private Date createdDateTime;
	
	public Integer getPercentileId() {
		return percentileId;
	}
	public void setPercentileId(Integer percentileId) {
		this.percentileId = percentileId;
	}	
	public DomainMaster getDomainMaster() {
		return domainMaster;
	}
	public void setDomainMaster(DomainMaster domainMaster) {
		this.domainMaster = domainMaster;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Integer getFrequency() {
		return frequency;
	}
	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}
	public Integer getCumulativeFrequency() {
		return cumulativeFrequency;
	}
	public void setCumulativeFrequency(Integer cumulativeFrequency) {
		this.cumulativeFrequency = cumulativeFrequency;
	}
	public Double getPercentile() {
		return percentile;
	}
	public void setPercentile(Double percentile) {
		this.percentile = percentile;
	}
	public Double getzValue() {
		return zValue;
	}
	public void setzValue(Double zValue) {
		this.zValue = zValue;
	}
	public Double gettValue() {
		return tValue;
	}
	public void settValue(Double tValue) {
		this.tValue = tValue;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime){
		this.createdDateTime = createdDateTime;
	}
	public int compareTo(PercentileScoreByJob pcal) {
		return this.score.compareTo(pcal.score);		
	}
}
