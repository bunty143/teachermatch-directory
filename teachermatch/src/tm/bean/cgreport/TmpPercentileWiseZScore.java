package tm.bean.cgreport;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="tmppercentilewisezscore")
public class TmpPercentileWiseZScore implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 240988601342959796L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private Double percentile;
	private Double zScore;
	
	
	public Double getPercentile() {
		return percentile;
	}
	public void setPercentile(Double percentile) {
		this.percentile = percentile;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getzScore() {
		return zScore;
	}
	public void setzScore(Double zScore) {
		this.zScore = zScore;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
