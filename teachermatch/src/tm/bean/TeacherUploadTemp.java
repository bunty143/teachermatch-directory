package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.QuestionOptions;
import tm.bean.assessment.QuestionsPool;

@Entity
@Table(name="teacheruploadtemp")
public class TeacherUploadTemp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3164310493217515497L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long teachertempid;
	private String date_applied;
	private String job_id;      
	private String user_first_name;  
	private String user_last_name;  
	private String user_email;  
	private String sessionid;  
	private Date date;
	
	public String getDate_applied() {
		return date_applied;
	}
	public void setDate_applied(String dateApplied) {
		date_applied = dateApplied;
	}
	private String errortext;
	private Integer exist_teacherId;
	
	public Integer getExist_teacherId() {
		return exist_teacherId;
	}
	public void setExist_teacherId(Integer existTeacherId) {
		exist_teacherId = existTeacherId;
	}
	public String getErrortext() {
		return errortext;
	}
	public void setErrortext(String errortext) {
		this.errortext = errortext;
	}
	public Long getTeachertempid() {
		return teachertempid;
	}
	public void setTeachertempid(Long teachertempid) {
		this.teachertempid = teachertempid;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String jobId) {
		job_id = jobId;
	}
	public String getUser_first_name() {
		return user_first_name;
	}
	public void setUser_first_name(String userFirstName) {
		user_first_name = userFirstName;
	}
	public String getUser_last_name() {
		return user_last_name;
	}
	public void setUser_last_name(String userLastName) {
		user_last_name = userLastName;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String userEmail) {
		user_email = userEmail;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	

}
