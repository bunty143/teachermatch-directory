package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.QuestionTypeMaster;

@Entity(name="teacheranswerdetail")
public class TeacherAnswerDetail implements Serializable{

	private static final long serialVersionUID = 5621930841950801989L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer answerId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	private Double questionWeightage;
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster questionTypeMaster;
	@ManyToOne
	@JoinColumn(name="teacherAssessmentQuestionId",referencedColumnName="teacherAssessmentQuestionId")
	private TeacherAssessmentQuestion teacherAssessmentQuestion;
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentDetail;
	@ManyToOne
	@JoinColumn(name="teacherAssessmentId",referencedColumnName="teacherAssessmentId")
	private TeacherAssessmentdetail teacherAssessmentdetail;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	private String selectedOptions;
	private String insertedText;
	private String insertedRanks;
	private String optionScore;
	private Double totalScore;
	private Double maxMarks;
	private Date createdDateTime;
	private Integer questionOptionId;
	private Integer questionOptionTag;
	
	public Integer getAnswerId() {
		return answerId;
	}
	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public Double getQuestionWeightage() {
		return questionWeightage;
	}
	public void setQuestionWeightage(Double questionWeightage) {
		this.questionWeightage = questionWeightage;
	}
	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}
	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}
	public TeacherAssessmentQuestion getTeacherAssessmentQuestion() {
		return teacherAssessmentQuestion;
	}
	public void setTeacherAssessmentQuestion(
			TeacherAssessmentQuestion teacherAssessmentQuestion) {
		this.teacherAssessmentQuestion = teacherAssessmentQuestion;
	}
	public AssessmentDetail getAssessmentDetail() {
		return assessmentDetail;
	}
	public void setAssessmentDetail(AssessmentDetail assessmentDetail) {
		this.assessmentDetail = assessmentDetail;
	}
	public TeacherAssessmentdetail getTeacherAssessmentdetail() {
		return teacherAssessmentdetail;
	}
	public void setTeacherAssessmentdetail(
			TeacherAssessmentdetail teacherAssessmentdetail) {
		this.teacherAssessmentdetail = teacherAssessmentdetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public String getSelectedOptions() {
		return selectedOptions;
	}
	public void setSelectedOptions(String selectedOptions) {
		this.selectedOptions = selectedOptions;
	}
	public String getInsertedText() {
		return insertedText;
	}
	public void setInsertedText(String insertedText) {
		this.insertedText = insertedText;
	}
	public String getInsertedRanks() {
		return insertedRanks;
	}
	public void setInsertedRanks(String insertedRanks) {
		this.insertedRanks = insertedRanks;
	}
	public String getOptionScore() {
		return optionScore;
	}
	public void setOptionScore(String optionScore) {
		this.optionScore = optionScore;
	}
	public Double getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(Double totalScore) {
		this.totalScore = totalScore;
	}
	
	public Double getMaxMarks() {
		return maxMarks;
	}
	public void setMaxMarks(Double maxMarks) {
		this.maxMarks = maxMarks;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Integer getQuestionOptionId() {
		return questionOptionId;
	}
	public void setQuestionOptionId(Integer questionOptionId) {
		this.questionOptionId = questionOptionId;
	}
	public Integer getQuestionOptionTag() {
		return questionOptionTag;
	}
	public void setQuestionOptionTag(Integer questionOptionTag) {
		this.questionOptionTag = questionOptionTag;
	}
}
