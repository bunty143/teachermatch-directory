package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="epiquestiondetail")
public class EpiQuestionDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4299686234394025701L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer epiquestionId;
	private Integer itemId;
	private String itemType;
	private String itemDescription;
	private String answerOpt1;
	private String answerOpt2;
	private String answerOpt3;
	private String answerOpt4;
	private String answerOpt5;
	private String answerOpt6;
	
	public Integer getEpiquestionId() {
		return epiquestionId;
	}
	public void setEpiquestionId(Integer epiquestionId) {
		this.epiquestionId = epiquestionId;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public String getAnswerOpt1() {
		return answerOpt1;
	}
	public void setAnswerOpt1(String answerOpt1) {
		this.answerOpt1 = answerOpt1;
	}
	public String getAnswerOpt2() {
		return answerOpt2;
	}
	public void setAnswerOpt2(String answerOpt2) {
		this.answerOpt2 = answerOpt2;
	}
	public String getAnswerOpt3() {
		return answerOpt3;
	}
	public void setAnswerOpt3(String answerOpt3) {
		this.answerOpt3 = answerOpt3;
	}
	public String getAnswerOpt4() {
		return answerOpt4;
	}
	public void setAnswerOpt4(String answerOpt4) {
		this.answerOpt4 = answerOpt4;
	}
	public String getAnswerOpt5() {
		return answerOpt5;
	}
	public void setAnswerOpt5(String answerOpt5) {
		this.answerOpt5 = answerOpt5;
	}
	public String getAnswerOpt6() {
		return answerOpt6;
	}
	public void setAnswerOpt6(String answerOpt6) {
		this.answerOpt6 = answerOpt6;
	}
	
	

	
}
