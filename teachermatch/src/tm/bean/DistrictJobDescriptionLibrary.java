package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.sun.jna.platform.win32.Netapi32Util.User;

import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="districtjobdescriptionlibrary")
public class DistrictJobDescriptionLibrary implements Serializable {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtjobdescriptionlibraryId;
	
	@ManyToOne
	@JoinColumn(name="districtId", referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobcategoryId", referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	
	private String jobDescriptionName;
	private String jobDescription;
	
	
	@OneToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBy;
	
	
	@OneToOne
	@JoinColumn(name="lastUpdateBy",referencedColumnName="userId")
	private UserMaster lastUpdateBy;
	
	private String status;
	private Date createdDateTime;
	private Date updateDateTime;

	
	//......................getter and setter.......................
	
	public Integer getDistrictjobdescriptionlibraryId() {
		return districtjobdescriptionlibraryId;
	}
	public void setDistrictjobdescriptionlibraryId(
			Integer districtjobdescriptionlibraryId) {
		this.districtjobdescriptionlibraryId = districtjobdescriptionlibraryId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}
	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}
	public String getJobDescriptionName() {
		return jobDescriptionName;
	}
	public void setJobDescriptionName(String jobDescriptionName) {
		this.jobDescriptionName = jobDescriptionName;
	}
	public String getJobDescription() {
		return jobDescription;
	}
	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}
	public UserMaster getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(UserMaster createdBy) {
		this.createdBy = createdBy;
	}
	public UserMaster getLastUpdateBy() {
		return lastUpdateBy;
	}
	public void setLastUpdateBy(UserMaster lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Date getUpdateDateTime() {
		return updateDateTime;
	}
	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
	
	
	
	
}
