package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="uploadfacilitatortemp")
public class FacilitatorUploadTemp implements Serializable{
	private static final long serialVersionUID = 960324534143489961L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    int tempfacilitatorId;
	String firstName,lastName,emailId,sessionId,errorText;
	public String getErrorText() {
		return errorText;
	}
	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}
	public int getTempfacilitatorId() {
		return tempfacilitatorId;
	}
	public void setTempfacilitatorId(int tempfacilitatorId) {
		this.tempfacilitatorId = tempfacilitatorId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


}
