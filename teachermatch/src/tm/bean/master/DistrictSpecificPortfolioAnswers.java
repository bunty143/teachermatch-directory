package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;

@Entity
@Table(name="districtspecificportfolioanswers")
public class DistrictSpecificPortfolioAnswers implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4995768947073576883L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer answerId; 	
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	/*@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")*/
	//private Integer headQuarterMaster;
	private Integer headQuarterId;
	
	/*@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")*/
	//private  Integer branchMaster;
	private  Integer branchId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster  jobCategoryMaster;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	@ManyToOne
	@JoinColumn(name="universityId",referencedColumnName="universityId")
	private UniversityMaster universityId;
	
	@ManyToOne
	@JoinColumn(name="questionId",referencedColumnName="questionId")
	private DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions;
	
	private String question;
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster  questionTypeMaster;
	
	private String questionType;
	private String selectedOptions;
	private String questionOption;
	private String insertedText;
	private String fileName;
	
	
	private Boolean isValidAnswer; 	
	private Boolean isActive; 	
	private String insertedRanks;
	private String optionScore;
	private Double totalScore;
	private Double maxMarks;
	private Integer sliderScore;
	/*@ManyToOne
	@JoinColumn(name="scoreBy",referencedColumnName="userId")*/
	private Integer scoreBy;	
	private Date scoreDateTime;
	
	private Integer updatedBy;
	
	private Date updatedDateTime;
	
	private Date createdDateTime;
	
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Transient
	private Long schoolIdTemp;

	public String getInsertedRanks() {
		return insertedRanks;
	}

	public void setInsertedRanks(String insertedRanks) {
		this.insertedRanks = insertedRanks;
	}

	public String getOptionScore() {
		return optionScore;
	}

	public void setOptionScore(String optionScore) {
		this.optionScore = optionScore;
	}

	public Double getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(Double totalScore) {
		this.totalScore = totalScore;
	}

	public Double getMaxMarks() {
		return maxMarks;
	}

	public void setMaxMarks(Double maxMarks) {
		this.maxMarks = maxMarks;
	}

	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public Boolean getIsValidAnswer() {
		return isValidAnswer;
	}

	public void setIsValidAnswer(Boolean isValidAnswer) {
		this.isValidAnswer = isValidAnswer;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getAnswerId() {
		return answerId;
	}

	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}

	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}

	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}

	public DistrictSpecificPortfolioQuestions getDistrictSpecificPortfolioQuestions() {
		return districtSpecificPortfolioQuestions;
	}

	public void setDistrictSpecificPortfolioQuestions(
			DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions) {
		this.districtSpecificPortfolioQuestions = districtSpecificPortfolioQuestions;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}

	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getSelectedOptions() {
		return selectedOptions;
	}

	public void setSelectedOptions(String selectedOptions) {
		this.selectedOptions = selectedOptions;
	}

	public String getQuestionOption() {
		return questionOption;
	}

	public void setQuestionOption(String questionOption) {
		this.questionOption = questionOption;
	}

	public String getInsertedText() {
		return insertedText;
	}

	public void setInsertedText(String insertedText) {
		this.insertedText = insertedText;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public Integer getSliderScore() {
		return sliderScore;
	}

	public void setSliderScore(Integer sliderScore) {
		this.sliderScore = sliderScore;
	}		

public Integer getScoreBy() {
		return scoreBy;
	}

	public void setScoreBy(Integer scoreBy) {
		this.scoreBy = scoreBy;
	}

		public Date getScoreDateTime() {
		return scoreDateTime;
	}

	public void setScoreDateTime(Date scoreDateTime) {
		this.scoreDateTime = scoreDateTime;
	}

	public Long getSchoolIdTemp() {
		return schoolIdTemp;
	}

	public void setSchoolIdTemp(Long schoolIdTemp) {
		this.schoolIdTemp = schoolIdTemp;
	}

	public UniversityMaster getUniversityId() {
		return universityId;
	}

	public void setUniversityId(UniversityMaster universityId) {
		this.universityId = universityId;
	}

	public Integer getHeadQuarterId() {
		return headQuarterId;
	}

	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	/*public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}*/
	
	
	
}
