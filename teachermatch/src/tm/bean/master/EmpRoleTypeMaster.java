package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="emproletypemaster")
public class EmpRoleTypeMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7219436846666247841L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer empRoleTypeId;     
	private String empRoleTypeName;     
	private String status;
	public Integer getEmpRoleTypeId() {
		return empRoleTypeId;
	}
	public void setEmpRoleTypeId(Integer empRoleTypeId) {
		this.empRoleTypeId = empRoleTypeId;
	}
	public String getEmpRoleTypeName() {
		return empRoleTypeName;
	}
	public void setEmpRoleTypeName(String empRoleTypeName) {
		this.empRoleTypeName = empRoleTypeName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
}
