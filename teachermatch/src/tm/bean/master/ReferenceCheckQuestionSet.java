package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.user.UserMaster;

@Entity
@Table(name="referencecheckquestionset")
public class ReferenceCheckQuestionSet implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 765780752444863874L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer referenceQuestionId;
	
	@ManyToOne
	@JoinColumn(name="questionSetID",referencedColumnName="ID")
	private ReferenceQuestionSets referenceQuestionSets;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	 
	private Integer headQuarterId;
	 
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	private Date createdDateTime;

	
	
	
	 
	
	
	
	
	public Integer getReferenceQuestionId() {
		return referenceQuestionId;
	}

	public void setReferenceQuestionId(Integer referenceQuestionId) {
		this.referenceQuestionId = referenceQuestionId;
	}

	public ReferenceQuestionSets getReferenceQuestionSets() {
		return referenceQuestionSets;
	}

	public void setReferenceQuestionSets(ReferenceQuestionSets referenceQuestionSets) {
		this.referenceQuestionSets = referenceQuestionSets;
	}

	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}

	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	
	 
 
	public Integer getHeadQuarterId() {
		return headQuarterId;
	}

	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}


}
