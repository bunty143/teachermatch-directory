package tm.bean.master;

import java.io.Serializable;
import java.util.Comparator;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.user.UserMaster;

@Entity
@Table(name="districtschools")
public class DistrictSchools implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3273541660965012618L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtSchoolId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	private String schoolName;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBy;
	
	private Date createdDateTime;
	
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public Integer getDistrictSchoolId() {
		return districtSchoolId;
	}
	public void setDistrictSchoolId(Integer districtSchoolId) {
		this.districtSchoolId = districtSchoolId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public UserMaster getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(UserMaster createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
	
	/* @Start
	 * @Ashish
	 * @Description :: this code using when districtId.equals("0") || districtId.equals("") in JobsBoard {Not Tested} 
	 * */
		public static Comparator<DistrictSchools> getCompSchoolMaster() {
			return compSchoolMaster;
		}
		public static void setCompSchoolMaster(Comparator<DistrictSchools> compSchoolMaster) {
			DistrictSchools.compSchoolMaster = compSchoolMaster;
		}
	
		public static Comparator<DistrictSchools> compSchoolMaster   = new Comparator<DistrictSchools>() 
		{
			public int compare(DistrictSchools school1, DistrictSchools school2) {
			
				System.out.println(" inside DistrictSchools compare.....");
				
				System.out.println(" school1 :: "+school1.getSchoolMaster().getSchoolName());
				System.out.println(" school2 :: "+school2.getSchoolName());
				
				
				String schoolName1= school1.getSchoolName();
				String schoolName2= school2.getSchoolName();
				
				return schoolName1.compareTo(schoolName2);
			}		
		};
	/* @End
	 * @Ashish
	 * @Description :: this code using when districtId.equals("0") || districtId.equals("") in JobsBoard {Not Tested} 
	 * */
	
	
	
	@Override
	public boolean equals(Object object){
		if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof DistrictSchools))return false;
	    DistrictSchools districtSchool= (DistrictSchools)object;
	  
	    if(this.districtSchoolId.equals(districtSchool.getDistrictSchoolId())){
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}
	
	@Override
	public int hashCode(){
		return new Integer(""+districtSchoolId);
	}
}
