package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;

@Entity
@Table(name="affidavitmaster")
public class AffidavitMaster implements Serializable{

	/**
	 * Ram Nath
	 * 
	 */
	private static final long serialVersionUID = 8589642766135184408L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer affidavitId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	
	private String affidavitheading;
	
	@Column
	private String content;
	@Column(length=1)
	private String status;
	@Column
	private Date createdDateTime;
	
	public Integer getAffidavitId() {
		return affidavitId;
	}
	public void setAffidavitId(Integer affidavitId) {
		this.affidavitId = affidavitId;
	}
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getAffidavitheading() {
		return affidavitheading;
	}
	public void setAffidavitheading(String affidavitheading) {
		this.affidavitheading = affidavitheading;
	}
	
	
	
}
