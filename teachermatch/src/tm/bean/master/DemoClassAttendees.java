package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobForTeacher;
import tm.bean.user.UserMaster;

@Entity
@Table(name="democlassattendees")
public class DemoClassAttendees implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2684343129761225759L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer attendeeId;
	
	@ManyToOne
	@JoinColumn(name="demoId",referencedColumnName="demoId")
	private DemoClassSchedule demoId;
	
	@ManyToOne
	@JoinColumn(name="inviteeId",referencedColumnName="userId")
	private UserMaster inviteeId;
	
	@ManyToOne
	@JoinColumn(name="hostId",referencedColumnName="userId")
	private UserMaster hostId;
	
	private String demoStatus;
	private Date demoDate;
	
	public Integer getAttendeeId() {
		return attendeeId;
	}
	public void setAttendeeId(Integer attendeeId) {
		this.attendeeId = attendeeId;
	}
	public DemoClassSchedule getDemoId() {
		return demoId;
	}
	public void setDemoId(DemoClassSchedule demoId) {
		this.demoId = demoId;
	}
	
	public UserMaster getInviteeId() {
		return inviteeId;
	}
	public void setInviteeId(UserMaster inviteeId) {
		this.inviteeId = inviteeId;
	}
	
	public UserMaster getHostId() {
		return hostId;
	}
	public void setHostId(UserMaster hostId) {
		this.hostId = hostId;
	}
	public String getDemoStatus() {
		return demoStatus;
	}
	public void setDemoStatus(String demoStatus) {
		this.demoStatus = demoStatus;
	}
	public Date getDemoDate() {
		return demoDate;
	}
	public void setDemoDate(Date demoDate) {
		this.demoDate = demoDate;
	}
	
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof JobForTeacher))return false;
	    DemoClassAttendees demoClassAttendees = (DemoClassAttendees)object;
	  
	    if(this.demoId.equals(demoClassAttendees.getDemoId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
}
