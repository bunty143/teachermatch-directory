package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;

@Entity
@Table(name="districtspecificrefchkanswers")
public class DistrictSpecificRefChkAnswers implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -291756657750765951L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer answerId; 	
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	private Integer headQuarterMasterId; 
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="questionId",referencedColumnName="questionId")
	private DistrictSpecificRefChkQuestions districtSpecificRefChkQuestions;
	
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster  questionTypeMaster;
	
	@ManyToOne
	@JoinColumn(name="answeredBy",referencedColumnName="userId")
	private UserMaster  answeredBy;
	
	private String selectedOptions;
	
	private String insertedText;
	private Boolean isValidAnswer;
	private Date createdDateTime;
	private String question;
	private String questionOption;
	private String questionType;
	private Boolean isActive;
	private Integer elerefAutoId;
	private Integer questionMaxScore;
	

	public Integer getQuestionMaxScore() {
		return questionMaxScore;
	}
	public void setQuestionMaxScore(Integer questionMaxScore) {
		this.questionMaxScore = questionMaxScore;
	}
	public Integer getElerefAutoId() {
		return elerefAutoId;
	}
	public void setElerefAutoId(Integer elerefAutoId) {
		this.elerefAutoId = elerefAutoId;
	}
	public Integer getAnswerId() {
		return answerId;
	}
	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	 
	public Integer getHeadQuarterMasterId() {
		return headQuarterMasterId;
	}
	public void setHeadQuarterMasterId(Integer headQuarterMasterId) {
		this.headQuarterMasterId = headQuarterMasterId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public DistrictSpecificRefChkQuestions getDistrictSpecificRefChkQuestions() {
		return districtSpecificRefChkQuestions;
	}
	public void setDistrictSpecificRefChkQuestions(
			DistrictSpecificRefChkQuestions districtSpecificRefChkQuestions) {
		this.districtSpecificRefChkQuestions = districtSpecificRefChkQuestions;
	}
	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}
	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}
	public String getSelectedOptions() {
		return selectedOptions;
	}
	public void setSelectedOptions(String selectedOptions) {
		this.selectedOptions = selectedOptions;
	}
	public String getInsertedText() {
		return insertedText;
	}
	public void setInsertedText(String insertedText) {
		this.insertedText = insertedText;
	}
	public Boolean getIsValidAnswer() {
		return isValidAnswer;
	}
	public void setIsValidAnswer(Boolean isValidAnswer) {
		this.isValidAnswer = isValidAnswer;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getQuestionOption() {
		return questionOption;
	}
	public void setQuestionOption(String questionOption) {
		this.questionOption = questionOption;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public UserMaster getAnsweredBy() {
		return answeredBy;
	}
	public void setAnsweredBy(UserMaster answeredBy) {
		this.answeredBy = answeredBy;
	}
	
	
	
}
