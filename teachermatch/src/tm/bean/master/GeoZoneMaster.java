package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="geozonemaster")
public class GeoZoneMaster implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3667091018747617850L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer geoZoneId;

	@ManyToOne
	@JoinColumn(name="districtId", referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private String geoZoneName;
	private String geoZoneCode;
	
	private String geoZoneDescription;
	private Integer noOfSchoolsInGeoZone;
	private Date updatedDateTime;
	private Date createdDateTime;
	private String status;
	
	@Transient
	private String[] schoolIds;
	
	@Transient
	private String[] schoolNames;
	
	public String[] getSchoolIds() {
		return schoolIds;
	}
	public void setSchoolIds(String[] schoolIds) {
		this.schoolIds = schoolIds;
	}
	public String[] getSchoolNames() {
		return schoolNames;
	}
	public void setSchoolNames(String[] schoolNames) {
		this.schoolNames = schoolNames;
	}
	public String getGeoZoneCode() {
		return geoZoneCode;
	}
	public void setGeoZoneCode(String geoZoneCode) {
		this.geoZoneCode = geoZoneCode;
	}
	public Integer getGeoZoneId() {
		return geoZoneId;
	}
	public void setGeoZoneId(Integer geoZoneId) {
		this.geoZoneId = geoZoneId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getGeoZoneName() {
		return geoZoneName;
	}
	public void setGeoZoneName(String geoZoneName) {
		this.geoZoneName = geoZoneName;
	}
	public String getGeoZoneDescription() {
		return geoZoneDescription;
	}
	public void setGeoZoneDescription(String geoZoneDescription) {
		this.geoZoneDescription = geoZoneDescription;
	}
	public Integer getNoOfSchoolsInGeoZone() {
		return noOfSchoolsInGeoZone;
	}
	public void setNoOfSchoolsInGeoZone(Integer noOfSchoolsInGeoZone) {
		this.noOfSchoolsInGeoZone = noOfSchoolsInGeoZone;
	}
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((geoZoneName == null) ? 0 : geoZoneName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeoZoneMaster other = (GeoZoneMaster) obj;
		if (geoZoneName == null) {
			if (other.geoZoneName != null)
				return false;
		} else if (!geoZoneName.equals(other.geoZoneName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return geoZoneName;
	}
	
}
