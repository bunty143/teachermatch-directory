package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="certificatenamemaster")
public class CertificateNameMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4749105190276485913L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer certNameId;     
	
	@ManyToOne
	@JoinColumn(name="certTypeId" , referencedColumnName="certTypeId")
	private CertificateTypeMaster certificateTypeMaster;     
	private String certName;     
	private Integer numberofScores;     
	private Boolean status;
	public Integer getCertNameId() {
		return certNameId;
	}
	public void setCertNameId(Integer certNameId) {
		this.certNameId = certNameId;
	}
	public CertificateTypeMaster getCertificateTypeMaster() {
		return certificateTypeMaster;
	}
	public void setCertificateTypeMaster(CertificateTypeMaster certificateTypeMaster) {
		this.certificateTypeMaster = certificateTypeMaster;
	}
	public String getCertName() {
		return certName;
	}
	public void setCertName(String certName) {
		this.certName = certName;
	}
	public Integer getNumberofScores() {
		return numberofScores;
	}
	public void setNumberofScores(Integer numberofScores) {
		this.numberofScores = numberofScores;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
}
