package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="currencymaster")
public class CurrencyMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7945638523311455254L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer currencyId; 	
	private String currencyName;
	private String currencyShortName;
	private String status;
	
	public Integer getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}
	public String getCurrencyName() {
		return currencyName;
	}
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
	public String getCurrencyShortName() {
		return currencyShortName;
	}
	public void setCurrencyShortName(String currencyShortName) {
		this.currencyShortName = currencyShortName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
