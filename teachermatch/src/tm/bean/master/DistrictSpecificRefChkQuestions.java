package tm.bean.master;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="districtspecificrefchkquestions")
public class DistrictSpecificRefChkQuestions implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1569346704883675258L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer questionId; 	
	

	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")    // @ Anurag
	private HeadQuarterMaster headQuarterMaster;
	

	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")			 // @ Anurag
	private BranchMaster branchMaster;
	
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private String question;
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster questionTypeMaster;
	private String questionInstructions;

	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private Date createdDateTime;
	private String status;
	private String questionExplanation;
	private Integer questionMaxScore;
	
	@OneToMany(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="questionId",referencedColumnName="questionId",insertable=false,updatable=false)
	private List<DistrictSpecificRefChkOptions> questionOptions;
	
	public Integer getQuestionMaxScore() {
		return questionMaxScore;
	}
	public void setQuestionMaxScore(Integer questionMaxScore) {
		this.questionMaxScore = questionMaxScore;
	}
	public List<DistrictSpecificRefChkOptions> getQuestionOptions() {
		return questionOptions;
	}
	public void setQuestionOptions(
			List<DistrictSpecificRefChkOptions> questionOptions) {
		this.questionOptions = questionOptions;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	 
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}
	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}
	public String getQuestionInstructions() {
		return questionInstructions;
	}
	public void setQuestionInstructions(String questionInstructions) {
		this.questionInstructions = questionInstructions;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getQuestionExplanation() {
		return questionExplanation;
	}
	public void setQuestionExplanation(String questionExplanation) {
		this.questionExplanation = questionExplanation;
	}
	
	
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof DistrictSpecificRefChkQuestions))return false;
	    DistrictSpecificRefChkQuestions jobOrder = (DistrictSpecificRefChkQuestions)object;
	  
	    if(this.questionId.equals(jobOrder.getQuestionId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+questionId);
	}
	
}
