package tm.bean.master;

import java.io.Serializable;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="applitrackdistricts")
public class ApplitrackDistricts implements Serializable 
{
	private static final long serialVersionUID = 8453856364089865830L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer appDistrictId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	private String userName;
	private String password;
	private String clientcode;
	private String apiURL;
	private String realm;
	private String defaultJobCategory;
	private Integer headQuarterId;
	private String eventType;
	
	public Integer getAppDistrictId() {
		return appDistrictId;
	}
	public void setAppDistrictId(Integer appDistrictId) {
		this.appDistrictId = appDistrictId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getApiURL() {
		return apiURL;
	}
	public void setApiURL(String apiURL) {
		this.apiURL = apiURL;
	}
	public String getRealm() {
		return realm;
	}
	public void setRealm(String realm) {
		this.realm = realm;
	}
	public String getDefaultJobCategory() {
		return defaultJobCategory;
	}
	public void setDefaultJobCategory(String defaultJobCategory) {
		this.defaultJobCategory = defaultJobCategory;
	}
	public Integer getHeadQuarterId() {
		return headQuarterId;
	}
	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public void setClientcode(String clientcode) {
		this.clientcode = clientcode;
	}
	public String getClientcode() {
		return clientcode;
	}
	
}
