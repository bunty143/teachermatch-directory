package tm.bean.master;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;

@Entity
@Table(name="degreemaster")
public class DegreeMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1264809657182727279L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long degreeId;     
	private String degreeName;
	private String degreeType;
	private String degreeShortName;
	private String status;
	
	public String getDegreeShortName() {
		return degreeShortName;
	}
	public void setDegreeShortName(String degreeShortName) {
		this.degreeShortName = degreeShortName;
	}
	public Long getDegreeId() {
		return degreeId;
	}
	public void setDegreeId(Long degreeId) {
		this.degreeId = degreeId;
	}
	public String getDegreeName() {
		return degreeName;
	}
	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}
	public String getDegreeType() {
		return degreeType;
	}
	public void setDegreeType(String degreeType) {
		this.degreeType = degreeType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof DegreeMaster))return false;
	    DegreeMaster degreeMaster = (DegreeMaster)object;
	  
	    if(this.degreeId.equals(degreeMaster.getDegreeId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	/*
	public static Comparator<DegreeMaster> jobForTeacherComparator = new Comparator<DegreeMaster>() {		
		public int compare(DegreeMaster degreemaster1, DegreeMaster degreemaster2) {
			String dm1= degreemaster1.getDegreeName();
			String dm2 = degreemaster2.getDegreeName();
		
			return dm1.compareTo(dm2);
		}		
	};
	*/
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+degreeId);
	}
	
}
