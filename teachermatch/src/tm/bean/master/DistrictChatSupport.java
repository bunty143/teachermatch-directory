package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="districtchatsupport ")
public class DistrictChatSupport implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8779230891325966054L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtChatSupportId;	
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;	
	private Boolean chatSupport;
	private Boolean showMe;
	private String status;
	private Date createdDateTime;
	public Integer getDistrictChatSupportId() {
		return districtChatSupportId;
	}
	public void setDistrictChatSupportId(Integer districtChatSupportId) {
		this.districtChatSupportId = districtChatSupportId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public Boolean getChatSupport() {
		return chatSupport;
	}
	public void setChatSupport(Boolean chatSupport) {
		this.chatSupport = chatSupport;
	}
	public Boolean getShowMe() {
		return showMe;
	}
	public void setShowMe(Boolean showMe) {
		this.showMe = showMe;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
	
	
}
