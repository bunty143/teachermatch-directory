package tm.bean.master;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.commons.CommonsMultipartFile;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4QuestionSets;
import tm.utility.Utility;

@Entity
@Table(name="futuredistrictmaster")
public class FutureDistrictMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 165852867838440702L;
	private Integer isZoneRequired;
	private Boolean areAllSchoolsInContract;     
	private String exitURL; 
	private int jobApplicationCriteriaForProspects;
	private Integer flagForURL; 
	private Integer flagForMessage; 
	private String exitMessage;     
	private Integer exclusivePeriod;
	private Integer canTMApproach;     
	private Date createdDateTime;	
	
	@ManyToOne
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private StateMaster stateId;	
	private String authKey;
	private String apiRedirectUrl;
	private Integer ncesStateId;     
	private String districtName;
	private String locationCode;
	private String phoneNumber;     
	private String address;                         
	private String zipCode;     
	private String type;                                             
	private Integer totalNoOfSchools;     
	private Integer totalNoOfTeachers;     
	private Integer totalNoOfStudents;	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtId;	    
	private String cityName;	
	private String alternatePhone;    
	private String faxNumber;     
	private String website;     
	private String displayName;     
	private String logoPath;     
	private String description;     
	private String dmName;     
	private String dmEmailAddress;     
	private String dmPhoneNumber;     
	private String dmPassword;     
	private String acName;     
	private String acEmailAddress;     
	private String acPhoneNumber; 
	private String amName;     
	private String amEmailAddress;     
	private String amPhoneNumber; 
	private Date initiatedOnDate;     
	private Date contractStartDate;     
	private Date contractEndDate; 
	private Integer schoolsUnderContract;     
	private Integer teachersUnderContract;     
	private Integer studentsUnderContract;     
	private Double annualSubsciptionAmount;
	private String status;
	private Integer pkOffered;
	private Integer kgOffered;
	private Integer g01Offered;
	private Integer g02Offered;
	private Integer g03Offered;
	private Integer g04Offered;
	private Integer g05Offered;
	private Integer g06Offered;
	private Integer g07Offered;
	private Integer g08Offered;
	private Integer g09Offered;
	private Integer g10Offered;
	private Integer g11Offered;
	private Integer g12Offered;
	private Integer noSchoolUnderContract;    
	private Integer allSchoolsUnderContract;    
	private Integer allGradeUnderContract;    
	private Integer selectedSchoolsUnderContract;    
	private String assessmentUploadURL;
	
	private CommonsMultipartFile 	logoPathFile;
	private CommonsMultipartFile 	assessmentUploadURLFile;
	
	private Integer postingOnDistrictWall;
	private Integer postingOnTMWall;
	private Integer allowMessageTeacher;
	private String emailForTeacher;
	private String hiringAuthority; 
	private Integer districtApproval; 

	private Integer candidateFeedNormScore;
	private Integer candidateFeedDaysOfNoActivity;
	private Boolean canSchoolOverrideCandidateFeed;
	
	private String distributionEmail;
	private Integer jobFeedCriticalJobActiveDays;
	private Integer jobFeedCriticalCandidateRatio;
	private Integer jobFeedCriticalNormScore;
	private String lblCriticialJobName;
	
	private Integer jobFeedAttentionJobActiveDays;
	private Boolean jobFeedAttentionJobNotFilled;
	private Integer jobFeedAttentionNormScore;
	private String lblAttentionJobName;
	
	@ManyToOne
	@JoinColumn(name="statusIdForSchoolPrivilege",referencedColumnName="statusId")
	private StatusMaster statusMaster;
	private Boolean isReqNoRequired;
	private Boolean isReqNoForHiring;
	private Boolean statusNotes;
	private Boolean displayTMDefaultJobCategory;
	private Boolean displayCommunication;
	private Boolean setAssociatedStatusToSetDPoints;
	private String accessDPoints;
	
	private Boolean canSchoolOverrideJobFeed;
	private Boolean writePrivilegeToSchool;
	
	private Boolean isPortfolioNeeded;
	private Integer isWeeklyCgReport;
	private Boolean isResearchDistrict;
	private Integer communicationsAccess;
	
	private Boolean displayAchievementScore;
	private Boolean displayTFA;
	private Boolean displayDemoClass;
	private Boolean displayJSI;
	private String textForDistrictSpecificQuestions;
	private Boolean displayYearsTeaching;
	private Boolean displayExpectedSalary;
	private Boolean displayFitScore;
	
	private Boolean statusPrivilegeForSchools;
	
	private Boolean resetQualificationIssuesPrivilegeToSchool;
	private Boolean isTeacherPoolOnLoad;
	private Boolean displayCGPA;
	private String latitude;
	private String longitude;
	//private boolean jobCompletionEmail;
	//private Integer defaultPDP;
	


	
	@ManyToOne
	@JoinColumn(name="secondaryStatusIdForSchoolPrivilege",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;
	
	private Boolean offerDistrictSpecificItems;
	private Boolean offerQualificationItems;
	private Boolean offerEPI;
	private Boolean offerJSI;
	private Boolean offerPortfolioNeeded;
	private Integer noOfDaysRestricted;
	private Boolean autoNotifyCandidateOnAttachingWithJob;
	
	private Boolean noEPI;
	
	private Boolean offerVirtualVideoInterview;
	

	@ManyToOne
	@JoinColumn(name="vviQuestionSet",referencedColumnName="ID")
	private I4QuestionSets i4QuestionSets;
	private Integer maxScoreForVVI;
	private Boolean sendAutoVVILink;
	private Boolean sACreateDistJOb;
	//private Boolean sAEditUpdateJob; 
	
	//private Boolean eeocRequired;
	//private Boolean jobCompletionNeeded;
	//private Boolean jobCompletedVVILink;
	
	//private Boolean selfServicePortfolioStatus;
	//private Integer selfServiceCustomQuestion;
	

	@ManyToOne
	@JoinColumn(name="candidateConsiderationSatusId",referencedColumnName="statusId")
	private StatusMaster statusMasterForCC;
	
	@ManyToOne
	@JoinColumn(name="candidateConsiderationSecondarySatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatusForCC;
	
	
	@ManyToOne
	@JoinColumn(name="statusIdForAutoVVILink",referencedColumnName="statusId")
	private StatusMaster statusMasterForVVI;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusIdForAutoVVILink",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatusForVVI;
	@ManyToOne
	@JoinColumn(name="qqsetid", referencedColumnName="ID")
	private QqQuestionSets qqQuestionSets;
	private Integer timeAllowedPerQuestion;
	private Integer VVIExpiresInDays;
	
	private Boolean jobAppliedDate;
	
	private Boolean offerAssessmentInviteOnly;
	private String districtAssessmentId;
	
	private Integer sendReferenceOnJobComplete;
	
	private String textForReferenceInstruction;
	
	private Boolean buildApprovalGroup;
	
	private String sendNotificationOnNegativeQQ;
	
	private Boolean qqThumbShowOrNot;
	
	private Integer showReferenceToSA;
	private Boolean displaySeniorityNumber;
	//private Integer showApprovalGropus;
	 
	@ManyToOne
	@JoinColumn(name="statusIdForeReferenceFinalize",referencedColumnName="statusId")
	private StatusMaster statusMasterForReference;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusIdForeReferenceFinalize",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatusForReference;
	
	@ManyToOne
	@JoinColumn(name="qqSetIdForOnboard", referencedColumnName="ID")
	private QqQuestionSets qqQuestionSetsForOnboarding;
	
	private Boolean approvalByPredefinedGroups;
	
	//private Boolean sACreateEvent;
	
	//private Boolean hrIntegrated;
	
	//private Boolean sendReminderForPortfolio;
	//private Integer reminderFrequencyInDaysPortfolio;
	//private Integer noOfReminderPortfolio;
	//private Integer reminderOfFirstFrequencyInDaysPortfolio;
	//private Integer statusIdReminderExpireActionPortfolio;
	//private String statusSendMailPortfolio;
	//private String secondaryStatusSendMailPortfolio;
	
	//private Boolean emailCredentials;
	
	//private Boolean districtEmailFlag;
	//private String epiSetting;
	//private String jsiSetting;
	//private String displayCNFGColumn;
	
	//private Boolean isCreateTags;
	//private Boolean isEditTags;
	//private Boolean isRemoveTags;
	//private Boolean isDistrictWideTags;
	//private Boolean isJobSpecificTags;
	//private Boolean isApplitrackIntegration;
	
	//timezone field in edit district account information page start
	//private String timezone;
	//private Boolean skipApprovalProcess;
	//private Boolean displaySubmenuAsaParentMenu;
/*	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getTimezone() {
		return timezone;
	}
	//timezone field in edit district account information page end
	
	*//******TPL-4823--- Add configuration option HireVue Integration for District start*****//*
	//public Boolean isHireVueIntegration;
	public Boolean getIsHireVueIntegration() {
		return isHireVueIntegration;
	}
	public void setIsHireVueIntegration(Boolean isHireVueIntegration) {
		this.isHireVueIntegration = isHireVueIntegration;
	}
	
	//public String hireVueAPIKey;
	public String getHireVueAPIKey() {
		return hireVueAPIKey;
	}

	public void setHireVueAPIKey(String hireVueAPIKey) {
		this.hireVueAPIKey = hireVueAPIKey;
	}
	*//******TPL-4823--- Add configuration option HireVue Integration for District end*****//*
	*//****TPL-4992 --- Enable district to set district-wide job closing time at manage district start*****//*
	//public String postingStartTime;
	//public String postingEndTime;
	public String getPostingStartTime() {
		return postingStartTime;
	}

	public void setPostingStartTime(String postingStartTime) {
		this.postingStartTime = postingStartTime;
	}

	public String getPostingEndTime() {
		return postingEndTime;
	}

	public void setPostingEndTime(String postingEndTime) {
		this.postingEndTime = postingEndTime;
	}
	*//******TPL-4992 --- Enable district to set district-wide job closing time at manage district end *****//*
	*//***** TPL-3973 ****************************************************************************//*
	//public Boolean candidatePoolRefresh;
	public Boolean getCandidatePoolRefresh() {
		return candidatePoolRefresh;
	}
	public void setCandidatePoolRefresh(Boolean candidatePoolRefresh) {
		this.candidatePoolRefresh = candidatePoolRefresh;
	}
	*//***** TPL-3973 ****************************************************************************//*

	public Boolean getDistrictEmailFlag() {
		return districtEmailFlag;
	}

	public void setDistrictEmailFlag(Boolean districtEmailFlag) {
		this.districtEmailFlag = districtEmailFlag;
	}

	public String getEpiSetting() {
		return epiSetting;
	}

	public void setEpiSetting(String epiSetting) {
		this.epiSetting = epiSetting;
	}

	public String getJsiSetting() {
		return jsiSetting;
	}

	public void setJsiSetting(String jsiSetting) {
		this.jsiSetting = jsiSetting;
	}

	public Boolean getEmailCredentials() {
		return emailCredentials;
	}

	public void setEmailCredentials(Boolean emailCredentials) {
		this.emailCredentials = emailCredentials;
	}

	//private Boolean displayCandidateTOMosaic;
	//private Boolean includeState;
	//private Boolean includeZipCode;
	//private Boolean includeZone;
	//private Boolean autoInactiveOrNot;
	
	public Boolean getDisplayCandidateTOMosaic() {
		return displayCandidateTOMosaic;
	}

	public void setDisplayCandidateTOMosaic(Boolean displayCandidateTOMosaic) {
		this.displayCandidateTOMosaic = displayCandidateTOMosaic;
	}

	public Boolean getSendReminderForPortfolio() {
		return sendReminderForPortfolio;
	}

	public void setSendReminderForPortfolio(Boolean sendReminderForPortfolio) {
		this.sendReminderForPortfolio = sendReminderForPortfolio;
	}

	public Integer getReminderFrequencyInDaysPortfolio() {
		return reminderFrequencyInDaysPortfolio;
	}

	public void setReminderFrequencyInDaysPortfolio(
			Integer reminderFrequencyInDaysPortfolio) {
		this.reminderFrequencyInDaysPortfolio = reminderFrequencyInDaysPortfolio;
	}

	public Integer getNoOfReminderPortfolio() {
		return noOfReminderPortfolio;
	}

	public void setNoOfReminderPortfolio(Integer noOfReminderPortfolio) {
		this.noOfReminderPortfolio = noOfReminderPortfolio;
	}

	public Integer getReminderOfFirstFrequencyInDaysPortfolio() {
		return reminderOfFirstFrequencyInDaysPortfolio;
	}

	public void setReminderOfFirstFrequencyInDaysPortfolio(
			Integer reminderOfFirstFrequencyInDaysPortfolio) {
		this.reminderOfFirstFrequencyInDaysPortfolio = reminderOfFirstFrequencyInDaysPortfolio;
	}

	public Integer getStatusIdReminderExpireActionPortfolio() {
		return statusIdReminderExpireActionPortfolio;
	}

	public void setStatusIdReminderExpireActionPortfolio(
			Integer statusIdReminderExpireActionPortfolio) {
		this.statusIdReminderExpireActionPortfolio = statusIdReminderExpireActionPortfolio;
	}

	public String getStatusSendMailPortfolio() {
		return statusSendMailPortfolio;
	}

	public void setStatusSendMailPortfolio(String statusSendMailPortfolio) {
		this.statusSendMailPortfolio = statusSendMailPortfolio;
	}
	
	public String getSecondaryStatusSendMailPortfolio() {
		return secondaryStatusSendMailPortfolio;
	}

	public void setSecondaryStatusSendMailPortfolio(
			String secondaryStatusSendMailPortfolio) {
		this.secondaryStatusSendMailPortfolio = secondaryStatusSendMailPortfolio;
	}

	public Boolean getsACreateEvent() {
		return sACreateEvent;
	}

	public void setsACreateEvent(Boolean sACreateEvent) {
		this.sACreateEvent = sACreateEvent;
	}*/

	public Boolean getApprovalByPredefinedGroups() {
		return approvalByPredefinedGroups;
	}

	public void setApprovalByPredefinedGroups(Boolean approvalByPredefinedGroups) {
		this.approvalByPredefinedGroups = approvalByPredefinedGroups;
	}

	 

	public String getSendNotificationOnNegativeQQ() {
		return sendNotificationOnNegativeQQ;
	}
	
	public void setSendNotificationOnNegativeQQ(String sendNotificationOnNegativeQQ) {
		this.sendNotificationOnNegativeQQ = sendNotificationOnNegativeQQ;
	}

	public Boolean getBuildApprovalGroup() {
		return buildApprovalGroup;
	}

	public void setBuildApprovalGroup(Boolean buildApprovalGroup) {
		this.buildApprovalGroup = buildApprovalGroup;
	}

	public Integer getSendReferenceOnJobComplete() {
		return sendReferenceOnJobComplete;
	}

	public void setSendReferenceOnJobComplete(Integer sendReferenceOnJobComplete) {
		this.sendReferenceOnJobComplete = sendReferenceOnJobComplete;
	}
	
	public Integer getShowReferenceToSA() {
		return showReferenceToSA;
	}

	public void setShowReferenceToSA(Integer showReferenceToSA) {
		this.showReferenceToSA = showReferenceToSA;
	}

	 

	 

	public String getTextForReferenceInstruction() {
		return textForReferenceInstruction;
	}

	public void setTextForReferenceInstruction(String textForReferenceInstruction) {
		this.textForReferenceInstruction = textForReferenceInstruction;
	}

	@Transient
	String [] chkStatusMaster;
	
	@Transient
	String [] chkSecondaryStatusName;
	
	@Transient
	String [] chkDStatusMaster;
	
	@Transient
	String [] chkStatusMasterEml;
	

	@Transient
	String [] chkSecondaryStatusNameEml;
	
	@Transient
	boolean autoEmailRequired;
	
	private Boolean approvalBeforeGoLive;
	private Integer noOfApprovalNeeded;
	
	private Boolean sendReminderToIcompCandiates;
	private Integer reminderFrequencyInDays;
	private Integer noOfReminder;
	private Integer reminderOfFirstFrequencyInDays;
	
	 
	@ManyToOne
	@JoinColumn(name="statusIdReminderExpireAction",referencedColumnName="statusId")
	private StatusMaster statusIdReminderExpireAction;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusIdReminderExpireAction",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatusIdReminderExpireAction;
	
	private Boolean displayPhoneInterview;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
   

	private Integer questDistrict;

	

	
	
	public Integer getIsZoneRequired() {
		return isZoneRequired;
	}

	public void setIsZoneRequired(Integer isZoneRequired) {
		this.isZoneRequired = isZoneRequired;
	}

	public String getExitURL() {
		return exitURL;
	}

	public void setExitURL(String exitURL) {
		this.exitURL = exitURL;
	}

	public int getJobApplicationCriteriaForProspects() {
		return jobApplicationCriteriaForProspects;
	}

	public void setJobApplicationCriteriaForProspects(
			int jobApplicationCriteriaForProspects) {
		this.jobApplicationCriteriaForProspects = jobApplicationCriteriaForProspects;
	}

	public Integer getFlagForURL() {
		return flagForURL;
	}

	public void setFlagForURL(Integer flagForURL) {
		this.flagForURL = flagForURL;
	}

	public Integer getFlagForMessage() {
		return flagForMessage;
	}

	public void setFlagForMessage(Integer flagForMessage) {
		this.flagForMessage = flagForMessage;
	}

	public String getExitMessage() {
		return exitMessage;
	}

	public void setExitMessage(String exitMessage) {
		this.exitMessage = exitMessage;
	}

	public Integer getExclusivePeriod() {
		return exclusivePeriod;
	}

	public void setExclusivePeriod(Integer exclusivePeriod) {
		this.exclusivePeriod = exclusivePeriod;
	}

	public Integer getCanTMApproach() {
		return canTMApproach;
	}

	public void setCanTMApproach(Integer canTMApproach) {
		this.canTMApproach = canTMApproach;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public StateMaster getStateId() {
		return stateId;
	}

	public void setStateId(StateMaster stateId) {
		this.stateId = stateId;
	}

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	public String getApiRedirectUrl() {
		return apiRedirectUrl;
	}

	public void setApiRedirectUrl(String apiRedirectUrl) {
		this.apiRedirectUrl = apiRedirectUrl;
	}

	public Integer getNcesStateId() {
		return ncesStateId;
	}

	public void setNcesStateId(Integer ncesStateId) {
		this.ncesStateId = ncesStateId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getTotalNoOfSchools() {
		return totalNoOfSchools;
	}

	public void setTotalNoOfSchools(Integer totalNoOfSchools) {
		this.totalNoOfSchools = totalNoOfSchools;
	}

	public Integer getTotalNoOfTeachers() {
		return totalNoOfTeachers;
	}

	public void setTotalNoOfTeachers(Integer totalNoOfTeachers) {
		this.totalNoOfTeachers = totalNoOfTeachers;
	}

	public Integer getTotalNoOfStudents() {
		return totalNoOfStudents;
	}

	public void setTotalNoOfStudents(Integer totalNoOfStudents) {
		this.totalNoOfStudents = totalNoOfStudents;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAlternatePhone() {
		return alternatePhone;
	}

	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getLogoPath() {
		return logoPath;
	}

	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDmName() {
		return dmName;
	}

	public void setDmName(String dmName) {
		this.dmName = dmName;
	}

	public String getDmEmailAddress() {
		return dmEmailAddress;
	}

	public void setDmEmailAddress(String dmEmailAddress) {
		this.dmEmailAddress = dmEmailAddress;
	}

	public String getDmPhoneNumber() {
		return dmPhoneNumber;
	}

	public void setDmPhoneNumber(String dmPhoneNumber) {
		this.dmPhoneNumber = dmPhoneNumber;
	}

	public String getDmPassword() {
		return dmPassword;
	}

	public void setDmPassword(String dmPassword) {
		this.dmPassword = dmPassword;
	}

	public String getAcName() {
		return acName;
	}

	public void setAcName(String acName) {
		this.acName = acName;
	}

	public String getAcEmailAddress() {
		return acEmailAddress;
	}

	public void setAcEmailAddress(String acEmailAddress) {
		this.acEmailAddress = acEmailAddress;
	}

	public String getAcPhoneNumber() {
		return acPhoneNumber;
	}

	public void setAcPhoneNumber(String acPhoneNumber) {
		this.acPhoneNumber = acPhoneNumber;
	}

	public String getAmName() {
		return amName;
	}

	public void setAmName(String amName) {
		this.amName = amName;
	}

	public String getAmEmailAddress() {
		return amEmailAddress;
	}

	public void setAmEmailAddress(String amEmailAddress) {
		this.amEmailAddress = amEmailAddress;
	}

	public String getAmPhoneNumber() {
		return amPhoneNumber;
	}

	public void setAmPhoneNumber(String amPhoneNumber) {
		this.amPhoneNumber = amPhoneNumber;
	}

	public Date getInitiatedOnDate() {
		return initiatedOnDate;
	}

	public void setInitiatedOnDate(Date initiatedOnDate) {
		this.initiatedOnDate = initiatedOnDate;
	}

	public Date getContractStartDate() {
		return contractStartDate;
	}

	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}

	public Date getContractEndDate() {
		return contractEndDate;
	}

	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}

	public Integer getSchoolsUnderContract() {
		return schoolsUnderContract;
	}

	public void setSchoolsUnderContract(Integer schoolsUnderContract) {
		this.schoolsUnderContract = schoolsUnderContract;
	}

	public Integer getTeachersUnderContract() {
		return teachersUnderContract;
	}

	public void setTeachersUnderContract(Integer teachersUnderContract) {
		this.teachersUnderContract = teachersUnderContract;
	}

	public Integer getStudentsUnderContract() {
		return studentsUnderContract;
	}

	public void setStudentsUnderContract(Integer studentsUnderContract) {
		this.studentsUnderContract = studentsUnderContract;
	}

	public Double getAnnualSubsciptionAmount() {
		return annualSubsciptionAmount;
	}

	public void setAnnualSubsciptionAmount(Double annualSubsciptionAmount) {
		this.annualSubsciptionAmount = annualSubsciptionAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getPkOffered() {
		return pkOffered;
	}

	public void setPkOffered(Integer pkOffered) {
		this.pkOffered = pkOffered;
	}

	public Integer getKgOffered() {
		return kgOffered;
	}

	public void setKgOffered(Integer kgOffered) {
		this.kgOffered = kgOffered;
	}

	public Integer getG01Offered() {
		return g01Offered;
	}

	public void setG01Offered(Integer g01Offered) {
		this.g01Offered = g01Offered;
	}

	public Integer getG02Offered() {
		return g02Offered;
	}

	public void setG02Offered(Integer g02Offered) {
		this.g02Offered = g02Offered;
	}

	public Integer getG03Offered() {
		return g03Offered;
	}

	public void setG03Offered(Integer g03Offered) {
		this.g03Offered = g03Offered;
	}

	public Integer getG04Offered() {
		return g04Offered;
	}

	public void setG04Offered(Integer g04Offered) {
		this.g04Offered = g04Offered;
	}

	public Integer getG05Offered() {
		return g05Offered;
	}

	public void setG05Offered(Integer g05Offered) {
		this.g05Offered = g05Offered;
	}

	public Integer getG06Offered() {
		return g06Offered;
	}

	public void setG06Offered(Integer g06Offered) {
		this.g06Offered = g06Offered;
	}

	public Integer getG07Offered() {
		return g07Offered;
	}

	public void setG07Offered(Integer g07Offered) {
		this.g07Offered = g07Offered;
	}

	public Integer getG08Offered() {
		return g08Offered;
	}

	public void setG08Offered(Integer g08Offered) {
		this.g08Offered = g08Offered;
	}

	public Integer getG09Offered() {
		return g09Offered;
	}

	public void setG09Offered(Integer g09Offered) {
		this.g09Offered = g09Offered;
	}

	public Integer getG10Offered() {
		return g10Offered;
	}

	public void setG10Offered(Integer g10Offered) {
		this.g10Offered = g10Offered;
	}

	public Integer getG11Offered() {
		return g11Offered;
	}

	public void setG11Offered(Integer g11Offered) {
		this.g11Offered = g11Offered;
	}

	public Integer getG12Offered() {
		return g12Offered;
	}

	public void setG12Offered(Integer g12Offered) {
		this.g12Offered = g12Offered;
	}

	public Integer getNoSchoolUnderContract() {
		return noSchoolUnderContract;
	}

	public void setNoSchoolUnderContract(Integer noSchoolUnderContract) {
		this.noSchoolUnderContract = noSchoolUnderContract;
	}

	public Integer getAllSchoolsUnderContract() {
		return allSchoolsUnderContract;
	}

	public void setAllSchoolsUnderContract(Integer allSchoolsUnderContract) {
		this.allSchoolsUnderContract = allSchoolsUnderContract;
	}

	public Integer getAllGradeUnderContract() {
		return allGradeUnderContract;
	}

	public void setAllGradeUnderContract(Integer allGradeUnderContract) {
		this.allGradeUnderContract = allGradeUnderContract;
	}

	public Integer getSelectedSchoolsUnderContract() {
		return selectedSchoolsUnderContract;
	}

	public void setSelectedSchoolsUnderContract(Integer selectedSchoolsUnderContract) {
		this.selectedSchoolsUnderContract = selectedSchoolsUnderContract;
	}

	public String getAssessmentUploadURL() {
		return assessmentUploadURL;
	}

	public void setAssessmentUploadURL(String assessmentUploadURL) {
		this.assessmentUploadURL = assessmentUploadURL;
	}

	public CommonsMultipartFile getLogoPathFile() {
		return logoPathFile;
	}

	public void setLogoPathFile(CommonsMultipartFile logoPathFile) {
		this.logoPathFile = logoPathFile;
	}

	public CommonsMultipartFile getAssessmentUploadURLFile() {
		return assessmentUploadURLFile;
	}

	public void setAssessmentUploadURLFile(
			CommonsMultipartFile assessmentUploadURLFile) {
		this.assessmentUploadURLFile = assessmentUploadURLFile;
	}

	public Integer getPostingOnDistrictWall() {
		return postingOnDistrictWall;
	}

	public void setPostingOnDistrictWall(Integer postingOnDistrictWall) {
		this.postingOnDistrictWall = postingOnDistrictWall;
	}

	public Integer getPostingOnTMWall() {
		return postingOnTMWall;
	}

	public void setPostingOnTMWall(Integer postingOnTMWall) {
		this.postingOnTMWall = postingOnTMWall;
	}

	public Integer getAllowMessageTeacher() {
		return allowMessageTeacher;
	}

	public void setAllowMessageTeacher(Integer allowMessageTeacher) {
		this.allowMessageTeacher = allowMessageTeacher;
	}

	public String getEmailForTeacher() {
		return emailForTeacher;
	}

	public void setEmailForTeacher(String emailForTeacher) {
		this.emailForTeacher = emailForTeacher;
	}

	public String getHiringAuthority() {
		return hiringAuthority;
	}

	public void setHiringAuthority(String hiringAuthority) {
		this.hiringAuthority = hiringAuthority;
	}

	public Integer getDistrictApproval() {
		return districtApproval;
	}

	public void setDistrictApproval(Integer districtApproval) {
		this.districtApproval = districtApproval;
	}

	public Integer getCandidateFeedNormScore() {
		return candidateFeedNormScore;
	}

	public void setCandidateFeedNormScore(Integer candidateFeedNormScore) {
		this.candidateFeedNormScore = candidateFeedNormScore;
	}

	public Integer getCandidateFeedDaysOfNoActivity() {
		return candidateFeedDaysOfNoActivity;
	}

	public void setCandidateFeedDaysOfNoActivity(
			Integer candidateFeedDaysOfNoActivity) {
		this.candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity;
	}

	public Boolean getCanSchoolOverrideCandidateFeed() {
		return canSchoolOverrideCandidateFeed;
	}

	public void setCanSchoolOverrideCandidateFeed(
			Boolean canSchoolOverrideCandidateFeed) {
		this.canSchoolOverrideCandidateFeed = canSchoolOverrideCandidateFeed;
	}

	public String getDistributionEmail() {
		return distributionEmail;
	}

	public void setDistributionEmail(String distributionEmail) {
		this.distributionEmail = distributionEmail;
	}

	public Integer getJobFeedCriticalJobActiveDays() {
		return jobFeedCriticalJobActiveDays;
	}

	public void setJobFeedCriticalJobActiveDays(Integer jobFeedCriticalJobActiveDays) {
		this.jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays;
	}

	public Integer getJobFeedCriticalCandidateRatio() {
		return jobFeedCriticalCandidateRatio;
	}

	public void setJobFeedCriticalCandidateRatio(
			Integer jobFeedCriticalCandidateRatio) {
		this.jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio;
	}

	public Integer getJobFeedCriticalNormScore() {
		return jobFeedCriticalNormScore;
	}

	public void setJobFeedCriticalNormScore(Integer jobFeedCriticalNormScore) {
		this.jobFeedCriticalNormScore = jobFeedCriticalNormScore;
	}

	public String getLblCriticialJobName() {
		return lblCriticialJobName;
	}

	public void setLblCriticialJobName(String lblCriticialJobName) {
		this.lblCriticialJobName = lblCriticialJobName;
	}

	public Integer getJobFeedAttentionJobActiveDays() {
		return jobFeedAttentionJobActiveDays;
	}

	public void setJobFeedAttentionJobActiveDays(
			Integer jobFeedAttentionJobActiveDays) {
		this.jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays;
	}

	public Boolean getJobFeedAttentionJobNotFilled() {
		return jobFeedAttentionJobNotFilled;
	}

	public void setJobFeedAttentionJobNotFilled(Boolean jobFeedAttentionJobNotFilled) {
		this.jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled;
	}

	public Integer getJobFeedAttentionNormScore() {
		return jobFeedAttentionNormScore;
	}

	public void setJobFeedAttentionNormScore(Integer jobFeedAttentionNormScore) {
		this.jobFeedAttentionNormScore = jobFeedAttentionNormScore;
	}

	public String getLblAttentionJobName() {
		return lblAttentionJobName;
	}

	public void setLblAttentionJobName(String lblAttentionJobName) {
		this.lblAttentionJobName = lblAttentionJobName;
	}

	public StatusMaster getStatusMaster() {
		return statusMaster;
	}

	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}

	public Boolean getIsReqNoRequired() {
		return isReqNoRequired;
	}

	public void setIsReqNoRequired(Boolean isReqNoRequired) {
		this.isReqNoRequired = isReqNoRequired;
	}

	public Boolean getIsReqNoForHiring() {
		return isReqNoForHiring;
	}

	public void setIsReqNoForHiring(Boolean isReqNoForHiring) {
		this.isReqNoForHiring = isReqNoForHiring;
	}

	public Boolean getStatusNotes() {
		return statusNotes;
	}

	public void setStatusNotes(Boolean statusNotes) {
		this.statusNotes = statusNotes;
	}

	public Boolean getDisplayTMDefaultJobCategory() {
		return displayTMDefaultJobCategory;
	}

	public void setDisplayTMDefaultJobCategory(Boolean displayTMDefaultJobCategory) {
		this.displayTMDefaultJobCategory = displayTMDefaultJobCategory;
	}

	public Boolean getDisplayCommunication() {
		return displayCommunication;
	}

	public void setDisplayCommunication(Boolean displayCommunication) {
		this.displayCommunication = displayCommunication;
	}

	public Boolean getSetAssociatedStatusToSetDPoints() {
		return setAssociatedStatusToSetDPoints;
	}

	public void setSetAssociatedStatusToSetDPoints(
			Boolean setAssociatedStatusToSetDPoints) {
		this.setAssociatedStatusToSetDPoints = setAssociatedStatusToSetDPoints;
	}

	public String getAccessDPoints() {
		return accessDPoints;
	}

	public void setAccessDPoints(String accessDPoints) {
		this.accessDPoints = accessDPoints;
	}

	public Boolean getCanSchoolOverrideJobFeed() {
		return canSchoolOverrideJobFeed;
	}

	public void setCanSchoolOverrideJobFeed(Boolean canSchoolOverrideJobFeed) {
		this.canSchoolOverrideJobFeed = canSchoolOverrideJobFeed;
	}

	public Boolean getWritePrivilegeToSchool() {
		return writePrivilegeToSchool;
	}

	public void setWritePrivilegeToSchool(Boolean writePrivilegeToSchool) {
		this.writePrivilegeToSchool = writePrivilegeToSchool;
	}

	public Boolean getIsPortfolioNeeded() {
		return isPortfolioNeeded;
	}

	public void setIsPortfolioNeeded(Boolean isPortfolioNeeded) {
		this.isPortfolioNeeded = isPortfolioNeeded;
	}

	public Integer getIsWeeklyCgReport() {
		return isWeeklyCgReport;
	}

	public void setIsWeeklyCgReport(Integer isWeeklyCgReport) {
		this.isWeeklyCgReport = isWeeklyCgReport;
	}

	public Boolean getIsResearchDistrict() {
		return isResearchDistrict;
	}

	public void setIsResearchDistrict(Boolean isResearchDistrict) {
		this.isResearchDistrict = isResearchDistrict;
	}

	public Integer getCommunicationsAccess() {
		return communicationsAccess;
	}

	public void setCommunicationsAccess(Integer communicationsAccess) {
		this.communicationsAccess = communicationsAccess;
	}

	public Boolean getDisplayAchievementScore() {
		return displayAchievementScore;
	}

	public void setDisplayAchievementScore(Boolean displayAchievementScore) {
		this.displayAchievementScore = displayAchievementScore;
	}

	public Boolean getDisplayTFA() {
		return displayTFA;
	}

	public void setDisplayTFA(Boolean displayTFA) {
		this.displayTFA = displayTFA;
	}

	public Boolean getDisplayDemoClass() {
		return displayDemoClass;
	}

	public void setDisplayDemoClass(Boolean displayDemoClass) {
		this.displayDemoClass = displayDemoClass;
	}

	public Boolean getDisplayJSI() {
		return displayJSI;
	}

	public void setDisplayJSI(Boolean displayJSI) {
		this.displayJSI = displayJSI;
	}

	public String getTextForDistrictSpecificQuestions() {
		return textForDistrictSpecificQuestions;
	}

	public void setTextForDistrictSpecificQuestions(
			String textForDistrictSpecificQuestions) {
		this.textForDistrictSpecificQuestions = textForDistrictSpecificQuestions;
	}

	public Boolean getDisplayYearsTeaching() {
		return displayYearsTeaching;
	}

	public void setDisplayYearsTeaching(Boolean displayYearsTeaching) {
		this.displayYearsTeaching = displayYearsTeaching;
	}

	public Boolean getDisplayExpectedSalary() {
		return displayExpectedSalary;
	}

	public void setDisplayExpectedSalary(Boolean displayExpectedSalary) {
		this.displayExpectedSalary = displayExpectedSalary;
	}

	public Boolean getDisplayFitScore() {
		return displayFitScore;
	}

	public void setDisplayFitScore(Boolean displayFitScore) {
		this.displayFitScore = displayFitScore;
	}

	public Boolean getStatusPrivilegeForSchools() {
		return statusPrivilegeForSchools;
	}

	public void setStatusPrivilegeForSchools(Boolean statusPrivilegeForSchools) {
		this.statusPrivilegeForSchools = statusPrivilegeForSchools;
	}

	public Boolean getResetQualificationIssuesPrivilegeToSchool() {
		return resetQualificationIssuesPrivilegeToSchool;
	}

	public void setResetQualificationIssuesPrivilegeToSchool(
			Boolean resetQualificationIssuesPrivilegeToSchool) {
		this.resetQualificationIssuesPrivilegeToSchool = resetQualificationIssuesPrivilegeToSchool;
	}

	public Boolean getIsTeacherPoolOnLoad() {
		return isTeacherPoolOnLoad;
	}

	public void setIsTeacherPoolOnLoad(Boolean isTeacherPoolOnLoad) {
		this.isTeacherPoolOnLoad = isTeacherPoolOnLoad;
	}

	public Boolean getDisplayCGPA() {
		return displayCGPA;
	}

	public void setDisplayCGPA(Boolean displayCGPA) {
		this.displayCGPA = displayCGPA;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}

	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}

	public Boolean getOfferDistrictSpecificItems() {
		return offerDistrictSpecificItems;
	}

	public void setOfferDistrictSpecificItems(Boolean offerDistrictSpecificItems) {
		this.offerDistrictSpecificItems = offerDistrictSpecificItems;
	}

	public Boolean getOfferQualificationItems() {
		return offerQualificationItems;
	}

	public void setOfferQualificationItems(Boolean offerQualificationItems) {
		this.offerQualificationItems = offerQualificationItems;
	}

	public Boolean getOfferEPI() {
		return offerEPI;
	}

	public void setOfferEPI(Boolean offerEPI) {
		this.offerEPI = offerEPI;
	}

	public Boolean getOfferJSI() {
		return offerJSI;
	}

	public void setOfferJSI(Boolean offerJSI) {
		this.offerJSI = offerJSI;
	}

	public Boolean getOfferPortfolioNeeded() {
		return offerPortfolioNeeded;
	}

	public void setOfferPortfolioNeeded(Boolean offerPortfolioNeeded) {
		this.offerPortfolioNeeded = offerPortfolioNeeded;
	}

	public Integer getNoOfDaysRestricted() {
		return noOfDaysRestricted;
	}

	public void setNoOfDaysRestricted(Integer noOfDaysRestricted) {
		this.noOfDaysRestricted = noOfDaysRestricted;
	}

	public Boolean getAutoNotifyCandidateOnAttachingWithJob() {
		return autoNotifyCandidateOnAttachingWithJob;
	}

	public void setAutoNotifyCandidateOnAttachingWithJob(
			Boolean autoNotifyCandidateOnAttachingWithJob) {
		this.autoNotifyCandidateOnAttachingWithJob = autoNotifyCandidateOnAttachingWithJob;
	}

	public Boolean getNoEPI() {
		return noEPI;
	}

	public void setNoEPI(Boolean noEPI) {
		this.noEPI = noEPI;
	}

	public Boolean getOfferVirtualVideoInterview() {
		return offerVirtualVideoInterview;
	}

	public void setOfferVirtualVideoInterview(Boolean offerVirtualVideoInterview) {
		this.offerVirtualVideoInterview = offerVirtualVideoInterview;
	}

	public I4QuestionSets getI4QuestionSets() {
		return i4QuestionSets;
	}

	public void setI4QuestionSets(I4QuestionSets i4QuestionSets) {
		this.i4QuestionSets = i4QuestionSets;
	}

	public Integer getMaxScoreForVVI() {
		return maxScoreForVVI;
	}

	public void setMaxScoreForVVI(Integer maxScoreForVVI) {
		this.maxScoreForVVI = maxScoreForVVI;
	}

	public Boolean getSendAutoVVILink() {
		return sendAutoVVILink;
	}

	public void setSendAutoVVILink(Boolean sendAutoVVILink) {
		this.sendAutoVVILink = sendAutoVVILink;
	}

	public Boolean getsACreateDistJOb() {
		return sACreateDistJOb;
	}

	public void setsACreateDistJOb(Boolean sACreateDistJOb) {
		this.sACreateDistJOb = sACreateDistJOb;
	}

	public StatusMaster getStatusMasterForCC() {
		return statusMasterForCC;
	}

	public void setStatusMasterForCC(StatusMaster statusMasterForCC) {
		this.statusMasterForCC = statusMasterForCC;
	}

	public SecondaryStatus getSecondaryStatusForCC() {
		return secondaryStatusForCC;
	}

	public void setSecondaryStatusForCC(SecondaryStatus secondaryStatusForCC) {
		this.secondaryStatusForCC = secondaryStatusForCC;
	}

	public StatusMaster getStatusMasterForVVI() {
		return statusMasterForVVI;
	}

	public void setStatusMasterForVVI(StatusMaster statusMasterForVVI) {
		this.statusMasterForVVI = statusMasterForVVI;
	}

	public SecondaryStatus getSecondaryStatusForVVI() {
		return secondaryStatusForVVI;
	}

	public void setSecondaryStatusForVVI(SecondaryStatus secondaryStatusForVVI) {
		this.secondaryStatusForVVI = secondaryStatusForVVI;
	}

	public QqQuestionSets getQqQuestionSets() {
		return qqQuestionSets;
	}

	public void setQqQuestionSets(QqQuestionSets qqQuestionSets) {
		this.qqQuestionSets = qqQuestionSets;
	}

	public Integer getTimeAllowedPerQuestion() {
		return timeAllowedPerQuestion;
	}

	public void setTimeAllowedPerQuestion(Integer timeAllowedPerQuestion) {
		this.timeAllowedPerQuestion = timeAllowedPerQuestion;
	}

	public Integer getVVIExpiresInDays() {
		return VVIExpiresInDays;
	}

	public void setVVIExpiresInDays(Integer vVIExpiresInDays) {
		VVIExpiresInDays = vVIExpiresInDays;
	}

	public Boolean getJobAppliedDate() {
		return jobAppliedDate;
	}

	public void setJobAppliedDate(Boolean jobAppliedDate) {
		this.jobAppliedDate = jobAppliedDate;
	}

	public Boolean getOfferAssessmentInviteOnly() {
		return offerAssessmentInviteOnly;
	}

	public void setOfferAssessmentInviteOnly(Boolean offerAssessmentInviteOnly) {
		this.offerAssessmentInviteOnly = offerAssessmentInviteOnly;
	}

	public String getDistrictAssessmentId() {
		return districtAssessmentId;
	}

	public void setDistrictAssessmentId(String districtAssessmentId) {
		this.districtAssessmentId = districtAssessmentId;
	}

	public Boolean getQqThumbShowOrNot() {
		return qqThumbShowOrNot;
	}

	public void setQqThumbShowOrNot(Boolean qqThumbShowOrNot) {
		this.qqThumbShowOrNot = qqThumbShowOrNot;
	}

	public Boolean getDisplaySeniorityNumber() {
		return displaySeniorityNumber;
	}

	public void setDisplaySeniorityNumber(Boolean displaySeniorityNumber) {
		this.displaySeniorityNumber = displaySeniorityNumber;
	}

	public StatusMaster getStatusMasterForReference() {
		return statusMasterForReference;
	}

	public void setStatusMasterForReference(StatusMaster statusMasterForReference) {
		this.statusMasterForReference = statusMasterForReference;
	}

	public SecondaryStatus getSecondaryStatusForReference() {
		return secondaryStatusForReference;
	}

	public void setSecondaryStatusForReference(
			SecondaryStatus secondaryStatusForReference) {
		this.secondaryStatusForReference = secondaryStatusForReference;
	}

	public QqQuestionSets getQqQuestionSetsForOnboarding() {
		return qqQuestionSetsForOnboarding;
	}

	public void setQqQuestionSetsForOnboarding(
			QqQuestionSets qqQuestionSetsForOnboarding) {
		this.qqQuestionSetsForOnboarding = qqQuestionSetsForOnboarding;
	}

	public String[] getChkStatusMaster() {
		return chkStatusMaster;
	}

	public void setChkStatusMaster(String[] chkStatusMaster) {
		this.chkStatusMaster = chkStatusMaster;
	}

	public String[] getChkSecondaryStatusName() {
		return chkSecondaryStatusName;
	}

	public void setChkSecondaryStatusName(String[] chkSecondaryStatusName) {
		this.chkSecondaryStatusName = chkSecondaryStatusName;
	}

	public String[] getChkDStatusMaster() {
		return chkDStatusMaster;
	}

	public void setChkDStatusMaster(String[] chkDStatusMaster) {
		this.chkDStatusMaster = chkDStatusMaster;
	}

	public String[] getChkStatusMasterEml() {
		return chkStatusMasterEml;
	}

	public void setChkStatusMasterEml(String[] chkStatusMasterEml) {
		this.chkStatusMasterEml = chkStatusMasterEml;
	}

	public String[] getChkSecondaryStatusNameEml() {
		return chkSecondaryStatusNameEml;
	}

	public void setChkSecondaryStatusNameEml(String[] chkSecondaryStatusNameEml) {
		this.chkSecondaryStatusNameEml = chkSecondaryStatusNameEml;
	}

	public boolean isAutoEmailRequired() {
		return autoEmailRequired;
	}

	public void setAutoEmailRequired(boolean autoEmailRequired) {
		this.autoEmailRequired = autoEmailRequired;
	}

	public Boolean getApprovalBeforeGoLive() {
		return approvalBeforeGoLive;
	}

	public void setApprovalBeforeGoLive(Boolean approvalBeforeGoLive) {
		this.approvalBeforeGoLive = approvalBeforeGoLive;
	}

	public Integer getNoOfApprovalNeeded() {
		return noOfApprovalNeeded;
	}

	public void setNoOfApprovalNeeded(Integer noOfApprovalNeeded) {
		this.noOfApprovalNeeded = noOfApprovalNeeded;
	}

	public Boolean getSendReminderToIcompCandiates() {
		return sendReminderToIcompCandiates;
	}

	public void setSendReminderToIcompCandiates(Boolean sendReminderToIcompCandiates) {
		this.sendReminderToIcompCandiates = sendReminderToIcompCandiates;
	}

	public Integer getReminderFrequencyInDays() {
		return reminderFrequencyInDays;
	}

	public void setReminderFrequencyInDays(Integer reminderFrequencyInDays) {
		this.reminderFrequencyInDays = reminderFrequencyInDays;
	}

	public Integer getNoOfReminder() {
		return noOfReminder;
	}

	public void setNoOfReminder(Integer noOfReminder) {
		this.noOfReminder = noOfReminder;
	}

	public Integer getReminderOfFirstFrequencyInDays() {
		return reminderOfFirstFrequencyInDays;
	}

	public void setReminderOfFirstFrequencyInDays(
			Integer reminderOfFirstFrequencyInDays) {
		this.reminderOfFirstFrequencyInDays = reminderOfFirstFrequencyInDays;
	}

	public StatusMaster getStatusIdReminderExpireAction() {
		return statusIdReminderExpireAction;
	}

	public void setStatusIdReminderExpireAction(
			StatusMaster statusIdReminderExpireAction) {
		this.statusIdReminderExpireAction = statusIdReminderExpireAction;
	}

	public SecondaryStatus getSecondaryStatusIdReminderExpireAction() {
		return secondaryStatusIdReminderExpireAction;
	}

	public void setSecondaryStatusIdReminderExpireAction(
			SecondaryStatus secondaryStatusIdReminderExpireAction) {
		this.secondaryStatusIdReminderExpireAction = secondaryStatusIdReminderExpireAction;
	}

	public Boolean getDisplayPhoneInterview() {
		return displayPhoneInterview;
	}

	public void setDisplayPhoneInterview(Boolean displayPhoneInterview) {
		this.displayPhoneInterview = displayPhoneInterview;
	}

	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}

	public Integer getQuestDistrict() {
		return questDistrict;
	}

	public void setQuestDistrict(Integer questDistrict) {
		this.questDistrict = questDistrict;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return this.districtName;
	}

	
	public Boolean getAreAllSchoolsInContract() {
		return areAllSchoolsInContract;
	}

	public void setAreAllSchoolsInContract(Boolean areAllSchoolsInContract) {
		this.areAllSchoolsInContract = areAllSchoolsInContract;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FutureDistrictMaster other = (FutureDistrictMaster) obj;
		if (districtName == null) {
			if (other.districtName != null)
				return false;
		} else if (!districtName.equals(other.districtName))
			return false;
		return true;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((districtName == null) ? 0 : districtName.hashCode());
		return result;
	}

	

}
