package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="exclusiveperiodmaster")
public class ExclusivePeriodMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -96983822903033500L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long exclPeriodId;
	private Integer exclPeriod;
	private Date updatedDateTime;
	public Long getExclPeriodId() {
		return exclPeriodId;
	}
	public void setExclPeriodId(Long exclPeriodId) {
		this.exclPeriodId = exclPeriodId;
	}
	public Integer getExclPeriod() {
		return exclPeriod;
	}
	public void setExclPeriod(Integer exclPeriod) {
		this.exclPeriod = exclPeriod;
	}
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
	
	
}
