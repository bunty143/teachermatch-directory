package tm.bean.master;
 
import java.io.Serializable;
 
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
 
 
@Entity
@Table(name="qqquestionsetquestions")
public class QqQuestionsetQuestions implements Serializable{
 
    /**
     * 
     */
    private static final long serialVersionUID = 6618351714397394798L;
 
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer ID;
    
    @ManyToOne
    @JoinColumn(name="QuestionSetID",referencedColumnName="ID")
    private QqQuestionSets questionSets;
    
    @ManyToOne
    @JoinColumn(name="QuestionID",referencedColumnName="questionId")
    private DistrictSpecificQuestions districtSpecificQuestions;
    
    private Integer QuestionSequence;
 
    public Integer getID() {
        return ID;
    }
 
    public void setID(Integer iD) {
        ID = iD;
    }
    
    public QqQuestionSets getQuestionSets() {
		return questionSets;
	}

	public void setQuestionSets(QqQuestionSets questionSets) {
		this.questionSets = questionSets;
	}

	public DistrictSpecificQuestions getDistrictSpecificQuestions() {
        return districtSpecificQuestions;
    }
 
    public void setDistrictSpecificQuestions(
            DistrictSpecificQuestions districtSpecificQuestions) {
        this.districtSpecificQuestions = districtSpecificQuestions;
    }
 
    public Integer getQuestionSequence() {
        return QuestionSequence;
    }
 
    public void setQuestionSequence(Integer questionSequence) {
        QuestionSequence = questionSequence;
    }
    
}
