package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;

@Entity
@Table(name="jobwisepanelstatus")
public class JobWisePanelStatus implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3294383313376481648L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Integer jobPanelStatusId;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="statusId",referencedColumnName="statusId")
	private StatusMaster statusMaster;
	
	@ManyToOne 
	@JoinColumn(name="secondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;
	
	private Boolean panelStatus;

	public Integer getJobPanelStatusId() {
		return jobPanelStatusId;
	}

	public void setJobPanelStatusId(Integer jobPanelStatusId) {
		this.jobPanelStatusId = jobPanelStatusId;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public StatusMaster getStatusMaster() {
		return statusMaster;
	}

	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}

	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}

	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}

	public Boolean getPanelStatus() {
		return panelStatus;
	}

	public void setPanelStatus(Boolean panelStatus) {
		this.panelStatus = panelStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((panelStatus == null) ? 0 : panelStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobWisePanelStatus other = (JobWisePanelStatus) obj;
		if (panelStatus == null) {
			if (other.panelStatus != null)
				return false;
		} else if (!panelStatus.equals(other.panelStatus))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JobWisePanelStatus [panelStatus=" + panelStatus + "]";
	}

	
	
	
	
}
