package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="useremailnotifications")
public class UserEmailNotifications implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3763820607308546116L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer emailNotificationId;
	
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	private String notificationTabName;
	
	private String notificationShortName;
	
	Boolean notificationFlag;
	
	private Date createdDateTime;

	public Integer getEmailNotificationId() {
		return emailNotificationId;
	}

	public void setEmailNotificationId(Integer emailNotificationId) {
		this.emailNotificationId = emailNotificationId;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}

	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}

	public String getNotificationTabName() {
		return notificationTabName;
	}

	public void setNotificationTabName(String notificationTabName) {
		this.notificationTabName = notificationTabName;
	}

	public String getNotificationShortName() {
		return notificationShortName;
	}

	public void setNotificationShortName(String notificationShortName) {
		this.notificationShortName = notificationShortName;
	}

	public Boolean getNotificationFlag() {
		return notificationFlag;
	}

	public void setNotificationFlag(Boolean notificationFlag) {
		this.notificationFlag = notificationFlag;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	
	
	
	
}
