package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="jobpoolmaster")
public class JobPoolMaster implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8469772720899817634L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer JobpoolId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobMaster jobId;
	
	@ManyToOne
	@JoinColumn(name="poolId",referencedColumnName="poolId")
	private PoolMaster poolId;
	
	public Integer getJobpoolId() {
		return JobpoolId;
	}
	public void setJobpoolId(Integer jobpoolId) {
		JobpoolId = jobpoolId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public JobMaster getJobId() {
		return jobId;
	}
	public void setJobId(JobMaster jobId) {
		this.jobId = jobId;
	}
	public PoolMaster getPoolId() {
		return poolId;
	}
	public void setPoolId(PoolMaster poolId) {
		this.poolId = poolId;
	}
	
	
	
	
}
