package tm.bean.master;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

@Entity
@Table(name="schoolmaster")
public class SchoolMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 360515233095931383L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long schoolId;
	
	@ManyToOne
	@JoinColumn(name="geoId",referencedColumnName="actualGeoId")
	private GeoMapping geoMapping;     
	
	@ManyToOne
	@JoinColumn(name="schoolTypeId",referencedColumnName="schoolTypeId")
	private SchoolTypeMaster schoolTypeId;     

	@ManyToOne
	@JoinColumn(name="geoZoneId",referencedColumnName="geoZoneId")
	private GeoZoneMaster geoZoneMaster;
	
	@ManyToOne
	@JoinColumn(name="regionId",referencedColumnName="regionId")
	private RegionMaster regionId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtId;
	private String authKey;
	private String apiRedirectUrl;
	
	private String schoolName;
	private String locationCode;
	
	@ManyToOne
	@JoinColumn(name="schoolStandardId",referencedColumnName="schoolStandardId")
	private SchoolStandardMaster schoolStandardId;
	
	private String address;
	private Long noOfTeachers;     
	private Long noOfStudents;     
	private String status;     
	private Date createdDateTime;
	
	private String exitURL; 
	private Integer flagForURL; 
	private Integer flagForMessage; 
	private String exitMessage; 
	
	private Integer candidateFeedNormScore;
	private Integer candidateFeedDaysOfNoActivity;
	
	private Integer jobFeedCriticalJobActiveDays;
	private Integer jobFeedCriticalCandidateRatio;
	private Integer jobFeedCriticalNormScore;
	private String lblCriticialJobName;
	
	private Integer jobFeedAttentionJobActiveDays;
	private Boolean jobFeedAttentionJobNotFilled;
	private Integer jobFeedAttentionNormScore;
	private String lblAttentionJobName;
	
	
	private String dmName;     
	private String dmEmailAddress;     
	private String dmPhoneNumber;     
	private String dmPassword;     
	private String acName;     
	private String acEmailAddress;     
	private String acPhoneNumber; 
	private String amName;     
	private String amEmailAddress;     
	private String amPhoneNumber; 
	private Date initiatedOnDate;  
	private Date contractStartDate;     
	private Date contractEndDate; 
	private Integer canTMApproach;
	private Integer teachersUnderContract;     
	private Integer studentsUnderContract;     
	private Integer ncesStateId;
	private String ncesSchoolId;
	private String districtName;

	private String zip;
	
	@ManyToOne
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private StateMaster stateMaster;
	private String cityName;

	


	private String phoneNumber;
	private String alternatePhone;    
	private String faxNumber;
	private String website;
	private String displayName;
	private String description;
	
	@Column(name="PKOFFRD")
	private Integer pkOffered;
	
	
	@Column(name="KGOFFRD")
	private Integer kgOffered;
	
	@Column(name="G01OFFRD")
	private Integer g01Offered;
	
	@Column(name="G02OFFRD")
	private Integer g02Offered;
	
	@Column(name="G03OFFRD")
	private Integer g03Offered;
	
	@Column(name="G04OFFRD")
	private Integer g04Offered;
	
	@Column(name="G05OFFRD")
	private Integer g05Offered;
	
	@Column(name="G06OFFRD")
	private Integer g06Offered;
	
	@Column(name="G07OFFRD")
	private Integer g07Offered;
	
	@Column(name="G08OFFRD")
	private Integer g08Offered;
	
	@Column(name="G09OFFRD")
	private Integer g09Offered;
	
	@Column(name="G10OFFRD")
	private Integer g10Offered;
	
	@Column(name="G11OFFRD")
	private Integer g11Offered;
	
	@Column(name="G12OFFRD")
	private Integer g12Offered;
	
	
	private String assessmentUploadURL;
	
	private String logoPath; 
	private CommonsMultipartFile 	logoPathFile;
	private CommonsMultipartFile 	assessmentUploadURLFile;
	
	private Integer postingOnDistrictWall;
	private Integer postingOnSchoolWall;
	private Integer postingOnTMWall;
	private Integer allowMessageTeacher; 
	private String emailForTeacher;
	
	private String division;

	private Integer isWeeklyCgReport;
	private Boolean isPortfolioNeeded;
	
	public GeoZoneMaster getGeoZoneMaster() {
		return geoZoneMaster;
	}
	public void setGeoZoneMaster(GeoZoneMaster geoZoneMaster) {
		this.geoZoneMaster = geoZoneMaster;
	}
	public Boolean getIsPortfolioNeeded() {
		return isPortfolioNeeded;
	}
	public void setIsPortfolioNeeded(Boolean isPortfolioNeeded) {
		this.isPortfolioNeeded = isPortfolioNeeded;
	}
	public Integer getIsWeeklyCgReport() {
		return isWeeklyCgReport;
	}
	public void setIsWeeklyCgReport(Integer isWeeklyCgReport) {
		this.isWeeklyCgReport = isWeeklyCgReport;
	}
	
	public Integer getAllowMessageTeacher() {
		return allowMessageTeacher;
	}
	public void setAllowMessageTeacher(Integer allowMessageTeacher) {
		this.allowMessageTeacher = allowMessageTeacher;
	}
	public String getEmailForTeacher() {
		return emailForTeacher;
	}
	public void setEmailForTeacher(String emailForTeacher) {
		this.emailForTeacher = emailForTeacher;
	}
	
	public Long getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}
	public GeoMapping getGeoMapping() {
		return geoMapping;
	}
	public void setGeoMapping(GeoMapping geoMapping) {
		this.geoMapping = geoMapping;
	}
	public SchoolTypeMaster getSchoolTypeId() {
		return schoolTypeId;
	}
	public void setSchoolTypeId(SchoolTypeMaster schoolTypeId) {
		this.schoolTypeId = schoolTypeId;
	}
	public RegionMaster getRegionId() {
		return regionId;
	}
	public void setRegionId(RegionMaster regionId) {
		this.regionId = regionId;
	}
	public DistrictMaster getDistrictId() {
		return districtId;
	}
	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	public String getApiRedirectUrl() {
		return apiRedirectUrl;
	}
	public void setApiRedirectUrl(String apiRedirectUrl) {
		this.apiRedirectUrl = apiRedirectUrl;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public SchoolStandardMaster getSchoolStandardId() {
		return schoolStandardId;
	}
	public void setSchoolStandardId(SchoolStandardMaster schoolStandardId) {
		this.schoolStandardId = schoolStandardId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getNoOfTeachers() {
		return noOfTeachers;
	}
	public void setNoOfTeachers(Long noOfTeachers) {
		this.noOfTeachers = noOfTeachers;
	}
	public Long getNoOfStudents() {
		return noOfStudents;
	}
	public void setNoOfStudents(Long noOfStudents) {
		this.noOfStudents = noOfStudents;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getExitURL() {
		return exitURL;
	}
	public void setExitURL(String exitURL) {
		this.exitURL = exitURL;
	}
	public Integer getFlagForURL() {
		return flagForURL;
	}
	public void setFlagForURL(Integer flagForURL) {
		this.flagForURL = flagForURL;
	}
	public Integer getFlagForMessage() {
		return flagForMessage;
	}
	public void setFlagForMessage(Integer flagForMessage) {
		this.flagForMessage = flagForMessage;
	}
	public String getExitMessage() {
		return exitMessage;
	}
	public void setExitMessage(String exitMessage) {
		this.exitMessage = exitMessage;
	}
	public Integer getCandidateFeedNormScore() {
		return candidateFeedNormScore;
	}
	public void setCandidateFeedNormScore(Integer candidateFeedNormScore) {
		this.candidateFeedNormScore = candidateFeedNormScore;
	}
	public Integer getCandidateFeedDaysOfNoActivity() {
		return candidateFeedDaysOfNoActivity;
	}
	public void setCandidateFeedDaysOfNoActivity(
			Integer candidateFeedDaysOfNoActivity) {
		this.candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity;
	}
	public Integer getJobFeedCriticalJobActiveDays() {
		return jobFeedCriticalJobActiveDays;
	}
	public void setJobFeedCriticalJobActiveDays(Integer jobFeedCriticalJobActiveDays) {
		this.jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays;
	}
	public Integer getJobFeedCriticalCandidateRatio() {
		return jobFeedCriticalCandidateRatio;
	}
	public void setJobFeedCriticalCandidateRatio(
			Integer jobFeedCriticalCandidateRatio) {
		this.jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio;
	}
	public Integer getJobFeedCriticalNormScore() {
		return jobFeedCriticalNormScore;
	}
	public void setJobFeedCriticalNormScore(Integer jobFeedCriticalNormScore) {
		this.jobFeedCriticalNormScore = jobFeedCriticalNormScore;
	}
	public String getLblCriticialJobName() {
		return lblCriticialJobName;
	}
	public void setLblCriticialJobName(String lblCriticialJobName) {
		this.lblCriticialJobName = lblCriticialJobName;
	}
	public Integer getJobFeedAttentionJobActiveDays() {
		return jobFeedAttentionJobActiveDays;
	}
	public void setJobFeedAttentionJobActiveDays(
			Integer jobFeedAttentionJobActiveDays) {
		this.jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays;
	}
	public Boolean getJobFeedAttentionJobNotFilled() {
		return jobFeedAttentionJobNotFilled;
	}
	public void setJobFeedAttentionJobNotFilled(Boolean jobFeedAttentionJobNotFilled) {
		this.jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled;
	}
	public Integer getJobFeedAttentionNormScore() {
		return jobFeedAttentionNormScore;
	}
	public void setJobFeedAttentionNormScore(Integer jobFeedAttentionNormScore) {
		this.jobFeedAttentionNormScore = jobFeedAttentionNormScore;
	}
	public String getLblAttentionJobName() {
		return lblAttentionJobName;
	}
	public void setLblAttentionJobName(String lblAttentionJobName) {
		this.lblAttentionJobName = lblAttentionJobName;
	}
	public String getDmName() {
		return dmName;
	}
	public void setDmName(String dmName) {
		this.dmName = dmName;
	}
	public String getDmEmailAddress() {
		return dmEmailAddress;
	}
	public void setDmEmailAddress(String dmEmailAddress) {
		this.dmEmailAddress = dmEmailAddress;
	}
	public String getDmPhoneNumber() {
		return dmPhoneNumber;
	}
	public void setDmPhoneNumber(String dmPhoneNumber) {
		this.dmPhoneNumber = dmPhoneNumber;
	}
	public String getDmPassword() {
		return dmPassword;
	}
	public void setDmPassword(String dmPassword) {
		this.dmPassword = dmPassword;
	}
	public String getAcName() {
		return acName;
	}
	public void setAcName(String acName) {
		this.acName = acName;
	}
	public String getAcEmailAddress() {
		return acEmailAddress;
	}
	public void setAcEmailAddress(String acEmailAddress) {
		this.acEmailAddress = acEmailAddress;
	}
	public String getAcPhoneNumber() {
		return acPhoneNumber;
	}
	public void setAcPhoneNumber(String acPhoneNumber) {
		this.acPhoneNumber = acPhoneNumber;
	}
	public String getAmName() {
		return amName;
	}
	public void setAmName(String amName) {
		this.amName = amName;
	}
	public String getAmEmailAddress() {
		return amEmailAddress;
	}
	public void setAmEmailAddress(String amEmailAddress) {
		this.amEmailAddress = amEmailAddress;
	}
	public String getAmPhoneNumber() {
		return amPhoneNumber;
	}
	public void setAmPhoneNumber(String amPhoneNumber) {
		this.amPhoneNumber = amPhoneNumber;
	}
	public Date getInitiatedOnDate() {
		return initiatedOnDate;
	}
	public void setInitiatedOnDate(Date initiatedOnDate) {
		this.initiatedOnDate = initiatedOnDate;
	}
	public Date getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public Date getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public Integer getCanTMApproach() {
		return canTMApproach;
	}
	public void setCanTMApproach(Integer canTMApproach) {
		this.canTMApproach = canTMApproach;
	}
	public Integer getTeachersUnderContract() {
		return teachersUnderContract;
	}
	public void setTeachersUnderContract(Integer teachersUnderContract) {
		this.teachersUnderContract = teachersUnderContract;
	}
	public Integer getStudentsUnderContract() {
		return studentsUnderContract;
	}
	public void setStudentsUnderContract(Integer studentsUnderContract) {
		this.studentsUnderContract = studentsUnderContract;
	}
	public Integer getNcesStateId() {
		return ncesStateId;
	}
	public void setNcesStateId(Integer ncesStateId) {
		this.ncesStateId = ncesStateId;
	}
	public String getNcesSchoolId() {
		return ncesSchoolId;
	}
	public void setNcesSchoolId(String ncesSchoolId) {
		this.ncesSchoolId = ncesSchoolId;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public StateMaster getStateMaster() {
		return stateMaster;
	}
	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAlternatePhone() {
		return alternatePhone;
	}
	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getPkOffered() {
		return pkOffered;
	}
	public void setPkOffered(Integer pkOffered) {
		this.pkOffered = pkOffered;
	}
	public Integer getKgOffered() {
		return kgOffered;
	}
	public void setKgOffered(Integer kgOffered) {
		this.kgOffered = kgOffered;
	}
	public Integer getG01Offered() {
		return g01Offered;
	}
	public void setG01Offered(Integer g01Offered) {
		this.g01Offered = g01Offered;
	}
	public Integer getG02Offered() {
		return g02Offered;
	}
	public void setG02Offered(Integer g02Offered) {
		this.g02Offered = g02Offered;
	}
	public Integer getG03Offered() {
		return g03Offered;
	}
	public void setG03Offered(Integer g03Offered) {
		this.g03Offered = g03Offered;
	}
	public Integer getG04Offered() {
		return g04Offered;
	}
	public void setG04Offered(Integer g04Offered) {
		this.g04Offered = g04Offered;
	}
	public Integer getG05Offered() {
		return g05Offered;
	}
	public void setG05Offered(Integer g05Offered) {
		this.g05Offered = g05Offered;
	}
	public Integer getG06Offered() {
		return g06Offered;
	}
	public void setG06Offered(Integer g06Offered) {
		this.g06Offered = g06Offered;
	}
	public Integer getG07Offered() {
		return g07Offered;
	}
	public void setG07Offered(Integer g07Offered) {
		this.g07Offered = g07Offered;
	}
	public Integer getG08Offered() {
		return g08Offered;
	}
	public void setG08Offered(Integer g08Offered) {
		this.g08Offered = g08Offered;
	}
	public Integer getG09Offered() {
		return g09Offered;
	}
	public void setG09Offered(Integer g09Offered) {
		this.g09Offered = g09Offered;
	}
	public Integer getG10Offered() {
		return g10Offered;
	}
	public void setG10Offered(Integer g10Offered) {
		this.g10Offered = g10Offered;
	}
	public Integer getG11Offered() {
		return g11Offered;
	}
	public void setG11Offered(Integer g11Offered) {
		this.g11Offered = g11Offered;
	}
	public Integer getG12Offered() {
		return g12Offered;
	}
	public void setG12Offered(Integer g12Offered) {
		this.g12Offered = g12Offered;
	}
	public String getAssessmentUploadURL() {
		return assessmentUploadURL;
	}
	public void setAssessmentUploadURL(String assessmentUploadURL) {
		this.assessmentUploadURL = assessmentUploadURL;
	}
	public String getLogoPath() {
		return logoPath;
	}
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}
	public CommonsMultipartFile getLogoPathFile() {
		return logoPathFile;
	}
	public void setLogoPathFile(CommonsMultipartFile logoPathFile) {
		this.logoPathFile = logoPathFile;
	}
	public CommonsMultipartFile getAssessmentUploadURLFile() {
		return assessmentUploadURLFile;
	}
	public void setAssessmentUploadURLFile(
			CommonsMultipartFile assessmentUploadURLFile) {
		this.assessmentUploadURLFile = assessmentUploadURLFile;
	}
	public Integer getPostingOnDistrictWall() {
		return postingOnDistrictWall;
	}
	public void setPostingOnDistrictWall(Integer postingOnDistrictWall) {
		this.postingOnDistrictWall = postingOnDistrictWall;
	}
	public Integer getPostingOnSchoolWall() {
		return postingOnSchoolWall;
	}
	public void setPostingOnSchoolWall(Integer postingOnSchoolWall) {
		this.postingOnSchoolWall = postingOnSchoolWall;
	}
	public Integer getPostingOnTMWall() {
		return postingOnTMWall;
	}
	public void setPostingOnTMWall(Integer postingOnTMWall) {
		this.postingOnTMWall = postingOnTMWall;
	}
	public static Comparator<SchoolMaster> getCompSchoolMaster() {
		return compSchoolMaster;
	}
	public static void setCompSchoolMaster(Comparator<SchoolMaster> compSchoolMaster) {
		SchoolMaster.compSchoolMaster = compSchoolMaster;
	}

	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}

	public static Comparator<SchoolMaster> compSchoolMaster   = new Comparator<SchoolMaster>() 
	{
		public int compare(SchoolMaster school1, SchoolMaster school2) {
		
			String schoolName1= school1.getSchoolName();
			String schoolName2= school2.getSchoolName();
			
			//descending order
			//return schoolName2.compareTo(schoolName1);
			
			//ascending order
			return schoolName1.compareTo(schoolName2);
		}		
	};

	
	@Override
	public boolean equals(Object object){
		if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof SchoolMaster))return false;
	    SchoolMaster schoolMaster = (SchoolMaster)object;
	  
	    if(this.schoolId.equals(schoolMaster.getSchoolId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override
	public int hashCode(){
			return new Integer(""+schoolId);
	}

	
}
