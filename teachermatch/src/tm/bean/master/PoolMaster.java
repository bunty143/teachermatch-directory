package tm.bean.master;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.master.DistrictMaster;


@Entity
@Table(name="poolmaster")
public class PoolMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6553110337741844654L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer poolId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	private String poolDescription;
	private String poolTitle;
	private String status;
	private Date createdDateTime;
	
	public Integer getPoolId() {
		return poolId;
	}
	public void setPoolId(Integer poolId) {
		this.poolId = poolId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	
	public String getPoolDescription() {
		return poolDescription;
	}
	public void setPoolDescription(String poolDescription) {
		this.poolDescription = poolDescription;
	}
	public String getPoolTitle() {
		return poolTitle;
	}
	public void setPoolTitle(String poolTitle) {
		this.poolTitle = poolTitle;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
	
}
