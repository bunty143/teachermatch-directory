package tm.bean.master;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="readonlycheckpointtransaction")
public class ReadOnlyCheckPointTransaction {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer checkPointTransactionId;
	Integer checkPointMasterId;
	Integer onBoardingId;
	Integer districtId;
	String checkPointName;
	Date createdDate;
	String createdByUser;
	Date modifiedDate;
	String modifiedByUser;
	String ipAddress;
	String status;
	public Integer getCheckPointTransactionId() {
		return checkPointTransactionId;
	}
	public void setCheckPointTransactionId(Integer checkPointTransactionId) {
		this.checkPointTransactionId = checkPointTransactionId;
	}
	public Integer getCheckPointMasterId() {
		return checkPointMasterId;
	}
	public void setCheckPointMasterId(Integer checkPointMasterId) {
		this.checkPointMasterId = checkPointMasterId;
	}
	public Integer getOnBoardingId() {
		return onBoardingId;
	}
	public void setOnBoardingId(Integer onBoardingId) {
		this.onBoardingId = onBoardingId;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public String getCheckPointName() {
		return checkPointName;
	}
	public void setCheckPointName(String checkPointName) {
		this.checkPointName = checkPointName;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedByUser() {
		return createdByUser;
	}
	public void setCreatedByUser(String createdByUser) {
		this.createdByUser = createdByUser;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedByUser() {
		return modifiedByUser;
	}
	public void setModifiedByUser(String modifiedByUser) {
		this.modifiedByUser = modifiedByUser;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
