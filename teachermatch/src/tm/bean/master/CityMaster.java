package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="citymaster")
public class CityMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7665008623199400146L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long cityId; 	
	private String zipCode;
	private String zipcodetype;
	private String cityName; 
	@ManyToOne
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private StateMaster stateId;  	
	private String lats;
	private String longs;
	private String decommisioned;	
	private String status;
	public Long getCityId() {
		return cityId;
	}
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getZipcodetype() {
		return zipcodetype;
	}
	public void setZipcodetype(String zipcodetype) {
		this.zipcodetype = zipcodetype;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public StateMaster getStateId() {
		return stateId;
	}
	public void setStateId(StateMaster stateId) {
		this.stateId = stateId;
	}
	public String getLats() {
		return lats;
	}
	public void setLats(String lats) {
		this.lats = lats;
	}
	public String getLongs() {
		return longs;
	}
	public void setLongs(String longs) {
		this.longs = longs;
	}
	public String getDecommisioned() {
		return decommisioned;
	}
	public void setDecommisioned(String decommisioned) {
		this.decommisioned = decommisioned;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
