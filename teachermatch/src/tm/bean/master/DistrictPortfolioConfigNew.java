package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="districtportfolioconfignew")
public class DistrictPortfolioConfigNew implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6631718743665590819L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtportfolioconfignewId;
	
	@ManyToOne
	@JoinColumn(name="dspqFieldId",referencedColumnName="dspqFieldId")
	private DspqFieldMaster dspqFieldMaster;
	
	@ManyToOne
	@JoinColumn(name="dspqPortfolioNameId",referencedColumnName="dspqPortfolioNameId")
	private DspqPortfolioName dspqPortfolioName;
	
	private String displayName;
	private Boolean isRequired;
	private String applicantType;
	private Integer numberRequired;
	private String status;
	private Date createdDateTime;

	public Integer getDistrictportfolioconfignewId() {
		return districtportfolioconfignewId;
	}
	public void setDistrictportfolioconfignewId(Integer districtportfolioconfignewId) {
		this.districtportfolioconfignewId = districtportfolioconfignewId;
	}
	public DspqFieldMaster getDspqFieldMaster() {
		return dspqFieldMaster;
	}
	public void setDspqFieldMaster(DspqFieldMaster dspqFieldMaster) {
		this.dspqFieldMaster = dspqFieldMaster;
	}
	public DspqPortfolioName getDspqPortfolioName() {
		return dspqPortfolioName;
	}
	public void setDspqPortfolioName(DspqPortfolioName dspqPortfolioName) {
		this.dspqPortfolioName = dspqPortfolioName;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public Boolean getIsRequired() {
		return isRequired;
	}
	public void setIsRequired(Boolean isRequired) {
		this.isRequired = isRequired;
	}
	public String getApplicantType() {
		return applicantType;
	}
	public void setApplicantType(String applicantType) {
		this.applicantType = applicantType;
	}
	public Integer getNumberRequired() {
		return numberRequired;
	}
	public void setNumberRequired(Integer numberRequired) {
		this.numberRequired = numberRequired;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return districtportfolioconfignewId+" : "+displayName+" : "+applicantType +" : "+isRequired+" : "+dspqFieldMaster.getDspqFieldId();
	}
	
}
