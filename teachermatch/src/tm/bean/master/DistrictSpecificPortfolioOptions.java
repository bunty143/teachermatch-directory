package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;

@Entity
@Table(name="districtspecificportfoliooptions")
public class DistrictSpecificPortfolioOptions implements Serializable
{

	private static final long serialVersionUID = 2188476270084168926L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer optionId; 	
	
	@ManyToOne
	@JoinColumn(name="questionId",referencedColumnName="questionId")
	private DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions;
	
	private String questionOption;
	
	private Integer validOption;
	
	private Integer rank;                      
	private Double score;    
	private Boolean openText;
	private String subSectionTitle;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private String status;
	
	private Date createdDateTime;
	private Integer optionOrder;
	
	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Integer getOptionId() {
		return optionId;
	}

	public void setOptionId(Integer optionId) {
		this.optionId = optionId;
	}

	public DistrictSpecificPortfolioQuestions getDistrictSpecificPortfolioQuestions() {
		return districtSpecificPortfolioQuestions;
	}

	public void setDistrictSpecificPortfolioQuestions(
			DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions) {
		this.districtSpecificPortfolioQuestions = districtSpecificPortfolioQuestions;
	}

	public String getQuestionOption() {
		return questionOption;
	}

	public void setQuestionOption(String questionOption) {
		this.questionOption = questionOption;
	}

	public Integer getValidOption() {
		return validOption;
	}

	public void setValidOption(Integer validOption) {
		this.validOption = validOption;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getOpenText() {
		return openText;
	}

	public void setOpenText(Boolean openText) {
		this.openText = openText;
	}

	public String getSubSectionTitle() {
		return subSectionTitle;
	}

	public void setSubSectionTitle(String subSectionTitle) {
		this.subSectionTitle = subSectionTitle;
	}

	public Integer getOptionOrder() {
		return optionOrder;
	}

	public void setOptionOrder(Integer optionOrder) {
		this.optionOrder = optionOrder;
	}
}
