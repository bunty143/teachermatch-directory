package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;
import tm.utility.Utility;

@Entity
@Table(name="schoolnotes")
public class SchoolNotes implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7114590789681989630L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer notesId;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	private String note;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBy;
	private Date createdDateTime;
	
	public Integer getNotesId() 
	{
		return notesId;
	}
	public void setNotesId(Integer notesId) 
	{
		this.notesId = notesId;
	}
	public SchoolMaster getSchoolMaster() 
	{
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) 
	{
		this.schoolMaster = schoolMaster;
	}
	public String getNote() 
	{
		return note;
	}
	public void setNote(String note) 
	{
		this.note = Utility.trim(note);
	}
	public UserMaster getCreatedBy() 
	{
		return createdBy;
	}
	public void setCreatedBy(UserMaster createdBy) 
	{
		this.createdBy = createdBy;
	}
	public Date getCreatedDateTime() 
	{
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) 
	{
		this.createdDateTime = createdDateTime;
	}
}
