package tm.bean.master;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teachersavedbycandidates")
public class SaveCandidatesToFolder implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8761096402105311830L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer savedTeacherId;
	
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherId;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder joborder;    
	
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private Integer headQuarterId;	
	
	private Integer branchId;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
 	
	@ManyToOne
	@JoinColumn(name="folderId",referencedColumnName="folderId")
	private UserFolderStructure userFolder;
	
	private boolean viewed;
	
	@ManyToOne
	@JoinColumn(name="savedBy",referencedColumnName="userId")
	private UserMaster saveUserMaster;
	
	@ManyToOne
	@JoinColumn(name="sharedBy",referencedColumnName="userId")
	private UserMaster sharedUserMaster;
	
	private Date savedDateTime;
	private Date sharedDateTime;
	
	@Transient
	private Double normScore = -0.0;
	
	
	public boolean isViewed() {
		return viewed;
	}
	public void setViewed(boolean viewed) {
		this.viewed = viewed;
	}
	
	public Double getNormScore() {
		return normScore;
	}
	public void setNormScore(Double normScore) {
		this.normScore = normScore;
	}
	public Integer getSavedTeacherId() {
		return savedTeacherId;
	}
	public void setSavedTeacherId(Integer savedTeacherId) {
		this.savedTeacherId = savedTeacherId;
	}
	public TeacherDetail getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}
	public JobOrder getJoborder() {
		return joborder;
	}
	public void setJoborder(JobOrder joborder) {
		this.joborder = joborder;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}	
	public Integer getHeadQuarterId() {
		return headQuarterId;
	}
	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public UserFolderStructure getUserFolder() {
		return userFolder;
	}
	public void setUserFolder(UserFolderStructure userFolder) {
		this.userFolder = userFolder;
	}
	public UserMaster getSaveUserMaster() {
		return saveUserMaster;
	}
	public void setSaveUserMaster(UserMaster saveUserMaster) {
		this.saveUserMaster = saveUserMaster;
	}
	public UserMaster getSharedUserMaster() {
		return sharedUserMaster;
	}
	public void setSharedUserMaster(UserMaster sharedUserMaster) {
		this.sharedUserMaster = sharedUserMaster;
	}
	public Date getSavedDateTime() {
		return savedDateTime;
	}
	public void setSavedDateTime(Date savedDateTime) {
		this.savedDateTime = savedDateTime;
	}
	public Date getSharedDateTime() {
		return sharedDateTime;
	}
	public void setSharedDateTime(Date sharedDateTime) {
		this.sharedDateTime = sharedDateTime;
	}
	
	public static Comparator<SaveCandidatesToFolder> jobNormScore = new Comparator<SaveCandidatesToFolder>(){
		public int compare(SaveCandidatesToFolder jft1, SaveCandidatesToFolder jft2) {
			Long cScore1 = Math.round(jft1.getNormScore());
			Long cScore2 = Math.round(jft2.getNormScore());		
			int c = cScore1.compareTo(cScore2);
			return c;
		}		
	};
	
	public static Comparator<SaveCandidatesToFolder> jobNormScoreDesc  = new Comparator<SaveCandidatesToFolder>(){
		public int compare(SaveCandidatesToFolder jft1, SaveCandidatesToFolder jft2) {			
			Long cScore1 = Math.round(jft1.getNormScore());
			Long cScore2 = Math.round(jft2.getNormScore());		
			int c = cScore2.compareTo(cScore1);
			return c;
		}		
	};
	
	public static Comparator<SaveCandidatesToFolder> teacherName  = new Comparator<SaveCandidatesToFolder>(){
		public int compare(SaveCandidatesToFolder jft1, SaveCandidatesToFolder jft2) {			
			return jft1.getTeacherId().getLastName().toUpperCase().compareTo(jft2.getTeacherId().getLastName().toUpperCase());
		}		
	};
	
	public static Comparator<SaveCandidatesToFolder> teacherNameDesc  = new Comparator<SaveCandidatesToFolder>(){
		public int compare(SaveCandidatesToFolder jft1, SaveCandidatesToFolder jft2) {
			int c = jft2.getTeacherId().getLastName().toUpperCase().compareTo(jft1.getTeacherId().getLastName().toUpperCase());
			return c;
		}		
	};
}
