package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="referencequestionsets")
public class ReferenceQuestionSets implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -124317139319261524L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer ID;
	
	@ManyToOne
	@JoinColumn(name="DistrictID",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	
	private Integer headQuarterId;
	
	 
	
	private String QuestionSetText;
	private String RefChkQuestionSetID;
	private Date DateCreated;
	private String Status;
	
	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
 
	
	public Integer getHeadQuarterId() {
		return headQuarterId;
	}

	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}

	public String getQuestionSetText() {
		return QuestionSetText;
	}

	public void setQuestionSetText(String questionSetText) {
		QuestionSetText = questionSetText;
	}

	public String getRefChkQuestionSetID() {
		return RefChkQuestionSetID;
	}

	public void setRefChkQuestionSetID(String refChkQuestionSetID) {
		RefChkQuestionSetID = refChkQuestionSetID;
	}

	public Date getDateCreated() {
		return DateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		DateCreated = dateCreated;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof ReferenceQuestionSets))return false;
	    ReferenceQuestionSets i4QuestionSets = (ReferenceQuestionSets)object;
	  
	    if(this.ID.equals(i4QuestionSets.getID()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+ID);
	}
	
}
