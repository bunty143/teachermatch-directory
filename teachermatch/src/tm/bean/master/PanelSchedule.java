package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;

@Entity
@Table(name="panelschedule")
public class PanelSchedule implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1589789604251109719L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer panelId;
	
	private Integer headQuarterId;
	private Integer branchId;
	
	private Integer districtId;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolId;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobPanelStatusId",referencedColumnName="jobPanelStatusId")
	private JobWisePanelStatus jobWisePanelStatus;
	
	@ManyToOne
	@JoinColumn(name="timeZoneId",referencedColumnName="timeZoneId")
	private TimeZoneMaster timeZoneMaster;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBy;	
	
	
	private Date panelDate;
	private Date panelStandardTime; 
	private String panelTime;
	private String timeFormat;
	private String panelAddress;
	private String panelDescription;
	private String panelStatus;
	private Date createdDateTime;
	
	public Integer getHeadQuarterId() {
		return headQuarterId;
	}
	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public Integer getPanelId() {
		return panelId;
	}
	public void setPanelId(Integer panelId) {
		this.panelId = panelId;
	}
	
	public SchoolMaster getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(SchoolMaster schoolId) {
		this.schoolId = schoolId;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	
	public TimeZoneMaster getTimeZoneMaster() {
		return timeZoneMaster;
	}
	public void setTimeZoneMaster(TimeZoneMaster timeZoneMaster) {
		this.timeZoneMaster = timeZoneMaster;
	}
	public UserMaster getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(UserMaster createdBy) {
		this.createdBy = createdBy;
	}
	public Date getPanelDate() {
		return panelDate;
	}
	public void setPanelDate(Date panelDate) {
		this.panelDate = panelDate;
	}
	public Date getPanelStandardTime() {
		return panelStandardTime;
	}
	public void setPanelStandardTime(Date panelStandardTime) {
		this.panelStandardTime = panelStandardTime;
	}
	public String getPanelTime() {
		return panelTime;
	}
	public void setPanelTime(String panelTime) {
		this.panelTime = panelTime;
	}
	public String getTimeFormat() {
		return timeFormat;
	}
	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}
	public String getPanelAddress() {
		return panelAddress;
	}
	public void setPanelAddress(String panelAddress) {
		this.panelAddress = panelAddress;
	}
	public String getPanelDescription() {
		return panelDescription;
	}
	public void setPanelDescription(String panelDescription) {
		this.panelDescription = panelDescription;
	}
	public String getPanelStatus() {
		return panelStatus;
	}
	public void setPanelStatus(String panelStatus) {
		this.panelStatus = panelStatus;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobWisePanelStatus getJobWisePanelStatus() {
		return jobWisePanelStatus;
	}
	public void setJobWisePanelStatus(JobWisePanelStatus jobWisePanelStatus) {
		this.jobWisePanelStatus = jobWisePanelStatus;
	}
	
	
	
}
