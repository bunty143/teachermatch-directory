package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;
import tm.utility.Utility;


@Entity
@Table(name="schoolkeycontact")
public class SchoolKeyContact implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5871394843440475645L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer keyContactId;
	private Long schoolId;
	@ManyToOne
	@JoinColumn(name="keyContactTypeId",referencedColumnName="contactTypeId")
	private ContactTypeMaster keyContactTypeId;
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userMaster;
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	private String keyContactFirstName;
	private String keyContactLastName;
	private String keyContactEmailAddress;
	private String keyContactPhoneNumber;
	private String keyContactTitle;
	
	
	public String getKeyContactFirstName() {
		return keyContactFirstName;
	}
	public void setKeyContactFirstName(String keyContactFirstName) {
		this.keyContactFirstName = keyContactFirstName;
	}
	public String getKeyContactLastName() {
		return keyContactLastName;
	}
	public void setKeyContactLastName(String keyContactLastName) {
		this.keyContactLastName = keyContactLastName;
	}
	public Integer getKeyContactId() {
		return keyContactId;
	}
	public void setKeyContactId(Integer keyContactId) {
		this.keyContactId = keyContactId;
	}
	public Long getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}
	public ContactTypeMaster getKeyContactTypeId() {
		return keyContactTypeId;
	}
	public void setKeyContactTypeId(ContactTypeMaster keyContactTypeId) {
		this.keyContactTypeId = keyContactTypeId;
	}
	public String getKeyContactEmailAddress() {
		return keyContactEmailAddress;
	}
	public void setKeyContactEmailAddress(String keyContactEmailAddress) {
		this.keyContactEmailAddress = Utility.trim(keyContactEmailAddress);
	}
	public String getKeyContactPhoneNumber() {
		return keyContactPhoneNumber;
	}
	public void setKeyContactPhoneNumber(String keyContactPhoneNumber) {
		this.keyContactPhoneNumber = Utility.trim(keyContactPhoneNumber);
	}
	public String getKeyContactTitle() {
		return keyContactTitle;
	}
	public void setKeyContactTitle(String keyContactTitle) {
		this.keyContactTitle = Utility.trim(keyContactTitle);
	}
	
	
	
	
	
	
	
	
}
