package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="orgtypemaster")
public class OrgTypeMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1957499491464037956L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer orgTypeId;     
	private String orgType;     
	private String status;
	public Integer getOrgTypeId() {
		return orgTypeId;
	}
	public void setOrgTypeId(Integer orgTypeId) {
		this.orgTypeId = orgTypeId;
	}
	public String getOrgType() {
		return orgType;
	}
	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	} 
	
	
}
