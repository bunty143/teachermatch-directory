package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="timezonemaster")
public class TimeZoneMaster implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7897632100387357802L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer timeZoneId;
	private String timeZoneName;
	private String timeZoneShortName;
	private String status;
	public Integer getTimeZoneId() {
		return timeZoneId;
	}
	public void setTimeZoneId(Integer timeZoneId) {
		this.timeZoneId = timeZoneId;
	}
	public String getTimeZoneName() {
		return timeZoneName;
	}
	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}
	public String getTimeZoneShortName() {
		return timeZoneShortName;
	}
	public void setTimeZoneShortName(String timeZoneShortName) {
		this.timeZoneShortName = timeZoneShortName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
