package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="statusspecificquestions")
public class StatusSpecificQuestions implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3406930529612589789L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer questionId;
	
	//@ManyToOne
	//@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	//private HeadQuarterMaster headQuarterMaster;
	private Integer headQuarterId;

	//@ManyToOne
	//@JoinColumn(name="branchId",referencedColumnName="branchId")
	//private BranchMaster branchMaster;
	private Integer branchId;
	
	//@ManyToOne
	//@JoinColumn(name="districtId",referencedColumnName="districtId")
	//private DistrictMaster districtMaster;
	private Integer districtId;
	
	//@ManyToOne
	//@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	//private JobCategoryMaster jobCategoryMaster;
	
	private Integer jobCategoryId;
	
	//@ManyToOne
	//@JoinColumn(name="statusId",referencedColumnName="statusId")
	//private StatusMaster statusMaster;
	private Integer statusId;
	
	//@ManyToOne
	//@JoinColumn(name="secondaryStatusId",referencedColumnName="secondaryStatusId")
	//private SecondaryStatus secondaryStatus;
	private Integer secondaryStatusId;
	
	private String question;
	
	@ManyToOne
	@JoinColumn(name="skillAttributeId",referencedColumnName="skillAttributeId")
	private SkillAttributesMaster skillAttributesMaster;
	//private Integer skillAttributeId;
	
	private Double maxScore;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	//private Integer createdBy; 
	
	private Date createdDateTime;
	
	private String status;
	
	private boolean isQuestionRequired;
	
	/*public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}*/
	public boolean getIsQuestionRequired() {
		return isQuestionRequired;
	}
	public void setIsQuestionRequired(Boolean isQuestionRequired) {
		this.isQuestionRequired = isQuestionRequired;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public SkillAttributesMaster getSkillAttributesMaster() {
		return skillAttributesMaster;
	}

	public void setSkillAttributesMaster(SkillAttributesMaster skillAttributesMaster) {
		this.skillAttributesMaster = skillAttributesMaster;
	}

	public Integer getJobCategoryId() {
		return jobCategoryId;
	}

	public void setJobCategoryId(Integer jobCategoryId) {
		this.jobCategoryId = jobCategoryId;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getSecondaryStatusId() {
		return secondaryStatusId;
	}

	public void setSecondaryStatusId(Integer secondaryStatusId) {
		this.secondaryStatusId = secondaryStatusId;
	}

	public Integer getHeadQuarterId() {
		return headQuarterId;
	}

	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	/*public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
*/
	

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Double getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(Double maxScore) {
		this.maxScore = maxScore;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
}
