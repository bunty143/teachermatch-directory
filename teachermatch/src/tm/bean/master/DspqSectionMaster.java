package tm.bean.master;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import java.util.Date;

@Entity
@Table(name="dspqsectionmaster",uniqueConstraints = { @UniqueConstraint( columnNames = { "groupId", "sectionName" } ) } )
public class DspqSectionMaster implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5569048647005043026L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer sectionId;
	
	@ManyToOne
	@JoinColumn(name="groupId",referencedColumnName="groupId")
	private DspqGroupMaster dspqGroupMaster;	
	private String sectionName;
	private String status;	
	private Integer sectionByOrder;
	private Date createdDateTime;
	
	public Integer getSectionId() {
		return sectionId;
	}
	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}
	public DspqGroupMaster getDspqGroupMaster() {
		return dspqGroupMaster;
	}
	public void setDspqGroupMaster(DspqGroupMaster dspqGroupMaster) {
		this.dspqGroupMaster = dspqGroupMaster;
	}	
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getSectionByOrder() {
		return sectionByOrder;
	}
	public void setSectionByOrder(Integer sectionByOrder) {
		this.sectionByOrder = sectionByOrder;
	}	
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
}
