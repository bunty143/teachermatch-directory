package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="statusmaster")
public class StatusMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2072539861662131898L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer statusId;
	private String status;
	private String statusShortName;
	
	@ManyToOne
	@JoinColumn(name="statusNodeId",referencedColumnName="statusNodeId")
	private StatusNodeMaster statusNodeMaster;
	
	private Integer displayInStatusLifeCycle;
	private Integer banchmarkStatus;
	private Integer parentStatusID;
	private Integer orderNumber;
	
	@Transient
	private Double maxFitScore;

	
	
	public Double getMaxFitScore() {
		return maxFitScore;
	}
	public void setMaxFitScore(Double maxFitScore) {
		this.maxFitScore = maxFitScore;
	}
	public StatusNodeMaster getStatusNodeMaster() {
		return statusNodeMaster;
	}
	public void setStatusNodeMaster(StatusNodeMaster statusNodeMaster) {
		this.statusNodeMaster = statusNodeMaster;
	}
	public Integer getDisplayInStatusLifeCycle() {
		return displayInStatusLifeCycle;
	}
	public void setDisplayInStatusLifeCycle(Integer displayInStatusLifeCycle) {
		this.displayInStatusLifeCycle = displayInStatusLifeCycle;
	}
	public Integer getBanchmarkStatus() {
		return banchmarkStatus;
	}
	public void setBanchmarkStatus(Integer banchmarkStatus) {
		this.banchmarkStatus = banchmarkStatus;
	}
	public Integer getParentStatusID() {
		return parentStatusID;
	}
	public void setParentStatusID(Integer parentStatusID) {
		this.parentStatusID = parentStatusID;
	}
	public Integer getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusShortName() {
		return statusShortName;
	}
	public void setStatusShortName(String statusShortName) {
		this.statusShortName = statusShortName;
	}
	@Override
	public String toString() {
		return status ;
	}
	
	
	
}
