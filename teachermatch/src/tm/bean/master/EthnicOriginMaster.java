package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ethnicoriginmaster")
public class EthnicOriginMaster implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4571748642758720494L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer ethnicOriginId;
	private String ethnicOriginName;
	private String ethnicOriginCode;
	private Integer orderBy;
	private String status;
	public Integer getEthnicOriginId() {
		return ethnicOriginId;
	}
	public void setEthnicOriginId(Integer ethnicOriginId) {
		this.ethnicOriginId = ethnicOriginId;
	}
	public String getEthnicOriginName() {
		return ethnicOriginName;
	}
	public void setEthnicOriginName(String ethnicOriginName) {
		this.ethnicOriginName = ethnicOriginName;
	}
	public void setEthnicOriginCode(String ethnicOriginCode) {
		this.ethnicOriginCode = ethnicOriginCode;
	}
	public String getEthnicOriginCode() {
		return ethnicOriginCode;
	}
	public Integer getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
