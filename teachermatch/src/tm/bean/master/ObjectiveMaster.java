package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.CompetencyMaster;

@Entity
@Table(name="objectivemaster")
public class ObjectiveMaster implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5702978230466854454L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer objectiveId;
	private String objectiveUId;	
	@ManyToOne
	@JoinColumn(name="competencyId",referencedColumnName="competencyId")
	private CompetencyMaster competencyMaster ;
	private String objectiveName;
	private String status;
	private String objectiveStrength;
	private String objectiveOpportunity;
	
	public Integer getObjectiveId() {
		return objectiveId;
	}
	public void setObjectiveId(Integer objectiveId) {
		this.objectiveId = objectiveId;
	}
	public String getObjectiveUId() {
		return objectiveUId;
	}
	public void setObjectiveUId(String objectiveUId) {
		this.objectiveUId = objectiveUId;
	}
	public CompetencyMaster getCompetencyMaster() {
		return competencyMaster;
	}
	public void setCompetencyMaster(CompetencyMaster competencyMaster) {
		this.competencyMaster = competencyMaster;
	}
	public String getObjectiveName() {
		return objectiveName;
	}
	public void setObjectiveName(String objectiveName) {
		this.objectiveName = objectiveName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getObjectiveStrength() {
		return objectiveStrength;
	}
	public void setObjectiveStrength(String objectiveStrength) {
		this.objectiveStrength = objectiveStrength;
	}
	public String getObjectiveOpportunity() {
		return objectiveOpportunity;
	}
	public void setObjectiveOpportunity(String objectiveOpportunity) {
		this.objectiveOpportunity = objectiveOpportunity;
	}
	
	
	
}
