package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="districtdomains")
public class DistrictDomain implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2850788790095036415L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtDomainId; 
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private String domainName;
	private String status;
	
	private  Date createdDateTime;

	public Integer getDistrictDomainId() {
		return districtDomainId;
	}

	public void setDistrictDomainId(Integer districtDomainId) {
		this.districtDomainId = districtDomainId;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
