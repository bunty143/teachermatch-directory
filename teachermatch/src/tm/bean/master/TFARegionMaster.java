package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tfaregionmaster")
public class TFARegionMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7340304625409892362L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer tfaRegionId;
	private String tfaRegionName;	
	private String status; 
	
	public Integer getTfaRegionId() {
		return tfaRegionId;
	}
	public void setTfaRegionId(Integer tfaRegionId) {
		this.tfaRegionId = tfaRegionId;
	}
	public String getTfaRegionName() {
		return tfaRegionName;
	}
	public void setTfaRegionName(String tfaRegionName) {
		this.tfaRegionName = tfaRegionName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
