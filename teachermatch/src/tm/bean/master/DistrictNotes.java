package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;
import tm.utility.Utility;

@Entity
@Table(name="districtnotes")
public class DistrictNotes implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2182405924617530146L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer notesId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private String note;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBy;
	private Date createdDateTime;
	
	public Integer getNotesId()
	{
		return notesId;
	}
	public void setNotesId(Integer notesId) 
	{
		this.notesId = notesId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) 
	{
		this.note = Utility.trim(note);
	}
	public UserMaster getCreatedBy() 
	{
		return createdBy;
	}
	public void setCreatedBy(UserMaster createdBy)
	{
		this.createdBy = createdBy;
	}
	public Date getCreatedDateTime() 
	{
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
}
