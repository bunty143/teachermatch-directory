package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="domainmaster")
public class DomainMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -563789151855932187L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer domainId;
	
	private String domainUId;	
	private String domainName; 
	private Short displayInPDReport;
	private Double multiplier;
	private String status;
	public Integer getDomainId() {
		return domainId;
	}
	public void setDomainId(Integer domainId) {
		this.domainId = domainId;
	}
	public String getDomainUId() {
		return domainUId;
	}
	public void setDomainUId(String domainUId) {
		this.domainUId = domainUId;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public Short getDisplayInPDReport() {
		return displayInPDReport;
	}
	public void setDisplayInPDReport(Short displayInPDReport) {
		this.displayInPDReport = displayInPDReport;
	}
	public Double getMultiplier() {
		return multiplier;
	}
	public void setMultiplier(Double multiplier) {
		this.multiplier = multiplier;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
