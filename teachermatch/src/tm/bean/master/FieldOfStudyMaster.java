package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="fieldofstudymaster")
public class FieldOfStudyMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3553204148086459066L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long fieldId;     
	private String fieldName;     
	private String status;
	private String fieldCode;
	public Long getFieldId() {
		return fieldId;
	}
	public void setFieldId(Long fieldId) {
		this.fieldId = fieldId;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldCode() {
		return fieldCode;
	}
	public void setFieldCode(String fieldCode) {
		this.fieldCode = fieldCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof FieldOfStudyMaster))return false;
	    FieldOfStudyMaster fieldOfStudyMaster= (FieldOfStudyMaster)object;
	  
	    if(this.fieldId.equals(fieldOfStudyMaster.getFieldId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+fieldId);
	}
}
