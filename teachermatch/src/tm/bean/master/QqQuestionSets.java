package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;

@Entity
@Table(name="qqquestionsets")
public class QqQuestionSets implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6449146591810033939L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer ID;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name="DistrictID",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private String QuestionSetText;
	private String QQQuestionSetID;
	private Date DateCreated;
	private String Status;
	
	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public String getQuestionSetText() {
		return QuestionSetText;
	}

	public void setQuestionSetText(String questionSetText) {
		QuestionSetText = questionSetText;
	}

	public String getQQQuestionSetID() {
		return QQQuestionSetID;
	}

	public void setQQQuestionSetID(String qQQuestionSetID) {
		QQQuestionSetID = qQQuestionSetID;
	}

	public Date getDateCreated() {
		return DateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		DateCreated = dateCreated;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}

	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof QqQuestionSets))return false;
	    QqQuestionSets i4QuestionSets = (QqQuestionSets)object;
	  
	    if(this.ID.equals(i4QuestionSets.getID()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+ID);
	}

	@Override
	public String toString() {
		return this.QuestionSetText ;
	}
	
	
	
}
