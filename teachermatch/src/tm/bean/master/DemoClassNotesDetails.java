package tm.bean.master;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;

@Entity
@Table(name="democlassnotesdetails")
public class DemoClassNotesDetails implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6282735163287873560L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer demoNoteId;
	
	@ManyToOne
	@JoinColumn(name="demoId",referencedColumnName="demoId")
	private DemoClassSchedule demoClassSchedule;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder joborder;    
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	@ManyToOne
	@JoinColumn(name="noteCreatedBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private String demoNoteDescription;
	private String demoNoteAttachment;
	private Date noteCreatedDate;
	
	public Integer getDemoNoteId() {
		return demoNoteId;
	}
	public void setDemoNoteId(Integer demoNoteId) {
		this.demoNoteId = demoNoteId;
	}
	
	public DemoClassSchedule getDemoClassSchedule() {
		return demoClassSchedule;
	}
	public void setDemoClassSchedule(DemoClassSchedule demoClassSchedule) {
		this.demoClassSchedule = demoClassSchedule;
	}
	public JobOrder getJoborder() {
		return joborder;
	}
	public void setJoborder(JobOrder joborder) {
		this.joborder = joborder;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getDemoNoteAttachment() {
		return demoNoteAttachment;
	}
	public void setDemoNoteAttachment(String demoNoteAttachment) {
		this.demoNoteAttachment = demoNoteAttachment;
	}
	public String getDemoNoteDescription() {
		return demoNoteDescription;
	}
	public void setDemoNoteDescription(String demoNoteDescription) {
		this.demoNoteDescription = demoNoteDescription;
	}
	public Date getNoteCreatedDate() {
		return noteCreatedDate;
	}
	public void setNoteCreatedDate(Date noteCreatedDate) {
		this.noteCreatedDate = noteCreatedDate;
	}
	public static Comparator<DemoClassNotesDetails> demoNoteCreatedDate= new Comparator<DemoClassNotesDetails>(){
		 public int compare(DemoClassNotesDetails m1, DemoClassNotesDetails m2) {
		        return m2.getNoteCreatedDate().compareTo(m1.getNoteCreatedDate());
		  }		
	};
}
