package tm.bean.master;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="onboardingcommunication")
public class OnBoardingCommunication {
	private static final long serialVersionUID = 6631718743665590819L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer communicationId;
	Integer districtId;
	Integer onBoardingId;
	Integer checkPointId;
	String candidateType;
	String cEntity;
	String cUserType;
	String appliedTo;
	String templeteName;
	String subjectLine;
	String emailText;
	String cStatus;
	Integer setDefault;
	Date createdDate;
	Integer createdByUser;
	Integer modifiedByUser;
	Date modifiedDate;
	String ipAddress;
	String status;
	public Integer getCommunicationId() {
		return communicationId;
	}
	public void setCommunicationId(Integer communicationId) {
		this.communicationId = communicationId;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public Integer getOnBoardingId() {
		return onBoardingId;
	}
	public void setOnBoardingId(Integer onBoardingId) {
		this.onBoardingId = onBoardingId;
	}
	public Integer getCheckPointId() {
		return checkPointId;
	}
	public void setCheckPointId(Integer checkPointId) {
		this.checkPointId = checkPointId;
	}
	public String getCandidateType() {
		return candidateType;
	}
	public void setCandidateType(String candidateType) {
		this.candidateType = candidateType;
	}
	public String getcEntity() {
		return cEntity;
	}
	public void setcEntity(String cEntity) {
		this.cEntity = cEntity;
	}
	public String getcUserType() {
		return cUserType;
	}
	public void setcUserType(String cUserType) {
		this.cUserType = cUserType;
	}
	public String getAppliedTo() {
		return appliedTo;
	}
	public void setAppliedTo(String appliedTo) {
		this.appliedTo = appliedTo;
	}
	public String getTempleteName() {
		return templeteName;
	}
	public void setTempleteName(String templeteName) {
		this.templeteName = templeteName;
	}
	public String getSubjectLine() {
		return subjectLine;
	}
	public void setSubjectLine(String subjectLine) {
		this.subjectLine = subjectLine;
	}
	public String getEmailText() {
		return emailText;
	}
	public void setEmailText(String emailText) {
		this.emailText = emailText;
	}
	public String getcStatus() {
		return cStatus;
	}
	public void setcStatus(String cStatus) {
		this.cStatus = cStatus;
	}
	public Integer getSetDefault() {
		return setDefault;
	}
	public void setSetDefault(Integer setDefault) {
		this.setDefault = setDefault;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getCreatedByUser() {
		return createdByUser;
	}
	public void setCreatedByUser(Integer createdByUser) {
		this.createdByUser = createdByUser;
	}
	public Integer getModifiedByUser() {
		return modifiedByUser;
	}
	public void setModifiedByUser(Integer modifiedByUser) {
		this.modifiedByUser = modifiedByUser;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
