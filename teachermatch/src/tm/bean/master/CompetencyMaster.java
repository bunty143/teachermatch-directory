package tm.bean.master;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.JobForTeacher;

@Entity
@Table(name="competencymaster")
public class CompetencyMaster implements Comparable<CompetencyMaster>,Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5370552755417118720L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer competencyId;
	private String competencyUId;	

	/*======== Join For domainId ============*/
	@ManyToOne
	@JoinColumn(name="domainId",referencedColumnName="domainId")
	private DomainMaster domainMaster;
	private String competencyName; 
	private String competencyShortName;
	private Short rank;
	private String status;
	private Boolean displayInPDReport;
	
	@Transient
	private Double teacherScore;
	@Transient
	private Double totalScore;
	@Transient
	private String rowResult;
	
	public Integer getCompetencyId() {
		return competencyId;
	}
	public void setCompetencyId(Integer competencyId) {
		this.competencyId = competencyId;
	}
	public String getCompetencyUId() {
		return competencyUId;
	}
	public void setCompetencyUId(String competencyUId) {
		this.competencyUId = competencyUId;
	}	
	public DomainMaster getDomainMaster() {
		return domainMaster;
	}
	public void setDomainMaster(DomainMaster domainMaster) {
		this.domainMaster = domainMaster;
	}
	public String getCompetencyName() {
		return competencyName;
	}
	public void setCompetencyName(String competencyName) {
		this.competencyName = competencyName;
	}
	public String getCompetencyShortName() {
		return competencyShortName;
	}
	public void setCompetencyShortName(String competencyShortName) {
		this.competencyShortName = competencyShortName;
	}
	public Short getRank() {
		return rank;
	}
	public void setRank(Short rank) {
		this.rank = rank;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getDisplayInPDReport() {
		return displayInPDReport;
	}
	public void setDisplayInPDReport(Boolean displayInPDReport) {
		this.displayInPDReport = displayInPDReport;
	}
	public Double getTeacherScore() {
		return teacherScore;
	}
	public void setTeacherScore(Double teacherScore) {
		this.teacherScore = teacherScore;
	}
	public Double getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(Double totalScore) {
		this.totalScore = totalScore;
	}
	public String getRowResult() {
		return rowResult;
	}
	public void setRowResult(String rowResult) {
		this.rowResult = rowResult;
	}
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof CompetencyMaster))return false;
	    CompetencyMaster competencyMaster = (CompetencyMaster)object;
	  
	    if(this.competencyId.equals(competencyMaster.getCompetencyId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	public int compareTo(CompetencyMaster competencyMaster) 
	{   
		return this.teacherScore.compareTo(competencyMaster.teacherScore);
		
		/*if (this.compositeScore == otherJFT.getCompositeScore())
			return 0;
		else if (this.compositeScore < otherJFT.getCompositeScore())
			return 1;
		else
			return -1;
        */
    }
	
	public static Comparator<CompetencyMaster> competencyMasterComparator 
		    = new Comparator<CompetencyMaster>() {
		
		public int compare(CompetencyMaster cm1, CompetencyMaster cm2) {
		
		Double cScore1 = cm1.getTeacherScore();
		Double cScore2 = cm2.getTeacherScore();
		
		//ascending order
		//return cScore1.compareTo(cScore2);
		
		//descending order
		return cScore2.compareTo(cScore1);
	}		
	};
	
	@Override
	public String toString() 
	{
		return super.toString();
	}
	
}
