package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;

@Entity
@Table(name="eligibilitymaster")
public class EligibilityMaster implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9100805509222564265L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer eligibilityId;
	private Integer headQuarterId;
	private Integer branchId;
	private Integer districtId;
	private String eligibilityName;
	private String eligibilityCode;
	private String status;
	private Integer orderBy;
	
	public Integer getHeadQuarterId() {
		return headQuarterId;
	}
	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public Integer getEligibilityId() {
		return eligibilityId;
	}
	public void setEligibilityId(Integer eligibilityId) {
		this.eligibilityId = eligibilityId;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public String getEligibilityName() {
		return eligibilityName;
	}
	public void setEligibilityName(String eligibilityName) {
		this.eligibilityName = eligibilityName;
	}
	public String getEligibilityCode() {
		return eligibilityCode;
	}
	public void setEligibilityCode(String eligibilityCode) {
		this.eligibilityCode = eligibilityCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}
	
	
}
