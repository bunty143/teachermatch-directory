	package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4QuestionSets;

@Entity
@Table(name="jobcategorymaster")
public class JobCategoryMaster implements Serializable
{
	private static final long serialVersionUID = -1548576530279008455L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobCategoryId; 
	private String jobCategoryName;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="QuestionSetID",referencedColumnName="ID")
	private QqQuestionSets questionSets;	 
	private Boolean jobInviteOnly;
	private Boolean offerDSPQ;
	private Boolean hiddenJob;
	private Boolean preHireSmartPractices;
	private Boolean postHireSmartPractices;
	private Boolean qualificationQuestion;
	private Boolean baseStatus;
	private Boolean epiForFullTimeTeachers;
	private Boolean offerDistrictSpecificItems;
	private Boolean offerQualificationItems;
	private Integer offerJSI;
	private Boolean offerPortfolioNeeded;
	private String assessmentDocument;
	private String status;
	private Boolean offerVVIForInternalCandidates;
	private Boolean offerVirtualVideoInterview;
	private Boolean epiForIMCandidates;
	private Boolean portfolioForIMCandidates;
	private Boolean districtSpecificItemsForIMCandidates;
	private Boolean qualificationItemsForIMCandidates;
	private Integer jsiForIMCandidates;
	
	@ManyToOne
	@JoinColumn(name="vviQuestionSet",referencedColumnName="ID")
	private I4QuestionSets i4QuestionSets;
	private Integer maxScoreForVVI;
	private Boolean sendAutoVVILink;
	private Integer assessmentGroupId;
	private Boolean jobCompletedVVILink;
	
/*	@ManyToOne
	@JoinColumn(name="statusIdForAutoVVILink",referencedColumnName="statusId")
	private StatusMaster statusMaster ;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusIdForAutoVVILink",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus ;*/
	
	private Integer statusIdForAutoVVILink;
	private Integer secondaryStatusIdForAutoVVILink;
	private Integer timeAllowedPerQuestion;
	private Integer VVIExpiresInDays;
	private Integer minDaysJobWillDisplay;
	private Boolean offerAssessmentInviteOnly;
	private String districtAssessmentId;
	private Boolean schoolSelection;
	
	@ManyToOne
	@JoinColumn(name="parentJobCategoryId", referencedColumnName = "jobCategoryId")
	private JobCategoryMaster parentJobCategoryId;
	private Boolean attachDSPQFromJC;
	private Boolean attachSLCFromJC;
	
	@ManyToOne
	@JoinColumn(name="QuestionSetIDForOnboard",referencedColumnName="ID")
	private QqQuestionSets questionSetsForOnboarding;
	
	private Boolean approvalBeforeGoLive;
	private Integer noOfApprovalNeeded;
	private Boolean buildApprovalGroup;
	private Boolean approvalByPredefinedGroups;
	private Integer spAssessmentId;
	private String primaryJobCode;
	private Integer autoRejectScore;
	
	public String getPrimaryJobCode() {
		return primaryJobCode;
	}
	public void setPrimaryJobCode(String primaryJobCode) {
		this.primaryJobCode = primaryJobCode;
	}

	public Boolean getApprovalByPredefinedGroups() {
		return approvalByPredefinedGroups;
	}

	public void setApprovalByPredefinedGroups(Boolean approvalByPredefinedGroups) {
		this.approvalByPredefinedGroups = approvalByPredefinedGroups;
	}
	
	public QqQuestionSets getQuestionSetsForOnboarding() {
		return questionSetsForOnboarding;
	}
	public void setQuestionSetsForOnboarding(
			QqQuestionSets questionSetsForOnboarding) {
		this.questionSetsForOnboarding = questionSetsForOnboarding;
	}
	public Boolean getApprovalBeforeGoLive() {
		return approvalBeforeGoLive==null?false:approvalBeforeGoLive;
	}
	public void setApprovalBeforeGoLive(Boolean approvalBeforeGoLive) {
		this.approvalBeforeGoLive = approvalBeforeGoLive;
	}
	public Integer getNoOfApprovalNeeded() {
		return noOfApprovalNeeded==null?0:noOfApprovalNeeded;
	}
	public void setNoOfApprovalNeeded(Integer noOfApprovalNeeded) {
		this.noOfApprovalNeeded = noOfApprovalNeeded;
	}
	public Boolean getBuildApprovalGroup() {
		return buildApprovalGroup==null?false:buildApprovalGroup;
	}
	public void setBuildApprovalGroup(Boolean buildApprovalGroup) {
		this.buildApprovalGroup = buildApprovalGroup;
	}
	public Boolean getSchoolSelection() {
		return schoolSelection==null?true:schoolSelection;
	}
	public void setSchoolSelection(Boolean schoolSelection) {
		this.schoolSelection = schoolSelection;
	}
	public Boolean getOfferDistrictSpecificItems() {
		return offerDistrictSpecificItems==null?false:offerDistrictSpecificItems;
	}
	public void setOfferDistrictSpecificItems(Boolean offerDistrictSpecificItems) {
		this.offerDistrictSpecificItems = offerDistrictSpecificItems;
	}
	public Boolean getOfferQualificationItems() {
		return offerQualificationItems==null?false:offerQualificationItems;
	}
	public void setOfferQualificationItems(Boolean offerQualificationItems) {
		this.offerQualificationItems = offerQualificationItems;
	}
	public Integer getOfferJSI() {
		return offerJSI==null?0:offerJSI;
	}
	public void setOfferJSI(Integer offerJSI) {
		this.offerJSI = offerJSI;
	}
	public Boolean getOfferPortfolioNeeded() {
		return offerPortfolioNeeded==null?false:offerPortfolioNeeded;
	}
	public void setOfferPortfolioNeeded(Boolean offerPortfolioNeeded) {
		this.offerPortfolioNeeded = offerPortfolioNeeded;
	}
	public Integer getJobCategoryId() {
		return jobCategoryId;
	}
	public void setJobCategoryId(Integer jobCategoryId) {
		this.jobCategoryId = jobCategoryId;
	}
	public String getJobCategoryName() {
		return jobCategoryName;
	}
	public void setJobCategoryName(String jobCategoryName) {
		this.jobCategoryName = jobCategoryName;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public Boolean getBaseStatus() {
		
		return baseStatus==null?false:baseStatus;
	}
	public void setBaseStatus(Boolean baseStatus) {
		this.baseStatus = baseStatus;
	}
	public String getAssessmentDocument() {
		return assessmentDocument;
	}
	public void setAssessmentDocument(String assessmentDocument) {
		this.assessmentDocument = assessmentDocument;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getEpiForFullTimeTeachers() {
		return epiForFullTimeTeachers==null?false:epiForFullTimeTeachers;
	}
	public void setEpiForFullTimeTeachers(Boolean epiForFullTimeTeachers) {
		this.epiForFullTimeTeachers = epiForFullTimeTeachers;
	}
	public Boolean getOfferVVIForInternalCandidates() {
		return offerVVIForInternalCandidates;
	}
	public void setOfferVVIForInternalCandidates(
			Boolean offerVVIForInternalCandidates) {
		this.offerVVIForInternalCandidates = offerVVIForInternalCandidates;
	}
	public Boolean getOfferVirtualVideoInterview() {
		return offerVirtualVideoInterview==null ?false:offerVirtualVideoInterview;
	}
	public void setOfferVirtualVideoInterview(Boolean offerVirtualVideoInterview) {
		this.offerVirtualVideoInterview = offerVirtualVideoInterview;
	}
	public I4QuestionSets getI4QuestionSets() {
		return i4QuestionSets;
	}
	public void setI4QuestionSets(I4QuestionSets i4QuestionSets) {
		this.i4QuestionSets = i4QuestionSets;
	}
	public Integer getMaxScoreForVVI() {
		return maxScoreForVVI;
	}
	public void setMaxScoreForVVI(Integer maxScoreForVVI) {
		this.maxScoreForVVI = maxScoreForVVI;
	}
	public Boolean getSendAutoVVILink() {
		return sendAutoVVILink;
	}
	public void setSendAutoVVILink(Boolean sendAutoVVILink) {
		this.sendAutoVVILink = sendAutoVVILink;
	}
	public Integer getTimeAllowedPerQuestion() {
		return timeAllowedPerQuestion;
	}
	public void setTimeAllowedPerQuestion(Integer timeAllowedPerQuestion) {
		this.timeAllowedPerQuestion = timeAllowedPerQuestion;
	}
	public Integer getVVIExpiresInDays() {
		return VVIExpiresInDays;
	}
	public void setVVIExpiresInDays(Integer vVIExpiresInDays) {
		VVIExpiresInDays = vVIExpiresInDays;
	}
	public Integer getMinDaysJobWillDisplay() {
		return minDaysJobWillDisplay==null?8:minDaysJobWillDisplay;
		
	}
	public void setMinDaysJobWillDisplay(Integer minDaysJobWillDisplay) {
		this.minDaysJobWillDisplay = minDaysJobWillDisplay;
	}
	public Boolean getOfferAssessmentInviteOnly() {
		return offerAssessmentInviteOnly;
	}
	public void setOfferAssessmentInviteOnly(Boolean offerAssessmentInviteOnly) {
		this.offerAssessmentInviteOnly = offerAssessmentInviteOnly;
	}
	public String getDistrictAssessmentId() {
		return districtAssessmentId;
	}
	public void setDistrictAssessmentId(String districtAssessmentId) {
		this.districtAssessmentId = districtAssessmentId;
	}
	public Boolean getEpiForIMCandidates() {
		return epiForIMCandidates;
	}
	public void setEpiForIMCandidates(Boolean epiForIMCandidates) {
		this.epiForIMCandidates = epiForIMCandidates;
	}
	public Boolean getPortfolioForIMCandidates() {
		return portfolioForIMCandidates;
	}
	public void setPortfolioForIMCandidates(Boolean portfolioForIMCandidates) {
		this.portfolioForIMCandidates = portfolioForIMCandidates;
	}
	public Boolean getDistrictSpecificItemsForIMCandidates() {
		return districtSpecificItemsForIMCandidates;
	}
	public void setDistrictSpecificItemsForIMCandidates(Boolean districtSpecificItemsForIMCandidates) {
		this.districtSpecificItemsForIMCandidates = districtSpecificItemsForIMCandidates;
	}
	public Boolean getQualificationItemsForIMCandidates() {
		return qualificationItemsForIMCandidates;
	}
	public void setQualificationItemsForIMCandidates(
			Boolean qualificationItemsForIMCandidates) {
		this.qualificationItemsForIMCandidates = qualificationItemsForIMCandidates;
	}
	public Integer getJsiForIMCandidates() {
		return jsiForIMCandidates;
	}
	public void setJsiForIMCandidates(Integer jsiForIMCandidates) {
		this.jsiForIMCandidates = jsiForIMCandidates;
	}
	public QqQuestionSets getQuestionSets() {
		return questionSets;
	}
	public void setQuestionSets(QqQuestionSets questionSets) {
		this.questionSets = questionSets;
	}
	public Integer getStatusIdForAutoVVILink() {
		return statusIdForAutoVVILink;
	}
	public void setStatusIdForAutoVVILink(Integer statusIdForAutoVVILink) {
		this.statusIdForAutoVVILink = statusIdForAutoVVILink;
	}
	public Integer getSecondaryStatusIdForAutoVVILink() {
		return secondaryStatusIdForAutoVVILink;
	}
	public void setSecondaryStatusIdForAutoVVILink(Integer secondaryStatusIdForAutoVVILink) {
		this.secondaryStatusIdForAutoVVILink = secondaryStatusIdForAutoVVILink;
	}
	public Boolean getAttachDSPQFromJC() {
		return attachDSPQFromJC==null?false:attachDSPQFromJC;
	}
	public void setAttachDSPQFromJC(Boolean attachDSPQFromJC) {
		this.attachDSPQFromJC = attachDSPQFromJC;
	}
	public Boolean getAttachSLCFromJC() {
		return attachSLCFromJC==null?false:attachSLCFromJC;
	}
	public void setAttachSLCFromJC(Boolean attachSLCFromJC) {
		this.attachSLCFromJC = attachSLCFromJC;
	}
	public JobCategoryMaster getParentJobCategoryId() {
		return parentJobCategoryId;
	}
	public void setParentJobCategoryId(JobCategoryMaster parentJobCategoryId) {
		this.parentJobCategoryId = parentJobCategoryId;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public Boolean getJobInviteOnly() {
		return jobInviteOnly==null?false:jobInviteOnly;
	}
	public void setJobInviteOnly(Boolean jobInviteOnly) {
		this.jobInviteOnly = jobInviteOnly;
	}
	public Boolean getHiddenJob() {
		return hiddenJob;
	}
	public void setHiddenJob(Boolean hiddenJob) {
		this.hiddenJob = hiddenJob;
	}
	public Boolean getPreHireSmartPractices() {
		return preHireSmartPractices==null?false:preHireSmartPractices;
	}
	public void setPreHireSmartPractices(Boolean preHireSmartPractices) {
		this.preHireSmartPractices = preHireSmartPractices;
	}
	public Boolean getQualificationQuestion() {
		return qualificationQuestion==null?false:qualificationQuestion;
	}
	public void setQualificationQuestion(Boolean qualificationQuestion) {
		this.qualificationQuestion = qualificationQuestion;
	}
	public Boolean getOfferDSPQ() {
		return offerDSPQ==null?false:offerDSPQ;
	}
	public void setOfferDSPQ(Boolean offerDSPQ) {
		this.offerDSPQ = offerDSPQ;
	}
	public Integer getSpAssessmentId() {
		return spAssessmentId;
	}
	public void setSpAssessmentId(Integer spAssessmentId) {
		this.spAssessmentId = spAssessmentId;
	}
	public Integer getAssessmentGroupId() {
		return assessmentGroupId;
	}
	public void setAssessmentGroupId(Integer assessmentGroupId) {
		this.assessmentGroupId = assessmentGroupId;
	}
	public Boolean getJobCompletedVVILink() {
		return jobCompletedVVILink;
	}
	public void setJobCompletedVVILink(Boolean jobCompletedVVILink) {
		this.jobCompletedVVILink = jobCompletedVVILink;
	}
	public Boolean getPostHireSmartPractices() {
		return postHireSmartPractices;
	}
	public void setPostHireSmartPractices(Boolean postHireSmartPractices) {
		this.postHireSmartPractices = postHireSmartPractices;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobCategoryMaster other = (JobCategoryMaster) obj;
		if (jobCategoryName == null) {
			if (other.jobCategoryName != null)
				return false;
		} else if (!jobCategoryName.equals(other.jobCategoryName))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((jobCategoryName == null) ? 0 : jobCategoryName.hashCode());
		return result;
	}
	
	@Override
	public String toString() 
	{
		return this.jobCategoryName;
	}
	public Integer getAutoRejectScore() {
		return autoRejectScore;
	}
	public void setAutoRejectScore(Integer autoRejectScore) {
		this.autoRejectScore = autoRejectScore;
	}
}
