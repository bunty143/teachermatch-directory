package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ethinicitymaster")
public class EthinicityMaster implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3941737310782358493L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer ethnicityId;
	
	private String ethnicityCode;
	private String ethnicityName;
	private Integer orderBy;
	private String status;
	
	public String getEthnicityCode() {
		return ethnicityCode;
	}
	public void setEthnicityCode(String ethnicityCode) {
		this.ethnicityCode = ethnicityCode;
	}
	public Integer getEthnicityId() {
		return ethnicityId;
	}
	public void setEthnicityId(Integer ethnicityId) {
		this.ethnicityId = ethnicityId;
	}
	public String getEthnicityName() {
		return ethnicityName;
	}
	public void setEthnicityName(String ethnicityName) {
		this.ethnicityName = ethnicityName;
	}
	public Integer getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
