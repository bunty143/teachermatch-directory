package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;

@Entity
@Table(name="statusspecificscore")
public class StatusSpecificScore implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2422123975285557870L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer answerId;
	
	private Integer headQuarterId;
	private Integer branchId;
	private Integer districtId;
	
	private Long schoolId;
	
	//@ManyToOne
	//@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	///private JobCategoryMaster jobCategoryMaster;
	private Integer jobCategoryId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	//@ManyToOne
	//@JoinColumn(name="statusId",referencedColumnName="statusId")
	//private StatusMaster statusMaster;
	private Integer statusId;
	
	//@ManyToOne
	//@JoinColumn(name="secondaryStatusId",referencedColumnName="secondaryStatusId")
	//private SecondaryStatus secondaryStatus;
	private Integer secondaryStatusId;
	
	@ManyToOne
	@JoinColumn(name="questionId",referencedColumnName="questionId")
	private StatusSpecificQuestions statusSpecificQuestions;
	
	private String question;
	
	@ManyToOne
	@JoinColumn(name="answerDetailId",referencedColumnName="answerId")
	private TeacherAnswerDetail teacherAnswerDetail;
	
	
	@ManyToOne
	@JoinColumn(name="scoringCaptionId",referencedColumnName="skillAttributeId")
	private SkillAttributesMaster skillAttributesMaster;
	//private Integer scoringCaptionId;
	
	
	private String scoringCaption;
	
	private Integer scoreProvided;
	
	private Double maxScore;
	
	private Integer finalizeStatus;
	
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userMaster;
	
	
	private Date createdDateTime;

	

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public SkillAttributesMaster getSkillAttributesMaster() {
		return skillAttributesMaster;
	}

	public void setSkillAttributesMaster(SkillAttributesMaster skillAttributesMaster) {
		this.skillAttributesMaster = skillAttributesMaster;
	}

	public Integer getJobCategoryId() {
		return jobCategoryId;
	}

	public void setJobCategoryId(Integer jobCategoryId) {
		this.jobCategoryId = jobCategoryId;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getSecondaryStatusId() {
		return secondaryStatusId;
	}

	public void setSecondaryStatusId(Integer secondaryStatusId) {
		this.secondaryStatusId = secondaryStatusId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Integer getHeadQuarterId() {
		return headQuarterId;
	}

	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public TeacherAnswerDetail getTeacherAnswerDetail() {
		return teacherAnswerDetail;
	}

	public void setTeacherAnswerDetail(TeacherAnswerDetail teacherAnswerDetail) {
		this.teacherAnswerDetail = teacherAnswerDetail;
	}

	public Integer getAnswerId() {
		return answerId;
	}

	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}

	

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public StatusSpecificQuestions getStatusSpecificQuestions() {
		return statusSpecificQuestions;
	}

	public void setStatusSpecificQuestions(
			StatusSpecificQuestions statusSpecificQuestions) {
		this.statusSpecificQuestions = statusSpecificQuestions;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getScoringCaption() {
		return scoringCaption;
	}

	public void setScoringCaption(String scoringCaption) {
		this.scoringCaption = scoringCaption;
	}

	public Integer getScoreProvided() {
		return scoreProvided;
	}

	public void setScoreProvided(Integer scoreProvided) {
		this.scoreProvided = scoreProvided;
	}

	

	public Double getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(Double maxScore) {
		this.maxScore = maxScore;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public Integer getFinalizeStatus() {
		return finalizeStatus;
	}

	public void setFinalizeStatus(Integer finalizeStatus) {
		this.finalizeStatus = finalizeStatus;
	}
	
	
	
	}
