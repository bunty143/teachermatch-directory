package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="statemaster")
public class StateMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3190297470741634624L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long stateId;     
	private String stateName;             
	private String stateShortName;	
	@ManyToOne
	@JoinColumn(name="regionId",referencedColumnName="regionId")
	private RegionMaster regionId;	
	private String status;
	
	@ManyToOne
	@JoinColumn(name="countryId",referencedColumnName="countryId")
	private CountryMaster countryMaster;
	
	
	public Long getStateId() {
		return stateId;
	}
	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getStateShortName() {
		return stateShortName;
	}
	public void setStateShortName(String stateShortName) {
		this.stateShortName = stateShortName;
	}
	public RegionMaster getRegionId() {
		return regionId;
	}
	public void setRegionId(RegionMaster regionId) {
		this.regionId = regionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public CountryMaster getCountryMaster() {
		return countryMaster;
	}
	public void setCountryMaster(CountryMaster countryMaster) {
		this.countryMaster = countryMaster;
	}
	@Override
	public String toString() {
		return "StateMaster [countryMaster=" + countryMaster + ", regionId="
				+ regionId + ", stateId=" + stateId + ", stateName="
				+ stateName + ", stateShortName=" + stateShortName
				+ ", status=" + status + "]";
	}	
}
