package tm.utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TestTool {

	public static void getTraceTime(String sSequenceNo)
	{
	     /* int second, minute,MILLISECOND;
	      GregorianCalendar date = new GregorianCalendar();
	 
	      MILLISECOND = date.get(Calendar.MILLISECOND);
	      second = date.get(Calendar.SECOND);
	      minute = date.get(Calendar.MINUTE);
	      
	      String stime=minute+":"+second+":"+MILLISECOND;
	 
		if(sSequenceNo!=null && !sSequenceNo.equalsIgnoreCase(""))
			System.out.println(sSequenceNo+" "+stime);
		else
			System.out.println(stime);*/
	}
	
	public static void getTraceTimeStatus(String sSequenceNo)
	{
	      int second, minute,MILLISECOND;
	      GregorianCalendar date = new GregorianCalendar();
	 
	      MILLISECOND = date.get(Calendar.MILLISECOND);
	      second = date.get(Calendar.SECOND);
	      minute = date.get(Calendar.MINUTE);
	      
	      String stime=minute+":"+second+":"+MILLISECOND;
	 
		if(sSequenceNo!=null && !sSequenceNo.equalsIgnoreCase(""))
			System.out.println(sSequenceNo+" "+stime);
		else
			System.out.println(stime);
	}

	public static void getTraceTimeTPAP(String sSequenceNo)
	{
	      int second, minute,MILLISECOND;
	      GregorianCalendar date = new GregorianCalendar();
	 
	      MILLISECOND = date.get(Calendar.MILLISECOND);
	      second = date.get(Calendar.SECOND);
	      minute = date.get(Calendar.MINUTE);
	      
	      String stime=minute+":"+second+":"+MILLISECOND;
	 
		if(sSequenceNo!=null && !sSequenceNo.equalsIgnoreCase(""))
			System.out.println(sSequenceNo+" "+stime);
		else
			System.out.println(stime);
	}
	public static void getTraceSecond(String sSequenceNo)
	{
	      int second, minute,MILLISECOND;
	      GregorianCalendar date = new GregorianCalendar();
	 
	      MILLISECOND = date.get(Calendar.MILLISECOND);
	      second = date.get(Calendar.SECOND);
	      minute = date.get(Calendar.MINUTE);
	      
	      String stime=minute+":"+second+":"+MILLISECOND;
	 
		if(sSequenceNo!=null && !sSequenceNo.equalsIgnoreCase(""))
			System.out.println(sSequenceNo+" "+stime);
		else
			System.out.println(stime);
	}
	public static void getTraceTimeForJobOfInterest(String sSequenceNo)
	{
	      int second, minute,MILLISECOND;
	      GregorianCalendar date = new GregorianCalendar();
	 
	      MILLISECOND = date.get(Calendar.MILLISECOND);
	      second = date.get(Calendar.SECOND);
	      minute = date.get(Calendar.MINUTE);
	      
	      String stime=minute+":"+second+":"+MILLISECOND;
	 
		if(sSequenceNo!=null && !sSequenceNo.equalsIgnoreCase(""))
			System.out.println(sSequenceNo+" "+stime);
		else
			System.out.println(stime);
	}
	
	public static String getTraceTotalSecond(String sSequenceFirst,String sSequenceLast,String sSequenceNo)
	{
		String timediff=""; 
		try {
			
			String time1 = sSequenceFirst;
			String time2 = sSequenceLast;
	 
			SimpleDateFormat format = new SimpleDateFormat("mm:ss:SSS");
			Date date1 = format.parse(time1);
			Date date2 = format.parse(time2);
			long difference = date2.getTime() - date1.getTime();
			
			if(sSequenceNo!=null && !sSequenceNo.equalsIgnoreCase("")){
				System.out.println(sSequenceNo+" "+difference);
			timediff=difference+"";
		}else{
				System.out.println(difference);
				timediff=difference+"";
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return timediff;
	}
	
	public static String getTraceTimeReturn(String sSequenceNo)
	{
	      int second, minute,MILLISECOND;
	      GregorianCalendar date = new GregorianCalendar();
	 
	      MILLISECOND = date.get(Calendar.MILLISECOND);
	      second = date.get(Calendar.SECOND);
	      minute = date.get(Calendar.MINUTE);
	      
	      String stime=minute+":"+second+":"+MILLISECOND;
	      return stime;
/*		if(sSequenceNo!=null && !sSequenceNo.equalsIgnoreCase(""))
			System.out.println(sSequenceNo+" "+stime);
		else
			System.out.println(stime);*/
	}
}
