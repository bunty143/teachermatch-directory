package tm.utility;

public class ElasticSearchConfig {
	
	// For local
	public static String sElasticSearchURL=Utility.getValueOfPropByKey("ElasticSearchURL");
	public static String sElasticSearchDBURL=Utility.getValueOfPropByKey("ElasticSearchDBURL");
	public static String sElasticSearchDBUser=Utility.getValueOfPropByKey("ElasticSearchDBUser");
	public static String sElasticSearchDBPass=Utility.getValueOfPropByKey("ElasticSearchDBPass");
	public static String sElasticSearchURLPlatform=Utility.getValueOfPropByKey("ElasticSearchURLPlatform");
	
	
	//for districtJobboard
	public static String indexForDistrictJobBoard="districtjobboard";
	public static String indexForDistrictJobOrder="districtjoborder";
	public static String indexForjobsOfInterest="jobsofinterest";
	public static String indexForjobsOfInterestForCandidate="jobsofinterestforcandidate";
	public static String indexForschoolJobOrder="schooljoborder";
	public static String indexForhqDistrictJobBoard="hqdistrictjobboard";
	
	public static String serverName=Utility.getValueOfPropByKey("serverName");
	public static String serviceURL=Utility.getValueOfPropByKey("serviceURL");
	public static int rowSize=1000;
	
	
	
	// For Titan New
	/*public static String sElasticSearchURL="http://localhost:9200";
	public static String sElasticSearchDBURL="jdbc:mysql://10.183.250.112/teachermatch";
	public static String sElasticSearchDBUser="teachermatch";
	public static String sElasticSearchDBPass="Teacher@Match2016";*/
	
	// titan -a 
	/*public static String sElasticSearchURL="http://localhost:9200";
	public static String sElasticSearchDBURL="jdbc:mysql://166.78.114.12:3306/teachermatch";
	public static String sElasticSearchDBUser="teachermatch1";
	public static String sElasticSearchDBPass="Teacher@Match2016";*/
	
	
	// Main server 
	/*public static String sElasticSearchURL="http://10.178.5.188:9200";
	public static String sElasticSearchDBURL="jdbc:mysql://10.210.97.232:3306/teachermatch";
	public static String sElasticSearchDBUser="platform";
	public static String sElasticSearchDBUserForSch="platform";
	public static String sElasticSearchDBPass="platform2013";*/
	
	/*// titan server 
		public static String sElasticSearchURL="http://10.178.5.188:9200";
		public static String sElasticSearchDBURL="jdbc:mysql://10.178.128.70:3306/teachermatch";
		public static String sElasticSearchDBUser="titannewstack";
		public static String sElasticSearchDBUserForSch="titannewstack";
		public static String sElasticSearchDBPass="TM@NS_2015";*/
	
	// platform server 
	/*public static String sElasticSearchURL="http://10.177.8.72:9200";
	public static String sElasticSearchDBURL="jdbc:mysql://10.210.74.130:3306/teachermatch";
	public static String sElasticSearchDBUser="platform";
	public static String sElasticSearchDBUserForSch="platform";
	public static String sElasticSearchDBPass="platform2013";*/
			
	public static String river="_river/";
	public static String jdbcRiver="_jdbc_river";
	
		
		
		
}
