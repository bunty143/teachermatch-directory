package tm.utility;

import java.io.*;
import java.util.*;
import java.util.logging.*;

/** Collected methods to ease manipulation of text files.*/
public final class FileUtil {

	/**
	 * @author : Vishwanath Kumar
	 * 
	 * Return the full content of <tt>aInputStream</tt> as a <tt>String</tt>.
	 *
	 * <P>If <tt>aInputStream</tt> has no content, then return an empty 
	 * <tt>String</tt>.
	 */
	public static String getStreamAsString(InputStream aInputStream){
		StringBuilder result = new StringBuilder(Consts.EMPTY_STRING);
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(aInputStream));
			try { 
				String line = null;
				while (( line = input.readLine()) != null){
					result.append(line);
					result.append(Consts.NEW_LINE);
				}
			}
			finally {
				input.close();
			}
		}
		catch(IOException ex){
			fLogger.severe("Cannot read input stream:" + aInputStream);
		}
		return result.toString();
	}

	/**
	 * Return the full content of <tt>aInputStream</tt> as an unmodifiable 
	 * <tt>List</tt> of <tt>String</tt>s, one for each line of input. 
	 *
	 * <P>The iteration order of the returned <tt>List</tt> reflects the 
	 * line order in the underlying stream. If <tt>aInputStream</tt> has 
	 * no content, then return an empty <tt>List</tt>.
	 *
	 * <P>Intended for text files having structured data, with each line 
	 * corresponding to a single record. The caller may use this method to transform 
	 * such a file into a collection, which may be iterated in the usual fashion.
	 */
	public static List<String> getStreamAsList(InputStream aInputStream){
		List<String> result = new ArrayList<String>();
		try {
			BufferedReader input = new BufferedReader( new InputStreamReader(aInputStream) );
			try {       
				String line = null;
				while (( line = input.readLine()) != null){
					result.add(line);
				}
			}
			finally {
				input.close();
			}
		}
		catch(IOException ex){
			fLogger.severe("Cannot read input stream:" + aInputStream);
		}
		return Collections.unmodifiableList(result);
	}

	//===================method to copy from one to another directory
	public static void copyDirectory(File srcPath, File dstPath)
	throws IOException{

		if (srcPath.isDirectory()){
			if (!dstPath.exists()){
				dstPath.mkdir();
			}

			String files[] = srcPath.list();

			for(int i = 0; i < files.length; i++){
				copyDirectory(new File(srcPath, files[i]), 
						new File(dstPath, files[i]));
			}
		}
		else{
			if(!srcPath.exists()){
				System.out.println("File or directory does not exist.");
				System.exit(0);
			}
			else
			{

				InputStream in = new FileInputStream(srcPath);
				OutputStream out = new FileOutputStream(dstPath); 
				// Transfer bytes from in to out
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				out.close();
			}
		}
		System.out.println("Directory copied.");
	}

	  
	  public static String getRemoveLineFromFile(String file) {
		  String lineToRemove="not available"; 
		  try {
		      File inFile = new File(file);
		      String line = null;
		      if (!inFile.isFile()) {
		        System.out.println("Parameter is not an existing file");
		        return "FNF";
		      }
		      //Construct the new file that will later be renamed to the original filename. 
		      File tempFile = new File(inFile.getAbsolutePath() + ".tmp");
		      BufferedReader br = new BufferedReader(new FileReader(file));
		      PrintWriter pw = null; 

		      //Read from the original file and write to the new 
		      //unless content matches data to be removed.
		     if((line = br.readLine()) != null) {
		    	 lineToRemove=line;
		    	 pw=new PrintWriter(new FileWriter(tempFile));
		    	 while ((line = br.readLine()) != null) {
		 	        
		 	        if (!line.trim().equals(lineToRemove)) {

		 	          pw.println(line);
		 	          pw.flush();
		 	        }
		 	      }
		    	 
		    	 pw.close();
			     br.close();
			     
			      
			      //Delete the original file
			      if (!inFile.delete()) {
			        System.out.println("Could not delete file");
			        return "FND";
			      } 
			      
			      //Rename the new file to the filename the original file had.
			      if (!tempFile.renameTo(inFile)){
			        System.out.println("Could not rename file");
			      	return "FNR" ;
			      }
		     }
		     else{
		    	 lineToRemove="0";
		     }
		    }
		    catch (FileNotFoundException ex) {
		      ex.printStackTrace();
		    }
		    catch (IOException ex) {
		      ex.printStackTrace();
		    }
		    finally{
		    	
		    }
		    return lineToRemove;
		  }

	// PRIVATE //
	private static final Logger fLogger = Util.getLogger(FileUtil.class);  
}
