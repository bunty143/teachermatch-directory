package tm.utility;

import java.util.Comparator;

import tm.bean.JobOrder;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;

public class NormScoreforJobOrderCompratorDesc implements Comparator<JobOrder> 
{
	public int compare(JobOrder o1, JobOrder o2) {
		Double cScore1 =o1.getNormScore();
		Double cScore2 =o2.getNormScore();		
		int c = cScore2.compareTo(cScore1);
		return c;
	}

	
}
