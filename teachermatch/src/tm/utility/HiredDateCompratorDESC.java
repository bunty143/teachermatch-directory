package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;

public class HiredDateCompratorDESC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		return o2.getHiredDate().compareTo(o1.getHiredDate());
	}
}
