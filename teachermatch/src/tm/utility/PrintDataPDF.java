package tm.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.itextpdf.text.Document;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

public class PrintDataPDF
{
	private File output;
	private String outputFileName;
	private String outputFilePath;
	private FileOutputStream fos;
	private Document document;
	private boolean isCreatePDF;
	private List<String> pdfFilesList = new ArrayList<String>();
	private String htmlText="";
	private int currentColspan=0;
	private String tableName="";
	private String webappURL="";
	
	private Map<String, String> targetSourceFileMapping = new HashMap<String, String>();
	private String correptedFileName="";
		
	public PrintDataPDF(String output, boolean isCreatePDF, String webappURL)
	{
		if(output!=null && !output.equals("") && webappURL!=null && !webappURL.equals(""))
		{
		this.output = new File(output);
		this.isCreatePDF = isCreatePDF;
		this.webappURL = webappURL;
		
		outputFileName = this.output.getName();
		outputFilePath = this.output.toString().substring(0, output.toString().lastIndexOf(outputFileName)-1);
		
		if( !this.output.exists())
			this.output.mkdirs();
		
		System.out.println("outputFilePath:- "+outputFilePath);
		File test = new File(outputFilePath);
		if(test.listFiles()!=null)
			Utility.deleteAllFileFromDir(outputFilePath);
		
		if(this.isCreatePDF)
		{
			this.isCreatePDF =initiatePDFWriter(null);
		}
		}
		else
			isCreatePDF=false;
	}
	
	private boolean initiatePDFWriter(FileOutputStream fos)
	{
		try
		{
			outputFileName = output.getName();
			outputFilePath = output.toString().substring(0, output.toString().lastIndexOf(outputFileName));
			
			if(fos==null)
			{
				output = new File(outputFilePath+outputFileName);
				this.fos = new FileOutputStream(output);
			}
			else
				this.fos = fos;
			
			document = new Document();
			PdfWriter.getInstance(document, this.fos);
			document.open();
			
			
			
			if( !(outputFilePath.endsWith("/") || outputFilePath.endsWith("\\")) )
				outputFilePath = outputFilePath + File.separator;
			
			return true;
		}
		catch(Exception e)
		{
			System.out.println("Initiate PDF Writer is failed");
		}
		return false;
	}
	
	private boolean closePDFWriter()
	{
		try
		{
			try
			{
			if(document!=null)
				document.close();
			}
			catch(Exception e){}
			
			try
			{
			if(fos!=null)
				fos.close();
			}
			catch(Exception e){}
			
			fos = null;
			document=null;
			
			return true;
		}
		catch(Exception e)
		{
			System.out.println("Could not ClosedPDFWriter");
		}
		return false;
	}
	//Pavan Gupta
	public void appendCreatedTextIntoPdf()
	{		
		if(isCreatePDF)
		{
			htmlText = htmlText + " </table></div></body></html>";
			try
			{	//document.newPage();		
				HTMLWorker htmlWorker = new HTMLWorker(document);
				htmlWorker.parse(new StringReader(htmlText));
				System.out.println("Text is appended successfully using appendCreatedTextIntoPdf");
			}
			catch(Exception e){}
			htmlText ="";
			currentColspan=0;
		}
	}
	public void appendTextIntoPdf(String appendText, boolean isCreateNewPage)
	{
		if(isCreatePDF)
		{
			String htmlText = "<html><body><table> "+appendText+" </table></body></html>";
			try
			{
				if(isCreateNewPage)
					document.newPage();
				HTMLWorker htmlWorker = new HTMLWorker(document);
				htmlText = htmlText.replaceAll("[<](/)?img[^>]*[>]", "").replaceAll("[<](/)?a[^>]*[>]", "").replaceAll("line-height", "");					
				htmlWorker.parse(new StringReader(htmlText) );
				System.out.println("Text is appended successfully:- "+htmlText);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			htmlText ="";
		}
	}	
	public void appendTableName(String... tableHeading)
	{
		if(isCreatePDF && tableHeading!=null)
		{
			System.out.println("Table is creating "+tableHeading[0]);
			this.tableName = "<div><b>"+tableHeading[0]+"</b>";
		}
	}
	
	public void appendHeading(String tableHeading)
	{
		if(isCreatePDF && tableHeading!=null)
		{
			System.out.println("Table is creating "+tableHeading);
			this.tableName = tableHeading;
		}
	}
	
	public void appendColumn(String... columnNames)
	{
		if(isCreatePDF)
		{
			currentColspan = columnNames.length;
			
			if(htmlText.equals(""))
				htmlText = htmlText + "<html><body><div style='font-size: 8px'><br><table><tr>";
			
			if(!tableName.equals(""))
			{
				htmlText = htmlText+"<td colspan='"+currentColspan+"'> "+tableName+"</td></tr><tr>";
				tableName="";
			}
			
			if(columnNames!=null && columnNames.length>0)
				for(String cname : columnNames)
					htmlText = htmlText+"<td><b>"+cname+"</b></td>";
			htmlText = htmlText+"</tr>";
		}
	}
	
	public void appendRows(String... rowsValues)
	{
		if(isCreatePDF)
		{
			if(rowsValues!=null && rowsValues.length>0)
			{
				htmlText = htmlText+"<tr>";
				for(String rname : rowsValues)
					htmlText = htmlText+"<td>"+rname+"</td>";
				htmlText = htmlText+"</tr>";
			}
		}
	}
	
	public void appendRows(String rowsValues, boolean isColspan)
	{
		if(isCreatePDF)
		{
			if(rowsValues!=null)
				if(isColspan)
					htmlText = htmlText+"<tr><td colspan='"+currentColspan+"'> "+rowsValues+" </td></tr>";
				else
					htmlText = htmlText+"<tr><td>"+rowsValues+" </td></tr>";
		}
	}
	public void appendFileIntoPdf(String FilePathWithName)
	{		
		if(isCreatePDF)
		{
			System.out.println(":::::::::::::::::::::::::::: File is appended Successfully....");
			
			closePDFWriter();
			
			File sourceFile = new File(FilePathWithName);
			File targetFile = new File(outputFilePath+ Utility.getUniqueNo()+"_Resume.pdf");
			
			File outputFileRename = new File(outputFilePath+Utility.getUniqueNo()+"_MergedFile"+outputFileName);
			output.renameTo(outputFileRename);
			pdfFilesList.add(outputFileRename.toString());
			targetSourceFileMapping.put(outputFileRename.toString(), output.toString());
			
			if(Utility.convertToPdf(sourceFile, targetFile))
			{
				targetSourceFileMapping.put(targetFile.toString(), sourceFile.toString());
				pdfFilesList.add(targetFile.toString());
			}
			else
			if(targetFile!=null && targetFile.exists())
				targetFile.delete();
			
			initiatePDFWriter(null);
		}
	}
	
	public String destroy()
	{
		File finalPath = new File(outputFilePath+"temp_"+Utility.getUniqueNo()+"_FINAL "+outputFileName);
		
		closePDFWriter();
		
		pdfFilesList.add(output.toString());
		
		System.out.println("Total Files:- ");
		for(String nm : pdfFilesList)
			System.out.println("Path:- "+nm);
		
		
		
		if(pdfFilesList!=null && pdfFilesList.size()>1)
		if(mergePDF(pdfFilesList, finalPath))
		{
			if(finalPath.exists())
				outputFileName = finalPath.getName();
			else
				outputFileName="";
		}
		else
			outputFileName = "";
		
		return output.toString();
	}
	
	public String parseXMLCSSAndHTMLContent(String content)
	{
		if(isCreatePDF && content!=null && !content.equals("") )
		{
			String brTag = "#BR#";
			String data=content;
			if(!data.contains("w:LsdException"))
			{
				System.out.println("in the if condition of the printDataPDF file");
			content = content.replaceAll("<(?i)br>", brTag);
			content = content.replaceAll("<(?i)br/>", brTag);
			content = content.replaceAll("<(?i)p ", brTag+"<");
			content = content.replaceAll("<(?i)p>", brTag);
			
			content = content.replaceAll("(?s)<!--.*?-->","");	//remove all html comments and it's content
			content = content.replaceAll("(?s)<xml[^>]*>.*?</xml>","");	//remove all xml tag and it's  content
			content = content.replaceAll("(?s)<style[^>]*>.*?</style>","");	//remove all style tag and it's content
			//content = content.replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", " ");	//remove all html content
			content = content.replaceAll("&nbsp;","");	//remove all balnk space
			content = content.replaceAll("&n bsp;", "");	//remove all balnk space Special case
			
			content = content.replaceAll(brTag, "<br>");
			}
			else
			{
				System.out.println("in the else condition of the printDataPDF file");
				content = content.replaceAll("(?s)<!--.*?-->","");	//remove all html comments and it's content
				content = content.replaceAll("(?s)<xml[^>]*>.*?</xml>","");	//remove all xml tag and it's  content
				content = content.replaceAll("(?s)<style[^>]*>.*?</style>","");	//remove all style tag and it's content	
				content = content.replaceAll("&nbsp;","&#032;");
				content = content.replaceAll("cellpadding=\"0\"","cellpadding=\"10\"");
			}
			return content;
		}
		return "";
	}
	
	public String getPDFURL()
	{
		System.out.println("Final OutPut");
		webappURL = webappURL + outputFileName;
		
		System.out.println("OutputFile Name:- "+outputFileName);
		if( outputFileName.equals("") )
			return "#ErrorFileName#"+correptedFileName+"#";
		else
			return webappURL;
	}
	
	private boolean mergePDF(List<String> pdfFilePathList, File outputFile)
	{
		try
		{
			System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			System.out.println("Total No Of Files:- "+pdfFilePathList.size());
			Document document = new Document();
	        PdfCopy copy = new PdfCopy(document, new FileOutputStream(outputFile));
	        document.open();
	        
	        PdfReader reader=null;
	        int n;
	        
	        for (int i = 0; i < pdfFilePathList.size(); i++)
	        {
	        	if( new File(pdfFilePathList.get(i)).exists() )	//if File Exist
	        	{
	        		try
	        		{
	        			
	        			System.out.println("MergePDF(Target):- "+pdfFilePathList.get(i));
	        			reader = new PdfReader(pdfFilePathList.get(i));
	        			n = reader.getNumberOfPages();
	        			for (int page = 1; page <= n; page++)
	        			{
	        				copy.addPage(copy.getImportedPage(reader, page));
	        			}
	        			copy.freeReader(reader);
	        			
	        			reader.close();
	        		}
	        		catch(Exception e)
	        		{}
	        		
	        		catch(Throwable e)
	        		{
	        			if(targetSourceFileMapping.get(pdfFilePathList.get(i)) !=null)
	        			{
	        				correptedFileName = targetSourceFileMapping.get(pdfFilePathList.get(i));
	        				correptedFileName = correptedFileName.substring( correptedFileName.lastIndexOf(File.separator) );
	        			
	        				System.out.println("Could not merge PDF File correptedFileName:- "+correptedFileName);
	        				//e.printStackTrace();
	        			}
	        		}
	        	}
	        	else
	        		System.out.println("File Does not exist");
	        }
	        
	        document.close();
	        copy.close();
	        
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		}
		return false;
	}
}