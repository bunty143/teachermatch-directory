package tm.utility;

import java.util.Comparator;

import tm.bean.TeacherDetail;

public class TeacherDetailComparatorStatusDesc implements Comparator<TeacherDetail>{
	
	public int compare(TeacherDetail td1, TeacherDetail td2) {			
		return td2.gettStatus().compareTo(td1.gettStatus());
		
	}

}
