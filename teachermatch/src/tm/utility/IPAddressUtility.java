package tm.utility;

import javax.servlet.http.HttpServletRequest;

public class IPAddressUtility 
{
	public static String getIpAddress(HttpServletRequest request)
	{
		String sIPAddress=null;
		try {
			sIPAddress = request.getHeader("X-FORWARDED-FOR");  
			if (sIPAddress == null || sIPAddress.equals(""))
			{  
			   sIPAddress=request.getRemoteAddr();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			System.out.println("Client IP Address "+sIPAddress);
			return sIPAddress;
		}
	}
}
