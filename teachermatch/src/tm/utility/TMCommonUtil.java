package tm.utility;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.datacontract.schemas._2004._07.KSN_Entities_Data.Message;
import org.datacontract.schemas._2004._07.KSN_Entities_Data_KSNData_TeacherMatch.TalentDetail;
import org.datacontract.schemas._2004._07.KSN_Entities_Request.ExternalRequestBase;
import org.datacontract.schemas._2004._07.KSN_Entities_Request_KSNData.AuthenticateERegUserRequest;
import org.datacontract.schemas._2004._07.KSN_Entities_Request_KSNData.CheckForTalentEmailAddressRequest;
import org.datacontract.schemas._2004._07.KSN_Entities_Request_KSNData.InviteTalentToWorkflowRequest;
import org.datacontract.schemas._2004._07.KSN_Entities_Response_KSNData.AuthenticateERegUserResponse;
import org.datacontract.schemas._2004._07.KSN_Entities_Response_KSNData.CheckForTalentEmailAddressResponse;
import org.datacontract.schemas._2004._07.KSN_Entities_Response_KSNData.InviteTalentToWorkflowResponse;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.tempuri.IKSNDataServiceProxy;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;

import tm.api.UtilityAPI;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TalentKSNDetail;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherPersonalInfo;
import tm.bean.assessment.AssessmentCompetencyScore;
import tm.bean.assessment.AssessmentDomainScore;
import tm.bean.hqbranchesmaster.StatusNodeColorHistory;
import tm.bean.master.ApplitrackDistricts;
import tm.bean.master.DistrictMaster;
import tm.bean.master.RaceMaster;
import tm.bean.mq.MQEvent;
import tm.bean.mq.MQEventHistory;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.TalentKSNDetailDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.assessment.AssessmentCompetencyScoreDAO;
import tm.dao.assessment.AssessmentDomainScoreDAO;
import tm.dao.hqbranchesmaster.StatusNodeColorHistoryDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.mq.MQEventHistoryDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.mq.MQService;
import tm.services.report.CandidateGridService;
import tm.servlet.WorkThreadServlet;

import com.sun.jersey.api.core.InjectParam;

import edu.emory.mathcs.backport.java.util.Arrays;

public class TMCommonUtil {

	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private  MQEventDAO mqEventDAO;
	
	@Autowired
	private TalentKSNDetailDAO talentKSNDetailDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private StatusNodeColorHistoryDAO statusNodeColorHistoryDAO;
	
	@Autowired
	private CandidateGridService candidateGridService;
	
	static String locale = Utility.getValueOfPropByKey("locale");

	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static String updateSDSCHiredCandidate(JobForTeacher jft,TeacherPersonalInfo teacherPersonalInfo,Map<String,RaceMaster> raceMap,String postingNo)
	{
		String retrunValue = "";
		String strURL="https://patsuat.scsd.us/WCFServices/451API.svc/HiredApplicantData";
		
		PostMethod post = new PostMethod(strURL);
		try {
			JobForTeacher jobForTeacher = jft;
			TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
			JobOrder jobOrder = jobForTeacher.getJobId();
			System.out.println("teacherDetail.getTeacherId()::: "+teacherDetail.getTeacherId());
			System.out.println("jobOrder::: "+jobOrder.getJobId());
			//StringRequestEntity requestEntity = new StringRequestEntity(req,"application/xml","UTF-8");
			String candidateRace = "";
			String raceIds[] = new String[10];
			if(teacherPersonalInfo.getRaceId()!=null)
			{
				raceIds = teacherPersonalInfo.getRaceId().split(",");
			}
			if(raceIds.length>0)
			{
				for (int i = 0; i < raceIds.length; i++) {
					RaceMaster raceMaster = raceMap.get(""+raceIds[i]);
					if(raceMaster!=null)
					candidateRace+=raceMaster.getRaceName()+";";
				}

				if(candidateRace.length()>0)
				candidateRace = candidateRace.substring(0,candidateRace.length()-1);
			}
			/*String employeeType = "";
			if(teacherPersonalInfo.getEmployeeType()!=null)
			{
				if(teacherPersonalInfo.getEmployeeType()==1)
					employeeType = "Instructional";
				else
					employeeType = "Non-Instructional";
			}*/
			System.out.println("teacherPersonalInfo:::: "+teacherPersonalInfo.getEmailAddress());
			JSONObject jsonOutput = new JSONObject();
			jsonOutput.put("UserName", Utility.encodeInBase64("451API"));
			jsonOutput.put("Password", Utility.encodeInBase64("Gv@TYbqwt$A"));
			/*if(jobForTeacher.getRequisitionNumber()!=null)
				jsonOutput.put("PostingNumber", jobForTeacher.getRequisitionNumber());
			else
				jsonOutput.put("PostingNumber", "");*/
			if(jobOrder.getApiJobId()!=null)
				jsonOutput.put("JobCode", jobOrder.getApiJobId());
			else
				jsonOutput.put("JobCode", "");
			
			if(postingNo!=null)
			{
				jsonOutput.put("postingNo", postingNo);
			}else{
				jsonOutput.put("postingNo", "");
			}
			
			jsonOutput.put("EmailAddr", teacherDetail.getEmailAddress());
			jsonOutput.put("FirstName", teacherPersonalInfo.getFirstName());
			jsonOutput.put("LastName", teacherPersonalInfo.getLastName());
			
			if(teacherPersonalInfo.getAddressLine1()!=null)
				jsonOutput.put("StreetAddress",teacherPersonalInfo.getAddressLine1());
			else
				jsonOutput.put("StreetAddress","");
			if(teacherPersonalInfo.getCityId()!=null)
				jsonOutput.put("City", teacherPersonalInfo.getCityId().getCityName());
			else
				jsonOutput.put("City", "");
			if(teacherPersonalInfo.getStateId()!=null)
				jsonOutput.put("State", teacherPersonalInfo.getStateId().getStateShortName());
			else
				jsonOutput.put("State", "");
			if(teacherPersonalInfo.getZipCode()!=null)
				jsonOutput.put("Postal", teacherPersonalInfo.getZipCode());
			else
				jsonOutput.put("Postal", "");
			if(teacherPersonalInfo.getPhoneNumber()!=null)
				jsonOutput.put("HomePhone", teacherPersonalInfo.getPhoneNumber());
			else
				jsonOutput.put("HomePhone", "");
			if(teacherPersonalInfo.getDob()!=null)
				jsonOutput.put("DOB",  Utility.getDateWithoutTime(teacherPersonalInfo.getDob()));
			else
				jsonOutput.put("DOB", "");
			if(teacherPersonalInfo.getGenderId()!=null)
				jsonOutput.put("Gender", teacherPersonalInfo.getGenderId().getGenderName());
			else
				jsonOutput.put("Gender", "");
			
			if(teacherPersonalInfo.getEthnicityId()!=null)
				jsonOutput.put("Ethnicity", teacherPersonalInfo.getEthnicityId().getEthnicityName());
			else
				jsonOutput.put("Ethnicity", "");
			
			jsonOutput.put("Race", candidateRace);
			
			if(teacherPersonalInfo.getSSN()!=null)
				jsonOutput.put("AppSSN", teacherPersonalInfo.getSSN());
			else
				jsonOutput.put("AppSSN", "");
			//jsonOutput.put("EmployeeType", employeeType);
			//jsonOutput.put("JobTitle", jobOrder.getJobTitle());
			/*jsonOutput.put("DatePosted", Utility.getDateWithoutTime(jobOrder.getJobStartDate()));
			jsonOutput.put("DateClosed", Utility.getDateWithoutTime(jobOrder.getJobEndDate()));*/
			
			
			String req=jsonOutput.toString();
			
			System.out.println("req:: "+req);
			StringRequestEntity requestEntity = new StringRequestEntity(req,"application/json","UTF-8");

			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "application/json");
			HttpClient httpclient = new HttpClient();

			int result = httpclient.executeMethod(post);            
			System.out.println("Response status code: " + result);            
			System.out.println("Response body: ");
			System.out.println(post.getResponseBodyAsString());    
			retrunValue = post.getResponseBodyAsString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			post.releaseConnection();
		}

		return retrunValue;
	}
	public static String authDistrict()
	{
		String retrunValue = "";
		String strURL="http://demo.teachermatch.org/service/json/authenticateOrganization.do?authKey=RlpSN0FLTVoyMDEzMDYxMzA3MDMyNg==&orgType=D";
		
		GetMethod get = new GetMethod(strURL); 
		try {
			
			get.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
			HttpClient httpclient = new HttpClient();

			int result = httpclient.executeMethod(get);            
			System.out.println("Response status code: " + result);            
			//System.out.println("Response body: ");
			System.out.println(get.getResponseBodyAsString());    
			//retrunValue = post.getResponseBodyAsString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			get.releaseConnection();
		}

		return retrunValue;
	}
	public static String addEmployee()
	{
		String retrunValue = "";
		//String strURL="http://localhost:8080/teachermatch/service/json/addHiredCandidate.do";
		//String strURL="https://patsuat.scsd.us/wcfservices/addhiredcandidate.svc";
		String strURL="http://demo.teachermatch.org/service/json/addEmployees.do";
		strURL="http://localhost:8080/teachermatch/services/addEmployee";
		PostMethod post = new PostMethod(strURL);
		try {
			
			String req="";
			JSONObject jsonOutput = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONObject json = new JSONObject();
			json.put("firstName","Will");
			json.put("middleName","D");
			json.put("lastName","Smith");
			json.put("employeeCode","98798");
			json.put("address1","56, WALL STREET");
			json.put("city","KEY WEST");
			json.put("state","FL");
			json.put("zipCode","330403304");
			json.put("birthDate","1986-12-19");
			json.put("phone","3057975115");
			json.put("mobile","3052931234");
			json.put("staffType","N");
			
			jsonArray.add(json);
			
			jsonOutput.put("Employee", jsonArray);
			
			req = jsonOutput.toString();
			System.out.println("req::: "+req);
			StringRequestEntity requestEntity = new StringRequestEntity(req,"application/json","UTF-8");

			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "application/json;");
			HttpClient httpclient = new HttpClient();

			int result = httpclient.executeMethod(post);            
			System.out.println("Response status code: " + result);   
			BufferedReader br = new BufferedReader(new InputStreamReader(post.getResponseBodyAsStream()));
	        String readLine;
	        while(((readLine = br.readLine()) != null)) {
	          System.err.println(readLine);
	        }
			//System.out.println("Response body: ");
			//System.out.println(post.getResponseBodyAsString());    
			//retrunValue = post.getResponseBodyAsString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			post.releaseConnection();
		}

		return retrunValue;
	}
	
	public static String updateEmployee()
	{
		String retrunValue = "";
		//String strURL="http://localhost:8080/teachermatch/service/json/addHiredCandidate.do";
		//String strURL="https://patsuat.scsd.us/wcfservices/addhiredcandidate.svc";
		String strURL="http://demo.teachermatch.org/service/json/updateEmployees.do";
		
		PostMethod post = new PostMethod(strURL);
		try {
			
			String req="";
			JSONObject jsonOutput = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONObject json = new JSONObject();
			json.put("firstName","Will");
			json.put("middleName","D");
			json.put("lastName","Smith");
			json.put("employeeCode","98798");
			json.put("address1","56, WALL STREET");
			json.put("city","KEY WEST");
			json.put("state","FL");
			json.put("zipCode","330403304");
			json.put("birthDate","1986-12-19");
			json.put("phone","3057975115");
			json.put("mobile","3052931234");
			json.put("staffType","N");
			
			jsonArray.add(json);
			
			jsonOutput.put("Employee", jsonArray);
			
			req = jsonOutput.toString();
			System.out.println("req::: "+req);
			
			StringRequestEntity requestEntity = new StringRequestEntity(req,"application/json","UTF-8");

			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
			HttpClient httpclient = new HttpClient();

			int result = httpclient.executeMethod(post);            
			System.out.println("Response status code: " + result);            
			//System.out.println("Response body: ");
			System.out.println(post.getResponseBodyAsString());    
			//retrunValue = post.getResponseBodyAsString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			post.releaseConnection();
		}

		return retrunValue;
	}
	
	public static String updatePatsPosting(JobOrder jobOrder,String locationCode,String positionNumber,String postingNo,String expectedHires)
	{
		String retrunValue = "";
		String strURL="https://patsuat.scsd.us/wcfservices/451API.svc/UpdatePatsPosting";
		System.out.println("https://patsuat.scsd.us/wcfservices/451API.svc/UpdatePatsPosting");
		PostMethod post = new PostMethod(strURL);
		try {
			
			JSONObject jsonOutput = new JSONObject();
			jsonOutput.put("UserName", Utility.encodeInBase64("451API"));
			jsonOutput.put("Password", Utility.encodeInBase64("Gv@TYbqwt$A"));
			System.out.println("======================== Gv@TYbqwt$A");
			positionNumber = positionNumber==null?"":positionNumber;
			postingNo = postingNo==null?"":postingNo;
			
			jsonOutput.put("PositionNumber", positionNumber);
			jsonOutput.put("PostingNo", postingNo);
			//jsonOutput.put("PositionNumber", "021121003791");
			//jsonOutput.put("LocationCode", "0141");
			locationCode = locationCode==null?"":locationCode;
			jsonOutput.put("LocationCode", locationCode);
			String jobType = jobOrder.getJobType()==null?"":jobOrder.getJobType();
			jsonOutput.put("EmploymentType", jobType);
			jsonOutput.put("FTE", ""); // always send blank
			if(jobOrder.getApiJobId()!=null)
				jsonOutput.put("JobCode", jobOrder.getApiJobId());
			else
				jsonOutput.put("JobCode", "");
			
			jsonOutput.put("JobTitle", jobOrder.getJobTitle());
			
			if(expectedHires!=null)
				jsonOutput.put("OfferExtendCount", expectedHires);// no of hiring count
			else
				jsonOutput.put("OfferExtendCount", "");
			jsonOutput.put("PayType", ""); // always send blank
			
			String req=jsonOutput.toString();
			
			System.out.println("req:****************: "+req);
			StringRequestEntity requestEntity = new StringRequestEntity(req,"application/json","UTF-8");

			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "application/json");
			HttpClient httpclient = new HttpClient();

			int result = httpclient.executeMethod(post);            
			System.out.println("Response status code: " + result);            
			System.out.println("Response body: ");
			System.out.println(post.getResponseBodyAsString());    
			retrunValue = post.getResponseBodyAsString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			post.releaseConnection();
		}

		return retrunValue;
	}
	
	public static String sendComposteScoreJSON(TeacherNormScore teacherNormScore,String callBackURL)
	{
		String retrunValue = "";
		//String strURL="https://teachermatch.tedk12staging.com/Hire/HttpHandler/TeacherMatchSetEpiStatus.ashx";
		//String strURL="https://midwayisd.tedk12.com/hire/HttpHandler/TeacherMatchSetEpiStatus.ashx";
		String strURL = callBackURL;
		strURL += ("?candidateEmail=" + UtilityAPI.encodeInBase64(teacherNormScore.getTeacherDetail().getEmailAddress()));
		//strURL="http://localhost:8080/teachermatch/service/json/sendComposteScoreJSON.do";
		
		System.out.println("strURL: "+strURL);
		PostMethod post = new PostMethod(strURL);
		try {
			
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			
			String req="";
			
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("candidateEmail",teacherNormScore.getTeacherDetail().getEmailAddress());
			jsonResponse.put("epiStatus","Complete");
			jsonResponse.put("compositeScore", teacherNormScore.getTeacherNormScore());
			jsonResponse.put("colorCode", teacherNormScore.getDecileColor());
			jsonResponse.put("expirationDate", UtilityAPI.getDateTime(eDate));
			jsonResponse.put("timestamp", UtilityAPI.getCurrentTimeStamp());

			req = jsonResponse.toString();
			System.out.println("req::: "+req);
			StringRequestEntity requestEntity = new StringRequestEntity(req,"application/json","UTF-8");
			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "application/json;");
			HttpClient httpclient = new HttpClient();
			int result = httpclient.executeMethod(post);            
			System.out.println("Response status code: " + result);   
			BufferedReader br = new BufferedReader(new InputStreamReader(post.getResponseBodyAsStream()));
	        String readLine;
	        while(((readLine = br.readLine()) != null)) {
	          System.err.println(readLine);
	        }
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			post.releaseConnection();
		}

		return retrunValue;
	}
	
	
	public static String sendJobDetailJSON(JobOrder jobOrder,UserMaster userMaster)
	{
		System.out.println(" <<<<<<<<<<<<<<<<<<< sendJobDetailJSON >>>>>>>>>>>>>>>>> ");
		
		String retrunValue = "";
		String strURL = "http://192.168.0.16:8080/teachermatch/services/callback/sendjobdetailsto";
		PostMethod post = new PostMethod(strURL);
		
		try{
			String req="";
			
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("authKey",jobOrder.getDistrictMaster().getAuthKey());
			jsonResponse.put("jobTitle",jobOrder.getJobTitle());
			jsonResponse.put("workYear", "");
			jsonResponse.put("jobDescription", jobOrder.getJobDescription());
			jsonResponse.put("salaryInfo", jobOrder.getSalaryRange()!=null?jobOrder.getSalaryRange():"");
			jsonResponse.put("dateAvailable", UtilityAPI.getCurrentTimeStamp());
			jsonResponse.put("dateClosing", Utility.getUSformatDateTime(jobOrder.getJobEndDate()));
			jsonResponse.put("contactPerson", userMaster!=null?userMaster.getEmailAddress():"");
			jsonResponse.put("datePosted", Utility.getUSformatDateTime(jobOrder.getJobStartDate()));
			jsonResponse.put("jobCategory", jobOrder.getJobCategoryMaster()!=null?jobOrder.getJobCategoryMaster().getJobCategoryName():"");
			
			req = jsonResponse.toString();
			
			StringRequestEntity requestEntity = new StringRequestEntity(req,"application/json","UTF-8");
			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "application/json;");
			HttpClient httpclient = new HttpClient();
			int result = httpclient.executeMethod(post);            
			
			System.out.println("Response status code: " + result);   
			BufferedReader br = new BufferedReader(new InputStreamReader(post.getResponseBodyAsStream()));
	        String readLine;
	        while(((readLine = br.readLine()) != null)) {
	          System.err.println(readLine);
	        }
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return retrunValue;
	}
	
	
	
	public String sendComposteScoreJSON(TeacherDetail teacherDetail)
	{
		System.out.println(">>>>sendComposteScoreJSON<<<>>>");
		JSONObject jsonResponse = new JSONObject();		
		try {	
			
			List<TeacherNormScore> list=teacherNormScoreDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherDetail));
			sendComposteScoreJSON(list.get(0),null);
			
		}catch (Exception e){
			jsonResponse = UtilityAPI.writeJSONErrors(10017, "Server Error.", e);
			e.printStackTrace();
		}		
		return null;	
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param teacherAssessmentStatus
	 * @param teacherAssessmentStatusDAO
	 * @param assessmentDomainScoreDAO
	 * @param assessmentCompetencyScoreDAO
	 * @return {@link String}
	 */
	/*public static String postIPIJSON(String baseURL, TeacherAssessmentStatus teacherAssessmentStatus,TeacherAssessmentStatusDAO teacherAssessmentStatusDAO,AssessmentDomainScoreDAO assessmentDomainScoreDAO,AssessmentCompetencyScoreDAO assessmentCompetencyScoreDAO)
	{
		String strURL1 = "http://45.79.10.85:80/service/publishInventoryScores";
		String strURL2 = "http://45.56.64.203:80/service/publishInventoryScores";
		String retrunValue = "";
		
		try {
			retrunValue = postIPIJSON(baseURL, teacherAssessmentStatus, teacherAssessmentStatusDAO, assessmentDomainScoreDAO, assessmentCompetencyScoreDAO, strURL1);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			if(!retrunValue.equalsIgnoreCase("Success."))
				postIPIJSON(baseURL, teacherAssessmentStatus, teacherAssessmentStatusDAO, assessmentDomainScoreDAO, assessmentCompetencyScoreDAO, strURL2);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}*/
	
	@SuppressWarnings("unchecked")
	public static String postIPIJSON(String ipiReferralURL, String baseURL, TeacherAssessmentStatus teacherAssessmentStatus,TeacherAssessmentStatusDAO teacherAssessmentStatusDAO,AssessmentDomainScoreDAO assessmentDomainScoreDAO,AssessmentCompetencyScoreDAO assessmentCompetencyScoreDAO)
	{
		System.out.println(":::::::::::::::::::::: TMCommonUtil : postIPIJSON ::::::::::::::::::::::");
		String retrunValue = "";
		//String strURL = "";
		//strURL = "http://45.56.64.203:80/service/publishInventoryScores";
		//strURL = "http://45.79.10.85:80/service/publishInventoryScores";
		//strURL="http://localhost:8080/teachermatch/service/json/sendIPIJSON.do";
		System.out.println("ipiReferralURL : "+ipiReferralURL);
		/*if(ipiReferralURL!="" && ipiReferralURL!=null){
			String[] referralURLArr = ipiReferralURL.split("/");
			strURL = referralURLArr[0]+"//"+referralURLArr[2]+"/service/publishInventoryScores";
		}
		System.out.println("strURL: "+strURL);*/
		System.out.println("teacherAssessmentStatus.getTeacherAssessmentdetail() : "+teacherAssessmentStatus.getTeacherAssessmentdetail());
		PostMethod post = new PostMethod(ipiReferralURL);
		try{
			String req = "";
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("candidateEmail",Utility.encodeInBase64(teacherAssessmentStatus.getTeacherDetail().getEmailAddress()));
			jsonResponse.put("grpName", Utility.encodeInBase64(teacherAssessmentStatus.getAssessmentDetail().getAssessmentGroupDetails().getAssessmentGroupName()));
			
			int assessmentAttemptNo = teacherAssessmentStatusDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherAssessmentStatus.getTeacherDetail()),Restrictions.eq("assessmentType", teacherAssessmentStatus.getAssessmentType())).size();
			
			jsonResponse.put("assessmentAttemptNo",assessmentAttemptNo);
			jsonResponse.put("assessmentStatus",teacherAssessmentStatus.getStatusMaster().getStatus());
			List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
			teacherDetailList.add(teacherAssessmentStatus.getTeacherDetail());
			List assessmentDomainData = new ArrayList();
			
			assessmentDomainData = assessmentDomainScoreDAO.calculateCandidatesNormScore(teacherDetailList, teacherAssessmentStatus.getTeacherAssessmentdetail());
			Integer normscore = 0;
			for(Object oo: assessmentDomainData){
				Object obj[] = (Object[])oo;
				System.out.println("obj[0]:: "+obj[0]);
				Double normscoreD = (Double)obj[0];
				normscore = Integer.valueOf((int) Math.round(normscoreD));
			}
			
			jsonResponse.put("totalRawScore",normscore);
			
			JSONArray jsonArrayDomain = new JSONArray();
			List<AssessmentDomainScore> assessmentDomainScoreList = assessmentDomainScoreDAO.getAssessmentDomainScore(teacherDetailList,teacherAssessmentStatus.getAssessmentDetail(),teacherAssessmentStatus.getAssessmentType(),teacherAssessmentStatus.getAssessmentTakenCount());
			for(AssessmentDomainScore assessmentDomainScore : assessmentDomainScoreList){
				JSONObject jsonObjectDomain = new JSONObject();
				jsonObjectDomain.put("domainId", assessmentDomainScore.getDomainMaster().getDomainId());
				jsonObjectDomain.put("domainName", assessmentDomainScore.getDomainMaster().getDomainName());
				jsonObjectDomain.put("score", assessmentDomainScore.getNormscore());
				jsonArrayDomain.add(jsonObjectDomain);
			}
			jsonResponse.put("domainScores", jsonArrayDomain);
			
			JSONArray jsonArrayCompetency = new JSONArray();
			List<AssessmentCompetencyScore> assessmentCompetencyScoreList = assessmentCompetencyScoreDAO.getAssessmentCompetencyScore(teacherDetailList,teacherAssessmentStatus.getAssessmentDetail(),teacherAssessmentStatus.getAssessmentType(),teacherAssessmentStatus.getAssessmentTakenCount());
			for(AssessmentCompetencyScore assessmentCompetencyScore : assessmentCompetencyScoreList){
				JSONObject jsonObjectCompetency = new JSONObject();
				jsonObjectCompetency.put("competencyId", assessmentCompetencyScore.getCompetencyMaster().getCompetencyId());
				jsonObjectCompetency.put("competencyName", assessmentCompetencyScore.getCompetencyMaster().getCompetencyName());
				jsonObjectCompetency.put("score", assessmentCompetencyScore.getNormscore());
				jsonObjectCompetency.put("rank", assessmentCompetencyScore.getCompetencyMaster().getRank());
				jsonArrayCompetency.add(jsonObjectCompetency);
			}
			jsonResponse.put("competencyScores", jsonArrayCompetency);
			
			String pdURL = null;
			JSONObject jsonObject1 = new JSONObject();
   			jsonObject1.put("authKey", "NkRUT1hFTzMyMDEzMTEyNjA2MjAwMg==");
   			jsonObject1.put("orgType", "D");
   			jsonObject1.put("assessmentType", teacherAssessmentStatus.getAssessmentType());
   			jsonObject1.put("assessmentAttemptNo",assessmentAttemptNo);
   			jsonObject1.put("grpName", Utility.encodeInBase64(teacherAssessmentStatus.getAssessmentDetail().getAssessmentGroupDetails().getAssessmentGroupName()));
   			jsonObject1.put("candidateEmail", Utility.encodeInBase64(teacherAssessmentStatus.getTeacherDetail().getEmailAddress()));
   			String encryptText = AESEncryption.encrypt(jsonObject1.toString());
			if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equals("comp")){
				pdURL = baseURL+"service/pdf/inventoryPDP.do?id="+encryptText;
				pdURL = pdURL.trim().replace("\n", "");
				pdURL = pdURL.trim().replace("\r", "");
			}
			jsonResponse.put("pdURL", pdURL);
			
			req = jsonResponse.toString();
			System.out.println("req::: "+req);
			String encryptedText = AESEncryption.encrypt(req);
			//System.out.println("encryptText == "+encryptedText);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("encryptedText", encryptedText);
			System.out.println("jsonObject.toString() :: "+jsonObject.toString());
			StringRequestEntity requestEntity = new StringRequestEntity(jsonObject.toString(),"application/json","UTF-8");
			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "application/json;");
			
			HttpClient httpclient = new HttpClient();
			int result = httpclient.executeMethod(post);
			System.out.println("Response status code: " + result);
			BufferedReader br = new BufferedReader(new InputStreamReader(post.getResponseBodyAsStream()));
	        String readLine;
	        while(((readLine = br.readLine()) != null)) {
	          System.err.println(readLine);
	          retrunValue = readLine;
	        }
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			post.releaseConnection();
		}

		return retrunValue;
	}
	
	public static void main(String[] args) {
		//updateSDSCHiredCandidate(null,null,null);
		//authDistrict();
		System.out.println("=====================");
		//test();
		//addEmployee();
		//updatePatsPosting(null);
		
		try {
			//testPostToHireVue();
			String[] tokens = loginToHireVue(null);
			//getPositions(tokens[0],tokens[1]);
			//addACandidateToAPosition(tokens[0],tokens[1]);
			//getPostionss(testPostToHireVue());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/*public static String checkForTalentEmailAddress(JobForTeacher jft,JobForTeacherDAO jobForTeacherDAO , String tmTransactionId , Integer mqEventHistoryId , MQEventHistoryDAO mqEventHistoryDAO)
	{
			System.out.println(">>>>>>>>>>>>>>checkForTalentEmailAddress.. jftId :: "+jft.getJobForTeacherId());
			String retrunValue = "";
			
			ApplitrackDistricts applitrackDistricts = WorkThreadServlet.applitrackDisMap.get("CheckForTalentEmail");
			ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
			String strURL = null;
			if(applitrackDistricts != null){
			     strURL = applitrackDistricts.getApiURL();
			}else{
				System.out.println("URL Not Found");
				return null;
			}
			System.out.println("Post URL Of Kelly For Check Email "+ strURL);
			PostMethod post = new PostMethod(strURL);
			try {
				
		        Document doc = null;
				Element root = null;
				DocumentBuilder docBuilder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
				doc = docBuilder.newDocument();
				root = doc.createElement("TeacherMatch");
				doc.appendChild(root);
				root.setAttribute("method", "CheckForTalentEmailAddress");
				
				
				Element eleTMTransaction = doc.createElement("TMTransactionID");
				Text tmTransaction = doc.createTextNode(tmTransactionId);
				eleTMTransaction.appendChild(tmTransaction);
				
				Element eleKellyTransaction = doc.createElement("KellyTransactionID");
				Text textKellyTransaction = doc.createTextNode("");
				eleKellyTransaction.appendChild(textKellyTransaction);
			
				
				Element eleEmailID = doc.createElement("EmailID");
				Text textEmailID = doc.createTextNode(jft.getTeacherId().getEmailAddress());
				eleEmailID.appendChild(textEmailID);
				
				
				Element eleWebResponse = doc.createElement("Request");
				Element eleWebAuthentication = doc.createElement("Authorization");
				
				Element eleWebPassword = doc.createElement("Password");
				Text password = doc.createTextNode(applitrackDistricts.getPassword());
				eleWebPassword.appendChild(password);
				
				Element eleWebUserName = doc.createElement("UserName");
				Text userName = doc.createTextNode(applitrackDistricts.getUserName());
				eleWebUserName.appendChild(userName);
				eleWebAuthentication.appendChild(eleWebUserName);
				eleWebAuthentication.appendChild(eleWebPassword);
				
				eleWebResponse.appendChild(eleWebAuthentication);
				eleWebResponse.appendChild(eleTMTransaction);
				eleWebResponse.appendChild(eleEmailID);
				
				root.appendChild(eleWebResponse);
				TransformerFactory factory = TransformerFactory.newInstance();
				Transformer transformer = factory.newTransformer();
				
				StringWriter sw = new StringWriter();
				StreamResult streamResult = new StreamResult(sw);
				DOMSource source = new DOMSource(doc);
				transformer.transform(source, streamResult);
				String req = sw.toString();
				System.out.println("##################### req "+ req);
				System.out.println("##################### mqEventHistoryDAO "+ mqEventHistoryDAO);
				
				if(mqEventHistoryDAO != null){
					MQEventHistory mqEventHistory = mqEventHistoryDAO.findById(mqEventHistoryId, false, false);
					mqEventHistory.setXmlObject(req);
					mqEventHistoryDAO.makePersistent(mqEventHistory);
				}
				
				
				StringRequestEntity requestEntity = new StringRequestEntity(req,"application/xml","UTF-8");
				
				post.setRequestEntity(requestEntity);
				post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
				System.out.println("--------Content-type---------text/xml");
				HttpClient httpclient = new HttpClient();
				System.out.println("#################### post "+post);
				int result = httpclient.executeMethod(post);  
				
				System.out.println("Response status code for Check Email: " + result);            
				retrunValue = post.getResponseBodyAsString();
				System.out.println("1111111111111111111111111  "+retrunValue);
				
				
				InputSource inputSource = new InputSource();
				inputSource.setCharacterStream(new StringReader(retrunValue));
				doc = docBuilder.parse(inputSource);
			
			
				NodeList CandidateDetailsTM = doc.getElementsByTagName("TeacherMatch");
				
				String messageResponse="";
				if (CandidateDetailsTM.getLength() > 0) {
			        Element element = (Element) CandidateDetailsTM.item(0);
			        String status = element.getElementsByTagName("ResponseStatus").item(0).getTextContent();
		            messageResponse = element.getElementsByTagName("Message").item(0).getTextContent();
		            
		            System.out.println("Status: "+status);
		            System.out.println("messageResponse: "+messageResponse);
			        String ksnTalentID = null;
			        String firstName = null;
			        String lastName = null;
			        String lastFourSSN= null;
			        String tmTalentID = null;
			        String messageDescription = null;
			        
			        NodeList talentList = doc.getElementsByTagName("TalentList");
			        NodeList messageList = doc.getElementsByTagName("Message");
			        talentKSNDetailDAO = (TalentKSNDetailDAO)context0.getBean("talentKSNDetailDAO");
			        userMasterDAO = (UserMasterDAO)context0.getBean("userMasterDAO");
					mqEventDAO = (MQEventDAO)context0.getBean("mqEventDAO"); 
					mqEventHistoryDAO = (MQEventHistoryDAO)context0.getBean("mqEventHistoryDAO"); 
		        	Criterion criteria	 =	Restrictions.eq("teacherdetail",jft.getTeacherId()); 
			        List<MQEvent> mqEventList = mqEventDAO.findByCriteria(criteria);
			        UserMaster userMaster=userMasterDAO.findById(mqEventList.get(0).getCreatedBy(),false,false);
			        MQEvent mqEvent = null;
			        if(status.equalsIgnoreCase("Success")){
			        	
			        	   if(mqEventList != null && mqEventList.size()>0){
					        	mqEvent =	mqEventList.get(0);
						        mqEvent.setAckStatus(status);
						        mqEvent.setMsgStatus(messageResponse);
						        mqEventDAO.makePersistent(mqEvent);
						        
			        	   }
			        	   for(int i=0;i<talentList.getLength();i++){
			        		        Element elementTalent = (Element) talentList.item(i);
			        		   
			        		        if(elementTalent.getElementsByTagName("KSNTalentID").item(0) != null)
			        		        ksnTalentID = elementTalent.getElementsByTagName("KSNTalentID").item(0).getTextContent();
								    System.out.println("KSNTalentID: "+ksnTalentID);
							        
							        if(elementTalent.getElementsByTagName("FirstName").item(0) != null)
							        firstName = elementTalent.getElementsByTagName("FirstName").item(0).getTextContent();
							        System.out.println("FirstName: "+firstName);
							        
								    if(elementTalent.getElementsByTagName("LastName").item(0) != null)    
								    lastName = elementTalent.getElementsByTagName("LastName").item(0).getTextContent();
							        System.out.println("LastName: "+lastName);
				
							        if(elementTalent.getElementsByTagName("TMTalentID").item(0) != null) 
							        tmTalentID = elementTalent.getElementsByTagName("TMTalentID").item(0).getTextContent();
							        System.out.println("TMTalentID: "+tmTalentID);
							        
							        if(elementTalent.getElementsByTagName("LastFourSSN").item(0) != null) 
							        lastFourSSN = elementTalent.getElementsByTagName("LastFourSSN").item(0).getTextContent();
							        System.out.println("LastFourSSN: "+lastFourSSN);
							        
					           if(mqEventList != null && mqEventList.size() >0){
					        	   TalentKSNDetail talentKSNDetail = new TalentKSNDetail();
					        	   talentKSNDetail.setFirstName(firstName);
					        	   talentKSNDetail.setTeacherId(mqEventList.get(0).getTeacherdetail());
					        	   talentKSNDetail.setLastName(lastName);
					        	   talentKSNDetail.setKSNID(Integer.parseInt(ksnTalentID));
					        	   talentKSNDetail.setEventId(mqEventList.get(0));
					        	   talentKSNDetail.setTmTalentId(Utility.getIntValue(tmTalentID));
					        	   talentKSNDetail.setCreatedDateTime(new Date());
					        	   talentKSNDetail.setStatus("A");
					        	   talentKSNDetailDAO.makePersistent(talentKSNDetail);
					           } 	   
			        	}    
			        }else{
			        	
			        	 if(mqEventList != null && mqEventList.size()>0){
					        	mqEvent =	mqEventList.get(0);
						        mqEvent.setAckStatus(status);
						        mqEvent.setStatus("R");
						        mqEvent.setMsgStatus(messageResponse);
						        mqEventDAO.makePersistent(mqEvent);
						        
			        	   }
			        	
			        	
			        	for(int i=0;i<messageList.getLength();i++){
			        		
			        		Element elementTalent = (Element) messageList.item(i);
					        if(elementTalent.getElementsByTagName("MessageDescription").item(i) != null)
	        		        messageDescription = elementTalent.getElementsByTagName("MessageDescription").item(i).getTextContent();
						    System.out.println("MessageDescription: "+messageDescription);	
						    //mqService.statusUpdate(jft,null,mqEventList.get(0).getSecondaryStatus(),userMaster,context0,messageDescription,true);
			        	}
			        }
				}  
			    System.out.println("checkForTalentEmailAddress response:: "+retrunValue);
				}catch(Exception e)
				{
					e.printStackTrace();
				}finally {
					post.releaseConnection();
				}
		
				return retrunValue;
		}*/
	
	
//update sandeep 12-11-15	
	
	public  String checkForTalentEmailAddress(JobForTeacher jft , String tmTransactionId , Integer mqEventHistoryId, StatusNodeColorHistory statusNodeColorHistory)
	{
			System.out.println(">>>>>>>>>>>>>>checkForTalentEmailAddress.. jftId :: "+jft.getJobForTeacherId());
			String retrunValue = null;
			 MQEvent mqEvent = new MQEvent();
			 Map<String,MQEvent> mapNodeStatus= new HashMap<String, MQEvent>();
			try {
			ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
			
			JobOrder jobOrder=null;
			if(jft!=null && jft.getJobId()!=null)
			{
				jobOrder=jft.getJobId();
			}
			
        	//Criterion criteria	 =	Restrictions.eq("teacherdetail",jft.getTeacherId());
        	//Criterion criteria2	 =	Restrictions.eq("eventType",Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)); 
	        List<MQEvent> mqEventList = mqEventDAO.findEventByEventType(jft.getTeacherId(),Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)); //findByCriteria(criteria,criteria2);
	        UserMaster userMaster = null;
	        Integer userId = 0;
	        if(mqEventList!=null && mqEventList.size()>0 && mqEventList.get(0).getCreatedBy()!=null)
	        	userId = mqEventList.get(0).getCreatedBy();
	        else
	        	userId = jft.getJobId().getCreatedBy();
	        
	        userMaster= userMasterDAO.findById(userId,false,false);
	        
			ApplitrackDistricts applitrackDistricts = WorkThreadServlet.applitrackDisMap.get("CheckForTalentEmail");
			if(applitrackDistricts == null){
			    System.out.println("applitrackDistricts Object Can Not Be Null");
				return null;
			}
			
			if(mqEventList != null && mqEventList.size()>0){
	        	mqEvent =	mqEventList.get(0);
			}
			Long ksnTalentID = null;
	        String firstName = null;
	        String lastName = null;
	        String lastFourSSN= null;
	        Long tmTalentID = null;
	        String status = null;
			
			IKSNDataServiceProxy proxy = new IKSNDataServiceProxy();

			CheckForTalentEmailAddressRequest request = new CheckForTalentEmailAddressRequest();

			ExternalRequestBase authorization = new ExternalRequestBase();
			authorization.setPassword(applitrackDistricts.getPassword());
			authorization.setUserName(applitrackDistricts.getUserName());

			request.setAuthorization(authorization);
			request.setTMTransactionID(tmTransactionId);
			request.setEmailID(jft.getTeacherId().getEmailAddress());
			//request.setEmailID("");
			
			   
				CheckForTalentEmailAddressResponse response = proxy.checkForTalentEmailAddress(request);
			   
				status = response.getResponseBase().getResponseStatus().toString();
				System.out.println("ResponseStatus :    "+response.getResponseBase().getResponseStatus());
				String subject="";
				MQService mqServiceObj=new MQService();
				
				if(status.equalsIgnoreCase("Successful")){
				
				System.out.println("TalentList Length    "+response.getTalentList().length);
				
				TalentDetail[] talentList = response.getTalentList();
				System.out.println("talentList       "+talentList);

				if(mqEventList != null && mqEventList.size()>0){
		        	mqEvent =	mqEventList.get(0);
			        mqEvent.setAckStatus("Received");
			        mqEvent.setMsgStatus(null);
			        mqEventDAO.makePersistent(mqEvent);
		        }
		        
				if(mqEvent.getSecondaryStatus()!=null){
					subject=mqEvent.getSecondaryStatus().getSecondaryStatusName();
				}else{
					subject=mqEvent.getEventType();
				}
				
				List<TalentKSNDetail> talentKSNDetailList = null;
				// inactive previous records
					Integer selectedKSNID = 0;
		    	 List<TalentKSNDetail> talentksndList = talentKSNDetailDAO.getListTalentKSNDetail(jft.getTeacherId(), "A");
		    	 if(talentksndList!=null && talentksndList.size()>0){
		    		 for(TalentKSNDetail tsn : talentksndList){
		    			 if(tsn.getIsSelected().equalsIgnoreCase("n")){
		    				 tsn.setStatus("I");
		    				 talentKSNDetailDAO.updatePersistent(tsn);
		    			 }
		    			 else{
		    				 selectedKSNID = tsn.getKSNID();
		    			 }
		    		 }
		    	 }	
				
				SessionFactory sessionFactory=talentKSNDetailDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				Transaction txOpen =statelesSsession.beginTransaction();
					for(TalentDetail talentDetail : talentList){  
	    		   
	    		        
	    		        ksnTalentID = talentDetail.getKSNTalentID();
					    System.out.println("KSNTalentID: "+ksnTalentID);
				        
				        firstName = talentDetail.getFirstName();
				        System.out.println("FirstName: "+firstName);
				        
					    lastName = talentDetail.getLastName();
				        System.out.println("LastName: "+lastName);
				         
				        tmTalentID = talentDetail.getTeacherMatchID();
				        System.out.println("TMTalentID: "+tmTalentID);
				        
				        lastFourSSN = talentDetail.getLastFourSSN();
				        System.out.println("LastFourSSN: "+lastFourSSN);
				        
				     if(mqEventList != null && mqEventList.size() >0){
		        	   TalentKSNDetail talentKSNDetail = new TalentKSNDetail();
		        	   talentKSNDetail.setFirstName(firstName);
		        	   talentKSNDetail.setEmailAddress(jft.getTeacherId().getEmailAddress());
		        	   talentKSNDetail.setTeacherId(mqEventList.get(0).getTeacherdetail());
		        	   talentKSNDetail.setLastName(lastName);
		        	   talentKSNDetail.setLastFourSSN(lastFourSSN);
		        	   talentKSNDetail.setKSNID((int) (long)ksnTalentID);
		        	   talentKSNDetail.setEventId(mqEventList.get(0));
		        	   if(tmTalentID!=null)
		        		   talentKSNDetail.setTmTalentId((int) (long)tmTalentID);
		        	   else
		        		   talentKSNDetail.setTmTalentId(0);
		        	   talentKSNDetail.setCreatedDateTime(new Date());
		        	   talentKSNDetail.setStatus("A");
		        	   talentKSNDetail.setIsSelected("N");
		        	   if(selectedKSNID==0 || (selectedKSNID!=0 && !(selectedKSNID.intValue()==(ksnTalentID.intValue()))))
		        		   statelesSsession.insert(talentKSNDetail);
		           } 	   
			    } 
					txOpen.commit();
					statelesSsession.close();
			   }else{
				   if(mqEventList != null && mqEventList.size()>0){
			        	mqEvent =	mqEventList.get(0);
				        mqEvent.setAckStatus(status);
				       // mqEvent.setMsgStatus(messageResponse);
				        mqEventDAO.makePersistent(mqEvent);
				        
			        }
				   Message[] responseMessage = response.getResponseBase().getResponseMessages();
				   for(Message message : responseMessage){
					   try {
						   mqServiceObj.saveTeacherStatusNotes(mqEvent, message.getMessageDescription(), userMaster, context0, subject, jobOrder);
						} catch (Exception e) {
							// TODO: handle exception
						}
				   }
			   }
				
				/**
			        * add to track node color history
			        */
				try {
					if(statusNodeColorHistory!=null){
						List<MQEvent> mqEventsList=new ArrayList<MQEvent>();
						 mqEventsList=mqEventDAO.findMQEventByTeacher(mqEvent.getTeacherdetail());
						 if(mqEventsList.size()>0){
							 for (MQEvent mqEv : mqEventsList) {
								 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
							 }
						 }
					   String newColor = candidateGridService.getStatusNodeColor(mqEvent.getEventType(),mqEvent.getTeacherdetail(),mapNodeStatus);
					   System.out.println(" newColor :: "+newColor);
					   statusNodeColorHistory.setCurrentColor(newColor);
					   statusNodeColorHistory.setUpdateDateTime(new Date());
					   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
				    }
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				/**
			      * end
			      */
	
			} catch (RemoteException e) {
				e.printStackTrace();
				
			/**
		        * add to track node color history
		        */
			try {
				if(statusNodeColorHistory!=null){
					   //CandidateGridService candidateGridService = new CandidateGridService();
					List<MQEvent> mqEventsList=new ArrayList<MQEvent>();
					 mqEventsList=mqEventDAO.findMQEventByTeacher(mqEvent.getTeacherdetail());
					 if(mqEventsList.size()>0){
						 for (MQEvent mqEv : mqEventsList) {
							 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
						 }
					 }
					   String newColor = candidateGridService.getStatusNodeColor(mqEvent.getEventType(),mqEvent.getTeacherdetail(),mapNodeStatus);
					   System.out.println(" newColor :: "+newColor);
					   statusNodeColorHistory.setCurrentColor(newColor);
					   statusNodeColorHistory.setUpdateDateTime(new Date());
					   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
				   }
			} catch (Exception exx) {
				exx.printStackTrace();
			}
	
			}
			
			return retrunValue;
		}
	
	
	
	
	public static String updateKESCandidate(JobForTeacher jft,JobForTeacherDAO jobForTeacherDAO , String tmTransactionId , Integer mqEventHistoryId , MQEventHistoryDAO mqEventHistoryDAO,String ksnId)
	{
			System.out.println(">>>>>>>>>>>>>>updateKESCandidate.. jftId :: "+jft.getJobForTeacherId());
			String retrunValue = "";
			
			//String strURL="https://TeacherMatchdev.kellyservices.com/TeacherMatch/WebPost.aspx";
			
			ApplitrackDistricts applitrackDistricts = WorkThreadServlet.applitrackDisMap.get("UpdateKESCandidate");
			String strURL = null;
			String branchCode = "";
			if(applitrackDistricts != null){
			     strURL = applitrackDistricts.getApiURL();
			}else{
				System.out.println("URL Not Found");
				return null;
			}
			
			System.out.println("Post URL Of Kelly "+ strURL);
			PostMethod post = new PostMethod(strURL);
			try {
				
		        Document doc = null;
				Element root = null;
				DocumentBuilder docBuilder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
				doc = docBuilder.newDocument();
				root = doc.createElement("TeacherMatch");
				doc.appendChild(root);
				root.setAttribute("method", "AddUpdate");
				
				tmTransactionId = "TM-"+(Utility.randomString(8)+Utility.getDateTime());
				Element eleTMTransaction = doc.createElement("TMTransactionID");
				Text tmTransaction = doc.createTextNode(tmTransactionId);
				eleTMTransaction.appendChild(tmTransaction);
			
				Element eleFirstName = doc.createElement("FirstName");
				Text textFirstName = doc.createTextNode(jft.getTeacherId().getFirstName());
				eleFirstName.appendChild(textFirstName);
				
				Element eleLastName = doc.createElement("LastName");
				Text textLastName = doc.createTextNode(jft.getTeacherId().getLastName());
				eleLastName.appendChild(textLastName);
				
				Element eleEmailID = doc.createElement("EmailID");
				Text textEmailID = doc.createTextNode(jft.getTeacherId().getEmailAddress());
				eleEmailID.appendChild(textEmailID);
				
				Element eleTalentID = doc.createElement("TMTalentID");
				Text textTalentID = doc.createTextNode(jft.getTeacherId().getTeacherId()+"");
				
				eleTalentID.appendChild(textTalentID);
				
				Element eleKSNID = doc.createElement("KSNTalentID");
				Text textKSNID = null;
				if(ksnId==null || ksnId.length()==0){
					/*if(jft != null && jft.getTeacherId() != null && jft.getTeacherId().getKSNID() != null)
						textKSNID = doc.createTextNode(jft.getTeacherId().getKSNID().toString());
					else*/
						textKSNID = doc.createTextNode("");	
					eleKSNID.appendChild(textKSNID);
				}
				else if(ksnId!=null && ksnId.length()>0){
					textKSNID = doc.createTextNode(ksnId);
					eleKSNID.appendChild(textKSNID);
				}
				
				Text textManagedByBranch = null;
				Element eleManagedByBranch = doc.createElement("ManagedByBranch");
				if(jft.getJobId()!=null && jft.getJobId().getBranchMaster()!=null){
					   System.out.println(" jobForTeacher.getJobId().getBranchMaster().getBranchCode() ::::: "+jft.getJobId().getBranchMaster().getBranchCode());
					   textManagedByBranch = doc.createTextNode(jft.getJobId().getBranchMaster().getBranchCode());
				   }else{
					textManagedByBranch = doc.createTextNode("");	
					}
				eleManagedByBranch.appendChild(textManagedByBranch);
				
				Element eleJobCode = doc.createElement("JobCode");
				try{
					Text textJobCode = null;
						if(jft.getJobId() != null && jft.getJobId().getJobCategoryMaster() != null && jft.getJobId().getJobCategoryMaster().getPrimaryJobCode()!=null)
							textJobCode = doc.createTextNode(jft.getJobId().getJobCategoryMaster().getPrimaryJobCode().toString());	
						else
							textJobCode = doc.createTextNode("");
						
				System.out.println("Link To KSN JobCode" + textJobCode);
				eleJobCode.appendChild(textJobCode);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			
			Element eleWebResponse = doc.createElement("Request");
			eleWebResponse.appendChild(eleTMTransaction);
			eleWebResponse.appendChild(eleFirstName);
			eleWebResponse.appendChild(eleLastName);
			eleWebResponse.appendChild(eleEmailID);
			eleWebResponse.appendChild(eleTalentID);
			eleWebResponse.appendChild(eleKSNID);
			eleWebResponse.appendChild(eleManagedByBranch);
			eleWebResponse.appendChild(eleJobCode);
			
			root.appendChild(eleWebResponse);
			System.out.println("Link To KSN Request XML "+eleWebResponse);
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			
			StringWriter sw = new StringWriter();
			StreamResult streamResult = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, streamResult);
			String req = sw.toString();
			System.out.println("Rest Call for "+jft.getTeacherId().getEmailAddress()+" TimeStamp ::: "+Utility.getCurrentDate()+"    ################## req "+ req);
			System.out.println("##################### mqEventHistoryDAO "+ mqEventHistoryDAO);
			
			if(mqEventHistoryDAO != null){
				try {
					MQEventHistory mqEventHistory = mqEventHistoryDAO.findById(mqEventHistoryId, false, false);
					if(mqEventHistory != null){
						mqEventHistory.setXmlObject(req);
						mqEventHistoryDAO.makePersistent(mqEventHistory);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}	
			}
			
			if(jft!=null){
				System.out.println(" Send Request for Teacher <<<<<:::>>>>> "+jft.getTeacherId().getTeacherId());
			}
			
			StringRequestEntity requestEntity = new StringRequestEntity(req,"application/xml","UTF-8");
			
			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
			HttpClient httpclient = new HttpClient();
			System.out.println("#################### post "+post);
			int result = httpclient.executeMethod(post);  
			
			System.out.println("Response status code: " + result);            
			retrunValue = post.getResponseBodyAsString();
			
			//Checking for success and failure..........
			InputSource inputSource = new InputSource();
			inputSource.setCharacterStream(new StringReader(retrunValue));
			doc = docBuilder.parse(inputSource);
			
		  
		  System.out.println("Rest Response for "+jft.getTeacherId().getEmailAddress()+" TimeStamp ::: "+Utility.getCurrentDate()+"    updateKESCandidate response:: "+retrunValue);
		  if(jft!=null){
				System.out.println(" Recieve Response for Teacher <<<<<:::>>>>> "+jft.getTeacherId().getTeacherId());
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			
		}finally {
			post.releaseConnection();
		}

		return retrunValue;
	}

	
	
	/*public static String updateERegistration(JobForTeacher jft,JobForTeacherDAO jobForTeacherDAO , String tmTransactionId , Integer mqEventHistoryId , MQEventHistoryDAO mqEventHistoryDAO , String workFlowID )
	{
		System.out.println(">>>>>>>>>>>>>>updateERegistration.. jftId :: "+jft.getJobForTeacherId());
		String retrunValue = "";
		
		//String strURL="https://TeacherMatchdev.kellyservices.com/TeacherMatch/WebPost.aspx";
		
		ApplitrackDistricts applitrackDistricts = WorkThreadServlet.applitrackDisMap.get("UpdateERegistration");
		String strURL = null;
		if(applitrackDistricts != null){
		     strURL = applitrackDistricts.getApiURL();
		}else{
			System.out.println("URL Not Found");
			return null;
		}
		System.out.println("Post URL Of Kelly for E Registration"+ strURL);
		PostMethod post = new PostMethod(strURL);
		try {
			
	        Document doc = null;
			Element root = null;
			
			DocumentBuilder docBuilder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
			doc = docBuilder.newDocument();
			root = doc.createElement("TeacherMatch");
			doc.appendChild(root);
			root.setAttribute("method", "requestEReg");
			
			  
				//Dynamic Data
			   
				Element eleTMTransaction = doc.createElement("TMTransactionID");
				Text tmTransaction = doc.createTextNode(tmTransactionId);
				eleTMTransaction.appendChild(tmTransaction);
				
				Element eleKSNTransaction = doc.createElement("KSNTransactionID");
				Text KSNTransaction = doc.createTextNode("");
				eleKSNTransaction.appendChild(KSNTransaction);
				
				
				Element eleFirstName = doc.createElement("FirstName");
				Text textFirstName = doc.createTextNode(jft.getTeacherId().getFirstName());
				eleFirstName.appendChild(textFirstName);
				
				Element eleLastName = doc.createElement("LastName");
				Text textLastName = doc.createTextNode(jft.getTeacherId().getLastName());
				eleLastName.appendChild(textLastName);
				
				Element eleEmailID = doc.createElement("EmailID");
				Text textEmailID = doc.createTextNode(jft.getTeacherId().getEmailAddress());
				eleEmailID.appendChild(textEmailID);
				
				Element eleTalentID = doc.createElement("TalentID");
				Text textTalentID = doc.createTextNode(jft.getTeacherId().getTeacherId()+"");
				eleTalentID.appendChild(textTalentID);
				
				Element eleKSNID = doc.createElement("KSNID");
				Text textKSNID = null;
				if(jft != null && jft.getTeacherId() != null && jft.getTeacherId().getKSNID() != null)
				textKSNID = doc.createTextNode(jft.getTeacherId().getKSNID().toString());
				eleKSNID.appendChild(textKSNID);
				
				Element eleWorkFlow = doc.createElement("WorkFlowID");
				Text WorkFlow = doc.createTextNode(workFlowID);
				eleWorkFlow.appendChild(WorkFlow);
				
				Element eleNotes = doc.createElement("Notes");
				Text notes = doc.createTextNode("");
				eleNotes.appendChild(notes);
				
								
				
			
			Element eleWebResponse = doc.createElement("Request");
			
			Element eleWebAuthentication = doc.createElement("Authorization");
			
			Element eleWebPassword = doc.createElement("Password");
			Text password = doc.createTextNode(applitrackDistricts.getPassword());
			eleWebPassword.appendChild(password);
			
			Element eleWebUserName = doc.createElement("UserName");
			Text userName = doc.createTextNode(applitrackDistricts.getUserName());
			eleWebUserName.appendChild(userName);
			eleWebAuthentication.appendChild(eleWebUserName);
			eleWebAuthentication.appendChild(eleWebPassword);
			
			eleWebResponse.appendChild(eleWebAuthentication);
			
			eleWebResponse.appendChild(eleTMTransaction);
			eleWebResponse.appendChild(eleKSNTransaction);
			eleWebResponse.appendChild(eleTalentID);
			eleWebResponse.appendChild(eleKSNID);
			eleWebResponse.appendChild(eleFirstName);
			eleWebResponse.appendChild(eleLastName);
			eleWebResponse.appendChild(eleEmailID);
			eleWebResponse.appendChild(eleWorkFlow);
			eleWebResponse.appendChild(eleNotes);
			
			
			
			root.appendChild(eleWebResponse);
			System.out.println("###############eleWebResponse "+eleWebResponse);
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			
			StringWriter sw = new StringWriter();
			StreamResult streamResult = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, streamResult);
			String req = sw.toString();
			System.out.println("##################### req "+ req);
			System.out.println("##################### mqEventHistoryDAO "+ mqEventHistoryDAO);
			
			if(mqEventHistoryDAO != null){
				MQEventHistory mqEventHistory = mqEventHistoryDAO.findById(mqEventHistoryId, false, false);
				mqEventHistory.setXmlObject(req);
				mqEventHistoryDAO.makePersistent(mqEventHistory);
			}
			
			
			StringRequestEntity requestEntity = new StringRequestEntity(req,"application/xml","UTF-8");
			
			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
			System.out.println("--------Content-type---------text/xml");
			HttpClient httpclient = new HttpClient();
			System.out.println("#################### post "+post);
			int result = httpclient.executeMethod(post);  
			
			System.out.println("Response status code: " + result);            
			retrunValue = post.getResponseBodyAsString();
			
			//Checking for success and failure..........
			InputSource inputSource = new InputSource();
			inputSource.setCharacterStream(new StringReader(retrunValue));
			doc = docBuilder.parse(inputSource);
			
			
		  
		  System.out.println("updateERegistration response:: "+retrunValue);
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			post.releaseConnection();
		}

		return retrunValue;
	}*/

	
	
	
	
	
	
	public static InviteTalentToWorkflowResponse updateERegistration(JobForTeacher jft,JobForTeacherDAO jobForTeacherDAO , String tmTransactionId , Integer mqEventHistoryId , MQEventHistoryDAO mqEventHistoryDAO , String workFlowID )
	{
		System.out.println(">>>>>>>>>>>>>>updateERegistration.. jftId :: "+jft.getJobForTeacherId());
		ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		ApplitrackDistricts applitrackDistricts = WorkThreadServlet.applitrackDisMap.get("UpdateERegistration");
		if(applitrackDistricts == null){
		    System.out.println("applitrackDistricts Object Can Not Be Null");
			return null;
		}
		InviteTalentToWorkflowResponse response = new InviteTalentToWorkflowResponse();
		String retrunValue = null;
		String status = null;
		IKSNDataServiceProxy proxy = new IKSNDataServiceProxy();

		InviteTalentToWorkflowRequest request = new InviteTalentToWorkflowRequest();

		ExternalRequestBase authorization = new ExternalRequestBase();
		authorization.setPassword(applitrackDistricts.getPassword());
		authorization.setUserName(applitrackDistricts.getUserName());

		request.setAuthorization(authorization);
		request.setTMTransactionID(tmTransactionId);
		request.setKSNTalentID(jft.getTeacherId().getKSNID().longValue());
		request.setWorkflowID(workFlowID);
		request.setNotes("");
		
		
		
		try {
			response = proxy.inviteTalentToWorkflow(request);

			status = response.getResponseBase().getResponseStatus().toString();
			System.out.println("ResponseStatus :    "+response.getResponseBase().getResponseStatus());
			//System.out.println("TalentList Length    "+response.getTalentList().length);
			
			
			
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	
	
	
	
	
	public static Map<String,String> singleSignOn(TeacherDetail teacherDetail, MQEvent mqEvent , MQEventHistory mqEventHistory,MQEventDAO mqEventDAO,MQEventHistoryDAO mqEventHistoryDAO)
	{
		System.out.println(".... Single Sign On Call .... ");
		Map<String,String> mapStatus = new HashMap<String, String>();
		ApplitrackDistricts applitrackDistricts = WorkThreadServlet.applitrackDisMap.get("SingleSignOn");
		String strURL = null;
		//String strURL = "http://192.168.0.103:8080/teachermatch/services/callback/requestURL";
		if(applitrackDistricts != null){
		     strURL = applitrackDistricts.getApiURL();
		}else{
			System.out.println("URL Not Found");
			return null;
		}
		System.out.println("Post URL Of Kelly for E Registration ::: "+ strURL);
		PostMethod post = new PostMethod(strURL);
		String tokenId = "";
		String key = Utility.getLocaleValuePropByKey("SSOLoginKeyWord", locale);
		String msg ="";
		String status = "failed";
		String messageResponse="";
		 String url  ="";
		try {
			
			msg = key + teacherDetail.getKSNID().toString();
			System.out.println("tokenID before encryption :::::::  "+msg);
			tokenId = Utility.getTrustedTokenId(msg, key, "HmacMD5");
			System.out.println("tokenID after encryption  :::::::  "+tokenId);
			
			IKSNDataServiceProxy proxy = new IKSNDataServiceProxy();
			AuthenticateERegUserRequest request = new AuthenticateERegUserRequest();
			
			ExternalRequestBase authorization = new ExternalRequestBase();
			authorization.setPassword(applitrackDistricts.getPassword());
			authorization.setUserName(applitrackDistricts.getUserName());

			request.setAuthorization(authorization);
			request.setKsnId(teacherDetail.getKSNID().toString());
			request.setTokenId(tokenId);
			   
			AuthenticateERegUserResponse response = proxy.authenticateERegUser(request);
			   
			status = response.getResponseBase().getResponseStatus().toString();
			System.out.println("ResponseStatus :    "+response.getResponseBase().getResponseStatus());
			
			String req = "";
			
				try {
					if(mqEventHistory!=null){
						mqEventHistory.setXmlObject(req);
						mqEventHistoryDAO.makePersistent(mqEventHistory);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			
			if(status.length()>0){
		        
	            System.out.println("Status: "+status);
	            System.out.println("messageResponse: "+messageResponse);
		        if(status.contains("Success")){
		    
		        	url = response.getReturnURL();
		        	mapStatus.put("Status", "Success");
		        	mapStatus.put("Msg", messageResponse);
		        	mapStatus.put("URL", url);
		        	
		        	mqEvent.setStatus("C");
			        mqEvent.setAckStatus(status);
			        mqEvent.setMsgStatus(messageResponse);
			        mqEvent.seteRegURL(url);
			        mqEventDAO.makePersistent(mqEvent);
			        
			        mqEventHistory.setMqEvent(mqEvent);
			        mqEventHistory.setXmlObject(req);
			        mqEventHistoryDAO.makePersistent(mqEventHistory);
		        	
		        
		        }else{
		        	mapStatus.put("Status", "Failed");
		        	mapStatus.put("Msg", messageResponse);
		        	mqEvent.setStatus("F");
			        mqEvent.setAckStatus(status);
			        mqEvent.setMsgStatus(messageResponse);
			        mqEventDAO.makePersistent(mqEvent);
			          
			        mqEventHistory.setMqEvent(mqEvent);
				    mqEventHistoryDAO.makePersistent(mqEventHistory);
		        }
			}   
			else{
	        	mqEvent.setStatus("F");
		        mqEvent.setAckStatus(status);
		        mqEvent.setMsgStatus(messageResponse);
		        mqEventDAO.makePersistent(mqEvent);
		          
			    mqEventHistory.setMqEvent(mqEvent);
			    mqEventHistoryDAO.makePersistent(mqEventHistory);
			}
			
		  
		  System.out.println("Single Sign On response:: "+url);
		  System.out.println(" status ::: "+status);
		}catch(Exception e)
		{
        	mqEvent.setStatus("F");
	        mqEvent.setAckStatus(status);
	        mqEvent.setMsgStatus(messageResponse);
	        mqEventDAO.makePersistent(mqEvent);
	          
	        mqEventHistory.setMqEvent(mqEvent);
		    mqEventHistoryDAO.makePersistent(mqEventHistory);
			e.printStackTrace();
		}finally {
			post.releaseConnection();
		}

		return mapStatus;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static String startBackgroundCheck(TeacherDetail teacherDetail, TeacherPersonalInfo teacherPersonalInfo,Map<String,RaceMaster> raceMap,DistrictMaster districtMaster)
	{
		String retrunValue = "";
		//String strURL="https://triton.bisi.com/adam12/PullAdam12.php";
		String strURL="https://triton.bisi.com/TeacherMatch/";
		
		System.out.println("STRURL=============="+strURL);
		
		PostMethod post = new PostMethod(strURL);
		try {
			System.out.println("teacherDetail.getTeacherId()::: "+teacherDetail.getTeacherId());
			//StringRequestEntity requestEntity = new StringRequestEntity(req,"application/xml","UTF-8");
			org.json.simple.JSONArray jsonArray = new org.json.simple.JSONArray();
			String candidateRace = "";
			String raceIds[] = new String[10];
			if(teacherPersonalInfo.getRaceId()!=null)
			{
				raceIds = teacherPersonalInfo.getRaceId().split(",");
			}
			if(raceIds.length>0)
			{
				for (int i = 0; i < raceIds.length; i++) {
					RaceMaster raceMaster = raceMap.get(""+raceIds[i]);
					if(raceMaster!=null)
					{
						candidateRace+=raceMaster.getRaceName()+";";
						jsonArray.add(raceMaster.getRaceName());
					}
				}

				if(candidateRace.length()>0)
				candidateRace = candidateRace.substring(0,candidateRace.length()-1);
			}
		
			System.out.println("teacherPersonalInfo:::: "+teacherPersonalInfo.getEmailAddress());
			JSONObject jsonOutput = new JSONObject();
			String authKey = districtMaster.getAuthKey()==null?"":districtMaster.getAuthKey();
			jsonOutput.put("authKey", authKey);
			jsonOutput.put("emailAddress", teacherDetail.getEmailAddress());
			jsonOutput.put("firstName", teacherPersonalInfo.getFirstName());
			jsonOutput.put("lastName", teacherPersonalInfo.getLastName());
			
			if(teacherPersonalInfo.getAddressLine1()!=null)
				jsonOutput.put("streetLine1",teacherPersonalInfo.getAddressLine1());
			else
				jsonOutput.put("streetLine1","");
			
			if(teacherPersonalInfo.getAddressLine2()!=null)
				jsonOutput.put("streetLine2",teacherPersonalInfo.getAddressLine2());
			else
				jsonOutput.put("streetLine2","");
			
			if(teacherPersonalInfo.getCityId()!=null)
				jsonOutput.put("city", teacherPersonalInfo.getCityId().getCityName());
			else
				jsonOutput.put("city", "");
			if(teacherPersonalInfo.getStateId()!=null)
				jsonOutput.put("state", teacherPersonalInfo.getStateId().getStateShortName());
			else
				jsonOutput.put("state", "");
			
			if(teacherPersonalInfo.getZipCode()!=null)
				jsonOutput.put("postalCode", teacherPersonalInfo.getZipCode());
			else
				jsonOutput.put("postalCode", "");
			if(teacherPersonalInfo.getPhoneNumber()!=null)
				jsonOutput.put("homePhone", teacherPersonalInfo.getPhoneNumber());
			else
				jsonOutput.put("homePhone", "");
			
			if(teacherPersonalInfo.getMobileNumber()!=null)
				jsonOutput.put("mobilePhone", teacherPersonalInfo.getMobileNumber());
			else
				jsonOutput.put("mobilePhone", "");
			
			if(teacherPersonalInfo.getGenderId()!=null)
				jsonOutput.put("gender", teacherPersonalInfo.getGenderId().getGenderName());
			else
				jsonOutput.put("gender", "");
			
			
			jsonOutput.put("ethnicity", jsonArray);
			
			
			//jsonOutput.put("Race", candidateRace);
			
			
			//jsonOutput.put("EmployeeType", employeeType);
			//jsonOutput.put("JobTitle", jobOrder.getJobTitle());
			/*jsonOutput.put("DatePosted", Utility.getDateWithoutTime(jobOrder.getJobStartDate()));
			jsonOutput.put("DateClosed", Utility.getDateWithoutTime(jobOrder.getJobEndDate()));*/
			
			
			String req=jsonOutput.toString();
			
			System.out.println("req:: "+req);
			StringRequestEntity requestEntity = new StringRequestEntity(req,"application/json","UTF-8");

			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "application/json");
			HttpClient httpclient = new HttpClient();

			int result = httpclient.executeMethod(post);            
			System.out.println("Response status code: " + result);            
			System.out.println("Response body: ");
			System.out.println(post.getResponseBodyAsString());    
			retrunValue = post.getResponseBodyAsString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			post.releaseConnection();
		}

		return retrunValue;
	}
	
	
	public static String[] loginToHireVue(String apiKey) throws Exception 
	{
		String csfrToken = null;
		CookieHandler.setDefault(new CookieManager());
		
		String url = "https://app.hirevue.com/api/v1/login/";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
 
		//add request header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", "");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("Content-Type", "application/json");
		 
		JSONObject jsonResponse = new JSONObject();
		/*jsonResponse.put("username", "tdisessa@teachermatch.org");
		jsonResponse.put("password","W4uk3$h4");
		jsonResponse.put("applicationToken", "test_public_token");
		jsonResponse.put("version", "1.2.0");*/
		
		jsonResponse.put("impersonate", "tdisessa@teachermatch.org");
		//jsonResponse.put("apiKey","azuwMzJMZ4b7Jg9QbAC376:100bb4073069b5c793c2ac1608f60c0880b6dec33223a276f7fcc9553ef9a50f");
		jsonResponse.put("apiKey",apiKey);
		jsonResponse.put("applicationToken", "test_public_token");
		jsonResponse.put("version", "1.2.0");
		
		System.out.println(jsonResponse.toString());
		
		String urlParameters = jsonResponse.toString();

		con.setDoOutput(true);
	    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
		
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		String sessionid = "";
		// List<String> cookieList = null;
		Map<String, List<String>> headerFieldsMap = null;
		headerFieldsMap = con.getHeaderFields();

		
		/*for (String header : headerFieldsMap.keySet())
		{
			for (String cookie :headerFieldsMap.get(header))
			{
				System.out.println("--->" + header + ":" + cookie + "");
				if(header!= null && header.contains("Set-Cookie"))
				{
					System.out.println("---< SessionId:" + header + ":" + cookie + "");
				}
			}
		}*/
		
		List<String> headerList = headerFieldsMap.get("csrftoken");
		for (String headerField : headerList)
		{
			csfrToken = headerField;
			System.out.println("csfrToken::"+ csfrToken);
		}
		
		headerList = headerFieldsMap.get("Set-Cookie");
		for (String headerField : headerList)
		{
			if(headerField!=null && headerField.contains("sessionid"))
			{
				sessionid = headerField.replace("sessionid=", "");
				System.out.println("sessionid:: "+ sessionid);
			}
		}
		
		if(sessionid==null || sessionid.equals(""))
		{
			headerList = headerFieldsMap.get("X-HvApi-Session-Key");
			for (String headerField : headerList)
			{
				sessionid = headerField;
				System.out.println("sessionid::"+ sessionid);
			}
		}
		
		String tokens[] = new String[2];
		tokens[0] = csfrToken;
		tokens[1] = sessionid;
		
		return tokens;
		
		}
	
	public static String getPositions(String csfrToken,String sessionid) throws Exception 
	{
		
		String retrunValue = "";
		String strURL="https://app.hirevue.com/api/v1/positions/";
		
		GetMethod get = new GetMethod(strURL); 
		try {
			
					
			BasicCookieStore cookieStore = new BasicCookieStore();
//		    BasicClientCookie cookie1 = new BasicClientCookie("csrftoken", "q5HgHql1XWoNZnj23qeLrixHEaXzConR");
			//BasicClientCookie cookie2 = new BasicClientCookie("sessionid", "z79i56ykhdex55grl9qjd7dbhrcgvt2q");
		    BasicClientCookie cookie2 = new BasicClientCookie("sessionid", sessionid);

//		    cookie1.setDomain("app.hirevue.com");
		    cookie2.setDomain("app.hirevue.com");
		    
		    //cookieStore.addCookie(cookie1);
		    cookieStore.addCookie(cookie2);
		    
		    org.apache.http.client.HttpClient client = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
		    HttpGet request = new HttpGet(strURL);
		    
		    HttpResponse response = client.execute(request);
		    
			System.out.println("Response status code: " + response.toString());   
			
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	        String readLine;
	        StringBuffer sb = new StringBuffer();
	        while(((readLine = br.readLine()) != null)) {
	        	sb.append(readLine);
	        }
	        System.out.println("Response:"+sb.toString());
	        return sb.toString();
		    
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			get.releaseConnection();
		}
		return null;
	}
	
	public static String addACandidateToAPosition(String csfrToken, String sessionid, String posId, String firstName, String lastName, String email) throws Exception
	//public static String addACandidateToAPosition(String csfrToken,String sessionid) throws Exception 
	{
		System.out.println(csfrToken +" "+sessionid+" "+posId+" "+firstName+" "+lastName+" "+email);
		
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put("firstName", firstName);
		jsonResponse.put("lastName",lastName);
		jsonResponse.put("email", email);
		//String posId = "363342";//372954
		String strURL="https://app.hirevue.com/api/v1/positions/"+posId+"/interviews/";
		HttpPost post = new HttpPost(strURL); 
		StringEntity requestEntity = new StringEntity(jsonResponse.toString(),"application/json","UTF-8");
		post.setEntity(requestEntity);
		post.setHeader("X-CSRFToken", csfrToken);
		post.setHeader("Content-type", "application/json;");
		try {
			BasicCookieStore cookieStore = new BasicCookieStore();
		    BasicClientCookie cookie1 = new BasicClientCookie("csrftoken", csfrToken);
		    BasicClientCookie cookie2 = new BasicClientCookie("sessionid", sessionid);
		    cookie1.setDomain("app.hirevue.com");
		    cookie2.setDomain("app.hirevue.com");
		    cookieStore.addCookie(cookie1);
		    cookieStore.addCookie(cookie2);
		    org.apache.http.client.HttpClient client = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
		    HttpResponse response = client.execute(post);
			System.out.println("Response status code: " + response.toString());   
			
			
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	        String readLine;
	        StringBuffer sb = new StringBuffer();
	        while(((readLine = br.readLine()) != null)) {
	        	sb.append(readLine);
	        }
	        System.out.println("Response:"+sb.toString());
	        
	        if(response.toString().contains("HTTP/1.1 201 CREATED"))
	        	return response.toString();
	        else
	        	return sb.toString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			post.releaseConnection();
		}
		return null;
	}
	
	public static String getPostionss(String csrftoken) throws Exception 
	{
		
		String retrunValue = "";
		//String strURL="https://app.hirevue.com/api/v1/positions/";
		String strURL="https://app.hirevue.com/api/v1/positions/";
		
		GetMethod get = new GetMethod(strURL); 
		try {
			
		/*	//get.setRequestHeader("Content-type", "application/json");
			get.setRequestHeader("csrftoken", "d8sPf7DsppOsGL0Wty6LZw4rAgUQWf80");
			get.setRequestHeader("sessionid", "l4k39ln67132kikl77x3n3afrzb782ez");
			HttpClient httpclient = new HttpClient();

			int result = httpclient.executeMethod(get);            
			System.out.println("Response status code: " + result);            
			//System.out.println("Response body: ");
			System.out.println(get.getResponseBodyAsString());    
			//retrunValue = post.getResponseBodyAsString();
*/			
			
			BasicCookieStore cookieStore = new BasicCookieStore();
//		    BasicClientCookie cookie1 = new BasicClientCookie("__utma", "267033751.1635173688.1453461979.1453529212.1453906928.3");
//		    BasicClientCookie cookie2 = new BasicClientCookie("__utmz", "267033751.1453461979.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)");
//		    BasicClientCookie cookie3 = new BasicClientCookie("csrftoken", "q5HgHql1XWoNZnj23qeLrixHEaXzConR");
//		    BasicClientCookie cookie4 = new BasicClientCookie("mp_cf2c528ffc5b55d55669a346d04724ca_mixpanel", "9dl2px3d7rrk232dpw1ixwicl58z5vl8");
		    BasicClientCookie cookie5 = new BasicClientCookie("sessionid", "z79i56ykhdex55grl9qjd7dbhrcgvt2q");
		   
//		    cookie1.setPath("/");
//		    cookie1.setDomain(".hirevue.com");
//		    
//		    cookie2.setPath("/");
//		    cookie2.setDomain(".hirevue.com");
//		    
//		    cookie3.setPath("/");
//		    cookie3.setDomain("app.hirevue.com");
		    
//		    cookie4.setPath("/");
//		    cookie4.setDomain(".hireveue.com");
		    
//		    cookie5.setPath("/");
		    cookie5.setDomain("app.hirevue.com");
		    
		    //cookieStore.addCookie(cookie1);
		    //cookieStore.addCookie(cookie2);
		    //cookieStore.addCookie(cookie3);
		    cookieStore.addCookie(cookie5);
		    
		    org.apache.http.client.HttpClient client = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
		    HttpGet request = new HttpGet(strURL);
		    
		    HttpResponse response = client.execute(request);
		    
			System.out.println("Response status code: " + response.toString());   
			
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	        String readLine;
	        while(((readLine = br.readLine()) != null)) {
	          System.err.println(readLine);
	        }
		    
		    
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			get.releaseConnection();
		}
		return null;
	
		
		}
	
	
	
	}