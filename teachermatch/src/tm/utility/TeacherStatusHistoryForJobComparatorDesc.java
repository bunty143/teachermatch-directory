package tm.utility;

import java.util.Comparator;

import tm.bean.TeacherStatusHistoryForJob;

public class TeacherStatusHistoryForJobComparatorDesc implements Comparator<TeacherStatusHistoryForJob>{
	
	//public static Comparator<TeacherStatusHistoryForJob> teacherStatusHistoryForJobComparatorDesc = new Comparator<TeacherStatusHistoryForJob>() {
		
		public int compare(TeacherStatusHistoryForJob jft1, TeacherStatusHistoryForJob jft2) {
		
			Long cScore1 = jft1.getTeacherStatusHistoryForJobId();
			Long cScore2 = jft2.getTeacherStatusHistoryForJobId();	
			int c = cScore2.compareTo(cScore1);
			return c;
		}		
}
