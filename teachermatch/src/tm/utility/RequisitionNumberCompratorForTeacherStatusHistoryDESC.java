package tm.utility;

import java.util.Comparator;

import tm.bean.TeacherStatusHistoryForJob;

public class RequisitionNumberCompratorForTeacherStatusHistoryDESC implements Comparator<TeacherStatusHistoryForJob> {

	public int compare(TeacherStatusHistoryForJob o1, TeacherStatusHistoryForJob o2) {
		return o2.getRequisitionNumber().toUpperCase().compareTo(o1.getRequisitionNumber().toUpperCase());
	}
}
