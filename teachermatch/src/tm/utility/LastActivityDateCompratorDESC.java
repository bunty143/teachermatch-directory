package tm.utility;

import java.util.Comparator;
import java.util.Date;

import tm.bean.JobForTeacher;

public class LastActivityDateCompratorDESC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		
		if( o2.getLastActivityDate()!=null && o1.getLastActivityDate()!=null)
			 return o2.getLastActivityDate().compareTo(o1.getLastActivityDate());
		
		else if( o1.getLastActivityDate()!=null && o2.getLastActivityDate()==null)
			return (new Date(0)).compareTo(o1.getLastActivityDate());
			
		else if( o1.getLastActivityDate()==null && o2.getLastActivityDate()!=null)
			return o2.getLastActivityDate().compareTo(new Date(0));
			
		else return-1;
	}
}
