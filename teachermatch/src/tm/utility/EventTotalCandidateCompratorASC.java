package tm.utility;

import java.util.Comparator;

import tm.bean.EventDetails;

public class EventTotalCandidateCompratorASC implements Comparator<EventDetails> {

	public int compare(EventDetails o1, EventDetails o2) {
		if(o1.getNoOfCandidate()== null)
			if(o2.getNoOfCandidate()== null)
				 return 0; //equal
			else
				 return -1; //null is before other strings
		
	       else // this.member != null
	    	   if(o2.getNoOfCandidate()== null)
	            return 1;  // all other strings are after null
		
		return o1.getNoOfCandidate().compareTo(o2.getNoOfCandidate());
	
	}
}
