package tm.utility;

import java.util.Comparator;

import tm.bean.JobOrder;

public class JobOrderSchoolNameCompratorASC implements Comparator<JobOrder> {

	public int compare(JobOrder o1, JobOrder o2) {
		return o1.getSchoolName().toUpperCase().compareTo(o2.getSchoolName().toUpperCase());
	}
}
