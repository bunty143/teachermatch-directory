package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;

public class HiredDateCompratorASC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		return o1.getHiredDate().compareTo(o2.getHiredDate());
	}
}
