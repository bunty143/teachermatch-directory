package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;
import tm.bean.TeacherPersonalInfo;

public class AvlCandidateCompratorDESC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		Integer a = o1.getAvlCandidate();
		Integer b = o2.getAvlCandidate();
		
		if(a==null)
			a=0;
		
		if(b==null)
			b=0;
		
		return b.compareTo(a);
		}
	
}
