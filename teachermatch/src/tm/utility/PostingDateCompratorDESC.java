package tm.utility;

import java.util.Comparator;
import java.util.Date;

import tm.bean.JobForTeacher;

public class PostingDateCompratorDESC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		
		if(o1.getPostDate()==null)
			o1.setPostDate(new Date());
		
		if(o2.getPostDate()==null)
			o2.setPostDate(new Date());
		
		return o2.getPostDate().compareTo(o1.getPostDate());
	}
}
