package tm.utility;

import java.util.Comparator;

import tm.bean.EventDetails;

public class EventStartDateCompratorASC implements Comparator<EventDetails> {

	public int compare(EventDetails o1, EventDetails o2) {

		if(o1.getEventStartDate()== null)
			if(o2.getEventStartDate()== null)
				 return 0; //equal
			else
				 return -1; //null is before other strings
		
	       else // this.member != null
	    	   if(o2.getEventStartDate()== null)
	            return 1;  // all other strings are after null
	         else
	        	 return o1.getEventStartDate().compareTo(o2.getEventStartDate());
	
	}
}
