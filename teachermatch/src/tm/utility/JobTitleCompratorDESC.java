package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;
import tm.bean.TeacherPersonalInfo;

public class JobTitleCompratorDESC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		
		if(o1.getJobTitle()==null)
			o1.setJobTitle("");
		
		if(o2.getJobTitle()==null)
			o2.setJobTitle("");
		return o2.getJobTitle().toUpperCase().compareTo(o1.getJobTitle().toUpperCase());
	}
}
