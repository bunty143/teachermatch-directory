package tm.utility;

import java.util.Comparator;

import tm.bean.EventDetails;

public class EventStartDateCompratorDESC implements Comparator<EventDetails> {

	public int compare(EventDetails o1, EventDetails o2) {
		
		if(o2.getEventStartDate()== null)
			if(o1.getEventStartDate()== null)
				 return 0; //equal
			else
				 return -1; //null is before other strings
		
	       else // this.member != null
	    	   if(o1.getEventStartDate()== null)
	            return 1;  // all other strings are after null
	         else
	        	 return o2.getEventStartDate().compareTo(o1.getEventStartDate());
	
	
	}
}
