package tm.utility;

import java.util.List;
import java.util.Map;
public class MemoryRelease {
	public static List setClearAndNull(List<List> listAllData, List<Map>  map) {
		  try{
			  synchronized(MemoryRelease.class){
				  if(listAllData!=null)
				  for(List list1:listAllData){
				      try{
				       list1.clear();
				       list1=null;
				      }catch (Exception e){e.printStackTrace();}
				     
				     }
				  if(map!=null)
				  for(Map map1:map){
				      try{
				       map1.clear();
				       map1=null;
				      }catch (Exception e){e.printStackTrace();}
				     
				     }
					}
		     }catch(Exception e){e.printStackTrace();} 
		     finally{
		      listAllData.clear();
		      listAllData=null;
		      map.clear();
		      map=null;
		     }
		     return null;
		 }

}
