package tm.utility;

import java.util.Comparator;

import tm.bean.TeacherDetail;

public class TeacherDetailComparatorLastNameDesc implements Comparator<TeacherDetail>{

	public int compare(TeacherDetail td1, TeacherDetail td2) {			
		return td2.gettLastName().toUpperCase().compareTo(td1.gettLastName().toUpperCase());
		
	}
}
