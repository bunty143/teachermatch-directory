package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;

public class InternalCandidateCompratorDESC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		return (o2.getIsAffilated()-o1.getIsAffilated());
	}
}
