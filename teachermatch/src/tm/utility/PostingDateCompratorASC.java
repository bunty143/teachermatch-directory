package tm.utility;

import java.util.Comparator;
import java.util.Date;

import tm.bean.JobForTeacher;

public class PostingDateCompratorASC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		
		if(o1.getPostDate()==null)
			o1.setPostDate(new Date());
		
		if(o2.getPostDate()==null)
			o2.setPostDate(new Date());
		
		return o1.getPostDate().compareTo(o2.getPostDate());
	}
}
