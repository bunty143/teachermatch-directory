package tm.utility;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import tm.services.district.GlobalServices;

public class JDBCFunction {
	public static Set<String> WRAPPER_CLASS_PRIMITIVE=new TreeSet<String>();
	static{
		setAllWrapper();
	}
	public static void setAllWrapper(){
		WRAPPER_CLASS_PRIMITIVE.add("class java.lang.String");
		WRAPPER_CLASS_PRIMITIVE.add("class java.lang.Integer");
		WRAPPER_CLASS_PRIMITIVE.add("class java.lang.Long");
		WRAPPER_CLASS_PRIMITIVE.add("class java.lang.Boolean");
		WRAPPER_CLASS_PRIMITIVE.add("class java.lang.Double");
		WRAPPER_CLASS_PRIMITIVE.add("class java.lang.Float");
		WRAPPER_CLASS_PRIMITIVE.add("class java.util.Date");
		WRAPPER_CLASS_PRIMITIVE.add("int");
		WRAPPER_CLASS_PRIMITIVE.add("float");
		WRAPPER_CLASS_PRIMITIVE.add("double");
		WRAPPER_CLASS_PRIMITIVE.add("long");
		WRAPPER_CLASS_PRIMITIVE.add("boolean");
	}
	public static void isPrint(boolean isPrint){
		setAllWrapper();
		if(isPrint){
			GlobalServices.IS_PRINT=isPrint;
		}
	}
	public synchronized static String insertObject(Object obj,Connection connection) throws Exception{
		String returnInsertedValue=null;
			String idMainTableColName=null;
			LinkedList<Object> valueObj=new LinkedList<Object> ();
			String tableName=null;
			for (Annotation annotation : obj.getClass().getAnnotations()) {
	            Class<? extends Annotation> type = annotation.annotationType();
	           GlobalServices.println("Values of " + type.getName());
	            if(type.getName().contains("javax.persistence.Table"))
	            for (Method method : type.getDeclaredMethods()) {
	                Object value = method.invoke(annotation, (Object[])null);
	                if(method.getName().equals("name")){
	                	tableName=((String)value);
	                	//GlobalServices.println(" " + method.getName() + ": " + value);
	                	break;
	                }
	            }
	        }

			
			StringBuffer br=new StringBuffer("INSERT INTO "+tableName+" (");
			StringBuffer brVal=new StringBuffer("");
			int count=0;
			for (Field field : obj.getClass().getDeclaredFields()) {
			    field.setAccessible(true); // You might want to set modifier to public first.
			    Object value = field.get(obj); 
			    if (value != null) {
			      GlobalServices.println(field.getName() + "=======---======" + value.getClass());
			        if(!field.getName().toString().contains("serialVersionUID") && !isTransient(obj,field) && !field.getType().toString().contains("java.util.Comparator")){
			        	if(WRAPPER_CLASS_PRIMITIVE.contains(field.getType().toString()) )
			        	{
					        if(count==0){
					        	br.append(field.getName());
					        	brVal.append("?");
					        }else{
					        	br.append(", "+field.getName());
					        	brVal.append(", ?");
					        }
			        	}else if(!field.getType().toString().contains(obj.getClass().toString()+"$")){
			        		GlobalServices.println("join check ===="+field.getName()+"        "+value.getClass());
			        		String objectColumnName=null;
			        		
			        		
			        		for(Field field1 : obj.getClass().getDeclaredFields()){
			            		  Class type1 = field1.getType();
			            		  String name = field1.getName();
			            		  //GlobalServices.println("===========>"+type1+"                "+name);
			            		  
			            		  Annotation[] annotations = field1.getDeclaredAnnotations();
			            		  for(Annotation anno:annotations){
			            			  Class<? extends Annotation> type = anno.annotationType();
							           GlobalServices.println("Annotation Values of yyy ::: " + type.getName());
							            if(type.getName().contains("javax.persistence.Id"))
							            idMainTableColName=name;
							            if(field.getName().toString().trim().equals(name.toString().trim())){
							            if(type.getName().contains("javax.persistence.JoinColumn"))
							            for (Method method : type.getDeclaredMethods()) {
							                Object valueInvoke = method.invoke(anno, (Object[])null);
							                if(method.getName().equals("name")){
							                	objectColumnName=((String)valueInvoke);
							                	GlobalServices.println(" " + method.getName() + ": " + valueInvoke);
							                	break;
							                }
							            }
							           }
			            		  }
			            		}
							GlobalServices.println("end join check=objectColumnName====="+objectColumnName);
							if(count==0){
					        	br.append(objectColumnName);
					        	brVal.append("?");
					        }else{
					        	br.append(", "+objectColumnName);
					        	brVal.append(", ?");
					        }
			        	}
			        	valueObj.add(value);
				        ++count;
			        }
			    }
			}
			br.append(") VALUES("+brVal.toString()+")");
			System.out.println(br.toString());
			PreparedStatement ps = connection.prepareStatement(br.toString(),Statement.RETURN_GENERATED_KEYS);
			for(int colVal=1;colVal<=valueObj.size();colVal++){
				Object objVAL=valueObj.get(colVal-1);
				GlobalServices.println("class name==="+objVAL.getClass());
				if(objVAL.getClass().toString().contains("java.lang.String")){
					String valueString=((String)objVAL);
					ps.setString(colVal, valueString);
					GlobalServices.println("value String======="+valueString+"    "+objVAL);
				}else if(objVAL.getClass().toString().contains("java.lang.Boolean") || objVAL.getClass().toString().contains("boolean")){
					Boolean valueString=((Boolean)objVAL);
					if(valueString)
					ps.setInt(colVal, 1);
					else
						ps.setInt(colVal, 0);	
					GlobalServices.println("value Boolean======="+valueString);
				}else if(objVAL.getClass().toString().contains("java.lang.Integer") || objVAL.getClass().toString().contains("int")){
					Integer valueString=((Integer)objVAL);
					ps.setLong(colVal, valueString);
					GlobalServices.println("value Integer======="+valueString);
				}else if(objVAL.getClass().toString().contains("java.lang.Long") || objVAL.getClass().toString().contains("long")){
					Long valueString=((Long)objVAL);
					ps.setLong(colVal, valueString);
					GlobalServices.println("value Long======="+valueString);
				}else if(objVAL.getClass().toString().contains("java.lang.Float") || objVAL.getClass().toString().contains("float")){
					Float valueString=((Float)objVAL);
					ps.setFloat(colVal, valueString);
					GlobalServices.println("value Integer======="+valueString);
				}else if(objVAL.getClass().toString().contains("java.lang.Double") || objVAL.getClass().toString().contains("double")){
					Double valueString=((Double)objVAL);
					ps.setDouble(colVal, valueString);
					GlobalServices.println("value Long======="+valueString);
				}else if(objVAL.getClass().toString().contains("java.util.Date")){
					Date valueString=((Date)objVAL);
					ps.setTimestamp(colVal,new java.sql.Timestamp(valueString.getTime()));
					GlobalServices.println("value Date======="+valueString);
				}
				else if(objVAL.getClass().toString().contains("java.sql.Timestamp")){
					Date valueString=((Date)objVAL);
					ps.setTimestamp(colVal,new java.sql.Timestamp(valueString.getTime()));
					GlobalServices.println("value Date======="+valueString);
				}else if(!objVAL.getClass().toString().contains(obj.getClass().toString()+"$")){
					Object valueString=((Object)objVAL);
					GlobalServices.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy    "+objVAL.getClass()+"      "+objVAL);
					
					String classId=getId(valueString);
					GlobalServices.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy end "+classId);
					ps.setString(colVal, classId);
					//GlobalServices.println("valueOther======="+objVAL.getClass()+"   "+classId);
				}
			}
			boolean affectedRows = ps.execute();
			ResultSet generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next())
				returnInsertedValue=generatedKeys.getString(1);
				GlobalServices.println("returnInsertedValue=========================="+returnInsertedValue);
			
			
			/*for (Field f : obj.getClass().getDeclaredFields()) {
				GlobalServices.println("idMainTableColName========================================="+idMainTableColName);
				   f.setAccessible(true); // You might want to set modifier to public first.
				   Object value = f.get(obj);
			       String name = f.getName();
			       if(idMainTableColName.equals(name)){
			    	   Class t = field.getType();
			    	   f.set(f.getType(), );
			    	  GlobalServices.println(name+"                      =============================="+f.getType());
			       }
				   
			       // Need set value field base on its name and type. 

			} */
			return returnInsertedValue;
	}
	
	public static String getId(Object obj)throws Exception{
		String id=null;
		String idFieldName=null;
		GlobalServices.println("----------------------------------------------"+obj.getClass()+"---------------------------------------------------");
		for(Field field1 : obj.getClass().getDeclaredFields()){
  		  Class type1 = field1.getType();
  		  String name = field1.getName();
  		 GlobalServices.println("===========>"+type1+"                "+name);
  		  Annotation[] annotations = field1.getDeclaredAnnotations();
  		  for(Annotation anno:annotations){
  			  Class<? extends Annotation> type = anno.annotationType();
		           GlobalServices.println("Annotation Values of " + type.getName()+"    ");
		            if(type.getName().contains("javax.persistence.Id"))
		            	 idFieldName=name;
  		  }
  		}
		
		
		for (Field fieldSub : obj.getClass().getDeclaredFields()) {
		    fieldSub.setAccessible(true); // You might want to set modifier to public first.
		    Object valueSub = fieldSub.get(obj); 
		    if (valueSub != null) {
		    	GlobalServices.println(idFieldName+"       "+fieldSub.getName());
		    	if(idFieldName.trim().equals(fieldSub.getName().trim())){
		    		id=valueSub.toString();
		    		break;
		    	}
		    	GlobalServices.println(fieldSub.getName() + "=======Sub======" + valueSub);
		    }
		}
		GlobalServices.println("id==========================="+id);
		return id;
	}
	
	public synchronized static String updateObject(Object obj,Connection connection,Map<String,String> criteria) throws Exception{
		
		if(criteria==null && criteria.isEmpty()){
			throw new SQLQueryCreateException("Sql Update Query create exception please provide criteria map");
		}
		String returnInsertedValue=null;
			String idMainTableColName=null;
			LinkedList<Object> valueObj=new LinkedList<Object> ();
			String tableName=null;
			for (Annotation annotation : obj.getClass().getAnnotations()) {
	            Class<? extends Annotation> type = annotation.annotationType();
	           GlobalServices.println("Values of " + type.getName());
	            if(type.getName().contains("javax.persistence.Table"))
	            for (Method method : type.getDeclaredMethods()) {
	                Object value = method.invoke(annotation, (Object[])null);
	                if(method.getName().equals("name")){
	                	tableName=((String)value);
	                	//GlobalServices.println(" " + method.getName() + ": " + value);
	                	break;
	                }
	            }
	        }

			
			StringBuffer br=new StringBuffer("UPDATE "+tableName+" SET ");
			StringBuffer brVal=new StringBuffer("");
			int count=0;
			for (Field field : obj.getClass().getDeclaredFields()) {
			    field.setAccessible(true); // You might want to set modifier to public first.
			    Object value = field.get(obj); 
			    if (value != null) {
			      // GlobalServices.println(field.getName() + "=======---======" + value.getClass());
			        if(!field.getName().toString().contains("serialVersionUID") && !isTransient(obj,field) && !field.getType().toString().contains("java.util.Comparator")){
			        	
			        	if(WRAPPER_CLASS_PRIMITIVE.contains(field.getType().toString()) )
			        	{
					        if(count==0){
					        	br.append(field.getName());
					        	br.append("=?");
					        }else{
					        	br.append(", "+field.getName());
					        	br.append("=?");
					        }
			        	}else{
			        		GlobalServices.println("join check ===="+field.getName());
			        		String objectColumnName=null;
			        		
			        		
			        		for(Field field1 : obj.getClass().getDeclaredFields()){
			            		  Class type1 = field1.getType();
			            		  String name = field1.getName();
			            		  //GlobalServices.println("===========>"+type1+"                "+name);
			            		  
			            		  Annotation[] annotations = field1.getDeclaredAnnotations();
			            		  for(Annotation anno:annotations){
			            			  Class<? extends Annotation> type = anno.annotationType();
							           GlobalServices.println("Annotation Values of yyy ::: " + type.getName());
							            if(type.getName().contains("javax.persistence.Id"))
							            idMainTableColName=name;
							            if(field.getName().toString().trim().equals(name.toString().trim())){
							            if(type.getName().contains("javax.persistence.JoinColumn"))
							            for (Method method : type.getDeclaredMethods()) {
							                Object valueInvoke = method.invoke(anno, (Object[])null);
							                if(method.getName().equals("name")){
							                	objectColumnName=((String)valueInvoke);
							                	GlobalServices.println(" " + method.getName() + ": " + valueInvoke);
							                	break;
							                }
							            }
							           }
			            		  }
			            		}
							GlobalServices.println("end join check=objectColumnName====="+objectColumnName);
							if(count==0){
					        	br.append(objectColumnName);
					        	br.append("=?");
					        }else{
					        	br.append(", "+objectColumnName);
					        	br.append("=?");
					        }
			        	}
			        	valueObj.add(value);
				        ++count;
			        }
			    }
			}
			count=0;
			if(criteria!=null)
			for(Map.Entry<String, String> entry:criteria.entrySet()){
				if(count==0){
					br.append(" WHERE ");
					br.append(" "+entry.getKey()+"=?");
				}else{
					br.append(" AND "+entry.getKey()+"=? ");
				}
				++count;
			}
			System.out.println(br.toString());
			PreparedStatement ps = connection.prepareStatement(br.toString());
			int colVal=1;
			for(colVal=1;colVal<=valueObj.size();colVal++){
				Object objVAL=valueObj.get(colVal-1);
				GlobalServices.println("class name==="+objVAL.getClass());
				if(objVAL.getClass().toString().contains("java.lang.String")){
					String valueString=((String)objVAL);
					ps.setString(colVal, valueString);
					GlobalServices.println("value String======="+valueString+"    "+objVAL);
				}else if(objVAL.getClass().toString().contains("java.lang.Boolean") || objVAL.getClass().toString().contains("boolean")){
					Boolean valueString=((Boolean)objVAL);
					if(valueString)
					ps.setInt(colVal, 1);
					else
						ps.setInt(colVal, 0);	
					GlobalServices.println("value Boolean======="+valueString);
				}else if(objVAL.getClass().toString().contains("java.lang.Integer") || objVAL.getClass().toString().contains("int")){
					Integer valueString=((Integer)objVAL);
					ps.setLong(colVal, valueString);
					GlobalServices.println("value Integer======="+valueString);
				}else if(objVAL.getClass().toString().contains("java.lang.Long") || objVAL.getClass().toString().contains("long")){
					Long valueString=((Long)objVAL);
					ps.setLong(colVal, valueString);
					GlobalServices.println("value Long======="+valueString);
				}else if(objVAL.getClass().toString().contains("java.lang.Float") || objVAL.getClass().toString().contains("float")){
					Float valueString=((Float)objVAL);
					ps.setFloat(colVal, valueString);
					GlobalServices.println("value Integer======="+valueString);
				}else if(objVAL.getClass().toString().contains("java.lang.Double") || objVAL.getClass().toString().contains("double")){
					Double valueString=((Double)objVAL);
					ps.setDouble(colVal, valueString);
					GlobalServices.println("value Long======="+valueString);
				}else if(objVAL.getClass().toString().contains("java.util.Date")){
					Date valueString=((Date)objVAL);
					ps.setTimestamp(colVal,new java.sql.Timestamp(valueString.getTime()));
					GlobalServices.println("value Date======="+valueString);
				}
				else if(objVAL.getClass().toString().contains("java.sql.Timestamp")){
					Date valueString=((Date)objVAL);
					ps.setTimestamp(colVal,new java.sql.Timestamp(valueString.getTime()));
					GlobalServices.println("value Date======="+valueString);
				}else if(!objVAL.getClass().toString().contains(obj.getClass().toString()+"$")){
					Object valueString=((Object)objVAL);
					GlobalServices.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
					String classId=getId(valueString);
					GlobalServices.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy end");
					ps.setString(colVal, classId);
					GlobalServices.println("valueOther======="+objVAL.getClass()+"   "+classId);
				}
			}
			for(String entry:criteria.keySet()){
				ps.setString(colVal, criteria.get(entry));
				++colVal;
			}
			int affectedRows = ps.executeUpdate();
				returnInsertedValue=""+affectedRows;
				GlobalServices.println("returnInsertedValue=========================="+returnInsertedValue);
			
			
			/*for (Field f : obj.getClass().getDeclaredFields()) {
				GlobalServices.println("idMainTableColName========================================="+idMainTableColName);
				   f.setAccessible(true); // You might want to set modifier to public first.
				   Object value = f.get(obj);
			       String name = f.getName();
			       if(idMainTableColName.equals(name)){
			    	   Class t = field.getType();
			    	   f.set(f.getType(), );
			    	  GlobalServices.println(name+"                      =============================="+f.getType());
			       }
				   
			       // Need set value field base on its name and type. 

			} */
			return returnInsertedValue;
	}
	public static boolean isTransient(Object obj, Field field)throws Exception{ 
			for (Field field1 : obj.getClass().getDeclaredFields()) {
				Class type1 = field1.getType();
				String name = field1.getName();
				// GlobalServices.println("===========>"+type1+"                "+name);
				if (field.getName().toString().trim().equals(
						name.toString().trim())) {
					Annotation[] annotations = field1.getDeclaredAnnotations();
					for (Annotation anno : annotations) {
						Class<? extends Annotation> type = anno.annotationType();
						GlobalServices.println("Annotation Values of yyy-------------- ::: "+ type.getName());
						if (type.getName().contains(
								"javax.persistence.Transient"))
							return true;
					}
				}
			}
		return false;
	}
	
	
	public synchronized static String insertQuery(Object obj) throws Exception{
		GlobalServices.IS_PRINT=true;
		String idMainTableColName=null;
		LinkedList<Object> valueObj=new LinkedList<Object> ();
		String tableName=null;
		for (Annotation annotation : obj.getClass().getAnnotations()) {
            Class<? extends Annotation> type = annotation.annotationType();
           GlobalServices.println("Values of " + type.getName());
            if(type.getName().contains("javax.persistence.Table"))
            for (Method method : type.getDeclaredMethods()) {
                Object value = method.invoke(annotation, (Object[])null);
                if(method.getName().equals("name")){
                	tableName=((String)value);
                	//GlobalServices.println(" " + method.getName() + ": " + value);
                	break;
                }
            }
        }

		
		StringBuffer br=new StringBuffer("INSERT INTO "+tableName+" (");
		StringBuffer brVal=new StringBuffer("");
		int count=0;
		for (Field field : obj.getClass().getDeclaredFields()) {
		    field.setAccessible(true); // You might want to set modifier to public first.
		    Object value = field.get(obj); 
		    if (value != null || value ==null) {
		      GlobalServices.println(field.getName() + "=======---======" + field.getType());
		        if(!field.getName().toString().contains("serialVersionUID") && !isTransient(obj,field) && !field.getType().toString().contains("java.util.Comparator")){
		        	if(WRAPPER_CLASS_PRIMITIVE.contains(field.getType().toString()) )
		        	{
				        if(count==0){
				        	br.append(field.getName());
				        	brVal.append(getValue(value));
				        }else{
				        	br.append(", "+field.getName());
				        	brVal.append(", "+getValue(value));
				        }
		        	}else if(!field.getType().toString().contains(obj.getClass().toString()+"$")){
		        		GlobalServices.println("join check ===="+field.getName()+"        "+field.getType());
		        		String objectColumnName=null;
		        		
		        		
		        		for(Field field1 : obj.getClass().getDeclaredFields()){
		            		  Class type1 = field1.getType();
		            		  String name = field1.getName();
		            		  //GlobalServices.println("===========>"+type1+"                "+name);
		            		  
		            		  Annotation[] annotations = field1.getDeclaredAnnotations();
		            		  for(Annotation anno:annotations){
		            			  Class<? extends Annotation> type = anno.annotationType();
						           GlobalServices.println("Annotation Values of yyy ::: " + type.getName());
						            if(type.getName().contains("javax.persistence.Id"))
						            idMainTableColName=name;
						            if(field.getName().toString().trim().equals(name.toString().trim())){
						            if(type.getName().contains("javax.persistence.JoinColumn"))
						            for (Method method : type.getDeclaredMethods()) {
						                Object valueInvoke = method.invoke(anno, (Object[])null);
						                if(method.getName().equals("name")){
						                	objectColumnName=((String)valueInvoke);
						                	GlobalServices.println(" " + method.getName() + ": " + valueInvoke);
						                	break;
						                }
						            }
						           }
		            		  }
		            		}
						GlobalServices.println("end join check=objectColumnName====="+objectColumnName);
						if(count==0){
				        	br.append(objectColumnName);
				        	if(value!=null)
				        	brVal.append(getId(value));
				        	else
				        		brVal.append("NULL");
				        }else{
				        	br.append(", "+objectColumnName);
				        	if(value!=null)
				        	brVal.append(", "+getId(value));
				        	else
				        		brVal.append(", NULL");
				        }
		        	}
		        	valueObj.add(value);
		        	
			        ++count;
		        }
		    }
		}
		br.append(") VALUES("+brVal.toString()+")");
		System.out.println(br.toString());
		return br.toString();
}
	
	public synchronized static String getJDBCInsertQuery(LinkedHashMap<String,String> insertColumnWithValue, String tableName)throws Exception{
		StringBuffer bufferString=new StringBuffer("INSERT INTO "+tableName+" ");
		StringBuffer bufferStringCol=new StringBuffer();
		StringBuffer bufferStringVal=new StringBuffer();
		if(tableName!=null && !tableName.equals("") && insertColumnWithValue!=null && insertColumnWithValue.size()>0){
		int columnCount=1;
				for (Map.Entry<String, String> entry:insertColumnWithValue.entrySet()) {
					if (columnCount == 1)
						bufferStringCol.append("(" + entry.getKey());
					else if (columnCount == insertColumnWithValue.size())
						bufferStringCol.append("," + entry.getKey() + ") ");
					else
						bufferStringCol.append("," + entry.getKey());
					
					if (columnCount == 1)
						bufferStringVal.append("VALUES(?");
					else if (columnCount == insertColumnWithValue.size())
						bufferStringVal.append(",?) ");
					else
						bufferStringVal.append(",?");
					++columnCount;
				}
				bufferString.append(bufferStringCol.toString() + " "+bufferStringVal.toString());
			return bufferString.toString();
		}else{
			throw new SQLQueryCreateException("Sql Insert Query create exception please check your table name and input collection map");
		}
	}
	public synchronized static String getJDBCUpdateQuery(Map<String,String> updateColumnWithValue, String tableName, Map<String,String> whereClauseMap)throws Exception{
		StringBuffer bufferString=new StringBuffer("UPDATE "+tableName+" SET ");
		if(tableName!=null && !tableName.equals("") && updateColumnWithValue!=null && updateColumnWithValue.size()>0 ){
			int columnCount=0;
			for (Map.Entry<String, String> entry:updateColumnWithValue.entrySet()) {
				if(columnCount==0)
					bufferString.append(entry.getKey()+"=?");
				else
					bufferString.append(","+entry.getKey()+"=?");
				++columnCount;
			}
			if(whereClauseMap!=null && whereClauseMap.size()>0){
				columnCount=0;
				bufferString.append(" WHERE ");
				for (Map.Entry<String, String> entry:whereClauseMap.entrySet()) {
					if(columnCount==0)
						bufferString.append(entry.getKey()+"=?");
					else
						bufferString.append(" AND "+entry.getKey()+"=?");
					++columnCount;
				}
			}
			
			return bufferString.toString();
		}else{
			throw new SQLQueryCreateException("Sql Update Query create exception please check your inputs");
		}
	}
	
	public static boolean executeUpdateQuery(String query, Connection connection)throws SQLException{
		PreparedStatement statement = connection.prepareStatement(query);
		int affectedRows = statement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Creating user failed, no rows affected.");
        }        
		return true;
	}
	public static String executeInsertQuery(String query, Connection connection)throws SQLException{
		PreparedStatement ps = connection.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
		boolean affectedRows = ps.execute();
        if (!affectedRows) {
            throw new SQLException("Insert failed, no rows affected.");
        }
        ResultSet generatedKeys = ps.getGeneratedKeys();
        if (generatedKeys.next()) {
        	return generatedKeys.getString(1);
        }
        return null;
	}
	
	static class SQLQueryCreateException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		SQLQueryCreateException(String s){  
		  super(s);  
		}  
	}
	
	public synchronized static <T> LinkedHashMap<String,T> createMap() {
	    return new LinkedHashMap<String,T>();
	 }
	
	
	public static String getValue(Object obj){
		if(obj instanceof String){
			if(obj!=null)
			return ((String)obj).toString();
			else
				return null;
		}else if(obj instanceof Integer){
			if(obj!=null)
				return ((Integer)obj).toString();
				else
					return null;
			}else if(obj instanceof Long){
				if(obj!=null)
					return ((Long)obj).toString();
					else
						return null;
				}else if(obj instanceof Boolean){
					if(obj!=null)
						return ((Boolean)obj)?"1":"0";
						else
							return null;
					}else if(obj instanceof Double){
						if(obj!=null)
							return ((Double)obj).toString();
							else
								return null;
						}else if(obj instanceof Float){
							if(obj!=null)
								return ((Float)obj).toString();
								else
									return null;
							}else if(obj instanceof Date){
								if(obj!=null)
									return ((Date)obj).toString();
									else
										return null;
								}else{
									return null;
								}
	}
	
	public synchronized static String insertWithMap(Map columnKey_columnValue,Connection connection,String tableName) throws Exception{
		/*
		 * CREATE TABLE `trackingrequestapi` (
		  `id` int(10) NOT NULL auto_increment,
		  `apiName` varchar(255) NOT NULL,
		  `requestParamenter` text NOT NULL,
		  `respondMessage` text default NULL,
		  `apiCallingDateTime` datetime NOT NULL,
		  PRIMARY KEY  (`id`)
		) ENGINE=InnoDB;
		 */
		
		/*
		 * How to get Connection from Hibernate
		 * 
		 *  SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor)sessionFactory;
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
			Connection connection = connectionProvider.getConnection();
		 */
		
		/*
		 * Example : How to use this method
		 * Table : trackingrequestapi 
		 * Column : id, apiName, requestParamenter, apiCallingDateTime, respondMessage
		 * Map map=new LinkedHashMap();
		 * map.put("id",1);
		 * map.put("apiName","Ram");
		 * map.put("requestParamenter","{"authKey":"juljljljljljljljljljl==="}");
		 * map.put("apiCallingDateTime",new Date());
		 * map.put("respondMessage","Employee Id does not exists");
		 * How to call this method : JDBCFunction.insertWithMap(map,jdbcConnection,"trackingrequestapi");
		 */
		if(columnKey_columnValue!=null && connection!=null && tableName!=null){
		StringBuffer br=new StringBuffer();
		StringBuffer column=new StringBuffer();
		StringBuffer value=new StringBuffer();
		br.append("INSERT INTO "+tableName+" (");
		int i=0;
		for(Object key:columnKey_columnValue.keySet()){
			String columnName=(String)key;
			if(i==0)
			{
				column.append(columnName);	
				value.append("?");
			}else{
				column.append(","+columnName);	
				value.append(",?");
			}
			++i;
		}
		String query=br.append(column.toString()+") VALUES("+value.toString()+")").toString();
		System.out.println(query);
		PreparedStatement ps = connection.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
		i=1;
		for(Object key:columnKey_columnValue.keySet()){
			setValue(columnKey_columnValue.get(key),ps,i);
			++i;
		}
		ps.execute();
		ResultSet generatedKeys = ps.getGeneratedKeys();
		if (generatedKeys.next())
			return generatedKeys.getString(1);
		}else
			throw new SQLQueryCreateException("Please check you input parameter");
		
		return null;
	}
	public synchronized static void setValue(Object obj,PreparedStatement ps,int counter)throws SQLException{
		if(obj!=null){
		if(obj instanceof String){
			if(obj!=null)
				ps.setString(counter,((String)obj).toString());
		}else if(obj instanceof Integer){
			if(obj!=null)
				ps.setInt(counter, ((Integer)obj));
			}else if(obj instanceof Long){
				if(obj!=null)
					ps.setLong(counter, ((Long)obj));
				}else if(obj instanceof Boolean){
					if(obj!=null)
						ps.setBoolean(counter, ((Boolean)obj));
					}else if(obj instanceof Double){
						if(obj!=null)
							ps.setDouble(counter, ((Double)obj));
						}else if(obj instanceof Float){
							if(obj!=null)
								ps.setFloat(counter, ((Float)obj));
							}else if(obj instanceof Date){
								if(obj!=null)
									ps.setString(counter,new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(((Date)obj).getTime()));
								}
		}
	}
}
