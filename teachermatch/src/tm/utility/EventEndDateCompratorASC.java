package tm.utility;

import java.util.Comparator;

import tm.bean.EventDetails;

public class EventEndDateCompratorASC implements Comparator<EventDetails> {

	public int compare(EventDetails o1, EventDetails o2) {
		
		if(o1.getEventEndDate()== null)
			if(o2.getEventEndDate()== null)
				 return 0; //equal
			else
				 return -1; //null is before other strings
		
	       else // this.member != null
	    	   if(o2.getEventEndDate()== null)
	            return 1;  // all other strings are after null
	         else
	        	 return o1.getEventEndDate().compareTo(o2.getEventEndDate());
	
	}
}
