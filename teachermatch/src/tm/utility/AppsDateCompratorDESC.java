package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;

public class AppsDateCompratorDESC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		return o2.getAppsDate().compareTo(o1.getAppsDate());
	}
}
