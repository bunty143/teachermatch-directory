package tm.utility;

import java.util.Comparator;

import tm.bean.TeacherStatusHistoryForJob;

public class RequisitionNumberCompratorforTeacherStatusHistoryASC implements Comparator<TeacherStatusHistoryForJob> {

	public int compare(TeacherStatusHistoryForJob o1, TeacherStatusHistoryForJob o2) {
		return o1.getRequisitionNumber().toUpperCase().compareTo(o2.getRequisitionNumber().toUpperCase());
	}
}
