package tm.dao.mq;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.WorkFlowStatus;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.mq.MQEvent;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class MQEventDAO extends GenericHibernateDAO<MQEvent, Integer> {
	
	String locale = Utility.getValueOfPropByKey("locale");

	public MQEventDAO() 
	{
		super(MQEvent.class);
	}
	
	@Transactional(readOnly=true)
	public List<MQEvent> findEventByTeacherJobStatus(TeacherDetail teacherDetail , JobOrder jobOrder , StatusMaster statusMaster)
	{
		List <MQEvent> lstMQEvent = null;
		try {
			Criterion criterion1 = Restrictions.eq("teacherdetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion3 = Restrictions.eq("statusMaster",statusMaster);
			lstMQEvent = findByCriteria(criterion1,criterion2,criterion3);
		} catch (Exception e) {
			e.printStackTrace();
			lstMQEvent = new ArrayList<MQEvent>();
		}
        return lstMQEvent;
	}
	
	@Transactional(readOnly=true)
	public MQEvent findNodeStatusByTeacherJobStatus(TeacherDetail teacherDetail , JobOrder jobOrder , StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		System.out.println(":::::::::::::::::::findNodeStatusByTeacherJobStatus >::::::::::::::::");
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		MQEvent mqEvent=null;
		try {
			Criterion criterion1 = Restrictions.eq("teacherdetail",teacherDetail);
			//Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
			
			Criterion criterion3 =null;
			if(statusMaster!=null){
				criterion3 =Restrictions.eq("statusMaster",statusMaster);
			}else if(secondaryStatus!=null){
				criterion3 =Restrictions.eq("secondaryStatus",secondaryStatus);
			}
			if(criterion3!=null){
				lstMQEvent = findByCriteria(Order.desc("eventId"),criterion1,criterion3);
			}else{
				lstMQEvent = findByCriteria(Order.desc("eventId"),criterion1);
			}
			if(lstMQEvent.size()>0){
				System.out.println("Inside Check:::::::::");
				mqEvent=lstMQEvent.get(0);
			}
			System.out.println("Before Event Check");
		} catch (Exception e) {
			e.printStackTrace();
		}
        return mqEvent;
	}
	
	@Transactional(readOnly=true)
	public MQEvent findNodeStatusByTeacherJobStatus(TeacherDetail teacherDetail , StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		System.out.println(":::::::::::::::::::findNodeStatusByTeacherJobStatus >::::::::::::::::");
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		MQEvent mqEvent=null;
		try {
			Criterion criterion1 = Restrictions.eq("teacherdetail",teacherDetail);
			
			Criterion criterion3 =null;
			if(statusMaster!=null){
				criterion3 =Restrictions.eq("statusMaster",statusMaster);
			}else if(secondaryStatus!=null){
				criterion3 =Restrictions.eq("secondaryStatus",secondaryStatus);
			}
			if(criterion3!=null){
				lstMQEvent = findByCriteria(Order.desc("eventId"),criterion1,criterion3);
			}else{
				lstMQEvent = findByCriteria(Order.desc("eventId"),criterion1);
			}
			if(lstMQEvent.size()>0){
				System.out.println("Inside Check:::::::::");
				mqEvent=lstMQEvent.get(0);
			}
			System.out.println("Before Event Check");
		} catch (Exception e) {
			e.printStackTrace();
		}
        return mqEvent;
	}
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventByTeacherJobStatus(TeacherDetail teacherDetail , JobOrder jobOrder)
	{
		System.out.println(":::::::::::::::::::findMQEventByTeacherJobStatus >::::::::::::::::");
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criterion1 = Restrictions.eq("teacherdetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
			lstMQEvent = findByCriteria(Order.asc("eventId"),criterion1,criterion2);
			System.out.println("Before Event Check");
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	@Transactional(readOnly=true)
	public List<MQEvent> findDisconnectMQEventByTeacher(TeacherDetail teacherDetail)
	{
		System.out.println(":::::::::::::::::::findDisconnectMQEventByTeacher >::::::::::::::::");
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criterion1 = Restrictions.eq("teacherdetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status","R");
			lstMQEvent = findByCriteria(Order.asc("eventId"),criterion1,criterion2);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	@Transactional(readOnly=true)
	public MQEvent findEventByEventTypeAndTeacher(TeacherDetail teacherDetail ,String eventType,String status)
	{
		System.out.println(":::::::::::::::::::findNodeStatusByTeacherJobStatus >::::::::::::::::");
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		MQEvent mqEvent=null;
		   try {
			 	Criterion criterion1  = Restrictions.eq("teacherdetail",teacherDetail);
		        Criterion criterion2  = Restrictions.eq("status",status);
		        Criterion criterion3  = Restrictions.eq("eventType",eventType);
		        lstMQEvent = findByCriteria(Order.desc("eventId"),criterion1,criterion2,criterion3);
				if(lstMQEvent.size()>0){
					System.out.println("Inside Check:::::::::");
					mqEvent=lstMQEvent.get(0);
				}
				System.out.println("Before Event Check");
		} catch (Exception e) {
			e.printStackTrace();
		}
        return mqEvent;
	}
	
	@Transactional(readOnly=true)
	public List<MQEvent> findLinkToKSNByTeacher(TeacherDetail teacherDetail)
	{
		System.out.println(":::::::::::::::::::findLinkToKSNByTeacher >>::::::::::::::::");
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criterion1 = Restrictions.eq("teacherdetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status","R");
			lstMQEvent = findByCriteria(Order.asc("eventId"),criterion1,criterion2);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	@Transactional(readOnly=true)
	public List<MQEvent> findLinkToKSNByTeacherList(List<TeacherDetail> teacherDetail)
	{
		System.out.println(":::::::::::::::::::findLinkToKSNByTeacher >>::::::::::::::::");
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criterion1 = Restrictions.in("teacherdetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status","R");
			lstMQEvent = findByCriteria(Order.asc("eventId"),criterion1,criterion2);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	// end here ..
	
	
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventByTeacherList(List<TeacherDetail> teacherDetail)
	{
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criterion1 = Restrictions.in("teacherdetail",teacherDetail);
			lstMQEvent = findByCriteria(Order.asc("eventId"),criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventByTeacher(TeacherDetail teacherDetail)
	{
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criterion1 = Restrictions.eq("teacherdetail",teacherDetail);
			lstMQEvent = findByCriteria(Order.asc("eventId"),criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventExceptLTKNdDiss(TeacherDetail teacherDetail)
	{
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criteria1	 =	Restrictions.eq("teacherdetail",teacherDetail);
		    Criterion criteria2	 =	Restrictions.ne("eventType",Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
		    Criterion criteria3	 =	Restrictions.ne("eventType",Utility.getLocaleValuePropByKey("msgDisconnectTalent", locale));
			lstMQEvent = findByCriteria(Order.asc("eventId"),criteria1,criteria2,criteria3);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventForWFStatusByTeacher(TeacherDetail teacherDetail)
	{
		String LinkToKsn = Utility.getLocaleValuePropByKey("lblLinkToKsn", locale);
		String INVWF1 = Utility.getLocaleValuePropByKey("lblINVWF1", locale);
		String WF1COM = Utility.getLocaleValuePropByKey("lblWF1COM", locale);
		String INVWF2 = Utility.getLocaleValuePropByKey("lblINVWF2", locale);
		String WF2COM = Utility.getLocaleValuePropByKey("lblWF2COM", locale);
		String INVWF3 = Utility.getLocaleValuePropByKey("lblINVWF3", locale);
		String WF3COM = Utility.getLocaleValuePropByKey("lblWF3COM", locale);
		List<String> stausesInMqEvent = new ArrayList<String>();
		
		if(LinkToKsn!=null)
			stausesInMqEvent.add(LinkToKsn);
		if(INVWF1!=null)
			stausesInMqEvent.add(INVWF1);
		if(WF1COM!=null)
			stausesInMqEvent.add(WF1COM);
		if(INVWF2!=null)
			stausesInMqEvent.add(INVWF2);
		if(WF2COM!=null)
			stausesInMqEvent.add(WF2COM);
		if(INVWF3!=null)
			stausesInMqEvent.add(INVWF3);
		if(WF3COM!=null)
			stausesInMqEvent.add(WF3COM);
		
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criteria1	 =	Restrictions.eq("teacherdetail",teacherDetail);
		    Criterion criteria2	 =	Restrictions.in("eventType",stausesInMqEvent);
			lstMQEvent = findByCriteria(criteria1,criteria2);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}

	@Transactional(readOnly=true)
	public List<MQEvent> findWFStatusByTeacherList(List<TeacherDetail> teacherDetail,WorkFlowStatus wrkflow)
	{ 
		String LinkToKsn = Utility.getLocaleValuePropByKey("lblLinkToKsn", locale);
		String INVWF1 = Utility.getLocaleValuePropByKey("lblINVWF1", locale);
		String INVWF2 = Utility.getLocaleValuePropByKey("lblINVWF2", locale);
		String INVWF3 = Utility.getLocaleValuePropByKey("lblINVWF3", locale);
		List<String> stausesInMqEvent = new ArrayList<String>();
		
		if(LinkToKsn!=null)
			stausesInMqEvent.add(LinkToKsn);
		if(INVWF1!=null)
			stausesInMqEvent.add(INVWF1);
		if(INVWF2!=null)
			stausesInMqEvent.add(INVWF2);
		if(INVWF3!=null)
			stausesInMqEvent.add(INVWF3);		
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criteria1	 =	Restrictions.in("teacherdetail",teacherDetail);
		    Criterion criteria2	 =	Restrictions.in("eventType",stausesInMqEvent);
		    Criterion criteria3	 =	Restrictions.eq("workFlowStatusId",wrkflow);
			lstMQEvent = findByCriteria(criteria1,criteria2,criteria3);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}

	@Transactional(readOnly=true)
	public MQEvent findNodeStatusByTeacher(TeacherDetail teacherDetail , StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		System.out.println(":::::::::::::::::::findNodeStatusByTeacher >::::::::::::::::");
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		MQEvent mqEvent=null;
		try {
			Criterion criterion1 = Restrictions.eq("teacherdetail",teacherDetail);
			
			Criterion criterion3 =null;
			if(statusMaster!=null){
				criterion3 =Restrictions.eq("statusMaster",statusMaster);
			}else if(secondaryStatus!=null){
				criterion3 =Restrictions.eq("secondaryStatus",secondaryStatus);
			}
			if(criterion3!=null){
				lstMQEvent = findByCriteria(Order.desc("eventId"),criterion1,criterion3);
			}else{
				lstMQEvent = findByCriteria(Order.desc("eventId"),criterion1);
			}
			if(lstMQEvent.size()>0){
				mqEvent=lstMQEvent.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        return mqEvent;
	}
	
	
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventByTeacherWithoutWF(TeacherDetail teacherDetail,List<WorkFlowStatus> workFlowStatuses)
	{
		String INVWF1 = Utility.getLocaleValuePropByKey("lblINVWF1", locale);
		String WF1COM = Utility.getLocaleValuePropByKey("lblWF1COM", locale);
		String INVWF2 = Utility.getLocaleValuePropByKey("lblINVWF2", locale);
		String WF2COM = Utility.getLocaleValuePropByKey("lblWF2COM", locale);
		String INVWF3 = Utility.getLocaleValuePropByKey("lblINVWF3", locale);
		String WF3COM = Utility.getLocaleValuePropByKey("lblWF3COM", locale);
		List<String> stausesInMqEvent = new ArrayList<String>();
		
		if(INVWF1!=null)
			stausesInMqEvent.add(INVWF1);
		if(WF1COM!=null)
			stausesInMqEvent.add(WF1COM);
		if(INVWF2!=null)
			stausesInMqEvent.add(INVWF2);
		if(WF2COM!=null)
			stausesInMqEvent.add(WF2COM);
		if(INVWF3!=null)
			stausesInMqEvent.add(INVWF3);
		if(WF3COM!=null)
			stausesInMqEvent.add(WF3COM);
		
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criteria1	 =	Restrictions.eq("teacherdetail",teacherDetail);
		    Criterion criteria2	 =	Restrictions.in("eventType",stausesInMqEvent);
		    Criterion criterionWF = Restrictions.in("workFlowStatusId", workFlowStatuses);
		    Criterion criteria3  =	Restrictions.or(Restrictions.isNull("workFlowStatusId"), Restrictions.not(criterionWF));
		    if(workFlowStatuses!=null && workFlowStatuses.size()>0)
		    	lstMQEvent = findByCriteria(criteria1,criteria2,criteria3);
		    else
		    	lstMQEvent = findByCriteria(criteria1,criteria2);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	@Transactional(readOnly=true)
	public MQEvent findMQEventSSO(TeacherDetail teacherDetail)
	{
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criteria1	 =	Restrictions.eq("teacherdetail",teacherDetail);
		    Criterion criteria2	 =	Restrictions.eq("eventType",Utility.getLocaleValuePropByKey("lbleRegSSO", locale));
			lstMQEvent = findByCriteria(Order.asc("eventId"),criteria1,criteria2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(lstMQEvent.size()>0)
			return lstMQEvent.get(0);
		else
			return null;
	}
	
	@Transactional(readOnly=true)
	public List<MQEvent> findEventByEventType(TeacherDetail teacherDetail ,String eventType)
	{
		System.out.println(":::::::::::::::::::findEventByEventType >::::::::::::::::"+teacherDetail);
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		   try {
			 	Criterion criterion1  = Restrictions.eq("teacherdetail",teacherDetail);
		        Criterion criterion3  = Restrictions.eq("eventType",eventType);
		        lstMQEvent = findByCriteria(Order.desc("eventId"),criterion1,criterion3);
				System.out.println("Before Event Check");
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	@Transactional(readOnly=true)
	public List<MQEvent> findEventByEventTypeList(List<TeacherDetail> teacherDetail ,String eventType)
	{
		System.out.println(":::::::::::::::::::findEventByEventTypeList::::::::::::::::");
		List <MQEvent> lstMQEventList =new ArrayList<MQEvent>();
		   try {
			 	Criterion criterion1  = Restrictions.in("teacherdetail",teacherDetail);
		        Criterion criterion3  = Restrictions.eq("eventType",eventType);
		        lstMQEventList = findByCriteria(Order.desc("eventId"),criterion1,criterion3);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEventList;
	}
	@Transactional(readOnly=true)
	public List<MQEvent> findEventTypeStatus(Integer actionSearchId)
	{ 
		String LinkToKsn = Utility.getLocaleValuePropByKey("lblLinkToKsn", locale);
		String INVWF1 = Utility.getLocaleValuePropByKey("lblINVWF1", locale);
		String INVWF2 = Utility.getLocaleValuePropByKey("lblINVWF2", locale);
		List<String> stausesInMqEvent = new ArrayList<String>();
		
		if(LinkToKsn!=null && actionSearchId==1)
			stausesInMqEvent.add(LinkToKsn);
		if(INVWF1!=null && (actionSearchId==1 || actionSearchId==3 || actionSearchId==4))
			stausesInMqEvent.add(INVWF1);
		if(INVWF2!=null && (actionSearchId==4 || actionSearchId==5))
			stausesInMqEvent.add(INVWF2);		
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
		    Criterion criteria1	 =	Restrictions.in("eventType",stausesInMqEvent);
			lstMQEvent = findByCriteria(criteria1);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventByTeacherListt(List<TeacherDetail> teacherDetail)
	{
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criterion1 = Restrictions.in("teacherdetail",teacherDetail);
			lstMQEvent = findByCriteria(Order.asc("eventId"),criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	
	
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventExceptWorkFlowId(TeacherDetail teacherDetail)
	{
		
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criteria1	 =	Restrictions.eq("teacherdetail",teacherDetail);
		    Criterion criteria2	 =	Restrictions.isNotNull("workFlowStatusId");
		    lstMQEvent = findByCriteria(Order.asc("eventId"),criteria1,criteria2);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventForWorkFlowsByNode(TeacherDetail teacherDetail,String nodeName)
	{
		
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			String INVWF1 = Utility.getLocaleValuePropByKey("lblINVWF1", locale);
			String WF1COM = Utility.getLocaleValuePropByKey("lblWF1COM", locale);
			String INVWF2 = Utility.getLocaleValuePropByKey("lblINVWF2", locale);
			String WF2COM = Utility.getLocaleValuePropByKey("lblWF2COM", locale);
			String INVWF3 = Utility.getLocaleValuePropByKey("lblINVWF3", locale);
			String WF3COM = Utility.getLocaleValuePropByKey("lblWF3COM", locale);
			Criterion criteria2 = null;
			Criterion criteria1	 =	Restrictions.eq("teacherdetail",teacherDetail);
			criteria.add(criteria1);
			
			if(nodeName.equalsIgnoreCase(INVWF2)){
				criteria.add(Restrictions.eq("eventType",INVWF1));
				criteria.add(Restrictions.eq("status","C"));
				criteria.add(Restrictions.eq("eventType",WF1COM));
				criteria.add(Restrictions.eq("status","C"));
			}
			else if(nodeName.equalsIgnoreCase(INVWF3)){
				criteria.add(Restrictions.eq("eventType",INVWF1));
				criteria.add(Restrictions.eq("status","C"));
				criteria.add(Restrictions.eq("eventType",WF1COM));
				criteria.add(Restrictions.eq("status","C"));
			}
			
		    lstMQEvent = findByCriteria(Order.asc("eventId"),criteria1,criteria2);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	@Transactional(readOnly=true)
	public MQEvent findLinkToKSNIniatedByTeacher(TeacherDetail teacherDetail)
	{
		MQEvent mqEvent = null;
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criteria1	 =	Restrictions.eq("teacherdetail",teacherDetail);
			Criterion criteria2	 =	Restrictions.eq("eventType",Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
			
			criteria.add(criteria1);
			criteria.add(criteria2);
			criteria.setProjection(Projections.projectionList()
			.add(Projections.property("status"))
			.add(Projections.property("ackStatus")));
			
			List<String[]> lstMQEvent = new ArrayList<String[]>();
		    lstMQEvent = criteria.list();
		    if(lstMQEvent!=null && lstMQEvent.size()>0){
		    	for(Iterator it = lstMQEvent.iterator(); it.hasNext();){
		    		mqEvent =new MQEvent();
		    		Object[] row = (Object[]) it.next(); 
		    		if(row[0]!=null)
		    			mqEvent.setStatus(row[0].toString());
		    		if(row[1]!=null)
		    			mqEvent.setMsgStatus(row[1].toString());
		    	}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return mqEvent;
	}
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventByTeacherList_Op(String teacherDetail)
	{
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass(),"mqe");
			//criteria.add(Property.forName("mqe.teacherdetail.teacherId").in(teacherDetail));
			criteria.add(Restrictions.sqlRestriction("this_.teacherId in("+teacherDetail+")"));
			criteria.createAlias("workFlowStatusId", "workflowStatusId",criteria.LEFT_JOIN)
			.createAlias("statusMaster", "st",criteria.LEFT_JOIN)
			.createAlias("secondaryStatus", "sec",criteria.LEFT_JOIN)
			.setProjection(Projections.projectionList()
			.add(Projections.property("status"))
			.add(Projections.property("eventType"))
			.add(Projections.property("teacherdetail.teacherId"))
			.add(Projections.property("ackStatus"))
			.add(Projections.property("workflowStatusId.workFlowStatusId"))
			.add(Projections.property("workflowStatusId.workFlowStatus"))
			.add(Projections.property("eventId"))
			.add(Projections.property("tmInitiate"))
			.add(Projections.property("st.statusId"))
			.add(Projections.property("sec.secondaryStatusId"))
			);
			
			List<String[]> templstMQEvent = new ArrayList<String[]>();
			templstMQEvent = criteria.list();
		    if(templstMQEvent!=null && templstMQEvent.size()>0){
		    	for(Iterator it = templstMQEvent.iterator(); it.hasNext();){
		    		MQEvent mqEvent =new MQEvent();
		    		Object[] row = (Object[]) it.next(); 
		    		if(row[0]!=null)
		    			mqEvent.setStatus(row[0].toString());
		    		if(row[1]!=null)
		    			mqEvent.setEventType(row[1].toString());
		    		if(row[2]!=null){
		    			TeacherDetail teacDetail = new TeacherDetail();
		    			teacDetail.setTeacherId(Integer.parseInt(row[2].toString()));
		    			mqEvent.setTeacherdetail(teacDetail);
		    		}
		    		if(row[3]!=null)
		    			mqEvent.setAckStatus(row[3].toString());
		    		
		    		if(row[4]!=null){	
		    			WorkFlowStatus workFlowStatus = new WorkFlowStatus();
		    			workFlowStatus.setWorkFlowStatusId(Integer.parseInt(row[4].toString()));
		    			
		    			if(row[5]!=null)
		    				workFlowStatus.setWorkFlowStatus(row[5].toString());
		    			
		    			mqEvent.setWorkFlowStatusId(workFlowStatus);
		    		}
		    		if(row[6]!=null){
		    			mqEvent.setEventId(Integer.parseInt(row[6].toString()));
		    		}
		    		if(row[7]!=null){
		    			if(row[7].toString().equalsIgnoreCase("true"))
		    					mqEvent.setTmInitiate(true);
		    			else
		    				mqEvent.setTmInitiate(false);
		    		}
		    		if(row[8]!=null){
		    			StatusMaster st = new StatusMaster();
		    			st.setStatusId(Integer.parseInt(row[8].toString()));
		    			mqEvent.setStatusMaster(st);
		    		}
		    		if(row[9]!=null){
		    			SecondaryStatus sec = new SecondaryStatus();
		    			sec.setSecondaryStatusId(Integer.parseInt(row[9].toString()));
		    			mqEvent.setSecondaryStatus(sec);
		    		}
		    		lstMQEvent.add(mqEvent);
		    	}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventByTeacherList_Op(List<TeacherDetail> teacherDetail)
	{
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criteria1	 =	Restrictions.in("teacherdetail",teacherDetail);
			criteria.add(criteria1);
			criteria.createAlias("workFlowStatusId", "workflowStatusId",criteria.LEFT_JOIN)
			.createAlias("statusMaster", "st",criteria.LEFT_JOIN)
			.createAlias("secondaryStatus", "sec",criteria.LEFT_JOIN)
			.setProjection(Projections.projectionList()
			.add(Projections.property("status"))
			.add(Projections.property("eventType"))
			.add(Projections.property("teacherdetail.teacherId"))
			.add(Projections.property("ackStatus"))
			.add(Projections.property("workflowStatusId.workFlowStatusId"))
			.add(Projections.property("workflowStatusId.workFlowStatus"))
			.add(Projections.property("eventId"))
			.add(Projections.property("tmInitiate"))
			.add(Projections.property("st.statusId"))
			.add(Projections.property("sec.secondaryStatusId"))
			);
			
			List<String[]> templstMQEvent = new ArrayList<String[]>();
			templstMQEvent = criteria.list();
		    if(templstMQEvent!=null && templstMQEvent.size()>0){
		    	for(Iterator it = templstMQEvent.iterator(); it.hasNext();){
		    		MQEvent mqEvent =new MQEvent();
		    		Object[] row = (Object[]) it.next(); 
		    		if(row[0]!=null)
		    			mqEvent.setStatus(row[0].toString());
		    		if(row[1]!=null)
		    			mqEvent.setEventType(row[1].toString());
		    		if(row[2]!=null){
		    			TeacherDetail teacDetail = new TeacherDetail();
		    			teacDetail.setTeacherId(Integer.parseInt(row[2].toString()));
		    			mqEvent.setTeacherdetail(teacDetail);
		    		}
		    		if(row[3]!=null)
		    			mqEvent.setAckStatus(row[3].toString());
		    		
		    		if(row[4]!=null){	
		    			WorkFlowStatus workFlowStatus = new WorkFlowStatus();
		    			workFlowStatus.setWorkFlowStatusId(Integer.parseInt(row[4].toString()));
		    			
		    			if(row[5]!=null)
		    				workFlowStatus.setWorkFlowStatus(row[5].toString());
		    			
		    			mqEvent.setWorkFlowStatusId(workFlowStatus);
		    		}
		    		if(row[6]!=null){
		    			mqEvent.setEventId(Integer.parseInt(row[6].toString()));
		    		}
		    		if(row[7]!=null){
		    			if(row[7].toString().equalsIgnoreCase("true"))
		    					mqEvent.setTmInitiate(true);
		    			else
		    				mqEvent.setTmInitiate(false);
		    		}
		    		if(row[8]!=null){
		    			StatusMaster st = new StatusMaster();
		    			st.setStatusId(Integer.parseInt(row[8].toString()));
		    			mqEvent.setStatusMaster(st);
		    		}
		    		if(row[9]!=null){
		    			SecondaryStatus sec = new SecondaryStatus();
		    			sec.setSecondaryStatusId(Integer.parseInt(row[9].toString()));
		    			mqEvent.setSecondaryStatus(sec);
		    		}
		    		lstMQEvent.add(mqEvent);
		    	}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventByTeacher_Op(TeacherDetail teacherDetail) throws Exception
	{
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criteria1	 =	Restrictions.eq("teacherdetail",teacherDetail);
			criteria.add(criteria1);
			criteria.createAlias("workFlowStatusId", "workflowStatusId",criteria.LEFT_JOIN)
			.createAlias("statusMaster", "st",criteria.LEFT_JOIN)
			.createAlias("secondaryStatus", "sec",criteria.LEFT_JOIN)
			.setProjection(Projections.projectionList()
			.add(Projections.property("status"))
			.add(Projections.property("eventType"))
			.add(Projections.property("teacherdetail.teacherId"))
			.add(Projections.property("ackStatus"))
			.add(Projections.property("workflowStatusId.workFlowStatusId"))
			.add(Projections.property("workflowStatusId.workFlowStatus"))
			.add(Projections.property("eventId"))
			.add(Projections.property("tmInitiate"))
			.add(Projections.property("st.statusId"))
			.add(Projections.property("sec.secondaryStatusId"))
			);
			
			List<String[]> templstMQEvent = new ArrayList<String[]>();
			templstMQEvent = criteria.list();
		    if(templstMQEvent!=null && templstMQEvent.size()>0){
		    	for(Iterator it = templstMQEvent.iterator(); it.hasNext();){
		    		MQEvent mqEvent =new MQEvent();
		    		Object[] row = (Object[]) it.next(); 
		    		if(row[0]!=null)
		    			mqEvent.setStatus(row[0].toString());
		    		if(row[1]!=null)
		    			mqEvent.setEventType(row[1].toString());
		    		if(row[2]!=null){
		    			TeacherDetail teacDetail = new TeacherDetail();
		    			teacDetail.setTeacherId(Integer.parseInt(row[2].toString()));
		    			mqEvent.setTeacherdetail(teacDetail);
		    		}
		    		if(row[3]!=null)
		    			mqEvent.setAckStatus(row[3].toString());
		    		
		    		if(row[4]!=null){	
		    			WorkFlowStatus workFlowStatus = new WorkFlowStatus();
		    			workFlowStatus.setWorkFlowStatusId(Integer.parseInt(row[4].toString()));
		    			
		    			if(row[5]!=null)
		    				workFlowStatus.setWorkFlowStatus(row[5].toString());
		    			
		    			mqEvent.setWorkFlowStatusId(workFlowStatus);
		    		}
		    		if(row[6]!=null){
		    			mqEvent.setEventId(Integer.parseInt(row[6].toString()));
		    		}
		    		if(row[7]!=null){
		    			if(row[7].toString().equalsIgnoreCase("true"))
		    					mqEvent.setTmInitiate(true);
		    			else
		    				mqEvent.setTmInitiate(false);
		    		}
		    		if(row[8]!=null){
		    			StatusMaster st = new StatusMaster();
		    			st.setStatusId(Integer.parseInt(row[8].toString()));
		    			mqEvent.setStatusMaster(st);
		    		}
		    		if(row[9]!=null){
		    			SecondaryStatus sec = new SecondaryStatus();
		    			sec.setSecondaryStatusId(Integer.parseInt(row[9].toString()));
		    			mqEvent.setSecondaryStatus(sec);
		    		}
		    		lstMQEvent.add(mqEvent);
		    	}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	@Transactional(readOnly=true)
	public List<MQEvent> findNodeStatusByTeacherUndoINF(TeacherDetail teacherDetail ,List<String> statusNameList)
	{
		System.out.println(":::::::::::::::::::findNodeStatusByTeacherJobStatus >::::::::::::::::");
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		MQEvent mqEvent=null;
		try {
			Criterion criterion1 = Restrictions.eq("teacherdetail",teacherDetail);
			Criterion criterion2 = Restrictions.in("eventType",statusNameList);
			lstMQEvent = findByCriteria(Order.desc("eventId"),criterion1,criterion2);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventByTeacher_Op(TeacherDetail teacherDetail,String eventType) throws Exception
	{
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criteria1	 =	Restrictions.eq("teacherdetail",teacherDetail);
			Criterion criteria2	 =	Restrictions.eq("eventType",eventType);
			criteria.add(criteria1);
			criteria.add(criteria2);
			criteria.createAlias("workFlowStatusId", "workflowStatusId",criteria.LEFT_JOIN)
			.createAlias("statusMaster", "st",criteria.LEFT_JOIN)
			.createAlias("secondaryStatus", "sec",criteria.LEFT_JOIN)
			.setProjection(Projections.projectionList()
			.add(Projections.property("status"))
			.add(Projections.property("eventType"))
			.add(Projections.property("teacherdetail.teacherId"))
			.add(Projections.property("ackStatus"))
			.add(Projections.property("workflowStatusId.workFlowStatusId"))
			.add(Projections.property("workflowStatusId.workFlowStatus"))
			.add(Projections.property("eventId"))
			.add(Projections.property("tmInitiate"))
			.add(Projections.property("st.statusId"))
			.add(Projections.property("sec.secondaryStatusId"))
			);
			
			List<String[]> templstMQEvent = new ArrayList<String[]>();
			templstMQEvent = criteria.list();
		    if(templstMQEvent!=null && templstMQEvent.size()>0){
		    	for(Iterator it = templstMQEvent.iterator(); it.hasNext();){
		    		MQEvent mqEvent =new MQEvent();
		    		Object[] row = (Object[]) it.next(); 
		    		if(row[0]!=null)
		    			mqEvent.setStatus(row[0].toString());
		    		if(row[1]!=null)
		    			mqEvent.setEventType(row[1].toString());
		    		if(row[2]!=null){
		    			TeacherDetail teacDetail = new TeacherDetail();
		    			teacDetail.setTeacherId(Integer.parseInt(row[2].toString()));
		    			mqEvent.setTeacherdetail(teacDetail);
		    		}
		    		if(row[3]!=null)
		    			mqEvent.setAckStatus(row[3].toString());
		    		
		    		if(row[4]!=null){	
		    			WorkFlowStatus workFlowStatus = new WorkFlowStatus();
		    			workFlowStatus.setWorkFlowStatusId(Integer.parseInt(row[4].toString()));
		    			
		    			if(row[5]!=null)
		    				workFlowStatus.setWorkFlowStatus(row[5].toString());
		    			
		    			mqEvent.setWorkFlowStatusId(workFlowStatus);
		    		}
		    		if(row[6]!=null){
		    			mqEvent.setEventId(Integer.parseInt(row[6].toString()));
		    		}
		    		if(row[7]!=null){
		    			if(row[7].toString().equalsIgnoreCase("true"))
		    					mqEvent.setTmInitiate(true);
		    			else
		    				mqEvent.setTmInitiate(false);
		    		}
		    		if(row[8]!=null){
		    			StatusMaster st = new StatusMaster();
		    			st.setStatusId(Integer.parseInt(row[8].toString()));
		    			mqEvent.setStatusMaster(st);
		    		}
		    		if(row[9]!=null){
		    			SecondaryStatus sec = new SecondaryStatus();
		    			sec.setSecondaryStatusId(Integer.parseInt(row[9].toString()));
		    			mqEvent.setSecondaryStatus(sec);
		    		}
		    		lstMQEvent.add(mqEvent);
		    	}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventForEventType(TeacherDetail teacherDetail)
	{
		
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criteria1	 =	Restrictions.eq("teacherdetail",teacherDetail);
		    Criterion criteria2	 =	Restrictions.eq("eventType","Talent Status").ignoreCase();
		    lstMQEvent = findByCriteria(criteria1,criteria2);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	@Transactional(readOnly=true)
	public List<MQEvent> findMQEventByTeacherList_Op(List<TeacherDetail> teacherDetail, String statusNode)
	{
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criteria1	 =	Restrictions.in("teacherdetail",teacherDetail);
			Criterion criteria2	 =	Restrictions.eq("eventType",statusNode);
			criteria.add(criteria1);
			if(statusNode!=null)
			criteria.add(criteria2);
			
			criteria.createAlias("workFlowStatusId", "workflowStatusId",criteria.LEFT_JOIN)
			.createAlias("statusMaster", "st",criteria.LEFT_JOIN)
			.createAlias("secondaryStatus", "sec",criteria.LEFT_JOIN)
			.setProjection(Projections.projectionList()
			.add(Projections.property("status"))
			.add(Projections.property("eventType"))
			.add(Projections.property("teacherdetail.teacherId"))
			.add(Projections.property("ackStatus"))
			.add(Projections.property("workflowStatusId.workFlowStatusId"))
			.add(Projections.property("workflowStatusId.workFlowStatus"))
			.add(Projections.property("eventId"))
			.add(Projections.property("tmInitiate"))
			.add(Projections.property("st.statusId"))
			.add(Projections.property("sec.secondaryStatusId"))
			);
			
			List<String[]> templstMQEvent = new ArrayList<String[]>();
			templstMQEvent = criteria.list();
		    if(templstMQEvent!=null && templstMQEvent.size()>0){
		    	for(Iterator it = templstMQEvent.iterator(); it.hasNext();){
		    		MQEvent mqEvent =new MQEvent();
		    		Object[] row = (Object[]) it.next(); 
		    		if(row[0]!=null)
		    			mqEvent.setStatus(row[0].toString());
		    		if(row[1]!=null)
		    			mqEvent.setEventType(row[1].toString());
		    		if(row[2]!=null){
		    			TeacherDetail teacDetail = new TeacherDetail();
		    			teacDetail.setTeacherId(Integer.parseInt(row[2].toString()));
		    			mqEvent.setTeacherdetail(teacDetail);
		    		}
		    		if(row[3]!=null)
		    			mqEvent.setAckStatus(row[3].toString());
		    		
		    		if(row[4]!=null){	
		    			WorkFlowStatus workFlowStatus = new WorkFlowStatus();
		    			workFlowStatus.setWorkFlowStatusId(Integer.parseInt(row[4].toString()));
		    			
		    			if(row[5]!=null)
		    				workFlowStatus.setWorkFlowStatus(row[5].toString());
		    			
		    			mqEvent.setWorkFlowStatusId(workFlowStatus);
		    		}
		    		if(row[6]!=null){
		    			mqEvent.setEventId(Integer.parseInt(row[6].toString()));
		    		}
		    		if(row[7]!=null){
		    			if(row[7].toString().equalsIgnoreCase("true"))
		    					mqEvent.setTmInitiate(true);
		    			else
		    				mqEvent.setTmInitiate(false);
		    		}
		    		if(row[8]!=null){
		    			StatusMaster st = new StatusMaster();
		    			st.setStatusId(Integer.parseInt(row[8].toString()));
		    			mqEvent.setStatusMaster(st);
		    		}
		    		if(row[9]!=null){
		    			SecondaryStatus sec = new SecondaryStatus();
		    			sec.setSecondaryStatusId(Integer.parseInt(row[9].toString()));
		    			mqEvent.setSecondaryStatus(sec);
		    		}
		    		lstMQEvent.add(mqEvent);
		    	}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	@Transactional(readOnly=true)
	public List<MQEvent> findExistStatusByTeacherListAndNode_Op(List<TeacherDetail> teacherDetail,String statusNode)
	{
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criteria1	 =	Restrictions.in("teacherdetail",teacherDetail);
			Criterion criteria2	 =	Restrictions.eq("eventType",statusNode);
			Criterion criteria3	 =	Restrictions.eq("status","C");
			Criterion criteria4 = Restrictions.or(Restrictions.isNull("ackStatus"),Restrictions.eq("ackStatus", "Received"));
			Criterion criteria5 = Restrictions.or(Restrictions.eq("ackStatus", "Success"), Restrictions.eq("ackStatus", "Successful"));
			Criterion criteria6 = Restrictions.or(criteria4, criteria5);
			Criterion criteria7 = Restrictions.and(Restrictions.eq("status","R"), criteria6);
			Criterion criteria8 = Restrictions.or(criteria3, criteria7);
			criteria.add(criteria1);
			criteria.add(criteria2);
			criteria.add(criteria8);
			criteria.setProjection(Projections.projectionList()
			.add(Projections.property("status"))
			.add(Projections.property("eventId"))
			);
			
			List<String[]> templstMQEvent = new ArrayList<String[]>();
			templstMQEvent = criteria.list();
		    if(templstMQEvent!=null && templstMQEvent.size()>0){
		    	for(Iterator it = templstMQEvent.iterator(); it.hasNext();){
		    		MQEvent mqEvent =new MQEvent();
		    		Object[] row = (Object[]) it.next(); 
		    		if(row[0]!=null)
		    			mqEvent.setStatus(row[0].toString());
		    		if(row[1]!=null){
		    			mqEvent.setEventId(Integer.parseInt(row[1].toString()));
		    		}
		    		lstMQEvent.add(mqEvent);
		    	}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	@Transactional(readOnly=true)
	public List<MQEvent> findMQeacherList(List<TeacherDetail> teacherDetail)
	{ 
		String LinkToKsn = Utility.getLocaleValuePropByKey("lblLinkToKsn", locale);
		String INVWF1 = Utility.getLocaleValuePropByKey("lblINVWF1", locale);
		String INVWF2 = Utility.getLocaleValuePropByKey("lblINVWF2", locale);
		List<String> stausesInMqEvent = new ArrayList<String>();
		
		if(LinkToKsn!=null)
			stausesInMqEvent.add(LinkToKsn);
		if(INVWF1!=null)
			stausesInMqEvent.add(INVWF1);
		if(INVWF2!=null)
			stausesInMqEvent.add(INVWF2);
		
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criteria1	 =	Restrictions.in("teacherdetail",teacherDetail);
		    Criterion criteria2	 =	Restrictions.in("eventType",stausesInMqEvent);
		    lstMQEvent = findByCriteria(criteria1,criteria2);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
	
	
	@Transactional(readOnly=true)
	public List<MQEvent> findMQTeacherListByNode(List<TeacherDetail> teacherDetail,String nodeName)
	{ 
		List <MQEvent> lstMQEvent =new ArrayList<MQEvent>();
		try {
			Criterion criteria1	 =	Restrictions.in("teacherdetail",teacherDetail);
		    Criterion criteria2	 =	Restrictions.eq("eventType",nodeName);
		    lstMQEvent = findByCriteria(criteria1,criteria2);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstMQEvent;
	}
}
