package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.FinalizeSpecificTemplates;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.dao.generic.GenericHibernateDAO;

public class FinalizeSpecificTemplatesDAO extends GenericHibernateDAO<FinalizeSpecificTemplates, Long>
{
	public FinalizeSpecificTemplatesDAO()
	{
		super(FinalizeSpecificTemplates.class);
	}
	
	@Transactional(readOnly=true)
	public List<FinalizeSpecificTemplates> findByDistrictJobCategoryNodeName(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster, String nodeName, String notifyUser)
	{
		List<FinalizeSpecificTemplates> finalizeSpecificTemplateList = new ArrayList<FinalizeSpecificTemplates>();
		try
		{
			notifyUser = (notifyUser==null || notifyUser.equals(""))? "2" : notifyUser.trim();
			
			Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2 = Restrictions.or(Restrictions.eq("jobCategoryMaster",jobCategoryMaster), Restrictions.isNull("jobCategoryMaster"));
			Criterion criterion3 = Restrictions.eq("nodeName", nodeName);
			Criterion criterion4 = Restrictions.or(Restrictions.eq("notifyUser","2"), Restrictions.eq("notifyUser", notifyUser));
			Criterion criterion5 = Restrictions.eq("status", "A");
			
			
			finalizeSpecificTemplateList = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return finalizeSpecificTemplateList;
	}
}