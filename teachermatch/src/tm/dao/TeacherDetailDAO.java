package tm.dao;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.mapping.Array;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;
public class TeacherDetailDAO  extends GenericHibernateDAO<TeacherDetail, Integer> 
{
	public TeacherDetailDAO() 
	{
		super(TeacherDetail.class);
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<TeacherDetail> checkDuplicateTeacherEmail(String  teacherId,String emailAddress) 
	{
		Session session = getSession();
		if(teacherId!=null && teacherId!="")
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.not(Restrictions.eq("teacherId",Integer.parseInt(teacherId)))) 
			.add(Restrictions.eq("emailAddress", emailAddress)) 
			.list();
			return result;
		}
		else
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.eq("emailAddress", emailAddress)) 
			.list();
			return result;
		}
	}
	@Transactional(readOnly=true)
	public List<TeacherDetail> findByEmail(String emailAddress)
	{
		List <TeacherDetail> lstTeacherDetails = null;
		Criterion criterion = Restrictions.eq("emailAddress",emailAddress);
		lstTeacherDetails = findByCriteria(criterion);  

		return lstTeacherDetails;
	}

	@Transactional(readOnly=true)
	public List<TeacherDetail> getLogin(String emailAddress, String password)
	{
		List <TeacherDetail> lstGindiaUsers = null;
		Criterion criterion1 = Restrictions.eq("emailAddress",emailAddress.trim());
		Criterion criterion2 = Restrictions.eq("password", password);
		Criterion criterion3=  Restrictions.and(criterion1, criterion2);
		lstGindiaUsers= findByCriteria(criterion3);  
		return lstGindiaUsers;
	}

	@Transactional(readOnly=true)
	public List<TeacherDetail> checkTeacherEmail(String emailAddress)
	{
		List <TeacherDetail> lstteacherEmail = null;
		Criterion criterion1 = Restrictions.eq("emailAddress",emailAddress.trim());
		lstteacherEmail= findByCriteria(criterion1);  
		return lstteacherEmail;
	}

	@Transactional(readOnly=true)
	public List<TeacherDetail> getTeacherByKeyAndID(String key, Integer id)
	{
		List <TeacherDetail> lstTeacherDetail= null;
		try 
		{	         
			Criterion criterion1 = Restrictions.eq("teacherId",id);
			Criterion criterion2 = Restrictions.eq("verificationCode",key );
			Criterion criterion3=  Restrictions.and(criterion1, criterion2);
			lstTeacherDetail = findByCriteria(criterion3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstTeacherDetail;
	}
	@Transactional(readOnly=true)
	public List<TeacherDetail> getTeacherByKeyAndIDWithStatus(String key, Integer id)
	{
		List <TeacherDetail> lstTeacherDetail= null;
		try 
		{	         
			Criterion criterion1 = Restrictions.eq("teacherId",id);
			Criterion criterion2 = Restrictions.eq("verificationCode",key );
			Criterion criterion3=  Restrictions.and(criterion1, criterion2);
			Criterion criterion4 = Restrictions.eq("verificationStatus",0);
			lstTeacherDetail = findByCriteria(criterion3,criterion4);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstTeacherDetail;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByJob(int entityID,List<JobOrder> lstJobOrder,Order order, DistrictMaster districtMaster,String firstName,String lastName,String emailAddress)
	{
		List<TeacherDetail> lstTeacherDetail= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
			.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
			.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
			.addOrder(order);
			lstTeacherDetail =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}

	// Created for GetApplicant by epi: Gagan
	@Transactional(readOnly=false)
	public List<TeacherDetail> getApplicantsByEPI(String firstName,String lastName, String emailAddress,Order order,String p,String q,int r)
	{
		List<TeacherDetail> lstTeacherDetail= null;
		try{
			Calendar cal = Calendar.getInstance();

			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			Date dateWithoutTime = cal.getTime();

			Calendar cal1 = Calendar.getInstance();
			cal1.add(Calendar.DATE, -1);
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);


			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("userType",q);
			Criterion criterion2 = Restrictions.eq("verificationStatus",r);
			Criterion criterion3 = Restrictions.le("createdDateTime",dateWithoutTime);
			Criterion criterion4 = Restrictions.ge("createdDateTime",cal1.getTime());
			Criterion criterion5 = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
			Criterion criterion6 = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
			Criterion criterion7 = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);

			Criteria criteria = session.createCriteria(getPersistentClass());

			if(p.equalsIgnoreCase("y"))
			{
				if(r==0)
				{
					//teacherDetails = teacherDetailDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4);

					criteria.add(criterion1)
					.add(criterion2)
					.add(criterion3)
					.add(criterion4)
					.add(criterion5)
					.add(criterion6)
					.add(criterion7)
					.addOrder(order);

					lstTeacherDetail =  criteria.list();
				}else
				{
					//teacherDetails = teacherDetailDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4);
					criteria.add(criterion1)
					.add(criterion2)
					.add(criterion3)
					.add(criterion4)
					.add(criterion5)
					.add(criterion6)
					.add(criterion7)
					.addOrder(order);

					lstTeacherDetail =  criteria.list();
				}

			}
			else
			{
				if(r==0)
				{
					//teacherDetails = teacherDetailDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2);

					criteria.add(criterion1)
					.add(criterion2)
					.add(criterion5)
					.add(criterion6)
					.add(criterion7)
					.addOrder(order);

					lstTeacherDetail =  criteria.list();




				}else
				{
					//teacherDetails = teacherDetailDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2);

					criteria.add(criterion1)
					.add(criterion2)
					.add(criterion5)
					.add(criterion6)
					.add(criterion7)
					.addOrder(order);

					lstTeacherDetail =  criteria.list();
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> getCadidatesByEPI(String fromDate,String toDate,int statusId,String firstName,String lastName, String emailAddress,Order order,int r)
	{
		List<TeacherDetail> lstTeacherDetail= null;
		try{
			Calendar cal = Calendar.getInstance();

			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			Date dateWithoutTime = cal.getTime();

			DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
			Date startDate = df.parse(fromDate);
			Date endDate = df.parse(toDate);

			if(fromDate.equalsIgnoreCase(toDate))
			{
				df = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");
				endDate=df.parse(toDate+" 23:59:59");
			}
			if(endDate.equals(dateWithoutTime))
				endDate = new Date();

			Session session = getSession();
			Criterion criterion1 = null;
			Criterion criterion3 = Restrictions.between("createdDateTime", startDate, endDate);
			Criterion criterion4 = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
			Criterion criterion5 = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
			Criterion criterion6 = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
			Criterion criterion7 = Restrictions.eq("isPortfolioNeeded", true);

			Criteria criteria = session.createCriteria(getPersistentClass());
			if(r!=2)
			{
				criterion1 = Restrictions.eq("verificationStatus",r);
				criteria.add(criterion1)
				.add(criterion3)
				.add(criterion4)
				.add(criterion5)
				.add(criterion6)
				.add(criterion7)
				.addOrder(order);
			}else
			{
				criteria.add(criterion3)
				.add(criterion4)
				.add(criterion5)
				.add(criterion6)
				.add(criterion7)
				.addOrder(order);
			}
			lstTeacherDetail =  criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findAllTeacherDetails(List emails){
		List<TeacherDetail> TeacherDetailList= null;
		try 
		{
			Criterion criterion = Restrictions.eq("status","A");
			Criterion criterion1 = Restrictions.in("emailAddress",emails);	
			TeacherDetailList =	findByCriteria(criterion,criterion1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return TeacherDetailList;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findAllTeacherStatusWisePortfolioNeeded(String status,int auth,boolean datacheck){
		List<TeacherDetail> TeacherDetailList= null;

		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		try 
		{
			Criterion criterion = Restrictions.eq("status","A");
			Criterion criterion1 = Restrictions.eq("verificationStatus",auth);	
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion3 = Restrictions.eq("isPortfolioNeeded",true);
			if(datacheck)
				TeacherDetailList =	findByCriteria(criterion,criterion1,criterion2,criterion3);
			else
				TeacherDetailList =	findByCriteria(criterion,criterion1,criterion3);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return TeacherDetailList;
	}

	@Transactional(readOnly=false)
	public void deleteFromTeacherDetails(List<TeacherDetail> teacherDetails){
		try {
			String hql = "delete from TeacherDetail WHERE teacherId IN (:teacherDetails)";
			Query query = null;
			Session session = getSession();

			query = session.createQuery(hql);
			query.setParameterList("teacherId", teacherDetails);
			System.out.println("  OOO  "+query.executeUpdate());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: .
	 */
	@Transactional(readOnly=true)
	public List<TeacherDetail> getRNDTeachers()
	{
		List<TeacherDetail> teacherDetails = null;

		Criterion criterion = Restrictions.sqlRestriction("1=1 order by rand()");
		teacherDetails = findByCriteria(criterion);

		return teacherDetails;	
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findIcompTeacherDetails(List teacherIds){
		List<TeacherDetail> TeacherDetailList= null;
		try 
		{
			Criterion criterion1 = Restrictions.not(Restrictions.in("teacherId",teacherIds));	
			TeacherDetailList =	findByCriteria(criterion1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return TeacherDetailList;
	}

	/*@Transactional(readOnly=false)
	public List<TeacherDetail> findTeacherListByNormScore(){
		List<TeacherDetail> teacherDetailList= new ArrayList<TeacherDetail>();
		System.out.println("findTeacherListByNormScore=:::::::::::::");
		try {// vishwanath
			//String hql = "from TeacherNormScore tns ,TeacherDetail td where tns.teacherDetail =1";
			String hql = "from TeacherDetail td,TeacherNormScore tns where tns.teacherDetail =1";
			Query query = null;
			Session session = getSession();
			query = session.createQuery(hql);
			//query.setParameterList("lstJobOrder", lstJobOrder);
			//query.setParameter("status", statusMaster);
			teacherDetailList = query.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return teacherDetailList;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findByTeacher()
	{
		List<TeacherDetail> teacherDetailList= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.createCriteria("teacherDetail",CriteriaSpecification.FULL_JOIN);
			teacherDetailList =criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherDetailList;
	}*/

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<TeacherDetail> getTeachersHQL(Integer techersList[]) 
	{
		try {
			Session session = getSession();

			Query q = session.createQuery("SELECT teacherDetail FROM TeacherDetail teacherDetail WHERE teacherId IN (:names)");
			q.setParameterList("names", techersList);

			return q.list();

		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	@Transactional(readOnly=false)
	public List<TeacherDetail> findByTeacherFirstName(String firstName)
	{

		List<TeacherDetail> lstteacherDetails = new ArrayList<TeacherDetail>();
		try 
		{Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		criteria.add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE));
		lstteacherDetails =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		/*if(lstteacherDetails==null || lstteacherDetails.size()==0)
			return null;
		else*/
		return lstteacherDetails;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findByTeacherLastName(String lastName)
	{

		List<TeacherDetail> lstteacherDetails = new ArrayList<TeacherDetail>();
		try 
		{
			System.out.println("Dao Last Name :: "+lastName);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE));
			lstteacherDetails =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		/*if(lstteacherDetails==null || lstteacherDetails.size()==0)
			return null;
		else*/
		return lstteacherDetails;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findByTeacherEmailAddress(String emailAddress)
	{

		List<TeacherDetail> lstteacherDetails = new ArrayList<TeacherDetail>();
		try 
		{
			System.out.println("Dao EmailAddresss :: "+emailAddress);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
			lstteacherDetails =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		/*if(lstteacherDetails==null || lstteacherDetails.size()==0)
			return null;
		else*/
		return lstteacherDetails;
	}


	/* ====Start :: ========== For candidates List ====================*/
	@Transactional(readOnly=false)
	public List<TeacherDetail> getAllTeacherList(){
		List<TeacherDetail> TeacherDetailList= null;

		try 
		{
			TeacherDetailList =	findByCriteria(Order.asc("lastName"));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return TeacherDetailList;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeachersByNameAndEmail(List<Criterion> criterians){
		List<TeacherDetail> TeacherDetailList= null;

		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			for(Criterion cr:criterians )
			{
				criteria.add(cr);
			}
			TeacherDetailList =	criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return TeacherDetailList;
	}

	/* ====End========== For candidates List ====================*/

	@Transactional(readOnly=false)
	public List<TeacherDetail> findByTeacherIds(List<Integer> lstTeacherIds){
		List<TeacherDetail> TeacherDetailList= new ArrayList<TeacherDetail>();
		try 
		{
			Criterion criterion =Restrictions.in("teacherId",lstTeacherIds);	
			TeacherDetailList =	findByCriteria(criterion);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return TeacherDetailList;
	}

	// For Active Teacher List
	@Transactional(readOnly=false)
	public List<TeacherDetail> getActiveTeacherList(List<Integer> lstTeacherIds){
		List<TeacherDetail> TeacherDetailList= new ArrayList<TeacherDetail>();
		try 
		{
			Criterion criterion =Restrictions.in("teacherId",lstTeacherIds);	
			Criterion criterion1 =Restrictions.eq("status","A");	
			TeacherDetailList =	findByCriteria(criterion,criterion1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return TeacherDetailList;
	}



	@Transactional(readOnly=false)
	public List<TeacherDetail> findByTeacherDetails(String tfname,String tlname,String temail,String tid)
	{

		List<TeacherDetail> lstteacherDetails = new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.setMaxResults(100);
			if(tfname!=null){
				criteria.add(Restrictions.like("firstName", tfname.trim(),MatchMode.ANYWHERE));
			}
			if(tlname!=null){
				criteria.add(Restrictions.like("lastName", tlname.trim(),MatchMode.ANYWHERE));
			}
			if(temail!=null){
				criteria.add(Restrictions.like("emailAddress", temail.trim(),MatchMode.START));
			}
			try{
				if(tid!=null){
					criteria.add(Restrictions.eq("teacherId",Integer.parseInt(tid)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			lstteacherDetails =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstteacherDetails;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findAvailableCandidteNotApplYAnyJOb(Order order,int startPos,int limit,List<Integer> lstTeacherIds,Criterion firstlastEmailCri)
	{
		List<TeacherDetail> lstTeacher= new ArrayList<TeacherDetail>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			if(firstlastEmailCri!=null ){
				//for(Criterion cri: firstlastEmailCri)
				criteria.add(firstlastEmailCri);
			}
			Criterion criterion = Restrictions.not(Restrictions.in("teacherId", lstTeacherIds));
			Criterion cr1 = Restrictions.eq("status", "A");
			criteria.add(criterion);
			criteria.add(cr1);

			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);			
			criteria.addOrder(order);

			lstTeacher = criteria.list();
		} catch (Exception e) {			
		}
		return lstTeacher;

	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findAvailableCandidteNotApplYAnyJObCount(List<Integer> lstTeacherIds,List<Criterion>firstlastEmailCri)
	{
		List<TeacherDetail> lstTeacher= new ArrayList<TeacherDetail>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			if(firstlastEmailCri!=null && firstlastEmailCri.size()>0){
				for(Criterion cri: firstlastEmailCri)
					criteria.add(cri);
			}
			Criterion criterion = Restrictions.not(Restrictions.in("teacherId", lstTeacherIds));
			Criterion cr1 = Restrictions.eq("status", "A");
			criteria.add(criterion);
			criteria.add(cr1);

			lstTeacher = criteria.list();
		} catch (Exception e) {			
		}
		return lstTeacher;

	}

	@Transactional(readOnly=true)
	public List<TeacherDetail> findByEmails(List<String> emailAddress)
	{
		List <TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();
		try {
			Criterion criterion = Restrictions.in("emailAddress",emailAddress);
			lstTeacherDetails = findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}  
		return lstTeacherDetails;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findAllTeacherForKellyDashboard(String teacherFNames,String teacherLNames,String emailId,BigInteger ksnId)
	{
		List<TeacherDetail> lstTeacher= new ArrayList<TeacherDetail>();
		try 
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());


			if((teacherFNames!= null && !teacherFNames.equals(""))&&(teacherLNames!= null && !teacherLNames.equals(""))){
				criteria.add(Restrictions.like("firstName","%"+teacherFNames+"%")).add(Restrictions.like("lastName","%"+teacherLNames+"%"));
			}else if(teacherFNames!= null && !teacherFNames.equals("")){
				criteria.add(Restrictions.like("firstName","%"+teacherFNames+"%"));
			}else if(teacherLNames!= null && !teacherLNames.equals("")){
				criteria.add(Restrictions.like("lastName","%"+teacherLNames+"%"));
			}	
			if(emailId!= null && !emailId.equals("")){
				criteria.add(Restrictions.like("emailAddress","%"+emailId+"%"));
			}
			if(ksnId!= null && !ksnId.equals("")){
				criteria.add(Restrictions.eq("KSNID", ksnId));
			}			
			lstTeacher = criteria.list();
			System.out.println(lstTeacher.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacher;
	}

	//Sandeep 15-10-15	
	@Transactional(readOnly=true)
	public List<TeacherDetail> findByKSN(BigInteger ksnId)
	{
		List <TeacherDetail> lstTeacherDetails = null;
		Criterion criterion = Restrictions.eq("KSNID",ksnId);
		lstTeacherDetails = findByCriteria(criterion);  

		return lstTeacherDetails;
	}

	@Transactional(readOnly=true)
	public List<TeacherDetail> findTeacherByKSN(BigInteger ksnId)
	{
		List <TeacherDetail> lstTeacherDetails = null;
		Criterion criterion = Restrictions.eq("KSNID",ksnId);
		lstTeacherDetails = findByCriteria(criterion);  

		return lstTeacherDetails;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getActiveKellyTeacherList(Set<Integer> lstTeacherIds){
		List<TeacherDetail> TeacherDetailList= new ArrayList<TeacherDetail>();
		try 
		{
			Criterion criterion =Restrictions.in("teacherId",lstTeacherIds);		
			TeacherDetailList =	findByCriteria(criterion);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return TeacherDetailList;
	}


	//Optimization for Candidate Pool

	@Transactional(readOnly=false)
	public List<Integer> findIcompTeacherDetails_Op(List<Integer> teachersId){
		List<Integer> teacherDetailList= null;

		if(teachersId!=null && teachersId.size()>0)
			try 
		{
				Criteria criteria =  getSession().createCriteria(this.getPersistentClass());
				criteria.add(Restrictions.not(Restrictions.in("teacherId",teachersId)));
				criteria.setProjection(Projections.property("teacherId"));
				teacherDetailList =	criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return teacherDetailList;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getAllNotLinkedToKSNTeacherList(){
		List<TeacherDetail> TeacherDetailList= new ArrayList<TeacherDetail>();
		try 
		{
			Criterion criterion =Restrictions.isNull("KSNID");		
			TeacherDetailList =	findByCriteria(criterion);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return TeacherDetailList;
	}

	public JSONArray[] getProspectGrid(String sortingStr,int start, int end,String noOfRow,String firstName,String lastName,String emailAddress,String teacherSSN,String empNmbr,String searchTId){

		JSONArray jsonArray = new JSONArray();
		JSONArray jsonCountArray = new JSONArray();

		JSONArray[] returnJsonArray = new JSONArray[2];
		Connection connection =null;
		int totalCount=0;
		try {		


			String limit="";
			if(Integer.parseInt(noOfRow)!=0){
				limit=" limit "+start+","+end;
			}


			String where="";
			List<String> whereCond=new ArrayList<String>();


			if(!firstName.equalsIgnoreCase("")){			
				whereCond.add(" td.firstName like '%"+firstName+"%'");
			}
			if(!lastName.equalsIgnoreCase("")){
				whereCond.add(" td.lastName like '%"+lastName+"%'");
			}
			if(!emailAddress.equalsIgnoreCase("")){
				whereCond.add(" td.emailAddress like '%"+emailAddress+"%'");
			}
			if(!teacherSSN.equalsIgnoreCase("")){
				whereCond.add(" tpinfo.SSN ='"+emailAddress+"'");
			}
			if(!empNmbr.equalsIgnoreCase("")){
				whereCond.add(" tpinfo.employeeNumber ='"+empNmbr+"'");
			}
			int j=0;
			for (String string : whereCond) {
				if(j==0){
					where="where "+string;
				}else{
					where+=" and "+string;
				}
				j++;
			}

			if(searchTId!=null && !searchTId.equalsIgnoreCase("")){

				if(where.equalsIgnoreCase("")){
					where=" where td.teacherId in ("+searchTId+")";
				}
				else{
					where=where+" and td.teacherId in ("+searchTId+")";
				}
			}
			String sql=" SELECT SQL_CALC_FOUND_ROWS td.teacherId,CONCAT(td.firstName,' ',td.lastName) as teacherName,td.emailAddress,IFNULL(tns.teacherNormScore,'N/A') AS normScore,tns.decileColor,IFNULL(tfa.tfaShortAffiliateName,'N/A') AS tfa,"+
			"CASE  WHEN (tex.expCertTeacherTraining IS NULL OR tex.expCertTeacherTraining=0)  THEN 'N/A' ELSE tex.expCertTeacherTraining END AS teaExp,"+
			"CASE  WHEN (tpinfo.expectedSalary IS NULL OR tpinfo.expectedSalary=0)  THEN 'N/A' ELSE tpinfo.expectedSalary END AS teaExpSal,"+
			"CASE td.status WHEN 'A' THEN 'Active' WHEN 'I' THEN 'Deactivate' END AS status,"+
			"(SELECT CASE WHEN COUNT(*) >0 THEN 1 ELSE 0 END AS timeIcon FROM teacherwiseassessmenttime AS techAssTime WHERE YEAR(techAssTime.createdDateTime) =(YEAR(NOW())) AND techAssTime.assessmentType=1 AND techAssTime.teacherId=td.teacherId ORDER BY techAssTime.assessmentTimeId DESC)  AS timeIcon,"+
			"(SELECT COUNT(*) FROM teacherassessmentstatus teaAss WHERE td.teacherId=teaAss.teacherId AND assessmentType=2) AS jsiCount,"+
			"(SELECT COUNT(*) FROM teacherassessmentstatus teaAss WHERE td.teacherId=teaAss.teacherId AND assessmentType=1) AS epiCount,"+
			"IFNULL(" +
			"(SELECT CASE   WHEN (tchraass.cgUpdated=1 AND tchraass.statusId=4) THEN '1' WHEN tchraass.statusId=9 THEN '2' END AS isBase FROM teacherassessmentstatus AS tchraass WHERE YEAR(tchraass.createdDateTime) =( YEAR(NOW())) AND tchraass.assessmentType=1 AND tchraass.jobId IS NULL AND (tchraass.statusId=4 OR tchraass.statusId=9) AND tchraass.teacherId=td.teacherId  ORDER BY teacherAssessmentStatusId DESC LIMIT 1) " +
			",'') AS isBase,"+
			"(SELECT COUNT(*) as jobApply FROM jobforteacher WHERE teacherId=td.teacherId AND STATUS IN (3,4,6,7,9,10,16,17,18,19,21,22)) as jobApply "+
			" FROM teacherdetail td "+
			"LEFT JOIN teachernormscore AS tns ON tns.teacherNorScoreId=(SELECT tmnscr.teacherNorScoreId FROM `teachernormscore` tmnscr WHERE tmnscr.teacherId=td.teacherId ORDER BY tmnscr.teacherAssessmentId DESC LIMIT 1) "+ 
			"LEFT JOIN `teacherexperience` AS tex ON tex.teacherId=td.teacherId "+
			"LEFT JOIN tfaaffiliatemaster AS tfa ON tex.tfaAffiliateId=tfa.tfaAffiliateId "+
			"LEFT JOIN `teacherpersonalinfo` AS tpinfo ON tpinfo.teacherId=td.teacherId "+where+" "+sortingStr+" "+limit;

			SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) getSessionFactory();
			ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
			connection = connectionProvider.getConnection();
			PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);

			PreparedStatement psCount=connection.prepareStatement("SELECT FOUND_ROWS()",ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);


			System.out.println("sql:::"+sql);

			ResultSet rs=ps.executeQuery();
			ResultSet rsCount=psCount.executeQuery();	
			while(rsCount.next()){
				totalCount=rsCount.getInt(1);
				System.out.println("count ::::"+totalCount);
			}
			rs.last();
			int rowcount=rs.getRow();
			rs.beforeFirst();
			int cnt=0;
			ResultSetMetaData rsmd = rs.getMetaData();
			if(rs.next()){
				do{
					cnt++;  	
					JSONObject jsonRec = new JSONObject();
					for (int i = 1; i <=rsmd.getColumnCount(); i++) {
						String name = rsmd.getColumnName(i);
						//			    		if(cnt==1)
						//			    			System.out.println("name    "+name);
						jsonRec.put(name, rs.getString(name));
					}

					jsonArray.add(jsonRec);
				}while(rs.next());
			}



			JSONObject jsonCount = new JSONObject();
			jsonCount.put("totalCount", totalCount);
			jsonCountArray.add(jsonCount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			if(connection!=null)
				try {connection.close();} catch (SQLException e) {	e.printStackTrace();}
		}

		returnJsonArray[0] = jsonArray;
		returnJsonArray[1] = jsonCountArray;
		return returnJsonArray;
	}	

	@Transactional(readOnly=false)
	public JSONObject getAdminTeacherList(String all){
		
		JSONObject jsonRec = new JSONObject();
		/*
		try {
			Query query = getSession().createSQLQuery(
			"CALL ADMIN_DASHBOARD_NATIVE_REFERAL()");

			result = query.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return result;*/
		
		try {
			
			SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) getSessionFactory();
			ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
			Connection con = connectionProvider.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("{call ADMIN_DASHBOARD_NATIVE_REFERAL}");
            while (rs.next()) {
           /* System.out.println("total"+" "+ rs.getString("total"));
            System.out.println("native_teacher"+" "+ rs.getString("native_teacher"));
            System.out.println("authentication_native"+" "+ rs.getString("authentication_native"));
            System.out.println("emp_comp_native"+" "+ rs.getString("emp_comp_native"));
            System.out.println("emp_comp_native_apply"+" "+ rs.getString("emp_comp_native_apply"));
            System.out.println("no_apply_anyJob_native"+" "+ rs.getString("no_apply_anyJob_native"));
            System.out.println("emp_Incomp_native"+" "+ rs.getString("emp_Incomp_native"));
            System.out.println("time_out_native"+" "+ rs.getString("time_out_native"));
            System.out.println("unauthentication_native"+" "+ rs.getString("unauthentication_native"));
            System.out.println("referal_teacher"+" "+ rs.getString("referal_teacher"));
            System.out.println("authentication_referal"+" "+ rs.getString("authentication_referal"));
            System.out.println("emp_comp_referal"+" "+ rs.getString("emp_comp_referal"));
            System.out.println("emp_comp_referal_apply"+" "+ rs.getString("emp_comp_referal_apply"));
            System.out.println("no_apply_anyJob_referal"+" "+ rs.getString("no_apply_anyJob_referal"));
            System.out.println("emp_Incomp_referal"+" "+ rs.getString("emp_Incomp_referal"));
            System.out.println("(authentication_referal-(emp_comp_referal+emp_Incomp_referal))"+" "+ rs.getString("(authentication_referal-(emp_comp_referal+emp_Incomp_referal))"));
            System.out.println("unauthentication_referal"+" "+ rs.getString("unauthentication_referal"));*/
            
            jsonRec.put("total", rs.getString("total"));
            jsonRec.put("native_teacher", rs.getString("native_teacher"));
            jsonRec.put("authentication_native", rs.getString("authentication_native"));
            jsonRec.put("emp_comp_native", rs.getString("emp_comp_native"));
            jsonRec.put("emp_comp_native_apply", rs.getString("emp_comp_native_apply"));
            jsonRec.put("no_apply_anyJob_native", rs.getString("no_apply_anyJob_native"));
            jsonRec.put("emp_Incomp_native", rs.getString("emp_Incomp_native"));
            jsonRec.put("time_out_native", rs.getString("time_out_native"));
            jsonRec.put("unauthentication_native", rs.getString("unauthentication_native"));
            jsonRec.put("referal_teacher", rs.getString("referal_teacher"));
            jsonRec.put("authentication_referal", rs.getString("authentication_referal"));
            jsonRec.put("emp_comp_referal", rs.getString("emp_comp_referal"));
            jsonRec.put("emp_comp_referal_apply", rs.getString("emp_comp_referal_apply"));
            jsonRec.put("no_apply_anyJob_referal", rs.getString("no_apply_anyJob_referal"));
            jsonRec.put("emp_Incomp_referal", rs.getString("emp_Incomp_referal"));
            jsonRec.put("EPI_Incomp_erferal", rs.getString("(authentication_referal-(emp_comp_referal+emp_Incomp_referal))"));
            jsonRec.put("unauthentication_referal", rs.getString("unauthentication_referal"));
            }
            System.out.println("PPPPPPPPP: "+jsonRec.toString());
            /*int cnt=0;
			ResultSetMetaData rsmd = rs.getMetaData();
			if(rs.next()){
				do{
					cnt++;  	
					JSONObject jsonRec = new JSONObject();
					for (int i = 1; i <=rsmd.getColumnCount(); i++) {
						String name = rsmd.getColumnName(i);
						//			    		if(cnt==1)
						//			    			System.out.println("name    "+name);
						jsonRec.put(name, rs.getString(name));
					}

					jsonArray.add(jsonRec);
				}while(rs.next());
			}

			System.out.println("LLLLLLLL : "+jsonArray.toString());*/
			
            rs.close();
            stmt.close();
            }
        catch (Exception e) {
            e.printStackTrace();
            }
        
        return jsonRec;
	}
	
	/**
	 * for Kelly API
	 * @param ksnId
	 * @return
	 */
	@Transactional(readOnly=true)
	public List<TeacherDetail> findByKSN_OP(BigInteger ksnId) throws Exception
	{
		List <TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		Criterion criterion = Restrictions.eq("KSNID",ksnId);
		criteria.add(criterion);
		criteria.setProjection(Projections.projectionList().add(Projections.property("teacherId")));
		List results = criteria.list();
		if(results!=null && results.size()>0){
			//for(Iterator it = results.iterator();it.hasNext();){
				Integer row = (Integer) results.get(0);
				if(row!=null){
					TeacherDetail  teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(row);
					lstTeacherDetails.add(teacherDetail);
				}
			//}
		}

		return lstTeacherDetails;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> getActiveKellyTeacherList_OP(Set<Integer> lstTeacherIds){
		List<TeacherDetail> teacherDetailList= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion =Restrictions.in("teacherId",lstTeacherIds);
			criteria.add(criterion);
			criteria.setProjection(Projections.projectionList()
					.add(Projections.property("teacherId"))
					.add(Projections.property("emailAddress")));
			
			List<String[]> templstTecher = new ArrayList<String[]>();
			templstTecher = criteria.list();
			if(templstTecher!=null && templstTecher.size()>0){
				for(Iterator it = templstTecher.iterator(); it.hasNext();){
					TeacherDetail teacherDetail = new TeacherDetail();
					Object[] row = (Object[]) it.next();
						
					if(row[0]!=null){
						teacherDetail.setTeacherId(Integer.parseInt(row[0].toString()));
					}
					if(row[1]!=null){
						teacherDetail.setEmailAddress(row[1].toString());
					}
					teacherDetailList.add(teacherDetail);
				}
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return teacherDetailList;
	}

}
