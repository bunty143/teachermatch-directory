package tm.dao;

import tm.bean.FileUploadHistory;
import tm.dao.generic.GenericHibernateDAO;

public class FileUploadHistoryDAO extends GenericHibernateDAO<FileUploadHistory, Integer>{
	public FileUploadHistoryDAO()
    {
        super(FileUploadHistory.class);
    }  

}
