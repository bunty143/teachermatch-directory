package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobActionFeed;
import tm.bean.JobOrder;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobActionFeedDAO extends GenericHibernateDAO<JobActionFeed, Integer> 
{
	public JobActionFeedDAO() {
		super(JobActionFeed.class);
	}

	@Transactional(readOnly=false)
	public List findJobListByLimit(int startPos,int limit,DistrictMaster districtMaster,SchoolMaster schoolMaster,int type,List<JobOrder> jobOrders)
	{

		try
		{
			
			Integer jobFeedCriticalJobActiveDays=0;
			Integer jobFeedAttentionJobActiveDays=0;
			Integer jobFeedCriticalCandidateRatio=0;
			Boolean jobFeedAttentionJobNotFilled = false;
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtMaster!=null && schoolMaster==null)
			{
				jobFeedCriticalJobActiveDays = districtMaster.getJobFeedCriticalJobActiveDays();
				jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays==null?0:jobFeedCriticalJobActiveDays;
				jobFeedAttentionJobActiveDays = districtMaster.getJobFeedAttentionJobActiveDays();
				jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays==null?0:jobFeedAttentionJobActiveDays;
				jobFeedCriticalCandidateRatio = districtMaster.getJobFeedCriticalCandidateRatio();
				jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio==null?0:jobFeedCriticalCandidateRatio;
				jobFeedAttentionJobNotFilled = districtMaster.getJobFeedAttentionJobNotFilled();
				jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled==null?false:jobFeedAttentionJobNotFilled;
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
			}else if(schoolMaster!=null)
			{
				jobFeedCriticalJobActiveDays = schoolMaster.getJobFeedCriticalJobActiveDays();
				jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays==null?0:jobFeedCriticalJobActiveDays;
				jobFeedAttentionJobActiveDays = schoolMaster.getJobFeedAttentionJobActiveDays();
				jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays==null?0:jobFeedAttentionJobActiveDays;
				jobFeedCriticalCandidateRatio = schoolMaster.getJobFeedCriticalCandidateRatio();
				jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio==null?0:jobFeedCriticalCandidateRatio;
				jobFeedAttentionJobNotFilled = schoolMaster.getJobFeedAttentionJobNotFilled();
				jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled==null?false:jobFeedAttentionJobNotFilled;
				criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}
			// RED (Critical)
			if(type==0)
			{
				criteria.add(Restrictions.ge("jobActiveDays", jobFeedCriticalJobActiveDays));
				criteria.add(Restrictions.lt("criticalNormScoreVsOpeningRatio", jobFeedCriticalCandidateRatio));
			}
			else // YELLOW (Attention)
			{
				criteria.add(Restrictions.ge("jobActiveDays", jobFeedAttentionJobActiveDays));
				criteria.add(Restrictions.ge("attentionNormScoreVsOpeningRatio", jobFeedCriticalCandidateRatio));
				
				if(jobFeedAttentionJobNotFilled)
					criteria.add(Restrictions.gtProperty("expectedHires", "noOfHiredCandidates"));
				else
					criteria.add(Restrictions.eq("noOfHiredCandidates", 0));
				
				criteria.add(Restrictions.isNull("lastActivityDate"));
				
			}
			if(jobOrders.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("jobOrder", jobOrders)));

			criteria.setProjection( Projections.projectionList()
					.add(Projections.groupProperty("jobOrder"))
					.add(Projections.property("jobActiveDays"))
					.add(Projections.property("expectedHires"))
					.add(Projections.property("totalApplicants"))
					.add(Projections.property("noOfHiredCandidates"))

			);
			

			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			List rows = criteria.list();
			//System.out.println("rows()::::::::::::::::::::: "+rows.size());
			//System.out.println("getClasssssssssssssssssssss: "+rows.get(0).getClass());
			return rows;


		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;


	}
	
	@Transactional(readOnly=false)
	public List findJobListByLimitByHBD(int startPos,int limit,DistrictMaster districtMaster,SchoolMaster schoolMaster,int type,List<JobOrder> jobOrders,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
	{

		try
		{
			
			Integer jobFeedCriticalJobActiveDays=0;
			Integer jobFeedAttentionJobActiveDays=0;
			Integer jobFeedCriticalCandidateRatio=0;
			Boolean jobFeedAttentionJobNotFilled = false;
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(branchMaster!=null){
				jobFeedCriticalJobActiveDays = branchMaster.getJobFeedCriticalJobActiveDays();
				jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays==null?0:jobFeedCriticalJobActiveDays;
				jobFeedAttentionJobActiveDays = branchMaster.getJobFeedAttentionJobActiveDays();
				jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays==null?0:jobFeedAttentionJobActiveDays;
				jobFeedCriticalCandidateRatio = branchMaster.getJobFeedCriticalCandidateRatio();
				jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio==null?0:jobFeedCriticalCandidateRatio;
				jobFeedAttentionJobNotFilled = branchMaster.getJobFeedAttentionJobNotFilled();
				jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled==null?false:jobFeedAttentionJobNotFilled;
				criteria.add(Restrictions.eq("branchMaster", branchMaster));
			}
			if(headQuarterMaster!=null){
				jobFeedCriticalJobActiveDays = headQuarterMaster.getJobFeedCriticalJobActiveDays();
				jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays==null?0:jobFeedCriticalJobActiveDays;
				jobFeedAttentionJobActiveDays = headQuarterMaster.getJobFeedAttentionJobActiveDays();
				jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays==null?0:jobFeedAttentionJobActiveDays;
				jobFeedCriticalCandidateRatio = headQuarterMaster.getJobFeedCriticalCandidateRatio();
				jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio==null?0:jobFeedCriticalCandidateRatio;
				jobFeedAttentionJobNotFilled = headQuarterMaster.getJobFeedAttentionJobNotFilled();
				jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled==null?false:jobFeedAttentionJobNotFilled;
				criteria.add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			}
			if(districtMaster!=null && schoolMaster==null)
			{
				jobFeedCriticalJobActiveDays = districtMaster.getJobFeedCriticalJobActiveDays();
				jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays==null?0:jobFeedCriticalJobActiveDays;
				jobFeedAttentionJobActiveDays = districtMaster.getJobFeedAttentionJobActiveDays();
				jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays==null?0:jobFeedAttentionJobActiveDays;
				jobFeedCriticalCandidateRatio = districtMaster.getJobFeedCriticalCandidateRatio();
				jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio==null?0:jobFeedCriticalCandidateRatio;
				jobFeedAttentionJobNotFilled = districtMaster.getJobFeedAttentionJobNotFilled();
				jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled==null?false:jobFeedAttentionJobNotFilled;
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
			}else if(schoolMaster!=null)
			{
				jobFeedCriticalJobActiveDays = schoolMaster.getJobFeedCriticalJobActiveDays();
				jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays==null?0:jobFeedCriticalJobActiveDays;
				jobFeedAttentionJobActiveDays = schoolMaster.getJobFeedAttentionJobActiveDays();
				jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays==null?0:jobFeedAttentionJobActiveDays;
				jobFeedCriticalCandidateRatio = schoolMaster.getJobFeedCriticalCandidateRatio();
				jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio==null?0:jobFeedCriticalCandidateRatio;
				jobFeedAttentionJobNotFilled = schoolMaster.getJobFeedAttentionJobNotFilled();
				jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled==null?false:jobFeedAttentionJobNotFilled;
				criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}
			// RED (Critical)
			if(type==0)
			{
				criteria.add(Restrictions.ge("jobActiveDays", jobFeedCriticalJobActiveDays));
				criteria.add(Restrictions.lt("criticalNormScoreVsOpeningRatio", jobFeedCriticalCandidateRatio));
			}
			else // YELLOW (Attention)
			{
				criteria.add(Restrictions.ge("jobActiveDays", jobFeedAttentionJobActiveDays));
				criteria.add(Restrictions.ge("attentionNormScoreVsOpeningRatio", jobFeedCriticalCandidateRatio));
				
				if(jobFeedAttentionJobNotFilled)
					criteria.add(Restrictions.gtProperty("expectedHires", "noOfHiredCandidates"));
				else
					criteria.add(Restrictions.eq("noOfHiredCandidates", 0));
				
				criteria.add(Restrictions.isNull("lastActivityDate"));
				
			}
			if(jobOrders.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("jobOrder", jobOrders)));

			criteria.setProjection( Projections.projectionList()
					.add(Projections.groupProperty("jobOrder"))
					.add(Projections.property("jobActiveDays"))
					.add(Projections.property("expectedHires"))
					.add(Projections.property("totalApplicants"))
					.add(Projections.property("noOfHiredCandidates"))

			);
			

			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			List rows = criteria.list();
			//System.out.println("rows()::::::::::::::::::::: "+rows.size());
			//System.out.println("getClasssssssssssssssssssss: "+rows.get(0).getClass());
			return rows;


		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;


	}

	@Transactional(readOnly=false)
	public List countJobList(DistrictMaster districtMaster,SchoolMaster schoolMaster,boolean all)
	{

		try 
		{
			Integer jobFeedCriticalJobActiveDays=0;
			Integer jobFeedAttentionJobActiveDays=0;
			Integer jobFeedCriticalCandidateRatio=0;
			Boolean jobFeedAttentionJobNotFilled = false;
			
			if(districtMaster!=null && schoolMaster==null)
			{
				jobFeedCriticalJobActiveDays = districtMaster.getJobFeedCriticalJobActiveDays();
				jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays==null?0:jobFeedCriticalJobActiveDays;
				jobFeedAttentionJobActiveDays = districtMaster.getJobFeedAttentionJobActiveDays();
				jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays==null?0:jobFeedAttentionJobActiveDays;
				jobFeedCriticalCandidateRatio = districtMaster.getJobFeedCriticalCandidateRatio();
				jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio==null?0:jobFeedCriticalCandidateRatio;
				jobFeedAttentionJobNotFilled = districtMaster.getJobFeedAttentionJobNotFilled();
				jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled==null?false:jobFeedAttentionJobNotFilled;
			}else if(schoolMaster!=null)
			{
				jobFeedCriticalJobActiveDays = schoolMaster.getJobFeedCriticalJobActiveDays();
				jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays==null?0:jobFeedCriticalJobActiveDays;
				jobFeedAttentionJobActiveDays = schoolMaster.getJobFeedAttentionJobActiveDays();
				jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays==null?0:jobFeedAttentionJobActiveDays;
				jobFeedCriticalCandidateRatio = schoolMaster.getJobFeedCriticalCandidateRatio();
				jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio==null?0:jobFeedCriticalCandidateRatio;
				jobFeedAttentionJobNotFilled = schoolMaster.getJobFeedAttentionJobNotFilled();
				jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled==null?false:jobFeedAttentionJobNotFilled;
			}
			
			Session session = getSession();
			String school="";
			String sql ="SELECT count(*) as total,COUNT(IF(jobActiveDays>=:jobFeedCriticalJobActiveDays and criticalNormScoreVsOpeningRatio<:criticalNormScoreVsOpeningRatio ,1,NULL)) as red";
			if(jobFeedAttentionJobNotFilled)
				sql+=",COUNT(IF(jobActiveDays>=:jobFeedAttentionJobActiveDays and attentionNormScoreVsOpeningRatio>=:attentionNormScoreVsOpeningRatio and expectedHires>noOfHiredCandidates and isNull(lastActivityDate) ,1,NULL)) as yellow" ;
			else
				sql+=",COUNT(IF(jobActiveDays>=:jobFeedAttentionJobActiveDays and attentionNormScoreVsOpeningRatio>=:attentionNormScoreVsOpeningRatio and noOfHiredCandidates=0 and isNull(lastActivityDate) ,1,NULL)) as yellow" ;
			
			if(schoolMaster!=null && !all)
				sql+=" ,schoolId FROM `jobactionfeed` " ;
			else
				sql+=" ,districtId FROM `jobactionfeed` " ;	
			
			if(!all)
			{
				sql+=" where districtId=:districtMaster";
				if(schoolMaster!=null)
					school = " and schoolId=:schoolMaster";
			}
			sql+=""+school+" group by jobId";
			
			/*System.out.println(sql);
			System.out.println("jobFeedCriticalJobActiveDays: "+jobFeedCriticalJobActiveDays);
			System.out.println("jobFeedAttentionJobActiveDays: "+jobFeedAttentionJobActiveDays );
			System.out.println("jobFeedCriticalCandidateRatio: "+jobFeedCriticalCandidateRatio );
			System.out.println("jobFeedCriticalCandidateRatio :"+jobFeedCriticalCandidateRatio );*/
			Query query = session.createSQLQuery(sql);

			if(!all)
			{
				if(schoolMaster!=null)
				{
					query.setParameter("schoolMaster", schoolMaster );
					query.setParameter("districtMaster", districtMaster );
				}
				else
					query.setParameter("districtMaster", districtMaster );
			}
			query.setParameter("jobFeedCriticalJobActiveDays", jobFeedCriticalJobActiveDays );
			query.setParameter("jobFeedAttentionJobActiveDays", jobFeedAttentionJobActiveDays );
			query.setParameter("criticalNormScoreVsOpeningRatio", jobFeedCriticalCandidateRatio );
			query.setParameter("attentionNormScoreVsOpeningRatio", jobFeedCriticalCandidateRatio );


			List<Object[]> rows = query.list();
			//System.out.println("SSSSSSSSSSSSSSSSSSSSS : "+rows.size());
			return rows;


		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;


	}
	
	
	@Transactional(readOnly=false)
	public List countJobListByFilter(DistrictMaster districtMaster,SchoolMaster schoolMaster,boolean all, String lstJobOrders, boolean chekflag, boolean chkbyopening, String jobIDSbyopening ,int numOpeningSelectVal)
	{
		System.out.println("lstJobOrders DAOOOO:: "+lstJobOrders);
		try 
		{
			Integer jobFeedCriticalJobActiveDays=0;
			Integer jobFeedAttentionJobActiveDays=0;
			Integer jobFeedCriticalCandidateRatio=0;
			Boolean jobFeedAttentionJobNotFilled = false;
			
			if(districtMaster!=null && schoolMaster==null)
			{
				jobFeedCriticalJobActiveDays = districtMaster.getJobFeedCriticalJobActiveDays();
				jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays==null?0:jobFeedCriticalJobActiveDays;
				jobFeedAttentionJobActiveDays = districtMaster.getJobFeedAttentionJobActiveDays();
				jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays==null?0:jobFeedAttentionJobActiveDays;
				jobFeedCriticalCandidateRatio = districtMaster.getJobFeedCriticalCandidateRatio();
				jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio==null?0:jobFeedCriticalCandidateRatio;
				jobFeedAttentionJobNotFilled = districtMaster.getJobFeedAttentionJobNotFilled();
				jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled==null?false:jobFeedAttentionJobNotFilled;

			}else if(schoolMaster!=null)
			{
				jobFeedCriticalJobActiveDays = schoolMaster.getJobFeedCriticalJobActiveDays();
				jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays==null?0:jobFeedCriticalJobActiveDays;
				jobFeedAttentionJobActiveDays = schoolMaster.getJobFeedAttentionJobActiveDays();
				jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays==null?0:jobFeedAttentionJobActiveDays;
				jobFeedCriticalCandidateRatio = schoolMaster.getJobFeedCriticalCandidateRatio();
				jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio==null?0:jobFeedCriticalCandidateRatio;
				jobFeedAttentionJobNotFilled = schoolMaster.getJobFeedAttentionJobNotFilled();
				jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled==null?false:jobFeedAttentionJobNotFilled;
			}
			
			Session session = getSession();
			String school="";
			String sql ="SELECT count(*) as total,COUNT(IF(jobActiveDays>=:jobFeedCriticalJobActiveDays and criticalNormScoreVsOpeningRatio<:criticalNormScoreVsOpeningRatio ,1,NULL)) as red";
			if(jobFeedAttentionJobNotFilled)
				sql+=",COUNT(IF(jobActiveDays>=:jobFeedAttentionJobActiveDays and attentionNormScoreVsOpeningRatio>=:attentionNormScoreVsOpeningRatio and expectedHires>noOfHiredCandidates and isNull(lastActivityDate) ,1,NULL)) as yellow" ;
			else
				sql+=",COUNT(IF(jobActiveDays>=:jobFeedAttentionJobActiveDays and attentionNormScoreVsOpeningRatio>=:attentionNormScoreVsOpeningRatio and noOfHiredCandidates=0 and isNull(lastActivityDate) ,1,NULL)) as yellow" ;
			
			if(schoolMaster!=null )
				sql+=" ,schoolId FROM `jobactionfeed` " ;
			else
				sql+=" ,districtId FROM `jobactionfeed` " ;	
			String where="";
			if(!all)
			{
				sql+=" where districtId=:districtMaster";
				if(schoolMaster!=null)
					school = " and schoolId=:schoolMaster";
				where="where";
			}
			
			if(chekflag==true  )
			{
				if(!lstJobOrders.equalsIgnoreCase("")){
					if(where.equalsIgnoreCase("where"))
						sql+=" and jobId IN("+ lstJobOrders +")";
					else
						sql+=" where jobId IN("+ lstJobOrders +")";
				}else{
					
						return new ArrayList();
					
				}
				
			}
			if(chkbyopening && numOpeningSelectVal!=0){
				if(!lstJobOrders.equalsIgnoreCase("")){
					if(where.equalsIgnoreCase("where"))
						sql+=" and jobId IN("+ jobIDSbyopening +")";
					else
						sql+=" where jobId IN("+ jobIDSbyopening +")";
				}else{
					
						return new ArrayList();
					
				}
				
			}
			sql+=""+school+" group by jobId";
			
			Query query = session.createSQLQuery(sql);

			if(!all)
			{
				if(schoolMaster!=null)
				{
					query.setParameter("schoolMaster", schoolMaster );
					query.setParameter("districtMaster", districtMaster );
				}
				else
					query.setParameter("districtMaster", districtMaster );
			}
			query.setParameter("jobFeedCriticalJobActiveDays", jobFeedCriticalJobActiveDays );
			query.setParameter("jobFeedAttentionJobActiveDays", jobFeedAttentionJobActiveDays );
			query.setParameter("criticalNormScoreVsOpeningRatio", jobFeedCriticalCandidateRatio );
			query.setParameter("attentionNormScoreVsOpeningRatio", jobFeedCriticalCandidateRatio );
			
			/*if(chekflag==true)
				query.setParameterList("jobs", lstJobOrders );*/
			
			System.out.println("query :: "+query);
			List<Object[]> rows =new ArrayList<Object[]>();
			
			
			 rows = query.list();
			System.out.println("ENDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD Size  : " +rows.size());
			return rows;


		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;


	}
	@Transactional(readOnly=false)	
	public List<JobActionFeed>	findbyDistrictAndOrSchool(DistrictMaster districtMaster,SchoolMaster schoolMaster){
		List<JobActionFeed> lstJobActionFeeds = new ArrayList<JobActionFeed>();
		Criterion criterion2;
		Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
		if(schoolMaster!=null){
			      criterion2      = Restrictions.eq("districtMaster",schoolMaster);
	       lstJobActionFeeds      = findByCriteria(criterion1,criterion2);
		}else{
			lstJobActionFeeds      = findByCriteria(criterion1);
		}
		System.out.println("JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ :: "+lstJobActionFeeds.size());
		return lstJobActionFeeds;
	}
}
