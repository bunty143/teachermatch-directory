package tm.dao.user;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.user.MasterPassword;
import tm.dao.generic.GenericHibernateDAO;

public class MasterPasswordDAO extends GenericHibernateDAO<MasterPassword, Integer>{

	public MasterPasswordDAO() 
	{
		super(MasterPassword.class);
	}
	
	@Transactional(readOnly=true)
	public List<MasterPassword> validateUser(String userType, String password)
	{
		List <MasterPassword> masterPasswords = new ArrayList<MasterPassword>();
		Criterion criterion1 = Restrictions.eq("userType",userType);
		Criterion criterion2 = Restrictions.eq("password", password);
		Criterion criterion3 = Restrictions.eq("status", "a");
		masterPasswords = findByCriteria(criterion1,criterion2,criterion3);  
		return masterPasswords;
	}
	
	@Transactional(readOnly=true)
	public boolean isValidateUser(String userType, String password)
	{
		List <MasterPassword> masterPasswords = new ArrayList<MasterPassword>();
		masterPasswords = validateUser(userType,password);
		if(masterPasswords.size()==1)
			return true;
		else
			return false;
	}
	
	}
