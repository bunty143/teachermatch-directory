package tm.dao.user;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.user.RoleMaster;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Vishwanath Kumar
 * @Discription: RoleMaster DAO.
 */
public class RoleMasterDAO extends GenericHibernateDAO<RoleMaster, Integer> 
{

	public RoleMasterDAO() {
		super(RoleMaster.class);
	}
	
	/* @Author: Gagan 
	 * @Discription: It is used to return rolename by entitytype.
	 */	
	@Transactional(readOnly=false)
	public List<RoleMaster> getRoleNameByEntityType(Integer entityType)
	{
		List<RoleMaster> roleMaster		=	null;
		try{
			Criterion criterion			=	Restrictions.eq("status", "a");
			Criterion criterion1		=	Restrictions.eq("entityType", entityType);
			roleMaster					=	findByCriteria(Order.asc("roleName"), criterion,criterion1);
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return roleMaster;
		
	}
	@Transactional(readOnly=false)
	public List<RoleMaster> getRoleByEntityType(List<Integer> roleIds)
	{
		List<RoleMaster> roleMaster		=	new ArrayList<RoleMaster>();
		try{
			Criterion criterion			=	Restrictions.eq("status", "a");
			Criterion criterion1		=	Restrictions.in("roleId", roleIds);
			roleMaster					=	findByCriteria(Order.asc("roleName"), criterion,criterion1);
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return roleMaster;
		
	}
}
