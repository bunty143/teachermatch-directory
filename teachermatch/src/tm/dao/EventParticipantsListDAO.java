package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.CmsEventTypeMaster;
import tm.bean.EventDetails;
import tm.bean.EventParticipantsList;
import tm.bean.JobOrder;
import tm.bean.i4.I4InterviewInvites;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class EventParticipantsListDAO extends GenericHibernateDAO<EventParticipantsList, Integer>{

	public EventParticipantsListDAO(){
		super(EventParticipantsList.class);
	}
	
	@Autowired
	private CmsEventTypeMasterDao cmsEventTypeMasterDao;
	
	@Transactional(readOnly=false)
	public List<EventParticipantsList> findListOfParticipants(Order order,int startPos,int limit,DistrictMaster  districtMaster,EventDetails eventId)
	{
		List<EventParticipantsList> lstEventParticipantsList= null;
		try 
		{
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				 Criterion criterionDis = Restrictions.eq("districtMaster", districtMaster);
				 Criterion criterionEvent = Restrictions.eq("eventDetails", eventId);
				 
				 criteria.add(criterionDis);
				    criteria.add(criterionEvent);
				    
				    criteria.setFirstResult(startPos);
					criteria.setMaxResults(limit);			
					criteria.addOrder(order);
					
					lstEventParticipantsList =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstEventParticipantsList;
	}
	
	@Transactional(readOnly=false)
	public List<EventParticipantsList> findListOfParticipantsCount(DistrictMaster  districtMaster,EventDetails eventId)
	{
		List<EventParticipantsList> lstEventParticipantsList= null;
		try 
		{
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				 Criterion criterionDis = Restrictions.eq("districtMaster", districtMaster);
				 Criterion criterionEvent = Restrictions.eq("eventDetails", eventId);
				 
				 criteria.add(criterionDis);
				 criteria.add(criterionEvent);
				 lstEventParticipantsList =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstEventParticipantsList;
	}
	
	@Transactional(readOnly=false)
	public List<EventParticipantsList> findByEventDetails(EventDetails eventDetails)
	{
		System.out.println(" ===== findByEventDetails ======= ");
		List<EventParticipantsList> lstEventParticipantsList = new ArrayList<EventParticipantsList>();
		try 
		{
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				Criterion criterionEvent = Restrictions.eq("eventDetails", eventDetails);
				 
				 criteria.add(criterionEvent);
				 lstEventParticipantsList = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstEventParticipantsList;
	}
	
	@Transactional(readOnly=false)
	public List<EventParticipantsList> findByEventList(List<EventDetails> eventDetails)
	{
		System.out.println(" ===== findByEventDetails ======= ");
		List<EventParticipantsList> lstEventParticipantsList = new ArrayList<EventParticipantsList>();
		try 
		{
			CmsEventTypeMaster cmsEventTypeMaster = cmsEventTypeMasterDao.findById(1, false, false); 
			
			
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				Criterion criterionEvent = Restrictions.in("eventDetails", eventDetails);
				criteria.add(criterionEvent);
				criteria.createCriteria("eventDetails").add(Restrictions.eq("eventTypeId", cmsEventTypeMaster));
				
				lstEventParticipantsList = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstEventParticipantsList;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<EventParticipantsList> findeventParticipantsListByTeacherId(List<Integer> teacherId,JobOrder jobOrder)
	{
		List<EventParticipantsList> lstEventParticipantsList = new ArrayList<EventParticipantsList>();
		try 
		{
			if(teacherId!=null && teacherId.size()>0)
			{
				CmsEventTypeMaster cmsEventTypeMaster = cmsEventTypeMasterDao.findById(6, false, false); 
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				Criterion criterionEvent = Restrictions.in("teacherId", teacherId);
				criteria.add(criterionEvent);
				//criteria.createCriteria("eventDetails").add(Restrictions.eq("eventTypeId", cmsEventTypeMaster));
				Criteria subCriteriaeventDetails=criteria.createCriteria("eventDetails");
							subCriteriaeventDetails.add(Restrictions.eq("eventTypeId", cmsEventTypeMaster));
							subCriteriaeventDetails.add(Restrictions.eq("jobOrder", jobOrder));
				lstEventParticipantsList = criteria.list();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstEventParticipantsList;
	}
	
	@Transactional(readOnly=false)
	public List<EventParticipantsList> findeventParticipantsListByI4InterviewId(I4InterviewInvites i4InterviewInvites)
	{
		List<EventParticipantsList> lstEventParticipantsList = new ArrayList<EventParticipantsList>();
		try 
		{
			if(i4InterviewInvites!=null )
			{
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				Criterion criterionEvent = Restrictions.eq("i4InterviewInvites", i4InterviewInvites);
				 
				 criteria.add(criterionEvent);
				 lstEventParticipantsList = criteria.list();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstEventParticipantsList;
	}
	
}

