package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Date;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.ExaminationCodeDetails;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class ExaminationCodeDetailsDAO extends GenericHibernateDAO<ExaminationCodeDetails, Integer> 
{
	public ExaminationCodeDetailsDAO() {
		super(ExaminationCodeDetails.class);
	}

	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExamCode(String sExamCode)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
		if(sExamCode!=null && !sExamCode.equals(""))
		{
			try {
				Criterion criterion1 = Restrictions.eq("examinationCode",sExamCode);
				Criterion criterion2 = Restrictions.eq("status","A");
				lstExaminationCodeDetails=findByCriteria(criterion1,criterion2);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstExaminationCodeDetails;
	}

	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExamCodeByCategoryAndTeacher(JobCategoryMaster jobCategoryMaster,TeacherDetail teacherDetail)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
			try {
				Criterion criterion1 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
				Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion3 = Restrictions.eq("status","A");
				lstExaminationCodeDetails=findByCriteria(criterion1,criterion2,criterion3);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return lstExaminationCodeDetails;
	}
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExamCodeByJobOrderAndTeacher(JobOrder jobOrder,TeacherDetail teacherDetail)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
			try {
				Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
				Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion3 = Restrictions.eq("status","A");
				lstExaminationCodeDetails=findByCriteria(criterion1,criterion2,criterion3);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return lstExaminationCodeDetails;
	}
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExamCodeByCategoryListAndTeacherList(List<JobCategoryMaster> jobCategoryMasterList,List<TeacherDetail> teacherDetailList)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
			try {
				Criterion criterion1 = Restrictions.in("jobCategoryMaster",jobCategoryMasterList);
				Criterion criterion2 = Restrictions.in("teacherDetail",teacherDetailList);
				Criterion criterion3 = Restrictions.eq("status","A");
				lstExaminationCodeDetails=findByCriteria(criterion1,criterion2,criterion3);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return lstExaminationCodeDetails;
	}
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExamCodeByJobOrderAndTeacherList(JobOrder jobOrder,List<TeacherDetail> teacherDetailList)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
			try {
				Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
				Criterion criterion2 = Restrictions.in("teacherDetail",teacherDetailList);
				Criterion criterion3 = Restrictions.eq("status","A");
				lstExaminationCodeDetails=findByCriteria(criterion1,criterion2,criterion3);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return lstExaminationCodeDetails;
	}
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExaminationCodeDetails(DistrictMaster districtMaster,String startDate, String endDate,String firstName,String lastName,String emailAddress,String examCode,Integer startPos,Integer limit,Order order,List<Integer> teacherIdList)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			java.util.Date date1 =null;
			java.util.Date date2 =null;
			java.util.Date defaultDate =null;
			
			if(startDate!=null && !startDate.equals(""))
				date1 = Utility.getCurrentDateFormart(startDate);
			if(endDate!=null && !endDate.equals(""))
				date2 = Utility.getCurrentDateFormart(endDate);
			if(startDate!=null && startDate.equals("") && endDate!=null && endDate.equals(""))
			{
				defaultDate = new Date();
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(defaultDate);
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
				defaultDate = calendar.getTime();
			}
			
			if(districtMaster!=null)
				criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster", districtMaster));
			
			criteria.add(Restrictions.eq("status", "A"));
			
			if(date1!=null && date2!=null)
				criteria.add(Restrictions.between("examDate", date1, date2));
			else
			if(date1!=null && date2==null)
				criteria.add(Restrictions.ge("examDate", date1));
			else
			if(date1==null && date2!=null)
				criteria.add(Restrictions.le("examDate", date2));
			else
			if(defaultDate!=null)
				criteria.add(Restrictions.eq("examDate", defaultDate));
			
			//if( (startDate==null || startDate.equals("")) && (endDate==null || endDate.equals("")) && (firstName==null || firstName.equals("")) && (lastName==null || lastName.equals("")) && (emailAddress==null || emailAddress.equals("")) && (examCode==null || examCode.equals("")) )
			//	criteria.add(Restrictions.eq("examDate", defaultDate));
			
			
			Criteria teacherCriteria = criteria.createAlias("teacherDetail", "td");
			if(firstName!=null && !firstName.equals(""))
				teacherCriteria.add(Restrictions.like("td.firstName", firstName.trim(),MatchMode.ANYWHERE));
			if(lastName!=null && !lastName.equals(""))
				teacherCriteria.add(Restrictions.like("td.lastName", lastName.trim(),MatchMode.ANYWHERE));
			if(emailAddress!=null && !emailAddress.equals(""))
				teacherCriteria.add(Restrictions.like("td.emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
			//if(teacherIdList!=null && teacherIdList.size()>0)
				//teacherCriteria.add(Restrictions.in("td.teacherId", teacherIdList));
			
			if(teacherIdList!=null && teacherIdList.size()>0)
				criteria.add(Restrictions.in("codeId", teacherIdList));
			
			if(examCode!=null && !examCode.equals(""))
				criteria.add(Restrictions.eq("examinationCode", examCode));
			
			if(startPos!=null)
				criteria.setFirstResult(startPos);
			
			if(limit!=null)
				criteria.setMaxResults(limit);
			
			if(order != null)
				criteria.addOrder(order);
			
			lstExaminationCodeDetails = criteria.list();
			
			if(lstExaminationCodeDetails==null)
				lstExaminationCodeDetails = new ArrayList<ExaminationCodeDetails>();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return lstExaminationCodeDetails;
	}
	
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExaminationCodeDetailsCount(DistrictMaster districtMaster,String startDate, String endDate,String firstName,String lastName,String emailAddress,String examCode,List<Integer> teacherIdList)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			java.util.Date date1 =null;
			java.util.Date date2 =null;
			java.util.Date defaultDate =null;
			
			if(startDate!=null && !startDate.equals(""))
				date1 = Utility.getCurrentDateFormart(startDate);
			if(endDate!=null && !endDate.equals(""))
				date2 = Utility.getCurrentDateFormart(endDate);
			if(startDate!=null && startDate.equals("") && endDate!=null && endDate.equals(""))
			{
				defaultDate = new Date();
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(defaultDate);
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
				defaultDate = calendar.getTime();
			}
			
			if(districtMaster!=null)
				criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster", districtMaster));
			
			if(date1!=null && date2!=null)
				criteria.add(Restrictions.between("examDate", date1, date2));
			else
			if(date1!=null && date2==null)
				criteria.add(Restrictions.ge("examDate", date1));
			else
			if(date1==null && date2!=null)
				criteria.add(Restrictions.le("examDate", date2));
			else
			if(defaultDate!=null)
				criteria.add(Restrictions.eq("examDate", defaultDate));
			
			//if( (startDate==null || startDate.equals("")) && (endDate==null || endDate.equals("")) && (firstName==null || firstName.equals("")) && (lastName==null || lastName.equals("")) && (emailAddress==null || emailAddress.equals("")) && (examCode==null || examCode.equals("")) )
			//	criteria.add(Restrictions.eq("examDate", defaultDate));
			
			
			Criteria teacherCriteria = criteria.createAlias("teacherDetail", "td");
			if(firstName!=null && !firstName.equals(""))
				teacherCriteria.add(Restrictions.like("td.firstName", firstName.trim(),MatchMode.ANYWHERE));
			if(lastName!=null && !lastName.equals(""))
				teacherCriteria.add(Restrictions.like("td.lastName", lastName.trim(),MatchMode.ANYWHERE));
			if(emailAddress!=null && !emailAddress.equals(""))
				teacherCriteria.add(Restrictions.like("td.emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
			/*if(teacherIdList!=null && teacherIdList.size()>0)
				teacherCriteria.add(Restrictions.in("td.teacherId", teacherIdList));*/
			
			if(teacherIdList!=null && teacherIdList.size()>0)
				criteria.add(Restrictions.in("codeId", teacherIdList));
			
			if(examCode!=null && !examCode.equals(""))
				criteria.add(Restrictions.eq("examinationCode", examCode));
						
			lstExaminationCodeDetails = criteria.list();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return lstExaminationCodeDetails;
	}
	
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExamCodesByStatus()
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
			try {
				Criterion criterion = Restrictions.eq("status","A");
				Criterion criterion1 = Restrictions.ne("inviteStatus","C");
				Criterion criterion2 = Restrictions.isNull("examDate");
				lstExaminationCodeDetails=findByCriteria(criterion,criterion1,criterion2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return lstExaminationCodeDetails;
	}
	
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExamCodeByDAssessmentAndTeacher(List<DistrictAssessmentDetail> districtAssessmentDetailList,TeacherDetail teacherDetail)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
			try {
				if(districtAssessmentDetailList.size()>0){
					Criterion criterion1 = Restrictions.in("districtAssessmentDetail",districtAssessmentDetailList);
					Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
					Criterion criterion3 = Restrictions.eq("status","A");
					lstExaminationCodeDetails=findByCriteria(criterion1,criterion2,criterion3);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		return lstExaminationCodeDetails;
	}
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getInviteByDAssessmentAndTeacherAndJob(DistrictAssessmentDetail districtAssessmentDetail,TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
			try {
					Criterion criterion1 = Restrictions.eq("districtAssessmentDetail",districtAssessmentDetail);
					Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
					Criterion criterion3 = Restrictions.eq("jobOrder",jobOrder);
					Criterion criterion4 = Restrictions.eq("status","A");
					Criterion criterion5 = Restrictions.eq("inviteStatus","I");
					Criterion criterion6 = Restrictions.isNull("examDate");
					lstExaminationCodeDetails=findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return lstExaminationCodeDetails;
	}
	
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExamCodeByDAssessAndTeacher(DistrictAssessmentDetail districtAssessmentDetail,TeacherDetail teacherDetail)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
			try {
					Criterion criterion1 = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);
					Criterion criterion2 = Restrictions.eq("teacherDetail", teacherDetail);
					Criterion criterion4 = Restrictions.eq("status","A");
					lstExaminationCodeDetails=findByCriteria(criterion1,criterion2,criterion4);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return lstExaminationCodeDetails;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExamCodeByTeacher(TeacherDetail teacherDetail, int sortingcheck,Order sortOrderStrVal,int start , int limit,Boolean flag)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
			try {
					Session session = getSession();
					Criteria criteria= session.createCriteria(getPersistentClass());
					Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
					Criterion criterion2 = Restrictions.eq("status","A");
					criteria.add(criterion1);
					criteria.add(criterion2);
					
					
					Criteria c1= null;
					Criteria c2= null;
					Criteria c3= null;
					if(sortingcheck==0){
						c1 = criteria.createCriteria("districtAssessmentDetail");
						c1.addOrder(sortOrderStrVal);
					}
					/*if(sortingcheck==1){
						c1 = criteria.createCriteria("districtMaster");
						c1.addOrder(sortOrderStrVal);
					}*/
					if(sortingcheck==2){
						criteria.addOrder(sortOrderStrVal);
					}
					if(sortingcheck==3){
						criteria.addOrder(sortOrderStrVal);
					}
				/*	if(sortingcheck==4){
						c3 = criteria.createCriteria("jobCategoryMaster");
						c3.addOrder(sortOrderStrVal);
					}*/
					if(flag)
					{
						criteria.setFirstResult(start);
						criteria.setMaxResults(limit);
					}
					
					lstExaminationCodeDetails=criteria.list();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return lstExaminationCodeDetails;
	}
	
	@Transactional(readOnly=false)
	public int getTotalExamCodeByTeacher(TeacherDetail teacherDetail, int sortingcheck,Order sortOrderStrVal,int start , int limit)
	{
		int countRecord = 0;
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
			try {
					Session session = getSession();
					Criteria criteria= session.createCriteria(getPersistentClass());
					Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
					Criterion criterion2 = Restrictions.eq("status","A");
					criteria.add(criterion1);
					criteria.add(criterion2);
					
					
					Criteria c1= null;
					Criteria c2= null;
					Criteria c3= null;
					if(sortingcheck==0){
						c1 = criteria.createCriteria("districtAssessmentDetail");
						c1.addOrder(sortOrderStrVal);
					}
					/*if(sortingcheck==1){
						c1 = criteria.createCriteria("districtMaster");
						c1.addOrder(sortOrderStrVal);
					}*/
					if(sortingcheck==2){
						criteria.addOrder(sortOrderStrVal);
					}
					if(sortingcheck==3){
						criteria.addOrder(sortOrderStrVal);
					}
				/*	if(sortingcheck==4){
						c3 = criteria.createCriteria("jobCategoryMaster");
						c3.addOrder(sortOrderStrVal);
					}*/
					
					
					lstExaminationCodeDetails=criteria.list();
					countRecord = lstExaminationCodeDetails.size();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return countRecord;
	}
	
	
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getDistrictAssessmentByTeacher(TeacherDetail teacherDetail)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
			try {
					Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
					Criterion criterion2 = Restrictions.eq("status","A");
					lstExaminationCodeDetails=findByCriteria(criterion1,criterion2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return lstExaminationCodeDetails;
	}
	
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExamCodeByTeachers(DistrictAssessmentDetail districtAssessmentDetail,List<TeacherDetail> teacherDetail)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
			try {
					Criterion criterion1 = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);
					Criterion criterion2 = Restrictions.in("teacherDetail", teacherDetail);
					Criterion criterion4 = Restrictions.eq("status","A");
					
					lstExaminationCodeDetails=findByCriteria(criterion1,criterion2,criterion4);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return lstExaminationCodeDetails;
	}
	
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExamCodeByDate(Date date)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
		if(date!=null)
		{
			try {
				Criterion criterion1 = Restrictions.eq("examDate",date);
				Criterion criterion2 = Restrictions.eq("status","A");
				lstExaminationCodeDetails=findByCriteria(criterion1,criterion2);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstExaminationCodeDetails;
	}
	
	@Transactional(readOnly=false)
	public List<ExaminationCodeDetails> getExamCodeByDistrictAndDate(DistrictMaster districtMaster,Date date)
	{
		List<ExaminationCodeDetails> lstExaminationCodeDetails=new ArrayList<ExaminationCodeDetails>();
		if(date!=null)
		{
			try {
				Session session = getSession();
				Criteria criteria= session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.eq("examDate",date);
				Criterion criterion2 = Restrictions.eq("status","A");
				criteria.add(criterion1);
				criteria.add(criterion2);
				
				criteria.createCriteria("districtAssessmentDetail").add(Restrictions.eq("districtMaster", districtMaster));
				
				Criterion criterion4 = Restrictions.isNotNull("examDate");
				
				criteria.add(criterion4);
				lstExaminationCodeDetails=criteria.list();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstExaminationCodeDetails;
	}
}