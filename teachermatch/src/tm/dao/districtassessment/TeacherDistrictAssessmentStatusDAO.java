package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentStatus;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherDistrictAssessmentStatusDAO extends GenericHibernateDAO<TeacherDistrictAssessmentStatus, Integer>{

	public TeacherDistrictAssessmentStatusDAO() {
		super(TeacherDistrictAssessmentStatus.class);
	}
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentStatus> findDistrictAssessmentTaken(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList = new ArrayList<TeacherDistrictAssessmentStatus>();
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = null;
			criterion1 = Restrictions.eq("jobOrder", jobOrder);
			
			if(teacherDetail==null){
				teacherDistrictAssessmentStatusList = findByCriteria(criterion1);
			}
			else{
				teacherDistrictAssessmentStatusList = findByCriteria(criterion,criterion1);
			}

		
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return teacherDistrictAssessmentStatusList;

	}
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentStatus> findDistrictAssessmentTakenByTeacher(TeacherDetail teacherDetail,DistrictAssessmentDetail districtAssessmentDetail)
	{

		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);

			teacherDistrictAssessmentStatusList = findByCriteria(criterion,criterion1);


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentStatusList;

	}
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentStatus> findDistrictAssessmentStatusByTeacherAssessmentdetail(TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail)
	{
		List<TeacherDistrictAssessmentStatus> lstDistrictTeacherAssessmentStatus = null;


		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDistrictAssessmentDetail",teacherDistrictAssessmentDetail);

			lstDistrictTeacherAssessmentStatus = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		

		return lstDistrictTeacherAssessmentStatus;
		
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentStatus> getStatus(DistrictAssessmentDetail districtAssessmentDetail,TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherDistrictAssessmentStatus> assessmentStatus=new ArrayList<TeacherDistrictAssessmentStatus>();
		
		Criterion criterion4 = Restrictions.eq("districtAssessmentDetail",districtAssessmentDetail);
		Criterion criterion5 = Restrictions.eq("teacherDetail",teacherDetail);
		Criterion criterion6 = Restrictions.eq("jobOrder",jobOrder);
		
		try {
			assessmentStatus=findByCriteria(criterion4,criterion5,criterion6);
		} catch (Exception e) {
			
		}
		return assessmentStatus;
	}
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentStatus> findAssessmentTakenByTeacher(TeacherDetail teacherDetail,DistrictAssessmentDetail districtAssessmentDetail)
	{

		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList = new ArrayList<TeacherDistrictAssessmentStatus>();
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);

			teacherDistrictAssessmentStatusList = findByCriteria(criterion,criterion1);


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentStatusList;

	}
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentStatus> findAssessmentTakenByTeachers(List<TeacherDetail> teacherDetails)
	{
		
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList = new ArrayList<TeacherDistrictAssessmentStatus>();
		try {
			if(teacherDetails.size()>0){
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				teacherDistrictAssessmentStatusList = findByCriteria(criterion1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentStatusList;

	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> findDistrictAssessmentsResponses() 
	{
		Session session = getSession();

		List result = session.createCriteria(getPersistentClass())       
		.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("districtAssessmentDetail"))
				.add(Projections.count("districtAssessmentDetail"))           
		).list();

		System.out.println(" districtAssessmentId "+result.size());
		
		
		Map<Integer,Integer> assessmentAttempsMap = new HashMap<Integer, Integer>();
		DistrictAssessmentDetail assessmentDetail = null;
		for (Object object : result) {
			Object rows[] = (Object[])object;
			assessmentDetail=((DistrictAssessmentDetail)rows[0]);
			assessmentAttempsMap.put(assessmentDetail.getDistrictAssessmentId(), ((Integer)rows[1]));
		}
		return assessmentAttempsMap;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentStatus> findDistrictAssessmentStatusByTeacher(TeacherDetail teacherDetail,TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail)
	{

		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.eq("teacherDistrictAssessmentDetail", teacherDistrictAssessmentDetail);

			teacherDistrictAssessmentStatusList = findByCriteria(criterion,criterion1);


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentStatusList;

	}
	
	@Transactional(readOnly=false)
	public TeacherDistrictAssessmentStatus findAssessmentObjByTeacher(TeacherDetail teacherDetail,DistrictAssessmentDetail districtAssessmentDetail)
	{
		TeacherDistrictAssessmentStatus teacherDistrictAssessmentStatus=null;
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList = new ArrayList<TeacherDistrictAssessmentStatus>();
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);
			teacherDistrictAssessmentStatusList = findByCriteria(criterion,criterion1);
			if(teacherDistrictAssessmentStatusList.size()>0){
				if(teacherDistrictAssessmentStatusList.get(0).getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
					teacherDistrictAssessmentStatus=teacherDistrictAssessmentStatusList.get(0);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentStatus;

	}
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentStatus> findAssessmentStatusByTeachersAndAssessment(List<TeacherDetail> teacherDetails,List<DistrictAssessmentDetail> districtAssessmentDetailList)
	{
		
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList = new ArrayList<TeacherDistrictAssessmentStatus>();
		try {
			if(teacherDetails.size()>0){
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				teacherDistrictAssessmentStatusList = findByCriteria(criterion1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentStatusList;
	}
	@Transactional(readOnly=false)
	public void updatefitScore(TeacherDetail teacherDetail,DistrictAssessmentDetail districtAssessmentDetail,Double marksObtained,Double totalScore,int updateType)
	{
		System.out.println("::::::::updatefitScore::::");		
		try{
			TeacherDistrictAssessmentStatus teacherDistrictAssessmentStatus = findAssessmentObjByTeacher(teacherDetail, districtAssessmentDetail);
			Double cScore=0.0;
            Double maxScore=0.0;
           
			if(teacherDistrictAssessmentStatus!=null && updateType==1){
				int oldCScore=0;
	            int oldMaxScore=0;
	            int finalCSore = 0;
	            int finalMaxSore = 0;
				oldMaxScore = teacherDistrictAssessmentStatus.getTotalMarks().intValue();
				oldCScore	= teacherDistrictAssessmentStatus.getMarksObtained().intValue();
				
				teacherDistrictAssessmentStatus.setMarksObtained(marksObtained);
				teacherDistrictAssessmentStatus.setTotalMarks(totalScore);
				updatePersistent(teacherDistrictAssessmentStatus);
											
				int newMaxScore = teacherDistrictAssessmentStatus.getTotalMarks().intValue();
				int newCScore	= teacherDistrictAssessmentStatus.getMarksObtained().intValue();
				/*							
				System.out.println("oldMaxScore:::"+oldMaxScore);
				System.out.println("oldCScore:::"+oldCScore);
				System.out.println("newMaxScore:::"+newMaxScore);
				System.out.println("newCScore:::"+newCScore);*/
				
				
				// for cscore
				if(oldCScore < newCScore)
				{
					finalCSore = newCScore - oldCScore;
				}
				else if(oldCScore > newCScore)
				{
					finalCSore = newCScore - oldCScore ;
				}
				
				
				// for maxscore
				if(oldMaxScore < newMaxScore)
				{
					finalMaxSore =  newMaxScore - oldMaxScore;
				}
				else if(oldMaxScore > newMaxScore)
				{
					finalMaxSore = 	newMaxScore - oldMaxScore;
				}
				
				JobWiseConsolidatedTeacherScore jwScoreObj=null;
	            jwScoreObj=jobWiseConsolidatedTeacherScoreDAO.getJWCTScore(teacherDetail, teacherDistrictAssessmentStatus.getJobOrder());
	            
	            if(jwScoreObj!=null)
	            {
	            	cScore = jwScoreObj.getJobWiseConsolidatedScore();
					maxScore = jwScoreObj.getJobWiseMaxScore();
	            	jwScoreObj.setJobWiseConsolidatedScore(finalCSore+cScore);
	                jwScoreObj.setJobWiseMaxScore(finalMaxSore+maxScore);
	                jobWiseConsolidatedTeacherScoreDAO.updatePersistent(jwScoreObj);
	            }
	            else
	            {
	            	jwScoreObj = new JobWiseConsolidatedTeacherScore();
	            	jwScoreObj.setTeacherDetail(teacherDetail);
	            	jwScoreObj.setJobOrder(teacherDistrictAssessmentStatus.getJobOrder());
	            	jwScoreObj.setJobWiseConsolidatedScore(marksObtained);
	                jwScoreObj.setJobWiseMaxScore(totalScore);
	                jwScoreObj.setUpdatedDateTime(new Date());
	                jobWiseConsolidatedTeacherScoreDAO.makePersistent(jwScoreObj);
	            }
			}else{
				JobWiseConsolidatedTeacherScore jwScoreObj=null;
	            jwScoreObj=jobWiseConsolidatedTeacherScoreDAO.getJWCTScore(teacherDetail, teacherDistrictAssessmentStatus.getJobOrder());
	            if(jwScoreObj!=null)
	            {
	            	cScore = jwScoreObj.getJobWiseConsolidatedScore();
					maxScore = jwScoreObj.getJobWiseMaxScore();
	            	jwScoreObj.setJobWiseConsolidatedScore(marksObtained+cScore);
	                jwScoreObj.setJobWiseMaxScore(totalScore+maxScore);
	                jobWiseConsolidatedTeacherScoreDAO.updatePersistent(jwScoreObj);
	            }
	            else
	            {
	            	jwScoreObj = new JobWiseConsolidatedTeacherScore();
	            	jwScoreObj.setTeacherDetail(teacherDetail);
	            	jwScoreObj.setJobOrder(teacherDistrictAssessmentStatus.getJobOrder());
	            	jwScoreObj.setJobWiseConsolidatedScore(marksObtained);
	                jwScoreObj.setJobWiseMaxScore(totalScore);
	                jwScoreObj.setUpdatedDateTime(new Date());
	                jobWiseConsolidatedTeacherScoreDAO.makePersistent(jwScoreObj);
	            }
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}