
package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.districtassessment.DistrictAssessmentDetail;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import tm.bean.assessment.AssessmentQuestions;
import tm.bean.assessment.AssessmentSections;
import tm.bean.districtassessment.DistrictAssessmentQuestions;
import tm.bean.districtassessment.DistrictAssessmentSection;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictAssessmentQuestionsDAO extends GenericHibernateDAO<DistrictAssessmentQuestions, Integer> 
{
	public DistrictAssessmentQuestionsDAO() {
		super(DistrictAssessmentQuestions.class);
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<DistrictAssessmentQuestions> getDistrictAssessmentQuestions(DistrictAssessmentDetail districtAssessmentDetail)
	{
		List<DistrictAssessmentQuestions> districtAssessmentQuestionsList = new ArrayList<DistrictAssessmentQuestions>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			Criterion criterion = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);
			Criterion criterion1 = Restrictions.eq("status", "A");

			criteria.add(criterion);
			criteria.add(criterion1);

			districtAssessmentQuestionsList = criteria.list();
			//System.out.println("LLLLLLLLLLL : "+assessmentQuestions.size());

		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return districtAssessmentQuestionsList;
	}

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<DistrictAssessmentQuestions> getDistrictAssessmentQuestions(DistrictAssessmentDetail districtAssessmentDetail,DistrictAssessmentSection districtAssessmentSection)
	{
		List<DistrictAssessmentQuestions> districtAssessmentQuestionsList = new ArrayList<DistrictAssessmentQuestions>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			Criterion criterion = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);
			Criterion criterion1 = Restrictions.eq("status", "A");
			Criterion criterion2 = Restrictions.eq("districtAssessmentSection", districtAssessmentSection);
			
			criteria.add(criterion);
			criteria.add(criterion1);
			criteria.add(criterion2);
			
			districtAssessmentQuestionsList = criteria.list();
			//System.out.println("LLLLLLLLLLL : "+assessmentQuestions.size());

		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return districtAssessmentQuestionsList;
	}
	
	
	public List<DistrictAssessmentQuestions> getSectionQuestions(DistrictAssessmentSection districtAssessmentSection)
	{
		List<DistrictAssessmentQuestions> assessmentQuestions = null;

		try {
			Criterion criterion = Restrictions.eq("districtAssessmentSection", districtAssessmentSection);
			assessmentQuestions = findByCriteria(criterion);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return assessmentQuestions;
	}
}
