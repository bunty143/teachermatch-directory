package tm.dao.districtassessment;

import tm.bean.districtassessment.CenterMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CenterMasterDAO extends GenericHibernateDAO<CenterMaster, Integer> 
{
	public CenterMasterDAO() {
		super(CenterMaster.class);
	}

}
