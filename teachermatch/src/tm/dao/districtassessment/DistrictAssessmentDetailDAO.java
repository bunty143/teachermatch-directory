package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictAssessmentDetailDAO extends GenericHibernateDAO<DistrictAssessmentDetail, Integer> 
{
	public DistrictAssessmentDetailDAO() {
		super(DistrictAssessmentDetail.class);
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public DistrictAssessmentDetail findAssessmentDetail(Integer districtAssessmentId) 
	{
		DistrictAssessmentDetail districtAssessment =null;
		List<DistrictAssessmentDetail> districtAssessmentDetailList=new ArrayList<DistrictAssessmentDetail>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("status", "A");
			Criterion criterion1 = Restrictions.eq("districtAssessmentId",districtAssessmentId);
			criteria.add(criterion);
			criteria.add(criterion1);
			districtAssessmentDetailList = criteria.list();
			if(districtAssessmentDetailList.size()>0){
				districtAssessment=districtAssessmentDetailList.get(0);
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return districtAssessment;
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<DistrictAssessmentDetail> findByDistrict(DistrictMaster districtMaster)
	{ 
		List<DistrictAssessmentDetail> districtAMtList = new ArrayList<DistrictAssessmentDetail>();
		
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("districtMaster", districtMaster);
			
			criteria.add(criterion);
			
			districtAMtList = criteria.list();
			
			
		}catch(Exception e)
		{e.printStackTrace();}
		
		
		return districtAMtList;
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<DistrictAssessmentDetail> findByDistAssessmentIds(List  lstDASMTIdArray)
	{ 
		List<DistrictAssessmentDetail> districtAMtList = new ArrayList<DistrictAssessmentDetail>();
		
		try
		{
			if(lstDASMTIdArray.size()>0)
			{
				Criterion criterion1 	= 	Restrictions.in("districtAssessmentId",lstDASMTIdArray);
				districtAMtList =findByCriteria(criterion1);
			}
	
		}catch(Exception e)
		{e.printStackTrace();}
		
		
		return districtAMtList;
	}
}
