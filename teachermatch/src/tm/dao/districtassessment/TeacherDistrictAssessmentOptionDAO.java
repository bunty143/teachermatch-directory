package tm.dao.districtassessment;

import tm.bean.districtassessment.TeacherDistrictAssessmentOption;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherDistrictAssessmentOptionDAO extends GenericHibernateDAO<TeacherDistrictAssessmentOption, Integer>{

	public TeacherDistrictAssessmentOptionDAO() {
		super(TeacherDistrictAssessmentOption.class);
	}
}
