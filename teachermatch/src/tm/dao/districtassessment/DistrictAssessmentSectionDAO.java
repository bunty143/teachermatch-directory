package tm.dao.districtassessment;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.DistrictAssessmentSection;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictAssessmentSectionDAO extends GenericHibernateDAO<DistrictAssessmentSection, Integer> 
{

	public DistrictAssessmentSectionDAO() {
		super(DistrictAssessmentSection.class);
	}
	
	@Transactional(readOnly=true)
	public List<DistrictAssessmentSection> getSectionsByDistrictAssessmentId(DistrictAssessmentDetail districtAssessmentDetail)
	{
		List<DistrictAssessmentSection> districtAssessmentSections = null;

		Criterion criterion = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);
		districtAssessmentSections = findByCriteria(criterion);

		return districtAssessmentSections;	
	}
	
	@Transactional(readOnly=true)
	public List<DistrictAssessmentSection> getRNDSectionsByDistrictAssessmentId(DistrictAssessmentDetail districtAssessmentDetail)
	{
		List<DistrictAssessmentSection> districtAssessmentSections = null;

		Criterion criterion = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);
		Criterion criterion1 = Restrictions.sqlRestriction("1=1 order by rand()");
		//Restrictions.sqlRestriction("1=1 order by rand()");
		districtAssessmentSections = findByCriteria(criterion,criterion1);

		return districtAssessmentSections;	
	}
	
	
}
