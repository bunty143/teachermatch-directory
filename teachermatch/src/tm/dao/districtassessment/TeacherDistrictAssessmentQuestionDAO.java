package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.districtassessment.TeacherDistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentQuestion;
import tm.bean.districtassessment.TeacherDistrictSectionDetail;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherDistrictAssessmentQuestionDAO extends GenericHibernateDAO<TeacherDistrictAssessmentQuestion, Long>{

	public TeacherDistrictAssessmentQuestionDAO() {
		super(TeacherDistrictAssessmentQuestion.class);
	}
	
	@Transactional(readOnly=false)
	public
	Map<Integer,List<TeacherDistrictAssessmentQuestion>> getQuestion(JobOrder jobOrder)
	{
		Map<Integer,List<TeacherDistrictAssessmentQuestion>> dSPQMap=new HashMap<Integer, List<TeacherDistrictAssessmentQuestion>>();
		List<TeacherDistrictAssessmentQuestion> lstDistSpecQuestions= new ArrayList<TeacherDistrictAssessmentQuestion>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
			Criterion criterion3 = Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
			Criterion criterion4 = Restrictions.eq("status","A");
			lstDistSpecQuestions = findByCriteria(criterion1,criterion3,criterion4);
			int flag=0;
			if(lstDistSpecQuestions.size()==0){
				Criterion criterion6 = Restrictions.isNull("jobOrder");
				lstDistSpecQuestions = findByCriteria(criterion2,criterion3,criterion4,criterion6);
				flag=1;
			}
			if(lstDistSpecQuestions.size()==0){
				Criterion criterion5 = Restrictions.isNull("jobCategoryMaster");
				Criterion criterion6 = Restrictions.isNull("jobOrder");
				lstDistSpecQuestions = findByCriteria(criterion3,criterion4,criterion5,criterion6);
				flag=2;
			}
			dSPQMap.put(flag, lstDistSpecQuestions);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return dSPQMap;		
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<TeacherDistrictAssessmentQuestion> getTeacherDistrictAssessmentQuestions(TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail,TeacherDistrictSectionDetail teacherDistrictSectionDetail)
	{
		List<TeacherDistrictAssessmentQuestion> teacherDistrictAssessmentQuestionsList = new ArrayList<TeacherDistrictAssessmentQuestion>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			Criterion criterion = Restrictions.eq("teacherDistrictAssessmentDetail", teacherDistrictAssessmentDetail);
			Criterion criterion2 = Restrictions.eq("teacherDistrictSectionDetail", teacherDistrictSectionDetail);
			
			criteria.add(criterion);
			criteria.add(criterion2);
			
			teacherDistrictAssessmentQuestionsList = criteria.list();
			//System.out.println("LLLLLLLLLLL : "+assessmentQuestions.size());

		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentQuestionsList;
	}
	
	@Transactional(readOnly=false)
	public void updateQuestionDone(List<Long> teacherDistrictAssessmentQuestionIdIds){
		try {
			
			String hql = "update TeacherDistrictAssessmentQuestion set isAttempted=1 WHERE teacherDistrictAssessmentQuestionId IN (:teacherDistrictAssessmentQuestionIdIds)";
			Query query = null;
			Session session = getSession();

			query = session.createQuery(hql);
			query.setParameterList("teacherDistrictAssessmentQuestionIdIds", teacherDistrictAssessmentQuestionIdIds);
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}
}
