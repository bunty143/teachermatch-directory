package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.DistrictAssessmentJobRelation;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictAssessmentJobRelationDAO extends GenericHibernateDAO<DistrictAssessmentJobRelation, Integer> 
{

	public DistrictAssessmentJobRelationDAO() {
		super(DistrictAssessmentJobRelation.class);
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<DistrictAssessmentJobRelation> findRelationByJobCategory(JobOrder jobOrder) 
	{
		List<DistrictAssessmentJobRelation> districtAssessmentJobRelation = new ArrayList<DistrictAssessmentJobRelation>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("status", "A");
			Criterion criterion1 = Restrictions.eq("jobCategoryMaster", jobOrder.getJobCategoryMaster());

			criteria.add(criterion);
			criteria.add(criterion1);
			districtAssessmentJobRelation = criteria.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return districtAssessmentJobRelation;

	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<DistrictAssessmentJobRelation> findRelationByJobOrder(JobOrder jobOrder) 
	{
		List<DistrictAssessmentJobRelation> districtAssessmentJobRelations = null;
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion2 = Restrictions.eq("status", "A");
			criteria.add(criterion1);
			criteria.add(criterion2);
			districtAssessmentJobRelations = criteria.list();
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return districtAssessmentJobRelations;

	}
	
	@Transactional(readOnly=false)
	public List<DistrictAssessmentJobRelation> getDAJR(JobOrder jobOrder)
	{
		List<DistrictAssessmentJobRelation> lstDistrictAssessmentJobRelation = new ArrayList<DistrictAssessmentJobRelation>();
		try
		{
			Criterion criterion1 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
			lstDistrictAssessmentJobRelation = findByCriteria(criterion1);
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictAssessmentJobRelation;
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<DistrictAssessmentDetail> findDistrictAssessmentDetailByJobOrder(JobOrder jobOrder) 
	{
		List<DistrictAssessmentDetail> districtAssessmentDetailList = new ArrayList<DistrictAssessmentDetail>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("status", "A");
			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			criteria.add(criterion);
			criteria.add(criterion1);
			criteria.createCriteria("districtAssessmentDetail").addOrder(Order.asc("districtAssessmentId")).add(criterion);
			criteria.setProjection(Projections.groupProperty("districtAssessmentDetail"));
			districtAssessmentDetailList = criteria.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return districtAssessmentDetailList;
	}
}
