package tm.dao.districtassessment;

import tm.bean.districtassessment.DistrictAssessmentQuestionOptions;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictAssessmentQuestionOptionsDAO extends GenericHibernateDAO<DistrictAssessmentQuestionOptions, Integer> 
{
	public DistrictAssessmentQuestionOptionsDAO() {
		super(DistrictAssessmentQuestionOptions.class);
	}
}
