package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.districtassessment.AssessmentsPriorityMaster;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentsPriority;
import tm.dao.generic.GenericHibernateDAO;

public class AssessmentsPriorityMasterDAO extends GenericHibernateDAO<AssessmentsPriorityMaster, Integer> 
{
	public AssessmentsPriorityMasterDAO() {
		super(AssessmentsPriorityMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<AssessmentsPriorityMaster> findByPriorityValue(int value)
	{
		List<AssessmentsPriorityMaster> priorityList=new ArrayList<AssessmentsPriorityMaster>();
		try {
			Criterion criterion1 = Restrictions.eq("priorityId", value);			
			priorityList = findByCriteria(criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return priorityList;
	}
}
