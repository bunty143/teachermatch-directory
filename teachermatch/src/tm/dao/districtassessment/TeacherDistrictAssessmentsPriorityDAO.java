package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentsPriority;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherDistrictAssessmentsPriorityDAO extends GenericHibernateDAO<TeacherDistrictAssessmentsPriority, Integer> 
{
	public TeacherDistrictAssessmentsPriorityDAO() {
		super(TeacherDistrictAssessmentsPriority.class);
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentsPriority> findByDistrictAssessment(DistrictAssessmentDetail districtAssessmentDetail)
	{
		List<TeacherDistrictAssessmentsPriority> priorDays=new ArrayList<TeacherDistrictAssessmentsPriority>();
		try {
			Criterion criterion1 = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);			
			priorDays = findByCriteria(criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return priorDays;
	}
}
