package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.districtassessment.AssessmentsPriorityMaster;
import tm.bean.districtassessment.PriorityAssessmentDays;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class PriorityAssessmentDaysDAO extends GenericHibernateDAO<PriorityAssessmentDays, Integer> 
{
	public PriorityAssessmentDaysDAO() {
		super(PriorityAssessmentDays.class);
	}

	@Transactional(readOnly=false)
	public List<PriorityAssessmentDays> getPriorityDaysByPriorityAndDistrict(AssessmentsPriorityMaster assessmentsPriorityMaster,DistrictMaster districtMaster)
	{
		List<PriorityAssessmentDays> priorDays=new ArrayList<PriorityAssessmentDays>();
		try {
			Criterion criterion1 = Restrictions.eq("priority", assessmentsPriorityMaster);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			priorDays=findByCriteria(criterion1,criterion2);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return priorDays;
	}
	
	@Transactional(readOnly=false)
	public List<PriorityAssessmentDays> getPriorityDaysByDistrict(DistrictMaster districtMaster)
	{
		List<PriorityAssessmentDays> priorDays=new ArrayList<PriorityAssessmentDays>();
		try {
			Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
			//Criterion criterion2 = Restrictions.eq("status","A");
			//priorDays=findByCriteria(criterion1,criterion2);
			priorDays=findByCriteria(criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return priorDays;
	}
	
	@Transactional(readOnly=false)
	public List<PriorityAssessmentDays> getPriorityDaysByDistrictAndDate(DistrictMaster districtMaster,Date date)
	{
		List<PriorityAssessmentDays> priorDays=new ArrayList<PriorityAssessmentDays>();
		try {
			Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
			//Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("preferredDates",date);
			priorDays=findByCriteria(criterion1,criterion3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return priorDays;
	}
	
	@Transactional(readOnly=false)
	public List<PriorityAssessmentDays> getPriorityDaysByDistrictAndWeekly(DistrictMaster districtMaster, String startDate, String endDate)
	{
		List<PriorityAssessmentDays> priorDays=new ArrayList<PriorityAssessmentDays>();
		try{
			java.util.Date date1 =null;
			java.util.Date date2 =null;
			
			if(startDate!=null && !startDate.equals(""))
				date1 = Utility.getCurrentDateFormart(startDate);
			if(endDate!=null && !endDate.equals(""))
				date2 = Utility.getCurrentDateFormart(endDate);
			
			Criterion criterion1 = Restrictions.between("preferredDates", date1, date2);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion = Restrictions.and(criterion1, criterion2);
			priorDays=findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return priorDays;
	}
}
