package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.TeacherDetail;
import tm.bean.districtassessment.AssessmentNotesHistory;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.dao.generic.GenericHibernateDAO;

public class AssessmentNotesHistoryDAO extends GenericHibernateDAO<AssessmentNotesHistory, Integer> 
{
	public AssessmentNotesHistoryDAO() {
		super(AssessmentNotesHistory.class);
	}

	@Transactional(readOnly=false)
	public List<AssessmentNotesHistory> findDistrictAssessmentNotesByTeacher(TeacherDetail teacherDetail,DistrictAssessmentDetail districtAssessmentDetail)
	{
		List<AssessmentNotesHistory> assessmentNotesHistoryList = new ArrayList<AssessmentNotesHistory>();
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);
			assessmentNotesHistoryList = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assessmentNotesHistoryList;

	}
}
