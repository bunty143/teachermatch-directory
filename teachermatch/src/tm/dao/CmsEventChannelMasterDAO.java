package tm.dao;

import tm.bean.CmsEventChannelMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CmsEventChannelMasterDAO extends GenericHibernateDAO<CmsEventChannelMaster, Integer>{

	public CmsEventChannelMasterDAO() {
		super(CmsEventChannelMaster.class);
	}
}
