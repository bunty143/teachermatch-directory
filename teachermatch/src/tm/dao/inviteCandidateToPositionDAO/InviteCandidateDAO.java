package tm.dao.inviteCandidateToPositionDAO;

import org.springframework.transaction.annotation.Transactional;

import tm.bean.invitecandidatetoposition.InviteCandidateToPositions;
import tm.dao.generic.GenericHibernateDAO;

public class InviteCandidateDAO extends GenericHibernateDAO<InviteCandidateToPositions, Integer> 
{
	public InviteCandidateDAO() 
	{
		super(InviteCandidateToPositions.class);
	}
	
	@Transactional(readOnly=false)
	public void saveOrUpdateInviteCandidateToPosition(InviteCandidateToPositions invitecandidate)
	{
		try 
		{
			makeTransient(invitecandidate);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
}
