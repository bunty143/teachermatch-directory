package tm.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.dao.generic.GenericHibernateDAO;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.engine.SessionFactoryImplementor;



public class TeacherAssessmentDetailDAO extends GenericHibernateDAO<TeacherAssessmentdetail, Integer>   
{
	public TeacherAssessmentDetailDAO() {
		super(TeacherAssessmentdetail.class);
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to check active Base Assessments.
	 */
		@Transactional(readOnly=true)
		public List<TeacherAssessmentdetail> getTeacherAssessmentdetails(AssessmentDetail assessmentDetail,TeacherDetail teacherDetail)
		{
			List<TeacherAssessmentdetail> teacherAssessmentdetails = null;
			try {
				
				Calendar cal = Calendar.getInstance();

				cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
				Date sDate=cal.getTime();
				cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
				Date eDate=cal.getTime();
				
				Criterion criterion = Restrictions.eq("assessmentDetail", assessmentDetail);
				Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				Order order = Order.desc("teacherAssessmentId");
				
				if(assessmentDetail.getAssessmentType()==1)
				{
					Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
					teacherAssessmentdetails = findByCriteria(order,criterion,criterion1,criterion2);
				}else
					teacherAssessmentdetails = findByCriteria(order,criterion,criterion1);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return teacherAssessmentdetails;
		}
		
		@Transactional(readOnly=true)
		public List<TeacherAssessmentdetail> getTeacherAssessmentdetails(AssessmentDetail assessmentDetail)
		{
			List<TeacherAssessmentdetail> teacherAssessmentdetails = null;
			try {			
				Criterion criterion = Restrictions.eq("assessmentDetail", assessmentDetail);
				Order order = Order.desc("teacherAssessmentId");
				
				teacherAssessmentdetails = findByCriteria(order,criterion);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return teacherAssessmentdetails;
		}
		
		/**
		 * @author Amit Chaudhary
		 * @param teacherDetailList
		 * @param assessmentDetail
		 * @param assessmentType
		 * @param assessmentTakenCount
		 * @return teacherAssessmentdetailList
		 */
		@Transactional(readOnly=true)
		public List<TeacherAssessmentdetail> getTeacherAssessmentdetails(List<TeacherDetail> teacherDetailList,AssessmentDetail assessmentDetail,Integer assessmentType,Integer assessmentTakenCount)
		{
			List<TeacherAssessmentdetail> teacherAssessmentdetailList = null;
			try {
				Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetailList);
				Criterion criterion2 = Restrictions.eq("assessmentDetail", assessmentDetail);
				Criterion criterion3 = Restrictions.eq("assessmentType", assessmentType);
				Criterion criterion4 = Restrictions.eq("assessmentTakenCount", assessmentTakenCount);
				
				teacherAssessmentdetailList = findByCriteria(criterion1,criterion2,criterion3,criterion4);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return teacherAssessmentdetailList;
		}
		
		@Transactional(readOnly=false)
		@SuppressWarnings("unchecked")
		public  List<String[]> smartPracticesAssessment(int bid,int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName,String fname,String lname,String email,String assessDate) 
		{
			System.out.println("fname:::on daoooo:::::::::"+fname);
			System.out.println("fname:::on daoooo:::::::::"+lname);
			System.out.println("fname:::on daoooo:::::::::"+email);
			System.out.println("assessDate :::::::::::::::"+assessDate);
			System.out.println("branch id::::"+bid);
			List<String[]> lst=new ArrayList<String[]>(); 
			Session session = getSession();
			String sql = "";
			String queryPs="";
			String querySet="";
			int n=1;
			 Connection connection =null;
			try {
			    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
			    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
			      connection = connectionProvider.getConnection();
				
				
		        System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);
		      
				
		       if(sortColomnName.equalsIgnoreCase("headquarters"))
					sortColomnName = "Headquarters";
				else if(sortColomnName.equalsIgnoreCase("branch"))
					sortColomnName = "Branch";
				else if(sortColomnName.equalsIgnoreCase("teacherId"))
					sortColomnName = "Teacher_Id";
				else if(sortColomnName.equalsIgnoreCase("lastName"))
					sortColomnName = "Applicant_Last_Name";
				else if(sortColomnName.equalsIgnoreCase("firstName"))
					sortColomnName = "Applicant_First_Name";
				else if(sortColomnName.equalsIgnoreCase("assessment"))
					sortColomnName = "Assessment";
				else if(sortColomnName.equalsIgnoreCase("Assessmentstatus"))
					sortColomnName = "Assessment_Status";
				else if(sortColomnName.equalsIgnoreCase("assessmentTime"))
					sortColomnName = "Assessment_Time";
				else if(sortColomnName.equalsIgnoreCase("score"))
					sortColomnName = "Score";
				else if(sortColomnName.equalsIgnoreCase("passfail"))
					sortColomnName = "Pass/Fail";
				else if(sortColomnName.equalsIgnoreCase("pdp"))
					sortColomnName = "PD_Progress";
						      
				String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
				System.out.println("orderbyorderby::::::"+orderby);
				if(bid!=0)
				{
				 queryPs="and bm.branchid=?";
				 n=2;
				}				
					if(fname!=null&&!fname.equals(""))
					{
						queryPs=queryPs+"and td.firstname=?";
						
					}
					if(lname!=null&&!lname.equals(""))
					{
						queryPs=queryPs+"and td.lastname=?";

					}
					if(email!=null&&!email.equals(""))
					{
						queryPs=queryPs+"and td.emailAddress=?";

					}	
					if(assessDate!=null && !assessDate.equals(""))
					{
						queryPs=queryPs+"and tas.createdDateTime> ? and tas.createdDateTime< ?";
					}
				 
				 sql=" SELECT  distinct "+
				 "	hm.headquarterName as `Headquarters`, "+
				 "   bm.branchName as `Branch`, "+
				 "   jft.teacherid as `Teacher_Id`, "+
				 "   td.lastname as `Applicant_Last_Name`, "+
				 "   td.firstname as `Applicant_First_Name`, "+
				"	tads.assessmentname as `Assessment`, "+
				 "   case when sm.status is null then 'Not Started' "+
				"    else sm.status "+
				"    end as `Assessment_Status`, "+
				"    tas.createdDateTime as `Assessment_Time`, "+
				"    rdfd.score as `Score`, "+
				"    Case when tas.pass='P' then 'Pass' "+
				"         when tas.pass='F' then 'Fail' "+
				"         when tas.statusId='3' then 'In Progress' "+
				"         when tas.statusId='9' then 'Timed Out' "+
				"         else 'Not Started' "+
				"	end as `Pass/Fail`, "+
				"    case when sp.currentLessonNo/sp.totalLessons is null then 'Not Started' "+
				"    else concat(round((sp.currentLessonNo/sp.totalLessons)*100,0),'%') "+
				"    end as `PD_Progress` "+
				" from teacherdetail td  "+
				" inner join jobforteacher jft on jft.teacherid=td.teacherid "+
				" inner join joborder jo on jft.jobid=jo.jobid "+
				" inner join jobcategorymaster jcm on jo.jobCategoryId=jcm.jobCategoryId and jcm.preHireSmartPractices=1 "+
				" left join branchmaster bm on jo.branchid=bm.branchid "+
				" inner join headquartermaster hm on jo.headquarterid=hm.headquarterid "+
				" left join teacherassessmentdetails tads on tads.teacherid=jft.teacherid "+
				" left join teacherassessmentstatus tas on tads.teacherid=tas.teacherid  "+
				"	and tads.teacherAssessmentId=tas.teacherassessmentId "+
				" left join statusmaster sm on tas.statusId=sm.statusId "+
				" left join rawdatafordomain rdfd on rdfd.teacherid=tas.teacherid "+
				"	and rdfd.teacherassessmentid=tas.teacherassessmentid "+
				"	and rdfd.domainid=12 "+
				" left join assessmentdetail ad on ad.assessmentid=tads.assessmentid and ad.assessmentgroupid=1 "+
				" left join spinboundapicallrecord sp on td.teacherid=sp.teacherId and sp.totalLessons is not null "+
				" WHERE jft.status!=8 and tads.assessmentType = 3  "+queryPs+
				""+orderby;
							 
			PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);
			if(bid!=0)
			{
				System.out.println("inside bidddddd");
				ps.setString(1, bid+"");
				
			}
			if(fname!=null&&!fname.equals(""))
			{
				ps.setString(n, fname);				
				n++;
			
			}
			if(lname!=null&&!lname.equals(""))
			{				
				ps.setString(n, lname);
				n++;			
			}
			if(email!=null&&!email.equals(""))
			{
				ps.setString(n, email);	
			
			}
			if(assessDate!=null&&!assessDate.equals(""))
			{               
					String month=assessDate.substring(0, 2);
					String day=assessDate.substring(3, 5);
					String year=assessDate.substring(6, 10);
					String finalDate1=year+"-"+month+"-"+day+" 00:00:00";
					String finalDate2=year+"-"+month+"-"+day+" 23:59:59";
					
				 	ps.setString(n, finalDate1);
				 	ps.setString(n+1, finalDate2);
					
			}
						
			ResultSet rs=ps.executeQuery();
				
				if(rs.next()){				
					do{
						final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
						for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
						   {
							allInfo[i-1]=rs.getString(i);
						   }
						lst.add(allInfo);
						
					}while(rs.next());
				}
				else{
					System.out.println("Record not found.");
				}
				
				return lst;
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				if(connection!=null)
					try {
						connection.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			return null;
		}
		
		@Transactional(readOnly=true)
		public List<TeacherAssessmentdetail> findByTeacherAssessmentIds(List<Integer> teacherAssessmentIdList){
			List<TeacherAssessmentdetail> teacherAssessmentdetailList = new ArrayList<TeacherAssessmentdetail>();
			try{
				Criterion criterion = Restrictions.in("teacherAssessmentId",teacherAssessmentIdList);
				teacherAssessmentdetailList =	findByCriteria(criterion);
			} 
			catch (Exception e){
				e.printStackTrace();
			}
			return teacherAssessmentdetailList;
		}
}
