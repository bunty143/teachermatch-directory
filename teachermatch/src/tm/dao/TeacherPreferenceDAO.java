package tm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.TeacherPreference;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherPreferenceDAO extends GenericHibernateDAO<TeacherPreference, Long> 
{
	public TeacherPreferenceDAO() 
	{
		super(TeacherPreference.class);
	}
	
	@Transactional(readOnly=false)
	public TeacherPreference findPrefByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherPreference> lstPreferences = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			lstPreferences = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstPreferences==null || lstPreferences.size()==0)
		return null;
		else
		return lstPreferences.get(0);
	}
	
	
	@Transactional(readOnly=false)
	public List<TeacherPreference> findPrefForTeachers(List<TeacherDetail> teacherDetails)
	{
		List<TeacherPreference> lstPreferences = null;
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherId",teacherDetails);
			lstPreferences = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstPreferences;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findRegionByJobForTeacher(String regionId)
	{
         List <TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();
         String regionIdFirst=regionId+"|",regionIdSecond="|"+regionId+"|",regionIdThird="|"+regionId;
         try{
        	Session session = getSession();
 			Criteria criteria = session.createCriteria(getPersistentClass());
 			Criterion criterion1 = Restrictions.like("regionId", regionIdFirst,MatchMode.START);
 			Criterion criterion2 = Restrictions.like("regionId", regionIdThird,MatchMode.END);
 			Criterion criterion3 = Restrictions.ilike("regionId","%"+regionIdSecond.trim()+"%");
 			Criterion criterion4 = Restrictions.eq("regionId",regionId);
 			Criterion criterion5 = Restrictions.or(criterion1, criterion2);
 			Criterion criterion6 = Restrictions.or(criterion3, criterion4);
 			Criterion criterion7 = Restrictions.or(criterion5, criterion6);
 			criteria.add(criterion7);
 			criteria.setProjection(Projections.groupProperty("teacherId"));
        	lstTeacherDetail =  criteria.list();
         }catch(Exception e){
        	 e.printStackTrace();
         }
		 return lstTeacherDetail;
	}
	
	
	// Optimization for candidate Pool search replacement of findRegionByJobForTeacher
	
	@Transactional(readOnly=false)
	public List<Integer> findRegionByJobForTeacher_Op(String regionId,List<Integer> teachersId)
	{
         List <Integer> lstTeacherDetail = new ArrayList<Integer>();
         String regionIdFirst=regionId+"|",regionIdSecond="|"+regionId+"|",regionIdThird="|"+regionId;
         if(teachersId!=null && teachersId.size()>0)
	         try{
	        	Session session = getSession();
	 			Criteria criteria = session.createCriteria(getPersistentClass());
	 			Criterion criterion1 = Restrictions.like("regionId", regionIdFirst,MatchMode.START);
	 			Criterion criterion2 = Restrictions.like("regionId", regionIdThird,MatchMode.END);
	 			Criterion criterion3 = Restrictions.ilike("regionId","%"+regionIdSecond.trim()+"%");
	 			Criterion criterion4 = Restrictions.eq("regionId",regionId);
	 			Criterion criterion5 = Restrictions.or(criterion1, criterion2);
	 			Criterion criterion6 = Restrictions.or(criterion3, criterion4);
	 			Criterion criterion7 = Restrictions.or(criterion5, criterion6);
	 			criteria.add(criterion7);
	 			
	 			criteria.add(Restrictions.in("teacherId.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
	        	lstTeacherDetail =  criteria.list();
	         }catch(Exception e){
	        	 e.printStackTrace();
	         }
		 return lstTeacherDetail;
	}
 	@Transactional(readOnly=false)
	 public TeacherPreference findPrefByTeacherd_Op(TeacherDetail teacherDetail)
	 {
		TeacherPreference teacherPreferenceObj= new TeacherPreference();
	  try{
		  teacherPreferenceObj = (TeacherPreference)session.createCriteria(TeacherPreference.class).add(Restrictions.eq("teacherId", teacherDetail)).uniqueResult();
	           }
	   catch(Exception e){
	    e.printStackTrace();
	  }
	   return teacherPreferenceObj;
	  }
 
	

	@Transactional(readOnly=true)
	public TeacherPreference findPrefByTeacher_Op(TeacherDetail teacherDetail)
	{
		TeacherPreference teacherPreference=new TeacherPreference();
		try {
			Session session = getSession();
			if(teacherDetail!=null)
				teacherPreference = (TeacherPreference)session.createCriteria(TeacherPreference.class).add(Restrictions.eq("teacherId", teacherDetail)).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			 
		}
	 
		
		if(teacherPreference==null )
			return null;
			else
			return teacherPreference ;
        
	}
}
