package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictPanelMaster;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictPanelMasterDAO extends GenericHibernateDAO<DistrictPanelMaster, Integer> {
	DistrictPanelMasterDAO()
	{
		super(DistrictPanelMaster.class);
	}
	
	@Transactional(readOnly=true)
	public List<DistrictPanelMaster> getPanelsByDistrict(DistrictMaster districtMaster)
	{
	
		List <DistrictPanelMaster> districtPanelMasters = new ArrayList<DistrictPanelMaster>();
		try 
		{	         
			 Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			 Criterion criterion2 = Restrictions.eq("status","A");
			 districtPanelMasters = findByCriteria(Order.asc("panelName"),criterion1, criterion2);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return districtPanelMasters;
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<DistrictPanelMaster> checkDuplicatePanelName(String panelName,Integer panelId,DistrictMaster districtMaster) 
	{
		Session session = getSession();

		if(panelId!=null)
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.eq("panelName", panelName)) 
			.add(Restrictions.not(Restrictions.in("panelId",new Integer[]{panelId})))
			.add(Restrictions.eq("districtMaster", districtMaster)) 
			.list();
			System.out.println(" getPersistentClass  "+result);
			return result;

		}
		else
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.eq("panelName", panelName)) 
			.add(Restrictions.eq("districtMaster", districtMaster)) 
			.list();
			return result;
		}
	}
}
