package tm.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.CertificationStatusMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherCertificateDAO extends GenericHibernateDAO<TeacherCertificate, Integer> 
{
	public TeacherCertificateDAO() 
	{
		super(TeacherCertificate.class);
	}


	@Transactional(readOnly=false)
	public List<TeacherCertificate> findCertificateByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherCertificate> lstCertificate = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstCertificate = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstCertificate;
	}

	/* =======  For Sorting on Certification Page ======== */
	@Transactional(readOnly=false)
	public List<TeacherCertificate> findSortedCertificateByTeacher(Order  sortOrderStrVal,	TeacherDetail teacherDetail)
	{
		List<TeacherCertificate> lstCertificate = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstCertificate = findByCriteria(sortOrderStrVal,criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstCertificate;
	}

	@Transactional(readOnly=false)
	public Map<Integer, Boolean> findActiveCertificateStatus(List<TeacherDetail> teacherDetails)
	{
		List<TeacherCertificate> lstTeacherCertificate= null;
		Map<Integer,Boolean> mapTeacherCertificate= new TreeMap<Integer, Boolean>();
		try 
		{
			if(teacherDetails.size()>0)
			{
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				lstTeacherCertificate = findByCriteria(criterion1);		
				for(TeacherCertificate tCertificate : lstTeacherCertificate){
					mapTeacherCertificate.put(tCertificate.getTeacherDetail().getTeacherId(), true);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		return mapTeacherCertificate;
	}
	@Transactional(readOnly=false)
	public List<TeacherCertificate> findCertificateByTeacher(List<TeacherDetail> teacherDetailList)
	{
		List <TeacherCertificate> lstTeacherCertificate = new ArrayList<TeacherCertificate>();
	    try
	    {
	    	Criterion criterion = Restrictions.in("teacherDetail",teacherDetailList);
			lstTeacherCertificate = findByCriteria(criterion);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    return lstTeacherCertificate;
	}
	@Transactional(readOnly=true)
	public List<TeacherDetail> findCertificateByJobForTeacher(List<CertificateTypeMaster> certificateTypeMasterList)
	{
	         List <TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();
	         try{
	        	Session session = getSession();
	 			Criteria criteria = session.createCriteria(getPersistentClass());
	 			Criterion criterion2 = Restrictions.in("certificateTypeMaster",certificateTypeMasterList);
	 			criteria.add(criterion2);
	 			criteria.setProjection(Projections.groupProperty("teacherDetail"));
	        	lstTeacherDetail =  criteria.list();
	         }catch(Exception e){
	        	 e.printStackTrace();
	         }
			 return lstTeacherDetail;
	}

	
	@Transactional(readOnly=false)
	public List<TeacherCertificate> findAttachPathOfCertification(TeacherDetail teacherDetail)
	{
		List<TeacherCertificate> lstCertificate = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.disjunction()
	        .add(Restrictions.isNull("pathOfCertification"))
	        .add(Restrictions.eq("pathOfCertification", ""));
			//certUrl
			Criterion criterion3 = Restrictions.disjunction()
	        .add(Restrictions.isNull("certUrl"))
	        .add(Restrictions.eq("certUrl", ""));
			lstCertificate = findByCriteria(criterion1,criterion2,criterion3);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstCertificate;
	}
	@Transactional(readOnly=false)
	public boolean findDuplicateCertificateTypeForTeacher(TeacherDetail teacherDetail,CertificateTypeMaster certificateTypeMaster,TeacherCertificate teacherCertificate,int addEditFlag)
	{
		List<TeacherCertificate> lstCertificate = new ArrayList<TeacherCertificate>();
		try 
		{
			if(certificateTypeMaster!=null){
				Criterion criterion1=Restrictions.eq("certificateTypeMaster", certificateTypeMaster);
				Criterion criterion2=Restrictions.eq("teacherDetail", teacherDetail);			
				if(addEditFlag==0){
					lstCertificate=findByCriteria(criterion1,criterion2);
				}else{	
					Criterion criterion3=Restrictions.ne("certId",teacherCertificate.getCertId());
					lstCertificate=findByCriteria(criterion1,criterion2,criterion3);
				}
			}else{
				return false;
			}
			if(lstCertificate.size()>0)
				return true;
			else
				return false;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return false;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherCertificate> findAttachPathOfCertificationWithNoPlanning(TeacherDetail teacherDetail,List<CertificationStatusMaster> lstCertificationStatusMaster)
	{
		List<TeacherCertificate> lstCertificate = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.disjunction()
	        .add(Restrictions.isNull("pathOfCertification"))
	        .add(Restrictions.eq("pathOfCertification", ""));
			//certUrl
			Criterion criterion3 = Restrictions.disjunction()
	        .add(Restrictions.isNull("certUrl"))
	        .add(Restrictions.eq("certUrl", ""));
			
			Criterion criterion4 = Restrictions.not(Restrictions.in("certificationStatusMaster", lstCertificationStatusMaster));
			
			lstCertificate = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstCertificate;
	}
	
	
	@Transactional(readOnly=false)
	public List<TeacherCertificate> findByTeacherDetailsObj(List<TeacherDetail> teacherDetailsObj)
	{
		List<TeacherCertificate> lstTeacherCertificates = new ArrayList<TeacherCertificate>();
		
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailsObj);
			lstTeacherCertificates = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherCertificates==null || lstTeacherCertificates.size()==0)
			return null;
		else
			return lstTeacherCertificates;
	}
	
	
	@Transactional(readOnly=false)
	public boolean findDuplicateCertificateTypeForDSPQ(TeacherDetail teacherDetail,CertificateTypeMaster certificateTypeMaster,int certId,int addEditFlag)
	{
		List<TeacherCertificate> lstCertificate = new ArrayList<TeacherCertificate>();
		try 
		{
			if(certificateTypeMaster!=null){
				Criterion criterion1=Restrictions.eq("certificateTypeMaster", certificateTypeMaster);
				Criterion criterion2=Restrictions.eq("teacherDetail", teacherDetail);			
				if(addEditFlag==0){
					lstCertificate=findByCriteria(criterion1,criterion2);
				}else{	
					Criterion criterion3=Restrictions.ne("certId",certId);
					lstCertificate=findByCriteria(criterion1,criterion2,criterion3);
				}
			}else{
				return false;
			}
			if(lstCertificate.size()>0)
				return true;
			else
				return false;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return false;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherCertificate> findCertificateByTeacher(List<TeacherDetail> teacherDetailList,CertificateTypeMaster certificateTypeMaster)
	{
		List <TeacherCertificate> teacherCertificateList = new ArrayList<TeacherCertificate>();
	    try
	    {
	    	Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailList);
	        Criterion criterion2 = Restrictions.eq("certificateTypeMaster",certificateTypeMaster);
	        teacherCertificateList = findByCriteria(criterion1,criterion2);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    return teacherCertificateList;
	}
	
	
	@Transactional(readOnly=false)
    public List<TeacherCertificate> findCertificateAscByTeacher(TeacherDetail teacherDetail)
    {
        List<TeacherCertificate> lstCertificate = null;
        try 
        {
            Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
            lstCertificate = findByCriteria(Order.asc("certificateTypeMaster"),criterion1);                     
        } 
        catch (Exception e) {
          e.printStackTrace();
        }                              
        return lstCertificate;
    }
	
	@Transactional(readOnly=true)
	public List<Integer> findTeacherByCertificates(List<Integer> certTypeIdList)
	{
		List<Integer> teacherDetailList = new ArrayList<Integer>();
		try
		{
			Session session = getSession();
			String ids = "";
			String sql = "";
			for(int i=0;i<certTypeIdList.size();i++)
			{
				if(i==0)
					ids+=Integer.toString(certTypeIdList.get(i));
				else
					ids+=", "+Integer.toString(certTypeIdList.get(i));
			}
			sql+="select a.teacherId from (select teacherId from teachercertificate where certTypeId in("+ids+"))as a group by a.teacherId having count(*)>="+certTypeIdList.size();
			System.out.println("sql======"+sql);
			Query query = session.createSQLQuery(sql);
			teacherDetailList = query.list();
			return teacherDetailList;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	@Transactional(readOnly=false)
	public Integer findIEINByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherCertificate> lstCertificate = new ArrayList<TeacherCertificate>();
		List<TeacherCertificate> lstCertificatetemp = new ArrayList<TeacherCertificate>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.ne("ieinNumber","");
			Criterion criterion3 = Restrictions.isNotNull("ieinNumber");			
			lstCertificatetemp = findByCriteria(criterion1);
			
			for (TeacherCertificate teacherCertificate : lstCertificatetemp) {
				if(teacherCertificate.getCertificationStatusMaster().getCertificationStatusId().equals(5)){
					lstCertificate.add(teacherCertificate);
				}else{
					if(teacherCertificate.getIeinNumber()!=null && !teacherCertificate.getIeinNumber().equals("")){
						lstCertificate.add(teacherCertificate);	
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstCertificate.size();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
	    public List<TeacherDetail> getTeachersCertificate(List<TeacherDetail> listTeacherDetail){
		   List<TeacherDetail> lstCertificate = null;
		   if(listTeacherDetail!=null){
	    	try{
	    		Session session = getSession();
			    Criteria criteria = session.createCriteria(getPersistentClass());
			    criteria.add(Restrictions.in("teacherDetail", listTeacherDetail));
			    criteria.setProjection(Projections.distinct(Projections.property("teacherDetail")));
			    lstCertificate= criteria.list();
	       }catch(Exception e){e.printStackTrace();}
	    	return lstCertificate;
	     }else{
	    	return null;
	    }
	  }
	//.................brajesh
	@Transactional(readOnly=true)
	public List<TeacherDetail> findTeacherCertificateByCertificateTypeMasterList(StateMaster statemaster,List<CertificateTypeMaster> certificateTypeMasterList)
	{
	         List <TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();
	         try{
	        	 
		       	 if(certificateTypeMasterList!=null && certificateTypeMasterList.size()>0){
		        	Session session = getSession();
		 			Criteria criteria = session.createCriteria(getPersistentClass());
		 			Criterion criterion1 = Restrictions.in("certificateTypeMaster",certificateTypeMasterList);
		 			Criterion criterion2 = Restrictions.eq("stateMaster",statemaster);
		 			criteria.add(criterion1);criteria.add(criterion2);
		 			criteria.setProjection(Projections.groupProperty("teacherDetail"));
		 			lstTeacherDetail =  criteria.list();
		        	 }
	         }catch(Exception e){
	        	 e.printStackTrace();
	         }
			 return lstTeacherDetail;
	}
	//.....................
	
	
	// Optimization and replacement of findTeacherCertificateByCertificateTypeMasterList
	
	@Transactional(readOnly=true)
	public List<Integer> findTeacherCertificateByCertificateTypeMasterList_Op(StateMaster statemaster,List<CertificateTypeMaster> certificateTypeMasterList)
	{
	         List <Integer> lstTeacherDetail = new ArrayList<Integer>();
	         try{
	        	 
		       	 if(certificateTypeMasterList!=null && certificateTypeMasterList.size()>0){
		        	Session session = getSession();
		 			Criteria criteria = session.createCriteria(getPersistentClass());
		 			Criterion criterion1 = Restrictions.in("certificateTypeMaster",certificateTypeMasterList);
		 			Criterion criterion2 = Restrictions.eq("stateMaster",statemaster);
		 			criteria.add(criterion1);criteria.add(criterion2);
		 			criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId"))); 
		 			lstTeacherDetail =  criteria.setProjection(Projections.property("teacherDetail.teacherId")).list();
		        	 }
	         }catch(Exception e){
	        	 e.printStackTrace();
	         }
			 return lstTeacherDetail;
	}
	
	// Optimization and replacement of findCertificateByJobForTeacher
	
	@Transactional(readOnly=true)
	public List<Integer> findCertificateByJobForTeacher_Op(List<Integer> certificateTypeMasterList,List<Integer> teachersId)
	{
	         List <Integer> lstTeacherDetail = new ArrayList<Integer>();
	         
	         if(teachersId!=null && teachersId.size()>0)
		         try{
		        	 if(teachersId!=null && teachersId.size()>0){
			        	Session session = getSession();
			 			Criteria criteria = session.createCriteria(getPersistentClass());
			 			Criterion criterion2 = Restrictions.in("certificateTypeMaster.certTypeId",certificateTypeMasterList);
			 			criteria.add(criterion2);
		
			 			criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
			 			criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
						criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId"))); 
			        	lstTeacherDetail =  criteria.list();
		        	 }
		         }catch(Exception e){
		        	 e.printStackTrace();
		         }
			 return lstTeacherDetail;
	}
	
	@Transactional(readOnly=true)
	public List<Integer> findCertificateByJobForTeacherByCertIds_Op(List<Integer> certificateTypeMasterList)
	{
	         List <Integer> lstTeacherDetail = new ArrayList<Integer>();
	         
	         if(certificateTypeMasterList!=null && certificateTypeMasterList.size()>0)
		         try{
		        	 if(certificateTypeMasterList!=null && certificateTypeMasterList.size()>0){
			        	Session session = getSession();
			 			Criteria criteria = session.createCriteria(getPersistentClass());
			 			Criterion criterion2 = Restrictions.in("certificateTypeMaster.certTypeId",certificateTypeMasterList);
			 			criteria.add(criterion2);
		
			 			criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
						criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId"))); 
			        	lstTeacherDetail =  criteria.list();
		        	 }
		         }catch(Exception e){
		        	 e.printStackTrace();
		         }
			 return lstTeacherDetail;
	}
	
	
	@Transactional(readOnly=false)
	public List<Integer> getCertificateCountByTeacherJAF(TeacherDetail teacherDetail)
	{
		List<Integer> lstTeacherCertificate= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion=Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion);
			criteria.setProjection(Projections.projectionList().add(Projections.count("teacherDetail"))) ;
			lstTeacherCertificate =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstTeacherCertificate;
		}
		
	}
}



