package tm.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherElectronicReferencesDAO extends GenericHibernateDAO<TeacherElectronicReferences, Integer> 
{
	public TeacherElectronicReferencesDAO() 
	{
		super(TeacherElectronicReferences.class);
	}

	@Transactional(readOnly=false)
	public TeacherElectronicReferences findTeacherExperienceByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferences> lstTeacherElectronicReferences = new ArrayList<TeacherElectronicReferences>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstTeacherElectronicReferences = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherElectronicReferences==null || lstTeacherElectronicReferences.size()==0)
			return null;
		else
			return lstTeacherElectronicReferences.get(0);
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeacherReferences()
	{
		try 
		{
			Session session = getSession();
			List result = session.createCriteria(getPersistentClass()) 
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherDetail"))
			).list();
			return result;
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}
	
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferences> findByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferences> lstTeacherElectronicReferences = new ArrayList<TeacherElectronicReferences>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstTeacherElectronicReferences = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		finally
		{
			return lstTeacherElectronicReferences;
		}
	}
	
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferences> findByPathOfReference(TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferences> lstTeacherElectronicReferences = new ArrayList<TeacherElectronicReferences>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.disjunction()
	        .add(Restrictions.isNull("pathOfReference"))
	        .add(Restrictions.eq("pathOfReference", ""));
			lstTeacherElectronicReferences = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		finally
		{
			return lstTeacherElectronicReferences;
		}
	}
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferences> findActiveReferencesByPathOfReference(TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferences> lstTeacherElectronicReferences = new ArrayList<TeacherElectronicReferences>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.disjunction()
	        .add(Restrictions.isNull("pathOfReference"))
	        .add(Restrictions.eq("pathOfReference", ""));
			Criterion criterion3 = Restrictions.eq("status",1);
			lstTeacherElectronicReferences = findByCriteria(criterion1,criterion2,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		finally
		{
			return lstTeacherElectronicReferences;
		}
	}
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferences> findActiveReferencesByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferences> lstTeacherElectronicReferences = new ArrayList<TeacherElectronicReferences>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status",1);
			lstTeacherElectronicReferences = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		finally
		{
			return lstTeacherElectronicReferences;
		}
	}
	
	//ashish choudhary
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferences> findActiveRdcontactedReferencesByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferences> lstTeacherElectronicReferences = new ArrayList<TeacherElectronicReferences>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status",1);
			Criterion criterion3 = Restrictions.eq("rdcontacted",true);
			lstTeacherElectronicReferences = findByCriteria(criterion1,criterion2,criterion3);	
		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		finally
		{
			
			return lstTeacherElectronicReferences;
		}
	}
	
	
	
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferences> findByTeacherDetailsObj(List<TeacherDetail> teacherDetailsObj)
	{
		List<TeacherElectronicReferences> lstteacherElectronicReferences = new ArrayList<TeacherElectronicReferences>();
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailsObj);
			lstteacherElectronicReferences = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstteacherElectronicReferences;
	}
	
	@Transactional(readOnly=false)
	public List getRef(List<TeacherDetail> teacherDetailsObj)
	{
		System.out.println("---------------- teacherDetailsObj size "+teacherDetailsObj.size());
		List lst = null;
		if(teacherDetailsObj.size() > 0){
			try 
			{
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailsObj);
				Criterion criterion2 = Restrictions.eq("status",1);
				Session session = getSession();
				lst = session.createCriteria(getPersistentClass()) 
				.add(criterion1)
				.add(criterion2)
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.count("teacherDetail")) 
				).list();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lst;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int findTotalReference(TeacherDetail teacherDetail)
	{
		int iReturnValue=0;
		if(teacherDetail!=null){
			try{
				Session session = getSession();
				String sql ="SELECT COUNT(elerefAutoId) AS teacherElect from teacherelectronicreference tElec where tElec.teacherId = "+teacherDetail.getTeacherId()+" and status = 1";
				Query query = session.createSQLQuery(sql);
				List<BigInteger> rowCount = query.list();
				if(rowCount!=null && rowCount.size()==1)
					iReturnValue=rowCount.get(0).intValue();			
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		return iReturnValue;	
		
	}
	
	@SuppressWarnings("finally")
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferences> findActiveContactedReferencesByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferences> lstTeacherElectronicReferences = new ArrayList<TeacherElectronicReferences>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status",1);
			Criterion criterion3 = Restrictions.eq("rdcontacted",true);
			lstTeacherElectronicReferences = findByCriteria(criterion1,criterion2,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		finally
		{
			return lstTeacherElectronicReferences;
		}
	}
	
// Optimization for Candidate Pool search 
	
	
	@Transactional(readOnly=false)
	public List<Integer> findTeacherReferences_Op(List<Integer> teachersId)
	{
		List<Integer> result = new ArrayList<Integer>();
		if(teachersId!=null && teachersId.size()>0)
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass()) ;
				criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				result = criteria.list();
				
			}catch (Exception e) {
				e.printStackTrace();
			}		
		return result;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> getReferencesCountByTeacherJAF(TeacherDetail teacherDetail)
	{
		List<Integer> lstTeacherReferences= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status",1);
			criteria.add(criterion1);
			criteria.add(criterion2);
			
			criteria.setProjection(Projections.projectionList().add(Projections.count("teacherDetail"))) ;
			lstTeacherReferences =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstTeacherReferences;
		}
		
	}

	@Transactional(readOnly=false)
	public List<Integer> findTeacherReferences_Op()
	{
		List<Integer> result = new ArrayList<Integer>();
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass()) ;
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				result = criteria.list();
				
			}catch (Exception e) {
				e.printStackTrace();
			}		
		return result;
	}
}
