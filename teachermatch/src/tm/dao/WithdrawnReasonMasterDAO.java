package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.WithdrawnReasonMaster;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class WithdrawnReasonMasterDAO extends GenericHibernateDAO<WithdrawnReasonMaster, Integer> 
{
	public WithdrawnReasonMasterDAO() 
	{
		super(WithdrawnReasonMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<WithdrawnReasonMaster> findAllWithdrawnReasonMasterList(DistrictMaster districtMaster)
	{
		System.out.println(":::::::::::: findAllWithdrawnReasonMaster ::::::::::::");
		List<WithdrawnReasonMaster> withdrawnReasonMasterList = new ArrayList<WithdrawnReasonMaster>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion1 = Restrictions.eq("status", "A");
			criteria.add(criterion1);
			
			if(districtMaster!=null)
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
			else
				criteria.add(Restrictions.isNull("districtMaster"));
			
			withdrawnReasonMasterList = criteria.list();
			
		} catch (Exception exception){
			exception.printStackTrace();
		}
		
		return withdrawnReasonMasterList;
	}
	
	@Transactional(readOnly=false)
	public List findAllWithdrawnReasonMaster(DistrictMaster districtMaster)
	{
		List withdrawnReasonMasterList = new ArrayList();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion1 = Restrictions.eq("status", "A");
			criteria.add(criterion1);
			
			if(districtMaster!=null)
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
			else
				criteria.add(Restrictions.isNull("districtMaster"));
			
			criteria.setProjection( Projections.projectionList()
					.add(Projections.property("withdrawnreasonmasterId"))
					.add(Projections.property("reason"))) ;
			
			withdrawnReasonMasterList =  criteria.list();
			// .add(Projections.property("districtMaster.districtId"))
		}catch(Exception exception){
			exception.printStackTrace();
		}
		return withdrawnReasonMasterList;
	}
	
	@Transactional(readOnly=false)
	public WithdrawnReasonMaster findbydistrictId(Integer districtId)
	{
		WithdrawnReasonMaster master=null;
		 List<WithdrawnReasonMaster> withdrawnReasonMasterList = new ArrayList<WithdrawnReasonMaster>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("status", "I");
			Criterion criterion1 = Restrictions.eq("districtMaster.districtId", districtId);
			Criterion criterion2 = Restrictions.isNotNull("districtMaster");
			criteria.add(criterion);criteria.add(criterion1);criteria.add(criterion2);
			withdrawnReasonMasterList = criteria.list();
		} catch (Exception exception){
			exception.printStackTrace();
		}
		if(withdrawnReasonMasterList!=null && withdrawnReasonMasterList.size()>0)
			master=withdrawnReasonMasterList.get(0);
		return master;
	}
	
	@Transactional(readOnly=false)
	public WithdrawnReasonMaster findbydistrictIdIsNullInactiveRecord()
	{
		WithdrawnReasonMaster master=null;
		 List<WithdrawnReasonMaster> withdrawnReasonMasterList = new ArrayList<WithdrawnReasonMaster>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("status", "I");
			Criterion criterion2 = Restrictions.isNull("districtMaster");
			criteria.add(criterion1);
			criteria.add(criterion2);
			withdrawnReasonMasterList = criteria.list();
		} catch (Exception exception){
			exception.printStackTrace();
		}
		if(withdrawnReasonMasterList!=null && withdrawnReasonMasterList.size()>0)
			master=withdrawnReasonMasterList.get(0);
		return master;
	}
	

	/*******TPL-3682 DA/SA: Advanced Search starts************************************************************************/
	@Transactional(readOnly=false)
	public List<WithdrawnReasonMaster> findbydistrictIdActiveRecord(Integer districtId)
	{
		WithdrawnReasonMaster master=null;
		 List<WithdrawnReasonMaster> withdrawnReasonMasterList = new ArrayList<WithdrawnReasonMaster>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("status", "A");
			Criterion criterion2 = Restrictions.eq("districtMaster.districtId", districtId);
			criteria.add(criterion1);
			criteria.add(criterion2);
			withdrawnReasonMasterList = criteria.list();
		} catch (Exception exception){
			exception.printStackTrace();
		}
		for(WithdrawnReasonMaster id:withdrawnReasonMasterList)
			System.out.println("districtIdIsNull withdrawnReasonMasterById: "+id.getWithdrawnreasonmasterId());
		
		
		return withdrawnReasonMasterList;
	}

	@Transactional(readOnly=false)
	public List<WithdrawnReasonMaster> findByIds(Integer[] withdrawnReasonMasterByIds){
		System.out.println("Entering in the findWithdrawnReasonMasterByIds..........size "+withdrawnReasonMasterByIds.length);
		 List<WithdrawnReasonMaster> withdrawnReasonMasterList = new ArrayList<WithdrawnReasonMaster>();
		try{
			for(Integer withdrawnReasonMasterById:withdrawnReasonMasterByIds){
				System.out.println("withdrawnReasonMasterById: "+withdrawnReasonMasterById);
				withdrawnReasonMasterList.add(this.findById(withdrawnReasonMasterById, false, false));
			}
		} catch (Exception exception){
			exception.printStackTrace();
		}
		System.out.println("Leaving from findWithdrawnReasonMasterByIds..........size "+withdrawnReasonMasterList.size());
		return withdrawnReasonMasterList;
	}
	/*******TPL-3682 DA/SA: Advanced Search ends************************************************************************/	
}
