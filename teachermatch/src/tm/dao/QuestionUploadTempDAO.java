package tm.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.QuestionUploadTemp;
import tm.bean.TeacherUploadTemp;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictNotes;
import tm.bean.master.DistrictSchools;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class QuestionUploadTempDAO extends GenericHibernateDAO<QuestionUploadTemp,Integer> 
{
	public QuestionUploadTempDAO() {
		super(QuestionUploadTemp.class);
	}

	@Transactional(readOnly=false)
	public boolean deleteQuestionTemp(String sessionId){
		try{
			Criterion criterion = Restrictions.eq("sessionid",sessionId);
        	List<QuestionUploadTemp> questionUploadTemplst	=findByCriteria(criterion);
        	for(QuestionUploadTemp questionUploadTemp: questionUploadTemplst){
        		makeTransient(questionUploadTemp);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Transactional(readOnly=false)
	public boolean deleteAllTeacherTemp(String sessionId)
	{
		try{
			Calendar c = Calendar.getInstance(); 
			c.add(Calendar.DAY_OF_YEAR, -2);  
			Date date = c.getTime();
			Criterion criterion1=Restrictions.lt("date",date);
			Criterion criterion2 = Restrictions.eq("sessionid",sessionId);
			Criterion complete3 = Restrictions.or(criterion1,criterion2);
        	List<QuestionUploadTemp> questionUploadTemplst	=findByCriteria(complete3);
        	System.out.println("teacherUploadTemplst::::"+questionUploadTemplst.size());
			for(QuestionUploadTemp questionUploadTemp: questionUploadTemplst){
        		makeTransient(questionUploadTemp);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return true;
	}

	@Transactional(readOnly=true)
	public List<QuestionUploadTemp> findByTempTeacher(Order order,int startPos,int limit,Criterion... criterion)
	{
		List<QuestionUploadTemp> lstQuestionUploadTemp = new ArrayList<QuestionUploadTemp>();
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			criteria.addOrder(order);
			lstQuestionUploadTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstQuestionUploadTemp;
	}
	@Transactional(readOnly=true)
	public List<TeacherUploadTemp> findByAllTempTeacher()
	{
		List<TeacherUploadTemp> lstTeacherUploadTemp = null;
		try{
			Session session = getSession();
			Calendar c = Calendar.getInstance(); // starts with today's date and time
			c.add(Calendar.DAY_OF_YEAR, -2);  // advances day by 2
			Date date = c.getTime();
			Criterion criterion=Restrictions.lt("date",date);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion);
			lstTeacherUploadTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherUploadTemp;
	}
	
	@Transactional(readOnly=true)
	public int getRowCountTempTeacher(Criterion... criterion)
	{
		List<QuestionUploadTemp> lstQuestionUploadTemp = new ArrayList<QuestionUploadTemp>();
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			lstQuestionUploadTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstQuestionUploadTemp.size();
	}
	@Transactional(readOnly=true)
	public List<TeacherUploadTemp> findByAllTempTeacherCurrentSession(String sessionId)
	{
		List<TeacherUploadTemp> lstTeacherUploadTemp = null;
		try{
			Session session = getSession();
			Criterion criterion2 = Restrictions.eq("sessionid",sessionId);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion2);
			lstTeacherUploadTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherUploadTemp;
	}
	
}
