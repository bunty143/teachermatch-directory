package tm.dao;

import tm.bean.RemovedTeacherMailDetails;
import tm.dao.generic.GenericHibernateDAO;

public class RemovedTeacherMailDetailsDAO extends GenericHibernateDAO<RemovedTeacherMailDetails, Integer> 
{
	public RemovedTeacherMailDetailsDAO() {
		super(RemovedTeacherMailDetails.class);
	}
}
