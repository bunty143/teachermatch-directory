package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EmploymentServicesTechnician;
import tm.dao.generic.GenericHibernateDAO;

public class EmploymentServicesTechnicianDAO extends GenericHibernateDAO<EmploymentServicesTechnician, Integer>{
	public EmploymentServicesTechnicianDAO() {
		super(EmploymentServicesTechnician.class);
	}
	@Transactional(readOnly=false)
	public List<EmploymentServicesTechnician> getAllEST(){
		List<EmploymentServicesTechnician> listEST=new ArrayList<EmploymentServicesTechnician>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());						
			listEST= criteria.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return listEST;
	}
}
