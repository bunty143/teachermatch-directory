package tm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.master.DistrictMaster;
 
import tm.bean.master.SchoolMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobRequisitionNumbersDAO extends GenericHibernateDAO<JobRequisitionNumbers, Integer> {

	JobRequisitionNumbersDAO()
	{
		super(JobRequisitionNumbers.class);
	}
	
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequisitionNumbersByJobWithSchool(JobOrder jobOrder,SchoolMaster schoolMaster)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion3 = Restrictions.eq("status",new Integer(0));
			if(schoolMaster!=null)
			{
				Criterion criterion2 = Restrictions.eq("schoolMaster",schoolMaster);
				if(jobOrder.getIsExpHireNotEqualToReqNo()!=null && jobOrder.getIsExpHireNotEqualToReqNo().equals(new Boolean(true)))
					list = findByCriteria(criterion1,criterion2);
				else
					list = findByCriteria(criterion1,criterion2,criterion3);
				
			}
			else
			{
				if(jobOrder.getIsExpHireNotEqualToReqNo()!=null && jobOrder.getIsExpHireNotEqualToReqNo().equals(new Boolean(true)))
					list = findByCriteria(criterion1);
				else
					list = findByCriteria(criterion1,criterion3);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequisitionNumbersByJobWithSchool(JobOrder jobOrder,SchoolMaster schoolMaster,String offerReady)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion3= Restrictions.eq("status",new Integer(0));
			
			if(schoolMaster!=null)
			{
				Criterion criterion2 = Restrictions.eq("schoolMaster",schoolMaster);
				if(jobOrder.getIsExpHireNotEqualToReqNo()!=null && jobOrder.getIsExpHireNotEqualToReqNo().equals(new Boolean(true)))
					list = findByCriteria(criterion1,criterion2);
				else{
					if((offerReady!=null && offerReady.equals("Offer Ready"))){
						list = findByCriteria(criterion1,criterion2);	
					}else{
						list = findByCriteria(criterion1,criterion2,criterion3);
					}
				}
				
			}
			else
			{
				if(jobOrder.getIsExpHireNotEqualToReqNo()!=null && jobOrder.getIsExpHireNotEqualToReqNo().equals(new Boolean(true)))
					list = findByCriteria(criterion1);
				else{
					if((offerReady!=null && offerReady.equals("Offer Ready"))){
						list = findByCriteria(criterion1);	
					}else{
						list = findByCriteria(criterion1,criterion3);
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	@Transactional(readOnly=false)
	public JobRequisitionNumbers findJobReqNumbersObj(DistrictRequisitionNumbers districtRequisitionNumbers)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		JobRequisitionNumbers jrn=null;
		try{
			
			if(districtRequisitionNumbers!=null){
				Criterion criterion1 = Restrictions.eq("districtRequisitionNumbers",districtRequisitionNumbers);
				list = findByCriteria(criterion1);
				if(list.size()>0){
					jrn=list.get(0);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return jrn;
	}
	
	
	public String updateRequisitionNumber(String requisitionNumberId){
		JobRequisitionNumbers jRNOb=null;
		String requisitionNumber=null;
			try{
				if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
					jRNOb=findById(Integer.parseInt(requisitionNumberId),false,false);
					if(jRNOb!=null){
						requisitionNumber=jRNOb.getDistrictRequisitionNumbers().getRequisitionNumber();
						jRNOb.setStatus(1);
						updatePersistent(jRNOb);
					}
					
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		return requisitionNumber;
	}
	
	@Transactional(readOnly=false)
	public SchoolMaster findSchoolByJob(JobOrder jobOrder,DistrictRequisitionNumbers DRNObj)
	{
		SchoolMaster schoolMaster=null;
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2 = Restrictions.eq("districtRequisitionNumbers",DRNObj);
			list = findByCriteria(criterion1,criterion2);
			if(list.size()>0){
				schoolMaster=list.get(0).getSchoolMaster();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolMaster;
	}
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findSchoolByJobsAndDRN(List<JobOrder> jobOrders,List<DistrictRequisitionNumbers> DRNObjs)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			if(jobOrders.size()>0 && DRNObjs.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobOrder",jobOrders);
				Criterion criterion2 = Restrictions.in("districtRequisitionNumbers",DRNObjs);
				list = findByCriteria(criterion1,criterion2);
			}
			
			System.out.println("findSchoolByJobsAndDRN:::::::::"+list.size());
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findRequisitionsByJob(JobOrder jobOrder)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			list = findByCriteria(criterion1);
			
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobReqNumbers(DistrictRequisitionNumbers districtRequisitionNumbers)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			
			if(districtRequisitionNumbers!=null){
				Criterion criterion1 = Restrictions.eq("districtRequisitionNumbers",districtRequisitionNumbers);
				list = findByCriteria(criterion1);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> getJobRequisitionList(boolean hourlyCategory,DistrictRequisitionNumbers districtRequisitionNumbers,JobOrder jobOrder)
	{
		List<JobRequisitionNumbers> lstJobRequisitionNumbers= new ArrayList<JobRequisitionNumbers>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(hourlyCategory==false){
				Criterion criterion1 = Restrictions.eq("districtRequisitionNumbers",districtRequisitionNumbers);
				criteria.add(criterion1);
			}else{
				Criterion criterion1 = Restrictions.eq("districtRequisitionNumbers",districtRequisitionNumbers);
				criteria.add(criterion1);
				criteria.createCriteria("jobOrder").add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster())).add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}
			lstJobRequisitionNumbers = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobRequisitionNumbers;
	}
	@Transactional(readOnly=false)
	public SchoolMaster findSchoolByJobReqId(JobOrder jobOrder,String requisitionNumberId)
	{
		SchoolMaster schoolMaster=null;
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2 = Restrictions.eq("jobRequisitionId",Integer.parseInt(requisitionNumberId));
			list = findByCriteria(criterion1,criterion2);
			if(list.size()>0){
				schoolMaster=list.get(0).getSchoolMaster();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolMaster;
	}
	@Transactional(readOnly=false)
	public DistrictRequisitionNumbers findDistrictReqIdByJobReqId(JobOrder jobOrder,String requisitionNumberId)
	{
		DistrictRequisitionNumbers districtRequisitionNumbers=null;
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2 = Restrictions.eq("jobRequisitionId",Integer.parseInt(requisitionNumberId));
			list = findByCriteria(criterion1,criterion2);
			if(list.size()>0){
				districtRequisitionNumbers=list.get(0).getDistrictRequisitionNumbers();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return districtRequisitionNumbers;
	}
	@Transactional(readOnly=false)
	public JobRequisitionNumbers findJobReqNumbersObjByJob(JobOrder jobOrder,DistrictRequisitionNumbers districtRequisitionNumbers)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		JobRequisitionNumbers jrn=null;
		try{
			
			if(districtRequisitionNumbers!=null){
				Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
				Criterion criterion2 = Restrictions.eq("districtRequisitionNumbers",districtRequisitionNumbers);
				list = findByCriteria(criterion1,criterion2);
				if(list.size()>0){
					jrn=list.get(0);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return jrn;
	}
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequisitionNumbersBySchoolInJobOrder(JobOrder jobOrder,List<SchoolMaster> schoolMasters)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			if(schoolMasters.size()>0){
				Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
				Criterion criterion2 = Restrictions.in("schoolMaster",schoolMasters);
				list = findByCriteria(criterion1,criterion2);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}	

	
	 
	
	
	
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findReqsObjInJRN(List<DistrictRequisitionNumbers> DRNObjs)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			
			Criterion criterion1 = Restrictions.in("districtRequisitionNumbers",DRNObjs);
			list = findByCriteria(criterion1);
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequisitionNumbersByJobAndDistrict(JobOrder jobOrder,DistrictMaster districtMaster,String requisitionNumber)
	{
		List<JobRequisitionNumbers> listNumbers=new ArrayList<JobRequisitionNumbers>();
		try{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
				criteria.add(criterion1);
				criteria.createCriteria("districtRequisitionNumbers").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.eq("requisitionNumber",requisitionNumber)).addOrder(Order.desc("districtRequisitionId"));
				listNumbers =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return listNumbers;
	}	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequisitionNumbersByDistrictOrJob(JobOrder jobOrder,DistrictMaster districtMaster,String requisitionNumber)
	{
		List<JobRequisitionNumbers> listNumbers=new ArrayList<JobRequisitionNumbers>();
		try{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(jobOrder!=null){
					Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
					criteria.add(criterion1);
				}
				if(districtMaster!=null){
					criteria.createCriteria("districtRequisitionNumbers").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.eq("requisitionNumber",requisitionNumber)).addOrder(Order.desc("districtRequisitionId"));
				}else{
					criteria.createCriteria("districtRequisitionNumbers").add(Restrictions.eq("requisitionNumber",requisitionNumber)).addOrder(Order.desc("districtRequisitionId"));
				}
				listNumbers =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return listNumbers;
	}	
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequsition(List<DistrictRequisitionNumbers> DRNObjs)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion2 = Restrictions.in("districtRequisitionNumbers",DRNObjs);
			list = findByCriteria(criterion2);
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> getHRStatusByJobId(List<JobOrder> jobOrders)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			//SessionFactory sessionFactory = teacherAnswerHistoryDAO.getSessionFactory();
			//StatelessSession statelessSession=sessionFactory.openStatelessSession();
			//Transaction transaction=statelessSession.beginTransaction();
			Criterion criterion2 = Restrictions.in("jobOrder",jobOrders);
			list=getSession().createCriteria(getPersistentClass()).add(criterion2).list();
			
			//list = findByCriteria(criterion2);
		}catch (Exception e) {
			
			e.printStackTrace();
		}	
		
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobReqNumbersForNC(List<DistrictRequisitionNumbers> districtRequisitionNumbers)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			
			if(districtRequisitionNumbers!=null){
				Criterion criterion1 = Restrictions.in("districtRequisitionNumbers",districtRequisitionNumbers);
				list = findByCriteria(criterion1);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobReqNumbersByJobIdAndDRNumber(List<DistrictRequisitionNumbers> districtRequisitionNumbers, JobOrder jobOrder)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			
			if(districtRequisitionNumbers!=null){
				Criterion criDRN = Restrictions.in("districtRequisitionNumbers",districtRequisitionNumbers);
				Criterion criJOBID = Restrictions.eq("jobOrder",jobOrder);
				list = findByCriteria(criDRN,criJOBID);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}

 // For Optimization of OnBoarding dashboard;
	
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findSchoolByJobsAndDRNOp(List<JobOrder> jobOrders,List<DistrictRequisitionNumbers> DRNObjs)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		List<Object> data =  new ArrayList<Object>();
		try{
			if(jobOrders.size()>0 && DRNObjs.size()>0)
			{
				Criteria criteria = getSession().createCriteria(this.getPersistentClass()).createAlias("districtRequisitionNumbers", "DRN");
				criteria.add(Restrictions.in("jobOrder",jobOrders));
				criteria.add(Restrictions.in("districtRequisitionNumbers",DRNObjs));
				criteria.setProjection(Projections.projectionList()
						.add(Projections.property("jobRequisitionId"))
						.add(Projections.property("jobOrder.jobId"))
						.add(Projections.property("DRN.districtRequisitionId"))
						.add(Projections.property("DRN.posType"))
					   .add(Projections.property("DRN.requisitionNumber")));
				
				data = criteria.list(); 
				
				
				for (Iterator it = data.iterator(); it.hasNext();)
				{
					Integer jobRequisitionId=null;
					Integer jobId= null;
					Integer districtRequisitionId=null;
					String posType="";
					String requisitionNumber = "";
		            Object[] row = (Object[]) it.next();
		            if(row[0]!=null)
		            {
		            	jobRequisitionId=Integer.parseInt(row[0].toString());
		            	jobId=Integer.parseInt(row[1].toString());
		            	districtRequisitionId = Integer.parseInt(row[2].toString());
		            	posType = row[3].toString();
		            	requisitionNumber = row[4].toString();
		            	JobRequisitionNumbers jrn = new JobRequisitionNumbers();
		            	jrn.setJobRequisitionId(jobRequisitionId);
		            	if(jobId!=null)
		            		{
		            		JobOrder jo = new JobOrder();
		            		jo.setJobId(jobId);
		            		jrn.setJobOrder(jo);
		            		}
		            	
		            	if(districtRequisitionId!=null)
	            		{
	            		DistrictRequisitionNumbers drn = new DistrictRequisitionNumbers();
	            		drn.setDistrictRequisitionId(districtRequisitionId); 
	            		drn.setPosType(posType);
	            		drn.setRequisitionNumber(requisitionNumber);
	            		jrn.setDistrictRequisitionNumbers(drn);	     
	            		}
		            	
		            	list.add(jrn);
		            	
		            	//System.out.println(""+j+"\n teacherId=="+teacherId+"::::::::: td=="+td);
		            }
		           //++j;
				}
				System.out.println("findSchoolByJobsAndDRNOp:::::::::"+list.size());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	@Transactional(readOnly=false)
	public List<Object[]> getJobIdAndRequisitionNo(List<JobOrder> listjobOrder)
	{
		
		List<Object[]> list=new ArrayList<Object[]>();
		try
		{
			
			Criteria criteria=getSession().createCriteria(getPersistentClass(),"jrn");
			if(listjobOrder!=null && !listjobOrder.isEmpty())
				criteria.add(Restrictions.in("jrn.jobOrder", listjobOrder));
			list=criteria.createAlias("districtRequisitionNumbers", "drn")
			.setProjection(Projections.projectionList()
					.add(Projections.property("jrn.jobOrder.jobId"))
					.add(Projections.property("drn.requisitionNumber"))
					)
				.list();
			
			
		}catch (Exception e) {
			
			e.printStackTrace();
		}	
		
		return list;
		
	}
//Added by kumar avinash
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequisitionNumbersBySchoolInJobOrder_OP(JobOrder jobOrder,List<SchoolMaster> schoolMasters)
	{int p=0;
	List<Object[]> objList=null;
	Map<Integer,Object> map=new HashMap<Integer,Object>();
		List<JobRequisitionNumbers> jobRequisitionNumberslist=new ArrayList<JobRequisitionNumbers>();
		try 
		{
  			Query query1=null;
  			List<Object[]> objListTemp=null;
			if(schoolMasters.size()>=0){
 				Session session = getSession();
 			
 				String schoolIds ="";
 				
 				for(SchoolMaster schoolMasterObj: schoolMasters){
 					if(p==0){
 						schoolIds=schoolMasterObj.getSchoolId()+"";
 					}else{
 						schoolIds+=","+schoolMasterObj.getSchoolId()+"";
 					}
 					
 					p++; 
 				}
 				
 				String hql=	"SELECT  jRN.jobRequisitionId,jRN.schoolId,jRN.districtRequisitionId  FROM  jobrequisitionnumbers jRN "
			 	    +   "left join joborder jo on jo.jobId= jRN.jobId "
			 	   +   "left join schoolmaster sm on sm.schoolId= jRN.schoolId "
				    + " WHERE   jRN.jobId="+jobOrder.getJobId()+" and jRN.schoolId in ("+schoolIds+")";
 				
 				query1=session.createSQLQuery(hql);
 				
 				objList = query1.list();
 	 			if(objList.size()>=0){ 			
 					JobRequisitionNumbers jobRequisitionNumbers=null;
				for (int j = 0; j < objList.size(); j++) {
						Object[] objArr = objList.get(j);
						jobRequisitionNumbers = new JobRequisitionNumbers();
						jobRequisitionNumbers.setJobRequisitionId(objArr[0] == null ? 0 : Integer.parseInt(objArr[0].toString()));
		 			 	Long schoolIdTemp=objArr[1] == null ? 0 : Long.parseLong(objArr[1].toString());
		 					SchoolMaster schoolMaster=new SchoolMaster();
							schoolMaster.setSchoolId(schoolIdTemp);	
							jobRequisitionNumbers.setSchoolMaster(schoolMaster);
		 				int districtRequisitionNumbersTemp= objArr[2] == null ? 0 : Integer.parseInt(objArr[2].toString());
			 				DistrictRequisitionNumbers districtRequisitionNumbers=new DistrictRequisitionNumbers();
			 				districtRequisitionNumbers.setDistrictRequisitionId( districtRequisitionNumbersTemp);
			 				jobRequisitionNumbers.setDistrictRequisitionNumbers(districtRequisitionNumbers);
 						     jobRequisitionNumberslist.add(jobRequisitionNumbers);
				     }	 			 
				}
 			}
		} 
	 	catch(Exception e){
 		         e.printStackTrace(); 
		}finally{
			// session.close(); 
			map=null;
 		}
		return jobRequisitionNumberslist;
	}	
	
	public List<Long> jobForteacherIdsBySchool(Long schoolId){
		List<Long> lstJobForTeacher =new ArrayList<Long>();
		List<Object[]> objList=null;
		try {

			Query query1=null;
			String hql=	"SELECT jft.jobForTeacherId,jft.teacherId FROM jobforteacher jft "
				+"INNER JOIN jobrequisitionnumbers jrq ON jrq.jobId =jft.jobId "+
				" INNER JOIN districtrequisitionnumbers drq ON drq.districtRequisitionId = jrq.districtRequisitionId "
				+" WHERE  jrq.jobId = jft.jobId AND  jft.requisitionNumber = drq.requisitionNumber AND jrq.schoolId ="+schoolId				
				;
				
			System.out.println("sql query    "+hql);
				query1=session.createSQLQuery(hql);
				
				objList = query1.list();
				if(objList.size()>=0){
					for (int j = 0; j < objList.size(); j++) {
						Object[] objArr = objList.get(j);
							if(objArr[0]!=null){
								lstJobForTeacher.add(Long.parseLong(objArr[0].toString()));
							}
						}
				}
				
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return lstJobForTeacher;
	}
	
}
