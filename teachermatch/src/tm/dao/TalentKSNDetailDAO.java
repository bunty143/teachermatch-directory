package tm.dao;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TalentKSNDetail;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;

public class TalentKSNDetailDAO extends GenericHibernateDAO<TalentKSNDetail, Integer> {
	
	public TalentKSNDetailDAO()
	{
		super(TalentKSNDetail.class);
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<TalentKSNDetail> getListTalentKSNDetail(TeacherDetail dtl, String status){
		Criterion tlntKSNDTLTchrIdcrtria=Restrictions.eq("teacherId", dtl);
		Criterion tlntKSNDTLTchrcrStustria=Restrictions.eq("status", status);
		Criteria crit = getSession().createCriteria(getPersistentClass());
			crit.add(tlntKSNDTLTchrIdcrtria);
			crit.add(tlntKSNDTLTchrcrStustria);
		return crit.list();
	}
	
	@Transactional(readOnly=true)
	public TalentKSNDetail getTalentKSNDetailByKSNID(TeacherDetail dtl, String ksnID){
		List<TalentKSNDetail> ksnDetail = new ArrayList<TalentKSNDetail>();
		Criteria crit = getSession().createCriteria(getPersistentClass());
		try{
			Criterion tlntKSNDTLTchrIdcrtria=Restrictions.eq("teacherId", dtl);
			Criterion tlntKSNDTLTKSN=Restrictions.eq("KSNID", Integer.parseInt(ksnID));
			crit.add(tlntKSNDTLTchrIdcrtria);
			crit.add(tlntKSNDTLTKSN);
			ksnDetail = crit.list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		if(ksnDetail!=null && ksnDetail.size()>0)
			return ksnDetail.get(0);
		else
			return null;
	}
	
	@Transactional(readOnly=true)
	public List<TalentKSNDetail> getListTalentKSNDetail_Op(List<TeacherDetail> t){
		
		List<TalentKSNDetail> talentDetails = new ArrayList<TalentKSNDetail>();
		try {
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass());
			Criterion tlntKSNDTLTchrIdcrtria=Restrictions.in("teacherId", t);
			Criterion tlntKSNDTLTchrcrStustria=Restrictions.eq("status", "A");
			criteria.add(tlntKSNDTLTchrIdcrtria);
			criteria.add(tlntKSNDTLTchrcrStustria);
			criteria.setProjection(Projections.projectionList()
					.add(Projections.property("ksnDetailId"))
					.add(Projections.property("status"))
					.add(Projections.property("KSNID"))
					.add(Projections.property("teacherId.teacherId"))
					.add(Projections.property("isSelected"))
					.add(Projections.property("createdDateTime"))
					.add(Projections.property("firstName"))
					.add(Projections.property("lastName"))
					.add(Projections.property("emailAddress")));
			
			List<String[]> lstTalent = new ArrayList<String[]>();
			lstTalent = criteria.list();
		    if(lstTalent!=null && lstTalent.size()>0){
		    	for(Iterator it = lstTalent.iterator(); it.hasNext();){
		    		TalentKSNDetail talent = new TalentKSNDetail();
		    		Object[] row = (Object[]) it.next();
		    		if(row[0]!=null){
		    			talent.setKsnDetailId(Integer.parseInt(row[0].toString()));
		    		}
		    		if(row[1]!=null){
		    			talent.setStatus(row[1].toString());
		    		}
		    		if(row[2]!=null){
		    			talent.setKSNID(Integer.parseInt(row[2].toString()));
		    		}
		    		if(row[3]!=null){
		    			TeacherDetail teacherDetail = new TeacherDetail();
		    			teacherDetail.setTeacherId(Integer.parseInt(row[3].toString()));
		    			talent.setTeacherId(teacherDetail);
		    		}
		    		if(row[4]!=null){
		    			talent.setIsSelected(row[4].toString());
		    		}
		    		if(row[5]!=null){
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						Date newDate = df.parse(row[5].toString());
						talent.setCreatedDateTime(newDate);
					}
		    		if(row[6]!=null){
		    			talent.setFirstName(row[6].toString());
		    		}
		    		if(row[7]!=null){
		    			talent.setLastName(row[7].toString());
		    		}
		    		if(row[8]!=null){
		    			talent.setEmailAddress(row[8].toString());
		    		}
		    		talentDetails.add(talent);
		    	}
		    }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return talentDetails;
		
	}
	
}
