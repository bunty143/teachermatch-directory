package tm.dao;

import java.util.ArrayList;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.annotations.OrderBy;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.InternalTransferCandidates;
import tm.bean.JobCategoryForInternalCandidate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherUploadTemp;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SubjectMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobCategoryForInternalCandidateDAO extends GenericHibernateDAO<JobCategoryForInternalCandidate, Integer> 
{
	public JobCategoryForInternalCandidateDAO() 
	{
		super(JobCategoryForInternalCandidate.class);
	}
	@Transactional(readOnly=false)
	public List<JobCategoryForInternalCandidate> getCategoryByTeacher(TeacherDetail teacherDetail){
		List<JobCategoryForInternalCandidate> jfiList= new ArrayList<JobCategoryForInternalCandidate>();
		try{
			if(teacherDetail!=null){
				Criterion criterion1=Restrictions.eq("teacherDetail",teacherDetail);
				jfiList=findByCriteria(Order.asc("jobCategoryMaster"),criterion1);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return jfiList;
	}
	@Transactional(readOnly=false)
	public List<JobCategoryForInternalCandidate> getCategoryByTeacherWithCategory(TeacherDetail teacherDetail,JobCategoryMaster jobCategoryMaster){
		List<JobCategoryForInternalCandidate> jfiList= new ArrayList<JobCategoryForInternalCandidate>();
		try{
			if(teacherDetail!=null){
				Criterion criterion1=Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2=Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
				jfiList=findByCriteria(Order.asc("jobCategoryMaster"),criterion1,criterion2);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return jfiList;
	}
	@Transactional(readOnly=false)
	public boolean deleteCategoryByTeacher(JobCategoryMaster jobCategoryMaster){
		try{
			Criterion criterion1=Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
        	List<JobCategoryForInternalCandidate> jfiList	=findByCriteria(criterion1);
        	for(JobCategoryForInternalCandidate jfiObj: jfiList){
        		makeTransient(jfiObj);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Transactional(readOnly=false)
	public List<JobCategoryForInternalCandidate> getTeachers(DistrictMaster districtMaster ,JobCategoryMaster jobCategoryMaster,SubjectMaster subjectMaster){
		List<JobCategoryForInternalCandidate> jfiList= new ArrayList<JobCategoryForInternalCandidate>();
		try{
				
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion=Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
				Criterion criterion1=Restrictions.eq("subjectMaster",subjectMaster);

				criteria.add(criterion);
				criteria.add(criterion1);
				
				criteria.createCriteria("internalTransferCandidates")
				.add(Restrictions.eq("districtMaster", districtMaster))
				.add(Restrictions.eq("optEmails", true))
				.add(Restrictions.eq("verificationStatus", true))
				.add(Restrictions.eq("status", "A"));

				jfiList = criteria.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return jfiList;
	}
}
