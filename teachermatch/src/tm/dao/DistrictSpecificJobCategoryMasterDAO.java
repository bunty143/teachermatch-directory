package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictSpecificJobCategoryMaster;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificJobCategoryMasterDAO extends GenericHibernateDAO<DistrictSpecificJobCategoryMaster, Integer> 
{
	public DistrictSpecificJobCategoryMasterDAO() {
		super(DistrictSpecificJobCategoryMaster.class);
	}
	
	@Transactional(readOnly=false)
	public DistrictSpecificJobCategoryMaster checkJobCategoryByJobCode(String jobCode,DistrictMaster districtMaster)
	{
		List<DistrictSpecificJobCategoryMaster> specificJobCategoryMasterList = new ArrayList<DistrictSpecificJobCategoryMaster>();
		DistrictSpecificJobCategoryMaster specificJobCategoryMaster = null;
		try 
		{
			
			//Criterion criterion1 = Restrictions.eq("jobCode",jobCode);
			Criterion criterion1 = Restrictions.eq("desc",jobCode);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			specificJobCategoryMasterList = findByCriteria(criterion1,criterion2);
			if(specificJobCategoryMasterList.size()>0)
				specificJobCategoryMaster = specificJobCategoryMasterList.get(0);
				
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return specificJobCategoryMaster;
	}
}
