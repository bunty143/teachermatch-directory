package tm.dao;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictJobCode;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictJobCodeDAO extends GenericHibernateDAO<DistrictJobCode, Integer>{

	public DistrictJobCodeDAO() {
		super(DistrictJobCode.class); 
	}
	
	@Transactional(readOnly=false)
	public boolean checkDuplicateJobCode(Integer districtRequisitionId,DistrictMaster districtMaster,String jobCode ){
		boolean r=false;
		try{
			List result=null;
			Criterion crn=Restrictions.eq("districtMaster", districtMaster );
			if(districtRequisitionId!=null)
			{
				Criterion c1=Restrictions.eq("jobCode", jobCode);
				Criterion c2=Restrictions.not(Restrictions.in("jobCodeId",new Integer[]{districtRequisitionId}));
				result=findByCriteria(crn,c1,c2);
			}else
			{
				Criterion c1=Restrictions.eq("jobCode", jobCode); 
				result=findByCriteria(crn,c1);
			}
			if(result.size()>0)
				r=true;
			else
				r=false;
			
		}catch (Exception e) {
			e.printStackTrace();
			
		}
		return r;
	}
	
	@Transactional(readOnly=false)
	public DistrictJobCode getDistrictJobCode(DistrictMaster districtMaster,String requisitionNumber)
	{
		List<DistrictJobCode> DistrictJobCodes= null;
		DistrictJobCode districtJobCode=null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("jobCode",requisitionNumber);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			DistrictJobCodes = findByCriteria(criterion1,criterion2);		
			if(DistrictJobCodes.size()>0){
				districtJobCode=DistrictJobCodes.get(0);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return districtJobCode;
	}
}
