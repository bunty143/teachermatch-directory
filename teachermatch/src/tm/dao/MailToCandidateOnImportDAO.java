package tm.dao;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MailToCandidateOnImport;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;

public class MailToCandidateOnImportDAO extends GenericHibernateDAO<MailToCandidateOnImport, Integer> 
{
	public MailToCandidateOnImportDAO() 
	{
		super(MailToCandidateOnImport.class);
	}
	
	/*   ===== Gagan : It is used */
	@Transactional(readOnly=false)
	public List<MailToCandidateOnImport> getDistrictByDistrictIdList(List districtId,Integer headQuarterId,Integer branchId)
	{
		List<MailToCandidateOnImport> lstMailToCandidateOnImport= null;
		try 
		{
			Criterion criterion1=null;
			if(headQuarterId!=null)
			{
				criterion1= Restrictions.eq("headQuarterId",headQuarterId);	
			}
			else 
			{
				criterion1= Restrictions.in("districtMaster",districtId);
			}
			lstMailToCandidateOnImport = findByCriteria(criterion1);	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstMailToCandidateOnImport;
	}

}
