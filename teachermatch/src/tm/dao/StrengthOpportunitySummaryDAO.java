package tm.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.JobOrder;
import tm.bean.StrengthOpportunitySummary;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;

public class StrengthOpportunitySummaryDAO extends GenericHibernateDAO<StrengthOpportunitySummary, Integer> 
{
	public StrengthOpportunitySummaryDAO() {
		super(StrengthOpportunitySummary.class);
	}
	
	@Transactional(readOnly=false)
	public List<StrengthOpportunitySummary> findStrengthOpportunityByTeacherIdandJobId(TeacherDetail teacherDetails,JobOrder jobOrder)
	{
		List<StrengthOpportunitySummary> strengthOpportunitySummaryList = new ArrayList<StrengthOpportunitySummary>();
		try {
			if(teacherDetails!=null && jobOrder!=null){
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetails);
				Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
				strengthOpportunitySummaryList = findByCriteria(criterion1,criterion2);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strengthOpportunitySummaryList;
	}
	
	@Transactional(readOnly=false)
	public List<String> getAllCompetencies()
	{
		List<String> competencyList = new ArrayList<String>();
		try {
			competencyList.add("Organizational Alignment");
			competencyList.add("Instructional Leadership");
			competencyList.add("Personal Leadership");
			competencyList.add("Cultural and  Community Leadership");
			competencyList.add("Operational Management");
			competencyList.add("Results Driven");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return competencyList;
	}
}
