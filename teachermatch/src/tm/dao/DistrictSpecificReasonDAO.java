package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictSpecificReason;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificReasonDAO extends GenericHibernateDAO<DistrictSpecificReason, Integer>{
	
	public DistrictSpecificReasonDAO(){
		super(DistrictSpecificReason.class);
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificReason> getAllReasonForDistrict(DistrictMaster districtMaster)
	{
		List<DistrictSpecificReason> districtSpecificReason=new ArrayList<DistrictSpecificReason>();
		try{
		Criterion criterion=Restrictions.eq("districtMaster", districtMaster);
		Criterion criterion1=Restrictions.eq("status", "A");
		districtSpecificReason=findByCriteria(criterion,criterion1);
		}catch(Exception e){}
	return districtSpecificReason;	
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificReason> getAllReasonDefault()
	{
		List<DistrictSpecificReason> districtSpecificReason=new ArrayList<DistrictSpecificReason>();
		try{
		Criterion criterion=Restrictions.isNull("districtMaster");
		Criterion criterion1=Restrictions.eq("status", "A");
		districtSpecificReason=findByCriteria(criterion1,criterion);
		}catch(Exception e){
			e.printStackTrace();
		}
	return districtSpecificReason;	
	}
	
}
