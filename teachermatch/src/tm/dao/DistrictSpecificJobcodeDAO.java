package tm.dao;

import java.util.List;


import org.hibernate.Criteria;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictSpecificJobcode;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificJobcodeDAO extends GenericHibernateDAO<DistrictSpecificJobcode, Integer>{

	public DistrictSpecificJobcodeDAO() {
		super(DistrictSpecificJobcode.class);
	}

	/**
	 * Vikas
	 */
	private static final long serialVersionUID = 1L;
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificJobcode> findJobCategoryByJobcode(DistrictMaster districtMaster,String jobCode){
		
		System.out.println(districtMaster.getDistrictId()+"|findJobCategoryByJobcode| "+jobCode);
		List<DistrictSpecificJobcode> lstDistrictSpecificJobcode = null;
		
		Criterion criterion1 = Restrictions.eq("jobCode", jobCode);
 	    Criterion criterion2 = Restrictions.eq("districtMaster", districtMaster);
 	    Criterion criterion3 = Restrictions.eq("status", "A");
		try{
		Session session =getSession(); 	
 	    Criteria cri = session.createCriteria(getPersistentClass());
 	    cri.add(criterion1);
 	    cri.add(criterion2);
 	    cri.add(criterion3);
 	    lstDistrictSpecificJobcode = cri.list();
		}
		catch(ObjectNotFoundException e){
			//e.printStackTrace();
			System.out.println("*No row with the given identifier exists: ");
			return null;
			}
		System.out.println("Retrun List: "+lstDistrictSpecificJobcode);
		return lstDistrictSpecificJobcode;
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificJobcode> findJobCodeList(DistrictMaster districtMaster,String jobCode){
		
		System.out.println(districtMaster.getDistrictId()+"|findJobCategoryByJobcode| "+jobCode);
		List<DistrictSpecificJobcode> lstDistrictSpecificJobcode = null;
		Criterion criterion1 = Restrictions.like("jobCode", "%"+jobCode+"%");
		Criterion criterion2 = Restrictions.eq("districtMaster", districtMaster);
 	    Criterion criterion3 = Restrictions.eq("status", "A");
		try{
		Session session =getSession(); 	
 	    Criteria cri = session.createCriteria(getPersistentClass());
 	    //cri.add(criterion1);
 	    cri.add(criterion2);
 	    cri.add(criterion3);
        cri.add(Restrictions.or(
        Restrictions.like("jobCode", "%"+jobCode+"%"),
        Restrictions.like("jobTitle", "%"+jobCode+"%")));
 	    lstDistrictSpecificJobcode =cri.list();
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("*No row with the given identifier exists: ");
			return null;
			}
		System.out.println("Retrun List: "+lstDistrictSpecificJobcode.size());
		return lstDistrictSpecificJobcode;
	}
	
	/***************************SWADESH*****************************/
	@Transactional(readOnly=false)
	public List<DistrictSpecificJobcode> findJobCodedetails(DistrictMaster districtMaster, String jobCode){
		
		List<DistrictSpecificJobcode> lstDistrictSpecificJobcode = null;
		Criterion criterion2 = Restrictions.eq("districtMaster", districtMaster);
 	    Criterion criterion3 = Restrictions.eq("jobCode", jobCode);
		try{
		Session session =getSession(); 	
 	    Criteria cri = session.createCriteria(getPersistentClass());
 	    cri.add(criterion3);
 	   cri.add(criterion2);
 	    lstDistrictSpecificJobcode =findByCriteria(criterion2,criterion3);
 	    
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("*No row with the given identifier exists: ");
			return null;
			}
		System.out.println("Retrun List: "+lstDistrictSpecificJobcode);
		return lstDistrictSpecificJobcode;
	}
	
}
