package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.JobWiseApprovalProcess;
import tm.dao.generic.GenericHibernateDAO;

public class JobWiseApprovalProcessDAO<JobApprovalProcess>  extends GenericHibernateDAO<JobWiseApprovalProcess, Integer>{
	
	public JobWiseApprovalProcessDAO() {
		super(JobWiseApprovalProcess.class); 
	}

	@Transactional(readOnly=true)
	public List<JobWiseApprovalProcess> findByJobId(JobOrder jobId)
	{
		List<JobWiseApprovalProcess> list = new ArrayList<JobWiseApprovalProcess>();
		try{
		 Criterion criterion1 = Restrictions.eq("jobId",jobId);
		 list = findByCriteria(criterion1);
		}catch(Exception e){e.printStackTrace();}
		return list;
	}
	@Transactional(readOnly=true)
	public List<JobWiseApprovalProcess> findByJobIdAndApprovalId(JobOrder jobId,JobApprovalProcess jobApprovalProcessId)
	{
		List<JobWiseApprovalProcess> list = new ArrayList<JobWiseApprovalProcess>();
		try{
		 Criterion criterion1 = Restrictions.eq("jobId",jobId);
		 Criterion criterion2 = Restrictions.eq("jobApprovalProcessId",jobApprovalProcessId);
		 list = findByCriteria(criterion1,criterion2);
		}catch(Exception e){e.printStackTrace();}
		return list;
	}
	
}
