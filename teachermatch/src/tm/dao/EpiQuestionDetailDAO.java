package tm.dao;

import tm.bean.EpiQuestionDetail;
import tm.dao.generic.GenericHibernateDAO;

public class EpiQuestionDetailDAO extends GenericHibernateDAO<EpiQuestionDetail, Integer>  
{
	public EpiQuestionDetailDAO() {
		super(EpiQuestionDetail.class);
	}

}
