package tm.dao.i4;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.i4.I4Candidate;
import tm.bean.i4.VVIResponse;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class VVIResponseDAO extends GenericHibernateDAO<VVIResponse, Integer> 
{
	public VVIResponseDAO() 
	{
		super(VVIResponse.class);
	}
	
	@Transactional(readOnly=false)
	public List<VVIResponse> findByI4InviteId(Integer i4InviteId) {
		List<VVIResponse> responseList = new ArrayList<VVIResponse>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("i4InviteId",i4InviteId+"");
			responseList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return responseList;
	}
}
