package tm.dao.i4;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.i4.I4InterviewInvites;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class I4InterviewInvitesDAO extends GenericHibernateDAO<I4InterviewInvites, Integer> 
{
	public I4InterviewInvitesDAO() 
	{
		super(I4InterviewInvites.class);
	}
	
	@Transactional(readOnly=false)
	public List<I4InterviewInvites> getListbyTeacherAndjobId(JobOrder joborder,TeacherDetail teacherDetail) {
		List<I4InterviewInvites> statusList = new ArrayList<I4InterviewInvites>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("jobOrder",joborder);
			Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
			statusList = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public List<I4InterviewInvites> findByInviteId(String i4InviteId) {
		List<I4InterviewInvites> statusList = null;
		try 
		{
			System.out.println("i4InviteId:: "+i4InviteId);
			Criterion criterion1 = Restrictions.eq("I4inviteid",Integer.parseInt(i4InviteId));
			
			statusList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public List<I4InterviewInvites> getListbyTeacherListAndjobId(JobOrder jobOrder,List<TeacherDetail> teacherDetails) {
		
		//System.out.println(" =========== getListbyTeacherListAndjobId ==========="+teacherDetails.size());
		List<I4InterviewInvites> statusList = new ArrayList<I4InterviewInvites>();
		try 
		{
			if(teacherDetails!=null && teacherDetails.size()>0)
			{
				Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
				Criterion criterion2 = Restrictions.in("teacherDetail",teacherDetails);
				statusList = findByCriteria(criterion1,criterion2);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public List<I4InterviewInvites> getListbyTeacherAndDistForEventManagement(Integer  districtId,TeacherDetail teacherDetail)
	{
		System.out.println(" ==== getListbyTeacherAndDistForEventManagement ====");
		List<I4InterviewInvites> statusList = new ArrayList<I4InterviewInvites>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtId);
			Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 = Restrictions.isNull("jobOrder");
			statusList = findByCriteria(criterion1,criterion2,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public boolean inviteAlreadySentInSameQuestionSet(Integer questionSetId,Integer teacherId,Integer districtId)
	{
		boolean status = false;
		try 
		{
			Session session = getSession();
			String sql =" SELECT COUNT(*) from i4_interviewinvites  where questionsetid = "+questionSetId+" and candidateid="+teacherId+
						" and districtId="+districtId+" " ;
			Query query = session.createSQLQuery(sql);
			List<BigInteger> rowCount = query.list();
			if(rowCount!=null && rowCount.size()==1 && rowCount.get(0).intValue()>0)
				status=true;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return status;
	}

	@Transactional(readOnly=false)
	public Map<Integer,Integer> getListbyTeacherListAndQuesSetId(JobOrder jobOrder,List<TeacherDetail> teacherDetails) {
		
		//System.out.println(" =========== getListbyTeacherListAndjobId ==========="+teacherDetails.size());
		Map<Integer,Integer> mapInviteInterviewToTeacher = new HashMap<Integer,Integer>();
		try 
		{
			if(teacherDetails!=null && teacherDetails.size()>0)
			{
				Criterion criterion1 = Restrictions.eq("i4QuestionSets",jobOrder.getI4QuestionSets());
				Criterion criterion2 = Restrictions.in("teacherDetail",teacherDetails);
				//statusList = findByCriteria(criterion1,criterion2);
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				criteria.createAlias("teacherDetail", "td").setProjection(Projections.projectionList()
						.add(Projections.property("td.teacherId"))
						.add(Projections.property("status"))
						);
				criteria.add(criterion1).add(criterion2);
				
				List<String[]> stList = new ArrayList<String[]>();
				stList = criteria.list();
				
				if(stList.size()>0){
					for (int j = 0; j < stList.size(); j++) {
						Object[] row = (Object[]) stList.get(j);						
						if(row[0]!=null && row[1]!=null){
							System.out.println(Integer.parseInt(row[0].toString())+" hello bhai  "+Integer.parseInt(row[1].toString()));
							mapInviteInterviewToTeacher.put(Integer.parseInt(row[0].toString()), Integer.parseInt(row[1].toString()));
						}
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return mapInviteInterviewToTeacher;
	}
}
