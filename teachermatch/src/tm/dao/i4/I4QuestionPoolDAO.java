package tm.dao.i4;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.python.antlr.op.Add;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4QuestionPool;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class I4QuestionPoolDAO extends GenericHibernateDAO<I4QuestionPool, Integer> 
{
	public I4QuestionPoolDAO() 
	{
		super(I4QuestionPool.class);
	}
	
	@Transactional(readOnly=false)
	public List<I4QuestionPool> findi4QuesByStatus(String i4QuesStatus) {
		List<I4QuestionPool> statusList = new ArrayList<I4QuestionPool>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("Status",i4QuesStatus);
			statusList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	
	@Transactional(readOnly=false)
	public List<I4QuestionPool> findi4QuesByQues(String Questext) {
		List<I4QuestionPool> statusList = new ArrayList<I4QuestionPool>();
		try 
		{
			Criterion criterion1 = Restrictions.like("QuestionText",Questext,MatchMode.ANYWHERE);
			statusList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public List<I4QuestionPool> findByDistrict(DistrictMaster districtMaster) {
		List<I4QuestionPool> listBydistrict = new ArrayList<I4QuestionPool>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			listBydistrict = findByCriteria(Order.asc("QuestionText"), criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	@Transactional(readOnly=false)
	public List<I4QuestionPool> findExistQuestion(String quesTxt,DistrictMaster districtMaster) {
		List<I4QuestionPool> listBydistrict = new ArrayList<I4QuestionPool>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("QuestionText",quesTxt).ignoreCase();
			listBydistrict = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	@Transactional(readOnly=false)
	public List<I4QuestionPool> findByI4QuestionIds(List<String> i4questionIds) {
		List<I4QuestionPool> listBydistrict = new ArrayList<I4QuestionPool>();
		try 
		{
			Criterion criterion1 = Restrictions.in("I4QuestionID",i4questionIds);
			
			listBydistrict = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	
	// @ Anurag
	
	@Transactional(readOnly=false)
	public List<I4QuestionPool> findByHeadQuarter(HeadQuarterMaster headQuarterMaster) {
		List<I4QuestionPool> listBydistrict = new ArrayList<I4QuestionPool>();
		try 
		{
			if(headQuarterMaster!=null){
			Criterion criterion1 = Restrictions.eq("headQuarterId",headQuarterMaster.getHeadQuarterId());
			listBydistrict = findByCriteria(Order.asc("QuestionText"), criterion1);		
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
}
