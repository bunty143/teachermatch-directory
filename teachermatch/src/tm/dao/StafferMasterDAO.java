package tm.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.quartz.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.sun.mail.imap.protocol.Status;

import tm.bean.JobOrder;
import tm.bean.StafferMaster;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.SchoolMaster;

import tm.dao.generic.GenericHibernateDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.user.UserMasterDAO;

public class StafferMasterDAO extends GenericHibernateDAO<StafferMaster, Integer> 
{
	public StafferMasterDAO() 
	{
		super(StafferMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<StafferMaster> findStafferMasterList(DistrictMaster districtMaster,SchoolMaster schoolMaster){
		List<StafferMaster> slist=new ArrayList<StafferMaster>();
		try{
			if(districtMaster!=null && schoolMaster!=null){
				Criterion criterion1=Restrictions.eq("districtMaster", districtMaster);
				Criterion criterion2=Restrictions.eq("schoolMaster", schoolMaster);
				slist=findByCriteria(criterion1,criterion2);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return slist;
	}
}

