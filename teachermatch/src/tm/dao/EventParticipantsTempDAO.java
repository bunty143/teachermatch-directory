package tm.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EventDetails;
import tm.bean.EventParticipantsList;
import tm.bean.EventParticipantsTemp;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class EventParticipantsTempDAO extends GenericHibernateDAO<EventParticipantsTemp, Integer>{

	public EventParticipantsTempDAO(){
		super(EventParticipantsTemp.class);
	}
	
	@Transactional(readOnly=false)
	public int deleterecords(String sessionId){
		
		try 
		{
			Session session = getSession();

			String hql = "delete from EventParticipantsTemp where tempId= :tempId";
			session.createQuery(hql).setString("tempId", sessionId).executeUpdate();
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 1;
	}
	
	@Transactional(readOnly=false)
	public List<EventParticipantsTemp> findListOfParticipants(Order order,int startPos,int limit,String sessId)
	{
		List<EventParticipantsTemp> lstEventParticipantsList= null;
		try 
		{
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				 Criterion criterionTemp = Restrictions.eq("tempId", sessId);
				 
				 criteria.add(criterionTemp);
				    
				    /*criteria.setFirstResult(startPos);
					criteria.setMaxResults(limit);	*/		
					criteria.addOrder(order);
					
					lstEventParticipantsList =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstEventParticipantsList;
	}
	
	@Transactional(readOnly=false)
	public List<EventParticipantsList> findListOfParticipantsCount(String sessId)
	{
		List<EventParticipantsList> lstEventParticipantsList= null;
		try 
		{
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				Criterion criterionTemp = Restrictions.eq("tempId", sessId);
				 
				 criteria.add(criterionTemp);
				 lstEventParticipantsList =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstEventParticipantsList;
	}
}
