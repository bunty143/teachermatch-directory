package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.SapAcademicDetails;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;

public class SapAcademicDetailsDAO extends GenericHibernateDAO<SapAcademicDetails, Integer> 
{
	public SapAcademicDetailsDAO() {
		super(SapAcademicDetails.class);
	}

	@Transactional(readOnly=false)
	public List<SapAcademicDetails> findSAPAcadamicDetailByTeacher(TeacherDetail teacherDetail)
	{
		List<SapAcademicDetails> lstAcademics= new ArrayList<SapAcademicDetails>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstAcademics = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstAcademics;
	}
	
	@Transactional(readOnly=false)
	public List<SapAcademicDetails> findSAPAcadamicDetailByTeacherAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<SapAcademicDetails> lstAcademics= new ArrayList<SapAcademicDetails>();
		try 
		{
			Criterion criterion1 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 	= 	Restrictions.eq("jobOrder",jobOrder);
			lstAcademics = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstAcademics;
	}
}
