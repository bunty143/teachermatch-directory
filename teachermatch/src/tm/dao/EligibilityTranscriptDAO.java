package tm.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.quartz.JobDetail;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EligibilityTranscript;
import tm.bean.EligibilityVerificationHistroy;
import tm.bean.JobOrder;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.EligibilityMaster;
import tm.bean.EligibilityStatusMaster;
import tm.bean.user.UserMaster;
import tm.bean.TeacherAcademics;
import tm.dao.generic.GenericHibernateDAO;

public class EligibilityTranscriptDAO extends GenericHibernateDAO<EligibilityTranscript, Integer> 
{

	public EligibilityTranscriptDAO()
	{
		super(EligibilityTranscript.class);
	}
	
	
	@Transactional(readOnly=false)
	public List<EligibilityTranscript> findAllTranscriptByJobId(DistrictMaster districtMaster,TeacherDetail teacherDetail,JobOrder jobOrder,TeacherAcademics teacherAcademics)
	{
		List<EligibilityTranscript> eligibilityTranscript= null;
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 	= 	Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion4 	= 	Restrictions.eq("teacherAcademics",teacherAcademics);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			eligibilityTranscript = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityTranscript;
	}
	
	@Transactional(readOnly=false)
	public List<EligibilityTranscript> findTranscriptByJobId(DistrictMaster districtMaster,TeacherDetail teacherDetail,JobOrder jobOrder,EligibilityMaster eligibilityMaster)
	{
		List<EligibilityTranscript> eligibilityTranscript= null;
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 	= 	Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion4 	= 	Restrictions.eq("eligibilityMaster",eligibilityMaster);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			eligibilityTranscript = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityTranscript;
	}
	
	@Transactional(readOnly=false)
	public List<EligibilityTranscript> findAllTeacher(List<TeacherDetail> teacherDetailList)
	{
		List<EligibilityTranscript> eligibilityTranscript= new ArrayList<EligibilityTranscript>();
		try
		{
			if(teacherDetailList.size()>0){
				Session session 		= 	getSession();
				Criteria criteria 		= 	session.createCriteria(getPersistentClass());
				Criterion criterion1 	= 	Restrictions.in("teacherDetail",teacherDetailList);
				criteria.add(criterion1);
				eligibilityTranscript = criteria.list();
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityTranscript;
	}
	
	@Transactional(readOnly=false)
	public List<EligibilityTranscript> findAllTeacherBYDistrict(List<TeacherDetail> teacherDetailList,DistrictMaster districtMaster)
	{
		List<EligibilityTranscript> eligibilityTranscript= new ArrayList<EligibilityTranscript>();
		List<Object> data =  new ArrayList<Object>();
		try
		{
			if(teacherDetailList.size()>0){
				Session session 		= 	getSession();
				Criteria criteria 		= 	session.createCriteria(getPersistentClass());
				Criterion criterion1 	= 	Restrictions.in("teacherDetail",teacherDetailList);
				criteria.add(Restrictions.eq("districtMaster",districtMaster));
				criteria.add(criterion1);								
				criteria.setProjection(Projections.projectionList()
						.add(Projections.property("teacherDetail.teacherId"))
						.add(Projections.property("districtMaster.districtId"))
						.add(Projections.property("jobOrder.jobId"))
						.add(Projections.property("eligibilityMaster.eligibilityId"))
						.add(Projections.property("eligibilityStatusMaster.eligibilityStatusId"))
						.add(Projections.property("userMaster.userId"))
						.add(Projections.property("teacherAcademics.academicId"))
						);
				data = criteria.list();
				for (Iterator it = data.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[0]!=null)
		            {
		            	///System.out.println(" --->>>    "+row[0]+"  ||  "+row[1]+"  ||  "+row[2]+"  ||  "+row[3]+"  ||  "+row[4]+" ||  "+row[5]+"  ||  "+row[6]);
		            	EligibilityTranscript eligibilityTranscript2 = new EligibilityTranscript();
		            	TeacherDetail teacherDetail = new TeacherDetail();
		            	teacherDetail.setTeacherId(Integer.parseInt(row[0].toString()));
		            	eligibilityTranscript2.setTeacherDetail(teacherDetail);
		            	DistrictMaster districtMaster2 = new DistrictMaster();
		            	districtMaster2.setDistrictId(Integer.parseInt(row[1].toString()));
		            	eligibilityTranscript2.setDistrictMaster(districtMaster2);
		            	JobOrder jobOrder = new JobOrder();
		            	jobOrder.setJobId(Integer.parseInt(row[2].toString()));
		            	eligibilityTranscript2.setJobOrder(jobOrder);
		            	EligibilityMaster eligibilityMaster = new EligibilityMaster();
		            	eligibilityMaster.setEligibilityId(Integer.parseInt(row[3].toString()));
		            	eligibilityTranscript2.setEligibilityMaster(eligibilityMaster);
		            	EligibilityStatusMaster eligibilityStatusMaster = new EligibilityStatusMaster();
		            	eligibilityStatusMaster.setEligibilityStatusId(Integer.parseInt(row[4].toString()));
		            	eligibilityTranscript2.setEligibilityStatusMaster(eligibilityStatusMaster);
		            	UserMaster userMaster = new UserMaster();
		            	userMaster.setUserId(Integer.parseInt(row[5].toString()));
		            	eligibilityTranscript2.setUserMaster(userMaster);
		            	TeacherAcademics teacherAcademics = new TeacherAcademics();
		            	teacherAcademics.setAcademicId(Long.parseLong(row[6].toString()));
		            	eligibilityTranscript2.setTeacherAcademics(teacherAcademics);
		            	eligibilityTranscript.add(eligibilityTranscript2);
		            }
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityTranscript;
	}

}
