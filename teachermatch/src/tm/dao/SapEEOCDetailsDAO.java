package tm.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.quartz.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.sun.mail.imap.protocol.Status;

import tm.bean.EligibilityVerificationHistroy;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SapEEOCDetails;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.EligibilityMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.user.UserMasterDAO;

public class SapEEOCDetailsDAO extends GenericHibernateDAO<SapEEOCDetails, Integer> 
{
	public SapEEOCDetailsDAO() 
	{
		super(SapEEOCDetails.class);
	}
	
	@Transactional(readOnly=false)
	public List<SapEEOCDetails> findByTeacherId(TeacherDetail teacherDetail)
	{
		List<SapEEOCDetails> sapEEOCDetailsList = new ArrayList<SapEEOCDetails>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			sapEEOCDetailsList = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return sapEEOCDetailsList;
	}
	
	@Transactional(readOnly=false)
	public List<SapEEOCDetails> findByTeacherIdList(List<TeacherDetail> lstTeacherDetail)
	{
		List<SapEEOCDetails> sapEEOCDetailsList =  new ArrayList<SapEEOCDetails>();
		try
		{
			if(lstTeacherDetail.size()>0){
				Criterion criterion1 = Restrictions.in("teacherDetail",lstTeacherDetail);
				sapEEOCDetailsList = findByCriteria(criterion1);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return sapEEOCDetailsList;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherDetail> getAllTeacherDetail()
	{
		List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			
			lstTeacher = criteria.list();
			System.out.println("getAllTeacherDetail:::::::::::::::::::::::"+lstTeacher.size());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstTeacher;
	}
	
	// for Optimization of onBoarding Dashboard
	

	@Transactional(readOnly=true)
	public List<Integer> getAllTeacherDetailOp(List<TeacherDetail> teacherDetails)
	{
		List<Integer> lstTeacher=new ArrayList<Integer>();
		
		if(teacherDetails!=null && teacherDetails.size()>0)
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.in("teacherDetail", teacherDetails));
			criteria.setProjection(Projections.distinct(Projections.property("teacherDetail")));
			criteria.setProjection(Projections.property("teacherDetail.teacherId"));
			
			lstTeacher = criteria.list();
			
			System.out.println("getAllTeacherDetailOp:::::::::::::::::::::::"+lstTeacher.size());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstTeacher;
	}

}
