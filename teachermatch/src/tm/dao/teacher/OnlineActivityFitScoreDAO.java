package tm.dao.teacher;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.OnlineActivityFitScore;
import tm.dao.generic.GenericHibernateDAO;

public class OnlineActivityFitScoreDAO extends GenericHibernateDAO<OnlineActivityFitScore, Integer> 
{
	public OnlineActivityFitScoreDAO() 
	{
		super(OnlineActivityFitScore.class);
	}
	
	@Transactional(readOnly=false)
	public List<OnlineActivityFitScore> getRecordsByDistAndTeacherId(DistrictMaster districtMaster, TeacherDetail teacherDetail){
		
		List<OnlineActivityFitScore> allRecord = new ArrayList<OnlineActivityFitScore>();
		try{
			Criterion criterion1	=	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2	=	Restrictions.eq("teacherDetail", teacherDetail);
			
			allRecord				=	findByCriteria(criterion1,criterion2);
			
		} catch (Exception exception){
			exception.printStackTrace();
		}
		return allRecord;
	}
	
	@Transactional(readOnly=false)
	public List<OnlineActivityFitScore> getRecordsByDistId(DistrictMaster districtMaster){
		
		List<OnlineActivityFitScore> allRecord = new ArrayList<OnlineActivityFitScore>();
		try{
			Criterion criterion1	=	Restrictions.eq("districtMaster", districtMaster);
			allRecord				=	findByCriteria(criterion1);
			
		} catch (Exception exception){
			exception.printStackTrace();
		}
		return allRecord;
	}
	
	@Transactional(readOnly=false)
	public List<OnlineActivityFitScore> getRecordsByDistrictAndTeacherList(DistrictMaster districtMaster,List<TeacherDetail> teacherDetails){
		
		List<OnlineActivityFitScore> allRecord = new ArrayList<OnlineActivityFitScore>();
		try{
			Criterion criterion1	=	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2	=	Restrictions.in("teacherDetail", teacherDetails);
			allRecord				=	findByCriteria(criterion1,criterion2);
			
		} catch (Exception exception){
			exception.printStackTrace();
		}
		return allRecord;
	}
	
	@Transactional(readOnly=false)
	public OnlineActivityFitScore getOAFSScore(TeacherDetail teacherDetail,JobOrder jobOrder){
		OnlineActivityFitScore oAFSS=null;
		List<OnlineActivityFitScore> allRecord = new ArrayList<OnlineActivityFitScore>();
		try{
			Criterion criterion1	=	Restrictions.eq("jobCategoryMaster", jobOrder.getJobCategoryMaster());
			Criterion criterion2	=	Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion3	=	Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
			allRecord				=	findByCriteria(criterion1,criterion2,criterion3);
			if(allRecord.size()!=0){
				oAFSS=allRecord.get(0);
			}
			
		} catch (Exception exception){
			exception.printStackTrace();
		}
		return oAFSS;
	}
	
	@Transactional(readOnly=true)
	public List getAllOnlineActivityStatusScoreBreakAvgList(List<TeacherDetail> teacherDetail,JobOrder jobOrder)
	{
		List resultList =new ArrayList();
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobCategoryMaster", jobOrder.getJobCategoryMaster());
			Criterion criterion4 = Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
			Session session = getSession();
			List result1  = session.createCriteria(getPersistentClass()) 
			.add(criterion1) 
			.add(criterion2) 
			.add(criterion4) 
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherDetail"))
					.add(Projections.avg("minScore"))
					.add(Projections.avg("maxScore")) 
			).list();
			
			resultList.add(result1);		
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return resultList;
	}
}
