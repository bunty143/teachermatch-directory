package tm.dao.teacher;

import tm.bean.teacher.StudentTeacherExperience;
import tm.dao.generic.GenericHibernateDAO;

public class StudentTeacherExperienceDAO extends GenericHibernateDAO<StudentTeacherExperience, Integer>{

	public StudentTeacherExperienceDAO(){		
		super(StudentTeacherExperience.class);
	}

}
