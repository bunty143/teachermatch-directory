package tm.dao.teacher;

import tm.bean.teacher.SpInboundAPICallRecordLog;
import tm.dao.generic.GenericHibernateDAO;

public class SpInboundAPICallRecordLogDAO extends GenericHibernateDAO<SpInboundAPICallRecordLog, Integer>{

	public SpInboundAPICallRecordLogDAO(){		
		super(SpInboundAPICallRecordLog.class);
	}
}
