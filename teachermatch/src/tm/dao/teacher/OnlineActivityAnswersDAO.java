package tm.dao.teacher;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.teacher.OnlineActivityAnswers;
import tm.bean.teacher.OnlineActivityQuestionSet;
import tm.dao.generic.GenericHibernateDAO;

public class OnlineActivityAnswersDAO extends GenericHibernateDAO<OnlineActivityAnswers, Integer> 
{
	public OnlineActivityAnswersDAO() 
	{
		super(OnlineActivityAnswers.class);
	}
	@Transactional(readOnly=false)
	public List<OnlineActivityAnswers> findOnlineActivityAnswers(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster)
	{
		List<OnlineActivityAnswers> onlineActivityAnswersList = new ArrayList<OnlineActivityAnswers>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("districtId", districtMaster.getDistrictId());
			Criterion criterion3 = Restrictions.eq("jobCategoryId",jobCategoryMaster.getJobCategoryId());
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			onlineActivityAnswersList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return onlineActivityAnswersList;
	}
	@Transactional(readOnly=false)
	public List<OnlineActivityAnswers> findOnlineActivityAnswersByQuestionSetId(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,OnlineActivityQuestionSet onlineActivityQuestionSet)
	{
		List<OnlineActivityAnswers> onlineActivityAnswersList = new ArrayList<OnlineActivityAnswers>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("districtId", districtMaster.getDistrictId());
			Criterion criterion3 = Restrictions.eq("jobCategoryId",jobCategoryMaster.getJobCategoryId());
			Criterion criterion4 = Restrictions.eq("onlineActivityQuestionSet",onlineActivityQuestionSet);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			onlineActivityAnswersList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return onlineActivityAnswersList;
	}
	@Transactional(readOnly=true)
	public Double[] getAllFinalizeStatusScoreAvg(TeacherDetail teacherDetail,OnlineActivityQuestionSet onlineActivityQuestionSet)
	{
		System.out.println("::::::::::getAllFinalizeStatusScoreAvg--:::::::::::::::");
		Double[] arrMix=new Double[2];
		Double scoreP=0.0;
		Double maxScore=0.0;
		List<OnlineActivityAnswers> onlineActivityAnswersList = new ArrayList<OnlineActivityAnswers>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("onlineActivityQuestionSet", onlineActivityQuestionSet);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			onlineActivityAnswersList = criteria.list();
			System.out.println("onlineActivityAnswersList::::::::"+onlineActivityAnswersList.size());
			System.out.println("teacherDetail::::::::"+teacherDetail.getTeacherId());
			for (OnlineActivityAnswers onlineActivityAnswers : onlineActivityAnswersList) {
				if(onlineActivityAnswers.getScore()!=null)
				scoreP+=Double.parseDouble(onlineActivityAnswers.getScore()+"");
				if(onlineActivityAnswers.getTotalScore()!=null)
				maxScore+=Double.parseDouble(onlineActivityAnswers.getTotalScore()+"");
			}
			arrMix[0]=scoreP;
			arrMix[1]=maxScore;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return arrMix;
	}
	
	@Transactional(readOnly=true)
	public List<OnlineActivityAnswers> getAllFinalizeStatusScoreAvgForTeacherList(List<TeacherDetail> teacherDetailList,List<OnlineActivityQuestionSet> onlineActivityQuestionSetList)
	{
		System.out.println("::::::::::getAllFinalizeStatusScoreAvgForTeacherList-->:::::::::::::::");
		List<OnlineActivityAnswers> onlineActivityAnswersList = new ArrayList<OnlineActivityAnswers>();
		try 
		{
			if(teacherDetailList!=null && teacherDetailList.size()>0 && onlineActivityQuestionSetList!=null && onlineActivityQuestionSetList.size()>0){
				Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetailList);
				Criterion criterion2 = Restrictions.in("onlineActivityQuestionSet", onlineActivityQuestionSetList);
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				onlineActivityAnswersList = criteria.list();
				System.out.println("onlineActivityAnswersList::::::::"+onlineActivityAnswersList.size());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return onlineActivityAnswersList;
	}
}
