package tm.dao.teacher;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.teacher.OnlineActivityQuestionSet;
import tm.bean.teacher.OnlineActivityStatus;
import tm.dao.generic.GenericHibernateDAO;

public class OnlineActivityStatusDAO extends GenericHibernateDAO<OnlineActivityStatus, Integer> 
{
	public OnlineActivityStatusDAO() 
	{
		super(OnlineActivityStatus.class);
	}
	
	@Transactional(readOnly=false)
	public OnlineActivityStatus findOnlineActivityStatus(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster)
	{
		List<OnlineActivityStatus> onlineActivityStatusList = new ArrayList<OnlineActivityStatus>();
		OnlineActivityStatus onlineActivityStatus=null; 
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion3 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			onlineActivityStatusList = criteria.list();
			
			if(onlineActivityStatusList.size()>0){
				onlineActivityStatus=onlineActivityStatusList.get(0);
			}
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return onlineActivityStatus;
	}
	@Transactional(readOnly=false)
	public OnlineActivityStatus findOnlineActivityStatusBySetId(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,OnlineActivityQuestionSet onlineActivityQuestionSet)
	{
		List<OnlineActivityStatus> onlineActivityStatusList = new ArrayList<OnlineActivityStatus>();
		OnlineActivityStatus onlineActivityStatus=null; 
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion3 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criterion criterion4 = Restrictions.eq("onlineActivityQuestionSet",onlineActivityQuestionSet);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			onlineActivityStatusList = criteria.list();
			
			if(onlineActivityStatusList.size()>0){
				onlineActivityStatus=onlineActivityStatusList.get(0);
			}
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return onlineActivityStatus;
	}
	@Transactional(readOnly=false)
	public List<OnlineActivityStatus> findOnlineActivityStatusList(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster)
	{
		List<OnlineActivityStatus> onlineActivityStatusList = new ArrayList<OnlineActivityStatus>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion3 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			onlineActivityStatusList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return onlineActivityStatusList;
	}
	@Transactional(readOnly=false)
	public List<OnlineActivityStatus> findExpiredAcivityByTeacher(List<TeacherDetail> teacherDetail,DistrictMaster districtMaster)
	{
		List<OnlineActivityStatus> onlineActivityStatusList = new ArrayList<OnlineActivityStatus>();
		try 
		{
			if(teacherDetail.size()>0){
				Session session = getSession();
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 = Restrictions.eq("status", "I");
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				onlineActivityStatusList = criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return onlineActivityStatusList;
	}
	
	@Transactional(readOnly=false)
	public List<OnlineActivityStatus> findExpiredAcivityByTeacher(TeacherDetail teacherDetail,DistrictMaster districtMaster)
	{
		List<OnlineActivityStatus> onlineActivityStatusList = new ArrayList<OnlineActivityStatus>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion3 = Restrictions.ne("status", "C");
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			onlineActivityStatusList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return onlineActivityStatusList;
	}
	@Transactional(readOnly=false)
	public OnlineActivityStatus findOnlineActivityStatusByHBD(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<OnlineActivityStatus> onlineActivityStatusList = new ArrayList<OnlineActivityStatus>();
		OnlineActivityStatus onlineActivityStatus=null; 
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			criteria.add(criterion3);
			onlineActivityStatusList = criteria.list();
			
			if(onlineActivityStatusList.size()>0){
				onlineActivityStatus=onlineActivityStatusList.get(0);
			}
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return onlineActivityStatus;
	}
}
