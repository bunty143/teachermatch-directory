package tm.dao;

import tm.bean.CmsEventScheduleMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CmsEventScheduleMasterDAO extends GenericHibernateDAO<CmsEventScheduleMaster, Integer>{

	public CmsEventScheduleMasterDAO() {
		super(CmsEventScheduleMaster.class);
		}

}
