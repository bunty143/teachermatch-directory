package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.DistrictSpecificDocuments;
import tm.bean.DocumentFileMaster;
import tm.bean.JobOrder;
import tm.bean.SchoolSelectedByCandidate;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;


public class DistrictSpecificDocumentsDAO extends GenericHibernateDAO<DistrictSpecificDocuments, Integer> 
{
	public DistrictSpecificDocumentsDAO() {
		super(DistrictSpecificDocuments.class);
	}

	@Transactional(readOnly=false)
	public List<DistrictSpecificDocuments> getJobDocumentsDetails(List<Integer> filterList, int sortingcheck,Order sortOrderStrVal,int start , int limit,Boolean flag)
	{
		System.out.println("============getJobDocumentsDetails call==========");
		List<DistrictSpecificDocuments> lstJobDocuments= new ArrayList<DistrictSpecificDocuments>();
		try 
		{
			System.out.println(" sortingcheck :: "+sortingcheck+" sortOrderStrVal :: "+sortOrderStrVal+" start :: "+start+" limit :: "+limit);
			Session session = getSession();
			Criteria criteria= session.createCriteria(getPersistentClass());
			Criterion criterion2 = null;
			Criterion criterion = null;
			/*if(districtMaster!=null)
			{*/
				//criterion = Restrictions.eq("districtMaster",filterList);
				criterion = Restrictions.in("documentId", filterList);
				criteria.add(criterion);
			//}
		
			if(sortingcheck==0)
				criteria.addOrder(sortOrderStrVal);

			Criteria c1= null;
			Criteria c2= null;
			Criteria c3= null;
			if(sortingcheck==1){
				c1 = criteria.createCriteria("userMaster");
				c1.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==2){
				criteria.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==3){
				criteria.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==4){
				c3 = criteria.createCriteria("jobCategoryMaster");
				c3.addOrder(sortOrderStrVal);
			}

			if(flag)
			{
				criteria.setFirstResult(start);
				criteria.setMaxResults(limit);
			}
			try {
				lstJobDocuments = criteria.list();
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("List Size :: "+lstJobDocuments.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobDocuments;
	}

	@Transactional(readOnly=false)
	public int getTotalJobDocumentsDetails(List<Integer> filterList, int sortingcheck,Order sortOrderStrVal,int start , int limit)
	{
		System.out.println("============getTotalJobDocumentsDetails call==========");
		int rowCount = 0;
		List<DistrictSpecificDocuments> lstJobDocuments= new ArrayList<DistrictSpecificDocuments>();
		try 
		{
			System.out.println(" sortingcheck :: "+sortingcheck+" sortOrderStrVal :: "+sortOrderStrVal+" start :: "+start+" limit :: "+limit);
			Session session = getSession();
			Criteria criteria= session.createCriteria(getPersistentClass());
			Criterion criterion = null;
			Criterion criterion2 = null;
			/*if(districtMaster!=null)
			{*/
				criterion = Restrictions.in("documentId",filterList); 
				criteria.add(criterion);
			//}
			if(sortingcheck==0)
				criteria.addOrder(sortOrderStrVal);

			Criteria c1= null;
			Criteria c2= null;
			Criteria c3= null;
			if(sortingcheck==1){
				c1 = criteria.createCriteria("userMaster");
				c1.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==2){
				criteria.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==3){
				criteria.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==4){
				c3 = criteria.createCriteria("jobCategoryMaster");
				c3.addOrder(sortOrderStrVal);
			}
			lstJobDocuments = criteria.list();
			rowCount = lstJobDocuments.size();
			System.out.println("List Size :: "+rowCount);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return rowCount;
	}

	@Transactional(readOnly=false)
	public List<DistrictSpecificDocuments> checkJobCategoryExists(UserMaster userMaster)
	{
		List<DistrictSpecificDocuments> jobDocumentsByDistricts = new ArrayList<DistrictSpecificDocuments>();
		Session session = getSession();
		Criteria criteria= session.createCriteria(getPersistentClass());
		Criterion criterion = Restrictions.eq("userMaster",userMaster); 
		criteria.add(criterion);
		jobDocumentsByDistricts = criteria.list();
		return jobDocumentsByDistricts;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificDocuments> getJobDocumentsForCandidate(List<Integer> lst, int sortingcheck,Order sortOrderStrVal,int start , int limit,Boolean flag)
	{
		System.out.println("============getJobDocumentsDetails call==========");
		List<DistrictSpecificDocuments> lstJobDocuments= new ArrayList<DistrictSpecificDocuments>();
		try 
		{
			System.out.println(" sortingcheck :: "+sortingcheck+" sortOrderStrVal :: "+sortOrderStrVal+" start :: "+start+" limit :: "+limit);
			Session session = getSession();
			Criteria criteria= session.createCriteria(getPersistentClass());
			Criterion criterion2 = null;
			Criterion criterion = null;
			criterion = Restrictions.in("jobDocsId",lst);
			criteria.add(criterion);

		
			if(sortingcheck==0)
				criteria.addOrder(sortOrderStrVal);

			Criteria c1= null;
			Criteria c2= null;
			Criteria c3= null;
			if(sortingcheck==1){
				//c1 = criteria.createCriteria("userMaster");
				c1.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==2){
				c2 = criteria.createCriteria("districtMaster");
				c2.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==3){
				criteria.addOrder(sortOrderStrVal);
			}
			/*if(sortingcheck==4){
				c3 = criteria.createCriteria("jobCategoryMaster");
				c3.addOrder(sortOrderStrVal);
			}*/
			if(flag)
			{
				criteria.setFirstResult(start);
				criteria.setMaxResults(limit);
			}
			lstJobDocuments = criteria.list();
			System.out.println("List Size :: "+lstJobDocuments.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobDocuments;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> getAllRecords(DistrictMaster districtMaster) 
	{
		List<Integer> districtSpecificDocuments = new ArrayList<Integer>();
		Session session = getSession();
		String sql = "";
		sql = "SELECT documentId FROM districtspecificdocuments WHERE districtId = :districtId GROUP BY documentName, jobCategoryId";
		try {
			Query query = session.createSQLQuery(sql);
			query.setParameter("districtId", districtMaster );
			districtSpecificDocuments  = query.list();
		  } catch (HibernateException e) {
			e.printStackTrace();
		  }
		  return districtSpecificDocuments;
	}
	
	@Transactional(readOnly=false)
	public int countRecord(String documentName, JobCategoryMaster jobCategoryMaster )
	{
		int rowCount = 0;
		List<DistrictSpecificDocuments> jobDocumentsByDistricts = new ArrayList<DistrictSpecificDocuments>();
		Session session = getSession();
		Criteria criteria= session.createCriteria(getPersistentClass());
		Criterion criterion = Restrictions.eq("documentName",documentName); 
		Criterion criterion2 = Restrictions.eq("jobCategoryId",jobCategoryMaster); 
		criteria.add(criterion);
		criteria.add(criterion2);
		//	Criterion criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster); 
		jobDocumentsByDistricts = criteria.list();
		rowCount = jobDocumentsByDistricts.size();
		return rowCount;
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificDocuments> findByDistrict(DistrictMaster districtMaster){
		List<DistrictSpecificDocuments> jobDocumentsList =  new ArrayList<DistrictSpecificDocuments>();
		try{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			jobDocumentsList = findByCriteria(criterion1);
		}catch(Exception e){
			e.printStackTrace();
		}			
	  return jobDocumentsList;	
	}
	
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificDocuments> findByDistrictJobCatgoryAndDocumentName (DistrictMaster districtMaster,List<JobCategoryMaster> jobCategoryMasters,DocumentFileMaster fileMaster){
		List<DistrictSpecificDocuments> jobDocumentsList =  new ArrayList<DistrictSpecificDocuments>();
		try{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.in("jobCategoryMaster",jobCategoryMasters);
			Criterion criterion3 = Restrictions.eq("documentFileMaster",fileMaster);
			
			jobDocumentsList = findByCriteria(criterion1,criterion2,criterion3);
		}catch(Exception e){
			e.printStackTrace();
		}			
	  return jobDocumentsList;	
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificDocuments> findByDistrictJobCatgory(DistrictMaster districtMaster,List<JobCategoryMaster> jobCategoryMasters){
		List<DistrictSpecificDocuments> jobDocumentsList =  new ArrayList<DistrictSpecificDocuments>();
		try{
			if(districtMaster!=null && jobCategoryMasters!=null && jobCategoryMasters.size()>0){
				Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion2 = Restrictions.in("jobCategoryMaster",jobCategoryMasters);
				jobDocumentsList = findByCriteria(Order.asc("documentFileMaster"),criterion1,criterion2);
			}
		}catch(Exception e){
			e.printStackTrace();
		}			
	  return jobDocumentsList;	
	}
}
