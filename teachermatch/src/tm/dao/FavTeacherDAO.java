package tm.dao;

import java.util.List;
import java.util.Random;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.config.ListFactoryBean;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.FavTeacher;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPortfolioStatus;
import tm.dao.generic.GenericHibernateDAO;

public class FavTeacherDAO extends GenericHibernateDAO<FavTeacher, Integer>
{
	public FavTeacherDAO() 
	{
		super(FavTeacher.class);
	}
	
	@Transactional(readOnly=false)
	public FavTeacher findFavTeacherByTeacher(TeacherDetail teacherDetail)
	{
		List<FavTeacher> lstFavTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			lstFavTeacher = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstFavTeacher==null || lstFavTeacher.size()==0)
		return null;
		else
		return lstFavTeacher.get(0);
	}
	
	/* 
	 * This method return random favourite Teacher object from database.
	 * IF we specify favTeacher as argument it will not search that favTeacher.
	 * If we specify null as argument it will search in all favTeacher.
	 */
	@Transactional(readOnly=false)
	public FavTeacher findRandomFavTeacher(FavTeacher favTeacher)
	{
		
		List<FavTeacher> lstFavTeacher= null;
		FavTeacher ranFavTeacher = null;
		int index;
		try 
		{
			if(favTeacher==null)
			{
				
				lstFavTeacher = findByCriteria();
				
			}
			else
			{
				
				Criterion criterion1 = Restrictions.ne("favTeacherId",favTeacher.getFavTeacherId());			
				lstFavTeacher = findByCriteria(criterion1);
			}	
			if(lstFavTeacher!=null && lstFavTeacher.size()!=0)
			{			
				Random randomGenerator =  new Random();
				index = randomGenerator.nextInt(lstFavTeacher.size());				
				ranFavTeacher = lstFavTeacher.get(index);
			}
			
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return ranFavTeacher;
		
	}
	
}
