package tm.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.dao.generic.GenericHibernateDAO;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStrikeLog;

public class TeacherStrikeLogDAO extends GenericHibernateDAO<TeacherStrikeLog, Integer> 
{
	public TeacherStrikeLogDAO() {
		super(TeacherStrikeLog.class);
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Total no. of Strikes By Teacher.
	 */

	@Transactional(readOnly=false)
	public List<TeacherStrikeLog> getTotalStrikesByTeacher(TeacherDetail teacherDetail, TeacherAssessmentdetail teacherAssessmentdetail)
	{
		List<TeacherStrikeLog> teacherStrikeLogList= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("teacherAssessmentdetail",teacherAssessmentdetail);
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);

			teacherStrikeLogList = findByCriteria(criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherStrikeLogList;
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment responses of assessments.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> findAssessmentsStrikesForBase(List<TeacherDetail> teacherDetails,List<TeacherAssessmentdetail> teacherAssessmentdetails) 
	{
		
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		
		List result;
		Map<Integer, Integer> assessmentAttempsMap = null;
		try {
			Session session = getSession();

			Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion2 = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion3 = Restrictions.in("teacherAssessmentdetail",teacherAssessmentdetails);
			
			result = session.createCriteria(getPersistentClass()) 
			.add(criterion1)
			.add(criterion2)
			.add(criterion3)
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherDetail"))
					.add(Projections.count("teacherDetail"))           
			).list();

			assessmentAttempsMap = new HashMap<Integer, Integer>();
			TeacherDetail teacherDetail = null;
			
			for (Object object : result) {
				
					Object rows[] = (Object[])object;
					//System.out.println(rows[0]+ " -- " +rows[1]);
					teacherDetail=((TeacherDetail)rows[0]);
					assessmentAttempsMap.put(teacherDetail.getTeacherId(), (Integer)rows[1]);
					//System.out.println("Response:::::::::::: "+result.size());
			}
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//System.out.println("vv: "+assessmentAttempsMap.get(75));
		return assessmentAttempsMap;
	}
}
