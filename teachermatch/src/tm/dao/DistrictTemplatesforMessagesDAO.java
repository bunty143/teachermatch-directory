package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictTemplatesforMessages;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictTemplatesforMessagesDAO extends GenericHibernateDAO<DistrictTemplatesforMessages, Integer> {
	
	public DistrictTemplatesforMessagesDAO()
	{
		super(DistrictTemplatesforMessages.class);
	}
	
	@Transactional(readOnly=false)
	public List<DistrictTemplatesforMessages> findByDistrict(Order sortOrderStrVal,DistrictMaster districtMaster)
	{
		List<DistrictTemplatesforMessages> districtTemplatesforMessages = new ArrayList<DistrictTemplatesforMessages>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion c1 = Restrictions.eq("districtMaster",districtMaster);
			criteria.addOrder(sortOrderStrVal);
			criteria.add(c1);
			districtTemplatesforMessages = criteria.list();
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return districtTemplatesforMessages;
	}
	@Transactional(readOnly=false)
	public List<DistrictTemplatesforMessages> findByDistrictAndTemplateName(DistrictMaster districtMaster,String templateName)
	{
		List<DistrictTemplatesforMessages> districtTemplatesforMessagesList = null;
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion c1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion c3 = Restrictions.eq("status","A");
			Criterion c2 = Restrictions.eq("templateName",templateName.trim());
			criteria.add(c1);
			criteria.add(c2);
			criteria.add(c3);
			districtTemplatesforMessagesList = criteria.list();
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return districtTemplatesforMessagesList;
	}
	@Transactional(readOnly=false)
	public String findSubjectByDistrictAndTemplateName(DistrictMaster districtMaster,String templateName)
	{
		String subjectLine="";
		List<String> districtTemplatesforMessagesSubjectList = null;
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion c1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion c3 = Restrictions.eq("status","A");
			Criterion c2 = Restrictions.eq("templateName",templateName.trim());
			criteria.add(c1);
			criteria.add(c2);
			criteria.add(c3);
			criteria.setProjection(Projections.projectionList().add(Projections.property("subjectLine"))); 
			districtTemplatesforMessagesSubjectList = criteria.list();	
			if(districtTemplatesforMessagesSubjectList!=null && districtTemplatesforMessagesSubjectList.size()>0)
			subjectLine=districtTemplatesforMessagesSubjectList.get(0);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return subjectLine;
	}
}
