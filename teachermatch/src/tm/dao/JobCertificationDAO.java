package tm.dao;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobCertification;
import tm.bean.JobOrder;
import tm.bean.master.CertificateTypeMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobCertificationDAO extends GenericHibernateDAO<JobCertification, Integer> 
{
	public JobCertificationDAO() 
	{
		super(JobCertification.class);
	}
	/*@Transactional(readOnly=false)
	public JobCertification findCertificationByJob(JobOrder jobOrder,String CertificationType)
	{
		List<JobCertification> lstJobCertification= null;
		try{  
			if(!CertificationType.equals("")){
				Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
				Criterion criterion2 = Restrictions.ilike("certType","%"+CertificationType.trim()+"%" );
				lstJobCertification = findByCriteria(criterion1,criterion2);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		if(lstJobCertification==null || lstJobCertification.size()==0)
			return null;
		else
			return lstJobCertification.get(0);
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<Integer,JobCertification> findCertificationByJob() 
	{
		Session session = getSession();
		List result = session.createCriteria(getPersistentClass())
		.list();

		Map<Integer,JobCertification> JobCertificationMap = new HashMap<Integer, JobCertification>();
		JobCertification jobCertification = null;
		int i=0;
		for (Object object : result) {
			jobCertification=((JobCertification)object);
			JobCertificationMap.put(new Integer(""+i),jobCertification);
			i++;
		}
		return JobCertificationMap;
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<Integer,JobCertification> findCertificationByJobs(List<JobOrder> lstJobOrder) 
	{
		Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
		Session session = getSession();
		List result = session.createCriteria(getPersistentClass())
		.add(criterion1)
		.list();

		Map<Integer,JobCertification> JobCertificationMap = new HashMap<Integer, JobCertification>();
		JobCertification jobCertification = null;
		int i=0;
		for (Object object : result) {
			jobCertification=((JobCertification)object);
			JobCertificationMap.put(new Integer(""+i),jobCertification);
			i++;
		}
		return JobCertificationMap;
	}*/
	@Transactional(readOnly=false)
	public List<JobOrder> findCertificationByJob(String certType) 
	{
		System.out.println("certType: "+certType);
		
		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		List<JobCertification> jobCertificationList= new ArrayList<JobCertification>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			//Criterion criterion1 = Restrictions.ilike("certType","%"+certType.trim()+"%" );
			CertificateTypeMaster certificateTypeMaster = new CertificateTypeMaster();
			certificateTypeMaster.setCertTypeId(Integer.parseInt(certType));
			Criterion criterion1 = Restrictions.eq("certificateTypeMaster",certificateTypeMaster );
			criteria.add(criterion1);
			jobCertificationList = criteria.list();
			Iterator itr = jobCertificationList.iterator();
			JobCertification jobCertification =null;
			while(itr.hasNext())
			{
				jobCertification=(JobCertification)itr.next();
				jobOrderList.add(jobCertification.getJobId());
				//System.out.print(jobCertification.getJobId().getJobId()+",");
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
	return jobOrderList;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> findCertificationByJobWithState(List<CertificateTypeMaster> certificateTypeMasterList) 
	{
		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		List<JobCertification> jobCertificationList= new ArrayList<JobCertification>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.in("certificateTypeMaster",certificateTypeMasterList );
			criteria.add(criterion1);
			jobCertificationList = criteria.list();
			Iterator itr = jobCertificationList.iterator();
			JobCertification jobCertification =null;
			while(itr.hasNext())
			{
				jobCertification=(JobCertification)itr.next();
				jobOrderList.add(jobCertification.getJobId());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
	return jobOrderList;
	}
	
	@Transactional(readOnly=false)
	public List<JobCertification> findCertificationByJobOrder(JobOrder jobOrder) 
	{
		
		List<JobCertification> jobCertificationList= new ArrayList<JobCertification>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			criteria.add(criterion1);
			jobCertificationList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
	return jobCertificationList;
	}
	
	@Transactional(readOnly=false)
	public List<JobCertification> findCertificationByJobOrders(List<JobOrder> jobOrders) 
	{
		
		List<JobCertification> jobCertificationList= new ArrayList<JobCertification>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.in("jobId",jobOrders);
			criteria.add(criterion1);
			jobCertificationList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
	return jobCertificationList;
	}
	
	@Transactional(readOnly=false)
	public void deleteFromJobCertification(List<JobCertification> jobCertifications){
		if(jobCertifications.size()>0)
		{
			Integer [] jobCertificationIds = new Integer[jobCertifications.size()];
			int i=0;
			for (JobCertification jobCertification : jobCertifications) {
				jobCertificationIds[i] = jobCertification.getJobCertificationId();
				i++;
			}
			try {
				String hql = "delete from JobCertification WHERE jobCertificationId IN (:jobCertifications)";
				Query query = null;
				Session session = getSession();
	
				query = session.createQuery(hql);
				query.setParameterList("jobCertifications", jobCertificationIds);
				System.out.println("  OOO  "+query.executeUpdate());
			} catch (HibernateException e) {
				e.printStackTrace();
			}
		}
	}
	

	@Transactional(readOnly=false)
    public List<JobCertification> findCertificationsByJobOrders(List<JobOrder> jobOrders,List<CertificateTypeMaster> certObjList) 
    {
                    
        List<JobCertification> jobCertificationList= new ArrayList<JobCertification>();
        try 
        {                                                                                              
	        Criterion criterion1 = Restrictions.in("jobId",jobOrders);
	      //  Criterion criterion2 = Restrictions.in("certificateTypeMaster",certObjList);
	        jobCertificationList = findByCriteria(Order.asc("certificateTypeMaster"),criterion1);
        } 
        catch (Exception e) {
    		e.printStackTrace();
        }                              
    return jobCertificationList;
    }

}
