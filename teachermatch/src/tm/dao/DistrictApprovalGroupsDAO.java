package tm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictApprovalGroups;
import tm.bean.JobOrder;
import tm.bean.cgreport.DistrictMaxFitScore;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictApprovalGroupsDAO  extends GenericHibernateDAO<DistrictApprovalGroups, Integer>
{
	public DistrictApprovalGroupsDAO()
	{
		super(DistrictApprovalGroups.class);
	}
	
	@Transactional(readOnly=true)
	public List<DistrictApprovalGroups> findDistrictApprovalGroupsByDistrict(DistrictMaster districtMaster)
	{
		List <DistrictApprovalGroups> districtApprovalGroupsList=null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.isNull("jobCategoryId");//Restrictions.eq("jobCategoryId",districtMaster);
			Criterion criterion3 = Restrictions.isNull("jobId");
			Criterion criterion4 = Restrictions.eq("status","A");
			Order order = Order.asc("districtApprovalGroupsId");
			
			Criterion criterion2And3 = Restrictions.and(criterion2, criterion3);
			Criterion criterion = Restrictions.and(criterion1, criterion2And3);
			districtApprovalGroupsList = findByCriteria(order, criterion, criterion4);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return districtApprovalGroupsList;
	}
	
	@Transactional(readOnly=true)
	public List<DistrictApprovalGroups> findDistrictApprovalGroupsByJobCategory(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster)
	{
		List <DistrictApprovalGroups> districtApprovalGroupsList=null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.eq("jobCategoryId",jobCategoryMaster);
			Criterion criterion3 = Restrictions.isNull("jobId");
			Criterion criterion4 = Restrictions.eq("status","A");
			Order order = Order.asc("districtApprovalGroupsId");
			
			Criterion criterion2And3 = Restrictions.and(criterion2, criterion3);
			Criterion criterion = Restrictions.and(criterion1, criterion2And3);
			districtApprovalGroupsList = findByCriteria(order, criterion, criterion4);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return districtApprovalGroupsList;
	}
	
	@Transactional(readOnly=true)
	public List<DistrictApprovalGroups> findDistrictApprovalGroupsByDistrictIdAndJobIdIsNull(DistrictMaster districtMaster)
	{
		List <DistrictApprovalGroups> districtApprovalGroupsList=null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.isNotNull("jobCategoryId");
			Criterion criterion3 = Restrictions.isNull("jobId");
			Criterion criterion4 = Restrictions.eq("status","A");
			Order order = Order.asc("districtApprovalGroupsId");
			
			Criterion criterion2And3 = Restrictions.and(criterion2, criterion3);
			Criterion criterion = Restrictions.and(criterion1, criterion2And3);
			districtApprovalGroupsList = findByCriteria(order, criterion, criterion4);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return districtApprovalGroupsList;
	}
	
	@Transactional(readOnly=true)
	public List<DistrictApprovalGroups> findAllDistrictApprovalGroupsByDistrict(DistrictMaster districtMaster)
	{
		List <DistrictApprovalGroups> districtApprovalGroupsList=null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.isNull("jobCategoryId");//Restrictions.eq("jobCategoryId",districtMaster);
			Criterion criterion3 = Restrictions.isNull("jobId");
			Criterion criterion4 = Restrictions.eq("status","A");
			Order order = Order.asc("districtApprovalGroupsId");
			
			Criterion criterion2And3 = Restrictions.and(criterion2, criterion3);
			Criterion criterion = Restrictions.and(criterion1, criterion2And3);
			districtApprovalGroupsList = findByCriteria(order, criterion, criterion4);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return districtApprovalGroupsList;
	}
	
	@Transactional(readOnly=true)
	public List<DistrictApprovalGroups> findAllDistrictApprovalGroupsByJobCategory(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster)
	{
		List <DistrictApprovalGroups> districtApprovalGroupsList=null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.eq("jobCategoryId",jobCategoryMaster);
			Criterion criterion3 = Restrictions.isNull("jobId");
			Order order = Order.asc("districtApprovalGroupsId");
			
			Criterion criterion2And3 = Restrictions.and(criterion2, criterion3);
			Criterion criterion = Restrictions.and(criterion1, criterion2And3);
			districtApprovalGroupsList = findByCriteria(order, criterion);
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return districtApprovalGroupsList;
	}

	
	@Transactional(readOnly=true)
	public boolean isGroupExistByDistrict(Integer districtId, String groupName)
	{
		boolean isGroupExist=false;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId.districtId",districtId);
			Criterion criterion2 = Restrictions.isNull("jobCategoryId");//Restrictions.eq("jobCategoryId",districtMaster);
			Criterion criterion3 = Restrictions.isNull("jobId");
			Criterion criterion4 = Restrictions.eq("groupName",groupName);
			
			Criterion criterion2And3 = Restrictions.and(criterion2, criterion3);
			List<DistrictApprovalGroups> districtApprovalGroupsList = findByCriteria(criterion1, criterion2And3, criterion4);
			
			if(districtApprovalGroupsList!=null && districtApprovalGroupsList.size()>0)
				isGroupExist=true;
			
			System.out.println("districtApprovalGroupsList:- "+districtApprovalGroupsList+", isGroupExist:- "+isGroupExist);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return isGroupExist;
	}
	
	/*public boolean addGroupByDistrict(DistrictMaster districtId, String groupName, Integer noOfApproval, String groupMembers)
	{
		boolean isGroupadded=false;
		try
		{
			DistrictApprovalGroups districtApprovalGroups = new DistrictApprovalGroups();
			districtApprovalGroups.setApprovalGroupCreatedDateTime(new Date());
			districtApprovalGroups.setGroupName(groupName);
			districtApprovalGroups.setDistrictId(districtId);
			districtApprovalGroups.setGroupMembers(groupMembers);
			districtApprovalGroups.setJobCategoryId(null);
			districtApprovalGroups.setJobId(null);
			districtApprovalGroups.setNoOfApprovals(noOfApproval);
			
			SessionFactory sessionFactory = getSessionFactory();
			StatelessSession statelesSsession 	= sessionFactory.openStatelessSession();
			Transaction txOpen =statelesSsession.beginTransaction();
			statelesSsession.insert(districtApprovalGroups);
			txOpen.commit();
	       	statelesSsession.close();
	       	
			isGroupadded=true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return isGroupadded;
	}*/
	
	@Transactional(readOnly=false)
	public boolean removeApprovalGroupByGroupId(Integer groupId)
	{
		try
		{
			String hql = "delete from DistrictApprovalGroups WHERE districtApprovalGroupsId= :groupId";
			Query query = null;
			Session session = getSession();
			
			query = session.createQuery(hql);
			query.setInteger("groupId", groupId);
			System.out.println("delete from districtapprovalgroups:- "+query.executeUpdate());
			
			return true;
		}
		catch (HibernateException e)
		{
			e.printStackTrace();
		}
		return false;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictApprovalGroups> findByGroupNameAndDistrictMaster(List<String> groupName, DistrictMaster districtMaster)
	{
		List<DistrictApprovalGroups> districtApprovalGroupsList = new ArrayList<DistrictApprovalGroups>();
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.isNull("jobCategoryId");//Restrictions.eq("jobCategoryId",districtMaster);
			Criterion criterion3 = Restrictions.isNull("jobId");
			Criterion criterion4 = Restrictions.in("groupName",groupName);
			//Criterion criterion5 = Restrictions.eq("status","A");
			
			districtApprovalGroupsList = findByCriteria(criterion1, criterion2, criterion3, criterion4);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return districtApprovalGroupsList;
	}
	
	/*@Transactional(readOnly=true)
	public List<DistrictApprovalGroups> findDistrictApprovalGroupsByJobCategory(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster)
	{
		List <DistrictApprovalGroups> districtApprovalGroupsList=null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.eq("jobCategoryId",jobCategoryMaster);
			Criterion criterion3 = Restrictions.isNull("jobId");
			
			Criterion criterion1And2 = Restrictions.and(criterion1, criterion2);
			Criterion criterion = Restrictions.and(criterion1And2, criterion3);
			districtApprovalGroupsList = findByCriteria(criterion);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return districtApprovalGroupsList;
	}
	
	@Transactional(readOnly=true)
	public List<DistrictApprovalGroups> findDistrictApprovalGroupsByJobId(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,JobOrder jobOrder)
	{
		List <DistrictApprovalGroups> districtApprovalGroupsList=null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.eq("jobCategoryId",jobCategoryMaster);
			Criterion criterion3 = Restrictions.eq("jobId",jobOrder);
			
			Criterion criterion1And2 = Restrictions.and(criterion1, criterion2);
			Criterion criterion = Restrictions.and(criterion1And2, criterion3);
			districtApprovalGroupsList = findByCriteria(criterion);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return districtApprovalGroupsList;
	}*/
	
	@Transactional(readOnly=true)
	public List<DistrictApprovalGroups> findGroupsByDistrictAndName(DistrictMaster districtMaster, String groupName)
	{
		List<DistrictApprovalGroups> districtApprovalGroupsList = new ArrayList<DistrictApprovalGroups>();
		
		try{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.isNull("jobCategoryId");
			Criterion criterion3 = Restrictions.isNull("jobId");
			Criterion criterion4 = Restrictions.eq("groupName",groupName);
			Criterion criterion2And3 = Restrictions.and(criterion2, criterion3);
			
			districtApprovalGroupsList = findByCriteria(criterion1, criterion2And3, criterion4);
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return districtApprovalGroupsList;
	}
	@Transactional(readOnly=true)
	public List<DistrictApprovalGroups> findGroupsByDistrictAndDistrictApprovalGroupsId(DistrictMaster districtMaster, List<Integer> districtApprovalGroupsId)
	{
		List<DistrictApprovalGroups> districtApprovalGroupsList = null;
		try{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.in("districtApprovalGroupsId",districtApprovalGroupsId);
			Criterion criterion3 = Restrictions.eq("status","A");	
			districtApprovalGroupsList = findByCriteria(criterion1, criterion2, criterion3);	
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return districtApprovalGroupsList;
		
	}
	
}