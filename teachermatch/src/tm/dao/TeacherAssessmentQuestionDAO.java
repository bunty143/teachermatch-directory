package tm.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.ObjectiveMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherAssessmentQuestionDAO extends GenericHibernateDAO<TeacherAssessmentQuestion, Long> 
{
	public TeacherAssessmentQuestionDAO() {
		super(TeacherAssessmentQuestion.class);
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentQuestion> findTAQbyDomainAndTeacher(DomainMaster domainMaster, TeacherDetail teacherDetail)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		List<TeacherAssessmentQuestion> lstAssessmentQuestions = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("domainMaster",domainMaster);
			Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 = Restrictions.between("createdDateTime", sDate, eDate);
			
			lstAssessmentQuestions = findByCriteria(criterion1,criterion2,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstAssessmentQuestions;
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment responses of assessments.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<String> findUniqueQuestionUId() 
	{
		Session session = getSession();
		List result = session.createCriteria(getPersistentClass())
		.add(Restrictions.not(Restrictions.isNull("domainMaster")))
		.addOrder(Order.asc("teacherAssessmentQuestionId"))       
		.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("questionUId"))
		).list();
		
		return result;
	}
	
	@Transactional(readOnly=true)
    @SuppressWarnings("unchecked")
    public List<DomainMaster> findTeacherDomains(TeacherAssessmentdetail teacherAssessmentdetail) 
    {
        
        List<DomainMaster> domainMasters = new ArrayList<DomainMaster>();
        try {
            
            Criterion criterion1 = Restrictions.eq("teacherAssessmentdetail",teacherAssessmentdetail);
            Criterion criterion2 = Restrictions.eq("teacherDetail",teacherAssessmentdetail.getTeacherDetail());
            
            Session session = getSession();
            domainMasters = session.createCriteria(getPersistentClass())
            .add(criterion1)
            .add(criterion2)
            .setProjection(Projections.groupProperty("domainMaster")
            ).list();
            return domainMasters;
 
        } catch (Exception e) {
            e.printStackTrace();
        }
        return domainMasters;
    }
    
    @Transactional(readOnly=true)
    @SuppressWarnings("unchecked")
    public List<CompetencyMaster> findTeacherCompetency(TeacherAssessmentdetail teacherAssessmentdetail) 
    {
        
        List<CompetencyMaster> competencyMasters = new ArrayList<CompetencyMaster>();
        try {
            
            Criterion criterion1 = Restrictions.eq("teacherAssessmentdetail",teacherAssessmentdetail);
            Criterion criterion2 = Restrictions.eq("teacherDetail",teacherAssessmentdetail.getTeacherDetail());
            
            Session session = getSession();
            competencyMasters = session.createCriteria(getPersistentClass())
            .add(criterion1)
            .add(criterion2)
            .setProjection(Projections.groupProperty("competencyMaster")
            ).list();
            return competencyMasters;
 
        } catch (Exception e) {
            e.printStackTrace();
        }
        return competencyMasters;
    }

    @Transactional(readOnly=true)
    @SuppressWarnings("unchecked")
    public List<ObjectiveMaster> findTeacherObjective(TeacherAssessmentdetail teacherAssessmentdetail) 
    {
        List<ObjectiveMaster> objectiveMasterList = new ArrayList<ObjectiveMaster>();
        try {
            Criterion criterion1 = Restrictions.eq("teacherAssessmentdetail",teacherAssessmentdetail);
            Criterion criterion2 = Restrictions.eq("teacherDetail",teacherAssessmentdetail.getTeacherDetail());
            
            Session session = getSession();
            objectiveMasterList = session.createCriteria(getPersistentClass())
            .add(criterion1)
            .add(criterion2)
            .setProjection(Projections.groupProperty("objectiveMaster")
            ).list();
            return objectiveMasterList;
 
        } catch (Exception e) {
            e.printStackTrace();
        }
        return objectiveMasterList;
    }
    
	@Transactional(readOnly=true)
    public Map<Integer, String> getTAQDates(String teacherAssessmentIds, int teacherId)
    {
    	Map<Integer, String> dateMap = new HashMap<Integer, String>();
    	try{
    		List<Object[]> objectList = null;
    		Query query = null;
    		if(!teacherAssessmentIds.equals("")){
    			Session session = getSession();
    			String hql = "SELECT teacherAssessmentId, min(createdDateTimeOld), min(createdDateTime) FROM teacherassessmentquestions "
    						+"WHERE teacherAssessmentId in ("+teacherAssessmentIds+") and teacherId = "+teacherId+" group by teacherAssessmentId";
    			System.out.println("hql : "+hql);
    			query = session.createSQLQuery(hql);
    			long a = System.currentTimeMillis();
    			objectList = query.list();
				long b = System.currentTimeMillis();
				System.out.println("Execution time : " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec, Number of records fetch : " + objectList.size());
    			if(objectList!=null && objectList.size()>0){
    				for(Object[] object : objectList){
    					dateMap.put(Integer.parseInt(object[0].toString()), object[1]==null?(object[2]==null?null:object[2].toString()):object[1].toString());
    				}
    			}
    		}
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    	return dateMap;
    }
}
