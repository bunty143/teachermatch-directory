package tm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EventSchedule;
import tm.bean.JobOrder;
import tm.bean.SlotSelection;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;

public class SlotSelectionDAO extends GenericHibernateDAO<SlotSelection,Integer>{

	public SlotSelectionDAO() {
		super(SlotSelection.class);
		}

	@Transactional(readOnly=false)
	public List<SlotSelection> findByEventScheduleList(List<EventSchedule> eventSchedule)
	{
		List<SlotSelection> listjobOrder=new ArrayList<SlotSelection>();
		try 
		{
			Criterion criterion =	Restrictions.in("eventSchedule",eventSchedule);
			listjobOrder = findByCriteria(criterion);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return listjobOrder;
	}
	
	
	@Transactional(readOnly=false)
	public List<SlotSelection> findByEventScheduleId(EventSchedule eventSchedule)
	{
		List<SlotSelection> listjobOrder=new ArrayList<SlotSelection>();
		try 
		{
			Criterion criterion =	Restrictions.eq("eventSchedule",eventSchedule);
			listjobOrder = findByCriteria(criterion);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return listjobOrder;
	}
	
	@Transactional(readOnly=false)
	public List<SlotSelection> findByEventScheduleList(EventSchedule eventSchedule,int start,int end,Order sortOrderStrVal)
	{
		List<SlotSelection> listjobOrder=new ArrayList<SlotSelection>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion =	Restrictions.eq("eventSchedule",eventSchedule);
			criteria.add(criterion);
			Criteria c1=criteria.createCriteria("teacherDetail");
			c1.addOrder(sortOrderStrVal);
			
			criteria.setFirstResult(start);
			criteria.setMaxResults(end);
			listjobOrder = criteria.list();	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return listjobOrder;
	}
	
	@Transactional(readOnly=false)
	public int countByEventScheduleList(EventSchedule eventSchedule,int start,int end,Order sortOrderStrVal)
	{
		int count=0;
		List<SlotSelection> listjobOrder=new ArrayList<SlotSelection>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion =	Restrictions.eq("eventSchedule",eventSchedule);
			criteria.add(criterion);
			criteria.createCriteria("teacherId").addOrder(sortOrderStrVal);
			
			listjobOrder = findByCriteria(criterion);
			count=listjobOrder.size();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return count;
	}
	
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> schedulewiseCandidateCount(List<EventSchedule> eventSchedules) 
	{
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		Session session = getSession();
		
		try{
			 String sql =" SELECT ss.eventScheduleId, COUNT(*) AS totalApp " +
					" from slotselection ss where ss.eventScheduleId IN (:eventSchedules)  group by ss.eventScheduleId";
				
				Query query = session.createSQLQuery(sql);
				query.setParameterList("eventSchedules", eventSchedules);
				List<Object[]> rows = query.list();
				  if(rows.size()>0){
						for(Object oo: rows){
							Object obj[] = (Object[])oo;
							map.put((Integer)obj[0], Integer.parseInt(obj[1].toString()));
						}
				  }
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	 return map;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<SlotSelection> fidSlotByCandidateAndEvent(EventSchedule eventSchedule,TeacherDetail teacherDetail) 
	{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		List<SlotSelection> listSlotSelections = new ArrayList<SlotSelection>();
		
		try{
			if(teacherDetail!=null){
				Criterion criterion1 =	Restrictions.eq("teacherDetail",teacherDetail);
				criteria.add(criterion1);
				criteria.createCriteria("eventSchedule").add(Restrictions.eq("eventDetails",eventSchedule.getEventDetails()));
				listSlotSelections=criteria.list();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	 return listSlotSelections;
	}
}
