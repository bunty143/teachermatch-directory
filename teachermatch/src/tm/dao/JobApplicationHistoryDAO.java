package tm.dao;

import tm.bean.JobApplicationHistory;
import tm.dao.generic.GenericHibernateDAO;


public class JobApplicationHistoryDAO extends GenericHibernateDAO<JobApplicationHistory, Integer> 
{
	public JobApplicationHistoryDAO() {
		super(JobApplicationHistory.class);
	}
	
}
