package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherRole;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherRoleDAO extends GenericHibernateDAO<TeacherRole, Integer> 
{
	public TeacherRoleDAO() 
	{
		super(TeacherRole.class);
	}
	
	@Transactional(readOnly=false)
	public List<TeacherRole> findTeacherRoleByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherRole> lstTeacherRoles =new ArrayList<TeacherRole>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstTeacherRoles = findByCriteria(Order.desc("roleId"),criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherRoles;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherRole> findSortedTeacherRoleByTeacher(Order  sortOrderStrVal,TeacherDetail teacherDetail)
	{
		List<TeacherRole> lstTeacherRoles = new ArrayList<TeacherRole>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstTeacherRoles = findByCriteria(sortOrderStrVal,criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherRoles;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherRole> findByTeacherDetailsObj(List<TeacherDetail> teacherDetailsObj)
	{
		List<TeacherRole> lstTeacherRole = new ArrayList<TeacherRole>();
		
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailsObj);
			lstTeacherRole = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherRole==null || lstTeacherRole.size()==0)
			return null;
		else
			return lstTeacherRole;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> getTeacherRoleCountByTeacherJAF(TeacherDetail teacherDetail)
	{
		List<Integer> lstTeacherRole= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			
			criteria.setProjection(Projections.projectionList().add(Projections.count("teacherDetail"))) ;
			lstTeacherRole =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstTeacherRole;
		}
		
	}
}
