package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EventDetails;
import tm.bean.EventFacilitatorsList;
import tm.dao.generic.GenericHibernateDAO;

public class EventFacilitatorsListDAO extends GenericHibernateDAO<EventFacilitatorsList, Integer>{

	public EventFacilitatorsListDAO() {
		super(EventFacilitatorsList.class);
		}
	
	@Transactional(readOnly=false)
	public List<EventFacilitatorsList> findEventFacilitatorByEmail(String userEmail,EventDetails eventDetails){
		List<EventFacilitatorsList> EventFacilitatorsLists =  new ArrayList<EventFacilitatorsList>();
		
		try{
			
			Criterion criterion1 = Restrictions.eq("facilitatorEmailAddress",userEmail);
			Criterion criterion2 = Restrictions.eq("eventDetails",eventDetails);
			EventFacilitatorsLists = findByCriteria(criterion1,criterion2);
		}catch(Exception e){
			e.printStackTrace();
		}			
			return EventFacilitatorsLists;	
	}
	@Transactional(readOnly=false)
	public List<EventFacilitatorsList> findEventFacilitatorsByEvent(EventDetails eventDetails){
		List<EventFacilitatorsList> EventFacilitatorsLists =  new ArrayList<EventFacilitatorsList>();
		
		try{
			
			Criterion criterion1 = Restrictions.eq("eventDetails",eventDetails);
			EventFacilitatorsLists = findByCriteria(criterion1);
		}catch(Exception e){
			e.printStackTrace();
		}			
			return EventFacilitatorsLists;	
	}
}
