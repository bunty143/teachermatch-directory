package tm.dao;

import tm.bean.CmsEventTypeMaster;

import tm.dao.generic.GenericHibernateDAO;

public class CmsEventTypeMasterDao extends GenericHibernateDAO<CmsEventTypeMaster, Integer>{

	public CmsEventTypeMasterDao()
	{
		super(CmsEventTypeMaster.class);
	}

}
