package tm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EpiTakenList;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.dao.generic.GenericHibernateDAO;

public class EpiTakenListDAO extends GenericHibernateDAO<EpiTakenList, Integer> 
{
	public EpiTakenListDAO() {
		super(EpiTakenList.class);
	}
	
	@Transactional(readOnly=true)
	public Map<Boolean,EpiTakenList> getEPITakenMap()
	{
		Map<Boolean,EpiTakenList> epiTakenMap = new HashMap<Boolean, EpiTakenList>();
		List<EpiTakenList> epiTakenLists = findAll();
		try {
			for (EpiTakenList epiTakenList : epiTakenLists) {
				if(epiTakenList.getIsResearchEPI())
					epiTakenMap.put(true, epiTakenList);
				else
					epiTakenMap.put(false, epiTakenList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return epiTakenMap;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<EpiTakenList> getEpiTakenList(AssessmentGroupDetails assessmentGroupDetails)
	{
		List<EpiTakenList> epiTakenList = new ArrayList<EpiTakenList>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("assessmentGroupDetails", assessmentGroupDetails));
			
			epiTakenList = criteria.list();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return epiTakenList;
	}
}
