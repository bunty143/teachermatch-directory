package tm.dao;

import tm.bean.CmsEventFormatMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CmsEventFormatMasterDAO extends GenericHibernateDAO<CmsEventFormatMaster, Integer>{

	public CmsEventFormatMasterDAO() {
		super(CmsEventFormatMaster.class);
		
	}

}
