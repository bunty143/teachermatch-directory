package tm.dao;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictWiseCandidateName;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictWiseCandidateNameDAO extends GenericHibernateDAO<DistrictWiseCandidateName,Integer> 
{
	public DistrictWiseCandidateNameDAO() {
		super(DistrictWiseCandidateName.class);
	}
	
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(
			TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	
	@Transactional(readOnly=false)
	public DistrictWiseCandidateName getDistrictWiseCandidateName(DistrictMaster districtId, TeacherDetail teacherId)
	{
		DistrictWiseCandidateName obj=null;
		Criterion c1=Restrictions.eq("districtId", districtId);
		Criterion c2=Restrictions.eq("teacherId", teacherId);
		List<DistrictWiseCandidateName> districtWiseCandidateNameList=findByCriteria(c1,c2);
		if(districtWiseCandidateNameList!=null && districtWiseCandidateNameList.size()>0)
		{
			obj=districtWiseCandidateNameList.get(0); 
		}
		return obj;
	}
	
	@Transactional(readOnly=false)
	public DistrictWiseCandidateName getDistrictWiseCandidateNameByDistrict(DistrictMaster districtId, TeacherDetail teacherId)
	{
		DistrictWiseCandidateName obj=null;
		Integer employeeType = 0;
		
		Criterion c1=Restrictions.eq("districtId", districtId);
		Criterion c2=Restrictions.eq("teacherId", teacherId);
		
		TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherId.getTeacherId(), false, false);
		
		if(teacherPersonalInfo!=null)
		{
			employeeType = teacherPersonalInfo.getEmployeeType();
		}
		
		
		Criterion c3=Restrictions.eq("employeeType", employeeType);
		
		List<DistrictWiseCandidateName> districtWiseCandidateNameList=findByCriteria(c1,c2,c3);
		if(districtWiseCandidateNameList!=null && districtWiseCandidateNameList.size()>0)
		{
			obj=districtWiseCandidateNameList.get(0); 
		}
		return obj;
	}
	
	@Transactional(readOnly=false)
	public DistrictWiseCandidateName getTeacherPersoinalInfo(TeacherDetail teacherId)
	{
		DistrictWiseCandidateName obj=null;
		Criterion c2=Restrictions.eq("teacherId", teacherId);
		List<DistrictWiseCandidateName> districtWiseCandidateNameList=findByCriteria(c2);
		if(districtWiseCandidateNameList!=null && districtWiseCandidateNameList.size()>0)
		{
			obj=districtWiseCandidateNameList.get(0); 
		}
		return obj;
	}
	
	/************* TPL-3832  Enhance Transcript Verification Feature Starts*************************************/
	@Transactional(readOnly=false)
	public List<DistrictWiseCandidateName> getDistrictWiseCandidateNameList(DistrictMaster districtId, TeacherDetail teacherId)
	{
		List<DistrictWiseCandidateName> districtWiseCandidateNameList=null;
		Criterion c1=Restrictions.eq("districtId", districtId);
		Criterion c2=Restrictions.eq("teacherId", teacherId);
		Criterion c3=Restrictions.eq("status", "A");
		districtWiseCandidateNameList = findByCriteria(Order.asc("updatedDateTime"),c1,c2,c3);
		return districtWiseCandidateNameList;
	}
	/************* TPL-3832  Enhance Transcript Verification Feature Ends*************************************/	
}
