package tm.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.quartz.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.sun.mail.imap.protocol.Status;

import tm.bean.EligibilityProficiencyMaster;
import tm.bean.EligibilityStatusMaster;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.user.UserMasterDAO;

public class EligibilityProficiencyMasterDAO extends GenericHibernateDAO<EligibilityProficiencyMaster, Long> 
{
	public EligibilityProficiencyMasterDAO() 
	{
		super(EligibilityProficiencyMaster.class);
	}
	
}
