package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictTemplatesforMessages;
import tm.bean.EmailMessageTemplatesForFacilitators;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class EmailMessageTemplatesForFacilitatorsDAO extends GenericHibernateDAO<EmailMessageTemplatesForFacilitators,Integer>
{
	public EmailMessageTemplatesForFacilitatorsDAO(){
		super(EmailMessageTemplatesForFacilitators.class);
	}

	@Transactional(readOnly=false)
	public List<EmailMessageTemplatesForFacilitators> findByDistrict(Order sortOrderStrVal,DistrictMaster districtMaster)
	{
		List<EmailMessageTemplatesForFacilitators> emailMessageTemplatesForFacilitators = new ArrayList<EmailMessageTemplatesForFacilitators>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion c1 = Restrictions.eq("distritMaster",districtMaster);
			criteria.addOrder(sortOrderStrVal);
			criteria.add(c1);
			emailMessageTemplatesForFacilitators = criteria.list();
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return emailMessageTemplatesForFacilitators;
	}
	
}
