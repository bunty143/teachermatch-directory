package tm.dao.assessment;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class AssessmentGroupDetailsDAO  extends GenericHibernateDAO<AssessmentGroupDetails, Integer>  {
	public AssessmentGroupDetailsDAO() {
		super(AssessmentGroupDetails.class);
	}
	
	/* @Author: Ashish Chaudhary
	 * @Discription: Returns list of group.
	 */
	@Transactional(readOnly=true)
	public List<AssessmentGroupDetails> getAllAssessmentGroupDetails() {
		List<AssessmentGroupDetails> assessmentGroupDetails = null;
		try {			
			assessmentGroupDetails = getSession().createCriteria(getPersistentClass()).list();			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return assessmentGroupDetails;
	}
	
	/* @Author: Ashish Chaudhary
	 * @Discription: Returns list of group.
	 */
	@Transactional(readOnly=false)
	public AssessmentGroupDetails saveAssessmentGroupDetails(AssessmentDetail assessmentDetail, UserMaster user, String ipAddress) {
		AssessmentGroupDetails agd = null;
		try {
			agd = new AssessmentGroupDetails();
			agd.setAssessmentGroupName(assessmentDetail.getAssessmentGroupDetails().getAssessmentGroupName());
			
			System.out.println("Group name is : " + assessmentDetail.getAssessmentGroupDetails().getAssessmentGroupName());
			
			agd.setAssessmentType(assessmentDetail.getAssessmentType());
			agd.setStatus("A");
			agd.setUserMaster(user);
			agd.setCreatedDateTime(new Date());
			agd.setIpaddress(ipAddress);
			
			int id = (Integer) getSession().save(agd);
			
			agd.setAssessmentGroupId(id);
		}catch (Exception e) {
               e.printStackTrace();
		}
		return agd;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param assessmentType 3 or 4
	 * @param from 0 for all and 1 for status A
	 * @return assessmentGroupDetailsList
	 */
	@Transactional(readOnly=false)
	public List<AssessmentGroupDetails> getAllAssessmentGroupDetailsList(int assessmentType, int from)
	{
		List<AssessmentGroupDetails> assessmentGroupDetailsList = new ArrayList<AssessmentGroupDetails>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("assessmentType", assessmentType));
			if(from==1)
				criteria.add(Restrictions.eq("status", "A"));
			
			assessmentGroupDetailsList = criteria.list();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return assessmentGroupDetailsList;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param assessmentType
	 * @param assessmentGroupName
	 * @return assessmentGroupDetailsList
	 */
	@Transactional(readOnly=false)
	public List<AssessmentGroupDetails> getAssessmentGroupDetailsListByTypeAndName(Integer assessmentType, String assessmentGroupName)
	{
		List<AssessmentGroupDetails> assessmentGroupDetailsList = new ArrayList<AssessmentGroupDetails>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("assessmentType", assessmentType));
			criteria.add(Restrictions.eq("assessmentGroupName", assessmentGroupName));
			
			assessmentGroupDetailsList = criteria.list();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return assessmentGroupDetailsList;
	}
	
	@Transactional(readOnly=false)
	public List<AssessmentGroupDetails> getAssessmentGroupDetailsListByType(Integer assessmentType)
	{
		List<AssessmentGroupDetails> assessmentGroupDetailsList = new ArrayList<AssessmentGroupDetails>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("assessmentType", assessmentType))
			.add(Restrictions.eq("status", "A"))
			.addOrder(Order.asc("assessmentGroupId"));
			criteria.setFirstResult(0);
			criteria.setMaxResults(1);
			assessmentGroupDetailsList = criteria.list();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return assessmentGroupDetailsList;
	}
	
	@Transactional(readOnly=false)
	public void updateAGDs(int assessmentType, boolean isAll, Integer assessmentGroupId)
	{
		try {
			String hql = "";
			if(isAll && assessmentGroupId==null)
				hql = "update AssessmentGroupDetails set isDefault = NULL where assessmentType="+assessmentType;
			else if(!isAll && assessmentGroupId!=null && assessmentGroupId>0)
				hql = "update AssessmentGroupDetails set isDefault = 1 where assessmentType="+assessmentType+" and assessmentGroupId="+assessmentGroupId;
			
			Query query = null;
			Session session = getSession();
			query = session.createQuery(hql);
			System.out.println("updateAGDs : "+query.executeUpdate());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}
}
