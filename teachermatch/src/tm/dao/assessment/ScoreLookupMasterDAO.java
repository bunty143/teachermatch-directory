package tm.dao.assessment;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.ScoreLookupMaster;
import tm.dao.generic.GenericHibernateDAO;

public class ScoreLookupMasterDAO extends GenericHibernateDAO<ScoreLookupMaster, Integer> 
{
	public ScoreLookupMasterDAO() {
		super(ScoreLookupMaster.class);
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param shortName "EPI" or "SP" or "IPI"
	 * @return scoreLookupMasterList
	 */
	@Transactional(readOnly=false)
	public List<ScoreLookupMaster> getScoringMethods(String shortName)
	{
		List<ScoreLookupMaster> scoreLookupMasterList = new ArrayList<ScoreLookupMaster>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("shortName", shortName));
			criteria.add(Restrictions.eq("status", "A"));
			
			scoreLookupMasterList = criteria.list();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return scoreLookupMasterList;
	}
}
