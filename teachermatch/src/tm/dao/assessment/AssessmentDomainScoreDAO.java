package tm.dao.assessment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentDomainScore;
import tm.bean.assessment.ScoreLookupMaster;
import tm.bean.cgreport.RawDataForDomain;
import tm.bean.master.DomainMaster;
import tm.dao.generic.GenericHibernateDAO;

public class AssessmentDomainScoreDAO extends GenericHibernateDAO<AssessmentDomainScore, Integer> 
{
	public AssessmentDomainScoreDAO() {
		super(AssessmentDomainScore.class);
	}
	
	/*SELECT ROUND( SUM(  `normscore` ) / COUNT( * ) ) AS norm, SUM(  `normscore` ) 
	FROM  `assessmentdomainscore` 
	WHERE teacherId =1 and `lookupId`=1
	
	SELECT ROUND( SUM(  `ncevalue` )) AS norm, SUM(  `ncevalue` ) 
	FROM  `assessmentdomainscore` 
	WHERE teacherId =1 and `lookupId`=1
	
	*/
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List calculateCandidatesNormScore(List<TeacherDetail> teacherDetails,TeacherAssessmentdetail teacherAssessmentdetail) 
	{
		Session session = getSession();
		String sql = "";
		sql = "SELECT ROUND(SUM(normscore*dm.multiplier)) ,teacherId FROM  assessmentdomainscore ads join domainmaster dm on dm.domainId=ads.domainId WHERE teacherId IN (:teacherDetails) and lookupId="+teacherAssessmentdetail.getScoreLookupMaster().getLookupId()+" and assessmentId="+teacherAssessmentdetail.getAssessmentDetail().getAssessmentId()+" and assessmentType="+teacherAssessmentdetail.getAssessmentType()+" and assessmentTakenCount="+teacherAssessmentdetail.getAssessmentTakenCount()+" group by teacherId";
		
		System.out.println("ssssssss: "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			query.setParameterList("teacherDetails", teacherDetails );
			List<Object[]> rows = query.list();
			
			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	public List<AssessmentDomainScore> findAllDomainByTeacher(TeacherDetail teacherDetail,ScoreLookupMaster scoreLookupMaster,List<DomainMaster> domainMasters)
	{
		List<AssessmentDomainScore> assessmentDomainScoreList = new ArrayList<AssessmentDomainScore>();
		try {
			
			Criterion criterion1 = Restrictions.eq("scoreLookupMaster",scoreLookupMaster);
			Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
			if(domainMasters!=null && domainMasters.size()>0)
			{
				Criterion criterion3 = Restrictions.in("domainMaster",domainMasters);
				assessmentDomainScoreList = findByCriteria(criterion1,criterion2,criterion3);
			}else
				assessmentDomainScoreList = findByCriteria(criterion1,criterion2);

		} catch (Exception e) {
			e.printStackTrace();
		}		
		return assessmentDomainScoreList;
	} 
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List calculateCandidatesNormScore(Integer lookupId) 
	{
		Session session = getSession();
		String sql = "";
		sql = "SELECT ROUND(SUM(normscore*dm.multiplier)) ,teacherId FROM  assessmentdomainscore ads join domainmaster dm on dm.domainId=ads.domainId WHERE lookupId="+lookupId+" group by teacherId";
		
		System.out.println("ssssssss: "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			
			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<String,String> findAssessmentDomainScoreByTeacherList(List<TeacherDetail> teacherDetails)
	{
		List<AssessmentDomainScore> assessmentDomainScoreList = null;
		Map<String,String> mapAssessmentDomainScore =  new HashMap<String, String>(); 
		try 
		{
			Criteria criteria = getSession().createCriteria(getPersistentClass());
			if(teacherDetails!=null && teacherDetails.size()>0)
			{
				Criterion criterion = Restrictions.in("teacherDetail",teacherDetails);
				criteria.add(criterion);
				assessmentDomainScoreList=criteria.list();
			}
			if(assessmentDomainScoreList!=null && assessmentDomainScoreList.size()>0){
				for (AssessmentDomainScore domainScore : assessmentDomainScoreList) {
					mapAssessmentDomainScore.put(domainScore.getTeacherDetail().getTeacherId()+"#"+domainScore.getDomainMaster().getDomainId(),domainScore.getNormscore().intValue()+"");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return mapAssessmentDomainScore;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param teacherDetailList
	 * @param assessmentDetail
	 * @param assessmentType
	 * @param assessmentTakenCount
	 * @return assessmentDomainScoreList
	 */
	@Transactional(readOnly=true)
	public List<AssessmentDomainScore> getAssessmentDomainScore(List<TeacherDetail> teacherDetailList,AssessmentDetail assessmentDetail,Integer assessmentType,Integer assessmentTakenCount)
	{
		List<AssessmentDomainScore> assessmentDomainScoreList = null;
		try {
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetailList);
			Criterion criterion2 = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion3 = Restrictions.eq("assessmentType", assessmentType);
			Criterion criterion4 = Restrictions.eq("assessmentTakenCount", assessmentTakenCount);
			
			assessmentDomainScoreList = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assessmentDomainScoreList;
	}
	
	@Transactional(readOnly=false)
	public List<AssessmentDomainScore> findByDomainAndTeacherList(DomainMaster domainMaster, List<TeacherDetail> teacherDetailList)
	{
		List<AssessmentDomainScore> assessmentDomainScoreList = new ArrayList<AssessmentDomainScore>();
		try{		
			if(teacherDetailList!=null && teacherDetailList.size()>0){
				
				Criterion criterion1 = Restrictions.eq("domainMaster", domainMaster);
				Criterion criterion2 = Restrictions.in("teacherDetail", teacherDetailList);
				assessmentDomainScoreList = findByCriteria(criterion1,criterion2);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return assessmentDomainScoreList;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List calculateCandidatesNormScore(List<TeacherDetail> teacherDetails) 
	{
		Session session = getSession();
		String sql = "";
		sql = "SELECT ROUND(SUM(normscore*dm.multiplier)) ,teacherId FROM  assessmentdomainscore ads join domainmaster dm on dm.domainId=ads.domainId WHERE teacherId IN (:teacherDetails) and lookupId=1 and assessmentId=110 and assessmentType=1 group by teacherId";
		
		System.out.println("ssssssss: "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			query.setParameterList("teacherDetails", teacherDetails );
			List<Object[]> rows = query.list();
			
			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	public List<AssessmentDomainScore> findByDomainTeacherAndTAD(DomainMaster domainMaster, List<TeacherDetail> teacherDetailList, List<RawDataForDomain> rawDataForDomainList)
	{
		List<AssessmentDomainScore> assessmentDomainScoreList = new ArrayList<AssessmentDomainScore>();
		try{		
			if(teacherDetailList!=null && teacherDetailList.size()>0){
				
				Criterion criterion1 = Restrictions.eq("domainMaster", domainMaster);
				Criterion criterion2 = Restrictions.in("teacherDetail", teacherDetailList);
				Criterion criterion3 = Restrictions.in("rawDataForDomain", rawDataForDomainList);
				assessmentDomainScoreList = findByCriteria(criterion1,criterion2,criterion3);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return assessmentDomainScoreList;
	}
	@Transactional(readOnly=false)
	public List<AssessmentDomainScore> findByTeacher(TeacherDetail teacherDetail)
	{
		List<AssessmentDomainScore> assessmentDomainScoreList = new ArrayList<AssessmentDomainScore>();
		try{					
				Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				assessmentDomainScoreList = findByCriteria(criterion1);		
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return assessmentDomainScoreList;
	}
	@Transactional(readOnly=false)
	public List<AssessmentDomainScore> findByDomainAndTeacherListForSp(List<TeacherDetail> teacherDetailList)
	{
		List<AssessmentDomainScore> assessmentDomainScoreList = new ArrayList<AssessmentDomainScore>();
		try{		
			if(teacherDetailList!=null && teacherDetailList.size()>0){
				
				Criterion criterion1 = Restrictions.eq("assessmentType", 3);
				Criterion criterion2 = Restrictions.in("teacherDetail", teacherDetailList);
				assessmentDomainScoreList = findByCriteria(criterion1,criterion2);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return assessmentDomainScoreList;
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<String,String> findAssessmentDomainScoreByTeacherListCGOp(List<TeacherDetail> teacherDetails)
	{
		List<AssessmentDomainScore> assessmentDomainScoreList = null;
		Map<String,String> mapAssessmentDomainScore =  new HashMap<String, String>(); 
		try 
		{
			Criteria criteria = getSession().createCriteria(getPersistentClass());
			List<String []> assessmentDomainSco=new ArrayList<String []>();
			if(teacherDetails!=null && teacherDetails.size()>0)
			{
				Criterion criterion = Restrictions.in("teacherDetail",teacherDetails);
				criteria.add(criterion);
				criteria.createAlias("teacherDetail", "td").createAlias("domainMaster", "dmn")
				.setProjection(Projections.projectionList()
						.add(Projections.property("td.teacherId"))
						.add(Projections.property("dmn.domainId"))
						.add(Projections.property("normscore"))
						);
				
				assessmentDomainSco=criteria.list();
			}
			if(assessmentDomainSco!=null && assessmentDomainSco.size()>0){
				
				for (Iterator it = assessmentDomainSco.iterator(); it.hasNext();)
				{
    				//System.out.println("call");
    				JobForTeacher jobForTeacher = new JobForTeacher();
    				Object[] row = (Object[]) it.next();
    				String genKey =row[0].toString()+"#"+row[1].toString();
    				String valueKey="";
    				if(row[2]!=null){
    					valueKey=row[2].toString();
    				}
    				mapAssessmentDomainScore.put(genKey,valueKey);
				}
				/*for (AssessmentDomainScore domainScore : assessmentDomainScoreList) {
					mapAssessmentDomainScore.put(domainScore.getTeacherDetail().getTeacherId()+"#"+domainScore.getDomainMaster().getDomainId(),domainScore.getNormscore().intValue()+"");
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return mapAssessmentDomainScore;
	}
}
