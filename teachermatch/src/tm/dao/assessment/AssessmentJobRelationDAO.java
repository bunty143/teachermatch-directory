package tm.dao.assessment;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;
/* @Author: Vishwanath Kumar
 * @Discription: AssessmentJobRelation DAO.
 */
public class AssessmentJobRelationDAO extends GenericHibernateDAO<AssessmentJobRelation, Integer>
{

	public AssessmentJobRelationDAO() {
		super(AssessmentJobRelation.class);
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is use to delete Assessment Job Relations when assessmentId is available.
	 */
	@Transactional(readOnly=false)
	public boolean deleteAssessmentJobRelations(AssessmentDetail assessmentDetail)
	{

		List<AssessmentJobRelation> assessmentJobRelations = assessmentDetail.getAssessmentJobRelations();
		if(assessmentJobRelations!=null)
		{
			for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations) 
			{
				makeTransient(assessmentJobRelation);
			}
		}
		return false;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get findUniqueJobOrders.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List findUniqueJobOrders() 
	{
		Session session = getSession();

		List result = session.createCriteria(getPersistentClass()).list();
		/*List result = session.createCriteria(getPersistentClass())       
		.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("jobId"))
				.add(Projections.property("jobId"))  
				.add(Projections.property("assessmentJobRelationId")) 
				.add(Projections.property("assessmentId"))
		).list();*/

		return result;

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get findRelationByJobOrder.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<AssessmentJobRelation> findRelationByJobOrder(JobOrder jobOrder) 
	{
		List<AssessmentJobRelation> assessmentJobRelations = null;
		try {
			Date dateWithoutTime = Utility.getDateWithoutTime();
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("status", "A");
			Criterion criterion1 = Restrictions.eq("jobId", jobOrder);

			criteria.add(criterion);
			criteria.add(criterion1);
			
			Criteria ctr=criteria.createCriteria("jobId");
			ctr.add(Restrictions.eq("isJobAssessment",true));
			ctr.add(Restrictions.eq("status","a")); 
			//ctr.add(Restrictions.eq("jobStatus","o"));
			ctr.add(Restrictions.le("jobStartDate",dateWithoutTime));
			ctr.add(Restrictions.ge("jobEndDate",dateWithoutTime));
			
			assessmentJobRelations = criteria.list();
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentJobRelations;

	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get findRelationByJobOrder.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<AssessmentJobRelation> findRelationByAssessment(AssessmentDetail assessmentDetail) 
	{
		List<AssessmentJobRelation> assessmentJobRelations = null;
		try {
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("status", "A");
			Criterion criterion1 = Restrictions.eq("assessmentId", assessmentDetail);

			criteria.add(criterion);
			criteria.add(criterion1);
			
			Criteria ctr=criteria.createCriteria("jobId");
			ctr.add(Restrictions.eq("isJobAssessment",true));
			ctr.add(Restrictions.eq("status","a")); 
			//ctr.add(Restrictions.eq("jobStatus","o"));
			ctr.add(Restrictions.le("jobStartDate",dateWithoutTime));
			ctr.add(Restrictions.ge("jobEndDate",dateWithoutTime));
			
			assessmentJobRelations = criteria.list();
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentJobRelations;

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get findRelationByJobOrders.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<AssessmentJobRelation> findRelationByJobOrders(List<JobOrder> jobOrders) 
	{
		List<AssessmentJobRelation> assessmentJobRelations = null;
		try {
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			if(jobOrders.size()>0)
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion = Restrictions.eq("status", "A");
				Criterion criterion1 = Restrictions.in("jobId", jobOrders);
				criteria.add(criterion);
				criteria.add(criterion1);
				
				Criteria ctr=criteria.createCriteria("jobId");
				ctr.add(Restrictions.eq("isJobAssessment",true));
				ctr.add(Restrictions.eq("status","a")); 
				//ctr.add(Restrictions.eq("jobStatus","o"));
				ctr.add(Restrictions.le("jobStartDate",dateWithoutTime));
				ctr.add(Restrictions.ge("jobEndDate",dateWithoutTime));
				
				assessmentJobRelations = criteria.list();
			}
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentJobRelations;

	}
	
	/* @Author: Sudhansu Sekhar Swain	
	 * @Discription: It is used to get find Relation exist By JobOrder.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public AssessmentJobRelation findRelationExistByJobOrder(JobOrder jobOrder) 
	{
		List<AssessmentJobRelation> assessmentJobRelationslst = null;
		AssessmentJobRelation assessmentJobRelations = null;
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("status", "A");
			Criterion criterion1 = Restrictions.eq("jobId", jobOrder);

			criteria.add(criterion);
			criteria.add(criterion1);
			assessmentJobRelationslst = criteria.list();
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		if(assessmentJobRelationslst.size()>0){
			assessmentJobRelations=assessmentJobRelationslst.get(0);
			return assessmentJobRelations;
		}else{
			return null;
		}

	}
	@Transactional(readOnly=false)
	public List<AssessmentJobRelation> findByAllAssessmentJobRelation()
	{
		List<AssessmentJobRelation> lstAssessmentJobRelation= null;
		try 
		{
			lstAssessmentJobRelation = findByCriteria();		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstAssessmentJobRelation;
	}
	
	/* @Author: Sudhansu
	 * @Discription: It is used to get findRelationByJobOrder.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<AssessmentJobRelation> findRelationByJobOrder(List<JobOrder> jobOrders)
	{
		List<AssessmentJobRelation> assessmentJobRelations = new ArrayList<AssessmentJobRelation>();
		try {
			if(jobOrders.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion = Restrictions.eq("status", "A");
				Criterion criterion1 = Restrictions.in("jobId", jobOrders);
	
				criteria.add(criterion);
				criteria.add(criterion1);
				
				Criteria ctr=criteria.createCriteria("jobId");
				ctr.add(Restrictions.eq("isJobAssessment",true));
				// No active check
				//ctr.add(Restrictions.eq("status","a")); 
				//ctr.add(Restrictions.eq("jobStatus","o"));
				assessmentJobRelations = criteria.list();
			}
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentJobRelations;

	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<AssessmentJobRelation> findRelationByJobOrderNoDate(JobOrder jobOrder) 
	{
		List<AssessmentJobRelation> assessmentJobRelations = null;
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("status", "A");
			Criterion criterion1 = Restrictions.eq("jobId", jobOrder);

			criteria.add(criterion);
			criteria.add(criterion1);
			
			Criteria ctr=criteria.createCriteria("jobId");
			ctr.add(Restrictions.eq("isJobAssessment",true));
			//ctr.add(Restrictions.eq("status","a")); 
			//ctr.add(Restrictions.eq("jobStatus","o"));
			assessmentJobRelations = criteria.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentJobRelations;

	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<AssessmentJobRelation> findRelationByJobOrdersNoDate(List<JobOrder> jobOrders) 
	{
		List<AssessmentJobRelation> assessmentJobRelations = null;
		try {
			if(jobOrders.size()>0)
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion = Restrictions.eq("status", "A");
				Criterion criterion1 = Restrictions.in("jobId", jobOrders);
				criteria.add(criterion);
				criteria.add(criterion1);
				
				Criteria ctr=criteria.createCriteria("jobId");
				ctr.add(Restrictions.eq("isJobAssessment",true));
				ctr.add(Restrictions.eq("status","a")); 
				//ctr.add(Restrictions.eq("jobStatus","o"));
				assessmentJobRelations = criteria.list();
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentJobRelations;

	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public AssessmentJobRelation getAssessmentJobRelationByJobOrder(JobOrder jobOrder) 
	{
		List<AssessmentJobRelation> assessmentJobRelationslst = new ArrayList<AssessmentJobRelation>();
		AssessmentJobRelation assessmentJobRelations = null;
		try {
			Criterion criterion1 = Restrictions.eq("status", "A");
			Criterion criterion2 = Restrictions.eq("jobId", jobOrder);

			assessmentJobRelationslst=findByCriteria(criterion1,criterion2);
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		if(assessmentJobRelationslst.size()==1)
			assessmentJobRelations=assessmentJobRelationslst.get(0);
			
		return assessmentJobRelations;
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get findRelationByJobOrder.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<AssessmentJobRelation> findRelationByJobOrderForCG(JobOrder jobOrder) 
	{
		List<AssessmentJobRelation> assessmentJobRelations = null;
		try {
			//Date dateWithoutTime = Utility.getDateWithoutTime();
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("status", "A");
			Criterion criterion1 = Restrictions.eq("jobId", jobOrder);

			criteria.add(criterion);
			criteria.add(criterion1);
			
			Criteria ctr=criteria.createCriteria("jobId");
			ctr.add(Restrictions.eq("isJobAssessment",true));
			//ctr.add(Restrictions.eq("status","a")); 
			//ctr.add(Restrictions.eq("jobStatus","o"));
			//ctr.add(Restrictions.le("jobStartDate",dateWithoutTime));
			//ctr.add(Restrictions.ge("jobEndDate",dateWithoutTime));
			
			assessmentJobRelations = criteria.list();
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentJobRelations;

	}
	//shadab
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<AssessmentJobRelation> findRelationByJobOrderForJSI(JobOrder jobOrder) 
	{
		List<AssessmentJobRelation> assessmentJobRelations = null;
		try {
			Date dateWithoutTime = Utility.getDateWithoutTime();
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("status", "A");
			Criterion criterion1 = Restrictions.eq("jobId", jobOrder);

			criteria.add(criterion);
			criteria.add(criterion1);
			
			Criteria ctr=criteria.createCriteria("jobId");
			ctr.add(Restrictions.eq("isJobAssessment",true));
			ctr.add(Restrictions.eq("status","a")); 
			//ctr.add(Restrictions.eq("jobStatus","o"));
			//ctr.add(Restrictions.le("jobStartDate",dateWithoutTime));
			//ctr.add(Restrictions.ge("jobEndDate",dateWithoutTime));
			
			assessmentJobRelations = criteria.list();
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentJobRelations;

	}
	
	/**
	 * It is used to find the {@link List} of <code>JobOrder</code> by <code>AssessmentDetail</code>.
	 * @author Amit Chaudhary
	 * @param assessmentDetail
	 * @return {@link List} of <code>JobOrder</code>
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<JobOrder> findJobsByAssessment(AssessmentDetail assessmentDetail) 
	{
		List<JobOrder> jobOrderList = null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("assessmentId", assessmentDetail));
			criteria.setProjection(Projections.groupProperty("jobId"));
			jobOrderList = criteria.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return jobOrderList;
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<AssessmentJobRelation> findRelationByJobOrderNoDateOP(JobOrder jobOrder) 
	{
		List<AssessmentJobRelation> assessmentJobRelations = new ArrayList<AssessmentJobRelation>();;
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("status", "A");
			Criterion criterion1 = Restrictions.eq("jobId", jobOrder);
			criteria.createAlias("assessmentId", "assessId");
			criteria.setProjection(Projections.projectionList()
					.add(Projections.property("assessmentJobRelationId"))
					.add(Projections.property("assessId.assessmentId"))
					);
			criteria.add(criterion);
			criteria.add(criterion1);
			
			Criteria ctr=criteria.createCriteria("jobId");
			ctr.add(Restrictions.eq("isJobAssessment",true));
			//ctr.add(Restrictions.eq("status","a")); 
			List<String []> assessmentJobRelationsLst=new ArrayList<String []>();
			assessmentJobRelationsLst = criteria.list();
			for (Iterator it = assessmentJobRelationsLst.iterator(); it.hasNext();)
			{
				Object[] row = (Object[]) it.next();
				
				if(row[0]!=null){
					AssessmentJobRelation assessmentJobRelation = new AssessmentJobRelation();
					assessmentJobRelation.setAssessmentJobRelationId(Integer.parseInt(row[0].toString()));
					if(row[1]!=null){
						AssessmentDetail assDetail = new AssessmentDetail();
						assDetail.setAssessmentId(Integer.parseInt(row[1].toString()));
						assessmentJobRelation.setAssessmentId(assDetail);
					}	
					assessmentJobRelations.add(assessmentJobRelation);
				}
			}
		
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentJobRelations;

	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<AssessmentJobRelation> findAssessmentJobRelationByAssessmentId(List<JobOrder> lstJobOrder,List<AssessmentDetail> lstAssessment) 
	{
		List<AssessmentJobRelation> assessmentJobRelationList = new ArrayList<AssessmentJobRelation>();
		try{
	 		Criterion criterion1 = Restrictions.in("jobId", lstJobOrder);
			//Criterion criterion2 = Restrictions.in("assessmentId",lstAssessment);				
			assessmentJobRelationList = findByCriteria(criterion1);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assessmentJobRelationList;
	}
//Added by Kumar Avinash	
	@Transactional(readOnly=false)	
 	public List<AssessmentJobRelation>  findRelationByJobOrder_Op(List<JobOrder> jobOrders) throws Exception 
 	{	  
		List<Object[]> objList=null; 
		List<AssessmentJobRelation> assessmentJobRelationList = new ArrayList<AssessmentJobRelation>();
  	String jobOrderIDs="";
  	int p=0;
		for(JobOrder jobOrder: jobOrders){
			if(p==0){
				jobOrderIDs=jobOrder.getJobId()+"";
			}else{
				jobOrderIDs+=","+jobOrder.getJobId()+"";
			}
 			p++; 
		}	
    		 try{
 		     Query  query=null;
 	 	     Session session = getSession();
 	  	    if(jobOrderIDs!=null ){ 
 	     	String   hql = "SELECT   ajr.jobId,ajr.assessmentId FROM  assessmentjobrelation ajr "
  		       +   "left join joborder jo on jo.jobId= ajr.jobId "
  		     +   "left join assessmentdetail ad on ad.assessmentId= ajr.assessmentId "
  		         + " WHERE   ajr.status='A' and ajr.jobId in("+jobOrderIDs+") and jo.isJobAssessment=1 ";
 		        query=session.createSQLQuery(hql);
 		        objList  = query.list(); 
 		       AssessmentJobRelation assessmentJobRelation=null;
 		        if(objList.size()>0){
 		           for (int j = 0; j < objList.size(); j++) {
 		       	    Object[] objArr2 = objList.get(j);
 		       	assessmentJobRelation = new AssessmentJobRelation();
 	 	         AssessmentDetail assessmentDetail=new AssessmentDetail();
 	 	         JobOrder jobOrder=new JobOrder();

 	 	         if(objArr2[0]!=null){
 	 	        	jobOrder.setJobId(objArr2[0] == null ? null : Integer.parseInt(objArr2[0].toString()));
 	 	        	assessmentJobRelation.setJobId(jobOrder);
 	 	         }
 	 	          if(objArr2[1]!=null){
 	 	         assessmentDetail.setAssessmentId(objArr2[1] == null ? null : Integer.parseInt(objArr2[1].toString()));
 	 	          assessmentJobRelation.setAssessmentId(assessmentDetail);
 	 	         }  
 	 	         assessmentJobRelationList.add(assessmentJobRelation);
 		              }
 		            }
 		          }
 		        }
 		   catch(Exception e){
 		    e.printStackTrace();
 		  }
 		   return assessmentJobRelationList;
 	}	
	
	@Transactional(readOnly=true)
 	public AssessmentJobRelation findRelationExistByJobOrder_Opp(JobOrder jobOrder) 
	{
 		AssessmentJobRelation assessmentJobRelations = null;
		try {
			Session session = getSession();
	  if(jobOrder!=null){
			assessmentJobRelations = (AssessmentJobRelation) session.createCriteria(AssessmentJobRelation.class).add(Restrictions.eq("status", "A")).add(Restrictions.eq("jobId", jobOrder)).uniqueResult();
	      }	
	  } catch (HibernateException e) {
			e.printStackTrace();
		}
		
		if(assessmentJobRelations!=null){
			 return assessmentJobRelations;

		 }
		 else
			 return null;
		

	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<AssessmentJobRelation> findRelationByJobOrderNoDate_Opp(JobOrder jobOrder) 
	{
		List<AssessmentJobRelation> assessmentJobRelations = new ArrayList<AssessmentJobRelation>();
		List<Object[]> objList=null; 
		try {
			Session session = getSession();
		  	String   hql = "SELECT   ajr.assessmentId,ajr.jobId FROM  assessmentjobrelation ajr "
		         +   "left join joborder jo on jo.jobId= ajr.jobId "
		          + " WHERE   ajr.status='A' and ajr.jobId ="+jobOrder.getJobId()+" and jo.isJobAssessment=1 ";
		         Query  query=session.createSQLQuery(hql);
		         objList  = query.list();
		         AssessmentJobRelation assessmentJobRelation=null;
		         JobOrder jobOrder1=null;
		         AssessmentDetail assessmentDetail=null;
		         if(objList.size()>0&&objList!=null){
		        	 for (int j = 0; j < objList.size(); j++) {
		 		       	    Object[] objArr2 = objList.get(j);
		 		       	assessmentJobRelation = new AssessmentJobRelation();
		 		         jobOrder1=new JobOrder();
		 		        assessmentDetail=new AssessmentDetail();
		 		       if(objArr2[1]!=null){
		 		    	  jobOrder1.setJobId(objArr2[1] == null ? null : Integer.parseInt(objArr2[1].toString()));
		 	 	        	assessmentJobRelation.setJobId(jobOrder1);
		 	 	         }
		 	 	          if(objArr2[0]!=null){
		 	 	           assessmentDetail.setAssessmentId(objArr2[0] == null ? null : Integer.parseInt(objArr2[0].toString()));
 		 	 	          assessmentJobRelation.setAssessmentId(assessmentDetail);
		 	 	         } 
		 	 	        assessmentJobRelations.add(assessmentJobRelation);
		        	 }
		        	 
		         }
			
		} 
		
		catch (HibernateException e) {
			e.printStackTrace();
		}
		
    	
		         
		         
		
		return assessmentJobRelations;

	}
	
	
	
	
	
	
	
}
