package tm.dao.assessment;

import tm.bean.assessment.AssessmentWiseStepMessage;
import tm.dao.generic.GenericHibernateDAO;

public class AssessmentWiseStepMessageDAO extends GenericHibernateDAO<AssessmentWiseStepMessage, Integer> {

	public AssessmentWiseStepMessageDAO() {
		super(AssessmentWiseStepMessage.class);
	}
}
