package tm.dao.assessment;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.StageThreeStatus;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Hanzala Subhani
 * @Discription: StageTwoStatusDAO DAO.
 */
public class StageThreeStatusDAO extends GenericHibernateDAO<StageThreeStatus, Integer> 
{
	public StageThreeStatusDAO() {
		super(StageThreeStatus.class);
	}

	@Transactional(readOnly=false)
	public List<StageThreeStatus> getActiveStageThree(){
		List<StageThreeStatus> stageThreeStatus =  new ArrayList<StageThreeStatus>();
		try{
			Criterion criterion = 	Restrictions.eq("status", "A");
			stageThreeStatus	=	findByCriteria(Order.asc("name") ,criterion);

		} catch(Exception e){

		}
		return stageThreeStatus;
	}

}
