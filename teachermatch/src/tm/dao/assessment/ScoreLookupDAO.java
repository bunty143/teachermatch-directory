package tm.dao.assessment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.assessment.ScoreLookup;
import tm.bean.assessment.ScoreLookupMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class ScoreLookupDAO extends GenericHibernateDAO<ScoreLookup, Integer> 
{
	
	public ScoreLookupDAO() {
		super(ScoreLookup.class);
	}
	
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List getCandidatesNormScoreDomainWise(String domainScores) 
	{
		Session session = getSession();
		String sql = "";
		sql = "SELECT domainId,percentile,zscore,tscore,ncevalue,normscore,ritvalue,pass FROM scorelookup where "+domainScores;
		//sql = "SELECT * FROM  scorelookup where "+domainScores+" AND  scoreLookupMaster=:scoreLookupMaster";
		
		System.out.println("getCandidatesNormScoreDomainWise sql == "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			//query.setParameter("scoreLookupMaster", scoreLookupMaster );
			List<Object[]> rows = query.list();
			
			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List getCandidatesNormScoreCompetencyWise(String competencyScores) 
	{
		Session session = getSession();
		String sql = "";
		sql = "SELECT competencyId,percentile,zscore,tscore,ncevalue,normscore,ritvalue	FROM scorelookup where "+competencyScores;
		
		System.out.println("getCandidatesNormScoreCompetencyWise sql == "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			
			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	public List<ScoreLookup> findScoreLookup(Integer type, Integer lookupId)
	{
		List<ScoreLookup> scoreLookups = new ArrayList<ScoreLookup>();
		try 
		{
			if(type==1)
			{
				Criterion criterion1 = Restrictions.isNotNull("domainMaster");
				Criterion criterion2 = Restrictions.eq("scoreLookupMaster.lookupId",lookupId);
				scoreLookups = findByCriteria(criterion1,criterion2);
				//scoreLookups = findByCriteria(criterion1);
			}else
			{
				Criterion criterion1 = Restrictions.isNotNull("competencyMaster");
				Criterion criterion2 = Restrictions.eq("scoreLookupMaster.lookupId",lookupId);
				scoreLookups = findByCriteria(criterion1,criterion2);
				//scoreLookups = findByCriteria(criterion1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return scoreLookups;
	}
	
}
