package tm.dao.assessment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.engine.SessionFactoryImplementor;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.AssessmentStepMaster;
import tm.dao.generic.GenericHibernateDAO;

public class AssessmentStepMasterDAO extends GenericHibernateDAO<AssessmentStepMaster, Integer> {

	public AssessmentStepMasterDAO() {
		super(AssessmentStepMaster.class);
	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List getStepByAssTempAndAss(Integer assessmentTemplateId, Integer assessmentId)
	{
		Session session = getSession();
		try {
			List<String[]> lst=new ArrayList<String[]>();
			Connection connection =null;
			SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
			ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
			connection = connectionProvider.getConnection();
			String sql = "SELECT assessmentstepmaster.assessmentStepId,assessmentstepmaster.stepName,temptable.stepMessage FROM assessmentstepmaster as assessmentstepmaster inner join (select assessmentStepId as assessmentStepId, stepMessage as stepMessage from templatewisestepmessage WHERE assessmentTemplateId="+assessmentTemplateId+" and status='A' and case when((select assessmentStepId from assessmentwisestepmessage WHERE assessmentId="+assessmentId+" and status='A' limit 0,1) IS NULL) then true else assessmentStepId not in (select assessmentStepId from assessmentwisestepmessage WHERE assessmentId="+assessmentId+" and status='A') end) temptable on temptable.assessmentStepId=assessmentstepmaster.assessmentStepId where assessmentstepmaster.status='A'";
			System.out.println("sql : "+sql);
		
			Statement ps=connection.createStatement(); 
			ResultSet rs=ps.executeQuery(sql);
			if(rs.next()){
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
      		        {
						allInfo[i-1]=rs.getString(i);
      		        }
					lst.add(allInfo);      		    
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
