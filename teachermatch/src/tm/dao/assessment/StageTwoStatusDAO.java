package tm.dao.assessment;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.StageTwoStatus;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Hanzala Subhani
 * @Discription: StageTwoStatusDAO DAO.
 */
public class StageTwoStatusDAO extends GenericHibernateDAO<StageTwoStatus, Integer> 
{
	public StageTwoStatusDAO() {
		super(StageTwoStatus.class);
	}

	@Transactional(readOnly=false)
	public List<StageTwoStatus> getActiveStageTwo(){
		List<StageTwoStatus> stageTwoStatus =  new ArrayList<StageTwoStatus>();
		try{
			Criterion criterion = 	Restrictions.eq("status", "A");
			stageTwoStatus		=	findByCriteria(Order.asc("name") ,criterion);

		} catch(Exception e){

		}
		return stageTwoStatus;
	}

}
