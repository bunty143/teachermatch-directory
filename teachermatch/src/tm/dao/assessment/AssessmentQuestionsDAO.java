package tm.dao.assessment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentQuestions;
import tm.bean.assessment.AssessmentSections;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.ObjectiveMaster;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Vishwanath Kumar
 * @Discription: AssessmentQuestionsDAO.
 */
public class AssessmentQuestionsDAO extends GenericHibernateDAO<AssessmentQuestions, Integer> 
{
	public AssessmentQuestionsDAO() {
		super(AssessmentQuestions.class);
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get questions of a particular section if sectionId is available.
	 */
	public List<AssessmentQuestions> getRNDSectionQuestions(AssessmentSections assessmentSections)
	{
		List<AssessmentQuestions> assessmentQuestions = new ArrayList<AssessmentQuestions>();

		try {

			Criterion criterion = Restrictions.eq("assessmentSections", assessmentSections);
			Criterion criterion1 = Restrictions.sqlRestriction("1=1 order by rand()");
			Criterion criterion2 = Restrictions.eq("status", "A");
			/*
			assessmentQuestions = findByCriteria(criterion,criterion1);*/
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion);
			criteria.add(criterion2);
			//Removed questionWeightage check
			//criteria.add(Restrictions.gt("questionWeightage", 0.0));
			
			if(assessmentSections.getAssessmentDetail().getAssessmentType()==1)
			{
				Criteria qp = criteria.createCriteria("questionsPool");
				//qp.add(Restrictions.gt("questionWeightage", 0.0));
				Criteria stageThreeCriteria = qp.createCriteria("stageThreeStatus");
				stageThreeCriteria.add(Restrictions.eq("shortName", "Active"));
				stageThreeCriteria.add(Restrictions.eq("status", "A"));
			}

			//criteria.add(criterion1);
			assessmentQuestions = criteria.list();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return assessmentQuestions;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get questions of a particular section if sectionId is available.
	 */
	public List<AssessmentQuestions> getSectionQuestionsWithWeightage(AssessmentSections assessmentSections)
	{
		List<AssessmentQuestions> assessmentQuestions = null;
		System.out.println("getSectionQuestionsWithWeightage::::::::::::::");
		try {
			Criterion criterion = Restrictions.eq("assessmentSections", assessmentSections);
			Criterion criterion1 = Restrictions.eq("status", "A");
			/*
			assessmentQuestions = findByCriteria(criterion);*/
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion);
			criteria.add(criterion1);
			
			if(assessmentSections.getAssessmentDetail().getAssessmentType()==1 || assessmentSections.getAssessmentDetail().getAssessmentType()==3 || assessmentSections.getAssessmentDetail().getAssessmentType()==4)
			{
				Criteria qp = criteria.createCriteria("questionsPool");
				//Removed questionWeightage check
				//criteria.add(Restrictions.gt("questionWeightage", 0.0));
				Criteria stageThreeCriteria = qp.createCriteria("stageThreeStatus");
				stageThreeCriteria.add(Restrictions.eq("shortName", "Active"));
				stageThreeCriteria.add(Restrictions.eq("status", "A"));
			}

			assessmentQuestions = criteria.list();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return assessmentQuestions;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get questions of a particular given sections if sectionIds are available.
	 */
	public List<AssessmentQuestions> getSectionsQuestionListWithoutWeightage(List<AssessmentSections> assessmentSectionsList,int noOfExperimentQuestions)
	{
		List<AssessmentQuestions> assessmentQuestions = null;

		try {
			Criterion criterion = Restrictions.in("assessmentSections", assessmentSectionsList);
			Criterion criterion1 = Restrictions.sqlRestriction("1=1 order by rand()");
			Criterion criterion2 = Restrictions.eq("status", "A");
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion);
			criteria.add(criterion2);
			//criteria.add(criterion1);
			
			criteria.add(Restrictions.eq("questionWeightage", 0.0));
			criteria.add(Restrictions.eq("isExperimental", true));
			
			/*if(assessmentSectionsList.get(0).getAssessmentDetail().getAssessmentType()==1)
			{
				Criteria criteria1 = criteria.createCriteria("questionsPool");
				criteria1.add(Restrictions.eq("questionWeightage", 0.0));
				criteria1.add(criterion1);
				//criteria1.addOrder(Order.asc("questionTaken"));


				Criteria qp = criteria.createCriteria("questionsPool");
				
				//qp.add(Restrictions.eq("questionWeightage", 0.0));
				
				Criteria stageThreeCriteria = qp.createCriteria("stageTwoStatus");
				stageThreeCriteria.add(Restrictions.eq("shortName", "Exptl"));
				stageThreeCriteria.add(Restrictions.eq("status", "A"));
			}*/

			//criteria.setMaxResults(noOfExperimentQuestions);
			assessmentQuestions = criteria.list();
			if(assessmentQuestions.size()>0)
			{
				Collections.shuffle(assessmentQuestions);
				if(assessmentQuestions.size()>noOfExperimentQuestions)
					assessmentQuestions = assessmentQuestions.subList(0, noOfExperimentQuestions);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return assessmentQuestions;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get questions of a particular section if sectionId is available.
	 */
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<AssessmentQuestions> getSectionQuestions(AssessmentSections assessmentSections)
	{
		List<AssessmentQuestions> assessmentQuestions = null;

		try {
			Criterion criterion = Restrictions.eq("assessmentSections", assessmentSections);
			assessmentQuestions = findByCriteria(criterion);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return assessmentQuestions;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get questions of a particular section according to particular filter.
	 */
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<AssessmentQuestions> filterQuestions(AssessmentQuestions assessmentQuestion,DomainMaster domainMaster,CompetencyMaster competencyMaster,ObjectiveMaster objectiveMaster)
	{
		List<AssessmentQuestions> assessmentQuestions = null;
		System.out.println("filterQuestions......................");
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			Criterion criterion = Restrictions.eq("assessmentDetail", assessmentQuestion.getAssessmentDetail());
			Criterion criterion1 = Restrictions.eq("assessmentSections", assessmentQuestion.getAssessmentSections());

			criteria.add(criterion);
			criteria.add(criterion1);

			Criteria ctr=criteria.createCriteria("questionsPool");
			ctr.createCriteria("domainMaster").add(Restrictions.eq("domainId", domainMaster.getDomainId()));

			if(competencyMaster!=null && competencyMaster.getCompetencyId()>0)
				ctr.createCriteria("competencyMaster").add(Restrictions.eq("competencyId", competencyMaster.getCompetencyId()));
			if(objectiveMaster!=null && objectiveMaster.getObjectiveId()>0)
				ctr.createCriteria("objectiveMaster").add(Restrictions.eq("objectiveId", objectiveMaster.getObjectiveId()));

			assessmentQuestions = criteria.list();
			//System.out.println("LLLLLLLLLLL : "+assessmentQuestions.size());

		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentQuestions;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get questions of a particular assessment.
	 */
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<AssessmentQuestions> getAssessmentQuestions(AssessmentDetail assessmentDetail)
	{
		List<AssessmentQuestions> assessmentQuestions = null;
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			Criterion criterion = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion1 = Restrictions.eq("status", "A");

			criteria.add(criterion);
			criteria.add(criterion1);

			assessmentQuestions = criteria.list();
			//System.out.println("LLLLLLLLLLL : "+assessmentQuestions.size());

		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentQuestions;
	}

	public List<AssessmentQuestions> getSectionQuestionsWithoutWeightageExp(List<AssessmentSections> assessmentSectionsList,int noOfExperimentQuestions)
	{
		List<AssessmentQuestions> assessmentQuestions = new ArrayList<AssessmentQuestions>();

		try {
			Criterion criterion = Restrictions.in("assessmentSections", assessmentSectionsList);
			/*
			assessmentQuestions = findByCriteria(criterion);*/
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion);
			criteria.add(Restrictions.eq("questionWeightage", 0.0));
			Criteria qp = criteria.createCriteria("questionsPool");
			//qp.add(Restrictions.eq("questionWeightage", 0.0));
			Criteria stageThreeCriteria = qp.createCriteria("stageThreeStatus");
			stageThreeCriteria.add(Restrictions.eq("shortName", "Active"));
			stageThreeCriteria.add(Restrictions.eq("status", "A"));


			criteria.setMaxResults(noOfExperimentQuestions);
			assessmentQuestions = criteria.list();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return assessmentQuestions;
	}
}
