package tm.dao.assessment;

import tm.bean.assessment.QuestionOptions;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Vishwanath Kumar
 * @Discription: QuestionOptions DAO.
 */
public class QuestionOptionsDAO extends GenericHibernateDAO<QuestionOptions, Integer> 
{
	public QuestionOptionsDAO() {
		super(QuestionOptions.class);
	}

}
