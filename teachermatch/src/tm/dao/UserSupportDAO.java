package tm.dao;

import tm.bean.UserSupport;
import tm.dao.generic.GenericHibernateDAO;

public class UserSupportDAO extends GenericHibernateDAO<UserSupport, Integer>{

	public UserSupportDAO() {
		super(UserSupport.class);
	}
}
