package tm.dao;

import tm.bean.JobShare;
import tm.dao.generic.GenericHibernateDAO;

public class JobShareDAO extends GenericHibernateDAO<JobShare, Integer>{
	public JobShareDAO() {
		super(JobShare.class);
	}

}
