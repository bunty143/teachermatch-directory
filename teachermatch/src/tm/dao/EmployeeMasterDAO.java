package tm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EmployeeMaster;
import tm.bean.JobForTeacher;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class EmployeeMasterDAO extends GenericHibernateDAO<EmployeeMaster, Integer> 
{
	public EmployeeMasterDAO() {
		super(EmployeeMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<EmployeeMaster> checkEmployee(String ssn,String employeeCode,String firstName,String lastName,DistrictMaster districtMaster)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		try 
		{
			employeeCode=employeeCode==null?"":employeeCode;
			Criterion criterion1 = Restrictions.eq("SSN",ssn);
			Criterion criterion2 = Restrictions.eq("employeeCode",employeeCode);
			Criterion criterion3 = Restrictions.eq("firstName",firstName.trim());
			Criterion criterion4 = Restrictions.eq("lastName",lastName.trim());
			Criterion criterion5 = Restrictions.eq("districtMaster",districtMaster);
			if(employeeCode.equals(""))
			    employeeMasters = findByCriteria(criterion1,criterion3,criterion4,criterion5);
			else
				employeeMasters = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMasters;
	}
	@Transactional(readOnly=false)
	public EmployeeMaster checkEmployeeByEmployeeCode(String employeeCode,DistrictMaster districtMaster)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		EmployeeMaster employeeMaster = null;
		try 
		{
			
			Criterion criterion1 = Restrictions.eq("employeeCode",employeeCode);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			employeeMasters = findByCriteria(criterion1,criterion2);
			if(employeeMasters.size()>0)
				employeeMaster = employeeMasters.get(0);
				
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMaster;
	}
	@Transactional(readOnly=false)
	public List<EmployeeMaster> checkEmployeeBySSN(String ssn,String firstName,String lastName,DistrictMaster districtMaster)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("SSN",ssn);
			Criterion criterion3 = Restrictions.eq("firstName",firstName.trim());
			Criterion criterion4 = Restrictions.eq("lastName",lastName.trim());
			Criterion criterion5 = Restrictions.eq("districtMaster",districtMaster);
			employeeMasters = findByCriteria(criterion1,criterion3,criterion4,criterion5);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMasters;
	}
	
	@Transactional(readOnly=false)
	public List<EmployeeMaster> checkEmployeeByFistName(List<String> teacherFNameList,List<String> teacherLNameList,List<String> teacherSSNList,DistrictMaster districtMaster)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		try 
		{
			if(teacherFNameList.size()>0 && districtMaster!=null){
				Criterion criterion1 = Restrictions.in("firstName",teacherFNameList);
				Criterion criterion2 = Restrictions.in("lastName",teacherLNameList);
				Criterion criterion3 = Restrictions.in("SSN",teacherSSNList);
				Criterion criterion4 = Restrictions.eq("districtMaster",districtMaster);
				
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				criteria.setProjection(Projections.distinct(Projections.property("employeeId")));
				List<Integer> empFList = criteria.add(criterion1).add(criterion4).list();
				if(empFList.size()>0 && teacherLNameList.size()>0)
				{
					Criteria  criteria2 = session.createCriteria(getPersistentClass()) ;
					criteria2.setProjection(Projections.distinct(Projections.property("employeeId")));
					List<Integer> empLList = criteria2.add(criterion2).add(Restrictions.in("employeeId",empFList)).add(criterion4).list();
					if(empLList.size()>0 && teacherSSNList.size()>0)
					{
						employeeMasters = findByCriteria(criterion3,Restrictions.in("employeeId",empLList));
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMasters;
	}
	
	@Transactional(readOnly=false)
	public List<EmployeeMaster> checkEmployeesByEmployeeCodes(List<String> employeeCodeList,DistrictMaster districtMaster)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.in("employeeCode",employeeCodeList);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			employeeMasters = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMasters;
	}
	
	@Transactional(readOnly=false)
	public EmployeeMaster getByEmployeeCodeANdDID(String employeeCode,DistrictMaster districtMaster,String iFECode)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		EmployeeMaster employeeMaster = null;
		try 
		{
			
			Criterion criterion1 = Restrictions.eq("employeeCode",employeeCode);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion3 =null;
			if(iFECode.equalsIgnoreCase("1"))
				 criterion3=Restrictions.ne("FECode",0+"");
			else
				 criterion3=Restrictions.eq("FECode",iFECode);
			
			employeeMasters = findByCriteria(criterion1,criterion2,criterion3);
			if(employeeMasters!=null && employeeMasters.size()>0)
				employeeMaster = employeeMasters.get(0);
				
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMaster;
	}
	
	@Transactional(readOnly=false)
	public EmployeeMaster getByNameSSNAndDID(String ssn,String firstName,String lastName,DistrictMaster districtMaster,String iFECode)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		EmployeeMaster employeeMaster = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("SSN",ssn);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion3 = Restrictions.eq("firstName",firstName.trim());
			Criterion criterion4 = Restrictions.eq("lastName",lastName.trim());
			Criterion criterion5 =null;
			if(iFECode.equalsIgnoreCase("1"))
				 criterion5=Restrictions.ne("FECode",0+"");
			else
				 criterion5=Restrictions.eq("FECode",iFECode);
			
			employeeMasters = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
			if(employeeMasters!=null && employeeMasters.size()>0)
				employeeMaster = employeeMasters.get(0);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMaster;
	}
	
	
	@Transactional(readOnly=false)
	public List<EmployeeMaster> getByEmployeeCodeANdDIDInEx(List<String> employeeCodes,DistrictMaster districtMaster)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.in("employeeCode",employeeCodes);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			employeeMasters = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMasters;
	}
	
	@Transactional(readOnly=false)
	public List<EmployeeMaster> checkEmployeeByFullSSN(String ssn,DistrictMaster districtMaster)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("SSN",ssn);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			employeeMasters = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMasters;
	}
	
	@Transactional(readOnly=false)
	public List<EmployeeMaster> getDismissedTeacherListBySsn(List<String> ssnList)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		Date dateWithoutTime = Utility.getDateWithoutTime();
		try 
		{
			if(ssnList!=null && ssnList.size()>0)
			{
			Criterion criterion1 = Restrictions.in("SSN",ssnList);
			Criterion crDate =  Restrictions.isNotNull("dismissedDate");
			Criterion criStatus = Restrictions.eq("status","A");
			
			employeeMasters = findByCriteria(criterion1,criStatus,crDate);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		System.out.println("list size from dismis teacher dao"+employeeMasters.size());
		return employeeMasters;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<EmployeeMaster> getDoNotHireTeacherListBySsnTeacher(List<String> ssnList)
	{
		
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		Date dateWithoutTime = Utility.getDateWithoutTime();
		try 
		{
			if(ssnList!=null && ssnList.size()>0)
			{
			Criterion criterion1 = Restrictions.in("SSN",ssnList);
			Criterion crDoNotHire =  Restrictions.eq("doNotHireStatus","1");
			employeeMasters = findByCriteria(criterion1,crDoNotHire);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMasters;
	}
	
	
	@Transactional(readOnly=false)
	public List<EmployeeMaster> getDoNotHireTeacherListBySsn(List<String> ssnList,DistrictMaster districtMaster)
	{
		System.out.println("District id in employeemaster in ncdpi::"+districtMaster.getDistrictId());
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		Date dateWithoutTime = Utility.getDateWithoutTime();
		try 
		{
			Criterion criterion1 = Restrictions.in("SSN",ssnList);
			Criterion crDoNotHire =  Restrictions.eq("doNotHireStatus","1");
			Criterion crdistrictMaster =  Restrictions.eq("districtMaster",districtMaster);
			
			employeeMasters = findByCriteria(criterion1,crDoNotHire,crdistrictMaster);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMasters;
	}
	@Transactional(readOnly=false)
	public List<EmployeeMaster> getDoNotHireTeacherListBySsn1(List<String> ssnList,DistrictMaster districtMaster)
	{
		System.out.println("District id in employeemaster in ncdpi::"+districtMaster.getDistrictId());
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.in("SSN",ssnList);
			Criterion crdistrictMaster =  Restrictions.eq("districtMaster",districtMaster);
			employeeMasters = findByCriteria(criterion1,crdistrictMaster);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMasters;
	}
	
	@Transactional(readOnly=false)
	public List<EmployeeMaster> getTeacherInfoListBySsn(String ssnList,DistrictMaster districtId)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("SSN",ssnList);
			Criterion criStatus = Restrictions.eq("status","A");
			Criterion Did = Restrictions.eq("districtMaster",districtId);						
			employeeMasters = findByCriteria(criterion1,criStatus,Did);
			
			if(employeeMasters==null)
			employeeMasters = findByCriteria(criterion1,criStatus);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMasters;
	}
	

	
	@Transactional(readOnly=false)
	public List<EmployeeMaster> getEmployeeInfoByEmpId(Integer empId)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("employeeId",empId);
			Criterion criStatus = Restrictions.eq("status","A");
			//Criterion Did = Restrictions.eq("districtMaster",districtId);
						
			employeeMasters = findByCriteria(criterion1,criStatus);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
	 	    return employeeMasters;
	  }
	

//Sandeep 27-July
	
	@Transactional(readOnly=false)
	public List<EmployeeMaster> findEmployeeByDistricts(List<DistrictMaster> districtMasters){
		List<EmployeeMaster> lstEmployeeMaster= null;
		try 
		{
			Criterion criterionDistrict = Restrictions.in("districtMaster",districtMasters);
			lstEmployeeMaster = findByCriteria(criterionDistrict);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstEmployeeMaster;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<EmployeeMaster> findEmployeeByHQL(int districtId,String dob,String firstName,String lastName,String email,String empSSN){
		List<EmployeeMaster> lstEmployeeMaster= new ArrayList<EmployeeMaster>();
		Session session   = getSession();
		Connection connection = null;
		try{
			 SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
			 ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
			 connection = connectionProvider.getConnection();
			 
/*			 String dobValue1   = dob.substring(0, 2);
			 String dobValue2   = dob.substring(2,4);
			 String dobValue    = dobValue1+"-"+dobValue2;*/
			 String emailCOnd ="";
			 if(email!="" && email!=null)
			  emailCOnd   = " AND emailAddress='"+email+"'";
			 String sql        = "SELECT * from employeemaster where districtId="+districtId+" AND dob like '%"+dob+"' AND SSN='"+empSSN+"'";
			 System.out.println(":::Sql"+sql);
			 Statement st      = connection.createStatement();
			 ResultSet rst     = st.executeQuery(sql);
			 if(rst!=null){
				 while (rst.next()) {
					 EmployeeMaster emp = new EmployeeMaster();
					 String fName   = rst.getString("firstName");
					 String lName   = rst.getString("lastName");
					 int employeeId = rst.getInt("employeeId");
					 String pCCode   = rst.getString("PCcode");
					 String employeeCode   = rst.getString("employeeCode"); 
					 String fECode   = rst.getString("FECode");
					 emp.setEmployeeId(employeeId);
					 emp.setFirstName(firstName);
					 emp.setLastName(lastName);
		             emp.setPCCode(pCCode);
		             emp.setEmployeeCode(employeeCode);
		             emp.setFECode(fECode);
		             lstEmployeeMaster.add(emp);
				 }
			 }
		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
	
		return lstEmployeeMaster;
	}

	@Transactional(readOnly=false)
	public List<EmployeeMaster> checkEmployeeByFistNameOp(List<String> teacherFNameList,List<String> teacherLNameList,List<String> teacherSSNList,DistrictMaster districtMaster)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		try 
		{
			if(teacherFNameList.size()>0 && districtMaster!=null){
				Criterion criterion1 = Restrictions.in("firstName",teacherFNameList);
				Criterion criterion2 = Restrictions.in("lastName",teacherLNameList);
				Criterion criterion3 = Restrictions.in("SSN",teacherSSNList);
				Criterion criterion4 = Restrictions.eq("districtMaster",districtMaster);
				
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				criteria.setProjection(Projections.distinct(Projections.property("employeeId")));
				List<Integer> empFList = criteria.add(criterion1).add(criterion4).list();
				if(empFList.size()>0 && teacherLNameList.size()>0)
				{
					Criteria  criteria2 = session.createCriteria(getPersistentClass()) ;
					criteria2.setProjection(Projections.distinct(Projections.property("employeeId")));
					List<Integer> empLList = criteria2.add(criterion2).add(Restrictions.in("employeeId",empFList)).add(criterion4).list();
					if(empLList.size()>0 && teacherSSNList.size()>0)
					{
						Criteria  criteriaNew = session.createCriteria(getPersistentClass()) ;
						criteriaNew.setProjection(Projections.projectionList()
								.add(Projections.property("employeeId"))
								.add(Projections.property("PCCode"))
								.add(Projections.property("OCCode"))
								.add(Projections.property("RRCode"))
								.add(Projections.property("FECode"))
								.add(Projections.property("employeeCode"))
								.add(Projections.property("SSN"))
								);
						criteriaNew.add(Restrictions.in("employeeId",empLList)).add(criterion3);
						List<String []> emplistLst=new ArrayList<String []>();
						emplistLst = criteriaNew.list();
						//employeeMasters = findByCriteria(criterion3,Restrictions.in("employeeId",empLList));
						
						for (Iterator it = emplistLst.iterator(); it.hasNext();)
						{
		    				//System.out.println("call");
		    				
		    				Object[] row = (Object[]) it.next();    				
		    				if(row[0]!=null){
		    					EmployeeMaster empMas = new EmployeeMaster();
		    						empMas.setEmployeeId(Integer.parseInt(row[0].toString()));
		    					if(row[1]!=null)
		    						empMas.setPCCode(row[1].toString());
		    					if(row[2]!=null)
		    						empMas.setOCCode(row[2].toString());
		    					if(row[3]!=null)
		    						empMas.setRRCode(row[3].toString());
		    					if(row[4]!=null)
		    						empMas.setFECode(row[4].toString());
		    					if(row[5]!=null)
		    						empMas.setEmployeeCode(row[5].toString());
		    					if(row[6]!=null)
		    						empMas.setSSN(row[6].toString());
		    				}
						}
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMasters;
	}
	@Transactional(readOnly=false)
	public EmployeeMaster checkEmployeeSSNAndDob(String ssn,Date dob)
	{
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		EmployeeMaster employeeMaster = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("SSN",ssn);
			Criterion criterion2 = Restrictions.eq("dob",dob);
			employeeMasters = findByCriteria(criterion1,criterion2);
		if(employeeMasters.size()>0)
				employeeMaster = employeeMasters.get(0);
				
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMaster;
	}

  @Transactional(readOnly=false)
  public List<EmployeeMaster> findEmployeesByCode(List<String> employeeCodes)
  {
		List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.in("employeeCode",employeeCodes);
			employeeMasters = findByCriteria(criterion1);
				
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return employeeMasters;
  }
}
