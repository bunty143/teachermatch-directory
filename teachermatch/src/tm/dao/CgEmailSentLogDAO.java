package tm.dao;

import tm.bean.CgEmailSentLog;
import tm.dao.generic.GenericHibernateDAO;

public class CgEmailSentLogDAO extends GenericHibernateDAO<CgEmailSentLog, Integer>  
{
	public CgEmailSentLogDAO() {
		super(CgEmailSentLog.class);
	}
}
