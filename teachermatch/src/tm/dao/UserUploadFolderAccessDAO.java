package tm.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import tm.bean.UserUploadFolderAccess;
import tm.dao.generic.GenericHibernateDAO;

public class UserUploadFolderAccessDAO extends GenericHibernateDAO<UserUploadFolderAccess, Integer> {
	public UserUploadFolderAccessDAO()
    {
        super(UserUploadFolderAccess.class);
    }   
}
