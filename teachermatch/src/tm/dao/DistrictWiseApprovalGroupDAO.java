package tm.dao;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictWiseApprovalGroup;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictWiseApprovalGroupDAO extends GenericHibernateDAO<DistrictWiseApprovalGroup, Integer>{
	
	public DistrictWiseApprovalGroupDAO() {
		super(DistrictWiseApprovalGroup.class);
	}
	
	@Transactional(readOnly=false)
	public List<DistrictWiseApprovalGroup> findGroupsByDistrictAndShortName(DistrictMaster districtMaster, String groupShortName)
	{
		List<DistrictWiseApprovalGroup> districtWiseApprovalGroupList = new ArrayList<DistrictWiseApprovalGroup>(); 
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.eq("groupShortName",groupShortName);
			
			districtWiseApprovalGroupList = findByCriteria(criterion1, criterion2);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return districtWiseApprovalGroupList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictWiseApprovalGroup> findByGroupShortName(List<String> groupShortName, DistrictMaster districtMaster)
		{
			System.out.println("------inside findByGroupShortName-----");
			System.out.println("groupShortName:- "+groupShortName+", districtMaster:- "+districtMaster.getDistrictId());
			
			List<DistrictWiseApprovalGroup> list = new ArrayList<DistrictWiseApprovalGroup>();
			
			try
			{
			Criterion criterion1 = Restrictions.in("groupShortName", groupShortName);
			Criterion criterion2 = Restrictions.eq("status", "A");
			Criterion criterion3 = Restrictions.eq("districtId", districtMaster);
			
			list = findByCriteria(criterion1, criterion2, criterion3);
			System.out.println("list:- "+list.size());
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return list;
		}
		
		@Transactional(readOnly=false)
		public Map<String, String> mapGroupShortNameWithGroupName(String approvalFlow, DistrictMaster districtMaster)
		{
			System.out.println("---------------inside mapGroupShortNameWithGroupName 123----------");
			
			System.out.println("approvalFlow:- "+approvalFlow+", DistirctId:- "+districtMaster.getDistrictId());
			
			Map<String, String> map = new LinkedHashMap<String, String>();
			List<String> groupShortName = new ArrayList<String>();
			
			for(String flow : approvalFlow.split("#"))
				if(!flow.equals(""))
					groupShortName.add(flow);
			
			System.out.println("groupShortName:- "+groupShortName);
			try
			{
			Criterion criterion1 = Restrictions.in("groupShortName", groupShortName);
			Criterion criterion2 = Restrictions.eq("status", "A");
			Criterion criterion3 = Restrictions.eq("districtId", districtMaster);
			
			List<DistrictWiseApprovalGroup> list = findByCriteria(criterion1, criterion2, criterion3);
			System.out.println("list:- "+list.size());
			
			for(DistrictWiseApprovalGroup districtWiseApprovalGroup : list)
			{
				map.put(districtWiseApprovalGroup.getGroupShortName(), districtWiseApprovalGroup.getGroupName());
				//map.put(districtWiseApprovalGroup.getGroupName(), districtWiseApprovalGroup.getGroupShortName());
				System.out.println("districtWiseApprovalGroup:- "+districtWiseApprovalGroup.getGroupName()+", "+districtWiseApprovalGroup.getGroupShortName());
			}
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			System.out.println("map:- "+map);
			return map;
	}
}