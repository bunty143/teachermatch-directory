package tm.dao;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictAccountingCode;
import tm.bean.DistrictJobCode;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictAccountingCodeDAO extends GenericHibernateDAO<DistrictAccountingCode, Integer> 
{
	public DistrictAccountingCodeDAO() {
		super(DistrictAccountingCode.class);
	}

	@Transactional(readOnly=false)
	public DistrictAccountingCode getDistrictAccountingCode(DistrictMaster districtMaster,String accountingCode)
	{
		List<DistrictAccountingCode> districtAccountingCodes= null;
		DistrictAccountingCode districtAccountingCode=null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("accountingCode",accountingCode);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			districtAccountingCodes = findByCriteria(criterion1,criterion2);	
			if(districtAccountingCodes.size()>0){
				districtAccountingCode=districtAccountingCodes.get(0);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return districtAccountingCode;
	}
	
}
