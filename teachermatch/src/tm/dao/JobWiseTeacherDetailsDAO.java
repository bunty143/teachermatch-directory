package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import tm.bean.JobOrder;
import tm.bean.JobWiseTeacherDetails;
import tm.bean.OctDetails;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;

public class JobWiseTeacherDetailsDAO extends GenericHibernateDAO<JobWiseTeacherDetails, Integer>{
	public JobWiseTeacherDetailsDAO() {
		super(JobWiseTeacherDetails.class);
		// TODO Auto-generated constructor stub
	}
	
	public List<JobWiseTeacherDetails> teacherSenNums(JobOrder jobOrder,List<TeacherDetail> teacherDetail){
		//jobWiseTeacherDetailsDAO
		List<JobWiseTeacherDetails> jobWiseTeacherDetails = new ArrayList<JobWiseTeacherDetails>();
		try {
			Criterion criterion = Restrictions.in("teacherDetail", teacherDetail);
			Criterion criterionJob = Restrictions.eq("jobOrder", jobOrder);
			jobWiseTeacherDetails = findByCriteria(criterion,criterionJob);
			System.out.println("jobWiseTeacherDetailsjobWiseTeacherDetailsjobWiseTeacherDetailsjobWiseTeacherDetails    "+jobWiseTeacherDetails.size());
		} catch (Exception e) {
		}
		return jobWiseTeacherDetails;
	}
}
