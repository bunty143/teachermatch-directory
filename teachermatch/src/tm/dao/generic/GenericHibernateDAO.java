package tm.dao.generic;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Filter;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.transaction.annotation.Transactional;

public abstract class GenericHibernateDAO<T, ID extends Serializable> 
{

	private Class<T> persistentClass;

	@Autowired
	private SessionFactory sessionFactory;

	protected Session session;
	public void setSession(Session session) {
		this.session = session;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public GenericHibernateDAO(Class<T> persistentClass) {
		/*this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];*/
		this.persistentClass=persistentClass;
	}


	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public T findById(ID id, boolean lock, boolean evict) {
		T entity;
		Session session = getSession();
		if (lock)
			entity = (T) session.get(getPersistentClass(), id, LockMode.UPGRADE);
		else
			entity = (T) session.get(getPersistentClass(), id);

		if(evict)
			session.evict(entity);

		return entity;
	}

	@Transactional(readOnly=true)
	public List<T> findAll() {
		return findByCriteria();
	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<T> findWithLimit(Order order,int startPos,int limit,Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}
		crit.setFirstResult(startPos);
		crit.setMaxResults(limit);

		if(order != null)
			crit.addOrder(order);

		return crit.list();
	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<T> findByExample(T exampleInstance, String[] excludeProperty) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		Example example =  Example.create(exampleInstance);
		for (String exclude : excludeProperty) {
			example.excludeProperty(exclude);
		}
		crit.add(example);
		return crit.list();
	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int getRowCount(Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}

		crit.setProjection(Projections.rowCount());
		List results = crit.list();
		int rowCount = (Integer) results.get(0);
		return rowCount;
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int getRowCountWithSort(Order order,Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}
		crit.setProjection(Projections.rowCount());
		if(order != null)
			crit.addOrder(order);
		
		List results = crit.list();
		int rowCount = (Integer) results.get(0);
		return rowCount;
	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<T> findWithAll(Order order,Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}
		return crit.list();
	}
	
	
	@Transactional(readOnly=false)
	public void makePersistent(T entity) {
		getSession().saveOrUpdate(entity);
	}

	@Transactional(readOnly=false)
	public void updatePersistent(T entity) {
		getSession().update(entity);
	}

	@Transactional(readOnly=false)
	public void makeTransient(T entity) {
		getSession().delete(entity);
		session.flush();
	}

	public void flush() {
		getSession().flush();
	}

	public void clear() {
		getSession().clear();
	}

	/**
	 * Use this inside subclasses as a convenience method.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<T> findByCriteria(Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}
		return crit.list();
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<T> findByCriteriaTopTen(Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}
		crit.setFirstResult(0);
		crit.setMaxResults(10);
		
		return crit.list();
	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<T> findByLeftJoinCriteria(String alias,Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		crit.createAlias(alias,alias,CriteriaSpecification.LEFT_JOIN);

		for (Criterion c : criterion) {
			crit.add(c);
		}
		return crit.list();
	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List findByCriteriaProjection(ProjectionList projectionList,Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		crit.setProjection(projectionList);


		for (Criterion c : criterion) {
			crit.add(c);
		}
		return crit.list();
	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<T> findByCriteria(Order order,Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}
		if(order != null)
			crit.addOrder(order);

		return crit.list();
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<T> findByExample(T exampleInstance) {
		Session session = getSession();

		Example example = Example.create(exampleInstance)
		.enableLike(MatchMode.ANYWHERE)
		.excludeZeroes()
		.ignoreCase();

		List<T> result = session.createCriteria(getPersistentClass()).add(example).list();

		return result;

	}
	private String[] filterName;
	private String[] paramName;
	private String[] paramValue;

	public void applyFilterOn(String[] filterName,String[] paramName,String[] paramValue){
		this.filterName=filterName;
		this.paramName=paramName;
		this.paramValue=paramValue;
	}

	public void applyFilter(Session session){
		//System.out.println("apply filter");
		Filter filter = null;
		int i=0;
		//System.out.println(filterName);
		if(filterName !=null){
			for(String filterName:this.filterName){
				//System.out.println(paramName[i]+" : "+ paramValue[i]);
				filter = session.enableFilter(filterName);
				filter.setParameter(paramName[i], paramValue[i]);
				i++;
			}
		}
	}


	public Session getSession(){
		if(this.session==null || !this.session.isOpen() || this.session.isConnected())
			this.session=SessionFactoryUtils.getSession(sessionFactory, Boolean.FALSE);
		this.applyFilter(session);
		return this.session;

	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int getRowCountByExample(T exampleInstance) {
		Session session = getSession();

		Example example = Example.create(exampleInstance)
		.enableLike(MatchMode.ANYWHERE)
		.excludeZeroes()
		.ignoreCase();
		Criteria criteria = session.createCriteria(getPersistentClass());

		criteria.add(example);

		criteria.setProjection(Projections.rowCount());
		List results = criteria.list();
		int rowCount = (Integer) results.get(0);
		return rowCount;

	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<T> findByExample(T exampleInstance,int startPos,int limit, Order order) {
		Session session = getSession();

		Example example = Example.create(exampleInstance)
					 .enableLike(MatchMode.ANYWHERE)
					 .excludeZeroes()
					 .ignoreCase();
		Criteria criteria = session.createCriteria(getPersistentClass());
		criteria.add(example);
		//System.out.println("limit : "+limit);
		if(limit>0){
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
		}
		
		if(order!=null)
			criteria.addOrder(order);
		
		List<T> result = criteria.list();
								 
		return result;

	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int truncate(boolean flag) throws Exception {
	    int rowsAffected = 0;
	    try {
	    	System.out.println("sssddd");
	        Class c = getPersistentClass();
	        String hql = null;
	        if(flag)
	        	hql = "truncate " + c.getSimpleName().toLowerCase();
	        else
	        	hql = "delete from " + c.getSimpleName().toLowerCase();
	        
	        System.out.println("hql: "+hql);
	        Query q = getSession().createQuery( hql );
	        rowsAffected = q.executeUpdate();
	    } catch (Exception e ) {
	    	e.printStackTrace();
	    }
	    return rowsAffected;
	}
	
   @SuppressWarnings("deprecation")
   public void truncate() {	                 
	   System.out.println("sssdddfff");
	   Session destSession = sessionFactory.openSession();
	   destSession.beginTransaction();
	   Connection destConn = destSession.connection();		 
	   try {	        
		   
	       destConn.createStatement().executeUpdate("truncate "+getPersistentClass().getSimpleName().toLowerCase());		         
	   } 
	   catch (Exception e) {
	         throw new RuntimeException(e);
	   } 
	   finally {
	         destSession.getTransaction().commit();
	         destSession.close();
	   }       
   }
	   
 /*  private String getCreateSourceTableStatement(String tableName) {
	    Session srcSession = sourceSessionFactory.openSession();
	    
	    srcSession.beginTransaction();
	    Object[] result = (Object[]) srcSession.createSQLQuery("show create table " + tableName).uniqueResult();
	    String createStatement = (String) result[1]; 
	    srcSession.getTransaction().commit();
	    srcSession.close();
	    return createStatement;
	  }*/
   
   
   /*
    *  @Author:- Anurg
    *  purpose:- count row with qualified criteria or without criteria.
    *  Technique:- Using projection. 
   */
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int countRowByCriterion(Criterion...criterions) {
		int result =0;
		try{
			Criteria criteria = getSession().createCriteria(getPersistentClass());
			for(Criterion cr:criterions)
				criteria.add(cr);
			criteria.setProjection(Projections.rowCount());
			result= (Integer) criteria.uniqueResult();
	}
	catch (Exception e) {
		e.printStackTrace();
	}	
		return result;
	}
	
	
	
	  /*
	    *  @Author:- Anurg
	    *  purpose:- create proxy Object of Entity Bean class using ID.
	    *  Technique:- Hibernate Proxy Object. 
	   */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public T loadById(ID id) {
		T entity = null;
		
		try {
			Session session = getSession();
			entity = (T)session.load(this.persistentClass, id);
		} catch (HibernateException e) {
		   System.err.println("Exception in load EntityBean Object ");
			e.printStackTrace();
		}

		return entity;
	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<T> findByCriteria(List<Order> lstOrder,Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}
		
		for(Order o:lstOrder)
			crit.addOrder(o);

		return crit.list();
	}
	
}

