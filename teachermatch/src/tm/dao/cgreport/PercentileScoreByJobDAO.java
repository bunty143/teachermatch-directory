package tm.dao.cgreport;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.cgreport.PercentileCalculation;
import tm.bean.cgreport.PercentileScoreByJob;
import tm.bean.master.DomainMaster;
import tm.dao.generic.GenericHibernateDAO;

public class PercentileScoreByJobDAO extends GenericHibernateDAO<PercentileScoreByJob, Integer>{
	public PercentileScoreByJobDAO(){
		super(PercentileScoreByJob.class);
	}
	
	@Transactional(readOnly=false)
	public List<PercentileScoreByJob> findPercentileScoreByJobAndScore(List<Integer> lstScore, DomainMaster domainMaster, JobOrder jobOrder)
	{
		List<PercentileScoreByJob> lstPercentileScoreByJob= null;
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		try {
			Criterion criterion0 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion1 = Restrictions.in("score", lstScore);
			Criterion criterion2 = Restrictions.eq("domainMaster", domainMaster);
			Criterion criterion3 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterionStatus = Restrictions.eq("status", "A");
			lstPercentileScoreByJob = findByCriteria(criterion0, criterion1, criterion2, criterion3, criterionStatus);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		return lstPercentileScoreByJob;
	}
	
	@Transactional(readOnly=false)
	public List<PercentileScoreByJob> findPercentileScoreByJobAndDomain(DomainMaster domainMaster, JobOrder jobOrder)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		List<PercentileScoreByJob> lstPercentileScoreByJob= null;
		try{
			Criterion criterion0 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion1 = Restrictions.eq("domainMaster",domainMaster);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterionStatus = Restrictions.eq("status", "A");
			lstPercentileScoreByJob = findByCriteria(criterion0, criterion1, criterion2, criterionStatus);		
		} 
		catch (Exception e){
			e.printStackTrace();
		}		
		return lstPercentileScoreByJob;
	}
	
	@Transactional(readOnly=false)
	public List<PercentileScoreByJob> findPercentileScoreByJob(JobOrder jobOrder)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		List<PercentileScoreByJob> lstPercentileScoreByJob= null;
		try{
			Criterion criterion0 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterionStatus = Restrictions.eq("status", "A");
			lstPercentileScoreByJob = findByCriteria(criterion0, criterion2, criterionStatus);		
		} 
		catch (Exception e){
			e.printStackTrace();
		}		
		return lstPercentileScoreByJob;
	}
	@Transactional(readOnly=false)
	public List<PercentileScoreByJob> findPercentileScoreByJobList(List<JobOrder> jobOrderList)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		List<PercentileScoreByJob> lstPercentileScoreByJob= null;
		try{
			Criterion criterion0 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion2 = Restrictions.in("jobOrder", jobOrderList);
			Criterion criterionStatus = Restrictions.eq("status", "A");
			lstPercentileScoreByJob = findByCriteria(criterion0, criterion2, criterionStatus);		
		} 
		catch (Exception e){
			e.printStackTrace();
		}		
		return lstPercentileScoreByJob;
	}
}
