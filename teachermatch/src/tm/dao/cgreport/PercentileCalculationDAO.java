package tm.dao.cgreport;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.cgreport.PercentileCalculation;
import tm.bean.master.DomainMaster;
import tm.dao.generic.GenericHibernateDAO;

public class PercentileCalculationDAO extends GenericHibernateDAO<PercentileCalculation, Integer>{
	public PercentileCalculationDAO() {
		super(PercentileCalculation.class);
	}
	
	@Transactional(readOnly=false)
	public List<PercentileCalculation> findPercentileCalculationByScore(List<Integer> lstScore, DomainMaster domainMaster)
	{
		List<PercentileCalculation> lstPercentileCalculation = null;
		try {
			Criterion criterion1 = Restrictions.in("score", lstScore);
			Criterion criterion2 = Restrictions.eq("domainMaster", domainMaster);
			Criterion criterionStatus = Restrictions.eq("status", "A");
			lstPercentileCalculation = findByCriteria(criterion1, criterion2, criterionStatus);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstPercentileCalculation;
	}
	
	@Transactional(readOnly=false)
	public List<PercentileCalculation> findPercentileCalculationByDomain(DomainMaster domainMaster)
	{
		List<PercentileCalculation> lstPercentileCalculation= null;
		try	{
			Criterion criterion1 = Restrictions.eq("domainMaster",domainMaster);
			Criterion criterionStatus = Restrictions.eq("status", "A");
			lstPercentileCalculation = findByCriteria(criterion1, criterionStatus);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstPercentileCalculation;
	}
	@Transactional(readOnly=false)
	public List<PercentileCalculation> findPercentileCalculation()
	{
		List<PercentileCalculation> lstPercentileCalculation= null;
		try	{
			Criterion criterionStatus = Restrictions.eq("status", "A");
			lstPercentileCalculation = findByCriteria(Order.asc("tValue"),criterionStatus);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstPercentileCalculation;
	}
	@Transactional(readOnly=false)
	public List<PercentileCalculation> findAllInCurrenctYear()
	{
		List<PercentileCalculation> lstPercentileCalculation= null;
		try	{
			Criterion criterionStatus = Restrictions.eq("status", "A");
			lstPercentileCalculation = findByCriteria(criterionStatus);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstPercentileCalculation;
	}
}
