package tm.dao.cgreport;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.cgreport.JobCategoryWiseStatusPrivilege;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobCategoryWiseStatusPrivilegeDAO extends GenericHibernateDAO<JobCategoryWiseStatusPrivilege, Integer>{
	public JobCategoryWiseStatusPrivilegeDAO(){
		super(JobCategoryWiseStatusPrivilege.class);
	}
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> findStatusPrivilegeSchool(JobOrder jobOrder)
	{
		List<JobCategoryWiseStatusPrivilege> lstStatusPForSchool= new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());			
			Criterion criterion2 = Restrictions.eq("jobCategoryMaster", jobOrder.getJobCategoryMaster());
			lstStatusPForSchool = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstStatusPForSchool;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getStatus(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			Criterion criterion_district= Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criterion criterion_secondaryStatus= Restrictions.eq("secondaryStatus",secondaryStatus);
			jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterion_jobCategory,criterion_secondaryStatus);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
		
	}
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getAttacheFileNameStatus(JobOrder jobOrder,SecondaryStatus secondaryStatus)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try{
			Criterion criterion_district = Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());			
			Criterion criterion_jobCategory = Restrictions.eq("jobCategoryMaster", jobOrder.getJobCategoryMaster());
			Criterion criterion_secondaryStatus= Restrictions.eq("secondaryStatus",secondaryStatus);
			Criterion criterion_IAFN =Restrictions.and(Restrictions.isNotNull("instructionAttachFileName"),Restrictions.ne("instructionAttachFileName", ""));;
			jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterion_jobCategory,criterion_secondaryStatus,criterion_IAFN);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
		
	}
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getJobCWSP(JobOrder jobOrder,SecondaryStatus secondaryStatus,StatusMaster statusMaster)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try{
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			}else{
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			}
			
			Criterion criterion_district= Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
			jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterion_jobCategory,criterionStatus);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
		
	}
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getMaxJSIValueList(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus,StatusMaster statusMaster)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges= new ArrayList<JobCategoryWiseStatusPrivilege>();
		try{
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			}else{
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			}
			Criterion criterion_district= Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criterion criterion1= Restrictions.ge("maxValuePerJSIQuestion",1);
			jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterion_jobCategory,criterionStatus,criterion1);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
		
	}
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getFilterStatus(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			Criterion criterion_district= Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criterion criterion_secondaryStatus= Restrictions.eq("secondaryStatus",secondaryStatus);
			Criterion criterion_Date1= Restrictions.eq("statusUpdateExpiryDate",d2);
			Criterion criterion_Date2= Restrictions.ge("statusUpdateExpiryDate",d2);
			Criterion criterion_Date3= Restrictions.or(criterion_Date1,criterion_Date2);
			Criterion criterion_Date4= Restrictions.isNull("statusUpdateExpiryDate");
			Criterion criterion_Date= Restrictions.or(criterion_Date3,criterion_Date4);
			jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterion_jobCategory,criterion_secondaryStatus,criterion_Date);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
	}
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getFilterStatusByName(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus)
	{
		
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			Criterion criterion_district= Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			
			//Criterion criterion_secondaryStatus= Restrictions.eq("secondaryStatus.secondaryStatusName",secondaryStatus.getSecondaryStatusName());
			
			criteria.createCriteria("secondaryStatus").add(Restrictions.eq("secondaryStatusName", secondaryStatus.getSecondaryStatusName())).add(Restrictions.eq("status","A"));
			
			
			Criterion criterion_Date1= Restrictions.eq("statusUpdateExpiryDate",d2);
			Criterion criterion_Date2= Restrictions.ge("statusUpdateExpiryDate",d2);
			Criterion criterion_Date3= Restrictions.or(criterion_Date1,criterion_Date2);
			Criterion criterion_Date4= Restrictions.isNull("statusUpdateExpiryDate");
			Criterion criterion_Date= Restrictions.or(criterion_Date3,criterion_Date4);
			
			criteria.add(criterion_district);
			criteria.add(criterion_jobCategory);
			criteria.add(criterion_Date);
			
			jobCategoryWiseStatusPrivileges=criteria.list();
			
			//jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterion_jobCategory,criterion_secondaryStatus,criterion_Date);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> findStatusPrivilegeByJob(JobOrder jobOrder)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			Criterion criterion_district = null;
			Criterion criterion_jobCategory = null;
			criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());

			if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
				criterion_district= Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
			}
			else{
				criterion_district= Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
				if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null)
					criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster().getParentJobCategoryId());
			}
			
			Criterion criterion_Auto= Restrictions.eq("autoUpdateStatus",true);
			Criterion criterion_Date1= Restrictions.eq("statusUpdateExpiryDate",d2);
			Criterion criterion_Date2= Restrictions.ge("statusUpdateExpiryDate",d2);
			Criterion criterion_Date3= Restrictions.or(criterion_Date1,criterion_Date2);
			Criterion criterion_Date4= Restrictions.isNull("statusUpdateExpiryDate");
			Criterion criterion_Date= Restrictions.or(criterion_Date3,criterion_Date4);
			jobCategoryWiseStatusPrivileges=findByCriteria(criterion_Auto,criterion_district,criterion_jobCategory,criterion_Date);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
	}
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getFilterStatusWithDPoint(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus,StatusMaster statusMaster)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			}else{
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			}
			Criterion criterion_district= Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criterion criterion_Date1= Restrictions.eq("statusUpdateExpiryDate",d2);
			Criterion criterion_Date2= Restrictions.ge("statusUpdateExpiryDate",d2);
			Criterion criterion_Date3= Restrictions.or(criterion_Date1,criterion_Date2);
			Criterion criterion_Date4= Restrictions.isNull("statusUpdateExpiryDate");
			Criterion criterion_Date= Restrictions.or(criterion_Date3,criterion_Date4);
			jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterion_jobCategory,criterionStatus,criterion_Date);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
		
	}
	
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getFilterBySecondaryStatus(DistrictMaster districtMaster,SecondaryStatus secondaryStatus)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			Criterion criterionStatus=null;
			
			criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			
			Criterion criterion_district= Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_Date1= Restrictions.eq("statusUpdateExpiryDate",d2);
			Criterion criterion_Date2= Restrictions.ge("statusUpdateExpiryDate",d2);
			Criterion criterion_Date3= Restrictions.or(criterion_Date1,criterion_Date2);
			Criterion criterion_Date4= Restrictions.isNull("statusUpdateExpiryDate");
			Criterion criterion_Date= Restrictions.or(criterion_Date3,criterion_Date4);
			
			jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterionStatus,criterion_Date);
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
		
	}
		
	//***************************************************TPL 2024 (Internal And External Candidate) **************************************//
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getStatus(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster , List<SecondaryStatus> lstSecondaryStatus)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			System.out.println("sistricxt====="+districtMaster.getDistrictId());
			System.out.println("jobCategoryMaster====="+jobCategoryMaster.getJobCategoryId());
			System.out.println("secondaryStatus====="+lstSecondaryStatus.size());
			List<Integer> lstSS=new ArrayList<Integer>();
			for(SecondaryStatus ss:lstSecondaryStatus)
			lstSS.add(ss.getSecondaryStatusId());
			System.out.println(lstSS);
			Criterion criterion_district= Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criterion criterion_notNullIntAndExtCan= Restrictions.isNotNull("internalExternalOrBothCandidate");
			Criterion criterion_secondaryStatus= Restrictions.in("secondaryStatus",lstSecondaryStatus);
			jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterion_jobCategory,criterion_secondaryStatus);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
		
	}
	//***************************************************TPL 2024 (Internal And External Candidate) **************************************//
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> findStatusPrivilegeSchoolByHeadBranchAndDistrict(JobOrder jobOrder)
	{
		List<JobCategoryWiseStatusPrivilege> lstStatusPForSchool= new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			criteria.add(Restrictions.eq("jobCategoryMaster", jobOrder.getJobCategoryMaster()));
			lstStatusPForSchool = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstStatusPForSchool;
	}
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getStatusByHBD(JobOrder jobOrder,SecondaryStatus secondaryStatus)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			criteria.add(Restrictions.eq("jobCategoryMaster", jobOrder.getJobCategoryMaster()));
			criteria.add(Restrictions.eq("secondaryStatus",secondaryStatus));
			jobCategoryWiseStatusPrivileges = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
	}
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getFilterStatusByHBD(JobOrder jobOrder,SecondaryStatus secondaryStatus)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()==null && jobOrder.getDistrictMaster()!=null)
				criteria.add(Restrictions.eq("jobCategoryMaster", jobOrder.getJobCategoryMaster()));
			else
				criteria.add(Restrictions.eq("jobCategoryMaster", jobOrder.getJobCategoryMaster().getParentJobCategoryId()));
			
			criteria.add(Restrictions.eq("secondaryStatus",secondaryStatus));
			
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			Criterion criterion_Date1= Restrictions.eq("statusUpdateExpiryDate",d2);
			Criterion criterion_Date2= Restrictions.ge("statusUpdateExpiryDate",d2);
			Criterion criterion_Date3= Restrictions.or(criterion_Date1,criterion_Date2);
			Criterion criterion_Date4= Restrictions.isNull("statusUpdateExpiryDate");
			Criterion criterion_Date= Restrictions.or(criterion_Date3,criterion_Date4);
			criteria.add(criterion_Date);
			jobCategoryWiseStatusPrivileges = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
	}
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getMaxJSIValueListBYHBD(SecondaryStatus secondaryStatus)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges= new ArrayList<JobCategoryWiseStatusPrivilege>();
		try{
			Criterion criterionStatus=null;
			if(secondaryStatus.getStatusMaster()!=null){
				criterionStatus = Restrictions.eq("statusMaster", secondaryStatus.getStatusMaster());
			}else{
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			}
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(secondaryStatus.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",secondaryStatus.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(secondaryStatus.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",secondaryStatus.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(secondaryStatus.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",secondaryStatus.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			criteria.add(Restrictions.eq("jobCategoryMaster", secondaryStatus.getJobCategoryMaster()));
			criteria.add(Restrictions.ge("maxValuePerJSIQuestion",1));
			criteria.add(criterionStatus);
			jobCategoryWiseStatusPrivileges = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
		
	}
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getHeadquarterFilterStatusByName(HeadQuarterMaster headQuarterMaster, JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			Criterion criterion_headquarter= Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			criteria.createCriteria("secondaryStatus").add(Restrictions.eq("secondaryStatusName", secondaryStatus.getSecondaryStatusName())).add(Restrictions.eq("status","A"));
			Criterion criterion_Date1= Restrictions.eq("statusUpdateExpiryDate",d2);
			Criterion criterion_Date2= Restrictions.ge("statusUpdateExpiryDate",d2);
			Criterion criterion_Date3= Restrictions.or(criterion_Date1,criterion_Date2);
			Criterion criterion_Date4= Restrictions.isNull("statusUpdateExpiryDate");
			Criterion criterion_Date= Restrictions.or(criterion_Date3,criterion_Date4);
			criteria.add(criterion_headquarter);
			criteria.add(criterion_jobCategory);
			criteria.add(criterion_Date);
			jobCategoryWiseStatusPrivileges=criteria.list();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
	}
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> findstatusForFirstTimeJobApply(JobOrder jobOrder)
	{
		//System.out.println(":::::::::::::::: District Id :::::::"+jobOrder.getDistrictMaster().getDistrictId());
		//System.out.println(":::::::::::::::: Job Category Id :::::::"+jobOrder.getJobCategoryMaster().getJobCategoryId());
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			Criterion criterion_district= Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
			Criterion criterion_Auto= Restrictions.eq("statusForFirstTimeJobApply",1);
			jobCategoryWiseStatusPrivileges=findByCriteria(criterion_Auto,criterion_district,criterion_jobCategory);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
	}
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getStatusForFinalizeEvent(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus,StatusMaster statusMaster)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{	//statusMaster
			if(statusMaster!=null || secondaryStatus!=null){
				Criterion criterion_secondaryStatus=null;
				Criterion criterion_district= Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion_jobCategory= null;
				/*if(jobCategoryMaster!=null && jobCategoryMaster.getParentJobCategoryId()!=null){
					criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobCategoryMaster.getParentJobCategoryId());
				}else*/
					criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
				
				
				if(statusMaster!=null)
				 criterion_secondaryStatus= Restrictions.eq("statusMaster",statusMaster);
				else
					criterion_secondaryStatus= Restrictions.eq("secondaryStatus",secondaryStatus);
				jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterion_jobCategory,criterion_secondaryStatus);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
		
	}

	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getStatus_SLC_OP(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus,StatusMaster statusMaster)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			Criterion criterion_district= Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			//Criterion criterion_secondaryStatus= Restrictions.eq("secondaryStatus",secondaryStatus);
			
			Criterion criterion_secondaryStatus=null;
			if(statusMaster!=null){
				criterion_secondaryStatus = Restrictions.eq("statusMaster", statusMaster);
			}else{
				criterion_secondaryStatus= Restrictions.eq("secondaryStatus",secondaryStatus);
			}
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			criteria.add(criterion_district);
			criteria.add(criterion_jobCategory);
			criteria.add(criterion_secondaryStatus);
			
			criteria.createAlias("secondaryStatus", "sstatus",criteria.LEFT_JOIN).createAlias("statusMaster", "statusmaster",criteria.LEFT_JOIN);
			criteria.setProjection(Projections.projectionList()
					.add(Projections.property("jobCategoryStatusPrivilegeId"))
					.add(Projections.property("autoUpdateStatus"))
					.add(Projections.property("updateStatusOption"))					
					.add(Projections.property("maxValuePerJSIQuestion"))
					.add(Projections.property("instructionAttachFileName"))
					.add(Projections.property("sstatus.secondaryStatusId"))
					.add(Projections.property("sstatus.secondaryStatusName"))
					.add(Projections.property("statusmaster.statusId"))
					.add(Projections.property("statusmaster.status"))
					.add(Projections.property("autoNotifyAll"))
					);
			
			List<String[]> jobCategoryWiseStatusPrivi = new ArrayList<String[]>();
			jobCategoryWiseStatusPrivi = criteria.list();
			
			for(Iterator it = jobCategoryWiseStatusPrivi.iterator(); it.hasNext();){
				Object[] row = (Object[]) it.next(); 
				if(row[0]!=null){
					JobCategoryWiseStatusPrivilege jcwsp = new JobCategoryWiseStatusPrivilege();
					jcwsp.setJobCategoryStatusPrivilegeId(Integer.parseInt(row[0].toString()));
					
					if(row[1]!=null)
						jcwsp.setAutoUpdateStatus(Boolean.parseBoolean(row[1].toString()));
					if(row[2]!=null)
						jcwsp.setUpdateStatusOption(Integer.parseInt(row[2].toString()));
					
					if(row[3]!=null)
						jcwsp.setMaxValuePerJSIQuestion(Double.parseDouble(row[3].toString()));
					
					if(row[4]!=null)
						jcwsp.setInstructionAttachFileName(row[4].toString());
					
					if(row[5]!=null){
						SecondaryStatus secStatus = new SecondaryStatus();
						secStatus.setSecondaryStatusId(Integer.parseInt(row[5].toString()));
						secStatus.setSecondaryStatusName(row[6].toString());
						jcwsp.setSecondaryStatus(secStatus);
					}
					
					if(row[7]!=null){
						StatusMaster stMaster = new StatusMaster();
						stMaster.setStatusId(Integer.parseInt(row[7].toString()));
						stMaster.setStatus(row[8].toString());
						jcwsp.setStatusMaster(stMaster);
					}
					if(row[9]!=null){
						jcwsp.setAutoNotifyAll(Boolean.parseBoolean(row[9].toString()));
					}
					jobCategoryWiseStatusPrivileges.add(jcwsp);
				}
			}
			//jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterion_jobCategory,criterion_secondaryStatus);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
		
	}
	
	
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getFilterStatusByName_SLC_Op(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus)
	{
		
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			Criterion criterion_district= Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			
			//Criterion criterion_secondaryStatus= Restrictions.eq("secondaryStatus.secondaryStatusName",secondaryStatus.getSecondaryStatusName());
			
			criteria.createCriteria("secondaryStatus").add(Restrictions.eq("secondaryStatusName", secondaryStatus.getSecondaryStatusName())).add(Restrictions.eq("status","A"));
			
			
			Criterion criterion_Date1= Restrictions.eq("statusUpdateExpiryDate",d2);
			Criterion criterion_Date2= Restrictions.ge("statusUpdateExpiryDate",d2);
			Criterion criterion_Date3= Restrictions.or(criterion_Date1,criterion_Date2);
			Criterion criterion_Date4= Restrictions.isNull("statusUpdateExpiryDate");
			Criterion criterion_Date= Restrictions.or(criterion_Date3,criterion_Date4);
			
			criteria.add(criterion_district);
			criteria.add(criterion_jobCategory);
			criteria.add(criterion_Date);
			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.property("jobCategoryStatusPrivilegeId"))
					.add(Projections.property("autoUpdateStatus"))
					.add(Projections.property("updateStatusOption"))
					.add(Projections.property("autoNotifyAll"))
					);
			
			List<String[]> jobCategoryWiseStatusPrivi = new ArrayList<String[]>();
			jobCategoryWiseStatusPrivi = criteria.list();
			
			for(Iterator it = jobCategoryWiseStatusPrivi.iterator(); it.hasNext();){
				Object[] row = (Object[]) it.next(); 
				if(row[0]!=null){
					JobCategoryWiseStatusPrivilege jcwsp = new JobCategoryWiseStatusPrivilege();
					if(row[1]!=null)
						jcwsp.setAutoUpdateStatus(Boolean.parseBoolean(row[1].toString()));
					if(row[2]!=null)
						jcwsp.setUpdateStatusOption(Integer.parseInt(row[2].toString()));
					if(row[3]!=null)
						jcwsp.setAutoNotifyAll(Boolean.parseBoolean(row[3].toString()));
						
					jobCategoryWiseStatusPrivileges.add(jcwsp);
				}
			}
			
			//jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterion_jobCategory,criterion_secondaryStatus,criterion_Date);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
	}
	
	@Transactional
	public List<JobCategoryWiseStatusPrivilege> getAllSupportUserIn(JobOrder jobOrder, Integer jobCategoryMasters)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges= new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion=Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
			Criterion criterion2=Restrictions.isNotNull("emailNotificationToSelectedDistrictAdmins");
			Criterion criterion3=Restrictions.eq("jobCategoryMaster.jobCategoryId", jobCategoryMasters);
			criteria.add(criterion);
			criteria.add(criterion2);
			criteria.add(criterion3);
			//criteria.setProjection(Projections.property("emailNotificationToSelectedDistrictAdmins"));
			jobCategoryWiseStatusPrivileges=criteria.list();
			System.out.println(":::::::::::::::::::::::::::::::   size  "+jobCategoryWiseStatusPrivileges.size());
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
		
	}
	//nadeem
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getFilterStatusWithDPointForHQA(HeadQuarterMaster headQuarterMaster, JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus,StatusMaster statusMaster)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			}else{
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			}
			Criterion criterion_district= Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryMaster",jobCategoryMaster.getParentJobCategoryId());
			Criterion criterion_Date1= Restrictions.eq("statusUpdateExpiryDate",d2);
			Criterion criterion_Date2= Restrictions.ge("statusUpdateExpiryDate",d2);
			Criterion criterion_Date3= Restrictions.or(criterion_Date1,criterion_Date2);
			Criterion criterion_Date4= Restrictions.isNull("statusUpdateExpiryDate");
			Criterion criterion_Date= Restrictions.or(criterion_Date3,criterion_Date4);
			jobCategoryWiseStatusPrivileges=findByCriteria(criterion_district,criterion_jobCategory,criterionStatus,criterion_Date);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
		
	}
	//nadeem
	@Transactional(readOnly=false)
	public List<JobCategoryWiseStatusPrivilege> getFilterStatusForHQA(JobOrder jobOrder,SecondaryStatus secondaryStatus)
	{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
		    if(jobOrder.getBranchMaster()!=null){
				//criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()==null && jobOrder.getDistrictMaster()!=null)
				criteria.add(Restrictions.eq("jobCategoryMaster", jobOrder.getJobCategoryMaster()));
			else
				criteria.add(Restrictions.eq("jobCategoryMaster", jobOrder.getJobCategoryMaster().getParentJobCategoryId()));
			criteria.add(Restrictions.eq("secondaryStatus",secondaryStatus));
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			Criterion criterion_Date1= Restrictions.eq("statusUpdateExpiryDate",d2);
			Criterion criterion_Date2= Restrictions.ge("statusUpdateExpiryDate",d2);
			Criterion criterion_Date3= Restrictions.or(criterion_Date1,criterion_Date2);
			Criterion criterion_Date4= Restrictions.isNull("statusUpdateExpiryDate");
			Criterion criterion_Date= Restrictions.or(criterion_Date3,criterion_Date4);
			criteria.add(criterion_Date);
			jobCategoryWiseStatusPrivileges = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobCategoryWiseStatusPrivileges;
	}
}

