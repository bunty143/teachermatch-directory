package tm.dao.cgreport;

import tm.bean.cgreport.JobWiseConsolidatedTeacherScoreHistory;
import tm.dao.generic.GenericHibernateDAO;

public class JobWiseConsolidatedTeacherScoreHistoryDAO extends GenericHibernateDAO<JobWiseConsolidatedTeacherScoreHistory, Integer>
{
	public JobWiseConsolidatedTeacherScoreHistoryDAO(){
		super(JobWiseConsolidatedTeacherScoreHistory.class);
	}

}
