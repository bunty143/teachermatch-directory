package tm.dao.cgreport;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.cgreport.TeacherStatusScores;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.controller.school.SchoolManageController;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherStatusNotesDAO extends GenericHibernateDAO<TeacherStatusNotes, Integer>{
	public TeacherStatusNotesDAO(){
		super(TeacherStatusNotes.class);
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusNotes> getStatusNoteList(TeacherDetail teacherDetail,JobOrder jobOrder,Order order,int startPos,int limit)
	{
		List<TeacherStatusNotes> lstTeacherStatusDetails = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			if(order != null){
				criteria.addOrder(order);
			}
			
			lstTeacherStatusDetails = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusDetails;
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusNotes> getStatusNoteAllList(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			lstTeacherStatusDetails = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusDetails;
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusNotes> getFinalizeStatusScore(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		List<TeacherStatusNotes> lstTeacherStatusNotes = null;
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			}else{
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			}
			
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion4 = Restrictions.eq("finalizeStatus", true);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion4);
			criteria.add(criterionStatus);
			lstTeacherStatusNotes = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusNotes;
	}
	
	
	@Transactional(readOnly=true)
	public List<TeacherStatusNotes> getFinalizeNotes(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster)
	{
		List<TeacherStatusNotes> lstTeacherStatusNotes = new ArrayList<TeacherStatusNotes>();
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null)
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			else
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("finalizeStatus", true);
			Criterion criterion4 = Restrictions.eq("userMaster", userMaster);
			lstTeacherStatusNotes=findByCriteria(criterionStatus,criterion1,criterion2,criterion3,criterion4);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusNotes;
	}
	
	
	@Transactional(readOnly=true)
	public List<TeacherStatusNotes> getFinalizeStatusNote(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("finalizeStatus", true);
			lstTeacherStatusDetails =findByCriteria(criterion1,criterion2,criterion3);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusDetails;
	}
	
	
	
	
	@Transactional(readOnly=true)
	public TeacherStatusNotes getQuestionNoteObj(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtMaster,Long teacherAssessmentQuestionId)
	{
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("districtId", districtMaster.getDistrictId());
			Criterion criterion4 = Restrictions.eq("teacherAssessmentQuestionId",teacherAssessmentQuestionId);
			lstTeacherStatusDetails =findByCriteria(criterion1,criterion2,criterion3,criterion4);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherStatusDetails!=null && lstTeacherStatusDetails.size()>0)
			return lstTeacherStatusDetails.get(0);
		else
			return null;
	}
	@Transactional(readOnly=true)
	public TeacherStatusNotes getQuestionNoteObjByUserMaster(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtMaster,Long teacherAssessmentQuestionId,UserMaster userMaster)
	{
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("districtId", districtMaster.getDistrictId());
			Criterion criterion4 = Restrictions.eq("teacherAssessmentQuestionId",teacherAssessmentQuestionId);
			Criterion criterion5=Restrictions.eq("userMaster",userMaster );
			lstTeacherStatusDetails =findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherStatusDetails!=null && lstTeacherStatusDetails.size()>0)
			return lstTeacherStatusDetails.get(0);
		else
			return null;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusNotes> getQuestionNoteObjBySuperUserMaster(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtMaster,Long teacherAssessmentQuestionId,UserMaster userMaster)
	{
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("districtId", districtMaster.getDistrictId());
			Criterion criterion4 = Restrictions.eq("teacherAssessmentQuestionId",teacherAssessmentQuestionId);			
			Criterion criterion5=Restrictions.eq("userMaster",userMaster );
			Criterion criterion6=Restrictions.isNotNull("statusNotes");
			
			if(userMaster!=null)
			lstTeacherStatusDetails =findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);
			else
			lstTeacherStatusDetails =findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion6);	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherStatusDetails!=null && lstTeacherStatusDetails.size()>0)
			return lstTeacherStatusDetails;
		else
			return null;
	}
	
	@Transactional(readOnly=true)
	public TeacherStatusNotes  chkQuestionNotesStatus(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtMaster,List<Long> teacherAssessmentQuestionId)
	{
		
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		try 
		{	if(teacherAssessmentQuestionId.size()>0){
				Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.eq("districtId", districtMaster.getDistrictId());
				Criterion criterion4 = Restrictions.in("teacherAssessmentQuestionId",teacherAssessmentQuestionId);
				lstTeacherStatusDetails =findByCriteria(criterion1,criterion2,criterion3,criterion4);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherStatusDetails!=null && lstTeacherStatusDetails.size()>0)
			return lstTeacherStatusDetails.get(0);
		else
			return null;
	}
	@Transactional(readOnly=true)
	public TeacherStatusNotes  chkQuestionNotesStatusByUserMaster(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtMaster,List<Long> teacherAssessmentQuestionId,UserMaster userMaster)
	{
		
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		try 
		{	if(teacherAssessmentQuestionId.size()>0){
				Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.eq("districtId", districtMaster.getDistrictId());
				Criterion criterion4 = Restrictions.in("teacherAssessmentQuestionId",teacherAssessmentQuestionId);
				Criterion criterion5=Restrictions.eq("userMaster",userMaster );
				lstTeacherStatusDetails =findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherStatusDetails!=null && lstTeacherStatusDetails.size()>0)
			return lstTeacherStatusDetails.get(0);
		else
			return null;
	}
	
	
	@Transactional(readOnly=true)
	public List<TeacherStatusNotes> getTeacheStatusNotesList_CGMass(List<TeacherDetail> lstTeacherDetails,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster)
	{
		List<TeacherStatusNotes> lstTeacherStatusNotes = new ArrayList<TeacherStatusNotes>();
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null)
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			else
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			
			Criterion criterion1 = Restrictions.in("teacherDetail", lstTeacherDetails);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion4 = Restrictions.eq("userMaster", userMaster);
			Criterion criterion5 = Restrictions.isNotNull("teacherAssessmentQuestionId");
			
			lstTeacherStatusNotes=findByCriteria(criterionStatus,criterion1,criterion2,criterion4,criterion5);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusNotes;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusNotes> getFinalizeTSNList_CGMass(List<TeacherDetail> lstTeacherDetails,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster)
	{
		List<TeacherStatusNotes> lstTeacherStatusNotes = new ArrayList<TeacherStatusNotes>();
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null)
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			else
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			
			Criterion criterion1 = Restrictions.in("teacherDetail", lstTeacherDetails);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion4 = Restrictions.eq("userMaster", userMaster);
			Criterion criterion5 = Restrictions.isNotNull("teacherAssessmentQuestionId");
			Criterion criterion6 = Restrictions.eq("finalizeStatus", true);
			lstTeacherStatusNotes=findByCriteria(criterionStatus,criterion1,criterion2,criterion4,criterion5,criterion6);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusNotes;
	}
	
	public boolean deleteTSN(List<TeacherStatusNotes> lstTeacherStatusNotes)
	{
		boolean bReturnValue=true;
		try {
			if(lstTeacherStatusNotes!=null && lstTeacherStatusNotes.size()>0)
			{
				int i=0;
				for(TeacherStatusNotes teacherStatusNotes:lstTeacherStatusNotes)
				{
					makeTransient(teacherStatusNotes);
					i++;
					if(i%3==0)
					{
						flush();
						clear();
					}
				}
				
			}
		} catch (Exception e) {
			bReturnValue=false;
		}
		finally
		{
			return bReturnValue;
		}
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusNotes> getStatusNoteAllListByTeacherAndJOb(List<TeacherDetail> teacherDetails,List<JobOrder> jobOrders)
	{
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetails);
			Criterion criterion2 = Restrictions.in("jobOrder", jobOrders);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			lstTeacherStatusDetails = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusDetails;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusNotes> getStatusNoteByJobOrTeacher(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			if(jobOrder!=null){
				Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
				criteria.add(criterion2);
			}
			lstTeacherStatusDetails = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusDetails;
	}
	
	@Transactional(readOnly=false)	
	public List<TeacherStatusNotes> findJobByTeachersWithDistrict(TeacherDetail teacherDetail,JobOrder jobOrder) 
	{	
		List<TeacherStatusNotes> teacherList = new ArrayList<TeacherStatusNotes>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			criteria.addOrder(Order.desc("createdDateTime"));
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null)
				criteria.createCriteria("jobOrder").addOrder(Order.desc("createdDateTime")).add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			else
				criteria.createCriteria("jobOrder").addOrder(Order.desc("createdDateTime")).add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			
			teacherList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
	@Transactional(readOnly=true)
	public TeacherStatusNotes getFinalizeNotesByOrder(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster)
	{
		List<TeacherStatusNotes> lstTeacherStatusNotes = new ArrayList<TeacherStatusNotes>();
		TeacherStatusNotes teacherStatusNotes=null;
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null)
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			else
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("finalizeStatus", true);
			Criterion criterion4 = Restrictions.eq("userMaster", userMaster);
			lstTeacherStatusNotes=findByCriteria(Order.desc("createdDateTime"),criterionStatus,criterion1,criterion2,criterion3,criterion4);
			if(lstTeacherStatusNotes.size()>0){
				teacherStatusNotes=lstTeacherStatusNotes.get(0);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherStatusNotes;
	}
	
	@Transactional(readOnly=false)	
	public List<TeacherStatusNotes> getTSNDelta(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster,List<JobOrder> lstJobOrder) 
	{	
		List<TeacherStatusNotes> teacherList = new ArrayList<TeacherStatusNotes>();
		if(teacherDetails!=null && teacherDetails.size()>0)
		{
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				criteria.add(criterion1);
				criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster",districtMaster));
				if(lstJobOrder!=null && lstJobOrder.size()>0)
				{
					Criterion criterion6 = Restrictions.in("jobOrder",lstJobOrder);
					criteria.add(criterion6);
				}
				teacherList = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}	
		}
			
		return teacherList;
	}
	@Transactional(readOnly=true)
	public TeacherStatusNotes  chkQuestionNotesStatusByUserMasterAndHBD(TeacherDetail teacherDetail,JobOrder jobOrder,List<Long> teacherAssessmentQuestionId,UserMaster userMaster)
	{
		
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		try{	
			if(teacherAssessmentQuestionId.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(jobOrder.getDistrictMaster()!=null){
					criteria.add(Restrictions.eq("districtId",jobOrder.getDistrictMaster().getDistrictId()));
				}else{
					criteria.add(Restrictions.isNull("districtId"));
				}
				/*if(jobOrder.getBranchMaster()!=null){
					criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
				}else{
					criteria.add(Restrictions.isNull("branchMaster"));
				}
				if(jobOrder.getHeadQuarterMaster()!=null){
					criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
				}else{
					criteria.add(Restrictions.isNull("headQuarterMaster"));
				}*/
				criteria.add(Restrictions.eq("jobOrder",jobOrder));
				criteria.add(Restrictions.eq("teacherDetail",teacherDetail));
				criteria.add(Restrictions.eq("userMaster",userMaster));
				criteria.add(Restrictions.in("teacherAssessmentQuestionId",teacherAssessmentQuestionId));
				lstTeacherStatusDetails = criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherStatusDetails!=null && lstTeacherStatusDetails.size()>0)
			return lstTeacherStatusDetails.get(0);
		else
			return null;
	}
	@Transactional(readOnly=true)
	public TeacherStatusNotes getQuestionNoteObjByUserMasterAndHBD(TeacherDetail teacherDetail,JobOrder jobOrder,Long teacherAssessmentQuestionId,UserMaster userMaster)
	{
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtId",jobOrder.getDistrictMaster().getDistrictId()));
			}else{
				criteria.add(Restrictions.isNull("districtId"));
			}
			/*if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}*/
			criteria.add(Restrictions.eq("jobOrder",jobOrder));
			criteria.add(Restrictions.eq("teacherDetail",teacherDetail));
			criteria.add(Restrictions.eq("userMaster",userMaster));
			criteria.add(Restrictions.eq("teacherAssessmentQuestionId",teacherAssessmentQuestionId));
			lstTeacherStatusDetails = criteria.list();
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherStatusDetails!=null && lstTeacherStatusDetails.size()>0)
			return lstTeacherStatusDetails.get(0);
		else
			return null;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusNotes> getFinalizeStatusNoteOp(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		try 
		{
			List<String []> tempTeacherNotes=new ArrayList<String []>();
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass(),"tsn");
			criteria.add(Restrictions.eq("teacherDetail", teacherDetail)).add(Restrictions.eq("jobOrder", jobOrder)).add(Restrictions.eq("tsn.finalizeStatus", true));
			criteria.createAlias("secondaryStatus", "secondaryStatus",Criteria.LEFT_JOIN).createAlias("statusMaster", "statusmaster",Criteria.LEFT_JOIN)
			.createAlias("userMaster", "usermaster",Criteria.LEFT_JOIN).createAlias("usermaster.schoolId", "schoolId",Criteria.LEFT_JOIN)
			.setProjection(Projections.projectionList()		    
		    		.add( Projections.property("usermaster.userId"), "userId" )
		    		.add( Projections.property("usermaster.entityType"), "entityType" )
		    		.add( Projections.property("schoolId.schoolId"), "schoolId" )
		    		.add( Projections.property("finalizeStatus"), "finalizeStatus" )
		    		.add( Projections.property("secondaryStatus.secondaryStatusId"), "secondaryStatusId" )
		    		.add( Projections.property("statusmaster.statusId"), "statusId" )
		    		);
			long a = System.currentTimeMillis();
			//criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			tempTeacherNotes= criteria.list();	
			long b = System.currentTimeMillis();
			System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + tempTeacherNotes.size() );
			
		    		
		    		for (Iterator it = tempTeacherNotes.iterator(); it.hasNext();)
		    		{
						TeacherStatusNotes teacherStatusNotes = new TeacherStatusNotes();
						Object[] row = (Object[]) it.next();
						if(row[0]!=null){
							UserMaster userMaster = new UserMaster();
							userMaster.setUserId(Integer.parseInt(row[0].toString()));
							userMaster.setEntityType(Integer.parseInt(row[1].toString()));
							if(row[2]!=null){
								SchoolMaster schoolMaster = new SchoolMaster();
								schoolMaster.setSchoolId(Long.parseLong(row[2].toString()));
								userMaster.setSchoolId(schoolMaster);	
							}
							teacherStatusNotes.setUserMaster(userMaster);
							if(row[3]!=null)
							teacherStatusNotes.setFinalizeStatus(Boolean.parseBoolean(row[3].toString()));
							
							if(row[4]!=null){
								SecondaryStatus secondaryStatus = new SecondaryStatus();
								secondaryStatus.setSecondaryStatusId(Integer.parseInt(row[4].toString()));
								teacherStatusNotes.setSecondaryStatus(secondaryStatus);
							}											
							if(row[5]!=null){
								StatusMaster statusMaster = new StatusMaster();
								statusMaster.setStatusId(Integer.parseInt(row[5].toString()));
								teacherStatusNotes.setStatusMaster(statusMaster);
							}
							teacherStatusNotes.setTeacherDetail(teacherDetail);
							teacherStatusNotes.setJobOrder(jobOrder);
							lstTeacherStatusDetails.add(teacherStatusNotes);
						}
		    		}
			
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusDetails;
	}
	
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<TeacherStatusNotes> findWithLimit_Op_or_withoutLimit_op(Order order,int startPos,int limit,Criterion... criterion) {
		
		List<TeacherStatusNotes> lstTeacherStatusDetails = new ArrayList<TeacherStatusNotes>();
		
		Session session = getSession();
		Criteria crit = session.createCriteria(getPersistentClass(),"tsn");
		
		for (Criterion c : criterion) {
			crit.add(c);
		}
		if(startPos==0 && limit==0){
			/*crit.setFirstResult(startPos);			
			crit.setMaxResults(limit);*/
		}else{
			crit.setFirstResult(startPos);			
			crit.setMaxResults(limit);
		}

		crit.createAlias("secondaryStatus", "secondaryStatus",Criteria.LEFT_JOIN).createAlias("statusMaster", "statusmaster",Criteria.LEFT_JOIN)
		.createAlias("userMaster", "usermaster",Criteria.LEFT_JOIN).createAlias("usermaster.schoolId", "schoolId",Criteria.LEFT_JOIN)
		.setProjection(Projections.projectionList()		    
	    		.add( Projections.property("usermaster.userId"), "userId" )
	    		.add( Projections.property("usermaster.entityType"), "entityType" )
	    		.add( Projections.property("schoolId.schoolId"), "schoolId" )
	    		.add( Projections.property("finalizeStatus"), "finalizeStatus" )
	    		.add( Projections.property("secondaryStatus.secondaryStatusId"), "secondaryStatusId" )
	    		.add( Projections.property("statusmaster.statusId"), "statusId" )
	    		//new fields
	    		.add( Projections.property("statusNotes"))//6
	    		.add( Projections.property("statusNoteFileName"))//7
	    		.add( Projections.property("createdDateTime"))//8
	    		.add( Projections.property("teacherStatusNoteId"))//9
	    		.add( Projections.property("usermaster.firstName"))//10
	    		.add( Projections.property("usermaster.lastName"))//11
	    		.add( Projections.property("usermaster.middleName"))//12
	    		);
		
		if(order != null)
			crit.addOrder(order);
		
		List<String []> tempTeacherNotes=new ArrayList<String []>();
		tempTeacherNotes= crit.list();

		for (Iterator it = tempTeacherNotes.iterator(); it.hasNext();)
		{
			TeacherStatusNotes teacherStatusNotes = new TeacherStatusNotes();
			Object[] row = (Object[]) it.next();
			if(row[9]!=null){
				teacherStatusNotes.setTeacherStatusNoteId(Integer.parseInt(row[9].toString()));
				UserMaster userMaster = new UserMaster();
				userMaster.setUserId(Integer.parseInt(row[0].toString()));
				userMaster.setEntityType(Integer.parseInt(row[1].toString()));
				userMaster.setFirstName(row[10].toString());
				userMaster.setLastName(row[11].toString());
				if(row[12]!=null){
					userMaster.setMiddleName(row[12].toString());
				}
				
				if(row[2]!=null){
					SchoolMaster schoolMaster = new SchoolMaster();
					schoolMaster.setSchoolId(Long.parseLong(row[2].toString()));
					userMaster.setSchoolId(schoolMaster);	
				}
				teacherStatusNotes.setUserMaster(userMaster);
				if(row[3]!=null)
				teacherStatusNotes.setFinalizeStatus(Boolean.parseBoolean(row[3].toString()));
				
				if(row[4]!=null){
					SecondaryStatus secondaryStatus = new SecondaryStatus();
					secondaryStatus.setSecondaryStatusId(Integer.parseInt(row[4].toString()));
					teacherStatusNotes.setSecondaryStatus(secondaryStatus);
				}											
				if(row[5]!=null){
					StatusMaster statusMaster = new StatusMaster();
					statusMaster.setStatusId(Integer.parseInt(row[5].toString()));
					teacherStatusNotes.setStatusMaster(statusMaster);
				}
				if(row[6]!=null){
					teacherStatusNotes.setStatusNotes(row[6].toString());
				}
				if(row[7]!=null){
					teacherStatusNotes.setStatusNoteFileName(row[7].toString());
				}				
				if(row[8]!=null){
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					Date newDate = null;
					try {
						newDate = df.parse(row[8].toString());
					} catch (ParseException e) {}
					
					teacherStatusNotes.setCreatedDateTime(newDate);
				}
				/*teacherStatusNotes.setTeacherDetail(teacherDetail);
				teacherStatusNotes.setJobOrder(jobOrder);*/
				lstTeacherStatusDetails.add(teacherStatusNotes);
			}
		}
		
		return lstTeacherStatusDetails;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusNotes> getFinalizeStatusScoreOp(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		List<TeacherStatusNotes> lstTeacherStatusScores = null;
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			}else{
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			}
			
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion4 = Restrictions.eq("finalizeStatus", true);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion4);
			criteria.add(criterionStatus).createAlias("userMaster", "usermaster").setProjection(
					Projections.projectionList()
					.add(Projections.property("usermaster.userId"))
					.add(Projections.property("usermaster.firstName"))
					.add(Projections.property("usermaster.lastName"))
					.add(Projections.property("usermaster.middleName"))
			);
			List<Object[]> ts = new ArrayList<Object[]>();
			ts = criteria.list();
			if(ts.size()>0){
				lstTeacherStatusScores = new ArrayList<TeacherStatusNotes>();
			for(Iterator it = ts.iterator(); it.hasNext();){
				
				Object[] row = (Object[]) it.next(); 
				if(row[0]!=null){
					TeacherStatusNotes teacherStSc = new TeacherStatusNotes();
					UserMaster umm = new UserMaster();
					umm.setUserId(Integer.parseInt(row[0].toString()));
					
					umm.setFirstName(row[1].toString());
					umm.setLastName(row[2].toString());
					if(row[3]!=null){
						umm.setMiddleName(row[3].toString());
					}
					
					teacherStSc.setUserMaster(umm);
					lstTeacherStatusScores.add(teacherStSc);
				}
			}
			}
			//lstTeacherStatusScores = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusScores;
	}
}
