package tm.dao.cgreport;

import java.util.Calendar;




import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.cgreport.DistrictMaxFitScore;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictMaxFitScoreDAO extends GenericHibernateDAO<DistrictMaxFitScore, Integer>{
	public DistrictMaxFitScoreDAO(){
		super(DistrictMaxFitScore.class);
	}
	
	@Transactional(readOnly=false)
	public List<DistrictMaxFitScore> getFitStatusScore(DistrictMaster  districtMaster)
	{	
		List<DistrictMaxFitScore> districtMaxFitScoreList= new ArrayList<DistrictMaxFitScore>();	
		try{
				Criterion criterion2=	Restrictions.eq("status","A");
				Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
				districtMaxFitScoreList = findByCriteria(criterion,criterion2);				
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return districtMaxFitScoreList;
	}
	@Transactional(readOnly=false)
	public List<DistrictMaxFitScore> getStatusMasterList(DistrictMaster  districtMaster)
	{	
		List<DistrictMaxFitScore> districtMaxFitScoreList= new ArrayList<DistrictMaxFitScore>();	
		try{
				Criterion criterion2=	Restrictions.eq("status","A");
				Criterion criterion1=	Restrictions.isNotNull("statusMaster");
				Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
				districtMaxFitScoreList = findByCriteria(criterion,criterion1,criterion2);				
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return districtMaxFitScoreList;
	}
	@Transactional(readOnly=false)
	public List<DistrictMaxFitScore> getSecStatusMasterList(DistrictMaster  districtMaster)
	{	
		List<DistrictMaxFitScore> districtMaxFitScoreList= new ArrayList<DistrictMaxFitScore>();	
		try{
				Criterion criterion2=	Restrictions.eq("status","A");
				Criterion criterion1=	Restrictions.isNotNull("secondaryStatus");
				Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
				districtMaxFitScoreList = findByCriteria(criterion,criterion1,criterion2);				
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return districtMaxFitScoreList;
	}
	
	
	@Transactional(readOnly=false)
	public List<DistrictMaxFitScore> getMaxFitScoreByDID_SM_SS_msu(DistrictMaster districtMaster,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{	
		List<DistrictMaxFitScore> districtMaxFitScoreList= new ArrayList<DistrictMaxFitScore>();	
		try
		{
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion1=	Restrictions.eq("status","A");
			
			Criterion criterionSM=null;
			Criterion criterionSS=null;
			
			if(statusMaster!=null && secondaryStatus==null)
			{
				criterionSM=Restrictions.eq("statusMaster", statusMaster);
				criterionSS=Restrictions.isNull("secondaryStatus");
				districtMaxFitScoreList = findByCriteria(criterion,criterion1,criterionSM,criterionSS);
			}
			else if(statusMaster==null && secondaryStatus!=null)
			{
				criterionSM=Restrictions.isNull("statusMaster");
				criterionSS=Restrictions.eq("secondaryStatus", secondaryStatus);
				districtMaxFitScoreList = findByCriteria(criterion,criterion1,criterionSM,criterionSS);
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return districtMaxFitScoreList;
	}
	@Transactional(readOnly=false)
	public List<DistrictMaxFitScore> getFitStatusScoreByHeadBranchAndDistrict(JobOrder jobOrder)
	{	
		List<DistrictMaxFitScore> districtMaxFitScoreList= new ArrayList<DistrictMaxFitScore>();	
		try{

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			criteria.add(Restrictions.eq("status","A"));
			
			
			districtMaxFitScoreList = criteria.list();
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return districtMaxFitScoreList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictMaxFitScore> getFitStatusScoreByHeadBranchAndDistrictOp(JobOrder jobOrder)
	{	
		List<DistrictMaxFitScore> districtMaxFitScoreList= new ArrayList<DistrictMaxFitScore>();	
		try{
			List<String []> tempDistrictScore=new ArrayList<String []>();
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.createAlias("secondaryStatus", "secondaryStatus",Criteria.LEFT_JOIN).createAlias("statusMaster", "statusmaster",Criteria.LEFT_JOIN)
			.setProjection(Projections.projectionList()
		    		.add( Projections.property("maxFitScore"), "maxFitScore" )
		    		.add( Projections.property("secondaryStatus.secondaryStatusId"), "secondaryStatusId" )
		    		.add( Projections.property("statusmaster.statusId"), "statusId" )
			);
			
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			criteria.add(Restrictions.eq("status","A"));
			tempDistrictScore = criteria.list();
			
			for (Iterator it = tempDistrictScore.iterator(); it.hasNext();)
			{
				JobForTeacher jobForTeacher = new JobForTeacher();
				Object[] row = (Object[]) it.next();
				DistrictMaxFitScore districtMaxFitScore = new DistrictMaxFitScore();
				if(row[0]!=null){
					districtMaxFitScore.setMaxFitScore(Double.parseDouble(row[0].toString()));					
					if(row[1]!=null){
						SecondaryStatus secondaryStatus = new SecondaryStatus();
						secondaryStatus.setSecondaryStatusId(Integer.parseInt(row[1].toString()));
						districtMaxFitScore.setSecondaryStatus(secondaryStatus);
					}											
					if(row[2]!=null){
						StatusMaster statusMaster = new StatusMaster();
						statusMaster.setStatusId(Integer.parseInt(row[2].toString()));
						districtMaxFitScore.setStatusMaster(statusMaster);
					}
						districtMaxFitScoreList.add(districtMaxFitScore);
				}
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return districtMaxFitScoreList;
	}
}
