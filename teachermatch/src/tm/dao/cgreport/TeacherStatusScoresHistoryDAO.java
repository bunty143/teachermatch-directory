package tm.dao.cgreport;


import tm.bean.cgreport.TeacherStatusScoresHistory;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherStatusScoresHistoryDAO extends GenericHibernateDAO<TeacherStatusScoresHistory, Integer>{
	public TeacherStatusScoresHistoryDAO(){
		super(TeacherStatusScoresHistory.class);
	}
	
	}
