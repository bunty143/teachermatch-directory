package tm.dao.cgreport;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.cgreport.RawDataForCompetency;
import tm.bean.cgreport.RawDataForDomain;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.dao.generic.GenericHibernateDAO;

public class RawDataForCompetencyDAO extends GenericHibernateDAO<RawDataForCompetency, Integer>{

	public RawDataForCompetencyDAO() {
		super(RawDataForCompetency.class);
	}
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional(readOnly=false)
	public List<RawDataForCompetency> findByCompetencyAndTeachers(CompetencyMaster competencyMaster, List<TeacherDetail> lstTeacherDetails)
	{
		List<RawDataForCompetency> rawDataForCompetencieList = new LinkedList<RawDataForCompetency>();
		try{		
			if(lstTeacherDetails!=null && lstTeacherDetails.size()>0){
				Calendar cal = Calendar.getInstance();
				cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
				Date sDate=cal.getTime();
				cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
				Date eDate=cal.getTime();
				
				Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
				Criterion criterion = Restrictions.eq("competencyMaster", competencyMaster);
				Criterion criterion2 = Restrictions.in("teacherDetail", lstTeacherDetails);
				rawDataForCompetencieList = findByCriteria(criterion,criterion1,criterion2);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return rawDataForCompetencieList;
	}
	
	@Transactional(readOnly=false)
	public List<RawDataForCompetency> findByCompetenciesAndTeacher(List<CompetencyMaster> competencyMasters,TeacherDetail teacherDetail)
	{
		List<RawDataForCompetency> rawDataForCompetencieList = new LinkedList<RawDataForCompetency>();
		try{		
			if(competencyMasters!=null && competencyMasters.size()>0){
				Calendar cal = Calendar.getInstance();
				cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
				Date sDate=cal.getTime();
				cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
				Date eDate=cal.getTime();
				
				Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
				Criterion criterion = Restrictions.in("competencyMaster", competencyMasters);
				Criterion criterion2 = Restrictions.eq("teacherDetail", teacherDetail);
				rawDataForCompetencieList = findByCriteria(criterion,criterion1,criterion2);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return rawDataForCompetencieList;
	}
	@Transactional(readOnly=false)
	public List<RawDataForCompetency> findByCompetenciesAndTeacher(List<CompetencyMaster> competencyMasters,List<TeacherAssessmentdetail> lstTeacherDetailsAssessmentdetails)
	{
		List<RawDataForCompetency> rawDataForCompetencieList = new LinkedList<RawDataForCompetency>();
		try{		
			if(competencyMasters!=null && competencyMasters.size()>0){
				
				Criterion criterion = Restrictions.in("competencyMaster", competencyMasters);
				Criterion criterion2 = Restrictions.in("teacherAssessmentdetail", lstTeacherDetailsAssessmentdetails);
				rawDataForCompetencieList = findByCriteria(criterion,criterion2);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return rawDataForCompetencieList;
	}
	@Transactional(readOnly=false)
	public List<RawDataForCompetency> findAllInCurrentYearByTeacher(List<TeacherDetail> teacherDetails, TeacherAssessmentdetail teacherAssessmentdetail)
	{
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		List<RawDataForCompetency> lstRawDataForCompetency = null;
		try {
			Criterion criterion2 = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion3 = Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail);
			criteria.add(criterion2);
			criteria.add(criterion3);
			lstRawDataForCompetency = criteria.list();

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return lstRawDataForCompetency;
	}
	@Transactional(readOnly=false)
	public List<RawDataForCompetency> findAllInCurrentYear()
	{
		List<RawDataForCompetency> lstRawDataForCompetency = null;
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
			lstRawDataForCompetency = findByCriteria(criterion1);			

		} catch (Exception e) {
			e.printStackTrace();
		}		
		return lstRawDataForCompetency;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param teacherDetailList
	 * @param assessmentDetail
	 * @param assessmentType
	 * @param assessmentTakenCount
	 * @return rawDataForCompetencyList
	 */
	@Transactional(readOnly=true)
	public List<RawDataForCompetency> getRawDataForCompetency(List<TeacherDetail> teacherDetailList,AssessmentDetail assessmentDetail,Integer assessmentType,Integer assessmentTakenCount)
	{
		List<RawDataForCompetency> rawDataForCompetencyList = null;
		try {
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetailList);
			Criterion criterion2 = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion3 = Restrictions.eq("assessmentType", assessmentType);
			Criterion criterion4 = Restrictions.eq("assessmentTakenCount", assessmentTakenCount);
			
			rawDataForCompetencyList = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rawDataForCompetencyList;
	}
	
	@Transactional(readOnly=false)
	public List<RawDataForCompetency> findByCompetencyTeacherAndTAD(CompetencyMaster competencyMaster, List<TeacherDetail> teacherDetailList, List<TeacherAssessmentdetail> teacherAssessmentdetailList)
	{
		List<RawDataForCompetency> rawDataForCompetencyList = new LinkedList<RawDataForCompetency>();
		try{		
			if(teacherDetailList!=null && teacherDetailList.size()>0){
				
				Criterion criterion1 = Restrictions.eq("competencyMaster", competencyMaster);
				Criterion criterion2 = Restrictions.in("teacherDetail", teacherDetailList);
				Criterion criterion3 = Restrictions.in("teacherAssessmentdetail", teacherAssessmentdetailList);
				rawDataForCompetencyList = findByCriteria(criterion1,criterion2,criterion3);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return rawDataForCompetencyList;
	}
}
