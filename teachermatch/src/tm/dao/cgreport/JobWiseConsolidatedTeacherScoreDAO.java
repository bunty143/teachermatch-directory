package tm.dao.cgreport;

import java.util.Calendar;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.master.DemoClassSchedule;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobWiseConsolidatedTeacherScoreDAO extends GenericHibernateDAO<JobWiseConsolidatedTeacherScore, Integer>{
	public JobWiseConsolidatedTeacherScoreDAO(){
		super(JobWiseConsolidatedTeacherScore.class);
	}

	@Transactional(readOnly=false)
	public List<JobWiseConsolidatedTeacherScore> getFitScore(List<TeacherDetail> teacherDetails, JobOrder jobOrder)
	{	
		List<JobWiseConsolidatedTeacherScore> jobWiseConsolidatedTeacherScoreList= new ArrayList<JobWiseConsolidatedTeacherScore>();	
		try{
			if(teacherDetails.size()>0)
			{
				Criterion criterion = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
				jobWiseConsolidatedTeacherScoreList = findByCriteria(criterion,criterion1);
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return jobWiseConsolidatedTeacherScoreList;
	}
	
	@Transactional(readOnly=false)
	public Map<Integer,String> getFitScoreTeacherWise(TeacherDetail teacherDetail, List<JobOrder> jobOrders)
	{	
		
		Map<Integer,String> scoremap = new HashMap<Integer, String>();
		try{
			if(jobOrders.size()>0)
			{
				List<JobWiseConsolidatedTeacherScore> jobWiseConsolidatedTeacherScoreList= new ArrayList<JobWiseConsolidatedTeacherScore>();
				Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion1 = Restrictions.in("jobOrder", jobOrders);
				jobWiseConsolidatedTeacherScoreList = findByCriteria(criterion,criterion1);
				
				String fitScore = null;
				for(JobWiseConsolidatedTeacherScore jobWiseConsolidatedTeacherScore:jobWiseConsolidatedTeacherScoreList){
					fitScore=jobWiseConsolidatedTeacherScore.getJobWiseConsolidatedScore()+"/"+jobWiseConsolidatedTeacherScore.getJobWiseMaxScore();
					scoremap.put(jobWiseConsolidatedTeacherScore.getJobOrder().getJobId(), fitScore);
				}
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return scoremap;
	}
	@Transactional(readOnly=false)
	public JobWiseConsolidatedTeacherScore getJWCTScore(TeacherDetail teacherDetail, JobOrder jobOrder)
	{	
		JobWiseConsolidatedTeacherScore jWScore=null;
		List<JobWiseConsolidatedTeacherScore> jobWiseConsolidatedTeacherScoreList= new ArrayList<JobWiseConsolidatedTeacherScore>();	
		try{
			if(teacherDetail!=null)
			{
				Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
				jobWiseConsolidatedTeacherScoreList = findByCriteria(criterion,criterion1);
				if(jobWiseConsolidatedTeacherScoreList.size()!=0){
					jWScore=jobWiseConsolidatedTeacherScoreList.get(0);
				}
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return jWScore;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeacherListByFitScore(String normScoreVal,String fitScoreSelectVal)
	{
		List<TeacherDetail> teacherList= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 =null;
			if(fitScoreSelectVal.equals("1")){
				criterion1=Restrictions.eq("jobWiseConsolidatedScore",Double.parseDouble(normScoreVal));
			}else if(fitScoreSelectVal.equals("2")){
				criterion1=Restrictions.lt("jobWiseConsolidatedScore",Double.parseDouble(normScoreVal));
			}else if(fitScoreSelectVal.equals("3")){
				criterion1=Restrictions.le("jobWiseConsolidatedScore",Double.parseDouble(normScoreVal));
			}else if(fitScoreSelectVal.equals("4")){
				criterion1=Restrictions.gt("jobWiseConsolidatedScore",Double.parseDouble(normScoreVal));
			}else if(fitScoreSelectVal.equals("5")){
				criterion1=Restrictions.ge("jobWiseConsolidatedScore",Double.parseDouble(normScoreVal));
			}
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			teacherList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
	@Transactional(readOnly=false)
	public List<JobWiseConsolidatedTeacherScore> findbyTeacherDetails(List<TeacherDetail> teacherDetail)
	{	
		List<JobWiseConsolidatedTeacherScore> jobWiseConsolidatedTeacherScoreList= new ArrayList<JobWiseConsolidatedTeacherScore>();	
		try{
              Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetail);
			  jobWiseConsolidatedTeacherScoreList = findByCriteria(criterion1);
			} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return jobWiseConsolidatedTeacherScoreList;
	}
	
	@Transactional(readOnly=false)
	public List<JobWiseConsolidatedTeacherScore> getJWCTScore_CGMass(List<TeacherDetail> lstTeacherDetail, JobOrder jobOrder)
	{	
		List<JobWiseConsolidatedTeacherScore> jobWiseConsolidatedTeacherScoreList= new ArrayList<JobWiseConsolidatedTeacherScore>();	
		try{
			if(lstTeacherDetail!=null && lstTeacherDetail.size() > 0)
			{
				Criterion criterion = Restrictions.in("teacherDetail",lstTeacherDetail);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
				jobWiseConsolidatedTeacherScoreList = findByCriteria(criterion,criterion1);
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return jobWiseConsolidatedTeacherScoreList;
	}
	
	@Transactional(readOnly=false)
	public List<JobWiseConsolidatedTeacherScore> getJCTDelta(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster,List<JobOrder> lstJobOrder)
	{	
		List<JobWiseConsolidatedTeacherScore> jobWiseConsolidatedTeacherScoreList= new ArrayList<JobWiseConsolidatedTeacherScore>();
		if(teacherDetails!=null && teacherDetails.size() >0)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				criteria.add(criterion1);
				criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster",districtMaster));
				if(lstJobOrder!=null && lstJobOrder.size()>0)
				{
					Criterion criterion6 = Restrictions.in("jobOrder",lstJobOrder);
					criteria.add(criterion6);
				}
				jobWiseConsolidatedTeacherScoreList = criteria.list();
				} 
			catch (Exception e){
				e.printStackTrace();
			}
		}
		return jobWiseConsolidatedTeacherScoreList;
	}
	
	// Optimization for candidate Pool search
	
	@Transactional(readOnly=false)
	public List<Integer> findTeacherListByFitScore_Op(String normScoreVal,String fitScoreSelectVal,List<Integer> teachersId)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				if(fitScoreSelectVal.equals("1")){
					criterion1=Restrictions.eq("jobWiseConsolidatedScore",Double.parseDouble(normScoreVal));
				}else if(fitScoreSelectVal.equals("2")){
					criterion1=Restrictions.lt("jobWiseConsolidatedScore",Double.parseDouble(normScoreVal));
				}else if(fitScoreSelectVal.equals("3")){
					criterion1=Restrictions.le("jobWiseConsolidatedScore",Double.parseDouble(normScoreVal));
				}else if(fitScoreSelectVal.equals("4")){
					criterion1=Restrictions.gt("jobWiseConsolidatedScore",Double.parseDouble(normScoreVal));
				}else if(fitScoreSelectVal.equals("5")){
					criterion1=Restrictions.ge("jobWiseConsolidatedScore",Double.parseDouble(normScoreVal));
				}
				criteria.add(criterion1);
				criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
	
	@Transactional(readOnly=false)
	public List<JobWiseConsolidatedTeacherScore> getFitScore_Opp(List<TeacherDetail> teacherDetails, JobOrder jobOrder)
	{	
		List<JobWiseConsolidatedTeacherScore> jobWiseConsolidatedTeacherScoreList= new ArrayList<JobWiseConsolidatedTeacherScore>();
		List<Object[]>  objList=null;
		try{
		 	if(teacherDetails.size()>0)
				{
				String teacherDetailsList ="";
			  int p=0;
	 	  		for(TeacherDetail teacherDetail: teacherDetails){
					if(p==0){
						teacherDetailsList=teacherDetail.getTeacherId()+"";
					}else{
						teacherDetailsList+=","+teacherDetail.getTeacherId()+"";
					   }
					p++; 
				}
				  Query  query=null;
			       Session session = getSession();
			     	String hql = " select jwt.teacherConsolidatedScoreId , jwt.jobWiseConsolidatedScore,jwt.jobWiseMaxScore,jwt.teacherId  from  jobwiseconsolidatedteacherscore jwt "
			     		 + " left join teacherdetail td on td.teacherId=jwt.teacherId "
			     		  +  " Where  jwt.teacherId in ("+teacherDetailsList+")  and jwt.jobId = '"+jobOrder.getJobId()+"' " ;
			            query=session.createSQLQuery(hql);
		 		        objList = query.list();
		 		       JobWiseConsolidatedTeacherScore jobWiseConsolidatedTeacherScore=null;
				        TeacherDetail teacherDetail=null;
				        JobOrder jobOrder1=null;
				        if(objList.size()>0){
				           for (int j = 0; j < objList.size(); j++) {
				       	    Object[] objArr2 = objList.get(j);
				       	    jobWiseConsolidatedTeacherScore = new JobWiseConsolidatedTeacherScore();
				       	    teacherDetail=new TeacherDetail();
				       	     jobOrder1  =new JobOrder();
					        if(objArr2[0]!=null)
					        	jobWiseConsolidatedTeacherScore.setTeacherConsolidatedScoreId(objArr2[0] == null ? null : Integer.parseInt(objArr2[0].toString()));
					        if(objArr2[1]!=null)
					        	jobWiseConsolidatedTeacherScore.setJobWiseConsolidatedScore(objArr2[1] == null ? null : Double.parseDouble(objArr2[1].toString()));
					        if(objArr2[2]!=null)
					        	jobWiseConsolidatedTeacherScore.setJobWiseMaxScore(objArr2[2] == null ? null : Double.parseDouble(objArr2[2].toString()));
					        if(objArr2[3]!=null){
					        	teacherDetail.setTeacherId(objArr2[3] == null ? null : Integer.parseInt(objArr2[3].toString()));
					        	jobWiseConsolidatedTeacherScore.setTeacherDetail(teacherDetail);
					           }
					        jobWiseConsolidatedTeacherScoreList.add(jobWiseConsolidatedTeacherScore);
				           }
				          }	
		 	            }
		             } 
		catch (Exception e){
			e.printStackTrace();
		}	
		return jobWiseConsolidatedTeacherScoreList;
	}
 	
}
