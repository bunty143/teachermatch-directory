package tm.dao.cgreport;

import java.util.Calendar;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNotes;
import tm.bean.cgreport.TeacherPhoneCallHistory;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherPhoneCallHistoryDAO extends GenericHibernateDAO<TeacherPhoneCallHistory, Integer>{
	public TeacherPhoneCallHistoryDAO(){
		super(TeacherPhoneCallHistory.class);
	}
	@Transactional(readOnly=true)
	public List<TeacherPhoneCallHistory> findByTeacher(TeacherDetail teacherDetail)
	{
	         List <TeacherPhoneCallHistory> lstTeacherPhone = null;
			 Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			 lstTeacherPhone = findByCriteria(criterion);
			 return lstTeacherPhone;
	}
	@Transactional(readOnly=true)
	public List<TeacherPhoneCallHistory> findByTeacher(TeacherDetail teacherDetail,JobOrder jobOrder,Order order,int startPos,int limit)
	{
		List<TeacherPhoneCallHistory> lstTeacherPhone = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			if(order != null){
				criteria.addOrder(order);
			}
			
			lstTeacherPhone = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherPhone;
	}
	
	@Transactional(readOnly=true)
	public int getRowcountByTeacher(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherPhoneCallHistory> lstTeacherPhone = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			lstTeacherPhone = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherPhone.size();
	}
	
	@Transactional(readOnly=true)
	public List<TeacherPhoneCallHistory> findByTeacher(TeacherDetail teacherDetail,JobOrder jobOrder,UserMaster userMaster)
	{
		List<TeacherPhoneCallHistory> lstTeacherPhone = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			if(jobOrder==null){
				if(userMaster.getEntityType()==2){
					Criteria cc = criteria.createCriteria("userMaster");	
					cc.add(Restrictions.eq("districtId",userMaster.getDistrictId()));
				}if(userMaster.getEntityType()==3){
					Criteria cc = criteria.createCriteria("userMaster");	
					cc.add(Restrictions.eq("schoolId",userMaster.getSchoolId()));
				}
			}else{
				criteria.add(criterion2);
			}
			lstTeacherPhone = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherPhone;
	}
	
	
}
