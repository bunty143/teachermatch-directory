package tm.dao.cgreport;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.cgreport.ZScore;
import tm.dao.generic.GenericHibernateDAO;

public class ZScoreDAO extends GenericHibernateDAO<ZScore, Integer>{
	public ZScoreDAO() {
		super(ZScore.class);
	}
	
	@Transactional(readOnly=false)
	public List<ZScore> findActiveJobAlert(Double percentile){
		List<ZScore> lstZScore = null;
		try{
			Criterion criterion1 = Restrictions.eq("percentile",percentile);
			lstZScore = findByCriteria(Order.desc("senderName"),criterion1);			
		} 
		catch (Exception e){
			e.printStackTrace();
		}		
		return lstZScore;
	}
}