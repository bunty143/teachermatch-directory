package tm.dao;

import tm.bean.DocumentFileMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DocumentFileMasterDAO extends GenericHibernateDAO<DocumentFileMaster, Integer> 
{
	public DocumentFileMasterDAO() {
		super(DocumentFileMaster.class);
	}
}
