package tm.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.CandidateEventDetails;
import tm.bean.EventDetails;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;

public class CandidateEventDetailsDAO extends GenericHibernateDAO<CandidateEventDetails,Integer>
{
	public CandidateEventDetailsDAO(){
		super(CandidateEventDetails.class);
	}
	
	@Transactional(readOnly=true)
	public List<CandidateEventDetails> findByEmailAndEvent(String emailAddress,EventDetails eventDetails)
	{
		List <CandidateEventDetails> lstTeacherDetails = new ArrayList<CandidateEventDetails>();
		Criterion criterion1 = Restrictions.eq("emailAddress",emailAddress);
		Criterion criterion2 = Restrictions.eq("eventDetails",eventDetails);
		lstTeacherDetails = findByCriteria(criterion1,criterion2);  

		return lstTeacherDetails;
	}
	
	@Transactional(readOnly=true)
	public List<CandidateEventDetails> findByEventDetail(EventDetails eventDetails,int start,int end,Order sortOrderStrVal)
	{
		List <CandidateEventDetails> lstTeacherDetails = new ArrayList<CandidateEventDetails>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("eventDetails",eventDetails);
			criteria.add(criterion1);
			criteria.addOrder(sortOrderStrVal);
			
			criteria.setFirstResult(start);
			criteria.setMaxResults(end);
			lstTeacherDetails=criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstTeacherDetails;
	}
	@Transactional(readOnly=true)
	public int getRowEventDetail(EventDetails eventDetails,int start,int end,Order sortOrderStrVal)
	{
		int rows=0;
		List <CandidateEventDetails> lstTeacherDetails = new ArrayList<CandidateEventDetails>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("eventDetails",eventDetails);
			criteria.add(criterion1);
			criteria.addOrder(sortOrderStrVal);
			
			lstTeacherDetails=criteria.list();
			rows=lstTeacherDetails.size();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rows;
	}

}
