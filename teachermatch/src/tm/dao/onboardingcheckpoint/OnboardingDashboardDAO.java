package tm.dao.onboardingcheckpoint;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.DistrictMaster;
import tm.bean.master.OnboardingDashboard;
import tm.dao.generic.GenericHibernateDAO;

public class OnboardingDashboardDAO extends GenericHibernateDAO<OnboardingDashboard, Integer> {
	public OnboardingDashboardDAO()
	{
		super(OnboardingDashboard.class);
	}
	
	@Transactional(readOnly=false)
	public  List<OnboardingDashboard> findOnBoardingList(Order order,int startPos,int limit,Criterion...criterions)
	{
		List<OnboardingDashboard> OnboardingDashboardList=null;
		
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			for(Criterion c:criterions){
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			if(order != null)
				criteria.addOrder(order);
			
			OnboardingDashboardList=criteria.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return OnboardingDashboardList;
	}
	
	@Transactional(readOnly=false)
	public List<OnboardingDashboard> findOnBoardingNameByDistrict(DistrictMaster districtMaster)
	{
    	List<OnboardingDashboard> OnboardingDashboardList = new ArrayList<OnboardingDashboard>();
		try 
		{
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
		        criteria.add(Restrictions.eq("districtMaster",districtMaster));
		        criteria.add(Restrictions.eq("status","A"));
		        criteria.addOrder(Order.asc("onboardingName"));
		        OnboardingDashboardList = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return OnboardingDashboardList;
	}
	
	@Transactional(readOnly=false)
    public OnboardingDashboard findByDistrictAndOnboardingName(DistrictMaster districtMaster,  String onboardingName)
    {
        List<OnboardingDashboard> onboardingNameList = new ArrayList<OnboardingDashboard>();
        try{
            
            Session session         =     getSession();
            Criteria criteria         =     session.createCriteria(getPersistentClass());
            Criterion criterion1    =    null;
            Criterion criterion2    =    null;
            criterion1 = Restrictions.eq("districtMaster",districtMaster);           
            criterion2 = Restrictions.eq("onboardingName",onboardingName);
            criteria.add(criterion1);
            criteria.add(criterion2);
 
            onboardingNameList = criteria.list();
            
        } catch(Exception e){
            e.printStackTrace();
        }
        
        if(onboardingNameList==null || onboardingNameList.size()==0)
             return null;
         else
             return onboardingNameList.get(0);
    }
}
