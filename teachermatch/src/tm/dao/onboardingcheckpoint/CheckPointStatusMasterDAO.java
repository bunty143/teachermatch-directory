package tm.dao.onboardingcheckpoint;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.onboarding.CheckPointStatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CheckPointStatusMasterDAO extends GenericHibernateDAO<CheckPointStatusMaster, Integer>{
public CheckPointStatusMasterDAO(){
	super(CheckPointStatusMaster.class);
}
@Transactional
public List<CheckPointStatusMaster> getcheckpointstatus()
{
	List<CheckPointStatusMaster> checkPointStatusMasterList=null;
	
	try{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		criteria.add(Restrictions.eq("status","A"));
		checkPointStatusMasterList=criteria.list();
		
	}catch (Exception e) {
		e.printStackTrace();
	}
	return checkPointStatusMasterList;
}
}
