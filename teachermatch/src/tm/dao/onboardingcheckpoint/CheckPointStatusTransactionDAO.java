package tm.dao.onboardingcheckpoint;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.onboarding.CheckPointStatusTransaction;
import tm.dao.generic.GenericHibernateDAO;

public class CheckPointStatusTransactionDAO extends GenericHibernateDAO<CheckPointStatusTransaction, Integer>{
public CheckPointStatusTransactionDAO(){
	super(CheckPointStatusTransaction.class);
}
@SuppressWarnings("unchecked")
@Transactional
public List<CheckPointStatusTransaction> getCheckPointStatusRecord(Integer districtId,Integer onBoardingId,Integer checkPointId,String candidateType)
{
	List<CheckPointStatusTransaction> checkPointStatusTransactionList=null;
	try{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		if(districtId!=null)
			criteria.add(Restrictions.eq("districtId",districtId));
		if(onBoardingId!=null)
			criteria.add(Restrictions.eq("onboardingId",onBoardingId));
		if(checkPointId!=null)
			criteria.add(Restrictions.eq("checkPointId",checkPointId));
		if(candidateType!=null)
			criteria.add(Restrictions.eq("candidateType",candidateType));
		else
			criteria.add(Restrictions.eq("candidateType","E"));
		//criteria.add(Restrictions.eq("status","A"));
		checkPointStatusTransactionList=criteria.list();
		
	}catch (Exception e) {
		e.printStackTrace();
	}
	return checkPointStatusTransactionList;
}

@Transactional
public List<CheckPointStatusTransaction> getCheckPointStatusRecord(Integer districtId,Integer onBoardingId,Integer checkPointId,List<String> candateType)
{
	List<CheckPointStatusTransaction> checkPointStatusTransactionList=null;
	try{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		if(districtId!=null)
			criteria.add(Restrictions.eq("districtId",districtId));
		if(onBoardingId!=null)
			criteria.add(Restrictions.eq("onboardingId",onBoardingId));
		if(checkPointId!=null)
			criteria.add(Restrictions.eq("checkPointId",checkPointId));
		
		criteria.add(Restrictions.in("candidateType",candateType));
		criteria.add(Restrictions.eq("status","A"));
		checkPointStatusTransactionList=criteria.list();
		
	}catch (Exception e) {
		e.printStackTrace();
	}
	return checkPointStatusTransactionList;
}


@Transactional
public List<CheckPointStatusTransaction> getCheckPointStatusRecordCount(Integer districtId,Integer onBoardingId)
{ 
	List<CheckPointStatusTransaction> recordCount=new ArrayList<CheckPointStatusTransaction>();
	try{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		if(districtId!=null)
			criteria.add(Restrictions.eq("districtId",districtId));
		if(onBoardingId!=null)
			criteria.add(Restrictions.eq("onboardingId",onBoardingId));
		criteria.add(Restrictions.eq("status","A"));
		
		/*criteria.setProjection(Projections.projectionList()
				.add(Projections.property("checkPointId"))
				.add(Projections.property("cpStatusTransactionId"))
				.add(Projections.property("cpStatusMasterId"))
				.add(Projections.property("candidateType"))
		        .add(Projections.property("candidateType")));*/
		   
		/*criteria.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("checkPointId"))
				.add(Projections.groupProperty("candidateType")));
		*/
		// criteria.setResultTransformer(new AliasToBeanResultTransformer(CheckPointStatusTransaction.class));
		//recordCount=criteria.setResultTransformer( new AliasToBeanResultTransformer(this.getPersistentClass())).list();
		recordCount=criteria.list();
	}catch(Exception e){
		e.printStackTrace();
	}
	return recordCount;
}
@Transactional
public List<CheckPointStatusTransaction> getCheckPointEditStatusTransactionDetails(Integer districtId,Integer onBoardingId,Integer checkPointId)
{
	List<CheckPointStatusTransaction> list=null;
	list=new ArrayList<CheckPointStatusTransaction>();
	try{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		if(districtId!=null)
			criteria.add(Restrictions.eq("districtId",districtId));
		if(onBoardingId!=null)
			criteria.add(Restrictions.eq("onboardingId",onBoardingId));
		if(checkPointId!=null)
			criteria.add(Restrictions.eq("checkPointId", checkPointId));
		criteria.add(Restrictions.eq("status","A"));
		 list = criteria.list();
		
		
	}catch (Exception e) {
		e.printStackTrace();
	}
	return list;
}
}
