package tm.dao.onboardingcheckpoint;

import tm.bean.onboarding.AddCheckPointMaster;
import tm.dao.generic.GenericHibernateDAO;

public class AddCheckPointMasterDAO extends GenericHibernateDAO<AddCheckPointMaster, Integer> {

	public AddCheckPointMasterDAO() {
		super(AddCheckPointMaster.class);
	}
	

}
