package tm.dao.onboardingcheckpoint;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.onboarding.CheckPointPermissionMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CheckPointPermissionMasterDAO extends GenericHibernateDAO<CheckPointPermissionMaster,Integer> {
public CheckPointPermissionMasterDAO(){
	super(CheckPointPermissionMaster.class);
}
@Transactional
public List<CheckPointPermissionMaster> getPermissionRecord()
{
	List<CheckPointPermissionMaster> checkPointPermissionMasterList=null;
	try{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		criteria.add(Restrictions.eq("status","A"));
		checkPointPermissionMasterList=criteria.list();
	}catch (Exception e) {
		e.printStackTrace();
	}
	return checkPointPermissionMasterList;
}
}
