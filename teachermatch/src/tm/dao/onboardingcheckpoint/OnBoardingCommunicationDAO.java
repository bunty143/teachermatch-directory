package tm.dao.onboardingcheckpoint;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.OnBoardingCommunication;
import tm.dao.generic.GenericHibernateDAO;

public class OnBoardingCommunicationDAO extends GenericHibernateDAO<OnBoardingCommunication, Integer> {
  public OnBoardingCommunicationDAO(){
	  super(OnBoardingCommunication.class);
  }
  @Transactional(readOnly=false)
	public  List<OnBoardingCommunication> findOnBoardingCommunicationList(Order order,int startPos,int limit,Criterion...criterions){
	  List<OnBoardingCommunication> communicationRecordList=null;
	  try{
		  Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			for(Criterion c:criterions){
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			if(order != null)
				criteria.addOrder(order);
			communicationRecordList= criteria.list();
	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  return communicationRecordList;
  }
  @SuppressWarnings("unchecked")
  @Transactional
  public List<OnBoardingCommunication> getCommunicationRecord(Integer districtId,Integer onBoardingId,Integer checkPointId,String userType){
	  List<OnBoardingCommunication> communicationRecordList=null;
	  try{
		  Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtId!=null)
				criteria.add(Restrictions.eq("districtId",districtId));
			if(onBoardingId!=null)
				criteria.add(Restrictions.eq("onboardingId",onBoardingId));
			if(checkPointId!=null)
				criteria.add(Restrictions.eq("checkPointId",checkPointId));
			
			criteria.add(Restrictions.eq("cUserType",userType));
			
			communicationRecordList=criteria.list();	
	  }catch (Exception e) {
		e.printStackTrace();
	}
	  return communicationRecordList;
  }
 
   	@Transactional(readOnly=false)
	public List<OnBoardingCommunication> findOnbOardingCommunicationNameByDistrict(Integer districtId)
	{
  	List<OnBoardingCommunication> OnBoardingCommunicationList = new ArrayList<OnBoardingCommunication>();
		try 
		{
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
		        criteria.add(Restrictions.eq("districtId",districtId));
		        criteria.add(Restrictions.eq("status","A"));
		        criteria.addOrder(Order.asc("templeteName"));
		        OnBoardingCommunicationList = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return OnBoardingCommunicationList;
	}
   	
	@Transactional(readOnly=false)
	public List<OnBoardingCommunication> findCommunicationByDistrictAndOnboardingId(Integer districtId,Integer onBoardingId)
	{
		List<OnBoardingCommunication> OnBoardingCommunicationList = new ArrayList<OnBoardingCommunication>();
		try 
		{
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				if(districtId!=null)
					criteria.add(Restrictions.eq("districtId",districtId));
				if(onBoardingId!=null)
					criteria.add(Restrictions.eq("onBoardingId",onBoardingId));
				
		        OnBoardingCommunicationList = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return OnBoardingCommunicationList;
	}
}
