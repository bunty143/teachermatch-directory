package tm.dao.onboardingcheckpoint;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.onboarding.OnBoardingCheckPointOptions;
import tm.dao.generic.GenericHibernateDAO;

public class OnboardCheckPointOptionsDAO extends GenericHibernateDAO<OnBoardingCheckPointOptions, Integer>{
	public OnboardCheckPointOptionsDAO(){
		super(OnBoardingCheckPointOptions.class);
	}

	
	@Transactional
	public List<OnBoardingCheckPointOptions> getCheckPointEditOptionDetails(Integer checkPointQuestionId)
	{
		List<OnBoardingCheckPointOptions> list=null;
		list=new ArrayList<OnBoardingCheckPointOptions>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(checkPointQuestionId!=null)
				criteria.add(Restrictions.eq("checkPointQuestionId",checkPointQuestionId));
			
			 list = criteria.list();
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
