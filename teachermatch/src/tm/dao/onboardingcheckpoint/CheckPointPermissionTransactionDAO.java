package tm.dao.onboardingcheckpoint;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.onboarding.CheckPointPermissionTransaction;
import tm.dao.generic.GenericHibernateDAO;

public class CheckPointPermissionTransactionDAO extends GenericHibernateDAO<CheckPointPermissionTransaction,Integer>{
	public CheckPointPermissionTransactionDAO(){
		super(CheckPointPermissionTransaction.class);
	}
	@Transactional
	public List<CheckPointPermissionTransaction> getCheckPointPermissionRecord(Integer districtId,Integer onBoardingId,Integer checkPointId,String candidateType)
	{
		List<CheckPointPermissionTransaction> checkPointPermissionTransactionList=null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtId!=null)
				criteria.add(Restrictions.eq("districtId",districtId));
			if(onBoardingId!=null)
				criteria.add(Restrictions.eq("onBoardingId",onBoardingId));
			if(checkPointId!=null)
				criteria.add(Restrictions.eq("checkPointId",checkPointId));
			if(candidateType!=null)
				criteria.add(Restrictions.eq("candidateType",candidateType));
			else
				criteria.add(Restrictions.eq("candidateType","E"));
			criteria.add(Restrictions.eq("status", "A"));
			checkPointPermissionTransactionList=criteria.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return checkPointPermissionTransactionList;
	}
	
	@Transactional
	public Integer getCheckPointPermissionCount(Integer districtId,Integer onBoardingId,Integer checkPointId,List<String> candidateType)
	{
		Integer noOfRecord=0;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtId!=null)
				criteria.add(Restrictions.eq("districtId",districtId));
			if(onBoardingId!=null)
				criteria.add(Restrictions.eq("onBoardingId",onBoardingId));
			if(checkPointId!=null)
				criteria.add(Restrictions.eq("checkPointId",checkPointId));
			
			criteria.add(Restrictions.in("candidateType",candidateType));
			criteria.setProjection(Projections.rowCount());
			noOfRecord=(Integer) criteria.uniqueResult();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return noOfRecord;
	}
	@Transactional
	public List<CheckPointPermissionTransaction> findcheckPointPermissionByDistrictAndOnboardingId(Integer districtId,Integer onBoardingId)
	{
		List<CheckPointPermissionTransaction> checkPointPermissionList=new ArrayList<CheckPointPermissionTransaction>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtId!=null)
				criteria.add(Restrictions.eq("districtId",districtId));
			if(onBoardingId!=null)
				criteria.add(Restrictions.eq("onBoardingId",onBoardingId));
			criteria.add(Restrictions.eq("status","A"));
			
			checkPointPermissionList=criteria.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return checkPointPermissionList;
	}
	
	@Transactional
	public List<CheckPointPermissionTransaction> getEditCheckPointDetails(Integer onBoardingId,Integer checkPointId,Integer districtId,List<String> candidateType){
		List<CheckPointPermissionTransaction> checkPointPermissionList=null;
		checkPointPermissionList=new ArrayList<CheckPointPermissionTransaction>();
		Session session= getSession();
		Criteria criteria= session.createCriteria(getPersistentClass());
		if(districtId!=null)
			criteria.add(Restrictions.eq("districtId",districtId));
		if(onBoardingId!=null)
			criteria.add(Restrictions.eq("onBoardingId",onBoardingId));
		if(checkPointId!=null)
			criteria.add(Restrictions.eq("checkPointId",checkPointId));
		if(candidateType!=null)
			criteria.add(Restrictions.in("candidateType",candidateType));
		else
			criteria.add(Restrictions.eq("candidateType","E"));
		checkPointPermissionList=criteria.list();
		return checkPointPermissionList;
	}
	 
	
}
