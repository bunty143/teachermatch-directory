package tm.dao.textfile;


import tm.bean.textfile.LicenseArea;
import tm.dao.generic.GenericHibernateDAO;

/* 
 * @Author: Sandeep Yadav
 */

public class LicenseAreaDAO extends GenericHibernateDAO<LicenseArea, Integer>{
	
	
	public LicenseAreaDAO() 
	{
		super(LicenseArea.class);
	}
}
