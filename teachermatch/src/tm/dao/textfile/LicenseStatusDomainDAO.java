package tm.dao.textfile;


import tm.bean.textfile.LicenseStatusDomain;
import tm.dao.generic.GenericHibernateDAO;

/* 
 * @Author: Sandeep Yadav
 */


public class LicenseStatusDomainDAO extends GenericHibernateDAO<LicenseStatusDomain, Integer>{
	
	
	public LicenseStatusDomainDAO() 
	{
		super(LicenseStatusDomain.class);
	}

}
