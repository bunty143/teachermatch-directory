package tm.dao.textfile;

import tm.bean.textfile.DegreeTypeMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DegreeTypeMasterDAO extends GenericHibernateDAO<DegreeTypeMaster, Integer>{

	public DegreeTypeMasterDAO() {
		super(DegreeTypeMaster.class);
		
	}
	
	

}
