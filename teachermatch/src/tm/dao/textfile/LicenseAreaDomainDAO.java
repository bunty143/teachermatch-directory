package tm.dao.textfile;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.textfile.LicenseAreaDomain;
import tm.bean.textfile.LicenseClassLevelDomain;
import tm.dao.generic.GenericHibernateDAO;

/* 
 * @Author: Sandeep Yadav
 */

public class LicenseAreaDomainDAO extends GenericHibernateDAO<LicenseAreaDomain, Integer>{
	
	
	public LicenseAreaDomainDAO() 
	{
		super(LicenseAreaDomain.class);
	}

	@Transactional(readOnly=false)
	public List<LicenseAreaDomain> getListByClassCodeList(List<String> areaCodeList){
		List<LicenseAreaDomain> licenseAreaList = new ArrayList<LicenseAreaDomain>();
		try{
			Criterion cr = Restrictions.in("licenseAreaCode", areaCodeList);
			Criterion crStatus = Restrictions.eq("status", "A");
			licenseAreaList = findByCriteria(cr,crStatus);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return licenseAreaList;
	}
	
}
