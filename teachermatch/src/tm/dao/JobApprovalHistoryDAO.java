package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.python.antlr.PythonParser.return_stmt_return;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictApprovalGroups;
import tm.bean.JobApprovalHistory;
import tm.bean.JobOrder;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobApprovalHistoryDAO extends GenericHibernateDAO<JobApprovalHistory, Integer>
{

	public JobApprovalHistoryDAO() {
		super(JobApprovalHistory.class);
	}
	
	@Transactional(readOnly=false)
	public List<JobApprovalHistory> findByJobId(JobOrder jobOrder)
	{
		List<JobApprovalHistory> jobApprovalHistories = new ArrayList<JobApprovalHistory>();
		try 
		{	
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			
			criteria.add(criterion1);
			jobApprovalHistories =criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobApprovalHistories;
	}
	@Transactional(readOnly=false)
	public List<JobApprovalHistory> findByUserIdAndJobId(UserMaster userMaster,JobOrder jobOrder)
	{
		List<JobApprovalHistory> jobApprovalHistories = new ArrayList<JobApprovalHistory>();
		Criterion criterion1=Restrictions.eq("userMaster", userMaster);
		Criterion criterion2=Restrictions.eq("jobOrder", jobOrder);
		jobApprovalHistories=findByCriteria(criterion2);
		return jobApprovalHistories;
	}
	
	@Transactional(readOnly=false)
	public List<JobApprovalHistory> findHistoryByUser(List<UserMaster> listuserMasters)
	{
		List<JobApprovalHistory> jobApprovalHistories = new ArrayList<JobApprovalHistory>();
		Criterion criterion=Restrictions.in("userMaster", listuserMasters);
		jobApprovalHistories=findByCriteria(criterion);
		return jobApprovalHistories;
	}
	
	@Transactional(readOnly=false)
	public boolean removeHistoryByGroupId(DistrictApprovalGroups groupId)
	{
		try
		{
			String hql = "delete from JobApprovalHistory WHERE districtApprovalGroupsId = :groupId";
			Query query = null;
			Session session = getSession();
			
			query = session.createQuery(hql);
			query.setEntity("groupId", groupId);
			System.out.println("delete from JobApprovalHistory:- "+query.executeUpdate());
			
			return true;
		}
		catch (HibernateException e)
		{
			e.printStackTrace();
		}
		return false;
	}
}