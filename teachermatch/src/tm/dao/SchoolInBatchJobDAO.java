package tm.dao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.BatchJobOrder;
import tm.bean.JobOrder;
import tm.bean.SchoolInBatchJob;
import tm.bean.SchoolInJobOrder;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.SchoolMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SchoolInBatchJobDAO extends GenericHibernateDAO<SchoolInBatchJob, Integer> 
{
	public SchoolInBatchJobDAO() 
	{
		super(SchoolInBatchJob.class);
	}
	@Transactional(readOnly=false)
	public List<SchoolInBatchJob> findBatchJobOrder(BatchJobOrder batchJobOrder)
	{
		List<SchoolInBatchJob> lstSchoolInBatchJob= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			lstSchoolInBatchJob = findByCriteria(Order.asc("batchJobId"),criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolInBatchJob;
	}
	@Transactional(readOnly=false)
	public boolean findJobAccepted(BatchJobOrder batchJobOrder)
	{
		boolean isExist=false;
		List<SchoolInBatchJob> lstSchoolInBatchJob= null;
		try{
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			Criterion criterion2 = Restrictions.eq("jobAccepted",true);
			lstSchoolInBatchJob = findByCriteria(criterion1,criterion2);	
			if(lstSchoolInBatchJob.size()>0){
				isExist=true;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return isExist;
	}
	@Transactional(readOnly=false)
	public boolean findAcceptStatus(BatchJobOrder batchJobOrder,SchoolMaster schoolMaster)
	{
		boolean isExist=false;
		List<SchoolInBatchJob> lstSchoolInBatchJob= null;
		try{
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			Criterion criterion2 = Restrictions.eq("schoolId",schoolMaster);
			lstSchoolInBatchJob = findByCriteria(criterion1,criterion2);
			if(lstSchoolInBatchJob.size()>0){
				if(lstSchoolInBatchJob.get(0).isAcceptStatus()){
					isExist=true;
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return isExist;
	}
	@Transactional(readOnly=false)
	public int findNoOfExpHires(BatchJobOrder batchJobOrder,SchoolMaster schoolMaster,int allSchool)
	{
		int  noOfExpHires=0;
		List<SchoolInBatchJob> lstSchoolInBatchJob= null;
		try{
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			if(allSchool==0){
				Criterion criterion2 = Restrictions.eq("schoolId",schoolMaster);
				lstSchoolInBatchJob = findByCriteria(criterion1,criterion2);
			}else{
				lstSchoolInBatchJob = findByCriteria(criterion1);
			}
			if(lstSchoolInBatchJob.size()>0){
				if(lstSchoolInBatchJob.get(0).getNoOfSchoolExpHires()!=0){
					noOfExpHires=lstSchoolInBatchJob.get(0).getNoOfSchoolExpHires();
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return noOfExpHires;
	}
	
	@Transactional(readOnly=false)
	public SchoolInBatchJob findSchoolInJobBySchoolAndJob(BatchJobOrder batchJobOrder, SchoolMaster schoolMaster)
	{
		List<SchoolInBatchJob> lstSchoolInBatchJob= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			Criterion criterion2 = Restrictions.eq("schoolId",schoolMaster);
			lstSchoolInBatchJob = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		if(lstSchoolInBatchJob==null || lstSchoolInBatchJob.size()==0)
			return null;
		else
			return lstSchoolInBatchJob.get(0);
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<Integer,SchoolInBatchJob> findSchoolInJobBySchoolAndJob() 
	{
		Session session = getSession();
		List result = session.createCriteria(getPersistentClass())
		.list();

		Map<Integer,SchoolInBatchJob> schoolInBatchJobMap = new HashMap<Integer, SchoolInBatchJob>();
		SchoolInBatchJob schoolInBatchJob = null;
		int i=0;
		for (Object object : result) {
			schoolInBatchJob=((SchoolInBatchJob)object);
			schoolInBatchJobMap.put(new Integer(""+i),schoolInBatchJob);
			i++;
		}
		return schoolInBatchJobMap;
	}
	@Transactional(readOnly=false)
	public int findSchoolJobId(BatchJobOrder batchJobOrder,SchoolMaster schoolMaster)
	{
		int  schoolJobId=0;
		List<SchoolInBatchJob> lstSchoolInBatchJob= null;
		try{
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			Criterion criterion2 = Restrictions.eq("schoolId",schoolMaster);
			lstSchoolInBatchJob = findByCriteria(criterion1,criterion2);
			if(lstSchoolInBatchJob.size()>0){
				if(lstSchoolInBatchJob.get(0).getCorrespondingSchoolJobOrder()!=null){
					if(!lstSchoolInBatchJob.get(0).getCorrespondingSchoolJobOrder().equals("")){
						schoolJobId=lstSchoolInBatchJob.get(0).getCorrespondingSchoolJobOrder();
					}else{
						schoolJobId=0;
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolJobId;
	}
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolIds(BatchJobOrder batchJobOrder)
	{
		List<SchoolMaster>  lstSchoolId = new ArrayList(); 
		List<SchoolInBatchJob> lstSchoolInBatchJob= null;
		try{
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			lstSchoolInBatchJob = findByCriteria(criterion1);
			for(SchoolInBatchJob schoolInBatchJob: lstSchoolInBatchJob){
				lstSchoolId.add(schoolInBatchJob.getSchoolId());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolId;
	}
	@Transactional(readOnly=false)
	public int getNoOfSchoolExpHiresBySchool(BatchJobOrder batchJobOrder,SchoolMaster schoolMaster)
	{
		List<SchoolInBatchJob> lsttotalBatchJobOrderBySchool= null;
		int noOfSchoolExpHires=0;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			Criterion criterion2 = Restrictions.eq("schoolId", schoolMaster);
			
			criteria.add(criterion1);
			criteria.add(criterion2);
			lsttotalBatchJobOrderBySchool = criteria.list();
			if(lsttotalBatchJobOrderBySchool.size()>0){
				noOfSchoolExpHires=	lsttotalBatchJobOrderBySchool.get(0).getNoOfSchoolExpHires();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return noOfSchoolExpHires;
	}
	@Transactional(readOnly=false)
	public List<SchoolMaster> findJobAcceptedORHasSchool(BatchJobOrder batchJobOrder,int Type)
	{
		List<SchoolMaster>  lstSchoolId = new ArrayList(); 
		List<SchoolInBatchJob> lstSchoolInBatchJob= null;
		try{
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			lstSchoolInBatchJob = findByCriteria(criterion1);
			for(SchoolInBatchJob schoolInBatchJob: lstSchoolInBatchJob){
				if(Type==1){
					if(schoolInBatchJob.isAcceptStatus()==true && schoolInBatchJob.getBatchJobId().getBatchJobId()==batchJobOrder.getBatchJobId()){
						lstSchoolId.add(schoolInBatchJob.getSchoolId());
					}
				}else{
					if(schoolInBatchJob.getBatchJobId().getBatchJobId()==batchJobOrder.getBatchJobId()){
						lstSchoolId.add(schoolInBatchJob.getSchoolId());
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolId;
	}
	
}
