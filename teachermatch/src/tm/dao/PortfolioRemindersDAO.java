package tm.dao;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.PortfolioReminders;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;
import java.util.ArrayList;
import java.util.List;


public class PortfolioRemindersDAO extends GenericHibernateDAO<PortfolioReminders,Integer> 
{
	public PortfolioRemindersDAO() {
		super(PortfolioReminders.class);
	}
	
	@Transactional(readOnly=false)
	public List<PortfolioReminders> getRecordByDistrict(DistrictMaster districtMaster){
		
		List<PortfolioReminders> portfolioRemindersList = new ArrayList<PortfolioReminders>();
		try{
			Criterion criterion1 	= Restrictions.eq("districtMaster",districtMaster);
			portfolioRemindersList 	= findByCriteria(criterion1);
		} catch(Exception exception){
			exception.printStackTrace();
		}
		return portfolioRemindersList;
	}

}
