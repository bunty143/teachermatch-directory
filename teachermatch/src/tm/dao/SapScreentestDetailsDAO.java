package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.SapAcademicDetails;
import tm.bean.SapScreentestDetails;
import tm.bean.TeacherDetail;				
import tm.dao.generic.GenericHibernateDAO;

public class SapScreentestDetailsDAO extends GenericHibernateDAO<SapScreentestDetails, Integer> 
{
	public SapScreentestDetailsDAO() {
		super(SapScreentestDetails.class);
	}

	@Transactional(readOnly=false)
	public List<SapScreentestDetails> findSAPScreentestDetailByTeacher(TeacherDetail teacherDetail)
	{
		List<SapScreentestDetails> lstSapScreentestDetails= new ArrayList<SapScreentestDetails>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstSapScreentestDetails = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSapScreentestDetails;
	}
	
	@Transactional(readOnly=false)
	public List<SapScreentestDetails> findSAPScreentestDetailByTeacherAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<SapScreentestDetails> lstSapScreentestDetails= new ArrayList<SapScreentestDetails>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = 	Restrictions.eq("jobOrder",jobOrder);
			lstSapScreentestDetails = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSapScreentestDetails;
	}
	
	
}
