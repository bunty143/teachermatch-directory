package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.CountryMaster;
import tm.bean.master.StateMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class StateMasterDAO extends GenericHibernateDAO<StateMaster, Long> 
{
	public StateMasterDAO() 
	{
		super(StateMaster.class);
	}
	
	@Transactional(readOnly=true)
	public List<StateMaster> findAllStateByOrder()
	{
		List<StateMaster> lstState= null;
		try 
		{
			
			//lstState = findByCriteria(Order.asc("stateName"));
			CountryMaster countryMaster=new CountryMaster();
			if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr"))
				countryMaster.setCountryId(38);
			else
				countryMaster.setCountryId(223);
			Criterion criterion1 = Restrictions.eq("status", "A");
			Criterion criterion2 = Restrictions.eq("countryMaster", countryMaster);
			lstState = findByCriteria(Order.asc("stateName"),criterion1,criterion2);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstState;
	}
	
	
	/* @Start
	 * @ASHish Kumar
	 * @Description :: Find Active States...*/
		@Transactional(readOnly=true)
		public List<StateMaster> findActiveState()
		{
			List<StateMaster> lstState = null;
			try
			{
				lstState = getSession().createCriteria(StateMaster.class).add(Restrictions.eq("status", "A")).addOrder(Order.asc("stateName")).list();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			return lstState;
		}
		
		@Transactional(readOnly=true)
		public List<StateMaster> findActiveStateByCountryId(CountryMaster countryMaster)
		{
			List<StateMaster> lstState = new ArrayList<StateMaster>();
			try
			{
				Criterion criterion1 = Restrictions.eq("status", "A");
				Criterion criterion2 = Restrictions.eq("countryMaster", countryMaster);
				
				lstState = findByCriteria(Order.asc("stateName"),criterion1,criterion2);
			}
			catch (Exception e)
			{
				//e.printStackTrace();
			}
			
			return lstState;
		}
		
		
		@Transactional(readOnly=true)
		public StateMaster findByStateCode(String stateCode)
		{
			List<StateMaster> lstState = new ArrayList<StateMaster>();
			try
			{
				Criterion criterion1 = Restrictions.eq("status", "A");
				Criterion criterion2 = Restrictions.eq("stateShortName", stateCode);
				
				lstState = findByCriteria(Order.asc("stateName"),criterion1,criterion2);
				
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			if(lstState!=null && lstState.size()>0)
				return lstState.get(0);
			else
				return null;
		}
	
		@Transactional(readOnly=true)
		public List<StateMaster> findByStateCodes(List<String> stateCodes)
		{
			List<StateMaster> lstState = new ArrayList<StateMaster>();
			try
			{
				Criterion criterion1 = Restrictions.eq("status", "A");
				Criterion criterion2 = Restrictions.in("stateShortName", stateCodes);
				
				lstState = findByCriteria(Order.asc("stateName"),criterion1,criterion2);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			return lstState;
		}
	@Transactional(readOnly=true)
	public List<StateMaster> findAllActiveState()
	{
		List<StateMaster> lstState= null;
		List<Long> lstStateId=new ArrayList<Long>();
		for(long i=52;i<63;i++){
			lstStateId.add(i);
		}
		try 
		{
			
			//lstState = findByCriteria(Order.asc("stateName"));
			CountryMaster countryMaster=new CountryMaster();
			if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr"))
				countryMaster.setCountryId(38);
			else
				countryMaster.setCountryId(223);
			Criterion criterion1 = Restrictions.eq("status", "A");
			Criterion criterion2 = Restrictions.eq("countryMaster", countryMaster);
			Criterion criterion3=Restrictions.not(Restrictions.in("stateId", lstStateId));
			lstState = findByCriteria(Order.asc("stateName"),criterion1,criterion2,criterion3);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstState;
	}
		
	
}
