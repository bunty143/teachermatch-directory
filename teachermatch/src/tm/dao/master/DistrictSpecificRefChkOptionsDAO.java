package tm.dao.master;

import tm.bean.master.DistrictSpecificRefChkOptions;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificRefChkOptionsDAO extends GenericHibernateDAO<DistrictSpecificRefChkOptions, Integer> 
{
	public DistrictSpecificRefChkOptionsDAO() 
	{
		super(DistrictSpecificRefChkOptions.class);
	}
}