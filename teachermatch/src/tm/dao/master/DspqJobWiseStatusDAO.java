package tm.dao.master;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DspqJobWiseStatus;
import tm.dao.generic.GenericHibernateDAO;

public class DspqJobWiseStatusDAO extends GenericHibernateDAO<DspqJobWiseStatus, Integer> 
{
	public DspqJobWiseStatusDAO() 
	{
		super(DspqJobWiseStatus.class);
	}
	
	
	@Transactional(readOnly=true)
	public DspqJobWiseStatus getDSPQStatus(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
		Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
		
		List<DspqJobWiseStatus> dspqJobWiseStatuss=findByCriteria(criterion1,criterion2);
		DspqJobWiseStatus dspqJobWiseStatus=null;
		if(dspqJobWiseStatuss!=null && dspqJobWiseStatuss.size() >0)
			dspqJobWiseStatus=dspqJobWiseStatuss.get(0);
		return dspqJobWiseStatus;
	}
	
	
}
