package tm.dao.master;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.dao.generic.GenericHibernateDAO;

/* @Author: Vishwanath Kumar
 * @Discription: CompetencyMaster DAO.
 */
public class CompetencyMasterDAO extends GenericHibernateDAO<CompetencyMaster, Integer> 
{
	public CompetencyMasterDAO() {
		super(CompetencyMaster.class);
	}

/* @Author: Vishwanath Kumar
 * @Discription: It is used to get Active Competencies .
 */
	@Transactional(readOnly=false)
	public List<CompetencyMaster> getActiveCompetencies()
	{
		List<CompetencyMaster> competencyMasters = null;
		try{
			Criterion criterion = Restrictions.eq("status", "a");
			competencyMasters = findByCriteria(Order.asc("competencyName"), criterion);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return competencyMasters;
		
	}
	
/* @Author: Vishwanath Kumar
 * @Discription: It is used to get Competencies if domainId is available.
 */
	@Transactional(readOnly=false)
	public List<CompetencyMaster> getCompetencesByDomain(DomainMaster domainMaster)
	{
		List<CompetencyMaster> competencyMasters = null;
		try{
			Criterion criterion = Restrictions.eq("status", "a");
			Criterion criterion1 = Restrictions.eq("domainMaster", domainMaster);
			competencyMasters = findByCriteria(Order.asc("competencyName"), criterion,criterion1);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return competencyMasters;
		
	}
/* @Author: Gagan 
 * @Discription: It is used to check new competency name is existing or not in database.
 */	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<CompetencyMaster> checkDuplicateCompetencyName(DomainMaster domainMaster,String competencyName,Integer competencyId) 
	{
		Session session = getSession();

		if(competencyId!=null)
		{
			List result = session.createCriteria(getPersistentClass())       
			//.add(Restrictions.eq("domainMaster", domainMaster)) 
			.add(Restrictions.not(Restrictions.in("competencyId",new Integer[]{competencyId}))) 
			.add(Restrictions.eq("competencyName", competencyName)) 
			.add(Restrictions.eq("domainMaster.domainId", domainMaster.getDomainId())) 
			.list();
			return result;

		}
		else
		{
			List result = session.createCriteria(getPersistentClass())       
			//.add(Restrictions.eq("domainMaster", domainMaster)) 
			.add(Restrictions.eq("competencyName", competencyName)) 
			.add(Restrictions.eq("domainMaster.domainId", domainMaster.getDomainId())) 
			.list();
			return result;
		}
	}
	/* @Discription: It is used to check new Rank is existing or not in database.*/
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<CompetencyMaster> checkDuplicateRank(DomainMaster domainMaster,Short rank,Integer competencyId) 
	{
		Session session = getSession();

		if(competencyId!=null)
		{
			List result = session.createCriteria(getPersistentClass())       
			//.add(Restrictions.eq("domainMaster", domainMaster)) 
			.add(Restrictions.not(Restrictions.in("competencyId",new Integer[]{competencyId}))) 
			.add(Restrictions.eq("rank", rank)) 
			.add(Restrictions.eq("domainMaster.domainId", domainMaster.getDomainId())) 
			.list();
			return result;

		}
		else
		{
			List result = session.createCriteria(getPersistentClass())       
			//.add(Restrictions.eq("domainMaster", domainMaster)) 
			.add(Restrictions.eq("rank", rank)) 
			.add(Restrictions.eq("domainMaster.domainId", domainMaster.getDomainId())) 
			.list();
			return result;
		}
	}
	
	@Transactional(readOnly=false)
	public List<CompetencyMaster> displayCompetencies(DomainMaster domainMaster)
	{
		List<CompetencyMaster> competencyMasters = null;
		try{
			Criterion criterion = Restrictions.eq("status", "a");
			Criterion criterion1 = Restrictions.eq("domainMaster", domainMaster);
			competencyMasters = findByCriteria(Order.asc("competencyName"), criterion,criterion1);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return competencyMasters;
		
	}
/* @Author: Vishwanath Kumar
 * @Discription: It is used to get Competencies For PDR .
 */	
	@Transactional(readOnly=false)
	public List<CompetencyMaster> getCompetenciesForPDR(boolean displayInPDReport)
	{
		List<CompetencyMaster> competencyMasters = null;
		try{
			Criterion criterion = Restrictions.eq("status", "a");
			Criterion criterion1 = Restrictions.eq("displayInPDReport", displayInPDReport);
			competencyMasters = findByCriteria(Order.asc("competencyName"), criterion,criterion1);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return competencyMasters;
	}
	
	@Transactional(readOnly=true)
	public List<CompetencyMaster> findCompetenciesByDomainAndUId(DomainMaster domainMaster, String competencyUId)
	{
		List<CompetencyMaster> competencyMasters = null;
		try{
			Criterion criterion1 = Restrictions.eq("domainMaster", domainMaster);
			Criterion criterion2 = Restrictions.eq("competencyUId", competencyUId);
			competencyMasters = findByCriteria(criterion1,criterion2);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return competencyMasters;
	}
}
