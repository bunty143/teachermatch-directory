package tm.dao.master;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SchoolMasterDAO extends GenericHibernateDAO<SchoolMaster, Long>
{
	public SchoolMasterDAO() 
	{
		super(SchoolMaster.class);
	}
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}
	@Transactional(readOnly=false)
	public List<SchoolMaster> findDistrictSchoolId(DistrictMaster districtMaster){
		List<SchoolMaster> lstSchoolMaster= null;
		try 
		{
			List<DistrictSchools> lstDistrictSchools=null;
			Criterion criterionDistrict = Restrictions.eq("districtMaster",districtMaster);
			lstDistrictSchools =	districtSchoolsDAO.findByCriteria(criterionDistrict);
			List<Long> schoolIds = new ArrayList<Long>();
			for(DistrictSchools districtSchools: lstDistrictSchools){
				schoolIds.add(districtSchools.getSchoolMaster().getSchoolId());
			}
			if(schoolIds!=null && schoolIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("schoolId",schoolIds);			
				lstSchoolMaster = findByCriteria(criterion1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstSchoolMaster;
	}
	@Transactional(readOnly=false)
	public boolean activateDeactivateSchool(Long schoolId,String status){
		try 
		{
			SchoolMaster schoolMaster = findById(schoolId, false, false);
			schoolMaster.setStatus(status);
			updatePersistent(schoolMaster);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return true;
	}
	
	@Transactional(readOnly=false)
	public SchoolMaster validateSchoolByAuthkey(String authKey)
	{
		List<SchoolMaster> lstSchoolMaster = null;
		try 
		{
			Criterion criterion2 = Restrictions.eq("authKey",authKey);
			lstSchoolMaster = findByCriteria(criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstSchoolMaster==null || lstSchoolMaster.size()==0)
			return null;
		else
			return lstSchoolMaster.get(0);	
	}

	@Transactional(readOnly=true)
	public List<SchoolMaster> findBySchool(Order order,int startPos,int limit,Criterion... criterion)
	{
		List<SchoolMaster> lstSchoolMaster = null;
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			
			Criteria cc = criteria.createCriteria("districtId");	
			if(order!=null && order.toString().contains("districtName")){
				cc.addOrder(order);
			}
			else if(order != null){
				criteria.addOrder(order);
			}
			lstSchoolMaster = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolMaster;
	}
	
	@Transactional(readOnly=false)
	public SchoolMaster findBySchoolName(String schoolName, DistrictMaster districtMaster)
	{
		List<SchoolMaster> lstSchoolMaster = null;
		try{
			
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.eq("schoolName",schoolName);
			
			lstSchoolMaster = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstSchoolMaster==null || lstSchoolMaster.size()==0)
			return null;
		else
			return lstSchoolMaster.get(0);	
	}
	
	
	@Transactional(readOnly=false)
	public List<SchoolMaster> findDistrictsWeeklyCgReportRequired(Integer val)
	{
		List<SchoolMaster> lstSchoolMaster = null;
		try 
		{		
			Criterion criterion2 = Restrictions.eq("isWeeklyCgReport",val);
			lstSchoolMaster = findByCriteria(criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolMaster;
	}
@Transactional(readOnly=false)
	public SchoolMaster findBySchoolNameWithDistrictId(String schoolName,DistrictMaster districtMaster)
	{
		List<SchoolMaster> lstSchoolMaster = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("schoolName",schoolName);
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion3 = Restrictions.eq("status","A");
			lstSchoolMaster = findByCriteria(criterion1,criterion2,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstSchoolMaster==null || lstSchoolMaster.size()==0)
			return null;
		else
			return lstSchoolMaster.get(0);	
	}


/* @Start
 * @Ashish Kumar
 * @Description :: Find Data By DistrictId and ZipCode and SchoolID and State
 * */

	// Find School List By DistrictID and ZipCOde List
	@Transactional(readOnly=false)
	public List<SchoolMaster> findByDistrictIdAndZipCode(List<String> zipCodeList,DistrictMaster districtMaster)
	{
		List<SchoolMaster> lstSchoolMaster = null;
		//List<Integer> zipCode = new ArrayList<Integer>();
		
		try 
		{
			/*for(int i=0;i<zipCodeList.size();i++){
				zipCode.add(new Integer(zipCodeList.get(i)));
			}*/
			
			Criterion criterion1 = Restrictions.in("zip",zipCodeList);
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion3 = Restrictions.eq("status","A");
			lstSchoolMaster = findByCriteria(criterion1,criterion2,criterion3);	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstSchoolMaster==null || lstSchoolMaster.size()==0)
			return null;
		else
			return lstSchoolMaster;	
	}
	
	// Find By Data by School ID and ZipCOde
	@Transactional(readOnly=false)
	public SchoolMaster findBySchoolIdAndZipCode(String zipCode,Long schoolId)
	{
		List<SchoolMaster> schoolMasterlst =  null;
		
		try 
		{
			//int zip = new Integer(zipCode);
			
			Criterion criterion1 = Restrictions.eq("zip",zipCode);
			Criterion criterion2 = Restrictions.eq("schoolId",schoolId);
			Criterion criterion3 = Restrictions.eq("status","A");
			schoolMasterlst = findByCriteria(criterion1,criterion2,criterion3);	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(schoolMasterlst!=null && schoolMasterlst.size()>0){
			return schoolMasterlst.get(0);
		}else{
			return null;
		}
			
	}
	
	// Find school data by state and district
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolListByStateAndDistrict(StateMaster stateMaster,DistrictMaster districtMaster){
		
		List<SchoolMaster> schoolMasterlst =  null;
		
		try{
			
			Criterion criterion1 = Restrictions.eq("stateMaster",stateMaster);
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
			schoolMasterlst = findByCriteria(criterion1,criterion2);
		}catch(Exception e){
			e.printStackTrace();
		}
		if(schoolMasterlst==null || schoolMasterlst.size()==0)
			return null;
		else
			return schoolMasterlst;	
	}
	
	@Transactional(readOnly=false)
	public List<SchoolMaster> findByZipCodeList(List<String> zipCode)
	{
		
		List<SchoolMaster> schoolMasterlst =  new ArrayList<SchoolMaster>();
		/*List<Integer> zCode = new ArrayList<Integer>();
	
		for(int i=0; i<zipCode.size();i++){
			zCode.add(new Integer(zipCode.get(i)));
		}
*/		
		try
		{
			Criterion criterion1 = Restrictions.in("zip",zipCode);
			schoolMasterlst = findByCriteria(criterion1);
			
		}catch (Exception e){
			e.printStackTrace();
		}
		if(schoolMasterlst.size()>0)
			return schoolMasterlst;
		else
			return null;
	}
	
	@Transactional(readOnly=false)
	public SchoolMaster checkSchoolByLocationCode(String locationCode)
	{
		List<SchoolMaster> lstSchoolMaster = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("status","A");
			Criterion criterion2 = Restrictions.eq("locationCode",locationCode);
			lstSchoolMaster = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstSchoolMaster==null || lstSchoolMaster.size()==0)
			return null;
		else
			return lstSchoolMaster.get(0);	
	}
	
	
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolListByDistrict(DistrictMaster districtMaster){
		
		List<SchoolMaster> schoolMasterlst =  new ArrayList<SchoolMaster>();
		
		try{
			
		//	Criterion criterion1 = Restrictions.isNotEmpty("locationCode");
			Criterion criterion3 = Restrictions.isNotNull("locationCode");
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
			//Criterion criterion4 = Restrictions.or(criterion1, criterion3);
			schoolMasterlst = findByCriteria(criterion2,criterion3);
		}catch(Exception e){
			e.printStackTrace();
		}
		
			return schoolMasterlst;	
	}
	
	@Transactional(readOnly=false)
	public SchoolMaster findByschoolName(String schoolName){		
		List<SchoolMaster> schoolMasterlst =null;	
		SchoolMaster master=null;
		try{			
			Criterion criterion1 = Restrictions.eq("schoolName",schoolName);		
			schoolMasterlst =  findByCriteria(criterion1);
		}catch(Exception e){
			e.printStackTrace();
		}		
		if(schoolMasterlst.size()!=0)
			master=schoolMasterlst.get(0);
		
		return master;	
	}
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolMasters(List<Long> schoolIds){
		List<SchoolMaster> lstSchoolMaster= new ArrayList<SchoolMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.in("schoolId",schoolIds);			
			lstSchoolMaster = findByCriteria(criterion1);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstSchoolMaster;
	}
	@Transactional(readOnly=false)
	public SchoolMaster checkSchoolByLocationCodeAndDistrict(String locationCode,DistrictMaster districtMaster)
	{
		List<SchoolMaster> lstSchoolMaster = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("status","A");
			Criterion criterion2 = Restrictions.eq("locationCode",locationCode);
			Criterion criterion3 = Restrictions.eq("districtMaster",districtMaster);
			lstSchoolMaster = findByCriteria(criterion1,criterion2,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstSchoolMaster==null || lstSchoolMaster.size()==0)
			return null;
		else
			return lstSchoolMaster.get(0);	
	}
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolMastersByDistrict(List<Long> schoolIds,DistrictMaster districtMaster){
		List<SchoolMaster> lstSchoolMaster= new ArrayList<SchoolMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.in("schoolId",schoolIds);	
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
			lstSchoolMaster = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstSchoolMaster;
	}
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolMastersByLocatonCode(List<String> locationCodes){
		List<SchoolMaster> lstSchoolMaster= new ArrayList<SchoolMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.in("locationCode",locationCodes);			
			lstSchoolMaster = findByCriteria(criterion1);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstSchoolMaster;
	}
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolMastersByDistrictAndLocation(List<String> locationCodes,DistrictMaster districtMaster){
		List<SchoolMaster> lstSchoolMaster= new ArrayList<SchoolMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.in("locationCode",locationCodes);	
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
			lstSchoolMaster = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstSchoolMaster;
	}
	
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolsByDistricts(List<DistrictMaster> districtMasters){
		List<SchoolMaster> lstSchoolMaster= null;
		try 
		{
			Criterion criterionDistrict = Restrictions.in("districtId",districtMasters);
			lstSchoolMaster = findByCriteria(criterionDistrict);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstSchoolMaster;
	}
}
