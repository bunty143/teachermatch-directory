package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictKeyContactDAO extends GenericHibernateDAO<DistrictKeyContact, Integer>
{
	public DistrictKeyContactDAO() 
	{
		super(DistrictKeyContact.class);
	}
	
	@Transactional(readOnly=false)
	public List<DistrictKeyContact> findDistrictsWeeklyCgReportRequired(DistrictMaster districtMaster)
	{
		List<DistrictKeyContact> districtKeyContactList = null;
		try 
		{	
			Criterion criterion1 = Restrictions.isNotNull("keyContactEmailAddress");
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			districtKeyContactList = findByCriteria(criterion2,criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return districtKeyContactList;
	}
	
	@Transactional(readOnly=false)
	public String findEmail(Integer keyContactId)
	{
		String email="";
		List<DistrictKeyContact> districtKeyContactList = null;
		try 
		{	
			Criterion criterion1 = Restrictions.eq("keyContactId",keyContactId);
			districtKeyContactList = findByCriteria(criterion1);
			if(districtKeyContactList!=null)
			email=districtKeyContactList.get(0).getKeyContactEmailAddress();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return email;
	}
	@Transactional(readOnly=false)
	public int checkEmail(int flag,Integer keyContactId,String email,DistrictMaster districtMaster)
	{
		int size=0;
		List<DistrictKeyContact> districtKeyContactList = null;
		try 
		{	
			Criterion criterion1 = Restrictions.eq("keyContactEmailAddress",email);
			Criterion criterion2 = Restrictions.ne("keyContactId",keyContactId);
			Criterion criterion3 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			if(flag==1)
				districtKeyContactList = findByCriteria(criterion1,criterion2,criterion3);
			else
				districtKeyContactList = findByCriteria(criterion1,criterion3);
			size=districtKeyContactList.size();
			System.out.println("size::::::::::::::::::"+size);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return size;
	}
	@Transactional(readOnly=false)
	public int checkkeyContactType(ContactTypeMaster keyContactTypeId,DistrictMaster districtMaster)
	{
			int size=0;
			List<DistrictKeyContact> districtKeyContactList = null;		
			Criterion criterion1 = Restrictions.eq("keyContactTypeId",keyContactTypeId);
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());			
			districtKeyContactList = findByCriteria(criterion1,criterion2);
			size=districtKeyContactList.size();
			System.out.println("size======== "+size);				
		    return size;
	}	
	@Transactional(readOnly=false)
	public List<DistrictKeyContact> findKeyContacts(String email)
	{
		List<DistrictKeyContact> districtKeyContactList = null;
		try 
		{	
			Criterion criterion = Restrictions.eq("keyContactEmailAddress",email);
			districtKeyContactList = findByCriteria(criterion);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return districtKeyContactList;
	}
	
	
	@Transactional(readOnly=false)
	public List<DistrictKeyContact> getHRList(ContactTypeMaster contactTypeMaster,Integer districtId)
	{
		List<DistrictKeyContact> districtKeyContactList = null;
		try 
		{	
			Criterion criterion1 = Restrictions.eq("districtId",districtId);
			Criterion criterion2 = Restrictions.eq("keyContactTypeId",contactTypeMaster);
			districtKeyContactList = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return districtKeyContactList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictKeyContact> findOfferAccepted(DistrictMaster districtMaster)
	{
		List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();;
		try 
		{	
			if(districtMaster!=null){
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				
				Criterion criterion1 = Restrictions.isNotNull("keyContactEmailAddress");
				Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());
				Criterion criterion3 = Restrictions.eq("contactType","Offer Accepted");
				
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.createCriteria("keyContactTypeId").add(criterion3);
				districtKeyContactList =criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return districtKeyContactList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictKeyContact> findByContactType(DistrictMaster districtMaster,String contactType)
	{
		List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
		try 
		{	
			if(districtMaster!=null){
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				
				Criterion criterion1 = Restrictions.isNotNull("keyContactEmailAddress");
				Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());
				Criterion criterion3 = Restrictions.eq("contactType",contactType);
				
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.createCriteria("keyContactTypeId").add(criterion3);
				districtKeyContactList =criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return districtKeyContactList;
	}

	
	
	// For specific contact type users
	@Transactional(readOnly=false)
	public List<DistrictKeyContact> findByContactTypeAndDistrict(UserMaster usermaster,ContactTypeMaster canTypeMaster)
	{
		List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();;
		try 
		{	if(usermaster!=null){
			
			Integer districtId = 0;
			if(usermaster.getDistrictId()!=null)
				districtId = usermaster.getDistrictId().getDistrictId();
			
				Criterion criterion1 = Restrictions.isNotNull("keyContactEmailAddress");
				Criterion criterion2 = Restrictions.eq("districtId",districtId);
				Criterion criterion3 = Restrictions.eq("keyContactTypeId",canTypeMaster);
				Criterion criterion4 = Restrictions.eq("userMaster",usermaster);
				districtKeyContactList =  findByCriteria(criterion1,criterion2,criterion3,criterion4);
			}
		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return districtKeyContactList;
	}
	
	
	@Transactional(readOnly=false)
	public DistrictKeyContact findByUserId(UserMaster master)
	{
		List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();	
		Criterion criterion1 = Restrictions.eq("userMaster",master);
		districtKeyContactList=findByCriteria(criterion1);
		DistrictKeyContact districtKeyContact=null;
		if(districtKeyContactList.size()>0)
		{
			districtKeyContact=districtKeyContactList.get(0);
		}
		   return districtKeyContact;
		
	}
	
	@Transactional(readOnly=false)
	public List<DistrictKeyContact> findByContactType(UserMaster usermaster,String contactType)
	{
		List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
		try 
		{	
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			
			Criterion criterion1 = Restrictions.isNotNull("keyContactEmailAddress");
			Criterion criterion2 = Restrictions.eq("userMaster",usermaster);
			Criterion criterion3 = Restrictions.eq("contactType",contactType);
			
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.createCriteria("keyContactTypeId").add(criterion3);
			districtKeyContactList =criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return districtKeyContactList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictKeyContact> findByUserMasterAndContactType(UserMaster usermasterList,String contactType)
	{
		List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
		try 
		{	
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			
			
			Criterion criterion1 = Restrictions.isNotNull("keyContactEmailAddress");
			Criterion criterion2 = Restrictions.eq("districtId",usermasterList.getDistrictId().getDistrictId());
			Criterion criterion3 = Restrictions.eq("userMaster",usermasterList);
			Criterion criterion4 = Restrictions.eq("contactType",contactType);
			Criterion criterion5 = Restrictions.eq("status","A");
			
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.createCriteria("keyContactTypeId").add(criterion4).add(criterion5);
			
			districtKeyContactList =criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return districtKeyContactList;
	}
	
	@Transactional(readOnly=false)
	public int checkEmailBranchMaster(int flag,Integer keyContactId,String email,BranchMaster branchMaster)
	{
		int size=0;
		List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
		try 
		{	
			Criterion criterion1 = Restrictions.eq("keyContactEmailAddress",email);
			Criterion criterion2 = Restrictions.ne("keyContactId",keyContactId);
			Criterion criterion3 = Restrictions.eq("branchMaster",branchMaster);
			if(flag==1)
				districtKeyContactList = findByCriteria(criterion1,criterion2,criterion3);
			else
				districtKeyContactList = findByCriteria(criterion1,criterion3);
			size=districtKeyContactList.size();
			System.out.println("size::::::::::::::::::"+size);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return size;
	}
	
	@Transactional(readOnly=false)
	public int checkEmailHeadQuarterMaster(int flag,Integer keyContactId,String email,HeadQuarterMaster headQuarterMaster)
	{
		int size=0;
		List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
		try 
		{	
			Criterion criterion1 = Restrictions.eq("keyContactEmailAddress",email);
			Criterion criterion2 = Restrictions.ne("keyContactId",keyContactId);
			Criterion criterion3 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			if(flag==1)
				districtKeyContactList = findByCriteria(criterion1,criterion2,criterion3);
			else
				districtKeyContactList = findByCriteria(criterion1,criterion3);
			if(districtKeyContactList!=null && districtKeyContactList.size()>0)
				size=districtKeyContactList.size();
			System.out.println("size::::::::::::::::::"+size);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return size;
	}
	
	@Transactional(readOnly=false)
	public int checkkeyContactTypeBranchMaster(ContactTypeMaster keyContactTypeId,BranchMaster branchMaster)
	{
			int size=0;
			List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();		
			Criterion criterion1 = Restrictions.eq("keyContactTypeId",keyContactTypeId);
			Criterion criterion2 = Restrictions.eq("branchMaster",branchMaster);			
			districtKeyContactList = findByCriteria(criterion1,criterion2);
			if(districtKeyContactList!=null && districtKeyContactList.size()>0)
				size = districtKeyContactList.size();
			System.out.println("size======== "+size);				
		    return size;
	}	
	
	@Transactional(readOnly=false)
	public int checkkeyContactTypeHeadQuarterMaster(ContactTypeMaster keyContactTypeId,HeadQuarterMaster headQuarterMaster)
	{
			int size=0;
			List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();		
			Criterion criterion1 = Restrictions.eq("keyContactTypeId",keyContactTypeId);
			Criterion criterion2 = Restrictions.eq("headQuarterMaster",headQuarterMaster);			
			districtKeyContactList = findByCriteria(criterion1,criterion2);
			if(districtKeyContactList!=null && districtKeyContactList.size()>0)
				size=districtKeyContactList.size();
			System.out.println("size======== "+size);				
		    return size;
	}	
	@Transactional(readOnly=false)
	public int checkkeyContactType(ContactTypeMaster keyContactTypeId,DistrictMaster districtMaster,String keyContactEmailAddress)
	{
			int size=0;
			List<DistrictKeyContact> districtKeyContactList = null;		
			Criterion criterion1 = Restrictions.eq("keyContactTypeId",keyContactTypeId);
			Criterion criterion3 = Restrictions.eq("keyContactEmailAddress",keyContactEmailAddress);//shriram
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());			
			districtKeyContactList = findByCriteria(criterion1,criterion2,criterion3);
			size=districtKeyContactList.size();
			System.out.println("size======== "+size);				
		    return size;
	}
	@Transactional(readOnly=false)
	public int checkkeyContactTypeContact(ContactTypeMaster keyContactTypeId,DistrictMaster districtMaster,String keyContactEmailAddress,Integer keyContactId)
	{
			int size=0;
			List<DistrictKeyContact> districtKeyContactList = null;		
			Criterion criterion1 = Restrictions.eq("keyContactTypeId",keyContactTypeId);
			Criterion criterion3 = Restrictions.eq("keyContactEmailAddress",keyContactEmailAddress);//shriram
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			if(keyContactId!=null){
			 Criterion criterion4 = Restrictions.ne("keyContactId",keyContactId);
			 districtKeyContactList = findByCriteria(criterion1,criterion2,criterion3,criterion4);
			}else{
				
				districtKeyContactList = findByCriteria(criterion1,criterion2,criterion3);
			}
			
			size=districtKeyContactList.size();
			System.out.println(":::::::::::::::::::::::size======== "+size);				
		    return size;
	}
	
	
	@Transactional(readOnly=false)
		public int checkEmailContact(int flag,ContactTypeMaster keyContactTypeId,String email,DistrictMaster districtMaster,Integer keyContactId)
		{
			System.out.println("::::::::::::::::keyContactTypeId:---"+keyContactTypeId.getContactTypeId()+"\t email"+email+"\tdistrictMasterId"+districtMaster.getDistrictId()+"\tkeyContactId="+keyContactId);
			int size=0;
			List<DistrictKeyContact> districtKeyContactList = null;
			try 
			{	
				Criterion criterion1 = Restrictions.eq("keyContactEmailAddress",email);
				Criterion criterion2 = Restrictions.eq("keyContactTypeId",keyContactTypeId);//shriram
				Criterion criterion3 = Restrictions.eq("districtId",districtMaster.getDistrictId());
				Criterion criterion4 = Restrictions.ne("keyContactId",keyContactId);
				

				if(flag==1){
					if(keyContactId!=null)
					districtKeyContactList = findByCriteria(criterion1,criterion2,criterion3,criterion4);
					else{
						districtKeyContactList = findByCriteria(criterion1,criterion2,criterion3);
					}
				}
				else{
					if(keyContactId!=null)
					districtKeyContactList = findByCriteria(criterion1,criterion3,criterion4);
					else
					districtKeyContactList = findByCriteria(criterion1,criterion3);	
				}
				
				size=districtKeyContactList.size();
				System.out.println("size::::::::::::::::::"+size);
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return size;
		}
	@Transactional(readOnly=false)
	public List<DistrictKeyContact> checkEmailAndContactTypeId(String email,List<ContactTypeMaster> listContactTypeId,Integer districtId)
	{
		List<DistrictKeyContact> districtKeyContactList = null;
			try{
				Criterion criterion1 = Restrictions.eq("keyContactEmailAddress",email);
				Criterion criterion2 = Restrictions.eq("districtId",districtId);
				if(listContactTypeId!=null)
				{
					Criterion criterion3 =  Restrictions.not(Restrictions.in("keyContactTypeId",listContactTypeId));
					districtKeyContactList=	findByCriteria(criterion1,criterion2,criterion3);
				} else {
					districtKeyContactList=	findByCriteria(criterion1,criterion2);
				}
					
				
			} catch(Exception ex)
			{
				
			}
		
		return districtKeyContactList;
	}
	@Transactional(readOnly=false)
	public List<DistrictKeyContact> findByContactTypeAndEmail(String email,DistrictMaster districtMaster)
	{
		List<DistrictKeyContact> districtKeyContactList=null;
		try{
			Criterion criterion1 = Restrictions.eq("keyContactEmailAddress",email);
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			districtKeyContactList=	findByCriteria(criterion1,criterion2);

		} catch(Exception ex){
			
		}
		
		return districtKeyContactList;
	}
}
