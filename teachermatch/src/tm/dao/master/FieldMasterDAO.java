package tm.dao.master;

import tm.bean.master.FieldMaster;
import tm.dao.generic.GenericHibernateDAO;

public class FieldMasterDAO extends GenericHibernateDAO<FieldMaster, Integer> 
{
	public FieldMasterDAO() 
	{
		super(FieldMaster.class);
	}

}
