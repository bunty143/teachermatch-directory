package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.SdpCodesWithCertificates;
import tm.bean.master.StateMaster;
import tm.dao.generic.GenericHibernateDAO;

/**
 * @author Amit
 * @date 20-Apr-15
 */
public class SdpCodesWithCertificatesDAO extends GenericHibernateDAO<SdpCodesWithCertificates, Integer> {

	public SdpCodesWithCertificatesDAO() {
		super(SdpCodesWithCertificates.class);
	}
	
	@Transactional(readOnly=false)
	public List<SdpCodesWithCertificates> findSdpCodesBySate(StateMaster stateMaster)
	{
		List<SdpCodesWithCertificates> sdpCodesWithCertificateList = new ArrayList<SdpCodesWithCertificates>();
		try
		{
			Criterion criterion1 = Restrictions.eq("stateMaster",stateMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			sdpCodesWithCertificateList = findByCriteria(criterion1, criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return sdpCodesWithCertificateList;
	}

}