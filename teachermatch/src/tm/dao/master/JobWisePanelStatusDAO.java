package tm.dao.master;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobWisePanelStatusDAO extends GenericHibernateDAO<JobWisePanelStatus,Integer> {
	public JobWisePanelStatusDAO(){
		super(JobWisePanelStatus.class);
	}
	
	@Transactional(readOnly=false)
	public List<JobWisePanelStatus> checkStatusJobOrderWise(JobOrder jobOrder){
		List<JobWisePanelStatus> list=null;
		try{
			Criterion criterion1=Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion2=Restrictions.isNotNull("statusMaster");
			list=findByCriteria(criterion1,criterion2);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobWisePanelStatus> checkSecStatusJobOrderWise(JobOrder jobOrder){
		List<JobWisePanelStatus> list=null;
		try{
			Criterion criterion1=Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion2=Restrictions.isNotNull("secondaryStatus");
			list=findByCriteria(criterion1,criterion2);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobWisePanelStatus> getPanel(JobOrder jobOrder,SecondaryStatus secondaryStatus,StatusMaster statusMaster)
	{
		List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
		try{
			Criterion criterionStatus=null;
			if(statusMaster!=null)
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			else
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			
			Criterion criterion_jobOrder=null;
			if(jobOrder!=null)
				criterion_jobOrder= Restrictions.eq("jobOrder",jobOrder);
			
			Criterion criterion_panelStatus=Restrictions.eq("panelStatus",new Boolean(true));
			jobWisePanelStatusList=findByCriteria(criterion_jobOrder,criterionStatus,criterion_panelStatus);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobWisePanelStatusList;
		
	}
	
	
	@Transactional(readOnly=false)
	public List<JobWisePanelStatus> getPanelByJobAndSSID(JobOrder jobOrder,List<SecondaryStatus> lstsecondaryStatus)
	{
		List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
		try{
			Criterion criterionStatus=null;
			criterionStatus = Restrictions.in("secondaryStatus", lstsecondaryStatus);
			
			Criterion criterion_jobOrder=null;
			if(jobOrder!=null)
				criterion_jobOrder= Restrictions.eq("jobOrder",jobOrder);
			
			Criterion criterion_panelStatus=Restrictions.eq("panelStatus",new Boolean(true));
			jobWisePanelStatusList=findByCriteria(criterion_jobOrder,criterionStatus,criterion_panelStatus);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobWisePanelStatusList;
		
	}
	
	@Transactional(readOnly=false)
	public List<JobWisePanelStatus> getPanelByJob(JobOrder jobOrder)
	{
		List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
		try{
			Criterion criterion_jobOrder=null;
			if(jobOrder!=null)
				criterion_jobOrder= Restrictions.eq("jobOrder",jobOrder);
			
			Criterion criterion_panelStatus=Restrictions.eq("panelStatus",new Boolean(true));
			jobWisePanelStatusList=findByCriteria(criterion_jobOrder,criterion_panelStatus);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobWisePanelStatusList;
	}
	
	
	@Transactional(readOnly=false)
	public JobWisePanelStatus findJobWisePanelStatusObj(JobOrder jobOrder)
	{
		List<JobWisePanelStatus> list= new ArrayList<JobWisePanelStatus>();
		JobWisePanelStatus jqp=null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion_panelStatus=Restrictions.eq("panelStatus",new Boolean(true));
			Criterion criterion_jobOrder= Restrictions.eq("jobOrder",jobOrder);
			criteria.add(criterion_panelStatus);
			criteria.add(criterion_jobOrder);
			criteria.createCriteria("secondaryStatus").add(Restrictions.eq("secondaryStatusName","Offer Ready"));
			list = criteria.list();
			if(list.size()>0){
				jqp=list.get(0);
			}

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jqp;
	}
	
	@Transactional(readOnly=false)
	public List<JobWisePanelStatus> findJobWisePanelStatusList(List<JobOrder> jobOrders)
	{
		List<JobWisePanelStatus> list= new ArrayList<JobWisePanelStatus>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion_panelStatus=Restrictions.eq("panelStatus",new Boolean(true));
			Criterion criterion_jobOrder= Restrictions.in("jobOrder",jobOrders);
			criteria.add(criterion_panelStatus);
			criteria.add(criterion_jobOrder);
			criteria.createCriteria("secondaryStatus").add(Restrictions.eq("secondaryStatusName","Offer Ready"));
			list = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobWisePanelStatus> getPanelForJobs(List<JobOrder> jobOrders,SecondaryStatus secondaryStatus)
	{
		List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
		try
		{
			Criterion criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			Criterion criterion_jobOrder= Restrictions.in("jobOrder",jobOrders);
			Criterion criterion_panelStatus=Restrictions.eq("panelStatus",new Boolean(true));
			jobWisePanelStatusList=findByCriteria(criterion_jobOrder,criterionStatus,criterion_panelStatus);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return jobWisePanelStatusList;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobWisePanelStatus> getPanelCGView(JobOrder jobOrder,SecondaryStatus secondaryStatus)
	{
		List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
		try{
			if(secondaryStatus!=null)
			{
				Criterion criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
				Criterion criterion_jobOrder=null;
				if(jobOrder!=null)
					criterion_jobOrder= Restrictions.eq("jobOrder",jobOrder);
				
				Criterion criterion_panelStatus=Restrictions.eq("panelStatus",new Boolean(true));
				jobWisePanelStatusList=findByCriteria(criterion_jobOrder,criterionStatus,criterion_panelStatus);
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobWisePanelStatusList;
		
	}
	
	//added by ram nath
	@Transactional(readOnly=false)
	public JobWisePanelStatus findJobWisePanelStatusObj(JobOrder jobOrder,String status)
	{
		List<JobWisePanelStatus> list= new ArrayList<JobWisePanelStatus>();
		JobWisePanelStatus jqp=null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion_panelStatus=Restrictions.eq("panelStatus",new Boolean(true));
			Criterion criterion_jobOrder= Restrictions.eq("jobOrder",jobOrder);
			criteria.add(criterion_panelStatus);
			criteria.add(criterion_jobOrder);
			criteria.createCriteria("secondaryStatus").add(Restrictions.eq("secondaryStatusName",status));
			list = criteria.list();
			if(list.size()>0){
				jqp=list.get(0);
			}

		} 
		catch (Throwable e) {
			e.printStackTrace();
		}		
		return jqp;
	}	
	//end by RAm nath
	
	
	// Anurag Optimization
	
	@Transactional(readOnly=false)
	public Map<Integer,String> jobwisePanelStatusList(Criterion...criterions){
		Map<Integer,String> result = new HashMap<Integer, String>();
		List<Object> data = new ArrayList<Object>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			for(Criterion cr :criterions){
				criteria.add(cr);
			}
			criteria.setProjection(Projections.projectionList()
					.add(Projections.property("jobPanelStatusId"))
					.add(Projections.property("statusMaster.statusId"))
					.add(Projections.property("secondaryStatus.secondaryStatusId"))
					.add(Projections.property("panelStatus"))
					);
			
			data = criteria.list();
			
			Integer status = null;
			Integer secStatus =null;
			 if(data.size()>0){
                 for (Iterator it = data.iterator(); it.hasNext();) {
                	 Object[] row = (Object[]) it.next() ;    
                	 if(row[1]!=null) status = Integer.parseInt(row[1].toString()); else status = null;
                	 if(row[2]!=null) secStatus = Integer.parseInt(row[2].toString()); else secStatus = null;
                                                                              
                 result.put(Integer.parseInt(row[0].toString()),""+status+"##"+secStatus+"##"+((Boolean)row[3]));
             }               
            }
			 
			 
			 
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	@Transactional(readOnly=false)
	public List<JobWisePanelStatus> getPanelByJobOp(JobOrder jobOrder)
	{
		List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
		try{
			Criterion criterion_jobOrder=null;
			if(jobOrder!=null)
				criterion_jobOrder= Restrictions.eq("jobOrder",jobOrder);
			
			Criterion criterion_panelStatus=Restrictions.eq("panelStatus",new Boolean(true));
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion_jobOrder).add(criterion_panelStatus);
			criteria.setProjection(Projections.projectionList()
					.add(Projections.property("jobPanelStatusId"))
					.add(Projections.property("statusMaster.statusId"))
					.add(Projections.property("secondaryStatus.secondaryStatusId"))
					.add(Projections.property("panelStatus"))
					);
			List<String []> jobWisePanelStatusTemp=new ArrayList<String []>();
			jobWisePanelStatusTemp = criteria.list();
			
			for (Iterator it = jobWisePanelStatusTemp.iterator(); it.hasNext();)
			{
				Object[] row = (Object[]) it.next();    				
				if(row[0]!=null){
					JobWisePanelStatus tempJobWise = new JobWisePanelStatus();					
					tempJobWise.setJobPanelStatusId(Integer.parseInt(row[0].toString()));
					if(row[1]!=null){
						StatusMaster statusMaster = new StatusMaster();
						statusMaster.setStatusId(Integer.parseInt(row[1].toString()));
						tempJobWise.setStatusMaster(statusMaster);
					}
					
					if(row[2]!=null){
						SecondaryStatus tempSec = new SecondaryStatus();
							tempSec.setSecondaryStatusId(Integer.parseInt(row[2].toString()));
							tempJobWise.setSecondaryStatus(tempSec);
					}
					
					if(row[3]!=null){
						tempJobWise.setPanelStatus(Boolean.parseBoolean(row[3].toString()));
					}
					jobWisePanelStatusList.add(tempJobWise);
				}
			}
	//		jobWisePanelStatusList=findByCriteria(criterion_jobOrder,criterion_panelStatus);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobWisePanelStatusList;
	}

	@Transactional(readOnly=false)
	public List<JobWisePanelStatus> checkSecStatusJobOrderWise_Op(JobOrder jobOrder)
	{
 		 List<JobWisePanelStatus> list=new ArrayList<JobWisePanelStatus>();
		try 
		{
  			Query query1=null;
  			List<Object[]> objList=null;
			if(jobOrder!=null){
 				Session session = getSession();
  				String hql=	"SELECT  jWP.secondaryStatusId,jWP.panelStatus   FROM  jobwisepanelstatus jWP "
			 	    +   "left join joborder jo on jo.jobId= jWP.jobId "
			 	   +   "left join secondarystatus ss on ss.secondaryStatusId= jWP.secondaryStatusId "
				    + " WHERE   jWP.jobId=? and jWP.secondaryStatusId  IS NOT NULL ";
 				query1=session.createSQLQuery(hql);
 				query1.setParameter(0, jobOrder.getJobId());
 				objList = query1.list(); 
  		 		JobWisePanelStatus obj=null;
				if(objList.size()>0 && objList!=null ){
				for (int j = 0; j < objList.size(); j++) {
						Object[] objArr2 = objList.get(j);
						obj = new JobWisePanelStatus();
						int secondaryStatusId=objArr2[0] == null ? 0 : Integer.parseInt(objArr2[0].toString());
						SecondaryStatus secondaryStatus=new SecondaryStatus();
			 				
			 				secondaryStatus.setSecondaryStatusId(secondaryStatusId);
			 				obj.setSecondaryStatus(secondaryStatus);
						   
			           		/*	else{
			           				obj.setSecondaryStatus(secondaryStatus);
						}*/
						//Boolean.parseBoolean(row[3].toString())
					 	obj.setPanelStatus(Boolean.parseBoolean(objArr2[1].toString()));
					 	//System.out.println("Classs Name is ssssssssssssssss"+getPersistentClass());
		 			 	list.add(obj);
				   }
				}
			 
			}
		} 
	 	catch(Exception e){
			   
		         e.printStackTrace(); 
		}finally{
			// session.close(); 
		}
		return list;
	}
}

