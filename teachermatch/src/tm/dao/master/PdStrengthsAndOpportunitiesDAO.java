package tm.dao.master;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.CompetencyMaster;
import tm.bean.master.ObjectiveMaster;
import tm.bean.master.PdStrengthsAndOpportunities;
import tm.dao.generic.GenericHibernateDAO;

public class PdStrengthsAndOpportunitiesDAO extends GenericHibernateDAO<PdStrengthsAndOpportunities, Integer> 
{

	public PdStrengthsAndOpportunitiesDAO() {
		super(PdStrengthsAndOpportunities.class);
	}
	
/* @Author: Vishwanath Kumar
 * @Discription: It is used to get Statement By Objective when objectiveMaster is given.
 */	
	@Transactional(readOnly=true)
	public List<PdStrengthsAndOpportunities> getStatementByObjective(ObjectiveMaster objectiveMaster,String statementType)
	{
		List<PdStrengthsAndOpportunities> pdStrengthsAndOpportunitiesList =  null;
		try{
			Criterion criterion =  Restrictions.eq("status", "a");
			Criterion criterion1 =  Restrictions.eq("objectiveMaster", objectiveMaster);
			Criterion criterion2 =  Restrictions.isNull("strengthsAndOpportunities");
			Criterion criterion3 =  Restrictions.eq("statementType",statementType);
			//pdStrengthsAndOpportunitiesList = findByCriteria(Order.asc("statement"), criterion,criterion1,criterion2);
			pdStrengthsAndOpportunitiesList = findByCriteria(criterion,criterion1,criterion2,criterion3);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return pdStrengthsAndOpportunitiesList;
	}
	
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Statement By Objective when objectiveMaster is given.
	 */	
		@Transactional(readOnly=true)
		public List<PdStrengthsAndOpportunities> getStatementByObjectiveChild(ObjectiveMaster objectiveMaster,String statementType)
		{
			List<PdStrengthsAndOpportunities> pdStrengthsAndOpportunitiesList =  null;
			try{
				Criterion criterion =  Restrictions.eq("status", "a");
				Criterion criterion1 =  Restrictions.eq("objectiveMaster", objectiveMaster);
				Criterion criterion2 =  Restrictions.isNotNull("strengthsAndOpportunities");
				Criterion criterion3 =  Restrictions.eq("statementType",statementType);
				//pdStrengthsAndOpportunitiesList = findByCriteria(Order.asc("statement"), criterion,criterion1,criterion2);
				pdStrengthsAndOpportunitiesList = findByCriteria(criterion,criterion1,criterion2,criterion3);
				
			}catch (Exception e) {
				e.printStackTrace();
			}
			return pdStrengthsAndOpportunitiesList;
		}
		

		/* @Author: Vishwanath Kumar
		 * @Discription: It is used to get Statement By Objective when objectiveMaster is given.
		 */	
			@Transactional(readOnly=true)
			public List<PdStrengthsAndOpportunities> getStatementByObjectiveFromParent(ObjectiveMaster objectiveMaster,PdStrengthsAndOpportunities pdStrengthsAndOpportunities,String statementType)
			{
				List<PdStrengthsAndOpportunities> pdStrengthsAndOpportunitiesList =  null;
				try{
					Criterion criterion =  Restrictions.eq("status", "a");
					Criterion criterion1 =  Restrictions.eq("objectiveMaster", objectiveMaster);
					Criterion criterion2 =  Restrictions.eq("strengthsAndOpportunities",pdStrengthsAndOpportunities);
					Criterion criterion3 =  Restrictions.eq("statementType",statementType);
					//pdStrengthsAndOpportunitiesList = findByCriteria(Order.asc("statement"), criterion,criterion1,criterion2);
					pdStrengthsAndOpportunitiesList = findByCriteria(criterion,criterion1,criterion2,criterion3);
				}catch (Exception e) {
					e.printStackTrace();
				}
				return pdStrengthsAndOpportunitiesList;
			}
}
