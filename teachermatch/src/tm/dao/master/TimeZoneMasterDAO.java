package tm.dao.master;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.TimeZoneMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TimeZoneMasterDAO extends	GenericHibernateDAO<TimeZoneMaster, Integer> {
	public TimeZoneMasterDAO() {
		super(TimeZoneMaster.class);
	}
	@Transactional(readOnly=false)
	public List<TimeZoneMaster> getAllTimeZone(){
		List<TimeZoneMaster> timeZoneMasterList=null;
		try{
			timeZoneMasterList=getSession().createCriteria(getPersistentClass()).addOrder(Order.asc("timeZoneShortName")).list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		 return timeZoneMasterList;
	}
}
