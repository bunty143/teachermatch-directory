package tm.dao.master;

import tm.bean.master.DistrictNotes;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictNotesDAO extends GenericHibernateDAO<DistrictNotes, Integer> 
{

	public DistrictNotesDAO()
	{
		super(DistrictNotes.class);
	}
}
