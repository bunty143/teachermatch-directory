package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;



import tm.bean.master.UserFolderStructure;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class UserFolderStructureDAO extends GenericHibernateDAO<UserFolderStructure, Integer>
{
	public UserFolderStructureDAO()
	{
		super(UserFolderStructure.class);
	}

	@Transactional(readOnly=true)
	public List<UserFolderStructure> getFolderExistListByFolderName(UserMaster userMaster,String folderName)
	{
		List<UserFolderStructure> lstExistFolder = new ArrayList<UserFolderStructure>();
		try 
		{
			System.out.println("UserId :"+userMaster.getUserId());
			System.out.println("Folder name:"+folderName);
			Criterion criterion1 = Restrictions.eq("usermaster",userMaster);
			Criterion criterion2 = Restrictions.eq("districtMaster",userMaster.getDistrictId());
			Criterion criterion4 = Restrictions.eq("folderName",folderName).ignoreCase();
			Criterion criterion3 = null;
			if(userMaster.getSchoolId()!=null)
				criterion3 = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
			else
				criterion3 = Restrictions.isNull("schoolMaster");
			
			lstExistFolder = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstExistFolder;
	}
	
	@Transactional(readOnly=true)
	public List<UserFolderStructure> getParaentObjByUser(UserMaster userMaster)
	{
		List<UserFolderStructure> lstExistFolder = new ArrayList<UserFolderStructure>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("usermaster",userMaster);
		    Criterion criterion2 = Restrictions.eq("districtMaster",userMaster.getDistrictId());
			Criterion criterion4 = Restrictions.isNull("userFolderStructure");
			Criterion criterion3 = null;
			
			if(userMaster.getSchoolId()!=null&& !userMaster.getSchoolId().equals(""))
			{
				
				criterion3 = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
			}
			else{
				    criterion3 = Restrictions.isNull("schoolMaster");
			     }
			lstExistFolder = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstExistFolder;
	}


}
