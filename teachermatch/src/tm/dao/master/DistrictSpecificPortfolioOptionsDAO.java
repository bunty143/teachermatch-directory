package tm.dao.master;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.DistrictSpecificPortfolioOptions;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificPortfolioOptionsDAO extends GenericHibernateDAO<DistrictSpecificPortfolioOptions, Integer> 
{
	public DistrictSpecificPortfolioOptionsDAO() 
	{
		super(DistrictSpecificPortfolioOptions.class);
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioOptions> getCustumFieldOptionByQuestionLst(List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions)
	{
		List<DistrictSpecificPortfolioOptions> lstDistrictSpecificPortfolioOptions=new ArrayList<DistrictSpecificPortfolioOptions>();
		try 
		{
			Criterion criterion1 = Restrictions.in("districtSpecificPortfolioQuestions", lstDistrictSpecificPortfolioQuestions);
			lstDistrictSpecificPortfolioOptions=findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictSpecificPortfolioOptions;		
	}
	
}
