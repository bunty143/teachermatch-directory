package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.StateMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CertificateTypeMasterDAO extends GenericHibernateDAO<CertificateTypeMaster, Integer> 
{
	public CertificateTypeMasterDAO() 
	{
		super(CertificateTypeMaster.class);
	}
	@Transactional(readOnly=false)
	public List<CertificateTypeMaster> findCertificationByJob(StateMaster stateMaster,String certType)
	{
		List<CertificateTypeMaster> certificateTypeMasterList=  new ArrayList<CertificateTypeMaster>();
		try{  
				if(!certType.equals("0") && !certType.equals("")){
					Criterion criterion2 = Restrictions.eq("certTypeId",Integer.parseInt(certType));
					Criterion criterion3 = Restrictions.eq("status","A");
					certificateTypeMasterList = findByCriteria(criterion2,criterion3);	
				}else if(certType.equals("0")){
					Criterion criterion1 = Restrictions.eq("stateId",stateMaster);
					Criterion criterion4 = Restrictions.eq("status","A");
					certificateTypeMasterList = findByCriteria(criterion1,criterion4);
				}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return certificateTypeMasterList;
	}
	
	@Transactional(readOnly=false)
	public String getCertificateNameByIds(String certificationType){
		StringBuffer finalStrId=new  StringBuffer();
		try{
			String[] cerString=null;
			if(certificationType!=null && !certificationType.equals("")){
				StringBuffer jcRecordsBuffer=new StringBuffer(certificationType);
				jcRecordsBuffer.deleteCharAt(jcRecordsBuffer.length()-1);
				cerString=jcRecordsBuffer.toString().split(",");
			}
			
			List<Integer> certiId=new ArrayList<Integer>();
			if(cerString!=null && cerString.length>0){
				for(String id:cerString){
					certiId.add(Integer.parseInt(id));
				}
			}
			
			if(certiId.size()>0){
				Criterion criterion=Restrictions.in("certTypeId", certiId);
				List<CertificateTypeMaster> list=findByCriteria(criterion);
				for(CertificateTypeMaster crt:list){
					finalStrId.append(crt.getCertType()+", ");
				}
			}
			if(finalStrId.length()>=2){
				finalStrId.deleteCharAt(finalStrId.length()-2).toString();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return  finalStrId.toString();
	}
	
	@Transactional(readOnly=false)
	public boolean findDuplicateCertificateType(CertificateTypeMaster certificateTypeMaster,String certificationType,StateMaster stateMaster,int addEditFlag){
		boolean b=false;
		try{
			if(!certificationType.equals("") && stateMaster!=null){
				Criterion criterion1=Restrictions.eq("certType", certificationType);
				Criterion criterion2=Restrictions.eq("stateId", stateMaster);
				List<CertificateTypeMaster> certificateTypeMasters=new ArrayList<CertificateTypeMaster>();
				
				if(addEditFlag==0){
					certificateTypeMasters=findByCriteria(criterion1,criterion2);
				}else{
					Criterion criterion3=Restrictions.ne("certTypeId", certificateTypeMaster.getCertTypeId());
					certificateTypeMasters=findByCriteria(criterion1,criterion2,criterion3);
				}
				
				if(certificateTypeMasters.size()>0)
					return true;
				else
					return false;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Transactional(readOnly=false)
	public CertificateTypeMaster findCertificationTypeByCertTypeCode(String certTypeCode)
	{
		CertificateTypeMaster certificateTypeMaster = null;
		try{  
				Criterion criterion1 = Restrictions.eq("certTypeCode",certTypeCode);
				Criterion criterion4 = Restrictions.eq("status","A");
				List<CertificateTypeMaster> certificateTypeMasterList = findByCriteria(criterion1,criterion4);
				if(certificateTypeMasterList.size()>0)
					certificateTypeMaster = certificateTypeMasterList.get(0);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return certificateTypeMaster;
	}
	
	@Transactional
	public List<CertificateTypeMaster> findCertificateByCertTypeId(List<Integer> certTypeIdList){
		List<CertificateTypeMaster> certificateTypeMasterList = null;
		try {
			Criterion criterion1 = Restrictions.in("certTypeId", certTypeIdList);
			Criterion criterion2 = Restrictions.eq("status","A");
			certificateTypeMasterList = findByCriteria(criterion1, criterion2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return certificateTypeMasterList; 
	}

	//Optimization for candidate Pool search
	
	
	@Transactional(readOnly=false)
	public List<Integer> findCertificationByJob_Op(StateMaster stateMaster,String certType)
	{
		List<Integer> certificateTypeMasterList=  new ArrayList<Integer>();
		try{  
			Criteria criteria = getSession().createCriteria(this.getPersistentClass());
				if(!certType.equals("0") && !certType.equals("")){
					criteria.add(Restrictions.eq("certTypeId",Integer.parseInt(certType)));
					criteria.add(Restrictions.eq("status","A"));
				}else if(certType.equals("0")){
					criteria.add(Restrictions.eq("stateId",stateMaster));
					criteria.add(Restrictions.eq("status","A"));
				}
				criteria.setProjection(Projections.property("certTypeId"));
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return certificateTypeMasterList;
	}

}
