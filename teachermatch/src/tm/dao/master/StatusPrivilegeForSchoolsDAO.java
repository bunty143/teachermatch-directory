package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusPrivilegeForSchools;
import tm.dao.generic.GenericHibernateDAO;

public class StatusPrivilegeForSchoolsDAO extends GenericHibernateDAO<StatusPrivilegeForSchools, Integer> 
{
	public StatusPrivilegeForSchoolsDAO() 
	{
		super(StatusPrivilegeForSchools.class);
	}
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findStatusPrivilege(DistrictMaster  districtMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);			
			lstStatusPForSchool = findByCriteria(criterion1);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstStatusPForSchool;
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findStatusPrivilegeForStatusMaster(DistrictMaster  districtMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			
			Criterion criterion_dm = Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion_sm = Restrictions.isNotNull("statusMaster");
			Criterion criterion_ss = Restrictions.isNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion_dm,criterion_sm,criterion_ss);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return lstStatusPForSchool;
		}
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findStatusPrivilegeForSecondaryStatusId(DistrictMaster  districtMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			Criterion criterion_dm = Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion_sm = Restrictions.isNull("statusMaster");
			Criterion criterion_ss = Restrictions.isNotNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion_dm,criterion_ss,criterion_sm);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return lstStatusPForSchool;
		}
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findSchoolStatusForStatusMasterId(DistrictMaster  districtMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_status=Restrictions.eq("canSchoolSetStatus", 1);
			Criterion criterion_sm=Restrictions.isNotNull("statusMaster");
			lstStatusPForSchool = findByCriteria(criterion,criterion_status,criterion_sm);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return lstStatusPForSchool;
		}
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findSchoolStatusForSecondaryStatusId(DistrictMaster  districtMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_status=Restrictions.eq("canSchoolSetStatus", 1);
			Criterion criterion_ss=Restrictions.isNotNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion,criterion_status,criterion_ss);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return lstStatusPForSchool;
		}
	}
	
	
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findSecondaryStatusIdBySecondaryStatusList(DistrictMaster  districtMaster,List<SecondaryStatus> lstsecondaryStatusForInactive)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion_ss=Restrictions.in("secondaryStatus",lstsecondaryStatusForInactive);
			lstStatusPForSchool = findByCriteria(criterion,criterion_ss);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return lstStatusPForSchool;
		}
	}
	
	
	
	@Transactional(readOnly=false)
	public int deleteData(List<StatusPrivilegeForSchools> statusPrivilegeForSchools)
	{
		int iReturnValue=0;
		try 
		{
			for(StatusPrivilegeForSchools pojo:statusPrivilegeForSchools)
			{
				makeTransient(pojo);
				iReturnValue++;
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return iReturnValue;
		}
		
		
	}
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findStatusPrivilegeForStatusMasterForHQ(HeadQuarterMaster headQuarterMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{		
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.isNull("districtMaster"));
			criteria.add(Restrictions.isNull("branchMaster"));
			criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			criteria.add(Restrictions.isNotNull("statusMaster"));
			criteria.add(Restrictions.isNull("secondaryStatus"));
			lstStatusPForSchool = criteria.list();
			
			/*Criterion criterion_dm = Restrictions.eq("headQuarterMaster", headQuarterMaster);
			Criterion criterion_branch = Restrictions.isNull("branchMaster");
			Criterion criterion_sm = Restrictions.isNotNull("statusMaster");
			Criterion criterion_ss = Restrictions.isNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion_dm,criterion_sm,criterion_ss,criterion_branch);		*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstStatusPForSchool;

	}
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findStatusPrivilegeForSecondaryStatusIdForHQ(HeadQuarterMaster headQuarterMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.isNull("districtMaster"));
			criteria.add(Restrictions.isNull("branchMaster"));
			criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			criteria.add(Restrictions.isNotNull("secondaryStatus"));
			criteria.add(Restrictions.isNull("statusMaster"));
			lstStatusPForSchool = criteria.list();
		/*	
			Criterion criterion_dm = Restrictions.eq("headQuarterMaster", headQuarterMaster);
			Criterion criterion_branch = Restrictions.isNull("branchMaster");
			Criterion criterion_sm = Restrictions.isNull("statusMaster");
			Criterion criterion_ss = Restrictions.isNotNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion_dm,criterion_ss,criterion_sm,criterion_branch);		*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

			return lstStatusPForSchool;

	}
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findStatusPrivilegeForStatusMasterForBranch(BranchMaster branchMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{	
			Criterion criterion_dm = Restrictions.eq("branchMaster", branchMaster);
			Criterion criterion_sm = Restrictions.isNotNull("statusMaster");
			Criterion criterion_ss = Restrictions.isNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion_dm,criterion_sm,criterion_ss);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstStatusPForSchool;

	}
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findStatusPrivilegeForSecondaryStatusIdForBranch(BranchMaster branchMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			Criterion criterion_dm = Restrictions.eq("branchMaster", branchMaster);
			Criterion criterion_sm = Restrictions.isNull("statusMaster");
			Criterion criterion_ss = Restrictions.isNotNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion_dm,criterion_ss,criterion_sm);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

			return lstStatusPForSchool;

	}
	
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findHqStatusForStatusMasterId(HeadQuarterMaster headQuarterMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.isNull("districtMaster"));
			criteria.add(Restrictions.isNull("branchMaster"));
			criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			criteria.add(Restrictions.isNotNull("statusMaster"));
			criteria.add(Restrictions.eq("canBranchSetStatus",1));
			lstStatusPForSchool = criteria.list();
			/*
			Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion_branch = Restrictions.isNull("branchMaster");
			Criterion criterion_status=Restrictions.eq("canBranchSetStatus", 1);
			Criterion criterion_sm=Restrictions.isNotNull("statusMaster");
			lstStatusPForSchool = findByCriteria(criterion,criterion_status,criterion_sm,criterion_branch);		*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

			return lstStatusPForSchool;
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findHqStatusForSecondaryStatusId(HeadQuarterMaster headQuarterMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.isNull("districtMaster"));
			criteria.add(Restrictions.isNull("branchMaster"));
			criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			criteria.add(Restrictions.isNotNull("secondaryStatus"));
			criteria.add(Restrictions.eq("canBranchSetStatus",1));
			lstStatusPForSchool = criteria.list();
			/*
			Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion_branch = Restrictions.isNull("branchMaster");
			Criterion criterion_status=Restrictions.eq("canBranchSetStatus", 1);
			Criterion criterion_ss=Restrictions.isNotNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion,criterion_status,criterion_ss,criterion_branch);		*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
			return lstStatusPForSchool;
	}
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findStatusPrivilegeByHeadBranchAndDistrict(JobOrder jobOrder)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			/*if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}*/
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			lstStatusPForSchool = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstStatusPForSchool;
	}
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findStatusMasterForHQ(HeadQuarterMaster headQuarterMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.isNull("districtMaster"));
			criteria.add(Restrictions.isNull("branchMaster"));
			criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			criteria.add(Restrictions.isNotNull("statusMaster"));
			criteria.add(Restrictions.eq("canHQSetStatus",1));
			lstStatusPForSchool = criteria.list();
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

			return lstStatusPForSchool;
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForSchools> findSecondaryStatusForHQ(HeadQuarterMaster headQuarterMaster)
	{
		List<StatusPrivilegeForSchools> lstStatusPForSchool= new ArrayList<StatusPrivilegeForSchools>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.isNull("districtMaster"));
			criteria.add(Restrictions.isNull("branchMaster"));
			criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			criteria.add(Restrictions.isNotNull("secondaryStatus"));
			criteria.add(Restrictions.eq("canHQSetStatus",1));
			lstStatusPForSchool = criteria.list();

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
			return lstStatusPForSchool;
	}
	
	@Transactional(readOnly=false)
	public List findStatusPrivilegeForStatusMasterForHQ(Integer headQuarterId)
	{
		List lstStatusPForSchool= new ArrayList();
		try 
		{		
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.isNull("districtMaster"));
			criteria.add(Restrictions.isNull("branchMaster"));
			criteria.add(Restrictions.eq("headQuarterMaster.headQuarterId",headQuarterId));
			criteria.add(Restrictions.isNotNull("statusMaster"));
			criteria.add(Restrictions.isNull("secondaryStatus"));
			
			ProjectionList proList = Projections.projectionList();
	        proList.add(Projections.property("statusMaster.statusId"));
	        proList.add(Projections.property("statusPrivilegeId"));
	        criteria.setProjection(proList);
			lstStatusPForSchool = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstStatusPForSchool;

	}
	
	
	@Transactional(readOnly=false)
	public List findStatusPrivilegeForSecondaryStatusIdForHQ(Integer headQuarterId)
	{
		List lstStatusPForSchool= new ArrayList();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.isNull("districtMaster"));
			criteria.add(Restrictions.isNull("branchMaster"));
			criteria.add(Restrictions.eq("headQuarterMaster.headQuarterId",headQuarterId));
			criteria.add(Restrictions.isNotNull("secondaryStatus"));
			criteria.add(Restrictions.isNull("statusMaster"));
			
			ProjectionList proList = Projections.projectionList();
	        proList.add(Projections.property("secondaryStatus.secondaryStatusId"));
	        proList.add(Projections.property("statusPrivilegeId"));
	        criteria.setProjection(proList);
			
			lstStatusPForSchool = criteria.list();
		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
			return lstStatusPForSchool;
	}
}
