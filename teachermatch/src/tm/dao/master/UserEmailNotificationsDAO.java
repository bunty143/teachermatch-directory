package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.UserEmailNotifications;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.UserEmailNotifications;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class UserEmailNotificationsDAO extends	GenericHibernateDAO<UserEmailNotifications, Integer> {
	public UserEmailNotificationsDAO() {
		super(UserEmailNotifications.class);
	}
	
	/* @Author: Vishwanath Kumar
	 * @User notification check
	 */
	@Transactional(readOnly=true)
	public List<UserEmailNotifications> getUserEmailNotifications(UserMaster userMaster,String[] notificationShortNames,String tabName)
	{
		List<UserEmailNotifications> userEmailNotifications = new ArrayList<UserEmailNotifications>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("userMaster",userMaster);
			Criterion criterion2 = Restrictions.in("notificationShortName",notificationShortNames);
			Criterion criterion3 = Restrictions.eq("notificationTabName",tabName);
			Criterion criterion4 = Restrictions.eq("districtMaster",userMaster.getDistrictId());
			Criterion criterion5 = Restrictions.eq("notificationFlag",true);
			Criterion criterion6 = null;
			if(userMaster.getSchoolId()!=null)
				criterion6 = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
			else
				criterion6 = Restrictions.isNull("schoolMaster");
			
			userEmailNotifications = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return userEmailNotifications;
	}
	/* @Author: Sekhar Swain
	 * @User notification check
	 */
	@Transactional(readOnly=false)
	public List<UserMaster> getUserByDistrictAndSchoolList(DistrictMaster districtMaster,List<SchoolMaster> schoolMasters,String notificationShortNames)
	{
		List <UserMaster> lstUserMaster= null;
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			 Criterion criterion2 = Restrictions.eq("entityType",2);
			 
			 Criterion criterion3 = Restrictions.in("schoolId",schoolMasters);
			 Criterion criterion4 = Restrictions.eq("entityType",3);
			 
			 Criterion criterion5 = Restrictions.and(criterion1,criterion2);
			 Criterion criterion6 = Restrictions.and(criterion3,criterion4);
			 
			 Criterion  criterion7=Restrictions.or(criterion5,criterion6);
			 Criterion criterion8 = Restrictions.eq("status","A");
			 
			Criterion criterion10 = Restrictions.eq("notificationShortName",notificationShortNames);
			Criterion criterion11 = Restrictions.eq("notificationTabName","cs");
			Criterion criterion12 = Restrictions.eq("notificationFlag",true);
			criteria.add(criterion10);
			criteria.add(criterion11);
			criteria.add(criterion12);
			criteria.setProjection(Projections.distinct(Projections.property("userMaster")));
			criteria.createCriteria("userMaster").add(criterion8).add(criterion7);
			lstUserMaster =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	/* @Author: Sekhar Swain
	 * @User notification check
	 */
	@Transactional(readOnly=false)
	public List<UserMaster> getUserByDistrictListOnlySchool(DistrictMaster districtMaster,SchoolMaster schoolMaster,JobOrder jobOrder,String notificationShortNames)
	{
		List <UserMaster> lstUserMaster= null;
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			 Criterion criterion2 = Restrictions.eq("entityType",2);
			 
			 Criterion criterion3 = Restrictions.eq("schoolId",schoolMaster);
			 Criterion criterion4 = Restrictions.eq("entityType",3);
			 
			 Criterion criterion5 = Restrictions.and(criterion1,criterion2);
			 Criterion criterion6 = Restrictions.and(criterion3,criterion4);
			 
			 Criterion  criterion7=Restrictions.or(criterion5,criterion6);
			 Criterion criterion8 = Restrictions.eq("status","A");
			 
			 Criterion criterion10 = Restrictions.eq("notificationShortName",notificationShortNames);
			 Criterion criterion11 = Restrictions.eq("notificationTabName","cs");
			 Criterion criterion12 = Restrictions.eq("notificationFlag",true);
			 criteria.add(criterion10);
			 criteria.add(criterion11);
			 criteria.add(criterion12); 
			criteria.setProjection(Projections.distinct(Projections.property("userMaster")));
			if(jobOrder.getDistrictMaster()!=null && (jobOrder.getWritePrivilegeToSchool()|| jobOrder.getDistrictMaster().getDistrictId()==7800040)){
				criteria.createCriteria("userMaster").add(criterion8).add(criterion7);
			}else
				criteria.createCriteria("userMaster").add(criterion8).add(criterion6);
			
			lstUserMaster =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	/* @Author: Sekhar Swain
	 * @User notification check
	 */
	@Transactional(readOnly=false)
	public List<UserMaster> getUserByOnlySchool(SchoolMaster schoolMaster,String notificationShortNames)
	{
		List <UserMaster> lstUserMaster= null;
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			Criterion criterion1 = Restrictions.eq("schoolId",schoolMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("entityType",3);
			
			Criterion criterion10 = Restrictions.eq("notificationShortName",notificationShortNames);
			Criterion criterion11 = Restrictions.eq("notificationTabName","cs");
			Criterion criterion12 = Restrictions.eq("notificationFlag",true);
			criteria.add(criterion10);
			criteria.add(criterion11);
			criteria.add(criterion12);
			criteria.setProjection(Projections.distinct(Projections.property("userMaster")));
			criteria.createCriteria("userMaster").add(criterion1).add(criterion2).add(criterion3);
			lstUserMaster =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	/* @Author: Sekhar Swain
	 * @User notification check
	 */
	@Transactional(readOnly=false)
	public List<UserMaster> getUserByDistrict(DistrictMaster districtMaster,String notificationShortNames)
	{
		List <UserMaster> lstUserMaster= null;
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			 Criterion criterion2 = Restrictions.eq("entityType",new Integer(2));
			 Criterion criterion3 = Restrictions.eq("status","A");
			
			Criterion criterion10 = Restrictions.eq("notificationShortName",notificationShortNames);
			Criterion criterion11 = Restrictions.eq("notificationTabName","cs");
			Criterion criterion12 = Restrictions.eq("notificationFlag",true);
			criteria.add(criterion10);
			criteria.add(criterion11);
			criteria.add(criterion12);
			criteria.setProjection(Projections.distinct(Projections.property("userMaster")));
			criteria.createCriteria("userMaster").add(criterion1).add(criterion2).add(criterion3);
			lstUserMaster =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	
	/* @Author: Gagan
	 * @ notification check
	 */
	@Transactional(readOnly=false)
	public List<UserMaster> getUserByDistrictAndSchoolList(DistrictMaster districtMaster,List<SchoolMaster> schoolMasters,JobOrder jobOrder,String notificationShortNames)
	{
		List <UserMaster> lstUserMaster= null;
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			 Criterion criterion2 = Restrictions.eq("entityType",2);
			 
			 Criterion criterion3 = Restrictions.in("schoolId",schoolMasters);
			 Criterion criterion4 = Restrictions.eq("entityType",3);
			 
			 Criterion criterion5 = Restrictions.and(criterion1,criterion2);
			 Criterion criterion6 = Restrictions.and(criterion3,criterion4);
			 
			 Criterion  criterion7=Restrictions.or(criterion5,criterion6);
			 Criterion criterion8 = Restrictions.eq("status","A");
			 
			Criterion criterion10 = Restrictions.eq("notificationShortName",notificationShortNames);
			Criterion criterion11 = Restrictions.eq("notificationTabName","cs");
			Criterion criterion12 = Restrictions.eq("notificationFlag",true);
			
			criteria.add(criterion10);
			criteria.add(criterion11);
			criteria.add(criterion12);
			criteria.setProjection(Projections.distinct(Projections.property("userMaster")));
			System.out.println("\n\n Gagan :[ UserEmailNotificationDAO ]  WritePrivilegeToSchool()  "+jobOrder.getWritePrivilegeToSchool());
			
			if(jobOrder.getDistrictMaster()!=null ){
				criteria.createCriteria("userMaster").add(criterion8).add(criterion7);
			}else
				criteria.createCriteria("userMaster").add(criterion8).add(criterion6);
			
			lstUserMaster =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=false)
	public List<UserMaster> getUserByDistrict_slc(DistrictMaster districtMaster,String notificationShortNames,List<RoleMaster> roleMasters)
	{
		List <UserMaster> lstUserMaster= null;
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			 Criterion criterion2 = Restrictions.eq("entityType",new Integer(2));
			 Criterion criterion3 = Restrictions.eq("status","A");
			
			 Criterion criterionRole = Restrictions.in("roleId", roleMasters);
			 
			Criterion criterion10 = Restrictions.eq("notificationShortName",notificationShortNames);
			Criterion criterion11 = Restrictions.eq("notificationTabName","cs");
			Criterion criterion12 = Restrictions.eq("notificationFlag",true);
			criteria.add(criterion10);
			criteria.add(criterion11);
			criteria.add(criterion12);
			criteria.setProjection(Projections.distinct(Projections.property("userMaster")));
			criteria.createCriteria("userMaster").add(criterion1).add(criterion2).add(criterion3).add(criterionRole);
			lstUserMaster =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}

	@Transactional(readOnly=false)
	public List<UserMaster> getUserByDistrictAndSchoolList_slc(DistrictMaster districtMaster,List<SchoolMaster> schoolMasters,JobOrder jobOrder,String notificationShortNames,List<RoleMaster> roleMasters)
	{
		List <UserMaster> lstUserMaster= null;
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			 Criterion criterion2 = Restrictions.eq("entityType",2);
			 
			 Criterion criterion3 = Restrictions.in("schoolId",schoolMasters);
			 Criterion criterion4 = Restrictions.eq("entityType",3);
			 
			 Criterion criterion5 = Restrictions.and(criterion1,criterion2);
			 Criterion criterion6 = Restrictions.and(criterion3,criterion4);
			 
			 Criterion  criterion7=Restrictions.or(criterion5,criterion6);
			 Criterion criterion8 = Restrictions.eq("status","A");
			 
			Criterion criterion10 = Restrictions.eq("notificationShortName",notificationShortNames);
			Criterion criterion11 = Restrictions.eq("notificationTabName","cs");
			Criterion criterion12 = Restrictions.eq("notificationFlag",true);
			
			Criterion criterionRole = Restrictions.in("roleId", roleMasters);
			
			criteria.add(criterion10);
			criteria.add(criterion11);
			criteria.add(criterion12);
			criteria.setProjection(Projections.distinct(Projections.property("userMaster")));
			
			if(jobOrder.getDistrictMaster()!=null){
				criteria.createCriteria("userMaster").add(criterion8).add(criterion7).add(criterionRole);
			}else
				criteria.createCriteria("userMaster").add(criterion8).add(criterion6).add(criterionRole);
			
			lstUserMaster =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=false)
	public List<UserMaster> getUserByDistrictListOnlySchool_slc(DistrictMaster districtMaster,SchoolMaster schoolMaster,JobOrder jobOrder,String notificationShortNames,List<RoleMaster> roleMasters)
	{
		List <UserMaster> lstUserMaster= null;
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			 Criterion criterion2 = Restrictions.eq("entityType",2);
			 
			 Criterion criterion3 = Restrictions.eq("schoolId",schoolMaster);
			 Criterion criterion4 = Restrictions.eq("entityType",3);
			 
			 Criterion criterion5 = Restrictions.and(criterion1,criterion2);
			 Criterion criterion6 = Restrictions.and(criterion3,criterion4);
			 
			 Criterion  criterion7=Restrictions.or(criterion5,criterion6);
			 Criterion criterion8 = Restrictions.eq("status","A");
			 
			 Criterion criterion10 = Restrictions.eq("notificationShortName",notificationShortNames);
			 Criterion criterion11 = Restrictions.eq("notificationTabName","cs");
			 Criterion criterion12 = Restrictions.eq("notificationFlag",true);
			 
			 Criterion criterionRole = Restrictions.in("roleId", roleMasters);
			 
			 criteria.add(criterion10);
			 criteria.add(criterion11);
			 criteria.add(criterion12); 
			criteria.setProjection(Projections.distinct(Projections.property("userMaster")));
			
			if(jobOrder.getDistrictMaster()!=null){
				criteria.createCriteria("userMaster").add(criterion8).add(criterion7).add(criterionRole);
			}else{
				criteria.createCriteria("userMaster").add(criterion8).add(criterion6).add(criterionRole);
			}
			
			lstUserMaster =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=false)
	public List<UserMaster> getUserByOnlySchool_slc(SchoolMaster schoolMaster,String notificationShortNames,List<RoleMaster> roleMasters)
	{
		List <UserMaster> lstUserMaster= null;
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			Criterion criterion1 = Restrictions.eq("schoolId",schoolMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("entityType",3);
			
			Criterion criterion10 = Restrictions.eq("notificationShortName",notificationShortNames);
			Criterion criterion11 = Restrictions.eq("notificationTabName","cs");
			Criterion criterion12 = Restrictions.eq("notificationFlag",true);
			
			Criterion criterionRole = Restrictions.in("roleId", roleMasters);
			
			criteria.add(criterion10);
			criteria.add(criterion11);
			criteria.add(criterion12);
			criteria.setProjection(Projections.distinct(Projections.property("userMaster")));
			criteria.createCriteria("userMaster").add(criterion1).add(criterion2).add(criterion3).add(criterionRole);
			lstUserMaster =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=false)
	public List<UserMaster> getUserByNotificationOn_slc(List<UserMaster> userMasters,String notificationShortNames,List<RoleMaster> roleMasters)
	{
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			Criterion criterion1 = Restrictions.in("userMaster",userMasters);
			Criterion criterion2 = Restrictions.eq("status","A");
			
			Criterion criterion10 = Restrictions.eq("notificationShortName",notificationShortNames);
			Criterion criterion11 = Restrictions.eq("notificationTabName","cs");
			Criterion criterion12 = Restrictions.eq("notificationFlag",true);
			
			Criterion criterionRole = Restrictions.in("roleId", roleMasters);
			
			criteria.add(criterion1);
			criteria.add(criterion10);
			criteria.add(criterion11);
			criteria.add(criterion12);
			criteria.setProjection(Projections.distinct(Projections.property("userMaster")));
			criteria.createCriteria("userMaster").add(criterion2).add(criterionRole);
			lstUserMaster =criteria.list();
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	@Transactional(readOnly=false)
	public List<UserMaster> getUserByDistrict_slcByHBD(JobOrder jobOrder,String notificationShortNames,List<RoleMaster> roleMasters)
	{
		List <UserMaster> lstUserMaster= null;
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			 Criterion criterion2 = Restrictions.eq("entityType",new Integer(2));
			 Criterion criterion3 = Restrictions.eq("status","A");
			
			 Criterion criterionRole = Restrictions.in("roleId", roleMasters);
			 			 
			Criterion criterion10 = Restrictions.eq("notificationShortName",notificationShortNames);
			Criterion criterion11 = Restrictions.eq("notificationTabName","cs");
			Criterion criterion12 = Restrictions.eq("notificationFlag",true);
			criteria.add(criterion10);
			criteria.add(criterion11);
			criteria.add(criterion12);
			criteria.setProjection(Projections.distinct(Projections.property("userMaster")));
			
			
			Criterion criterionDistrict=null;
			Criterion criterionBranch=null;
			Criterion criterionHead=null;
			if(jobOrder.getDistrictMaster()!=null){
				criterionDistrict = Restrictions.eq("districtId",jobOrder.getDistrictMaster());
			}else{
				criterionDistrict = Restrictions.isNull("districtId");
			}
			if(jobOrder.getBranchMaster()!=null){
				criterionBranch = Restrictions.eq("branchMaster",jobOrder.getBranchMaster());
			}else{
				criterionBranch = Restrictions.isNull("branchMaster");
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criterionHead = Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
			}else{
				criterionHead = Restrictions.isNull("headQuarterMaster");
			}
			if(Utility.isNC())
				criteria.createCriteria("userMaster").add(criterion2).add(criterion3).add(criterionRole).add(criterionDistrict);
			else
				criteria.createCriteria("userMaster").add(criterion2).add(criterion3).add(criterionRole).add(criterionDistrict).add(criterionBranch).add(criterionHead);
			
			lstUserMaster =criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}

}
