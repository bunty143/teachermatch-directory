package tm.dao.master;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.RegionMaster;
import tm.bean.master.SchoolTypeMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SchoolTypeMasterDAO extends GenericHibernateDAO<SchoolTypeMaster, Integer> 
{
	public SchoolTypeMasterDAO() 
	{
		super(SchoolTypeMaster.class);
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get active SchoolTypes.
	 */
	@Transactional(readOnly=true)
	public List<SchoolTypeMaster> getActiveSchoolTypes()
	{
		List<SchoolTypeMaster> schoolTypeMasters = null;
		try {
			Criterion criterion = Restrictions.eq("status", "A");
			schoolTypeMasters = findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return schoolTypeMasters;
	}
}
