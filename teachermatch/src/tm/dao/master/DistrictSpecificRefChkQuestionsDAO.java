package tm.dao.master;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.DistrictSpecificRefChkQuestions;
import tm.bean.master.ReferenceCheckQuestionSet;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificRefChkQuestionsDAO extends GenericHibernateDAO<DistrictSpecificRefChkQuestions, Integer> 
{
	
	@Autowired
	private ReferenceCheckQuestionSetDAO referenceCheckQuestionSetDAO;
	
	public DistrictSpecificRefChkQuestionsDAO() 
	{
		super(DistrictSpecificRefChkQuestions.class);
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkQuestions> getDistrictRefChkQuestionBYDistrict(DistrictMaster districtMaster,HeadQuarterMaster headQuarterMaster)		//  @ Anurag
	{
		List<DistrictSpecificRefChkQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificRefChkQuestions>();
		try 
		{
			Criterion criterion1 = null;
			if(districtMaster!=null)
				criterion1 = Restrictions.eq("districtMaster",districtMaster);
			if(headQuarterMaster!=null)
				criterion1 = Restrictions.eq("headQuarterMaster",headQuarterMaster);

			Criterion criterion2 = Restrictions.eq("status","A");
			if(criterion1!=null)
			lstDistSpecQuestions = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int findTotalQuestion(DistrictMaster districtMaster)
	{
		int iReturnValue=0;
		if(districtMaster!=null){
			try{
				Session session = getSession();
				String sql ="SELECT COUNT(questionId) AS appliedCnt from districtspecificrefchkquestions rfq where rfq.districtId = "+districtMaster.getDistrictId()+" and rfq.status='A'";
				System.out.println(sql);
				Query query = session.createSQLQuery(sql);
				List<BigInteger> rowCount = query.list();
				if(rowCount!=null && rowCount.size()==1)
					iReturnValue=rowCount.get(0).intValue();			
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		return iReturnValue;	
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkQuestions> getDistrictRefChkQuestionByDistrictAndJoCategory(DistrictMaster districtMaster, List<Integer> questionIds)
	{
		List<DistrictSpecificRefChkQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificRefChkQuestions>();
		List<ReferenceCheckQuestionSet> refChkSetList 	= 	null;
		try 
		{	
			Criterion criterion2 = null;
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			
			if(questionIds !=null && questionIds.size()>0){
				criterion2 = Restrictions.not(Restrictions.in("questionId", questionIds));
			}
			Criterion criterion3 = Restrictions.eq("status","A");
			
			if(questionIds !=null && questionIds.size()>0){
				lstDistSpecQuestions = findByCriteria(criterion1,criterion2,criterion3);
			} else {
				lstDistSpecQuestions = findByCriteria(criterion1,criterion3);
			}
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkQuestions> findi4QuesByQues(String Questext) {
		List<DistrictSpecificRefChkQuestions> statusList = null;
		try 
		{
			Criterion criterion1 = Restrictions.ilike("question",Questext,MatchMode.ANYWHERE);
			statusList = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkQuestions> findDistrict(DistrictMaster districtMaster)
	{
		List<DistrictSpecificRefChkQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificRefChkQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			lstDistSpecQuestions = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkQuestions> getDistrictSpecificRefQuestionBYHeadQuater(HeadQuarterMaster headQuarterMaster)
	{
		List<DistrictSpecificRefChkQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificRefChkQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.isNull("districtMaster");

			lstDistSpecQuestions = findByCriteria(criterion1,criterion2,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	
	
	
}
