package tm.dao.master;

import tm.bean.master.PeopleRangeMaster;
import tm.dao.generic.GenericHibernateDAO;

public class PeopleRangeMasterDAO extends GenericHibernateDAO<PeopleRangeMaster, Integer> 
{
	public PeopleRangeMasterDAO() {
		super(PeopleRangeMaster.class);
	}

}
