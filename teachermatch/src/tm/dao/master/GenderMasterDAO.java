package tm.dao.master;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.GenderMaster;
import tm.dao.generic.GenericHibernateDAO;
public class GenderMasterDAO extends GenericHibernateDAO<GenderMaster, Integer> 
{
	public GenderMasterDAO() 
	{
		super(GenderMaster.class);
	}
	
	@Transactional(readOnly=true)
	public List<GenderMaster> findAllGenderByOrder()
	{
		List<GenderMaster> lstGender= null;
		try 
		{
			Criterion criterion=Restrictions.ne("genderId",0); 
			lstGender = findByCriteria(Order.asc("orderBy"),criterion);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstGender;
	}
	
	
	
	
	
}
