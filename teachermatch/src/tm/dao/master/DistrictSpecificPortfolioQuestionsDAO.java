package tm.dao.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EligibilityVerificationHistroy;
import tm.bean.JobCertification;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.bean.master.DspqGroupMaster;
import tm.bean.master.DspqPortfolioName;
import tm.bean.master.DspqSectionMaster;
import tm.bean.master.JobCategoryMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificPortfolioQuestionsDAO extends GenericHibernateDAO<DistrictSpecificPortfolioQuestions, Integer> 
{
	public DistrictSpecificPortfolioQuestionsDAO() 
	{
		super(DistrictSpecificPortfolioQuestions.class);
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioQuestions> getDistrictSpecificPortfolioQuestionBYDistrict(DistrictMaster districtMaster)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("status","A");	
			
			lstDistSpecQuestions = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	@Transactional(readOnly=false)
	public Map<Integer,List<DistrictSpecificPortfolioQuestions>> getDistrictSpecificPortfolioQuestion(JobOrder jobOrder,boolean isAlsoSA)
	{
		Map<Integer,List<DistrictSpecificPortfolioQuestions>> dSPQMap=new HashMap<Integer, List<DistrictSpecificPortfolioQuestions>>();
		List<DistrictSpecificPortfolioQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		try 
		{
			System.out.println("QPQ 01");
				Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
				Criterion criterion2 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
				Criterion criterion3 = null;
				Criterion criterion8 = Restrictions.ne("isCustom",true);
				if(jobOrder.getDistrictMaster()!=null)
				    criterion3 = Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
				else if(jobOrder.getHeadQuarterMaster()!=null)
					criterion3 = Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
				Criterion criterion4 = Restrictions.eq("status","A");
				Criterion criterion7 = Restrictions.ne("isDisabledPrincipal",isAlsoSA);
				if(criterion3!=null)
				{
				if(isAlsoSA)
					lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterion1,criterion3,criterion4,criterion7,criterion8);
				else
					lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterion1,criterion3,criterion4,criterion8);
				}
				int flag=0;
				if(lstDistSpecQuestions.size()==0){
					Criterion criterion6 = Restrictions.isNull("jobOrder");
					if(criterion3!=null)
					{
					if(isAlsoSA)
						lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterion2,criterion3,criterion4,criterion6,criterion7,criterion8);
					else
						lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterion2,criterion3,criterion4,criterion6,criterion8);
					}
					flag=1;
					System.out.println("QPQ 02");
				}
				if(lstDistSpecQuestions.size()==0){
					
					Criterion criterion5 = Restrictions.isNull("jobCategoryMaster");
					Criterion criterion6 = Restrictions.isNull("jobOrder");
					if(isAlsoSA)
						lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterion3,criterion4,criterion5,criterion6,criterion7,criterion8);
					else
						lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterion3,criterion4,criterion5,criterion6,criterion8);
					flag=2;
					System.out.println("QPQ 03");
				}
				
				if(lstDistSpecQuestions.size()==0 && jobOrder.getHeadQuarterMaster()!=null && jobOrder.getBranchMaster()==null){
					System.out.println("*** jobOrder.getHeadQuarterMaster().getHeadQuarterId() "+jobOrder.getHeadQuarterMaster().getHeadQuarterId() +" flag "+flag);
					//Criterion criterionH = Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
					//Criterion criterionB = Restrictions.isNull("branchMaster");
					
					Criterion criterionH = Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
					Criterion criterionB = Restrictions.isNull("branchId");
					
					if(isAlsoSA)
						lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterionH,criterionB,criterion4,criterion7,criterion8);
					else
						lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterionH,criterionB,criterion4,criterion8);
					flag=3;
					System.out.println("QPQ 04");
				}
				
				if(lstDistSpecQuestions.size()==0 && jobOrder.getHeadQuarterMaster()!=null && jobOrder.getBranchMaster()!=null){
					//Criterion criterionH = Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
					//Criterion criterionB = Restrictions.eq("branchMaster",jobOrder.getBranchMaster());
					
					Criterion criterionH = Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
					Criterion criterionB = Restrictions.eq("branchId",jobOrder.getBranchMaster().getBranchId());
					
					if(isAlsoSA)
						lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterionH,criterionB,criterion4,criterion7,criterion8);
					else
						lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterionH,criterionB,criterion4,criterion8);
					flag=4;
					System.out.println("QPQ 05");
				}
				
					if(lstDistSpecQuestions.size()==0 && jobOrder.getHeadQuarterMaster()!=null){
						System.out.println("*** jobOrder.getHeadQuarterMaster().getHeadQuarterId() "+jobOrder.getHeadQuarterMaster().getHeadQuarterId() +" flag "+flag);
						//Criterion criterionH = Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
						//Criterion criterionB = Restrictions.isNull("branchMaster");
						
						Criterion criterionH = Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
						Criterion criterionB = Restrictions.isNull("branchId");
						
						if(isAlsoSA)
							lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterionH,criterionB,criterion4,criterion7,criterion8);
						else
							lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterionH,criterionB,criterion4,criterion8);
						flag=3;
						System.out.println("QPQ 06");
					}
				
				dSPQMap.put(flag, lstDistSpecQuestions);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return dSPQMap;		
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioQuestions> getDistrictSpecificPortfolioQuestionBYJOb(JobOrder jobOrder)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
			Criterion criterion3 = null;
			if(jobOrder.getDistrictMaster()!=null)
				criterion3 = Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
			Criterion criterion4 = Restrictions.eq("status","A");
			if(criterion3!=null)
			lstDistSpecQuestions = findByCriteria(criterion1,criterion3,criterion4);
			
			if(lstDistSpecQuestions.size()==0){
				Criterion criterion6 = Restrictions.isNull("jobOrder");
				if(criterion3!=null)
				lstDistSpecQuestions = findByCriteria(criterion2,criterion3,criterion4,criterion6);
			}
			if(lstDistSpecQuestions.size()==0){
				Criterion criterion5 = Restrictions.isNull("jobCategoryMaster");
				Criterion criterion6 = Restrictions.isNull("jobOrder");
				if(criterion3!=null)
					lstDistSpecQuestions = findByCriteria(criterion3,criterion4,criterion5,criterion6);
			}
			
			if(lstDistSpecQuestions.size()==0 && jobOrder.getHeadQuarterMaster()!=null && jobOrder.getBranchMaster()==null)
			{
				/*Criterion criterionH = Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
				Criterion criterionB = Restrictions.isNull("branchMaster");*/
				
				Criterion criterionH = Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
				Criterion criterionB = Restrictions.isNull("branchId");
				
				lstDistSpecQuestions = findByCriteria(criterion4,criterionH,criterionB);
			}
			if(lstDistSpecQuestions.size()==0 && jobOrder.getHeadQuarterMaster()!=null && jobOrder.getBranchMaster()!=null)
			{
				/*Criterion criterionH = Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
				Criterion criterionB = Restrictions.eq("branchMaster",jobOrder.getBranchMaster());*/
				
				Criterion criterionH = Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
				Criterion criterionB = Restrictions.eq("branchId",jobOrder.getBranchMaster().getBranchId());
				
				lstDistSpecQuestions = findByCriteria(criterion4,criterionH,criterionB);
			}
			
			if(lstDistSpecQuestions.size()==0 && jobOrder.getHeadQuarterMaster()!=null)
			{
				/*Criterion criterionH = Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
				Criterion criterionB = Restrictions.eq("branchMaster",jobOrder.getBranchMaster());*/
				
				Criterion criterionH = Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
				Criterion criterionB = Restrictions.isNull("branchId");
				
				lstDistSpecQuestions = findByCriteria(criterion4,criterionH,criterionB);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistSpecQuestions;		
	}
	
	@Transactional(readOnly=false)
	public
	Map<Integer,List<DistrictSpecificPortfolioQuestions>> getDistrictSpecificPortfolioBYDistrict(DistrictMaster districtMaster)
	{
		Map<Integer,List<DistrictSpecificPortfolioQuestions>> dSPQMap=new HashMap<Integer, List<DistrictSpecificPortfolioQuestions>>();
		List<DistrictSpecificPortfolioQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion5 = Restrictions.isNull("jobCategoryMaster");
			Criterion criterion6 = Restrictions.isNull("jobOrder");
			
			int flag=0;
				lstDistSpecQuestions = findByCriteria(Order.asc("questionOrder"),criterion1,criterion2,criterion5,criterion6);
				flag=2;			
			dSPQMap.put(flag, lstDistSpecQuestions);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return dSPQMap;		
	}
		
	@Transactional(readOnly=false)
	public Map<Integer,List<DistrictSpecificPortfolioQuestions>> getDSPQForPortfolioReminder(DistrictMaster district)
	{
		Map<Integer,List<DistrictSpecificPortfolioQuestions>> dSPQMap=new HashMap<Integer, List<DistrictSpecificPortfolioQuestions>>();
		List<DistrictSpecificPortfolioQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		try 
		{
				Criterion criterion1 = Restrictions.eq("districtMaster",district);
				Criterion criterion2 = Restrictions.eq("status","A");
					lstDistSpecQuestions = findByCriteria(criterion1,criterion2);
					System.out.println("lstDistSpecQuestions Size::"+lstDistSpecQuestions.size());
				    dSPQMap.put(2, lstDistSpecQuestions);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return dSPQMap;		
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioQuestions> getQuestionList(List dspqQuestionList)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		try 
		{	if(dspqQuestionList.size()>0){
				Criterion criterion1 = Restrictions.in("questionId",dspqQuestionList);
				Criterion criterion2 = Restrictions.eq("status","A");
				lstDistrictSpecificPortfolioQuestions = findByCriteria(criterion1,criterion2);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictSpecificPortfolioQuestions;
	}

	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioQuestions> getCustumFieldBySection(DistrictMaster districtMaster,DspqGroupMaster dspqGroupMaster, DspqSectionMaster dspqSectionMaster,String applicantType,DspqPortfolioName dspqPortfolioName)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		try 
		{
			
			/*Criteria criteria = session.createCriteria(getPersistentClass())
			.add(Restrictions.eq("districtMaster",districtMaster))
			.add(Restrictions.eq("status","A"))
			.add(Restrictions.eq("isCustom",true))
			.add(Restrictions.eq("dspqGroupMaster",dspqGroupMaster))
			.add(Restrictions.eq("dspqSectionMaster",dspqSectionMaster))
			.add(Restrictions.eq("dspqPortfolioName",dspqPortfolioName))
			.setProjection(Projections.projectionList()
			.add(Projections.groupProperty("parentQuestionId")));
			if(applicantType!=null && !applicantType.equalsIgnoreCase("")){
			criteria.add(Restrictions.eq("applicantType",applicantType));
			}
			
			lstDistrictSpecificPortfolioQuestions=criteria.setResultTransformer( new AliasToBeanResultTransformer(this.getPersistentClass())).list();
			//lstDistrictSpecificPortfolioQuestions=criteria.list();
			*/
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("isCustom",true);
			Criterion criterion4 = Restrictions.eq("dspqGroupMaster",dspqGroupMaster);
			Criterion criterion5 = Restrictions.eq("dspqSectionMaster",dspqSectionMaster);
			Criterion criterion8 = Restrictions.eq("dspqPortfolioName",dspqPortfolioName);
			if(applicantType.equalsIgnoreCase("")){
				Criterion criterion7 = Restrictions.eq("applicantType","E");
				lstDistrictSpecificPortfolioQuestions=findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion7,criterion8);
			}else{
				Criterion criterion7 = Restrictions.eq("applicantType",applicantType);
				lstDistrictSpecificPortfolioQuestions=findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion7,criterion8);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistrictSpecificPortfolioQuestions;		
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioQuestions> getDistrictSpecificPortfolioQuestionBYDistrictByAD(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.isNotNull("isCustom");
			Criterion criterion3 = null;
			if(jobCategoryMaster!=null)
				criterion3 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			else
				criterion3 = Restrictions.isNull("jobCategoryMaster");
			lstDistSpecQuestions = findByCriteria(criterion1,criterion2,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioQuestions> getDistrictSpecificPortfolioQuestionBYPortfolioNameId(DspqPortfolioName dspqPortfolioName)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("dspqPortfolioName",dspqPortfolioName);
			Criterion criterion2 = Restrictions.isNotNull("isCustom");			
			lstDistSpecQuestions = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioQuestions> getDistrictSpecificPortfolioQuestionByParentQuestionId(Integer parentQuestionId)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("parentQuestionId",parentQuestionId);
			Criterion criterion2 = Restrictions.isNotNull("isCustom");			
			lstDistSpecQuestions = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioQuestions> getDistrictSpecificPortfolioBySecID(DspqPortfolioName dspqPortfolioName,DspqSectionMaster dspqSectionMaster)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("dspqPortfolioName",dspqPortfolioName);
			Criterion criterion3 = Restrictions.eq("dspqSectionMaster",dspqSectionMaster);
			Criterion criterion2 = Restrictions.isNotNull("isCustom");			
			lstDistSpecQuestions = findByCriteria(criterion1,criterion2,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioQuestions> getDistrictSpecificPortfolioQuestionByDistrictGroupByQuestion(DistrictMaster districtMaster)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		
		Session session = getSession();				
		Criteria criteria = session.createCriteria(DistrictSpecificPortfolioQuestions.class);
		
		
		lstDistSpecQuestions = criteria.add(Restrictions.eq("districtMaster",districtMaster))
								.add(Restrictions.eq("status","A"))
								.setProjection(Projections.projectionList()
										.add(Projections.property("questionId"))
										.add(Projections.groupProperty("question"))
								).list();
		
		return lstDistSpecQuestions;
	}

	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioQuestions> getCustumFieldByQuockJobApply(DistrictMaster districtMaster,DspqPortfolioName dspqPortfolioName,String applicantType)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("isCustom",true);
			Criterion criterion7 = Restrictions.eq("applicantType",applicantType);
			Criterion criterion8 = Restrictions.eq("dspqPortfolioName",dspqPortfolioName);
			
			lstDistrictSpecificPortfolioQuestions=findByCriteria(criterion1,criterion2,criterion3,criterion7,criterion8);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistrictSpecificPortfolioQuestions;		
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioQuestions> getCustomFieldBySection(List<DistrictMaster> dspqPotfolioDM,List<DspqSectionMaster> lstdspqSectionMaster, String applicantType,List<DspqPortfolioName> dspqPortfolioName)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		Session session = getSession();	
		try 
		{					
			Criterion criterion1 = Restrictions.in("districtMaster",dspqPotfolioDM);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("isCustom",true);			
			Criterion criterion5 = Restrictions.in("dspqSectionMaster",lstdspqSectionMaster);
			Criterion criterion8 = Restrictions.in("dspqPortfolioName",dspqPortfolioName);
			Criterion criterion9 = Restrictions.ne("applicantType","T");
			
			List<Order> lstOrder=new ArrayList<Order>();
			lstOrder.add(Order.asc("dspqPortfolioName"));
			lstOrder.add(Order.asc("dspqGroupMaster"));
			lstOrder.add(Order.asc("dspqSectionMaster"));
			
			if(applicantType!=null && applicantType.equalsIgnoreCase("")){
				Criterion criterion7 = Restrictions.eq("applicantType","E");
				lstDistrictSpecificPortfolioQuestions=findByCriteria(lstOrder,criterion1,criterion2,criterion3,criterion5,criterion7,criterion8,criterion9);
			}else{
				Criterion criterion7 = Restrictions.eq("applicantType",applicantType);
				lstDistrictSpecificPortfolioQuestions=findByCriteria(lstOrder,criterion1,criterion2,criterion3,criterion5,criterion7,criterion8,criterion9);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistrictSpecificPortfolioQuestions;		
	}
	
	
	@Transactional(readOnly=true)
	public List<Object> getSectionForNewSS(String applicantType,DspqPortfolioName dspqPortfolioNameId)
	{
		List<Object> lstDistSpecQuestions= new ArrayList<Object>();
		Session session = getSession();				
		Criteria criteria = session.createCriteria(DistrictSpecificPortfolioQuestions.class);
		lstDistSpecQuestions = criteria.createAlias("dspqSectionMaster", "dspqsec").add(Restrictions.eq("status","A")).add(Restrictions.eq("applicantType",applicantType)).add(Restrictions.eq("isCustom",true)).add(Restrictions.eq("dspqPortfolioName",dspqPortfolioNameId))
								.setProjection(Projections.projectionList()
										.add(Projections.property("dspqsec.sectionId"))
										.add(Projections.groupProperty("dspqsec.sectionId"))
								).list();
		
		return lstDistSpecQuestions;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioQuestions> getCustomQuestionByDistrictList(List<DistrictMaster> districtMasterList)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions= new ArrayList<DistrictSpecificPortfolioQuestions>();
		//Session session = getSession();	
		try 
		{					
			Criterion criterion1 = Restrictions.in("districtMaster",districtMasterList);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("isCustom",false);
			
			List<Order> lstOrder=new ArrayList<Order>();
			lstOrder.add(Order.asc("districtMaster"));
			lstOrder.add(Order.asc("jobCategoryMaster"));
			
			lstDistrictSpecificPortfolioQuestions=findByCriteria(lstOrder,criterion1,criterion2,criterion3);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistrictSpecificPortfolioQuestions;		
	}
	
}

