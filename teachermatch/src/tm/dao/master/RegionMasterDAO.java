package tm.dao.master;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.GeographyMaster;
import tm.bean.master.RegionMaster;
import tm.dao.generic.GenericHibernateDAO;

public class RegionMasterDAO extends GenericHibernateDAO<RegionMaster, Integer> 
{
	public RegionMasterDAO() 
	{
		super(RegionMaster.class);
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get active Regions.
	 */
	@Transactional(readOnly=true)
	public List<RegionMaster> getActiveRegions()
	{
		List<RegionMaster> regionMasters = null;
		try {
			Order  order=Order.asc("regionName");
			 Criterion criterion = Restrictions.eq("status", "A");
		     regionMasters = findByCriteria(order,criterion);
		    } catch (Exception e) {
			e.printStackTrace();
		}
		return regionMasters;
	}
}
