package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.JobCategoryMasterHistory;
import tm.dao.generic.GenericHibernateDAO;

public class JobCategoryMasterHistoryDAO extends GenericHibernateDAO<JobCategoryMasterHistory, Integer> 
{
	public JobCategoryMasterHistoryDAO() 
	{
		super(JobCategoryMasterHistory.class);
	}
	
	@Transactional(readOnly=true)
	public int totalJobCategoryHistoryByPrevId(Integer jobCategoryId){
		int result =0;
		try{
			Session session = getSession();
			System.out.println("session  :: "+session);
			Criteria criteria = session.createCriteria(JobCategoryMasterHistory.class);
			
			List<JobCategoryMasterHistory> data = new ArrayList<JobCategoryMasterHistory>();
			
			 
			//Criterion criterion1 	= 	Restrictions.eq("jobCategoryId",jobCategoryId);
			 
			criteria.add(Restrictions.eq("jobCategoryId",jobCategoryId));
			data =  criteria.list();
		
		 if(data!=null)
		 result = data.size();
		}
		catch (Exception e) {
			 
			e.printStackTrace();
		}
		 
		return result;
	}
	
	
	@Transactional(readOnly=true)
	public List listJobCategoryHistoryByPrevId(Integer jobCategoryId){
		List<JobCategoryMasterHistory> data = new ArrayList<JobCategoryMasterHistory>();
		 try{
			Session session = getSession();
			System.out.println("session  :: "+session);
			Criteria criteria = session.createCriteria(JobCategoryMasterHistory.class);
			criteria.add(Restrictions.eq("jobCategoryId",jobCategoryId));
			criteria.addOrder(Order.desc("jobCategoryHistoryId"));
			data =  criteria.list();
		
		  
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		 
		return data;
	}
	
}