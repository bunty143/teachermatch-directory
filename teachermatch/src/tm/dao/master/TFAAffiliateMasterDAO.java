package tm.dao.master;

import tm.bean.master.TFAAffiliateMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TFAAffiliateMasterDAO extends GenericHibernateDAO<TFAAffiliateMaster,Integer >
{
	public TFAAffiliateMasterDAO()
	{
		super(TFAAffiliateMaster.class);
	}
}
