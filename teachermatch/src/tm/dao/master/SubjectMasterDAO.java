package tm.dao.master;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SubjectMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SubjectMasterDAO extends GenericHibernateDAO<SubjectMaster, Integer> {
	public SubjectMasterDAO() {
		super(SubjectMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<SubjectMaster> findActiveSubject(){
		List<SubjectMaster> subjectMasters=new ArrayList<SubjectMaster>();
		try{
			subjectMasters=getSession().createCriteria(SubjectMaster.class).add(Restrictions.eq("status", "A")).addOrder(Order.asc("subjectName")).list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return subjectMasters;
	}
	
	/* @Start
	 * @Ashish
	 * @Description :: get Object List of subjectMaster according subject Id   
	 * */
	
	
		@Transactional(readOnly=false)
		public List<SubjectMaster> findActiveSubjectByDistrict(DistrictMaster districtMaster)
		{
			List<SubjectMaster> subjectMasters=new ArrayList<SubjectMaster>();
			try{
				if(districtMaster!=null){
					subjectMasters=getSession().createCriteria(SubjectMaster.class)
								.add(Restrictions.eq("status", "A"))
								.add(Restrictions.eq("districtMaster", districtMaster))
								.addOrder(Order.asc("subjectName")).list();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			return subjectMasters;
		}

	
	
	
		@Transactional(readOnly=false)
		public List<SubjectMaster> getSubjectMasterlist(ArrayList<Integer> subjectIdList)
		{
			List<SubjectMaster> subjectMasters=new ArrayList<SubjectMaster>();
			try{
				subjectMasters=getSession().createCriteria(SubjectMaster.class).add(Restrictions.in("subjectId", subjectIdList)).list();
				System.out.println(" size of subject master :: "+subjectMasters.size());
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			return subjectMasters;
		}
		
		// Get subject master Data form MasterSubjectIds
		@Transactional(readOnly=false)
		public String getIdsFormMasterSubjectIds(String Ids)
		{	
			List<SubjectMaster> subjectMasters=new ArrayList<SubjectMaster>();
			List<SubjectMaster> subMasters = new ArrayList<SubjectMaster>();
			HashSet<String> uniqueSubIds = new HashSet<String>();
			String uniqueStr="";
			
			try{
				String subIdList[] = Ids.split(",");
		
				if(Integer.parseInt(subIdList[0])!=0)
				{	
					for(int i=0;i<subIdList.length;i++)
					{
						subjectMasters = getSession()
										.createCriteria(SubjectMaster.class)
										.add(Restrictions.like("masterSubjectId","%"+subIdList[i]+"%")).list();
						
						subMasters.addAll(subjectMasters);
					}
					
					//String s= subjectMasters.get(0).getSubjectId().toString();
					for(int z=0;z<subMasters.size();z++)
					{
						uniqueSubIds.add(subMasters.get(z).getSubjectId().toString());
					}
					
					Iterator itr=uniqueSubIds.iterator();
					
					while(itr.hasNext()){
						uniqueStr+=((String)itr.next())+",";
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
		
			return uniqueStr;
		}
		
		// Get subject master Data form MasterSubjectIds
		@Transactional(readOnly=false)
		public SubjectMaster getSubjectByNameAndDistrict(DistrictMaster districtMaster,String subName)
		{	
			System.out.println(" ===========Inside getSubjectByNameAndDistrict ===========");
			SubjectMaster subjectMaster = new SubjectMaster();
			
			try {
				Criterion c1 = Restrictions.eq("status", "A");
				Criterion c2 = Restrictions.eq("districtMaster", districtMaster);
				Criterion c3 = Restrictions.eq("subjectName", subName.trim());
				subjectMaster = findByCriteria(c1,c2,c3).get(0);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println(" ----------- subject master :: --------- "+subjectMaster.getSubjectName());
			
			return subjectMaster;
		}
		
		
	/* @End
	 * @Ashish
	 * @Description :: get Object List of subjectMaster according subject Id   
	 * */
	
		@Transactional(readOnly=false)
		public List<SubjectMaster> findActiveSubjectByDistrict_OP(DistrictMaster districtMaster)
		{
			List<SubjectMaster> subjectMastersList=new ArrayList<SubjectMaster>();
			try 
			{
	  			Query query1=null;
				if(districtMaster!=null){
	 				Session session = getSession();
	 				 
	 				  
	 				String hql=	"SELECT   sm.subjectId,sm.subjectName   FROM subjectmaster sm "
				 	    +   "left join districtmaster dm on  dm.districtId= sm.districtId  "
					    + " WHERE   sm.status=? and sm.districtId=? ";
	 				    hql=hql+ "order by  sm.subjectName  ASC ";
	 				  
					query1=session.createSQLQuery(hql);
	 				query1.setParameter(0, 'A');
	 				query1.setParameter(1, districtMaster.getDistrictId());
	 				List<Object[]> objList = query1.list(); 
	 				SubjectMaster obj=null;
					if(objList.size()>0){
					for (int j = 0; j < objList.size(); j++) {
							Object[] objArr2 = objList.get(j);
							obj = new SubjectMaster();
						 	obj.setSubjectId(objArr2[0] == null ? 0 : Integer.parseInt(objArr2[0].toString()));
						    obj.setSubjectName(objArr2[1] == null ? "NA" :  objArr2[1].toString());
			 			    subjectMastersList.add(obj);
					    }
					}
				 
				}
			} 
		 	catch(Exception e){
				   
			         e.printStackTrace(); 
			}finally{
				// session.close(); 
			}
			return subjectMastersList;
		}	
}
