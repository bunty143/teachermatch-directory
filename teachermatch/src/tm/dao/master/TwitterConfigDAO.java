package tm.dao.master;

import tm.bean.master.TwitterConfig;
import tm.dao.generic.GenericHibernateDAO;

/* @Author: Vishwanath Kumar
 * @Discription: 
 */
public class TwitterConfigDAO extends GenericHibernateDAO<TwitterConfig, Integer> 
{
	public TwitterConfigDAO() {
		super(TwitterConfig.class);
	}
}
