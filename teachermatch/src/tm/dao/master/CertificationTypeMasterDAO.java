package tm.dao.master;

import tm.bean.master.CertificationTypeMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CertificationTypeMasterDAO extends	GenericHibernateDAO<CertificationTypeMaster, Integer>
{
	public CertificationTypeMasterDAO()
	{
		super(CertificationTypeMaster.class);
	}
}
