package tm.dao.master;

import java.util.ArrayList;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.python.antlr.op.Add;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.JobCategoryMaster;
import tm.bean.master.MasterSubjectMaster;
import tm.dao.generic.GenericHibernateDAO;

public class MasterSubjectMasterDAO extends GenericHibernateDAO<MasterSubjectMaster, Integer>
{
	public MasterSubjectMasterDAO() {
		super(MasterSubjectMaster.class);
	}

	@Transactional(readOnly=true)
	public List<MasterSubjectMaster> findAllActiveSuject()
	{
		  List <MasterSubjectMaster> lstmsm = null;
	     
	     try{
	         
			 Criterion criterion = Restrictions.eq( "status","A");
			 lstmsm = findByCriteria(Order.asc("masterSubjectName"),criterion);
	     
	     }catch(Exception e){
	    	 e.printStackTrace();
	     }
	    
	     return lstmsm;
	}
	
}
