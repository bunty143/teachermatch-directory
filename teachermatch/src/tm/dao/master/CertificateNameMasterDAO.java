package tm.dao.master;

import tm.bean.master.CertificateNameMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CertificateNameMasterDAO extends GenericHibernateDAO<CertificateNameMaster, Integer>
{
	public CertificateNameMasterDAO() 
	{
		super(CertificateNameMaster.class);
	}
}
