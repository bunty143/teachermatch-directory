package tm.dao.master;

import tm.bean.master.LinkedInConfig;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Vishwanath Kumar
 * @Discription: 
 */
public class LinkedInConfigDAO extends GenericHibernateDAO<LinkedInConfig, Integer> 
{
	public LinkedInConfigDAO() {
		super(LinkedInConfig.class);
	}
}
