package tm.dao.master;

import tm.bean.master.DistrictAttachment;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictAttachmentDAO extends GenericHibernateDAO<DistrictAttachment, Integer> 
{
	public DistrictAttachmentDAO() 
	{
		super(DistrictAttachment.class);
	}

}
