package tm.dao.master;

import tm.bean.master.EthinicityMaster;
import tm.dao.generic.GenericHibernateDAO;

public class EthinicityMasterDAO extends GenericHibernateDAO<EthinicityMaster,Integer> {
	public EthinicityMasterDAO() {
		super(EthinicityMaster.class);
	}
	
}
