package tm.dao.master;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.ObjectiveMaster;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Vishwanath Kumar
 * @Discription: ObjectiveMaster DAO.
 */
public class ObjectiveMasterDAO extends GenericHibernateDAO<ObjectiveMaster, Integer> 
{
	public ObjectiveMasterDAO() {
		super(ObjectiveMaster.class);
	}
	
/* @Author: Vishwanath Kumar
 * @Discription: It is used to get all active Objectives.
 */	
	@Transactional(readOnly=true)
	public List<ObjectiveMaster> getActiveObjectives()
	{
		List<ObjectiveMaster> objectiveMasters =  null;
		try{
			
			Criterion criterion =  Restrictions.eq("status", "a");
			objectiveMasters = findByCriteria(Order.asc("objectiveName"), criterion);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return objectiveMasters;
	}
	
/* @Author: Vishwanath Kumar
 * @Discription: It is used to get Objectives when competencyId is given.
 */	
	@Transactional(readOnly=true)
	public List<ObjectiveMaster> getObjectivesByCompetency(CompetencyMaster competencyMaster)
	{
		List<ObjectiveMaster> objectiveMasters =  null;
		try{
			Criterion criterion =  Restrictions.eq("status", "a");
			Criterion criterion1 =  Restrictions.eq("competencyMaster", competencyMaster);
			objectiveMasters = findByCriteria(Order.asc("objectiveName"), criterion,criterion1);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return objectiveMasters;
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Objectives when competencyId is given No ordering.
	 */	
		@Transactional(readOnly=true)
		public List<ObjectiveMaster> getObjectivesByCompetencyNoOrdering(CompetencyMaster competencyMaster)
		{
			List<ObjectiveMaster> objectiveMasters =  null;
			try{
				Criterion criterion =  Restrictions.eq("status", "a");
				Criterion criterion1 =  Restrictions.eq("competencyMaster", competencyMaster);
				//objectiveMasters = findByCriteria(Order.asc("objectiveName"), criterion,criterion1);
				objectiveMasters = findByCriteria(criterion,criterion1);
				
			}catch (Exception e) {
				e.printStackTrace();
			}
			return objectiveMasters;
		}
		
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<ObjectiveMaster> checkDuplicateObjectiveName(CompetencyMaster competencyMaster,String objectiveName,Integer objectiveId) 
	{
		Session session = getSession();

		if(objectiveId!=null)
		{
			List result = session.createCriteria(getPersistentClass())   
			//.add(Restrictions.eq("competencyMaster", competencyMaster)) 
			.add(Restrictions.not(Restrictions.in("objectiveId",new Integer[]{objectiveId}))) 
			.add(Restrictions.eq("objectiveName", objectiveName)) 
			.add(Restrictions.eq("competencyMaster.competencyId", competencyMaster.getCompetencyId())) 
			.list();
			return result;

		}
		else
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.eq("objectiveName", objectiveName)) 
			.add(Restrictions.eq("competencyMaster.competencyId", competencyMaster.getCompetencyId())) 
			.list();
			return result;
		}
	}
	
	@Transactional(readOnly=true)
	public List<ObjectiveMaster> findObjectivesByCompetencyAndUId(CompetencyMaster competencyMaster,String objectiveUId)
	{
		List<ObjectiveMaster> objectiveMasterList =  null;
		try{
			Criterion criterion1 =  Restrictions.eq("competencyMaster", competencyMaster);
			Criterion criterion2 =  Restrictions.eq("objectiveUId", objectiveUId);
			objectiveMasterList = findByCriteria(criterion1,criterion2);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return objectiveMasterList;
	}
}
