package tm.dao.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.StatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class StatusMasterDAO extends GenericHibernateDAO<StatusMaster, Integer> 
{
	public StatusMasterDAO() 
	{
		super(StatusMaster.class);
	}
	
	@Transactional(readOnly=false)
	public StatusMaster findStatusByShortName(String statusShortName)
	{
		List<StatusMaster> lstStatus= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("statusShortName",statusShortName);
			lstStatus = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
		return null;
		else
		return lstStatus.get(0);
	}
	
	@Transactional(readOnly=false)
	public StatusMaster findStatusByShortNameHired(String statusShortName)
	{
		List<StatusMaster> lstStatus= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("statusShortName",statusShortName);
			lstStatus = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
		return null;
		else
		return lstStatus.get(0);
	}
	
	
	
	
	@Transactional(readOnly=false)
	public List<StatusMaster> findStatusByShortNames(String[] statusShortNames)
	{
		List<StatusMaster> lstStatus= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("statusShortName",statusShortNames);
			lstStatus = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstStatus;
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<Integer,StatusMaster> findStatusBySName(String statusShortName) 
	{
		Session session = getSession();
		List result = session.createCriteria(getPersistentClass())
		.add(Restrictions.eq("statusShortName",statusShortName))
		.list();

		Map<Integer,StatusMaster> statusMasterMap = new HashMap<Integer, StatusMaster>();
		StatusMaster statusMaster = null;
		int i=0;
		for (Object object : result) {
			statusMaster=((StatusMaster)object);
			statusMasterMap.put(new Integer(""+i),statusMaster);
			i++;
		}
		return statusMasterMap;
	}
	@Transactional(readOnly=false)
	public List<StatusMaster> findAllStatusMaster()
	{
		List<StatusMaster> lstStatus= null;
		try 
		{
			lstStatus = findByCriteria();		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
		return null;
		else
		return lstStatus;
	}
	
	/* ===================== Gagan : get Status List By Status Id List*/
	@Transactional(readOnly=false)
	public List<StatusMaster> findStatusByStatusIdList(List statusIdList)
	{
		List<StatusMaster> lstStatus= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("statusId",statusIdList);
			lstStatus = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<StatusMaster> findStatusNodeList()
	{
		List<StatusMaster> lstStatus= null;
		try 
		{
			Criterion criterion1 = Restrictions.isNotNull("statusNodeMaster");
			lstStatus = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
		return null;
		else
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<Integer> findStatusIdsByStatusByShortNames(String[] statusShortNames)
	{
		List<StatusMaster> lstStatus= null;
		List<Integer> listStatusIds= new ArrayList<Integer>();
		try 
		{
			Criterion criterion1 = Restrictions.in("statusShortName",statusShortNames);
			lstStatus = findByCriteria(criterion1);	
			
			if(lstStatus.size()>0){
				for(StatusMaster  statusMaster:  lstStatus){
					listStatusIds.add(statusMaster.getStatusId());
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return listStatusIds;
	}
	@Transactional(readOnly=false)
	public List<StatusMaster> findStatusByStatusByShortNames(String[] statusShortNames)
	{
		List<StatusMaster> listStatusMaster= new ArrayList<StatusMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.in("statusShortName",statusShortNames);
			listStatusMaster = findByCriteria(criterion1);	
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return listStatusMaster;
	}
	@Transactional(readOnly=false)
	public List<StatusMaster> findStatusByShortNameList(List<String> statusShortNames)
	{
		List<StatusMaster> lstStatus= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("statusShortName",statusShortNames);
			lstStatus = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<StatusMaster> findStatusByShortNameHashSet(HashSet<String> statusShortNames)
	{
		List<StatusMaster> lstStatus= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("statusShortName",statusShortNames);
			lstStatus = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstStatus;
	}
}
