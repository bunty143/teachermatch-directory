package tm.dao.master;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.python.antlr.PythonParser.list_for_return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictNotes;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSchoolsDAO extends GenericHibernateDAO<DistrictSchools, Integer> 
{
	public DistrictSchoolsDAO()
	{
		super(DistrictSchools.class);
	}
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Transactional(readOnly=false)
	public List<DistrictSchools> findSchoolId(List<SchoolMaster> lstSchoolMaster)
	{
		List<DistrictSchools> lstDistrictSchools= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("schoolMaster", lstSchoolMaster);			
			lstDistrictSchools = findByCriteria(criterion1);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstDistrictSchools;
	}
	@Transactional(readOnly=false)
	public List findSchoolIdList(DistrictMaster districtMaster)
	{
		List SchoolIds= new ArrayList();
		try{
			Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);			
			List<DistrictSchools> lstDistrictSchools = findByCriteria(criterion1);	
			for(DistrictSchools districtSchools: lstDistrictSchools){
				SchoolIds.add(districtSchools.getSchoolMaster().getSchoolId());
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return SchoolIds;
	}
	@Transactional(readOnly=false)
	public List findSchoolIdAllList()
	{
		List SchoolIds= new ArrayList();
		try{
			List<DistrictSchools> lstDistrictSchools = findByCriteria();	
			for(DistrictSchools districtSchools: lstDistrictSchools){
				SchoolIds.add(districtSchools.getSchoolMaster().getSchoolId());
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return SchoolIds;
	}
	@Transactional(readOnly=false)
	public List findSchoolObjAllList(DistrictMaster districtMaster)
	{
		List SchoolIds= new ArrayList();
		try{
			Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);			
			List<DistrictSchools> lstDistrictSchools = findByCriteria(criterion1);	
			for(DistrictSchools districtSchools: lstDistrictSchools){
				SchoolIds.add(districtSchools.getSchoolMaster());
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return SchoolIds;
	}
	
	@Transactional(readOnly=false)
	public List<SchoolMaster> findZoneWiseSchools(Integer districtId)
	{
		List<SchoolMaster> schoolMasters=new  ArrayList<SchoolMaster>(); 
		try{
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			List<DistrictSchools>districtSchools=findByCriteria(criterion1);
			for(DistrictSchools ds:districtSchools)
			{
				schoolMasters.add(ds.getSchoolMaster());
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return schoolMasters;
	}
	
	@Transactional(readOnly=false)
	public void allSchoolInsertInDistrictSchools(List<SchoolMaster> lstSchoolMaster,DistrictMaster districtMaster,UserMaster userMaster)
	{
		try 
		{
			for(SchoolMaster schoolMaster :lstSchoolMaster){
				DistrictSchools districtSchools = new DistrictSchools();
				districtSchools.setDistrictMaster(districtMaster);
				districtSchools.setSchoolMaster(schoolMaster);
				districtSchools.setSchoolName(schoolMaster.getSchoolName());
				districtSchools.setCreatedBy(userMaster);
				districtSchools.setCreatedDateTime(new Date());
				makePersistent(districtSchools);
			}	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	/* @Author: Sekhar 
	 * @Discription: It is used to delete DistrictSchools   .
	 */
	@Transactional(readOnly=false)
	public boolean deleteDistrictSchools(DistrictMaster districtMaster){
		try{
			DistrictNotes districtNotes			=	null;
			List<DistrictSchools> lstDistrictSchools		=	null;
			Criterion criterionDSchool = Restrictions.eq("districtMaster",districtMaster);
			lstDistrictSchools=findByCriteria(criterionDSchool);
			for(DistrictSchools districtSchools: lstDistrictSchools){
				makeTransient(districtSchools);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSchools> findByZip(String zipCode)
	{
		List<DistrictSchools> lstSchoolInJobOrder= null;		
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		
		try{
			criteria.createCriteria("schoolMaster")
			.add(Restrictions.eq("zip", new Integer(zipCode)));
			lstSchoolInJobOrder = criteria.list();
			System.out.println("---------districtScholls DAO-------lstSchoolInJobOrder-----------------:: "+lstSchoolInJobOrder.size());
			
		}catch (Exception e){
			e.printStackTrace();
			lstSchoolInJobOrder = new ArrayList<DistrictSchools>();
		}		
		return lstSchoolInJobOrder; 
	}
	
	/* @Start
	 * @Ashish Kuamr
	 * @Description :: find Data By ZipCode List
	 * */
	@Transactional(readOnly=false)
	public List<DistrictSchools> findByZipCodeList(List<String> zipCode,DistrictMaster districtMaster)
	{
		List<DistrictSchools> lstSchoolInJobOrder= null;		
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		
		List<Integer> zCode = new ArrayList<Integer>();
		
		for(int i=0; i<zipCode.size();i++){
			zCode.add(new Integer(zipCode.get(i)));
		}
		
		
		try{
			criteria.createCriteria("schoolMaster")
			.add(Restrictions.in("zip", zCode))
			.add(Restrictions.eq("districtId",districtMaster));
			lstSchoolInJobOrder = criteria.list();
			
			System.out.println(" ----lstSchoolInJobOrder.size()===:: "+lstSchoolInJobOrder.size());
			
			
		}catch (Exception e){
			e.printStackTrace();
			lstSchoolInJobOrder = new ArrayList<DistrictSchools>();
		}		
		return lstSchoolInJobOrder; 
	}
	
	// Find Data By ZipCode List And District List 
	@Transactional(readOnly=false)
	public List<DistrictSchools> findByZipCodeListAndDistrictList(List<String> zipCode,List<DistrictMaster> lstdistrictMaster)
	{
		List<DistrictSchools> lstSchoolInJobOrder= null;		
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		
		List<Integer> zCode = new ArrayList<Integer>();
		
		for(int i=0; i<zipCode.size();i++){
			zCode.add(new Integer(zipCode.get(i)));
		}
		
		
		try{
			criteria.createCriteria("schoolMaster")
			.add(Restrictions.in("zip", zCode))
			.add(Restrictions.in("districtId",lstdistrictMaster));
			lstSchoolInJobOrder = criteria.list();
			
		//	System.out.println(" ----lstSchoolInJobOrder.size()===:: "+lstSchoolInJobOrder.size());
			
			
		}catch (Exception e){
			e.printStackTrace();
			lstSchoolInJobOrder = new ArrayList<DistrictSchools>();
		}		
		return lstSchoolInJobOrder; 
	}
	/* @End
	 * @Ashish Kuamr
	 * @Description :: find Data By ZipCode List
	 * */
	
	
	@Transactional(readOnly=false)
	public List<DistrictSchools> findByZipAndDistrict(DistrictMaster districtMaster, String zipCode)
	{
		List<DistrictSchools> lstSchoolInJobOrder= null;		
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		
		
		try{
			criteria.add(Restrictions.eq("districtMaster", districtMaster));
			criteria.createCriteria("schoolMaster")
			.add(Restrictions.eq("zip", new Integer(zipCode)));
			lstSchoolInJobOrder = criteria.list();
		}catch (Exception e){
			e.printStackTrace();
		}		
		return lstSchoolInJobOrder; 
	}
	
	
		@Transactional(readOnly=false)
		public DistrictSchools findBySchoolMaster(SchoolMaster schoolMaster)
		{
			System.out.println(" inside findBySchoolMaster ");
			
			DistrictSchools districtSchools = new DistrictSchools();		
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			try{
				
				districtSchools = findByCriteria(Restrictions.eq("schoolMaster", schoolMaster)).get(0);
				
			}catch (Exception e){
				e.printStackTrace();
			}		
			return districtSchools; 
		}
		
		@Transactional(readOnly=false)
		public DistrictSchools findByDistrictAndSchool(DistrictMaster districtMaster, SchoolMaster schoolMaster)
		{
			System.out.println(" inside findBySchoolMaster ");
			
			DistrictSchools districtSchools = new DistrictSchools();		
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			criteria.add(Restrictions.eq("districtMaster", districtMaster));
			try{				
				districtSchools = (DistrictSchools) criteria.list().get(0);
				
			}catch (Exception e){
				e.printStackTrace();
			}		
			return districtSchools; 
		}
	
	
	
}
