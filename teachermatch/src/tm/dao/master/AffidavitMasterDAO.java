package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.AffidavitMaster;
import tm.dao.generic.GenericHibernateDAO;

public class AffidavitMasterDAO extends GenericHibernateDAO<AffidavitMaster, Integer>{
	public AffidavitMasterDAO() {
		super(AffidavitMaster.class);
	}
	@Autowired
	private SessionFactory sessionFactory;
	@Transactional(readOnly=true)
	public List<AffidavitMaster> getAllActiveAffidavit(){
		List<AffidavitMaster> allAM=new ArrayList<AffidavitMaster>();
		try{
			Session session = getSession();
			Criteria criteria=session.createCriteria(getPersistentClass()).createAlias("headQuarterMaster", "hq");
			criteria.add(Restrictions.eq("status", "A"));
			allAM=criteria.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return allAM;
	}
	
}
