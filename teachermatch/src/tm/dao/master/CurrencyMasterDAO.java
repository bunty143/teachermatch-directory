package tm.dao.master;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.CurrencyMaster;
import tm.dao.generic.GenericHibernateDAO;
public class CurrencyMasterDAO extends GenericHibernateDAO<CurrencyMaster, Integer> 
{
	public CurrencyMasterDAO() 
	{
		super(CurrencyMaster.class);
	}
	
	@Transactional(readOnly=true)
	public List<CurrencyMaster> findAllCurrencyByOrder()
	{
		List<CurrencyMaster> lstCurrency= null;
		try{
			Criterion criterion = Restrictions.eq("status", "A");
			lstCurrency = findByCriteria(Order.asc("currencyName"),criterion);		
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return lstCurrency;
	}
	
	
	
	
	
}
