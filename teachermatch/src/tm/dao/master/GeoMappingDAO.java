package tm.dao.master;

import tm.bean.master.GeoMapping;
import tm.bean.master.GeographyMaster;
import tm.dao.generic.GenericHibernateDAO;

public class GeoMappingDAO extends GenericHibernateDAO<GeoMapping, Integer> 
{
	public GeoMappingDAO() {
		super(GeoMapping.class);
	}
}
