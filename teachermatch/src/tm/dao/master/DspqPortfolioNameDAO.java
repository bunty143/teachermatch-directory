package tm.dao.master;
 
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DspqPortfolioName;
import tm.bean.master.JobCategoryMaster;
import tm.dao.generic.GenericHibernateDAO;
 
public class DspqPortfolioNameDAO extends GenericHibernateDAO<DspqPortfolioName, Integer> 
{
 
    public DspqPortfolioNameDAO()
    {
        super(DspqPortfolioName.class);
    }
    
    @Transactional(readOnly=true)
    @SuppressWarnings("unchecked")
    public List<DspqPortfolioName> getDspqPortfolioNameByDistrict(DistrictMaster  districtMaster)
    {    
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        List<DspqPortfolioName> dspqPortfolioNames = new ArrayList<DspqPortfolioName>();     
        try
        {
            if(districtMaster!=null){
                criteria.add(Restrictions.eq("districtMaster",districtMaster));
                dspqPortfolioNames=criteria.list();
            }
        } 
        catch (Exception e){
            e.printStackTrace();
        }    
        return dspqPortfolioNames;
    }
    @Transactional(readOnly=true)
    public List<DspqPortfolioName> getDspqPortfolioNameByDistrictMaster(DistrictMaster  districtMaster)
    {    
    	System.out.println(getSession());
        Criteria criteria = getSession().createCriteria(this.getPersistentClass());
        Criterion criterionDistrict=Restrictions.isNull("districtMaster");
        Criterion criterionStatus=Restrictions.eq("status", "A");
        Criterion criterion2=Restrictions.and(criterionDistrict, criterionStatus);
        List<DspqPortfolioName> dspqPortfolioNames = new ArrayList<DspqPortfolioName>();     
        try
        {
            if(districtMaster!=null){
                criteria.add(Restrictions.or(Restrictions.eq("districtMaster",districtMaster), criterion2));
                dspqPortfolioNames=criteria.list();
            }
        } 
        catch (Exception e){
            e.printStackTrace();
        }    
        return dspqPortfolioNames;
    }
    
    @Transactional(readOnly=false)
    public DspqPortfolioName findByDistrictAndJobCategory(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster)
    {
        List<DspqPortfolioName> dspqPortfolioNameList = new ArrayList<DspqPortfolioName>();
        try{
            
            Session session         =     getSession();
            Criteria criteria         =     session.createCriteria(getPersistentClass());
            Criterion criterion1    =    null;
            Criterion criterion2    =    null;
            criterion1 = Restrictions.eq("districtMaster",districtMaster);
            if(jobCategoryMaster!=null)
                criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
            else
                criterion2 = Restrictions.isNull("jobCategoryMaster");
            
            criteria.add(criterion1);
            criteria.add(criterion2);
 
            dspqPortfolioNameList = criteria.list();
            
        } catch(Exception e){
            e.printStackTrace();
        }
        
        if(dspqPortfolioNameList==null || dspqPortfolioNameList.size()==0)
             return null;
         else
             return dspqPortfolioNameList.get(0);
    }
    @Transactional(readOnly=false)
    public DspqPortfolioName findByDistrictAndPortfolioName(DistrictMaster districtMaster,  String portfolioName)
    {
        List<DspqPortfolioName> dspqPortfolioNameList = new ArrayList<DspqPortfolioName>();
        try{
            
            Session session         =     getSession();
            Criteria criteria         =     session.createCriteria(getPersistentClass());
            Criterion criterion1    =    null;
            Criterion criterion2    =    null;
            criterion1 = Restrictions.eq("districtMaster",districtMaster);           
            criterion2 = Restrictions.eq("portfolioName",portfolioName);
            criteria.add(criterion1);
            criteria.add(criterion2);
 
            dspqPortfolioNameList = criteria.list();
            
        } catch(Exception e){
            e.printStackTrace();
        }
        
        if(dspqPortfolioNameList==null || dspqPortfolioNameList.size()==0)
             return null;
         else
             return dspqPortfolioNameList.get(0);
    }
    @Transactional(readOnly=false)
    public DspqPortfolioName findByPortfolioNameIsNULL(String portfolioName)
    {
        List<DspqPortfolioName> dspqPortfolioNameList = new ArrayList<DspqPortfolioName>();
        try{
            
            Session session         =     getSession();
            Criteria criteria         =     session.createCriteria(getPersistentClass());
            Criterion criterion1    =    null;
            Criterion criterion2    =    null;
            criterion1 = Restrictions.isNull("districtMaster");
            criterion2 = Restrictions.eq("portfolioName",portfolioName);
            criteria.add(criterion1);
            criteria.add(criterion2);
 
            dspqPortfolioNameList = criteria.list();
            
        } catch(Exception e){
            e.printStackTrace();
        }
        
        if(dspqPortfolioNameList==null || dspqPortfolioNameList.size()==0)
             return null;
         else
             return dspqPortfolioNameList.get(0);
    }
    
    @Transactional(readOnly=false)
    public List<DspqPortfolioName> getDSPQPortfolioName(JobOrder jobOrder)
    {
        List<DspqPortfolioName> dspqPortfolioNameList = new ArrayList<DspqPortfolioName>();
        try{
        	
        	DistrictMaster districtMaster=null;
        	JobCategoryMaster jobCategoryMaster=null;
        	
        	//System.out.println("jobOrder "+jobOrder.getJobId());
        	
        	if(jobOrder!=null)
        	{
        		if(jobOrder.getDistrictMaster()!=null)
        			districtMaster=jobOrder.getDistrictMaster();
        		if(jobOrder.getJobCategoryMaster()!=null)
        			jobCategoryMaster=jobOrder.getJobCategoryMaster();
        		
        		System.out.println("districtMaster "+districtMaster.getDistrictId());
        		System.out.println("jobCategoryMaster "+jobCategoryMaster.getJobCategoryId());
        		
        	}
        	
            Criterion criterion1=null;
            Criterion criterion2=null;
            Criterion criterion3=null;
            criterion1 = Restrictions.eq("districtMaster",districtMaster);
            if(jobCategoryMaster!=null)
                criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
            
            criterion3 = Restrictions.isNull("jobCategoryMaster");
            
            Criterion criterion4=Restrictions.eq("status","A");
            
            dspqPortfolioNameList=findByCriteria(criterion1,criterion2,criterion4);
            if(dspqPortfolioNameList==null || dspqPortfolioNameList.size()==0)
            	dspqPortfolioNameList=findByCriteria(criterion1,criterion3,criterion4);
            
            
        } catch(Exception e){
            e.printStackTrace();
        }
        
         return dspqPortfolioNameList;
    }
    @Transactional(readOnly=false)
    public List<DspqPortfolioName> findByDistrict(DistrictMaster districtMaster)
    {
        List<DspqPortfolioName> dspqPortfolioNameList = new ArrayList<DspqPortfolioName>();
        try{
            
            Session session         =     getSession();
            Criteria criteria         =     session.createCriteria(getPersistentClass());
            Criterion criterion1    =    null;
            Criterion criterion2    =    null;
            criterion1 = Restrictions.eq("districtMaster",districtMaster); 
            criterion2 = Restrictions.isNotNull("jobCategoryMaster");
            criteria.add(criterion1);
            criteria.add(criterion2);
 
            dspqPortfolioNameList = criteria.list();
            
        } catch(Exception e){
            e.printStackTrace();
        }        
        return dspqPortfolioNameList;
    }
    
    @Transactional(readOnly=false)
	public List<JobCategoryMaster> findJobCategoryByDistrict(DistrictMaster districtMaster)
	{
		List<JobCategoryMaster> jobCategoryMasterList= new ArrayList<JobCategoryMaster>();
		try 
		{
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				criteria.setProjection(Projections.distinct(Projections.property("jobCategoryMaster")));
		        criteria.add(Restrictions.eq("districtMaster",districtMaster));
		        criteria.add(Restrictions.isNotNull("jobCategoryMaster"));
				jobCategoryMasterList = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return jobCategoryMasterList;
	}
    
    @Transactional(readOnly=true)
    @SuppressWarnings("unchecked")
    public List<DspqPortfolioName> getDspqPortfolioForCandidate(DistrictMaster districtMaster)
    {  
    	 List<DspqPortfolioName> dspqPortfolioNames = new ArrayList<DspqPortfolioName>();
    	 Criterion criterion1=Restrictions.isNull("isDeleted");
    	 Criterion criterion2=Restrictions.eq("status","A");
    	 Criterion criterion3=Restrictions.eq("districtMaster",districtMaster);
    	 dspqPortfolioNames=findByCriteria(criterion1,criterion2,criterion3);
    	 return dspqPortfolioNames;
    }
    
    @Transactional(readOnly=false)
	public List<DspqPortfolioName> findPortfolioNameByDistrict(DistrictMaster districtMaster)
	{
    	List<DspqPortfolioName> dspqPortfolioNameList = new ArrayList<DspqPortfolioName>();
		try 
		{
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
		        criteria.add(Restrictions.eq("districtMaster",districtMaster));
		        criteria.add(Restrictions.eq("status","A"));
		        criteria.addOrder(Order.asc("portfolioName"));
		        dspqPortfolioNameList = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dspqPortfolioNameList;
	}
    @Transactional(readOnly=false)
	public  List<DspqPortfolioName> findPortfolioList(Order order,int startPos,int limit,Criterion...criterions)
	{
		List<DspqPortfolioName> dspqPortfolioNameList=null;
		
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			for(Criterion c:criterions){
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			Order order2=Order.asc("districtMaster");
			if(order != null){
				criteria.addOrder(order2);
				criteria.addOrder(order);				
			}
			
			dspqPortfolioNameList=criteria.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return dspqPortfolioNameList;
	}
    @Transactional(readOnly=false)
	public List<DspqPortfolioName> findPortfolioNameByPortfolioIds(List<Integer> dspqPortfolioNameId )
	{
    	List<DspqPortfolioName> dspqPortfolioNameList = null;
		try 
		{
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				criteria.add(Restrictions.in("dspqPortfolioNameId", dspqPortfolioNameId));
		        criteria.add(Restrictions.eq("status","A"));
		        dspqPortfolioNameList = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dspqPortfolioNameList;
	}
}
