package tm.dao.master;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DomainMaster;

import tm.dao.generic.GenericHibernateDAO;
/* @Author: Vishwanath Kumar
 * @Discription:DomainMaster DAO.
 */
public class DomainMasterDAO extends GenericHibernateDAO<DomainMaster, Integer> 
{
	public DomainMasterDAO() 
	{
		super(DomainMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<DomainMaster> getActiveDomains()
	{
		List<DomainMaster> domainMasters =  null;
		try{
		Criterion criterion = Restrictions.eq("status", "a");
		domainMasters = findByCriteria(Order.asc("domainName") ,criterion);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return domainMasters;
	}
	
	@Transactional(readOnly=false)
	public List<DomainMaster> getDomainsListInReport()
	{
		List<DomainMaster> domainMasters =  null;
		try{
		Criterion criterion = Restrictions.eq("status", "a");
		Criterion criterion1 = Restrictions.eq("displayInPDReport", new Short("1"));
		domainMasters = findByCriteria(Order.asc("domainName") ,criterion, criterion1);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return domainMasters;
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<DomainMaster> checkDuplicateDomainName(String domainName,Integer domainId) 
	{
		Session session = getSession();

		if(domainId!=null)
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.eq("domainName", domainName)) 
			.add(Restrictions.not(Restrictions.in("domainId",new Integer[]{domainId})))
			.list();
			return result;

		}
		else
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.eq("domainName", domainName)) 
			.list();
			return result;
		}
	}
	
	@Transactional(readOnly=true)
	public List<DomainMaster> findDomainsByUId(String domainUId){
		List<DomainMaster> domainMasterList = null;
		try{
			Criterion criterion = Restrictions.eq("domainUId", domainUId);
			domainMasterList = findByCriteria(criterion);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return domainMasterList;
	}
}
