package tm.dao.master;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.ReferenceCheckQuestionSet;
import tm.bean.master.ReferenceCheckQuestionSetTemp;
import tm.bean.master.ReferenceQuestionSets;
import tm.dao.generic.GenericHibernateDAO;

public class ReferenceCheckQuestionSetTempDAO extends GenericHibernateDAO<ReferenceCheckQuestionSetTemp, Integer> 
{
	
	public ReferenceCheckQuestionSetTempDAO() 
	{
		super(ReferenceCheckQuestionSetTemp.class);
	}
	
/*	@Transactional(readOnly=false)
	public List<ReferenceCheckQuestionSet> getReferenceByDistrictAndJobCategory(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster)
	{
		List<ReferenceCheckQuestionSet> lstDistSpecQuestions = new ArrayList<ReferenceCheckQuestionSet>();
			try 
			{
				Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
				if(jobCategoryMaster!=null)
				{
				Criterion criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
				lstDistSpecQuestions = findByCriteria(criterion1,criterion2);
				}
				else
				lstDistSpecQuestions = findByCriteria(criterion1);
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		return lstDistSpecQuestions;		
	}
	
	
	@Transactional(readOnly=false)
	public List<ReferenceCheckQuestionSet> getQuestionByDistrictJobCategoryAndJob(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster, JobOrder jobOrder)
	{
		List<ReferenceCheckQuestionSet> lstDistSpecQuestions = new ArrayList<ReferenceCheckQuestionSet>();
			try 
			{
				Session session 		= 	getSession();
				Criteria criteria 		= 	session.createCriteria(getPersistentClass());
				Criterion criterion1	=	null;
				Criterion criterion2	=	null;
				Criterion criterion3	=	null;

				criterion1 = Restrictions.eq("districtMaster",districtMaster);
				criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
				criterion3 = Restrictions.eq("jobOrder",jobOrder);

				criteria.add(criterion1);

				if(jobOrder!=null){
					criteria.add(criterion2);
					criteria.add(criterion3);
				}

				lstDistSpecQuestions = criteria.list();

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		return lstDistSpecQuestions;		
	}*/
	@Transactional(readOnly=false)
	public List<ReferenceCheckQuestionSetTemp> findByQuestionAndJobCategory(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster)
	{
		List<ReferenceCheckQuestionSetTemp> lstDistSpecQuestions = new ArrayList<ReferenceCheckQuestionSetTemp>();
			try 
			{
				Session session 		= 	getSession();
				Criteria criteria 		= 	session.createCriteria(getPersistentClass());
				Criterion criterion1	=	null;
				Criterion criterion2	=	null;
				Criterion criterion3	=	null;

				criterion1 = Restrictions.eq("districtMaster",districtMaster);
				criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
				criteria.add(criterion1);
				criteria.add(criterion2);

				lstDistSpecQuestions = criteria.list();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		return lstDistSpecQuestions;
	}
	
	/*@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int findQuestionSetByDistrictAndJobCategory(Integer headQuarterId , DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster)
	{
		int iReturnValue=0;
		 
			try{
				Session session = getSession();
				String sql ="";
				if(districtMaster!=null)
				  sql ="SELECT COUNT(referenceQuestionId) AS appliedCnt from referencecheckquestionset rfq where rfq.jobCategoryId = "+jobCategoryMaster.getJobCategoryId()+" AND rfq.districtId = "+districtMaster.getDistrictId()+"";
				else if(headQuarterId!=0 &&  headQuarterId!=null)
					sql ="SELECT COUNT(referenceQuestionId) AS appliedCnt from referencecheckquestionset rfq where rfq.jobCategoryId = "+jobCategoryMaster.getJobCategoryId()+" AND rfq.headQuarterId = "+headQuarterId+"";
				Query query = session.createSQLQuery(sql);
				List<BigInteger> rowCount = query.list();
				if(rowCount!=null && rowCount.size()==1)
					iReturnValue=rowCount.get(0).intValue();			
			} catch(Exception exception){
				exception.printStackTrace();
			}
	 
		return iReturnValue;	
	}*/
	
	
	  // @ Anurag
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<ReferenceQuestionSets> getReferenceByHeadQuarter(HeadQuarterMaster headQuarterMaster)
	{
		System.out.println("******** getReferenceByHeadQuarter   from Dao  *********");
		List<ReferenceQuestionSets> result  =   new ArrayList<ReferenceQuestionSets>();
		if(headQuarterMaster!=null){
			try{
				
				Session session = getSession();
				Criteria c1 = session.createCriteria(ReferenceQuestionSets.class)
				.add(Restrictions.eq("headQuarterId", headQuarterMaster.getHeadQuarterId()))
				.add(Restrictions.eq("Status","A"));
				 
			result = c1.list();				 	
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;	
	}
	
	
	
	@Transactional(readOnly=false)
	public List<ReferenceCheckQuestionSet> findByQuestionAndJobCategoryByHQ(HeadQuarterMaster headQuarterMaster, JobCategoryMaster jobCategoryMaster)
	{
		
		List<ReferenceCheckQuestionSet> lstDistSpecQuestions = new ArrayList<ReferenceCheckQuestionSet>();
		{
			try 
			{
				if(headQuarterMaster!=null){
				Session session 		= 	getSession();
				Criteria criteria 		= 	session.createCriteria(getPersistentClass());
				Criterion criterion1	=	null;
				Criterion criterion2	=	null; 

				criterion1 = Restrictions.eq("headQuarterId",headQuarterMaster.getHeadQuarterId());
				criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
				criteria.add(criterion1);
				criteria.add(criterion2);

				lstDistSpecQuestions = criteria.list();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return lstDistSpecQuestions;
	}
	
	@Transactional(readOnly=false)
	public List<ReferenceCheckQuestionSet> findByJobcategoryMaster(JobCategoryMaster jobCategoryMaster)
	{
		
		List<ReferenceCheckQuestionSet> lstDistSpecQuestions = new ArrayList<ReferenceCheckQuestionSet>();
		{
			try 
			{
				if(jobCategoryMaster!=null){
				Session session 		= 	getSession();
				Criteria criteria 		= 	session.createCriteria(getPersistentClass()); 
				Criterion criterion2	=	null; 
 
				criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster); 
				criteria.add(criterion2);

				lstDistSpecQuestions = criteria.list();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return lstDistSpecQuestions;
	
	}
	
	
	
}
