package tm.dao.master;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DemoClassNotesDetails;
import tm.bean.master.DemoClassSchedule;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.generic.GenericHibernateDAO;

public class DemoClassNotesDetailsDAO extends
		GenericHibernateDAO<DemoClassNotesDetails, java.lang.Integer> {
	public DemoClassNotesDetailsDAO() {
		super(DemoClassNotesDetails.class);
	}
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
		this.teacherDetailDAO = teacherDetailDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Transactional(readOnly=true)
	public Boolean getDemoClassNotesByDemoSchedule(DemoClassSchedule demoClassSchedule){
		Boolean result=false;
		try{
			List<DemoClassNotesDetails> demoClassNotesDetails=null;
			Criterion criterion=Restrictions.eq("demoClassSchedule",demoClassSchedule);
			demoClassNotesDetails=findByCriteria(criterion);
			if(demoClassNotesDetails.size()>0)
				result=true;
			else
				result=false;
		}catch (Exception e) {
			e.printStackTrace();
		}	
		return result;
	}
	@Transactional(readOnly=true)
	public List<DemoClassNotesDetails> getDemoClassNotes(DemoClassSchedule demoClassSchedule){
		List<DemoClassNotesDetails> demoClassNotesDetails=null;
		try{
			Criterion criterion=Restrictions.eq("demoClassSchedule",demoClassSchedule);
			demoClassNotesDetails=findByCriteria(criterion);
		}catch (Exception e) {
			e.printStackTrace();
		}	
		return demoClassNotesDetails;
	}
	@Transactional(readOnly=true)
	public List<DemoClassNotesDetails> getDemoClassNotes(Integer teacherId,Integer jobId){
		List<DemoClassNotesDetails> demoClassNotesDetails=null;
		try{
			TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			
			Criterion criterion1=Restrictions.eq("joborder",jobOrder);
			Criterion criterion2=Restrictions.eq("teacherDetail",teacherDetail);
			demoClassNotesDetails=findByCriteria(criterion1,criterion2);
		}catch (Exception e) {
			e.printStackTrace();
		}	
		return demoClassNotesDetails;
	}
}
