package tm.dao.master;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.CountryMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CountryMasterDAO extends GenericHibernateDAO<CountryMaster, Integer> 
{
	public CountryMasterDAO() 
	{
		super(CountryMaster.class);
	}
	
	@Transactional(readOnly=true)
	public List<CountryMaster> findAllActiveCountry()
	{
		List<CountryMaster> lstCountryMaster= new ArrayList<CountryMaster>();
		try 
		{
			Criterion criterion1=Restrictions.eq("status","A"); 
			lstCountryMaster = findByCriteria(Order.asc("name"),criterion1);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstCountryMaster;
	}
	
	@Transactional(readOnly=true)
	public CountryMaster getCountryByShortCode(String sCountryName)
	{
		CountryMaster countryMaster=new CountryMaster();
		Criterion criterion1 = Restrictions.eq("shortCode","US");
		Criterion criterion2 = Restrictions.eq("status","A");
		List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();
		countryMasters=findByCriteria(criterion1,criterion2);
		if(countryMasters!=null && countryMasters.size() >0)
			countryMaster=countryMasters.get(0);
		
		return countryMaster;
	}
	
	@Transactional(readOnly=true)
	public CountryMaster getCountryByName(String sCountryName)
	{
		CountryMaster countryMaster=new CountryMaster();
		Criterion criterion1 = Restrictions.eq("name",sCountryName);
		Criterion criterion2 = Restrictions.eq("status","A");
		List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();
		countryMasters=findByCriteria(criterion1,criterion2);
		if(countryMasters!=null && countryMasters.size() >0)
			countryMaster=countryMasters.get(0);
		
		return countryMaster;
	}
	
	
}
