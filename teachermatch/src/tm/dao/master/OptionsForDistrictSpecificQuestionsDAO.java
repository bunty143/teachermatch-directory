package tm.dao.master;

import tm.bean.master.OptionsForDistrictSpecificQuestions;
import tm.dao.generic.GenericHibernateDAO;

public class OptionsForDistrictSpecificQuestionsDAO extends GenericHibernateDAO<OptionsForDistrictSpecificQuestions, Integer> 
{
	public OptionsForDistrictSpecificQuestionsDAO() 
	{
		super(OptionsForDistrictSpecificQuestions.class);
	}
}