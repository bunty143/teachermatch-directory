package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolKeyContact;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SchoolKeyContactDAO extends GenericHibernateDAO<SchoolKeyContact, Integer>
{
	SchoolKeyContactDAO()
	{
		super(SchoolKeyContact.class);
	}
	
	@Transactional(readOnly=false)
	public List<SchoolKeyContact> findDistrictsWeeklyCgReportRequired(SchoolMaster schoolMaster)
	{
		List<SchoolKeyContact> schoolKeyContactList = null;
		try 
		{		//System.out.println("schoolmaster "+schoolMaster.getSchoolId().getClass());
			Criterion criterion1 = Restrictions.isNotNull("keyContactEmailAddress");
			Criterion criterion2 = Restrictions.eq("schoolId",schoolMaster.getSchoolId());
			
			schoolKeyContactList = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolKeyContactList;
	}
	
	@Transactional(readOnly=false)
	public String findEmail(Integer keyContactId)
	{
		String email="";
		List<SchoolKeyContact> schoolKeyContactList = null;
		try 
		{	
			Criterion criterion1 = Restrictions.eq("keyContactId",keyContactId);
			schoolKeyContactList = findByCriteria(criterion1);
			email=schoolKeyContactList.get(0).getKeyContactEmailAddress();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return email;
	}
	@Transactional(readOnly=false)
	public int checkEmail(int flag,Integer keyContactId,String email,SchoolMaster schoolMaster)
	{
		int size=0;
		List<SchoolKeyContact> schoolKeyContactList = null;
		try 
		{	
			Criterion criterion1 = Restrictions.eq("keyContactEmailAddress",email.trim());
			Criterion criterion2 = Restrictions.eq("keyContactId",keyContactId);//shriram
			Criterion criterion3 = Restrictions.eq("schoolId",schoolMaster.getSchoolId());
			if(flag==1)
				schoolKeyContactList = findByCriteria(criterion1,criterion2,criterion3);
			else
				schoolKeyContactList = findByCriteria(criterion1,criterion3);
			size=schoolKeyContactList.size();			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return size;
	}
	@Transactional(readOnly=false)
	public int checkkeyContactType(ContactTypeMaster keyContactId,SchoolMaster schoolMaster,String keyContactEmailAddress)
	{
		int size=0;
		List<SchoolKeyContact> schoolKeyContactList = null;
		try 
		{	
			Criterion criterion2 = Restrictions.eq("keyContactTypeId",keyContactId);
			Criterion criterion3 = Restrictions.eq("keyContactEmailAddress",keyContactEmailAddress);//shriram
			
			Criterion criterion1 = Restrictions.eq("schoolId",schoolMaster.getSchoolId());			
			schoolKeyContactList = findByCriteria(criterion2,criterion3,criterion1);//shriram	
			size=schoolKeyContactList.size();			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return size;
	}
	@Transactional(readOnly=false)
	public List<SchoolKeyContact> findKeyContacts(String email)
	{
		List<SchoolKeyContact> schoolKeyContactList = null;
		try 
		{	
			Criterion criterion = Restrictions.eq("keyContactEmailAddress",email);
			schoolKeyContactList = findByCriteria(criterion);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolKeyContactList;
	}
	
	@Transactional(readOnly=false)
	public SchoolKeyContact findByUserId(UserMaster master)
	{
		List<SchoolKeyContact> schoolKeyContactList = new ArrayList<SchoolKeyContact>();	
		Criterion criterion1 = Restrictions.eq("userMaster",master);
		schoolKeyContactList=findByCriteria(criterion1);
		SchoolKeyContact schoolKeyContact=null;
		if(schoolKeyContactList.size()>0)
		{
			schoolKeyContact=schoolKeyContactList.get(0);
		}
		   return schoolKeyContact;
		
	}
	@Transactional(readOnly=false)
	public List<SchoolKeyContact> findBySchoolIdeAndEmail(String email,Long schoolId)
	{
		List<SchoolKeyContact> schoolKeyContactList=null;
		try{
			Criterion criterion1 = Restrictions.eq("keyContactEmailAddress",email);
			Criterion criterion3 = Restrictions.eq("schoolId",schoolId);
			schoolKeyContactList=	findByCriteria(criterion1,criterion3);
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return schoolKeyContactList;
	}
	@Transactional(readOnly=false)
	public List<SchoolKeyContact> checkEmailAndContactTypeId(String email,List<ContactTypeMaster> listContactTypeId,Long schoolId)
	{
		List<SchoolKeyContact> schoolKeyContactList = null;
			try{
				Criterion criterion1 = Restrictions.eq("keyContactEmailAddress",email);
				Criterion criterion2 = Restrictions.eq("schoolId",schoolId);
				if(listContactTypeId!=null){
					Criterion criterion3 =  Restrictions.not(Restrictions.in("keyContactTypeId",listContactTypeId));
					schoolKeyContactList=	findByCriteria(criterion1,criterion2,criterion3);
				} else {
					schoolKeyContactList=	findByCriteria(criterion1,criterion2);
				}
			} catch(Exception ex){
				
			}
		
		return schoolKeyContactList;
	}
}
