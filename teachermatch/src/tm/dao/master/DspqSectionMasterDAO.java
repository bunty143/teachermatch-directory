package tm.dao.master;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.DspqGroupMaster;
import tm.bean.master.DspqSectionMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DspqSectionMasterDAO extends GenericHibernateDAO<DspqSectionMaster, Integer> 
{
	public DspqSectionMasterDAO() 
	{
		super(DspqSectionMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<DspqSectionMaster> getDspqSectionListByGroup(DspqGroupMaster dspqGroupMaster)
	{
		List<DspqSectionMaster> dspqSectionList = new ArrayList<DspqSectionMaster>();
		try
		{
			Criterion criterion1 = Restrictions.eq("dspqGroupMaster",dspqGroupMaster);
			Criterion criterion = Restrictions.eq("status", "A");
			dspqSectionList=findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqSectionList;
	}
	@Transactional(readOnly=false)
	public List<DspqSectionMaster> getDspqSectionListByGroupByOrder(DspqGroupMaster dspqGroupMaster)
	{
		List<DspqSectionMaster> dspqSectionList = new ArrayList<DspqSectionMaster>();
		try
		{
			Criterion criterion1 = Restrictions.eq("dspqGroupMaster",dspqGroupMaster);
			Criterion criterion = Restrictions.eq("status", "A");			
			dspqSectionList=findByCriteria(Order.asc("sectionByOrder"),criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqSectionList;
	}
	
	
	@Transactional(readOnly=false)
	public List<DspqSectionMaster> getDspqSectionListByGroupList(List<DspqGroupMaster> dspqGroupMasterlst)
	{
		List<DspqSectionMaster> dspqSectionList = new ArrayList<DspqSectionMaster>();
		try
		{
			if(dspqGroupMasterlst!=null && dspqGroupMasterlst.size()>0)
			{
				Criterion criterion1 = Restrictions.in("dspqGroupMaster",dspqGroupMasterlst);
				Criterion criterion = Restrictions.eq("status", "A");			
				dspqSectionList=findByCriteria(criterion,criterion1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqSectionList;
	}
	
}
