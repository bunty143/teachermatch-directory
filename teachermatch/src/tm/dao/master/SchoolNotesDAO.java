package tm.dao.master;

import tm.bean.master.SchoolNotes;
import tm.dao.generic.GenericHibernateDAO;

public class SchoolNotesDAO extends GenericHibernateDAO<SchoolNotes, Integer> 
{
	SchoolNotesDAO()
	{
		super(SchoolNotes.class);
	}
}
