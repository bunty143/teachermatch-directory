package tm.dao.master;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.ReferenceQuestionSets;
import tm.dao.generic.GenericHibernateDAO;

public class ReferenceQuestionSetsDAO extends GenericHibernateDAO<ReferenceQuestionSets, Integer> 
{
	public ReferenceQuestionSetsDAO() 
	{
		super(ReferenceQuestionSets.class);
	}
	
	@Transactional(readOnly=false)
	public List<ReferenceQuestionSets> findi4QuesSetByStatus(String i4QuesStatus) {
		List<ReferenceQuestionSets> statusList = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("Status",i4QuesStatus);
			statusList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	
	@Transactional(readOnly=false)
	public List<ReferenceQuestionSets> findi4QuesSetByQues(String Questext) {
		List<ReferenceQuestionSets> statusList = null;
		try 
		{
			Criterion criterion1 = Restrictions.like("QuestionSetText",Questext,MatchMode.ANYWHERE);
			statusList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public List<ReferenceQuestionSets> findByDistrict(DistrictMaster districtMaster) {
		List<ReferenceQuestionSets> listBydistrict = null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			//Criterion criterion2 = Restrictions.eq("Status","A");
			listBydistrict = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	@Transactional(readOnly=false)
	public List<ReferenceQuestionSets> findExistQuestionSet(String quesTxt,DistrictMaster districtMaster) {
		List<ReferenceQuestionSets> listBydistrict = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("QuestionSetText",quesTxt).ignoreCase();
			listBydistrict = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	@Transactional(readOnly=false)
	public List<ReferenceQuestionSets> findQuestionSetByIdAndDist(Integer quesSetId,DistrictMaster districtMaster) {
		List<ReferenceQuestionSets> listBydistrict = new ArrayList<ReferenceQuestionSets>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("ID",quesSetId);
			listBydistrict = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	@Transactional(readOnly=false)
	public List<ReferenceQuestionSets> findByDistrictActive(DistrictMaster districtMaster) {
		List<ReferenceQuestionSets> listBydistrict = null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("Status","A");
			listBydistrict = findByCriteria(criterion1,criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}

	// @ Anurag
	@Transactional(readOnly=false)
	public List<ReferenceQuestionSets> findbyHeadQuarter(HeadQuarterMaster headQuarterMaster) {
		List<ReferenceQuestionSets> listByHQ = new ArrayList<ReferenceQuestionSets>();
		try
		{
			if(headQuarterMaster!=null){
			Criterion criterion1 = Restrictions.eq("headQuarterId",headQuarterMaster.getHeadQuarterId());
			//Criterion criterion2 = Restrictions.eq("Status","A");
			listByHQ = findByCriteria(criterion1);
			
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listByHQ;
	}
	
	
	// @ Anurag
	
	@Transactional(readOnly=false)
	public List<ReferenceQuestionSets> findExistQuestionSetByHQ(String quesTxt,HeadQuarterMaster headQuarterMaster) {
		List<ReferenceQuestionSets> listBydistrict = new ArrayList<ReferenceQuestionSets>();
		try 
		{ 
			if(headQuarterMaster!=null){
			Criterion criterion1 = Restrictions.eq("headQuarterId",headQuarterMaster.getHeadQuarterId());
			Criterion criterion2 = Restrictions.eq("QuestionSetText",quesTxt).ignoreCase();
			listBydistrict = findByCriteria(criterion1,criterion2);		
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	//Deepak for new job Category
	@Transactional
	public List<ReferenceQuestionSets> findByQuestionSetIds(Integer[] questionSetIds){
		List<ReferenceQuestionSets> listBydistrict = new ArrayList<ReferenceQuestionSets>();
		try 
		{ 
			Criterion criterion1 = Restrictions.in("ID",questionSetIds);
			Criterion criterion2 = Restrictions.eq("Status","A");
			listBydistrict = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
}
