
package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.SkillAttributesMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusSpecificQuestions;
import tm.dao.generic.GenericHibernateDAO;

public class StatusSpecificQuestionsDAO extends GenericHibernateDAO<StatusSpecificQuestions, Integer> 
{
	public StatusSpecificQuestionsDAO() 
	{
		super(StatusSpecificQuestions.class);
	}
	
	
	@Transactional(readOnly=true)
	public List<StatusSpecificQuestions> findByDistAndJobOrder(Order order,int startPos,int limit,Criterion... criterion)
	{
		List<StatusSpecificQuestions> lststatusSpecificQuestions = new ArrayList<StatusSpecificQuestions>();
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			
			if(order != null)
				criteria.addOrder(order);
				
			lststatusSpecificQuestions = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lststatusSpecificQuestions;
	}
	
	
	@Transactional(readOnly=false)
	public boolean isDuplicateAttributes(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus,SkillAttributesMaster skillAttributesMaster,Integer questionId)
	{
		boolean isDuplicate=false;
		try{
			Criterion criterion_district= Restrictions.eq("districtId",districtMaster.getDistrictId());
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryId",jobCategoryMaster.getJobCategoryId());
			Criterion criterion_secondaryStatus= Restrictions.eq("secondaryStatusId",secondaryStatus.getSecondaryStatusId());
			Criterion criterion_skillAttribute= Restrictions.eq("skillAttributesMaster",skillAttributesMaster);
			
			Criterion criterion_questionId= Restrictions.ne("questionId",questionId);
			List<StatusSpecificQuestions> lstStatusSpecificQuestion=new ArrayList<StatusSpecificQuestions>();
			
			if(questionId==null || questionId==0)
				lstStatusSpecificQuestion=findByCriteria(criterion_district,criterion_jobCategory,criterion_secondaryStatus,criterion_skillAttribute);
			else
				lstStatusSpecificQuestion=findByCriteria(criterion_district,criterion_jobCategory,criterion_secondaryStatus,criterion_skillAttribute,criterion_questionId);
			
			if(lstStatusSpecificQuestion.size()==0)
				return false;
			else
				return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return isDuplicate;
	}
	
	@Transactional(readOnly=false)
	public List<StatusSpecificQuestions> findQuestions(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus,StatusMaster statusMaster)
	{
		List<StatusSpecificQuestions> lstStatusSpecificQuestions = new ArrayList<StatusSpecificQuestions>();
		try{
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
			}else{
				criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
			}
			Criterion criterion_district= Restrictions.eq("districtId",districtMaster.getDistrictId());
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryId",jobCategoryMaster.getJobCategoryId());
				
			lstStatusSpecificQuestions=findByCriteria(criterion_district,criterion_jobCategory,criterionStatus);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatusSpecificQuestions;
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusSpecificQuestions> findQuestions_msu(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,SecondaryStatus secondaryStatus,StatusMaster statusMaster)
	{
		List<StatusSpecificQuestions> lstStatusSpecificQuestions = new ArrayList<StatusSpecificQuestions>();
		try{
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
			}else{
				criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
			}
			Criterion criterion_district= null;
				if(districtMaster!=null)
					criterion_district=Restrictions.eq("districtId",districtMaster.getDistrictId());
			Criterion criterion_jobCategory= Restrictions.eq("jobCategoryId",jobCategoryMaster.getJobCategoryId());
			Criterion criterion_status= Restrictions.eq("status","A");
			if(districtMaster!=null)
				lstStatusSpecificQuestions=findByCriteria(criterion_district,criterion_jobCategory,criterionStatus,criterion_status);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatusSpecificQuestions;
	}
	@Transactional(readOnly=false)
	public List<StatusSpecificQuestions> findQuestions_msuByHBD(JobOrder jobOrder,SecondaryStatus secondaryStatus,StatusMaster statusMaster)
	{
		List<StatusSpecificQuestions> lstStatusSpecificQuestions = new ArrayList<StatusSpecificQuestions>();
		try{
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
			}else{
				criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
			}
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtId",jobOrder.getDistrictMaster().getDistrictId()));
			}else{
				criteria.add(Restrictions.isNull("districtId"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchId",jobOrder.getBranchMaster().getBranchId()));
			}else{
				criteria.add(Restrictions.isNull("branchId"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterId"));
			}
			criteria.add(Restrictions.eq("jobCategoryId",jobOrder.getJobCategoryMaster().getJobCategoryId()));
			criteria.add(Restrictions.eq("status","A"));
			criteria.add(criterionStatus);
			lstStatusSpecificQuestions = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatusSpecificQuestions;
	}
	@Transactional(readOnly=false)
	public List<StatusSpecificQuestions> findQuestionsByHBD(SecondaryStatus secondaryStatus)
	{
		List<StatusSpecificQuestions> lstStatusSpecificQuestions = new ArrayList<StatusSpecificQuestions>();
		try{
			Criterion criterionStatus=null;
			if(secondaryStatus.getStatusMaster()!=null){
				criterionStatus = Restrictions.eq("statusId", secondaryStatus.getStatusMaster().getStatusId());
			}else{
				criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
			}
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(secondaryStatus.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtId",secondaryStatus.getDistrictMaster().getDistrictId()));
			}else{
				criteria.add(Restrictions.isNull("districtId"));
			}
			if(secondaryStatus.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchId",secondaryStatus.getBranchMaster().getBranchId()));
			}else{
				criteria.add(Restrictions.isNull("branchId"));
			}
			if(secondaryStatus.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterId",secondaryStatus.getHeadQuarterMaster().getHeadQuarterId()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterId"));
			}
			criteria.add(Restrictions.eq("jobCategoryId", secondaryStatus.getJobCategoryMaster().getJobCategoryId()));
			criteria.add(criterionStatus);
			lstStatusSpecificQuestions = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatusSpecificQuestions;
	}
}
