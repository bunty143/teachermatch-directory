package tm.dao.master;

import tm.bean.master.SchoolStandardMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SchoolStandardMasterDAO extends GenericHibernateDAO<SchoolStandardMaster, Integer> 
{
	public SchoolStandardMasterDAO() 
	{
		super(SchoolStandardMaster.class);
	}
	
}
