package tm.dao.master;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.QqQuestionSets;
import tm.dao.generic.GenericHibernateDAO;

public class QqQuestionSetsDAO extends GenericHibernateDAO<QqQuestionSets, Integer> 
{
	public QqQuestionSetsDAO() 
	{
		super(QqQuestionSets.class);
	}
	
	@Transactional(readOnly=false)
	public List<QqQuestionSets> findi4QuesSetByStatus(String i4QuesStatus) {
		List<QqQuestionSets> statusList = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("Status",i4QuesStatus);
			statusList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	
	@Transactional(readOnly=false)
	public List<QqQuestionSets> findi4QuesSetByQues(String Questext) {
		List<QqQuestionSets> statusList = null;
		try 
		{
			Criterion criterion1 = Restrictions.like("QuestionSetText",Questext,MatchMode.ANYWHERE);
			statusList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public List<QqQuestionSets> findByDistrict(DistrictMaster districtMaster) {
		List<QqQuestionSets> listBydistrict = null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			listBydistrict = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	@Transactional(readOnly=false)
	public List<QqQuestionSets> findByHeadQuater(HeadQuarterMaster headQuarterMaster) {
		List<QqQuestionSets> listBydistrict = new ArrayList<QqQuestionSets>();
		try
		{
			Criterion criterion1 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			listBydistrict = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<QqQuestionSets> findExistQuestionSet(String quesTxt,DistrictMaster districtMaster) {
		List<QqQuestionSets> listBydistrict = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("QuestionSetText",quesTxt).ignoreCase();
			listBydistrict = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	@Transactional(readOnly=false)
	public List<QqQuestionSets> findQuestionSetByIdAndDist(Integer quesSetId,DistrictMaster districtMaster) {
		List<QqQuestionSets> listBydistrict = new ArrayList<QqQuestionSets>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("ID",quesSetId);
			listBydistrict = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	@Transactional(readOnly=false)
	public List<QqQuestionSets> findByDistrictActive(DistrictMaster districtMaster) {
		List<QqQuestionSets> listBydistrict = null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("Status","A");
			listBydistrict = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	/* 
	 * @Deepak
	 * Method for Qualification Question Set of Job Category
	 * */
	@Transactional(readOnly=false)
	public List<QqQuestionSets> getQQSetForOnboardingByDistrictAndJobCategory(DistrictMaster districtMaster) {
		
		List<QqQuestionSets> districtSpecificQQList = null;
		try{
			
		    if(districtMaster!=null){	
		       Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
			   districtSpecificQQList =findByCriteria(criterion1);
		    }
		}catch(Exception e){
			e.printStackTrace();
		}
		return districtSpecificQQList;
	}
}
