package tm.dao;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.StatusWiseEmailSection;
import tm.bean.StatusWiseEmailSubSection;
import tm.dao.generic.GenericHibernateDAO;

public class StatusWiseEmailSubSectionDAO extends GenericHibernateDAO<StatusWiseEmailSubSection, Integer>{

	public StatusWiseEmailSubSectionDAO() {
		super(StatusWiseEmailSubSection.class);
	}

	@Transactional(readOnly=false)
	public List<StatusWiseEmailSubSection> findByEmailSection(Order sortOrderStrVal,StatusWiseEmailSection statusWiseEmailSection)
	{
		System.out.println(" sortOrderStrVal >>>>>>>> "+sortOrderStrVal);
		
		List<StatusWiseEmailSubSection> emailSections = new ArrayList<StatusWiseEmailSubSection>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion c1 = Restrictions.eq("statusWiseEmailSection", statusWiseEmailSection);
			criteria.addOrder(sortOrderStrVal);
			criteria.add(c1);
			emailSections = criteria.list();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return emailSections;
	}
	
}
