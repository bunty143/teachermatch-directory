package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherAcademics;
import tm.bean.TeacherAdditionalDocuments;
import tm.bean.TeacherDetail;
import tm.bean.TeacherSubjectAreaExam;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherAdditionalDocumentsDAO extends GenericHibernateDAO<TeacherAdditionalDocuments, Integer>{
	public TeacherAdditionalDocumentsDAO() {
		super(TeacherAdditionalDocuments.class);
	}

	
	@Transactional(readOnly=false)
	public TeacherAdditionalDocuments findTeacherAdditionalDocuments(TeacherDetail teacherDetail)
	{
		List<TeacherAdditionalDocuments> lstDocuments= new ArrayList<TeacherAdditionalDocuments>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstDocuments = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstDocuments==null || lstDocuments.size()==0)
			return null;
		else
			return lstDocuments.get(0);
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAdditionalDocuments> findSortedAdditionalDocumentsByTeacher(Order  sortOrderStrVal,TeacherDetail teacherDetail)
	{
		List<TeacherAdditionalDocuments> lstAdditionalDocuments= new ArrayList<TeacherAdditionalDocuments>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstAdditionalDocuments = findByCriteria(sortOrderStrVal,criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstAdditionalDocuments;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAdditionalDocuments> findTeacherAdditionalDoc(TeacherDetail teacherDetail)
	{
		List<TeacherAdditionalDocuments> lstDocuments= new ArrayList<TeacherAdditionalDocuments>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstDocuments = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
			return lstDocuments;
	}
	
	
	@Transactional(readOnly=false)
	public List<Integer> getTeacherAdditionalDocumentsCountByTeacherJAF(TeacherDetail teacherDetail)
	{
		List<Integer> lstTeacherAdditionalDocuments= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			
			criteria.setProjection(Projections.projectionList().add(Projections.count("teacherDetail"))) ;
			lstTeacherAdditionalDocuments =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstTeacherAdditionalDocuments;
		}
		
	}
	
}

