package tm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictApprovalGroups;
import tm.bean.DistrictSpecificApprovalFlow;
import tm.bean.DistrictWiseApprovalGroup;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificApprovalFlowDAO  extends GenericHibernateDAO<DistrictSpecificApprovalFlow, Integer>
{
	@Autowired
	DistrictWiseApprovalGroupDAO districtWiseApprovalGroupDAO;
	
	@Autowired
	DistrictApprovalGroupsDAO districtApprovalGroupsDAO;
	
	public DistrictSpecificApprovalFlowDAO()
	{
		super(DistrictSpecificApprovalFlow.class);
	}
	
	@Transactional(readOnly=true)
	public DistrictSpecificApprovalFlow findApprovalFlowByFlag(String specialEdFlag,DistrictMaster districtMaster)
	{
		DistrictSpecificApprovalFlow jeffcoSpecificApprovalFlow=null;
		try
		{
			Criterion criterion2 = Restrictions.eq("specialEdFlag",specialEdFlag);
			Criterion criterion3 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion4 = Restrictions.eq("status","A");
						
			List<DistrictSpecificApprovalFlow> approvalFlows = findByCriteria(criterion2, criterion3, criterion4);
			
			if(approvalFlows!=null && approvalFlows.size()>0)
				jeffcoSpecificApprovalFlow = approvalFlows.get(0);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return jeffcoSpecificApprovalFlow;
	}
		
	public /*Map<String, DistrictApprovalGroups>*/ List<DistrictApprovalGroups> findDistrictApprovalGroupByApprovalFlow(String approvalFlow, DistrictMaster districtMaster)
	{
		System.out.println("----------------inside findDistrictApprovalGroupByApprovalFlow--------------------------");
		System.out.println("Approval Flow:- "+approvalFlow+", DistrictId:- "+districtMaster.getDistrictId());
		
		//Map<String, DistrictApprovalGroups> map = new LinkedHashMap<String, DistrictApprovalGroups>();
		List<DistrictApprovalGroups> districtApprovalGroups = new ArrayList<DistrictApprovalGroups>();
		
		try
		{
			List<String> list = new ArrayList<String>();
			for(String flow : approvalFlow.split("#"))
				if(!flow.equals(""))
					list.add(flow);
			
			System.out.println("list:- "+list);
			
			Map<String, String> map2 = new HashMap<String, String>();
			List<String> groupName = new ArrayList<String>();
			
			System.out.println("Before districtWiseApprovalGroups");
			List<DistrictWiseApprovalGroup> districtWiseApprovalGroups = districtWiseApprovalGroupDAO.findByGroupShortName(list,districtMaster);
			System.out.println("After districtWiseApprovalGroups:- "+districtWiseApprovalGroups.size());
			
			for(DistrictWiseApprovalGroup districtWiseApprovalGroup : districtWiseApprovalGroups)
			{
				map2.put(districtWiseApprovalGroup.getGroupShortName(), districtWiseApprovalGroup.getGroupName());
				//map2.put(districtWiseApprovalGroup.getGroupName(), districtWiseApprovalGroup.getGroupShortName());
				groupName.add(districtWiseApprovalGroup.getGroupName());
			}
			
			List<DistrictApprovalGroups> approvalGroups = districtApprovalGroupsDAO.findByGroupNameAndDistrictMaster(groupName, districtMaster);
			
			//Sort The Approval Group
			for(String flow : list)
			{
				String groupFullName = map2.get(flow);
				for(DistrictApprovalGroups dag : approvalGroups)
				{
					if(dag.getGroupName().equals(groupFullName))
					{
						districtApprovalGroups.add(dag);
						break;
					}
				}
			}
			
/*			for(DistrictApprovalGroups dag : approvalGroups)
			{
				String groupShortName = map2.get(districtApprovalGroups.getGroupName());
				map.put(groupShortName, districtApprovalGroups);
			}
*/			
		}
		catch(Exception e){e.printStackTrace();}
		
		return districtApprovalGroups;
		//return map;
		
	}
	
	@Transactional(readOnly=true)
	public List<DistrictSpecificApprovalFlow> findAllApprovalFlow(DistrictMaster districtMaster)
	{
		List<DistrictSpecificApprovalFlow> jeffcoSpecificApprovalFlow=null;
		try
		{
			Criterion criterion3 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion4 = Restrictions.eq("status","A");
						
			jeffcoSpecificApprovalFlow = findByCriteria(criterion3, criterion4);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return jeffcoSpecificApprovalFlow;
	}
}