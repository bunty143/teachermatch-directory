package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.NationalBoardCertificationMaster;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class NationalBoardCertificationMasterDAO extends GenericHibernateDAO<NationalBoardCertificationMaster, Long>{
	public NationalBoardCertificationMasterDAO(){
		super(NationalBoardCertificationMaster.class);
	}
	
	@Transactional
	public List<NationalBoardCertificationMaster> getdistrictCustomQuestion(DistrictMaster districtMaster)
	{
		List<NationalBoardCertificationMaster> nationalBoardCertificationMasters = new ArrayList<NationalBoardCertificationMaster>();
		Criterion criterion=Restrictions.eq("districtId", districtMaster);
		try {
			nationalBoardCertificationMasters=findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return nationalBoardCertificationMasters;
	}

}
