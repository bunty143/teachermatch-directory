package tm.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import tm.bean.DistrictCandidates;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class DistrictCandidatesDAO extends GenericHibernateDAO<DistrictCandidates, Long>{
	public DistrictCandidatesDAO() {
		super(DistrictCandidates.class);
	}
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional(readOnly=false)
	public boolean updateDistrictCandidate(TeacherDetail teacherdetail,String status) {
		
		String sql = null;
		Session session = getSession();
		try{
		Date statusInDistrictUpdatedDateTime=Utility.getDateWithTime();
		sql = "UPDATE districtcandidate dt SET "
				+ " dt.statusInDistrict=:DCStatus ,dt.statusInDistrictUpdatedDateTime=:statusInDistrictUpdatedDateTime "
				+ " where dt.teacherId=:teacherId ";

		Query query = session.createSQLQuery(sql);
		query.setParameter("DCStatus", status);
		query.setParameter("statusInDistrictUpdatedDateTime", statusInDistrictUpdatedDateTime);
		query.setParameter("teacherId", teacherdetail.getTeacherId());
		System.out.println("SQL..  "+query.getQueryString());
		query.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

}
