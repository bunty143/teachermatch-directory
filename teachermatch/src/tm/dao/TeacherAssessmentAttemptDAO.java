package tm.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentAttempt;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Vishwanath Kumar
 * @Discription: TeacherAssessmentAttemptDAO.
 */
public class TeacherAssessmentAttemptDAO extends GenericHibernateDAO<TeacherAssessmentAttempt, Integer> 
{
	public TeacherAssessmentAttemptDAO() {
		super(TeacherAssessmentAttempt.class);

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment responses of particular assessment.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<TeacherAssessmentAttempt> findAssessmentResponses(AssessmentDetail assessmentDetail) 
	{
		Session session = getSession();

		List result = session.createCriteria(getPersistentClass())       
		.add(Restrictions.eq("assessmentDetail", assessmentDetail))      
		.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("teacherDetail"))
				.add(Projections.count("assessmentDetail"))           
		).list();

		//System.out.println("Response:::::::::::: "+result);

		return result;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment responses of assessments.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> findAssessmentsResponses() 
	{
		Session session = getSession();

		List result = session.createCriteria(getPersistentClass())       
		.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("assessmentDetail"))
				.add(Projections.groupProperty("teacherDetail"))
				.add(Projections.count("assessmentDetail"))           
		).list();

		Map<Integer,Integer> assessmentAttempsMap = new HashMap<Integer, Integer>();
		AssessmentDetail assessmentDetail = null;
		int i=0;
		int j=0;
		for (Object object : result) {
			Object rows[] = (Object[])object;
			//System.out.println(rows[0]+ " -- " +rows[1]);
			assessmentDetail=((AssessmentDetail)rows[0]);
			//System.out.println(assessmentDetail.getAssessmentId()+" "+assessmentDetail.getAssessmentName()+ " -- " );

			if(i==0 || i==assessmentDetail.getAssessmentId())
				j++;
			else
				j=1;

			assessmentAttempsMap.put(assessmentDetail.getAssessmentId(), j);
			i = assessmentDetail.getAssessmentId();				
		}
		//System.out.println("Response:::::::::::: "+result);
		//System.out.println("vv: "+assessmentAttempsMap.get(75));
		return assessmentAttempsMap;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment responses of particular assessment.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int updateAssessmentAttemptSessionTime(int id,boolean forced) 
	{

		try{
			int seconds = 0; 
			TeacherAssessmentAttempt teacherAssessmentAttempt = findById(id, false, false); 
			if(teacherAssessmentAttempt!=null)
			{
				//Date asssessmentEndTime= teacherAssessmentAttempt.getAssessmentEndTime();
				Date asssessmentStartTime= teacherAssessmentAttempt.getAssessmentStartTime();
				Date d=new Date();
				//System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk "+asssessmentEndTime);

				//long diff = asssessmentEndTime.getTime() - asssessmentStartTime.getTime();
				long diff = d.getTime() - asssessmentStartTime.getTime();

				//System.out.println("diff:::::::::::: "+diff);
				seconds = (int) (diff / 1000)  ;
				//seconds = (int) (diff  % 60 );
				//seconds = (int) (diff / 1000);

				teacherAssessmentAttempt.setAssessmentEndTime(new Date());
				teacherAssessmentAttempt.setAssessmentSessionTime(seconds);
				if(forced)
					teacherAssessmentAttempt.setIsForced(true);

				makePersistent(teacherAssessmentAttempt);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment responses of particular assessment and teacher.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<TeacherAssessmentAttempt> findNoOfAttempts(AssessmentDetail assessmentDetail,TeacherDetail teacherDetail,Integer assessmentTakenCount) 
	{
		List<TeacherAssessmentAttempt> teacherAssessmentAttempts = null;
		try {
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			Criterion criterion = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.between("assessmentStartTime", sDate, eDate);
			Criterion criterion3 = Restrictions.eq("assessmentTakenCount",assessmentTakenCount);
			teacherAssessmentAttempts = findByCriteria(Order.asc("attemptId"),criterion,criterion1,criterion2,criterion3);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentAttempts;

	}

	/* @Author: Sudhansu Sekhar Swain
	 * @Discription: It is used to get Job responses of particular Job.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<TeacherAssessmentAttempt> findJobResponses(JobOrder jobOrder) 
	{
		Session session = getSession();

		List result = session.createCriteria(getPersistentClass())       
		.add(Restrictions.eq("jobOrder", jobOrder))      
		.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("teacherDetail"))
				.add(Projections.count("jobOrder"))           
		).list();

		return result;
	}
	@Transactional(readOnly=false)
	public List<TeacherAssessmentAttempt> findAssessmentAttempts(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());

		
		//criteria.add(Restrictions.between("auditDate", cal.getTime(), eDate));

		List<TeacherAssessmentAttempt> teacherAssessmentAttempts = null;
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = null;

			if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0)
			{
				criterion1 = Restrictions.between("assessmentStartTime", sDate, eDate);
				criteria.add(criterion);
				criteria.add(criterion1);
				criteria.add(Restrictions.isNull("jobOrder"));
				criteria.createCriteria("assessmentDetail").add(Restrictions.eq("assessmentType", 1));
				teacherAssessmentAttempts = criteria.list();
				//teacherAssessmentAttempts = findByCriteria(criterion,criterion1,Restrictions.isNull("jobOrder"));
			}
			else
			{
				criterion1 = Restrictions.eq("jobOrder", jobOrder);
				teacherAssessmentAttempts = findByCriteria(criterion,criterion1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentAttempts;

	}
	
	/**
	 * @author Amit Chaudhary
	 * @param teacherDetail
	 * @param assessmentDetail
	 * @param assessmentTakenCount
	 * @return teacherAssessmentAttemptList
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentAttempt> findAssessmentAttempts(TeacherDetail teacherDetail, AssessmentDetail assessmentDetail, Integer assessmentTakenCount,JobOrder jobOrder)
	{
		List<TeacherAssessmentAttempt> teacherAssessmentAttemptList = null;
		try {
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion3 = Restrictions.eq("assessmentTakenCount", assessmentTakenCount);
			Criterion criterion4 = null;

			if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()<=0){
				teacherAssessmentAttemptList = findByCriteria(criterion1,criterion2,criterion3,Restrictions.isNull("jobOrder"));
			}
			else if(jobOrder!=null && jobOrder.getJobId()!=null){
				criterion4 = Restrictions.eq("jobOrder", jobOrder);
				teacherAssessmentAttemptList = findByCriteria(criterion1,criterion2,criterion3,criterion4);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentAttemptList;
	}


	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment responses of assessments.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> findAssessmentsResponsesForBase(List<TeacherDetail> teacherDetails) 
	{
		Map<Integer,Integer> assessmentAttempsMap = new HashMap<Integer, Integer>();

		if(teacherDetails.size()>0)
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			Session session = getSession();

			Criterion criterion1 = Restrictions.between("assessmentStartTime", sDate, eDate);
			Criterion criterion2 = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion3 = Restrictions.eq("isForced",false);

			List result = session.createCriteria(getPersistentClass()) 
			.add(criterion1)
			.add(criterion2)
			.add(criterion3)
			.add(Restrictions.isNull("jobOrder"))
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherDetail"))
					.add(Projections.count("teacherDetail"))           
			).list();


			TeacherDetail teacherDetail = null;
			for (Object object : result) {

				Object rows[] = (Object[])object;
				//System.out.println(rows[0]+ " -- " +rows[1]);
				teacherDetail=((TeacherDetail)rows[0]);
				assessmentAttempsMap.put(teacherDetail.getTeacherId(), (Integer)rows[1]);
			}
		}
		return assessmentAttempsMap;
	}
	
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentAttempt> findEpiCompletionDate(TeacherDetail teacherDetail)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		//criteria.add(Restrictions.between("auditDate", cal.getTime(), eDate));

		List<TeacherAssessmentAttempt> teacherAssessmentAttempts = null;
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = null;
			criterion1 = Restrictions.between("assessmentStartTime", sDate, eDate);
			teacherAssessmentAttempts = findByCriteria(Order.desc("assessmentEndTime"),criterion,criterion1,Restrictions.isNull("jobOrder"));
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentAttempts;

	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentAttempt> findEpiCompletionDate(TeacherAssessmentdetail teacherAssessmentdetail)
	{
		

		List<TeacherAssessmentAttempt> teacherAssessmentAttempts = null;
		try {
			Criterion criterion = Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail);
			
			teacherAssessmentAttempts = findByCriteria(Order.desc("assessmentEndTime"),criterion);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentAttempts;

	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<TeacherAssessmentAttempt> findNoOfAttempts(AssessmentDetail assessmentDetail,TeacherDetail teacherDetail,TeacherAssessmentdetail teacherAssessmentdetail) 
	{
		List<TeacherAssessmentAttempt> teacherAssessmentAttempts = null;
		try {
			
			Criterion criterion = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("teacherAssessmentdetail",teacherAssessmentdetail);
			teacherAssessmentAttempts = findByCriteria(Order.asc("attemptId"),criterion,criterion1,criterion2);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentAttempts;

	}
}
