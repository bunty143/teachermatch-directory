package tm.dao;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.SchoolSelectedByCandidate;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SchoolSelectedByCandidateDAO extends GenericHibernateDAO<SchoolSelectedByCandidate, Integer> 
{
	public SchoolSelectedByCandidateDAO() 
	{
		super(SchoolSelectedByCandidate.class);
	}
	@Transactional(readOnly=false)
	public List<SchoolSelectedByCandidate> getSchoolSelectedByCandidates(JobOrder jobOrder,TeacherDetail teacherDetail)
	{
		List<SchoolSelectedByCandidate> schoolSelectedByCandidateList= new ArrayList<SchoolSelectedByCandidate>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			criteria.add(criterion2);
			schoolSelectedByCandidateList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolSelectedByCandidateList;
	}
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolMasters(JobOrder jobOrder,TeacherDetail teacherDetail)
	{
		List<SchoolMaster>  lstSchoolId = new ArrayList(); 
		List<SchoolSelectedByCandidate> lstSchoolBYCandidate= new ArrayList<SchoolSelectedByCandidate>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
			lstSchoolBYCandidate = findByCriteria(criterion1,criterion2);
			for(SchoolSelectedByCandidate schoolBYCandidate: lstSchoolBYCandidate){
				lstSchoolId.add(schoolBYCandidate.getSchoolMaster());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolId;
	}
	@Transactional(readOnly=false)
	public List<SchoolSelectedByCandidate> getJobOrderBySelectedSchool(TeacherDetail teacherDetail,JobOrder jobOrder,Order order,int startPos,int limit)
	{
		List<SchoolSelectedByCandidate> schoolSelectedByCandidateList= new ArrayList<SchoolSelectedByCandidate>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion=Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2=Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion2);
			criteria.add(criterion);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			criteria.createCriteria("schoolMaster").addOrder(order);
			schoolSelectedByCandidateList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolSelectedByCandidateList;
	}
	@Transactional(readOnly=false)
	public List<SchoolSelectedByCandidate> getJobOrderBySelectedSchoolForAll(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<SchoolSelectedByCandidate> schoolSelectedByCandidateList= new ArrayList<SchoolSelectedByCandidate>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion=Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2=Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion2);
			criteria.add(criterion);
			schoolSelectedByCandidateList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolSelectedByCandidateList;
	}
	@Transactional(readOnly=false)
	public SchoolSelectedByCandidate getSchoolSelectedByCandidate(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolMaster)
	{
		List<SchoolSelectedByCandidate> schoolSelectedByCandidateList= new ArrayList<SchoolSelectedByCandidate>();
		SchoolSelectedByCandidate selectedByCandidate=null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1=Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2=Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3=Restrictions.eq("schoolMaster",schoolMaster);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			schoolSelectedByCandidateList =  criteria.list();
			if(schoolSelectedByCandidateList.size()>0){
				selectedByCandidate=schoolSelectedByCandidateList.get(0);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return selectedByCandidate;
	}
	@Transactional(readOnly=false)
	public List<SchoolSelectedByCandidate> getJobOrderByTeacherAndJobList(TeacherDetail teacherDetail,List<JobOrder> lstJobOrder)
	{
		List<SchoolSelectedByCandidate> schoolSelectedByCandidateList= new ArrayList<SchoolSelectedByCandidate>();
		try{
			if(lstJobOrder.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion=Restrictions.in("jobOrder",lstJobOrder);
				Criterion criterion2=Restrictions.eq("teacherDetail",teacherDetail);
				criteria.add(criterion2);
				criteria.add(criterion);
				schoolSelectedByCandidateList =  criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolSelectedByCandidateList;
	}
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolMastersByTeacherAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<SchoolMaster>  lstSchoolId = new ArrayList(); 
		List<SchoolSelectedByCandidate> lstSchoolBYCandidate= new ArrayList<SchoolSelectedByCandidate>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2=Restrictions.eq("teacherDetail",teacherDetail);
			lstSchoolBYCandidate = findByCriteria(criterion1,criterion2);
			if(lstSchoolBYCandidate.size()>0){
				for(SchoolSelectedByCandidate schoolBYCandidate: lstSchoolBYCandidate){
					lstSchoolId.add(schoolBYCandidate.getSchoolMaster());
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolId;
	}
	@Transactional(readOnly=false)
	public List<SchoolSelectedByCandidate> getJobOrderBySelectedSchoolForAllByTeacher(List<TeacherDetail> teacherDetails,JobOrder jobOrder)
	{
		List<SchoolSelectedByCandidate> schoolSelectedByCandidateList= new ArrayList<SchoolSelectedByCandidate>();
		try{
			if(teacherDetails.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion=Restrictions.eq("jobOrder",jobOrder);
				Criterion criterion2=Restrictions.in("teacherDetail",teacherDetails);
				criteria.add(criterion2);
				criteria.add(criterion);
				schoolSelectedByCandidateList =  criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolSelectedByCandidateList;
	}
	
	@Transactional(readOnly=false)
	public int getTotalSchoolSelectedCandidates(DistrictMaster districtMaster, int sortingcheck,Order sortOrderStrVal,int start , int limit)
	{
		System.out.println("============getTotalSchoolSelectedCandidates call==========");
		int rowCount = 0;
		List<SchoolSelectedByCandidate> lstSelectedCandidates= new ArrayList<SchoolSelectedByCandidate>();
		try 
		{
			System.out.println(" sortingcheck :: "+sortingcheck+" sortOrderStrVal :: "+sortOrderStrVal+" start :: "+start+" limit :: "+limit);
			Session session = getSession();
			Criteria criteria= session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster); 
			criteria.add(criterion);
			//criteria.addOrder(order);			
			if(sortingcheck==0)
				criteria.addOrder(sortOrderStrVal);
			
			Criteria c1= null;
			Criteria c2= null;
			if(sortingcheck==1){
				c1 = criteria.createCriteria("teacherDetail");
				c1.addOrder(sortOrderStrVal);
			}
			
			if(sortingcheck==2){
				c2 = criteria.createCriteria("schoolMaster");
				c2.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==3)
			{
				criteria.addOrder(sortOrderStrVal);
			}
			
			
			lstSelectedCandidates = criteria.list();
			rowCount = lstSelectedCandidates.size();
			System.out.println("List Size :: "+rowCount);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return rowCount;
	}
	
	@Transactional(readOnly=false)
	public List<SchoolSelectedByCandidate> getSchoolSelectedCandidates(DistrictMaster districtMaster, int sortingcheck,Order sortOrderStrVal,int start , int limit,Boolean flag)
	{
		System.out.println("============getSchoolSelectedCandidates call==========");
		List<SchoolSelectedByCandidate> lstSelectedCandidates= new ArrayList<SchoolSelectedByCandidate>();
		try 
		{
			System.out.println(" sortingcheck :: "+sortingcheck+" sortOrderStrVal :: "+sortOrderStrVal+" start :: "+start+" limit :: "+limit);
			Session session = getSession();
			Criteria criteria= session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster); 
			criteria.add(criterion);
			//criteria.addOrder(order);			
			if(sortingcheck==0)
				criteria.addOrder(sortOrderStrVal);
			
			Criteria c1= null;
			Criteria c2= null;
			if(sortingcheck==1){
				c1 = criteria.createCriteria("teacherDetail");
				c1.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==2){
				c2 = criteria.createCriteria("schoolMaster");
				c2.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==3){
				criteria.addOrder(sortOrderStrVal);
			}
			if(flag)
			{
				criteria.setFirstResult(start);
			criteria.setMaxResults(limit);
			}
			lstSelectedCandidates = criteria.list();
			System.out.println("List Size :: "+lstSelectedCandidates.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstSelectedCandidates;
	}
	
	@Transactional(readOnly=false)
	public List<SchoolSelectedByCandidate> getSchoolSelectedCandidatesSchools(DistrictMaster districtMaster, SchoolMaster schoolMaster, int sortingcheck,Order sortOrderStrVal,int start , int limit,Boolean flag)
	{
		System.out.println("============getSchoolSelectedCandidatesSchools call==========");
		List<SchoolSelectedByCandidate> lstSelectedCandidates= new ArrayList<SchoolSelectedByCandidate>();
		try 
		{
			Session session = getSession();
			Criteria criteria= session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("schoolMaster",schoolMaster);
			criteria.add(criterion1);
			criteria.add(criterion2);
			//criteria.addOrder(order);
			if(sortingcheck==0)
				criteria.addOrder(sortOrderStrVal);
			
			Criteria c1= null;
			Criteria c2= null;
			Criteria c3= null;
			if(sortingcheck==1){
				c1 = criteria.createCriteria("teacherDetail");
				c1.addOrder(sortOrderStrVal);
			}
			
			if(sortingcheck==2){
				c2 = criteria.createCriteria("schoolMaster");
				c2.addOrder(sortOrderStrVal);
			}	
			if(sortingcheck==3)
			{
				//c3 = criteria.createCriteria("schoolSelectionDate");
				criteria.addOrder(sortOrderStrVal);
			}
			if(flag)
			{
			criteria.setFirstResult(start);
			criteria.setMaxResults(limit);
			}
			lstSelectedCandidates = criteria.list();
			System.out.println("List Size :: "+lstSelectedCandidates.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstSelectedCandidates;
	}
}
