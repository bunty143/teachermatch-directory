package tm.dao.hrmspostionstext;

import tm.bean.hrmspostionstext.HrmsPositionsTextDetail;
import tm.dao.generic.GenericHibernateDAO;

public class HrmsPositionsTextDetailDAO extends GenericHibernateDAO<HrmsPositionsTextDetail, Integer>
{

	public HrmsPositionsTextDetailDAO() 
	{
		super(HrmsPositionsTextDetail.class);
		// TODO Auto-generated constructor stub
	}

}
