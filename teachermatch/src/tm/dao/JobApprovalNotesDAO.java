package tm.dao;

import tm.bean.JobApprovalNotes;
import tm.dao.generic.GenericHibernateDAO;

public class JobApprovalNotesDAO extends GenericHibernateDAO<JobApprovalNotes, Integer>{
		public JobApprovalNotesDAO() {
			super(JobApprovalNotes.class);
		}
}
