package tm.dao.hqbranchesmaster;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.dao.generic.GenericHibernateDAO;

public class HeadQuarterMasterDAO extends GenericHibernateDAO<HeadQuarterMaster, Integer> 
{
	public HeadQuarterMasterDAO() 
	{
		super(HeadQuarterMaster.class);
	}
	
	
	
	
	
	@Transactional(readOnly=false)
	public List<HeadQuarterMaster> findHeadquartersReminderRequired(Boolean val)
	{
		List<HeadQuarterMaster> lstHeadQuarterMaster = new ArrayList<HeadQuarterMaster>();
		try 
		{		
			Criterion criterion2 = Restrictions.eq("sendReminderToIcompTalent",(val)?1:0);
			lstHeadQuarterMaster = findByCriteria(criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstHeadQuarterMaster;
	}
	
}
