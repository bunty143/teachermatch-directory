package tm.dao.hqbranchesmaster;

import tm.bean.hqbranchesmaster.SpAssessmentConfig;
import tm.dao.generic.GenericHibernateDAO;

public class SpAssessmentConfigDAO extends GenericHibernateDAO<SpAssessmentConfig, Integer> 
{
	public SpAssessmentConfigDAO() 
	{
		super(SpAssessmentConfig.class);
	}
}
