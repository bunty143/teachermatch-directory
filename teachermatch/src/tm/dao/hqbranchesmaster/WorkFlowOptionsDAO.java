package tm.dao.hqbranchesmaster;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.WorkFlowOptions;
import tm.dao.generic.GenericHibernateDAO;

public class WorkFlowOptionsDAO extends GenericHibernateDAO<WorkFlowOptions, Integer> 
{
	public WorkFlowOptionsDAO() 
	{
		super(WorkFlowOptions.class);
	}
	
	@Transactional(readOnly=false)
	public List<WorkFlowOptions> getFlowOptions()
	{
		List<WorkFlowOptions> flowOptionsList = new ArrayList<WorkFlowOptions>();
		try {

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			flowOptionsList = criteria.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return flowOptionsList;
	}
}
