package tm.dao.hqbranchesmaster;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.StatusNodeColorHistory;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.mq.MQEvent;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class StatusNodeColorHistoryDAO extends GenericHibernateDAO<StatusNodeColorHistory, Integer> {
	
	public StatusNodeColorHistoryDAO() {
		super(StatusNodeColorHistory.class);
	}
	
	@Transactional(readOnly=false)
	public List<StatusNodeColorHistory> getStatusColorHistoryByTeacherandStatus(TeacherDetail teacherDetail,StatusMaster statusId,SecondaryStatus secondaryStatusId)
	{
		List<StatusNodeColorHistory> statusNodeColorHistory = new ArrayList<StatusNodeColorHistory>();
		try{
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail.getTeacherId());
			Criterion criterion2 = null;
			if(statusId!=null)
				criterion2 =  Restrictions.eq("statusId", statusId.getStatusId());
			else if(secondaryStatusId!=null)
				criterion2 =  Restrictions.eq("secondaryStatusId", statusId.getStatusId());
			
			statusNodeColorHistory = findByCriteria(criterion1,criterion2);
		}
		catch(Exception ex){
		}
		return statusNodeColorHistory;
	}
	
	@Transactional(readOnly=false)
	public List<StatusNodeColorHistory> getStatusColorHistoryByTeacher(TeacherDetail teacherDetail)
	{
		List<StatusNodeColorHistory> statusNodeColorHistory = new ArrayList<StatusNodeColorHistory>();
		try{
			Criterion criterion1 = Restrictions.eq("teacherDetail.teacherId", teacherDetail.getTeacherId());
			statusNodeColorHistory = findByCriteria(criterion1);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return statusNodeColorHistory;
	}
	
	@Transactional(readOnly=false)
	public List<StatusNodeColorHistory> getStatusColorHistoryByTeacherAndStatus(TeacherDetail teacherDetail,Integer statusId)
	{
		List<StatusNodeColorHistory> statusNodeColorHistory = new ArrayList<StatusNodeColorHistory>();
		try{
			Criterion criterion1 = Restrictions.eq("teacherDetail.teacherId", teacherDetail.getTeacherId());
			Criterion criterion2 = Restrictions.eq("statusId", statusId);
			statusNodeColorHistory = findByCriteria(criterion1,criterion2);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return statusNodeColorHistory;
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusNodeColorHistory> getStatusColorHistoryByTeacherandUserMaster(TeacherDetail teacherDetail,UserMaster userMaster,List<Integer> secList,List<Integer> statusList)
	{
		List<StatusNodeColorHistory> statusNodeColorHistory = new ArrayList<StatusNodeColorHistory>();
		try{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			criteria.add(Restrictions.eq("teacherDetail",teacherDetail));
			
			if(userMaster.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",userMaster.getHeadQuarterMaster()));
				criteria.add(Restrictions.eq("branchMaster",userMaster.getBranchMaster()));
			}
			else if(userMaster.getHeadQuarterMaster()!=null)
			{
				criteria.add(Restrictions.eq("headQuarterMaster",userMaster.getHeadQuarterMaster()));
			}
			else if(userMaster.getDistrictId()!=null){
				criteria.add(Restrictions.eq("districtMaster",userMaster.getDistrictId()));
			}
			
			if(statusList!=null && statusList.size()>0 && secList!=null && secList.size()>0)			
				criteria.add(Restrictions.or(Restrictions.in("statusId", statusList),Restrictions.in("secondaryStatusId", secList)));
			
			else if(statusNodeColorHistory.size()==0 && secList!=null && secList.size()>0)
				criteria.add(Restrictions.in("secondaryStatusId", secList));
			
			else if(statusNodeColorHistory.size()==0 && statusList!=null && statusList.size()>0)
				criteria.add(Restrictions.in("statusId", statusList));
			
			criteria.addOrder(Order.asc("statusNodeColorHistoryId"));
			
			statusNodeColorHistory = criteria.list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return statusNodeColorHistory;
	}
	@Transactional(readOnly=false)
	public List<StatusNodeColorHistory> getStatusColorHistoryByTeacherList(List<TeacherDetail> teacherDetailList)
	{
		List<StatusNodeColorHistory> statusNodeColorHistory = new ArrayList<StatusNodeColorHistory>();
		try{
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetailList);
			statusNodeColorHistory = findByCriteria(criterion1);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return statusNodeColorHistory;
	}
	
	/**
	 * for Kelly API
	 * @param teacherDetail
	 * @return
	 */
	@Transactional(readOnly=false)
	public List<StatusNodeColorHistory> getStatusColorHistoryByTeacher_OP(TeacherDetail teacherDetail) throws Exception
	{
		List<StatusNodeColorHistory> statusNodeColorHistory = new ArrayList<StatusNodeColorHistory>();
		try{
			Session session = getSession();
 			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail.teacherId", teacherDetail.getTeacherId());
			
			//statusNodeColorHistory = findByCriteria(criterion1);
			
			criteria.add(criterion1);
			criteria.createAlias("mqEvent", "mq")
			.setProjection( Projections.projectionList()
					.add(Projections.property("statusNodeColorHistoryId"))
					.add(Projections.property("mq.eventType"))
					.add(Projections.property("updateDateTime"))
					.add(Projections.property("prevColor"))
					.add(Projections.property("currentColor")));
			
			List results = criteria.list();
			if(results!=null && results.size()>0){
				for(Iterator it = results.iterator();it.hasNext();){
					Object[] row = (Object[]) it.next();
					StatusNodeColorHistory statusNodeColor = new StatusNodeColorHistory();
					if(row[0]!=null){
						statusNodeColor.setStatusNodeColorHistoryId(Integer.parseInt(row[0].toString()));
					}
					if(row[1]!=null){
						MQEvent mqEvent = new MQEvent();
						mqEvent.setEventType(row[1].toString());
						statusNodeColor.setMqEvent(mqEvent);
					}
					if(row[2]!=null){
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Date newDate = df.parse(row[2].toString());
						statusNodeColor.setUpdateDateTime(newDate);
					}
					if(row[3]!=null){
						statusNodeColor.setPrevColor(row[3].toString());
					}
					if(row[4]!=null){
						statusNodeColor.setCurrentColor(row[4].toString());
					}
					statusNodeColor.setTeacherDetail(teacherDetail);
					statusNodeColorHistory.add(statusNodeColor);
				}

			}
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return statusNodeColorHistory;
	}
	
	/**
	 * for Kelly API
	 * @param teacherDetail
	 * @return
	 */
	@Transactional(readOnly=false)
	public List<StatusNodeColorHistory> getStatusColorHistoryByTeacher_OP(List<TeacherDetail> teacherDetail) throws Exception
	{
		List<StatusNodeColorHistory> statusNodeColorHistory = new ArrayList<StatusNodeColorHistory>();
		try{
			Session session = getSession();
 			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetail);
			
			//statusNodeColorHistory = findByCriteria(criterion1);
			
			criteria.add(criterion1);
			criteria.createAlias("mqEvent", "mq")
			.setProjection( Projections.projectionList()
					.add(Projections.property("statusNodeColorHistoryId"))
					.add(Projections.property("mq.eventType"))
					.add(Projections.property("updateDateTime"))
					.add(Projections.property("teacherDetail.teacherId")));
			
			List results = criteria.list();
			if(results!=null && results.size()>0){
				for(Iterator it = results.iterator();it.hasNext();){
					Object[] row = (Object[]) it.next();
					StatusNodeColorHistory statusNodeColor = new StatusNodeColorHistory();
					if(row[0]!=null){
						statusNodeColor.setStatusNodeColorHistoryId(Integer.parseInt(row[0].toString()));
					}
					if(row[1]!=null){
						MQEvent mqEvent = new MQEvent();
						mqEvent.setEventType(row[1].toString());
						statusNodeColor.setMqEvent(mqEvent);
					}
					if(row[2]!=null){
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						Date newDate = df.parse(row[2].toString());
						statusNodeColor.setUpdateDateTime(newDate);
					}
					if(row[3]!=null){
						TeacherDetail teacDetail = new TeacherDetail();
						teacDetail.setTeacherId(Integer.parseInt(row[3].toString()));
						statusNodeColor.setTeacherDetail(teacDetail);	
					}
					statusNodeColorHistory.add(statusNodeColor);
				}
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return statusNodeColorHistory;
	}
	
	@Transactional(readOnly=false)
	public List<StatusNodeColorHistory> getPreviosColorSPS(TeacherDetail teacherDetail,String lblSPStatus)
	{
		List<StatusNodeColorHistory> statusNodeColorHistory = new ArrayList<StatusNodeColorHistory>();
		try{
			
			Criterion criterion1 = Restrictions.eq("teacherDetail.teacherId", teacherDetail.getTeacherId());
			Criterion criterion2 = Restrictions.eq("milestoneName", lblSPStatus);
			Session session = getSession();
 			Criteria criteria = session.createCriteria(getPersistentClass());
 			criteria.add(criterion1);
 			criteria.add(criterion2);
 			criteria.addOrder(Order.desc("statusNodeColorHistoryId"));
 			criteria.setMaxResults(1);
 			statusNodeColorHistory=criteria.list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return statusNodeColorHistory;
	}
	
	@Transactional(readOnly=false)
	public String getPreviosColorSPSByMQEventID(TeacherDetail teacherDetail,MQEvent mQEvent)
	{
		String color="Grey";
		List<StatusNodeColorHistory> statusNodeColorHistory = new ArrayList<StatusNodeColorHistory>();
		try{
			
			Criterion criterion1 = Restrictions.eq("teacherDetail.teacherId", teacherDetail.getTeacherId());
			Criterion criterion2 = Restrictions.eq("mqEvent", mQEvent);
			Session session = getSession();
 			Criteria criteria = session.createCriteria(getPersistentClass());
 			criteria.add(criterion1);
 			criteria.add(criterion2);
 			criteria.addOrder(Order.desc("statusNodeColorHistoryId"));
 			criteria.setMaxResults(1);
 			statusNodeColorHistory=criteria.list();
 			if(statusNodeColorHistory!=null && statusNodeColorHistory.size()>0){
 				color= statusNodeColorHistory.get(0).getCurrentColor();
 			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return color;
	}
	
	
}
