package tm.dao.hqbranchesmaster;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CityMaster;
import tm.bean.master.StateMaster;
import tm.dao.generic.GenericHibernateDAO;

public class BranchMasterDAO extends GenericHibernateDAO<BranchMaster, Integer> 
{
	public BranchMasterDAO() 
	{
		super(BranchMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<BranchMaster> getBranchesByHeadQuarter(HeadQuarterMaster headQuarterMaster,String status_filter)
	{
		List<BranchMaster> branchesList = new ArrayList<BranchMaster>();
		try
		{
			Criterion criterion1=null;				
			if(!status_filter.equals("ST")){
			Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			criterion1 = Restrictions.eq("status",status_filter);
			branchesList=findByCriteria(criterion,criterion1);
			}else{
				Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);
				branchesList=findByCriteria(criterion);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return branchesList;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<BranchMaster> findBranchesWithHeadQuarter(HeadQuarterMaster headQuarterMaster,int branchId, Order sortOrderStrVal,int startPos,int limit,String status_filter)
	{
		List<BranchMaster> branchMasters = new ArrayList<BranchMaster>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());			
			if(headQuarterMaster!=null && headQuarterMaster.getHeadQuarterId()!=null && headQuarterMaster.getHeadQuarterId()!=0)
			    {
				criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
				  if(!status_filter.equals("ST")){
					criteria.add(Restrictions.eq("status", status_filter));	
				  }
				}	  
			   
			if(branchId!=0){	
				criteria.add(Restrictions.eq("branchId",branchId));
				 if(!status_filter.equals("ST")){
			    	criteria.add(Restrictions.eq("status", status_filter));
					 }
		    	}
			    criteria.addOrder(sortOrderStrVal);
				criteria.setFirstResult(startPos);
				criteria.setMaxResults(limit);
				branchMasters = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return branchMasters;
	}
	
	@Transactional(readOnly=false)
	public List<BranchMaster> checkDuplicateBranchByName(String branchName)
	{
		List<BranchMaster> branchesList = new ArrayList<BranchMaster>();
		try
		{
			Criterion criterion = Restrictions.eq("branchName",branchName);			
			branchesList=findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return branchesList;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<BranchMaster> findBrancheMasterByStates(List<Long> stateIds)
	{
		List<BranchMaster> branchMasters = new ArrayList<BranchMaster>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			branchMasters = criteria.createCriteria("stateMaster").add(Restrictions.in("stateId", stateIds)).list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return branchMasters;
	}
	@Transactional(readOnly=false)
	public List<BranchMaster> getBranchByState(StateMaster state)
	{
		List<BranchMaster> branchMasters = new ArrayList<BranchMaster>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			branchMasters = criteria.add(Restrictions.eq("stateMaster", state)).list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return branchMasters;
	}
	@Transactional(readOnly=false)
	public List<BranchMaster> getBranchByCity(String city)
	{
		List<BranchMaster> branchMasters = new ArrayList<BranchMaster>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			branchMasters = criteria.add(Restrictions.eq("cityName", city)).list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return branchMasters;
	}
	@Transactional(readOnly=false)
	public List<BranchMaster> getBranchByZip(String zip)
	{
		List<BranchMaster> branchMasters = new ArrayList<BranchMaster>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			branchMasters = criteria.add(Restrictions.eq("zipCode", zip)).list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return branchMasters;
	}
	@Transactional(readOnly=false)
	public List<BranchMaster> getBranchByKeyWord(String keyWord)
	{
		List<BranchMaster> branchMasters = new ArrayList<BranchMaster>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			branchMasters = criteria.add(Restrictions.like("branchName", keyWord.trim(),MatchMode.ANYWHERE)).list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return branchMasters;
	}
	@Transactional(readOnly=false)
	public List<BranchMaster> getBranchBySorting(List<BranchMaster> branch, Order sortOrderStrVal)
	{
		Set<Integer> set = new HashSet<Integer>();
		for(BranchMaster bm :branch){
			set.add(bm.getBranchId());	
		}
		List<BranchMaster> branchMasters = new ArrayList<BranchMaster>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(set.size()>0)
			criteria.add(Restrictions.in("branchId", set)).list();
			criteria.addOrder(sortOrderStrVal);
			branchMasters = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return branchMasters;
	}
	@Transactional(readOnly=false)
	public List<BranchMaster> getAllBranches(HeadQuarterMaster headQuarterMaster,Order sortOrderStrVal)
	{
		List<BranchMaster> branchesList = new ArrayList<BranchMaster>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster)) ;	
			criteria.addOrder(sortOrderStrVal);
			branchesList=criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return branchesList;
	}
	
	// for excel...
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<BranchMaster> findBranchesWithHeadQuarterForExcel(HeadQuarterMaster headQuarterMaster,int branchId,String status_filter)
	{
		List<BranchMaster> branchMasters = new ArrayList<BranchMaster>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());			
			if(headQuarterMaster!=null && headQuarterMaster.getHeadQuarterId()!=null && headQuarterMaster.getHeadQuarterId()!=0)
			    {
				criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
				 if(!status_filter.equals("ST")){
						criteria.add(Restrictions.eq("status", status_filter));	
					  }					
				}
			if(branchId!=0){	
				criteria.add(Restrictions.eq("branchId",branchId));
				if(!status_filter.equals("ST")){
			    	criteria.add(Restrictions.eq("status", status_filter));
					 }
		    	}
			  //  criteria.addOrder(sortOrderStrVal);
			//	criteria.setFirstResult(startPos);
				//criteria.setMaxResults(limit);
				branchMasters = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return branchMasters;
	}
	
	@Transactional(readOnly=false)
	public List<BranchMaster> getBranchesByHeadQuarterForBA(HeadQuarterMaster headQuarterMaster,int tempBranchId)
	{
		System.out.println("head quater maste in DAO is::::::::::::"+headQuarterMaster);
		List<BranchMaster> branchesList = new ArrayList<BranchMaster>();
		try
		{		
			Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);	
			Criterion criterion1 = Restrictions.eq("branchId",tempBranchId);			
			branchesList=findByCriteria(criterion,criterion1);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return branchesList;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<BranchMaster> findBranchesWithHeadQuarterForBA(HeadQuarterMaster headQuarterMaster,int branchId, Order sortOrderStrVal,int startPos,int limit)
	{
		List<BranchMaster> branchMasters = new ArrayList<BranchMaster>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());			
			if(headQuarterMaster!=null && headQuarterMaster.getHeadQuarterId()!=null && headQuarterMaster.getHeadQuarterId()!=0)
			    {
				System.out.println("head quater id is:::::::::::::::::::"+headQuarterMaster);
				criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
				}	  
			   
			if(branchId!=0){	
				System.out.println("branch ID is::::::::::::::"+branchId);
				criteria.add(Restrictions.eq("branchId",branchId));				
		    	}
			    criteria.addOrder(sortOrderStrVal);
				criteria.setFirstResult(startPos);
				criteria.setMaxResults(limit);
				branchMasters = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return branchMasters;
	}
	
}
