package tm.dao.hqbranchesmaster;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.LinkToKsnTalentsTemp;
import tm.dao.generic.GenericHibernateDAO;

public class LinkToKsnTalentsTempDAO extends GenericHibernateDAO<LinkToKsnTalentsTemp, Integer> 
{
	public LinkToKsnTalentsTempDAO() 
	{
		super(LinkToKsnTalentsTemp.class);
	}
	
	
	@Transactional(readOnly=false)
	public boolean deleteTalentTemp(String sessionId){
		try{
			Criterion criterion = Restrictions.eq("sessionid",sessionId);
        	List<LinkToKsnTalentsTemp> LinkToKsnTalentsTemplst	=findByCriteria(criterion);
        	for(LinkToKsnTalentsTemp LinkToKsnTalentsTemp: LinkToKsnTalentsTemplst){
        		makeTransient(LinkToKsnTalentsTemp);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	@Transactional(readOnly=false)
	public boolean deleteAllTalentTemp(String sessionId)
	{
		try{
			Calendar c = Calendar.getInstance(); 
			c.add(Calendar.DAY_OF_YEAR, -2);  
			Date date = c.getTime();
			Criterion criterion1=Restrictions.lt("date",date);
			Criterion criterion2 = Restrictions.eq("sessionid",sessionId);
			Criterion complete3 = Restrictions.or(criterion1,criterion2);
        	List<LinkToKsnTalentsTemp> LinkToKsnTalentsTemplst	=findByCriteria(complete3);
        	System.out.println("LinkToKsnTalentsTemplst::::"+LinkToKsnTalentsTemplst.size());
			for(LinkToKsnTalentsTemp LinkToKsnTalentsTemp: LinkToKsnTalentsTemplst){
        		makeTransient(LinkToKsnTalentsTemp);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return true;
	}

	@Transactional(readOnly=true)
	public List<LinkToKsnTalentsTemp> findByTempTalent(Order order,int startPos,int limit,Criterion... criterion)
	{
		List<LinkToKsnTalentsTemp> lstLinkToKsnTalentsTemp = new ArrayList<LinkToKsnTalentsTemp>();
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			criteria.addOrder(order);
			lstLinkToKsnTalentsTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstLinkToKsnTalentsTemp;
	}
	@Transactional(readOnly=true)
	public List<LinkToKsnTalentsTemp> findByAllTempTalent()
	{
		List<LinkToKsnTalentsTemp> lstLinkToKsnTalentsTemp = null;
		try{
			Session session = getSession();
			Calendar c = Calendar.getInstance(); // starts with today's date and time
			c.add(Calendar.DAY_OF_YEAR, -2);  // advances day by 2
			Date date = c.getTime();
			Criterion criterion=Restrictions.lt("date",date);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion);
			lstLinkToKsnTalentsTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstLinkToKsnTalentsTemp;
	}
	
	@Transactional(readOnly=true)
	public int getRowCountTempTalent(Criterion... criterion)
	{
		List<LinkToKsnTalentsTemp> lstLinkToKsnTalentsTemp = null;
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			lstLinkToKsnTalentsTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstLinkToKsnTalentsTemp.size();
	}
	@Transactional(readOnly=true)
	public List<LinkToKsnTalentsTemp> findByAllTempTalentCurrentSession(String sessionId)
	{
		List<LinkToKsnTalentsTemp> lstLinkToKsnTalentsTemp = new ArrayList<LinkToKsnTalentsTemp>();
		try{
			Session session = getSession();
			Criterion criterion2 = Restrictions.eq("sessionid",sessionId);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion2);
			lstLinkToKsnTalentsTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstLinkToKsnTalentsTemp;
	}
	
	
	// ************* Adding By Deepak   ****************************
	@Transactional(readOnly=true)
	public List<LinkToKsnTalentsTemp> findByAllTempTalentEmail(List emails)
	{
		List<LinkToKsnTalentsTemp> teacherDetailListTemp= new ArrayList<LinkToKsnTalentsTemp>();
		try 
		{
			Criterion criterion = Restrictions.in("talent_email",emails);	
			teacherDetailListTemp =	findByCriteria(criterion);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return teacherDetailListTemp;
	}
	//******************************************************************
	@Transactional(readOnly=true)
	public List<LinkToKsnTalentsTemp> findByAllTempTalentId(List talentList)
	{
		List<LinkToKsnTalentsTemp> teacherDetailListTemp= new ArrayList<LinkToKsnTalentsTemp>();
		try 
		{
			Criterion criterion = Restrictions.in("talentId",talentList);	
			teacherDetailListTemp =	findByCriteria(criterion);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return teacherDetailListTemp;
	}
	@Transactional(readOnly=true)
	public List<LinkToKsnTalentsTemp> findByAllTempTalentIds(List<String> talentList)
	{
		List<LinkToKsnTalentsTemp> teacherDetailListTemp= new ArrayList<LinkToKsnTalentsTemp>();
		try 
		{
			Criterion criterion = Restrictions.in("talentId",talentList);	
			teacherDetailListTemp =	findByCriteria(criterion);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return teacherDetailListTemp;
	}
	
	// nadeem.......
	@Transactional(readOnly=true)
	public List<LinkToKsnTalentsTemp> findAllTempTalent()
	{
		List<LinkToKsnTalentsTemp> lstLinkToKsnTalentsTemp = null;
		try{
			Session session = getSession();	
			Criteria criteria = session.createCriteria(getPersistentClass());
			lstLinkToKsnTalentsTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstLinkToKsnTalentsTemp;
	}
	
	//nadeem
	@Transactional(readOnly=true)
	public List<LinkToKsnTalentsTemp> findByTalentId(String talentId)
	{
		List<LinkToKsnTalentsTemp> teacherDetailListTemp= new ArrayList<LinkToKsnTalentsTemp>();
		try 
		{
			Criterion criterion = Restrictions.eq("talentId",talentId);	
			teacherDetailListTemp =	findByCriteria(criterion);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return teacherDetailListTemp;
	}
	@Transactional(readOnly=false)
	public boolean deleteAllTalentsTemp(String sessionId)
	{
		try{
			Calendar c = Calendar.getInstance(); 
			c.add(Calendar.DAY_OF_YEAR, -2);  
			Date date = c.getTime();
			Criterion criterion1=Restrictions.lt("date",date);
			Criterion criterion2 = Restrictions.eq("sessionid",sessionId);
			Criterion complete3 = Restrictions.or(criterion1,criterion2);
        	List<LinkToKsnTalentsTemp> linkToKsnTalentsTemplst	=findByCriteria(complete3);
        	System.out.println("linkToKsnTalentsTemplst::::"+linkToKsnTalentsTemplst.size());
			for(LinkToKsnTalentsTemp linkToKsnTalentsTemp: linkToKsnTalentsTemplst){
        		makeTransient(linkToKsnTalentsTemp);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return true;
	}
}
