package tm.dao;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobCertification;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusNodeMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.master.SchoolMasterDAO;



public class SchoolInJobOrderDAO extends GenericHibernateDAO<SchoolInJobOrder, Integer> 
{
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	public SchoolInJobOrderDAO() 
	{
		super(SchoolInJobOrder.class);
	}
	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> findJobBySchool(SchoolMaster schoolId)
	{
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("schoolId",schoolId);
			lstSchoolInJobOrder = findByCriteria(Order.asc("jobId"),criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolInJobOrder;
	}

	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> findJobOrder(JobOrder jobOrder)
	{
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			lstSchoolInJobOrder = findByCriteria(Order.asc("jobId"),criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolInJobOrder;
	}

	/* @Author: Gagan 
	 * @Discription: It is used to totalJobOrderPostedBySchool in dashboard Page.
	 */
	/*===================== For School Dashboard  Total JoB Posted Functionality ===============================*/
	@Transactional(readOnly=false)
	public List<JobOrder> totalJobOrderPostedBySchool(SchoolMaster schoolMaster)
	{
		List<JobOrder> lsttotalJobOrderBySchool= null;
		try 
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			/*======== Fetching data from 2 table using criteria ======== */
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			criteria.add(criterion1);
			/* === Adding crireria from joborder Table ==================*/
			criteria.createCriteria("jobId")
			.add(Restrictions.between("createdDateTime", sDate, eDate));
			//.add(Restrictions.eq("status", "A"));
			lsttotalJobOrderBySchool = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lsttotalJobOrderBySchool;
	}
	/* @Author: Sekhar 
	 * @Discription: It is used to totalJobOrderPostedBySchool in dashboard Page.
	 */
	@Transactional(readOnly=false)
	public List<JobOrder> jobOrderPostedBySchool(SchoolMaster schoolMaster)
	{
		List<JobOrder> jobOrders = new ArrayList<JobOrder>();
		SchoolInJobOrder schoolInJobOrder = null;
		List<SchoolInJobOrder> lstJobOrderBySchool = null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			criteria.add(criterion1);
			/* === Adding crireria from joborder Table ==================*/
			criteria.createCriteria("jobId")
			.add(Restrictions.eq("status", "A"));
			lstJobOrderBySchool = criteria.list();
			Iterator itr = lstJobOrderBySchool.iterator();
			while(itr.hasNext())
			{
				schoolInJobOrder=(SchoolInJobOrder)itr.next();
				jobOrders.add(schoolInJobOrder.getJobId());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}

	@Transactional(readOnly=false)
	public List<JobOrder> jobOrderPostedBySchool(SchoolMaster schoolMaster,int daysAfter)
	{
		//System.out.println("schoolId: "+schoolMaster.getSchoolId());
		List<JobOrder> jobOrders = new ArrayList<JobOrder>();
		SchoolInJobOrder schoolInJobOrder = null;
		List<SchoolInJobOrder> lstJobOrderBySchool = null;
		try 
		{
			Date endDate = new Date();
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			criteria.add(criterion1);
			/* === Adding crireria from joborder Table ==================*/
			criteria.createCriteria("jobId")
			.add(Restrictions.eq("status", "A"));
			lstJobOrderBySchool = criteria.list();
			Iterator itr = lstJobOrderBySchool.iterator();
			while(itr.hasNext())
			{
				schoolInJobOrder=(SchoolInJobOrder)itr.next();
				int difInDays = (int) ((endDate.getTime() - schoolInJobOrder.getJobId().getJobStartDate().getTime())/(1000*60*60*24));
				if(difInDays>daysAfter)
					jobOrders.add(schoolInJobOrder.getJobId());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}

	/* @Author: Vishwanath 
	 * @Discription: It is used to total JobOrders For School in dashboard Page.
	 */
	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> totalJobOrdersForSchool(SchoolMaster schoolMaster)
	{
		List<SchoolInJobOrder> lsttotalJobOrderBySchool= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);

			criteria.add(criterion1);

			lsttotalJobOrderBySchool = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lsttotalJobOrderBySchool;
	}

	@Transactional(readOnly=false)
	public SchoolInJobOrder findSchoolInJobBySchoolAndJob(JobOrder jobOrder, SchoolMaster schoolMaster)
	{
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			Criterion criterion2 = Restrictions.eq("schoolId",schoolMaster);
			lstSchoolInJobOrder = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		

		if(lstSchoolInJobOrder==null || lstSchoolInJobOrder.size()==0)
			return null;
		else
			return lstSchoolInJobOrder.get(0);
	}

	@Transactional(readOnly=false)
	public int findNoOfExpHires(JobOrder jobOrder,SchoolMaster schoolMaster,int allSchool)
	{
		int  noOfExpHires=0;
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		try{
			Criterion criterion1 =  Restrictions.eq("jobId",jobOrder);
			if(allSchool==0){
				Criterion criterion2 = Restrictions.eq("schoolId",schoolMaster);
				lstSchoolInJobOrder = findByCriteria(criterion1,criterion2);
			}else{
				lstSchoolInJobOrder = findByCriteria(criterion1);
			}
			if(lstSchoolInJobOrder.size()>0){
				if(lstSchoolInJobOrder.get(0).getNoOfSchoolExpHires()!=0){
					noOfExpHires=lstSchoolInJobOrder.get(0).getNoOfSchoolExpHires();
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return noOfExpHires;
	}
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolIds(JobOrder jobOrder)
	{
		List<SchoolMaster>  lstSchoolId = new ArrayList(); 
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		try{
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			lstSchoolInJobOrder = findByCriteria(criterion1);
			for(SchoolInJobOrder schoolInJobOrder: lstSchoolInJobOrder){
				lstSchoolId.add(schoolInJobOrder.getSchoolId());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolId;
	}
	@Transactional(readOnly=false)
	public int getNoOfSchoolExpHiresBySchool(JobOrder jobOrder,SchoolMaster schoolMaster)
	{
		List<SchoolInJobOrder> lsttotalJobOrderBySchool= null;
		int noOfSchoolExpHires=0;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			Criterion criterion2 = Restrictions.eq("schoolId", schoolMaster);

			criteria.add(criterion1);
			criteria.add(criterion2);
			lsttotalJobOrderBySchool = criteria.list();
			if(lsttotalJobOrderBySchool.size()>0){
				noOfSchoolExpHires=	lsttotalJobOrderBySchool.get(0).getNoOfSchoolExpHires();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return noOfSchoolExpHires;
	}

	@Transactional(readOnly=false)
	public List<JobOrder> findJobBySchoolAndCategory(SchoolMaster schoolMaster,JobCategoryMaster jobCategoryMaster)
	{

		if(jobCategoryMaster!=null)
			System.out.println(jobCategoryMaster.getJobCategoryId());


		List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		try 
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			criteria.add(criterion1);


			if(jobCategoryMaster!=null)
			{
				criteria.createCriteria("jobId")
				//.add(Restrictions.between("createdDateTime", sDate, eDate))
				.add(Restrictions.eq("status", "A"))
				.add(Restrictions.le("jobStartDate",new Date()))
				.add(Restrictions.ge("jobEndDate",new Date()))
				.add(Restrictions.eq("jobCategoryMaster", jobCategoryMaster));

			}
			else
			{
				criteria.createCriteria("jobId")
				//.add(Restrictions.between("createdDateTime", sDate, eDate))
				.add(Restrictions.eq("status", "A"))
				.add(Restrictions.le("jobStartDate",new Date()))
				.add(Restrictions.ge("jobEndDate",new Date()));
			}

			lstSchoolInJobOrder = criteria.list();

			Iterator itr = lstSchoolInJobOrder.iterator();
			SchoolInJobOrder schoolInJobOrder =null;
			while(itr.hasNext())
			{
				schoolInJobOrder=(SchoolInJobOrder)itr.next();
				lstJobOrder.add(schoolInJobOrder.getJobId());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobOrder;
	}

	@Transactional(readOnly=false)
	public List<JobOrder> findSortedJobBySchoolAndCategory(Order  sortOrderStrVal,SchoolMaster schoolMaster,List<JobCategoryMaster> jobCategoryMasterList,List<SubjectMaster> ObjSubjectList)
	{
		
		//if(jobCategoryMaster!=null)
			//System.out.println(jobCategoryMaster.getJobCategoryId());
		


		List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		try 
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			criteria.add(criterion1);
		
			Date dateWithoutTime = null;
		    
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
			dateWithoutTime = sdf.parse(sdf.format(new Date()));
			
			
			
			if(jobCategoryMasterList!=null && jobCategoryMasterList.size()>0)
			{
				
				/* @Start
				 * @Ashish
				 * @Description :: Check null condition and get List according ObjSubjectList
				 * */
					if(ObjSubjectList!=null && ObjSubjectList.size()>0){
						criteria.createCriteria("jobId")
						//.add(Restrictions.between("createdDateTime", sDate, eDate))
						.add(Restrictions.eq("status", "A"))
						.add(Restrictions.le("jobStartDate",dateWithoutTime))
						.add(Restrictions.ge("jobEndDate",dateWithoutTime))
						.add(Restrictions.in("jobCategoryMaster", jobCategoryMasterList))
						.add(Restrictions.in("subjectMaster", ObjSubjectList))
						.addOrder(sortOrderStrVal);
					
					}else{
						criteria.createCriteria("jobId")
						//.add(Restrictions.between("createdDateTime", sDate, eDate))
						.add(Restrictions.eq("status", "A"))
						.add(Restrictions.le("jobStartDate",dateWithoutTime))
						.add(Restrictions.ge("jobEndDate",dateWithoutTime))
						.add(Restrictions.in("jobCategoryMaster", jobCategoryMasterList))
						.addOrder(sortOrderStrVal);
					}
				/* @End
				 * @Ashish
				 * @Description :: Check null condition and get List according ObjSubjectList
				 * */
			}
			else
			{
				/* @Start
				 * @Ashish
				 * @Description :: Check null condition and get List according ObjSubjectList
				 * */
					if(ObjSubjectList!=null && ObjSubjectList.size()>0){
						criteria.createCriteria("jobId")
						//.add(Restrictions.between("createdDateTime", sDate, eDate))
						.add(Restrictions.eq("status", "A"))
						.add(Restrictions.le("jobStartDate",dateWithoutTime))
						.add(Restrictions.ge("jobEndDate",dateWithoutTime))
						.add(Restrictions.in("subjectMaster", ObjSubjectList))
						.addOrder(sortOrderStrVal);
					
					}else{
						criteria.createCriteria("jobId")
						//.add(Restrictions.between("createdDateTime", sDate, eDate))
						.add(Restrictions.eq("status", "A"))
						.add(Restrictions.le("jobStartDate",dateWithoutTime))
						.add(Restrictions.ge("jobEndDate",dateWithoutTime))
						.addOrder(sortOrderStrVal);
					}
				/* @End
				 * @Ashish
				 * @Description :: Check null condition and get List according ObjSubjectList
				 * */
				
			}
			
			lstSchoolInJobOrder = criteria.list();
			
			Iterator itr = lstSchoolInJobOrder.iterator();
			SchoolInJobOrder schoolInJobOrder =null;
			while(itr.hasNext())
			{
				schoolInJobOrder=(SchoolInJobOrder)itr.next();
				lstJobOrder.add(schoolInJobOrder.getJobId());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobOrder;
	}
	
	/* @Start
	 * @AShish kumar
	 * @Description :: Find data according SchoolList
	 * */
	@Transactional(readOnly=false)
	public List<JobOrder> findSortedJobBySchoolAndCategoryForZip(Order  sortOrderStrVal,List<SchoolMaster> ObjSchoolList,List<JobCategoryMaster> jobCategoryMasterList,List<SubjectMaster> ObjSubjectList)
	{

		//if(jobCategoryMaster!=null)
			//System.out.println(jobCategoryMaster.getJobCategoryId());
		
		List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		try 
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(cal.YEAR), 0,1);
			Date sDate=cal.getTime();
			cal.set(cal.get(cal.YEAR), 11,31);
			Date eDate=cal.getTime();
		
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
		//	System.out.println(" ObjSchoolList :: "+ObjSchoolList+" ObjSchoolList.size() :: "+ObjSchoolList.size());
			
			Date dateWithoutTime = null;
		    
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
			dateWithoutTime = sdf.parse(sdf.format(new Date()));
			
			
			if(ObjSchoolList!=null && ObjSchoolList.size()>0)
			{
				Criterion criterion1 = Restrictions.in("schoolId", ObjSchoolList);
				criteria.add(criterion1);
			
				if(jobCategoryMasterList!=null && jobCategoryMasterList.size()>0)
				{
					/* === Gagan : Fix for display all job in jobsboard, removing current year check ======  */
					
						if(ObjSubjectList!=null && ObjSubjectList.size()>0){
							criteria.createCriteria("jobId")
							//.add(Restrictions.between("createdDateTime", sDate, eDate))
							.add(Restrictions.eq("status", "A"))
							.add(Restrictions.le("jobStartDate",dateWithoutTime))
							.add(Restrictions.ge("jobEndDate",dateWithoutTime))
							.add(Restrictions.in("jobCategoryMaster", jobCategoryMasterList))
							.add(Restrictions.in("subjectMaster", ObjSubjectList))
							.addOrder(sortOrderStrVal);
						
						}else{
							criteria.createCriteria("jobId")
							//.add(Restrictions.between("createdDateTime", sDate, eDate))
							.add(Restrictions.eq("status", "A"))
							.add(Restrictions.le("jobStartDate",dateWithoutTime))
							.add(Restrictions.ge("jobEndDate",dateWithoutTime))
							.add(Restrictions.in("jobCategoryMaster", jobCategoryMasterList))
							.addOrder(sortOrderStrVal);
						}
	
				}
				else
				{
					
						if(ObjSubjectList!=null && ObjSubjectList.size()>0){
							criteria.createCriteria("jobId")
							//.add(Restrictions.between("createdDateTime", sDate, eDate))
							.add(Restrictions.eq("status", "A"))
							.add(Restrictions.le("jobStartDate",dateWithoutTime))
							.add(Restrictions.ge("jobEndDate",dateWithoutTime))
							.add(Restrictions.in("subjectMaster", ObjSubjectList))
							.addOrder(sortOrderStrVal);
						
						}else{
							criteria.createCriteria("jobId")
							//.add(Restrictions.between("createdDateTime", sDate, eDate))
							.add(Restrictions.eq("status", "A"))
							.add(Restrictions.le("jobStartDate",dateWithoutTime))
							.add(Restrictions.ge("jobEndDate",dateWithoutTime))
							.addOrder(sortOrderStrVal);
						}
				}
				
				//System.out.println(" criteria.list()............................."+criteria.list().size());
				
				lstSchoolInJobOrder = criteria.list();
				
				Iterator itr = lstSchoolInJobOrder.iterator();
				SchoolInJobOrder schoolInJobOrder =null;
				while(itr.hasNext())
				{
					schoolInJobOrder=(SchoolInJobOrder)itr.next();
					lstJobOrder.add(schoolInJobOrder.getJobId());
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobOrder;
	}
	/* @End
	 * @AShish kumar
	 * @Description :: Find data according SchoolList
	 * */
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,SchoolInJobOrder> findByAllSchoolInJobOrder() 
	{
		Session session = getSession();
		List result = session.createCriteria(getPersistentClass())
		.list();
		Map<Integer,SchoolInJobOrder> schoolInJobOrderMap = new HashMap<Integer, SchoolInJobOrder>();
		SchoolInJobOrder schoolInJobOrder = null;
		int i=0;
		for (Object object : result) {
			schoolInJobOrder=((SchoolInJobOrder)object);
			schoolInJobOrderMap.put(new Integer(""+i),schoolInJobOrder);
			i++;
		}
		return schoolInJobOrderMap;
	}

	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> findJobBySchoolForCG(SchoolMaster schoolId)
	{
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		try 
		{
			/* Seven Days check
			Calendar cal1 = Calendar.getInstance();
			cal1.add(Calendar.DATE, -7);
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);*/
			//System.out.println(cal1.getTime());

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);

			Criterion criterion1 = Restrictions.eq("schoolId",schoolId);
			Criterion criterion2 = Restrictions.eq("status","A");
			//7 days check: Criterion criterion3 = Restrictions.ge("jobEndDate",cal1.getTime());
			Criterion criterion3 = Restrictions.ge("jobEndDate",cal.getTime());
			// No DJO/SJO check --- SJO check removed
			//Criterion criterion4 = Restrictions.eq("createdForEntity",3);

			session = getSession();
			Criteria crit = session.createCriteria(getPersistentClass());
			Criteria c=crit.createCriteria("jobId");
			c.add(criterion2);
			c.add(criterion3);
			//c.add(criterion4);

			List result = crit.add(criterion1).list();
			lstSchoolInJobOrder = result;

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolInJobOrder;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> totalJobOrderBySchool(Order sortOrderStrVal,int start,int end,SchoolMaster schoolMaster,boolean statusFlag,String status,int JobOrderType,int jobId,Criterion criterionCert,boolean subjectIdFlag,SubjectMaster subjectMaster)
	{
		Criterion criterionSubject=null;
		if(subjectIdFlag)
		{
			criterionSubject=Restrictions.eq("subjectMaster",subjectMaster);
		}

		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		List<SchoolInJobOrder> lstSchoolInJobOrder= new ArrayList<SchoolInJobOrder>();
		List<JobOrder> jobFinalList=new ArrayList<JobOrder>();
		try 
		{
			/*======== Fetching data from 2 table using criteria ======== */
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			if(criterionCert!=null){
				if(statusFlag){
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterionCert)
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add(criterion3);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterionCert)
							.addOrder(sortOrderStrVal)
							.add(criterion3);
						}

					}else{
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.add(criterion2)
							.add(criterion3)
							.addOrder(sortOrderStrVal)
							.add(criterion4);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.add(criterion3)
							.addOrder(sortOrderStrVal)
							.add(criterion4);
						}

					}
				}else{
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add(criterion3);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.addOrder(sortOrderStrVal)
							.add(criterion3);
						}
						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionCert)
						.addOrder(sortOrderStrVal)
						.add(criterion3);*/
					}else{
						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add(criterion3)
							.add(criterion4);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.addOrder(sortOrderStrVal)
							.add(criterion3)
							.add(criterion4);
						}
						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						Criterion criterion4= Restrictions.eq("jobId", jobId);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionCert)
						.addOrder(sortOrderStrVal)
						.add(criterion3)
						.add(criterion4);*/
					}
				}
			}else{
				if(statusFlag){
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add(criterion3);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.addOrder(sortOrderStrVal)
							.add(criterion3);
						}
						/*Criterion criterion2 = Restrictions.eq("status", status);
						Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterion2)
						.addOrder(sortOrderStrVal)
						.add(criterion3);*/
					}else{
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterion3)
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add(criterion4);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterion3)
							.addOrder(sortOrderStrVal)
							.add(criterion4);
						}
						/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterion3)
							.addOrder(sortOrderStrVal)
							.add(criterion4);*/
					}
				}else{
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add(criterion3);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.addOrder(sortOrderStrVal)
							.add(criterion3);
						}
						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.addOrder(sortOrderStrVal)
						.add(criterion3);*/
					}else{
						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add(criterion3)
							.add(criterion4);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.addOrder(sortOrderStrVal)
							.add(criterion3)
							.add(criterion4);
						}

						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						Criterion criterion4= Restrictions.eq("jobId", jobId);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.addOrder(sortOrderStrVal)
						.add(criterion3)
						.add(criterion4);*/
					}
				}
			}

			lstSchoolInJobOrder = criteria.list();

			Iterator itr = lstSchoolInJobOrder.iterator();
			SchoolInJobOrder schoolInJobOrder =null;
			while(itr.hasNext())
			{
				schoolInJobOrder=(SchoolInJobOrder)itr.next();
				jobOrderList.add(schoolInJobOrder.getJobId());
			}

			if(jobOrderList.size()<end)
				end=jobOrderList.size();

			jobFinalList=jobOrderList.subList(start, end);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobFinalList;
	}
	@Transactional(readOnly=false)
	public int totalJobOrderResords(SchoolMaster schoolMaster,boolean statusFlag,String status,int JobOrderType,int jobId,Criterion criterionCert,boolean subjectIdFlag,SubjectMaster subjectMaster)
	{
		Criterion criterionSubject=null;
		if(subjectIdFlag)
		{
			criterionSubject=Restrictions.eq("subjectMaster",subjectMaster);
		}

		List<SchoolInJobOrder> lstSchoolInJobOrder= new ArrayList<SchoolInJobOrder>();
		int totalRecords=0;
		try 
		{
			/*======== Fetching data from 2 table using criteria ======== */
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			if(criterionCert!=null){
				if(statusFlag){
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.add(criterion2)
							.add(criterion3);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.add(criterion3);
						}
						/*Criterion criterion2 = Restrictions.eq("status", status);
						Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionCert)
						.add(criterion2)
						.add(criterion3);*/
					}else{
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.add(criterion2)
							.add(criterion3)
							.add(criterion4);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.add(criterion3)
							.add(criterion4);
						}
						/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.add(criterion3)
							.add(criterion4);*/
					}
				}else{
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.add(criterion3);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion3);
						}
						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionCert)
						.add(criterion3);*/
					}else{
						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.add(criterion3)
							.add(criterion4);

						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion3)
							.add(criterion4);
						}
						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						Criterion criterion4= Restrictions.eq("jobId", jobId);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionCert)
						.add(criterion3)
						.add(criterion4);*/
					}
				}
			}else{ //else
				if(statusFlag){
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionSubject)
							.add(criterion2)
							.add(criterion3);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterion3);
						}

						/*Criterion criterion2 = Restrictions.eq("status", status);
						Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterion2)
						.add(criterion3);*/
					}else{


						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionSubject)
							.add(criterion2)
							.add(criterion3)
							.add(criterion4);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterion3)
							.add(criterion4);
						}

						/*	Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterion3)
							.add(criterion4);*/
					}
				}else{
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionSubject)
							.add(criterion3);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion3);
						}

						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterion3);*/
					}else{

						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionSubject)
							.add(criterion3)
							.add(criterion4);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion3)
							.add(criterion4);
						}

						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						Criterion criterion4= Restrictions.eq("jobId", jobId);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterion3)
						.add(criterion4);*/
					}
				}
			}
			lstSchoolInJobOrder = criteria.list();
			totalRecords=lstSchoolInJobOrder.size();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return totalRecords;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> totalJobOrderByDASchool(Order sortOrderStrVal,int start,int end,SchoolMaster schoolMaster,DistrictMaster districtMaster,boolean statusFlag,String status,int JobOrderType,int jobId,Criterion criterionCert,boolean subjectIdFlag,SubjectMaster subjectMaster)
	{
		Criterion criterionSubject=null;
		if(subjectIdFlag)
		{
			criterionSubject=Restrictions.eq("subjectMaster",subjectMaster);
		}
		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		List<SchoolInJobOrder> lstSchoolInJobOrder= new ArrayList<SchoolInJobOrder>();
		List<JobOrder> jobFinalList=new ArrayList<JobOrder>();
		try 
		{
			/*======== Fetching data from 2 table using criteria ======== */
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			Criterion criterionDA = Restrictions.eq("districtMaster", districtMaster);
			if(criterionCert!=null){
				if(statusFlag){
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.add(criterionDA)
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add(criterion3);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.add(criterionDA)
							.addOrder(sortOrderStrVal)
							.add(criterion3);
						}
						/*Criterion criterion2 = Restrictions.eq("status", status);
						Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionCert)
						.add(criterion2)
						.add(criterionDA)
						.addOrder(sortOrderStrVal)
						.add(criterion3);*/
					}else{

						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.add(criterion2)
							.add(criterion3)
							.add(criterionDA)
							.addOrder(sortOrderStrVal)
							.add(criterion4);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.add(criterion3)
							.add(criterionDA)
							.addOrder(sortOrderStrVal)
							.add(criterion4);
						}

						/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.add(criterion3)
							.add(criterionDA)
							.addOrder(sortOrderStrVal)
							.add(criterion4);*/
					}
				}else{
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add(criterionDA)
							.add(criterion3);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.addOrder(sortOrderStrVal)
							.add(criterionDA)
							.add(criterion3);
						}

						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionCert)
						.addOrder(sortOrderStrVal)
						.add(criterionDA)
						.add(criterion3);*/
					}else{

						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.addOrder(sortOrderStrVal)
							.add(criterionCert)
							.add(criterionSubject)
							.add(criterionDA)
							.add(criterion3)
							.add(criterion4);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.addOrder(sortOrderStrVal)
							.add(criterionCert)
							.add(criterionDA)
							.add(criterion3)
							.add(criterion4);
						}

						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						Criterion criterion4= Restrictions.eq("jobId", jobId);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.addOrder(sortOrderStrVal)
						.add(criterionCert)
						.add(criterionDA)
						.add(criterion3)
						.add(criterion4);*/
					}
				}
			}else{ //else
				if(statusFlag){
					if(jobId==0){

						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterionDA)
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add(criterion3);

						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterionDA)
							.addOrder(sortOrderStrVal)
							.add(criterion3);
						}

						/*Criterion criterion2 = Restrictions.eq("status", status);
						Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterion2)
						.add(criterionDA)
						.addOrder(sortOrderStrVal)
						.add(criterion3);*/
					}else{

						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterion3)
							.add(criterionDA)
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add(criterion4);

						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterion3)
							.add(criterionDA)
							.addOrder(sortOrderStrVal)
							.add(criterion4);
						}


						/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterion3)
							.add(criterionDA)
							.addOrder(sortOrderStrVal)
							.add(criterion4);*/
					}
				}else{
					if(jobId==0){

						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.addOrder(sortOrderStrVal)
							.add(criterionDA)
							.add(criterionSubject)
							.add(criterion3);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.addOrder(sortOrderStrVal)
							.add(criterionDA)
							.add(criterion3);
						}

						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.addOrder(sortOrderStrVal)
						.add(criterionDA)
						.add(criterion3);*/
					}else{

						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.addOrder(sortOrderStrVal)
							.add(criterionDA)
							.add(criterionSubject)
							.add(criterion3)
							.add(criterion4);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.addOrder(sortOrderStrVal)
							.add(criterionDA)
							.add(criterion3)
							.add(criterion4);
						}


						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						Criterion criterion4= Restrictions.eq("jobId", jobId);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.addOrder(sortOrderStrVal)
						.add(criterionDA)
						.add(criterion3)
						.add(criterion4);*/
					}
				}
			}
			lstSchoolInJobOrder = criteria.list();

			Iterator itr = lstSchoolInJobOrder.iterator();
			SchoolInJobOrder schoolInJobOrder =null;
			while(itr.hasNext())
			{
				schoolInJobOrder=(SchoolInJobOrder)itr.next();
				jobOrderList.add(schoolInJobOrder.getJobId());
			}

			if(jobOrderList.size()<end)
				end=jobOrderList.size();

			jobFinalList=jobOrderList.subList(start, end);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobFinalList;
	}
	@Transactional(readOnly=false)
	public int totalJobOrderDAResords(SchoolMaster schoolMaster,DistrictMaster districtMaster,boolean statusFlag,String status,int JobOrderType,int jobId,Criterion criterionCert,boolean subjectIdFlag,SubjectMaster subjectMaster)
	{
		Criterion criterionSubject=null;
		if(subjectIdFlag)
		{
			criterionSubject=Restrictions.eq("subjectMaster",subjectMaster);
		}
		List<SchoolInJobOrder> lstSchoolInJobOrder= new ArrayList<SchoolInJobOrder>();
		int totalRecords=0;
		try 
		{
			/*======== Fetching data from 2 table using criteria ======== */
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			Criterion criterionDA = Restrictions.eq("districtMaster", districtMaster);
			if(criterionCert!=null){
				if(statusFlag){
					if(jobId==0){

						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionDA)
							.add(criterionSubject)
							.add(criterion2)
							.add(criterion3);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionDA)
							.add(criterion2)
							.add(criterion3);
						}


						/*	Criterion criterion2 = Restrictions.eq("status", status);
						Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionCert)
						.add(criterionDA)
						.add(criterion2)
						.add(criterion3);*/
					}else{
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionDA)
							.add(criterionSubject)
							.add(criterion2)
							.add(criterion3)
							.add(criterion4);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionDA)
							.add(criterion2)
							.add(criterion3)
							.add(criterion4);
						}
						/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionDA)
							.add(criterion2)
							.add(criterion3)
							.add(criterion4);*/
					}
				}else{
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.add(criterionDA)
							.add(criterion3);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionDA)
							.add(criterion3);
						}

						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionCert)
						.add(criterionDA)
						.add(criterion3);*/
					}else{
						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.add(criterionDA)
							.add(criterion3)
							.add(criterion4);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionDA)
							.add(criterion3)
							.add(criterion4);
						}

						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						Criterion criterion4= Restrictions.eq("jobId", jobId);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionCert)
						.add(criterionDA)
						.add(criterion3)
						.add(criterion4);*/
					}
				}
			}else{ //else
				if(statusFlag){
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionDA)
							.add(criterionSubject)
							.add(criterion2)
							.add(criterion3);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionDA)
							.add(criterion2)
							.add(criterion3);
						}
						/*Criterion criterion2 = Restrictions.eq("status", status);
						Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionDA)
						.add(criterion2)
						.add(criterion3);*/
					}else{
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionDA)
							.add(criterionSubject)
							.add(criterion2)
							.add(criterion3)
							.add(criterion4);

						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionDA)
							.add(criterion2)
							.add(criterion3)
							.add(criterion4);
						}

						/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionDA)
							.add(criterion2)
							.add(criterion3)
							.add(criterion4);*/
					}
				}else{
					if(jobId==0){
						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionDA)
							.add(criterionSubject)
							.add(criterion3);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionDA)
							.add(criterion3);
						}

						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionDA)
						.add(criterion3);*/
					}else{

						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionDA)
							.add(criterionSubject)
							.add(criterion3)
							.add(criterion4);


						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionDA)
							.add(criterion3)
							.add(criterion4);
						}

						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
						Criterion criterion4= Restrictions.eq("jobId", jobId);
						criteria.add(criterion1);
						criteria.createCriteria("jobId")
						.add(criterionDA)
						.add(criterion3)
						.add(criterion4);*/
					}
				}
			}
			lstSchoolInJobOrder = criteria.list();
			totalRecords=lstSchoolInJobOrder.size();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return totalRecords;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> totalJobOrderBySASchool(Order sortOrderStrVal,int start,int end,DistrictMaster districtMaster,SchoolMaster schoolMaster,boolean certJobId,boolean statusFlag,String status,int JobOrderType,int jobId,Criterion criterionCert,boolean subjectIdFlag,SubjectMaster subjectMaster)
	{
		Criterion criterionSubject=null;
		if(subjectIdFlag)
		{
			criterionSubject=Restrictions.eq("subjectMaster",subjectMaster);
		}
		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		List<SchoolInJobOrder> lstSchoolInJobOrder= new ArrayList<SchoolInJobOrder>();
		List<JobOrder> jobFinalList=new ArrayList<JobOrder>();
		try 
		{
			//System.out.println("::::::::::::totalJobOrderBySASchool:::::::::: certJobId ==="+certJobId+"---statusFlag  "+statusFlag+" ===status==="+status+"===JobOrderType ===="+JobOrderType+"=== jobId === "+jobId+"===criterionCert ==="+criterionCert);
			/*======== Fetching data from 2 table using criteria ======== */
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass(),"sij");
			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			if(criterionCert!=null){
				if(certJobId){
					if(statusFlag){
						if(jobId==0){
							if(subjectIdFlag)
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterionSubject)
								.add(criterion2)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}
							else
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterion2)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}

							/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
						}else{
							if(subjectIdFlag)
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterionSubject)
								.add(criterion2)
								.add(criterion3)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion4);
							}
							else
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterion2)
								.add(criterion3)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion4);
							}

							/*Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterion2)
								.add(criterion3)
								.addOrder(sortOrderStrVal)
								.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
								.add(Restrictions.eq("districtMaster", districtMaster))
								.add(criterion4);*/
						}
					}else{
						if(jobId==0){

							if(subjectIdFlag)
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterionSubject)
								.add(criterion2)
								.add(criterion3)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion4);
							}
							else
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterion2)
								.add(criterion3)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion4);
							}





							/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
						}else{


							if(subjectIdFlag)
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterionSubject)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3)
										.add(criterion4);

							}
							else
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3)
										.add(criterion4);
							}
							/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3)
							.add(criterion4);*/
						}
					}
				}else{
					if(statusFlag){

						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.add(criterion2)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
					}else{

						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
					}
				}
			}else{ //else 
				if(certJobId){
					if(statusFlag){
						if(jobId==0){
							if(subjectIdFlag)
							{

								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterion2)
								.add(criterionSubject)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);

							}
							else
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterion2)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}
							/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
						}else{

							if(subjectIdFlag)
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterion2)
								.add(criterionSubject)
								.add(criterion3)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion4);

							}
							else
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterion2)
								.add(criterion3)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion4);
							}

							/*Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterion2)
								.add(criterion3)
								.addOrder(sortOrderStrVal)
								.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
								.add(Restrictions.eq("districtMaster", districtMaster))
								.add(criterion4);*/
						}
					}else{
						if(jobId==0){

							if(subjectIdFlag)
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionSubject)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}
							else
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}

							/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
						}else{

							if(subjectIdFlag)
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionSubject)
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3)
										.add(criterion4);
							}
							else
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.addOrder(sortOrderStrVal)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3)
										.add(criterion4);
							}

							/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3)
							.add(criterion4);*/
						}
					}
				}else{
					if(statusFlag){
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
					}else{

						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.createCriteria("jobId")
							.add(criterionSubject)
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);

						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.createCriteria("jobId")
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.createCriteria("jobId")
							.addOrder(sortOrderStrVal)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/

					}
				}
			}
			lstSchoolInJobOrder = criteria.list();

			Iterator itr = lstSchoolInJobOrder.iterator();
			SchoolInJobOrder schoolInJobOrder =null;
			while(itr.hasNext())
			{
				schoolInJobOrder=(SchoolInJobOrder)itr.next();
				jobOrderList.add(schoolInJobOrder.getJobId());
			}
			if(jobOrderList.size()<end)
				end=jobOrderList.size();

			jobFinalList=jobOrderList.subList(start, end);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobFinalList;
	}
	@Transactional(readOnly=false)
	public int totalJobOrderSAResords(SchoolMaster schoolMaster,DistrictMaster districtMaster, boolean certJobId,boolean statusFlag,String status,int JobOrderType,int jobId,Criterion criterionCert,boolean subjectIdFlag,SubjectMaster subjectMaster)
	{
		Criterion criterionSubject=null;
		if(subjectIdFlag)
		{
			criterionSubject=Restrictions.eq("subjectMaster",subjectMaster);
		}
		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		List<SchoolInJobOrder> lstSchoolInJobOrder= new ArrayList<SchoolInJobOrder>();
		int totalRecords=0;
		try 
		{
			//System.out.println("::::::::::::totalJobOrderSAResords:::::::::: certJobId ==="+certJobId+"---statusFlag  "+statusFlag+" ===status==="+status+"===JobOrderType ===="+JobOrderType+"=== jobId === "+jobId+"===criterionCert ==="+criterionCert);
			/*======== Fetching data from 2 table using criteria ======== */
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass(),"sij");
			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			if(criterionCert!=null){
				if(certJobId){
					if(statusFlag){
						if(jobId==0){
							if(subjectIdFlag)
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterionSubject)
								.add(criterion2)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}
							else
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterion2)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}

							/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
						}else{
							if(subjectIdFlag)
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterionSubject)
								.add(criterion2)
								.add(criterion3)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion4);
							}
							else
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterion2)
								.add(criterion3)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion4);
							}
							/*Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterion2)
								.add(criterion3)
								.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
								.add(Restrictions.eq("districtMaster", districtMaster))
								.add(criterion4);*/
						}
					}else{
						if(jobId==0){
							if(subjectIdFlag)
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterionSubject)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}
							else
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}

							/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
						}else{
							if(subjectIdFlag)
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add(criterionSubject)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3)
										.add(criterion4);
							}
							else
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionCert)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3)
										.add(criterion4);
							}


							/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3)
							.add(criterion4);*/
						}
					}
				}else{
					if(statusFlag){
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.add(criterion2)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterion2)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
					}else{

						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add(criterionSubject)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterionCert)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
					}
				}
			}else{ //else 
				if(certJobId){
					if(statusFlag){
						if(jobId==0){
							if(subjectIdFlag)
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterion2)
								.add(criterionSubject)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}
							else
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterion2)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}

							/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
						}else{
							if(subjectIdFlag)
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterion2)
								.add(criterion3)
								.add(criterionSubject)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion4);
							}
							else
							{
								Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterion2)
								.add(criterion3)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion4);
							}

							/*Criterion criterion2 = Restrictions.eq("status", status);
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterion2)
								.add(criterion3)
								.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
								.add(Restrictions.eq("districtMaster", districtMaster))
								.add(criterion4);*/
						}
					}else{
						if(jobId==0){
							if(subjectIdFlag)
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionSubject)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}
							else
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3);
							}
							/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
						}else{
							if(subjectIdFlag)
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add(criterionSubject)
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3)
										.add(criterion4);
							}
							else
							{
								Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
								Criterion criterion4= Restrictions.eq("jobId", jobId);
								//criteria.add(criterion1);
								criteria.createCriteria("jobId")
								.add( 
										Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) )
										.add(Restrictions.eq("districtMaster", districtMaster))
										.add(criterion3)
										.add(criterion4);
							}
							/*Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							Criterion criterion4= Restrictions.eq("jobId", jobId);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3)
							.add(criterion4);*/
						}
					}
				}else{
					if(statusFlag){
						if(subjectIdFlag)
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add(criterionSubject)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						else
						{
							Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}

						/*Criterion criterion2 = Restrictions.eq("status", status);
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							//criteria.add(criterion1);
							criteria.createCriteria("jobId")
							.add(criterion2)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
					}else{
						if(subjectIdFlag)
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.createCriteria("jobId")
							.add(criterionSubject)
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						else
						{
							Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.createCriteria("jobId")
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) )
									.add(Restrictions.eq("districtMaster", districtMaster))
									.add(criterion3);
						}
						/*	Criterion criterion3 = Restrictions.eq("createdForEntity", JobOrderType);
							criteria.createCriteria("jobId")
							.add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
									Restrictions.eq("writePrivilegeToSchool",true)
								) )
							.add(Restrictions.eq("districtMaster", districtMaster))
							.add(criterion3);*/
					}
				}
			}
			lstSchoolInJobOrder = criteria.list();

			Iterator itr = lstSchoolInJobOrder.iterator();
			SchoolInJobOrder schoolInJobOrder =null;
			while(itr.hasNext())
			{
				schoolInJobOrder=(SchoolInJobOrder)itr.next();
				jobOrderList.add(schoolInJobOrder.getJobId());
			}
			totalRecords=jobOrderList.size();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return totalRecords;
	}
	@Transactional(readOnly=false)
	public int noOfExpHires(JobOrder jobOrder,SchoolMaster schoolMaster,int allSchool)
	{
		int  noOfExpHires=0;
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		try{
			Criterion criterion1 =  Restrictions.eq("jobId",jobOrder);
			if(allSchool==0){
				Criterion criterion2 = Restrictions.eq("schoolId",schoolMaster);
				lstSchoolInJobOrder = findByCriteria(criterion1,criterion2);
			}else{
				lstSchoolInJobOrder = findByCriteria(criterion1);
			}
			if(lstSchoolInJobOrder.size()>0){
				for(SchoolInJobOrder schoolInJobOrder : lstSchoolInJobOrder)
					if(schoolInJobOrder.getNoOfSchoolExpHires()!=0){
						noOfExpHires+=schoolInJobOrder.getNoOfSchoolExpHires();
					}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return noOfExpHires;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> getJobOrdersInSIJO(List<JobOrder> jobOrders)
	{
		List<JobOrder> lstJobOrders = new ArrayList<JobOrder>();
		try{
			/*Criterion criterion1 =  Restrictions.in("jobId",jobOrders);
			lstSchoolInJobOrder = findByCriteria(criterion1);
			System.out.println("llllllllllllllllllllllllll "+lstSchoolInJobOrder.size());*/
			Session session = getSession();
			lstJobOrders = session.createCriteria(getPersistentClass()) 
			.add(Restrictions.in("jobId",jobOrders)) 
			.setProjection(Projections.projectionList()
					.add(Projections.property("jobId"))  
			).list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobOrders;
	}

	@Transactional(readOnly=false)
	public List<JobOrder> findJobListBySchool(SchoolMaster schoolMaster,DistrictMaster districtMaster,int JobOrderType,int entityID,int searchschoolflag,UserMaster userMaster)
	{
		List<JobOrder> lstJobOrders = new ArrayList<JobOrder>();
		try 
		{
			Criterion criterionJobOrderType = Restrictions.eq("createdForEntity",JobOrderType);
			Criterion criterion=Restrictions.eq("districtMaster",districtMaster);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass(),"sij");
			if(JobOrderType==2 && entityID==3){
				if(districtMaster.getWritePrivilegeToSchool()!=null){
					if(districtMaster.getWritePrivilegeToSchool()){
						if(searchschoolflag==0)//searchschoolflag==0 Means search on default school Id and searchschoolflag==1 when school id is get by school Id filter:
						{
							criteria.createCriteria("jobId").add(criterion).add(criterionJobOrderType).add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) );
						}
						if(searchschoolflag==1 && Long.parseLong(""+schoolMaster.getSchoolId())==Long.parseLong(""+userMaster.getSchoolId().getSchoolId())) // Checking this : when logged in SA will search his DJO in which school is attached. 
						{
							//System.out.println("------------- search if condition ");
							criteria.createCriteria("jobId").add(criterion).add(criterionJobOrderType).add( 
									Restrictions.eq("sij.schoolId", schoolMaster
									) );
						}
						else
						{
							//System.out.println("------------- search Else condition ");
							if(searchschoolflag==1 && schoolMaster.getSchoolId()!=userMaster.getSchoolId().getSchoolId()) // This condition is used for when Loggedin SA will search other School DJOs
							{
								criteria.createCriteria("jobId").add(criterion).add(criterionJobOrderType).add( 
										Restrictions.and(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) );
							}
						}
					}else{
						criteria.createCriteria("jobId").add(criterion).add(criterionJobOrderType).add( 
								Restrictions.eq("sij.schoolId", schoolMaster
								) );
					}
				}
			}else {
				criteria.createCriteria("jobId").add(criterion).add(criterionJobOrderType).add(Restrictions.eq("sij.schoolId", schoolMaster));
			}
			criteria.setProjection(Projections.groupProperty("jobId"));
			/*lstJobOrders = criteria.list();*/
			long a = System.currentTimeMillis();
			lstJobOrders = criteria.list();
			long b = System.currentTimeMillis();
			System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + lstJobOrders.size() );
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobOrders;
	}

	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> findInternalCandidate(List<JobOrder> lstjobOrder,SchoolMaster schoolMaster)
	{
		List<SchoolInJobOrder> lstSchoolInJobOrder=new ArrayList<SchoolInJobOrder>();
		try
		{
			if(lstjobOrder.size()>0)
			{
				Criterion criterion_job =  Restrictions.in("jobId",lstjobOrder);
				Criterion criterion_school = Restrictions.eq("schoolId",schoolMaster);
				lstSchoolInJobOrder=findByCriteria(criterion_job,criterion_school);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstSchoolInJobOrder;
		}
	}
	
	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> getSchoolInJobOrder(Integer jobOrderId,String strSchoolName,Integer iStartFrom,Integer iMaxRecords)
	{
		List<SchoolInJobOrder> schoolInJobOrders=new ArrayList<SchoolInJobOrder>();
		Session session = getSession();
		
		Criteria cr = session.createCriteria(getPersistentClass());
		cr.setFirstResult(iStartFrom);
		cr.setMaxResults(iMaxRecords);
		Criterion criterionSN;
		
		Criteria cr_schoolId = cr.createCriteria("schoolId");
		cr_schoolId.addOrder(Order.asc("schoolName"));
		
		boolean chkNum = true;
		try
		{
			 double d = Double.parseDouble(strSchoolName);  
		}catch(Exception e)
		{
			chkNum = false;
		}
		
		if(jobOrderId > 0)
		{
			JobOrder jobOrder=jobOrderDAO.findById(jobOrderId, false, false);
			if(jobOrder!=null)
			{
				DistrictMaster districtMaster=new DistrictMaster();
				districtMaster=jobOrder.getDistrictMaster();
				
				if(chkNum){
					criterionSN = Restrictions.like("locationCode", strSchoolName,MatchMode.START);
				}else{
					criterionSN = Restrictions.like("schoolName", strSchoolName,MatchMode.START);
				}
				
				Criterion criterionDM = Restrictions.eq("districtId",districtMaster);
				List<SchoolMaster> schoolMasterList=new ArrayList<SchoolMaster>();
				if(strSchoolName!=null && strSchoolName!="")
					schoolMasterList=schoolMasterDAO.findByCriteria(criterionSN,criterionDM);
				if(schoolMasterList.size()==0){
					schoolInJobOrders = cr
					.add(Restrictions.eq("jobId",jobOrder)) 
					.list();
				}else{
					schoolInJobOrders = cr
					.add(Restrictions.in("schoolId",schoolMasterList)) 
					.add(Restrictions.eq("jobId",jobOrder)) 
					.list();
				}
			}
		}
		return schoolInJobOrders;
	}
	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> getSIJO(List<JobOrder> jobOrders)
	{
		List<SchoolInJobOrder> schoolInJobOrders = new ArrayList<SchoolInJobOrder>();
		try{
			Criterion criterion1 =  Restrictions.in("jobId",jobOrders);
			schoolInJobOrders = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolInJobOrders;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> findJobBySchoolAndForZip(List<SchoolMaster> ObjSchoolList)
	{
		
		List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		
		try{
			Date dateWithoutTime = null;
		    
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
			dateWithoutTime = sdf.parse(sdf.format(new Date()));
			
			
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			criteria.createCriteria("jobId")
			//.add(Restrictions.between("createdDateTime", sDate, eDate))
			.add(Restrictions.eq("status", "A"))
			.add(Restrictions.le("jobStartDate",dateWithoutTime))
			.add(Restrictions.ge("jobEndDate",dateWithoutTime));
			
			lstSchoolInJobOrder = criteria.list();
			
			Iterator itr = lstSchoolInJobOrder.iterator();
			SchoolInJobOrder schoolInJobOrder =null;
			
			while(itr.hasNext())
			{
				schoolInJobOrder=(SchoolInJobOrder)itr.next();
				lstJobOrder.add(schoolInJobOrder.getJobId());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return lstJobOrder;
	}
	
	/*@Transactional(readOnly=false)
	public List<SchoolInJobOrder> getSchoolsByJobList(List<JobOrder> jobList){
		List<SchoolInJobOrder> noHiresList =  new ArrayList<SchoolInJobOrder>();
		
		try{
			
		}catch(Exception e){
			e.printStackTrace();
		}			
			return noHiresList;	
	}*/
	@Transactional(readOnly=false)
	public List<JobOrder> findAllJobBySchool(SchoolMaster schoolMaster)
	{
		
		List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
		List<SchoolInJobOrder> lsttotalJobOrderBySchool= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);

			criteria.add(criterion1);

			lsttotalJobOrderBySchool = criteria.list();
			Iterator itr = lsttotalJobOrderBySchool.iterator();
			SchoolInJobOrder schoolInJobOrder =null;
			while(itr.hasNext())
			{
				schoolInJobOrder=(SchoolInJobOrder)itr.next();
				lstJobOrder.add(schoolInJobOrder.getJobId());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobOrder;
	}
	@Transactional(readOnly=false)
	public void deleteFromSchoolInJobOrder(List<SchoolMaster> schoolMasters,JobOrder jobOrder){
		
			try {
				String hql = "delete from SchoolInJobOrder WHERE  "; 
				
				/*if(schoolMasters.size()>0)
					hql+=" schoolId not IN (:schoolMasters) and " ;*/
				
				hql+=" jobId =:jobOrder ";
				
				System.out.println("hql:: "+hql);
				Query query = null;
				Session session = getSession();
	
				query = session.createQuery(hql);
				if(schoolMasters.size()>0)
				query.setParameterList("schoolMasters", schoolMasters);
				
				query.setParameter("jobOrder", jobOrder);
				System.out.println("school  OOO  "+query.executeUpdate());
			} catch (HibernateException e) {
				e.printStackTrace();
			}
	}
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
	public List<Integer> findAllJobOrderBySchool(SchoolMaster schoolMaster)
	{
		
		List<Integer> lstJobOrders = new ArrayList<Integer>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("schoolId", schoolMaster);

			criteria.add(criterion);
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("jobId.jobId")));
			lstJobOrders = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobOrders;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> allJobOrderPostedBySchool(SchoolMaster schoolMaster)
	{
		List<JobOrder> jobOrders = new ArrayList<JobOrder>();
		SchoolInJobOrder schoolInJobOrder = null;
		List<SchoolInJobOrder> lstJobOrderBySchool = null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			criteria.add(criterion1);
			lstJobOrderBySchool = criteria.list();
			Iterator itr = lstJobOrderBySchool.iterator();
			while(itr.hasNext())
			{
				schoolInJobOrder=(SchoolInJobOrder)itr.next();
				jobOrders.add(schoolInJobOrder.getJobId());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	
	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> getSIJO(JobOrder jobOrder, SchoolMaster schoolMaster)
	{
		List<SchoolInJobOrder> lstSchoolInJobOrder= new ArrayList<SchoolInJobOrder>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			Criterion criterion2 = Restrictions.eq("schoolId",schoolMaster);
			if(schoolMaster==null)
				lstSchoolInJobOrder = findByCriteria(criterion1);
			else
				lstSchoolInJobOrder = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolInJobOrder;
	}
	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> getSchoolInJobOrderForHired(List<SchoolMaster> schoolMasters,JobOrder jobOrder,String strSchoolName,Integer iStartFrom,Integer iMaxRecords)
	{
		List<SchoolInJobOrder> schoolInJobOrders=new ArrayList<SchoolInJobOrder>();
		Session session = getSession();
		
		Criteria cr = session.createCriteria(getPersistentClass());
		cr.setFirstResult(iStartFrom);
		cr.setMaxResults(iMaxRecords);
		Criterion criterionSN;
		
		boolean chkNum = true;
		try
		{
			 double d = Double.parseDouble(strSchoolName);  
		}catch(Exception e)
		{
			chkNum = false;
		}
		if(jobOrder!=null)
		{
			if(chkNum){
				criterionSN = Restrictions.like("locationCode", strSchoolName,MatchMode.START);
			}else{
				criterionSN = Restrictions.like("schoolName", strSchoolName,MatchMode.START);
			}
			
			Criterion criterionDM = Restrictions.eq("districtId",jobOrder.getDistrictMaster());
			List schoolIds=new ArrayList();
			for (SchoolMaster schoolMaster : schoolMasters) {
				schoolIds.add(schoolMaster.getSchoolId());
			}
			
			Criterion criterionSchoolIds = Restrictions.in("schoolId",schoolIds);
			List<SchoolMaster> schoolMasterList=new ArrayList<SchoolMaster>();
			if(strSchoolName!=null && strSchoolName!=""){
				schoolMasterList=schoolMasterDAO.findByCriteria(criterionSN,criterionDM,criterionSchoolIds);
			}
			if(schoolMasterList.size()==0){
				schoolInJobOrders = cr.add(Restrictions.eq("jobId",jobOrder)).add(Restrictions.in("schoolId",schoolMasters)) 
				.list();
			}else{
				schoolInJobOrders = cr.add(Restrictions.in("schoolId",schoolMasterList)) .add(Restrictions.eq("jobId",jobOrder)) 
				.list();
			}
		}
		return schoolInJobOrders;
	}
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolMastersByFilter(JobOrder jobOrder,List<SchoolMaster> schoolMasters)
	{
		List<SchoolMaster>  lstSchoolId = new ArrayList(); 
		List<SchoolInJobOrder> lstSchoolInJobOrder= new ArrayList<SchoolInJobOrder>();
		try{
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			if(schoolMasters.size()>0){
				Criterion criterion2 = Restrictions.not(Restrictions.in("schoolId",schoolMasters));
				lstSchoolInJobOrder = findByCriteria(criterion1,criterion2);
			}else{
				lstSchoolInJobOrder = findByCriteria(criterion1);
			}
			for(SchoolInJobOrder schoolInJobOrder: lstSchoolInJobOrder){
				lstSchoolId.add(schoolInJobOrder.getSchoolId());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolId;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> allJobOrderBySchool(SchoolMaster schoolMaster)
	{
		List<JobOrder> jobOrders = new ArrayList<JobOrder>();
		SchoolInJobOrder schoolInJobOrder = null;
		List<SchoolInJobOrder> lstJobOrderBySchool = null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("schoolId", schoolMaster);
			criteria.add(criterion1);
			criteria.createCriteria("jobId").add(Restrictions.eq("status", "A"));
			lstJobOrderBySchool = criteria.list();
			Iterator itr = lstJobOrderBySchool.iterator();
			while(itr.hasNext())
			{
				schoolInJobOrder=(SchoolInJobOrder)itr.next();
				jobOrders.add(schoolInJobOrder.getJobId());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
		/************** add by ankit *********/
	@Transactional(readOnly = false)
	@SuppressWarnings("unchecked")
	public List<String[]> getNobleStSchoolPostion(int districtId,int sortingcheck, Order sortOrderStrVal, int start, int noOfRow,boolean report)
	{
		List<String[]> tpvh = new ArrayList<String[]>();
		try {

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria
					.createAlias("jobId", "jo").createAlias("schoolId", "sm").createAlias("jo.districtMaster", "dm")
					.setProjection(Projections.projectionList()
									.add(Projections.property("sm.schoolId"),"schoolId")
									.add(Projections.property("sm.schoolName"),"schoolName")
									.add(Projections.property("jo.jobId"),"jobId11")
									.add(Projections.property("noOfSchoolExpHires"),"noOfSchoolExpHires")
									
					);
			 criteria.add(Restrictions.eq("dm.districtId",7800038));
			criteria.addOrder(sortOrderStrVal);

			long a = System.currentTimeMillis();
			tpvh = criteria.list();
			long b = System.currentTimeMillis();
			if (tpvh.size() > 0) {
				for (Iterator it = tpvh.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
					
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return tpvh;
	}
	
	@Transactional(readOnly = false)
	@SuppressWarnings("unchecked")
	public List<String[]> getNobleStSchoolPostionCSV()
	{
		List<String[]> tpvh = new ArrayList<String[]>();
		try {

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.createAlias("jobId", "jo").createAlias("schoolId", "sm").createAlias("jo.districtMaster", "dm")
					.setProjection(Projections.projectionList()
									.add(Projections.property("sm.schoolId"),"schoolId")
									.add(Projections.property("sm.schoolName"),"schoolName")
									.add(Projections.property("jo.jobId"),"jobId11")
									.add(Projections.property("noOfSchoolExpHires"),"noOfSchoolExpHires")
									
					);
			 criteria.add(Restrictions.eq("dm.districtId",7800038));
			
			long a = System.currentTimeMillis();
			tpvh = criteria.list();
			long b = System.currentTimeMillis();
			if (tpvh.size() > 0) {
				for (Iterator it = tpvh.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
					
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return tpvh;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> getSchoolJobs(SchoolMaster schoolMaster)
	{
		List<Integer> jobOrders = new ArrayList<Integer>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());						
			criteria.createAlias("jobId", "jo")
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("jo.jobId"), "jobId" ));
			criteria.add(Restrictions.eq("schoolId", schoolMaster));
			criteria.add(Restrictions.eq("jo.status", "A"));

			jobOrders = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	
	@Transactional(readOnly=false)
	public List<Integer> getSchoolListJobs(List<SchoolMaster> schoolMaster)
	{
		List<Integer> jobOrders = new ArrayList<Integer>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());						
			criteria.createAlias("jobId", "jo")
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("jo.jobId"), "jobId" ));
			criteria.add(Restrictions.in("schoolId", schoolMaster));
			criteria.add(Restrictions.eq("jo.status", "A"));

			jobOrders = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> findSchoolJobByDistricts(List<DistrictMaster> districtMasters){
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		try 
		{
			Criterion criterionDistrict = Restrictions.in("districtMaster",districtMasters);
			lstSchoolInJobOrder = findByCriteria(criterionDistrict);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstSchoolInJobOrder;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> findJobIdBySchool(SchoolMaster schoolId,Integer districtId)
	{
		List<Integer> lstSchoolInJobOrder= null;
		try 
		{
			Session session = getSession();
            Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("jobId", "jo").createAlias("schoolId", "sm").createAlias("jo.districtMaster", "dm");             
            criteria.setProjection( Projections.projectionList()
                    .add( Projections.property("jo.jobId"), "jobId" )
             );
             criteria.add(Restrictions.eq("sm.schoolId",schoolId.getSchoolId()));
             criteria.add(Restrictions.eq("dm.districtId",districtId));
			lstSchoolInJobOrder = criteria.list();		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolInJobOrder;
	}

	
	@Transactional(readOnly=false)
	public List<SchoolMaster> findSchoolJobByDistrictMaster(DistrictMaster districtMaster){
		List<SchoolMaster> lstSchoolInJobOrder= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(SchoolInJobOrder.class).createAlias("schoolId", "sm").createAlias("jobId", "jo");						
			criteria.add(Restrictions.eq("jo.districtMaster", districtMaster));
			criteria.addOrder(Order.asc("sm.schoolName"));
			criteria.setProjection(Projections.projectionList()
		            .add(Projections.groupProperty("schoolId").as("schoolId"))
		            );
			lstSchoolInJobOrder = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstSchoolInJobOrder;
	}
	
	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> findJobListByJob(JobOrder jobOrder,Order sortOrderStrVal,int start,int end)
	{
		List<SchoolInJobOrder> lstSchoolInJobOrder= new ArrayList<SchoolInJobOrder>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("jobId", jobOrder));
			
			Criteria crit=	criteria.createCriteria("schoolId");
			//crit.setFirstResult(start);
			//crit.setMaxResults(end);

			if(sortOrderStrVal != null)
				crit.addOrder(sortOrderStrVal);
			
			lstSchoolInJobOrder = criteria.list();		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolInJobOrder;
	}
	@Transactional(readOnly=false)
	public List<Integer> findJobIdBySchool1(SchoolMaster schoolMaster,DistrictMaster districtMaster,int JobOrderType,int entityID,int searchschoolflag,UserMaster userMaster)
	{
		List<Integer> lstSchoolInJobOrder= null;
		try 
		{
			Session session = getSession();
            Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("jobId", "jo").createAlias("schoolId", "sm").createAlias("jo.districtMaster", "dm");             
            criteria.setProjection( Projections.projectionList()
                    .add( Projections.property("jo.jobId"), "jobId" )
             );
            
            Criterion criterionJobOrderType = Restrictions.eq("jo.createdForEntity",JobOrderType);
            Criterion criterion=Restrictions.eq("dm.districtId",districtMaster.getDistrictId());
            Criterion criterionSchoolId=Restrictions.eq("sm.schoolId",schoolMaster.getSchoolId());
            
            if(JobOrderType==2 && entityID==3){

				if(districtMaster.getWritePrivilegeToSchool()!=null){
					if(districtMaster.getWritePrivilegeToSchool()){
						if(searchschoolflag==0)//searchschoolflag==0 Means search on default school Id and searchschoolflag==1 when school id is get by school Id filter:
						{
							/*criteria.createCriteria("jobId").add(criterion).add(criterionJobOrderType).add( 
									Restrictions.or(Restrictions.eq("sij.schoolId", schoolMaster),
											Restrictions.eq("writePrivilegeToSchool",true)
									) );*/
							
							criteria.add(criterion).add(criterionJobOrderType).add( 
									Restrictions.or(criterionSchoolId,
											Restrictions.eq("jo.writePrivilegeToSchool",true)
									) );
						}
						if(searchschoolflag==1 && Long.parseLong(""+schoolMaster.getSchoolId())==Long.parseLong(""+userMaster.getSchoolId().getSchoolId())) // Checking this : when logged in SA will search his DJO in which school is attached. 
						{
							//System.out.println("------------- search if condition ");
							/*criteria.createCriteria("jobId").add(criterion).add(criterionJobOrderType).add( 
									Restrictions.eq("sij.schoolId", schoolMaster
									) );*/
							criteria.add(criterion).add(criterionJobOrderType).add(criterionSchoolId);
						}
						else
						{
							//System.out.println("------------- search Else condition ");
							if(searchschoolflag==1 && schoolMaster.getSchoolId()!=userMaster.getSchoolId().getSchoolId()) // This condition is used for when Loggedin SA will search other School DJOs
							{
								
								/*criteria.createCriteria("jobId").add(criterion).add(criterionJobOrderType).add( 
										Restrictions.and(Restrictions.eq("sij.schoolId", schoolMaster),
												Restrictions.eq("writePrivilegeToSchool",true)
										) );*/
								criteria.add(criterion).add(criterionJobOrderType).add( 
										Restrictions.and(criterionSchoolId,
												Restrictions.eq("jo.writePrivilegeToSchool",true)
										) );
							}
						}
					}else{
						/*criteria.createCriteria("jobId").add(criterion).add(criterionJobOrderType).add(Restrictions.eq("sij.schoolId", schoolMaster));*/
						criteria.add(criterion).add(criterionJobOrderType).add(criterionSchoolId);
					}
				}
            }else{
            	criteria.add(criterion).add(criterionJobOrderType).add(criterionSchoolId);
            	
            }
            
           
			long a = System.currentTimeMillis();
			criteria.setProjection(Projections.groupProperty("jo.jobId"));
			lstSchoolInJobOrder = criteria.list();	
			long b = System.currentTimeMillis();
			System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + lstSchoolInJobOrder.size() );
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolInJobOrder;
	}	


	// for Candidate pool search
	@Transactional(readOnly=false)
	public List<Integer> findJobsIdByCriterion(Criterion... criterions ) {
		List<Integer> jobsId = new  ArrayList<Integer>();
		try{
		Criteria criteria = getSession().createCriteria(this.getPersistentClass());
		for(Criterion criterion : criterions)
			criteria.add(criterion);
		
		criteria.setProjection(Projections.property("jobId.jobId"));
		jobsId = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return jobsId;
		
	}
	@Transactional(readOnly=false)
	public int noOfExpHiresSql(JobOrder jobOrder,SchoolMaster schoolMaster,int allSchool)
	{
		int  noOfExpHires=0;
		
		try{
			
			Session session = getSessionFactory().openSession(); 
			Criteria criteria = session.createCriteria(getPersistentClass())
			.setProjection( Projections.projectionList()
    	          .add( Projections.sum("noOfSchoolExpHires"), "noOfSchoolExpHires" )    	        
    	      );			
			criteria.add(Restrictions.eq("jobId",jobOrder));			
			
			if(allSchool==0){
				criteria.add(Restrictions.eq("schoolId",schoolMaster));				
			}			
			noOfExpHires=(Integer)criteria.list().get(0);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return noOfExpHires;
	}

	// for Optimization
	
	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> findInternalCandidateOp(List<Integer> lstjobOrder,SchoolMaster schoolMaster)
	{
		List<SchoolInJobOrder> lstSchoolInJobOrder=new ArrayList<SchoolInJobOrder>();
		try
		{
			if(lstjobOrder.size()>0)
			{
				Criteria criteria  = session.createCriteria(this.getPersistentClass());
				criteria.createAlias("jobId", "jo");
				Criterion criterion_job =  Restrictions.in("jo.jobId",lstjobOrder);
				Criterion criterion_school = Restrictions.eq("schoolId",schoolMaster);
				criteria.add(criterion_job).add(criterion_school);
				lstSchoolInJobOrder=criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
		
		}
		return lstSchoolInJobOrder;
	}
	@Transactional(readOnly=false)
	public List<SchoolInJobOrder> findJobOrder_Op(JobOrder jobOrder)
	{
		List<SchoolInJobOrder> lstStatus= new ArrayList<SchoolInJobOrder>();
		try 
		{
  			Query query=null;
  			List<Object[]> objList=null;
			if(jobOrder!=null ){
 				Session session = getSession();
 			
 				String hql=	"SELECT  sjo.jobOrderSchoolId, sjo.noOfSchoolExpHires , sjo.createdDateTime,sjo.schoolId,sjo.jobId,sm.schoolName,sm.locationCode FROM  schoolinjoborder sjo "
			 	    +   "left join joborder jo on jo.jobId= sjo.jobId "
			 	   +   "left join schoolmaster sm on sm.schoolId= sjo.schoolId "
 				    + " WHERE   sjo.jobId =?  order by sjo.jobId ASC ";
 				query=session.createSQLQuery(hql);
 			 	query.setParameter(0, jobOrder.getJobId());
 			  	objList = query.list(); 
 			  	SchoolInJobOrder schoolInJobOrder=null;
				if(objList.size()>0){
				for (int j = 0; j < objList.size(); j++) {
						Object[] objArr = objList.get(j);
						schoolInJobOrder = new SchoolInJobOrder();
						
						schoolInJobOrder.setJobOrderSchoolId(objArr[0] == null ? 0 : Integer.parseInt(objArr[0].toString()));
						schoolInJobOrder.setNoOfSchoolExpHires(objArr[1] == null ? 0 : Integer.parseInt(objArr[1].toString()));
					 	if(objArr[2]!=null)
					 		schoolInJobOrder.setCreatedDateTime((Date) objArr[2]);
	 				   Long schoolIdTemp	=objArr[3] == null ? 0 : Long.parseLong(objArr[3].toString());
					   int jobIdTemp	=objArr[4] == null ? 0 : Integer.parseInt(objArr[4].toString());
	 				     JobOrder jobOrderTemp=null;
					     SchoolMaster schoolMaster=null;
	 				     schoolMaster= new SchoolMaster();  
					     schoolMaster.setSchoolId(schoolIdTemp);
					     schoolInJobOrder.setSchoolId(schoolMaster);
					       
					     schoolMaster.setSchoolName(objArr[5] == null ? null : objArr[5].toString());
					     schoolMaster.setLocationCode(objArr[6] == null ? null : objArr[6].toString() );
					      jobOrderTemp =new JobOrder();
					     jobOrderTemp.setJobId(jobIdTemp);
					     schoolInJobOrder.setJobId(jobOrderTemp);
	                    
	                    
	                    
					   lstStatus.add(schoolInJobOrder);
	 					}
				}	
			}	 			 
	 		 
		} 
	 	catch(Exception e){
			   
		         e.printStackTrace(); 
		}
	 return	lstStatus;
	}
}

