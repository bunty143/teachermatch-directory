package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.mapping.Array;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictSpecificTeacherTfaOptions;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificTeacherTfaOptionsDAO extends GenericHibernateDAO<DistrictSpecificTeacherTfaOptions, Integer>{

	public DistrictSpecificTeacherTfaOptionsDAO() {
		super(DistrictSpecificTeacherTfaOptions.class);
		// TODO Auto-generated constructor stub
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificTeacherTfaOptions> findTeachersDistrictSpecificTeacherTfaOptions(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster){
		List<DistrictSpecificTeacherTfaOptions> districtSpecificTeacherTfaOptions= null;
		try {
			Criterion criteria = Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetails);
			districtSpecificTeacherTfaOptions = findByCriteria(criteria,criterion1);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return districtSpecificTeacherTfaOptions;
		
	}
}
