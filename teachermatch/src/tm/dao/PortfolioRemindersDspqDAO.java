package tm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.PortfolioRemindersDspq;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.dao.generic.GenericHibernateDAO;


public class PortfolioRemindersDspqDAO extends GenericHibernateDAO<PortfolioRemindersDspq,Integer> 
{
	public PortfolioRemindersDspqDAO() {
		super(PortfolioRemindersDspq.class);
	}
	
	@Transactional(readOnly=false)
	public List<PortfolioRemindersDspq> getRecordByDistrict(DistrictMaster districtMaster){
		
		List<PortfolioRemindersDspq> portfolioRemindersList = new ArrayList<PortfolioRemindersDspq>();
		try{
			Criterion criterion1 	= Restrictions.eq("districtMaster",districtMaster);
			portfolioRemindersList 	= findByCriteria(criterion1);
		} catch(Exception exception){
			exception.printStackTrace();
		}
		return portfolioRemindersList;
	}
	
	@Transactional(readOnly=false)
	public Map<Integer,List<PortfolioRemindersDspq>> getDSPQByDistrict(DistrictMaster district)
	{
		Map<Integer,List<PortfolioRemindersDspq>> dSPQMap=new HashMap<Integer, List<PortfolioRemindersDspq>>();
		List<PortfolioRemindersDspq> lstDistSpecQuestions= new ArrayList<PortfolioRemindersDspq>();
		try 
		{
			     Session session = getSession();
			     Criteria criteria = session.createCriteria(getPersistentClass());
			     criteria.add(Restrictions.eq("districtMaster",district));
			     criteria.createCriteria("districtSpecificPortfolioQuestions").add(Restrictions.eq("status", "A"));
			     lstDistSpecQuestions=criteria.list();

				 System.out.println("PortfolioRemindersDspqSize::"+lstDistSpecQuestions.size());
				 dSPQMap.put(2, lstDistSpecQuestions);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return dSPQMap;		
	}
	
	@Transactional(readOnly=false)
	public PortfolioRemindersDspq getRecordByDistrictAndQuestion(DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions,DistrictMaster districtMaster){
		
		List<PortfolioRemindersDspq> portfolioRemindersList = new ArrayList<PortfolioRemindersDspq>();
		try{
			Criterion criterion1 	= Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 	= Restrictions.eq("districtSpecificPortfolioQuestions",districtSpecificPortfolioQuestions);
			portfolioRemindersList 	= findByCriteria(criterion1,criterion2);
		} catch(Exception exception){
			exception.printStackTrace();
		}
		return portfolioRemindersList.get(1);
	}

}
