package tm.controller.district;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.user.UserMaster;

@Controller
public class WeCanExportController {
	
	@RequestMapping(value="/wecanexport.do", method=RequestMethod.GET)
	public String templateGet(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		UserMaster userSession	=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userSession		=	(UserMaster) session.getAttribute("userMaster");
		}

		String districtName = null;
		String districtId = null;
		
		System.out.println(districtId+" "+districtName);
		
		if(userSession.getEntityType()==2)
		{
			districtId = userSession.getDistrictId().getDistrictId().toString();
			districtName = userSession.getDistrictId().getDistrictName();
		}
		
		System.out.println(" userSession.getEntityType() :: "+userSession.getEntityType());
		
		map.addAttribute("entityType",userSession.getEntityType());
		map.addAttribute("districtId",districtId);
		map.addAttribute("districtName",districtName);
		return "wecanexport";
	}

}
