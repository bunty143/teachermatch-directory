package tm.controller.district;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONObject;

import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CityMaster;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DspqGroupMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.master.SkillAttributesMaster;
import tm.bean.master.StateMaster;
import tm.bean.user.UserMaster;

import tm.controller.assessment.AssessmentController;
import tm.controller.master.GeoZoneController;
import tm.controller.master.JobCategoryController;
import tm.controller.master.ManageTagsController;
import tm.controller.master.ONRDashBoardController;
import tm.controller.user.UserController;
import tm.dao.JobOrderDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DspqGroupMasterDAO;
import tm.dao.master.EthinicityMasterDAO;
import tm.dao.master.EthnicOriginMasterDAO;
import tm.dao.master.GenderMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.QuestionTypeMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.SkillAttributesMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TimeZoneMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.service.onboarding.OnboardingDashboardAjax;
import tm.utility.Utility;

@Controller
public class ManageDSPQController 
{
	String locale = Utility.getValueOfPropByKey("locale");	 
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO){
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private SkillAttributesMasterDAO skillAttributesMasterDAO;
	public void setSkillAttributesMasterDAO(
			SkillAttributesMasterDAO skillAttributesMasterDAO) {
		this.skillAttributesMasterDAO = skillAttributesMasterDAO;
	}
	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) {
		this.cityMasterDAO = cityMasterDAO;
	}
	
	@Autowired 
	private ContactTypeMasterDAO contactTypeMasterDAO;
	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private QuestionTypeMasterDAO questionTypeMasterDAO;
	
	@Autowired
	private DspqGroupMasterDAO dspqGroupMasterDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private GenderMasterDAO genderMasterDAO;
	
	@Autowired
	private EthinicityMasterDAO ethinicityMasterDAO;
	
	@Autowired
	private EthnicOriginMasterDAO ethnicOriginMasterDAO;
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	@Autowired
	private  StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private TimeZoneMasterDAO timeZoneMasterDAO;

	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	
	@Autowired
	OnboardingDashboardAjax onBoardingDashboardAjax;
	
	
	@RequestMapping(value="/manageportfolio.do", method=RequestMethod.GET)
	public String doDSPQConfigGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);		
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		UserMaster userSession=(UserMaster) session.getAttribute("userMaster");
		session.setAttribute("displayType","1");
		session.setAttribute("menuId", "#menu3");
		session.setAttribute("subMenuId", "#menu3_2");
		try{
			String sdistrictId=(request.getParameter("districtId")!=null)?request.getParameter("districtId").toString():"0";
			map.addAttribute("entityType",userSession.getEntityType());
			if(userSession.getEntityType()!=1){
				map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
			}else{
				if(Utility.getIntValue(sdistrictId)!=0){
					DistrictMaster districtMaster=districtMasterDAO.findById(Utility.getIntValue(sdistrictId), false, false);
					if(districtMaster!=null){
						map.addAttribute("DistrictName",districtMaster.getDistrictName());
						map.addAttribute("DistrictId",districtMaster.getDistrictId());
					}
				}else
					map.addAttribute("DistrictName",null);
			}
			List<DspqGroupMaster> dspqGroupMasters = dspqGroupMasterDAO.findAll();
            map.addAttribute("dspqGroupMasters", dspqGroupMasters);
            try{
            	List<QuestionTypeMaster> questionTypeMasters = questionTypeMasterDAO.getActiveQuestionTypesByShortName(new String[]{"slsel","tf","ml","et","mlsel","mloet","sloet","dt","drsls","sl","UAT","UATEF","sswc"});
            	JSONObject json = new JSONObject();
            	for (QuestionTypeMaster questionTypeMaster : questionTypeMasters) {
            		json.put(""+questionTypeMaster.getQuestionTypeId()+"", questionTypeMaster.getQuestionTypeShortName());
            	}
            	map.addAttribute("questionTypeMasters", questionTypeMasters);
            	map.addAttribute("json", json);
            	
            }
            catch (Exception e) 
            {
            	e.printStackTrace();
            }
		}catch (Exception e) {
			e.printStackTrace();
		}		
		map.addAttribute("userMaster", userSession);
		map.addAttribute("kendoEditor", 1);
		
		return "managedspq";
	}
	/*@RequestMapping(value="/wizardvideo.do", method=RequestMethod.GET)
	public String doWizardVideoGET(ModelMap map,HttpServletRequest request)
	{
		
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		session.setAttribute("displayType","1");
			if(userSession.getEntityType()!=1){
				map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictName",null);
			}
			if(userSession.getEntityType()==3){
				map.addAttribute("SchoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
			}else{
				map.addAttribute("SchoolName",null);
			}
			
			
		map.addAttribute("userMaster", userSession);
		map.addAttribute("wizardvideo", "4506");
		
		return "wizardvideo";
	}*/
	//*************** Adding By Deepak ***********************************
	@RequestMapping(value="/jobcategorydspq.do", method=RequestMethod.GET)
	public String jobcategorydspqGET(ModelMap map,HttpServletRequest request)
	{
		UserMaster userSession=null;
		try 
		{
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			session.setAttribute("displayType","1");
			session.setAttribute("menuId", "#menu3");
			session.setAttribute("subMenuId", "#menu3_1");
			userSession		=	(UserMaster) session.getAttribute("userMaster");
			JobCategoryController.jobCategoryCalculator(map, request, jobCategoryMasterDAO, userMasterDAO, districtMasterDAO);
		 
		    map.addAttribute("userMaster",userSession);
		    System.out.println("entityID   :::::::  "+userSession.getEntityType());
		    map.addAttribute("entityID",userSession.getEntityType());
		if(userSession.getEntityType()!=1){
			map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
			map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
		}else{
			map.addAttribute("districtName",null);
		}
		if(userSession.getEntityType()==3){
			map.addAttribute("SchoolName",userSession.getSchoolId().getSchoolName());
			map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
		}else{
			map.addAttribute("SchoolName",null);
		}
		
		map.addAttribute("userMaster", userSession);
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		if(userSession.getEntityType()==3){
			return "signin";
		}else{
			return "jobcategorydspq";

		}	
	}
	
	
	@RequestMapping(value="/home.do", method=RequestMethod.GET)
	public String tDashboardGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			session.setAttribute("displayType","1");
			session.setAttribute("menuId", "#menu1");
			session.setAttribute("subMenuId", "#menu1_1");
			//AdminDashboardController.tmdashbordMethod(map, request,demoClassScheduleDAO);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "home";
	}
	
	
	@RequestMapping(value="/managetagsdspq.do", method=RequestMethod.GET)
	public String tagsGET(ModelMap map,HttpServletRequest request)
	{
		UserMaster userSession=null;
		try 
		{
			HttpSession session = request.getSession(false);
			session.setAttribute("menuId", "#menu2");
			session.setAttribute("subMenuId", "#menu2_3");
			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			userSession		=	(UserMaster) session.getAttribute("userMaster");
			session.setAttribute("displayType","1");
			ManageTagsController.managetagsmethod(map,request,userMasterDAO);			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		if(userSession.getEntityType()==3){
			return "signin";
		}else{
			return "managetagsdspq";

		}	
	}

	@RequestMapping(value="/managedocumentsdspq.do", method=RequestMethod.GET)
	public String attachmentsGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			HttpSession session = request.getSession(false);
			session.setAttribute("menuId", "#menu2");
			session.setAttribute("subMenuId", "#menu2_2");
			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			session.setAttribute("displayType","1");
			ManageTagsController.managedocumentsmethod(map,request,roleAccessPermissionDAO,userMasterDAO);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
			return "managedocumentsdspq";
		}	
	
	
	@RequestMapping(value="/geozonesetupdspq.do", method=RequestMethod.GET)
	public String geozoneGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		
		UserMaster userMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
		}
		session.setAttribute("displayType","1");
		session.setAttribute("menuId", "#menu2");
		session.setAttribute("subMenuId", "#menu2_4");
		GeoZoneController.geozonesetupmethod(map,userMaster);
		
		return "geozonesetupdspq";
	}
	
	
	
	@RequestMapping(value="/pnrdashboarddspq.do", method=RequestMethod.GET)
    public String onr(ModelMap map,HttpServletRequest request){
         
		UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		session.setAttribute("displayType","1");
		session.setAttribute("menuId", "#menu6");
		session.setAttribute("subMenuId", "#menu6_1");
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		ONRDashBoardController.pnrdashboardmethod(map, request, userMaster, roleId, raceMasterDAO, genderMasterDAO, ethinicityMasterDAO, ethnicOriginMasterDAO, roleAccessPermissionDAO, statusMasterDAO, timeZoneMasterDAO, stateMasterDAO);

		 
		return "onrdashboarddspq";
    }
	
	
	@Transactional
	@RequestMapping(value="/districtquestionsdspq.do", method=RequestMethod.GET)
	public String getDistrictQuestions(ModelMap map,HttpServletRequest request)
	{
		
		try {
			HttpSession session = request.getSession(false);
			UserMaster userMaster=null;
			DistrictMaster districtMaster = null;
			Integer districtId =null;
			session.setAttribute("displayType","1");
			session.setAttribute("menuId", "#menu3");
			session.setAttribute("subMenuId", "#menu3_4");
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getEntityType()==3){
					return "redirect:index.jsp";
				}
				districtMaster = userMaster.getDistrictId();
				if(districtMaster!=null)
				districtId = districtMaster.getDistrictId();
			}
			AssessmentController.districtquestionsmethod(map,request,userMaster,districtId);
			
			
			System.out.println("districtId: ::: "+districtId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "districtquestionsdspq";
	}
	
	/*@RequestMapping(value="/districtspecificquestionsdspq.do", method=RequestMethod.GET)
	public String getDistrictQuestion(ModelMap map,HttpServletRequest request)
	{
		System.out.println("controller districtspecificquestionsdspq.do AssessmentController getDistrictQuestion()");
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			districtMaster = userMaster.getDistrictId();
		}
		session.setAttribute("displayType","1");
		AssessmentController.districtspecificquestionsmethod(map,request,roleId,districtMaster,questionTypeMasterDAO,districtSpecificQuestionsDAO,roleAccessPermissionDAO);
		
		return "districtspecificquestionsdspq";
	}*/
	
	@RequestMapping(value="/managestatusdspq.do", method=RequestMethod.GET)
	public String doManageStatusGET(ModelMap map,HttpServletRequest request)
	{
		
		HttpSession session = request.getSession(false);		
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}	
		session.setAttribute("displayType","1");
		session.setAttribute("menuId", "#menu3");
		session.setAttribute("subMenuId", "#menu3_5");
	    UserMaster userSession                    =    (UserMaster) session.getAttribute("userMaster");
	    //List<SecondaryStatus> lstTreeStructure=    new ArrayList<SecondaryStatus>();
	    System.out.println("userSession.getEntityType()::::::::::"+userSession.getEntityType());
	    //StringBuffer sb =    new StringBuffer();
	    try 
	    {
	        if(userSession.getEntityType()!=1 && userSession.getEntityType()!=5){
	            map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
	            map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
	        }else{
	            map.addAttribute("DistrictName",null);
	        }
	        if(userSession.getEntityType()==3){
	            map.addAttribute("SchoolName",userSession.getSchoolId().getSchoolName());
	            map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
	        }else{
	            map.addAttribute("SchoolName",null);
	        }
	        
	        Integer districtId=0;
	        String sdistrictId="";
	        
	        Integer jobCategory=0;
	        String sjobCategory="";
	        
	        Integer jobSubCateId=0;
			String sJobSubCateId ="";
	        
	        sdistrictId=(request.getParameter("districtId")!=null)?request.getParameter("districtId").toString():"0";
	        sjobCategory=(request.getParameter("jobCategory")!=null)?request.getParameter("jobCategory").toString():"0";
	        
	        
	        sJobSubCateId=(request.getParameter("jobSubCategoryId")!=null)?request.getParameter("jobSubCategoryId").toString():"0";
	        
	        
	        int headQuarterId = 0;
	        if(userSession.getHeadQuarterMaster()!=null){
	            headQuarterId = userSession.getHeadQuarterMaster().getHeadQuarterId();
	        }else{
	            headQuarterId = Integer.parseInt((request.getParameter("headQuarterId")!=null)?request.getParameter("headQuarterId").toString():"0");    
	        }
	        System.out.println("headQuarterId::::::::>>>>>>>>>>>>>>>>>====>"+headQuarterId);
	        HeadQuarterMaster headQuarterMaster = null;
	        if(headQuarterId!=0){
	            headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
	        }
	        if(headQuarterMaster!=null)
	        {
	            map.addAttribute("headQuarterName",headQuarterMaster.getHeadQuarterName());
	            map.addAttribute("headQuarterId",headQuarterId);
	        }
	        
	        int branchId = 0;
	        if(userSession.getBranchMaster()!=null){
	            branchId = userSession.getBranchMaster().getBranchId();
	        }else{
	            branchId = Integer.parseInt((request.getParameter("branchId")!=null)?request.getParameter("branchId").toString():"0");    
	        }
	        System.out.println("branchId::::::::>>>>>>>>>>>>>>>>>====>"+branchId);
	        
	        BranchMaster branchMaster = null;
	        if(branchId!=0){
	            branchMaster = branchMasterDAO.findById(branchId, false, false);
	        }
	        if(branchMaster!=null)
	        {
	            map.addAttribute("BranchName",branchMaster.getBranchName());
	            map.addAttribute("branchId",branchId);
	            
	        }
	        
	        if(!sdistrictId.equals("") && !sdistrictId.equals("0"))
	            districtId=Integer.parseInt(sdistrictId);
	        
	        DistrictMaster districtMaster=null;
	        
	        if(districtId!=0){
	            districtMaster=districtMasterDAO.findById(districtId, false, false);
	            session.setAttribute("districtMasterId", districtMaster.getDistrictId());
				session.setAttribute("districtMasterName", districtMaster.getDistrictName());
	        }
	        else{
	        	 session.setAttribute("districtMasterId", 0);
				 session.setAttribute("districtMasterName", "");
	        }
	        
	        
	        if(districtMaster!=null)
	        {
	            map.addAttribute("DistrictName",districtMaster.getDistrictName());
	            map.addAttribute("DistrictId",districtId);
	            
	        }
	        
	        if(!sjobCategory.equals("") && !sjobCategory.equals("0"))
	        {
	            jobCategory=Integer.parseInt(sjobCategory);
	            map.addAttribute("jobCategory",jobCategory);
	        }
	        else
	        {
	            map.addAttribute("jobCategory","");
	        }
	        
	        if(!sJobSubCateId.equals("") && !sJobSubCateId.equals("0")){
				jobSubCateId=Integer.parseInt(sJobSubCateId);
				map.addAttribute("jobSubCateId",jobSubCateId);
			}else{
				map.addAttribute("jobSubCateId","");
			}
	        
	        
	        map.addAttribute("entityType",userSession.getEntityType());;
	        
	        List<SkillAttributesMaster> skillAttributesMasters=skillAttributesMasterDAO.getActiveList(jobCategory);
	        map.addAttribute("skillAttributesMasters", skillAttributesMasters);
	        
	        
	    }catch (Exception e) {
	        e.printStackTrace();
	    }        
	    map.addAttribute("userMaster", userSession);	

	    return "managestatusdspq";
	}
	
	
	@RequestMapping(value="/districtspecificquestionssetdspq.do", method=RequestMethod.GET)
	public String I4QuestionsSetGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		session.setAttribute("displayType","1");
		session.setAttribute("menuId", "#menu3");
		session.setAttribute("subMenuId", "#menu3_3");
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		UserMaster userSession	=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userSession		=	(UserMaster) session.getAttribute("userMaster");
		}
		AssessmentController.districtspecificquestionssetMethod(map,request);
	
		
		return "districtspecificquestionssetdspq";
	}
	
	@RequestMapping(value="/managedistrictdspq.do", method=RequestMethod.GET)
	public String districtGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster	userMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			session.setAttribute("menuId", "#menu2");
			session.setAttribute("subMenuId", "#menu2_1");
			session.setAttribute("displayType","1");
			DistrictManageController.managedistrictMethod(map,request,roleAccessPermissionDAO,userLoginHistoryDAO);
	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "managedistrictdspq";
	}
	
	@RequestMapping(value="/manageuserdspq.do", method=RequestMethod.GET)
	public String doManageUserGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		session.setAttribute("displayType","1");
		session.setAttribute("menuId", "#menu5");
		session.setAttribute("subMenuId", "#menu5_1");
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		UserController.manageuserMethod(map,request,userMasterDAO,roleAccessPermissionDAO);

		return "manageuserdspq";
	}
	
	@RequestMapping(value="/communicationsdspq.do", method=RequestMethod.GET)
	public String templateGet(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		session.setAttribute("displayType","1");
		session.setAttribute("menuId", "#menu4");
		session.setAttribute("subMenuId", "#menu4_1");
		UserMaster userSession	=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userSession		=	(UserMaster) session.getAttribute("userMaster");
		}
		ManageTemplateController.communicationsMethod(map,request);

		return "managetemplatedspq";
	}
	
	
	//************************ New Adding Controller for Coming Soon ***************************************
	
	@RequestMapping(value="/notifications.do", method=RequestMethod.GET)
	public String doNotificationsGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		session.setAttribute("displayType","1");
		session.setAttribute("menuId", "#menu4");
		session.setAttribute("subMenuId", "#menu4_2");
		System.out.println("notifications.............................");
	   return "notifications";
	}
	
	@RequestMapping(value="/visibility.do", method=RequestMethod.GET)
	public String doVisibilityGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		session.setAttribute("displayType","1");
		session.setAttribute("menuId", "#menu5");
		session.setAttribute("subMenuId", "#menu5_2");
		System.out.println("visibility.............................");
	   return "visibility";
	}
	@RequestMapping(value="/onboardingworkflow.do", method=RequestMethod.GET)
	public String doOnBoardingWorkflowGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		UserMaster userSession	=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userSession		=	(UserMaster) session.getAttribute("userMaster");
		}
		session.setAttribute("displayType","1");
		map.addAttribute("entityType",userSession.getEntityType());
		
		if(userSession.getEntityType()!=1){
			map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
			map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());	
		}else{
			map.addAttribute("DistrictName",null);
		}
		session.setAttribute("menuId", "#menu3");
		session.setAttribute("subMenuId", "#menu3_6");
	   return "onboardingworkflow";
	}
	//***************************  Adding By Deepak *******************************************************************************88
	

	@RequestMapping(value="/actionfeeds.do", method=RequestMethod.GET)
	public String doActionFeedGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		
		UserMaster userSession									=	(UserMaster) session.getAttribute("userMaster");
		session.setAttribute("displayType", "0");
		session.setAttribute("menuId", "#menu6");
		session.setAttribute("subMenuId", "#menu6_2");
		DistrictMaster districtMaster							=	null;
		//String districtId = request.getParameter("distId")		==	null?"0":request.getParameter("distId").trim();
		
		String districtId="";
		if(userSession.getDistrictId()!=null){
		userSession.getDistrictId().getDistrictId().toString();
		}
		
		if(districtId.equals(""))
		{
			districtMaster=	new DistrictMaster();
		}
		else
		{
			int iDistrictId=Utility.getIntValue(districtId);
			if(iDistrictId >0)
				districtMaster=districtMasterDAO.findById(Utility.decryptNo(Integer.parseInt(districtId)), false, false);
			if(districtMaster==null)
				districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
		}
			if(districtMaster.getCanTMApproach()!=null && districtMaster.getCanTMApproach()!=0){
				districtMaster.setCanTMApproach(1);
			}
	
		// =============== for Getting the All database Field Values from database ====================
			List<ContactTypeMaster> contactTypeMaster			=	contactTypeMasterDAO.findByCriteria(Order.asc("contactType"));
			List<StateMaster> listStateMasters  = stateMasterDAO.findAllStateByOrder();
			List<CityMaster> listCityMasters	= cityMasterDAO.findCityByState(districtMaster.getStateId());
			
			/*========= If Decision Maker Password is available then showing it Blank*/
			//map.addAttribute("authKey", authKey);
			map.addAttribute("districtMaster", districtMaster);
			map.addAttribute("listStateMasters", listStateMasters);
			map.addAttribute("listCityMasters", listCityMasters);	
			map.addAttribute("userSession", userSession);
			map.addAttribute("contactTypeMaster", contactTypeMaster);
			
			if(districtMaster.getInitiatedOnDate()==null){
				map.addAttribute("initiatedOnDate",null);
			}else{
				map.addAttribute("initiatedOnDate", Utility.getCalenderDateFormart(districtMaster.getInitiatedOnDate()+""));
			}
			if(districtMaster.getContractStartDate()==null){
				map.addAttribute("contractStartDate",null);
			}else{
				map.addAttribute("contractStartDate", Utility.getCalenderDateFormart(districtMaster.getContractStartDate()+""));
			}
			
			if(districtMaster.getContractEndDate()==null){
				map.addAttribute("contractEndDate",null);
			}else{
				map.addAttribute("contractEndDate", Utility.getCalenderDateFormart(districtMaster.getContractEndDate()+""));
			}
			int jobOrderSize=0;
			if(districtMaster.getDistrictId()!=null){
				jobOrderSize=jobOrderDAO.checkDefaultDistrictAndSchool(districtMaster,null,1);
			}
			if(jobOrderSize==1){
				map.addAttribute("aJobRelation","disabled=\"disabled\"");
			}else{
				map.addAttribute("aJobRelation",null);
			}
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);
			
			// For VVI
			try
			{
				String statusListBoxValueForVVI="";
				String quesSetIDValue= "";
				String quesSetNameValue= "";
				if(districtMaster.getStatusMasterForVVI()!=null)
					statusListBoxValueForVVI=""+districtMaster.getStatusMasterForVVI().getStatusId();
				else if(districtMaster.getSecondaryStatusForVVI()!=null)
				{
					System.out.println("second");
					statusListBoxValueForVVI="SSIDForVVI_"+districtMaster.getSecondaryStatusForVVI().getSecondaryStatusId();
				}
					
				
				if(districtMaster.getI4QuestionSets()!=null)
				{
					quesSetIDValue=""+districtMaster.getI4QuestionSets().getID();
					quesSetNameValue=districtMaster.getI4QuestionSets().getQuestionSetText();
				}
				
				map.addAttribute("statusListBoxValueForVVI", statusListBoxValueForVVI);
				map.addAttribute("quesSetIDValue", quesSetIDValue);
				map.addAttribute("quesSetNameValue", quesSetNameValue);
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "actionfeeds";
	}
/*
 * @Author  : Deepak
 *
 * Using multiple file unloader Controller
 * Controller hit by Ajax 
 *  
 * <script>
 * 
 * var formData = new FormData(); 
 * 
 * loop start             //for the multiple file 
 * formData.append('file1',document.getElementById("fileTagName").files[0]);
 * loop end
 * 
 * formData.append('uploadFilePath','teacherRootPath');     //For the file directory location
 * formData.append('fileLocation','Document');              //Uploading file Folder name
 *  $.ajax({
                url : 'multipleDocumentUpload.do',
                data : formData,
                processData : false,
                contentType : false,
                type : 'POST',
                success : function(data) {
                    alert(data);
                },
                error : function(err) {
                    alert(err);
                }
            });
 * </script>
 * 
 * Note :- Controller return the file name. 
 *         If uploading multiple file than return ',' separated file names
 */
	 @RequestMapping(value = "/multipleDocumentUpload.do", method = RequestMethod.POST)
	    public @ResponseBody String uploadMultipleFileHandler(MultipartHttpServletRequest request , HttpServletResponse response) {
		 
		 Iterator<String> itr =  request.getFileNames();
		 Integer count=0;
		 String pathKey=request.getParameter("uploadFilePath");
		 String location=request.getParameter("fileLocation");
	     String message = "";
	        while (itr.hasNext()) {
	            MultipartFile file = request.getFile(itr.next());
	            String name = file.getOriginalFilename();
	            System.out.println("File name :::::  "+name);
	            try {
	                byte[] bytes = file.getBytes();
	                String rootPath=Utility.getValueOfPropByKey(pathKey);
	                File dir = new File(rootPath + File.separator + location);
	                if (!dir.exists())
	                    dir.mkdirs();
	 
	                File serverFile = new File(dir.getAbsolutePath()+ File.separator + file.getOriginalFilename());
	                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
	                stream.write(bytes);
	                stream.close();
	                if(count==0){
	                	message +=name;
	                	count=1;
	                }else{
	                	message +=","+name;
	                }
	            } catch (Exception e) {
	                return "Uploading Failed => " + e.getMessage();
	            }
	        }
	        return message;
	    }
	 
}
