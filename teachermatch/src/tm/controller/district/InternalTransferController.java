package tm.controller.district;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.InternalTransferCandidates;
import tm.bean.JobCategoryForInternalCandidate;
import tm.bean.TeacherDetail;
import tm.bean.cgreport.DistrictMaxFitScore;
import tm.bean.master.CityMaster;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictDomain;
import tm.bean.master.DistrictMaster;
import tm.bean.master.ExclusivePeriodMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.SkillAttributesMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusNodeMaster;
import tm.bean.master.StatusPrivilegeForSchools;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobCategoryForInternalCandidateDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.cgreport.DistrictMaxFitScoreDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictDomainDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.ExclusivePeriodMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.SkillAttributesMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.StatusNodeMasterDAO;
import tm.dao.master.StatusPrivilegeForSchoolsDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.utility.ImageResize;
import tm.utility.Utility;

@Controller
public class InternalTransferController 
{
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@Autowired
	private JobCategoryForInternalCandidateDAO jobCategoryForInternalCandidateDAO;
	
	@Autowired
	private ServletContext servletContext;
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	public void setSubjectMasterDAO(SubjectMasterDAO subjectMasterDAO) {
		this.subjectMasterDAO = subjectMasterDAO;
	}
	@Autowired
	private DistrictDomainDAO districtDomainDAO;
	
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@RequestMapping(value="/chkCategory.do", method=RequestMethod.POST)
	public String isChkCategory(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception{
		System.out.println("chkCategory.do");
		String categoryId = request.getParameter("categoryId")==null?"":request.getParameter("categoryId").trim();
		String teacherId = request.getParameter("teacherId")==null?"":request.getParameter("teacherId").trim();
		String categoryFlag= request.getParameter("categoryFlag")==null?"":request.getParameter("categoryFlag").trim();
		
		if(categoryId!=null && !categoryId.equals("")){
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			TeacherDetail teacherDetail=teacherDetailDAO.findById(Integer.parseInt(teacherId),false,false);
			List<JobCategoryForInternalCandidate> jcficList= new ArrayList<JobCategoryForInternalCandidate>();
			if(categoryFlag!=null && categoryFlag.equals("0")){
				jcficList=jobCategoryForInternalCandidateDAO.getCategoryByTeacher(teacherDetail);
				if((jcficList!=null && jcficList.size()>0)){
					pw.println("1");
				}else{
					pw.println("0");
				}
			}else{
				JobCategoryMaster jobCategoryMaster=jobCategoryMasterDAO.findById(Integer.parseInt(categoryId),false,false);
				jcficList=jobCategoryForInternalCandidateDAO.getCategoryByTeacherWithCategory(teacherDetail,jobCategoryMaster);
				if((jcficList!=null && jcficList.size()>0)){
					pw.println("1");
				}else{
					pw.println("0");
				}
			}
		}
		return null;
	}
	
	@RequestMapping(value="/chkUserEmail.do", method=RequestMethod.POST)
	public String isChkUserEmail(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception{
		System.out.println("chkUserEmail");
		String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
		List<UserMaster> lstUserMaster = userMasterDAO.findByEmail(emailAddress);				
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		if((lstUserMaster!=null && lstUserMaster.size()>0)){
			pw.println("1");
		}else{
			pw.println("0");
		}
		return null;
	}
	
	@RequestMapping(value="/chkInternalTeacher.do", method=RequestMethod.POST)
	public String isChkInternalTeacher(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception{
		System.out.println("chkInternalTeacher.do");
		String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
		String teacherId = request.getParameter("teacherId")==null?"":request.getParameter("teacherId").trim();
		List<TeacherDetail> lstTeacherDetail = teacherDetailDAO.checkDuplicateTeacherEmail(teacherId,emailAddress);				
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		if((lstTeacherDetail!=null && lstTeacherDetail.size()>0)){
			if(lstTeacherDetail.get(0).getInternalTransferCandidate()){
				pw.println("1");
			}else{
				pw.println("0");
			}
		}else{
			pw.println("0");
		}
		return null;
	}
	@RequestMapping(value="/internaltransfer.do", method=RequestMethod.GET)
	public String doJobCategory(ModelMap map,HttpServletRequest request, HttpServletResponse response){
		Utility.setRefererURLByJobsBoard(request);
		DistrictMaster districtMaster = null;
		SchoolMaster schoolMaster = null;
		HttpSession session = request.getSession();	
		System.out.println("::::::::::::::::::::internaltransfer.do:::::::::::::::::");		
		try 
		{	
			String source = "";
			String target = "";
			String path = "";
			
					
			String districtId =  "";			
			String schoolId = "";
			String teacherId="";
			
			try {
				districtId = request.getParameter("districtId")==null?"":request.getParameter("districtId");			
				districtId = ""+ Utility.decryptNo(Integer.parseInt(districtId));	
			} catch (Exception e) {
				
			}
			try {							
				schoolId = request.getParameter("schoolId")==null?"":request.getParameter("schoolId");
				schoolId = ""+ Utility.decryptNo(Integer.parseInt(schoolId));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {							
				teacherId = request.getParameter("teacherId")==null?"":request.getParameter("teacherId");
			} catch (Exception e) {
				// TODO: handle exception
			}
			session.removeAttribute("schoolMaster");
			session.removeAttribute("districtMaster");
		
		
			if((schoolId.equals("0")||schoolId.equals("")) && (districtId.equals("0")||districtId.equals("")) && (session.getAttribute("teacherDetail")==null)){
				//response.sendRedirect("signin.do");				
			}
			if(!schoolId.trim().equals("")){
				try {
					schoolMaster = schoolMasterDAO.findById(new Long(schoolId), false, false);
					if(schoolMaster!=null){
						districtMaster = schoolMaster.getDistrictId();
						if(schoolMaster!=null){
							map.addAttribute("schoolMaster", schoolMaster);
							map.addAttribute("districtMaster", districtMaster);
							session.setAttribute("schoolMaster", schoolMaster);
							session.setAttribute("districtMaster", districtMaster);					
							
							source = Utility.getValueOfPropByKey("schoolRootPath")+schoolMaster.getSchoolId()+"/"+schoolMaster.getLogoPath();
							target = servletContext.getRealPath("/")+"/"+"/school/"+schoolMaster.getSchoolId()+"/";
							
					        File targetDir = new File(target);
					        if(!targetDir.exists())
					        	targetDir.mkdirs();
					        File sourceFile = new File(source);
					        
					        File targetFile = new File(targetDir+"/"+sourceFile.getName());
					        
					        if(sourceFile.exists())
					        {
					        	FileUtils.copyFile(sourceFile, targetFile);
						        path =  Utility.getValueOfPropByKey("contextBasePath")+"/school/"+schoolMaster.getSchoolId()+"/"+sourceFile.getName();
						        
					        }
					        map.addAttribute("logoPath", path);
						}
					}
					else{
						//response.sendRedirect("signin.do");
					}
						
				} 
				catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			else if(!districtId.equals("")){
				try {
					districtMaster = districtMasterDAO.findById(new Integer(districtId), false, false);
					if(districtMaster!=null){
							
						map.addAttribute("districtMaster", districtMaster);
						session.setAttribute("districtMaster", districtMaster);					
						
						source = Utility.getValueOfPropByKey("districtRootPath")+districtMaster.getDistrictId()+"/"+districtMaster.getLogoPath();
						target = servletContext.getRealPath("/")+"/"+"/district/"+districtMaster.getDistrictId()+"/";
						
						File sourceFile = new File(source);
				        File targetDir = new File(target);
				        if(!targetDir.exists())
				        	targetDir.mkdirs();
				        
				        File targetFile = new File(targetDir+"/"+sourceFile.getName());
				        
				        if(sourceFile.exists())
				        {
					        FileUtils.copyFile(sourceFile, targetFile);
					        path =  Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtMaster.getDistrictId()+"/"+sourceFile.getName();
				        }
				        map.addAttribute("logoPath", path);					
					}
					else{
						//response.sendRedirect("signin.do");
					}	
				} 
				catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			if(session.getAttribute("logoPath")!=null && (!session.getAttribute("logoPath").equals("")))
				path=(String)session.getAttribute("logoPath");
			
			if(!Utility.existsURL(path))
			{
				path="images/applyfor-job.png";
				map.addAttribute("logoPath", path);
			}
			
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
			map.addAttribute("jobCategoryMasterlst",jobCategoryMasterlst);	
			
			List<SubjectMaster> lstSubjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(districtMaster);
			map.addAttribute("lstJobCategoryMasters", jobCategoryMasterlst);
			SubjectMaster sm = new SubjectMaster();
			sm.setSubjectName("No Subject");
			sm.setSubjectId(0);
			lstSubjectMasters.add(sm);
			map.addAttribute("lstSubjectMasters", lstSubjectMasters);
			List<DistrictDomain> districtDomainList= new ArrayList<DistrictDomain>();
			districtDomainList=districtDomainDAO.findDomainsByDistrict(districtMaster);
			String districtDomains="";
			int counter=0;
			for(DistrictDomain districtDomain : districtDomainList){
				if(counter==0)districtDomains="@";
				counter++;
				districtDomains+=""+districtDomain.getDomainName()+"@";
			}
			
			String districtDName="";
			if(districtMaster!=null){
				if(districtMaster.getDisplayName()!=null && !districtMaster.getDisplayName().equals("")){
					districtDName=districtMaster.getDisplayName();
				}else{
					districtDName=districtMaster.getDistrictName();
				}
			}
			
			
			TeacherDetail teacherDetail=null;
			boolean optEmails=false;
			InternalTransferCandidates iTCObj=null;
			if(teacherId!=null && teacherId!=""){
				teacherDetail=teacherDetailDAO.findById(Integer.parseInt(teacherId),false,false);
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getDistrictForTeacher(teacherDetail);
				if(itcList.size()>0){
					iTCObj=itcList.get(0);
					schoolMaster=iTCObj.getSchoolMaster();
					optEmails=iTCObj.isOptEmails();
				}
			}
			map.addAttribute("optEmails", optEmails);
			map.addAttribute("teacherDetail", teacherDetail);
			map.addAttribute("schoolMaster",schoolMaster);
			map.addAttribute("teacherId", teacherId);
			map.addAttribute("districtDName", districtDName);
			map.addAttribute("districtDomains", districtDomains);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		if(districtMaster==null && schoolMaster==null ){
			map.addAttribute("redirectTo", "1");			
		}
		return "internaltransfer";
	}
	
}
