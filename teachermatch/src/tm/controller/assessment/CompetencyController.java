package tm.controller.assessment;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.user.UserMaster;
import tm.dao.master.CompetencyMasterDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;

/* @Author: Gagan
 * @Discription: Competency Controller.
 */
@Controller
public class CompetencyController
{
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) 
	{
		this.domainMasterDAO = domainMasterDAO;
	}

	@Autowired
	private CompetencyMasterDAO competencyMasterDAO;

	public void setCompetencyMasterDAO(CompetencyMasterDAO competencyMasterDAO)
	{
		this.competencyMasterDAO = competencyMasterDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	/*============ Get Method of Competency Controller ====================*/
	@RequestMapping(value="/competency.do", method=RequestMethod.GET)
	public String competencyGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println(" Get Method of Competency Controller");
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,18,"competency.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("roleAccess="+roleAccess);
			map.addAttribute("roleAccess", roleAccess);
			/*=== Sending values to dropdown from domainmaster Table ===*/
			Criterion criterion = Restrictions.eq("status", "A");

			List<DomainMaster> beans = domainMasterDAO.findByCriteria(Order.asc("domainName"),criterion);
			map.addAttribute("beans", beans);			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "competency";
	}
	/*============End Of Get Method of Competency Controller ====================*/
	
}
