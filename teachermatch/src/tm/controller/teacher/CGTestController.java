package tm.controller.teacher;

import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.LinkedList;


import java.util.List;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.cgreport.RawDataForDomain;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.SchoolMaster;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.services.report.CGReportService;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

@Controller
public class CGTestController {
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}

	@Autowired
	private CGReportService cGReportService;
	public void setcGReportService(CGReportService cGReportService) {
		this.cGReportService = cGReportService;
	}

	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;
	public void setRawDataForDomainDAO(RawDataForDomainDAO rawDataForDomainDAO) {
		this.rawDataForDomainDAO = rawDataForDomainDAO;
	}


	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private DomainMasterDAO domainMasterDAO;
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) {
		this.domainMasterDAO = domainMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@RequestMapping(value="testcg.do", method=RequestMethod.GET)
	public String testCG(HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("testcg.do"+cGReportService);
		try{
			PrintWriter out = response.getWriter();			
			response.setContentType("text/html");			
			//List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findWithLimit(null, 0, 10000);			
			TeacherDetail t1 = teacherDetailDAO.findById(new Integer("1"), false, false);
			TeacherDetail t2 = teacherDetailDAO.findById(new Integer("2"), false, false);
			List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();
			lstTeacherDetails.add(t1);
			lstTeacherDetails.add(t2);

			//cGReportService.saveRawDataOfTeacher(lstTeacherDetails, null);

			out.println("CG");			
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}


	@RequestMapping(value="testcgcal.do", method=RequestMethod.GET)
	public String testCalculation(HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("testcg.do");
		try{
			PrintWriter out = response.getWriter();			
			response.setContentType("text/html");
			DomainMaster dm = domainMasterDAO.findById(new Integer(2), false, false); 
			List<Integer> lstScore = new ArrayList<Integer>();
			lstScore.add(20);
			lstScore.add(10);
			lstScore.add(10);
			lstScore.add(18);			
			//cGReportService.insertOrUpdatePercentile(lstScore, dm);
			out.println("CG Calculation Test");			
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping(value="testcgcaljob.do", method=RequestMethod.GET)
	public String testCalculationForJob(HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("testcgcaljob.do");
		try{
			PrintWriter out = response.getWriter();			
			response.setContentType("text/html");		

			DomainMaster dm = domainMasterDAO.findById(new Integer(3), false, false); 
			JobOrder jobOrder = jobOrderDAO.findById(new Integer(240), false, false);

			List<Integer> lstScore = new ArrayList<Integer>();
			lstScore.add(12);
			lstScore.add(88);
			lstScore.add(7);
			lstScore.add(67);			
			//cGReportService.insertOrUpdatePercentileByJob(lstScore, dm, jobOrder);

			out.println("CG Calculation Test by job ...");			
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	//First url, run only once after uploading CG view code
	@RequestMapping(value="updateRawDataWithNationalPercentile.do", method=RequestMethod.GET)
	public String updateRawData(HttpServletRequest request, HttpServletResponse response)
	{
		/* This controller will populate data in three table
		 * 1. rawdatafordomain
		 * 2. rawdataforcompetency
		 * 3. percentilecalculation
		 */

		System.out.println("testcgcaljob.do");
		try{
			PrintWriter out = response.getWriter();
			out.println("updaterawdata.do");
			out.println("start");
			response.setContentType("text/html");
			//cGReportService.updateRawDataFirstTime();						
			out.println("End");						
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}		

	//second url, run only once after uploading CG view code 
	@RequestMapping(value="updatepercentilebyjob.do", method=RequestMethod.GET)
	public String updateJobPercentile(HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("Update Job wise percentile ");
		try{
			PrintWriter out = response.getWriter();
			out.println("updaterawdata.do");
			out.println("start");
			response.setContentType("text/html");
			cGReportService.updateJobWisePercentileOneTime();
			out.println("End");
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	//second url, run only once after uploading CG view code 
	@RequestMapping(value="updatepercentilecompetencybyjob.do", method=RequestMethod.GET)
	public String updateJobPercentileCompetency(HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("Update Job wise percentile ");
		try{
			PrintWriter out = response.getWriter();
			out.println("updaterawdata.do");
			out.println("start");
			response.setContentType("text/html");
			List<Integer> lstIntegers = new LinkedList<Integer>();
			lstIntegers.add(12);
			lstIntegers.add(10);
			lstIntegers.add(12);
			lstIntegers.add(11);
			//CompetencyMaster cmp = co
			//cGReportService.insertOrUpdatePercentileCompetencyWise();
			out.println("End");
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value="zscore.do", method=RequestMethod.GET)
	public String populateZScore(HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("testcg.do");
		try{
			PrintWriter out = response.getWriter();			
			response.setContentType("text/html");		

			cGReportService.populateZScore();
			out.println("CG Calculation Test zScore");
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value="decile.do", method=RequestMethod.GET)
	public String decile(HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("testcg.do");
		try{
			PrintWriter out = response.getWriter();			
			response.setContentType("text/html");	
			Criterion criterion1 = Restrictions.gt("teacherId", 18837);
			List<TeacherDetail> t1 =teacherDetailDAO.findByCriteria(criterion1);
			for(TeacherDetail teacherDetail: t1){
				//System.out.println(teacherDetail.getEmailAddress());
			}


		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/policiesandprocedures.do", method=RequestMethod.GET)
	public String doPoliciesAndProcedures(ModelMap map,HttpServletRequest request)
	{
		return "policiesandprocedures";
	}	
}
