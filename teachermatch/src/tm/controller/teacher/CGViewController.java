package tm.controller.teacher;

import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.cgreport.PercentileCalculation;
import tm.bean.cgreport.PercentileNationalCompositeTScore;
import tm.bean.cgreport.RawDataForDomain;
import tm.dao.cgreport.PercentileCalculationDAO;
import tm.dao.cgreport.PercentileNationalCompositeTScoreDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.services.UpdateCGThread;
import tm.services.report.CGReportService;
import tm.utility.Utility;

@Controller
public class CGViewController {
	@Autowired
	private PercentileNationalCompositeTScoreDAO percentileNationalCompositeTScoreDAO;
	public void setPercentileNationalCompositeTScoreDAO(PercentileNationalCompositeTScoreDAO percentileNationalCompositeTScoreDAO) {
		this.percentileNationalCompositeTScoreDAO = percentileNationalCompositeTScoreDAO;
	}
	
	@Autowired
	private CGReportService cGReportService;
	public void setcGReportService(CGReportService cGReportService) {
		this.cGReportService = cGReportService;
	}
	
	@Autowired
	private PercentileCalculationDAO percentileCalculationDAO;
	public void setPercentileCalculationDAO(PercentileCalculationDAO percentileCalculationDAO){
		this.percentileCalculationDAO = percentileCalculationDAO;
	}
	
	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;
	public void setRawDataForDomainDAO(RawDataForDomainDAO rawDataForDomainDAO) {
		this.rawDataForDomainDAO = rawDataForDomainDAO;
	}
	
	@RequestMapping(value="updatecggriddata.do", method=RequestMethod.GET)
	public String updateCGGridData(HttpServletRequest request, HttpServletResponse response){	
		return "updatecggriddata";
	}
	
	@RequestMapping(value="/tmwait.do", method=RequestMethod.GET)
	public String doTmWait(ModelMap map,HttpServletRequest request)
	{	
		System.out.println(":::::::::::::::::::::::tmwait.do::::::::::::::::::::");
		return "tmwait";
	}
	@RequestMapping(value="updatecgbyuser.do", method=RequestMethod.GET)
	public String updateCGGridByUser(HttpServletRequest request, HttpServletResponse response){	
		try {
			System.out.println(":::::::::::: updatecgbyuser.do::::::::::::::::::");
			Date cgUpdatedDate = Utility.cgUpdatedDate;
			
			boolean dateCompareFlag = false;
			if(cgUpdatedDate!=null)
			{
				long diff = new Date().getTime() - cgUpdatedDate.getTime();
				 
				//long diffSeconds = diff / 1000 % 60;
				long diffMinutes = diff / (60 * 1000) % 60;
				if(diffMinutes>10)
				{
					dateCompareFlag=true;
				}
			}
			
			if(Utility.isCgUpdated==false || dateCompareFlag)
			{
				// Acquire the CG flag 
				Utility.isCgUpdated=true;
				Utility.cgUpdatedDate=new Date();
				
				response.setContentType("application/octet-stream");
				UpdateCGThread updateCGThread = new UpdateCGThread();
				updateCGThread.setcGReportService(cGReportService);
				updateCGThread.setPercentileCalculationDAO(percentileCalculationDAO);
				updateCGThread.setPercentileNationalCompositeTScoreDAO(percentileNationalCompositeTScoreDAO);
				updateCGThread.setRawDataForDomainDAO(rawDataForDomainDAO);
				
				updateCGThread.start();
				
			}else
			{
				PrintWriter out = response.getWriter();
				
				out.println("CG is currently updating. It may take atleast 10 minutes. Please wait and try again.");
				return "redirect:tmwait.do";
			}
			//////////////////////////////////////
			
			
			//run();
		}
		catch (Exception e){
			e.printStackTrace();
		}		
		return null;
	}
	
	public void run()
	{/*
		try {
			
			cGReportService.updateRawDataByUser();
			try {
				System.gc();				
			}catch (Exception e){
				e.printStackTrace();
			}
			
			try {
				percentileNationalCompositeTScoreDAO.truncate();			
				System.out.println(":::::::::::updateCgDataForNewApplyJob.do::::::::::::::::::::");

				Map<String, PercentileCalculation> mapNationalPercentile = new HashMap<String, PercentileCalculation>();
				List<PercentileCalculation> lstPercentileCalculation = percentileCalculationDAO.findAllInCurrenctYear();
				for(PercentileCalculation percentileCalculation : lstPercentileCalculation){
					mapNationalPercentile.put(percentileCalculation.getDomainMaster().getDomainId()+"##"+percentileCalculation.getScore(), percentileCalculation);
				}			
				List<RawDataForDomain> lstRawDataForDomains = rawDataForDomainDAO.findAllInCurrentYear();
				
				Collections.sort(lstRawDataForDomains,RawDataForDomain.comparatorRawDataForDomainByTeacherId );
				
				PercentileNationalCompositeTScore percentileNationalCompositeTScore = null;
				List<Double> lstCompositTScore = new LinkedList<Double>();
				PercentileCalculation percentileCalculation = null;
				double compositeTScore = 0.00;
				Integer teacherIdTemp = lstRawDataForDomains.get(0).getTeacherDetail().getTeacherId();
				DecimalFormat oneDForm = new DecimalFormat("###,###.0");
				for(RawDataForDomain rawDataForDomain : lstRawDataForDomains){				
					if(!rawDataForDomain.getTeacherDetail().getTeacherId().equals(teacherIdTemp)){					
						lstCompositTScore.add(Double.valueOf(oneDForm.format(compositeTScore)));
						teacherIdTemp=rawDataForDomain.getTeacherDetail().getTeacherId();					
						compositeTScore = 0.0;
					}
					percentileCalculation = mapNationalPercentile.get(rawDataForDomain.getDomainMaster().getDomainId()+"##"+Math.round(rawDataForDomain.getScore()));
					compositeTScore = compositeTScore + percentileCalculation.gettValue()*percentileCalculation.getDomainMaster().getMultiplier();				
				}			
				lstCompositTScore.add(Double.valueOf(oneDForm.format(compositeTScore)));
				
				cGReportService.insertorUpdateCompositeTValue(lstCompositTScore);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				System.gc();				
			} catch (Exception e) {
				e.printStackTrace();
			}
			//cGReportService.updateJobPercentileByUserOrThread();		
		}
		catch (Exception e){
			e.printStackTrace();
		}		
	*/}
	
	//First url to update domain recore
	@RequestMapping(value="updateCgData.do", method=RequestMethod.GET)
	public String updateCGData(HttpServletRequest request, HttpServletResponse response){
		System.out.println("updateCgData.do");
		try{
			PrintWriter out = response.getWriter();
			out.println("updaterawdata.do");
			out.println("start");
			response.setContentType("text/html");
			//cGReportService.updateRawDataByUser();
			out.println("End");
		} 
		catch (Exception e){
			e.printStackTrace();
		}		
		return null;
	}
	
	
	
	
	//Second url to update domain record
	@RequestMapping(value="updateCgDataForNewApplyJob.do", method=RequestMethod.GET)
	public String updateCGDataForNewApplyJob(HttpServletRequest request, HttpServletResponse response){
		System.out.println("updateCgDataForNewApplyJob.do");
		try{
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");			
			out.println("updateCgDataForNewApplyJob.do");
			out.println("start");			
			//cGReportService.updateJobPercentileByUserOrThread();
			out.println("End");
		} 
		catch (Exception e){
			e.printStackTrace();
		}		
		return null;
	}
	
	
	// Third url to update domain record
	@RequestMapping(value="updateCandidateCompositeScore.do", method=RequestMethod.GET)
	public String updateCandidateCompositeScore(HttpServletRequest request, HttpServletResponse response){		
		try {
			/*percentileNationalCompositeTScoreDAO.truncate();			
			System.out.println("updateCgDataForNewApplyJob.do");
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			out.println("start");

			Map<String, PercentileCalculation> mapNationalPercentile = new HashMap<String, PercentileCalculation>();
			List<PercentileCalculation> lstPercentileCalculation = percentileCalculationDAO.findAllInCurrenctYear();
			for(PercentileCalculation percentileCalculation : lstPercentileCalculation){
				mapNationalPercentile.put(percentileCalculation.getDomainMaster().getDomainId()+"##"+percentileCalculation.getScore(), percentileCalculation);
			}			
			List<RawDataForDomain> lstRawDataForDomains = rawDataForDomainDAO.findAllInCurrentYear();
			
			Collections.sort(lstRawDataForDomains,RawDataForDomain.comparatorRawDataForDomainByTeacherId );
			
			PercentileNationalCompositeTScore percentileNationalCompositeTScore = null;
			List<Double> lstCompositTScore = new LinkedList<Double>();
			PercentileCalculation percentileCalculation = null;
			double compositeTScore = 0.00;
			Integer teacherIdTemp = lstRawDataForDomains.get(0).getTeacherDetail().getTeacherId();
			DecimalFormat oneDForm = new DecimalFormat("###,###.0");
			for(RawDataForDomain rawDataForDomain : lstRawDataForDomains){				
				if(!rawDataForDomain.getTeacherDetail().getTeacherId().equals(teacherIdTemp)){					
					lstCompositTScore.add(Double.valueOf(oneDForm.format(compositeTScore)));
					teacherIdTemp=rawDataForDomain.getTeacherDetail().getTeacherId();					
					compositeTScore = 0.0;
				}
				percentileCalculation = mapNationalPercentile.get(rawDataForDomain.getDomainMaster().getDomainId()+"##"+Math.round(rawDataForDomain.getScore()));
				compositeTScore = compositeTScore + percentileCalculation.gettValue()*percentileCalculation.getDomainMaster().getMultiplier();				
			}			
			lstCompositTScore.add(Double.valueOf(oneDForm.format(compositeTScore)));
			
			cGReportService.insertorUpdateCompositeTValue(lstCompositTScore);
			
			out.println("End");*/
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@RequestMapping(value="cgview.do", method=RequestMethod.GET)
	public String cgView(HttpServletRequest request, HttpServletResponse response){
		return "cgview";
	}
	//Second url to update domain record
	/*	@RequestMapping(value="updateCandidateCompositeScore.do", method=RequestMethod.GET)
		public String updateCandidateCompositeScore(HttpServletRequest request, HttpServletResponse response){
			
			try {
				System.out.println("updateCgDataForNewApplyJob.do");
				PrintWriter out = response.getWriter();
				response.setContentType("text/html");
				out.println("start");
				
				List lstRawData = rawDataForDomainDAO.findTeacherCompositeScore();
				List<Integer> lstScore = new ArrayList<Integer>();
				TeacherDetail tDetail =null;
				Double dScore = 0.0;
				for(Object oo: lstRawData){
					Object obj[] = (Object[])oo;
					tDetail = (TeacherDetail) obj[0];
					dScore = new Double(""+obj[1]);
					lstScore.add(new Integer(""+Math.round(dScore)));
					System.out.println(""+tDetail.getTeacherId()+":>"+obj[1]);
				}						
				cGReportService.insertOrUpdatePercentileComposite(lstScore);									
				out.println("End");
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}*/
}
