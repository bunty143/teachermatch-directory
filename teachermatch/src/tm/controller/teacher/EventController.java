package tm.controller.teacher;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.TeacherDetail;
import tm.utility.Utility;
 
@Controller
public class EventController {
	@RequestMapping(value="/slotselection.do", method=RequestMethod.GET)
	public String slotSelection(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{		
		HttpSession session = request.getSession(false);
		int eventId=0;
		String id=request.getParameter("id");
		System.out.println(id);
		String encId = id.trim();
		try
		{				

			if (session == null || session.getAttribute("teacherDetail") == null) 
			{
				Cookie[] cookies = request.getCookies();
				for(int i=0;i<cookies.length;i++)
				{
					if(cookies[i].getName().equals("slotUrl"))
					{
						System.out.println("Cookie already exists");
						cookies[i].setValue("");
					}
				}
				Cookie cookie = new Cookie ("slotUrl",id);
				cookie.setMaxAge(1 * 60 * 60);
				response.addCookie(cookie);	    
				System.out.println("cookie set for slot url :: "+id);
				return "redirect:index.jsp";
			}
			else
			{
				Cookie[] cookies = request.getCookies();
				for(int i=0;i<cookies.length;i++)
				{
					if(cookies[i].getName().equals("slotUrl"))
					{
						System.out.println("Cookie already exists");
						Cookie cookie2 = cookies[i];
						cookie2.setValue(null);
						cookie2.setMaxAge(0);
			            response.addCookie(cookie2);					
					}
				}


				String forMated = Utility.decodeBase64(encId);
				String emailAddress="";
				System.out.println("forMateddddddddddd "+forMated);
				if(forMated.contains("###")){
					System.out.println("ddd");
					String arr[] = forMated.split("###");
					eventId=Integer.parseInt(arr[0]) ;
					emailAddress=arr[1];					
					System.out.println(emailAddress);
				}
				
				System.out.println("$$$$$$$$$$$$$$ :: "+emailAddress);
				TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");		
				map.addAttribute("teacherId", teacherDetail.getTeacherId()); 
				map.addAttribute("eventId", eventId);
				map.addAttribute("email", emailAddress); 


			}
		}

		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "slotselection";

	}
}