package tm.controller.master;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.MasterSubjectMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

@Controller
public class ManageApplicantReportController 
{
	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO; 
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
	{
		this.districtMasterDAO = districtMasterDAO;
	}
		
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired 
	private SubjectMasterDAO subjectMasterDAO;
	public void setSubjectMasterDAO(SubjectMasterDAO subjectMasterDAO) {
		this.subjectMasterDAO = subjectMasterDAO;
	}
	
	@Autowired
	private MasterSubjectMasterDAO masterSubjectMasterDAO;
	public void setMasterSubjectMasterDAO(
			MasterSubjectMasterDAO masterSubjectMasterDAO) {
		this.masterSubjectMasterDAO = masterSubjectMasterDAO;
	}
	
	
	@RequestMapping(value="/applicantReport.do", method=RequestMethod.GET)
	public String manageDistrictGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			UserMaster userMaster=null;
			DistrictMaster districtMaster=null;
			
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			String jobAuthKey=(Utility.randomString(8)+Utility.getDateTime());
			map.addAttribute("jobAuthKey",jobAuthKey);

			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,24,"managejoborders.do",0);
			}catch(Exception e){
				//e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			
			boolean miamiShowFlag=true;
			try{
				if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
					miamiShowFlag=false;
				}
			}catch(Exception e){}		
			map.addAttribute("miamiShowFlag", miamiShowFlag);
			if(userMaster.getEntityType()!=1){
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictOrSchoolName",null);
			}
			map.addAttribute("JobOrderType","2");
			if(userMaster.getEntityType()==3){
				districtMaster=districtMasterDAO.findById(userMaster.getDistrictId().getDistrictId(),false, false);

				map.addAttribute("writePrivilegFlag",districtMaster.getWritePrivilegeToSchool());
				map.addAttribute("schoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("schoolId",userMaster.getSchoolId().getSchoolId());
				
				map.addAttribute("addJobFlag",false);
			}else {
				map.addAttribute("addJobFlag",true);
			}
			try{
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in District Job Order");
			}catch(Exception e){
				//e.printStackTrace();
			
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "applicantreport";
	}
	
}
