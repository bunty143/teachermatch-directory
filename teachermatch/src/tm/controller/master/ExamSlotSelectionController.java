package tm.controller.master;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tm.bean.TeacherDetail;
import tm.utility.Utility;

@Controller
public class ExamSlotSelectionController 
{
	@RequestMapping(value="/examslotselection.do", method=RequestMethod.GET)
	public String getExamSlotSelection(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		HttpSession session = request.getSession(false);
		int districtAssessmentId=0;
		int jobId = 0;
		String id=request.getParameter("id");
		String encId = "";
		if(id!=null && id.length()>0)
		{
			encId = id.trim();	
		}
		
		try
		{				

			if (session == null || session.getAttribute("teacherDetail") == null) 
			{
				Cookie[] cookies = request.getCookies();
				for(int i=0;i<cookies.length;i++)
				{
					if(cookies[i].getName().equals("examSlotUrl"))
					{
						System.out.println("Cookie already exists");
						cookies[i].setValue("");
					}
				}
				Cookie cookie = new Cookie ("examSlotUrl",id);
				cookie.setMaxAge(1 * 60 * 60);
				response.addCookie(cookie);	    
				System.out.println("cookie set for exam slot url :: "+id);
				return "redirect:index.jsp";
			}
			else
			{
				Cookie[] cookies = request.getCookies();
				for(int i=0;i<cookies.length;i++)
				{
					if(cookies[i].getName().equals("examSlotUrl"))
					{
						System.out.println("Cookie already exists");
						Cookie cookie2 = cookies[i];
						cookie2.setValue(null);
						cookie2.setMaxAge(0);
			            response.addCookie(cookie2);					
					}
				}
				String forMated = Utility.decodeBase64(encId);
				String emailAddress="";
				System.out.println("formatted exam slot url :: "+forMated);
				if(forMated.contains("###")){
					System.out.println("ddd");
					String arr[] = forMated.split("###");
					emailAddress=arr[0] ;
					districtAssessmentId=Integer.parseInt(arr[1]);
					jobId=Integer.parseInt(arr[2]);	
					System.out.println(emailAddress);
				}
				System.out.println(" jobId :: "+jobId);
				TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");		
				map.addAttribute("teacherId", teacherDetail.getTeacherId()); 
				map.addAttribute("emailAddress", emailAddress);
				map.addAttribute("districtAssessmentId", districtAssessmentId);
				map.addAttribute("jobId", jobId); 
			}
		}

		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "examslotselection";
	}
	
	@RequestMapping(value="/myassessments.do", method=RequestMethod.GET)
	public String getMyAssessments(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		try {

			HttpSession session = request.getSession();			
			if (session == null || session.getAttribute("teacherDetail")==null) {				
						
				return "redirect:signin.do";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "myassessments";
	}
}
