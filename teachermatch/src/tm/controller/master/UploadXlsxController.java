package tm.controller.master;
/* @Author: Sekhar  
 * @Discription: view of Upload Controller.
 */
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.JobOrder;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherUploadTempDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.Utility;


@Controller
public class UploadXlsxController 
{
	
	static String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO)
	{
		this.cityMasterDAO = cityMasterDAO;
	}
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO){
		this.teacherDetailDAO = teacherDetailDAO;
	}
    private Vector vectorDataExcelXLSX = new Vector();
    
    @Autowired
	private TeacherUploadTempDAO teacherUploadTempDAO;
	public void setTeacherUploadTempDAO(TeacherUploadTempDAO teacherUploadTempDAO)
	{
		this.teacherUploadTempDAO = teacherUploadTempDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
    @RequestMapping(value="/importcandidatedetails.do", method=RequestMethod.GET)
	public String teacherjobuploadGET(ModelMap map,HttpServletRequest request)
	{
		try{
			System.out.println("\n =========== teacherjobupload Controller ===============");
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster userMaster =null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getEntityType()!=1)
				{
				 if(userMaster.getDistrictId()!=null)
				 {
				      map.addAttribute("districtId", userMaster.getDistrictId().getDistrictId());
				      map.addAttribute("entityType", userMaster.getEntityType());
				 }
				 else if(userMaster.getEntityType()==5)                                   // ******** Adding by deepak  ***********************
				 {
					 map.addAttribute("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
				      map.addAttribute("entityType", userMaster.getEntityType()); 
				 }
				 else if(userMaster.getEntityType()==6)
				 {
					 map.addAttribute("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
					 map.addAttribute("branchId", userMaster.getBranchMaster().getBranchId());
				      map.addAttribute("entityType", userMaster.getEntityType());  
				 }
				  //************* End   ************************
				}else{
					 map.addAttribute("entityType", userMaster.getEntityType());  
				}
				
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,47,"importcandidatedetails.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			
		   JobOrder jobOrder=null;
		   String jobId =request.getParameter("jobId")==null?"":request.getParameter("jobId");
		   String job_Id_discription =jobId==""?""+Utility.getLocaleValuePropByKey("lblJoId", locale)+"":"";
		   
		   if(jobId!="" && jobId!=null){
			    jobOrder = jobOrderDAO.findById(Integer.valueOf(jobId),false,false);
			    if(!jobOrder.getIsInviteOnly())
			    	return "redirect:managejoborders.do";
		   }
		   
		   map.addAttribute("job_Id_discription",job_Id_discription);
		   map.addAttribute("jobId", jobId);
		}catch (Exception e){
			e.printStackTrace();
		}
		return "teacherjobupload";
	}
    @RequestMapping(value="/candidatetemplist.do", method=RequestMethod.GET)
	public String teacherTempListGET(ModelMap map,HttpServletRequest request)
	{
		try{
			System.out.println("\n=@@@@@@@@@@@@@@= teacherjobupload Controller ======@@@@@@@@==== ");
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster userMaster =null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,47,"importcandidatedetails.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			boolean  invitejobflag =false;
			boolean hqbrflag=false;
			JobOrder jobOrder=null;
			String jobId=request.getParameter("invitejobId");
			if(jobId!=null && jobId!="" &&jobId.length()>0){
				jobOrder = jobOrderDAO.findById(Integer.valueOf(jobId), false, false);
				if(jobOrder!=null){
					if(jobOrder.getHeadQuarterMaster()!=null){
						hqbrflag=true;
					}
					if(jobOrder.getIsInviteOnly()){
						invitejobflag=true;
					}
				}
			}
			
			map.addAttribute("hqbrflag",hqbrflag);
			map.addAttribute("invitejobflag",invitejobflag);
			map.addAttribute("jobOrder",jobOrder);
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("sessionIdTxt", session.getId());
		}catch (Exception e){
			e.printStackTrace();
		}
		return "teachertemplist";
	}
    
    @RequestMapping(value="/importjobdetails.do", method=RequestMethod.GET)
	public String jobuploadGET(ModelMap map,HttpServletRequest request)
	{
		try{
			System.out.println("\n =========== teacherjobupload Controller ===============");
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster userMaster =null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,48,"importjobdetails.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
		}catch (Exception e){
			e.printStackTrace();
		}
		return "jobupload";
	}
    @RequestMapping(value="/jobtemplist.do", method=RequestMethod.GET)
	public String jobTempListGET(ModelMap map,HttpServletRequest request)
	{
		try{
			System.out.println("\n =========== teacherjobupload Controller ===============");
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster userMaster =null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,48,"importjobdetails.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
		}catch (Exception e){
			e.printStackTrace();
		}
		return "jobtemplist";
	}
    
    @RequestMapping(value="/importquestion.do", method=RequestMethod.GET)
	public String importquestionGET(ModelMap map,HttpServletRequest request)
	{
		try{
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster userMaster =null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "redirect:index.jsp";
			} else {
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null) {
					roleId	=	userMaster.getRoleId().getRoleId();
				}
			}
			String assessmentId	=	(String) request.getParameter("assessmentId");
			String sectionId	=	(String) request.getParameter("sectionId");
			String roleAccess=null;
			try{
				roleAccess	=	roleAccessPermissionDAO.getMenuOptionList(roleId,47,"importcandidatedetails.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("assessmentId", assessmentId);
			map.addAttribute("sectionId", sectionId);
	
		}catch (Exception e){
			e.printStackTrace();
		}
		return "importquestion";
	}
    
    @RequestMapping(value="/questiontemplist.do", method=RequestMethod.GET)
	public String questionTempListGET(ModelMap map,HttpServletRequest request)
	{
		try{
			System.out.println("\n =========== Question Controller ===============");
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster userMaster =null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			/*String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,47,"importcandidatedetails.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("roleAccess"+roleAccess);
			map.addAttribute("roleAccess", roleAccess);*/
			String assessmentId	=	(String) request.getParameter("assessmentId");
			String sectionId	=	(String) request.getParameter("sectionId");
			map.addAttribute("sessionIdTxt", session.getId());
			map.addAttribute("assessmentId", assessmentId);
			map.addAttribute("sectionId", sectionId);
			System.out.println(":::::::::::::::::::::::::::::::::::::::::::");
		}catch (Exception e){
			e.printStackTrace();
		}
		return "questiontemplist";
	}
    @RequestMapping(value="/linktoksntalents.do", method=RequestMethod.GET)
	public String linktoksnuploadGET(ModelMap map,HttpServletRequest request)
	{
		try{
			System.out.println("\n =========== linktoksntalents.do Controller ===============");
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster userMaster =null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getEntityType()!=1)
				{
				 if(userMaster.getDistrictId()!=null)
				 {
				      map.addAttribute("districtId", userMaster.getDistrictId().getDistrictId());
				      map.addAttribute("entityType", userMaster.getEntityType());
				 }
				 else if(userMaster.getEntityType()==5)                                   // ******** Adding by deepak  ***********************
				 {
					 map.addAttribute("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
				      map.addAttribute("entityType", userMaster.getEntityType()); 
				 }
				 else if(userMaster.getEntityType()==6)
				 {
					 map.addAttribute("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
					 map.addAttribute("branchId", userMaster.getBranchMaster().getBranchId());
				      map.addAttribute("entityType", userMaster.getEntityType());  
				 }
				  //************* End   ************************
				}else{
					 map.addAttribute("entityType", userMaster.getEntityType());  
				}
				
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		return "linktoksntalents";
	}
    @RequestMapping(value="/linktoksntalentslist.do", method=RequestMethod.GET)
	public String linkToKsnTalentsListGET(ModelMap map,HttpServletRequest request)
	{
		try{
			System.out.println("=============== linktoksntalentslist.do Controller ======@@@@@@@@==== ");
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster userMaster =null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			boolean hqbrflag=false;
			if(userMaster.getHeadQuarterMaster()!=null){
				hqbrflag=true;
			}
			map.addAttribute("hqbrflag",hqbrflag);
			map.addAttribute("sessionIdTxt", session.getId());
		}catch (Exception e){
			e.printStackTrace();
		}
		return "linktoksntalentslist";
	}
}
