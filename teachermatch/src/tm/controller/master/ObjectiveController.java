package tm.controller.master;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.user.UserMaster;
import tm.dao.master.CompetencyMasterDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;

@Controller
public class ObjectiveController 
{
	@Autowired
	private CompetencyMasterDAO competencyMasterDAO;

	public void setCompetencyMasterDAO(CompetencyMasterDAO competencyMasterDAO)
	{
		this.competencyMasterDAO = competencyMasterDAO;
	}
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) 
	{
		this.domainMasterDAO = domainMasterDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	/*============ Get Method of Objective Controller ====================*/
	@RequestMapping(value="/objective.do", method=RequestMethod.GET)
	public String objectiveGET(ModelMap map,HttpServletRequest request)
	{
		DomainMaster domainMaster	=	null;
		try 
		{
			System.out.println(" Get Method of Competency Controller");
			
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,19,"objective.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("roleAccess="+roleAccess);
			map.addAttribute("roleAccess", roleAccess);
			
			/*=== Sending values to dropdown from domainmaster Table ===*/
			Criterion criterion = Restrictions.eq("status", "A");

			List<DomainMaster> domainMasters = domainMasterDAO.findByCriteria(Order.asc("domainName"),criterion);
			map.addAttribute("domainMasters", domainMasters);	
			
			//List<CompetencyMaster> competencyMasters	= competencyMasterDAO.getCompetencesByDomain(domainMaster);
			//map.addAttribute("competencyMasters", competencyMasters);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "objective";
	}
	/*============End Of Get Method of Competency Controller ====================*/
	
}
