package tm.controller.master;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.master.StatusMaster;
import tm.bean.teacher.SpInboundAPICallRecord;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.teacher.SpInboundAPICallRecordDAO;
import tm.utility.Utility;

@Controller
public class SpTestController 
{
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	
	@Autowired
	private SpInboundAPICallRecordDAO spInboundAPICallRecord;
	
	@RequestMapping(value="/sp.do", method=RequestMethod.GET)
	public String spGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println("spGET Controller");
		try 
		{
			
			StringBuffer sb=new StringBuffer();
			sb.append("<BR>email,bcode,p<BR>");
			
			String password=request.getParameter("p");
			if(password!=null && !password.equals("") && password.equalsIgnoreCase("4G"))
			{
				String email=request.getParameter("email");
				String bcode=request.getParameter("bcode");
				
				TeacherDetail teacherDetail=null;
				if(email!=null && !email.equals(""))
				{
					List<TeacherDetail> lstTeacherDetails=teacherDetailDAO.findByEmail(email);
					if(lstTeacherDetails!=null && lstTeacherDetails.size()==1)
					{
						teacherDetail=lstTeacherDetails.get(0);
					}
				}
				
				sb.append("<p>Teacher Details");
				if(teacherDetail!=null)
				{
					sb.append("<BR><table>");
					sb.append("<tr>");
					sb.append("<td>teacherId</td>");
					sb.append("<td>firstName</td>");
					sb.append("<td>lastName</td>");
					sb.append("<td>emailAddress</td>");
					sb.append("<td>smartPracticesCandidate</td>");
					sb.append("<td>associatedBranchId</td>");
					sb.append("<td>forgetCounter</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td>"+teacherDetail.getTeacherId()+"</td>");
					sb.append("<td>"+teacherDetail.getFirstName()+"</td>");
					sb.append("<td>"+teacherDetail.getLastName()+"</td>");
					sb.append("<td>"+teacherDetail.getEmailAddress()+"</td>");
					sb.append("<td>"+teacherDetail.getSmartPracticesCandidate()+"</td>");
					if(teacherDetail.getAssociatedBranchId()!=null)
						sb.append("<td>"+teacherDetail.getAssociatedBranchId()+","+teacherDetail.getAssociatedBranchId().getBranchBusinessName()+"</td>");
					else
						sb.append("<td>N/A</td>");
					
					if(teacherDetail.getForgetCounter()!=null)
						sb.append("<td>"+teacherDetail.getForgetCounter()+"</td>");
					else
						sb.append("<td>N/A</td>");
					
					sb.append("</tr>");
					
					sb.append("</table><BR>");
					
					Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
					List<TeacherAssessmentStatus> lstTAS=teacherAssessmentStatusDAO.findByCriteria(criterion1);
					if(lstTAS!=null && lstTAS.size()>0)
					{
						sb.append("<p>Teacher Assessment Status Details");
						sb.append("<BR><table>");
						sb.append("<tr>");
						sb.append("<td>teacherId</td>");
						sb.append("<td>teacherAssessmentId</td>");
						sb.append("<td>assessmentId</td>");
						sb.append("<td>assessmentType</td>");
						sb.append("<td>statusId</td>");
						sb.append("<td>pass</td>");
						sb.append("<td>assessmentTakenCount</td>");
						sb.append("</tr>");
						for(TeacherAssessmentStatus tas:lstTAS)
						{
							try {
								sb.append("<tr>");
								sb.append("<td>"+tas.getTeacherDetail().getTeacherId()+"</td>");
								sb.append("<td>"+tas.getTeacherAssessmentdetail().getTeacherAssessmentId()+"</td>");
								sb.append("<td>"+tas.getAssessmentDetail().getAssessmentId()+"</td>");
								sb.append("<td>"+tas.getAssessmentType()+"</td>");
								sb.append("<td>"+tas.getStatusMaster().getStatusId()+"</td>");
								sb.append("<td>"+tas.getPass()+"</td>");
								sb.append("<td>"+tas.getAssessmentTakenCount()+"</td>");
								sb.append("</tr>");
							} catch (Exception e) {
								// TODO: handle exception
							}
							
						}
						sb.append("</table><BR>");
					}
					
					List<SpInboundAPICallRecord> lstsicr=spInboundAPICallRecord.findByCriteria(criterion1);
					if(lstsicr!=null && lstsicr.size()>0)
					{
						sb.append("<p>SpInboundAPICallRecord Details");
						sb.append("<BR><table>");
						sb.append("<tr>");
						sb.append("<td>spInboundAPICallRecordId</td>");
						sb.append("<td>teacherId</td>");
						sb.append("<td>currentLessonNo</td>");
						sb.append("<td>totalLessons</td>");
						sb.append("<td>lpm</td>");
						sb.append("<td>SQL</td>");
						sb.append("</tr>");
						for(SpInboundAPICallRecord sicr:lstsicr)
						{
							try {
								sb.append("<td>"+sicr.getSpInboundAPICallRecordId()+"</td>");
								sb.append("<td>"+sicr.getTeacherDetail().getTeacherId()+"</td>");
								sb.append("<td>"+sicr.getCurrentLessonNo()+"</td>");
								sb.append("<td>"+sicr.getTotalLessons()+"</td>");
								sb.append("<td>"+sicr.getLpm()+"</td>");
								sb.append("<td>UPDATE `spinboundapicallrecord` SET  `currentLessonNo` =  '39',`lpm` =  'complete02' WHERE `spInboundAPICallRecordId` ="+sicr.getSpInboundAPICallRecordId()+" LIMIT 1 ;</td>");
								sb.append("</tr>");
							} catch (Exception e) {
								// TODO: handle exception
							}
							
						}
						sb.append("</table><BR>");
					}
				}
				
				
				BranchMaster branchMaster=null;
				if(bcode!=null && !bcode.equals(""))
				{
					Criterion criterion1 = Restrictions.eq("branchCode",bcode);
					List<BranchMaster> branchMasters=branchMasterDAO.findByCriteria(criterion1);
					if(branchMasters!=null && branchMasters.size() >0)
					{
						sb.append("<p>Branch Details");
						sb.append("<BR><table>");
						sb.append("<tr>");
						sb.append("<td>branchId</td>");
						sb.append("<td>branchName</td>");
						sb.append("<td>branchCode</td>");
						sb.append("</tr>");
						for(BranchMaster bm:branchMasters)
						{
							sb.append("<tr>");
							sb.append("<td>"+bm.getBranchId()+"</td>");
							sb.append("<td>"+bm.getBranchName()+"</td>");
							sb.append("<td>"+bm.getBranchCode()+"</td>");
							sb.append("</tr>");
						}
						
						if(branchMasters.size()==1)
						{
							branchMaster=branchMasters.get(0);
						}
						sb.append("</table><BR>");
					}
				}
				
				
				if(teacherDetail!=null && branchMaster!=null)
				{
					JobOrder jobOrder=null;
					List<JobOrder> lstJobOrders=jobOrderDAO.getActiveBranchJobs(branchMaster,true);
					if(lstJobOrders!=null && lstJobOrders.size()>0)
					{
						sb.append("<p>Job Order Details");
						sb.append("<BR><table>");
						sb.append("<tr>");
						sb.append("<td>jobId</td>");
						sb.append("<td>branchId</td>");
						sb.append("<td>Base</td>");
						sb.append("</tr>");
						int ijCounter=0;
						boolean base=false;
						for(JobOrder jo:lstJobOrders)
						{
							try {
								if(ijCounter==0)
								{
									jobOrder=jo;
									ijCounter++;
								}
								sb.append("<tr>");
								sb.append("<td>"+jo.getJobId()+"</td>");
								if(jobOrder.getBranchMaster()!=null)
									sb.append("<td>"+jo.getBranchMaster().getBranchId() +" , "+jo.getBranchMaster().getBranchCode()+"</td>");
								else
									sb.append("<td>N/A</td>");
								
								if(jobOrder.getJobCategoryMaster().getBaseStatus()!=null)
								{
									sb.append("<td>"+jobOrder.getJobCategoryMaster().getJobCategoryId()+" , "+jobOrder.getJobCategoryMaster().getBaseStatus()+"</td>");
									if(jobOrder.getJobCategoryMaster().getBaseStatus() && ijCounter==1)
									{
										base=true;
									}
								}
								else
									sb.append("<td>N/A</td>");
								
								sb.append("</tr>");
							} catch (Exception e) {
								// TODO: handle exception
							}
							
						}
						sb.append("</table><BR>");
						sb.append("<HR> Update Teacher Query <BR>");
						sb.append("UPDATE `teacherdetail` SET  `smartPracticesCandidate` =  '1', `associatedBranchId` =  '"+branchMaster.getBranchId()+"' WHERE  `teacherdetail`.`teacherId` ="+teacherDetail.getTeacherId()+" LIMIT 1 ;");
						
						List<JobForTeacher> lstJFT=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrder);
						if(lstJFT!=null && lstJFT.size() >0)
						{
							sb.append("<p>Job For Teacher Details");
							sb.append("<HR><table>");
							sb.append("<tr>");
							sb.append("<td>JobForTeacherId</td>");
							sb.append("<td>TeacherId</td>");
							sb.append("<td>JobId</td>");
							sb.append("<td>Status</td>");
							sb.append("<td>SQL</td>");
							sb.append("</tr>");
							for(JobForTeacher jft:lstJFT)
							{
								try {
									sb.append("<tr>");
									sb.append("<td>"+jft.getJobForTeacherId()+"</td>");
									sb.append("<td>"+jft.getTeacherId().getTeacherId()+"</td>");
									sb.append("<td>"+jft.getJobId().getJobId()+"</td>");
									sb.append("<td>"+jft.getStatus().getStatusId()+"</td>");
									if(jft.getJobId().getJobCategoryMaster()!=null && jft.getJobId().getJobCategoryMaster().getBaseStatus()!=null && jft.getJobId().getJobCategoryMaster().getBaseStatus())
										sb.append("<td>UPDATE `jobforteacher` SET  `status` =  '3', `displayStatusId` =  '3' WHERE `jobForTeacherId` ="+jft.getJobForTeacherId()+" LIMIT 1 ;</td>");
									else
										sb.append("<td>UPDATE `jobforteacher` SET  `status` =  '4', `displayStatusId` =  '4' WHERE `jobForTeacherId` ="+jft.getJobForTeacherId()+" LIMIT 1 ;</td>");
									sb.append("</tr>");
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							sb.append("</table><BR>");
							
							
						}
						if(jobOrder!=null)
						{
							int iStatus=4;
							JobForTeacher jobForTeacher = new JobForTeacher();
							jobForTeacher.setTeacherId(teacherDetail);
							jobForTeacher.setJobId(jobOrder);
							StatusMaster statusMaster=null;
							try{
								int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
								statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
								if(statusMaster!=null){
									iStatus=statusMaster.getStatusId();
								}
							}catch(Exception ex){
								ex.printStackTrace();
							}
							sb.append("<BR> Insert JFT Query <BR>");
							sb.append("INSERT INTO `teachermatch`.`jobforteacher` (`jobForTeacherId`, `districtId`, `teacherId`, `jobId`, `redirectedFromURL`, `status`, `displayStatusId`, `displaySecondaryStatusId`, `internalStatus`, `internalSecondaryStatusId`, `cgUpdated`, `coverLetter`, `isAffilated`, `isImCandidate`, `staffType`, `flagForDistrictSpecificQuestions`, `noteForDistrictSpecificQuestions`, `isDistrictSpecificNoteFinalize`, `districtSpecificNoteFinalizeDate`, `districtSpecificNoteFinalizeBy`, `flagForDspq`, `requisitionNumber`, `updatedBy`, `updatedByEntity`, `updatedDate`, `lastActivity`, `lastActivityDate`, `lastActivityDoneBy`, `hiredBySchool`, `offerReady`, `offerMadeDate`, `offerAccepted`, `offerAcceptedDate`, `noResponseEmail`, `candidateConsideration`, `reminderSentDate`, `noOfReminderSent`, `createdDateTime`, `jobCompleteDate`, `noofTries`)"+
									"VALUES (NULL, NULL, '"+teacherDetail.getTeacherId()+"', '"+jobOrder.getJobId()+"', NULL, '"+iStatus+"', '"+iStatus+"', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '"+Utility.getDateWithoutTime(new Date())+"', NULL, NULL);");
							
						}
						sb.append("<BR> Extra Update JFT Query <BR>");
						sb.append("UPDATE `jobforteacher` SET  `status` =  '4', `displayStatusId` =  '4' WHERE `jobForTeacherId` =1 LIMIT 1 ;<BR>");
						sb.append("UPDATE `jobforteacher` SET  `status` =  '4', `displayStatusId` =  '4', jobId=1 WHERE `jobForTeacherId` =1 LIMIT 1 ;<BR>");
						
					}
				}
			}
			map.addAttribute("spdata",sb.toString());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "sphelp";
	}
	
		
}
