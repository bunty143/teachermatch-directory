package tm.controller.master;

import java.io.File;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.util.URIUtil;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.python.antlr.PythonParser.list_for_return;
import org.python.antlr.PythonParser.print_stmt_return;
import org.python.antlr.runtime.tree.BufferedTreeNodeStream;
import org.python.core.io.IOBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sun.net.www.URLConnection;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.EmployeeMaster;
import tm.bean.SchoolInJobOrder;
import tm.bean.master.DistrictSchools;
import tm.bean.JobRequisitionNumbers;
//import tm.bean.UserUploadFolderAccess;
import tm.bean.master.DistrictMaster;
import tm.bean.JobOrder;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.JobMaster;
import tm.bean.master.PoolMaster;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.master.JobPoolMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.master.JobMasterDAO;
import tm.dao.master.JobPoolMasterDAO;
import tm.dao.master.PoolMasterDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.bean.SchoolInJobOrder;
//import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.dao.JobOrderDAO;
import tm.services.EmailerService;
import tm.utility.Utility;

@Controller
public class JobUploadXlsxController {
	
	String locale = Utility.getValueOfPropByKey("locale");
	  String TM_instructional_vacancies4= Utility.getLocaleValuePropByKey("TM_instructional_vacancies4", locale);
	  String TM_instructional_vacancies5= Utility.getLocaleValuePropByKey("TM_instructional_vacancies5", locale);
	  String msgRejectedTheCandidate10= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate10", locale);
	  String TM_instructional_vacancies7= Utility.getLocaleValuePropByKey("TM_instructional_vacancies7", locale);
	  String TM_instructional_vacancies8= Utility.getLocaleValuePropByKey("TM_instructional_vacancies8", locale);
	  String TM_instructional_vacancies9= Utility.getLocaleValuePropByKey("TM_instructional_vacancies9", locale);
	  String TM_instructional_vacancies10= Utility.getLocaleValuePropByKey("TM_instructional_vacancies10", locale);
	  String msgRejectedTheCandidate11= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate11", locale);
	  String TM_instructional_vacancies15= Utility.getLocaleValuePropByKey("TM_instructional_vacancies15", locale);
	  String msgRejectedTheCandidate13= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate13", locale);
	  String TM_instructional_vacancies17= Utility.getLocaleValuePropByKey("TM_instructional_vacancies17", locale);
	  String msgRejectedTheCandidate14= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate14", locale);
	  String msgRejectedTheCandidate15= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate15", locale);
	  String msgRejectedTheCandidate16= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate16", locale);
	  String msgRejectedTheCandidate17= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate17", locale);
	  String TM_instructional_vacancies18= Utility.getLocaleValuePropByKey("TM_instructional_vacancies18", locale);
	  String TM_instructional_vacancies19= Utility.getLocaleValuePropByKey("TM_instructional_vacancies19", locale);
	  String msgRejectedTheCandidate18= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate18", locale);
	  String msgRejectedTheCandidate19= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate19", locale);
	  String TM_instructional_vacancies20= Utility.getLocaleValuePropByKey("TM_instructional_vacancies20", locale);
	  String msgRejectedTheCandidate20= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate20", locale);
		
	  String msgRejectedTheCandidate12= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate12", locale);
	  
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private JobMasterDAO jobMasterDAO;
	
	@Autowired
	private PoolMasterDAO poolMasterDAO;
	
	@Autowired
	private JobPoolMasterDAO jobpoolMasterDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;

	
	/*
	public void setUserUploadFolderAccessDAO(UserUploadFolderAccessDAO userUploadFolderAccessDAO) {
		this.userUploadFolderAccessDAO = userUploadFolderAccessDAO;
	}

	
		@RequestMapping(value="/jobuploadfolder.do", method=RequestMethod.GET)
		public String getallactivedistrict(ModelMap map,HttpServletRequest request)
		{
			System.out.println("::::: Use jobUploadXlsxController controller :::::");
			
			List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
			
			try {
				Criterion functionality = Restrictions.eq("functionality", "jobUpload");
				Criterion active = Restrictions.eq("status", "A");
				uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			for (UserUploadFolderAccess getRec : uploadFolderAccessesrec) {
				
				Integer districtId = getRec.getDistrictId().getDistrictId();
				ArrayList<File> filesList = findFilesByPath(getRec.getFolderPath(),getRec.getLastUploadDateTime());
				int size = filesList.size();
				
				HttpSession session = request.getSession();
		        for(int i=0;i<size;i++)
		        {
		            try {
						
						String root = Utility.getValueOfPropByKey("teacherRootPath");
						
						System.out.println("root::::::"+root);
						File path = new File(root + "/uploadjob");
						if (!path.exists()) {
							boolean status = path.mkdirs();
						}
						
						String myFullFileName = filesList.get(i).getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						String ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						//String fileName=session.getId()+ext;
						String fileName = UUID.randomUUID().toString()+ext;
						InputStream input = null;
						OutputStream output = null;							
						String originalFilePath = filesList.get(i).getPath();
						File uploadedFile = new File(path+"/"+fileName);
						
						try {
							input = new FileInputStream(originalFilePath);
							output = new FileOutputStream(uploadedFile);
							byte[] buf = new byte[1024];
							int bytesRead;
							while ((bytesRead = input.read(buf)) > 0) {
								output.write(buf, 0, bytesRead);
							}
						} finally {
							input.close();
							output.close();
							//Long districtId =getRec.getDistrictId(); 
							// filesList.get(i).lastModified()
							
							Integer rowId = getRec.getUseuploadFolderId();
							String folderPath = getRec.getFolderPath();
							String nameOfLastFileUploaded = filesList.get(i).getName();
							String lastUploadDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(filesList.get(i).lastModified()));
							
							savejob(fileName,districtId,rowId,folderPath,nameOfLastFileUploaded,lastUploadDateTime);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
		        }
			}
			return null;
		}
		*/
	
	
		public static ArrayList<File> findFilesByPath(String path,Date lastUploadDateTime) {
			String pathToScan = path;
	        String target_file ;
	        File folderToScan = new File(pathToScan);
	        
	        File[] listOfFiles = folderToScan.listFiles();
	        ArrayList<File> modifiedFile1 = new ArrayList(); 
	        String lastDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(lastUploadDateTime);
	        for (int i = 0; i < listOfFiles.length; i++) {
	        	if(!listOfFiles[i].getName().startsWith("~$") && (listOfFiles[i].getName().endsWith(".xls") || listOfFiles[i].getName().endsWith(".xlsx"))){
	           String date2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(listOfFiles[i].lastModified()));
	          
	           int comparison = date2.compareTo(lastDate);
		           if(comparison>0){
		        	   modifiedFile1.add(listOfFiles[i]);
		           }
	        	}
	        }
	        return modifiedFile1; 
		}
		
		//// test function
		
		@RequestMapping(value="/jobuploadxlsx.do", method=RequestMethod.GET)	
		public void fileUrl(String fAddress, String localFileName, String destinationDir) {
		OutputStream outStream = null;
		java.net.URLConnection  uCon = null;
		 int size=1024;
		
		InputStream is = null;
		
		String root = Utility.getValueOfPropByKey("teacherRootPath");	
		File path = new File(root + "/uploadvacancy");
		if (!path.exists()) {
			boolean status = path.mkdirs();
		}
		
		try {
		URL Url;
		byte[] buf;
		int ByteRead,ByteWritten=0;
		Url= new URL("http://propertytaxbill.com/uploadvacancy/Vacancy.xlsx");
		outStream = new BufferedOutputStream(new FileOutputStream(path+"/"+"Vacancy.xlsx"));
		
		uCon =  Url.openConnection();
		is = uCon.getInputStream();
		buf = new byte[size];
		while ((ByteRead = is.read(buf)) != -1) {
		outStream.write(buf, 0, ByteRead);
		ByteWritten += ByteRead;
		}
		System.out.println("Downloaded Successfully.");
		System.out.println("File name:\""+"Vacancy.xlsx"+ "\"\nNo ofbytes :" + ByteWritten);
		}
		catch (Exception e) {
		e.printStackTrace();
		}
		finally {
		try {
		is.close();
		outStream.close();
		savejob("Vacancy.xlsx");
		}
		catch (IOException e) {
		e.printStackTrace();
		}}}
		
	private Vector vectorDataExcelXLSX = new Vector();
	
	@RequestMapping(value="/jobimportxlsx.do", method=RequestMethod.GET)
	public String savejob(String fileName)
	{		
		int district_Id = 1200390;
		
	System.out.println(":::: Import vacancy ::::saveJob:::::::::"+district_Id);
	Map<String,JobOrder> jobOrderMap = new HashMap<String, JobOrder>();

   
    int mapCount=0;
	String returnVal="";
    try{
    	
    	 String root = Utility.getValueOfPropByKey("teacherRootPath")+"/uploadvacancy/";;
    	 System.out.println("file root"+root);
    	 String filePath=root+fileName;
    	 if(fileName.contains(".xlsx")){
    		 vectorDataExcelXLSX = readDataExcelXLSX(filePath);
    	 }else{
    		 vectorDataExcelXLSX = readDataExcelXLS(filePath);
    	 }
    	 
    	 System.out.println("read excel complete");
    	 
			int run_date_count=11;
			int Position_ID_count=11;
			int LocId_count=11;
			int Job_Code_count=11;
			int run_time_count=11;
			int Job_Category_count=11;
			int Expiration_Date_count=11;
			
			
			
	        DistrictMaster districtMaster=districtMasterDAO.findById(district_Id, false, false);
	        UserMaster userMaster = userMasterDAO.findById(1, false, false);
	        districtRequisitionNumbersDAO.emptyDeleteFlag("D",districtMaster);
	        
	        Criterion districtMaster_districtID = Restrictions.eq("districtMaster", districtMaster);
	        List<DistrictRequisitionNumbers> requisitionNumberExist = districtRequisitionNumbersDAO.findByCriteria(districtMaster_districtID);
	        
	        Map<String,DistrictRequisitionNumbers>requisitionNumberMap=new HashMap<String, DistrictRequisitionNumbers>();
			 for(DistrictRequisitionNumbers requisitionnumber:requisitionNumberExist)
			 {				 
				 requisitionNumberMap.put(requisitionnumber.getRequisitionNumber(), requisitionnumber);
			 }
			 
			 List<SchoolMaster> schoolLoc = schoolMasterDAO.findSchoolListByDistrict(districtMaster);

			 Map<String, SchoolMaster>schoolLocExistMap = new HashMap<String, SchoolMaster>();

			 if(schoolLoc.size()!=0){
				 for (SchoolMaster school : schoolLoc) {

					 String ss=school.getLocationCode();
					 schoolLocExistMap.put(ss, school);
				}
			 }
			 
			 			 
			 List<JobOrder> apiJobIdList = jobOrderDAO.findJobOrderListByDistrict(districtMaster);
			 
			 Map<String, JobOrder>apiJobIdMap = new HashMap<String, JobOrder>();
			 if(apiJobIdList!=null && apiJobIdList.size()!=0){
			 for(JobOrder apiJob : apiJobIdList){
				 apiJobIdMap.put(apiJob.getApiJobId(), apiJob);
			 }
			 }
			 
			 
			 
			 Criterion jobCodeStatus = Restrictions.eq("status", "A");
			 List<JobMaster> jobMasterList = jobMasterDAO.findByCriteria(districtMaster_districtID,jobCodeStatus);
			 Map<String, JobMaster> jobMasterMap = new HashMap<String, JobMaster>();
			 for(JobMaster job:jobMasterList){
				 jobMasterMap.put(job.getJobCode(), job);
			 }
			
			 Map<String, Boolean> jobMasterExistMap = new HashMap<String, Boolean>();
			 for(JobMaster job:jobMasterList){
				 jobMasterExistMap.put(job.getJobCode(), true);
			 }
			
			 List<JobCategoryMaster> JobCategoryMaster= jobCategoryMasterDAO.findActiveJobCategory();
			 Map<String, JobCategoryMaster>JobCategoryMap = new HashMap<String, JobCategoryMaster>();
			 for(JobCategoryMaster category:JobCategoryMaster){
				 JobCategoryMap.put(category.getJobCategoryName(), category);
			 }
			
			 //districtMaster_districtID
			 List<JobPoolMaster> jobPoolMaster = jobpoolMasterDAO.findByCriteria(districtMaster_districtID);
						
			 Map<Integer, Integer>jobPoolMasterMap = new HashMap<Integer, Integer>();
			 for(JobPoolMaster jobpool:jobPoolMaster){
				 jobPoolMasterMap.put(jobpool.getJobId().getJobId(), jobpool.getPoolId().getPoolId());
			 }
			 
			 List<SchoolInJobOrder> lstSchoolInJobOrders=null;
			 if(apiJobIdList.size()!=0){
				 lstSchoolInJobOrders = schoolInJobOrderDAO.getSIJO(apiJobIdList);
			 }
			 
				Map<String, SchoolInJobOrder>schoolInJobOrderMap = new HashMap<String, SchoolInJobOrder>();
				String jobAndsch=null;
				if(lstSchoolInJobOrders!=null && lstSchoolInJobOrders.size()!=0){				
					for(SchoolInJobOrder schoolInJO :lstSchoolInJobOrders){
						jobAndsch+=schoolInJO.getJobId()+"||"+schoolInJO.getSchoolId();
						schoolInJobOrderMap.put(jobAndsch, schoolInJO);
					}
				}
				
				Criterion criterion = Restrictions.eq("status", "A");
				Criterion criterion1 = Restrictions.eq("isDefault",true);
				Criterion criterion2 = Restrictions.eq("jobOrderType",1);
				Criterion criterion3 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion4 = Restrictions.eq("assessmentType", 2);				
				List<AssessmentDetail> lstAssessmentDetails = assessmentDetailDAO.findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4);
				
				Map<Integer,AssessmentDetail> assesmentMap =new HashMap<Integer, AssessmentDetail>();
				if(lstAssessmentDetails!=null && lstAssessmentDetails.size()!=0){
					for(AssessmentDetail assessmentlst:lstAssessmentDetails){
						assesmentMap.put(assessmentlst.getJobCategoryMaster().getJobCategoryId(),assessmentlst);
					}
				}
				
				Criterion statusAJR = Restrictions.eq("status", "A");
				Criterion statusAJR1 = Restrictions.eq("createdBy",userMaster);
				List<AssessmentJobRelation> lstAssessmentJobRelations = assessmentJobRelationDAO.findByCriteria(statusAJR,statusAJR1);
				
				Map<String, AssessmentJobRelation> ajrlistMAP = new HashMap<String, AssessmentJobRelation>();
				String jobAndASS=null;
				if(lstAssessmentJobRelations!=null && lstAssessmentJobRelations.size()!=0){
					for(AssessmentJobRelation jobInAJR:lstAssessmentJobRelations){
						jobAndASS+=jobInAJR.getJobId()+"||"+jobInAJR.getAssessmentJobRelationId();
						ajrlistMAP.put(jobAndASS, jobInAJR);
					}
				}
				
				List<PoolMaster> lstPoolMasters = poolMasterDAO.findByCriteria(districtMaster_districtID);
				
				Map<Integer, String> lstPoolMastersMap = new HashMap<Integer, String>();
				
				if(lstPoolMasters!=null && lstPoolMasters.size()!=0){
					for(PoolMaster poolDesc:lstPoolMasters){
						lstPoolMastersMap.put(poolDesc.getPoolId(), poolDesc.getPoolDescription());
					}
				}
				
		   boolean updateDistReqFlagFinal=false;
	       boolean insertDistReqFlagFinal=false;	       
	       String requisitionNumber_store="";
	       boolean row_error=false;
	       String final_error_store="";
	       String job_code_store="";
	       
	       ArrayList<Integer> deleteFlagUpdateStore = new ArrayList<Integer>();
	   	
	   	SessionFactory sessionFactory=jobMasterDAO.getSessionFactory();
	   	StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	    Transaction txOpen =statelesSsession.beginTransaction();

		 for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
			
            Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
            String run_date="";
            String Position_ID="";
  	        String LocId="";
  	        String Job_Code="";
  	        String run_time="";
  	        String Job_Category="";
  	        String Expiration_Date="";
  	        //String read_write_previlege="";
  	      String errorText="";
  	      String rowErrorText="";
  	      
  	        for(int j=0; j<vectorCellEachRowData.size(); j++) {
  	        	try{
  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("run_date")){
  	        			run_date_count=j;
	            	}
	            	if(run_date_count==j)
	            		run_date=vectorCellEachRowData.get(j).toString().trim();
  	        		
  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Position_ID")){
  	        			Position_ID_count=j;
	            	}
	            	if(Position_ID_count==j)
	            		Position_ID=vectorCellEachRowData.get(j).toString().trim();

	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Job_Code")){
	            		Job_Code_count=j;
	            	}
	            	if(Job_Code_count==j)
	            		Job_Code=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("LocId")){
								LocId_count=j;
							}
	            	if(LocId_count==j)
	            		LocId=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("run_time")){
	            		run_time_count=j;
	            	}
	            	if(run_time_count==j)
	            		run_time=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Job_Category")){
	            		Job_Category_count=j;
	            	}
	            	if(Job_Category_count==j)
	            		Job_Category=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Expiration_Date")){
	            		Expiration_Date_count=j;
	            	}
	            	if(Expiration_Date_count==j)
	            		Expiration_Date=vectorCellEachRowData.get(j).toString().trim();
            	}catch(Exception e){}
  	        }
  	        
  	        if(i==0){

  	        	if(!run_date.equals("run_date")){
  	        		 	rowErrorText+=TM_instructional_vacancies4;
			  			row_error=true;
  	        	}
  	        	
  	        	if(!Position_ID.equals("Position_ID")){
 	        		 if(row_error){
			  				rowErrorText+=",";
			  			  }
			  				  
			  			rowErrorText+=TM_instructional_vacancies5;
			  			row_error=true;
 	        	}
  	        	
  	        	if(!LocId.equals("LocId")){
	        		 if(row_error){
			  				rowErrorText+=",";
			  			  }
			  				  
			  			rowErrorText+=msgRejectedTheCandidate10;
			  			row_error=true;
	        	}
  	        	
  	        	if(!Job_Code.equals("Job_Code")){
	        		 if(row_error){
			  				rowErrorText+=",";
			  			  }
			  				  
			  			rowErrorText+=TM_instructional_vacancies7;
			  			row_error=true;
	        	}
  	        	if(!run_time.equals("run_time")){
	        		 if(row_error){
			  				rowErrorText+=",";
			  			  }
			  				  
			  			rowErrorText+=TM_instructional_vacancies8;
			  			row_error=true;
	        	}
  	        	
  	        	if(!Job_Category.equals("Job_Category")){
	        		 if(row_error){
			  				rowErrorText+=",";
			  			  }
			  				  
			  			rowErrorText+=TM_instructional_vacancies9;
			  			row_error=true;
	        	}
 	        	
  	        	if(!Expiration_Date.equals("Expiration_Date")){
	        		 if(row_error){
			  				rowErrorText+=",";
			  			  }
			  				  
			  			rowErrorText+=TM_instructional_vacancies10;
			  			row_error=true;
	        	}
  	        }
  	      
  	      if(row_error){
		  System.out.println("error in text file");
  	    	File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+"/uploadvacancy/excelUploadError.txt");
			
				if (!file.exists()) {
					file.createNewFile();
				}
					    				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(rowErrorText);

				bw.close();
		  		 
		  		 break;
		  	 }
  	    if(i!=0 && row_error==false){
  	        
  	        		Integer update_job_id =null; 
  	        		boolean errorFlag=false;
	  	        	boolean updateFlag=false;
	  	        	boolean updateDistReqFlag=false;
	  	        	boolean insertDistReqFlag=false;
	  	        	boolean jobOrderUpdateFlag=false;
	  	        	boolean deleteUpdateFlag=false;
	  	        	boolean assessmentDetailFlag=false;
	  	        	boolean AJRInsertFlag=false;
	  	        	
	  	        	
	  	        	if(!run_date.equalsIgnoreCase("")){
	  	        		if(isDateValidOnly(run_date)!=1){
	  	        			if(errorFlag){
	  							errorText+=",";	
	  						}
	  	        			errorText+=msgRejectedTheCandidate11;
		  	    			errorFlag=true;	
	  	        		}
	  	        	}else{
	  	        		if(errorFlag){
  							errorText+=",";	
  						}
  	        			errorText+=msgRejectedTheCandidate12;
	  	    			errorFlag=true;
	  	        	}
	  	        	
	  	        	if(!Position_ID.equalsIgnoreCase("")){
  	        			if(requisitionNumber_store.contains("||"+Position_ID+"||")){
  	        				if(errorFlag){
	  							errorText+=",";	
	  						}
	  	        			errorText+=TM_instructional_vacancies15;
		  	    			errorFlag=true;
  	        			}
	  	        		else if(requisitionNumberMap.get(Position_ID)==null){
	  	        			insertDistReqFlag=true;
	  	        		}else if(requisitionNumberMap.get(Position_ID)!=null && requisitionNumberMap.get(Position_ID).getIsUsed()==true){
	  	        			if(errorFlag){
	  							errorText+=",";	
	  						}
	  	        			errorText+=msgRejectedTheCandidate13;
		  	    			errorFlag=true;
		  	    			deleteFlagUpdateStore.add(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId());
	  	        		}else if(requisitionNumberMap.get(Position_ID)!=null && requisitionNumberMap.get(Position_ID).getIsUsed()==false){
	  	        			updateDistReqFlag=true;
	  	        		}
  	        		}else{
  	        			if(errorFlag){
  							errorText+=",";	
  						}
  	        			errorText+=TM_instructional_vacancies17;
	  	    			errorFlag=true;
  	        		}
	  	        	
	  	        	Long School_Id=null;
	  	        	//System.out.println(LocId);
	  	        	LocId=LocId;
	  	        	//System.out.println("LocId    "+LocId);
	  	        	if(!LocId.equalsIgnoreCase("")){
		  	        	if(schoolLocExistMap.get(LocId)!=null && schoolLocExistMap.get(LocId).getDistrictId().getStatus().equalsIgnoreCase("I")){
		  	        		if(errorFlag){
	  							errorText+=",";	
	  						}
		  	        		errorText+=msgRejectedTheCandidate14;
		  	    			errorFlag=true;
		  	        	}else if(schoolLocExistMap.get(LocId)!=null && schoolLocExistMap.get(LocId).getDistrictId().getStatus().equalsIgnoreCase("A") && schoolLocExistMap.get(LocId).getStatus().equalsIgnoreCase("I")){
		  	        		if(errorFlag){
	  							errorText+=",";	
	  						}
		  	        		errorText+=msgRejectedTheCandidate15;
		  	    			errorFlag=true;
	  	        		}else if(schoolLocExistMap.get(LocId)!=null && schoolLocExistMap.get(LocId).getDistrictId().getStatus().equalsIgnoreCase("A") && schoolLocExistMap.get(LocId).getStatus().equalsIgnoreCase("A")){
		  	        		School_Id = schoolLocExistMap.get(LocId).getSchoolId();
		  	        		}else{
		  	        			if(errorFlag){
		  							errorText+=",";	
		  						}
		  	        		errorText+=msgRejectedTheCandidate14;
		  	    			errorFlag=true;
		  	        	}
  	        		}else{
  	        			if(errorFlag){
  							errorText+=",";	
  						}
  	        			errorText+=msgRejectedTheCandidate16;
	  	    			errorFlag=true;
  	        		}
	  	        	
	  	        //job code check
	  	        	Boolean jobOrderUpdate = false;
	  	        	Boolean jobOrderInsert = false;
	  	        	Integer poolId=null;
	  	        	
	  	        	String new_job_code = Job_Code.substring(4);
	  	        	String jobPathDesc ="";
	  	        	if(!Job_Code.equalsIgnoreCase("")){
	  	        		if(jobMasterExistMap.size()>0){
	  	        			if(jobMasterExistMap.containsKey(new_job_code)){
	  	        				
	  	        				Integer jobId = jobMasterMap.get(new_job_code).getJobId();
	  	        				Integer ispooljob =jobPoolMasterMap.get(jobId);
	  	        				jobPathDesc = lstPoolMastersMap.get(jobPoolMasterMap.get(jobId));
	  	        				
	  	        				if(ispooljob==null){
	  	        					if(errorFlag){
		  	  							errorText+=",";	
		  	  						}
		  	  	        			errorText+=msgRejectedTheCandidate17;
		  		  	    			errorFlag=true;
	  	        				}else if(ispooljob==2){
	  	        					poolId=0;
	  	        				}else{
	  	        					poolId=1;
	  	        				}
	  	        				
	  	        				//apiJobIdMap
	  	        				if(apiJobIdMap.containsKey(new_job_code)){
	  	        					jobOrderUpdate = true;
	  	        					update_job_id = apiJobIdMap.get(new_job_code).getJobId();
	  	        				}else{
	  	        					jobOrderInsert = true;
	  	        				}
	  	        				
	  	        			}else{
	  	        				if(errorFlag){
	  	  							errorText+=",";	
	  	  						}
	  	  	        			errorText+=TM_instructional_vacancies18;
	  		  	    			errorFlag=true;
	  	        			}
	  	        		}else{}
	  	        	}else{
	  	        		if(errorFlag){
  							errorText+=",";	
  						}
  	        			errorText+=TM_instructional_vacancies19;
	  	    			errorFlag=true;
  	        		}

	  	        	JobCategoryMaster categoryMaster=null;
	  	        	AssessmentDetail assessmentDetail=null;
  	        		if(!Job_Category.equalsIgnoreCase("")){
  	        			if(!JobCategoryMap.containsKey(Job_Category)){
  	        				if(errorFlag){
  	  							errorText+=",";	
  	  						}
  	  	        			errorText+=msgRejectedTheCandidate18;
  		  	    			errorFlag=true;
  	        			}else{
  	        				if(!assesmentMap.containsKey(JobCategoryMap.get(Job_Category).getJobCategoryId())){
  	        					if(errorFlag){
  	  	  							errorText+=",";	
  	  	  						}
  	  	  	        			errorText+=msgRejectedTheCandidate19;
  	  		  	    			errorFlag=true;
  	        				}else{
  	        					categoryMaster=JobCategoryMap.get(Job_Category);
  	        					assessmentDetail=assesmentMap.get(JobCategoryMap.get(Job_Category).getJobCategoryId());
  	        					assessmentDetailFlag=true;
  	        				}
  	        			}
  	        		}else{
  	        			if(errorFlag){
  							errorText+=",";	
  						}
  	        			errorText+=TM_instructional_vacancies20;
	  	    			errorFlag=true;
  	        		}
  	        		
  	        		if(!Expiration_Date.equalsIgnoreCase("")){
  	        			String getYear = Expiration_Date.substring(Expiration_Date.length() - 4);
  	        			if(!getYear.equalsIgnoreCase("9999")){
  	        				if(isDateValidOnly(Expiration_Date)!=1){
  		  	        			if(errorFlag){
  		  							errorText+=",";	
  		  						}
  		  	        			errorText+=msgRejectedTheCandidate11;
  			  	    			errorFlag=true;	
  		  	        		}
  	        			}
  	        			
	  	        	}else{
	  	        		if(errorFlag){
  							errorText+=",";	
  						}
  	        			errorText+=msgRejectedTheCandidate20;
	  	    			errorFlag=true;
	  	        	}
  	        		
  	        		if(!Position_ID.equalsIgnoreCase("")){
  	        			requisitionNumber_store+="||"+Position_ID+"||";
  	        		}
  	        		
  	        		if(!errorText.equals("")){
  	        			int row = i+1;
	  	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+run_date+","+run_time+","+Position_ID+","+LocId+","+Job_Code+","+Job_Category+","+Expiration_Date+"<>";
	  	    		}
  	        		
  	        	  JobOrder a1 = new JobOrder();
  	       	      DistrictRequisitionNumbers districtRequisitionNumbers = new DistrictRequisitionNumbers(); 
  	       	      JobRequisitionNumbers jobRequisitionNumbers = new JobRequisitionNumbers();
  	       	      		SchoolInJobOrder schoolInJobOrder 		= new SchoolInJobOrder();
  	       	      		AssessmentJobRelation assessmentJobRelation = new AssessmentJobRelation();
  	       	      
  	        		if(errorText.equals("") && (updateDistReqFlag==true || insertDistReqFlag==true)){
  	        			
  	        			Date jobStartDate = Utility.getCurrentDateFormart(monthReplace(run_date).replaceAll("/","-"));
  	  	        		Date jobEndDate   = Utility.getCurrentDateFormart(monthReplace(Expiration_Date).replaceAll("/","-"));
  	  	        		
  	  	        		String newJobIdAndSchoolId = null;
  	  	        		
  	        			boolean jobOrderFlag = false;  	        			
  	        			boolean SIJOUpdateFlag = false;
  	        			boolean SIJOInsertFlag = false;
  	        			Integer newNoOfExpHr=null;
  	        			boolean districtRequisitionNumbersFlag=false;
  	        			boolean existJobIdInExcel=false;
  	        			boolean school_in_job_order=false;
  	        			
  	        			if(job_code_store.contains("||"+Job_Code+"||")){
            				existJobIdInExcel=true;
            			}
  	        			
  	        			if(jobOrderInsert==true && existJobIdInExcel==false){
		  	      			a1.setDistrictMaster(districtMaster);
		  	      			a1.setJobStartDate(jobStartDate);
		  	      			a1.setApiJobId(new_job_code);
		  	      			a1.setJobTitle(jobMasterMap.get(new_job_code).getJobTitle());
		  	      			a1.setJobEndDate(jobEndDate);
		  	      			a1.setJobCategoryMaster(categoryMaster);
		  	      			a1.setJobType("F");
		  	      			a1.setIsPoolJob(poolId);
		  	      			a1.setCreatedByEntity(1);
		  	      			a1.setCreatedForEntity(2);
		  	      		if(poolId==1){		  	      			
	  	      				a1.setIsJobAssessment(true);
	  	      				a1.setAttachDefaultJobSpecificPillar(1);
	  	      			}else{
	  	      				a1.setIsJobAssessment(false);
	  	      			}
		  	      			a1.setIsPortfolioNeeded(true);
							a1.setAllSchoolsInDistrict(2);
							a1.setAllGrades(2);
							a1.setPkOffered(2);
							a1.setKgOffered(2);
							a1.setG01Offered(2);
							a1.setG02Offered(2);
							a1.setG03Offered(2);
							a1.setG04Offered(2);
							a1.setG05Offered(2);
							a1.setG06Offered(2);
							a1.setG07Offered(2);
							a1.setG08Offered(2);
							a1.setG09Offered(2);
							a1.setG10Offered(2);
							a1.setG11Offered(2);
							a1.setG12Offered(2);
							a1.setAttachNewPillar(2);
							a1.setAttachDefaultDistrictPillar(2);
							a1.setAttachDefaultSchoolPillar(2);
							a1.setExitURL("http://www.dadeschools.net");
							a1.setPathOfJobDescription(jobPathDesc);
							a1.setWritePrivilegeToSchool(false);
							a1.setSelectedSchoolsInDistrict(2);
							a1.setNoSchoolAttach(1);
		  	      			a1.setJobStatus("O");
		  	      			a1.setStatus("A");
		  	      			
		  	      			jobOrderMap.put(Job_Code,a1);
		  	      			mapCount++;
	  	      				statelesSsession.insert(a1);
	  	      				update_job_id= a1.getJobId();
	  	      				jobOrderFlag = true;
	  	      			}else if(jobOrderUpdate==true && existJobIdInExcel==false){
	  	      				//update job order
		  	      			a1.setJobId(update_job_id);	
		  	      			a1.setDistrictMaster(districtMaster);
		  	      			a1.setJobStartDate(jobStartDate);
		  	      			a1.setApiJobId(new_job_code);
		  	      			a1.setJobTitle(jobMasterMap.get(new_job_code).getJobTitle());
		  	      			a1.setJobEndDate(jobEndDate);
		  	      			a1.setJobCategoryMaster(categoryMaster);
		  	      			a1.setJobType("F");
		  	      			a1.setIsPoolJob(poolId);
		  	      		if(poolId==1){		  	      			
	  	      				a1.setIsJobAssessment(true);
	  	      				a1.setAttachDefaultJobSpecificPillar(1);
	  	      			}else{
	  	      				a1.setIsJobAssessment(false);
	  	      			}
		  	      			a1.setCreatedByEntity(1);
		  	      			a1.setCreatedForEntity(2);		  	      			
		  	      			a1.setIsPortfolioNeeded(true);
							a1.setAllSchoolsInDistrict(2);
							a1.setAllGrades(2);
							a1.setPkOffered(2);
							a1.setKgOffered(2);
							a1.setG01Offered(2);
							a1.setG02Offered(2);
							a1.setG03Offered(2);
							a1.setG04Offered(2);
							a1.setG05Offered(2);
							a1.setG06Offered(2);
							a1.setG07Offered(2);
							a1.setG08Offered(2);
							a1.setG09Offered(2);
							a1.setG10Offered(2);
							a1.setG11Offered(2);
							a1.setG12Offered(2);
							a1.setAttachNewPillar(2);
							a1.setAttachDefaultDistrictPillar(2);
							a1.setAttachDefaultSchoolPillar(2);
							a1.setExitURL("http://www.dadeschools.net");
							a1.setPathOfJobDescription(jobPathDesc);
							a1.setWritePrivilegeToSchool(false);
							a1.setSelectedSchoolsInDistrict(2);
							a1.setNoSchoolAttach(1);
		  	      			a1.setJobStatus("O");
		  	      			a1.setStatus("A");
		  	      			
		  	      			jobOrderMap.put(Job_Code,a1);
		  	      			mapCount++;
	  	      				statelesSsession.update(a1);
	  	      				//update_job_id= a1.getJobId();
	  	      				jobOrderFlag = true;
	  	      			}else{
	  	      			jobOrderFlag = true;
	  	      			}
  	        			
  	        			if(assessmentDetailFlag==true && jobOrderFlag==true){
  	        				  	        				
  	        				System.out.println("user     "+userMaster.getUserId());
  	        				if(ajrlistMAP.get(jobOrderMap.get(Job_Code).getJobId()+"||"+assesmentMap.get(JobCategoryMap.get(Job_Category).getJobCategoryId()))==null){
  	        					assessmentJobRelation.setAssessmentId(assesmentMap.get(JobCategoryMap.get(Job_Category).getJobCategoryId()));
  	        					assessmentJobRelation.setJobId(jobOrderMap.get(Job_Code));
  	        					assessmentJobRelation.setCreatedBy(userMaster);
  	        					assessmentJobRelation.setStatus("A");
  	        					assessmentJobRelation.setIpaddress("122.177.4.102");
  	        					assessmentJobRelation.setCreatedDateTime(new Date());
  	        					statelesSsession.insert(assessmentJobRelation);
  	        					ajrlistMAP.put(jobOrderMap.get(Job_Code).getJobId()+"||"+assesmentMap.get(JobCategoryMap.get(Job_Category).getJobCategoryId()), assessmentJobRelation);
  	        				}
  	        				
  	        				
  	        			}
  	        			
  	        			job_code_store+="||"+Job_Code+"||";
  	        			
	  	      		if(updateDistReqFlag==true && jobOrderFlag ==true){
	  	      			districtRequisitionNumbers.setDistrictRequisitionId(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId());
	  	      			districtRequisitionNumbers.setDistrictMaster(districtMaster);
	  	      			districtRequisitionNumbers.setRequisitionNumber(Position_ID);
	  	      			districtRequisitionNumbers.setIsUsed(true);
	  	      			districtRequisitionNumbers.setDeleteFlag("N");
	  	      			districtRequisitionNumbers.setUserMaster(userMaster);
	  	      			districtRequisitionNumbers.setCreatedDateTime(new Date());
	  	      			statelesSsession.update(districtRequisitionNumbers);
	  	      			districtRequisitionNumbersFlag = true;
	  	      		}
	  	      		
	  	      		if(insertDistReqFlag==true && jobOrderFlag ==true){
	  	      			districtRequisitionNumbers.setDistrictMaster(districtMaster);
	  	      			districtRequisitionNumbers.setRequisitionNumber(Position_ID);
	  	      			districtRequisitionNumbers.setIsUsed(true);
	  	      			districtRequisitionNumbers.setDeleteFlag("N");
	  	      			districtRequisitionNumbers.setUserMaster(userMaster);
	  	      			districtRequisitionNumbers.setCreatedDateTime(new Date());
	  	      			statelesSsession.insert(districtRequisitionNumbers);
	  	      			districtRequisitionNumbersFlag = true;
	  	      		}
	  	      		
	  	      		if(districtRequisitionNumbersFlag==true){
		  	      		jobRequisitionNumbers.setJobOrder(jobOrderMap.get(Job_Code));
		  	      		jobRequisitionNumbers.setDistrictRequisitionNumbers(districtRequisitionNumbers);
		  	      		jobRequisitionNumbers.setSchoolMaster(schoolLocExistMap.get(LocId));
		  	      		jobRequisitionNumbers.setStatus(1);
		  	      		statelesSsession.insert(jobRequisitionNumbers);
		  	      		school_in_job_order=true;
	  	      		}
	  	      		
	  	      	Integer noOEhr=null;
	  	      	Integer noOEhrs=null;
  	      		if(districtRequisitionNumbersFlag && school_in_job_order){
	  	      		if(schoolInJobOrderMap.get(jobOrderMap.get(Job_Code).getJobId()+"||"+schoolLocExistMap.get(LocId).getSchoolId())!=null){
	  	      			
	  	      			noOEhrs=schoolInJobOrderMap.get(jobOrderMap.get(Job_Code).getJobId()+"||"+schoolLocExistMap.get(LocId).getSchoolId()).getNoOfSchoolExpHires();
	  	      			
	  	      			noOEhr = noOEhrs+1;
	  	      			Integer row_id=schoolInJobOrderMap.get(jobOrderMap.get(Job_Code).getJobId()+"||"+schoolLocExistMap.get(LocId).getSchoolId()).getJobOrderSchoolId();
	  	      			
	  	      			schoolInJobOrder.setJobOrderSchoolId(row_id);
	  	      			schoolInJobOrder.setJobId(jobOrderMap.get(Job_Code));
	  	      			schoolInJobOrder.setSchoolId(schoolLocExistMap.get(LocId));		  	      			
	  	      			schoolInJobOrder.setNoOfSchoolExpHires(noOEhr);
	  	      			schoolInJobOrder.setCreatedDateTime(new Date());
	  	      			statelesSsession.update(schoolInJobOrder);
	  	      			schoolInJobOrderMap.remove(jobOrderMap.get(Job_Code)+"||"+schoolLocExistMap.get(LocId));		  	      			
	  	      			
        			}else{
        				noOEhr=1;
        				
        				schoolInJobOrder.setJobId(jobOrderMap.get(Job_Code));
	  	      			schoolInJobOrder.setSchoolId(schoolLocExistMap.get(LocId));		  	      			
	  	      			schoolInJobOrder.setNoOfSchoolExpHires(noOEhr);
	  	      			schoolInJobOrder.setCreatedDateTime(new Date());
        				statelesSsession.insert(schoolInJobOrder);
        			}
	  	      		schoolInJobOrderMap.put(jobOrderMap.get(Job_Code).getJobId()+"||"+schoolLocExistMap.get(LocId).getSchoolId(),schoolInJobOrder);
	  	      		
	  	      		//System.out.println(jobOrderMap.get(Job_Code).getJobId()+" ||  "+schoolLocExistMap.get(LocId).getSchoolId()+" ||  "+noOEhr);
  	      		}
	  	      		
  	      		}else{
  	      		}
  	        	
	  	       }
	  	    } // i for loop end
		 txOpen.commit();
 	 	 statelesSsession.close();

 	 	 //record in districtrequisitionnumbers
 	 	 if(deleteFlagUpdateStore.size()==0){
 	 		deleteFlagUpdateStore.add(0);
 	 	 }
 	 	 
 	 	districtRequisitionNumbersDAO.updateDeleteFlag(deleteFlagUpdateStore);
 	 	districtRequisitionNumbersDAO.deleteFlagRecords();
 	 	//districtRequisitionNumbersDAO.emptyDeleteFlag(null);
 	 	 
 	 	/*
 	 	UserUploadFolderAccess b1 = new UserUploadFolderAccess();
 	 	b1.setUseuploadFolderId(rowId);
		b1.setDistrictId(districtMaster);
		b1.setFunctionality("jobUpload");
		b1.setFolderPath(folderPath);
		b1.setNameOfLastFileUploaded(nameOfLastFileUploaded);
		b1.setLastUploadDateTime(getCurrentDateFormart2(lastUploadDateTime));
		b1.setStatus("A");
		
		userUploadFolderAccessDAO.makePersistent(b1);
		
		*/
		
		deleteXlsAndXlsxFile(null, fileName);

		if(!final_error_store.equalsIgnoreCase("")){
 	 		String content = final_error_store;		    				
			File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+"excelUploadError.txt");
			
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			String[] parts = content.split("<>");	    				
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("run_date,run_time,Position_ID,LocId,Job_Code,Job_Category,Expiration_Date");
			bw.write("\r\n\r\n");
			int k =0;
			for(String cont :parts) {
				bw.write(cont+"\r\n\r\n");
			    k++;
			}
			bw.close();
			System.out.println("error text file complete   "+file);
			/*
			List<UserMaster> userMasterUsers = null;
			try {
//DistrictMaster districtMaster=districtMasterDAO.findById(district_Id, false, false);
				RoleMaster role_id=roleMasterDAO.findById(2, false, false);
				Criterion criterionByRoleId = Restrictions.eq("roleId",role_id);
				Criterion criterionByDistrictId = Restrictions.eq("districtId",districtMaster);
				Criterion criterionByActiveUser = Restrictions.like("status","A");
				userMasterUsers = userMasterDAO.findByCriteria(criterionByDistrictId,criterionByRoleId,criterionByActiveUser);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				String userMasterUsersemail = "";
				String allEmail="";
				for (UserMaster getAllemail : userMasterUsers) 
				{
					userMasterUsersemail+= getAllemail.getEmailAddress()+";";
				}
 */
			String to= "gourav@netsutra.com";
			String msgSubject = "Job Upload Error";
			String msg = "Error in upload excel";
			String from= "gourav@netsutra.com";
			String file_txt = Utility.getValueOfPropByKey("teacherRootPath")+"/uploadvacancy/excelUploadError.txt";			

			//EmailerService emailerService = new EmailerService();	    				
			//emailerService.sendMailWithAttachments(to, msgSubject,from,msg,file_txt);
			 //System.out.println(to+" "+msgSubject+" "+from+" "+msg+" "+file_txt);
 	 	}
		
 	 	 
	}catch (Exception e){
		e.printStackTrace();
	}
		return null;
	}

	

	public static Vector readDataExcelXLS(String fileName) throws IOException {

	System.out.println(":::::::::::::::::readDataExcel XLS::::::::::::");
	Vector vectorData = new Vector();
	
	try{
		InputStream ExcelFileToRead = new FileInputStream(fileName);
		HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
 
		HSSFSheet sheet=wb.getSheetAt(0);
		HSSFRow row; 
		HSSFCell cell;
 
		Iterator rows = sheet.rowIterator();
			int run_date_count=11;
			int run_time_count=11;
			int Position_ID_count=11;
			int LocId_count=11; 
			int Job_Code_count=11;	        
	        int Job_Category_count=11;
	        int Expiration_Date_count=11;
	        
        String indexCheck="";
		 
		while (rows.hasNext()){
			row=(HSSFRow) rows.next();
			Iterator cells = row.cellIterator();
			Vector vectorCellEachRowData = new Vector();
			int cIndex=0; 
			boolean cellFlag=false,dateFlag=false;
			String indexPrev="";
			Map<Integer,String> mapCell = new TreeMap<Integer, String>();
			while (cells.hasNext())
			{
				cell=(HSSFCell) cells.next();
				cIndex=cell.getColumnIndex();
				if(cell.toString().equalsIgnoreCase("run_date")){
					run_date_count=cIndex;
					indexCheck+="||"+cIndex+"||";
				}
				if(run_date_count==cIndex){
					cellFlag=true;
				}
				if(cell.toString().equalsIgnoreCase("run_time")){
					run_time_count=cIndex;
					indexCheck+="||"+cIndex+"||";
				}
				if(run_time_count==cIndex){
					cellFlag=true;
				}
				
				if(cell.toString().equalsIgnoreCase("Position_ID")){
					Position_ID_count=cIndex;
					indexCheck+="||"+cIndex+"||";
				}
				if(Position_ID_count==cIndex){
					cellFlag=true;
					dateFlag=true;
				}
				
				if(cell.toString().equalsIgnoreCase("LocId")){
					LocId_count=cIndex;
					indexCheck+="||"+cIndex+"||";
				}
				if(LocId_count==cIndex){
					cellFlag=true;
				}
				if(cell.toString().equalsIgnoreCase("Job_Code")){
					Job_Code_count=cIndex;
					indexCheck+="||"+cIndex+"||";
				}
				if(Job_Code_count==cIndex){
					cellFlag=true;
				}
				
				if(cell.toString().equalsIgnoreCase("Job_Category")){
					Job_Category_count=cIndex;
					indexCheck+="||"+cIndex+"||";
				}
				if(Job_Category_count==cIndex){
					cellFlag=true;
				}
				
				if(cell.toString().equalsIgnoreCase("Expiration_Date")){
					Expiration_Date_count=cIndex;
					indexCheck+="||"+cIndex+"||";
				}
				if(Expiration_Date_count==cIndex){
					cellFlag=true;
					dateFlag=true;
				}
				
				if(cellFlag){
					if(!dateFlag){
						try{
							mapCell.put(Integer.valueOf(cIndex),cell.getStringCellValue());
						}catch(Exception e){
							mapCell.put(Integer.valueOf(cIndex),new Double(cell.getNumericCellValue()).longValue()+"");
						}
					}else{
						mapCell.put(Integer.valueOf(cIndex),cell+"");
					}
				}
	        	cellFlag=false;
	        	dateFlag=false;
				
			}
		    vectorCellEachRowData=cellValuePopulate(mapCell);
			vectorData.addElement(vectorCellEachRowData);
		}
	}catch(Exception e){}
	//System.out.println(":::::::::::::::::readDataExcel XLS end::::::::::::"+vectorData);
	return vectorData;
}

	public static Vector readDataExcelXLSX(String fileName) {
	System.out.println(":::::::::::::::::readDataExcel XLSX ::::::::::::");
    Vector vectorData = new Vector();
    try {
    	System.out.println("fileName path  "+fileName);
    	
        FileInputStream fileInputStream = new FileInputStream(fileName);
        XSSFWorkbook xssfWorkBook = new XSSFWorkbook(fileInputStream);
        // Read data at sheet 0
        XSSFSheet xssfSheet = xssfWorkBook.getSheetAt(0);
        Iterator rowIteration = xssfSheet.rowIterator();
        	int run_date_count=11;
        	int run_time_count=11;
        	int Position_ID_count=11;
	        int LocId_count=11;
	        int Job_Code_count=11;
	        int Job_Category_count=11;
	        int Expiration_Date_count=11;

	        // Looping every row at sheet 0
        String indexCheck="";
        while (rowIteration.hasNext()) {
            XSSFRow xssfRow = (XSSFRow) rowIteration.next();
            Iterator cellIteration = xssfRow.cellIterator();
            Vector vectorCellEachRowData = new Vector();
			int cIndex=0; 
			boolean cellFlag=false,dateFlag=false;
			String indexPrev="";
			Map<Integer,String> mapCell = new TreeMap<Integer, String>();
            // Looping every cell in each row at sheet 0
            while (cellIteration.hasNext()){
                XSSFCell xssfCell = (XSSFCell) cellIteration.next();
                cIndex=xssfCell.getColumnIndex();
                
                if(xssfCell.toString().equalsIgnoreCase("run_date")){
            		run_date_count=cIndex;
            		indexCheck+="||"+cIndex+"||";
            	}
            	if(run_date_count==cIndex){
            		cellFlag=true;
            		dateFlag=true;
            	}
            	
            	if(xssfCell.toString().equalsIgnoreCase("run_time")){
            		run_time_count=cIndex;
            		indexCheck+="||"+cIndex+"||";
            	}
            	if(run_time_count==cIndex){
            		cellFlag=true;
            	}
            	
                if(xssfCell.toString().equalsIgnoreCase("Position_ID")){
                	Position_ID_count=cIndex;
            		indexCheck+="||"+cIndex+"||";
            	}
            	if(Position_ID_count==cIndex){
            		cellFlag=true;
            		
            	}	
            	  
            	if(xssfCell.toString().equalsIgnoreCase("LocId")){
            		LocId_count=cIndex;
            		indexCheck+="||"+cIndex+"||";
            	}
            	if(LocId_count==cIndex){
            		cellFlag=true;
            	}
            	
            	if(xssfCell.toString().equalsIgnoreCase("Job_Code")){
            		Job_Code_count=cIndex;
            		indexCheck+="||"+cIndex+"||";
            	}
            	if(Job_Code_count==cIndex){
            		cellFlag=true;
            	}
            	
            	if(xssfCell.toString().equalsIgnoreCase("Job_Category")){
            		Job_Category_count=cIndex;
            		indexCheck+="||"+cIndex+"||";
            	}
            	if(Job_Category_count==cIndex){
            		cellFlag=true;
            	}
            	
            	
            	if(xssfCell.toString().equalsIgnoreCase("Expiration_Date")){
            		Expiration_Date_count=cIndex;
            		indexCheck+="||"+cIndex+"||";
            	}
            	if(Expiration_Date_count==cIndex){
            		cellFlag=true;
            		dateFlag=true;
            	}
            	
            	if(cellFlag){
            		if(!dateFlag){
            			try{
            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getStringCellValue());
            			}catch(Exception e){
            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getRawValue()+"");
            			}
            		}else{
            			mapCell.put(Integer.valueOf(cIndex),xssfCell+"");
            		}
            	}
            	dateFlag=false;
            	cellFlag=false;
            }
            vectorCellEachRowData=cellValuePopulate(mapCell);
            vectorData.addElement(vectorCellEachRowData);
        }
    } catch (Exception ex) {
        ex.printStackTrace();
    }
   // System.out.println(":::::::::::::::::readDataExcel XLSX end::::::::::::"+vectorData);
    return vectorData;
}

	public static Vector cellValuePopulate(Map<Integer,String> mapCell){
	 Vector vectorCellEachRowData = new Vector();
	 Map<Integer,String> mapCellTemp = new TreeMap<Integer, String>();
	 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false;
	 for(Map.Entry<Integer, String> entry : mapCell.entrySet()){
		int key=entry.getKey();
		String cellValue=null;
		if(entry.getValue()!=null)
			cellValue=entry.getValue().trim();
		
		if(key==0){
			mapCellTemp.put(key, cellValue);
			flag0=true;
		}
		if(key==1){
			flag1=true;
			mapCellTemp.put(key, cellValue);
		}
		if(key==2){
			flag2=true;
			mapCellTemp.put(key, cellValue);
		}
		if(key==3){
			flag3=true;
			mapCellTemp.put(key, cellValue);
		}
		if(key==4){
			flag4=true;
			mapCellTemp.put(key, cellValue);
		}
		if(key==5){
			flag5=true;
			mapCellTemp.put(key, cellValue);
		}
		if(key==6){
			flag6=true;
			mapCellTemp.put(key, cellValue);
		}
	
	 }
	 if(flag0==false){
		 mapCellTemp.put(0, "");
	 }
	 if(flag1==false){
		 mapCellTemp.put(1, "");
	 }
	 if(flag2==false){
		 mapCellTemp.put(2, "");
	 }
	 if(flag3==false){
		 mapCellTemp.put(3, "");
	 }
	 if(flag4==false){
		 mapCellTemp.put(4, "");
	 }
	 if(flag5==false){
		 mapCellTemp.put(5, "");
	 }
	 if(flag6==false){
		 mapCellTemp.put(6, "");
	 }
			 
	 for(Map.Entry<Integer, String> entry : mapCellTemp.entrySet()){
		 vectorCellEachRowData.addElement(entry.getValue());
	 }
	 return vectorCellEachRowData;
}

	public String deleteXlsAndXlsxFile(HttpServletRequest request,String fileName){
	
	String filePath="";
	File file=null;
	String returnDel="";
	String root = Utility.getValueOfPropByKey("teacherRootPath")+"/uploadvacancy/";
	try{			
		filePath=root+fileName;
		file = new File(filePath);
		file.delete();
		returnDel="deleted";
	}catch(Exception e){}
	return returnDel;
}

	public static String monthReplace(String date){
	String currentDate="";
	try{
		
	   if(date.contains("Jan")){
		   currentDate=date.replaceAll("Jan","01");
	   }
	   if(date.contains("Feb")){
		   currentDate=date.replaceAll("Feb","02");
	   }
	   if(date.contains("Mar")){
		   currentDate=date.replaceAll("Mar","03");
	   }
	   if(date.contains("Apr")){
		   currentDate=date.replaceAll("Apr","04");
	   }
	   if(date.contains("May")){
		   currentDate=date.replaceAll("May","05");
	   }
	   if(date.contains("Jun")){
		   currentDate=date.replaceAll("Jun","06");
	   }
	   if(date.contains("Jul")){
		   currentDate=date.replaceAll("Jul","07");
	   }
	   if(date.contains("Aug")){
		   currentDate=date.replaceAll("Aug","08");
	   }
	   if(date.contains("Sep")){
		   currentDate=date.replaceAll("Sep","09");
	   }
	   if(date.contains("Oct")){
		   currentDate=date.replaceAll("Oct","10");
	   }
	   if(date.contains("Nov")){
		   currentDate=date.replaceAll("Nov","11");
	   }
	   if(date.contains("Dec")){
		   currentDate=date.replaceAll("Dec","12");
	   }
	   String uDate=currentDate.replaceAll("-","/");
	   if(Utility.isDateValid(uDate,0)==2){
		   currentDate=date; 
	   }else{
		   try{
			   String dateSplit[]=uDate.split("/");
			   currentDate=dateSplit[1]+"/"+dateSplit[0]+"/"+dateSplit[2];
		   }catch(Exception e){
			   currentDate=date; 
		   }
	   }
	}catch(Exception e){
		currentDate=date;
	}
	   return currentDate;
 }

	public static Date getCurrentDateFormart2(String oldDateString) throws ParseException{

		String OLD_FORMAT = "yyyy-MM-dd HH:mm:ss";
		String NEW_FORMAT = "yyyy-MM-dd HH:mm:ss";
		String newDateString;
		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date d = sdf.parse(oldDateString);
		sdf.applyPattern(NEW_FORMAT);
		newDateString = sdf.format(d);
	
		SimpleDateFormat stodate = new SimpleDateFormat(NEW_FORMAT);
		Date date = stodate.parse(newDateString);
		return date;
	}
	
	public static int isDateValidOnly(String date) 
	{
		String DATE_FORMAT ="mm-dd-yyyy";

        try {
        	SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        	sdf.setLenient(false);
        	sdf.parse(date);
        	//System.out.println(sdf.parse(date));
            return 1;
        } catch (Exception e) {
            return 2;
        }
	}
}

