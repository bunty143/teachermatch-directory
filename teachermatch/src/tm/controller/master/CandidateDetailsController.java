package tm.controller.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.menu.MenuMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolWiseCandidateStatusDAO;
import tm.dao.TeacherAdditionalDocumentsDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherGeneralKnowledgeExamDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.TeacherSubjectAreaExamDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.dao.master.PanelAttendeesDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectAreaExamMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.TimeZoneMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.EmailerService;
import tm.services.district.ManageStatusAjax;
import tm.utility.Utility;

@Controller
public class CandidateDetailsController 
{
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private SchoolWiseCandidateStatusDAO schoolWiseCandidateStatusDAO;
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	private PanelAttendeesDAO panelAttendeesDAO;
	
	@Autowired
	private PanelScheduleDAO panelScheduleDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO; 
	
	
	@Autowired
	private JobWisePanelStatusDAO jobWisePanelStatusDAO;
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private SubjectAreaExamMasterDAO subjectAreaExamMasterDAO;
	@Autowired
	private TeacherSubjectAreaExamDAO teacherSubjectAreaExamDAO;
	
	@Autowired
	private TeacherGeneralKnowledgeExamDAO teacherGeneralKnowledgeExamDAO;
	

	@Autowired
	private TeacherAdditionalDocumentsDAO teacherAdditionalDocumentsDAO;
	
	@Autowired
	private TimeZoneMasterDAO timeZoneMasterDAO;
	public void setTimeZoneMasterDAO(TimeZoneMasterDAO timeZoneMasterDAO) {
		this.timeZoneMasterDAO = timeZoneMasterDAO;
	}
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO)
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(
			JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private StateMasterDAO stateMasterDAO;

	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	
	@Autowired
	private ManageStatusAjax manageStatusAjax;
	
	
	@RequestMapping(value={"/prospectdetails.do","/prospectdetailsnew.do"}, method=RequestMethod.GET)
	public String candidateDetailsGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println("\n =========== prospectdetails.do in TeacherController  ===============");
		
		try 
		{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}

		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");

		map.addAttribute("userSession", userSession);
		int entityID=0,roleId=0;
		if(userSession!=null){
			entityID=userSession.getEntityType();
			if(userSession.getRoleId().getRoleId()!=null){
				roleId=userSession.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		map.addAttribute("entityID", entityID);
		/*if(userSession.getEntityType()!=1){
			map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
			map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
		}else{
			map.addAttribute("DistrictName",null);
		}
		
		if(userSession.getEntityType()==2){
			map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
			map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
			map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
			
		}
		if(userSession.getEntityType()==3){
			map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
			map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
			map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
		}else{
			map.addAttribute("SchoolName",null);
		}*/
		List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
		String[] statusTotal = {"comp","icomp","hird","rem","widrw","vlt"};
		try{
			lstStatusMasters = Utility.getStaticMasters(statusTotal);
		}catch (Exception e) {
			e.printStackTrace();
		}
		List<StateMaster> lstStateMaster = new ArrayList<StateMaster>();
		lstStateMaster = stateMasterDAO.findAllStateByOrder();
		
		// @Ashish :: Add job Category list
		DistrictMaster districtMaster = null;
		List<JobCategoryMaster> lstJobCategoryMasters = new ArrayList<JobCategoryMaster>();
		
		boolean writePrivilegeToSchool = false;
		boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
		if(entityID!=1)
		{

			districtMaster = districtMasterDAO.findById(userSession.getDistrictId().getDistrictId(), false, false);
			lstJobCategoryMasters = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
			try{

				DistrictMaster districtMasterObj=(DistrictMaster)userSession.getDistrictId();
				if(districtMasterObj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(districtMasterObj.getDisplayTFA()){
					tFA=true;
				}
				if(districtMasterObj.getDisplayDemoClass()){
					demoClass=true;
				}
				if(districtMasterObj.getDisplayJSI()){
					JSI=true;
				}
				if(districtMasterObj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				if(districtMasterObj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				if(districtMasterObj.getDisplayFitScore()){
					fitScore=true;
				}
				
				writePrivilegeToSchool = districtMasterObj.getWritePrivilegeToSchool();
				
			}catch(Exception e){ e.printStackTrace();}
		}else
		{
			achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
			//lstJobCategoryMasters = jobCategoryMasterDAO.findActiveJobCategory();
		}
		
		
		Map<String,Boolean> prefMap = new HashMap<String, Boolean>();
		prefMap.put("achievementScore", achievementScore);
		prefMap.put("tFA", tFA);
		prefMap.put("demoClass", demoClass);
		prefMap.put("JSI", JSI);
		prefMap.put("teachingOfYear", teachingOfYear);
		prefMap.put("expectedSalary", expectedSalary);
		prefMap.put("fitScore", fitScore);
		
		map.addAttribute("prefMap",prefMap);
		map.addAttribute("lstStateMaster",lstStateMaster);
		map.addAttribute("lstJobCategoryMasters",lstJobCategoryMasters);
		map.addAttribute("lstStatusMasters",lstStatusMasters);
		map.addAttribute("roleAccess", roleAccess);
		map.addAttribute("timezone",timeZoneMasterDAO.getAllTimeZone());
		map.addAttribute("writePrivilegeToSchool",writePrivilegeToSchool);
		List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();
		
		if(userSession.getEntityType()==2)
			subjectMasters = subjectMasterDAO.findActiveSubjectByDistrict((DistrictMaster)userSession.getDistrictId());
		
		map.addAttribute("subjectMasters",subjectMasters);
		map.addAttribute("entityType",userSession.getEntityType());
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}

		return "candidatedetails";
	}
	
	
	
	@RequestMapping(value="/adhoc.do", method=RequestMethod.GET)
	public String robynreview1(ModelMap map,HttpServletRequest request)
	{
		System.out.println("adhoc.do.........");
		try 
		{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}	
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		map.addAttribute("entityType", userSession.getEntityType());
		List<MenuMaster> menuCustomReports=new ArrayList<MenuMaster>();
		List<MenuMaster> menuStandardReports=new ArrayList<MenuMaster>();
		
		if(userSession.getHeadQuarterMaster()!=null){
		    if(userSession.getHeadQuarterMaster().getHeadQuarterId()==2)
		    	menuCustomReports=roleAccessPermissionDAO.getMenuReportList(request,91,userSession);	
		    else
		    	menuCustomReports=roleAccessPermissionDAO.getMenuReportList(request,120,userSession);
		}else{
			menuCustomReports=roleAccessPermissionDAO.getMenuReportList(request,91,userSession);	
		}
		
		
		if(userSession.getHeadQuarterMaster()!=null){
			 if(userSession.getHeadQuarterMaster().getHeadQuarterId()==2)
				 menuStandardReports=roleAccessPermissionDAO.getMenuReportList(request,92,userSession);	
			 else
				 menuStandardReports=roleAccessPermissionDAO.getMenuReportList(request,121,userSession);
		}else{
			menuStandardReports=roleAccessPermissionDAO.getMenuReportList(request,92,userSession);	
		}
		map.addAttribute("menuCustomReports",menuCustomReports);
		map.addAttribute("menuStandardReports",menuStandardReports);
		if(menuStandardReports.size()>0 || menuCustomReports.size()>0)
			map.addAttribute("showdescription","1");
		else
			map.addAttribute("showdescription","0");
		
		if(userSession!=null)
		{
			if(userSession.getDistrictId()!=null)
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());	
			else
				map.addAttribute("districtId","");
		}else{
		     map.addAttribute("districtId", "");
		     }
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "adhoc";
	}
}
