package tm.controller.master;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.master.PortfolioItem;
import tm.bean.user.UserMaster;
import tm.dao.master.PortfolioitemDAO;
import tm.dao.user.UserMasterDAO;

@Controller
public class PortfolioItemController {
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	@Autowired
	private PortfolioitemDAO portfolioitemDAO;
	public void setPortfolioitemDAO(PortfolioitemDAO portfolioitemDAO) {
		this.portfolioitemDAO = portfolioitemDAO;
	}
	
	@RequestMapping(value="/portfolioitem.do",method=RequestMethod.GET)
	public String districtspecificportfolioitemGet(ModelMap map,HttpServletRequest request){
		UserMaster userSession=null;
		try 
		{
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}

			userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			map.addAttribute("entityID", entityID);
			
			if(userSession.getEntityType()!=1){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("districtName",null);
			}
			
			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
			}
			map.addAttribute("portfolioitemlist",portfolioitemDAO.findByCriteria(Order.asc("portfolioItemName"),Restrictions.eq("status", "A")));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		if(userSession.getEntityType()==3){
			return "signin";
		}else{
			return "portfolioitem";
		}	
	}
}
