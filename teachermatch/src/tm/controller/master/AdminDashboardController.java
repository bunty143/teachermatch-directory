package tm.controller.master;

/* @Author: Gagan  
 * @Discription: view of Admin dashboard Controller.
 */
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.write.DateTime;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageFromDashboard;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.TeacherPreference;
import tm.bean.UserLoginHistory;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.UserLastVisitToDemoSchedule;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.MessageFromDashboardDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.master.DegreeMasterDAO;
import tm.dao.master.DemoClassScheduleDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.UserLastVisitToDemoScheduleDAO;
import tm.services.charts.ChartService;
import tm.services.district.PrintOnConsole;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

 
@Controller
public class AdminDashboardController 
{
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
	{
		this.districtMasterDAO = districtMasterDAO;
	}
	@Autowired
	private DemoClassScheduleDAO demoClassScheduleDAO;
	public void setDemoClassScheduleDAO(
			DemoClassScheduleDAO demoClassScheduleDAO) {
		this.demoClassScheduleDAO = demoClassScheduleDAO;
	}
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) 
	{
		this.schoolMasterDAO = schoolMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void SchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) 
	{
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;

	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;
	public void setTeacherPreferenceDAO(TeacherPreferenceDAO teacherPreferenceDAO) {
		this.teacherPreferenceDAO = teacherPreferenceDAO;
	}

	@Autowired 
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO) {
		this.teacherExperienceDAO = teacherExperienceDAO;
	}

	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	public void setTeacherAcademicsDAO(TeacherAcademicsDAO teacherAcademicsDAO) {
		this.teacherAcademicsDAO = teacherAcademicsDAO;
	}

	@Autowired
	private DegreeMasterDAO degreeMasterDAO;
	public void setDegreeMasterDAO(DegreeMasterDAO degreeMasterDAO) {
		this.degreeMasterDAO = degreeMasterDAO;
	}

	@Autowired
	private MessageFromDashboardDAO messageFromDashboardDAO;
	public void setMessageFromDashboardDAO(MessageFromDashboardDAO messageFromDashboardDAO) {
		this.messageFromDashboardDAO = messageFromDashboardDAO;
	}
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
		this.teacherDetailDAO = teacherDetailDAO;
	}
	/* @Author: Gagan 
	 * @Discription: view of District or School dashboard Controller.
	 */
	/*============ Get Method of AdminDashboard ====================*/
	@RequestMapping(value="/dashboard.do", method=RequestMethod.GET)
	public String dashboardGET(ModelMap map,HttpServletRequest request)
	{
			System.out.println("\n =========== dashboard.do in Admin Dashboard Controller  ===============");
			HttpSession session = request.getSession(false);
			Utility utility		=	new Utility();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			try{
				if(userSession.getEntityType()==2 || userSession.getEntityType()==6){
					userLoginHistoryDAO.insertUserLoginHistory(userSession,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in district dashboard");
				}else if(userSession.getEntityType()==3){
					userLoginHistoryDAO.insertUserLoginHistory(userSession,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in school dashboard");
				}
				
				if(userSession!=null && userSession.getEntityType()!=null && userSession.getEntityType()==2 && ( userSession.getRoleId().getRoleId()==7 || userSession.getRoleId().getRoleId()==8 || userSession.getRoleId().getRoleId()==9))
				{
					PrintOnConsole.debugPrintln("PNR Login","Try to login for RoleId "+userSession.getRoleId().getRoleId());
					return "redirect:onboardingdashboard.do";
				}

			}catch(Exception e){
				e.printStackTrace();
			}
		return "redirect:tmdashboard.do";
	}
	/*============End Of Get Method dashboardGET====================*/
	/* @Author: Gagan 
	 * @Discription: view of Admin dashboard Controller.
	 */
	/*============ Get Method of AdminDashboard ====================*/
	@RequestMapping(value="/admindashboard.do", method=RequestMethod.GET)
	public String adminDashboardGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== dashboard.do in Admin Dashboard Controller  ===============");
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			try{
					userLoginHistoryDAO.insertUserLoginHistory(userSession,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in admin dashboard");
			}catch(Exception e){
				e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "admindashboard";
	}
	/*============End Of Get Method of Domain Controller ====================*/

	/* @Author: Vishwanath 
	 * @Discription: 
	 */
	@RequestMapping(value="/candidatesboard.do", method=RequestMethod.GET)
	public String candidatesBoard(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			
			Criterion criterion = Restrictions.eq("teacherStatus","DashBoradMail");
			List<MessageFromDashboard> messageFromDashboardList = messageFromDashboardDAO.findByCriteria(criterion);
			if(messageFromDashboardList.size()>0)
				map.addAttribute("messageId", messageFromDashboardList.get(0).getMessageId());
			else
				map.addAttribute("messageId",0);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "candidatesboard";
		
	}
	
	/* @Author: Vishwanath 
	 * @Discription: 
	 */
	@RequestMapping(value="/candidatesdetail.do", method=RequestMethod.GET)
	public String candidatesDetails(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			
			List<MessageFromDashboard> messageFromDashboardList = messageFromDashboardDAO.findByCriteria();
			
			int NotAuthenticatedID = 0;
			int EPINotStartedID = 0;
			int EPINotCompletedID = 0;
			int EPICompletedID = 0;
			for (MessageFromDashboard messageFromDashboard : messageFromDashboardList) {
				if(messageFromDashboard.getTeacherStatus()!=null)
				if(messageFromDashboard.getTeacherStatus().equalsIgnoreCase("NotAuthenticated"))
				{
					NotAuthenticatedID = messageFromDashboard.getMessageId();
				}else if(messageFromDashboard.getTeacherStatus().equalsIgnoreCase("EPINotStarted"))
				{
					EPINotStartedID = messageFromDashboard.getMessageId();
				}else if(messageFromDashboard.getTeacherStatus().equalsIgnoreCase("EPINotCompleted"))
				{
					EPINotCompletedID = messageFromDashboard.getMessageId();
				}else if(messageFromDashboard.getTeacherStatus().equalsIgnoreCase("EPICompleted"))
				{
					EPICompletedID = messageFromDashboard.getMessageId();
				}
			}
			
			Calendar cal1 = Calendar.getInstance();
			cal1.add(Calendar.DATE, -1);
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);
			
			map.addAttribute("yesterday", cal1.getTime());
			map.addAttribute("NotAuthenticatedID", NotAuthenticatedID);
			map.addAttribute("EPINotStartedID", EPINotStartedID);
			map.addAttribute("EPINotCompletedID", EPINotCompletedID);
			map.addAttribute("EPICompletedID", EPICompletedID);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "candidatesdetail";
		
	}
	
	/* @Author: Vishwanath 
	 * @Discription: 
	 */
	@RequestMapping(value="/candidatesmaildetail.do", method=RequestMethod.GET)
	public String candidatesMailDetails(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			
			String tId = request.getParameter("o")==null?"0":request.getParameter("o");
			
			Integer teacherId =0;
			TeacherDetail teacherDetail = new TeacherDetail();
			try {
				teacherId = Integer.parseInt(tId);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(teacherId!=0)
			{
				teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			}
			
			map.addAttribute("teacherDetail1", teacherDetail);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "candidatesmaildetail";
		
	}
	@RequestMapping(value="/tmdashboard.do", method=RequestMethod.GET)
	public String tDashboardGET(ModelMap map,HttpServletRequest request)
	{
		 String dashboard="tmdashboard";
		try 
		{
			System.out.println("\n =========== dashboard.do in Admin Dashboard Controller  ===============");
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			tmdashbordMethod(map,request,demoClassScheduleDAO);
			UserMaster userMaster		=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId().equals(2)){
				dashboard="tmdashboardfornc";
			}
			/*DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			map.addAttribute("userSession", userMaster);
			if(userMaster.getEntityType()!=1){
				districtMaster=userMaster.getDistrictId();
				map.addAttribute("DistrictName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userMaster.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictName",null);
			}
			if(userMaster.getEntityType()==3){
				schoolMaster=userMaster.getSchoolId();
				map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
			}else{
				map.addAttribute("SchoolName",null);
			}
			map.addAttribute("entityType",userMaster.getEntityType());
			
			Boolean userVisitFlag=demoClassScheduleDAO.getUserDemoDateTime(districtMaster, schoolMaster, userMaster, new Date());
			map.addAttribute("userVisitFlag",userVisitFlag);*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return dashboard;
	}

//************** Adding by Deepak *********************
public synchronized static void  tmdashbordMethod(ModelMap map,HttpServletRequest request,DemoClassScheduleDAO demoClassScheduleDAO)
{
	
	HttpSession session = request.getSession(false);
	UserMaster userMaster		=	(UserMaster) session.getAttribute("userMaster");
	DistrictMaster districtMaster=null;
	SchoolMaster schoolMaster=null;
	String locale = Utility.getValueOfPropByKey("locale");
	try {
		
		String basepath = Utility.getValueOfPropByKey("basePath");
		if(basepath.equalsIgnoreCase("https://canada-en-test.teachermatch.org/")){
			map.addAttribute("stateKye", "Hired Candidates By Province");
		}else{
			map.addAttribute("stateKye", Utility.getLocaleValuePropByKey("HiredCondState", locale));
		}
		
		map.addAttribute("userSession", userMaster);
		if(userMaster.getEntityType()!=1 && userMaster.getEntityType()!=6 && userMaster.getEntityType()!=5){
			districtMaster=userMaster.getDistrictId();
			map.addAttribute("DistrictName",userMaster.getDistrictId().getDistrictName());
			map.addAttribute("DistrictId",userMaster.getDistrictId().getDistrictId());
		}else{
			map.addAttribute("DistrictName",null);
		}
		if(userMaster.getEntityType()==3){
			schoolMaster=userMaster.getSchoolId();
			map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
			map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
		}else{
			map.addAttribute("SchoolName",null);
		}
		map.addAttribute("entityType",userMaster.getEntityType());
		
		Boolean userVisitFlag=demoClassScheduleDAO.getUserDemoDateTime(districtMaster, schoolMaster, userMaster, new Date());
		map.addAttribute("userVisitFlag",userVisitFlag);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
	
}
//********************************************************************
@RequestMapping(value="/ncscripts.do", method=RequestMethod.GET)
public String ncscripts(ModelMap map,HttpServletRequest request)
{
	try 
	{
		System.out.println("\n =========== dashboard.do in Admin Dashboard Controller  ===============");
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}

	return "ncscripts";
}
}