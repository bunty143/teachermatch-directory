package tm.controller.master;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.EmployeeMaster;
import tm.bean.FileUploadHistory;
import tm.bean.UserUploadFolderAccess;
import tm.bean.master.DistrictMaster;
import tm.dao.EmployeeMasterDAO;
import tm.dao.FileUploadHistoryDAO;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.EmailerService;
import tm.utility.Utility;

@Controller
public class EmployeeUploadXlsxController {	
	
	String locale = Utility.getValueOfPropByKey("locale");
	  String TM_MDCPS_staff_delta1= Utility.getLocaleValuePropByKey("TM_MDCPS_staff_delta1", locale);
	  String MSGsdpstaffmasterFilefound2= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound2", locale);
      String MSGsdpstaffmasterFilefound3= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound3", locale);
      String MSGsdpstaffmasterFilefound4= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound4", locale);
      String MSGsdpstaffmasterFilefound5= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound5", locale);
	  String MSGsdpstaffmasterFilefound6= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound6", locale);
	  String MSGsdpstaffmasterFilefound7= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound7", locale);
	  String msgLocColumnNotFound1= Utility.getLocaleValuePropByKey("msgLocColumnNotFound1", locale);
	  String MSGsdpstaffmasterFilefound8= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound8", locale);
	  String MSGsdpstaffmasterFilefound9= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound9", locale);
	  String TM_MDCPS_staff_delta2= Utility.getLocaleValuePropByKey("TM_MDCPS_staff_delta2", locale);
	  String TM_MDCPS_staff_delta3= Utility.getLocaleValuePropByKey("TM_MDCPS_staff_delta3", locale);
	  String MSGsdpstaffmasterFilefound11= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound11", locale);
	  String MSGsdpstaffmasterFilefound12= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound12", locale);
	  String TM_MDCPS_staff_delta4= Utility.getLocaleValuePropByKey("TM_MDCPS_staff_delta4", locale);
	  String MSGsdpstaffmasterFilefound13= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound13", locale);
	  String TM_MDCPS_staff_delta5= Utility.getLocaleValuePropByKey("TM_MDCPS_staff_delta5", locale);
	  String MSGsdpstaffmasterFilefound14= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound14", locale);
	  String MSGsdpstaffmasterFilefound15= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound15", locale);
	  String lblFileName1 = Utility.getLocaleValuePropByKey("lblFileName1 ", locale);
	  String lblNewRecords1 = Utility.getLocaleValuePropByKey("lblNewRecords1 ", locale);
	  String lblUpdateRecords1 = Utility.getLocaleValuePropByKey("lblUpdateRecords1 ", locale);
	  String lblUploadDateTime1 = Utility.getLocaleValuePropByKey("lblUploadDateTime1 ", locale);
	  String msgNotFoundFileUpload1= Utility.getLocaleValuePropByKey("msgNotFoundFileUpload1", locale);









	  

	
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	
	@Autowired
	private RoleMasterDAO roleMasterDAO;
	public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO) {
		this.roleMasterDAO = roleMasterDAO;
	}
	
	@Autowired
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	public void setUserUploadFolderAccessDAO(UserUploadFolderAccessDAO userUploadFolderAccessDAO) {
		this.userUploadFolderAccessDAO = userUploadFolderAccessDAO;
	}
	
	@Autowired
	private FileUploadHistoryDAO fileUploadHistoryDAO;
	public void setFileUploadHistoryDAO(FileUploadHistoryDAO fileUploadHistoryDAO){
		this.fileUploadHistoryDAO = fileUploadHistoryDAO;
	}
	
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService){
        this.emailerService = emailerService;
    }
	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	@RequestMapping(value="/staffdeltafileupload.do", method=RequestMethod.GET)
	public String getallactivedistrict(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println("::::: Use EmployeeImportXlsxController controller :::::");
				
		List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
		
		try {
			Criterion functionality = Restrictions.eq("functionality", "employeeUpload");
			Criterion active = Restrictions.eq("status", "A");
			uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		int i=0;
		for (UserUploadFolderAccess getRec : uploadFolderAccessesrec) {
			i++;
			
			Integer districtId = getRec.getDistrictId().getDistrictId();
			File filesList = findLatestFilesByPath(getRec.getFolderPath());
			
			if(filesList==null){
				String to= "gourav@netsutra.com";
				String subject = TM_MDCPS_staff_delta1;
				String msg = msgNotFoundFileUpload1;						
				String from= "gourav@netsutra.com";
				
				//System.out.println(msg);
				System.out.println("filenotfound  "+msg);
				emailerService.sendMail(to, subject, msg);
			}else{
				try {
					String FileName = filesList.getName();
					Integer rowId = getRec.getUseuploadFolderId();
					String folderPath = getRec.getFolderPath();
					String lastUploadDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(filesList.lastModified()));
					
					savejob(FileName,districtId,rowId,folderPath,lastUploadDateTime);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} 
			
			if(i==1){
				break;
			}
		}
		return null;
	}
	
	public File findLatestFilesByPath(String path)
    {
        String pathToScan = path;
        File dir = new File(pathToScan);
        //File files[] = dir.listFiles();
        
        File[] files2 = dir.listFiles( new FilenameFilter() {
            public boolean accept( File dir,String name ) {
            	return name.startsWith("TM_MDCPS_staff_delta");
            }
        } );

        System.out.println("pathToScan   "+pathToScan+"       files2.length"+files2.length);
        
        if(files2.length == 0)
            return null;       
        
            File lastModifiedFile = files2[0];
        for(int i = 1; i < files2.length; i++){
	            if(lastModifiedFile.lastModified() < files2[i].lastModified()){	            	
	                lastModifiedFile = files2[i];	            	
	            }
        }       
        		return lastModifiedFile;
    }
	
		 private Vector vectorDataExcelXLSX = new Vector();
		 public String savejob(String fileName,Integer districtId,Integer rowId,String folderPath,String lastUploadDateTime)
			{							
			 //System.out.println(folderPath+fileName);
				int district_Id = districtId;
			System.out.println("::::::::saveJobTemp:::::::::"+district_Id);
			Map<Integer,EmployeeMaster> employeeMasterMap = new HashMap<Integer, EmployeeMaster>();
			String returnVal="";
	        try{
	        	 
	        	 String filePath=folderPath+fileName;
	        	 System.out.println("1  "+filePath);
	        	 if(fileName.contains(".csv") || fileName.contains(".CSV")){
	        		 System.out.println("2  "+filePath);
	        		 vectorDataExcelXLSX = readDataExcelCSV(filePath);
	        	 }
	        	 	int first_name_count=11;
		            int middle_name_count=11;
		 	        int last_name_count=11;	 	        
		 	        int employee_number_count=11;
		 	        int last4_count=11;
		 	        int pc_count=11;
		 	        int rr_count=11;
		 	        int oc_count=11;
		 	        int staff_type_count=11;
		 	        int status_count=11;
		 	        int email_count=11;
	 	        
	 	        SessionFactory sessionFactory=employeeMasterDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	     	    Transaction txOpen =statelesSsession.beginTransaction();
	     	    
	 	        int mapCount=0;
	 	       int updateCount=0;
		        int createCount=0;
		        int numOfError=0;
		        
	 	       String Employee_Number_store="";
	 	       String SSN_store="";
	 	       String final_error_store="";
	 	      DistrictMaster districtMaster=districtMasterDAO.findById(district_Id, false, false);
	 	      
	 	     //Criterion ssnCheck = Restrictions.eq("districtMaster", districtMaster);
			 List<EmployeeMaster> ssnExist = employeeMasterDAO.findAll();
			 
			 /*Map<String,Boolean>ssnMap=new HashMap<String, Boolean>();
			 for(EmployeeMaster emp:ssnExist)
			 {	
				ssnMap.put(emp.getSSN(), true);
			 }*/
			 
			 Map<String,Boolean>ssnMapEmpCode=new HashMap<String, Boolean>();
			 for(EmployeeMaster emp:ssnExist)
			 {	
				ssnMapEmpCode.put(emp.getEmployeeCode(), true);
			 }
			 
			 Map<String,Integer>updateMap=new HashMap<String, Integer>();
			 for(EmployeeMaster emp:ssnExist)
			 {	
				 updateMap.put(emp.getEmployeeCode(), emp.getEmployeeId());
			 }
	 	        //start check rows
			 boolean row_error=false;
				 for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
		            Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
		            String first_name="";
		            String middle_name="";
		            String last_name="";	 	        
		            String employee_number="";
		            String last4="";
		            String pc="";
		            String rr="";
		            String oc="";
		            String staff_type="";
		            String status="";
		            String email="";
		            
		  	        //String read_write_previlege="";
		  	      String errorText="";
		  	    String rowErrorText="";
		  	        for(int j=0; j<vectorCellEachRowData.size(); j++) {
		  	        	try{
		  	        		
		  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("first_name")){
		  	        			first_name_count=j;
			            	}
			            	if(first_name_count==j)
			            		first_name=vectorCellEachRowData.get(j).toString().trim();
		
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("middle_name")){
			            		middle_name_count=j;
			            	}
			            	if(middle_name_count==j)
			            		middle_name=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("last_name")){
										last_name_count=j;
									}
			            	if(last_name_count==j)
			            		last_name=vectorCellEachRowData.get(j).toString().trim();
			            				            	
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("employee_number")){
			            		employee_number_count=j;
			            	}
			            	if(employee_number_count==j)
			            		employee_number=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("last4")){
			            		last4_count=j;
			            	}
			            	if(last4_count==j)
			            		last4=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("pc")){
			            		pc_count=j;
			            	}
			            	if(pc_count==j)
			            		pc=vectorCellEachRowData.get(j).toString().trim();

			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("rr")){
			            		rr_count=j;
			            	}
			            	if(rr_count==j)
			            		rr=vectorCellEachRowData.get(j).toString().trim();
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("oc")){
			            		oc_count=j;
			            	}
			            	if(oc_count==j)
			            		oc=vectorCellEachRowData.get(j).toString().trim();
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("staff_type")){
			            		staff_type_count=j;
			            	}
			            	if(staff_type_count==j)
			            		staff_type=vectorCellEachRowData.get(j).toString().trim();
						if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("status")){
			            		status_count=j;
			            	}
			            	if(status_count==j)
			            		status=vectorCellEachRowData.get(j).toString().trim();
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("email")){
			            		email_count=j;
			            	}
			            	if(email_count==j)
			            		email=vectorCellEachRowData.get(j).toString().trim();
		            	}catch(Exception e){}
		  	        }
		  	        
		  	        if(i==0){
			  		  
			  		  if(!first_name.equalsIgnoreCase("first_name")){
			  			rowErrorText=MSGsdpstaffmasterFilefound2;
			  			row_error=true;
			  		  }
			  		  if(!middle_name.equalsIgnoreCase("middle_name")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound3;
			  			row_error=true;
			  		  }
			  		  if(!last_name.equalsIgnoreCase("last_name")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound4;
			  			row_error=true;  
			  		  }
			  		if(!employee_number.equalsIgnoreCase("employee_number")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound5;
			  			row_error=true;  
			  		  }
					  if(!last4.equalsIgnoreCase("last4")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound6;
			  			row_error=true;  
			  		  }
					  if(!pc.equalsIgnoreCase("PC")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound7;
			  			row_error=true;  
			  		  }
					  if(!rr.equalsIgnoreCase("RR")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound7;
			  			row_error=true;  
			  		  }
					  if(!oc.equalsIgnoreCase("OC")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=msgLocColumnNotFound1;
			  			row_error=true;  
			  		  }
					  if(!staff_type.equalsIgnoreCase("staff_type")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound8;
			  			row_error=true;  
			  		  }
					  if(!status.equalsIgnoreCase("status")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound9;
			  			row_error=true;  
			  		  }
					  if(!email.equalsIgnoreCase("email")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=TM_MDCPS_staff_delta2;
			  			row_error=true;  
			  		  }
			  		 }
		  	      if(row_error){
				  		File file = new File(folderPath+"/excelUploadError.txt");
						// if file doesnt exists, then create it
						if (!file.exists()) {
							file.createNewFile();
						}
							    				
						FileWriter fw = new FileWriter(file.getAbsoluteFile());
						BufferedWriter bw = new BufferedWriter(fw);
						bw.write(rowErrorText);
		
						bw.close();
				  		
						String to= "gourav@netsutra.com";
						String subject =TM_MDCPS_staff_delta3;
						String msg = "Error in "+fileName+" file";
						String from= "gourav@netsutra.com";
						String errorFileName="excelUploadError.txt";			

						emailerService.sendMailWithAttachments(to,subject,from,msg,folderPath,errorFileName);
						numOfError++;
				  		 break;
				  	 }
		  	        
		  	        EmployeeMaster a1 = new EmployeeMaster();
		  	        if(i!=0){
		  	        	
			  	        	boolean errorFlag=false;
			  	        	boolean updateFlag=false;
			  	        	Integer emp_id = null;
			  	        	if(first_name.equalsIgnoreCase("")){
			  	    			errorText+=MSGsdpstaffmasterFilefound11;
			  	    			errorFlag=true;
			  	    		}		  	    		
			  	    		if(last_name.equalsIgnoreCase("")){
			  	    			if(errorFlag){
		            	 			errorText+=",";	
		            	 		}
			  	    			errorText+=MSGsdpstaffmasterFilefound12;
			  	    			errorFlag=true;
			  	    		}
			  	    		
			  	    		if(employee_number.equalsIgnoreCase("")){
			  	    			if(errorFlag){
		            	 			errorText+=",";	
		            	 		}
			  	    			errorText+=TM_MDCPS_staff_delta4;
			  	    			errorFlag=true;
			  	    		}else if(!employee_number.equalsIgnoreCase("")){
			  	    			if(Employee_Number_store.contains("||"+employee_number+"||")){
			  	    				
			  	    				if(errorFlag){
			  	    				errorText+=",";	
			  	    				}
			  	    				errorText+=MSGsdpstaffmasterFilefound13;
			  	    				errorFlag=true;
			  	    			}else{			  	    				
				  	    				if(ssnMapEmpCode.size()>0)
				  	    			if(ssnMapEmpCode.containsKey(employee_number))
					  	    		{
					  	    			if(errorFlag){
				  							errorText+=",";	
				  						}
			  								updateFlag=true;
				  							emp_id = updateMap.get(employee_number);
					  	    		}
			  	    			}
			  	    		}


			  	    		if(!employee_number.equalsIgnoreCase("")){
			  	    			Employee_Number_store+="||"+employee_number+"||";
			  	    		}
			  	    		
			  	    		String errorTextHeader = TM_MDCPS_staff_delta5;
			  	    		if(!errorText.equals("")){
			  	    			int row = i+1;
			  	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+first_name+","+middle_name+","+last_name+","+employee_number+","+last4+","+pc+","+rr+","+oc+","+staff_type+","+status+","+email+"<>";
			  	    			numOfError++;
			  	    		}
			  	    		if(errorText.equals("")){
				  	    			if(updateFlag==false){	
					  	    			a1.setDistrictMaster(districtMaster);
					  	    			a1.setFirstName(first_name);
					  	    			a1.setMiddleName(middle_name);
					  	    			a1.setLastName(last_name);
					  	    			a1.setEmailAddress(email);
					  	    			a1.setSSN(last4);
					  	    			a1.setEmployeeCode(employee_number);
					  	    			a1.setFECode(status);
					  	    			a1.setRRCode(rr);
					  	    			a1.setOCCode(oc);
					  	    			a1.setPCCode(pc);
					  	    			a1.setStaffType(staff_type);
					  	    			a1.setSAPId("");
					  	    			a1.setStatus("A");			  	    			
					  	    			a1.setCreatedDateTime(new Date());
					  	    			employeeMasterMap.put(new Integer(""+mapCount),a1);
					  	    			mapCount++;
					  	    			createCount++;
					            	 	statelesSsession.insert(a1);
				  	    			}else{
				  	    				a1.setEmployeeId(emp_id);
				  	    				a1.setDistrictMaster(districtMaster);
				  	    				a1.setFirstName(first_name);
					  	    			a1.setMiddleName(middle_name);
					  	    			a1.setLastName(last_name);
					  	    			a1.setEmailAddress(email);
					  	    			a1.setSSN(last4);
					  	    			a1.setEmployeeCode(employee_number);
					  	    			a1.setFECode(status);
					  	    			a1.setRRCode(rr);
					  	    			a1.setOCCode(oc);
					  	    			a1.setPCCode(pc);
					  	    			a1.setStaffType(staff_type);
					  	    			a1.setSAPId("");
					  	    			a1.setStatus("A");			  	    			
					  	    			a1.setCreatedDateTime(new Date());
					  	    			employeeMasterMap.put(new Integer(""+mapCount),a1);
					  	    			mapCount++;	
					  	    			updateCount++;
					            	 	statelesSsession.update(a1);
				  	    			}
			  	    		}
		  	        	}
			  	    }
				 txOpen.commit();
	     	 	 statelesSsession.close();	     	 	
     	    
	     	 	UserUploadFolderAccess b1 = new UserUploadFolderAccess();
		 	 	b1.setUseuploadFolderId(rowId);
				b1.setDistrictId(districtMaster);
				b1.setFunctionality("employeeUpload");
				b1.setFolderPath(folderPath);
				b1.setNameOfLastFileUploaded(fileName);
				b1.setLastUploadDateTime(getCurrentDateFormart2(lastUploadDateTime));
				b1.setStatus("A");
				userUploadFolderAccessDAO.makePersistent(b1);
				
				FileUploadHistory fileUploadHistory = new FileUploadHistory();
				fileUploadHistory.setUseuploadFolderId(b1);
				fileUploadHistory.setInsertRecord(createCount);
				fileUploadHistory.setUpdateRecord(updateCount);
				fileUploadHistory.setNumOfError(numOfError);
				fileUploadHistory.setDateTime(new Date());
				fileUploadHistoryDAO.makePersistent(fileUploadHistory);

				deleteXlsAndXlsxFile(filePath);
				
				if(final_error_store.equalsIgnoreCase("")){
		 	 		String to= "gourav@netsutra.com";
							
					String subject = fileName+" "+MSGsdpstaffmasterFilefound14;
					String msg = MSGsdpstaffmasterFilefound15;
							msg+=lblFileName1 +" : "+fileName+"\n";
							msg+= lblNewRecords1+ " : "+createCount+"\n";
							msg+=lblUpdateRecords1+" : "+updateCount+"\n";
							msg+= lblUploadDateTime1 +": "+new Date()+"\n";
					String from= "gourav@netsutra.com";
					System.out.println(msg);
					emailerService.sendMail(to, subject, msg);
		 	 	}
				
	     	 	if(!final_error_store.equalsIgnoreCase("")){
	     	 		String content = final_error_store;		    				
    				File file = new File(folderPath+"excelUploadError.txt");
    				if (!file.exists()) {
    					file.createNewFile();
    				}
    				String[] parts = content.split("<>");	    				
    				FileWriter fw = new FileWriter(file.getAbsoluteFile());
    				BufferedWriter bw = new BufferedWriter(fw);
    				bw.write("first_name,middle_name,last_name,employee_number,last4,pc,rr,oc,staff_type,status,email");
    				bw.write("\r\n\r\n");
    				int k =0;
    				for(String cont :parts) {
    					bw.write(cont+"\r\n\r\n");
    				    k++;
    				}
    				bw.close();
    			
    				String to= "gourav@netsutra.com";
    				String subject = fileName+" Upload Error";
    				String msg = "Error in "+fileName+" file";
    				String from= "gourav@netsutra.com";
    				String errorFileName="excelUploadError.txt";
    				emailerService.sendMailWithAttachments(to,subject,from,msg,folderPath,errorFileName);
    				System.out.println("complete text file create");
	     	 	}
	     	 	 
			}catch (Exception e){
				e.printStackTrace();
			}
				return null;
			}
		
		 public static Vector readDataExcelCSV(String fileName) throws FileNotFoundException{
				
				Vector vectorData = new Vector();
				//String filepathhdr=Utility.getValueOfPropByKey("teacherRootPath")+"/uploademployee/TM_MDCPS_staff_delta_20140623_0734.csv";
				String filepathhdr=fileName;
				System.out.println("read csv");
				int rowshdr=0;
				int first_name_count=11;
	            int middle_name_count=11;
	 	        int last_name_count=11;	 	        
	 	        int employee_number_count=11;
	 	        int last4_count=11;
	 	        int pc_count=11;
	 	        int rr_count=11;
	 	        int oc_count=11;
	 	        int staff_type_count=11;
	 	        int status_count=11;
	 	        int email_count=11;
				
			        BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
			        
			        String strLineHdr="";	        
			        String hdrstr="";
			        
			        StringTokenizer sthdr=null;
			        StringTokenizer stdtl=null;
			        int lineNumberHdr=0;
			        try{
			            String indexCheck="";
			            while((strLineHdr=brhdr.readLine())!=null){               
			            	Vector vectorCellEachRowData = new Vector();
			                int cIndex=0;
			                boolean cellFlag=false,dateFlag=false;
			                Map<String,String> mapCell = new TreeMap<String, String>();
			                int i=1;
			                
			                String[] parts = strLineHdr.split(",");
			                
			                for(int j=0; j<parts.length; j++) {
			                	hdrstr=parts[j];
			                	cIndex=j;
			                	
		                        
			                       if(hdrstr.toString().trim().equalsIgnoreCase("first_name")){
				                    	first_name_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(first_name_count==cIndex){
					            		cellFlag=true;
					            		dateFlag=true;
					            	}
					            	
					            	if(hdrstr.toString().trim().equalsIgnoreCase("middle_name")){
					            		middle_name_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(middle_name_count==cIndex){
					            		cellFlag=true;
					            	}
					            	  
					            	if(hdrstr.toString().trim().equalsIgnoreCase("last_name")){
					            		last_name_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(last_name_count==cIndex){
					            		cellFlag=true;
					            	}
					            	
					            	if(hdrstr.toString().trim().equalsIgnoreCase("employee_number")){
					            		employee_number_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(employee_number_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("last4")){
					            		last4_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(last4_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("pc")){
					            		pc_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(pc_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("rr")){
					            		rr_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(rr_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("oc")){
					            		oc_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(oc_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("staff_type")){
					            		staff_type_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(staff_type_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("status")){
					            		status_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(status_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("email")){
					            		email_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(email_count==cIndex){
					            		cellFlag=true;
					            	} 
			                        
			                    	if(cellFlag){
			                    		if(!dateFlag){
			                    			try{
			                    				mapCell.put(cIndex+"",hdrstr);
			                    			}catch(Exception e){
			                    				mapCell.put(cIndex+"",hdrstr);
			                    			}
			                    		}else{
			                    			mapCell.put(cIndex+"",hdrstr);
			                    		}
			                    	}
			                }
			                    
			                    vectorCellEachRowData=cellValuePopulate(mapCell);
			                    vectorData.addElement(vectorCellEachRowData);
			                lineNumberHdr++;
			            }
			        }
			        catch(Exception e){
			            System.out.println(e.getMessage());
			        }
			        System.out.println("::::::::: read data from csv ::::::::::       "+vectorData);       
				return vectorData;
			}
		public static Vector cellValuePopulate(Map<String, String> mapCell){
			 Vector vectorCellEachRowData = new Vector();
			 Map<String,String> mapCellTemp = new TreeMap<String, String>();
			 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false,flag9=false,flag10=false;
			 for(Map.Entry<String, String> entry : mapCell.entrySet()){
				 String key=entry.getKey();
				String cellValue=null;
				if(entry.getValue()!=null)
					cellValue=entry.getValue().trim();
				
				if(key.equals("0")){
					mapCellTemp.put(key, cellValue);
					flag0=true;
				}
				if(key.equals("1")){
					flag1=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("2")){
					flag2=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("3")){
					flag3=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("4")){
					flag4=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("5")){
					flag5=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("6")){
					flag6=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("7")){
					flag7=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("8")){
					flag8=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("9")){
					flag9=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("10")){
					flag10=true;
					mapCellTemp.put(key, cellValue);
				}
			 }
			 if(flag0==false){
				 mapCellTemp.put(0+"", "");
			 }
			 if(flag1==false){
				 mapCellTemp.put(1+"", "");
			 }
			 if(flag2==false){
				 mapCellTemp.put(2+"", "");
			 }
			 if(flag3==false){
				 mapCellTemp.put(3+"", "");
			 }
			 if(flag4==false){
				 mapCellTemp.put(4+"", "");
			 }
			 if(flag5==false){
				 mapCellTemp.put(5+"", "");
			 }
			 if(flag6==false){
				 mapCellTemp.put(6+"", "");
			 }
			 if(flag7==false){
				 mapCellTemp.put(7+"", "");
			 }
			 if(flag8==false){
				 mapCellTemp.put(8+"", "");
			 }
			 if(flag9==false){
				 mapCellTemp.put(9+"", "");
			 }
			 if(flag10==false){
				 mapCellTemp.put(10+"", "");
			 }
			 			 
			 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
				 vectorCellEachRowData.addElement(entry.getValue());
			 }
			 return vectorCellEachRowData;
}
		
		public String deleteXlsAndXlsxFile(String fileName){
			
			String filePath="";
			File file=null;
			String returnDel="";
			try{			
				filePath=fileName;
				file = new File(filePath);
				file.delete();
				returnDel="deleted";
			}catch(Exception e){}
			return returnDel;
		}
		
		public static Date getCurrentDateFormart2(String oldDateString) throws ParseException{

			String OLD_FORMAT = "yyyy-MM-dd HH:mm:ss";
			String NEW_FORMAT = "yyyy-MM-dd HH:mm:ss";
			String newDateString;
			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);

			SimpleDateFormat stodate = new SimpleDateFormat(NEW_FORMAT);
			Date date = stodate.parse(newDateString);

			return date;
		}
		
	

}
