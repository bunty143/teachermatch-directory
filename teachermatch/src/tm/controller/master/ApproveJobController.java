package tm.controller.master;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.DistrictApprovalGroups;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobApprovalHistory;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictApprovalGroupsDAO;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobApprovalHistoryDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.MasterSubjectMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.ApproveJobAjax;
import tm.utility.Utility;
@Controller
public class ApproveJobController {
	
	@Autowired
	private DistrictApprovalGroupsDAO districtApprovalGroupsDAO;
	@Autowired
	private ApproveJobAjax approveJobAjax;
	@Autowired
	private JobApprovalHistoryDAO jobApprovalHistoryDAO;
	
	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO; 

	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void SchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}

	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}

	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;

	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;

	@Autowired 
	private SubjectMasterDAO subjectMasterDAO;
	public void setSubjectMasterDAO(SubjectMasterDAO subjectMasterDAO) {
		this.subjectMasterDAO = subjectMasterDAO;
	}		
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setuserMasterDAO(
			UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO)
	{
		this.districtMasterDAO=districtMasterDAO;
	}
	@Autowired
	private DistrictKeyContactDAO  districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO)
	{
		this.districtKeyContactDAO=districtKeyContactDAO;
	}
	@Autowired
	private JobApprovalHistoryDAO approvalHistoryDAO;
	public void setJobApprovalHistoryDAO(JobApprovalHistoryDAO approvalHistoryDAO)
	{
		this.approvalHistoryDAO=approvalHistoryDAO;
	}
	@RequestMapping(value="/approveJob.do",method=RequestMethod.GET)	
	public String approveJobDetail(ModelMap map,HttpServletRequest request)
	{
		int approveButton=0;

		String id = request.getParameter("id");
		if(id!=null)
		{
			HttpSession session = request.getSession(false);

			//String encId = Utility.encodeText(id.trim());
			String encId = id.trim();
			try {
				String forMated = Utility.decodeBase64(encId);
				String arr[] = forMated.split("###");
				
				int JobOrderType=2;
				int userId=0;
				int jobId=0;
				JobOrder jobOrder=null;	
				userId=Utility.decryptNo(Integer.parseInt(arr[1]));			
				map.addAttribute("userId", userId);
				jobId=Utility.decryptNo(Integer.parseInt(arr[0]));				
				map.addAttribute("jobId", jobId);

				if(jobId==0){
					jobOrder	=	new JobOrder();
				}else{
					jobOrder	=jobOrderDAO .findById(jobId, false, false);
				}
				//System.out.println("jobOrder::: "+jobOrder.getApprovalBeforeGoLive());
				UserMaster userMaster=null;		
				userMaster=userMasterDAO.findById(userId, false, false);
				
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
				System.out.println("session:- "+session);
				System.out.println("userId:- "+userId+", jobId:- "+jobId);
				if(session == null ||session.getAttribute("userMaster")==null) 
				{
					System.out.println("inside 1st Condition \napprovejobsignin:-" );
					//map.put("redirectURL", redirectURL);
					map.remove("userId");
					map.remove("jobId");
					return "redirect:approvejobsignin.do?id="+id;
					//redirect loginpage and set parameter to request
				}
				
				UserMaster userMaster322 = (UserMaster)session.getAttribute("userMaster");
				if(!userMaster322.getUserId().equals(userMaster.getUserId()))
				{
					map.remove("userId");
					map.remove("jobId");
					System.out.println("inside 2nd Condition");
					session.invalidate();
					//map.put("redirectURL", redirectURL);
					String msg="*This link is associated with some other account. Please use the same account details to login and approve the Job.";
					return "redirect:approvejobsignin.do?id="+id+"&msg="+msg;
					//redirect loginpage and set parameter to request
				}
				
				
				session = request.getSession(true);
				if(jobOrder==null || userMaster==null)
				{	
					System.out.println("jobOrder   "+  jobOrder+"        userMaster   "+userMaster);
					return "approveJob";			
				}
				session.setAttribute("userMaster", userMaster);

				DistrictMaster districtMaster = null;
				DistrictKeyContact districtKeyContact = null;
				List<JobApprovalHistory> approvalHistory=null;
				int noOfApprovalNeeded=0;

				if(jobOrder.getDistrictMaster()!=null){
					districtMaster=jobOrder.getDistrictMaster();
				}
				int approvalBeforeGoLive=0;
				List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
				if(jobOrder.getApprovalBeforeGoLive()!=null)
				{
					approvalBeforeGoLive=jobOrder.getApprovalBeforeGoLive();	
				}	
				System.out.println("approvalBeforeGoLive           " +approvalBeforeGoLive);
				if(approvalBeforeGoLive!=1)
				{			
					boolean districtapprovalBeforeGoLive=districtMaster.getApprovalBeforeGoLive()==null?false:true;
					noOfApprovalNeeded=districtMaster.getNoOfApprovalNeeded();			
					if(districtapprovalBeforeGoLive)
					{
						districtKeyContactList = districtKeyContactDAO.findByContactType(districtMaster,"Job Approval");
					}
					UserMaster user = null;
					DistrictKeyContact userKeyContact = null;
					for (DistrictKeyContact districtKeyContact1 : districtKeyContactList) {				
						if(districtKeyContact1.getUserMaster().getUserId().equals(userId))
						{
							user = districtKeyContact1.getUserMaster();
							userKeyContact = districtKeyContact1;
						}
					}
					System.out.println("user:: "+user);
					if(user==null && userMaster.getEntityType()!=3)
					{
						approveButton=3;//You are not authorized
						return "approveJob";
					}else
					{
						JobCategoryMaster jobCategoryMaster = jobOrder.getJobCategoryMaster();
						
						
						if((userKeyContact!=null && jobCategoryMaster!=null && jobCategoryMaster.getApprovalBeforeGoLive()!=null && jobCategoryMaster.getBuildApprovalGroup()!=null && jobCategoryMaster.getApprovalBeforeGoLive() && jobCategoryMaster.getBuildApprovalGroup()) || (jobCategoryMaster.getApprovalByPredefinedGroups()!=null && jobCategoryMaster.getApprovalByPredefinedGroups()) )
						{
							if (!(jobOrder.getDistrictMaster().getSkipApprovalProcess()!=null && jobOrder.getDistrictMaster().getSkipApprovalProcess())){
						//	if (!jobOrder.getDistrictMaster().getDistrictId().equals(806900)) {
							Integer groupId = Utility.decryptNo(Integer.parseInt(arr[2]));
							DistrictApprovalGroups currentApprovalGroup = districtApprovalGroupsDAO.findById(groupId, false, false);
							
							approvalHistory = jobApprovalHistoryDAO.findByCriteria( Restrictions.and(Restrictions.eq("districtApprovalGroups", currentApprovalGroup), Restrictions.eq("jobOrder", jobOrder)) );
							noOfApprovalNeeded=1;
							
							System.out.println("groupId:- "+groupId);}
						}
						else
						{
							approvalHistory=approvalHistoryDAO.findByUserIdAndJobId(userMaster,jobOrder);
						}
						
					//	System.out.println("approvalHistory.size()  "+approvalHistory.size()+" noOfApprovalNeeded "+noOfApprovalNeeded);
						if (!(jobOrder.getDistrictMaster().getSkipApprovalProcess()!=null && jobOrder.getDistrictMaster().getSkipApprovalProcess())){
					//	if (!jobOrder.getDistrictMaster().getDistrictId().equals(806900)) {
						if(noOfApprovalNeeded<=approvalHistory.size())
							approveButton=2;//already approved
						else
							approveButton=1;
						}else{
							int status=approveJobAjax.isApproveOrNotforAdams(jobOrder,request);
							if (status==1)
								approveButton=1;
							else 
								approveButton=2;
							
						}
						if(approvalHistory!=null &&approvalHistory.size()>0)
						for (JobApprovalHistory jobApprovalHistory : approvalHistory){
							if(jobApprovalHistory.getUserMaster().getUserId().equals(userId))
								if( !( (userKeyContact!=null && jobCategoryMaster!=null && jobCategoryMaster.getApprovalBeforeGoLive()!=null && jobCategoryMaster.getBuildApprovalGroup()!=null && jobCategoryMaster.getApprovalBeforeGoLive() && jobCategoryMaster.getBuildApprovalGroup()) || (jobCategoryMaster.getApprovalByPredefinedGroups()!=null && jobCategoryMaster.getApprovalByPredefinedGroups() && userMaster.getEntityType().equals(3))) )
									approveButton=4;
						}
						
					}
				}
				else
				{
					approveButton=2;	// already approved		
				}
				map.addAttribute("approveButton",approveButton);	

				int roleId=0;
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=0){
					roleId=userMaster.getRoleId().getRoleId();
				}	

				boolean miamiShowFlag=true;
				try{
					if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
						miamiShowFlag=false;
					}
				}catch(Exception e){e.printStackTrace();}
				map.addAttribute("miamiShowFlag", miamiShowFlag);

				String roleAccess=null;
				try{
					if(JobOrderType==2){
						roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
					}else{
						roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
					}
				}catch(Exception e){e.printStackTrace();

				}		
				map.addAttribute("roleAccess", roleAccess);			
				int radioDA =0;
				int radioSA =0;
				List<DistrictRequisitionNumbers> avlbList = new ArrayList<DistrictRequisitionNumbers>();
				List<JobRequisitionNumbers> attachedList = new ArrayList<JobRequisitionNumbers>();
				List<JobRequisitionNumbers> attachedSchoolList = new ArrayList<JobRequisitionNumbers>();
				StringBuffer schoolReqList=new StringBuffer(); 

				Criterion criterion  = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion1 = Restrictions.isNull("schoolMaster");
				Criterion criterion2 = Restrictions.isNotNull("schoolMaster");
				Criterion criterion3 = Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
				Criterion criterion4 = Restrictions.eq("isUsed", false);		
				String location ="";		
				try
				{
					if(jobOrder!=null && jobOrder.getSchool().size()>0)
					{
						if(jobOrder.getSchool().get(0).getLocationCode()!=null && jobOrder.getSchool().get(0).getLocationCode()!="")
							location =jobOrder.getSchool().get(0).getSchoolName()+" ("+jobOrder.getSchool().get(0).getLocationCode()+")";
						else
							location = jobOrder.getSchool().get(0).getSchoolName();
					}

				}catch(Exception e)
				{
					e.printStackTrace();
				}	

				if(jobOrder!=null)
				{
					avlbList = districtRequisitionNumbersDAO.findByCriteria(criterion3,criterion4);
					map.addAttribute("avlbList",avlbList);

					if(jobOrder!=null && jobOrder.getNoSchoolAttach()!=null)
					{	
						attachedList = jobRequisitionNumbersDAO.findByCriteria(criterion,criterion1);

						for(JobRequisitionNumbers jbrn : attachedList)
						{
							if(jbrn.getStatus()==1)
							{
								radioDA=1;
								break;
							}
						}
					}
					else if(jobOrder !=null && jobOrder.getSelectedSchoolsInDistrict()!=null)
					{
						attachedSchoolList = jobRequisitionNumbersDAO.findByCriteria(criterion,criterion2);
						List<JobRequisitionNumbers> jobreqList=jobRequisitionNumbersDAO.findByCriteria(criterion,criterion2);
						Map<Long, Long>tempSchooMap=new HashMap<Long, Long>();
						for(JobRequisitionNumbers  jrq:jobreqList){
							tempSchooMap.put(jrq.getSchoolMaster().getSchoolId(), jrq.getSchoolMaster().getSchoolId());
						}
						Set<Map.Entry<Long, Long>> set=tempSchooMap.entrySet();
						Iterator<Entry<Long, Long>>iter=set.iterator();
						while(iter.hasNext()){
							Entry<Long,Long>entry=iter.next();
							Long schoolId=tempSchooMap.get(entry.getKey());
							schoolReqList.append(schoolId+",");
						}
						if(schoolReqList.length()>0){
							schoolReqList.substring(0, schoolReqList.length()-1).toString();	
						}
						for(JobRequisitionNumbers jbrn : attachedSchoolList)
						{
							if(jbrn.getStatus()==1)
							{
								radioSA=1;
								break;
							}
						}
					}
					if(radioDA==1)
					{
						map.addAttribute("disableRadioSA", 0);
					}
					else if(radioSA == 1)
					{
						map.addAttribute("disableRadioDA", 0);
					}

					map.addAttribute("attachedList",attachedList);
					map.addAttribute("attachedSchoolList",attachedSchoolList);
					if(schoolReqList.length()!=0 && !schoolReqList.equals("")){
						map.addAttribute("requisitionSchoolList",schoolReqList.substring(0, schoolReqList.length()-1).toString());
					}
				}

				map.addAttribute("jobStartDate",Utility.getCalenderDateFormart(jobOrder.getJobStartDate()+""));
				map.addAttribute("jobEndDate",Utility.getCalenderDateFormart(jobOrder.getJobEndDate()+""));		
				map.addAttribute("jobType", jobOrder.getJobType());
				Criterion criterion5=Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
				List<GeoZoneMaster>zoneMasters=geoZoneMasterDAO.findByCriteria(criterion5);
				StringBuffer geozones=new StringBuffer();


				for(GeoZoneMaster zone:zoneMasters)
				{	
					if(jobOrder.getGeoZoneMaster()!=null)
					{	
						if(jobOrder.getGeoZoneMaster().getGeoZoneId().equals(zone.getGeoZoneId())){
							geozones.append("<option  value='"+zone.getGeoZoneId()+"'>"+zone.getGeoZoneName()+"</option>");
						}
					}	
				}

				map.addAttribute("jobzones",geozones.toString());
				map.addAttribute("zoneLableRed","show");


				if(jobOrder.getDistrictMaster().getIsReqNoRequired())
					map.addAttribute("isReqNoFlagByDistrict",1);
				else
					map.addAttribute("isReqNoFlagByDistrict",0);

				if(userMaster.getEntityType()==3 && JobOrderType==2){
					map.addAttribute("addJobFlag",false);
				}else {
					map.addAttribute("addJobFlag",true);
				}
				map.addAttribute("expHireNotEqualToReqNoFlag",jobOrder.getIsExpHireNotEqualToReqNo());

				if(userMaster.getEntityType()==2 && JobOrderType==2){
					try{
						if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
							map.addAttribute("DAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
						}else{
							map.addAttribute("DAssessmentUploadURL",null);
						}
					}catch(Exception e){
						map.addAttribute("DAssessmentUploadURL",null);
					}
					map.addAttribute("SAssessmentUploadURL","");
					map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
					map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
					try{	
						map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL());
					}catch(Exception e){
						map.addAttribute("DSAssessmentUploadURL",null);
					}
					map.addAttribute("PageFlag","1");
					map.addAttribute("SchoolDistrictNameFlag","1");
					map.addAttribute("SchoolName",null);
					map.addAttribute("SchoolId",0);
				}else if(userMaster.getEntityType()==3 && JobOrderType==3){

					try{
						map.addAttribute("DAssessmentUploadURL",jobOrder.getSchool().get(0).getDistrictId().getAssessmentUploadURL().trim());
					}catch(Exception e){
						map.addAttribute("DAssessmentUploadURL",null);

					}
					try{
						if(jobOrder.getSchool().get(0).getAssessmentUploadURL()!=null){
							map.addAttribute("SAssessmentUploadURL",jobOrder.getSchool().get(0).getAssessmentUploadURL().trim());	
						}else{
							map.addAttribute("SAssessmentUploadURL",null);
						}
					}catch(Exception e){
						map.addAttribute("SAssessmentUploadURL",null);

					}
					map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
					map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
					map.addAttribute("SchoolName","location");

					map.addAttribute("SchoolId",jobOrder.getSchool().get(0).getSchoolId());

					try{
						if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
							map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
						}else{
							map.addAttribute("DSAssessmentUploadURL",null);
						}
					}catch(Exception e){
						map.addAttribute("DSAssessmentUploadURL",null);
					}
					map.addAttribute("PageFlag","1");
					map.addAttribute("SchoolDistrictNameFlag","1");
				}else{
					if(JobOrderType==2){
						map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());

						try{	
							if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
								map.addAttribute("DAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());	
							}else{
								map.addAttribute("DAssessmentUploadURL",null);	
							}
						}catch(Exception e){
							map.addAttribute("DAssessmentUploadURL",null);
						}

						map.addAttribute("SAssessmentUploadURL","");
						map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
						try{
							map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL());
						}catch(Exception e){
							map.addAttribute("DSAssessmentUploadURL",null);
						}
						map.addAttribute("SchoolName",null);
						map.addAttribute("SchoolId",0);
					}else if(JobOrderType==3){
						map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
						map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
						try{
							map.addAttribute("SchoolName",location);
						}catch(Exception e){
							map.addAttribute("SchoolName",null);
						}
						try{	
							map.addAttribute("DAssessmentUploadURL",jobOrder.getSchool().get(0).getDistrictId().getAssessmentUploadURL().trim());
						}catch(Exception e){
							map.addAttribute("DAssessmentUploadURL",null);
						}
						try{	
							if(jobOrder.getSchool().get(0).getAssessmentUploadURL()!=null){
								map.addAttribute("SAssessmentUploadURL",jobOrder.getSchool().get(0).getAssessmentUploadURL().trim());
							}else{
								map.addAttribute("SAssessmentUploadURL",null);
							}
						}catch(Exception e){
							map.addAttribute("SAssessmentUploadURL",null);
						}
						try{
							map.addAttribute("SchoolId",jobOrder.getSchool().get(0).getSchoolId());
						}catch(Exception e){
							map.addAttribute("SchoolId",null);
						}


						try{
							if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
								map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
							}else{
								map.addAttribute("DSAssessmentUploadURL",null);
							}
						}catch(Exception e){
							map.addAttribute("DSAssessmentUploadURL",null);
						}

					}
					map.addAttribute("PageFlag","0");
					map.addAttribute("SchoolDistrictNameFlag","0");
				}

				if(jobOrder.getSelectedSchoolsInDistrict()!=null && jobOrder.getSelectedSchoolsInDistrict()==1){
					map.addAttribute("allSchoolGradeDistrictVal",2);
				}else{
					map.addAttribute("allSchoolGradeDistrictVal",1);
				}
				map.addAttribute("DistrictExitURL",jobOrder.getExitURL());
				map.addAttribute("DistrictExitMessage",jobOrder.getExitMessage());
				map.addAttribute("districtHiddenId",jobOrder.getDistrictMaster().getDistrictId());
				map.addAttribute("JobOrderType",JobOrderType);
				map.addAttribute("jobOrder",jobOrder);
				if(jobOrder.getDistrictMaster().getDistrictId().equals(1200390) || jobOrder.getDistrictMaster().getDistrictId().equals(7800043)){
					map.addAttribute("JobPostingURL",Utility.getValueOfPropByKey("basePath")+"applyteacherjob.do?jobId="+jobOrder.getJobId());
				} else {
					map.addAttribute("JobPostingURL",Utility.getShortURL(Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId()));
				}

				int exitURLMessageVal=1;
				if(jobOrder.getFlagForMessage()!=null)
					if(jobOrder.getFlagForMessage()!=0){
						if(jobOrder.getFlagForMessage()==1){
							exitURLMessageVal=2;
						}
					}
				int attachJobAssessmentVal=1;
				if(jobOrder.getIsJobAssessment()){
					if(jobOrder.getAttachDefaultDistrictPillar()==1){
						attachJobAssessmentVal=1;
					}else if(jobOrder.getAttachDefaultSchoolPillar()==1){
						attachJobAssessmentVal=3;
					}else if(jobOrder.getAttachDefaultJobSpecificPillar()==1){
						attachJobAssessmentVal=4;
					}else if(jobOrder.getAttachNewPillar()==1){
						attachJobAssessmentVal=2;
					}	
				}
				if(JobOrderType==2){
					map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
					map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
				}else if(JobOrderType==3){
					map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
					map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
				}else{
					map.addAttribute("districtRootPath",null);
					map.addAttribute("schoolRootPath",null);
				}
				map.addAttribute("attachJobAssessmentVal",attachJobAssessmentVal);
				map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
				if(userMaster.getEntityType()==3 && JobOrderType==2){
					int noOfExpHires=schoolInJobOrderDAO.findNoOfExpHires(jobOrder,userMaster.getSchoolId(),0);
					if(noOfExpHires!=0){
						map.addAttribute("noOfExpHires",noOfExpHires);
					}else{
						map.addAttribute("noOfExpHires","");
					}
				}else{
					int noOfExpHires=schoolInJobOrderDAO.findNoOfExpHires(jobOrder,userMaster.getSchoolId(),1);
					if(noOfExpHires!=0){
						map.addAttribute("noOfExpHires",noOfExpHires);
					}else{
						map.addAttribute("noOfExpHires","");
					}
				}
				map.addAttribute("dateTime",Utility.getDateTime());
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				map.addAttribute("windowFunc",windowFunc);	

				/*=== values to dropdown from jobCategoryMaster Table ===*/
				List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByJob(jobOrder);
				map.addAttribute("jobCategoryMasterlst",jobCategoryMasterlst);	



				List<SubjectMaster> subjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(districtMaster);

				map.addAttribute("subjectList",subjectMasters);
				List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
				listStateMaster = stateMasterDAO.findAllStateByOrder();
				map.addAttribute("listStateMaster", listStateMaster);

				map.addAttribute("entityType", userMaster.getEntityType());
				if(userMaster.getEntityType()==2 )
					map.addAttribute("stateId",jobOrder.getDistrictMaster().getStateId().getStateId());
				else
					if(userMaster.getEntityType()==3)
						map.addAttribute("stateId",jobOrder.getDistrictMaster().getStateId().getStateId());
					else
						if(userMaster.getEntityType()==1 && jobOrder!=null)
						{
							if(JobOrderType==2)
							{
								map.addAttribute("stateId",jobOrder.getDistrictMaster().getStateId().getStateId());
							}
							else
							{
								try{
									map.addAttribute("stateId",jobOrder.getSchool().get(0).getStateMaster().getStateId());
								}catch(Exception e){e.printStackTrace();

								}
							}
						}

				try{
					String logType="";
					if(JobOrderType==2){
						if(jobOrder.getDistrictMaster()!=null)
							userMaster.setDistrictId(jobOrder.getDistrictMaster());
						logType="Enter in edit District Job Order";
					}else{
						if(jobOrder.getDistrictMaster()!=null)
							userMaster.setDistrictId(jobOrder.getDistrictMaster());


						logType="Enter in edit School Job Order";
					}			

				}catch(Exception e){
					e.printStackTrace();
				}

			}catch (Exception e) {e.printStackTrace();
				// TODO: handle exception
			}
		}else
		{
			return "approveJob";
		}



		return "approveJob";
	}
}
