package tm.controller.master;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ftp.parser.ParserInitializationException;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobApprovalProcess;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.JobWiseApprovalProcess;
import tm.bean.OrderBySqlFormula;
import tm.bean.SchoolInJobOrder;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.JobTitleWithDescription;
import tm.bean.master.MasterSubjectMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.master.TimeZoneMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobApprovalProcessDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.JobWiseApprovalProcessDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.JobTitleWithDescriptionDAO;
import tm.dao.master.MasterSubjectMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.TimeZoneMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.services.teacher.DashboardAjax;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

@Controller 
public class ManageJobOrdersController 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	String msgEnterDistrictJobOrder = Utility.getLocaleValuePropByKey("msgEnterDistrictJobOrder", locale);
	String msgEnterSchoolJobOrder = Utility.getLocaleValuePropByKey("msgEnterSchoolJobOrder", locale);
	String msgEnterApplicantPool = Utility.getLocaleValuePropByKey("msgEnterApplicantPool", locale);

	
	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO; 
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) {
		this.districtKeyContactDAO = districtKeyContactDAO;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
	{
		this.districtMasterDAO = districtMasterDAO;
	}
		
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;

	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}

	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) {
		this.cityMasterDAO = cityMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void SchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	
	@Autowired
	private JobTitleWithDescriptionDAO jobTitleWithDescriptionDAO;
	
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired 
	private SubjectMasterDAO subjectMasterDAO;
	public void setSubjectMasterDAO(SubjectMasterDAO subjectMasterDAO) {
		this.subjectMasterDAO = subjectMasterDAO;
	}
	
	@Autowired
	private MasterSubjectMasterDAO masterSubjectMasterDAO;
	public void setMasterSubjectMasterDAO(
			MasterSubjectMasterDAO masterSubjectMasterDAO) {
		this.masterSubjectMasterDAO = masterSubjectMasterDAO;
	}
	@Autowired
	private JobApprovalProcessDAO jobApprovalProcessDAO;

	public void setJobApprovalProcessDAO(JobApprovalProcessDAO jobApprovalProcessDAO) {
		this.jobApprovalProcessDAO = jobApprovalProcessDAO;
	}
	
	@Autowired
	private JobWiseApprovalProcessDAO jobWiseApprovalProcessDAO;
	
	public void setJobWiseApprovalProcessDAO(
			JobWiseApprovalProcessDAO jobWiseApprovalProcessDAO) {
		this.jobWiseApprovalProcessDAO = jobWiseApprovalProcessDAO;
	}
	
	//Ajay Jain
	@Autowired
	private ServletContext servletContext;
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	//timezone field in edit district account information page start
	@Autowired
	private TimeZoneMasterDAO timeZoneMasterDAO;
	public void setTimeZoneMasterDAO(TimeZoneMasterDAO timeZoneMasterDAO) {
		this.timeZoneMasterDAO = timeZoneMasterDAO;
	}
	//timezone field in edit district account information page end
	
	
	@RequestMapping(value="/showFile.do", method=RequestMethod.GET)
	public String showFile(ModelMap map,HttpServletRequest request )
	{
		request.getSession();
		WebContext context;
		context = WebContextFactory.get();
		String path="";
		String source="";
		String target="";
		String type=null;
		if(request.getParameter("type")!=null){
			type=request.getParameter("type");
		}
		String id=null;
		if(request.getParameter("id")!=null){
			id=request.getParameter("id");
		}
		String  file=null;
		if(request.getParameter("file")!=null){
			file=request.getParameter("file");
		}
		try 
		{
			if(type.equals("2")){
				source = Utility.getValueOfPropByKey("districtRootPath")+id+"/"+file;
				target = request.getRealPath("/")+"/"+"/district/"+id+"/";
			}else{
				source = Utility.getValueOfPropByKey("schoolRootPath")+id+"/"+file;
				target = request.getRealPath("/")+"/"+"/school/"+id+"/";
			}
		    File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        FileUtils.copyFile(sourceFile, targetFile);
	        if(type.equals("2")){
	        	  path =  Utility.getValueOfPropByKey("contextBasePath")+"/district/"+id+"/"+file; 	
	        }else{
	        	  path =  Utility.getValueOfPropByKey("contextBasePath")+"/school/"+id+"/"+file;
	        }
	        map.addAttribute("path", path);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "showfile";
	}
	@RequestMapping(value="/managejoborders.do", method=RequestMethod.GET)
	public String manageDistrictGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println(":::::::::::::::::::::::::Manage Job Orders.do   public String manageDistrictGET(ModelMap map,HttpServletRequest request)::::::::::");
		try 
		{
			
			UserMaster userMaster=null;
			DistrictMaster districtMaster=null;
			
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			if(userMaster.getHeadQuarterMaster()!=null)
			{
			if(userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
				map.addAttribute("NCDPICheack", userMaster.getHeadQuarterMaster().getHeadQuarterId());
			    if(userMaster.getDistrictId() != null)
			      map.addAttribute("hrIntegrated", userMaster.getDistrictId().getHrIntegrated());
			}
			String requisitionNumber = request.getParameter("requisitionNumber")==null? "" : request.getParameter("requisitionNumber");
			map.addAttribute("requisitionNumber",requisitionNumber);
			
			String roleAccess=null;
			String jobAuthKey=(Utility.randomString(8)+Utility.getDateTime());
			map.addAttribute("jobAuthKey",jobAuthKey);

			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,24,"managejoborders.do",0);
			}catch(Exception e){
				//e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			
			boolean miamiShowFlag=true;
			try{
				if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
					miamiShowFlag=false;
				}
			}catch(Exception e){}		
			map.addAttribute("miamiShowFlag", miamiShowFlag);
			if(userMaster.getEntityType()==2 || userMaster.getEntityType()==3){
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictOrSchoolName",null);
			}
			map.addAttribute("JobOrderType","2");
			if(userMaster.getEntityType()==3){
				districtMaster=districtMasterDAO.findById(userMaster.getDistrictId().getDistrictId(),false, false);

				map.addAttribute("writePrivilegFlag",districtMaster.getWritePrivilegeToSchool());
				map.addAttribute("schoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("schoolId",userMaster.getSchoolId().getSchoolId());
				if(userMaster.getDistrictId().getsACreateDistJOb()!=null){
					if(userMaster.getDistrictId().getsACreateDistJOb()){
						map.addAttribute("addJobFlag",true);
					}else{
						map.addAttribute("addJobFlag",false);	
					 }
					}else{
						map.addAttribute("addJobFlag",false);
					}
			}else {
				map.addAttribute("addJobFlag",true);
			}
			try{
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in District Job Order");
			}catch(Exception e){
				//e.printStackTrace();
			
			}
			
			//Ajay Jain
			try{				
				districtMaster=districtMasterDAO.findById(userMaster.getDistrictId().getDistrictId(),false, false);
				map.addAttribute("districtMaster", districtMaster);	
				if(districtMaster!=null && districtMaster.getDistrictId()==804800){
					String source = "";
					String target = "";
					String path = "";
					source = Utility.getValueOfPropByKey("districtRootPath")+districtMaster.getDistrictId()+"/"+districtMaster.getLogoPath();
					target = servletContext.getRealPath("/")+"/"+"/district/"+districtMaster.getDistrictId()+"/";	
					File sourceFile = new File(source);
					File targetDir = new File(target);
					if(!targetDir.exists())
						targetDir.mkdirs();	
					File targetFile = new File(targetDir+"/"+sourceFile.getName());	
					if(sourceFile.exists())
					{
						FileUtils.copyFile(sourceFile, targetFile);
						path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtMaster.getDistrictId()+"/"+sourceFile.getName();
					}
					map.addAttribute("logoPath", path);					
				}					
			} 
			catch (Exception e) {
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "managejoborders";
	}
	@RequestMapping(value="/schooljoborders.do", method=RequestMethod.GET)
	public String manageSchoolGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,25,"schooljoborders.do",0);
			}catch(Exception e){
				//e.printStackTrace();
			}
			boolean miamiShowFlag=true;
			try{
				if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
					miamiShowFlag=false;
				}
			}catch(Exception e){}
			
			map.addAttribute("miamiShowFlag", miamiShowFlag);
			map.addAttribute("roleAccess", roleAccess);
			if(userMaster.getEntityType()==3){
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				
				map.addAttribute("schoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("schoolId",userMaster.getSchoolId().getSchoolId());
				map.addAttribute("PageFlag","1");
			}else if(userMaster.getEntityType()==2){
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				map.addAttribute("PageFlag","1");
			}else{
				map.addAttribute("DistrictOrSchoolName",null);
				map.addAttribute("PageFlag","0");
			}
			map.addAttribute("JobOrderType","3");
			map.addAttribute("addJobFlag",true);
			try{
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in School Job Order");
			}catch(Exception e){
				//e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "managejoborders";
	}
	@Transactional(readOnly=true)
	@RequestMapping(value="/addeditjoborderold.do", method=RequestMethod.GET)
	public String AddEditJobAction(ModelMap map,HttpServletRequest request)
	{
		String pageName="addeditjoborder";
		try 
		{
			//Added by Gaurav Kumar
			Date date = new Date();
	        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	        String dateStr = format.format(date).toString().substring(0,4);
	       
	        //dateStr = (Integer.parseInt(dateStr)+1)+"-"+(Integer.parseInt(dateStr)+2)+" "+Utility.getLocaleValuePropByKey("lblSchoolYear", locale);
	        dateStr = (Integer.parseInt(dateStr))+"-"+(Integer.parseInt(dateStr)+1)+" "+Utility.getLocaleValuePropByKey("lblSchoolYear", locale);
	        //String yr2 = format.parse(date2.toString()).toString();
	        System.out.println("::::::::::::::::::::::::::Formatted Date:::::::::::::::::::::"+dateStr);
	        List<String> positionStartList = new ArrayList<String>();
	        positionStartList.add(Utility.getLocaleValuePropByKey("lblCurrentImmediate", locale));
	        positionStartList.add(dateStr);
	        //End
	        
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			map.addAttribute("entityType", userMaster.getEntityType());
			map.addAttribute("userMaster", userMaster);
		
			if(userMaster!=null && userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(804800) || userMaster.getDistrictId().getDistrictId().equals(806900))){
				pageName="addeditjoborderjeffco";	
			}
			
			
			int JobOrderType=0;
			if(request.getParameter("JobOrderType")!=null){
				JobOrderType=Integer.parseInt(request.getParameter("JobOrderType"));
			}			
			map.addAttribute("JobId", 0);	
			map.addAttribute("positionStartList", positionStartList);
			String roleAccess=null;
			try{
				if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
				}
			}catch(Exception e){
				//e.printStackTrace();
			}
			
			/*Gaurav Kumar Edit By Dhananjay Verma*/ 
			try{
			if(userMaster!=null && userMaster.getDistrictId()!=null){
				
				List<JobApprovalProcess> jobApprovalProcessList= jobApprovalProcessDAO.getJobApprovalProcessList();
				
				map.addAttribute("jobApprovalProcessList",jobApprovalProcessList);
				
				
			} 
			}catch(Exception e){
				e.printStackTrace();
			}
			
			try{
				String logType="";
				if(JobOrderType==2)
					logType=msgEnterDistrictJobOrder;
				else
					logType=msgEnterSchoolJobOrder;
				
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				//e.printStackTrace();
			}			
			map.addAttribute("roleAccess", roleAccess);
			
			JobOrder jobOrder	=	new JobOrder(); 
			
			String notIficationToschool="";
			notIficationToschool="checked";
			
			boolean miamiShowFlag=true;
			try{
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
					miamiShowFlag=false;
				}
			}catch(Exception e){}
			map.addAttribute("miamiShowFlag", miamiShowFlag);
			if(userMaster.getEntityType()==3 && JobOrderType==2){
				map.addAttribute("addJobFlag",false);
			}else {
				map.addAttribute("addJobFlag",true);
			}
			
			if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getsACreateDistJOb()!=null)
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getsACreateDistJOb()){
					map.addAttribute("addJobFlag",true);
				}
			if(userMaster.getEntityType()==2 && JobOrderType==2){
				try{
					map.addAttribute("DAssessmentUploadURL",userMaster.getDistrictId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				map.addAttribute("SAssessmentUploadURL","");
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				map.addAttribute("DistrictExitURL",userMaster.getDistrictId().getExitURL());
				map.addAttribute("DistrictExitMessage",userMaster.getDistrictId().getExitMessage());
				map.addAttribute("districtHiddenId",userMaster.getDistrictId().getDistrictId());
				map.addAttribute("SchoolName",null);
				map.addAttribute("SchoolId",0);
				int exitURLMessageVal=1;
				if(userMaster.getDistrictId().getFlagForMessage()!=0){
					if(userMaster.getDistrictId().getFlagForMessage()==1){
						exitURLMessageVal=2;
					}
				}
				map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","1");
				
				DistrictMaster districtMaster	=	districtMasterDAO.findById(userMaster.getDistrictId().getDistrictId(),false,false);
				map.addAttribute("writePrivilegfordistrict",districtMaster.getWritePrivilegeToSchool());
				
			}else if(userMaster.getEntityType()==3 && JobOrderType==3){
				try{
					map.addAttribute("DAssessmentUploadURL",userMaster.getSchoolId().getDistrictId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				try{
					map.addAttribute("SAssessmentUploadURL",userMaster.getSchoolId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("SAssessmentUploadURL",null);
				}
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
				map.addAttribute("DistrictExitURL",userMaster.getSchoolId().getExitURL());
				map.addAttribute("DistrictExitMessage",userMaster.getSchoolId().getExitMessage());
				map.addAttribute("districtHiddenId",userMaster.getSchoolId().getDistrictId().getDistrictId());
				int exitURLMessageVal=1;
				if(userMaster.getSchoolId().getFlagForMessage()!=0){
					if(userMaster.getSchoolId().getFlagForMessage()==1){
						exitURLMessageVal=2;
					}
				}
				map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","1");
			}else{
				map.addAttribute("DistrictOrSchoolName",null);
				map.addAttribute("DistrictOrSchoolId",null);
				map.addAttribute("SchoolName",null);
				map.addAttribute("SchoolId",0);
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","0");
			}
			
			if(userMaster.getDistrictId()!=null &&  userMaster.getEntityType()==3 && JobOrderType==2 && userMaster.getDistrictId().getsACreateDistJOb()){

				try{
					map.addAttribute("DAssessmentUploadURL",userMaster.getSchoolId().getDistrictId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				try{
					map.addAttribute("SAssessmentUploadURL",userMaster.getSchoolId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("SAssessmentUploadURL",null);
				}
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
				map.addAttribute("DistrictExitURL",userMaster.getDistrictId().getExitURL());
				map.addAttribute("DistrictExitMessage",userMaster.getDistrictId().getExitMessage());
				map.addAttribute("districtHiddenId",userMaster.getSchoolId().getDistrictId().getDistrictId());
				int exitURLMessageVal=1;
				if(userMaster.getDistrictId().getFlagForMessage()!=0){
					if(userMaster.getDistrictId().getFlagForMessage()==1){
						exitURLMessageVal=2;
					}
				}
				map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","1");
			
			}
			
			if(JobOrderType==2){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else if(JobOrderType==3){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else{
				map.addAttribute("districtRootPath",null);
				map.addAttribute("schoolRootPath",null);
			}
			if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(804800)){
				map.addAttribute("allSchoolGradeDistrictVal",2);
				jobOrder.setSelectedSchoolsInDistrict(1);
			}else{			
				map.addAttribute("allSchoolGradeDistrictVal",1);			
			}
			map.addAttribute("JobOrderType",JobOrderType);
			map.addAttribute("jobOrder",jobOrder);
			map.addAttribute("attachJobAssessmentVal",2);
			map.addAttribute("noOfExpHires","");
			map.addAttribute("dateTime",Utility.getDateTime());
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);
			
			DistrictMaster districtMaster = null;
			
			
				if(userMaster.getEntityType()==3 || userMaster.getEntityType()==2)
					districtMaster = userMaster.getDistrictId();
			
			/*=== values to dropdown from jobCategoryMaster Table ===*/
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
			
			map.addAttribute("jobCategoryMasterlst",jobCategoryMasterlst);	
			
			///////////// For job subcategory
			List<JobCategoryMaster> jobSubCategoryMasterlst = jobCategoryMasterDAO.findAllJobSubCategoryNameByDistrict(jobOrder.getDistrictMaster());
			
			int chksubCate = 0;
			String chkCase = "addcase";
			if(jobSubCategoryMasterlst.size()>0)
				chksubCate = 1;
			
			System.out.println(" chksubCate :: "+chksubCate+" jobSubCategoryMasterlst size :: "+jobSubCategoryMasterlst.size());
			
			map.addAttribute("jobSubCategoryMasterlst",jobSubCategoryMasterlst);
			map.addAttribute("chksubCate",chksubCate);
			map.addAttribute("chkCase",chkCase);
			///////////////////////////////////////////////////
			map.addAttribute("jeffcoSpecificSchool", "");
			
			if(userMaster.getDistrictId()!=null){
				List<SubjectMaster> subjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(userMaster.getDistrictId());
				map.addAttribute("subjectList",subjectMasters);
			}
			
			
			List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("listStateMaster", listStateMaster);
			
			/*======== Gagan [Start] : mapping State Id to get auto select State acording to District ===============*/
			if(userMaster.getEntityType()==2 )
			{
				map.addAttribute("stateId",userMaster.getDistrictId().getStateId().getStateId());
				map.addAttribute("districtReqNumFlag",userMaster.getDistrictId().getIsReqNoRequired());
			}
			else
				if(userMaster.getEntityType()==3)
					map.addAttribute("stateId",userMaster.getSchoolId().getStateMaster().getStateId());
				
			/*======== Gagan [END] : mapping State Id to get auto select State acording to District ===============*/
			
			List<DistrictRequisitionNumbers> avlbList = new ArrayList<DistrictRequisitionNumbers>();
			List<JobRequisitionNumbers> attachedList = new ArrayList<JobRequisitionNumbers>();
			List<JobRequisitionNumbers> attachedSchoolList = new ArrayList<JobRequisitionNumbers>();
			List<JobTitleWithDescription> jobTitleList = new ArrayList<JobTitleWithDescription>();
			StringBuffer schoolReqList=new StringBuffer();
			
			if(userMaster.getDistrictId()!=null)
			{
				Criterion criterion1 = Restrictions.isNull("schoolMaster");
				Criterion criterion2 = Restrictions.isNotNull("schoolMaster");
				Criterion criterion3 = Restrictions.eq("districtMaster", userMaster.getDistrictId());
				Criterion criterion4 = Restrictions.eq("isUsed", false);
				Criterion criterion44 = Restrictions.eq("status", "A");
				Criterion criterion5 = Restrictions.isNull("jobCode");
				//BeanComparator sortByPositionNumber = new BeanComparator("requisitionNumber");
				try{

					//if(userMaster.getDistrictId().getIsReqNoRequired()!=null)
					//{	if(userMaster.getDistrictId().getIsReqNoRequired()){
							avlbList = districtRequisitionNumbersDAO.findByCriteria(criterion3,criterion4);
							
							//Collections.sort(avlbList, sortByPositionNumber);
							
							map.addAttribute("avlbList",avlbList);
							jobTitleList = jobTitleWithDescriptionDAO.findByCriteria(criterion3,criterion5); 
								map.addAttribute("jobTitleList",jobTitleList);
						//}
					//}

								/*if(userMaster.getDistrictId()!=null && userMaster!=null && userMaster.getEntityType()==3 && userMaster.getDistrictId().getDistrictId()==804800){
								Criterion criterion6 = Restrictions.eq("schoolMaster", userMaster.getSchoolId());
								avlbList = districtRequisitionNumbersDAO.findByCriteria(Order.asc("requisitionNumber"),criterion3,criterion4,criterion6);
								map.addAttribute("avlbList",avlbList);
							 }else{
								//if(userMaster.getDistrictId().getIsReqNoRequired()!=null)
								//{	if(userMaster.getDistrictId().getIsReqNoRequired()){
										avlbList = districtRequisitionNumbersDAO.findByCriteria(Order.asc("requisitionNumber"),criterion3,criterion4);
										map.addAttribute("avlbList",avlbList);
									//}*/
							if(userMaster!=null && userMaster.getDistrictId().getDistrictId().equals(804800)){
								Session sessionHibernate=districtRequisitionNumbersDAO.getSessionFactory().openSession();
								Criteria cri=sessionHibernate.createCriteria(DistrictRequisitionNumbers.class);
								cri.add(criterion3);
								cri.add(criterion4);
								cri.add(criterion44);
								//cri.addOrder(Order.asc("requisitionNumber"));
								cri.addOrder(OrderBySqlFormula.sqlFormula("cast(requisitionNumber as unsigned) asc"));
								try{
							if( userMaster.getEntityType()==3){
								Criterion criterion6 = Restrictions.eq("schoolMaster", userMaster.getSchoolId());
								cri.add(criterion6);
								avlbList = cri.list();
								map.addAttribute("avlbList",avlbList);
							 }else{
								//if(userMaster.getDistrictId().getIsReqNoRequired()!=null)
								//{	if(userMaster.getDistrictId().getIsReqNoRequired()){
										avlbList = cri.list();
										map.addAttribute("avlbList",avlbList);
									//}
							 }
						}catch(Exception e){
							e.printStackTrace();
						}
					}						
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			//for NC Check
			int ncCheck = 0;
			 map.addAttribute("ncCheck", ncCheck);
			
		} 
		catch (Throwable e) 
		{
			e.printStackTrace();
		}
		
		
		
		return pageName;
	}
	/* @Author: Sekhar 
	 * @Discription: editJobOrder.do Controller.
	 */	
	//editjoborder by shriram
	@SuppressWarnings("null")
	@RequestMapping(value="/editjoborder.do", method=RequestMethod.GET)
	public String doEditJobOrderGET(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		//Anurag    String pageName="addeditjoborder";
		  String pageName="addeditjobordernew";   // optimized
		try 
		{
			//Added by Gaurav Kumar
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String dateStr = format.format(date).toString().substring(0,4);
			//dateStr = (Integer.parseInt(dateStr)+1)+"-"+(Integer.parseInt(dateStr)+2)+" "+Utility.getLocaleValuePropByKey("lblSchoolYear", locale);
			dateStr = (Integer.parseInt(dateStr))+"-"+(Integer.parseInt(dateStr)+1)+" "+Utility.getLocaleValuePropByKey("lblSchoolYear", locale);
			List<String> positionStartList = new ArrayList<String>();
			positionStartList.add(Utility.getLocaleValuePropByKey("lblCurrentImmediate", locale));
			positionStartList.add(dateStr);
			//End
			
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			boolean notAccesFlag=false;
			DistrictMaster dMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					dMaster=userMaster.getDistrictId();
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			if(userMaster!=null && userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(804800) || userMaster.getDistrictId().getDistrictId().equals(806900))){
				pageName="addeditjoborderjeffco";
			}
			int JobOrderType=0;
			int JobId=0;
			JobOrder jobOrder=null;
			
			if(request.getParameter("JobOrderType")!=null){
				JobOrderType=Integer.parseInt(request.getParameter("JobOrderType"));
			}
			boolean miamiShowFlag=true;
			try{
				if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
					miamiShowFlag=false;
				}
			}catch(Exception e){}
			map.addAttribute("miamiShowFlag", miamiShowFlag);
			
			String roleAccess=null;
			try{
				if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
				}
			}catch(Exception e){
				//e.printStackTrace();
			}
			
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("positionStartList", positionStartList);
			if(request.getParameter("JobId")!=null){
				JobId=Integer.parseInt(request.getParameter("JobId"));
				map.addAttribute("JobId", JobId);
			}			
			if(JobId==0){
				jobOrder	=	new JobOrder();
			}else{
				jobOrder	=jobOrderDAO .findById(JobId, false, false);
				try{
					String jobDescription=jobOrder.getJobDescriptionHTML()!=null?jobOrder.getJobDescriptionHTML().replaceAll("&amp;#x13&amp;#x10;", "<br/>"):null;
					jobOrder.setJobDescription(jobDescription.replaceAll("&amp;amp;#x13&amp;amp;#x10;", "<br/>"));
					
				}catch(Exception e){} 
			}
			
			if(jobOrder!=null){
				if(!userMaster.getEntityType().equals(1)){
					if(!jobOrder.getDistrictMaster().getDistrictId().equals(dMaster.getDistrictId())){
						notAccesFlag=true;
					}
				}
			}
			
			
			if(jobOrder!=null){
				if(jobOrder.getJobStartTime()!=null && jobOrder.getJobEndTime()!=null)
				{
					map.addAttribute("postingStartTime",jobOrder.getJobStartTime());
					map.addAttribute("postingEndTime",jobOrder.getJobEndTime());
				}//please don't remove these commented code (shriram)
				/*else
					{
					if(userMaster.getEntityType()==2 || userMaster.getEntityType()==3)
						{
						map.addAttribute("postingStartTime",dMaster.getPostingStartTime());
						map.addAttribute("postingEndTime",dMaster.getPostingEndTime());
						}else if(userMaster.getEntityType()==1){
						map.addAttribute("postingStartTime",jobOrder.getDistrictMaster().getPostingStartTime());
						map.addAttribute("postingEndTime",jobOrder.getDistrictMaster().getPostingEndTime());
						}
						}*/
						}
			
			if(notAccesFlag)
			{
				PrintWriter out = response.getWriter();
				response.setContentType("text/html"); 
				out.write("<script type='text/javascript'>");
				out.write("alert('You have no access to this Job.');");
				out.write("window.location.href='/managejoborders.do';");
				out.write("</script>");
				return null;
			}
			
			/*======= Gagan : Disable Other Radio btn on Page load if Any Req No is attached with this joborder ========*/
			int radioDA =0;
			int radioSA =0;
			//List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
			//List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>(); 
			List<DistrictRequisitionNumbers> avlbList = new ArrayList<DistrictRequisitionNumbers>();
			List<JobRequisitionNumbers> attachedList = new ArrayList<JobRequisitionNumbers>();
			List<JobRequisitionNumbers> attachedSchoolList = new ArrayList<JobRequisitionNumbers>();
			List<JobRequisitionNumbers> attachedRequisitionList = new ArrayList<JobRequisitionNumbers>();
			StringBuffer schoolReqList=new StringBuffer(); 
			
			Criterion criterion  = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion1 = Restrictions.isNull("schoolMaster");
			Criterion criterion2 = Restrictions.isNotNull("schoolMaster");
			Criterion criterion3 = Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
			Criterion criterion4 = Restrictions.eq("isUsed", false);
			
			
			String location ="";
			
			try
			{
				//System.out.println(" ================  "+jobOrder.getSchool());
				if(jobOrder!=null && jobOrder.getSchool().size()>0)
				{
					if(jobOrder.getSchool().get(0).getLocationCode()!=null && jobOrder.getSchool().get(0).getLocationCode()!="")
						location =jobOrder.getSchool().get(0).getSchoolName()+" ("+jobOrder.getSchool().get(0).getLocationCode()+")";
					else
						location = jobOrder.getSchool().get(0).getSchoolName();
				}
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			//if(jobOrder!=null &&  jobOrder.getDistrictMaster().getIsReqNoRequired())
			if(jobOrder!=null)
			{
				avlbList = districtRequisitionNumbersDAO.findByCriteria(criterion3,criterion4);
				map.addAttribute("avlbList",avlbList);
				if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null)
				{
					Criterion criterion6 = Restrictions.eq("schoolMaster", userMaster.getSchoolId());
					attachedRequisitionList = jobRequisitionNumbersDAO.findByCriteria(criterion,criterion6);
					map.addAttribute("attachedRequisitionList",attachedRequisitionList);
				}
				else if(userMaster.getEntityType()==2 || userMaster.getEntityType()==1)
				{
					attachedRequisitionList = jobRequisitionNumbersDAO.findByCriteria(criterion);
					map.addAttribute("noOfExpHires",attachedRequisitionList.size());
				}
					
				if(jobOrder!=null && jobOrder.getNoSchoolAttach()!=null)
				{	
					attachedList = jobRequisitionNumbersDAO.findByCriteria(criterion,criterion1);
					
					for(JobRequisitionNumbers jbrn : attachedList)
					{
						if(jbrn.getStatus()==1)
						{
							radioDA=1;
							break;
						}
					}
				}
				else if(jobOrder !=null && jobOrder.getSelectedSchoolsInDistrict()!=null)
				{
					attachedSchoolList = jobRequisitionNumbersDAO.findByCriteria(criterion,criterion2);
					List<JobRequisitionNumbers> jobreqList=jobRequisitionNumbersDAO.findByCriteria(criterion,criterion2);
					Map<Long, Long>tempSchooMap=new HashMap<Long, Long>();
					for(JobRequisitionNumbers  jrq:jobreqList){
						tempSchooMap.put(jrq.getSchoolMaster().getSchoolId(), jrq.getSchoolMaster().getSchoolId());
					}
					Set<Map.Entry<Long, Long>> set=tempSchooMap.entrySet();
					Iterator<Entry<Long, Long>>iter=set.iterator();
					while(iter.hasNext()){
						Entry<Long,Long>entry=iter.next();
						Long schoolId=tempSchooMap.get(entry.getKey());
						schoolReqList.append(schoolId+",");
					}
					if(schoolReqList.length()>0){
						schoolReqList.substring(0, schoolReqList.length()-1).toString();	
					}
					for(JobRequisitionNumbers jbrn : attachedSchoolList)
					{
						if(jbrn.getStatus()==1)
						{
							radioSA=1;
							break;
						}
					}
				}
				if(radioDA==1)
				{
					map.addAttribute("disableRadioSA", 0);
				}
				else if(radioSA == 1)
				{
					map.addAttribute("disableRadioDA", 0);
				}
				
				map.addAttribute("attachedList",attachedList);
				map.addAttribute("attachedSchoolList",attachedSchoolList);
				if(schoolReqList.length()!=0 && !schoolReqList.equals("")){
					map.addAttribute("requisitionSchoolList",schoolReqList.substring(0, schoolReqList.length()-1).toString());
				}
			}
			/*===================== [ END ]  =======================*/
			
			map.addAttribute("jobStartDate",Utility.getCalenderDateFormart(jobOrder.getJobStartDate()+""));
			map.addAttribute("jobEndDate",Utility.getCalenderDateFormart(jobOrder.getJobEndDate()+""));
			
			
			//jeffco add district Id
			List<JobTitleWithDescription> jobTitleList = new ArrayList<JobTitleWithDescription>();
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("804800")){
				
				jobTitleList = jobTitleWithDescriptionDAO.findByCriteria(criterion3); 
				String jeffcoSpecificSchool="";
				if(jobOrder.getRequisitionNumber()!=null && !jobOrder.getRequisitionNumber().equalsIgnoreCase("")){
					DistrictRequisitionNumbers districtRequisitionNumbers = districtRequisitionNumbersDAO.getDistrictRequisitionNumber(jobOrder.getDistrictMaster(), jobOrder.getRequisitionNumber());
					if(districtRequisitionNumbers!=null){
						JobRequisitionNumbers jobRequisitionNumbers = jobRequisitionNumbersDAO.findJobReqNumbersObj(districtRequisitionNumbers);
							if(jobRequisitionNumbers!=null){
								if(jobRequisitionNumbers.getSchoolMaster()!=null)
								jeffcoSpecificSchool = jobRequisitionNumbers.getSchoolMaster().getSchoolName();
							}
						
					}
				}
				
				map.addAttribute("jobTitleList",jobTitleList);
				map.addAttribute("jeffcoSpecificSchool", jeffcoSpecificSchool);
				if(jobOrder.getPositionStartDate()!=null)
				map.addAttribute("positionStartDate",Utility.getCalenderDateFormart(jobOrder.getPositionStartDate()+""));
				if(jobOrder.getPositionEndDate()!=null)
				map.addAttribute("positionEndDate",Utility.getCalenderDateFormart(jobOrder.getPositionEndDate()+""));
				if(jobOrder.getSalaryRange()!=null)
				map.addAttribute("salaryRange",jobOrder.getSalaryRange());
				if(jobOrder.getEmploymentServicesTechnician()!=null)
				map.addAttribute("primaryESTechnicianHidden",jobOrder.getEmploymentServicesTechnician().getEmploymentservicestechnicianId());
				map.addAttribute("fte",jobOrder.getFte()!=null?jobOrder.getFte():"");
				map.addAttribute("paygrade",jobOrder.getPayGrade()!=null?jobOrder.getPayGrade():"");
				map.addAttribute("daysworked",jobOrder.getDaysWorked()!=null?jobOrder.getDaysWorked():"");
				map.addAttribute("fsalary",jobOrder.getFsalary()!=null?jobOrder.getFsalary():"");
				map.addAttribute("fsalaryA",jobOrder.getFsalaryA()!=null?jobOrder.getFsalaryA():"");
				map.addAttribute("ssalary",jobOrder.getSsalary()!=null?jobOrder.getSsalary():"");
			}
			try{
				
				/****************************************
				 * In Case Edit Job Order Page Showing 
				 * Interim/Temporary Start Date 
				 * Interim/Temporary End Date
				 * Only For Martin Country School District.
				 *********************************/
				if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("1201290")){
					
					if(jobOrder.getPositionStartDate()!=null)
						map.addAttribute("positionStartDate",Utility.getCalenderDateFormart(jobOrder.getPositionStartDate()+""));
						if(jobOrder.getPositionEndDate()!=null)
						map.addAttribute("positionEndDate",Utility.getCalenderDateFormart(jobOrder.getPositionEndDate()+""));
					
				}
				//End
				
				if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("806900")){
					map.addAttribute("fte",jobOrder.getFte()!=null?jobOrder.getFte():"");
					map.addAttribute("positionNumber",jobOrder.getRequisitionNumber());
					map.addAttribute("paygrade",jobOrder.getPayGrade()!=null?jobOrder.getPayGrade():"");
					map.addAttribute("mSalary",jobOrder.getmSalary()!=null?jobOrder.getmSalary():"");
					map.addAttribute("accountCode",jobOrder.getAccountCode()!=null?jobOrder.getAccountCode():"");
					map.addAttribute("positionType",jobOrder.getPositionType()!=null?jobOrder.getPositionType():"");
					map.addAttribute("monthsPerYear",jobOrder.getMonthsPerYear()!=null?jobOrder.getMonthsPerYear():"");

					map.addAttribute("salaryAdminPlan",jobOrder.getSalaryAdminPlan()!=null?jobOrder.getSalaryAdminPlan():"");
					map.addAttribute("approvalCode",jobOrder.getApprovalCode()!=null?jobOrder.getApprovalCode():"");
					map.addAttribute("step",jobOrder.getStep()!=null?jobOrder.getStep():"");
					//List<JobApprovalProcess> jobApprovalProcessList= jobApprovalProcessDAO.getJobApprovalProcessList();
					List<JobApprovalProcess> jobApprovalProcessList= jobApprovalProcessDAO.findAllDistrictJobApproval(jobOrder.getDistrictMaster());
					map.addAttribute("jobApprovalProcessList",jobApprovalProcessList);
				
					List<JobWiseApprovalProcess> listJobWiseApprovalProcessList = jobWiseApprovalProcessDAO.findByJobId(jobOrder);
				
					if(listJobWiseApprovalProcessList.size()>0){
					
						map.addAttribute("jobApporvalProcessSelected", listJobWiseApprovalProcessList.get(0));
						
					}
					
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			
			//Dhananjay Global: Job Order Add/Edit Page: Add "Job Approval Process"
			
			try {
				
				if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && !jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("806900") && !jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("804800")){
					
					List<JobApprovalProcess> jobApprovalProcessList= jobApprovalProcessDAO.findAllDistrictJobApproval(jobOrder.getDistrictMaster());
					map.addAttribute("jobApprovalProcessList",jobApprovalProcessList);
					
					
					List<JobWiseApprovalProcess> listJobWiseApprovalProcessList = jobWiseApprovalProcessDAO.findByJobId(jobOrder);
					
					if(listJobWiseApprovalProcessList.size()>0){
					
						map.addAttribute("jobApporvalProcessSelected", listJobWiseApprovalProcessList.get(0));
						
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			
			
			map.addAttribute("jobType", jobOrder.getJobType());
			Criterion criterion5=Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
			List<GeoZoneMaster>zoneMasters=new ArrayList<GeoZoneMaster>();
			
			if(request.getRequestURI()!=null && !request.getRequestURI().equals("") && request.getRequestURI().equals("/teachermatch/editjobordertest.do"))
				 zoneMasters= geoZoneMasterDAO.findByCriteria_OP(jobOrder.getDistrictMaster());//Added by kumar avinash
			else
			 zoneMasters=geoZoneMasterDAO.findByCriteria(criterion5);

			StringBuffer geozones=new StringBuffer();
			
			for(GeoZoneMaster zone:zoneMasters)
			{	
				if(jobOrder.getGeoZoneMaster()!=null)
				{	
					if(jobOrder.getGeoZoneMaster().getGeoZoneId().equals(zone.getGeoZoneId())){
						geozones.append("<option selected value='"+zone.getGeoZoneId()+"'>"+zone.getGeoZoneName()+"</option>");
					}else{
						geozones.append("<option value='"+zone.getGeoZoneId()+"'>"+zone.getGeoZoneName()+"</option>");
					}
					
				}
				else
				{
					geozones.append("<option  value='"+zone.getGeoZoneId()+"'>"+zone.getGeoZoneName()+"</option>");
				}	
			}
			
			map.addAttribute("jobzones",geozones.toString());
			map.addAttribute("zoneLableRed","show");
			
			
			if(jobOrder.getDistrictMaster().getIsReqNoRequired())
				map.addAttribute("isReqNoFlagByDistrict",1);
			else
				map.addAttribute("isReqNoFlagByDistrict",0);
				
			if(userMaster.getEntityType()==3 && JobOrderType==2){
				map.addAttribute("addJobFlag",false);
			}else {
				map.addAttribute("addJobFlag",true);
			}
			
			if(userMaster!=null  && userMaster.getEntityType()!=null && userMaster.getEntityType()==3 && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getsACreateDistJOb()!=null && userMaster.getDistrictId().getsACreateDistJOb()){
				map.addAttribute("addJobFlag",true);
			}
			
			map.addAttribute("expHireNotEqualToReqNoFlag",jobOrder.getIsExpHireNotEqualToReqNo());
			
			if(userMaster.getEntityType()==2 && JobOrderType==2){
				try{
					if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
						map.addAttribute("DAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
					}else{
						map.addAttribute("DAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				map.addAttribute("SAssessmentUploadURL","");
				map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
				try{	
					map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL());
				}catch(Exception e){
					map.addAttribute("DSAssessmentUploadURL",null);
				}
				map.addAttribute("PageFlag","1");
				map.addAttribute("SchoolDistrictNameFlag","1");
				map.addAttribute("SchoolName",null);
				map.addAttribute("SchoolId",0);
			}else if(userMaster.getEntityType()==3 && JobOrderType==3){
				
				try{
					map.addAttribute("DAssessmentUploadURL",jobOrder.getSchool().get(0).getDistrictId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
					//e.printStackTrace();
				}
				try{
					if(jobOrder.getSchool().get(0).getAssessmentUploadURL()!=null){
						map.addAttribute("SAssessmentUploadURL",jobOrder.getSchool().get(0).getAssessmentUploadURL().trim());	
					}else{
						map.addAttribute("SAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("SAssessmentUploadURL",null);
					//e.printStackTrace();
				}
				map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
				map.addAttribute("SchoolName","location");
				
				map.addAttribute("SchoolId",jobOrder.getSchool().get(0).getSchoolId());
				
				try{
					if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
						map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
					}else{
						map.addAttribute("DSAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("DSAssessmentUploadURL",null);
				}
				map.addAttribute("PageFlag","1");
				map.addAttribute("SchoolDistrictNameFlag","1");
			}else{
				if(JobOrderType==2){
					map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
					
					try{	
						if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
							map.addAttribute("DAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());	
						}else{
							map.addAttribute("DAssessmentUploadURL",null);	
						}
					}catch(Exception e){
						map.addAttribute("DAssessmentUploadURL",null);
					}
					
					map.addAttribute("SAssessmentUploadURL","");
					map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
					try{
						map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL());
					}catch(Exception e){
						map.addAttribute("DSAssessmentUploadURL",null);
					}
					map.addAttribute("SchoolName",null);
					map.addAttribute("SchoolId",0);
				}else if(JobOrderType==3){
						map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
						map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
						try{
							//map.addAttribute("SchoolName",jobOrder.getSchool().get(0).getSchoolName());
							map.addAttribute("SchoolName",location);
						}catch(Exception e){
							map.addAttribute("SchoolName",null);
						}
						try{	
							map.addAttribute("DAssessmentUploadURL",jobOrder.getSchool().get(0).getDistrictId().getAssessmentUploadURL().trim());
						}catch(Exception e){
							map.addAttribute("DAssessmentUploadURL",null);
						}
						try{	
							if(jobOrder.getSchool().get(0).getAssessmentUploadURL()!=null){
								map.addAttribute("SAssessmentUploadURL",jobOrder.getSchool().get(0).getAssessmentUploadURL().trim());
							}else{
								map.addAttribute("SAssessmentUploadURL",null);
							}
						}catch(Exception e){
							map.addAttribute("SAssessmentUploadURL",null);
						}
						try{
							map.addAttribute("SchoolId",jobOrder.getSchool().get(0).getSchoolId());
						}catch(Exception e){
							map.addAttribute("SchoolId",null);
						}
						
						
						try{
							if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
								map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
							}else{
								map.addAttribute("DSAssessmentUploadURL",null);
							}
						}catch(Exception e){
							map.addAttribute("DSAssessmentUploadURL",null);
						}
						
				}
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","0");
			}
			
			if(jobOrder.getSelectedSchoolsInDistrict()!=null && jobOrder.getSelectedSchoolsInDistrict()==1){
				map.addAttribute("allSchoolGradeDistrictVal",2);
			}else{
				map.addAttribute("allSchoolGradeDistrictVal",1);
			}
			map.addAttribute("DistrictExitURL",jobOrder.getExitURL());
			map.addAttribute("DistrictExitMessage",jobOrder.getExitMessage());
			map.addAttribute("districtHiddenId",jobOrder.getDistrictMaster().getDistrictId());
			map.addAttribute("JobOrderType",JobOrderType);
			map.addAttribute("jobOrder",jobOrder);
			if(jobOrder.getDistrictMaster().getDistrictId().equals(1200390) || jobOrder.getDistrictMaster().getDistrictId().equals(7800043)){
				map.addAttribute("JobPostingURL",Utility.getValueOfPropByKey("basePath")+"applyteacherjob.do?jobId="+jobOrder.getJobId());
			} else {
				//map.addAttribute("JobPostingURL",Utility.getShortURL(Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId()));
				map.addAttribute("JobPostingURL",Utility.getValueOfPropByKey("basePath")+"applyteacherjob.do?jobId="+jobOrder.getJobId());
			}

			int exitURLMessageVal=1;
			if(jobOrder.getFlagForMessage()!=null)
			if(jobOrder.getFlagForMessage()!=0){
				if(jobOrder.getFlagForMessage()==1){
					exitURLMessageVal=2;
				}
			}
			int attachJobAssessmentVal=1;
			if(jobOrder.getIsJobAssessment()){
					if(jobOrder.getAttachDefaultDistrictPillar()==1){
						attachJobAssessmentVal=1;
					}else if(jobOrder.getAttachDefaultSchoolPillar()==1){
						attachJobAssessmentVal=3;
					}else if(jobOrder.getAttachDefaultJobSpecificPillar()==1){
						attachJobAssessmentVal=4;
					}else if(jobOrder.getAttachNewPillar()==1){
						attachJobAssessmentVal=2;
					}	
			}
			if(JobOrderType==2){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else if(JobOrderType==3){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else{
				map.addAttribute("districtRootPath",null);
				map.addAttribute("schoolRootPath",null);
			}
			map.addAttribute("attachJobAssessmentVal",attachJobAssessmentVal);
			map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
			if(userMaster.getEntityType()==3 && JobOrderType==2){
				int noOfExpHires=schoolInJobOrderDAO.findNoOfExpHires(jobOrder,userMaster.getSchoolId(),0);
				if(noOfExpHires!=0){
					map.addAttribute("noOfExpHires",noOfExpHires);
				}else{
					map.addAttribute("noOfExpHires","");
				}
			}else{
				int noOfExpHires=schoolInJobOrderDAO.findNoOfExpHires(jobOrder,userMaster.getSchoolId(),1);
				if(noOfExpHires!=0){
					map.addAttribute("noOfExpHires",noOfExpHires);
				}else{
					map.addAttribute("noOfExpHires","");
				}
			}
			//shriram
			List<TimeZoneMaster> timezonemaster=timeZoneMasterDAO.getAllTimeZone();
			//if(userMaster!=null &&  userMaster.getDistrictId()!=null){
				map.addAttribute("timezone",timezonemaster);
			//}
				Integer jbTimeZoneId=null;
				// only for entity type 2 or 3
					//if(userMaster.getEntityType()==3 || userMaster.getEntityType()==2){ 
						jbTimeZoneId=jobOrder.getJobTimeZoneId();
						if(jbTimeZoneId==null)
						{
							if(jobOrder.getDistrictMaster().getTimezone()!=null)
							jbTimeZoneId=	Integer.parseInt(jobOrder.getDistrictMaster().getTimezone());
							
						}
							//disName=request.getParameter("districtId");
							//jbTimeZoneId=Integer.parseInt(dMaster.getTimezone());
						map.addAttribute( "jbTimeZoneId",jbTimeZoneId);
						System.out.println("******************************* "+jbTimeZoneId);

			int i=timeZoneMasterDAO.getAllTimeZone().size();
			System.out.println("????????????????????/size of time zone??????????????????????// ="+i);
			map.addAttribute("dateTime",Utility.getDateTime());
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);	
			
			/*=== values to dropdown from jobCategoryMaster Table ===*/
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(jobOrder.getDistrictMaster());//jobCategoryMasterDAO.findAllJobCategoryNameByJob(jobOrder);
			map.addAttribute("jobCategoryMasterlst",jobCategoryMasterlst);	
			
			System.out.println(" jobCategoryMasterlst :: "+jobCategoryMasterlst.size());
			
			System.out.println(" joborder cate id :: "+jobOrder.getJobCategoryMaster().getJobCategoryId());
			
			///////////////////// job Sub category
			try
			{
				List<JobCategoryMaster> jobSubCategoryMasterlst = jobCategoryMasterDAO.findAllJobSubCategoryNameByDistrict(jobOrder.getDistrictMaster());
				
				System.out.println(" jobSubCategoryMasterlst>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+jobSubCategoryMasterlst.size());
				
				int chksubCate = 0;
				String chkCase = "editcase";
				if(jobSubCategoryMasterlst.size()>0)
				{
					chksubCate = 1;
				}
				
				System.out.println(" chksubCate :: "+chksubCate+" jobSubCategoryMasterlst size :: "+jobSubCategoryMasterlst.size());
				
				map.addAttribute("jobSubCategoryMasterlst",jobSubCategoryMasterlst);
				map.addAttribute("chksubCate",chksubCate);
				map.addAttribute("chkCase",chkCase);
			//	map.addAttribute("jobcategoryIdForEdit",jobOrder.getJobCategoryMaster().getJobCategoryId());
				
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			//////////////////////////////////////////////////////////////////////////////////////////
			
			
			
			
			DistrictMaster districtMaster = null;
			if(jobOrder.getDistrictMaster()!=null){
				districtMaster=jobOrder.getDistrictMaster();
			}
			try{
				List<JobForTeacher> jobForTeachers=jobForTeacherDAO.findJFTApplicantbyJobOrder(jobOrder);
				if(jobForTeachers!=null && jobForTeachers.size()>0){
					map.addAttribute("jobCategoryDisabled","disabled=\"disabled\"");
					map.addAttribute("isJobApplied",true);
				}else{
					map.addAttribute("jobCategoryDisabled","");
					map.addAttribute("isJobApplied",false);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			List<SubjectMaster> subjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(districtMaster);
			
			map.addAttribute("subjectList",subjectMasters);
			List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("listStateMaster", listStateMaster);
			
			/*======== Gagan [Start] : mapping State Id to get auto select State acording to District ===============*/
			map.addAttribute("entityType", userMaster.getEntityType());
			if(userMaster.getEntityType()==2 )
				map.addAttribute("stateId",userMaster.getDistrictId().getStateId().getStateId());
			else
				if(userMaster.getEntityType()==3)
					map.addAttribute("stateId",userMaster.getSchoolId().getStateMaster().getStateId());
				else
					if(userMaster.getEntityType()==1 && jobOrder!=null)
					{
						if(JobOrderType==2)
						{
							map.addAttribute("stateId",jobOrder.getDistrictMaster().getStateId().getStateId());
						}
						else
						{
							try{
							// ========== GAgan : In Case of SJO only 1 record will found so i used getindex(0).
								map.addAttribute("stateId",jobOrder.getSchool().get(0).getStateMaster().getStateId());
							}catch(Exception e){
								//e.printStackTrace();
							}
						}
					}
						
			/*======== Gagan [END] : mapping State Id to get auto select State acording to District ===============*/
			
			try{
				String logType="";
				if(JobOrderType==2){
					if(jobOrder.getDistrictMaster()!=null)
						userMaster.setDistrictId(jobOrder.getDistrictMaster());
					logType=msgEnterDistrictJobOrder;
				}else{
					if(jobOrder.getDistrictMaster()!=null)
						userMaster.setDistrictId(jobOrder.getDistrictMaster());
					
					/*if(jobOrder.getSchool().get(0)!=null)
						userMaster.setSchoolId(jobOrder.getSchool().get(0));*/
					logType=msgEnterSchoolJobOrder;
				}
				
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			/* For VVI */
			try
			{
				if(jobOrder!=null)
				{
					String offerVirtualVideoInterview ="";
					String offerDistASMT ="";
					String wantScore="";
					String sendAutoVVILink="";
					String IsInviteOnlyFlag="";
					boolean hideJobFlag=false;
					String notIficationToschool="";
					String jobCompletedVVILink1 = "";
					String jobCompletedVVILink2 = "";
					
					if(jobOrder.getOfferVirtualVideoInterview())
						offerVirtualVideoInterview ="checked";
					
					if(jobOrder.getMaxScoreForVVI()!=null && !jobOrder.getMaxScoreForVVI().equals(""))
						wantScore="checked";
					
					if(jobOrder.getSendAutoVVILink()){
						sendAutoVVILink="checked";
						if(jobOrder.getJobCompletedVVILink()==null){
						
						}else if(jobOrder.getJobCompletedVVILink()==false){
							jobCompletedVVILink1 = "checked";
						}else if(jobOrder.getJobCompletedVVILink()==true){
							jobCompletedVVILink2 = "checked";
						}
					}
					
					if(jobOrder.getNotificationToschool())
						notIficationToschool="checked";
					
					map.addAttribute("offerVVIFlag",offerVirtualVideoInterview);
					map.addAttribute("wantScoreVVIFlag",wantScore);
					map.addAttribute("sendAutoVVILinkFlag",sendAutoVVILink);
					map.addAttribute("jobCompletedVVILink1",jobCompletedVVILink1);
					map.addAttribute("jobCompletedVVILink2",jobCompletedVVILink2);
					
					
					System.out.println("jobCompletedVVILink1 :: "+jobCompletedVVILink1+" jobCompletedVVILink2 :: "+jobCompletedVVILink2);
					
					
					if(jobOrder.getIsInviteOnly()!=null && jobOrder.getIsInviteOnly()){
						IsInviteOnlyFlag="checked";
						if(jobOrder.getHiddenJob()!=null && jobOrder.getHiddenJob()){
							hideJobFlag=true;
						}else if(jobOrder.getHiddenJob()!=null && !jobOrder.getHiddenJob()){
							hideJobFlag=false;
						}
					}
					map.addAttribute("hideJobFlag",hideJobFlag);	
					map.addAttribute("IsInviteOnlyFlag",IsInviteOnlyFlag);
					map.addAttribute("notIficationToschool" ,notIficationToschool);
					String hourPrDay ="";
					if(jobOrder.getHoursPerDay()!=null){
						hourPrDay=jobOrder.getHoursPerDay();
					}
					map.addAttribute("hourPrDay",hourPrDay);
					/*if(jobOrder.getOfferAssessmentInviteOnly())
						offerDistASMT ="checked";
					
					map.addAttribute("offerDistASMTFlag",offerDistASMT);*/

//start sandeep 13-08-15					
				  if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster() != null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId() ==DashboardAjax.NC_HEADQUARTER){
						
					  Criterion criterionRequestion  = Restrictions.eq("jobOrder", jobOrder);
					  List<JobRequisitionNumbers> jobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(criterionRequestion);    
					  if(jobRequisitionNumbers != null && jobRequisitionNumbers.size() >0){
						  if(jobRequisitionNumbers.get(0).getDistrictRequisitionNumbers()!=null && jobRequisitionNumbers.get(0).getDistrictRequisitionNumbers().getRequisitionNumber()!=null)
							  map.addAttribute("ncDistrictReqNo",jobRequisitionNumbers.get(0).getDistrictRequisitionNumbers().getRequisitionNumber());
						  if(jobRequisitionNumbers.get(0).getDistrictRequisitionNumbers() != null)
							  map.addAttribute("postionStatus",jobRequisitionNumbers.get(0).getDistrictRequisitionNumbers().getPostionStatus());
					  }
					  map.addAttribute("hrIntegrated", jobOrder.getDistrictMaster().getHrIntegrated());
				  }	
//end					
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			//for NC Check
			int ncCheck = 0;
			 map.addAttribute("ncCheck", ncCheck);
			 
			  map.addAttribute("schoolType", jobOrder.getGradeLevel());
			  if(districtMaster!=null&&districtMaster.getsAEditUpdateJob()!=null&& userMaster.getEntityType()==3){
				  System.out.println("If Executed. *************"+districtMaster.getsAEditUpdateJob());
				  map.addAttribute("sAEditUpdateJob",districtMaster.getsAEditUpdateJob()); 
			  }
			  else
			  {
				  map.addAttribute("sAEditUpdateJob",true);  
				  System.out.println("else Executed. *************"+" true ");
			  }
			  
			
		}catch (Exception e){
			e.printStackTrace();
	   }
		
		
		 
		return pageName;
	}
	
	/* @Author: Gagan 
	* @Discription: view of application pool Controller.
	*/
	/*============ Get Method of applicantPoolGET ====================*/
	@RequestMapping(value="/applicantpool.do", method=RequestMethod.GET)
	public String applicantPoolGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			HttpSession session = request.getSession(false);

			
			
			UserMaster userMaster = null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,41,"applicantpool.do",0);
			}catch(Exception e){
				//e.printStackTrace();
			}
			 
			String jobId = request.getParameter("jobId")		==	null?"0":request.getParameter("jobId").trim();
			JobOrder jobOrder									=	jobOrderDAO.findById(new Integer(jobId), false, false);
			List<JobForTeacher> applicantJobForTeacher 			=	jobForTeacherDAO.findJFTApplicantbyJobOrder(jobOrder);
			System.out.println("\n ============= applicantJobForTeacher "+applicantJobForTeacher.size());
			
			/***** Sorting by First Name then Sorting by Last Name from TeacherDetail table ******/
			SortedMap map1 = new TreeMap();
			List<JobForTeacher> sortedApplicantJobForTeacher	  	=	new ArrayList<JobForTeacher>();
				
			for(int i=0; i<applicantJobForTeacher.size();i++ ){
				map1.put(applicantJobForTeacher.get(i).getTeacherId().getFirstName()+"||"+applicantJobForTeacher.get(i).getTeacherId().getLastName(),applicantJobForTeacher.get(i));
			}
			
		    Iterator iterator = map1.keySet().iterator();
			while (iterator.hasNext()) {
				Object key = iterator.next();
				sortedApplicantJobForTeacher.add((JobForTeacher) map1.get(key));
			}
			/***** Sorting by First Name then Sorting by Last Name Stating ******/
			
			StatusMaster statushird = WorkThreadServlet.statusMap.get("hird");
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			lstStatusMasters.add(statushird);
			List<JobForTeacher> lstHiredApplicantFromJobForTeacher = jobForTeacherDAO.findJFTbyJobOrder(jobOrder,lstStatusMasters);
			if(jobOrder.getCreatedForEntity()==3)
			{
				List<SchoolInJobOrder> lstSchoolInJobOrder			= schoolInJobOrderDAO.findJobOrder(jobOrder);
				map.addAttribute("lstSchoolInJobOrder", lstSchoolInJobOrder.get(0));
			}
			
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			map.addAttribute("jobOrder", jobOrder);
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("totalNoOfApplicants", applicantJobForTeacher.size());
			map.addAttribute("applicantJobForTeacher", sortedApplicantJobForTeacher);
			map.addAttribute("totalHiredApplicants", lstHiredApplicantFromJobForTeacher.size());
			map.addAttribute("lstHiredApplicant", lstHiredApplicantFromJobForTeacher);
			try{
				String logType="";
				if(jobOrder.getCreatedForEntity()==2){
					if(jobOrder.getDistrictMaster()!=null)
						userMaster.setDistrictId(jobOrder.getDistrictMaster());
					logType=msgEnterApplicantPool;
				}else{
					if(jobOrder.getDistrictMaster()!=null)
						userMaster.setDistrictId(jobOrder.getDistrictMaster());
					
					/*if(jobOrder.getSchool().get(0)!=null)
						userMaster.setSchoolId(jobOrder.getSchool().get(0));*/
					logType=msgEnterApplicantPool;
				}
				
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				//e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "applicantpool";
	}
	/*============End Of Get Method of applicantPoolGET ====================*/
	
	@RequestMapping(value="/jobboard.do", method=RequestMethod.GET)
	public String jobBoardGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println(" ................inside Jobboard.do Controller.............");
		try 
		{
			List<SubjectMaster> lstSubjectMasters = subjectMasterDAO.findActiveSubject();
			List<MasterSubjectMaster> lstMasterSubjectMasters = masterSubjectMasterDAO.findAllActiveSuject();
			List<StateMaster> lstStateMasters = stateMasterDAO.findActiveState();
			map.addAttribute("lstSubjectMasters", lstSubjectMasters);
			map.addAttribute("lstMasterSubjectMasters", lstMasterSubjectMasters);
			map.addAttribute("lstStateMasters", lstStateMasters);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "jobboard";
	}
	
	@RequestMapping(value="/clonejoborder.do", method=RequestMethod.GET)
	public String cloneJobAction(ModelMap map,HttpServletRequest request)
	{
		String pageName="addeditjoborder";
		try 
		{
			System.out.println("clonejoborder.do");
			String currentUrl = request.getRequestURI().toString();
			
			//Added by Gaurav Kumar
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String dateStr = format.format(date).toString().substring(0,4);

			//dateStr = (Integer.parseInt(dateStr)+1)+"-"+(Integer.parseInt(dateStr)+2)+" "+Utility.getLocaleValuePropByKey("lblSchoolYear", locale);
			dateStr = (Integer.parseInt(dateStr))+"-"+(Integer.parseInt(dateStr)+1)+" "+Utility.getLocaleValuePropByKey("lblSchoolYear", locale);
			//String yr2 = format.parse(date2.toString()).toString();
			System.out.println("::::::::::::::::::::::::::Formatted Date:::::::::::::::::::::"+dateStr);
			List<String> positionStartList = new ArrayList<String>();
			positionStartList.add(Utility.getLocaleValuePropByKey("lblCurrentImmediate", locale));
			positionStartList.add(dateStr);
			//End
			
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			///////////////////////////////////////////////////////////////////////
			//shriram
			List<TimeZoneMaster> timezonemaster=timeZoneMasterDAO.getAllTimeZone();
				map.addAttribute("timezone",timezonemaster);
			if(userMaster!=null && userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(804800) || userMaster.getDistrictId().getDistrictId().equals(806900))){
				pageName="addeditjoborderjeffco";
			}
			int JobOrderType=0;
			int JobId=0;
			JobOrder jobOrder=null;
			
			if(request.getParameter("JobOrderType")!=null){
				JobOrderType=Integer.parseInt(request.getParameter("JobOrderType"));
			}
			boolean miamiShowFlag=true;
			try{
				if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
					miamiShowFlag=false;
				}
			}catch(Exception e){}
			map.addAttribute("miamiShowFlag", miamiShowFlag);
			
			String roleAccess=null;
			try{
				if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
				}
			}catch(Exception e){
				//e.printStackTrace();
			}
			
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("currentUrl", currentUrl);
			map.addAttribute("positionStartList", positionStartList);
			
			
			if(request.getParameter("JobId")!=null){
				JobId=Integer.parseInt(request.getParameter("JobId"));
				map.addAttribute("JobId", JobId);
			}			
			if(JobId==0){
				jobOrder	=	new JobOrder();
			}else{
				jobOrder	=jobOrderDAO .findById(JobId, false, false);
			}
			 map.addAttribute("schoolType", jobOrder.getGradeLevel());
			
			/*======= Gagan : Disable Other Radio btn on Page load if Any Req No is attached with this joborder ========*/
			int radioDA =0;
			int radioSA =0;
			//List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
			//List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>(); 
			List<DistrictRequisitionNumbers> avlbList = new ArrayList<DistrictRequisitionNumbers>();
			List<JobRequisitionNumbers> attachedList = new ArrayList<JobRequisitionNumbers>();
			List<JobRequisitionNumbers> attachedSchoolList = new ArrayList<JobRequisitionNumbers>();
			StringBuffer schoolReqList=new StringBuffer(); 
			
			Criterion criterion  = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion1 = Restrictions.isNull("schoolMaster");
			Criterion criterion2 = Restrictions.isNotNull("schoolMaster");
			Criterion criterion3 = Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
			Criterion criterion4 = Restrictions.eq("isUsed", false);
			
			List<JobTitleWithDescription> jobTitleList = new ArrayList<JobTitleWithDescription>();
			if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(804800)){
				jobTitleList = jobTitleWithDescriptionDAO.findByCriteria(criterion3); 
			}
			map.addAttribute("jobTitleList",jobTitleList);
			String location ="";
			
			try
			{
				//System.out.println(" ================  "+jobOrder.getSchool());
				if(jobOrder!=null && jobOrder.getSchool().size()>0)
				{
					if(jobOrder.getSchool().get(0).getLocationCode()!=null && jobOrder.getSchool().get(0).getLocationCode()!="")
						location =jobOrder.getSchool().get(0).getSchoolName()+" ("+jobOrder.getSchool().get(0).getLocationCode()+")";
					else
						location = jobOrder.getSchool().get(0).getSchoolName();
				}
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			//if(jobOrder!=null &&  jobOrder.getDistrictMaster().getIsReqNoRequired())
			if(jobOrder!=null)
			{
				avlbList = districtRequisitionNumbersDAO.findByCriteria(criterion3,criterion4);
				map.addAttribute("avlbList",avlbList);
				
				if(radioDA==1)
				{
					map.addAttribute("disableRadioSA", 0);
				}
				else if(radioSA == 1)
				{
					map.addAttribute("disableRadioDA", 0);
				}
				
				map.addAttribute("attachedList",attachedList);
				map.addAttribute("attachedSchoolList",attachedSchoolList);
				if(schoolReqList.length()!=0 && !schoolReqList.equals("")){
					map.addAttribute("requisitionSchoolList",schoolReqList.substring(0, schoolReqList.length()-1).toString());
				}
				try{
					if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("806900")){
						map.addAttribute("fte",jobOrder.getFte()!=null?jobOrder.getFte():"");
						map.addAttribute("apiJobId",jobOrder.getApiJobId()!=null?jobOrder.getApiJobId():"");
						map.addAttribute("positionNumber",jobOrder.getRequisitionNumber()!=null?jobOrder.getRequisitionNumber():"");
						map.addAttribute("paygrade",jobOrder.getPayGrade()!=null?jobOrder.getPayGrade():"");
						map.addAttribute("mSalary",jobOrder.getmSalary()!=null?jobOrder.getmSalary():"");
						map.addAttribute("accountCode",jobOrder.getAccountCode()!=null?jobOrder.getAccountCode():"");
						map.addAttribute("positionType",jobOrder.getPositionType()!=null?jobOrder.getPositionType():"");
						map.addAttribute("monthsPerYear",jobOrder.getMonthsPerYear()!=null?jobOrder.getMonthsPerYear():"");
						//map.addAttribute("departmentApproval",jobOrder.getDepartmentApproval()!=null?jobOrder.getDepartmentApproval():"");
						map.addAttribute("salaryAdminPlan",jobOrder.getSalaryAdminPlan()!=null?jobOrder.getSalaryAdminPlan():"");
						map.addAttribute("approvalCode",jobOrder.getApprovalCode()!=null?jobOrder.getApprovalCode():"");
						map.addAttribute("step",jobOrder.getStep()!=null?jobOrder.getStep():"");
						List<JobApprovalProcess> jobApprovalProcessList= jobApprovalProcessDAO.getJobApprovalProcessList();
						map.addAttribute("jobApprovalProcessList",jobApprovalProcessList);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				// for Jeffaco(804800)
				try{
					if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("804800")){
						if(jobOrder.getPositionStartDate()!=null)
							map.addAttribute("positionStartDate",Utility.getCalenderDateFormart(jobOrder.getPositionStartDate()+""));
							if(jobOrder.getPositionEndDate()!=null)
							map.addAttribute("positionEndDate",Utility.getCalenderDateFormart(jobOrder.getPositionEndDate()+""));
							if(jobOrder.getSalaryRange()!=null)
							map.addAttribute("salaryRange",jobOrder.getSalaryRange());
							if(jobOrder.getEmploymentServicesTechnician()!=null)
							map.addAttribute("primaryESTechnicianHidden",jobOrder.getEmploymentServicesTechnician().getEmploymentservicestechnicianId());
							map.addAttribute("fte",jobOrder.getFte()!=null?jobOrder.getFte():"");
							map.addAttribute("paygrade",jobOrder.getPayGrade()!=null?jobOrder.getPayGrade():"");
							map.addAttribute("daysworked",jobOrder.getDaysWorked()!=null?jobOrder.getDaysWorked():"");
							map.addAttribute("fsalary",jobOrder.getFsalary()!=null?jobOrder.getFsalary():"");
							map.addAttribute("fsalaryA",jobOrder.getFsalaryA()!=null?jobOrder.getFsalaryA():"");
							map.addAttribute("ssalary",jobOrder.getSsalary()!=null?jobOrder.getSsalary():"");
							
							
							String IsInviteOnlyFlag="";
							boolean hideJobFlag=false;
							String notIficationToschool="";
							if(jobOrder.getNotificationToschool()!=null && jobOrder.getNotificationToschool())
								notIficationToschool="checked";
							
							if(jobOrder.getIsInviteOnly()!=null && jobOrder.getIsInviteOnly()){
								IsInviteOnlyFlag="checked";
								if(jobOrder.getHiddenJob()!=null && jobOrder.getHiddenJob()){
									hideJobFlag=true;
								}else if(jobOrder.getHiddenJob()!=null && !jobOrder.getHiddenJob()){
									hideJobFlag=false;
								}
							}
							map.addAttribute("hideJobFlag",hideJobFlag);	
							map.addAttribute("IsInviteOnlyFlag",IsInviteOnlyFlag);
							map.addAttribute("notIficationToschool" ,notIficationToschool);
							String hourPrDay ="";
							if(jobOrder.getHoursPerDay()!=null){
								hourPrDay=jobOrder.getHoursPerDay();
							}
							map.addAttribute("hourPrDay",hourPrDay);
							map.addAttribute("jobCategoryDisabled","disabled=\"disabled\"");
							
							System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>> :: "+jobOrder.getApiJobId());
							//map.addAttribute("jobOrder",jobOrder);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				
			}
			/*===================== [ END ]  =======================*/
			
			map.addAttribute("jobStartDate",Utility.getCalenderDateFormart(jobOrder.getJobStartDate()+""));
			map.addAttribute("jobEndDate",Utility.getCalenderDateFormart(jobOrder.getJobEndDate()+""));
	
			map.addAttribute("jobType", jobOrder.getJobType());
			Criterion criterion5=Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
			List<GeoZoneMaster>zoneMasters=geoZoneMasterDAO.findByCriteria(criterion5);
			StringBuffer geozones=new StringBuffer();
			
			
			for(GeoZoneMaster zone:zoneMasters)
			{	
				if(jobOrder.getGeoZoneMaster()!=null)
				{	
					if(jobOrder.getGeoZoneMaster().getGeoZoneId().equals(zone.getGeoZoneId())){
						geozones.append("<option selected value='"+zone.getGeoZoneId()+"'>"+zone.getGeoZoneName()+"</option>");
					}else{
						geozones.append("<option value='"+zone.getGeoZoneId()+"'>"+zone.getGeoZoneName()+"</option>");
					}
					
				}
				else
				{
					geozones.append("<option  value='"+zone.getGeoZoneId()+"'>"+zone.getGeoZoneName()+"</option>");
				}	
			}
			
			map.addAttribute("jobzones",geozones.toString());
			map.addAttribute("zoneLableRed","show");
			
			
			if(jobOrder.getDistrictMaster().getIsReqNoRequired())
				map.addAttribute("isReqNoFlagByDistrict",1);
			else
				map.addAttribute("isReqNoFlagByDistrict",0);
				
			if(userMaster.getEntityType()==3 && JobOrderType==2){
				map.addAttribute("addJobFlag",false);
			}else {
				map.addAttribute("addJobFlag",true);
			}
			
			if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getsACreateDistJOb()!=null)
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getsACreateDistJOb()){
					map.addAttribute("addJobFlag",true);
				}
			
			map.addAttribute("expHireNotEqualToReqNoFlag",jobOrder.getIsExpHireNotEqualToReqNo());
			
			if(userMaster.getEntityType()==2 && JobOrderType==2){
				try{
					if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
						map.addAttribute("DAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
					}else{
						map.addAttribute("DAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				map.addAttribute("SAssessmentUploadURL","");
				map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
				try{	
					map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL());
				}catch(Exception e){
					map.addAttribute("DSAssessmentUploadURL",null);
				}
				map.addAttribute("PageFlag","1");
				map.addAttribute("SchoolDistrictNameFlag","1");
				map.addAttribute("SchoolName",null);
				map.addAttribute("SchoolId",0);
			}else if(userMaster.getEntityType()==3 && JobOrderType==3){
				
				try{
					map.addAttribute("DAssessmentUploadURL",jobOrder.getSchool().get(0).getDistrictId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
					//e.printStackTrace();
				}
				try{
					if(jobOrder.getSchool().get(0).getAssessmentUploadURL()!=null){
						map.addAttribute("SAssessmentUploadURL",jobOrder.getSchool().get(0).getAssessmentUploadURL().trim());	
					}else{
						map.addAttribute("SAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("SAssessmentUploadURL",null);
					//e.printStackTrace();
				}
				map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
				map.addAttribute("SchoolName","location");
				
				map.addAttribute("SchoolId",jobOrder.getSchool().get(0).getSchoolId());
				
				try{
					if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
						map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
					}else{
						map.addAttribute("DSAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("DSAssessmentUploadURL",null);
				}
				map.addAttribute("PageFlag","1");
				map.addAttribute("SchoolDistrictNameFlag","1");
			}else{
				if(JobOrderType==2){
					map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
					
					try{	
						if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
							map.addAttribute("DAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());	
						}else{
							map.addAttribute("DAssessmentUploadURL",null);	
						}
					}catch(Exception e){
						map.addAttribute("DAssessmentUploadURL",null);
					}
					
					map.addAttribute("SAssessmentUploadURL","");
					map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
					try{
						map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL());
					}catch(Exception e){
						map.addAttribute("DSAssessmentUploadURL",null);
					}
					map.addAttribute("SchoolName",null);
					map.addAttribute("SchoolId",0);
				}else if(JobOrderType==3){
						map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
						map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
						try{
							//map.addAttribute("SchoolName",jobOrder.getSchool().get(0).getSchoolName());
							map.addAttribute("SchoolName",location);
						}catch(Exception e){
							map.addAttribute("SchoolName",null);
						}
						try{	
							map.addAttribute("DAssessmentUploadURL",jobOrder.getSchool().get(0).getDistrictId().getAssessmentUploadURL().trim());
						}catch(Exception e){
							map.addAttribute("DAssessmentUploadURL",null);
						}
						try{	
							if(jobOrder.getSchool().get(0).getAssessmentUploadURL()!=null){
								map.addAttribute("SAssessmentUploadURL",jobOrder.getSchool().get(0).getAssessmentUploadURL().trim());
							}else{
								map.addAttribute("SAssessmentUploadURL",null);
							}
						}catch(Exception e){
							map.addAttribute("SAssessmentUploadURL",null);
						}
						try{
							map.addAttribute("SchoolId",jobOrder.getSchool().get(0).getSchoolId());
						}catch(Exception e){
							map.addAttribute("SchoolId",null);
						}
						
						
						try{
							if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
								map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
							}else{
								map.addAttribute("DSAssessmentUploadURL",null);
							}
						}catch(Exception e){
							map.addAttribute("DSAssessmentUploadURL",null);
						}
						
				}
				map.addAttribute("PageFlag","0");
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getsACreateDistJOb()){
					map.addAttribute("SchoolDistrictNameFlag","1");
				}else{
				map.addAttribute("SchoolDistrictNameFlag","0");
				}
			}
			
			if(jobOrder.getSelectedSchoolsInDistrict()!=null && jobOrder.getSelectedSchoolsInDistrict()==1){
				map.addAttribute("allSchoolGradeDistrictVal",2);
			}else{
				map.addAttribute("allSchoolGradeDistrictVal",1);
			}
			map.addAttribute("DistrictExitURL",jobOrder.getExitURL());
			map.addAttribute("DistrictExitMessage",jobOrder.getExitMessage());
			map.addAttribute("districtHiddenId",jobOrder.getDistrictMaster().getDistrictId());
			map.addAttribute("JobOrderType",JobOrderType);
			jobOrder.setPathOfJobDescription(null);
			jobOrder.setDistrictAttachment(null);
			jobOrder.setAssessmentDocument(null);
//			jobOrder.setApiJobId("");
			map.addAttribute("jobOrder",jobOrder);
			map.addAttribute("jobOrderUrl",jobOrder);
			/*if(jobOrder.getDistrictMaster().getDistrictId().equals(1200390) || jobOrder.getDistrictMaster().getDistrictId().equals(7800043)){
				map.addAttribute("JobPostingURL",Utility.getValueOfPropByKey("basePath")+"applyteacherjob.do?jobId="+jobOrder.getJobId());
			} else {
				map.addAttribute("JobPostingURL",Utility.getShortURL(Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId()));
			}
*/
			int exitURLMessageVal=1;
			if(jobOrder.getFlagForMessage()!=null)
			if(jobOrder.getFlagForMessage()!=0){
				if(jobOrder.getFlagForMessage()==1){
					exitURLMessageVal=2;
				}
			}
			int attachJobAssessmentVal=1;
			if(jobOrder.getIsJobAssessment()){
					if(jobOrder.getAttachDefaultDistrictPillar()==1){
						attachJobAssessmentVal=1;
					}else if(jobOrder.getAttachDefaultSchoolPillar()==1){
						attachJobAssessmentVal=3;
					}else if(jobOrder.getAttachDefaultJobSpecificPillar()==1){
						attachJobAssessmentVal=4;
					}else if(jobOrder.getAttachNewPillar()==1){
						attachJobAssessmentVal=2;
					}	
			}
			if(JobOrderType==2){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else if(JobOrderType==3){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else{
				map.addAttribute("districtRootPath",null);
				map.addAttribute("schoolRootPath",null);
			}
			map.addAttribute("attachJobAssessmentVal",attachJobAssessmentVal);
			map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
			if(userMaster.getEntityType()==3 && JobOrderType==2){
				int noOfExpHires=schoolInJobOrderDAO.findNoOfExpHires(jobOrder,userMaster.getSchoolId(),0);
				if(noOfExpHires!=0){
					map.addAttribute("noOfExpHires",noOfExpHires);
				}else{
					map.addAttribute("noOfExpHires","");
				}
			}else{
				int noOfExpHires=schoolInJobOrderDAO.findNoOfExpHires(jobOrder,userMaster.getSchoolId(),1);
				if(noOfExpHires!=0){
					map.addAttribute("noOfExpHires",noOfExpHires);
				}else{
					map.addAttribute("noOfExpHires","");
				}
			}
			map.addAttribute("dateTime",Utility.getDateTime());
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);	
			
			/*=== values to dropdown from jobCategoryMaster Table ===*/
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByJob(jobOrder);
			map.addAttribute("jobCategoryMasterlst",jobCategoryMasterlst);	
			
			
			///////////////////// job Sub category
			List<JobCategoryMaster> jobSubCategoryMasterlst = jobCategoryMasterDAO.findAllJobSubCategoryNameByDistrict(jobOrder.getDistrictMaster());

			int chksubCate = 0;
			String chkCase = "editcase";
			if(jobSubCategoryMasterlst.size()>0)
			{
				chksubCate = 1;
			}
			System.out.println(" chksubCate :: "+chksubCate+" jobSubCategoryMasterlst size :: "+jobSubCategoryMasterlst.size());

			map.addAttribute("jobSubCategoryMasterlst",jobSubCategoryMasterlst);
			map.addAttribute("chksubCate",chksubCate);
			map.addAttribute("chkCase",chkCase);
			//////////////////////////////////////////////////////////////////////////////////////////
			
			
			DistrictMaster districtMaster = null;
			if(jobOrder.getDistrictMaster()!=null){
				districtMaster=jobOrder.getDistrictMaster();
			}
			
			List<SubjectMaster> subjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(districtMaster);
			
			map.addAttribute("subjectList",subjectMasters);
			List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("listStateMaster", listStateMaster);
			
			/*======== Gagan [Start] : mapping State Id to get auto select State acording to District ===============*/
			map.addAttribute("entityType", userMaster.getEntityType());
			if(userMaster.getEntityType()==2 )
				map.addAttribute("stateId",userMaster.getDistrictId().getStateId().getStateId());
			else
				if(userMaster.getEntityType()==3)
					map.addAttribute("stateId",userMaster.getSchoolId().getStateMaster().getStateId());
				else
					if(userMaster.getEntityType()==1 && jobOrder!=null)
					{
						if(JobOrderType==2)
						{
							map.addAttribute("stateId",jobOrder.getDistrictMaster().getStateId().getStateId());
						}
						else
						{
							try{
							// ========== GAgan : In Case of SJO only 1 record will found so i used getindex(0).
								map.addAttribute("stateId",jobOrder.getSchool().get(0).getStateMaster().getStateId());
							}catch(Exception e){
								//e.printStackTrace();
							}
						}
					}
						
			/*======== Gagan [END] : mapping State Id to get auto select State acording to District ===============*/
			
			try{
				String logType="";
				if(JobOrderType==2){
					if(jobOrder.getDistrictMaster()!=null)
						userMaster.setDistrictId(jobOrder.getDistrictMaster());
					logType="Enter in edit District Job Order";
				}else{
					if(jobOrder.getDistrictMaster()!=null)
						userMaster.setDistrictId(jobOrder.getDistrictMaster());
					
					/*if(jobOrder.getSchool().get(0)!=null)
						userMaster.setSchoolId(jobOrder.getSchool().get(0));*/
					logType="Enter in edit School Job Order";
				}
				
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
				
//Start Sandeep 17-08-15 , Add flag for NC and this process for Clone creation.				
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster() != null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId() == DashboardAjax.NC_HEADQUARTER){
					
					Criterion criterionRequestion  = Restrictions.eq("jobOrder", jobOrder);
					  List<JobRequisitionNumbers> jobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(criterionRequestion);    
					  if(jobRequisitionNumbers != null && jobRequisitionNumbers.size() >0){
						  if(jobRequisitionNumbers.get(0).getDistrictRequisitionNumbers() != null)
						  {
							  if(jobRequisitionNumbers.get(0).getDistrictRequisitionNumbers()!=null && jobRequisitionNumbers.get(0).getDistrictRequisitionNumbers().getRequisitionNumber()!=null)
							  map.addAttribute("ncDistrictReqNo",jobRequisitionNumbers.get(0).getDistrictRequisitionNumbers().getRequisitionNumber());
							  if(jobRequisitionNumbers.get(0).getDistrictRequisitionNumbers().getPostionStatus().equals("51"))
							  map.addAttribute("NCHEADQUARTERId",2);
						  }
        				  }
					  if(jobOrder.getDistrictMaster().getHrIntegrated())
					  {						  
						  map.addAttribute("jobCategoryDisabled","disabled=\"disabled\"");  
					  }					
					map.addAttribute("hrIntegratedCloneFlag",jobOrder.getDistrictMaster().getHrIntegrated());
                }
				
//End				
			
				//for NC Check
				int ncCheck = 0;
				if(jobOrder!=null &&  jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2){
					ncCheck = 1;
				}
				map.addAttribute("ncCheck",ncCheck);
				System.out.println(" >>>>>>>>>>>>>>> ncCheck >>>>>>>>>>>>>>>>>>>> "+ncCheck);
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}catch (Exception e){
			e.printStackTrace();
	   }
		
		return pageName;
	}
	
	@RequestMapping(value="/managejobordersnew.do", method=RequestMethod.GET)
	public String manageDistrictGET1(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			String currentUrl = request.getRequestURI().toString();
			UserMaster userMaster=null;
			DistrictMaster districtMaster=null;
			
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			String jobAuthKey=(Utility.randomString(8)+Utility.getDateTime());
			map.addAttribute("jobAuthKey",jobAuthKey);

			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,24,"managejoborders.do",0);
			}catch(Exception e){
				//e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			
			boolean miamiShowFlag=true;
			try{
				if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
					miamiShowFlag=false;
				}
			}catch(Exception e){}		
			map.addAttribute("miamiShowFlag", miamiShowFlag);
			if(userMaster.getEntityType()!=1){
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictOrSchoolName",null);
			}
			map.addAttribute("JobOrderType","2");
			if(userMaster.getEntityType()==3){
				districtMaster=districtMasterDAO.findById(userMaster.getDistrictId().getDistrictId(),false, false);

				map.addAttribute("writePrivilegFlag",districtMaster.getWritePrivilegeToSchool());
				map.addAttribute("schoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("schoolId",userMaster.getSchoolId().getSchoolId());
				
				if(userMaster.getDistrictId().getsACreateDistJOb()!=null){
					if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getsACreateDistJOb()){
						map.addAttribute("addJobFlag",true);
					}else{
						map.addAttribute("addJobFlag",false);		
					}
				}else{
					map.addAttribute("addJobFlag",false);
				}
			}else {
				map.addAttribute("addJobFlag",true);
			}
			try{
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in District Job Order");
			}catch(Exception e){
				//e.printStackTrace();
			
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "managejoborders";
	}
	
	@RequestMapping(value="/schooljobordersnew.do", method=RequestMethod.GET)
	public String manageDistrictGETNew(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,25,"schooljoborders.do",0);
			}catch(Exception e){
				//e.printStackTrace();
			}
			boolean miamiShowFlag=true;
			try{
				if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
					miamiShowFlag=false;
				}
			}catch(Exception e){}
			
			map.addAttribute("miamiShowFlag", miamiShowFlag);
			map.addAttribute("roleAccess", roleAccess);
			if(userMaster.getEntityType()==3){
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				
				map.addAttribute("schoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("schoolId",userMaster.getSchoolId().getSchoolId());
				map.addAttribute("PageFlag","1");
			}else if(userMaster.getEntityType()==2){
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				map.addAttribute("PageFlag","1");
			}else{
				map.addAttribute("DistrictOrSchoolName",null);
				map.addAttribute("PageFlag","0");
			}
			map.addAttribute("JobOrderType","3");
			map.addAttribute("addJobFlag",true);
			try{
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in School Job Order");
			}catch(Exception e){
				//e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "managejoborders";
	}
	/////brajesh...........
	@RequestMapping(value="/districtJobDescriptionLibrary.do", method=RequestMethod.GET)
	public String districtJobDescriptionLibrary(ModelMap map,HttpServletRequest request)
	{
	 try{
			UserMaster userMaster=null;
			DistrictMaster districtMaster=null;
			List<JobCategoryMaster> jobCategoryMasters=null;
			
			HttpSession session = request.getSession(false);
			
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				jobCategoryMasters=jobCategoryMasterDAO.findAll();				
			}			
			int flagedit=0;
			List<DistrictKeyContact> districtkeycontact=null;
			if(userMaster!=null){
				String contactType="Job Description Library";
				districtkeycontact=districtKeyContactDAO.findByUserMasterAndContactType(userMaster, contactType);
			}			
			if((userMaster.getEntityType()==1) || (districtkeycontact!=null && districtkeycontact.size()>0))
				flagedit=1;			
			if(userMaster.getEntityType()!=1){
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
			}
			else{
				map.addAttribute("DistrictOrSchoolName",null);
			}
			map.addAttribute("userMaster",userMaster);
			map.addAttribute("jobCategoryList",jobCategoryMasters);
			map.addAttribute("testvalue","0");
			map.addAttribute("updateflag",flagedit);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "districtJobDescriptionLibrary";
	}
		/* Optimization @Anurag     */
//shriram addedit
	@Transactional(readOnly=true)
	@RequestMapping(value={"/addeditjoborder.do","/addeditjobordertest.do"}, method=RequestMethod.GET)
	public String AddEditJobAction_Op(ModelMap map,HttpServletRequest request)
	{
		String pageName="addeditjobordernew";
		try 
		{
			//Added by Gaurav Kumar
			Date date = new Date();
	        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	        String dateStr = format.format(date).toString().substring(0,4);
	        String strDateForAdams  = Utility.convertDateAndTimeToUSformatOnlyDate(new Date());
	        
	        map.put("strDateForAdams", strDateForAdams);
	        map.put("jobIdForAdams", 0);
	       
	        
	        dateStr = (Integer.parseInt(dateStr))+"-"+(Integer.parseInt(dateStr)+1)+" "+Utility.getLocaleValuePropByKey("lblSchoolYear", locale);
	        
	        System.out.println("::::::::::::::::::::::::::Formatted Date:::::::::::::::::::::"+dateStr);
	        List<String> positionStartList = new ArrayList<String>();
	        positionStartList.add(Utility.getLocaleValuePropByKey("lblCurrentImmediate", locale));
	        positionStartList.add(dateStr);
	        //End
	        
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			map.addAttribute("entityType", userMaster.getEntityType());
			map.addAttribute("userMaster", userMaster);
			map.addAttribute("sAEditUpdateJob",true);
			if(userMaster!=null && userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(804800) || userMaster.getDistrictId().getDistrictId().equals(806900))){
				pageName="addeditjoborderjeffco";	
			}
			
			
			int JobOrderType=0;
			if(request.getParameter("JobOrderType")!=null){
				JobOrderType=Integer.parseInt(request.getParameter("JobOrderType"));
			}			
			map.addAttribute("JobId", 0);	
			map.addAttribute("positionStartList", positionStartList);
			String roleAccess=null;
			try{
				if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
				}
			}catch(Exception e){
				//e.printStackTrace();
			}
			
			/*Gaurav Kumar*/
			try{
				
				
			if(userMaster!=null && userMaster.getDistrictId()!=null &&  userMaster.getDistrictId().getDistrictId().equals(806900)){
				
				List<JobApprovalProcess> jobApprovalProcessList= jobApprovalProcessDAO.findAllJobApprovalProcessByDistrict(userMaster.getDistrictId());
				
				map.addAttribute("jobApprovalProcessList",jobApprovalProcessList);
				
					
			} 
			
			/*******************************
			 * Dhananjay Verma Job Approval Process
			 * TPL 4719
			 *******************************/
			
			if(userMaster!=null && userMaster.getDistrictId()!=null &&  !userMaster.getDistrictId().getDistrictId().equals(806900) &&  !userMaster.getDistrictId().getDistrictId().equals(804800)){
				
				List<JobApprovalProcess> jobApprovalProcessList= jobApprovalProcessDAO.findAllJobApprovalProcessByDistrict(userMaster.getDistrictId());
				
				map.addAttribute("jobApprovalProcessList",jobApprovalProcessList);
				
					
			} 
			/*End */
			
			}catch(Exception e){
				e.printStackTrace();
			}
			
			try{
				String logType="";
				if(JobOrderType==2)
					logType=msgEnterDistrictJobOrder;
				else
					logType=msgEnterSchoolJobOrder;
				
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				//e.printStackTrace();
			}			
			map.addAttribute("roleAccess", roleAccess);
			
			JobOrder jobOrder	=	new JobOrder(); 
			
			String notIficationToschool="";
			notIficationToschool="checked";
			
			boolean miamiShowFlag=true;
			try{
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
					miamiShowFlag=false;
				}
			}catch(Exception e){}
			map.addAttribute("miamiShowFlag", miamiShowFlag);
			if(userMaster.getEntityType()==3 && JobOrderType==2){
				map.addAttribute("addJobFlag",false);
			}else {
				map.addAttribute("addJobFlag",true);
			}
			
			if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getsACreateDistJOb()!=null)
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getsACreateDistJOb()){
					map.addAttribute("addJobFlag",true);
				}
			if(userMaster.getEntityType()==2 && JobOrderType==2){
				try{
					map.addAttribute("DAssessmentUploadURL",userMaster.getDistrictId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				map.addAttribute("SAssessmentUploadURL","");
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				map.addAttribute("DistrictExitURL",userMaster.getDistrictId().getExitURL());
				map.addAttribute("DistrictExitMessage",userMaster.getDistrictId().getExitMessage());
				map.addAttribute("districtHiddenId",userMaster.getDistrictId().getDistrictId());
				map.addAttribute("SchoolName",null);
				map.addAttribute("SchoolId",0);
				int exitURLMessageVal=1;
				if(userMaster.getDistrictId().getFlagForMessage()!=0){
					if(userMaster.getDistrictId().getFlagForMessage()==1){
						exitURLMessageVal=2;
					}
				}
				map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","1");
				
				DistrictMaster districtMaster	=	districtMasterDAO.findById(userMaster.getDistrictId().getDistrictId(),false,false);
				map.addAttribute("writePrivilegfordistrict",districtMaster.getWritePrivilegeToSchool());
				
			}else if(userMaster.getEntityType()==3 && JobOrderType==3){
				try{
					map.addAttribute("DAssessmentUploadURL",userMaster.getSchoolId().getDistrictId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				try{
					map.addAttribute("SAssessmentUploadURL",userMaster.getSchoolId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("SAssessmentUploadURL",null);
				}
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
				map.addAttribute("DistrictExitURL",userMaster.getSchoolId().getExitURL());
				map.addAttribute("DistrictExitMessage",userMaster.getSchoolId().getExitMessage());
				map.addAttribute("districtHiddenId",userMaster.getSchoolId().getDistrictId().getDistrictId());
				int exitURLMessageVal=1;
				if(userMaster.getSchoolId().getFlagForMessage()!=0){
					if(userMaster.getSchoolId().getFlagForMessage()==1){
						exitURLMessageVal=2;
					}
				}
				map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","1");
			}else{
				map.addAttribute("DistrictOrSchoolName",null);
				map.addAttribute("DistrictOrSchoolId",null);
				map.addAttribute("SchoolName",null);
				map.addAttribute("SchoolId",0);
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","0");
			}
			
			if(userMaster.getDistrictId()!=null &&  userMaster.getEntityType()==3 && JobOrderType==2 && userMaster.getDistrictId().getsACreateDistJOb()){

				try{
					map.addAttribute("DAssessmentUploadURL",userMaster.getSchoolId().getDistrictId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				try{
					map.addAttribute("SAssessmentUploadURL",userMaster.getSchoolId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("SAssessmentUploadURL",null);
				}
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
				map.addAttribute("DistrictExitURL",userMaster.getDistrictId().getExitURL());
				map.addAttribute("DistrictExitMessage",userMaster.getDistrictId().getExitMessage());
				map.addAttribute("districtHiddenId",userMaster.getSchoolId().getDistrictId().getDistrictId());
				int exitURLMessageVal=1;
				if(userMaster.getDistrictId().getFlagForMessage()!=0){
					if(userMaster.getDistrictId().getFlagForMessage()==1){
						exitURLMessageVal=2;
					}
				}
				map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","1");
			
			}
			
			if(JobOrderType==2){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else if(JobOrderType==3){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else{
				map.addAttribute("districtRootPath",null);
				map.addAttribute("schoolRootPath",null);
			}
			if(userMaster!=null && userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(804800) || userMaster.getDistrictId().getDistrictId().equals(806900))){
				map.addAttribute("allSchoolGradeDistrictVal",2);
				jobOrder.setSelectedSchoolsInDistrict(1);
			}else{			
				map.addAttribute("allSchoolGradeDistrictVal",1);			
			}
			map.addAttribute("JobOrderType",JobOrderType);
			map.addAttribute("jobOrder",jobOrder);
			map.addAttribute("attachJobAssessmentVal",2);
			map.addAttribute("noOfExpHires","");
			map.addAttribute("dateTime",Utility.getDateTime());
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);
			
			DistrictMaster districtMaster = null;
			Integer jbTimeZoneId=null;
			// only for entity type 2 or 3
				if(userMaster.getEntityType()==3 || userMaster.getEntityType()==2){
					districtMaster = userMaster.getDistrictId();
					map.addAttribute("postingStartTime",districtMaster.getPostingStartTime());
					map.addAttribute("postingEndTime",districtMaster.getPostingEndTime());
					if(districtMaster.getTimezone()!=null)
					jbTimeZoneId=Integer.parseInt(districtMaster.getTimezone());
					map.addAttribute( "jbTimeZoneId",jbTimeZoneId);
					System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||"+jbTimeZoneId);
					System.out.println(districtMaster.getDistrictId());
				}
				
				
			
			/*=== values to dropdown from jobCategoryMaster Table ===*/
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
			
			map.addAttribute("jobCategoryMasterlst",jobCategoryMasterlst);	
			
			///////////// For job subcategory
			List<JobCategoryMaster> jobSubCategoryMasterlst = jobCategoryMasterDAO.findAllJobSubCategoryNameByDistrict(jobOrder.getDistrictMaster());
			
			int chksubCate = 0;
			String chkCase = "addcase";
			if(jobSubCategoryMasterlst.size()>0)
				chksubCate = 1;
			
			System.out.println(" chksubCate :: "+chksubCate+" jobSubCategoryMasterlst size :: "+jobSubCategoryMasterlst.size());
			
			map.addAttribute("jobSubCategoryMasterlst",jobSubCategoryMasterlst);
			map.addAttribute("chksubCate",chksubCate);
			map.addAttribute("chkCase",chkCase);
			///////////////////////////////////////////////////
			map.addAttribute("jeffcoSpecificSchool", "");
			
			
			if(userMaster.getDistrictId()!=null){
				List<SubjectMaster> subjectMasters=new ArrayList<SubjectMaster>();
				
				 subjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(userMaster.getDistrictId());
				
				
				map.addAttribute("subjectList",subjectMasters);
			}			
			
			List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("listStateMaster", listStateMaster);
			
			/*======== Gagan [Start] : mapping State Id to get auto select State acording to District ===============*/
			if(userMaster.getEntityType()==2 )
			{
				map.addAttribute("stateId",userMaster.getDistrictId().getStateId().getStateId());
				map.addAttribute("districtReqNumFlag",userMaster.getDistrictId().getIsReqNoRequired());
			}
			else
				if(userMaster.getEntityType()==3)
					map.addAttribute("stateId",userMaster.getSchoolId().getStateMaster().getStateId());
				
			/*======== Gagan [END] : mapping State Id to get auto select State acording to District ===============*/
			
			List<DistrictRequisitionNumbers> avlbList = new ArrayList<DistrictRequisitionNumbers>();
			List<JobTitleWithDescription> jobTitleList = new ArrayList<JobTitleWithDescription>();
			map.addAttribute("timezone",timeZoneMasterDAO.getAllTimeZone());
						if(userMaster.getDistrictId()!=null)
			{
				Criterion criterion3 = Restrictions.eq("districtMaster", userMaster.getDistrictId());
				Criterion criterion4 = Restrictions.eq("isUsed", false);
				Criterion criterion44 = Restrictions.eq("status", "A");
				Criterion criterion5 = Restrictions.isNull("jobCode");
				
				try{

				
							avlbList = districtRequisitionNumbersDAO.findByCriteria(criterion3,criterion4);
							
							map.addAttribute("avlbList",avlbList);
							jobTitleList = jobTitleWithDescriptionDAO.findByCriteria(criterion3,criterion5); 
								map.addAttribute("jobTitleList",jobTitleList);
						//}
					//}

								/*if(userMaster.getDistrictId()!=null && userMaster!=null && userMaster.getEntityType()==3 && userMaster.getDistrictId().getDistrictId()==804800){
								Criterion criterion6 = Restrictions.eq("schoolMaster", userMaster.getSchoolId());
								avlbList = districtRequisitionNumbersDAO.findByCriteria(Order.asc("requisitionNumber"),criterion3,criterion4,criterion6);
								map.addAttribute("avlbList",avlbList);
							 }else{
								//if(userMaster.getDistrictId().getIsReqNoRequired()!=null)
								//{	if(userMaster.getDistrictId().getIsReqNoRequired()){
										avlbList = districtRequisitionNumbersDAO.findByCriteria(Order.asc("requisitionNumber"),criterion3,criterion4);
										map.addAttribute("avlbList",avlbList);
									//}*/
							if(userMaster!=null && userMaster.getDistrictId().getDistrictId().equals(804800)){
								Session sessionHibernate=districtRequisitionNumbersDAO.getSessionFactory().openSession();
								Criteria cri=sessionHibernate.createCriteria(DistrictRequisitionNumbers.class);
								cri.add(criterion3);
								cri.add(criterion4);
								cri.add(criterion44);
								//cri.addOrder(Order.asc("requisitionNumber"));
								cri.addOrder(OrderBySqlFormula.sqlFormula("cast(requisitionNumber as unsigned) asc"));
								try{
							if( userMaster.getEntityType()==3){
								Criterion criterion6 = Restrictions.eq("schoolMaster", userMaster.getSchoolId());
								cri.add(criterion6);
								avlbList = cri.list();
								map.addAttribute("avlbList",avlbList);
							 }else{
								//if(userMaster.getDistrictId().getIsReqNoRequired()!=null)
								//{	if(userMaster.getDistrictId().getIsReqNoRequired()){
										avlbList = cri.list();
										map.addAttribute("avlbList",avlbList);
									//}
							 }
						}catch(Exception e){
							e.printStackTrace();
						}
					}		
							
							
							
							
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			//for NC Check
			int ncCheck = 0;
			 map.addAttribute("ncCheck", ncCheck);
			
		} 
		catch (Throwable e) 
		{
			e.printStackTrace();
		}
		
		
		
		return pageName;
	}
	
	@RequestMapping(value="/editjobordertest.do", method=RequestMethod.GET)
	public String doEditJobOrderGETOp(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		//Anurag    String pageName="addeditjoborder";
		  String pageName="addeditjobordernew";   // optimized
		try 
		{
			//Added by Gaurav Kumar
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String dateStr = format.format(date).toString().substring(0,4);
			//dateStr = (Integer.parseInt(dateStr)+1)+"-"+(Integer.parseInt(dateStr)+2)+" "+Utility.getLocaleValuePropByKey("lblSchoolYear", locale);
			dateStr = (Integer.parseInt(dateStr))+"-"+(Integer.parseInt(dateStr)+1)+" "+Utility.getLocaleValuePropByKey("lblSchoolYear", locale);
			List<String> positionStartList = new ArrayList<String>();
			positionStartList.add(Utility.getLocaleValuePropByKey("lblCurrentImmediate", locale));
			positionStartList.add(dateStr);
			//End
			
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			boolean notAccesFlag=false;
			DistrictMaster dMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					dMaster=userMaster.getDistrictId();
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			if(userMaster!=null && userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(804800) || userMaster.getDistrictId().getDistrictId().equals(806900))){
				pageName="addeditjoborderjeffco";
			}
			int JobOrderType=0;
			int JobId=0;
			JobOrder jobOrder=null;
			
			if(request.getParameter("JobOrderType")!=null){
				JobOrderType=Integer.parseInt(request.getParameter("JobOrderType"));
			}
			boolean miamiShowFlag=true;
			try{
				if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
					miamiShowFlag=false;
				}
			}catch(Exception e){}
			map.addAttribute("miamiShowFlag", miamiShowFlag);
			
			String roleAccess=null;
			try{
				if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
				}
			}catch(Exception e){
				//e.printStackTrace();
			}
			
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("positionStartList", positionStartList);
			
			if(request.getParameter("JobId")!=null){
				JobId=Integer.parseInt(request.getParameter("JobId"));
				map.addAttribute("JobId", JobId);
			}			
			if(JobId==0){
				jobOrder	=	new JobOrder();
			}else{
				jobOrder	=jobOrderDAO .findById(JobId, false, false);
				try{
					String jobDescription=jobOrder.getJobDescriptionHTML()!=null?jobOrder.getJobDescriptionHTML().replaceAll("&amp;#x13&amp;#x10;", "<br/>"):null;
					jobOrder.setJobDescription(jobDescription.replaceAll("&amp;amp;#x13&amp;amp;#x10;", "<br/>"));
					
				}catch(Exception e){} 
			}
			
			if(jobOrder!=null){
				if(!userMaster.getEntityType().equals(1)){
					if(!jobOrder.getDistrictMaster().getDistrictId().equals(dMaster.getDistrictId())){
						notAccesFlag=true;
					}
				}
			}
			if(notAccesFlag)
			{
				PrintWriter out = response.getWriter();
				response.setContentType("text/html"); 
				out.write("<script type='text/javascript'>");
				out.write("alert('You have no access to this Job.');");
				out.write("window.location.href='/managejoborders.do';");
				out.write("</script>");
				return null;
			}
			
			/*======= Gagan : Disable Other Radio btn on Page load if Any Req No is attached with this joborder ========*/
			int radioDA =0;
			int radioSA =0;
			//List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
			//List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>(); 
			List<DistrictRequisitionNumbers> avlbList = new ArrayList<DistrictRequisitionNumbers>();
			List<JobRequisitionNumbers> attachedList = new ArrayList<JobRequisitionNumbers>();
			List<JobRequisitionNumbers> attachedSchoolList = new ArrayList<JobRequisitionNumbers>();
			StringBuffer schoolReqList=new StringBuffer(); 
			
			Criterion criterion  = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion1 = Restrictions.isNull("schoolMaster");
			Criterion criterion2 = Restrictions.isNotNull("schoolMaster");
			Criterion criterion3 = Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
			Criterion criterion4 = Restrictions.eq("isUsed", false);
			
			
			String location ="";
			
			try
			{
				//System.out.println(" ================  "+jobOrder.getSchool());
				if(jobOrder!=null && jobOrder.getSchool().size()>0)
				{
					if(jobOrder.getSchool().get(0).getLocationCode()!=null && jobOrder.getSchool().get(0).getLocationCode()!="")
						location =jobOrder.getSchool().get(0).getSchoolName()+" ("+jobOrder.getSchool().get(0).getLocationCode()+")";
					else
						location = jobOrder.getSchool().get(0).getSchoolName();
				}
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			
			
			//if(jobOrder!=null &&  jobOrder.getDistrictMaster().getIsReqNoRequired())
			if(jobOrder!=null)
			{
				avlbList = districtRequisitionNumbersDAO.findByCriteria(criterion3,criterion4);
				map.addAttribute("avlbList",avlbList);
				
				if(jobOrder!=null && jobOrder.getNoSchoolAttach()!=null)
				{	
					attachedList = jobRequisitionNumbersDAO.findByCriteria(criterion,criterion1);
					
					for(JobRequisitionNumbers jbrn : attachedList)
					{
						if(jbrn.getStatus()==1)
						{
							radioDA=1;
							break;
						}
					}
				}
				else if(jobOrder !=null && jobOrder.getSelectedSchoolsInDistrict()!=null)
				{
					attachedSchoolList = jobRequisitionNumbersDAO.findByCriteria(criterion,criterion2);
					List<JobRequisitionNumbers> jobreqList=jobRequisitionNumbersDAO.findByCriteria(criterion,criterion2);
					Map<Long, Long>tempSchooMap=new HashMap<Long, Long>();
					for(JobRequisitionNumbers  jrq:jobreqList){
						tempSchooMap.put(jrq.getSchoolMaster().getSchoolId(), jrq.getSchoolMaster().getSchoolId());
					}
					Set<Map.Entry<Long, Long>> set=tempSchooMap.entrySet();
					Iterator<Entry<Long, Long>>iter=set.iterator();
					while(iter.hasNext()){
						Entry<Long,Long>entry=iter.next();
						Long schoolId=tempSchooMap.get(entry.getKey());
						schoolReqList.append(schoolId+",");
					}
					if(schoolReqList.length()>0){
						schoolReqList.substring(0, schoolReqList.length()-1).toString();	
					}
					for(JobRequisitionNumbers jbrn : attachedSchoolList)
					{
						if(jbrn.getStatus()==1)
						{
							radioSA=1;
							break;
						}
					}
				}
				if(radioDA==1)
				{
					map.addAttribute("disableRadioSA", 0);
				}
				else if(radioSA == 1)
				{
					map.addAttribute("disableRadioDA", 0);
				}
				
				map.addAttribute("attachedList",attachedList);
				map.addAttribute("attachedSchoolList",attachedSchoolList);
				if(schoolReqList.length()!=0 && !schoolReqList.equals("")){
					map.addAttribute("requisitionSchoolList",schoolReqList.substring(0, schoolReqList.length()-1).toString());
				}
			}
			/*===================== [ END ]  =======================*/
			
			map.addAttribute("jobStartDate",Utility.getCalenderDateFormart(jobOrder.getJobStartDate()+""));
			map.addAttribute("jobEndDate",Utility.getCalenderDateFormart(jobOrder.getJobEndDate()+""));
			
			
			//jeffco add district Id
			List<JobTitleWithDescription> jobTitleList = new ArrayList<JobTitleWithDescription>();
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("804800")){
				
				jobTitleList = jobTitleWithDescriptionDAO.findByCriteria(criterion3); 
				String jeffcoSpecificSchool="";
				if(jobOrder.getRequisitionNumber()!=null && !jobOrder.getRequisitionNumber().equalsIgnoreCase("")){
					DistrictRequisitionNumbers districtRequisitionNumbers = districtRequisitionNumbersDAO.getDistrictRequisitionNumber(jobOrder.getDistrictMaster(), jobOrder.getRequisitionNumber());
					if(districtRequisitionNumbers!=null){
						JobRequisitionNumbers jobRequisitionNumbers = jobRequisitionNumbersDAO.findJobReqNumbersObj(districtRequisitionNumbers);
							if(jobRequisitionNumbers!=null){
								if(jobRequisitionNumbers.getSchoolMaster()!=null)
								jeffcoSpecificSchool = jobRequisitionNumbers.getSchoolMaster().getSchoolName();
							}
						
					}
				}
				
				map.addAttribute("jobTitleList",jobTitleList);
				map.addAttribute("jeffcoSpecificSchool", jeffcoSpecificSchool);
				if(jobOrder.getPositionStartDate()!=null)
				map.addAttribute("positionStartDate",Utility.getCalenderDateFormart(jobOrder.getPositionStartDate()+""));
				if(jobOrder.getPositionEndDate()!=null)
				map.addAttribute("positionEndDate",Utility.getCalenderDateFormart(jobOrder.getPositionEndDate()+""));
				if(jobOrder.getSalaryRange()!=null)
				map.addAttribute("salaryRange",jobOrder.getSalaryRange());
				if(jobOrder.getEmploymentServicesTechnician()!=null)
				map.addAttribute("primaryESTechnicianHidden",jobOrder.getEmploymentServicesTechnician().getEmploymentservicestechnicianId());
				map.addAttribute("fte",jobOrder.getFte()!=null?jobOrder.getFte():"");
				map.addAttribute("paygrade",jobOrder.getPayGrade()!=null?jobOrder.getPayGrade():"");
				map.addAttribute("daysworked",jobOrder.getDaysWorked()!=null?jobOrder.getDaysWorked():"");
				map.addAttribute("fsalary",jobOrder.getFsalary()!=null?jobOrder.getFsalary():"");
				map.addAttribute("fsalaryA",jobOrder.getFsalaryA()!=null?jobOrder.getFsalaryA():"");
				map.addAttribute("ssalary",jobOrder.getSsalary()!=null?jobOrder.getSsalary():"");
			}
			try{
				if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("806900")){
					map.addAttribute("fte",jobOrder.getFte()!=null?jobOrder.getFte():"");
					map.addAttribute("positionNumber",jobOrder.getRequisitionNumber());
					map.addAttribute("paygrade",jobOrder.getPayGrade()!=null?jobOrder.getPayGrade():"");
					map.addAttribute("mSalary",jobOrder.getmSalary()!=null?jobOrder.getmSalary():"");
					map.addAttribute("accountCode",jobOrder.getAccountCode()!=null?jobOrder.getAccountCode():"");
					map.addAttribute("positionType",jobOrder.getPositionType()!=null?jobOrder.getPositionType():"");
					map.addAttribute("monthsPerYear",jobOrder.getMonthsPerYear()!=null?jobOrder.getMonthsPerYear():"");
					//map.addAttribute("departmentApproval",jobOrder.getDepartmentApproval()!=null?jobOrder.getDepartmentApproval():"");
					map.addAttribute("salaryAdminPlan",jobOrder.getSalaryAdminPlan()!=null?jobOrder.getSalaryAdminPlan():"");
					map.addAttribute("approvalCode",jobOrder.getApprovalCode()!=null?jobOrder.getApprovalCode():"");
					map.addAttribute("step",jobOrder.getStep()!=null?jobOrder.getStep():"");
					
					
					List<JobApprovalProcess> jobApprovalProcessList= jobApprovalProcessDAO.getJobApprovalProcessList();
					map.addAttribute("jobApprovalProcessList",jobApprovalProcessList);
					
					List<JobWiseApprovalProcess> listJobWiseApprovalProcessList = jobWiseApprovalProcessDAO.findByJobId(jobOrder);
				
					if(listJobWiseApprovalProcessList.size()>0){
					
						map.addAttribute("jobApporvalProcessSelected", listJobWiseApprovalProcessList.get(0));
						
					}
					
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			//Dhananjay Verma For Job Approval TPL 4719
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && !jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("806900") && !jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("804800")){
			
				List<JobApprovalProcess> jobApprovalProcessList= jobApprovalProcessDAO.getJobApprovalProcessList();
				map.addAttribute("jobApprovalProcessList",jobApprovalProcessList);
				
				List<JobWiseApprovalProcess> listJobWiseApprovalProcessList = jobWiseApprovalProcessDAO.findByJobId(jobOrder);
			
				if(listJobWiseApprovalProcessList.size()>0){
				
					map.addAttribute("jobApporvalProcessSelected", listJobWiseApprovalProcessList.get(0));
					
				}
			}
			
			
			
			map.addAttribute("jobType", jobOrder.getJobType());
			Criterion criterion5=Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
			List<GeoZoneMaster>zoneMasters=new ArrayList<GeoZoneMaster>();
			
			if(request.getRequestURI()!=null && !request.getRequestURI().equals("") && request.getRequestURI().contains("editjobordertest.do"))
				 zoneMasters= geoZoneMasterDAO.findByCriteria_OP(jobOrder.getDistrictMaster());//Added by kumar avinash
			else
			 zoneMasters=geoZoneMasterDAO.findByCriteria(criterion5);

			StringBuffer geozones=new StringBuffer();
			
			
			for(GeoZoneMaster zone:zoneMasters)
			{	
				if(jobOrder.getGeoZoneMaster()!=null)
				{	
					if(jobOrder.getGeoZoneMaster().getGeoZoneId().equals(zone.getGeoZoneId())){
						geozones.append("<option selected value='"+zone.getGeoZoneId()+"'>"+zone.getGeoZoneName()+"</option>");
					}else{
						geozones.append("<option value='"+zone.getGeoZoneId()+"'>"+zone.getGeoZoneName()+"</option>");
					}
					
				}
				else
				{
					geozones.append("<option  value='"+zone.getGeoZoneId()+"'>"+zone.getGeoZoneName()+"</option>");
				}	
			}
			
			map.addAttribute("jobzones",geozones.toString());
			map.addAttribute("zoneLableRed","show");
			
			
			if(jobOrder.getDistrictMaster().getIsReqNoRequired())
				map.addAttribute("isReqNoFlagByDistrict",1);
			else
				map.addAttribute("isReqNoFlagByDistrict",0);
				
			if(userMaster.getEntityType()==3 && JobOrderType==2){
				map.addAttribute("addJobFlag",false);
			}else {
				map.addAttribute("addJobFlag",true);
			}
			
			if(userMaster!=null  && userMaster.getEntityType()!=null && userMaster.getEntityType()==3 && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getsACreateDistJOb()!=null && userMaster.getDistrictId().getsACreateDistJOb()){
				map.addAttribute("addJobFlag",true);
			}
			
			map.addAttribute("expHireNotEqualToReqNoFlag",jobOrder.getIsExpHireNotEqualToReqNo());
			
			if(userMaster.getEntityType()==2 && JobOrderType==2){
				try{
					if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
						map.addAttribute("DAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
					}else{
						map.addAttribute("DAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				map.addAttribute("SAssessmentUploadURL","");
				map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
				try{	
					map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL());
				}catch(Exception e){
					map.addAttribute("DSAssessmentUploadURL",null);
				}
				map.addAttribute("PageFlag","1");
				map.addAttribute("SchoolDistrictNameFlag","1");
				map.addAttribute("SchoolName",null);
				map.addAttribute("SchoolId",0);
			}else if(userMaster.getEntityType()==3 && JobOrderType==3){
				
				try{
					map.addAttribute("DAssessmentUploadURL",jobOrder.getSchool().get(0).getDistrictId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
					//e.printStackTrace();
				}
				try{
					if(jobOrder.getSchool().get(0).getAssessmentUploadURL()!=null){
						map.addAttribute("SAssessmentUploadURL",jobOrder.getSchool().get(0).getAssessmentUploadURL().trim());	
					}else{
						map.addAttribute("SAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("SAssessmentUploadURL",null);
					//e.printStackTrace();
				}
				map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
				map.addAttribute("SchoolName","location");
				
				map.addAttribute("SchoolId",jobOrder.getSchool().get(0).getSchoolId());
				
				try{
					if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
						map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
					}else{
						map.addAttribute("DSAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("DSAssessmentUploadURL",null);
				}
				map.addAttribute("PageFlag","1");
				map.addAttribute("SchoolDistrictNameFlag","1");
			}else{
				if(JobOrderType==2){
					map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
					
					try{	
						if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
							map.addAttribute("DAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());	
						}else{
							map.addAttribute("DAssessmentUploadURL",null);	
						}
					}catch(Exception e){
						map.addAttribute("DAssessmentUploadURL",null);
					}
					
					map.addAttribute("SAssessmentUploadURL","");
					map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
					try{
						map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL());
					}catch(Exception e){
						map.addAttribute("DSAssessmentUploadURL",null);
					}
					map.addAttribute("SchoolName",null);
					map.addAttribute("SchoolId",0);
				}else if(JobOrderType==3){
						map.addAttribute("DistrictOrSchoolName",jobOrder.getDistrictMaster().getDistrictName());
						map.addAttribute("DistrictOrSchoolId",jobOrder.getDistrictMaster().getDistrictId());
						try{
							//map.addAttribute("SchoolName",jobOrder.getSchool().get(0).getSchoolName());
							map.addAttribute("SchoolName",location);
						}catch(Exception e){
							map.addAttribute("SchoolName",null);
						}
						try{	
							map.addAttribute("DAssessmentUploadURL",jobOrder.getSchool().get(0).getDistrictId().getAssessmentUploadURL().trim());
						}catch(Exception e){
							map.addAttribute("DAssessmentUploadURL",null);
						}
						try{	
							if(jobOrder.getSchool().get(0).getAssessmentUploadURL()!=null){
								map.addAttribute("SAssessmentUploadURL",jobOrder.getSchool().get(0).getAssessmentUploadURL().trim());
							}else{
								map.addAttribute("SAssessmentUploadURL",null);
							}
						}catch(Exception e){
							map.addAttribute("SAssessmentUploadURL",null);
						}
						try{
							map.addAttribute("SchoolId",jobOrder.getSchool().get(0).getSchoolId());
						}catch(Exception e){
							map.addAttribute("SchoolId",null);
						}
						
						
						try{
							if(jobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
								map.addAttribute("DSAssessmentUploadURL",jobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
							}else{
								map.addAttribute("DSAssessmentUploadURL",null);
							}
						}catch(Exception e){
							map.addAttribute("DSAssessmentUploadURL",null);
						}
						
				}
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","0");
			}
			
			if(jobOrder.getSelectedSchoolsInDistrict()!=null && jobOrder.getSelectedSchoolsInDistrict()==1){
				map.addAttribute("allSchoolGradeDistrictVal",2);
			}else{
				map.addAttribute("allSchoolGradeDistrictVal",1);
			}
			map.addAttribute("DistrictExitURL",jobOrder.getExitURL());
			map.addAttribute("DistrictExitMessage",jobOrder.getExitMessage());
			map.addAttribute("districtHiddenId",jobOrder.getDistrictMaster().getDistrictId());
			map.addAttribute("JobOrderType",JobOrderType);
			map.addAttribute("jobOrder",jobOrder);
			if(jobOrder.getDistrictMaster().getDistrictId().equals(1200390) || jobOrder.getDistrictMaster().getDistrictId().equals(7800043)){
				map.addAttribute("JobPostingURL",Utility.getValueOfPropByKey("basePath")+"applyteacherjob.do?jobId="+jobOrder.getJobId());
			} else {
				//map.addAttribute("JobPostingURL",Utility.getShortURL(Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId()));
				map.addAttribute("JobPostingURL",Utility.getValueOfPropByKey("basePath")+"applyteacherjob.do?jobId="+jobOrder.getJobId());
			}

			int exitURLMessageVal=1;
			if(jobOrder.getFlagForMessage()!=null)
			if(jobOrder.getFlagForMessage()!=0){
				if(jobOrder.getFlagForMessage()==1){
					exitURLMessageVal=2;
				}
			}
			int attachJobAssessmentVal=1;
			if(jobOrder.getIsJobAssessment()){
					if(jobOrder.getAttachDefaultDistrictPillar()==1){
						attachJobAssessmentVal=1;
					}else if(jobOrder.getAttachDefaultSchoolPillar()==1){
						attachJobAssessmentVal=3;
					}else if(jobOrder.getAttachDefaultJobSpecificPillar()==1){
						attachJobAssessmentVal=4;
					}else if(jobOrder.getAttachNewPillar()==1){
						attachJobAssessmentVal=2;
					}	
			}
			if(JobOrderType==2){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else if(JobOrderType==3){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else{
				map.addAttribute("districtRootPath",null);
				map.addAttribute("schoolRootPath",null);
			}
			map.addAttribute("attachJobAssessmentVal",attachJobAssessmentVal);
			map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
			if(userMaster.getEntityType()==3 && JobOrderType==2){
				int noOfExpHires=schoolInJobOrderDAO.noOfExpHiresSql(jobOrder,userMaster.getSchoolId(),0);
				if(noOfExpHires!=0){
					map.addAttribute("noOfExpHires",noOfExpHires);
				}else{
					map.addAttribute("noOfExpHires","");
				}
			}else{
				int noOfExpHires=schoolInJobOrderDAO.noOfExpHiresSql(jobOrder,userMaster.getSchoolId(),1);
				if(noOfExpHires!=0){
					map.addAttribute("noOfExpHires",noOfExpHires);
				}else{
					map.addAttribute("noOfExpHires","");
				}
			}
			map.addAttribute("dateTime",Utility.getDateTime());
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);	
			
			/*=== values to dropdown from jobCategoryMaster Table ===*/
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(jobOrder.getDistrictMaster());//jobCategoryMasterDAO.findAllJobCategoryNameByJob(jobOrder);
			map.addAttribute("jobCategoryMasterlst",jobCategoryMasterlst);	
			
			System.out.println(" jobCategoryMasterlst :: "+jobCategoryMasterlst.size());
			
			System.out.println(" joborder cate id :: "+jobOrder.getJobCategoryMaster().getJobCategoryId());
			
			///////////////////// job Sub category
			try
			{
				List<JobCategoryMaster> jobSubCategoryMasterlst = jobCategoryMasterDAO.findAllJobSubCategoryNameByDistrict(jobOrder.getDistrictMaster());
				
				System.out.println(" jobSubCategoryMasterlst>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+jobSubCategoryMasterlst.size());
				
				int chksubCate = 0;
				String chkCase = "editcase";
				if(jobSubCategoryMasterlst.size()>0)
				{
					chksubCate = 1;
				}
				
				System.out.println(" chksubCate :: "+chksubCate+" jobSubCategoryMasterlst size :: "+jobSubCategoryMasterlst.size());
				
				map.addAttribute("jobSubCategoryMasterlst",jobSubCategoryMasterlst);
				map.addAttribute("chksubCate",chksubCate);
				map.addAttribute("chkCase",chkCase);
			//	map.addAttribute("jobcategoryIdForEdit",jobOrder.getJobCategoryMaster().getJobCategoryId());
				
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			//////////////////////////////////////////////////////////////////////////////////////////
			
			
			
			
			DistrictMaster districtMaster = null;
			if(jobOrder.getDistrictMaster()!=null){
				districtMaster=jobOrder.getDistrictMaster();
			}
			try{
				List<JobForTeacher> jobForTeachers=jobForTeacherDAO.findJFTApplicantbyJobOrder_OP(jobOrder);
				if(jobForTeachers!=null && jobForTeachers.size()>0){
					map.addAttribute("jobCategoryDisabled","disabled=\"disabled\"");
					map.addAttribute("isJobApplied",true);
				}else{
					map.addAttribute("jobCategoryDisabled","");
					map.addAttribute("isJobApplied",false);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();
			if(request.getRequestURI()!=null && !request.getRequestURI().equals("") && request.getRequestURI().contains("editjobordertest.do"))
				subjectMasters=subjectMasterDAO.findActiveSubjectByDistrict_OP(districtMaster);
			else
				subjectMasters=subjectMasterDAO.findActiveSubjectByDistrict(districtMaster);
			
			map.addAttribute("subjectList",subjectMasters);
			List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("listStateMaster", listStateMaster);
			
			/*======== Gagan [Start] : mapping State Id to get auto select State acording to District ===============*/
			map.addAttribute("entityType", userMaster.getEntityType());
			if(userMaster.getEntityType()==2 )
				map.addAttribute("stateId",userMaster.getDistrictId().getStateId().getStateId());
			else
				if(userMaster.getEntityType()==3)
					map.addAttribute("stateId",userMaster.getSchoolId().getStateMaster().getStateId());
				else
					if(userMaster.getEntityType()==1 && jobOrder!=null)
					{
						if(JobOrderType==2)
						{
							map.addAttribute("stateId",jobOrder.getDistrictMaster().getStateId().getStateId());
						}
						else
						{
							try{
							// ========== GAgan : In Case of SJO only 1 record will found so i used getindex(0).
								map.addAttribute("stateId",jobOrder.getSchool().get(0).getStateMaster().getStateId());
							}catch(Exception e){
								//e.printStackTrace();
							}
						}
					}
						
			/*======== Gagan [END] : mapping State Id to get auto select State acording to District ===============*/
			
			try{
				String logType="";
				if(JobOrderType==2){
					if(jobOrder.getDistrictMaster()!=null)
						userMaster.setDistrictId(jobOrder.getDistrictMaster());
					logType=msgEnterDistrictJobOrder;
				}else{
					if(jobOrder.getDistrictMaster()!=null)
						userMaster.setDistrictId(jobOrder.getDistrictMaster());
					
					/*if(jobOrder.getSchool().get(0)!=null)
						userMaster.setSchoolId(jobOrder.getSchool().get(0));*/
					logType=msgEnterSchoolJobOrder;
				}
				
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			/* For VVI */
			try
			{
				if(jobOrder!=null)
				{
					String offerVirtualVideoInterview ="";
					String offerDistASMT ="";
					String wantScore="";
					String sendAutoVVILink="";
					String IsInviteOnlyFlag="";
					boolean hideJobFlag=false;
					String notIficationToschool="";
					String jobCompletedVVILink1 = "";
					String jobCompletedVVILink2 = "";
					
					if(jobOrder.getOfferVirtualVideoInterview())
						offerVirtualVideoInterview ="checked";
					
					if(jobOrder.getMaxScoreForVVI()!=null && !jobOrder.getMaxScoreForVVI().equals(""))
						wantScore="checked";
					
					if(jobOrder.getSendAutoVVILink()){
						sendAutoVVILink="checked";
						if(jobOrder.getJobCompletedVVILink()==null){
						
						}else if(jobOrder.getJobCompletedVVILink()==false){
							jobCompletedVVILink1 = "checked";
						}else if(jobOrder.getJobCompletedVVILink()==true){
							jobCompletedVVILink2 = "checked";
						}
					}
					
					if(jobOrder.getNotificationToschool())
						notIficationToschool="checked";
					
					map.addAttribute("offerVVIFlag",offerVirtualVideoInterview);
					map.addAttribute("wantScoreVVIFlag",wantScore);
					map.addAttribute("sendAutoVVILinkFlag",sendAutoVVILink);
					map.addAttribute("jobCompletedVVILink1",jobCompletedVVILink1);
					map.addAttribute("jobCompletedVVILink2",jobCompletedVVILink2);
					
					
					System.out.println("jobCompletedVVILink1 :: "+jobCompletedVVILink1+" jobCompletedVVILink2 :: "+jobCompletedVVILink2);
					
					
					if(jobOrder.getIsInviteOnly()!=null && jobOrder.getIsInviteOnly()){
						IsInviteOnlyFlag="checked";
						if(jobOrder.getHiddenJob()!=null && jobOrder.getHiddenJob()){
							hideJobFlag=true;
						}else if(jobOrder.getHiddenJob()!=null && !jobOrder.getHiddenJob()){
							hideJobFlag=false;
						}
					}
					map.addAttribute("hideJobFlag",hideJobFlag);	
					map.addAttribute("IsInviteOnlyFlag",IsInviteOnlyFlag);
					map.addAttribute("notIficationToschool" ,notIficationToschool);
					String hourPrDay ="";
					if(jobOrder.getHoursPerDay()!=null){
						hourPrDay=jobOrder.getHoursPerDay();
					}
					map.addAttribute("hourPrDay",hourPrDay);
					/*if(jobOrder.getOfferAssessmentInviteOnly())
						offerDistASMT ="checked";
					
					map.addAttribute("offerDistASMTFlag",offerDistASMT);*/

//start sandeep 13-08-15					
				  if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster() != null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId() ==DashboardAjax.NC_HEADQUARTER){
						
					  Criterion criterionRequestion  = Restrictions.eq("jobOrder", jobOrder);
					  List<JobRequisitionNumbers> jobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(criterionRequestion);    
					  if(jobRequisitionNumbers != null && jobRequisitionNumbers.size() >0){
						  if(jobRequisitionNumbers.get(0).getDistrictRequisitionNumbers() != null)
							  map.addAttribute("postionStatus",jobRequisitionNumbers.get(0).getDistrictRequisitionNumbers().getPostionStatus());
					  }
					  map.addAttribute("hrIntegrated", jobOrder.getDistrictMaster().getHrIntegrated());
				  }	
//end					
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			//for NC Check
			int ncCheck = 0;
			 map.addAttribute("ncCheck", ncCheck);
			 
			  map.addAttribute("schoolType", jobOrder.getGradeLevel());
			
		}catch (Exception e){
			e.printStackTrace();
	   }
		
		
		 
		return pageName;
	}	
	
}
