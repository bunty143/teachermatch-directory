package tm.controller.master;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.hqbranchesmaster.ReasonsForWaived;
import tm.bean.master.CityMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.EthinicityMaster;
import tm.bean.master.EthnicOriginMaster;
import tm.bean.master.GenderMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.RaceMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.EmploymentServicesTechnicianDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.hqbranchesmaster.ReasonsForWaivedDAO;
import tm.dao.master.EthinicityMasterDAO;
import tm.dao.master.EthnicOriginMasterDAO;
import tm.dao.master.GenderMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TimeZoneMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.Utility;

@Controller
public class ONRDashBoardController {
	
	@Autowired
	public EthinicityMasterDAO ethinicityMasterDAO;
	
	@Autowired
	public EthnicOriginMasterDAO ethnicOriginMasterDAO;
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private TimeZoneMasterDAO timeZoneMasterDAO;
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private ReasonsForWaivedDAO reasonsForWaivedDAO;
	
	public void setRaceMasterDAO(RaceMasterDAO raceMasterDAO) {
		this.raceMasterDAO = raceMasterDAO;
	}
	@Autowired
	private GenderMasterDAO genderMasterDAO;
	public void setGenderMasterDAO(GenderMasterDAO genderMasterDAO) {
		this.genderMasterDAO = genderMasterDAO;
	}
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private EmploymentServicesTechnicianDAO employmentServicesTechnicianDAO;
	
	
	@RequestMapping(value={"/onboardingdashboard.do","/pnrdashboardnew.do"}, method=RequestMethod.GET)
    public String onr(ModelMap map,HttpServletRequest request){
        UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		
		boolean ncCheck = false;
		
		if(userMaster!=null && userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
			ncCheck = true;
		
		map.addAttribute("entityType", userMaster.getEntityType());
		map.addAttribute("roleId", userMaster.getRoleId().getRoleId());
		map.addAttribute("ncCheck", ncCheck);
		
		//if(userMaster!=null && (userMaster.getDistrictId()!=null &&  userMaster.getDistrictId().getDistrictId().equals(804800)) || (userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)){
		if(userMaster!=null && (userMaster.getDistrictId()!=null &&  userMaster.getDistrictId().getDistrictId().equals(804800))){
			System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyy000000000000000000000000000000000");
			try
			{
				int flagCanEditOrView=0;
				if(userMaster.getEntityType()==2 && userMaster.getRoleId().getRoleName().trim().equalsIgnoreCase("District Admin")){
					flagCanEditOrView=1;
					map.addAttribute("allSchool",schoolInJobOrderDAO.findSchoolJobByDistrictMaster(userMaster.getDistrictId()));
				}
				if(userMaster.getDistrictId()!=null)
					map.addAttribute("districtId",userMaster.getDistrictId().getDistrictId());
				else
					map.addAttribute("hdId",userMaster.getHeadQuarterMaster().getHeadQuarterId());
				
				map.addAttribute("entityType",userMaster.getEntityType());
				List<SchoolMaster> listSchoolId=new ArrayList<SchoolMaster>();
				if(userMaster.getEntityType()==3){
					System.out.println("school Name=="+userMaster.getSchoolId().getSchoolName());
					listSchoolId.add(userMaster.getSchoolId());
					map.addAttribute("allSchool",listSchoolId);
				}
				map.addAttribute("flagCanEditOrView",flagCanEditOrView);	
				map.addAttribute("empST", employmentServicesTechnicianDAO.getAllEST());
				return "jeffcoonrdashboard";
			}catch(Exception e){e.printStackTrace();
				return "jeffcoonrdashboard";
				
			}
		}
		if(userMaster!=null && userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId().equals(1)){
			System.out.println("Kelly OnBoarding Controller");
			try{
				if(userMaster.getRoleId().getRoleId()==10 || userMaster.getRoleId().getRoleId()==12)
				{
					map.addAttribute("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
					map.addAttribute("headQuarterName", userMaster.getHeadQuarterMaster().getHeadQuarterName());
					map.addAttribute("headQuarterMaster", userMaster.getHeadQuarterMaster());
					map.addAttribute("branchMaster", userMaster.getBranchMaster());
				}
				else if(userMaster.getRoleId().getRoleId()==11 || userMaster.getRoleId().getRoleId()==13)
				{
					map.addAttribute("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
					map.addAttribute("headQuarterName", userMaster.getHeadQuarterMaster().getHeadQuarterName());
					map.addAttribute("branchMasterId", userMaster.getBranchMaster().getBranchId());
					map.addAttribute("branchName", userMaster.getBranchMaster().getBranchName());
					map.addAttribute("branchMaster", userMaster.getBranchMaster());
					
				}
				List<JobCategoryMaster> jobCategoryMastersList = null;
				List<ReasonsForWaived> reasonsForWaivedList = null;
				
					try {
						jobCategoryMastersList = jobCategoryMasterDAO.findAllMasterJobCategorysOfHeadQuarter(userMaster.getHeadQuarterMaster());
						reasonsForWaivedList=reasonsForWaivedDAO.findAll();
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					map.addAttribute("jobCategoryMastersList", jobCategoryMastersList);
					map.addAttribute("reasonsForWaivedList", reasonsForWaivedList);
				
				return "kellyonb";
			}catch(Exception e){
				e.printStackTrace();
				return "kellyonb";
			}
			
		}
		
		pnrdashboardmethod(map, request, userMaster, roleId, raceMasterDAO, genderMasterDAO, ethinicityMasterDAO, ethnicOriginMasterDAO, roleAccessPermissionDAO, statusMasterDAO, timeZoneMasterDAO,stateMasterDAO);
		
		
		/*
		 if(userMaster.getEntityType()==2){
				map.addAttribute("districtId",userMaster.getDistrictId().getDistrictId());
		 }
		 	List<StateMaster> listStateMasters 		= null;
			List<CityMaster> listCityMasters 		= null;
			TeacherPersonalInfo teacherpersonalinfo = null;
			TeacherDetail tDetail 					= null;

			List<EthnicOriginMaster> lstEthnicOriginMasters	=	null;
			List<EthinicityMaster> lstEthinicityMasters		=	null;
			List<RaceMaster> listRaceMasters 				= 	null;
			List<GenderMaster> listGenderMasters 			= 	null;
		 try{
			 
			 	listRaceMasters 	=  	raceMasterDAO.findAllRaceByOrder();
				listGenderMasters 	= 	genderMasterDAO.findAllGenderByOrder();
				lstEthinicityMasters	=	ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
				lstEthnicOriginMasters	=	ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));

				List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();

				map.addAttribute("lstEthinicityMasters", lstEthinicityMasters);
				map.addAttribute("lstEthnicOriginMasters", lstEthnicOriginMasters);
				map.addAttribute("listGenderMasters", listGenderMasters);
				map.addAttribute("listRaceMasters", listRaceMasters);
				map.addAttribute("teacherpersonalinfo", teacherpersonalinfo);
				
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 
		 String roleAccess=null;
		 String roleAccessForONR=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
				roleAccessForONR=roleAccessPermissionDAO.getMenuOptionList(roleId,61,"onboardingdashboard.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statusTotal = {"comp","icomp","hird","rem","widrw","vlt"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statusTotal);
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			if(userMaster.getEntityType()!=1){
				map.addAttribute("DistrictName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userMaster.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictName",null);
			}
			if(userMaster.getEntityType()==3){
				map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
			}else{
				map.addAttribute("SchoolName",null);
			}
			map.addAttribute("entityType",userMaster.getEntityType());
			
			map.addAttribute("lstStatusMasters",lstStatusMasters);
			List<StateMaster> lstStateMaster = new ArrayList<StateMaster>();
			lstStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("lstStateMaster",lstStateMaster);
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("roleAccessForONR", roleAccessForONR);
			map.addAttribute("dateTime", Utility.getDateTime());
			map.addAttribute("timezone",timeZoneMasterDAO.getAllTimeZone());
			
			String sAddStatus="0";
			if(roleAccessForONR!=null && roleAccessForONR.contains("|1|"))
				sAddStatus="1";
			map.addAttribute("sAddStatus", sAddStatus);
			
			boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,statusNotes=false;
			if(userMaster.getEntityType()!=1)
			{

				try{

					DistrictMaster districtMasterObj=(DistrictMaster)userMaster.getDistrictId();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getStatusNotes()){
						statusNotes=true;
					}
				}catch(Exception e){ e.printStackTrace();}
			}else
			{
				achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
			}
			Map<String,Boolean> prefMap = new HashMap<String, Boolean>();
			prefMap.put("achievementScore", achievementScore);
			prefMap.put("tFA", tFA);
			prefMap.put("demoClass", demoClass);
			prefMap.put("JSI", JSI);
			prefMap.put("teachingOfYear", teachingOfYear);
			prefMap.put("expectedSalary", expectedSalary);
			prefMap.put("fitScore", fitScore);
			map.addAttribute("statusNotes",statusNotes);
			map.addAttribute("prefMap",prefMap);
		 */
		 
		return "onrdashboard";
    }
	
	public synchronized static void pnrdashboardmethod(ModelMap map,HttpServletRequest request,UserMaster userMaster,int roleId,RaceMasterDAO raceMasterDAO,GenderMasterDAO genderMasterDAO,EthinicityMasterDAO ethinicityMasterDAO,EthnicOriginMasterDAO ethnicOriginMasterDAO,RoleAccessPermissionDAO roleAccessPermissionDAO,StatusMasterDAO statusMasterDAO,TimeZoneMasterDAO timeZoneMasterDAO,StateMasterDAO stateMasterDAO)
	{
		System.out.println( " pnrdashboardmethod ");

		 if(userMaster.getEntityType()==2){
				map.addAttribute("districtId",userMaster.getDistrictId().getDistrictId());
		 }
		 	List<StateMaster> listStateMasters 		= null;
			List<CityMaster> listCityMasters 		= null;
			TeacherPersonalInfo teacherpersonalinfo = null;
			TeacherDetail tDetail 					= null;

			List<EthnicOriginMaster> lstEthnicOriginMasters	=	null;
			List<EthinicityMaster> lstEthinicityMasters		=	null;
			List<RaceMaster> listRaceMasters 				= 	null;
			List<GenderMaster> listGenderMasters 			= 	null;
		 try{
			 
			 	listRaceMasters 	=  	raceMasterDAO.findAllRaceByOrder();
				listGenderMasters 	= 	genderMasterDAO.findAllGenderByOrder();
				lstEthinicityMasters	=	ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
				lstEthnicOriginMasters	=	ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));

				List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();

				map.addAttribute("lstEthinicityMasters", lstEthinicityMasters);
				map.addAttribute("lstEthnicOriginMasters", lstEthnicOriginMasters);
				map.addAttribute("listGenderMasters", listGenderMasters);
				map.addAttribute("listRaceMasters", listRaceMasters);
				map.addAttribute("teacherpersonalinfo", teacherpersonalinfo);
				
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 
		 String roleAccess=null;
		 String roleAccessForONR=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
				roleAccessForONR=roleAccessPermissionDAO.getMenuOptionList(roleId,61,"onboardingdashboard.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statusTotal = {"comp","icomp","hird","rem","widrw","vlt"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statusTotal);
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			if(userMaster.getEntityType()!=1 && userMaster.getEntityType()!=5 && userMaster.getEntityType()!=6){
				map.addAttribute("DistrictName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userMaster.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictName",null);
			}
			if(userMaster.getEntityType()==3){
				map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
			}else{
				map.addAttribute("SchoolName",null);
			}
			map.addAttribute("entityType",userMaster.getEntityType());
			
			map.addAttribute("lstStatusMasters",lstStatusMasters);
			List<StateMaster> lstStateMaster = new ArrayList<StateMaster>();
			lstStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("lstStateMaster",lstStateMaster);
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("roleAccessForONR", roleAccessForONR);
			map.addAttribute("dateTime", Utility.getDateTime());
			map.addAttribute("timezone",timeZoneMasterDAO.getAllTimeZone());
			
			String sAddStatus="0";
			if(roleAccessForONR!=null && roleAccessForONR.contains("|1|"))
				sAddStatus="1";
			map.addAttribute("sAddStatus", sAddStatus);
			
			boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,statusNotes=false;
			if(userMaster.getEntityType()!=1 && userMaster.getEntityType()!=5 && userMaster.getEntityType()!=6)
			{

				try{

					DistrictMaster districtMasterObj=(DistrictMaster)userMaster.getDistrictId();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getStatusNotes()){
						statusNotes=true;
					}
				}catch(Exception e){ e.printStackTrace();}
			}else
			{
				achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
			}
			Map<String,Boolean> prefMap = new HashMap<String, Boolean>();
			prefMap.put("achievementScore", achievementScore);
			prefMap.put("tFA", tFA);
			prefMap.put("demoClass", demoClass);
			prefMap.put("JSI", JSI);
			prefMap.put("teachingOfYear", teachingOfYear);
			prefMap.put("expectedSalary", expectedSalary);
			prefMap.put("fitScore", fitScore);
			map.addAttribute("statusNotes",statusNotes);
			map.addAttribute("prefMap",prefMap);
		 	
	}
}
