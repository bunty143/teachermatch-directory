package tm.controller.master;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.EventDetails;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.EventDetailsDAO;

@Controller
public class ManageParticipantsController 
{
	
	@Autowired
	private EventDetailsDAO eventDetailsDAO;
	
	
	@RequestMapping(value="/manageparticipant.do", method=RequestMethod.GET)
	public String manageParticipantGET(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		boolean notAccesFlag=false;
		DistrictMaster dMaster=null;
		UserMaster userSession=null;
		String eventId=request.getParameter("eventId");
		int importacilitatorCheck=0;
		EventDetails eventDetails=eventDetailsDAO.findById(Integer.parseInt(eventId), false,false);
		 
		try {
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
         	userSession		=	(UserMaster) session.getAttribute("userMaster");
         	if(userSession!=null){
         		dMaster=userSession.getDistrictId();
         	}
         	
         	try {
         		if(eventDetails!=null && !userSession.getEntityType().equals(1) && eventDetails.getHeadQuarterMaster()==null){
         			if(eventDetails.getDistrictMaster()!=null && !eventDetails.getDistrictMaster().getDistrictId().equals(dMaster.getDistrictId())){
         				notAccesFlag=true;
         			}
         		}
         		if(notAccesFlag)
         		{
         			PrintWriter out = response.getWriter();
         			response.setContentType("text/html"); 
         			out.write("<script type='text/javascript'>");
         			out.write("alert('You have no access to this event.');");
         			out.write("window.location.href='manageevents.do';");
         			out.write("</script>");
         			return null;
         		}
         	} catch (Exception e1) {
         		e1.printStackTrace();
         	}
         	
			map.addAttribute("userSession", userSession);
			map.addAttribute("eventDetails", eventDetails);			
			map.addAttribute("importacilitatorCheck",importacilitatorCheck);
			//System.out.println("call participants controller");
		} catch (Exception e) {
		}
		return "participants";
	}
	
	
}
