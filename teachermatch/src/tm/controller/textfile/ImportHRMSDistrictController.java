package tm.controller.textfile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import tm.bean.UserUploadFolderAccess;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.StateMaster;



import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.StateMasterDAO;


/* 
 * @Author: Sandeep Yadav
 */


@Controller
public class ImportHRMSDistrictController {

	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	

	public void setDistricMasterDAO(DistrictMasterDAO districMasterDAO) {
		this.districtMasterDAO = districMasterDAO;
	}
	
	@Autowired
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	
	public void setUserUploadFolderAccessDAO(
			UserUploadFolderAccessDAO userUploadFolderAccessDAO) {
		this.userUploadFolderAccessDAO = userUploadFolderAccessDAO;
	}

	@Autowired
	private CountryMasterDAO countryMasterDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;


	@RequestMapping(value="/hrmsdistrict.do", method=RequestMethod.GET)
	public @ResponseBody String getAllHRMSDistrictData(ModelMap map,HttpServletRequest request)
	{
		    System.out.println("::::: Use ImportHRMSDistrict Controller  :::::");
		    List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
			try {
				Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
				Criterion active = Restrictions.eq("status", "A");
				uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
			} catch (Exception e) {
				e.printStackTrace();
			}
				
			String fileName = "TM_LEA_DISTRICT.txt";
		    String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
	  // String folderPath = "E:\\";
			
		
		return saveHRMSDistrictData(fileName,folderPath);
	}
	
	
	private Vector vectorDataTxt = new Vector();
    public String saveHRMSDistrictData(String fileName,String folderPath){	
			
			 System.out.println("::::::::HRMSDistrictData:::::::::");
			 
			 String filePath=folderPath+fileName;
		 	 System.out.println("filePath  "+filePath);
		 	 try {
		 		 if(fileName.contains(".txt") || fileName.contains(".TXT")){
		 			   vectorDataTxt = readDataTxt(filePath);
		 		 }else{
	    			 //break;
	    	 }
		} catch (Exception e) {
			e.printStackTrace();
		}
    	 
		
 	    
    	 	    	 
    	 int LEANumber_count=12;
    	 int LEAName_count=12;
    	 int streetAddress_count=12; 
    	 int city_count=12;
    	 int ZIP_count=12;	  
    	 int state_count=12;
    	 int NCESLEAId_count=12;
    	 int NCESStateId_count=12;
    	 int phone_count=12; 
    	 int fax_count=12;
    	 int url_count=12;
    	 int district_status_count=12;
    	 
    	 String final_error_store="";
    	 int numOfError=0;
    	 boolean row_error=false;
 /*   	 List<DistrictMaster> districtMasterList =  districtMasterDAO.findAll();
    	 Map<String, DistrictMaster> districtMasterMap = new HashMap<String, DistrictMaster>();
    	 if(districtMasterList!=null && districtMasterList.size()>0)
    	 for (DistrictMaster districtMaster2 : districtMasterList) {
    		 districtMasterMap.put(districtMaster2.getDistrictName(), districtMaster2);
		}
 */   	 
    	 List<DistrictMaster> districtMasterList = new ArrayList<DistrictMaster>();
    	 Map<String, DistrictMaster> districtMasterMap = new HashMap<String, DistrictMaster>();
    	 Map<String, StateMaster> stateMasterMap = new HashMap<String, StateMaster>();
    	 List<StateMaster> stateMasterList = new ArrayList<StateMaster>();
    	 HeadQuarterMaster headQuarterMaster = new HeadQuarterMaster();
    	 headQuarterMaster.setHeadQuarterId(2);
    	 Transaction txOpen = null;
         StatelessSession statelesSsession =null;
    	
		try {
			SessionFactory sessionFactory=districtMasterDAO.getSessionFactory();
			 statelesSsession = sessionFactory.openStatelessSession();
			 txOpen = statelesSsession.beginTransaction();
			 
			 districtMasterList =  districtMasterDAO.getDistrictListByHeadQuater(headQuarterMaster);
			 districtMasterMap = new HashMap<String, DistrictMaster>();
			 if(districtMasterList!=null && districtMasterList.size()>0){
				 for (DistrictMaster districtMaster2 : districtMasterList) {
					 districtMasterMap.put(districtMaster2.getLocationCode(), districtMaster2);
				 }
			 }else{
				 System.out.println("districtMasterList size ##########"+districtMasterList.size());
				 return null;
			 }
			 
			 CountryMaster countryMaster = countryMasterDAO.getCountryByShortCode("US");
	    	 
	    	 stateMasterList =  stateMasterDAO.findActiveStateByCountryId(countryMaster);
			 if(stateMasterList!=null && stateMasterList.size()>0)
			 for (StateMaster stateMaster2 : stateMasterList) {
				 stateMasterMap.put(stateMaster2.getStateShortName(), stateMaster2);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
    	 
    	 txOpen =statelesSsession.beginTransaction();
    	 int insertCount = 0;
	     int updateCount = 0;
    	 
		 for(int i=0; i<vectorDataTxt.size(); i++) {
			 Vector vectorCellEachRowData = (Vector) vectorDataTxt.get(i);
			 
			 
	            String LEANumber="";
	            String LEAName="";
	            String streetAddress="";
	            String city =""; 
	            String ZIP="";
	            String state="";
	            String NCESLEAId="";
	            String NCESStateId="";
	            String phone =""; 
	            String fax="";
	            String url="";
	            String districtStatus="";
	            String errorText="";
		  	    String rowErrorText="";
	  	     
	  	      
	  	      
	  	    for(int j=0; j<vectorCellEachRowData.size(); j++) {
  	        	try{
  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("LEA Number")){
  	        			LEANumber_count=j;
	            	}
	            	if(LEANumber_count==j)
	            		LEANumber=vectorCellEachRowData.get(j).toString().trim();
  	        		
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("LEA Name")){
	            		LEAName_count=j;
	            	}
	            	if(LEAName_count==j)
	            		LEAName=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Street Address")){
	            		streetAddress_count=j;
					}
	            	if(streetAddress_count==j)
	            		streetAddress=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("LEA City")){
	            		city_count=j;
					}
	            	if(city_count==j)
	            		city=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("LEA Zip Code")){
	            		ZIP_count=j;
					}
	            	if(ZIP_count==j)
	            		ZIP=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("LEA State Code")){
	            		state_count=j;
	            	}
	            	if(state_count==j)
	            		state=vectorCellEachRowData.get(j).toString().trim();
  	        		
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("NCES LEA Identifier")){
	            		NCESLEAId_count=j;
	            	}
	            	if(NCESLEAId_count==j)
	            		NCESLEAId=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("NCES State Identifier")){
	            		NCESStateId_count=j;
					}
	            	if(NCESStateId_count==j)
	            		NCESStateId=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("LEA Telephone Number")){
	            		phone_count=j;
					}
	            	if(phone_count==j)
	            		phone=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("LEA Fax Number")){
	            		fax_count=j;
					}
	            	if(fax_count==j)
	            		fax=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Web Site URL")){
	            		url_count=j;
					}
	            	
	            	if(url_count==j)
	            		url=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Status")){
	            		district_status_count=j;
					}
	            	
	            	if(district_status_count==j)
	            		districtStatus=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	
	            	
	            	
	            	
            	}catch(Exception e){
            		e.printStackTrace();
            	}
  	        }
	  	    
	  	  if(i==0){
	  		  
	  		  if(!LEANumber.equalsIgnoreCase("LEA Number")){
	  			rowErrorText="LEA Number column is not found";
	  			row_error=true;
	  		  }
	  		  if(!LEAName.equalsIgnoreCase("LEA Name")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="LEA Name column is not found";
	  			row_error=true;
	  		  }
	  		  if(!streetAddress.equalsIgnoreCase("Street Address")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="Street Address column is not found";
	  			row_error=true;  
	  		  }
	  		if(!city.equalsIgnoreCase("LEA City")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="LEA City column is not found";
	  			row_error=true;  
	  		  }
	  		
	  		if(!ZIP.equalsIgnoreCase("LEA Zip Code")){
	  			rowErrorText="LEA Zip Code column is not found";
	  			row_error=true;
	  		  }
	  		  if(!state.equalsIgnoreCase("LEA State Code")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="LEA State Code column is not found";
	  			row_error=true;
	  		  }
	  		  if(!NCESLEAId.equalsIgnoreCase("NCES LEA Identifier")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="NCES LEA Identifier column is not found";
	  			row_error=true;  
	  		  }
	  		if(!NCESStateId.equalsIgnoreCase("NCES State Identifier")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="NCES State Identifier column is not found";
	  			row_error=true;  
	  		  }
	  		
	  		 if(!phone.equalsIgnoreCase("LEA Telephone Number")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="LEA Telephone Number column is not found";
		  			row_error=true;
		  		  }
		  		  if(!fax.equalsIgnoreCase("LEA Fax Number")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="LEA Fax Number column is not found";
		  			row_error=true;  
		  		  }
		  		if(!url.equalsIgnoreCase("Web Site URL")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="Web Site URL column is not found";
		  			row_error=true;  
		  		  }
		  		
		  		if(!districtStatus.equalsIgnoreCase("Status")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="Status column is not found";
		  			row_error=true;  
		  		  }
	  	}
	  	    
	  	  
		  	if(row_error){
		  		numOfError++;
		  		File file = new File(folderPath+"hrmsDistrictError.txt");
				// if file doesnt exists, then create it
		  		try {
			  		if (!file.exists()) {
			  			file.createNewFile();
					}	    				
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(rowErrorText);
		
					bw.close();
		  		} catch (IOException e) {
					
					e.printStackTrace();
				}
				
		  		 break;
		  	 }
	  	    
	  	    
	  	    
	  	    if(i != 0 && row_error==false){
	  	    	
	  	    	boolean errorFlag=false;
	  	    	
	  	    	if(LEANumber.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="LEA Number is empty";
  	    			    errorFlag=true;
	        	}
	  	    	
	  	    	
	  	    	if(LEAName.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="LEA Name is empty";
  	    			    errorFlag=true;
	        	}
	  	    	
                if(streetAddress.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Street Address is empty";
  	    			    errorFlag=true;
	        	}
                
                
                if(city.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		    if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="LEA City is empty";
  	    			    errorFlag=true;
	        	}
                
                
	            if(ZIP.replaceAll("\"", "").equalsIgnoreCase("")){
		  	    		
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="LEA Zip Code is empty";
		    			    errorFlag=true;
	        	}
	  	    	
	  	    	
	  	    	if(state.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="LEA State Code is empty";
		    			    errorFlag=true;
	        	}
	  	    	
	            if(NCESLEAId.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="NCES LEA Identifier is empty";
		    			    errorFlag=true;
	        	}
	            
	            
	            if(NCESStateId.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		    if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="NCES State Identifier is empty";
		    			    errorFlag=true;
	        	}
	            
	           
		        if(phone.replaceAll("\"", "").equalsIgnoreCase("")){
		  	    		
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="LEA Telephone Number is empty";
		    			    errorFlag=true;
	        	}
	  	    	
	  	    	
	  	    	if(fax.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="LEA Fax Number is empty";
		    			    errorFlag=true;
	        	}
	  	    	
	            if(url.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Web Site URL is empty";
		    			    errorFlag=true;
	        	}
	            
	            if(districtStatus.replaceAll("\"", "").equalsIgnoreCase("")){
  	    			if(errorFlag){
						errorText+=",";	
					}
        			errorText+="Status is empty";
	    			    errorFlag=true;
        	}
	            
	            
	            
                
                
	  	    	
                if(!errorText.equals("")){
	        			int row = i+1;
  	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+LEANumber+" , "+LEAName+" , "+streetAddress+" , "+city+" , "+ZIP+" , "+state+" , "+NCESLEAId+" , "+NCESStateId+" , "+phone+" , "+fax+" , "+url+","+districtStatus+"<>";
  	    			numOfError++;
  	    		}
	  	    	
                try {
                	
                	
                	DistrictMaster districtMasterObj = null;
                	StateMaster stateMasterObj = null;
                	
                	if(LEANumber.replaceAll("\"", "").isEmpty()){
                		System.out.println("Row "+i+" , LEANumber is empty");
                		continue;
                	}
                	if(streetAddress.replaceAll("\"", "").isEmpty()){
                		streetAddress = "";
                		//continue;
                	}
                	if(city.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , City is empty");
                		city="";
                		//continue;
                	}
                	if(ZIP.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , Zip is empty");
                		ZIP="";
                		//continue;
                	}
                	if(phone.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , Phone is empty");
                		phone="";
                		//continue;
                	}
                	if(fax.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , Fax is empty");
                		fax="";
                		//continue;
                	}
                	
                	if(url.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , URL is empty");
                		url="";
                		//continue;
                	}
                	if(NCESStateId.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , NCES State Id is empty");
                		NCESStateId="0";
                		//continue;
                	}                	
                	
                	if(districtMasterMap.containsKey(LEANumber.replaceAll("\"", ""))){
                		districtMasterObj = districtMasterMap.get(LEANumber.replaceAll("\"", ""));
                	}
                	
                	if(stateMasterMap.containsKey(state.replaceAll("\"", ""))){
                		stateMasterObj = stateMasterMap.get(state.replaceAll("\"", ""));
                	}
                	
                	if(stateMasterObj == null)
                		continue;
                	
                   
                     
                	 if(districtMasterObj == null){
                    	 districtMasterObj = new DistrictMaster();
                    	 
                    	 
                    	 districtMasterObj.setHeadQuarterMaster(headQuarterMaster);
                    	 districtMasterObj.setAreAllSchoolsInContract(false);
                    	 districtMasterObj.setDisplayCGPA(false);
                    	 districtMasterObj.setDisplayJSI(false);
                    	 districtMasterObj.setOfferAssessmentInviteOnly(false);
                    	 districtMasterObj.setPostingOnDistrictWall(2);
                    	 districtMasterObj.setPostingOnTMWall(2);
                    	 districtMasterObj.setCanTMApproach(1);
                    	 districtMasterObj.setQuestDistrict(0);
                    	 districtMasterObj.setHiringAuthority("");
                    	 districtMasterObj.setDistrictApproval(1);
                    	 districtMasterObj.setIsPortfolioNeeded(false);
                    	 districtMasterObj.setIsWeeklyCgReport(0);
                    	 districtMasterObj.setIsResearchDistrict(false);
                    	 districtMasterObj.setTotalNoOfSchools(1);
                    	 districtMasterObj.setTotalNoOfTeachers(1);
                    	 districtMasterObj.setTotalNoOfStudents(1);
                    	 districtMasterObj.setDisplayName("Distric Name");
                    	 districtMasterObj.setDmName("");
                    	 districtMasterObj.setAcName("");
                    	 districtMasterObj.setAnnualSubsciptionAmount(0.0);
                    	 districtMasterObj.setIsReqNoRequired(false);
                    	 districtMasterObj.setStatusNotes(false);
                    	 districtMasterObj.setDisplayTMDefaultJobCategory(true);
                    	 districtMasterObj.setSetAssociatedStatusToSetDPoints(false);
                    	 districtMasterObj.setWritePrivilegeToSchool(false);
                    	 districtMasterObj.setResetQualificationIssuesPrivilegeToSchool(true);
                    	 districtMasterObj.setStatusPrivilegeForSchools(false);
                    	 districtMasterObj.setCommunicationsAccess(2);
                    	 districtMasterObj.setDisplayAchievementScore(false);
                    	 districtMasterObj.setDisplayTFA(false);
                    	 districtMasterObj.setDisplayYearsTeaching(false);
                    	 districtMasterObj.setDisplayExpectedSalary(false);
                    	 districtMasterObj.setDisplayFitScore(false);
                    	 districtMasterObj.setDisplayDemoClass(false);
                    	 districtMasterObj.setOfferDistrictSpecificItems(false);
                    	 districtMasterObj.setOfferQualificationItems(false);
                    	 districtMasterObj.setOfferEPI(false);
                    	 districtMasterObj.setOfferJSI(false);
                    	 districtMasterObj.setOfferVirtualVideoInterview(false);
                    	 districtMasterObj.setSendAutoVVILink(false);
                    	 districtMasterObj.setOfferPortfolioNeeded(false);
                    	 districtMasterObj.setJobAppliedDate(false);
                    	 districtMasterObj.setIsZoneRequired(0);
                    	 districtMasterObj.setAutoNotifyCandidateOnAttachingWithJob(false);
                    	 
                    	 
                    	 districtMasterObj.setLocationCode(LEANumber.replaceAll("\"", ""));
                    	 districtMasterObj.setDistrictName(LEAName.replaceAll("\"", ""));
                    	 districtMasterObj.setAddress(streetAddress.replaceAll("\"", ""));
                    	 districtMasterObj.setCityName(city.replaceAll("\"", ""));
                    	 districtMasterObj.setStateId(stateMasterObj);
                    	 districtMasterObj.setZipCode(ZIP.replaceAll("\"", ""));
                    	 districtMasterObj.setPhoneNumber(phone.replaceAll("\"", ""));
                    	 districtMasterObj.setFaxNumber(fax.replaceAll("\"", ""));
                         districtMasterObj.setWebsite(url.replaceAll("\"", ""));
                         if(!NCESStateId.replaceAll("\"", "").isEmpty())
                    	 districtMasterObj.setNcesStateId(Integer.parseInt(NCESStateId.replaceAll("\"", "")));                         
                         if(districtStatus.replaceAll("\"", "").isEmpty()){                		
                     		districtStatus="A";                		
                     	}
                    	 districtMasterObj.setStatus(districtStatus);
                         
                    	 districtMasterObj.setCreatedDateTime(new Date());
                    	 
                    	 //start 02-09-15 (Sandeep)
                    	 districtMasterObj.setFlagForMessage(0);
                    	 districtMasterObj.setAllowMessageTeacher(0);
                    	 districtMasterObj.setNoSchoolUnderContract(0);
                    	 districtMasterObj.setAllSchoolsUnderContract(0);
                    	 districtMasterObj.setAllGradeUnderContract(0);
                    	 districtMasterObj.setSelectedSchoolsUnderContract(0);
                    	 districtMasterObj.setCandidateFeedNormScore(0);
                    	 districtMasterObj.setCandidateFeedDaysOfNoActivity(0);
                    	 districtMasterObj.setCanSchoolOverrideCandidateFeed(false);
                    	 districtMasterObj.setJobFeedCriticalJobActiveDays(0);
                    	 districtMasterObj.setJobFeedCriticalCandidateRatio(0);
                    	 districtMasterObj.setJobFeedCriticalNormScore(0);
                    	 districtMasterObj.setJobFeedAttentionJobActiveDays(0);
                    	 districtMasterObj.setJobFeedAttentionJobNotFilled(false);
                    	 districtMasterObj.setJobFeedAttentionNormScore(0);
                    	 districtMasterObj.setIsReqNoForHiring(false);
                    	 districtMasterObj.setCanSchoolOverrideJobFeed(false);
                    	 districtMasterObj.setDisplayPhoneInterview(false);
                    	 districtMasterObj.setNoEPI(false);
                    	 districtMasterObj.setIsTeacherPoolOnLoad(false);
                    	 districtMasterObj.setApprovalBeforeGoLive(false);
                    	 districtMasterObj.setNoOfApprovalNeeded(0);
                    	 districtMasterObj.setReminderFrequencyInDays(0);
                    	 districtMasterObj.setSendReminderToIcompCandiates(false);
                    	 districtMasterObj.setReminderOfFirstFrequencyInDays(0);
                    	 districtMasterObj.setNoOfReminder(0);
                    	 districtMasterObj.setSendReminderForPortfolio(false);
                    	 districtMasterObj.setShowReferenceToSA(0);
                    	 districtMasterObj.setBuildApprovalGroup(false);
                    	 districtMasterObj.setDisplaySeniorityNumber(false);
                    	 districtMasterObj.setQqThumbShowOrNot(false);
                    	 districtMasterObj.setDisplayCandidateTOMosaic(false);
                    	 
                    	 //end
                    	 
                    	 statelesSsession.insert(districtMasterObj);
                    	 insertCount++;
                    	 
                     }else{
                    	 
                    	 districtMasterObj.setLocationCode(LEANumber.replaceAll("\"", ""));
                    	 districtMasterObj.setAddress(streetAddress.replaceAll("\"", ""));
                    	 districtMasterObj.setCityName(city.replaceAll("\"", ""));
                    	 districtMasterObj.setStateId(stateMasterObj);
                    	 districtMasterObj.setZipCode(ZIP.replaceAll("\"", ""));
                    	 districtMasterObj.setPhoneNumber(phone.replaceAll("\"", ""));
                    	 districtMasterObj.setFaxNumber(fax.replaceAll("\"", ""));
                         districtMasterObj.setWebsite(url.replaceAll("\"", ""));
                         if(!NCESStateId.replaceAll("\"", "").isEmpty())
                    	 districtMasterObj.setNcesStateId(Integer.parseInt(NCESStateId.replaceAll("\"", "")));
                         if(districtStatus.replaceAll("\"", "").isEmpty()){ 
                        	 if(districtMasterObj.getStatus()!=null)
                        	 districtMasterObj.setStatus(districtMasterObj.getStatus());       		
                      	 }
                         else
                         {
                    	 districtMasterObj.setStatus(districtStatus);
                         }
                    	 districtMasterObj.setDistrictName(LEAName.replaceAll("\"", ""));                    
                    	 //districtMasterObj.setCreatedDateTime(new Date());
                    	 
                    	//start 02-09-15 (Sandeep)
//                    	 districtMasterObj.setFlagForMessage(0);
//                    	 districtMasterObj.setAllowMessageTeacher(0);	
//                    	 districtMasterObj.setNoSchoolUnderContract(0);
//                    	 districtMasterObj.setAllSchoolsUnderContract(0);
//                    	 districtMasterObj.setAllGradeUnderContract(0);
//                    	 districtMasterObj.setSelectedSchoolsUnderContract(0);
//                    	 districtMasterObj.setCandidateFeedNormScore(0);
//                    	 districtMasterObj.setCandidateFeedDaysOfNoActivity(0);
//                    	 districtMasterObj.setCanSchoolOverrideCandidateFeed(false);
//                    	 districtMasterObj.setJobFeedCriticalJobActiveDays(0);
//                    	 districtMasterObj.setJobFeedCriticalCandidateRatio(0);
//                    	 districtMasterObj.setJobFeedCriticalNormScore(0);
//                    	 districtMasterObj.setJobFeedAttentionJobActiveDays(0);
//                    	 districtMasterObj.setJobFeedAttentionJobNotFilled(false);
//                    	 districtMasterObj.setJobFeedAttentionNormScore(0);
//                    	 districtMasterObj.setIsReqNoForHiring(false);
//                    	 districtMasterObj.setCanSchoolOverrideJobFeed(false);
//                    	 districtMasterObj.setDisplayPhoneInterview(false);
//                    	 districtMasterObj.setNoEPI(false);
//                    	 districtMasterObj.setIsTeacherPoolOnLoad(false);
//                    	 districtMasterObj.setApprovalBeforeGoLive(false);
//                    	 districtMasterObj.setNoOfApprovalNeeded(0);
//                    	 districtMasterObj.setReminderFrequencyInDays(0);
//                    	 districtMasterObj.setSendReminderToIcompCandiates(false);
//                    	 districtMasterObj.setReminderOfFirstFrequencyInDays(0);
//                    	 districtMasterObj.setNoOfReminder(0);
//                    	 districtMasterObj.setSendReminderForPortfolio(false);
//                    	 districtMasterObj.setShowReferenceToSA(0);
//                    	 districtMasterObj.setBuildApprovalGroup(false);
//                    	 districtMasterObj.setDisplaySeniorityNumber(false);
//                    	 districtMasterObj.setQqThumbShowOrNot(false);
//                    	 districtMasterObj.setDisplayCandidateTOMosaic(false);
                    	 
                    	 //end
                    	 
	            	 	 statelesSsession.update(districtMasterObj);
	            	 	 updateCount++;
                     }
                     
                
	  	    		} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	  	    }
	  	    
	  	    
		 }   

		 try{
			if(!final_error_store.equalsIgnoreCase("")){
	 	 		String content = final_error_store;		    				
				File file = new File(folderPath+"hrmsDistrictError.txt");
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				String[] parts = content.split("<>");	    				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write("LEA Number , LEA Name , Street Address , LEA City , LEA Zip Code , LEA State Code , NCES LEA Identifier , NCES State Identifier , LEA Telephone Number , LEA Fax Number , Web Site URL, Status");
				bw.write("\r\n\r\n");
				int k =0;
				for(String cont :parts) {
					bw.write(cont+"\r\n\r\n");
				    k++;
				}
				bw.close();
				
	 	 	}	
		  } catch (Exception e) {
				e.printStackTrace();
			}
			
		  txOpen.commit();
		  String message = "After File Upload,  "+insertCount+"  row inserted and  "+updateCount+"  row updated.";
		  System.out.print(message);
	      return message;
}

    
    
    
    public static Vector readDataTxt(String filePath) throws FileNotFoundException{
    	
		Vector vectorData = new Vector();
		String filepathhdr=filePath;

			 int LEANumber_count=12;
		   	 int LEAName_count=12;
		   	 int streetAddress_count=12; 
		   	 int city_count=12;
		   	 int ZIP_count=12;	  
		   	 int state_count=12;
		   	 int NCESLEAId_count=12;
		   	 int NCESStateId_count=12;
		   	 int phone_count=12; 
		   	 int fax_count=12;
		   	 int url_count=12;
		   	 int district_status_count=12;
		
		    
	        BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
	        
	        String strLineHdr="";	        
	        String hdrstr="";
	        
	        
	        int lineNumberHdr=0;
	        try{
	            
	            while((strLineHdr=brhdr.readLine())!=null){  
	            	
	            	if(strLineHdr.isEmpty())
	            		continue;
	            	Vector vectorCellEachRowData = new Vector();
	                int cIndex=0;
	                boolean cellFlag=false;
	                Map<String,String> mapCell = new TreeMap<String, String>();
	                int i=1;
	                
	                String[] parts = strLineHdr.split("\\|");
	                
	                for(int j=0; j<parts.length; j++) {
	                	hdrstr=parts[j];
	                	cIndex=j;
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("LEA Number")){
	                        	LEANumber_count=cIndex;
	                    		
	                    	}
	                        
	                        if(LEANumber_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("LEA Name")){
	                        	LEAName_count=cIndex;
	                    		
	                    	}
	                    	if(LEAName_count==cIndex){
	                    		cellFlag=true;
	                    		
	                    	}	
	                    	  
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("Street Address")){
	                    		streetAddress_count=cIndex;
	                    		
	                    	}
	                    	if(streetAddress_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("LEA City")){
	                    		city_count=cIndex;
	                    		
	                    	}
	                    	if(city_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("LEA Zip Code")){
	                    		ZIP_count=cIndex;
	                    		
	                    	}
	                    	if(ZIP_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("LEA State Code")){
	                    		state_count=cIndex;
	                    		
	                    	}
	                        
	                        if(state_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("NCES LEA Identifier")){
	                        	NCESLEAId_count=cIndex;
	                    		
	                    	}
	                    	if(NCESLEAId_count==cIndex){
	                    		cellFlag=true;
	                    		
	                    	}	
	                    	  
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("NCES State Identifier")){
	                    		NCESStateId_count=cIndex;
	                    		
	                    	}
	                    	if(NCESStateId_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("LEA Telephone Number")){
	                    		phone_count=cIndex;
	                    		
	                    	}
	                    	if(phone_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("LEA Fax Number")){
	                    		fax_count=cIndex;
	                    		
	                    	}
	                    	if(fax_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("Web Site URL")){
	                    		url_count=cIndex;
	                    		
	                    	}
	                    	if(url_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("Status")){
	                    		district_status_count=cIndex;
	                    		
	                    	}
	                    	if(district_status_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	
	                    	if(cellFlag){
	                    		
	                    			try{
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}catch(Exception e){
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}
	                    		
	                    	}
	                        
	                    }
	                    
	                    vectorCellEachRowData=cellValuePopulate(mapCell);
	                    vectorData.addElement(vectorCellEachRowData);
	                lineNumberHdr++;
	            }
	        }
	        catch(Exception e){
	            System.out.println(e.getMessage());
	        }
	        //System.out.println("::::::::: read data from text ::::::::::       "+vectorData);       
		return vectorData;
	}
    
    
		    public static Vector cellValuePopulate(Map<String,String> mapCell){
		   	 Vector vectorCellEachRowData = new Vector();
		   	 Map<String,String> mapCellTemp = new TreeMap<String, String>();
		   	 
		   	 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false,flag9=false,flag10=false,flag11=false,flag12=false;
		   	 for(Map.Entry<String, String> entry : mapCell.entrySet()){
		   		 String key=entry.getKey();
		   		String cellValue=null;
		   		if(entry.getValue()!=null)
		   			cellValue=entry.getValue().trim();
		   		    
		   		
		   		if(key.equals("0")){
		   			mapCellTemp.put(key, cellValue);
		   			flag0=true;
		   		}
		   		if(key.equals("1")){
		   			flag1=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("2")){
		   			flag2=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("3")){
		   			flag3=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("4")){
		   			flag4=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		
		   		if(key.equals("5")){
		   			mapCellTemp.put(key, cellValue);
		   			flag5=true;
		   		}
		   		if(key.equals("6")){
		   			flag6=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("7")){
		   			flag7=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("8")){
		   			flag8=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("9")){
		   			flag9=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("10")){
		   			flag10=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("11")){
		   			flag11=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("12")){
		   			flag12=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		
		   		
		   	 }
		   	 if(flag0==false){
		   		 mapCellTemp.put(0+"", "");
		   	 }
		   	 if(flag1==false){
		   		 mapCellTemp.put(1+"", "");
		   	 }
		   	 if(flag2==false){
		   		 mapCellTemp.put(2+"", "");
		   	 }
		   	if(flag3==false){
		   		 mapCellTemp.put(3+"", "");
		   	 }
		   	if(flag4==false){
		   		 mapCellTemp.put(4+"", "");
		   	 }
		    if(flag5==false){
		   		 mapCellTemp.put(5+"", "");
		   	 }
		   	 if(flag6==false){
		   		 mapCellTemp.put(6+"", "");
		   	 }
		   	 if(flag7==false){
		   		 mapCellTemp.put(7+"", "");
		   	 }
		   	if(flag8==false){
		   		 mapCellTemp.put(8+"", "");
		   	 }
		   	if(flag9==false){
		   		 mapCellTemp.put(9+"", "");
		   	 }
			if(flag10==false){
		   		 mapCellTemp.put(10+"", "");
		   	 }
		   	if(flag11==false){
		   		 mapCellTemp.put(11+"", "");
		   	 }
			if(flag12==false){
		   		 mapCellTemp.put(12+"", "");
		   	 }
		   	
		   	 
		   	 			 
		   	 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
		   		
		   		 vectorCellEachRowData.addElement(entry.getValue());
		   	 }
		   	 return vectorCellEachRowData;
		   }
    
    
    
	
}
