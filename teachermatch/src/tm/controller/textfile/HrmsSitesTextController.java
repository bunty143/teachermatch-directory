package tm.controller.textfile;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import tm.bean.UserUploadFolderAccess;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoMapping;
import tm.bean.master.RegionMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SchoolStandardMaster;
import tm.bean.master.SchoolTypeMaster;
import tm.bean.master.StateMaster;
import tm.bean.user.UserMaster;

import tm.dao.UserUploadFolderAccessDAO;

import tm.dao.master.CountryMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.GeoMappingDAO;
import tm.dao.master.RegionMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SchoolStandardMasterDAO;
import tm.dao.master.SchoolTypeMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.user.UserMasterDAO;


@Controller
public class HrmsSitesTextController {


	@Autowired
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
			this.districtMasterDAO = districtMasterDAO;
	    }
	    
	    @Autowired
	    private StateMasterDAO stateMasterDAO;
	    public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
			this.stateMasterDAO = stateMasterDAO;
		}
	    @Autowired
	    private GeoMappingDAO geoMappingDAO;
	    public void setGeoMappingDAO(GeoMappingDAO geoMappingDAO) {
	    	this.geoMappingDAO = geoMappingDAO;
        }
	    
	    @Autowired
	    private RegionMasterDAO regionMasterDAO;
	    public void setRegionMasterDAO(RegionMasterDAO regionMasterDAO) {
		this.regionMasterDAO = regionMasterDAO;
	    }
   
	    @Autowired
	    private SchoolStandardMasterDAO schoolStandardMasterDAO;
	    public void setSchoolStandardMasterDAO(
			SchoolStandardMasterDAO schoolStandardMasterDAO) {
		this.schoolStandardMasterDAO = schoolStandardMasterDAO;
	    }

	    @Autowired
	    private SchoolTypeMasterDAO schoolTypeMasterDAO;
	    public void setSchoolTypeMasterDAO(SchoolTypeMasterDAO schoolTypeMasterDAO) {
	    	this.schoolTypeMasterDAO = schoolTypeMasterDAO;
	    }

	    @Autowired
		private CountryMasterDAO countryMasterDAO;


	@RequestMapping(value="/hrmssitesFile.do", method=RequestMethod.GET)
	public @ResponseBody  String getHrmsSitesData(ModelMap map,HttpServletRequest request)
	{
		    System.out.println("::::: Use HRMS Sites File controller :::::");
		    List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
			try {
				Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
				Criterion active = Restrictions.eq("status", "A");
				uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
			} catch (Exception e) {
				e.printStackTrace();
			}	
		    String fileName = "HRMS-CCYYMMDDHHMMSS-00-2260-SCH--SiteData.txt";
		    String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
		    //String folderPath = "E:\\";
		  	String msge=saveLicense(fileName,folderPath);
					
		return msge;
	}
	
	
	private Vector vectorDataTxt = new Vector();
    public String saveLicense(String fileName,String folderPath){	
			
			 System.out.println("::::::::saveSites:::::::::");
			 
			 String filePath=folderPath+fileName;
		 	 System.out.println("folderPath  "+folderPath);
		 	 try {
		 		 if(fileName.contains(".txt") || fileName.contains(".TXT")){
		 			   vectorDataTxt = readDataTxt(filePath);
		 		 }else{
	    			 //break;
	    	 }
		} catch (Exception e) {
			e.printStackTrace();
		}
    	 
		SessionFactory sessionFactory=schoolMasterDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
 	    Transaction txOpen =statelesSsession.beginTransaction();
 	    int insertData=0;
 	    int updateData=0;
 	    
    	 int LEA_Number_count=12;	    	 
    	 int Site_Number_count=12;
    	 int Site_Name_count=12;
    	 int Street_Address_count=12; 
    	 int City_count=12;
    	 int State_count=12;
    	 int ZIP_count=12;
    	 int NCES_School_ID_count=12;
    	 int Phone_count=12;
    	 int Fax_count=12;
    	 int URL_count=12;
    	 int School_Status_count=12;
    	 String final_error_store="";
    	 int numOfError=0;
    	 boolean row_error=false;

    	 HeadQuarterMaster headQuarterMaster = new HeadQuarterMaster();
    	 headQuarterMaster.setHeadQuarterId(2);
    	 
    	 List<DistrictMaster> districtMasterList =  districtMasterDAO.getDistrictListByHeadQuater(headQuarterMaster);
    	 Map<String, DistrictMaster> districtMasterMap = new HashMap<String, DistrictMaster>();
    	 if(districtMasterList!=null && districtMasterList.size()>0)
    	 for (DistrictMaster districtMaster2 : districtMasterList) {
    		 districtMasterMap.put(districtMaster2.getLocationCode(), districtMaster2);
		}
    	 
    	 List<SchoolMaster> hrmsSites =  schoolMasterDAO.findSchoolsByDistricts(districtMasterList);
    	 Map<String, SchoolMaster> hrmsSitesList = new HashMap<String, SchoolMaster>();
    	 if(hrmsSites!=null && hrmsSites.size()>0)
    	 for (SchoolMaster hrmsSitesList2 : hrmsSites) {
    		 //hrmsSitesList.put(hrmsSitesList2.getLocationCode(), hrmsSitesList2);
    		 //String val = hrmsSitesList2.getLocationCode()+"_"+hrmsSitesList2.getDistrictId().getDistrictId();
    		 String val = hrmsSitesList2.getLocationCode()+"_"+hrmsSitesList2.getDistrictId().getLocationCode();
    		 hrmsSitesList.put(val, hrmsSitesList2);
		}
    	 
    	 Map<String, StateMaster> stateMasterMap = new HashMap<String, StateMaster>();
    	 List<StateMaster> stateMasterList = new ArrayList<StateMaster>();
    	 CountryMaster countryMaster = countryMasterDAO.getCountryByShortCode("US");
    	 
    	 stateMasterList =  stateMasterDAO.findActiveStateByCountryId(countryMaster);
		 if(stateMasterList!=null && stateMasterList.size()>0)
		 for (StateMaster stateMaster2 : stateMasterList) {
			 stateMasterMap.put(stateMaster2.getStateShortName(), stateMaster2);
		}
    	 
    	 txOpen =statelesSsession.beginTransaction();
    	
    	 
    	 
    	 StateMaster stateMaster=null;
    	 RegionMaster regionMaster=null;
    	 SchoolStandardMaster schoolStandardMaster=null;
    	 SchoolTypeMaster schoolTypeMaster=null;
    	 
    	 schoolTypeMaster=schoolTypeMasterDAO.findById(1, false, false);
    	  schoolStandardMaster=schoolStandardMasterDAO.findById(0, false, false);
    	  regionMaster=regionMasterDAO.findById(1, false, false);//let seeee
    	   GeoMapping geoMapping=geoMappingDAO.findById(0, false, false);
    	   
		 for(int i=0; i<vectorDataTxt.size(); i++) {
			
			 Vector vectorCellEachRowData = (Vector) vectorDataTxt.get(i);
			 
			 
	            String LEANumber="";
	            String SiteNumber="";
	            String SiteName="";
	            String StreetAddress =""; 
	            String City="";
	            String State="";
	            String ZIP="";
	            String NCESSchoolID="";
	            String Phone =""; 
	            String Fax="";
	            String URL="";
	            String SchoolStatus="";
	            String errorText="";
		  	    String rowErrorText="";
	  	     
	  	      
	  	      
	  	    for(int j=0; j<vectorCellEachRowData.size(); j++) {
  	        	try{
  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("LEA Number")){
  	        			LEA_Number_count=j;
	            	}
	            	if(LEA_Number_count==j)
	            		LEANumber=vectorCellEachRowData.get(j).toString().trim();
  	        		
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("School Number")){
	            		Site_Number_count=j;
	            	}
	            	if(Site_Number_count==j)
	            		SiteNumber=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("School Name")){
	            		Site_Name_count=j;
					}
	            	if(Site_Name_count==j)
	            		SiteName=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("School Address")){
	            		Street_Address_count=j;
					}
	            	if(Street_Address_count==j)
	            		StreetAddress=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("School City")){
	            		City_count=j;
					}
	            	if(City_count==j)
	            		City=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("School State Code")){
	            		State_count=j;
					}
	            	if(State_count==j)
	            		State=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("School Zip Code")){
	            		ZIP_count=j;
					}
	            	if(ZIP_count==j)
	            		ZIP=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("NCES School Identifier")){
	            		NCES_School_ID_count=j;
					}
	            	if(NCES_School_ID_count==j)
	            		NCESSchoolID=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("School Telephone Number")){
	            		Phone_count=j;
					}
	            	if(Phone_count==j)
	            		Phone=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Fax")){
	            		Fax_count=j;
					}
	            	if(Fax_count==j)
	            		Fax=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("School Web Site URL")){
	            		URL_count=j;
					}
	            	if(URL_count==j)
	            		URL=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Status")){
	            		School_Status_count=j;
					}
	            	if(School_Status_count==j)
	            		SchoolStatus=vectorCellEachRowData.get(j).toString().trim();
	            	
            	}catch(Exception e){
            		e.printStackTrace();
            	}
  	        }
	  	    
	  	  if(i==0){
	  		  
	  		  if(!SiteNumber.equalsIgnoreCase("School Number")){
	  			rowErrorText="School Number column is not found";
	  			row_error=true;
	  		  }
	  		  if(!LEANumber.equalsIgnoreCase("LEA Number")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="LEA Number column is not found";
	  			row_error=true;
	  		  }
	  		  if(!SiteName.equalsIgnoreCase("School Name")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="School Name column is not found";
	  			row_error=true;  
	  		  }
	  		if(!StreetAddress.equalsIgnoreCase("School Address")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="School Address column is not found";
	  			row_error=true;  
	  		  }
	  		 
	  	  
		  	 if(!City.equalsIgnoreCase("School City")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="School City column is not found";
	  			row_error=true;  
	  		  }
		  	 
		  	 if(!State.equalsIgnoreCase("School State Code")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="School State Code column is not found";
		  			row_error=true;  
		  		  }
		  	 
		  	 if(!ZIP.equalsIgnoreCase("School Zip Code")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="School Zip Code column is not found";
		  			row_error=true;  
		  		  }
		  	 
		  	 if(!NCESSchoolID.equalsIgnoreCase("NCES School Identifier")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="NCES School Identifier column is not found";
		  			row_error=true;  
		  		  }
		  	 
		  	 if(!Phone.equalsIgnoreCase("School Telephone Number")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="School Telephone Number column is not found";
		  			row_error=true;  
		  		  }
		  	 
		  	 if(!Fax.equalsIgnoreCase("Fax")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="Fax column is not found";
		  			row_error=true;  
		  		  }
		  	 if(!URL.equalsIgnoreCase("School Web Site URL")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="School Web Site URL column is not found";
		  			row_error=true;  
		  		  }
		  	 
		  	if(!SchoolStatus.equalsIgnoreCase("Status")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="Status column is not found";
	  			row_error=true;  
	  		  }
		  	 
	  		 }
	  	    
	  	  
		  	if(row_error){
		  		numOfError++;
		  		File file = new File(folderPath+"hrmsSitesError.txt");
				// if file doesnt exists, then create it
		  		try {
			  		if (!file.exists()) {
			  			file.createNewFile();
					}	    				
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(rowErrorText);
		
					bw.close();
		  		} catch (IOException e) {
					
					e.printStackTrace();
				}
				
		  		 break;
		  	 }
	  	    
	  	    Boolean LEANumberFlag = true;
	  	    Boolean SiteNameFlag = true;
	  	    Boolean StreetAddressFlag = true;
	  	    Boolean CityFlag = true;
	  	    Boolean StateFlag = true;
	  	    Boolean ZIPFlag = true;
	  	    Boolean NCESSchoolIDFlag = true;
	  	    Boolean PhoneFlag = true;
	  	    Boolean FaxFlag = true;
	  	    Boolean URLFlag = true;
	  	    Boolean StatusFlag = true;
	  	   
	  	    
	  	    if(i != 0 && row_error==false){
	  	    	
	  	    	boolean errorFlag=false;
	  	    	
	  	    	if(!SiteNumber.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(SiteNumber.replaceAll("\"", "").length()>9)
	  	    			errorText+="School Number length does not greater than 9";
	  	    		    errorFlag=true;
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="School Number is empty";
  	    			    errorFlag=true;
	        	}
	  	    	
               if(!LEANumber.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(LEANumber.replaceAll("\"", "").length()>9)
	  	    			errorText+="School Site  Number length does not greater than 9";
	  	    		LEANumberFlag=true;
	  	    		if(LEANumberFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(LEANumberFlag){
							errorText+=",";	
						}
	        			errorText+="School Site Number is empty";
	        			LEANumberFlag=true;
	        	}
               
               if(!SiteName.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(SiteName.replaceAll("\"", "").length()>40)
	  	    			errorText+="School name length does not greater than 40 ";
	  	    		SiteNameFlag=true;
	  	    		if(SiteNameFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(SiteNameFlag){
							errorText+=",";	
						}
	        			errorText+="School name  is empty";
	        			SiteNameFlag=true;
	        	}
               
               if(!StreetAddress.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(StreetAddress.replaceAll("\"", "").length()>70)
	  	    			errorText+="School Address length does not greater than 70";
	  	    		StreetAddressFlag=true;
	  	    		if(StreetAddressFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(StreetAddressFlag){
							errorText+=",";	
						}
	        			errorText+="School Address is empty.";
	        			StreetAddressFlag=true;
	        	}
               
               if(!City.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(City.replaceAll("\"", "").length()>35)
	  	    			errorText+="School City length does not greater than 35";
	  	    		CityFlag=true;
	  	    		if(CityFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(CityFlag){
							errorText+=",";	
						}
	        			errorText+="School City is empty";
	        			CityFlag=true;
	        	}
               
               if(!State.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(State.replaceAll("\"", "").length()>2)
	  	    			errorText+="School State length does not greater than 2";
	  	    		StateFlag=true;
	  	    		if(StateFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(StateFlag){
							errorText+=",";	
						}
	        			errorText+="School State is empty";
	        			StateFlag=true;
	        	}
               
               if(!ZIP.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(ZIP.replaceAll("\"", "").length()>5)
	  	    			errorText+="School ZIP length does not greater than 5";
	  	    		ZIPFlag=true;
	  	    		if(ZIPFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(ZIPFlag){
							errorText+=",";	
						}
	        			errorText+="School ZIP is empty";
	        			ZIPFlag=true;
	        	}
               
               if(!NCESSchoolID.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(NCESSchoolID.replaceAll("\"", "").length()>5)
	  	    			errorText+="NCES School Identifier does not greater than 5";
	  	    		NCESSchoolIDFlag=true;
	  	    		if(NCESSchoolIDFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(NCESSchoolIDFlag){
							errorText+=",";	
						}
	        			errorText+="NCES School Identifier is empty";
	        			NCESSchoolIDFlag=true;
	        	}
               
               if(!Phone.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(Phone.replaceAll("\"", "").length()>12)
	  	    			errorText+="School Telephone Number length does not greater than 12";
	  	    		PhoneFlag=true;
	  	    		if(PhoneFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(PhoneFlag){
							errorText+=",";	
						}
	        			errorText+="School Telephone Number is empty";
	        			PhoneFlag=true;
	        	}
               
               if(!Fax.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(Fax.replaceAll("\"", "").length()>12)
	  	    			errorText+="Fax length does not greater than 12";
	  	    		FaxFlag=true;
	  	    		if(FaxFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(FaxFlag){
							errorText+=",";	
						}
	        			errorText+="Fax is empty";
	        			FaxFlag=true;
	        	}
               
               if(!URL.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(URL.replaceAll("\"", "").length()>150)
	  	    			errorText+="URL length does not greater than 150";
	  	    		URLFlag=true;
	  	    		if(URLFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(URLFlag){
							errorText+=",";	
						}
	        			errorText+="URL is empty";
	        			URLFlag=true;
	        	}
               
               if(SchoolStatus.replaceAll("\"", "").equalsIgnoreCase("")){
	        			if(StatusFlag){
							errorText+=",";	
						}
	        			errorText+="Status is empty";
	        			StatusFlag=true;
	        	}
               
                
	  	    	
                if(!errorText.equals("")){
	        			int row = i+1;
  	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+LEANumber+","+SiteNumber+","+SiteName+","+StreetAddress+","+City+","+State+","+ZIP+","+NCESSchoolID+","+Phone+","+Fax+","+URL+","+SchoolStatus+"<>";
  	    			numOfError++;
  	    		}
	  	    	
                try {
                	
                	
                	SchoolMaster schoolMaster = null;
                	DistrictMaster districtMaster=null;
                	if(SiteNumber.replaceAll("\"", "").isEmpty()){
                		continue;
                	}
                	
                	 //districtMaster=districtMasterDAO.findDistrictByLocationCode(LEANumber.replaceAll("\"", ""));
	                	if(districtMasterMap.containsKey(LEANumber.replaceAll("\"", ""))){
	                		districtMaster = districtMasterMap.get(LEANumber.replaceAll("\"", ""));
	                	}
	                if(districtMaster == null){
	                	System.out.println("Get DistrictMaster  null By LeaNumber##### "+LEANumber.replaceAll("\"", ""));
	                	continue;
	                }
                	
                	 String val = SiteNumber.replaceAll("\"", "")+"_"+districtMaster.getLocationCode();
                	 System.out.println("Get SchoolMaster By This Key==="+val);
                	 
                	if(hrmsSitesList.containsKey(val)){
                		schoolMaster = hrmsSitesList.get(val);
                		 
                	}
                	
                     if(schoolMaster == null){
                    	 System.out.println("###### insert##########");
                    	 schoolMaster = new SchoolMaster();
                    	 schoolMaster.setLocationCode(SiteNumber.replaceAll("\"", ""));
                    	
                    	                     	              	 
                    	 if(LEANumberFlag){
                    		 //districtMaster=districtMasterDAO.findDistrictByLocationCode(LEANumber.replaceAll("\"", ""));
                       	  schoolMaster.setDistrictId(districtMaster);
                       	  schoolMaster.setDistrictName(districtMaster.getDistrictName());
                       	  
                    	 }
                    	 if(SiteNameFlag){
                    		 schoolMaster.setSchoolName(SiteName.replaceAll("\"", ""));
                    	 }
                    	 if(StreetAddressFlag){
                    		  schoolMaster.setAddress(StreetAddress.replaceAll("\"", ""));
                    	 }
                    	 if(CityFlag){
                    		schoolMaster.setCityName(City.replaceAll("\"", ""));
                    	 }
                    	 if(StateFlag){
                    		// String statcode=State.replaceAll("\"", "");
                    		// stateMaster=stateMasterDAO.findByStateCode(statcode);
                    		 
                    		 if(stateMasterMap.containsKey(State.replaceAll("\"", ""))){
                    			 stateMaster = stateMasterMap.get(State.replaceAll("\"", ""));
                         	 }
                    		 schoolMaster.setStateMaster(stateMaster);
                    	 }
                    	 if(ZIPFlag){
                    		schoolMaster.setZip(ZIP.replaceAll("\"", ""));
                    	 }
                    	 if(NCESSchoolIDFlag){
                    		 schoolMaster.setNcesSchoolId(NCESSchoolID.replaceAll("\"", ""));
                    	 }
                    	 if(PhoneFlag){
                    		 schoolMaster.setPhoneNumber(Phone.replaceAll("\"", ""));
                    	 }
                    	 if(FaxFlag){
                    		schoolMaster.setFaxNumber(Fax.replaceAll("\"", ""));
                    	 }
                    	 if(URLFlag){
                    		schoolMaster.setWebsite(URL.replaceAll("\"", ""));
                    	 }
                    		if(SchoolStatus.replaceAll("\"", "").isEmpty()){                		
                    			SchoolStatus="A";                		
                        	}
                    		
                    	 schoolMaster.setStatus(SchoolStatus);
                    	 schoolMaster.setSchoolTypeId(schoolTypeMaster);
                    	 schoolMaster.setSchoolStandardId(schoolStandardMaster);
                    	 schoolMaster.setRegionId(regionMaster);
                    	 
                    	 schoolMaster.setPostingOnTMWall(2);
                    	 schoolMaster.setPostingOnSchoolWall(2);
                    	 schoolMaster.setPostingOnDistrictWall(2);
                    	 schoolMaster.setIsWeeklyCgReport(0);
                    	 schoolMaster.setIsPortfolioNeeded(true);
                    	 //geoMappingDAO
                    	 schoolMaster.setGeoMapping(geoMapping);
                    	 
                    	 schoolMaster.setDmName("");
                    	 schoolMaster.setAcName("");
                    	 schoolMaster.setAmName("");
                    	 schoolMaster.setCanTMApproach(1);
	            	   	statelesSsession.insert(schoolMaster);
	            	   	insertData++;
	            	   	
	            	 
                    	 
                     }else{
                    	 System.out.println("####### update######");
                    	 if(LEANumberFlag){
                    		// districtMaster=districtMasterDAO.findDistrictByLocationCode(LEANumber.replaceAll("\"", ""));
                          	  schoolMaster.setDistrictId(districtMaster);
                          	  schoolMaster.setDistrictName(districtMaster.getDistrictName());
                          	  
                    	 }
                    	 if(SiteNameFlag){
                    		 schoolMaster.setSchoolName(SiteName.replaceAll("\"", ""));
                    	 }
                    	 if(StreetAddressFlag){
                    		  schoolMaster.setAddress(StreetAddress.replaceAll("\"", ""));
                    	 }
                    	 if(CityFlag){
                    		schoolMaster.setCityName(City.replaceAll("\"", ""));
                    	 }
                    	 if(StateFlag){
                     		// String statcode=State.replaceAll("\"", "");
                     		// stateMaster=stateMasterDAO.findByStateCode(statcode);
                     		 
                     		 if(stateMasterMap.containsKey(State.replaceAll("\"", ""))){
                     			 stateMaster = stateMasterMap.get(State.replaceAll("\"", ""));
                          	 }
                     		 schoolMaster.setStateMaster(stateMaster);
                     	 }
                    	 if(ZIPFlag){
                    		schoolMaster.setZip(ZIP.replaceAll("\"", ""));
                    	 }
                    	 if(NCESSchoolIDFlag){
                    		 schoolMaster.setNcesSchoolId(NCESSchoolID.replaceAll("\"", ""));
                    	 }
                    	 if(PhoneFlag){
                    		 schoolMaster.setPhoneNumber(Phone.replaceAll("\"", ""));
                    	 }
                    	 if(FaxFlag){
                    		schoolMaster.setFaxNumber(Fax.replaceAll("\"", ""));
                    	 }
                    	 if(URLFlag){
                    		 schoolMaster.setWebsite(URL.replaceAll("\"", ""));
                    	 }
                    	 
                    	 if(SchoolStatus.replaceAll("\"", "").isEmpty())
                    	 {    if(schoolMaster.getStatus()!=null)            		
                    		 schoolMaster.setStatus(schoolMaster.getStatus());              		
                         }
                    	 else
                    	 {
                    		 schoolMaster.setStatus(SchoolStatus);
                    	 }
                    	
                    	 
	            	 	statelesSsession.update(schoolMaster);
	            	 	updateData++;
	            	 		            	 	
                     }
                     
                
	  	    		} catch (Exception e) {
						e.printStackTrace();
					}
	  	    }
	  	    
	  	    
		 }   

		 try{
			if(!final_error_store.equalsIgnoreCase("")){
	 	 		String content = final_error_store;		    				
				File file = new File(folderPath+"hrmsSitesError.txt");
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				String[] parts = content.split("<>");	    				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write("LEA Number,School Number,School Name,School Address,School City,School State Code,School Zip Code,NCES School Identifier,School Telephone Number,Fax,School Web Site URL,Status");
				bw.write("\r\n\r\n");
				int k =0;
				for(String cont :parts) {
					bw.write(cont+"\r\n\r\n");
				    k++;
				}
				bw.close();
				
	 	 	}	
			txOpen.commit();
		  } catch (Exception e) {
				e.printStackTrace();
			}
			
		
		String msg="After uploades files "+insertData+" rows inserted and "+updateData+" rows updated.";
		return msg;
}

    
    
    
    public static Vector readDataTxt(String filePath) throws FileNotFoundException{
    	
		Vector vectorData = new Vector();
		String filepathhdr=filePath;

		int LEA_Number_count=12;	    	 
   	 int Site_Number_count=12;
   	 int Site_Name_count=12;
   	 int Street_Address_count=12; 
   	 int City_count=12;
   	 int State_count=12;
   	 int ZIP_count=12;
   	 int NCES_School_ID_count=12;
   	 int Phone_count=12;
   	 int Fax_count=12;
   	 int URL_count=12;
   	int school_status_count=12;
		
		    
	        BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
	        
	        String strLineHdr="";	        
	        String hdrstr="";
	        
	        StringTokenizer sthdr=null;
	        StringTokenizer stdtl=null;
	        int lineNumberHdr=0;
	        try{
	            
	            while((strLineHdr=brhdr.readLine())!=null){  
	            	
	            	if(strLineHdr.isEmpty())
	            		continue;
	            	Vector vectorCellEachRowData = new Vector();
	                int cIndex=0;
	                boolean cellFlag=false;
	                Map<String,String> mapCell = new TreeMap<String, String>();
	                int i=1;
	                
	                String[] parts = strLineHdr.split("\\|");
	                
	                for(int j=0; j<parts.length; j++) {
	                	hdrstr=parts[j];
	                	cIndex=j;
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("LEA Number")){
	                        	LEA_Number_count=cIndex;
	                    		
	                    	}
	                        
	                        if(LEA_Number_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("School Number")){
	                        	Site_Number_count=cIndex;
	                    		
	                    	}
	                    	if(Site_Number_count==cIndex){
	                    		cellFlag=true;
	                    		
	                    	}	
	                    	  
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("School Name")){
	                    		Site_Name_count=cIndex;
	                    		
	                    	}
	                    	if(Site_Name_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("School Address")){
	                    		Street_Address_count=cIndex;
	                    		
	                    	}
	                    	if(Street_Address_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("School City")){
	                    		City_count=cIndex;
	                    		
	                    	}
	                    	if(City_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("School State Code")){
	                    		State_count=cIndex;
	                    		
	                    	}
	                    	if(State_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("School Zip Code")){
	                    		ZIP_count=cIndex;
	                    		
	                    	}
	                    	if(ZIP_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("NCES School Identifier")){
	                    		NCES_School_ID_count=cIndex;
	                    		
	                    	}
	                    	if(NCES_School_ID_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("School Telephone Number")){
	                    		Phone_count=cIndex;
	                    		
	                    	}
	                    	if(Phone_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("Fax")){
	                    		Fax_count=cIndex;
	                    		
	                    	}
	                    	if(Fax_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("School Web Site URL")){
	                    		URL_count=cIndex;
	                    		
	                    	}
	                    	if(URL_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("Status")){
	                    		school_status_count=cIndex;
	                    		
	                    	}
	                    	if(school_status_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(cellFlag){
	                    		
	                    			try{
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}catch(Exception e){
	                    				e.printStackTrace();
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}
	                    		
	                    	}
	                        
	                    }
	                    
	                    vectorCellEachRowData=cellValuePopulate(mapCell);
	                    vectorData.addElement(vectorCellEachRowData);
	                lineNumberHdr++;
	            }
	        }
	        catch(Exception e){
	        	e.printStackTrace();
	            System.out.println(e.getMessage());
	        }
	        System.out.println("::::::::: read data from text ::::::::::       "+vectorData);       
		return vectorData;
	}
    
    
		    public static Vector cellValuePopulate(Map<String,String> mapCell){
		   	 Vector vectorCellEachRowData = new Vector();
		   	 Map<String,String> mapCellTemp = new TreeMap<String, String>();
		   	 
		   	 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false,flag9=false,flag10=false,flag11=false;
		   	 for(Map.Entry<String, String> entry : mapCell.entrySet()){
		   		 String key=entry.getKey();
		   		String cellValue=null;
		   		if(entry.getValue()!=null)
		   			cellValue=entry.getValue().trim();
		   		    
		   		
		   		if(key.equals("0")){
		   			mapCellTemp.put(key, cellValue);
		   			flag0=true;
		   		}
		   		if(key.equals("1")){
		   			flag1=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("2")){
		   			flag2=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("3")){
		   			flag3=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("4")){
		   			flag4=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("5")){
		   			flag5=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("6")){
		   			flag6=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		
		   		if(key.equals("7")){
		   			flag7=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("8")){
		   			flag8=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("9")){
		   			flag9=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("10")){
		   			flag10=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("11")){
		   			flag11=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		
		   		
		   	 }
		   	 if(flag0==false){
		   		 mapCellTemp.put(0+"", "");
		   	 }
		   	 if(flag1==false){
		   		 mapCellTemp.put(1+"", "");
		   	 }
		   	 if(flag2==false){
		   		 mapCellTemp.put(2+"", "");
		   	 }
		   	if(flag3==false){
		   		 mapCellTemp.put(3+"", "");
		   	 }
		   	if(flag4==false){
		   		 mapCellTemp.put(4+"", "");
		   	 }
		   	if(flag5==false){
		   		 mapCellTemp.put(5+"", "");
		   	 }
		   	if(flag6==false){
		   		 mapCellTemp.put(6+"", "");
		   	 }
		   	if(flag7==false){
		   		 mapCellTemp.put(7+"", "");
		   	 }
		   	if(flag8==false){
		   		 mapCellTemp.put(8+"", "");
		   	 }
		   	if(flag9==false){
		   		 mapCellTemp.put(9+"", "");
		   	 }
		   	if(flag10==false){
		   		 mapCellTemp.put(10+"", "");
		   	 }
			if(flag11==false){
		   		 mapCellTemp.put(11+"", "");
		   	 }
		   	 			 
		   	 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
		   		
		   		 vectorCellEachRowData.addElement(entry.getValue());
		   	 }
		   	 return vectorCellEachRowData;
		   }
    
    
    
	
}