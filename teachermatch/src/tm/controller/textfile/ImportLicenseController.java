//package tm.controller.textfile;
//
//import java.io.BufferedReader;
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.StringTokenizer;
//import java.util.TreeMap;
//import java.util.Vector;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.hibernate.SessionFactory;
//import org.hibernate.StatelessSession;
//import org.hibernate.Transaction;
//import org.hibernate.criterion.Criterion;
//import org.hibernate.criterion.Restrictions;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.ModelMap;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import tm.bean.UserUploadFolderAccess;
//import tm.bean.textfile.License;
//import tm.bean.user.UserMaster;
//
//import tm.dao.UserUploadFolderAccessDAO;
//import tm.dao.textfile.LicenseDAO;
//import tm.dao.user.UserMasterDAO;
//
///* 
// * @Author: Sandeep Yadav
// */
//
//
//@Controller
//public class ImportLicenseController {
//
//	
//	@Autowired
//	private LicenseDAO licenseDAO;
//	public void setLicenseDAO(LicenseDAO licenseDAO) {
//		this.licenseDAO = licenseDAO;
//	}
//
//	@Autowired
//	private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
//	
//	public void setUserUploadFolderAccessDAO(
//			UserUploadFolderAccessDAO userUploadFolderAccessDAO) {
//		this.userUploadFolderAccessDAO = userUploadFolderAccessDAO;
//	}
//
//	@Autowired
//	private UserMasterDAO userMasterDAO;
//	
//
//	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
//		this.userMasterDAO = userMasterDAO;
//	}
//
//
//
//	@RequestMapping(value="/license.do", method=RequestMethod.GET)
//	public @ResponseBody String getAllLicenseData(ModelMap map,HttpServletRequest request)
//	{
//		    System.out.println("::::: Use ImportLicense controller :::::");
//		    List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
//			try {
//				Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
//				Criterion active = Restrictions.eq("status", "A");
//				uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}	
//		    String fileName = "HRMS--2260-LIC--Licenses.txt";
//		    String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
//		  	
//			
//		
//		return saveLicense(fileName,folderPath);
//	}
//	
//	
//	private Vector vectorDataTxt = new Vector();
//    public String saveLicense(String fileName,String folderPath){	
//			
//			 System.out.println("::::::::saveLicense:::::::::");
//			 
//			 String filePath=folderPath+fileName;
//		 	 System.out.println("filePath  "+filePath);
//		 	 try {
//		 		 if(fileName.contains(".txt") || fileName.contains(".TXT")){
//		 			   vectorDataTxt = readDataTxt(filePath);
//		 		 }else{
//	    			 //break;
//	    	 }
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//    	 
//		SessionFactory sessionFactory=licenseDAO.getSessionFactory();
//		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
// 	    Transaction txOpen =statelesSsession.beginTransaction();
// 	    
//    	 int SSN_count=11;	    	 
//    	 int LicenseExpireDate_count=11;
//    	 int LicenseEffectiveDate_count=11; 
//    	
//    	 String final_error_store="";
//    	 int numOfError=0;
//    	 boolean row_error=false;
//    	 List<License> license =  licenseDAO.findAll();
//    	 Map<String, License> licenseList = new HashMap<String, License>();
//    	 if(license!=null && license.size()>0)
//    	 for (License license2 : license) {
//    		 licenseList.put(license2.getSSN(), license2);
//		}
//    	 txOpen =statelesSsession.beginTransaction();
//    	 UserMaster userMaster = userMasterDAO.findById(1, false, false);
//    	 int insertCount = 0;
//	     int updateCount = 0;
//		 for(int i=0; i<vectorDataTxt.size(); i++) {
//			 Vector vectorCellEachRowData = (Vector) vectorDataTxt.get(i);
//			 
//			 
//	            String SSN="";
//	            String LicenseExpireDate="";
//	            String LicenseEffectiveDate =""; 
//	            String errorText="";
//		  	    String rowErrorText="";
//	  	     
//	  	      
//	  	      
//	  	    for(int j=0; j<vectorCellEachRowData.size(); j++) {
//  	        	try{
//  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("ssn_txtchar")){
//  	        			SSN_count=j;
//	            	}
//	            	if(SSN_count==j)
//	            		SSN=vectorCellEachRowData.get(j).toString().trim();
//  	        		
//	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("lic_expire_dte")){
//	            		LicenseExpireDate_count=j;
//	            	}
//	            	if(LicenseExpireDate_count==j)
//	            		LicenseExpireDate=vectorCellEachRowData.get(j).toString().trim();
//	            	
//	            	
//	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("lic_effect_dte")){
//	            		LicenseEffectiveDate_count=j;
//					}
//	            	if(LicenseEffectiveDate_count==j)
//	            		LicenseEffectiveDate=vectorCellEachRowData.get(j).toString().trim();
//	            	
//	            	
//            	}catch(Exception e){
//            		e.printStackTrace();
//            	}
//  	        }
//	  	    
//	  	  if(i==0){
//	  		  
//	  		  if(!SSN.equalsIgnoreCase("ssn_txtchar")){
//	  			rowErrorText="ssn_txtchar column is not found";
//	  			row_error=true;
//	  		  }
//	  		  if(!LicenseExpireDate.equalsIgnoreCase("lic_expire_dte")){
//	  			if(row_error){
//	  				rowErrorText+=",";
//	  			  }
//	  			rowErrorText+="lic_expire_dte column is not found";
//	  			row_error=true;
//	  		  }
//	  		  if(!LicenseEffectiveDate.equalsIgnoreCase("lic_effect_dte")){
//	  			if(row_error){
//	  				rowErrorText+=",";
//	  			  }
//	  			rowErrorText+="lic_effect_dte column is not found";
//	  			row_error=true;  
//	  		  }
//	  		
//	  		
//	  	  }
//	  	    
//	  	  
//		  	if(row_error){
//		  		numOfError++;
//		  		File file = new File(folderPath+"licenseError.txt");
//				// if file doesnt exists, then create it
//		  		try {
//			  		if (!file.exists()) {
//			  			file.createNewFile();
//					}	    				
//					FileWriter fw = new FileWriter(file.getAbsoluteFile());
//					BufferedWriter bw = new BufferedWriter(fw);
//					bw.write(rowErrorText);
//		
//					bw.close();
//		  		} catch (IOException e) {
//					
//					e.printStackTrace();
//				}
//				
//		  		 break;
//		  	 }
//	  	    
//	  	    Boolean LicenseExpireDateFlag = true;
//	  	    Boolean LicenseEffectiveDateFlag = true;
//	  	    Boolean LicenseRevokeRescindDateFlag = true;
//	  	    Boolean LicenseLastIssueDateFlag = true;
//	  	    
//	  	    if(i != 0 && row_error==false){
//	  	    	
//	  	    	boolean errorFlag=false;
//	  	    	
//	  	    	if(!SSN.replaceAll("\"", "").equalsIgnoreCase("")){
//	  	    		
//	  	    		if(SSN.replaceAll("\"", "").length()>9){
//	  	    			errorText+="SSN length does not greater than 9";
//	  	    		    errorFlag=true;
//	  	    		}   
//	  	    		if(errorFlag){
//							errorText+=",";	
//					}
//	  	    		
//	  	    	}else{
//	        			if(errorFlag){
//							errorText+=",";	
//						}
//	        			errorText+="SSN is empty";
//  	    			    errorFlag=true;
//	        	}
//	  	    	
//	  	    	
//	  	    	if(!LicenseExpireDate.replaceAll("\"", "").equalsIgnoreCase("")){
//	  	    		
//	  	    		if (!LicenseExpireDate.replaceAll("\"", "").matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")){
//	  	    			LicenseExpireDateFlag=false;
//	  	    			errorText+="License Expire Date should be 'MM/dd/yyyy' format";
//	  	    			errorFlag=true;
//	  	    		}	
//	  	    		if(errorFlag){
//							errorText+=",";	
//					}
//	  	    		
//	  	    	}else{
//	  	    		    if(errorFlag){
//							errorText+=",";	
//						}
//	        			errorText+="License Expire Date is empty";
//  	    			    errorFlag=true;
//  	    			    LicenseExpireDateFlag=false;
//	        	}
//	  	    	
//	  	    	
//	  	    	
//                if(!LicenseEffectiveDate.replaceAll("\"", "").equalsIgnoreCase("")){
//	  	    			
//                	if (!LicenseEffectiveDate.replaceAll("\"", "").matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")){
//                		LicenseEffectiveDateFlag=false;
//	  	    			errorText+="License Effective Date should be 'MM/dd/yyyy' format";
//	  	    			errorFlag=true;
//	  	    		}	
//	  	    		if(errorFlag){
//							errorText+=",";	
//					}
//	  	    		
//	  	    	}else{
//                	    if(errorFlag){
//							errorText+=",";	
//						}
//	        			errorText+="License Effective Date is empty";
//  	    			    errorFlag=true;
//  	    			    LicenseEffectiveDateFlag=false;
//	        	}
//                
//                
//                
//                
//	  	    	
//                if(!errorText.equals("")){
//	        			int row = i+1;
//  	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+SSN+","+LicenseExpireDate+","+LicenseEffectiveDate+"<>";
//  	    			numOfError++;
//  	    		}
//	  	    	
//                try {
//                	
//                	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
//                	License licenseObj = null;
//                	
//                	if(SSN.replaceAll("\"", "").isEmpty()){
//                		continue;
//                	}
//                	
//                	if(licenseList.containsKey(SSN.replaceAll("\"", ""))){
//                		licenseObj = licenseList.get(SSN.replaceAll("\"", ""));
//                	}
//                	
//                   
//                     
//                     if(licenseObj == null){
//                    	 licenseObj = new License();
//                    	 licenseObj.setSSN(SSN.replaceAll("\"", ""));
//                    	 if(LicenseEffectiveDateFlag){
//                    		 licenseObj.setLicenseEffectiveDate(formatter.parse(LicenseEffectiveDate.replaceAll("\"", "")));
//                    	 }
//                    	 
//                    	 if(LicenseExpireDateFlag){
//                    		 licenseObj.setLicenseExpireDate(formatter.parse(LicenseExpireDate.replaceAll("\"", "")));
//                    	 }
//                    	
//      					 licenseObj.setStatus("A");
//                    	 licenseObj.setCreatedDate(new Date());
//                    	 licenseObj.setUserMaster(userMaster); 
//     				   
//	            	   	statelesSsession.insert(licenseObj);
//	            	   	insertCount++;
//                    	 
//                     }else{
//                    	 
//                    	 if(LicenseEffectiveDateFlag){
//                    		 licenseObj.setLicenseEffectiveDate(formatter.parse(LicenseEffectiveDate.replaceAll("\"", "")));
//                    	 }
//                    	 
//                    	 if(LicenseExpireDateFlag){
//                    		 licenseObj.setLicenseExpireDate(formatter.parse(LicenseExpireDate.replaceAll("\"", "")));
//                    	 }
//                    	 
//                    	
//                    	 licenseObj.setStatus("A");
//                    	 licenseObj.setCreatedDate(new Date());
//                    	 licenseObj.setUserMaster(userMaster);  
//                    	//licenseDAO.makePersistent(license);
//       					//txOpen =statelesSsession.beginTransaction();
//	            	 	statelesSsession.update(licenseObj);
//	            	 	updateCount++;
//	            	 	//txOpen.commit();
//                     }
//                     
//                
//	  	    		} catch (Exception e) {
//						
//						e.printStackTrace();
//					}
//	  	    }
//	  	    
//	  	    
//		 }   
//
//		 try{
//			if(!final_error_store.equalsIgnoreCase("")){
//	 	 		String content = final_error_store;		    				
//				File file = new File(folderPath+"licenseError.txt");
//				// if file doesnt exists, then create it
//				if (!file.exists()) {
//					file.createNewFile();
//				}
//				String[] parts = content.split("<>");	    				
//				FileWriter fw = new FileWriter(file.getAbsoluteFile());
//				BufferedWriter bw = new BufferedWriter(fw);
//				bw.write("ssn_txtchar,lic_expire_dte,lic_effect_dte,lic_revoke_rescind_dte");
//				bw.write("\r\n\r\n");
//				int k =0;
//				for(String cont :parts) {
//					bw.write(cont+"\r\n\r\n");
//				    k++;
//				}
//				bw.close();
//				
//	 	 	}	
//		  } catch (Exception e) {
//				e.printStackTrace();
//			}
//			
//		  txOpen.commit();
//		  String message = "After File Upload,  "+insertCount+"  row inserted and  "+updateCount+"  row updated.";
//		  System.out.print(message);
//	      return message;
//}
//
//    
//    
//    
//    public static Vector readDataTxt(String filePath) throws FileNotFoundException{
//    	
//		Vector vectorData = new Vector();
//		String filepathhdr=filePath;
//
//			 int SSN_count=11;	    	 
//		   	 int LicenseExpireDate_count=11;
//		   	 int LicenseEffectiveDate_count=11; 
//		   	
//		
//		    
//	        BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
//	        
//	        String strLineHdr="";	        
//	        String hdrstr="";
//	        
//	        StringTokenizer sthdr=null;
//	        StringTokenizer stdtl=null;
//	        int lineNumberHdr=0;
//	        try{
//	            
//	            while((strLineHdr=brhdr.readLine())!=null){  
//	            	
//	            	if(strLineHdr.isEmpty())
//	            		continue;
//	            	Vector vectorCellEachRowData = new Vector();
//	                int cIndex=0;
//	                boolean cellFlag=false;
//	                Map<String,String> mapCell = new TreeMap<String, String>();
//	                int i=1;
//	                
//	                String[] parts = strLineHdr.split("\\|");
//	                
//	                for(int j=0; j<parts.length; j++) {
//	                	hdrstr=parts[j];
//	                	cIndex=j;
//	                        
//	                        if(hdrstr.toString().trim().equalsIgnoreCase("ssn_txtchar")){
//	                        	SSN_count=cIndex;
//	                    		
//	                    	}
//	                        
//	                        if(SSN_count==cIndex){
//	                    		cellFlag=true;
//	                    	}
//	                        
//	                        if(hdrstr.toString().trim().equalsIgnoreCase("lic_expire_dte")){
//	                        	LicenseExpireDate_count=cIndex;
//	                    		
//	                    	}
//	                    	if(LicenseExpireDate_count==cIndex){
//	                    		cellFlag=true;
//	                    		
//	                    	}	
//	                    	  
//	                    	
//	                    	
//	                    	if(hdrstr.toString().trim().equalsIgnoreCase("lic_effect_dte")){
//	                    		LicenseEffectiveDate_count=cIndex;
//	                    		
//	                    	}
//	                    	if(LicenseEffectiveDate_count==cIndex){
//	                    		cellFlag=true;
//	                    	}
//	                    	
//	                    	
//	                    	
//	                    	if(cellFlag){
//	                    		
//	                    			try{
//	                    				mapCell.put(cIndex+"",hdrstr);
//	                    			}catch(Exception e){
//	                    				mapCell.put(cIndex+"",hdrstr);
//	                    			}
//	                    		
//	                    	}
//	                        
//	                    }
//	                    
//	                    vectorCellEachRowData=cellValuePopulate(mapCell);
//	                    vectorData.addElement(vectorCellEachRowData);
//	                lineNumberHdr++;
//	            }
//	        }
//	        catch(Exception e){
//	            System.out.println(e.getMessage());
//	        }
//	       // System.out.println("::::::::: read data from text ::::::::::       "+vectorData);       
//		return vectorData;
//	}
//    
//    
//		    public static Vector cellValuePopulate(Map<String,String> mapCell){
//		   	 Vector vectorCellEachRowData = new Vector();
//		   	 Map<String,String> mapCellTemp = new TreeMap<String, String>();
//		   	 
//		   	 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false;
//		   	 for(Map.Entry<String, String> entry : mapCell.entrySet()){
//		   		 String key=entry.getKey();
//		   		String cellValue=null;
//		   		if(entry.getValue()!=null)
//		   			cellValue=entry.getValue().trim();
//		   		    
//		   		
//		   		if(key.equals("0")){
//		   			mapCellTemp.put(key, cellValue);
//		   			flag0=true;
//		   		}
//		   		if(key.equals("1")){
//		   			flag1=true;
//		   			mapCellTemp.put(key, cellValue);
//		   		}
//		   		if(key.equals("2")){
//		   			flag2=true;
//		   			mapCellTemp.put(key, cellValue);
//		   		}
//		   		if(key.equals("3")){
//		   			flag3=true;
//		   			mapCellTemp.put(key, cellValue);
//		   		}
//		   		if(key.equals("4")){
//		   			flag4=true;
//		   			mapCellTemp.put(key, cellValue);
//		   		}
//		   		
//		   		
//		   	 }
//		   	 if(flag0==false){
//		   		 mapCellTemp.put(0+"", "");
//		   	 }
//		   	 if(flag1==false){
//		   		 mapCellTemp.put(1+"", "");
//		   	 }
//		   	 if(flag2==false){
//		   		 mapCellTemp.put(2+"", "");
//		   	 }
//		   	if(flag3==false){
//		   		 mapCellTemp.put(3+"", "");
//		   	 }
//		   	if(flag4==false){
//		   		 mapCellTemp.put(4+"", "");
//		   	 }
//		   	 			 
//		   	 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
//		   		
//		   		 vectorCellEachRowData.addElement(entry.getValue());
//		   	 }
//		   	 return vectorCellEachRowData;
//		   }
//    
//    
//    
//	
//}
