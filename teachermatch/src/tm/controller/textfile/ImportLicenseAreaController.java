package tm.controller.textfile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import tm.bean.UserUploadFolderAccess;
import tm.bean.textfile.LicenseArea;
import tm.bean.user.UserMaster;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.textfile.LicenseAreaDAO;
import tm.dao.user.UserMasterDAO;

/* 
 * @Author: Sandeep Yadav
 */

@Controller
public class ImportLicenseAreaController {

	
	@Autowired
	private LicenseAreaDAO licenseAreaDAO;
	
	public void setLicenseAreaDAO(LicenseAreaDAO licenseAreaDAO) {
		this.licenseAreaDAO = licenseAreaDAO;
	}
	
	@Autowired
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	
	public void setUserUploadFolderAccessDAO(
			UserUploadFolderAccessDAO userUploadFolderAccessDAO) {
		this.userUploadFolderAccessDAO = userUploadFolderAccessDAO;
	}

	
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	

	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}



	@RequestMapping(value="/licensearea.do", method=RequestMethod.GET)
	public @ResponseBody String getAllLicenseAreaData(ModelMap map,HttpServletRequest request)
	{
		    System.out.println("::::: Use ImportLicenseAreaController :::::");
		    List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
			try {
				Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
				Criterion active = Restrictions.eq("status", "A");
				uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
			} catch (Exception e) {
				e.printStackTrace();
			}	
		    String fileName = "HRMS--2260-LAR--LicenseAreas.txt";
		    String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
		   // String folderPath = "E:/";
		   
			
		
		return saveLicenseArea(fileName,folderPath);
	}
	
	
	private Vector vectorDataTxt = new Vector();
    public String saveLicenseArea(String fileName,String folderPath){	
			
			 System.out.println("::::::::saveLicenseArea:::::::::");
			 
			 String filePath=folderPath+fileName;
		 	 System.out.println("filePath  "+filePath);
		 	 try {
		 		 if(fileName.contains(".txt") || fileName.contains(".TXT")){
		 			   vectorDataTxt = readDataTxt(filePath);
		 		 }else{
	    			 //break;
	    	 }
		} catch (Exception e) {
			e.printStackTrace();
		}
    	 
		SessionFactory sessionFactory=licenseAreaDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
 	    Transaction txOpen =statelesSsession.beginTransaction();
 	   
  	    List<LicenseArea> licenseAreaList =  licenseAreaDAO.findAll();
  	    Map<String, LicenseArea> licenseAreaMap = new HashMap<String, LicenseArea>();
  	    if(licenseAreaList!=null && licenseAreaList.size()>0)
  	    for (LicenseArea licenseArea2 : licenseAreaList) {
  	    	if(licenseArea2.getLicenseArea()!=null)
  	    	licenseAreaMap.put(licenseArea2.getSSN()+"##"+licenseArea2.getLicenseArea(), licenseArea2);
		}
		
		int SSN_count=11;
		int licenseArea_count=11;
		int licenseClassLevelCode_count=11;
		int licenseProgramStatusCode_count=11;
		int licenseProgramBasisCode_count=11;
		int licenseYearsOfExperience_count=11;
		int licenseAreaEffectiveDate_count=11;
		int licenseAreaHQCode_count=11;
		String final_error_store="";
	  	int numOfError=0;
	  	boolean row_error=false;
	  	int insertCount = 0;
	    int updateCount = 0;
	  	txOpen =statelesSsession.beginTransaction();  
	  	UserMaster userMaster = userMasterDAO.findById(1, false, false);
	  	
		 for(int i=0; i<vectorDataTxt.size(); i++) {
			 Vector vectorCellEachRowData = (Vector) vectorDataTxt.get(i);
	            String SSN="";
	            String licenseArea="";
	            String licenseClassLevelCode="";
	            String licenseProgramStatusCode =""; 
	            String licenseProgramBasisCode="";
	  	        String licenseYearsOfExperience="";
	  	        String licenseAreaEffectiveDate =""; 
	            String licenseAreaHQCode="";
	            String errorText="";
		  	    String rowErrorText="";
	  	       
	  	     
	  	      
	  	      
	  	    for(int j=0; j<vectorCellEachRowData.size(); j++) {
  	        	try{
  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("license_area. lar_ssn_txt")){
  	        			SSN_count=j;
	            	}
	            	if(SSN_count==j)
	            		SSN=vectorCellEachRowData.get(j).toString().trim();
  	        		
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("license_area.Lar_lic_area_cd")){
	            		licenseArea_count=j;
	            	}
	            	if(licenseArea_count==j)
	            		licenseArea=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("license_area.lar_cls_lvl_cd")){
	            		licenseClassLevelCode_count=j;
					}
	            	if(licenseClassLevelCode_count==j)
	            		licenseClassLevelCode=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("license_area.lar_pgm_sts_cd")){
	            		licenseProgramStatusCode_count=j;
					}
	            	if(licenseProgramStatusCode_count==j)
	            		licenseProgramStatusCode=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("license_area.lar_pgm_basis_cd")){
	            		licenseProgramBasisCode_count=j;
					}
	            	if(licenseProgramBasisCode_count==j)
	            		licenseProgramBasisCode=vectorCellEachRowData.get(j).toString().trim();

	           	   if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("license_area.lar_exper_num")){
	           		licenseYearsOfExperience_count=j;
					}
	            	if(licenseYearsOfExperience_count==j)
	            		licenseYearsOfExperience=vectorCellEachRowData.get(j).toString().trim();

	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("license_area.lar_effect_dte")){
	            		licenseAreaEffectiveDate_count=j;
					}
	            	if(licenseAreaEffectiveDate_count==j)
	            		licenseAreaEffectiveDate=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("license_area.lar_hq_cd")){
	            		licenseAreaHQCode_count=j;
					}
	            	if(licenseAreaHQCode_count==j)
	            		licenseAreaHQCode=vectorCellEachRowData.get(j).toString().trim();
	            	
            	}catch(Exception e){
            		e.printStackTrace();
            	}
  	        }
	  	    
	  	    
           if(i==0){
	  		  
	  		  if(!SSN.equalsIgnoreCase("license_area. lar_ssn_txt")){
	  			rowErrorText="license_area. lar_ssn_txt column is not found";
	  			row_error=true;
	  		  }
	  		  if(!licenseArea.equalsIgnoreCase("license_area.Lar_lic_area_cd")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="license_area.Lar_lic_area_cd column is not found";
	  			row_error=true;
	  		  }
	  		  if(!licenseClassLevelCode.equalsIgnoreCase("license_area.lar_cls_lvl_cd")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="license_area.lar_cls_lvl_cd column is not found";
	  			row_error=true;  
	  		  }
	  		if(!licenseProgramStatusCode.equalsIgnoreCase("license_area.lar_pgm_sts_cd")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="license_area.lar_pgm_sts_cd column is not found";
	  			row_error=true;  
	  		  }
	  		if(!licenseProgramBasisCode.equalsIgnoreCase("license_area.lar_pgm_basis_cd")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="license_area.lar_pgm_basis_cd column is not found";
	  			row_error=true;  
	  		  }
	 		if(!licenseYearsOfExperience.equalsIgnoreCase("license_area.lar_exper_num")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="license_area.lar_exper_num column is not found";
	  			row_error=true;  
	  		  }
			if(!licenseAreaEffectiveDate.equalsIgnoreCase("license_area.lar_effect_dte")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="license_area.lar_effect_dte column is not found";
	  			row_error=true;  
	  		  }
	  		if(!licenseAreaHQCode.equalsIgnoreCase("license_area.lar_hq_cd")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="license_area.lar_hq_cd column is not found";
	  			row_error=true;  
	  		  }
	  		
	  		 }
	  	    
	  	  
		  	if(row_error){
		  		numOfError++;
		  		File file = new File(folderPath+"licenseAreaError.txt");
				// if file doesnt exists, then create it
		  		try {
			  		if (!file.exists()) {
			  			file.createNewFile();
					}	    				
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(rowErrorText);
		
					bw.close();
		  		} catch (IOException e) {
					
					e.printStackTrace();
				}
				
		  		 break;
		  	 }
	  	    
	  	   
	  	   Boolean licenseAreaEffectiveDateFlag = true; 
	  	   
	  	   
	  	  if(i != 0 && row_error==false){
	  	    	
	  	    	boolean errorFlag=false;
	  	    	
	  	    	if(!SSN.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		if(SSN.replaceAll("\"", "").length()>9){
	  	    			errorText+="SSN length does not greater than 9";
	  	    		    errorFlag=true;
	  	    		}   
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="SSN is empty";
	    			    errorFlag=true;
	        	}
	  	    	
	  	    	
	  	    	if(licenseArea.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="License Area is empty";
	    			    errorFlag=true;
	        	}
	  	    	
               if(licenseClassLevelCode.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="License Class Level Code is empty";
	    			    errorFlag=true;
	        	}
              
              
              if(licenseProgramStatusCode.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		    if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="License Program Status Code is empty";
	    			    errorFlag=true;
	        	}
              if(licenseProgramBasisCode.replaceAll("\"", "").equalsIgnoreCase("")){
            	  
	  	    		
	    		    if(errorFlag){
						errorText+=",";	
					}
      			errorText+="License Program Basis Code is empty";
  			    errorFlag=true;
      	       }
              
              if(licenseYearsOfExperience.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	    		    if(errorFlag){
						errorText+=",";	
					}
    			errorText+="License Years Of Experience is empty";
			    errorFlag=true;
    	       }
              
             if(!licenseAreaEffectiveDate.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
            	  if (!licenseAreaEffectiveDate.replaceAll("\"", "").matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")){
            		  licenseAreaEffectiveDateFlag=false;
	  	    			errorText+="license Area Effective Date should be 'MM/dd/yyyy' format";
	  	    			errorFlag=true;
	  	    		}	
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
            	  if(errorFlag){
						errorText+=",";	
					}
      			errorText+="License Area Effective Date is empty";
  			    errorFlag=true;
  			    licenseAreaEffectiveDateFlag=false;
              }
              
              
              if(licenseAreaHQCode.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	    		    if(errorFlag){
						errorText+=",";	
					}
    			errorText+="License Area HQ Code is empty";
			    errorFlag=true;
            }
	  	    	
              if(!errorText.equals("")){
	        			int row = i+1;
	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+SSN+","+licenseArea+","+licenseClassLevelCode+","+licenseProgramStatusCode+","+licenseProgramBasisCode+","
	    			+licenseAreaEffectiveDate+","+licenseAreaHQCode+"<>";
	    			numOfError++;
	    		}
	  	    
	  	    
	  	    
              try {
	  	    	    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		  	    	LicenseArea licenseAreaObj = null;
	              	
	              	if(SSN.replaceAll("\"", "").isEmpty()){
	              		continue;
	              	}
	              	
	              	if(licenseAreaMap.containsKey(SSN.replaceAll("\"", "")+"##"+licenseArea.replaceAll("\"", ""))){
	              		licenseAreaObj = licenseAreaMap.get(SSN.replaceAll("\"", "")+"##"+licenseArea.replaceAll("\"", ""));
	              	}
	  	    	
	              	if(licenseAreaObj == null){
	              		licenseAreaObj = new LicenseArea();
	              		licenseAreaObj.setSSN(SSN.replaceAll("\"", ""));
	              		licenseAreaObj.setLicenseArea(licenseArea.replaceAll("\"", ""));
	              		licenseAreaObj.setLicenseClassLevelCode(licenseClassLevelCode.replaceAll("\"", ""));
	              		licenseAreaObj.setLicenseProgramStatusCode(licenseProgramStatusCode.replaceAll("\"", ""));
	              		licenseAreaObj.setLicenseProgramBasisCode(licenseProgramBasisCode.replaceAll("\"", ""));
	              		licenseAreaObj.setYearsOfExperience(licenseYearsOfExperience);
	              		if(licenseAreaEffectiveDateFlag)
	              		licenseAreaObj.setLicenseAreaEffectiveDate(formatter.parse(licenseAreaEffectiveDate.replaceAll("\"", "")));
	              		licenseAreaObj.setLicenseAreaHQCode(licenseAreaHQCode.replaceAll("\"", ""));
	              		licenseAreaObj.setStatus("A");
	              		licenseAreaObj.setCreatedDate(new Date());
	              		licenseAreaObj.setUserMaster(userMaster);
					  
	            	 	statelesSsession.insert(licenseAreaObj);
	            	 	insertCount++;
	            	 
	              	}else{
	              		licenseAreaObj.setLicenseArea(licenseArea.replaceAll("\"", ""));
	              		licenseAreaObj.setLicenseClassLevelCode(licenseClassLevelCode.replaceAll("\"", ""));
	              		licenseAreaObj.setLicenseProgramStatusCode(licenseProgramStatusCode.replaceAll("\"", ""));
	              		licenseAreaObj.setLicenseProgramBasisCode(licenseProgramBasisCode.replaceAll("\"", ""));
	              		licenseAreaObj.setYearsOfExperience(licenseYearsOfExperience);
	              		if(licenseAreaEffectiveDateFlag)
	              		licenseAreaObj.setLicenseAreaEffectiveDate(formatter.parse(licenseAreaEffectiveDate.replaceAll("\"", "")));
	              		licenseAreaObj.setLicenseAreaHQCode(licenseAreaHQCode.replaceAll("\"", ""));
	              		licenseAreaObj.setStatus("A");
	              		licenseAreaObj.setCreatedDate(new Date());
	              		licenseAreaObj.setUserMaster(userMaster);
					  
	              		statelesSsession.update(licenseAreaObj);
	            	 	updateCount++;
	            	 	
	              	}
	              	
	  	    		} catch (Exception e) {
					
						e.printStackTrace();
					}
	  	    }
	  	    
	  	    
		 }   

		 try{
				if(!final_error_store.equalsIgnoreCase("")){
		 	 		String content = final_error_store;		    				
					File file = new File(folderPath+"licenseAreaError.txt");
					// if file doesnt exists, then create it
					if (!file.exists()) {
						file.createNewFile();
					}
					String[] parts = content.split("<>");	    				
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write("license_area. lar_ssn_txt  ,  license_area.Lar_lic_area_cd  ,  license_area.lar_cls_lvl_cd  ,  license_area.lar_pgm_sts_cd  ,  license_area.lar_pgm_basis_cd  ,  license_area.lar_exper_num  ,  license_area.lar_effect_dte ,  license_area.lar_hq_cd");
					bw.write("\r\n\r\n");
					int k =0;
					for(String cont :parts) {
						bw.write(cont+"\r\n\r\n");
					    k++;
					}
					bw.close();
					
		 	 	}	
			  } catch (Exception e) {
					e.printStackTrace();
				}

			  txOpen.commit();
			  String message = "After File Upload,  "+insertCount+"  row inserted and  "+updateCount+"  row updated.";
			  System.out.print(message);
		     return message;
}

    
    
    
    public static Vector readDataTxt(String filePath) throws FileNotFoundException{
    	
		Vector vectorData = new Vector();
		String filepathhdr=filePath;

		
		int SSN_count=11;
		int LicenseArea_count=11;
		int LicenseClassLevelCode_count=11;
		int LicenseProgramStatusCode_count=11;
		int LicenseProgramBasisCode_count=11;
		int LicenseAreaEffectiveDate_count=11;
		int licenseYearsOfExperience_count=11;
		int LicenseAreaHQCode_count=11;
	
		    
	        BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
	        
	        String strLineHdr="";	        
	        String hdrstr="";
	        
	        
	        int lineNumberHdr=0;
	        try{
	            
	            while((strLineHdr=brhdr.readLine())!=null){   
	            	
	            	if(strLineHdr.isEmpty())
	            		continue;
	            	Vector vectorCellEachRowData = new Vector();
	                int cIndex=0;
	                boolean cellFlag=false,dateFlag=false;
	                Map<String,String> mapCell = new TreeMap<String, String>();
	                int i=1;
	                
	                String[] parts = strLineHdr.split("\\|");
	                
	                for(int j=0; j<parts.length; j++) {
	                	hdrstr=parts[j];
	                	cIndex=j;
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("license_area. lar_ssn_txt")){
	                        	SSN_count=cIndex;
	                    		
	                    	}
	                        
	                        if(SSN_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("license_area.Lar_lic_area_cd")){
	                        	LicenseArea_count=cIndex;
	                    		
	                    	}
	                    	if(LicenseArea_count==cIndex){
	                    		cellFlag=true;
	                    		
	                    	}	
	                    	  
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("license_area.lar_cls_lvl_cd")){
	                    		LicenseClassLevelCode_count=cIndex;
	                    		
	                    	}
	                    	if(LicenseClassLevelCode_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("license_area.lar_pgm_sts_cd")){
	                    		LicenseProgramStatusCode_count=cIndex;
	                    		
	                    	}
	                    	if(LicenseProgramStatusCode_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("license_area.lar_pgm_basis_cd")){
	                    		LicenseProgramBasisCode_count=cIndex;
	                    		
	                    	}
	                    	if(LicenseProgramBasisCode_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("license_area.lar_exper_num")){
	                    		licenseYearsOfExperience_count=cIndex;
	                    		
	                    	}
	                    	if(licenseYearsOfExperience_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                       	if(hdrstr.toString().trim().equalsIgnoreCase("license_area.lar_effect_dte")){
	                    		LicenseAreaEffectiveDate_count=cIndex;
	                    		
	                    	}
	                    	if(LicenseAreaEffectiveDate_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("license_area.lar_hq_cd")){
	                    		LicenseAreaHQCode_count=cIndex;
	                    		
	                    	}
	                    	if(LicenseAreaHQCode_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(cellFlag){
	                    		if(!dateFlag){
	                    			try{
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}catch(Exception e){
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}
	                    		}else{
	                    			mapCell.put(cIndex+"",hdrstr);
	                    		}
	                    	}
	                        
	                    }
	                    
	                    vectorCellEachRowData=cellValuePopulate(mapCell);
	                    vectorData.addElement(vectorCellEachRowData);
	                lineNumberHdr++;
	            }
	        }
	        catch(Exception e){
	            System.out.println(e.getMessage());
	        }
	       // System.out.println("::::::::: read data from Text ::::::::::       "+vectorData);       
		return vectorData;
	}
    
    
		    public static Vector cellValuePopulate(Map<String,String> mapCell){
		   	 Vector vectorCellEachRowData = new Vector();
		   	 Map<String,String> mapCellTemp = new TreeMap<String, String>();
		   	 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false;
		   	 for(Map.Entry<String, String> entry : mapCell.entrySet()){
		   		 String key=entry.getKey();
		   		String cellValue=null;
		   		if(entry.getValue()!=null)
		   			cellValue=entry.getValue().trim();
		   		
		   		if(key.equals("0")){
		   			mapCellTemp.put(key, cellValue);
		   			flag0=true;
		   		}
		   		if(key.equals("1")){
		   			flag1=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("2")){
		   			flag2=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("3")){
		   			flag3=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("4")){
		   			flag4=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("5")){
		   			flag5=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("6")){
		   			flag6=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("7")){
		   			flag7=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		
		   		
		   		
		   	 }
		   	 if(flag0==false){
		   		 mapCellTemp.put(0+"", "");
		   	 }
		   	 if(flag1==false){
		   		 mapCellTemp.put(1+"", "");
		   	 }
		   	 if(flag2==false){
		   		 mapCellTemp.put(2+"", "");
		   	 }
		   	if(flag3==false){
		   		 mapCellTemp.put(3+"", "");
		   	 }
		   	if(flag4==false){
		   		 mapCellTemp.put(4+"", "");
		   	 }
			if(flag5==false){
		   		 mapCellTemp.put(5+"", "");
		   	 }
			if(flag6==false){
		   		 mapCellTemp.put(6+"", "");
		   	 }
			if(flag7==false){
		   		 mapCellTemp.put(7+"", "");
		   	 }
			
		   	 			 
		   	 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
		   		 vectorCellEachRowData.addElement(entry.getValue());
		   	 }
		   	 return vectorCellEachRowData;
		   }
    
    
    
	
}
