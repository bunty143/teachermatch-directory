/* 
 * @Author: Anurag Kumar
 */

package tm.controller.textfile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import tm.bean.UserUploadFolderAccess;
import tm.bean.master.CityMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.UniversityMaster;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.UniversityMasterDAO;

@Controller
public class LicensureIHEDomain {
	
	@Autowired
	private UniversityMasterDAO universityMasterDAO;
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private CountryMasterDAO countryMasterDAO;
	
	@Autowired 
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO; 
	
	
	/**
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/importLicensureIHEDomain.do", method = RequestMethod.GET)
	public @ResponseBody String importLicensureIHEDomain(ModelMap map){

		 
		final long startTime = System.currentTimeMillis(); 		
		StringBuilder errorFlag = new StringBuilder();
		int newRec = 0, updateRec = 0, totalRec = 0;
		int rowcount = 0; 
		
		// header of txt file.		
		String iheCode="inst_higher_educ_dom.dom_ihe_id",iheName="inst_higher_educ_dom.dom_ihe_nm",iheCity = "inst_higher_educ_dom.dom_ihe_city_ad",iheState = "inst_higher_educ_dom.dom_ihe_st_ad";
		int iheCodepos=0,iheNamepos=0,iheStatepos=0,iheCitypos=0;
		int headerErrorFlag=0;
		int colCount=4;
		
		// begin Transaction ----------   start
		SessionFactory sessionFactory=universityMasterDAO.getSessionFactory();
		StatelessSession dbSession = sessionFactory.openStatelessSession();
		dbSession.beginTransaction();			
		// begin Transaction ----------   End
		
		
		String string="";		
		HashMap<String, UniversityMaster> dbdata = new HashMap<String, UniversityMaster>();
		HashMap<String, Integer>  cities	=	new HashMap<String, Integer>();
		HashMap<String, Integer>  states	=	new HashMap<String, Integer>();
		
		
		//get folder access Location dynamic from DB -----------  Start
	    System.out.println("::::: Use ImportLicense controller :::::");
	      List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
	   try {
	    Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
	    Criterion active = Restrictions.eq("status", "A");
	    uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
	   } catch (Exception e) {
	    e.printStackTrace();
	   } 
	   
	      String fileName = "HRMS--2260-IHED--IHEDom.txt" , errorFileName="HRMS--2260-IHED--IHEDom_error_"+startTime+".txt";
	      String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
	      
	      uploadFolderAccessesrec=null;  // for garbage collection 
	      
	  	//get folder access Location dynamic from DB -----------  end
		
		
		
		
		
		
		
		try  { 
			  
			File file = new File(folderPath+fileName);
			  string = FileUtils.readFileToString(file);
			
			// check file header format mismatch  
			
			  int headerLine=0;
			  
			for (String row : string.split("\n")) {
				rowcount++;
				System.out.println(" Line : "+rowcount);
				System.out.println(" row : "+row);
				System.out.println("total columns = " + row.split("\\|",colCount).length);

				// skip empty Line in txt files .......... Start				
				if (row.trim().length() < 1)
					continue;				
				// skip empty Line in txt files .......... End

				headerLine++; // for count Total record count
				
				int ccount = 0;  
				for (String col : row.split("\\|",colCount)) {
					 
					col=col.replaceAll("\r", ""); col=col.replaceAll("\"", ""); col=col.trim();
					if(headerLine==1){
						ccount++;
						if(col.equalsIgnoreCase(iheCode))
							iheCodepos=ccount;
						
						if(col.equalsIgnoreCase(iheName))
							iheNamepos=ccount;
						
						
						if(col.equalsIgnoreCase(iheState))
							iheStatepos=ccount;
						
						
						if(col.equalsIgnoreCase(iheCity))
							iheCitypos=ccount;
						
						 
	
					}
				}
				
				if(iheCodepos==0 || iheNamepos==0 || iheStatepos==0 || iheCitypos==0 ){
				headerErrorFlag=1;				
				}
				break;
			
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
			
		
			
		
			if(headerErrorFlag==0)
			{
				
				try{
			
			// get all data from database and put in hashmap with SSN and id As key-value   ................ Start
 
			if(string.trim().length()>10){
				
				List<UniversityMaster> alldata = universityMasterDAO.findAll();
				if(alldata.size()>0){
					for(UniversityMaster univdata : alldata){						
						dbdata.put(univdata.getUniversityName(), univdata);						
					}
					
					alldata=null;  // garbage
					
					List<CityMaster> allcity = cityMasterDAO.findAll();
					for(CityMaster city : allcity){						
						cities.put(city.getCityName(),(int) (long)city.getCityId());						
					}
					
						allcity=null ;  // garbage
					
					CountryMaster countryMaster = countryMasterDAO.getCountryByShortCode("US");
				    	 
				    List<StateMaster> allstate =  stateMasterDAO.findActiveStateByCountryId(countryMaster);	
						
					//List<StateMaster> allstate = stateMasterDAO.findAll();
					for(StateMaster state : allstate){		 			
					   states.put(state.getStateShortName(),(int) (long)state.getStateId());						
					}
					
							allstate=null ;//garbage
					
					
				}
			}
			
			// get all data from database and put in hashmap with SSN and id As key-value   ................ End
			
			
 

			
			
			for (String row : string.split("\n")) {
				rowcount++;
				System.out.println(" Line : "+rowcount);
				System.out.println("total columns = " + row.split("\\|",colCount).length);

				// skip empty Line in txt files .......... Start				
				if (row.trim().length() <= 1)
					continue;				
				// skip empty Line in txt files .......... End

				totalRec++; // for count Total record count
				
				// escape first line 
			  if(totalRec==1) continue;	
			
				
				
				int ccount = 0;
				UniversityMaster univ = new UniversityMaster();
				int colerror = 0;
				for (String col : row.split("\\|",colCount)) {
					 
					col=col.replaceAll("\r", ""); col=col.replaceAll("\"", "");  col=col.trim();
					 
					if (row.split("\\|",colCount).length == 4) {  System.out.println("col count= "+row.split("\\|",colCount).length);
						ccount++;
						try {
							if (ccount == iheCodepos) {								 
								univ.setIheCode(col);
							}

							if (ccount == iheNamepos) {
								univ.setUniversityName(col); 
							}
						

							if (ccount == iheCitypos) { 
								univ.setCityId(getCityId(cities, col));
							}
							
							
							if(ccount==iheStatepos){
								univ.setStateId(getStateId(states,col));
							}

						 
							
							
						} catch (Exception e) {
							System.out.println(" Exception occured " + e);
							colerror = 1;
						}

					}

					else {
						colerror = 1; 
						
						errorFlag.append(System.getProperty("line.separator"));
						errorFlag.append("Format Mismatched At line : "	+ rowcount + "   ( " + row + "   )");
						errorFlag.append(System.getProperty("line.separator"));
						break;
					}
					
					
						
						
					}

				 

				if (colerror == 0) {

					 
					univ.setStatus("A");
					  
					if (!dbdata.containsKey(univ.getUniversityName())) {
						
						try { dbSession.insert(univ); 
						
						newRec++; // count new Records
						} catch (Exception e) { System.out.println(" Insert Data Failure ::::::::::" + e.getMessage()); 
						
						 
						
						} 

						

					} else {

						System.out.println("updating   ..............................");
						
						UniversityMaster existData=dbdata.get(univ.getUniversityName());
						existData.setCityId( univ.getCityId());
						existData.setStateId(univ.getStateId());
						existData.setStatus("A");
					
						try { dbSession.update(existData); } catch (Exception e) { System.out.println(" Update Data Failure :::: "+e.getMessage()); 
						 
						}
						 

						updateRec++; // count Update Record.

					}
				} 

			}
			
			
			}
			
			
 
		 catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
			dbSession.getTransaction().commit();
			dbSession.close();
			
			
		}
 

		
		
		
		if (errorFlag.length() == 0)
			map.addAttribute("SuccessMessage", "File Import Successfully");
		else {
			map.addAttribute("ErrorMessage", " Errors In File Import");
		}

		errorFlag.insert(0, " Total Existing Records :-  " + updateRec);
		errorFlag.insert(0, System.getProperty("line.separator"));
		errorFlag.insert(0, " Total Records in file :-" + (totalRec-1));
		errorFlag.insert(0, System.getProperty("line.separator"));
		errorFlag.insert(0, " Total New Records in file :-" + newRec);
		errorFlag.insert(0, System.getProperty("line.separator"));

		errorFlag.append(System.getProperty("line.separator"));
		errorFlag.append(System.getProperty("line.separator"));
		errorFlag.append(System.getProperty("line.separator"));

		// writes errors in .txt file

		File f = new File(folderPath+errorFileName);

		try {

			// if file not Exist
			if (!f.exists()) {
				f.createNewFile();
			}

			FileWriter fwriter = new FileWriter(f);
			BufferedWriter bwriter = new BufferedWriter(fwriter);
			bwriter.write(errorFlag.toString());
			bwriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		
		
			}
			else{
				// header mismatch in txt file .
				
				File f = new File(folderPath+errorFileName);

				try {

					// if file not Exist
					if (!f.exists()) {
						f.createNewFile();
					}

					FileWriter fwriter = new FileWriter(f);
					BufferedWriter bwriter = new BufferedWriter(fwriter);
					bwriter.write("--------------- header format mismatch in txt file -----------------");
					bwriter.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
				
			}

		final long endtime = System.currentTimeMillis();

		System.out.println("total time Taken : " + (endtime - startTime));

		dbdata=null;  // erase data from map
		string=null;   // erase read file data from string
		cities=null;
		states=null;
		
		
		
		return errorFlag.toString();
	}
	
	
	public Integer getCityId(HashMap<String, Integer> city,String data){
		Integer result = null;
		if(city.containsKey(data)){
			result=city.get(data);
		}
		
		return result;
	}
	
	
	public Integer getStateId(HashMap<String, Integer> state,String data){
		Integer result = null;
		if(state.containsKey(data)){
			result=state.get(data);
		}
		
		return result;
	}
	
	
	  
	
}
