
/**
 * @author Anurag
 *
 */


package tm.controller.textfile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import tm.bean.EmployeeMaster;
import tm.bean.UserUploadFolderAccess;
import tm.bean.master.DistrictMaster;
import tm.dao.EmployeeMasterDAO;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.utility.Utility;

@Controller
public class DismissedMasterController {
	
	@Autowired
	EmployeeMasterDAO employeeMasterDAO;
	
	@Autowired 
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO; 
	
	@Autowired
	DistrictMasterDAO districtMasterDAO;
	
	@RequestMapping(value="/importDismissedMasters.do")
 public @ResponseBody String importDismissedMaster(){
		
		{

			 
			final long startTime = System.currentTimeMillis(); 		
			StringBuilder errorFlag = new StringBuilder();
			int newRec = 0, updateRec = 0, totalRec = 0;
			int rowcount = 0; 
			 
			
			DistrictMaster disMaster=null;
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			String datePattern="(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((19|20)\\d\\d)";
			
			
			// header of txt file.		
			String ssn="DTR_SSN_TXT",dismissDate="DTR_ACTION_DTE",statusCode = "DTR_STATUS_CD";
			int colCount=3;
			
			int ssnpos=0,dismissDatepos=0,statusCodepos=0;
			int headerErrorFlag=0;
			
			
			
			// begin Transaction ----------   start
			SessionFactory sessionFactory=employeeMasterDAO.getSessionFactory();
			StatelessSession dbSession = sessionFactory.openStatelessSession();
			dbSession.beginTransaction();			
			// begin Transaction ----------   End
			
			
			String string="";		
			HashMap<String, EmployeeMaster> dbdata = new HashMap<String, EmployeeMaster>();
			 
			
			//get folder access Location dynamic from DB -----------  Start
		    System.out.println("::::: Use importDismissedTeachers controller :::::");
		      List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
		      List<UserUploadFolderAccess> uploadFolderAccessesrec1 = null;
		   try {
		    Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
		    Criterion active = Restrictions.eq("status", "A");
		    uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
		    
		    // for district Id
		    
		    Criterion functionality1 = Restrictions.eq("functionality", "ncDistrictId");
		    Criterion active1 = Restrictions.eq("status", "A");
		    uploadFolderAccessesrec1 = userUploadFolderAccessDAO.findByCriteria(functionality1,active1);
		  
		    // get District Master from useruploadfolderaccessdetails table
		     disMaster=uploadFolderAccessesrec1.get(0).getDistrictId();
		     
		    
		    
		   } catch (Exception e) {
		    e.printStackTrace();
		   } 
		   
		      String fileName = "HRMS--2260-DMT--DismissedData.txt" , errorFileName="HRMS--2260-DMT--DismissedData_error_"+startTime+".txt";
		      String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
		      
		      uploadFolderAccessesrec=null;  // for garbage collection 
		      uploadFolderAccessesrec1=null;
		  	//get folder access Location dynamic from DB -----------  end
			
			
			
			
			
			
			
			try  { 
				  
				File file = new File(folderPath+fileName);
				  string = FileUtils.readFileToString(file);
				
				// check file header format mismatch  
				
				  int headerLine=0;
				  
				for (String row : string.split("\n")) {
					rowcount++;
					System.out.println(" Line : "+rowcount);
					System.out.println("total columns = " + row.split("\\|",colCount).length);

					// skip empty Line in txt files .......... Start				
					if (row.trim().length() < 1)
						continue;				
					// skip empty Line in txt files .......... End

					headerLine++; // for count Total record count
					
					int ccount = 0;  
					for (String col : row.split("\\|",colCount)) {
						col=col.replaceAll("\r", ""); col=col.replaceAll("\"", ""); col=col.trim();
						if(headerLine==1){
							ccount++;
							if(col.equalsIgnoreCase(ssn))
								ssnpos=ccount;
							
							if(col.equalsIgnoreCase(dismissDate))
								dismissDatepos=ccount;
							
							
							if(col.equalsIgnoreCase(statusCode))
								statusCodepos=ccount;
							
							
							 
		
						}
					}
					
					if(ssnpos==0 || dismissDatepos==0 || statusCodepos==0 ){
					headerErrorFlag=1;				
					}
					break;
				
				}
				
			}catch (Exception e) {
				e.printStackTrace();
			}
				
			
				
			
				if(headerErrorFlag==0)
				{
					
					try{
				
				// get all data of specific District from database and put in hashmap with SSN and id As key-value   ................ Start
	 
				if(string.trim().length()>10){
					
					Criterion cr1= Restrictions.eq("districtMaster.districtId", disMaster.getDistrictId());
					List<EmployeeMaster> alldata = employeeMasterDAO.findByCriteria(cr1);
					if(alldata.size()>0){
						for(EmployeeMaster empdata : alldata){						
							dbdata.put( Utility.decodeBase64(empdata.getSSN() ), empdata);						
						}
						
						alldata=null;  // garbage
						
						 
						
					}
				}
				
				// get all data from database and put in hashmap with SSN and id As key-value   ................ End
				
				
	 

				
				
				for (String row : string.split("\n")) {
					rowcount++;
					System.out.println(" Line : "+rowcount);
					System.out.println("total columns = " + row.split("\\|",colCount).length);

					// skip empty Line in txt files .......... Start				
					if (row.trim().length() <= 1)
						continue;				
					// skip empty Line in txt files .......... End

					totalRec++; // for count Total record count
					
					// escape first line 
				  if(totalRec==1) continue;	
				
					
					
					int ccount = 0;
					EmployeeMaster emp = new EmployeeMaster();
					int colerror = 0;
					for (String col : row.split("\\|",colCount)) {
						 
						col=col.replaceAll("\r", ""); col=col.replaceAll("\"", "");  col=col.trim();
						 
						if (row.split("\\|",colCount).length == colCount) { 
							ccount++;
							try {
								if (ccount == ssnpos) {								 
									emp.setSSN(Utility.encodeInBase64(col));
								}

								if (ccount == dismissDatepos) {
									emp.setDismissedDate(col.matches(datePattern) ? sdf.parse(col) : null); 
								}
							

								if (ccount == statusCodepos) { 
									emp.setApproveStatus(col);
								}
								
								 

							 
								
								
							} catch (Exception e) {
								System.out.println(" Exception occured " + e);
								colerror = 1;
							}

						}

						else {
							colerror = 1; 
							
							errorFlag.append(System.getProperty("line.separator"));
							errorFlag.append("Format Mismatched At line : "	+ rowcount + "   ( " + row + "   )");
							errorFlag.append(System.getProperty("line.separator"));
							break;
						}
						
						
							
							
						}

					 

					if (colerror == 0) {

						 //set Not Null Data of EmployeeMaster
					emp.setDistrictMaster(disMaster);	emp.setFirstName(""); emp.setMiddleName("");emp.setLastName(""); emp.setCreatedDateTime(new Date());
						emp.setStatus("A");
						  
						if (!dbdata.containsKey( Utility.decodeBase64(emp.getSSN()) )) {
							System.out.println("New Entry from txt file   ..............................");
							
							try { dbSession.insert(emp); } catch (Exception e) { System.out.println("Insert data failure :::: "+e.getMessage()); } 

							newRec++; // count new Records

						} else {

							System.out.println("updating   ..............................");
							
							EmployeeMaster existData=dbdata.get(Utility.decodeBase64(emp.getSSN()) );
							existData.setDismissedDate(emp.getDismissedDate());
							existData.setApproveStatus(emp.getApproveStatus());
							existData.setStatus("A");
						
							try { dbSession.update(existData); } catch (Exception e) { System.out.println(" Update data Failure  :::: "+ e.getMessage()); }
							 

							updateRec++; // count Update Record.

						}
					} 

				}
				
				
				}
				
				
	 
			 catch (Exception e) {
				e.printStackTrace();
			}
			finally{
				
				dbSession.getTransaction().commit();
				dbSession.close();
				
				
			}
	 

			
			
			 

			errorFlag.insert(0, " Total Existing Records :-  " + updateRec);
			errorFlag.insert(0, System.getProperty("line.separator"));
			errorFlag.insert(0, " Total Records in file :-" + (totalRec-1));
			errorFlag.insert(0, System.getProperty("line.separator"));
			errorFlag.insert(0, " Total New Records in file :-" + newRec);
			errorFlag.insert(0, System.getProperty("line.separator"));

			errorFlag.append(System.getProperty("line.separator"));
			errorFlag.append(System.getProperty("line.separator"));
			errorFlag.append(System.getProperty("line.separator"));

			// writes errors in .txt file

			File f = new File(folderPath+errorFileName);

			try {

				// if file not Exist
				if (!f.exists()) {
					f.createNewFile();
				}

				FileWriter fwriter = new FileWriter(f);
				BufferedWriter bwriter = new BufferedWriter(fwriter);
				bwriter.write(errorFlag.toString());
				bwriter.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			
			
			
				}
				else{
					// header mismatch in txt file .
					
					File f = new File(folderPath+errorFileName);

					try {

						// if file not Exist
						if (!f.exists()) {
							f.createNewFile();
						}

						FileWriter fwriter = new FileWriter(f);
						BufferedWriter bwriter = new BufferedWriter(fwriter);
						bwriter.write("--------------- header format mismatch in txt file -----------------\n\n\n\n\n");
						bwriter.write("\""+ssn+"\"|\""+dismissDate+"\"|\""+statusCode+"\"");
						bwriter.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
					
				}

			final long endtime = System.currentTimeMillis();

			System.out.println("total time Taken : " + (endtime - startTime));

			dbdata=null;  // erase data from map
			string=null;   // erase read file data from string
			 
			
			
			
			return errorFlag.toString();
		}
	}
}
