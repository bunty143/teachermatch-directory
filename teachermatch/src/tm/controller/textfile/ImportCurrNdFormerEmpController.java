package tm.controller.textfile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;


import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import tm.bean.EmployeeMaster;
import tm.bean.UserUploadFolderAccess;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;

import tm.dao.EmployeeMasterDAO;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.StateMasterDAO;


/* 
 * @Author: Sandeep Yadav
 */


@Controller
public class ImportCurrNdFormerEmpController {

	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	
	public void setEmployeeMasterDAO(EmployeeMasterDAO employeeMasterDAO) {
		this.employeeMasterDAO = employeeMasterDAO;
	}
	
	@Autowired
	private CountryMasterDAO countryMasterDAO;


	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	

	public void setDistricMasterDAO(DistrictMasterDAO districMasterDAO) {
		this.districtMasterDAO = districMasterDAO;
	}
	
	@Autowired
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	
	public void setUserUploadFolderAccessDAO(
			UserUploadFolderAccessDAO userUploadFolderAccessDAO) {
		this.userUploadFolderAccessDAO = userUploadFolderAccessDAO;
	}

	@Autowired
	private StateMasterDAO stateMasterDAO;


	@RequestMapping(value="/currentformeremp.do", method=RequestMethod.GET)
	public @ResponseBody String getAllCurrNfFormerEmpData(ModelMap map,HttpServletRequest request)
	{
		    System.out.println("::::: Use ImportCurrNdFormerEmpController  :::::");
		    List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
			try {
				Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
				Criterion active = Restrictions.eq("status", "A");
				uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
			} catch (Exception e) {
				e.printStackTrace();
			}
				
			String fileName = "TM_LEA_CURRENTFORMEREMPLOYEE.txt";
		    String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
			
		
		return saveCurrNfFormerEmpData(fileName,folderPath);
	}
	
	
	private Vector vectorDataTxt = new Vector();
	
	@Transactional(readOnly=false)
    public String saveCurrNfFormerEmpData(String fileName,String folderPath){	
			
			 System.out.println("::::::::saveCurrNdFormerEmpData:::::::::");
			 
			 String filePath=folderPath+fileName;
		 	 System.out.println("filePath  "+filePath);
		 	 try {
		 		 if(fileName.contains(".txt") || fileName.contains(".TXT")){
		 			   vectorDataTxt = readDataTxt(filePath);
		 		 }else{
	    			 //break;
	    	 }
		} catch (Exception e) {
			e.printStackTrace();
		}
    	 
/*		
			EmployeeMaster  empMaster = null;
			DistrictMaster  dstMaster = null;
			Map<String, EmployeeMaster> empMasterMap = new HashMap<String, EmployeeMaster>();
 	    	Session session=employeeMasterDAO.getSessionFactory().openSession();
	 	  try{	
			Criteria cr = session.createCriteria(EmployeeMaster.class);
			cr.setProjection(Projections.projectionList()
			  .add(Projections.property("districtMaster.districtId"),"districtId")	
		      .add(Projections.property("SSN"), "SSN")
			.add(Projections.property("employeeId"), "employeeId"));
	
		    List objList = cr.list();
		    session.close();
		    if(objList.size()>0){
                for (Iterator it = objList.iterator(); it.hasNext();) {
                
                    Object[] row = (Object[]) it.next()       ;                                                                 
	                empMaster = new EmployeeMaster();
	                dstMaster = new DistrictMaster();
	                dstMaster.setDistrictId((Integer) row[0]);
	                empMaster.setDistrictMaster(dstMaster);
	                empMaster.setSSN((String) row[1]);
	                empMaster.setEmployeeId((Integer)(row[2]));
	               
	                empMasterMap.put((String) row[1], empMaster);
	               
                }
		    }
		    
			} catch (Exception e) {
				e.printStackTrace();
			}
		

			
		 Map<String, EmployeeMaster> empMasterMap = new HashMap<String, EmployeeMaster>();	
			
			
    	 
    	 
    	 
    	 List<DistrictMaster> districtMasterList =  districtMasterDAO.findAll();
    	 Map<String, DistrictMaster> districtMasterMap = new HashMap<String, DistrictMaster>();
    	 if(districtMasterList!=null && districtMasterList.size()>0)
    	 for (DistrictMaster districtMaster2 : districtMasterList) {
    		 districtMasterMap.put(districtMaster2.getLocationCode(), districtMaster2);
		 }	 
	    	 
*/		 
			
		 HeadQuarterMaster headQuarterMaster = new HeadQuarterMaster();
    	 headQuarterMaster.setHeadQuarterId(2);
    	 Transaction txOpen = null;
         StatelessSession statelesSsession =null;
    	 
    	Map<String, DistrictMaster> districtMasterMap = new HashMap<String, DistrictMaster>();
		Map<String, EmployeeMaster> employeeMasterMap = new HashMap<String, EmployeeMaster>();
		Map<String, StateMaster> stateMasterMap = new HashMap<String, StateMaster>();
		List<DistrictMaster> districtMasterList = new ArrayList<DistrictMaster>();
		List<EmployeeMaster> employeeMasterList = new ArrayList<EmployeeMaster>();
		List<StateMaster> stateMasterList = new ArrayList<StateMaster>();
		
		try {
			 districtMasterList =  districtMasterDAO.getDistrictListByHeadQuater(headQuarterMaster);
			 districtMasterMap = new HashMap<String, DistrictMaster>();
			 if(districtMasterList!=null && districtMasterList.size()>0){
				 for (DistrictMaster districtMaster2 : districtMasterList) {
					 districtMasterMap.put(districtMaster2.getLocationCode(), districtMaster2);
				 }
			 }else{
				 System.out.println("districtMasterList size ##########"+districtMasterList.size());
				 return null;
			 }
			
			 employeeMasterList =  employeeMasterDAO.findEmployeeByDistricts(districtMasterList);
			 employeeMasterMap = new HashMap<String, EmployeeMaster>();
			 if(employeeMasterList!=null && employeeMasterList.size()>0)
			 for (EmployeeMaster employeeMaster2 : employeeMasterList) {
				 	 
				 employeeMasterMap.put(employeeMaster2.getDistrictMaster().getLocationCode()+"##"+employeeMaster2.getSSN(), employeeMaster2);
			 } 
				
             CountryMaster countryMaster = countryMasterDAO.getCountryByShortCode("US");
	    	 
	    	 stateMasterList =  stateMasterDAO.findActiveStateByCountryId(countryMaster);
			 if(stateMasterList!=null && stateMasterList.size()>0)
			 for (StateMaster stateMaster2 : stateMasterList) {
				 stateMasterMap.put(stateMaster2.getStateShortName(), stateMaster2);
			}
			 
			 SessionFactory sessionFactory=employeeMasterDAO.getSessionFactory();
			 statelesSsession = sessionFactory.openStatelessSession();
			 txOpen =statelesSsession.beginTransaction();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
		 
		 String final_error_store="";
    	 int numOfError=0;
    	 boolean row_error=false;
    	 int insertCount = 0;
	     int updateCount = 0;
    	 
	     int LEA_count=11;
    	 int SSN_count=11;
    	 int firstName_count=11; 
    	 int lastName_count=11;
    	 int streetAddress_count=11;
    	 int address2_count=11; 
    	 int city_count=11;
    	 int ZIP_count=11;	  
    	 int state_count=11;
    	 int homePhone_count=11;
    	 int cellPhone_count=11;
    	 int personalEmail_count=11; 
    	 int doNotHireDate_count=11;
    	 int doNotHireComment_count=11;
    	 int doNotHireStatus_count=11;
    	 int employmentStatus_count=11;
    	 
    	 
    	 
    	 
		 for(int i=0; i<vectorDataTxt.size(); i++) {
			 Vector vectorCellEachRowData = (Vector) vectorDataTxt.get(i);
			 
			 
	            String LEA="";
	            String SSN="";
	            String firstName="";
	            String lastName = "";
	            String streetAddress = "";
	            String address2  = "";
	            String city =""; 
	            String ZIP="";
	            String state="";
	            String homePhone="";
	            String cellPhone="";
	            String personalEmail = "";
	            String doNotHireDate =""; 
	            String doNotHireComment="";
	            String doNotHireStatus="";
	            String employmentStatus = "";
	            
	            String errorText="";
		  	    String rowErrorText="";
	  	     
	  	      
	  	      
	  	    for(int j=0; j<vectorCellEachRowData.size(); j++) {
  	        	try{
  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("LEA")){
  	        			LEA_count=j;
	            	}
	            	if(LEA_count==j)
	            		LEA=vectorCellEachRowData.get(j).toString().trim();
  	        		
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("EMP_SSN_TXT")){
	            		SSN_count=j;
	            	}
	            	if(SSN_count==j)
	            		SSN=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("EMP_FIRST_NM")){
	            		firstName_count=j;
					}
	            	if(firstName_count==j)
	            		firstName=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("EMP_LAST_NM")){
	            		lastName_count=j;
					}
	            	if(lastName_count==j)
	            		lastName=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("EMP_STREET_AD")){
	            		streetAddress_count=j;
					}
	            	if(streetAddress_count==j)
	            		streetAddress=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("EMP_MAIL_AD")){
	            		address2_count=j;
	            	}
	            	if(address2_count==j)
	            		address2=vectorCellEachRowData.get(j).toString().trim();
  	        		
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("EMP_CITY_AD")){
	            		city_count=j;
	            	}
	            	if(city_count==j)
	            		city=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("EMP_STATE_AD")){
	            		state_count=j;
					}
	            	if(state_count==j)
	            		state=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("EMP_ZIP_AD")){
	            		ZIP_count=j;
					}
	            	if(ZIP_count==j)
	            		ZIP=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("EMP_HPHONE_AD")){
	            		homePhone_count=j;
					}
	            	if(homePhone_count==j)
	            		homePhone=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("EMP_CPHONE_AD")){
	            		cellPhone_count=j;
					}
	            	if(cellPhone_count==j)
	            		cellPhone=vectorCellEachRowData.get(j).toString().trim();
	            	
	            
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("EMP_PERSONAL_EMAIL_AD")){
	            		personalEmail_count=j;
					}
	            	if(personalEmail_count==j)
	            		personalEmail=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("NOH_CREATE_DTE")){
	            		doNotHireDate_count=j;
					}
	            	if(doNotHireDate_count==j)
	            		doNotHireDate=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("NOH_COMMENT_TXT")){
	            		doNotHireComment_count=j;
					}
	            	if(doNotHireComment_count==j)
	            		doNotHireComment=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("NOH_EMP_APP_CD")){
	            		doNotHireStatus_count=j;
					}
	            	if(doNotHireStatus_count==j)
	            		doNotHireStatus=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Employment Status")){
	            		employmentStatus_count=j;
					}
	            	if(employmentStatus_count==j)
	            		employmentStatus=vectorCellEachRowData.get(j).toString().trim();
	            	
          	
	            	
            	}catch(Exception e){
            		e.printStackTrace();
            	}
  	        }
	  	    
	  	  if(i==0){
	  		  
	  		  if(!LEA.equalsIgnoreCase("LEA")){
	  			rowErrorText="LEA column is not found";
	  			row_error=true;
	  		  }
	  		  if(!SSN.equalsIgnoreCase("EMP_SSN_TXT")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="EMP_SSN_TXT column is not found";
	  			row_error=true;
	  		  }
	  		  if(!firstName.equalsIgnoreCase("EMP_FIRST_NM")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="EMP_FIRST_NM column is not found";
	  			row_error=true;  
	  		  }
	  		if(!lastName.equalsIgnoreCase("EMP_LAST_NM")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="EMP_LAST_NM column is not found";
	  			row_error=true;  
	  		  }
	  		
	  		if(!streetAddress.equalsIgnoreCase("EMP_STREET_AD")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText="EMP_STREET_AD column is not found";
	  			row_error=true;
	  		  }
	  		  if(!address2.equalsIgnoreCase("EMP_MAIL_AD")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="EMP_MAIL_AD column is not found";
	  			row_error=true;
	  		  }
	  		  if(!city.equalsIgnoreCase("EMP_CITY_AD")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="EMP_CITY_AD column is not found";
	  			row_error=true;  
	  		  }
	  		 if(!state.equalsIgnoreCase("EMP_STATE_AD")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="EMP_STATE_AD column is not found";
	  			row_error=true;  
	  		  }
	  		
	  		  if(!ZIP.equalsIgnoreCase("EMP_ZIP_AD")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="EMP_ZIP_AD column is not found";
		  			row_error=true;
		  		  }
	  		  if(!homePhone.equalsIgnoreCase("EMP_HPHONE_AD")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="EMP_HPHONE_AD column is not found";
	  			row_error=true;  
	  		  }
		  	   if(!cellPhone.equalsIgnoreCase("EMP_CPHONE_AD")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="EMP_CPHONE_AD column is not found";
		  			row_error=true;  
		  	    }
		  		
		  		
		  		if(!personalEmail.equalsIgnoreCase("EMP_PERSONAL_EMAIL_AD")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="EMP_PERSONAL_EMAIL_AD column is not found";
		  			row_error=true;  
		  		  }
		  		
		  		  if(!doNotHireDate.equalsIgnoreCase("NOH_CREATE_DTE")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+="NOH_CREATE_DTE column is not found";
			  			row_error=true;
			  	  }
			  	  if(!doNotHireComment.equalsIgnoreCase("NOH_COMMENT_TXT")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+="NOH_COMMENT_TXT column is not found";
			  			row_error=true;  
			  	   }
			  	   if(!doNotHireStatus.equalsIgnoreCase("NOH_EMP_APP_CD")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+="NOH_EMP_APP_CD column is not found";
			  			row_error=true;  
			  	     }
			  		
			  		if(!employmentStatus.equalsIgnoreCase("Employment Status")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+="Employment Status column is not found";
			  			row_error=true;  
			  		 }
		  		
	  	}
	  	    
	  	  
		  	if(row_error){
		  		numOfError++;
		  		File file = new File(folderPath+"TM_LEA_CURRENTFORMEREMPLOYEEError.txt");
				// if file doesnt exists, then create it
		  		try {
			  		if (!file.exists()) {
			  			file.createNewFile();
					}	    				
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(rowErrorText);
		
					bw.close();
		  		} catch (IOException e) {
					
					e.printStackTrace();
				}
				
		  		 break;
		  	 }
	  	    
	  	    
		  	Boolean doNotHireDateFlag = true;
	  	    
	  	    if(i != 0 && row_error==false){
	  	    	
	  	    	boolean errorFlag=false;
	  	    	
	  	    	if(LEA.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="LEA is empty";
  	    			    errorFlag=true;
	        	}
	  	    	
	  	    	
	  	    	if(SSN.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="SSN is empty";
  	    			    errorFlag=true;
	        	}
	  	    	
                if(firstName.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="First Name is empty";
  	    			    errorFlag=true;
	        	}
                
                
                if(lastName.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		    if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Last Name is empty";
  	    			    errorFlag=true;
	        	}
                
                
	            if(streetAddress.replaceAll("\"", "").equalsIgnoreCase("")){
		  	    		
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Street Address is empty";
		    			    errorFlag=true;
	        	}
	            
	            if(address2.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
  	    			if(errorFlag){
						errorText+=",";	
					}
        			errorText+="Address2 is empty";
	    			    errorFlag=true;
        	}
	  	    	
	  	    	
	  	    	if(state.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="State Code is empty";
		    			    errorFlag=true;
	        	}
	  	    	
	            if(city.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="City Name is empty";
		    			    errorFlag=true;
	        	}
	            
	            
	            if(ZIP.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		    if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="ZIP is empty";
		    			    errorFlag=true;
	        	}
	            
	           
		        if(cellPhone.replaceAll("\"", "").equalsIgnoreCase("")){
		  	    		
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Cell Phone is empty";
		    			    errorFlag=true;
	        	}
	  	    	
	  	    	
	  	    	if(homePhone.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Home Phone is empty";
		    			    errorFlag=true;
	        	}
	  	    	
	            if(personalEmail.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Personal Email is empty";
		    			    errorFlag=true;
	        	}
	            
	            
	            
		        if(!doNotHireDate.replaceAll("\"", "").equalsIgnoreCase("")){
		  	    		
		        	if (!doNotHireDate.replaceAll("\"", "").matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")){
		        		doNotHireDateFlag=false;
	  	    			errorText+="Do Not Hire Date should be 'MM/dd/yyyy' format";
	  	    			errorFlag=true;
	  	    		}	
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
		        	   if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Do Not Hire Date is empty";
		    			errorFlag=true;
		    			doNotHireDateFlag=false;
	        	}
	  	    	
	  	    	
	  	    	if(doNotHireComment.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Do Not Hire Comment is empty";
		    			    errorFlag=true;
	        	}
	  	    	
	            if(doNotHireStatus.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Do Not Hire Status is empty";
		    			    errorFlag=true;
	        	}
	            
	            if(employmentStatus.replaceAll("\"", "").equalsIgnoreCase("")){
  	    			if(errorFlag){
						errorText+=",";	
					}
        			errorText+="Employment Status is empty";
	    			    errorFlag=true;
        	}
	            
	            
	            
                if(!errorText.equals("")){
	        			int row = i+1;
  	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+LEA+" , "+SSN+" , "+firstName+" , "+lastName+" , "+streetAddress+" ,"+address2+" "+state+" , "+city+" , "+ZIP+" , "+cellPhone+" , "+homePhone+" , "+personalEmail+" , "+doNotHireDate+" , "+doNotHireComment+" , "+doNotHireStatus+" , "+employmentStatus+"<>";
  	    			numOfError++;
  	    		}
	  	    	
                
               
                
                try {
                	
                	
                	
                	if(LEA.replaceAll("\"", "").isEmpty()){
                		System.out.println("Row "+i+" , LEA is empty");
                		continue;
                	}
                	if(SSN.replaceAll("\"", "").isEmpty()){
                		System.out.println("Row "+i+" , SSN is empty");
                		continue;
                	}
                	if(firstName.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , First Name is empty");
                		firstName="";
                		//continue;
                	}
                	if(lastName.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , Last Name is empty");
                		//continue;
                		lastName="";
                	}
                	if(streetAddress.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , Street Address is empty");
                		//continue;
                		streetAddress="";
                	}
                	if(state.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , State is empty");
                		//continue;
                		state="";
                	}
                	
                	if(city.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , City is empty");
                		//continue;
                		city="";
                	}
                	if(ZIP.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , Zip is empty");
                		//continue;
                		ZIP="";
                	}
                	
                	
                	if(cellPhone.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , CellPhone is empty");
                		//continue;
                		cellPhone="";
                	}
                	if(homePhone.replaceAll("\"", "").isEmpty()){
                		//System.out.println("Row "+i+" , HomePhone is empty");
                		//continue;
                		homePhone="";
                	}
                	
                	
                	
               	    EmployeeMaster employeeMasterObj = null;
               	    DistrictMaster districtMasterObj = null;
                 	StateMaster stateMasterObj = null;
                	
                 	if(districtMasterMap.containsKey(LEA.replaceAll("\"", ""))){
                		districtMasterObj = districtMasterMap.get(LEA.replaceAll("\"", ""));
                	}
                	
                 	if(districtMasterObj == null){
                 		System.out.println("Row "+i+" , District Master Object is Null");
                 		continue;
                 	}
                 	   
                 	if(employeeMasterMap.containsKey(LEA.replaceAll("\"", "")+"##"+SSN.replaceAll("\"", ""))){
              		    employeeMasterObj = employeeMasterMap.get(LEA.replaceAll("\"", "")+"##"+SSN.replaceAll("\"", ""));
              
                	}
                	
                	 
                	if(stateMasterMap.containsKey(state.replaceAll("\"", ""))){
                		stateMasterObj = stateMasterMap.get(state.replaceAll("\"", ""));
                	}
                	
                	if(stateMasterObj == null)
                		continue;
                	 SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                     String address =  streetAddress+"  "+address2;
                    
                     
                     
                     if(employeeMasterObj == null){
                    	 employeeMasterObj = new EmployeeMaster();
                    	 employeeMasterObj.setDistrictMaster(districtMasterObj);
                    	 employeeMasterObj.setFirstName(firstName.replaceAll("\"", ""));
                    	 employeeMasterObj.setMiddleName("");
                    	 employeeMasterObj.setLastName(lastName.replaceAll("\"", ""));
                    	 employeeMasterObj.setSSN(SSN.replaceAll("\"", ""));
                    	 employeeMasterObj.setAddress1(address.replaceAll("\"", ""));
                    	 employeeMasterObj.setCityName(city.replaceAll("\"", ""));
                    	 employeeMasterObj.setStateMaster(stateMasterObj);
                    	 employeeMasterObj.setZipCode(ZIP.replaceAll("\"", ""));
                    	 employeeMasterObj.setPhone(homePhone.replaceAll("\"", ""));
                    	 employeeMasterObj.setMobile(cellPhone.replaceAll("\"", ""));
                    	 employeeMasterObj.setEmailAddress(personalEmail.replaceAll("\"", ""));
                    	 if(doNotHireDateFlag)
                    	 employeeMasterObj.setDoNotHireDate(formatter.parse(doNotHireDate.replaceAll("\"", "")));
                    	 employeeMasterObj.setDoNotHireComment(doNotHireComment.replaceAll("\"", ""));
                    	 employeeMasterObj.setDoNotHireStatus(doNotHireStatus.replaceAll("\"", ""));
                    	 employeeMasterObj.setEmploymentStatus(employmentStatus.replaceAll("\"", ""));
                    	 
                    	 
                    	 
                    	 employeeMasterObj.setStatus("A");
                    	 employeeMasterObj.setCreatedDateTime(new Date());
                    	 //employeeMasterDAO.makePersistent(employeeMasterObj);
                    	 statelesSsession.insert(employeeMasterObj);
                    	 insertCount++;
                    	 
                     }else{
                    	 
                    	 
                    	 employeeMasterObj.setDistrictMaster(districtMasterObj);
                    	 
                    	 employeeMasterObj.setFirstName(firstName.replaceAll("\"", ""));
                    	 employeeMasterObj.setLastName(lastName.replaceAll("\"", ""));
                    	 employeeMasterObj.setMiddleName("");
                    	 employeeMasterObj.setAddress1(address.replaceAll("\"", ""));
                    	 employeeMasterObj.setCityName(city.replaceAll("\"", ""));
                    	 employeeMasterObj.setStateMaster(stateMasterObj);
                    	 employeeMasterObj.setZipCode(ZIP.replaceAll("\"", ""));
                    	 employeeMasterObj.setPhone(homePhone.replaceAll("\"", ""));
                    	 employeeMasterObj.setMobile(cellPhone.replaceAll("\"", ""));
                    	 employeeMasterObj.setEmailAddress(personalEmail.replaceAll("\"", ""));
                    	 if(doNotHireDateFlag)
                    	 employeeMasterObj.setDoNotHireDate(formatter.parse(doNotHireDate.replaceAll("\"", "")));
                    	 employeeMasterObj.setDoNotHireComment(doNotHireComment.replaceAll("\"", ""));
                    	 employeeMasterObj.setDoNotHireStatus(doNotHireStatus.replaceAll("\"", ""));
                    	 employeeMasterObj.setEmploymentStatus(employmentStatus.replaceAll("\"", ""));
                    	 employeeMasterObj.setStatus("A");
                    	 employeeMasterObj.setCreatedDateTime(new Date());
                    	 statelesSsession.update(employeeMasterObj);
                    	// employeeMasterDAO.makePersistent(employeeMasterObj);
	            	 	 updateCount++;
                     }
                     
                    
	  	    		} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	  	    		
	  	    }
	  	    
	  	    
		 }   

		 try{
			if(!final_error_store.equalsIgnoreCase("")){
	 	 		String content = final_error_store;		    				
				File file = new File(folderPath+"TM_LEA_CURRENTFORMEREMPLOYEEError.txt");
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				String[] parts = content.split("<>");	    				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write("LEA , SSN , First Name , Last Name , Street address , Address2 , City , State , Zip Code , Home Phone , Cell Phone , Personal Email , Do Not Hire Date , Do Not Hire Comment , Do Not Hire Status , Employment Status");
				bw.write("\r\n\r\n");
				int k =0;
				for(String cont :parts) {
					bw.write(cont+"\r\n\r\n");
				    k++;
				}
				bw.close();
				
	 	 	}	
			txOpen.commit();
		  } catch (Exception e) {
				e.printStackTrace();
			}
			
		  
		  
		  String message = "After File Upload,  "+insertCount+"  row inserted and  "+updateCount+"  row updated.";
		  System.out.print(message);
	      return message;

}

    
    
    
    public static Vector readDataTxt(String filePath) throws FileNotFoundException{
    	
		Vector vectorData = new Vector();
		String filepathhdr=filePath;

			 int LEA_count=11;
	    	 int SSN_count=11;
	    	 int firstName_count=11; 
	    	 int lastName_count=11;
	    	 int streetAddress_count=11;
	    	 int address2_count=11; 
	    	 int city_count=11;
	    	 int ZIP_count=11;	  
	    	 int state_count=11;
	    	 int homePhone_count=11;
	    	 int cellPhone_count=11;
	    	 int personalEmail_count=11; 
	    	 int doNotHireDate_count=11;
	    	 int doNotHireComment_count=11;
	    	 int doNotHireStatus_count=11;
	    	 int employmentStatus_count=11;
		
		    
	        BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
	        
	        String strLineHdr="";	        
	        String hdrstr="";
	        
	        
	        int lineNumberHdr=0;
	        try{
	            
	            while((strLineHdr=brhdr.readLine())!=null){  
	            	
	            	if(strLineHdr.isEmpty())
	            		continue;
	            	Vector vectorCellEachRowData = new Vector();
	                int cIndex=0;
	                boolean cellFlag=false;
	                Map<String,String> mapCell = new TreeMap<String, String>();
	                int i=1;
	                
	                String[] parts = strLineHdr.split("\\|");
	                
	                for(int j=0; j<parts.length; j++) {
	                	hdrstr=parts[j];
	                	cIndex=j;
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("LEA")){
	                        	LEA_count=cIndex;
	                    		
	                    	}
	                        
	                        if(LEA_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("EMP_SSN_TXT")){
	                        	SSN_count=cIndex;
	                    		
	                    	}
	                    	if(SSN_count==cIndex){
	                    		cellFlag=true;
	                    		
	                    	}	
	                    	  
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("EMP_FIRST_NM")){
	                    		firstName_count=cIndex;
	                    		
	                    	}
	                    	if(firstName_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("EMP_LAST_NM")){
	                    		lastName_count=cIndex;
	                    		
	                    	}
	                    	if(lastName_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("EMP_STREET_AD")){
	                    		streetAddress_count=cIndex;
	                    		
	                    	}
	                    	if(streetAddress_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("EMP_MAIL_AD")){
	                    		address2_count=cIndex;
	                    		
	                    	}
	                        
	                        if(address2_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("EMP_CITY_AD")){
	                        	city_count=cIndex;
	                    		
	                    	}
	                    	if(city_count==cIndex){
	                    		cellFlag=true;
	                    		
	                    	}	
	                    	  
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("EMP_STATE_AD")){
	                    		state_count=cIndex;
	                    		
	                    	}
	                    	if(state_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("EMP_ZIP_AD")){
	                    		ZIP_count=cIndex;
	                    		
	                    	}
	                    	if(ZIP_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("EMP_HPHONE_AD")){
	                    		homePhone_count=cIndex;
	                    		
	                    	}
	                    	if(homePhone_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("EMP_CPHONE_AD")){
	                    		cellPhone_count=cIndex;
	                    		
	                    	}
	                    	if(cellPhone_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("EMP_PERSONAL_EMAIL_AD")){
	                    		personalEmail_count=cIndex;
	                    		
	                    	}
	                    	if(personalEmail_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("NOH_CREATE_DTE")){
	                    		doNotHireDate_count=cIndex;
	                    		
	                    	}
	                    	if(doNotHireDate_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("NOH_COMMENT_TXT")){
	                    		doNotHireComment_count=cIndex;
	                    		
	                    	}
	                    	if(doNotHireComment_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("NOH_EMP_APP_CD")){
	                    		doNotHireStatus_count=cIndex;
	                    		
	                    	}
	                    	if(doNotHireStatus_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("Employment Status")){
	                    		employmentStatus_count=cIndex;
	                    		
	                    	}
	                    	if(employmentStatus_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(cellFlag){
	                    		
	                    			try{
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}catch(Exception e){
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}
	                    		
	                    	}
	                        
	                    }
	                    
	                    vectorCellEachRowData=cellValuePopulate(mapCell);
	                    vectorData.addElement(vectorCellEachRowData);
	                lineNumberHdr++;
	            }
	        }
	        catch(Exception e){
	            System.out.println(e.getMessage());
	        }
	        //System.out.println("::::::::: read data from text ::::::::::       "+vectorData);       
		return vectorData;
	}
    
    
		    public static Vector cellValuePopulate(Map<String,String> mapCell){
		   	 Vector vectorCellEachRowData = new Vector();
		   	 Map<String,String> mapCellTemp = new TreeMap<String, String>();
		   	 
		   	 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false,flag9=false,flag10=false,flag11=false,flag12=false,flag13=false,flag14=false,flag15=false,flag16=false;
		   	 for(Map.Entry<String, String> entry : mapCell.entrySet()){
		   		 String key=entry.getKey();
		   		String cellValue=null;
		   		if(entry.getValue()!=null)
		   			cellValue=entry.getValue().trim();
		   		    
		   		
		   		if(key.equals("0")){
		   			mapCellTemp.put(key, cellValue);
		   			flag0=true;
		   		}
		   		if(key.equals("1")){
		   			flag1=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("2")){
		   			flag2=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("3")){
		   			flag3=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("4")){
		   			flag4=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		
		   		if(key.equals("5")){
		   			mapCellTemp.put(key, cellValue);
		   			flag5=true;
		   		}
		   		if(key.equals("6")){
		   			flag6=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("7")){
		   			flag7=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("8")){
		   			flag8=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("9")){
		   			flag9=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("10")){
		   			flag10=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("11")){
		   			flag11=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("12")){
		   			flag12=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("13")){
		   			flag13=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("14")){
		   			flag14=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("15")){
		   			flag15=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("16")){
		   			flag16=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   	
		   		
		   	 }
		   	 if(flag0==false){
		   		 mapCellTemp.put(0+"", "");
		   	 }
		   	 if(flag1==false){
		   		 mapCellTemp.put(1+"", "");
		   	 }
		   	 if(flag2==false){
		   		 mapCellTemp.put(2+"", "");
		   	 }
		   	if(flag3==false){
		   		 mapCellTemp.put(3+"", "");
		   	 }
		   	if(flag4==false){
		   		 mapCellTemp.put(4+"", "");
		   	 }
		    if(flag5==false){
		   		 mapCellTemp.put(5+"", "");
		   	 }
		   	 if(flag6==false){
		   		 mapCellTemp.put(6+"", "");
		   	 }
		   	 if(flag7==false){
		   		 mapCellTemp.put(7+"", "");
		   	 }
		   	if(flag8==false){
		   		 mapCellTemp.put(8+"", "");
		   	 }
		   	if(flag9==false){
		   		 mapCellTemp.put(9+"", "");
		   	 }
			if(flag10==false){
		   		 mapCellTemp.put(10+"", "");
		   	 }
		   	if(flag11==false){
		   		 mapCellTemp.put(11+"", "");
		   	 }
		   	
		   	if(flag12==false){
		   		 mapCellTemp.put(12+"", "");
		   	 }
		   	 if(flag13==false){
		   		 mapCellTemp.put(13+"", "");
		   	 }
		   	if(flag14==false){
		   		 mapCellTemp.put(14+"", "");
		   	 }
		   	if(flag15==false){
		   		 mapCellTemp.put(15+"", "");
		   	 }
			if(flag16==false){
		   		 mapCellTemp.put(16+"", "");
		   	 }
		   
		   	
		   	 
		   	 			 
		   	 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
		   		
		   		 vectorCellEachRowData.addElement(entry.getValue());
		   	 }
		   	 return vectorCellEachRowData;
		   }
    
    
    
	
}
