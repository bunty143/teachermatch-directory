package tm.controller.hqbranches;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.StatusWiseAutoEmailSend;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.ReasonsForWaived;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.ExclusivePeriodMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusNodeMaster;
import tm.bean.master.StatusPrivilegeForSchools;
import tm.bean.master.SubjectMaster;
import tm.bean.master.TimeZoneMaster;
import tm.bean.user.UserMaster;
import tm.dao.EmploymentServicesTechnicianDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.StatusWiseAutoEmailSendDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.hqbranchesmaster.ReasonsForWaivedDAO;
import tm.dao.i4.I4QuestionSetsDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.ExclusivePeriodMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.StatusNodeMasterDAO;
import tm.dao.master.StatusPrivilegeForSchoolsDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.TimeZoneMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.services.PaginationAndSorting;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.ImageResize;
import tm.utility.Utility;

@Controller
public class HqBranchController {
	
	private static final Logger logger = Logger.getLogger(HqBranchController.class.getName());

	
	@Autowired
	private ExclusivePeriodMasterDAO exclusivePeriodMasterDAO;
	
	@Autowired
	private StatusNodeMasterDAO statusNodeMasterDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	@Autowired
	private I4QuestionSetsDAO i4QuestionSetsDAO;
	
	@Autowired 
	private ContactTypeMasterDAO contactTypeMasterDAO;
	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}
	
	@Autowired
	private TimeZoneMasterDAO timeZoneMasterDAO;
	
	@Autowired
	private ServletContext servletContext;
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	public void setSubjectMasterDAO(SubjectMasterDAO subjectMasterDAO) {
		this.subjectMasterDAO = subjectMasterDAO;
	}
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private StatusWiseAutoEmailSendDAO statusWiseAutoEmailSendDAO;
	
	@Autowired
	private StatusPrivilegeForSchoolsDAO statusPrivilegeForSchoolsDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private ReasonsForWaivedDAO reasonsForWaivedDAO;
	
	@Autowired
	private EmploymentServicesTechnicianDAO employmentServicesTechnicianDAO;
	
	@RequestMapping(value="/manageheadquarter.do" ,method=RequestMethod.GET)
	public String manageHeadQuarter(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		logger.info("manageheadquarter.do call");
		//request.getSession();
		try 
		{
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster	userMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				logger.info(" email Address :: "+userMaster.getEmailAddress());
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String authKeyVal = request.getParameter("authKeyVal")==null?"0":request.getParameter("authKeyVal").trim();
			
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,98,"manageheadquarter.do",100);
				logger.info(" roleAccess :: "+roleAccess);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("authKeyVal", authKeyVal);
			try{
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in manage headquarter");
			}catch(Exception e){
				e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "manageheadquarter";
	}
	
	@RequestMapping(value="/managebranches.do" ,method=RequestMethod.GET)
	public String manageBranches(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		logger.info("managebranches.do call");
		try 
		{
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster	userMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				logger.info(" email Address :: "+userMaster.getEmailAddress());
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String authKeyVal = request.getParameter("authKeyVal")==null?"0":request.getParameter("authKeyVal").trim();
			
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,99,"managebranches.do",100);
				logger.info(" roleAccess :: "+roleAccess);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("authKeyVal", authKeyVal);
			if(roleId==10 || roleId==12){
				map.addAttribute("headQuarterMaster", userMaster.getHeadQuarterMaster());
			}
			else if(roleId==11 || roleId==13){
				map.addAttribute("headQuarterMaster", userMaster.getHeadQuarterMaster());
				map.addAttribute("branchMaster", userMaster.getBranchMaster());
			}
			logger.info(" userMaster.getHeadQuarterMaster() :::: "+ userMaster.getHeadQuarterMaster().getIsBranchExist());
			try{
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in manage branch");
			}catch(Exception e){
				e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "managebranches";
	}
	
	
	@RequestMapping(value="/editheadquarter.do" ,method=RequestMethod.GET)
	public String editHeadQuarter(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		logger.info(" editheadquarter.do........ call");
		
		try
		{
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}

			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			List<UserMaster> districtsUser	=	null;
			HeadQuarterMaster headQuarterMaster		=	null;
			String headQuarterId = request.getParameter("hqId")	==	null?"0":request.getParameter("hqId").trim();
			Boolean branchExists = false;
			if(headQuarterId.equals("0"))
			{
				headQuarterMaster	=	new HeadQuarterMaster();
			}
			else
			{
				headQuarterMaster	=	headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
				/*List<BranchMaster> branchMasters = branchMasterDAO.getBranchesByHeadQuarter(headQuarterMaster);
				if(branchMasters!=null && branchMasters.size()>0)
				{
					branchExists = true;
				}*/
				if(headQuarterMaster!=null && headQuarterMaster.getIsBranchExist())
				{
					branchExists = true;
				}
			}
			if(userSession!=null)
			{
				if(userSession.getHeadQuarterMaster()!=null)
				{
					map.put("hqId", headQuarterMaster.getHeadQuarterId());
				}
				else
				{
					map.put("hqId", 0);
				}
				if(userSession.getBranchMaster()!=null)
				{
					map.put("bId", userSession.getBranchMaster().getBranchId());
				}
				else
				{
					map.put("bId", 0);
				}
			}
			if(headQuarterMaster.getCanTMApproach()!=0){
				headQuarterMaster.setCanTMApproach(1);
			}
		
			String roleAccess=null,roleAccessGI=null,roleAccessCI=null,roleAccessU=null,roleAccessAI=null;
			int roleId=0;
			if(userSession.getRoleId()!=null){
				roleId=userSession.getRoleId().getRoleId();
			}
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,133,"editheadquarter.do",100);
				roleAccessGI=roleAccessPermissionDAO.getMenuOptionList(roleId,133,"editheadquarter.do",100);
				roleAccessCI=roleAccessPermissionDAO.getMenuOptionList(roleId,133,"editheadquarter.do",100);
				roleAccessU=roleAccessPermissionDAO.getMenuOptionList(roleId,133,"editheadquarter.do",100);
				roleAccessAI=roleAccessPermissionDAO.getMenuOptionList(roleId,133,"editheadquarter.do",100);				
				
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("roleAccessGI", roleAccessGI);
			map.addAttribute("roleAccessCI", roleAccessCI);
			map.addAttribute("roleAccessU", roleAccessU);
			map.addAttribute("roleAccessAI", roleAccessAI);
			map.addAttribute("headQuarterMaster", headQuarterMaster);
			map.addAttribute("branchExists", branchExists);
			List<ContactTypeMaster> contactTypeMaster			=	contactTypeMasterDAO.findByCriteria(Order.asc("contactType"));
			map.put("contactTypeMaster", contactTypeMaster);
			
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);
			String authKey=null;
			if(headQuarterMaster.getAuthKey()!=null){
				authKey=Utility.decodeBase64(headQuarterMaster.getAuthKey());
			}
			map.addAttribute("authKey",authKey);
			if(headQuarterMaster.getDisplayTMDefaultJobCategory()!=null){
				map.addAttribute("displayTMDefaultJobCategory", headQuarterMaster.getDisplayTMDefaultJobCategory());
			}else{
				map.addAttribute("displayTMDefaultJobCategory",true);
			}
			if(headQuarterMaster.getContractStartDate()==null){
				map.addAttribute("contractStartDate",null);
			}else{
				map.addAttribute("contractStartDate", Utility.getCalenderDateFormart(headQuarterMaster.getContractStartDate()+""));
			}
			
			if(headQuarterMaster.getContractEndDate()==null){
				map.addAttribute("contractEndDate",null);
			}else{
				map.addAttribute("contractEndDate", Utility.getCalenderDateFormart(headQuarterMaster.getContractEndDate()+""));
			}
			
			String JobBoardURL=null;
			try{
				logger.info(Utility.getBaseURL(request)+"jobsboard.do?hqId="+Utility.encryptNo(headQuarterMaster.getHeadQuarterId()));
				JobBoardURL = Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?hqId="+Utility.encryptNo(headQuarterMaster.getHeadQuarterId()));

			}catch (Exception e) {}
			
			map.addAttribute("JobBoardURL",JobBoardURL);
			map.addAttribute("userSession", userSession);
			////////////////////////////////////////////////
			List<SecondaryStatus> lstsecondaryStatus=  secondaryStatusDAO.findSecondaryStatusOnlyByHeadBranch(headQuarterMaster,null,null);
			
			map.addAttribute("lstsecondaryStatus", lstsecondaryStatus);
			List<SecondaryStatus> lstSecStatus=  secondaryStatusDAO.findSecondaryStatusHeadQuarterBranchOnly(headQuarterMaster,null,null);
			Map<Integer,SecondaryStatus> mapSStatus = new HashMap<Integer, SecondaryStatus>();
			if(lstsecondaryStatus!=null)
			for(SecondaryStatus secStatus : lstSecStatus){
				mapSStatus.put(secStatus.getStatusMaster().getStatusId(),secStatus);
			}
			List<StatusMaster> lstStatusMaster	=	null; 
			List<StatusMaster> statusMasterList	=new ArrayList<StatusMaster>();
			String[] statuss = {"hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
			lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statuss);
			SecondaryStatus secondaryStatus=null;
			for(StatusMaster sm: lstStatusMaster){
				if(sm.getBanchmarkStatus()==1){
					secondaryStatus=mapSStatus.get(sm.getStatusId());
					if(secondaryStatus!=null){
						sm.setStatus(secondaryStatus.getSecondaryStatusName());
					}
				}
				statusMasterList.add(sm);
			}
			map.addAttribute("lstStatusMaster", statusMasterList);
			///////////////////////////////////////////////////
			
			
			statusMasterList	=new ArrayList<StatusMaster>();
			
			for(StatusMaster sm: lstStatusMaster){
				if(sm.getBanchmarkStatus()==1){
					secondaryStatus=mapSStatus.get(sm.getStatusId());
					if(secondaryStatus!=null){
						sm.setStatus(secondaryStatus.getSecondaryStatusName());
					}
				}
				statusMasterList.add(sm);
			}
			
			
			
			
			//For Reference
			try
			{
				
				String statusListBoxValueForReminder="";
				
				if(headQuarterMaster.getStatusMasterForReference()!=null)
				{
					logger.info(" First ");
					statusListBoxValueForReminder=""+headQuarterMaster.getStatusMasterForReference().getStatusId();
				}
				else if(headQuarterMaster.getSecondaryStatusForReference()!=null)
				{
					logger.info("second");
					statusListBoxValueForReminder="SSIDForReminder_"+headQuarterMaster.getSecondaryStatusForReference().getSecondaryStatusId();
				}
				
				logger.info(" statusListBoxValueForReminder >>>>>>>>> "+statusListBoxValueForReminder);
				map.addAttribute("statusListBoxValueForReminder", statusListBoxValueForReminder);
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			
			map.addAttribute("lstStatusMaster_PFC", statusMasterList);
			map.addAttribute("lstsecondaryStatus_PFC", lstsecondaryStatus);
			
			if(headQuarterMaster!=null && headQuarterMaster.getStatusIdReminderExpireAction()!=null)
				map.addAttribute("statusIdReminderExpireAction", headQuarterMaster.getStatusIdReminderExpireAction().getStatusId());
			
			
		
			List<StatusWiseAutoEmailSend> listStatusWiseAutoEmailSends =null; 
			listStatusWiseAutoEmailSends = 	statusWiseAutoEmailSendDAO.findAllRecordsForHeadQuarter(headQuarterMaster);	
			
			List<Integer> statusIDs = new ArrayList<Integer>();
			List<Integer> sStatusID = new ArrayList<Integer>();
			
			if(listStatusWiseAutoEmailSends!=null){
				logger.info("listStatusWiseAutoEmailSends :: "+listStatusWiseAutoEmailSends.size());
				 for(StatusWiseAutoEmailSend swes : listStatusWiseAutoEmailSends){
					 if(swes.getStatusId()!=null && swes.getAutoEmailRequired().equals(1))
					 {
					  statusIDs.add(swes.getStatusId());
					 }
					 else if(swes.getSecondaryStatusId()!=null && swes.getAutoEmailRequired().equals(1))
					 {
					  sStatusID.add( swes.getSecondaryStatusId());
					 }
				 }
				 logger.info(" statusIDs :: "+statusIDs.size());
				 logger.info(" sStatusID :: "+sStatusID.size());
			}
			
			
			String [] chkStatusMasterEml=new String[statusIDs.size()];
			int k1=0;
			for(Integer pojo:statusIDs){
				chkStatusMasterEml[k1++]=pojo.toString();
			}
			headQuarterMaster.setChkStatusMasterEml(chkStatusMasterEml);
			
			String [] chkSecondaryStatusNameEml=new String[sStatusID.size()];
			int k2=0;
			for(Integer pojo:sStatusID){
				chkSecondaryStatusNameEml[k2++]=pojo.toString();
			}
			headQuarterMaster.setChkSecondaryStatusNameEml(chkSecondaryStatusNameEml);
			
			if(statusIDs.size()>0 || sStatusID.size()>0){
				headQuarterMaster.setAutoEmailRequired(true);
			}
			
			String statusListBoxValue="";

			if(headQuarterMaster.getStatusMaster()!=null)
				statusListBoxValue=""+headQuarterMaster.getStatusMaster().getStatusId();
			else if(headQuarterMaster.getSecondaryStatus()!=null)
				statusListBoxValue="SSID_"+headQuarterMaster.getSecondaryStatus().getSecondaryStatusId();
			map.addAttribute("statusListBoxValue", statusListBoxValue);			
			
			
			try {
				List<StatusPrivilegeForSchools> lstStatusPrivilegeForSchools = statusPrivilegeForSchoolsDAO.findHqStatusForStatusMasterId(headQuarterMaster);
				String [] chkStatusMaster=new String[lstStatusPrivilegeForSchools.size()];
				int k=0;
				if(lstStatusPrivilegeForSchools!=null && lstStatusPrivilegeForSchools.size()>0)
				{
					logger.info(" lstStatusPrivilegeForSchools :: "+lstStatusPrivilegeForSchools.size());
				for(StatusPrivilegeForSchools pojo:lstStatusPrivilegeForSchools)
					chkStatusMaster[k++]=pojo.getStatusMaster().getStatusId().toString();
				headQuarterMaster.setChkStatusMaster(chkStatusMaster);
				}
				
				
				List<Integer> secStatusIds=new ArrayList<Integer>();
				List<StatusPrivilegeForSchools> lstStatusPrivilegeForSchools_forSS=statusPrivilegeForSchoolsDAO.findHqStatusForSecondaryStatusId(headQuarterMaster);
				String [] chkSecondaryStatusName=new String[lstStatusPrivilegeForSchools_forSS.size()];
				int j=0;
				if(lstStatusPrivilegeForSchools_forSS!=null && lstStatusPrivilegeForSchools_forSS.size()>0)
				{
					logger.info(" lstStatusPrivilegeForSchools_forSS :: "+lstStatusPrivilegeForSchools_forSS.size());
					for(StatusPrivilegeForSchools pojo:lstStatusPrivilegeForSchools_forSS){
						chkSecondaryStatusName[j++]=pojo.getSecondaryStatus().getSecondaryStatusId().toString();
						secStatusIds.add(pojo.getSecondaryStatus().getSecondaryStatusId());				
					}
					headQuarterMaster.setChkSecondaryStatusName(chkSecondaryStatusName);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			try {
				List<StatusPrivilegeForSchools> lstStatusPrivilegeForSchools = statusPrivilegeForSchoolsDAO.findStatusMasterForHQ(headQuarterMaster);
				String [] chkStatusMaster=new String[lstStatusPrivilegeForSchools.size()];
				int k=0;
				if(lstStatusPrivilegeForSchools!=null && lstStatusPrivilegeForSchools.size()>0)
				{
				for(StatusPrivilegeForSchools pojo:lstStatusPrivilegeForSchools)
					chkStatusMaster[k++]=pojo.getStatusMaster().getStatusId().toString();
				headQuarterMaster.setChkHQStatusMaster(chkStatusMaster);
				}
				
				
				List<Integer> secStatusIds=new ArrayList<Integer>();
				List<StatusPrivilegeForSchools> lstStatusPrivilegeForSchools_forSS=statusPrivilegeForSchoolsDAO.findSecondaryStatusForHQ(headQuarterMaster);
				String [] chkSecondaryStatusName=new String[lstStatusPrivilegeForSchools_forSS.size()];
				int j=0;
				if(lstStatusPrivilegeForSchools_forSS!=null && lstStatusPrivilegeForSchools_forSS.size()>0)
				{
					for(StatusPrivilegeForSchools pojo:lstStatusPrivilegeForSchools_forSS){
						chkSecondaryStatusName[j++]=pojo.getSecondaryStatus().getSecondaryStatusId().toString();
						secStatusIds.add(pojo.getSecondaryStatus().getSecondaryStatusId());				
					}
					headQuarterMaster.setChkHQSecondaryStatusName(chkSecondaryStatusName);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return "editheadquarter";
	}
	
	
	
	@RequestMapping(value="/editbranch.do" ,method=RequestMethod.GET)
	public String editBranch(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		logger.info(" editbranch.do........ call");
		
		try
		{
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}

			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			List<UserMaster> districtsUser	=	null;
			BranchMaster branchMaster		=	null;
			String branchId = request.getParameter("bnchId")	==	null?"0":request.getParameter("bnchId").trim();
			if(branchId.equals("0"))
			{
				branchMaster	=	new BranchMaster();
			}
			else
			{
				branchMaster	=	branchMasterDAO.findById(Integer.parseInt(branchId), false, false);
			}
			if(userSession!=null)
			{
				if(userSession.getHeadQuarterMaster()!=null)
				{
					map.put("hqId", userSession.getHeadQuarterMaster().getHeadQuarterId());
				}
				else
				{
					map.put("hqId", 0);
				}
				if(userSession.getBranchMaster()!=null)
				{
					map.put("bId", userSession.getBranchMaster().getBranchId());
				}
				else
				{
					map.put("bId", 0);
				}
			}
			if(branchMaster.getCanTMApproach()!=0){
				branchMaster.setCanTMApproach(1);
			}
		
			String roleAccess=null,roleAccessGI=null,roleAccessCI=null,roleAccessU=null,roleAccessAI=null;
			int roleId=0;
			if(userSession.getRoleId()!=null){
				roleId=userSession.getRoleId().getRoleId();
			}
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,134,"editbranch.do",100);
				roleAccessGI=roleAccessPermissionDAO.getMenuOptionList(roleId,134,"editbranch.do",100);
				roleAccessCI=roleAccessPermissionDAO.getMenuOptionList(roleId,134,"editbranch.do",100);
				roleAccessU=roleAccessPermissionDAO.getMenuOptionList(roleId,134,"editbranch.do",100);
				roleAccessAI=roleAccessPermissionDAO.getMenuOptionList(roleId,134,"editbranch.do",100);				

				logger.info(" roleAccess :: "+roleAccess);
				logger.info(" roleAccessGI :: "+roleAccessGI);
				logger.info(" roleAccessCI :: "+roleAccessCI);
				logger.info(" roleAccessU :: "+roleAccessU);
				logger.info(" roleAccessAI :: "+roleAccessAI);
				
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("roleAccessGI", roleAccessGI);
			map.addAttribute("roleAccessCI", roleAccessCI);
			map.addAttribute("roleAccessU", roleAccessU);
			map.addAttribute("roleAccessAI", roleAccessAI);
			map.addAttribute("branchMaster", branchMaster);
			
			List<ContactTypeMaster> contactTypeMaster			=	contactTypeMasterDAO.findByCriteria(Order.asc("contactType"));
			map.put("contactTypeMaster", contactTypeMaster);
			
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);
			String authKey=null;
			if(branchMaster.getAuthKey()!=null){
				authKey=Utility.decodeBase64(branchMaster.getAuthKey());
			}
			map.addAttribute("authKey",authKey);
			if(branchMaster.getDisplayTMDefaultJobCategory()!=null){
				map.addAttribute("displayTMDefaultJobCategory", branchMaster.getDisplayTMDefaultJobCategory());
			}else{
				map.addAttribute("displayTMDefaultJobCategory",true);
			}
			if(branchMaster.getContractStartDate()==null){
				map.addAttribute("contractStartDate",null);
			}else{
				map.addAttribute("contractStartDate", Utility.getCalenderDateFormart(branchMaster.getContractStartDate()+""));
			}
			
			if(branchMaster.getContractEndDate()==null){
				map.addAttribute("contractEndDate",null);
			}else{
				map.addAttribute("contractEndDate", Utility.getCalenderDateFormart(branchMaster.getContractEndDate()+""));
			}
			
			String JobBoardURL=null;
			try{
				logger.info(Utility.getBaseURL(request)+"jobsboard.do?branchId="+Utility.encryptNo(branchMaster.getBranchId()));
				JobBoardURL = Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?branchId="+Utility.encryptNo(branchMaster.getBranchId()));

			}catch (Exception e) {}
			
			map.addAttribute("JobBoardURL",JobBoardURL);
			map.addAttribute("userSession", userSession);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return "editbranch";
	}
	
	@RequestMapping(value="/editbranch.do" ,method=RequestMethod.POST)
	@Transactional
	public String editBranchPost(@ModelAttribute(value="branchMaster") BranchMaster branchMaster,BindingResult result, ModelMap map,HttpServletRequest request)
	{
		logger.info(" For save branch ");

		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		String  authKeyVal="2";
		try
		{
			userMaster = (UserMaster) session.getAttribute("userMaster");
			logger.info(branchMaster.getBranchName()+"    "+branchMaster.getZipCode()+">>>>>>>>>>>>>>>>>>>>>> :: "+branchMaster.getBranchId());
			BranchMaster bMaster = branchMasterDAO.findById(branchMaster.getBranchId(), false, false);
			logger.info(" branch Id :: "+bMaster.getBranchId());	
			
			logger.info(" bMaster.getApprovalBeforeGoLive() :: "+branchMaster.getApprovalBeforeGoLive());
			bMaster.setContactNumber1(branchMaster.getContactNumber1());
			bMaster.setContactNumber2(branchMaster.getContactNumber2());
			bMaster.setBranchEmailAddress(branchMaster.getBranchEmailAddress());
			logger.info(branchMaster.getContactNumber1()+"\t" + branchMaster.getContactNumber2()+"\t"+branchMaster.getBranchEmailAddress());
			if(branchMaster.getApprovalBeforeGoLive()!=null){
				bMaster.setApprovalBeforeGoLive(branchMaster.getApprovalBeforeGoLive());
			}
			
			logger.info(" bMaster.getBranchCode() :: "+branchMaster.getBranchCode());
			if(branchMaster.getBranchCode()!=null){
				bMaster.setBranchCode(branchMaster.getBranchCode());
			}
			
			logger.info(" bMaster.getFaxNumber() :: "+branchMaster.getFaxNumber());
			if(branchMaster.getFaxNumber()!=null){
				bMaster.setFaxNumber(branchMaster.getFaxNumber());
			}
			
			logger.info(" bMaster.getWebsiteUrl() :: "+branchMaster.getWebsiteUrl());
			if(branchMaster.getWebsiteUrl()!=null){
				bMaster.setWebsiteUrl(branchMaster.getWebsiteUrl());
			}
			// selectedDistrictUnderContract
			logger.info(" bMaster.getSelectedDistrictUnderContract() :: "+branchMaster.getSelectedDistrictUnderContract());
			if(branchMaster.getSelectedDistrictUnderContract()!=null){
				bMaster.setSelectedDistrictUnderContract(branchMaster.getSelectedDistrictUnderContract());
			}
			
			logger.info(" branchMaster.getLogoPathFile() :: "+	branchMaster.getLogoPathFile());
			if(branchMaster.getLogoPathFile().getSize()>0){
				
				if(branchMaster.getLogoPath()!=null){
					try{
						String filePath="";
						filePath=Utility.getValueOfPropByKey("branchRootPath")+branchMaster.getBranchId()+"/"+branchMaster.getLogoPath();
						File file = new File(filePath);
			    		if(file.delete()){
			    			//logger.info(file.getName() + " is deleted!");
			    		}else{
			    			//logger.info("Delete operation is failed.");
			    		}
					}catch(Exception e){
				 		e.printStackTrace();
				  	}
				}
				
				String filePath=Utility.getValueOfPropByKey("branchRootPath")+branchMaster.getBranchId()+"/";
				
				File f=new File(filePath);
				if(!f.exists())
					 f.mkdirs();
				
				FileItem fileItem =branchMaster.getLogoPathFile().getFileItem();
				String fileName="Logo"+Utility.getUploadFileName(fileItem);
				fileItem.write(new File(filePath, fileName));
				ImageResize.resizeImage(filePath+ fileName);
				bMaster.setLogoPath(fileName);
			}else{
				bMaster.setLogoPath(bMaster.getLogoPath());
			}
			String contractStartDate =null;
			String contractEndDate =null;
			logger.info(" bMaster.getNoDistrictUnderContract() :: "+branchMaster.getNoDistrictUnderContract());
			logger.info(" bMaster.getDistrictsUnderContract() :: "+branchMaster.getDistrictsUnderContract());
			logger.info(" bMaster.getAnnualSubsciptionAmount() :: "+branchMaster.getAnnualSubsciptionAmount());
			logger.info(" bMaster.getSelectedDistrictUnderContract() :: "+branchMaster.getSelectedDistrictUnderContract());
			logger.info(" bMaster.getPostingOnHQWall() :: "+branchMaster.getPostingOnHQWall());
			
			if(userMaster.getEntityType()==5){
				if(branchMaster.getNoDistrictUnderContract()!=null)
				bMaster.setNoDistrictUnderContract(branchMaster.getNoDistrictUnderContract());
				if(branchMaster.getDistrictsUnderContract()!=null)
				bMaster.setDistrictsUnderContract(branchMaster.getDistrictsUnderContract());
				if(branchMaster.getAnnualSubsciptionAmount()!=null)
				bMaster.setAnnualSubsciptionAmount(branchMaster.getAnnualSubsciptionAmount());
				if(branchMaster.getSelectedDistrictUnderContract()!=null)
				bMaster.setSelectedDistrictUnderContract(branchMaster.getSelectedDistrictUnderContract());
			}
			if(userMaster.getEntityType()==5){
				if(branchMaster.getAllDistrictsUnderContract()!=null)
				bMaster.setAllDistrictsUnderContract(branchMaster.getAllDistrictsUnderContract());
			}else{
				if(branchMaster.getAllDistrictsUnderContract()!=null)
					if(branchMaster.getAllDistrictsUnderContract()==1)
						bMaster.setAllDistrictsUnderContract(1);
					else
						bMaster.setAllDistrictsUnderContract(2);
			}
			
			if(branchMaster.getPostingOnTMWall()==null){
					bMaster.setPostingOnTMWall(0);
			}
			else
			{
				bMaster.setPostingOnTMWall(branchMaster.getPostingOnTMWall());
			}
			
			if(branchMaster.getAuthKey()==null || branchMaster.getAuthKey().trim().equals("")){				
				String authorizationkey=(Utility.randomString(8)+Utility.getDateTime());
				authKeyVal=authorizationkey;
				bMaster.setAuthKey(Utility.encodeInBase64(authorizationkey));
			}else{
				bMaster.setAuthKey(branchMaster.getAuthKey());
			}
			
			
			logger.info(" bMaster.getAllowMessageTeacher() :: "+branchMaster.getAllowMessageTeacher());
			if(branchMaster.getAllowMessageTeacher()!=null){
					bMaster.setAllowMessageTeacher(branchMaster.getAllowMessageTeacher());
					bMaster.setEmailForTeacher(branchMaster.getEmailForTeacher());
			}
			else
			{
				bMaster.setAllowMessageTeacher(0);
				bMaster.setEmailForTeacher(null);
			}
			logger.info(" bMaster.getHiringAuthority() :: "+branchMaster.getHiringAuthority());
			if(branchMaster.getHiringAuthority()!=null)
				bMaster.setHiringAuthority(branchMaster.getHiringAuthority());
			
			boolean offerDistrictSpecificItems=Boolean.valueOf(request.getParameter("offerDistrictSpecificItems"));
			boolean offerQualificationItems=Boolean.valueOf(request.getParameter("offerQualificationItems"));
			boolean offerEPI=Boolean.valueOf(request.getParameter("offerEPI"));
			boolean offerJSI=Boolean.valueOf(request.getParameter("offerJSI"));
			boolean offerPortfolioNeeded=Boolean.valueOf(request.getParameter("offerPortfolioNeeded"));
			
			logger.info(" bMaster.getOfferDistrictSpecificItems() :: "+branchMaster.getOfferDistrictSpecificItems());
			logger.info(" bMaster.getOfferQualificationItems() :: "+branchMaster.getOfferQualificationItems());
			logger.info(" bMaster.getOfferEPI() :: "+branchMaster.getOfferEPI());
			logger.info(" bMaster.getOfferJSI() :: "+branchMaster.getOfferJSI());
			logger.info(" bMaster.getOfferPortfolioNeeded() :: "+branchMaster.getOfferPortfolioNeeded());
			bMaster.setOfferDistrictSpecificItems(offerDistrictSpecificItems);
			bMaster.setOfferQualificationItems(offerQualificationItems);
			bMaster.setOfferEPI(offerEPI);
			bMaster.setOfferJSI(offerJSI);
			bMaster.setOfferPortfolioNeeded(offerPortfolioNeeded);
			
			
			logger.info(" branchMaster.getAddress() :: "+branchMaster.getAddress());
			logger.info(" branchMaster.getZipCode() :: "+branchMaster.getZipCode());
			logger.info(" branchMaster.getCityName() :: "+branchMaster.getStateMaster());
			logger.info(" branchMaster.getAddress() :: "+branchMaster.getCityName());
			logger.info(" branchMaster.getPhoneNumber() :: "+branchMaster.getPhoneNumber());
			logger.info(" branchMaster.getTotalNoOfDistricts() :: "+branchMaster.getTotalNoOfDistricts());
			logger.info(" branchMaster.getCreatedDateTime() :: "+branchMaster.getCreatedDateTime());
			logger.info(" branchMaster.getStatus() :: "+branchMaster.getStatus());
			if(branchMaster.getLocationCode()!=null)
				bMaster.setLocationCode(branchMaster.getLocationCode());
			if(branchMaster.getBranchName()!=null)
				bMaster.setBranchName(branchMaster.getBranchName());
			if(branchMaster.getAddress()!=null)
				bMaster.setAddress(branchMaster.getAddress());
			if(branchMaster.getZipCode()!=null)
				bMaster.setZipCode(branchMaster.getZipCode());
			if(branchMaster.getStateMaster()!=null)
				bMaster.setStateMaster(branchMaster.getStateMaster());
			if(branchMaster.getCityName()!=null)
				bMaster.setCityName(branchMaster.getCityName());
			if(branchMaster.getPhoneNumber()!=null)
				bMaster.setPhoneNumber(branchMaster.getPhoneNumber());
			if(branchMaster.getTotalNoOfDistricts()!=null)
				bMaster.setTotalNoOfDistricts(branchMaster.getTotalNoOfDistricts());
			if(branchMaster.getCreatedDateTime()!=null)
				bMaster.setCreatedDateTime(branchMaster.getCreatedDateTime());
			if(branchMaster.getStatus()!=null)
			bMaster.setStatus(branchMaster.getStatus());
			if(branchMaster.getCompletionMessage()!=null)
				bMaster.setCompletionMessage(branchMaster.getCompletionMessage().trim());
			
			logger.info(" branchMaster.getIsPortfolioNeeded() :: "+branchMaster.getIsPortfolioNeeded());
			logger.info(" branchMaster.getIsWeeklyCgReport() :: "+branchMaster.getIsWeeklyCgReport());
			if(userMaster.getEntityType()==1){
				if(branchMaster.getIsWeeklyCgReport()==null){
					bMaster.setIsWeeklyCgReport(0);
				}
			}else{
				if(branchMaster.getIsPortfolioNeeded()!=null)
				bMaster.setIsPortfolioNeeded(branchMaster.getIsPortfolioNeeded());
				else
					bMaster.setIsPortfolioNeeded(false);
				
				if(branchMaster.getIsWeeklyCgReport()!=null)
					bMaster.setIsWeeklyCgReport(branchMaster.getIsWeeklyCgReport());
				else
					bMaster.setIsWeeklyCgReport(0);
			}
			
			logger.info(" branchMaster.getDisplayCGPA() :: "+branchMaster.getDisplayCGPA());
			if(branchMaster.getDisplayCGPA()!=null)
				bMaster.setDisplayCGPA(branchMaster.getDisplayCGPA());
			
			logger.info(" branchMaster.getDisplayDemoClass() :: "+branchMaster.getDisplayDemoClass());
			if(branchMaster.getDisplayDemoClass()!=null)
				bMaster.setDisplayDemoClass(branchMaster.getDisplayDemoClass());
			
			logger.info(" branchMaster.getDisplayExpectedSalary() :: "+branchMaster.getDisplayExpectedSalary());
			if(branchMaster.getDisplayExpectedSalary()!=null)
				bMaster.setDisplayExpectedSalary(branchMaster.getDisplayExpectedSalary());
			
			logger.info(" branchMaster.getDisplayFitScore() :: "+branchMaster.getDisplayFitScore());
			if(branchMaster.getDisplayFitScore()!=null)
				bMaster.setDisplayFitScore(branchMaster.getDisplayFitScore());
			
			logger.info(" branchMaster.getDisplayJSI() :: "+branchMaster.getDisplayJSI());
			if(branchMaster.getDisplayJSI()!=null)
				bMaster.setDisplayJSI(branchMaster.getDisplayJSI());
			
			logger.info(" branchMaster.getDisplayPhoneInterview() :: "+branchMaster.getDisplayPhoneInterview());
			if(branchMaster.getDisplayPhoneInterview()!=null)
				bMaster.setDisplayPhoneInterview(branchMaster.getDisplayPhoneInterview());
			
			logger.info(" branchMaster.getDisplayTFA() :: "+branchMaster.getDisplayTFA());
			if(branchMaster.getDisplayTFA()!=null)
				bMaster.setDisplayTFA(branchMaster.getDisplayTFA());
			
			logger.info(" branchMaster.getDisplayYearsTeaching() :: "+branchMaster.getDisplayYearsTeaching());
			if(branchMaster.getDisplayYearsTeaching()!=null)
				bMaster.setDisplayYearsTeaching(branchMaster.getDisplayYearsTeaching());
			
			logger.info(" branchMaster.getDisplayAchievementScore() :: "+branchMaster.getDisplayAchievementScore());
			if(branchMaster.getDisplayAchievementScore()!=null)
				bMaster.setDisplayAchievementScore(branchMaster.getDisplayAchievementScore());
			
			logger.info(" branchMaster.getDistributionEmail() :: "+branchMaster.getDistributionEmail());
			if(branchMaster.getDistributionEmail()!=null)
				bMaster.setDistributionEmail(branchMaster.getDistributionEmail());
			
			logger.info(" branchMaster.getCanTMApproach() :: "+branchMaster.getCanTMApproach());
			if(branchMaster.getCanTMApproach()!=null)
				bMaster.setCanTMApproach(branchMaster.getCanTMApproach());
			else
				bMaster.setCanTMApproach(0);
			
			
			try {
				branchMasterDAO.makePersistent(bMaster);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			try
			{
				userMaster.setBranchMaster(bMaster);
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Edited Branch");
				if(userMaster.getEntityType()==5)
					userMaster.setBranchMaster(null);
				logger.info(" userMaster :::::: "+userMaster.getBranchMaster());
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return "redirect:managebranches.do?authKeyVal="+authKeyVal;
	}
	public SecondaryStatus getSecondaryStatus(List<SecondaryStatus> secondaryStatusList,StatusNodeMaster statusNodeMaster)
	{
		SecondaryStatus secondaryStatusObj= new SecondaryStatus();
		try {
				if(secondaryStatusList.size()>0)
				for(SecondaryStatus secondaryStatus: secondaryStatusList){
					if(secondaryStatus.getStatusNodeMaster().getStatusNodeId().equals(statusNodeMaster.getStatusNodeId())){
						secondaryStatusObj=secondaryStatus;
					}
				}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return secondaryStatusObj;
	}
	
	@Transactional
	public int createDistrictStatusHeadQuarterHome(HttpServletRequest request,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
	{
		HttpSession session = request.getSession();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		SecondaryStatus secondaryStatus = new SecondaryStatus();
		List<SecondaryStatus> lstIsStatusHomeCreated=	null;
		try 
		{
			Criterion criterion1 = null;
			if(branchMaster!=null)
				criterion1 =	Restrictions.eq("branchMaster", branchMaster);
			else
				criterion1 =	Restrictions.eq("headQuarterMaster", headQuarterMaster);
			
			lstIsStatusHomeCreated				=	secondaryStatusDAO.findByCriteria(criterion1);
			if(lstIsStatusHomeCreated.size()==0){
				
				SessionFactory sessionFactory=secondaryStatusDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				Transaction txOpen =statelesSsession.beginTransaction();
				List<StatusNodeMaster> statusNodeMasterList= new ArrayList<StatusNodeMaster>();
				statusNodeMasterList=statusNodeMasterDAO.findStatusNodeList();
				
				List<StatusMaster> statusMasterList= new ArrayList<StatusMaster>();
				statusMasterList=statusMasterDAO.findStatusNodeList();
				
				for(StatusNodeMaster statusNodeMaster :statusNodeMasterList){
					secondaryStatus=new SecondaryStatus();
					secondaryStatus.setUsermaster(userSession);
					secondaryStatus.setHeadQuarterMaster(headQuarterMaster);
					if(branchMaster!=null)
						secondaryStatus.setBranchMaster(branchMaster);
					secondaryStatus.setDistrictMaster(null);
					secondaryStatus.setSecondaryStatusName(statusNodeMaster.getStatusNodeName());
					secondaryStatus.setOrderNumber(statusNodeMaster.getOrderNumber());
					secondaryStatus.setStatus("A");
					secondaryStatus.setStatusNodeMaster(statusNodeMaster);
					secondaryStatus.setCreatedDateTime(new Date());
					statelesSsession.insert(secondaryStatus);
				}
				txOpen.commit();
		       	statelesSsession.close(); 
		       	
				statelesSsession = sessionFactory.openStatelessSession();
				txOpen =statelesSsession.beginTransaction();
		      
		    	List<SecondaryStatus>	secondaryStatusList=	secondaryStatusDAO.findByCriteria(criterion1);
				SecondaryStatus secondaryStatusObj=new SecondaryStatus();
				for(StatusMaster statusMaster :statusMasterList){
					secondaryStatusObj=getSecondaryStatus(secondaryStatusList,statusMaster.getStatusNodeMaster());
					secondaryStatus=new SecondaryStatus();
					secondaryStatus.setUsermaster(userSession);
					secondaryStatus.setHeadQuarterMaster(headQuarterMaster);
					if(branchMaster!=null)
						secondaryStatus.setBranchMaster(branchMaster);
					secondaryStatus.setDistrictMaster(null);
					secondaryStatus.setSecondaryStatusName(statusMaster.getStatus());
					secondaryStatus.setOrderNumber(statusMaster.getOrderNumber());
					secondaryStatus.setStatus("A");
					secondaryStatus.setStatusMaster(statusMaster);
					secondaryStatus.setSecondaryStatus(secondaryStatusObj);
					secondaryStatus.setCreatedDateTime(new Date());
					statelesSsession.insert(secondaryStatus);
				}
				txOpen.commit();
		       	statelesSsession.close(); 
				 return 2;
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return 1;
	}
	
	
	@RequestMapping(value="/editheadquarter.do" ,method=RequestMethod.POST)
	@Transactional
	public String editHeadQuarterPost(@ModelAttribute(value="headQuarterMaster") HeadQuarterMaster headQuarterMaster,BindingResult result, ModelMap map,HttpServletRequest request)
	{
		logger.info(" For save headQuarter ");

		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		String  authKeyVal="2";
		try
		{			
			userMaster = (UserMaster) session.getAttribute("userMaster");
			logger.info(headQuarterMaster.getHeadQuarterName()+"    "+headQuarterMaster.getZipCode()+">>>>>>>>>>>>>>>>>>>>>> :: "+headQuarterMaster.getHeadQuarterId());
			HeadQuarterMaster hqMaster = headQuarterMasterDAO.findById(headQuarterMaster.getHeadQuarterId(), false, false);
			createDistrictStatusHeadQuarterHome(request,hqMaster,null);
			logger.info(" headQuarter Id :: "+hqMaster.getHeadQuarterId());	
			
			if(headQuarterMaster.getExitURL()!=null)
			{
				hqMaster.setExitURL(headQuarterMaster.getExitURL());
			}
			if(headQuarterMaster.getCompletionMessage()!=null)
			{
				hqMaster.setCompletionMessage(headQuarterMaster.getCompletionMessage());
			}
			try{
				if(hqMaster.getExclusivePeriod()==null || hqMaster.getExclusivePeriod()==0){
					List<ExclusivePeriodMaster> lstExclusivePeriodMaster = exclusivePeriodMasterDAO.findAll();
					ExclusivePeriodMaster exclusivePeriodMaster = lstExclusivePeriodMaster.get(1);
					hqMaster.setExclusivePeriod(exclusivePeriodMaster.getExclPeriod());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(headQuarterMaster.getSendReminderToIcompTalent()!=null)
			{
				if(headQuarterMaster.getNoOfReminder()!=null)
				{
					hqMaster.setNoOfReminder(headQuarterMaster.getNoOfReminder());
				}
				if(headQuarterMaster.getReminderFrequencyInDays()!=null)
				{
					hqMaster.setReminderFrequencyInDays(headQuarterMaster.getReminderFrequencyInDays());
				}
				if(headQuarterMaster.getReminderOfFirstFrequencyInDays()!=null)
				{
					hqMaster.setReminderOfFirstFrequencyInDays(headQuarterMaster.getReminderOfFirstFrequencyInDays());
				}
				
				if(request.getParameter("statusIdReminderExpireAction")!=null && !request.getParameter("statusIdReminderExpireAction").trim().equals(""))
				{
					StatusMaster statusMaster=new StatusMaster();
					statusMaster.setStatusId(Integer.parseInt(request.getParameter("statusIdReminderExpireAction")));
					hqMaster.setStatusIdReminderExpireAction(statusMaster);
				}
				else
					hqMaster.setStatusIdReminderExpireAction(null);
			}
			else
			{
				hqMaster.setNoOfReminder(null);
				hqMaster.setReminderFrequencyInDays(null);
				hqMaster.setReminderOfFirstFrequencyInDays(null);
				hqMaster.setStatusIdReminderExpireAction(null);
			}
			
			
			if(headQuarterMaster.getPostingOnHQWall()!=null){
				hqMaster.setPostingOnHQWall(headQuarterMaster.getPostingOnHQWall());
			}
			else{
				hqMaster.setPostingOnHQWall(0);
			}
			
			if(headQuarterMaster.getCommunicationsAccess()!=null){
				hqMaster.setCommunicationsAccess(headQuarterMaster.getCommunicationsAccess());
			}
			else{
				hqMaster.setCommunicationsAccess(0);
			}
			
			if(headQuarterMaster.getStatusNotes()!=null){
				hqMaster.setStatusNotes(headQuarterMaster.getStatusNotes());
			}
			
			if(headQuarterMaster.getSendReminderToIcompTalent()!=null){
				hqMaster.setSendReminderToIcompTalent(headQuarterMaster.getSendReminderToIcompTalent());
			}
			else{
				hqMaster.setSendReminderToIcompTalent(0);
			}
			
			if(headQuarterMaster.getSendReferenceOnJobComplete()!=null){
				hqMaster.setSendReferenceOnJobComplete(headQuarterMaster.getSendReferenceOnJobComplete());
			}
			else{
				hqMaster.setSendReferenceOnJobComplete(0);
			}
			
			if(headQuarterMaster.getWebsiteUrl()!=null){
				hqMaster.setWebsiteUrl(headQuarterMaster.getWebsiteUrl());
			}
			
			if(headQuarterMaster.getPostingOnTMWall()!=null){
				hqMaster.setPostingOnTMWall(headQuarterMaster.getPostingOnTMWall());
			}
			else{
				hqMaster.setPostingOnTMWall(0);
			}
			
			if(headQuarterMaster.getApprovalBeforeGoLive()!=null){
				hqMaster.setApprovalBeforeGoLive(headQuarterMaster.getApprovalBeforeGoLive());
			}
			
			if(headQuarterMaster.getHeadQuarterCode()!=null){
				hqMaster.setHeadQuarterCode(headQuarterMaster.getHeadQuarterCode());
			}
			
			if(headQuarterMaster.getFaxNumber()!=null){
				hqMaster.setFaxNumber(headQuarterMaster.getFaxNumber());
			}
			
			if(headQuarterMaster.getWebsiteUrl()!=null){
				hqMaster.setWebsiteUrl(headQuarterMaster.getWebsiteUrl());
			}
			
			if(headQuarterMaster.getWritePrivilegeToBranch()!=null){
				hqMaster.setWritePrivilegeToBranch(headQuarterMaster.getWritePrivilegeToBranch());
			}
						
			if(headQuarterMaster.getResetQualificationIssuesPrivilegeToBranch()!=null){
				hqMaster.setResetQualificationIssuesPrivilegeToBranch(headQuarterMaster.getResetQualificationIssuesPrivilegeToBranch());
			}
			else{
				hqMaster.setResetQualificationIssuesPrivilegeToBranch(false);
			}
			
			if(headQuarterMaster.getLogoPathFile().getSize()>0){
				
				if(headQuarterMaster.getLogoPath()!=null){
					try{
						String filePath="";
						filePath=Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterMaster.getHeadQuarterId()+"/"+headQuarterMaster.getLogoPath();
						File file = new File(filePath);
			    		if(file.delete()){
			    			//logger.info(file.getName() + " is deleted!");
			    		}else{
			    			//logger.info("Delete operation is failed.");
			    		}
					}catch(Exception e){
				 		e.printStackTrace();
				  	}
				}
				
				String filePath=Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterMaster.getHeadQuarterId()+"/";
				
				File f=new File(filePath);
				if(!f.exists())
					 f.mkdirs();
				
				FileItem fileItem =headQuarterMaster.getLogoPathFile().getFileItem();
				String fileName="Logo"+Utility.getUploadFileName(fileItem);
				fileItem.write(new File(filePath, fileName));
				ImageResize.resizeImage(filePath+ fileName);
				hqMaster.setLogoPath(fileName);
			}else{
				hqMaster.setLogoPath(hqMaster.getLogoPath());
			}

			
			try {
				SessionFactory sessionFactory = assessmentDetailDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				Transaction txOpen =statelesSsession.beginTransaction();
				
				int cbxUploadAssessment=0;
				if(request.getParameter("cbxUploadAssessment")!=null){
					if(request.getParameter("cbxUploadAssessment").equals("on")){
						cbxUploadAssessment=1;
					}else{
						cbxUploadAssessment=0;
					}
				}else if(request.getParameter("aJobRelation")!=null && !request.getParameter("aJobRelation").equals("")){
					cbxUploadAssessment=1;
				}
				AssessmentDetail assessmentDetail = null;
				
				if(headQuarterMaster.getAssessmentUploadURLFile().getSize()>0 && cbxUploadAssessment==1){
					if(headQuarterMaster.getAssessmentUploadURL()!=null){
						try{
							String filePath="";
							filePath=Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterMaster.getHeadQuarterId()+"/"+headQuarterMaster.getAssessmentUploadURL();
							File file = new File(filePath);
				    		if(file.delete()){
				    			//logger.info(file.getName() + " is deleted!");
				    		}else{
				    			//logger.info("Delete operation is failed.");
				    		}
						}catch(Exception e){
					 		e.printStackTrace();
					  	}
					}
					
					String filePath=Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterMaster.getHeadQuarterId()+"/";
					
					File f=new File(filePath);
					if(!f.exists())
						 f.mkdirs();
					
					FileItem fileItem =headQuarterMaster.getAssessmentUploadURLFile().getFileItem();
					String fileName="Inventory"+Utility.getUploadFileName(fileItem);
					fileItem.write(new File(filePath, fileName));
					hqMaster.setAssessmentUploadURL(fileName);
					
				}else{
					if(cbxUploadAssessment==1){
						logger.info(" headQuarterMaster.getAssessmentUploadURL() ::: "+headQuarterMaster.getAssessmentUploadURL());
						hqMaster.setAssessmentUploadURL(headQuarterMaster.getAssessmentUploadURL());
					}else{
						hqMaster.setAssessmentUploadURL(null);
					}
				}
				
				// Start JSI 
				
				List<AssessmentDetail> assessmentDetails = assessmentDetailDAO.findAssessmentByHeadQuarter(headQuarterMaster);
				logger.info("assessmentDetails "+assessmentDetails.size());
				if(cbxUploadAssessment==1)
				{
					logger.info(" assessmentDetails insert ");
					
					if(assessmentDetails==null || assessmentDetails.size()==0)
					{
						assessmentDetail = new AssessmentDetail();
						assessmentDetail.setHeadQuarterMaster(headQuarterMaster);
						assessmentDetail.setAssessmentName(headQuarterMaster.getHeadQuarterName()+" JSI");
						assessmentDetail.setJobOrderType(5);
						assessmentDetail.setAssessmentType(2);
						assessmentDetail.setIsDefault(true);
						assessmentDetail.setIsResearchEPI(false);
						assessmentDetail.setQuestionRandomization(false);
						assessmentDetail.setOptionRandomization(false);
						assessmentDetail.setStatus("A");
						assessmentDetail.setAssessmentSessionTime(600);
						assessmentDetail.setCreatedDateTime(new Date());
						assessmentDetail.setUserMaster(userMaster);
						assessmentDetail.setIpaddress(request.getRemoteAddr());
						
						assessmentDetailDAO.makePersistent(assessmentDetail);
					}
					else if(assessmentDetails!=null && assessmentDetails.size()>0)
					{
						assessmentDetail =assessmentDetails.get(0);
						assessmentDetail.setIsDefault(true);
						txOpen =statelesSsession.beginTransaction();
						//assessmentDetailDAO.makePersistent(assessmentDetail);
						statelesSsession.update(assessmentDetail);
						txOpen.commit();
					}
				}
				else if(cbxUploadAssessment==0)
				{
					logger.info(" assessmentDetails update ");

					if(assessmentDetails!=null && assessmentDetails.size()>0)
					{
						txOpen =statelesSsession.beginTransaction();
						assessmentDetail = assessmentDetails.get(0);
						logger.info(" assessmentDetail :: "+assessmentDetail.getIsDefault());
						assessmentDetail.setIsDefault(false);						
						statelesSsession.update(assessmentDetail);
						txOpen.commit();
						logger.info(" assessmentDetail :: "+assessmentDetail.getIsDefault());
					}
				}
				
				statelesSsession.close();

				// End JSI
				
			} catch (Exception e2) {
				hqMaster.setAssessmentUploadURL(headQuarterMaster.getAssessmentUploadURL());
				hqMaster.setLogoPath(headQuarterMaster.getLogoPath());
				e2.printStackTrace();
			}
		
		
			if(userMaster.getEntityType()==5){
				if(headQuarterMaster.getNoBranchUnderContract()!=null)
					hqMaster.setNoBranchUnderContract(headQuarterMaster.getNoBranchUnderContract());
				if(headQuarterMaster.getDistrictsUnderContract()!=null)
					hqMaster.setDistrictsUnderContract(headQuarterMaster.getDistrictsUnderContract());
				if(headQuarterMaster.getAnnualSubsciptionAmount()!=null)
					hqMaster.setAnnualSubsciptionAmount(headQuarterMaster.getAnnualSubsciptionAmount());
				if(headQuarterMaster.getSelectedBranchesUnderContract()!=null)
					hqMaster.setSelectedBranchesUnderContract(headQuarterMaster.getSelectedBranchesUnderContract());
			}
			if(userMaster.getEntityType()==5){
				if(headQuarterMaster.getAllBranchesUnderContract()!=null)
					hqMaster.setAllBranchesUnderContract(headQuarterMaster.getAllBranchesUnderContract());
			}else{
				if(headQuarterMaster.getAllBranchesUnderContract()!=null)
					if(headQuarterMaster.getAllBranchesUnderContract()==1)
						hqMaster.setAllBranchesUnderContract(1);
					else
						hqMaster.setAllBranchesUnderContract(2);
			}
						
			if(userMaster.getEntityType()==5){
				if(headQuarterMaster.getNoDistrictUnderContract()!=null)
					hqMaster.setNoDistrictUnderContract(headQuarterMaster.getNoDistrictUnderContract());
				if(headQuarterMaster.getSelectedDistrictsUnderContract()!=null)
					hqMaster.setSelectedDistrictsUnderContract(headQuarterMaster.getSelectedDistrictsUnderContract());
			}
			if(userMaster.getEntityType()==5){
				if(headQuarterMaster.getAllDistrictsUnderContract()!=null)
					hqMaster.setAllDistrictsUnderContract(headQuarterMaster.getAllDistrictsUnderContract());
			}else{
				if(headQuarterMaster.getAllDistrictsUnderContract()!=null)
					if(headQuarterMaster.getAllDistrictsUnderContract()==1)
						hqMaster.setAllDistrictsUnderContract(1);
					else
						hqMaster.setAllDistrictsUnderContract(2);
			}
			
			if(headQuarterMaster.getPostingOnTMWall()==null){
				hqMaster.setPostingOnTMWall(0);
			}
			else
			{
				hqMaster.setPostingOnTMWall(headQuarterMaster.getPostingOnTMWall());
			}
			
			if(hqMaster.getAuthKey()==null || hqMaster.getAuthKey().trim().equals("")){				
				String authorizationkey=(Utility.randomString(8)+Utility.getDateTime());
				authKeyVal=authorizationkey;
				hqMaster.setAuthKey(Utility.encodeInBase64(authorizationkey));
			}else{
				hqMaster.setAuthKey(hqMaster.getAuthKey());
			}
			
			if(headQuarterMaster.getAllowMessageTeacher()!=null){
				hqMaster.setAllowMessageTeacher(headQuarterMaster.getAllowMessageTeacher());
				hqMaster.setEmailForTeacher(headQuarterMaster.getEmailForTeacher());
			}
			else
			{
				hqMaster.setAllowMessageTeacher(0);
				hqMaster.setEmailForTeacher(null);
			}

			if(headQuarterMaster.getHiringAuthority()!=null)
				hqMaster.setHiringAuthority(headQuarterMaster.getHiringAuthority());
			
			boolean offerDistrictSpecificItems=Boolean.valueOf(request.getParameter("offerDistrictSpecificItems"));
			boolean offerQualificationItems=Boolean.valueOf(request.getParameter("offerQualificationItems"));
			boolean offerEPI=Boolean.valueOf(request.getParameter("offerEPI"));
			boolean offerJSI=Boolean.valueOf(request.getParameter("offerJSI"));
			boolean offerPortfolioNeeded=Boolean.valueOf(request.getParameter("offerPortfolioNeeded"));
			
			hqMaster.setOfferDistrictSpecificItems(offerDistrictSpecificItems);
			hqMaster.setOfferQualificationItems(offerQualificationItems);
			hqMaster.setOfferEPI(offerEPI);
			hqMaster.setOfferJSI(offerJSI);
			hqMaster.setOfferPortfolioNeeded(offerPortfolioNeeded);

			
			if(headQuarterMaster.getLocationCode()!=null)
				hqMaster.setLocationCode(headQuarterMaster.getLocationCode());
			if(headQuarterMaster.getHeadQuarterName()!=null)
				hqMaster.setHeadQuarterName(headQuarterMaster.getHeadQuarterName());
			if(headQuarterMaster.getAddress()!=null)
				hqMaster.setAddress(headQuarterMaster.getAddress());
			if(headQuarterMaster.getZipCode()!=null)
				hqMaster.setZipCode(headQuarterMaster.getZipCode());
			if(headQuarterMaster.getStateId()!=null)
				hqMaster.setStateId(headQuarterMaster.getStateId());
			if(headQuarterMaster.getCityName()!=null)
				hqMaster.setCityName(headQuarterMaster.getCityName());
			if(headQuarterMaster.getPhoneNumber()!=null)
				hqMaster.setPhoneNumber(headQuarterMaster.getPhoneNumber());
			if(headQuarterMaster.getTotalNoOfBranches()!=null)
				hqMaster.setTotalNoOfBranches(headQuarterMaster.getTotalNoOfBranches());
			if(headQuarterMaster.getCreatedDateTime()!=null)
				hqMaster.setCreatedDateTime(headQuarterMaster.getCreatedDateTime());
			if(headQuarterMaster.getStatus()!=null)
				hqMaster.setStatus(headQuarterMaster.getStatus());
			
			if(userMaster.getEntityType()==1){
				if(headQuarterMaster.getIsWeeklyCgReport()==null){
					hqMaster.setIsWeeklyCgReport(0);
				}
			}else{
				if(headQuarterMaster.getIsPortfolioNeeded()!=null)
					hqMaster.setIsPortfolioNeeded(headQuarterMaster.getIsPortfolioNeeded());
				else
					hqMaster.setIsPortfolioNeeded(false);
				
				if(headQuarterMaster.getIsWeeklyCgReport()!=null)
					hqMaster.setIsWeeklyCgReport(headQuarterMaster.getIsWeeklyCgReport());
				else
					hqMaster.setIsWeeklyCgReport(0);
			}
			
			if(headQuarterMaster.getDisplayCGPA()!=null)
				hqMaster.setDisplayCGPA(headQuarterMaster.getDisplayCGPA());
			
			if(headQuarterMaster.getDisplayDemoClass()!=null)
				hqMaster.setDisplayDemoClass(headQuarterMaster.getDisplayDemoClass());
			
			if(headQuarterMaster.getDisplayExpectedSalary()!=null)
				hqMaster.setDisplayExpectedSalary(headQuarterMaster.getDisplayExpectedSalary());
			
			if(headQuarterMaster.getDisplayFitScore()!=null)
				hqMaster.setDisplayFitScore(headQuarterMaster.getDisplayFitScore());
			
			if(headQuarterMaster.getDisplayJSI()!=null)
				hqMaster.setDisplayJSI(headQuarterMaster.getDisplayJSI());
			
			if(headQuarterMaster.getDisplayPhoneInterview()!=null)
				hqMaster.setDisplayPhoneInterview(headQuarterMaster.getDisplayPhoneInterview());
			
			if(headQuarterMaster.getDisplayTFA()!=null)
				hqMaster.setDisplayTFA(headQuarterMaster.getDisplayTFA());
			
			if(headQuarterMaster.getDisplayYearsTeaching()!=null)
				hqMaster.setDisplayYearsTeaching(headQuarterMaster.getDisplayYearsTeaching());
			
			if(headQuarterMaster.getDisplayAchievementScore()!=null)
				hqMaster.setDisplayAchievementScore(headQuarterMaster.getDisplayAchievementScore());
			
			if(headQuarterMaster.getDistributionEmail()!=null)
				hqMaster.setDistributionEmail(headQuarterMaster.getDistributionEmail());
			
			if(headQuarterMaster.getCanTMApproach()!=null)
				hqMaster.setCanTMApproach(headQuarterMaster.getCanTMApproach());
			else
				hqMaster.setCanTMApproach(0);
			
			if(headQuarterMaster.getDescription()!=null)
				hqMaster.setDescription(headQuarterMaster.getDescription());
			
			if(headQuarterMaster.getDmName()!=null)
				hqMaster.setDmName(headQuarterMaster.getDmName());
			
			if(headQuarterMaster.getDmEmailAddress()!=null)
				hqMaster.setDmEmailAddress(headQuarterMaster.getDmEmailAddress());
			
			if(headQuarterMaster.getDmPhoneNumber()!=null)
				hqMaster.setDmPhoneNumber(headQuarterMaster.getDmPhoneNumber());
			
			if(headQuarterMaster.getDmPassword()!=null)
				hqMaster.setDmPassword(headQuarterMaster.getDmPassword());
			
			if(headQuarterMaster.getAcName()!=null)
				hqMaster.setAcName(headQuarterMaster.getAcName());
			
			if(headQuarterMaster.getAcEmailAddress()!=null)
				hqMaster.setAcEmailAddress(headQuarterMaster.getAcEmailAddress());
			
			if(headQuarterMaster.getAcPhoneNumber()!=null)
				hqMaster.setAcPhoneNumber(headQuarterMaster.getAcPhoneNumber());
			
			if(headQuarterMaster.getAmName()!=null)
				hqMaster.setAmName(headQuarterMaster.getAmName());
			
			if(headQuarterMaster.getAmEmailAddress()!=null)
				hqMaster.setAmEmailAddress(headQuarterMaster.getAmEmailAddress());
			
			if(headQuarterMaster.getAmPhoneNumber()!=null)
				hqMaster.setAmPhoneNumber(headQuarterMaster.getAmPhoneNumber());
			
			if(headQuarterMaster.getNoEPI()!=null)
				hqMaster.setNoEPI(headQuarterMaster.getNoEPI());
			
			if(headQuarterMaster.getIsTeacherPoolOnLoad()!=null)
				hqMaster.setIsTeacherPoolOnLoad(headQuarterMaster.getIsTeacherPoolOnLoad());
			
			logger.info(" headQuarterMaster.getDisplayTMDefaultJobCategory() :: "+headQuarterMaster.getDisplayTMDefaultJobCategory());
			if(headQuarterMaster.getDisplayTMDefaultJobCategory()!=null)
				hqMaster.setDisplayTMDefaultJobCategory(headQuarterMaster.getDisplayTMDefaultJobCategory());
			
			if(headQuarterMaster.getOfferVirtualVideoInterview()!=null && headQuarterMaster.getOfferVirtualVideoInterview())
			{
				hqMaster.setOfferVirtualVideoInterview(headQuarterMaster.getOfferVirtualVideoInterview());
				logger.info(" headQuarterMaster.getMaxScoreForVVI() ::"+headQuarterMaster.getMaxScoreForVVI());
				if(headQuarterMaster.getMaxScoreForVVI()!=null)
					hqMaster.setMaxScoreForVVI(headQuarterMaster.getMaxScoreForVVI());
				if(headQuarterMaster.getSendAutoVVILink()!=null && headQuarterMaster.getSendAutoVVILink())
				{
					hqMaster.setSendAutoVVILink(headQuarterMaster.getSendAutoVVILink());
					if(headQuarterMaster.getTimeAllowedPerQuestion()!=null)
						hqMaster.setTimeAllowedPerQuestion(headQuarterMaster.getTimeAllowedPerQuestion());
					if(headQuarterMaster.getVVIExpiresInDays()!=null)
						hqMaster.setVVIExpiresInDays(headQuarterMaster.getVVIExpiresInDays());
				}
				else
				{
					hqMaster.setSendAutoVVILink(headQuarterMaster.getSendAutoVVILink());
				}
			}
			else
			{
				hqMaster.setOfferVirtualVideoInterview(headQuarterMaster.getOfferVirtualVideoInterview());
			}
			
			if(headQuarterMaster.getSetAssociatedStatusToSetDPoints()!=null && headQuarterMaster.getSetAssociatedStatusToSetDPoints())
				hqMaster.setSetAssociatedStatusToSetDPoints(headQuarterMaster.getSetAssociatedStatusToSetDPoints());
			else
				hqMaster.setSetAssociatedStatusToSetDPoints(false);
			
			if(headQuarterMaster.getAutoNotifyOnStatusChange()!=null)
				hqMaster.setAutoNotifyOnStatusChange(headQuarterMaster.getAutoNotifyOnStatusChange());
			
			List<SecondaryStatus> lstsecondaryStatus=secondaryStatusDAO.findSecondaryStatusOnlyForHeadQuarter(headQuarterMaster);
            logger.info("lstsecondaryStatus:::::::::::::"+lstsecondaryStatus.size());
            String[] statusShortName = {"hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
            List<StatusMaster> lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statusShortName);
			
			boolean deletedData=false;
			List<StatusWiseAutoEmailSend> allData = null;
			try {
				allData   = statusWiseAutoEmailSendDAO.findAllRecordsForHeadQuarter(headQuarterMaster);
			} catch (Exception e2) {
				e2.printStackTrace();
			}	
            
            logger.info(" deletedData ::: "+deletedData);
			
			//if(headQuarterMaster.getStatusPrivilegeForBranches()!=null){
            Map<String, Boolean> mapAutoNotifyStatus=new HashMap<String, Boolean>();
            Map<String, Boolean> mapAutoNotifySecStatus=new HashMap<String, Boolean>();
            
            String [] chkStatusMasterEml=headQuarterMaster.getChkStatusMasterEml();
            String [] chkSecondaryStatusNameEml=headQuarterMaster.getChkSecondaryStatusNameEml();
            
            if(lstStatusMaster.size()>0)
				for(StatusMaster pojo:lstStatusMaster)
					mapAutoNotifyStatus.put(""+pojo.getStatusId(), new Boolean(false));
			
			for(int i=0; i<chkStatusMasterEml.length; i++)
				if(mapAutoNotifyStatus.get(chkStatusMasterEml[i])!=null)
					mapAutoNotifyStatus.put(chkStatusMasterEml[i], new Boolean(true));
            
			if(lstsecondaryStatus!=null && lstsecondaryStatus.size()>0)
			{
				for(SecondaryStatus pojo:lstsecondaryStatus)
					mapAutoNotifySecStatus.put(""+pojo.getSecondaryStatusId(), new Boolean(false));
			}
			if(chkSecondaryStatusNameEml!=null && chkSecondaryStatusNameEml.length>0)
			{
				for(int i=0; i<chkSecondaryStatusNameEml.length; i++)
					if(mapAutoNotifySecStatus.get(chkSecondaryStatusNameEml[i])!=null)
						mapAutoNotifySecStatus.put(chkSecondaryStatusNameEml[i], new Boolean(true));
			}
			
            
				if(headQuarterMaster.getAutoNotifyOnStatusChange()!=null){		

					SessionFactory sessionFactory1 = statusWiseAutoEmailSendDAO.getSessionFactory();
					StatelessSession statelesSsession1 = sessionFactory1.openStatelessSession();
					Transaction txOpen1 =statelesSsession1.beginTransaction();
					
					Iterator iterStatus = mapAutoNotifyStatus.entrySet().iterator();
					Iterator iterSecStaus = mapAutoNotifySecStatus.entrySet().iterator();
					
					if(allData.size()==0){

						while(iterStatus.hasNext()){
							StatusWiseAutoEmailSend stAutoEmail =new StatusWiseAutoEmailSend();
							Map.Entry mEntry = (Map.Entry) iterStatus.next();
							if(mEntry.getValue().equals(new Boolean(true))){
								int statusMasterId=Integer.parseInt(mEntry.getKey().toString());
								stAutoEmail.setHeadQuarterMaster(headQuarterMaster);
								stAutoEmail.setDistrictId(null);
								stAutoEmail.setStatusId(statusMasterId);
								stAutoEmail.setAutoEmailRequired(1);
								stAutoEmail.setUsermaster(userMaster);
								stAutoEmail.setCreatedDateTime(new Date());
								statelesSsession1.insert(stAutoEmail);
							}
						}

						while(iterSecStaus.hasNext()){
							StatusWiseAutoEmailSend stAutoEmail =new StatusWiseAutoEmailSend();
							Map.Entry mEntry = (Map.Entry) iterSecStaus.next();
							if(mEntry.getValue().equals(new Boolean(true))){
								int secStatusMasterId=Integer.parseInt(mEntry.getKey().toString());
								stAutoEmail.setHeadQuarterMaster(headQuarterMaster);
								stAutoEmail.setDistrictId(null);
								stAutoEmail.setSecondaryStatusId(secStatusMasterId);
								stAutoEmail.setAutoEmailRequired(1);
								stAutoEmail.setUsermaster(userMaster);
								stAutoEmail.setCreatedDateTime(new Date());
								statelesSsession1.insert(stAutoEmail);
							}
						}
					}
					else{
						Boolean isFound = false;
						Integer autoNotify = 0;
						while(iterStatus.hasNext()){
							autoNotify = 0;
							isFound = false;
							StatusWiseAutoEmailSend stAutoEmail = null;
							Map.Entry mEntry = (Map.Entry) iterStatus.next();
							int statusMasterId=Integer.parseInt(mEntry.getKey().toString());
							if(mEntry.getValue().equals(new Boolean(true)))
								autoNotify=1;
							for(StatusWiseAutoEmailSend spfs : allData){ 
								if(spfs.getStatusId()!=null && spfs.getStatusId().equals(Integer.parseInt(mEntry.getKey().toString()))){
									isFound = true;
									stAutoEmail = spfs;
									break;
								}
							}
							if(isFound){
								stAutoEmail.setAutoEmailRequired(autoNotify);
								statelesSsession1.update(stAutoEmail);
							}
							else{
								stAutoEmail = new StatusWiseAutoEmailSend();
								stAutoEmail.setAutoEmailRequired(autoNotify);
								stAutoEmail.setHeadQuarterMaster(headQuarterMaster);
								stAutoEmail.setCreatedDateTime(new Date());
								stAutoEmail.setStatusId(statusMasterId);
								stAutoEmail.setSecondaryStatusId(null);
								stAutoEmail.setBranchMaster(null);
								stAutoEmail.setDistrictId(null);
								stAutoEmail.setUsermaster(userMaster);
								statelesSsession1.insert(stAutoEmail);
							}
						}
						
						while(iterSecStaus.hasNext()){
							isFound = false;
							autoNotify = 0;
							StatusWiseAutoEmailSend stAutoEmail = null;							
							Map.Entry mEntry = (Map.Entry) iterSecStaus.next();
							int secStatusMasterId=Integer.parseInt(mEntry.getKey().toString());
							if(mEntry.getValue().equals(new Boolean(true)))
								autoNotify=1;
							for(StatusWiseAutoEmailSend spfs : allData){ 
								if(spfs.getSecondaryStatusId()!=null && spfs.getSecondaryStatusId().equals(Integer.parseInt(mEntry.getKey().toString()))){
									isFound = true;
									stAutoEmail = spfs;
								}
							}
							if(isFound){
								stAutoEmail.setAutoEmailRequired(autoNotify);
								statelesSsession1.update(stAutoEmail);
							}
							else{
								stAutoEmail = new StatusWiseAutoEmailSend();
								stAutoEmail.setAutoEmailRequired(autoNotify);
								stAutoEmail.setHeadQuarterMaster(headQuarterMaster);
								stAutoEmail.setCreatedDateTime(new Date());
								stAutoEmail.setStatusId(null);
								stAutoEmail.setSecondaryStatusId(secStatusMasterId);
								stAutoEmail.setBranchMaster(null);
								stAutoEmail.setDistrictId(null);
								stAutoEmail.setUsermaster(userMaster);
								statelesSsession1.insert(stAutoEmail);
							}
						}
					}
					
					txOpen1.commit();
					statelesSsession1.close();
				}	
			
				StatusPrivilegeForSchools statusPrivilegeForSchools = new StatusPrivilegeForSchools();

	            Map<String, Boolean> mapstaus=new HashMap<String, Boolean>();
	            Map<String, Boolean> mapSstaus=new HashMap<String, Boolean>();
	            
	            Map<String, Boolean> maphqstatus=new HashMap<String, Boolean>();
	            Map<String, Boolean> maphqSstaus=new HashMap<String, Boolean>();
	            
	            /* for branch privileges */
	            String [] chkStatusMaster=headQuarterMaster.getChkStatusMaster();
	            String [] chkSecondaryStatusName=headQuarterMaster.getChkSecondaryStatusName();
	            /* for headquarter privileges */
	            String [] chkHQStatusMaster=headQuarterMaster.getChkHQStatusMaster();
	            String [] chkHQSecondaryStatusName=headQuarterMaster.getChkHQSecondaryStatusName();
			
            List lstStatusPrivilegeForHQForSM = statusPrivilegeForSchoolsDAO.findStatusPrivilegeForStatusMasterForHQ(headQuarterMaster.getHeadQuarterId());
			List lstStatusPrivilegeForHQForSS = statusPrivilegeForSchoolsDAO.findStatusPrivilegeForSecondaryStatusIdForHQ(headQuarterMaster.getHeadQuarterId());
			
						
			// *************** Status Master *********************
			// for branch
			if(lstStatusMaster.size()>0)
				for(StatusMaster pojo:lstStatusMaster)
					mapstaus.put(""+pojo.getStatusId(), new Boolean(false));
			
			for(int i=0; i<chkStatusMaster.length; i++)
				if(mapstaus.get(chkStatusMaster[i])!=null)
					mapstaus.put(chkStatusMaster[i], new Boolean(true));
			
			// for headquarter
			if(lstStatusMaster.size()>0)
				for(StatusMaster pojo:lstStatusMaster)
					maphqstatus.put(""+pojo.getStatusId(), new Boolean(false));
			
			for(int i=0; i<chkHQStatusMaster.length; i++)
				if(maphqstatus.get(chkHQStatusMaster[i])!=null)
					maphqstatus.put(chkHQStatusMaster[i], new Boolean(true));
			
			logger.info(" mapstaus ::::::::::: "+mapstaus.size());
			logger.info(" maphqstatus ::::::::::: "+maphqstatus.size());
			
			// *************** Secondary Status Master *********************
			// for branch
			if(lstsecondaryStatus!=null && lstsecondaryStatus.size()>0)
			{
				for(SecondaryStatus pojo:lstsecondaryStatus)
					mapSstaus.put(""+pojo.getSecondaryStatusId(), new Boolean(false));
			}
			if(chkSecondaryStatusName!=null && chkSecondaryStatusName.length>0)
			{
				for(int i=0; i<chkSecondaryStatusName.length; i++)
					if(mapSstaus.get(chkSecondaryStatusName[i])!=null)
						mapSstaus.put(chkSecondaryStatusName[i], new Boolean(true));
			}
			
			// for headquarter
			if(lstsecondaryStatus!=null && lstsecondaryStatus.size()>0)
			{
				for(SecondaryStatus pojo:lstsecondaryStatus)
					maphqSstaus.put(""+pojo.getSecondaryStatusId(), new Boolean(false));
			}
			if(chkHQSecondaryStatusName!=null && chkHQSecondaryStatusName.length>0)
			{
				for(int i=0; i<chkHQSecondaryStatusName.length; i++)
					if(maphqSstaus.get(chkHQSecondaryStatusName[i])!=null)
						maphqSstaus.put(chkHQSecondaryStatusName[i], new Boolean(true));
			}
			
			logger.info(" mapSstaus ::::::::::::: "+mapSstaus.size());
			logger.info(" maphqSstaus ::::::::::::: "+maphqSstaus.size());
			
			SessionFactory sessionFactory=statusPrivilegeForSchoolsDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			Transaction txOpen =statelesSsession.beginTransaction();
			
			if(headQuarterMaster.getStatusPrivilegeForHeadquarter()!=null){	
				hqMaster.setStatusPrivilegeForHeadquarter(headQuarterMaster.getStatusPrivilegeForHeadquarter());
			}
			
			if(headQuarterMaster.getStatusPrivilegeForBranches()!=null)
			{
				hqMaster.setStatusPrivilegeForBranches(headQuarterMaster.getStatusPrivilegeForBranches());
			}
			
					if(lstStatusPrivilegeForHQForSM.size()==0){
						List<StatusPrivilegeForSchools> tempStatusPrivilegeForSchools = new ArrayList<StatusPrivilegeForSchools>();
						Iterator iter = maphqstatus.entrySet().iterator();
						Iterator iterBr = mapstaus.entrySet().iterator();
						
						StatusMaster master = null;
						int count = 0;
						
						while (iter.hasNext())  // for hq
						{
							int counter = 0;
							statusPrivilegeForSchools = new StatusPrivilegeForSchools();
								
							Map.Entry mEntry = (Map.Entry) iter.next();
								int statusMasterId=Integer.parseInt(mEntry.getKey().toString());
								master = new StatusMaster();
								master.setStatusId(statusMasterId);
								if(mEntry.getValue().equals(new Boolean(true))){
									statusPrivilegeForSchools.setCanHQSetStatus(1);
								}
								else{
									statusPrivilegeForSchools.setCanHQSetStatus(0);
								}
							statusPrivilegeForSchools.setCanBranchSetStatus(0);
							statusPrivilegeForSchools.setSecondaryStatus(null);
							statusPrivilegeForSchools.setStatusMaster(master);
							statusPrivilegeForSchools.setHeadQuarterMaster(headQuarterMaster);
							statusPrivilegeForSchools.setCreatedDateTime(new Date());
							statusPrivilegeForSchools.setUserMaster(userMaster);
							tempStatusPrivilegeForSchools.add(statusPrivilegeForSchools);
						}
						
						while(iterBr.hasNext()){
							Map.Entry mEntryBr = (Map.Entry) iterBr.next();
							for(StatusPrivilegeForSchools spfs : tempStatusPrivilegeForSchools){ 
								if(spfs.getStatusMaster().getStatusId().equals(Integer.parseInt(mEntryBr.getKey().toString()))){
									if(mEntryBr.getValue().equals(new Boolean(true))){
										spfs.setCanBranchSetStatus(1);
									}
									else{
										spfs.setCanBranchSetStatus(0);
									}
									statelesSsession.insert(spfs);
								}
							}
						}
					}
					else{
						Iterator iter = maphqstatus.entrySet().iterator();
						Iterator iterBr = mapstaus.entrySet().iterator();
						Boolean isFound = false;
						Boolean isFoundBr = false;
						StatusMaster master= null;
						Integer icanBrSetStatus=0;
						Integer icanHQSetStatus=0;
						List<StatusPrivilegeForSchools> tempStatusPrivilegeForSchools = new ArrayList<StatusPrivilegeForSchools>();
							while (iter.hasNext()) 
							{
								icanHQSetStatus=0;
								statusPrivilegeForSchools = null;
								Map.Entry mEntry = (Map.Entry) iter.next();
								if(mEntry.getValue().equals(new Boolean(true)))
									icanHQSetStatus=1;
								int statusMasterId=Integer.parseInt(mEntry.getKey().toString());
								master = new StatusMaster();
								master.setStatusId(statusMasterId);
								
								Iterator it=lstStatusPrivilegeForHQForSM.iterator();
								 
								while(it.hasNext())
								{
									Object[] ob = (Object[])it.next();
									if(Integer.parseInt(""+ob[0])==statusMasterId){
										statusPrivilegeForSchools = new StatusPrivilegeForSchools();
										statusPrivilegeForSchools.setStatusPrivilegeId(Integer.parseInt(""+ob[1]));
										isFound = true;
									}
								}
								
								if(isFound){
									statusPrivilegeForSchools.setIsNew(false);
								}
								else{
									statusPrivilegeForSchools = new StatusPrivilegeForSchools();
									statusPrivilegeForSchools.setIsNew(true);
								}
								statusPrivilegeForSchools.setCanHQSetStatus(icanHQSetStatus);
								statusPrivilegeForSchools.setSecondaryStatus(null);
								statusPrivilegeForSchools.setStatusMaster(master);
								statusPrivilegeForSchools.setHeadQuarterMaster(headQuarterMaster);
								statusPrivilegeForSchools.setCreatedDateTime(new Date());
								statusPrivilegeForSchools.setUserMaster(userMaster);
								statusPrivilegeForSchools.setCanBranchSetStatus(icanBrSetStatus);
								
								tempStatusPrivilegeForSchools.add(statusPrivilegeForSchools);
							}

							while(iterBr.hasNext()){
									icanBrSetStatus=0;
								Map.Entry mEntryBr = (Map.Entry) iterBr.next();
								if(mEntryBr.getValue().equals(new Boolean(true)))
									icanBrSetStatus=1;
								int statusMasterIdBr=Integer.parseInt(mEntryBr.getKey().toString());

								for(StatusPrivilegeForSchools spfs : tempStatusPrivilegeForSchools){
									if(spfs.getStatusMaster().getStatusId().equals(Integer.parseInt(mEntryBr.getKey().toString()))){

										if(mEntryBr.getValue().equals(new Boolean(true))){
											spfs.setCanBranchSetStatus(icanBrSetStatus);
										}
										else{
											spfs.setCanBranchSetStatus(icanBrSetStatus);
										}

										if(spfs.getIsNew())
											statelesSsession.insert(spfs);
										else
											statelesSsession.update(spfs);
									}
								}
							}
					}
					
					
					if(lstStatusPrivilegeForHQForSS.size()==0){

						List<StatusPrivilegeForSchools> tempStatusPrivilegeForSchools = new ArrayList<StatusPrivilegeForSchools>();
						Iterator iter = maphqSstaus.entrySet().iterator();
						Iterator iterBr = mapSstaus.entrySet().iterator();
						SecondaryStatus master = null;
						int count = 0;
						while (iter.hasNext())  // for hq
						{
							int counter = 0;
							statusPrivilegeForSchools = new StatusPrivilegeForSchools();

							Map.Entry mEntry = (Map.Entry) iter.next();
							int secStatusMasterId=Integer.parseInt(mEntry.getKey().toString());
							master = new SecondaryStatus();
							master.setSecondaryStatusId(secStatusMasterId);
							if(mEntry.getValue().equals(new Boolean(true))){
								statusPrivilegeForSchools.setCanHQSetStatus(1);
							}
							else{
								statusPrivilegeForSchools.setCanHQSetStatus(0);
							}
							statusPrivilegeForSchools.setCanBranchSetStatus(0);
							statusPrivilegeForSchools.setSecondaryStatus(master);
							statusPrivilegeForSchools.setStatusMaster(null);
							statusPrivilegeForSchools.setHeadQuarterMaster(headQuarterMaster);
							statusPrivilegeForSchools.setCreatedDateTime(new Date());
							statusPrivilegeForSchools.setUserMaster(userMaster);
							tempStatusPrivilegeForSchools.add(statusPrivilegeForSchools);
						}

						while(iterBr.hasNext()){
							Map.Entry mEntryBr = (Map.Entry) iterBr.next();
							for(StatusPrivilegeForSchools spfs : tempStatusPrivilegeForSchools){ 
								if(spfs.getSecondaryStatus().getSecondaryStatusId().equals(Integer.parseInt(mEntryBr.getKey().toString()))){
									if(mEntryBr.getValue().equals(new Boolean(true))){
										spfs.setCanBranchSetStatus(1);
									}
									else{
										spfs.setCanBranchSetStatus(0);
									}
									statelesSsession.insert(spfs);
								}
							}
						}
					}
					else{
						Iterator iter = maphqSstaus.entrySet().iterator();
						Iterator iterBr = mapSstaus.entrySet().iterator();
						Boolean isFound = false;
						Boolean isFoundBr = false;
						SecondaryStatus master = null;
						Integer icanBrSetStatus=0;
						Integer icanHQSetStatus=0;
						List<StatusPrivilegeForSchools> tempStatusPrivilegeForSchools = new ArrayList<StatusPrivilegeForSchools>();
						while (iter.hasNext()) 
						{
							icanHQSetStatus=0;
							statusPrivilegeForSchools = null;
							Map.Entry mEntry = (Map.Entry) iter.next();
							if(mEntry.getValue().equals(new Boolean(true)))
								icanHQSetStatus=1;
							int secStatusId=Integer.parseInt(mEntry.getKey().toString());
							master = new SecondaryStatus();
							master.setSecondaryStatusId(secStatusId);
							
							Iterator it=lstStatusPrivilegeForHQForSS.iterator();
							 
							while(it.hasNext())
							{
								Object[] ob = (Object[])it.next();
								if(Integer.parseInt(""+ob[0])==secStatusId){
									statusPrivilegeForSchools = new StatusPrivilegeForSchools();
									statusPrivilegeForSchools.setStatusPrivilegeId(Integer.parseInt(""+ob[1]));
									isFound = true;
								}
							}
							
							if(isFound){
								statusPrivilegeForSchools.setCanHQSetStatus(icanHQSetStatus);
								statusPrivilegeForSchools.setIsNew(false);
							}
							else{
								statusPrivilegeForSchools = new StatusPrivilegeForSchools();
								statusPrivilegeForSchools.setIsNew(true);
							}
							statusPrivilegeForSchools.setCanHQSetStatus(icanHQSetStatus);
							statusPrivilegeForSchools.setSecondaryStatus(master);
							statusPrivilegeForSchools.setStatusMaster(null);
							statusPrivilegeForSchools.setHeadQuarterMaster(headQuarterMaster);
							statusPrivilegeForSchools.setCreatedDateTime(new Date());
							statusPrivilegeForSchools.setUserMaster(userMaster);
							statusPrivilegeForSchools.setCanBranchSetStatus(icanBrSetStatus);
							
							tempStatusPrivilegeForSchools.add(statusPrivilegeForSchools);
						}

						while(iterBr.hasNext()){
							icanBrSetStatus=0;
							Map.Entry mEntryBr = (Map.Entry) iterBr.next();
							if(mEntryBr.getValue().equals(new Boolean(true)))
								icanBrSetStatus=1;
							int statusMasterIdBr=Integer.parseInt(mEntryBr.getKey().toString());

							for(StatusPrivilegeForSchools spfs : tempStatusPrivilegeForSchools){
								if(spfs.getSecondaryStatus()!=null && spfs.getSecondaryStatus().getSecondaryStatusId().equals(Integer.parseInt(mEntryBr.getKey().toString()))){
									if(mEntryBr.getValue().equals(new Boolean(true))){
										spfs.setCanBranchSetStatus(icanBrSetStatus);
									}
									else{
										spfs.setCanBranchSetStatus(icanBrSetStatus);
									}

									if(spfs.getIsNew())
										statelesSsession.insert(spfs);
									else
										statelesSsession.update(spfs);
								}
							}
						}
					}
					
			
			try{
				
				/* 	e-Reference Set Status Anurag 28 Aug 15 */
				try
				{

					StatusMaster statusMasterForReference=new StatusMaster();
					SecondaryStatus secondaryStatusForReference=new SecondaryStatus();
					Integer statusIdForeReferenceFinalize			=	null;
					Integer secondaryStatusIdForeReferenceFinalize	=	null;
					String statusListBoxForReminder=request.getParameter("statusListBoxForReminder");
					if(statusListBoxForReminder!=null && !statusListBoxForReminder.equalsIgnoreCase(""))
					{
						if(statusListBoxForReminder.contains("SSIDForReminder_"))
						{
							secondaryStatusIdForeReferenceFinalize=new Integer(statusListBoxForReminder.substring(16));
							secondaryStatusForReference=secondaryStatusDAO.findById(secondaryStatusIdForeReferenceFinalize, false, false);
							if(secondaryStatusForReference!=null)
							{
								hqMaster.setSecondaryStatusForReference(secondaryStatusForReference);
							}
							hqMaster.setStatusMasterForReference(null);//------------------------------------------
						}
						else
						{
							statusIdForeReferenceFinalize=new Integer(statusListBoxForReminder);
							statusMasterForReference=WorkThreadServlet.statusIdMap.get(statusIdForeReferenceFinalize);
							if(statusMasterForReference!=null)
							{
								hqMaster.setStatusMasterForReference(statusMasterForReference);
							}
							hqMaster.setSecondaryStatusForReference(null);//===================================================
						}
					}
					else
					{
						hqMaster.setStatusMasterForReference(null);
						hqMaster.setSecondaryStatusForReference(null);
					}
				}catch(Exception e)
				{
					e.printStackTrace();
				}

				/*	END Section */
				txOpen.commit();
				statelesSsession.close();
				
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			
			try {
				if(headQuarterMaster.getJobCompletionNeeded()!=null)
				hqMaster.setJobCompletionNeeded(headQuarterMaster.getJobCompletionNeeded());
				headQuarterMasterDAO.makePersistent(hqMaster);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			try
			{
				userMaster.setHeadQuarterMaster(hqMaster);
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Edited HeadQuarter");
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return "redirect:manageheadquarter.do?authKeyVal="+authKeyVal;
	}
	
	@RequestMapping(value="/addbranch.do" ,method=RequestMethod.GET)
	public String addBranch(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		logger.info("addbranch.do call");
		//request.getSession();
		try 
		{
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster	userMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				logger.info(" email Address :: "+userMaster.getEmailAddress());
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
					
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,137,"addbranch.do",100);
				logger.info(" roleAccess :: "+roleAccess);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			
			List<TimeZoneMaster> timeZones = null;
			try {
				timeZones = timeZoneMasterDAO.getAllTimeZone();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			
			map.addAttribute("timeZones", timeZones);
			try{
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in add branch");
			}catch(Exception e){
				e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "addbranch";
	}
	
	@RequestMapping(value="/headquarterjobsboard.do", method=RequestMethod.GET)
	public String doSearchJob(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{		
		request.getSession();
		Utility.setRefererURLByJobsBoard(request);
		HeadQuarterMaster headQuarterMaster = null;
		BranchMaster branchMaster = null;
		DistrictMaster districtMaster = null;
		HttpSession session = request.getSession();	
		try 
		{	
			String source = "";
			String target = "";
			String path = "";

			String headQuarterId = "";
			String branchId = "";
			
			try {							
				headQuarterId = request.getParameter("hqId")==null?"":request.getParameter("hqId");
				headQuarterId = ""+ Utility.decryptNo(Integer.parseInt(headQuarterId));
			} catch (Exception e) {
			}
			
			try {							
				branchId = request.getParameter("branchId")==null?"":request.getParameter("branchId");
				branchId = ""+ Utility.decryptNo(Integer.parseInt(branchId));
			} catch (Exception e) {
			}

			session.removeAttribute("headQuarterMaster");
			session.removeAttribute("branchMaster");
				
			 if(!headQuarterId.equals("")){
				try {
					headQuarterMaster = headQuarterMasterDAO.findById(new Integer(headQuarterId), false, false);
					if(headQuarterMaster!=null){

						map.addAttribute("headQuarterMaster", headQuarterMaster);
						session.setAttribute("headQuarterMaster", headQuarterMaster);					

						source = Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterMaster.getHeadQuarterId()+"/"+headQuarterMaster.getLogoPath();
						target = servletContext.getRealPath("/")+"headquarter/"+headQuarterMaster.getHeadQuarterId()+"/";
						logger.info(" target ::: "+target);

						File sourceFile = new File(source);
						File targetDir = new File(target);
						if(!targetDir.exists())
							targetDir.mkdirs();

						File targetFile = new File(targetDir+"/"+sourceFile.getName());

						if(sourceFile.exists())
						{
							FileUtils.copyFile(sourceFile, targetFile);
							path = Utility.getValueOfPropByKey("contextBasePath")+"/headQuarter/"+headQuarterMaster.getHeadQuarterId()+"/"+sourceFile.getName();
						}

						map.addAttribute("logoPath", path);					
					}
					else{
						//response.sendRedirect("signin.do");
					}	
				} 
				catch (Exception e) {
				}
			}
			else if(!branchId.equals("")){
				try {
					branchMaster = branchMasterDAO.findById(new Integer(branchId), false, false);
					if(branchMaster!=null){

						map.addAttribute("branchMaster", branchMaster);
						session.setAttribute("branchMaster", branchMaster);					

						source = Utility.getValueOfPropByKey("branchRootPath")+branchMaster.getBranchId()+"/"+branchMaster.getLogoPath();
						target = servletContext.getRealPath("/")+"branch/"+branchMaster.getBranchId()+"/";
						logger.info(" target ::: "+target);

						File sourceFile = new File(source);
						File targetDir = new File(target);
						if(!targetDir.exists())
							targetDir.mkdirs();

						File targetFile = new File(targetDir+"/"+sourceFile.getName());

						if(sourceFile.exists())
						{
							FileUtils.copyFile(sourceFile, targetFile);
							path = Utility.getValueOfPropByKey("contextBasePath")+"/branch/"+branchMaster.getBranchId()+"/"+sourceFile.getName();
						}

						map.addAttribute("logoPath", path);					
					}
					else{
						//response.sendRedirect("signin.do");
					}	
				} 
				catch (Exception e) {
				}
			}
			if(session.getAttribute("logoPath")!=null && (!session.getAttribute("logoPath").equals("")))
				path=(String)session.getAttribute("logoPath");

			if(!Utility.existsURL(path))
			{
				path="images/applyfor-job.png";
				map.addAttribute("logoPath", path);
			}
			
			List<JobCategoryMaster> jobCategoryMasterlst = null;
			if(headQuarterMaster!=null)
			{
				jobCategoryMasterlst = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(headQuarterMaster, branchMaster, districtMaster);
			}
			map.addAttribute("jobCategoryMasterlst",jobCategoryMasterlst);	

			// @ASHish :: List of subject
			/*List<SubjectMaster> lstSubjectMasters =null;
			if(districtMaster!=null)
			{
				lstSubjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(districtMaster);
			}*/

			// @AShish :: List of State

			String Name="";
			String districtAddress="";
			String address="";
			String state="";
			String city = "";
			String zipcode="";
			String pnNo="";

						
			if(headQuarterMaster!=null){
				if(headQuarterMaster.getHeadQuarterName()!=null && !headQuarterMaster.getHeadQuarterName().equals("")){
					Name=headQuarterMaster.getHeadQuarterName();
				}else{
					Name=headQuarterMaster.getHeadQuarterName();
				}
			}
			
			if(branchMaster!=null){
				if(branchMaster.getBranchName()!=null && !branchMaster.getBranchName().equals("")){
					Name=branchMaster.getBranchName();
				}else{
					Name=branchMaster.getBranchName();
				}
			}

			/* @Start
			 * @Ankit Sharma
			 * @Description :: HeadQuarter Address*/
			if(headQuarterMaster!=null)
			{
				if(headQuarterMaster.getAddress()!=null && !headQuarterMaster.getAddress().equals(""))
				{
					if(!headQuarterMaster.getCityName().equals(""))
					{
						districtAddress = headQuarterMaster.getAddress()+", ";
					}
					else
					{
						districtAddress = headQuarterMaster.getAddress();
					}
				}
				if(!headQuarterMaster.getCityName().equals(""))
				{
					if(headQuarterMaster.getStateId()!=null && !headQuarterMaster.getStateId().getStateName().equals(""))
					{
						city = headQuarterMaster.getCityName()+", ";
					}else{
						city = headQuarterMaster.getCityName();
					}
				}
				if(headQuarterMaster.getStateId()!=null && !headQuarterMaster.getStateId().getStateName().equals(""))
				{
					if(!headQuarterMaster.getZipCode().equals(""))
					{
						state = headQuarterMaster.getStateId().getStateName()+", ";
					}
					else
					{
						state = headQuarterMaster.getStateId().getStateName();
					}	
				}
				if(!headQuarterMaster.getZipCode().equals(""))
				{
					if(headQuarterMaster.getPhoneNumber()!=null && !headQuarterMaster.getPhoneNumber().equals(""))
					{
						zipcode = headQuarterMaster.getZipCode()+", Phone #: ";
					}
					else
					{
						zipcode = headQuarterMaster.getZipCode();
					}
				}

				if(headQuarterMaster.getPhoneNumber()!=null && !headQuarterMaster.getPhoneNumber().equals("")){
					pnNo = headQuarterMaster.getPhoneNumber();
				}

				if(districtAddress !="" || city!="" ||state!="" || zipcode!="" || pnNo!=""){
					address = districtAddress+city+state+zipcode+pnNo;
				}else{
					address="";
				}
			}
			/* @End
			 * @Ankit Sharma
			 * @Description :: HeadQuarter Address*/
			
			
			/* @Start
			 * @Ankit Sharma
			 * @Description :: Branch Address*/
			if(branchMaster!=null)
			{
				if(branchMaster.getAddress()!=null && !branchMaster.getAddress().equals(""))
				{
					if(!branchMaster.getCityName().equals(""))
					{
						districtAddress = branchMaster.getAddress()+", ";
					}
					else
					{
						districtAddress = branchMaster.getAddress();
					}
				}
				if(!branchMaster.getCityName().equals(""))
				{
					if(branchMaster.getStateMaster()!=null && !branchMaster.getStateMaster().getStateName().equals(""))
					{
						city = branchMaster.getCityName()+", ";
					}else{
						city = branchMaster.getCityName();
					}
				}
				if(branchMaster.getStateMaster()!=null && !branchMaster.getStateMaster().getStateName().equals(""))
				{
					if(!branchMaster.getZipCode().equals(""))
					{
						state = branchMaster.getStateMaster().getStateName()+", ";
					}
					else
					{
						state = headQuarterMaster.getStateId().getStateName();
					}	
				}
				if(!branchMaster.getZipCode().equals(""))
				{
					if(branchMaster.getPhoneNumber()!=null && !branchMaster.getPhoneNumber().equals(""))
					{
						zipcode = branchMaster.getZipCode()+", Phone #: ";
					}
					else
					{
						zipcode = branchMaster.getZipCode();
					}
				}

				if(branchMaster.getPhoneNumber()!=null && !branchMaster.getPhoneNumber().equals("")){
					pnNo = branchMaster.getPhoneNumber();
				}

				if(districtAddress !="" || city!="" ||state!="" || zipcode!="" || pnNo!=""){
					address = districtAddress+city+state+zipcode+pnNo;
				}else{
					address="";
				}
			}
			/* @End
			 * @Ankit Sharma
			 * @Description :: Branch Address*/
			
			map.addAttribute("address", address);
			map.addAttribute("Name", Name);
			//map.addAttribute("lstJobCategoryMasters", jobCategoryMasterlst);
			//map.addAttribute("lstSubjectMasters", lstSubjectMasters);
			//	map.addAttribute("lstStateMasters", lstStateMasters);

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		String paramText="";
		if(headQuarterMaster==null && branchMaster==null){
			map.addAttribute("redirectTo", "1");			
		}
		return "headquarterjobsboard";
	}
	
	
	@RequestMapping(value="/gethqsearchjob.do", method=RequestMethod.POST)
	public String doSearchHQJobFilter(ModelMap modelmap,HttpServletRequest request, HttpServletResponse response)
	{	
		logger.info("======== doSearchHQJobFilter ========");
		StringBuffer sb = new StringBuffer();
		request.getSession();
		try
		{
			
			String headQuarterId =  request.getParameter("headQuarterId")==null?"0":request.getParameter("headQuarterId").trim();
			String branchId =  request.getParameter("branchId")==null?"0":request.getParameter("branchId").trim();
			String districtId =  request.getParameter("districtId")==null?"0":request.getParameter("districtId").trim();
			String jobCategoryId =  request.getParameter("jobCategoryId")==null?"0":request.getParameter("jobCategoryId").trim();
			String zipCode =  request.getParameter("zipCode")==null?"0":request.getParameter("zipCode").trim();

			String noOfRow			=	request.getParameter("noOfRows");
			String pageNo			=	request.getParameter("page");
			String sortOrder		=	request.getParameter("sortOrderStr");
			String sortOrderType	=	request.getParameter("sortOrderType");
			String subjectId		=   request.getParameter("subjectIdList");
			String pageName			=	request.getParameter("pageName");

			//-- get no of record in grid,
			//-- set start and end position

			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			//	int start 			= 	0;
			//	int end 			= 	1000;
			int totalRecord 	=	0;
			//------------------------------------

			
			ArrayList<Integer> subjectIdList=  new ArrayList<Integer>();
			List<SubjectMaster> ObjSubjectList = new ArrayList<SubjectMaster>();	
			List<String> zipCOdeList	=	new ArrayList<String>();
			List<JobOrder> finalJobOrderList = new ArrayList<JobOrder>();
			
			try
			{
				if(pageName.equalsIgnoreCase("internaljobsboard.do")){
					finalJobOrderList = jobOrderDAO.findAllJobForInternalJobBoard();
				}else{
					finalJobOrderList = jobOrderDAO.findAllJobWithPoolCondition();
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		
			SortedMap map = new TreeMap();
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"jobId";
			String sortOrderNoField		=	"jobId";

			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("jobId")&& !sortOrder.equals("jobTitle")&& !sortOrder.equals("schoolName") && !sortOrder.equals("location") && !sortOrder.equals("geoZoneName") && !sortOrder.equals("subjectName") && !sortOrder.equals("jobEndDate")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("jobId"))
				{
					sortOrderNoField="jobId";
				}
				if(sortOrder.equals("jobTitle"))
				{
					sortOrderNoField="jobTitle";
				}
				if(sortOrder.equals("districtName"))
				{
					sortOrderNoField="districtName";
				}
				if(sortOrder.equals("jobEndDate"))
				{
					sortOrderNoField="jobEndDate";
				}				
			}	
	
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}		
			/**End ------------------------------------**/
			
		
			
			PrintWriter out = response.getWriter();
			List<JobOrder> lstJobOrder = null;
			DistrictMaster districtMaster = null;
			boolean isAllCategory = false;
			JobCategoryMaster jobCategoryMaster = null;
			if(jobCategoryId==null || jobCategoryId.equals("") || jobCategoryId.equals("0")){
				isAllCategory = true;				
			} 
			else{
				jobCategoryMaster = jobCategoryMasterDAO.findById(new Integer(jobCategoryId), false, false);
			}

			if((headQuarterId.equals("0") || headQuarterId.equals("")) && (branchId.equals("0") || branchId.equals("")) && (districtId.equals("0")||districtId.equals("")) && ((zipCode.equals("0")) || (zipCode.equals("")))){

				if(isAllCategory)
					lstJobOrder = jobOrderDAO.findSortedJobtoShow(sortOrderStrVal);
				else				
					lstJobOrder = jobOrderDAO.findSortedJobbyJobCategory(sortOrderStrVal,jobCategoryMaster);
			}
			else if(!zipCode.equals("0") && !zipCode.equals("") && (zipCOdeList==null || zipCOdeList.size()==0))
			{
				
			}

			List<JobOrder> sortedlstJobOrder		=	new ArrayList<JobOrder>();
			SortedMap<String,JobOrder>	sortedMap 	= 	new TreeMap<String,JobOrder>();			
			if(sortOrderNoField.equals("jobId"))
			{
				sortOrderFieldName	=	"jobId";
			}
			if(sortOrderNoField.equals("jobTitle"))
			{
				sortOrderFieldName	=	"jobTitle";
			}						
			if(sortOrderNoField.equals("jobEndDate"))
			{
				sortOrderFieldName	=	"jobEndDate";
			}		
	
			int mapFlag=2;
			for (JobOrder jobOrder : finalJobOrderList){
				String orderFieldName=jobOrder.getStatus();
				
				if(sortOrderFieldName.equals("jobTitle")){
					orderFieldName=jobOrder.getJobTitle().toUpperCase()+"||"+jobOrder.getJobId();
					sortedMap.put(orderFieldName+"||",jobOrder);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
				
				if(jobOrder.getJobEndDate()!=null){
					orderFieldName=Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobEndDate());
					if(sortOrderFieldName.equals("jobEndDate")){
						orderFieldName=jobOrder.getJobEndDate()+"||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}else{
					if(sortOrderFieldName.equals("jobEndDate")){
						orderFieldName="0||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
			}
			
			
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstJobOrder.add((JobOrder) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstJobOrder.add((JobOrder) sortedMap.get(key));
				}
			}else{
				sortedlstJobOrder=finalJobOrderList;
			}
			
			totalRecord =sortedlstJobOrder.size();
			
			if(totalRecord<end)
				end=totalRecord;


			List<JobOrder> lstsortedJobOrder =	sortedlstJobOrder.subList(start,end);
						
			sb.append("<table width='100%' border='0' id='tblGrid'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");		
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("Job ID",sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");			

			responseText=PaginationAndSorting.responseSortingLink("Title",sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");			

			responseText=PaginationAndSorting.responseSortingLink("Zone",sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");			

			responseText=PaginationAndSorting.responseSortingLink("Subject",sortOrderFieldName,"subjectName",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>"); 

			responseText=PaginationAndSorting.responseSortingLink("School",sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");			

			responseText=PaginationAndSorting.responseSortingLink("Address",sortOrderFieldName,"location",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink("End Date",sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");

			sb.append("<th valign='top'>Actions/Apply</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			int rowCount = 0;

			String redirectTo = request.getParameter("redirectTo")==null?"":request.getParameter("redirectTo");			
			if(redirectTo.equals("1")){
				finalJobOrderList = new ArrayList<JobOrder>();
			}
			//Set<JobOrder> setJobOrder= new LinkedHashSet<JobOrder>(lstsortedJobOrder);
			//lstsortedJobOrder = new ArrayList<JobOrder>(new LinkedHashSet<JobOrder>(setJobOrder));
			for(JobOrder jbOrder : lstsortedJobOrder){
				rowCount++;			
				String subName =null;
				if(jbOrder.getSubjectMaster()!=null){
					subName=jbOrder.getSubjectMaster().getSubjectName();
				}else{
					subName="";
				}				
				sb.append("<tr>");
				sb.append("<td>"+jbOrder.getJobId()+"</td>");
				sb.append("<td>"+jbOrder.getJobTitle()+"</td>");

				if(jbOrder.getGeoZoneMaster()!=null){

					if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990){
						sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						//sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}else if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
						//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}else{
						sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jbOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jbOrder.getDistrictMaster().getDistrictId()+");\">"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}
				}else
				{
					sb.append("<td></td>");
				}				
				sb.append("<td>"+subName+"</td>");

				if(jbOrder.getDistrictMaster().getDistrictId()==1200390)
				{
					sb.append("<td>&nbsp;</td>");	
				}
				else
				{
					if(jbOrder.getCreatedForEntity().equals(3)){
						if(jbOrder.getIsPoolJob()==2){
							sb.append("<td>&nbsp;</td>");	
						}else{
							sb.append("<td>"+jbOrder.getSchool().get(0).getSchoolName()+"</td>");
						}
					}
					else{
						if(jbOrder.getSchool().size()==1){
							if(jbOrder.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");	
							}else{
								sb.append("<td>"+jbOrder.getSchool().get(0).getSchoolName()+"</td>");
							}
						}else if(jbOrder.getSchool().size()>1)
						{

							String schoolListIds = jbOrder.getSchool().get(0).getSchoolId().toString();
							for(int i=1;i<jbOrder.getSchool().size();i++)
							{
								schoolListIds = schoolListIds+","+jbOrder.getSchool().get(i).getSchoolId().toString();
							}
							sb.append("<td> <a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
						}else
							sb.append("<td>&nbsp;</td>");
					}

				}

				if(jbOrder.getDistrictMaster().getDistrictId()==1200390)
				{
					sb.append("<td>"+jbOrder.getDistrictMaster().getAddress()+"</td>");	
				}
				else
				{
					if(jbOrder.getSchool().size()>0)
					{
						String schoolAddress="";
						String sAddress="";
						String sstate="";
						String scity = "";
						String szipcode="";

						// Get School address
						if(jbOrder.getSchool().size()==1){
							if(jbOrder.getSchool()!=null)
							{
								if(jbOrder.getSchool().get(0).getAddress()!=null && !jbOrder.getSchool().get(0).getAddress().equals(""))
								{
									if(!jbOrder.getSchool().get(0).getCityName().equals(""))
									{
										schoolAddress = jbOrder.getSchool().get(0).getAddress()+", ";
									}
									else
									{
										schoolAddress = jbOrder.getSchool().get(0).getAddress();
									}
								}
								if(!jbOrder.getSchool().get(0).getCityName().equals(""))
								{
									if(jbOrder.getSchool().get(0).getStateMaster()!=null && !jbOrder.getSchool().get(0).getStateMaster().getStateName().equals(""))
									{
										scity = jbOrder.getSchool().get(0).getCityName()+", ";
									}else{
										scity = jbOrder.getSchool().get(0).getCityName();
									}
								}
								if(jbOrder.getSchool().get(0).getStateMaster()!=null && !jbOrder.getSchool().get(0).getStateMaster().getStateName().equals(""))
								{
									if(!jbOrder.getSchool().get(0).getZip().equals(""))
									{
										sstate = jbOrder.getSchool().get(0).getStateMaster().getStateName()+", ";
									}
									else
									{
										sstate = jbOrder.getSchool().get(0).getStateMaster().getStateName();
									}	
								}
								if(!jbOrder.getSchool().get(0).getZip().equals(""))
								{
									szipcode = jbOrder.getSchool().get(0).getZip().toString();
								}

								if(schoolAddress !="" || scity!="" ||sstate!="" || szipcode!=""){
									sAddress = schoolAddress+scity+sstate+szipcode;
								}else{
									sAddress="";
								}
								if(jbOrder.getIsPoolJob()==2){
								//	sb.append("<td>"+getDistrictFullAddress(jbOrder)+"</td>");
								}else{
									sb.append("<td>"+sAddress+"</td>");
								}

							}
						}
						else
						{
							String schoolListIds = jbOrder.getSchool().get(0).getSchoolId().toString();
							for(int i=1;i<jbOrder.getSchool().size();i++)
							{
								schoolListIds = schoolListIds+","+jbOrder.getSchool().get(i).getSchoolId().toString();
							}
							sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
						}
					}
					else
					{

						String districtAddress="";
						String address="";
						String state="";
						String city = "";
						String zipcode="";

						//Get district address
						if(jbOrder.getDistrictMaster()!=null)
						{
							if(jbOrder.getDistrictMaster().getAddress()!=null && !jbOrder.getDistrictMaster().getAddress().equals(""))
							{
								if(!jbOrder.getDistrictMaster().getCityName().equals(""))
								{
									districtAddress = jbOrder.getDistrictMaster().getAddress()+", ";
								}
								else
								{
									districtAddress = jbOrder.getDistrictMaster().getAddress();
								}
							}
							if(!jbOrder.getDistrictMaster().getCityName().equals(""))
							{
								if(jbOrder.getDistrictMaster().getStateId()!=null && !jbOrder.getDistrictMaster().getStateId().getStateName().equals(""))
								{
									city = jbOrder.getDistrictMaster().getCityName()+", ";
								}else{
									city = jbOrder.getDistrictMaster().getCityName();
								}
							}
							if(jbOrder.getDistrictMaster().getStateId()!=null && !jbOrder.getDistrictMaster().getStateId().getStateName().equals(""))
							{
								if(!jbOrder.getDistrictMaster().getZipCode().equals(""))
								{
									state = jbOrder.getDistrictMaster().getStateId().getStateName()+", ";
								}
								else
								{
									state = jbOrder.getDistrictMaster().getStateId().getStateName();
								}	
							}
							if(!jbOrder.getDistrictMaster().getZipCode().equals(""))
							{
								zipcode = jbOrder.getDistrictMaster().getZipCode();
							}

							if(districtAddress !="" || city!="" ||state!="" || zipcode!=""){
								address = districtAddress+city+state+zipcode;
							}else{
								address="";
							}
						}
						if(jbOrder.getIsPoolJob()==2){
							//address
							//sb.append("<td>"+getDistrictFullAddress(jbOrder)+"</td>");
						}else{
							sb.append("<td>"+address+"</td>");
						}
					}
				}

				if(Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate()).equals("Dec 25, 2099"))
					sb.append("<td style='font-size:11px;'>Until filled</td>");
				else
					sb.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate())+",11:59 PM CST</td>");

				sb.append("<td>");
				sb.append("<a data-original-title=' Apply Now' rel='tooltip' id='tpApply"+rowCount+"' href='applyteacherjob.do?jobId="+jbOrder.getJobId()+"' ><img src='images/option01.png' style='width:30px; height:25px;'></a>");
				sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:30px; height:25px;'></a>");
				sb.append("</td>");

				sb.append("</tr>");

			}
			if(finalJobOrderList.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='6'>");

				sb.append("No record found.");
				sb.append("</td>");	   				
				sb.append("</tr>");
			}			

			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request, totalRecord, noOfRow, pageNo));
			out.write(sb.toString());

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;

	}
	
	@RequestMapping(value = "/hqbrjoborder.do", method = RequestMethod.GET)
	public String headquartersjoborderGET(ModelMap map,	HttpServletRequest request) {

		HttpSession session = request.getSession(false);


		if (session == null	|| ((session.getAttribute("userMaster") == null ) && (session.getAttribute("teacherDetail") == null ))) {
			logger.info("redirect:index.jsp");
			return "redirect:index.jsp";
		}
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		String  roleAccessU=null ;
		int roleId=0;
		if(userMaster.getRoleId()!=null){
			roleId=userMaster.getRoleId().getRoleId();
		}
		try{
			 // roleAccessU=roleAccessPermissionDAO.getMenuOptionList(roleId,133,"editheadquarter.do",100);
			/**
			 * uncomment for titan server (menuid is differ on titankelly from localhost)
			 */
			
		roleAccessU=roleAccessPermissionDAO.getMenuOptionListByMenuId(roleId, 144, "hqbrjoborder.do", 0, 152);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		map.addAttribute("roleAccessU", roleAccessU); 
		map.addAttribute("entityType", userMaster.getEntityType());
		
		
		
		
		if(userMaster.getRoleId().getRoleId()==10 || userMaster.getRoleId().getRoleId()==12)
		{
			map.addAttribute("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
			map.addAttribute("headQuarterName", userMaster.getHeadQuarterMaster().getHeadQuarterName());
		}
		else if(userMaster.getRoleId().getRoleId()==11 || userMaster.getRoleId().getRoleId()==13)
		{
			map.addAttribute("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
			map.addAttribute("headQuarterName", userMaster.getHeadQuarterMaster().getHeadQuarterName());
			map.addAttribute("branchMasterId", userMaster.getBranchMaster().getBranchId());
			map.addAttribute("branchName", userMaster.getBranchMaster().getBranchName());
			map.addAttribute("branchRootPath",Utility.getValueOfPropByKey("branchRootPath"));
		}
		List<JobCategoryMaster> jobCategoryMastersList = null;
		if(userMaster.getEntityType()==5) // Headquarter job categories
		{
			logger.info("*** 5 ***");
			try {
				jobCategoryMastersList = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(userMaster.getHeadQuarterMaster(),null,null);
			} catch (Exception e) {
				e.printStackTrace();
			}
			logger.info("*** 5 *** "+jobCategoryMastersList.size());
			map.addAttribute("jobCategoryMastersList", jobCategoryMastersList);
		}
		else if(userMaster.getEntityType()==6) // Branch Job Categories
		{
			try {
				jobCategoryMastersList = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(null,userMaster.getBranchMaster(),null);
			} catch (Exception e) {
				e.printStackTrace();
			}
			map.addAttribute("jobCategoryMastersList", jobCategoryMastersList);
		}
		else // all job categories
		{
			jobCategoryMastersList = jobCategoryMasterDAO.findActiveJobCategory();
			map.addAttribute("jobCategoryMastersList", jobCategoryMastersList);
		}
		map.addAttribute("entityType", userMaster.getEntityType());
		int JobOrderType=0;
		if(request.getParameter("JobOrderType")!=null){
			JobOrderType=Integer.parseInt(request.getParameter("JobOrderType"));
		}
		map.addAttribute("JobOrderType", JobOrderType);
		
		String jobId = request.getParameter("jobId")==null?"0":request.getParameter("jobId");
		JobOrder jobOrder = null;
		 if(!jobId.equals("") && !jobId.equals("0"))
		 {
			 try {
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		 }
		 	try {
				if(userMaster.getEntityType()==5 && userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getAssessmentUploadURL()!=null)
					map.addAttribute("HQssessmentUploadURL",userMaster.getHeadQuarterMaster().getAssessmentUploadURL().trim());
				 else if(userMaster.getEntityType()==6 && userMaster.getBranchMaster()!=null && userMaster.getBranchMaster().getAssessmentUploadURL()!=null)
					 map.addAttribute("BRssessmentUploadURL",userMaster.getBranchMaster().getAssessmentUploadURL().trim());	 
				 else
				 map.addAttribute("BRssessmentUploadURL",null);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			 map.addAttribute("headQuarterRootPath",Utility.getValueOfPropByKey("headQuarterRootPath"));
			 map.addAttribute("attachJobAssessmentVal",1);
			 map.addAttribute("dateTime",Utility.getDateTime());
			 	
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);
			
		 if(jobOrder!=null)
		 {
			 String IsInviteOnlyFlag="";
			 boolean hideJobFlag=false;
			 try {
				 map.addAttribute("jobStartDate",Utility.getCalenderDateFormart(jobOrder.getJobStartDate()+""));
				 map.addAttribute("jobEndDate",Utility.getCalenderDateFormart(jobOrder.getJobEndDate()+""));
				 map.addAttribute("jobCat", jobOrder.getJobCategoryMaster().getJobCategoryId());
					map.addAttribute("JobOrderType", 5);					
				 try 
				 {
					 if(request.getParameter("clone") ==null || !request.getParameter("clone").equals("yes"))
						 map.addAttribute("JobPostingURL",Utility.getShortURL(Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId()));
				} catch (Exception e) {
					e.printStackTrace();
				}
				 List<SchoolInJobOrder> schoolInJobOrder = null;
				 schoolInJobOrder = schoolInJobOrderDAO.findJobOrder(jobOrder);
				 if(schoolInJobOrder!=null && schoolInJobOrder.size()>0)
				 {
					 map.addAttribute("attachD", true);
				 }
					if(jobOrder.getIsInviteOnly()!=null && jobOrder.getIsInviteOnly()){
						IsInviteOnlyFlag="checked";
						if(jobOrder.getHiddenJob()!=null && jobOrder.getHiddenJob()){
							hideJobFlag=true;
						}else if(jobOrder.getHiddenJob()!=null && !jobOrder.getHiddenJob()){
							hideJobFlag=false;
						}
					}
					map.addAttribute("hideJobFlag",hideJobFlag);	
					map.addAttribute("IsInviteOnlyFlag",IsInviteOnlyFlag);
					if(jobOrder.getBranchMaster()!=null)
						map.addAttribute("branchName",jobOrder.getBranchMaster().getBranchName());
				 
			 } catch (Exception e) {
				 e.printStackTrace();
			 }		 	
		 }
		 logger.info(request.getParameter("clone"));
		 if(request.getParameter("clone") !=null && request.getParameter("clone").equals("yes"))
			 jobOrder.setJobId(null);
		 map.addAttribute("jobOrder", jobOrder);
		 	
		 

     return "hqbrjoborder";
	}
	
	@RequestMapping(value = "/managehqbrjoborders.do", method = RequestMethod.GET)
	public String manageHqBrJobordersGet(ModelMap map,	HttpServletRequest request) {

		HttpSession session = request.getSession(false);

		if (session == null	|| ((session.getAttribute("userMaster") == null ) && (session.getAttribute("teacherDetail") == null ))) {
			logger.info("redirect:index.jsp");
			return "redirect:index.jsp";
		}
		String roleAccess=null;
		int roleId = 0;
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		roleId = userMaster.getRoleId().getRoleId();
		if(roleId==10 || roleId==12)
		{
			map.addAttribute("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
			map.addAttribute("headQuarterName", userMaster.getHeadQuarterMaster().getHeadQuarterName());
			map.addAttribute("JobOrderType", 5);
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,110,"managehqbrjoborders.do",1);
			}catch(Exception e){
				e.printStackTrace();
			}
		}else if(roleId==11 || roleId==13)
		{
			map.addAttribute("branchId", userMaster.getBranchMaster().getBranchId());
			map.addAttribute("BranchName", userMaster.getBranchMaster().getBranchName());
			map.addAttribute("JobOrderType",6);
			try{
				logger.info(" roleId :: "+roleId);
				roleAccess=roleAccessPermissionDAO.getMenuOptionListByMenuId(roleId,144,"managehqbrjoborders.do",1,151);
				logger.info(" roleAccess :: "+roleAccess);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		else
		{
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,110,"managehqbrjoborders.do",1);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		logger.info(roleId+" roleAccess >>:: "+roleAccess);
		String jobAuthKey=(Utility.randomString(8)+Utility.getDateTime());
		map.addAttribute("jobAuthKey",jobAuthKey);
		map.addAttribute("roleAccess", roleAccess);
     return "managehqbrjoborder";
	}
	
	@RequestMapping(value="/kesdashboard.do", method=RequestMethod.GET)
	public String doKesDashboardGET(ModelMap map,HttpServletRequest request)
	{
		logger.info(" kesdashboard.do GET");
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:signin.do";
		}
		else{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		HeadQuarterMaster headQuarterMaster = null;
		BranchMaster branchMaster = null;
		if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()==null){
			map.addAttribute("headQuarterMaster", userMaster.getHeadQuarterMaster());
		}
		else if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()!=null){
			map.addAttribute("branchMaster", userMaster.getBranchMaster());
			map.addAttribute("headQuarterMaster", userMaster.getHeadQuarterMaster());
		}
		return "kesdashboard";
	}
	
	//Ajay Jain
	@RequestMapping(value="/branchdashboard.do", method=RequestMethod.GET)
	public String doBranchDashboardGET(ModelMap map,HttpServletRequest request)
	{
		logger.info(" branchdashboard.do GET");
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:signin.do";
		}
		else{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}		
		if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()==null){
			map.addAttribute("headQuarterMaster", userMaster.getHeadQuarterMaster());
		}
		else if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()!=null){
			map.addAttribute("branchMaster", userMaster.getBranchMaster());
			map.addAttribute("headQuarterMaster", userMaster.getHeadQuarterMaster());
		}
		map.addAttribute("branchDashboard", "branchDashBoard");
		return "kesdashboard";
	}
	
	
	@RequestMapping(value="/kellyonboarding_new.do", method=RequestMethod.GET)
	public String kellyonboardingNEWGET(ModelMap map,HttpServletRequest request)
	{

        UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		
		boolean ncCheck = false;
		
		if(userMaster!=null && userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
			ncCheck = true;
		
		map.addAttribute("entityType", userMaster.getEntityType());
		map.addAttribute("roleId", userMaster.getRoleId().getRoleId());
		map.addAttribute("ncCheck", ncCheck);
		
		if(userMaster!=null && userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId().equals(1)){
			System.out.println("Kelly OnBoarding Controller");
			try{
				if(userMaster.getRoleId().getRoleId()==10 || userMaster.getRoleId().getRoleId()==12)
				{
					map.addAttribute("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
					map.addAttribute("headQuarterName", userMaster.getHeadQuarterMaster().getHeadQuarterName());
					map.addAttribute("headQuarterMaster", userMaster.getHeadQuarterMaster());
					map.addAttribute("branchMaster", userMaster.getBranchMaster());
				}
				else if(userMaster.getRoleId().getRoleId()==11 || userMaster.getRoleId().getRoleId()==13)
				{
					map.addAttribute("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
					map.addAttribute("headQuarterName", userMaster.getHeadQuarterMaster().getHeadQuarterName());
					map.addAttribute("branchMasterId", userMaster.getBranchMaster().getBranchId());
					map.addAttribute("branchName", userMaster.getBranchMaster().getBranchName());
					map.addAttribute("branchMaster", userMaster.getBranchMaster());
					
				}
				List<JobCategoryMaster> jobCategoryMastersList = null;
				List<ReasonsForWaived> reasonsForWaivedList = null;
				
					try {
						jobCategoryMastersList = jobCategoryMasterDAO.findAllMasterJobCategorysOfHeadQuarter(userMaster.getHeadQuarterMaster());
						reasonsForWaivedList=reasonsForWaivedDAO.findAll();
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					map.addAttribute("jobCategoryMastersList", jobCategoryMastersList);
					map.addAttribute("reasonsForWaivedList", reasonsForWaivedList);
				
				return "kellyonbnew";
			}catch(Exception e){
				e.printStackTrace();
				return "kellyonbnew";
			}
			
		}
		
		return "kellyonbnew";
	}
}
