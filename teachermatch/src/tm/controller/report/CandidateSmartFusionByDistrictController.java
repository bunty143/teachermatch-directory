package tm.controller.report;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tm.bean.user.UserMaster;

@Controller
public class CandidateSmartFusionByDistrictController {

	
	@RequestMapping(value="candidaterecorddetails.do",method=RequestMethod.GET)
	public String getCandidateRecordDetailByDistrict(ModelMap map, HttpServletRequest request, HttpServletResponse response){
        try{
		
        	System.out.println("****CandidateSmartFusionByDistrictController******");
        	UserMaster userMaster = null;
		    
		    HttpSession session = request.getSession(false);
		    int roleId = 0;
		    String roleAccessName = null;
		    
		    if(session == null || session.getAttribute("userMaster")== null){
		    	return "redirect:index.jsp";
		    }else{
		    	userMaster = (UserMaster)session.getAttribute("userMaster");
		    	if(userMaster.getRoleId().getRoleId()!=null){
		    		roleId = userMaster.getRoleId().getRoleId();
		    		roleAccessName = userMaster.getRoleId().getRoleName();
		    		map.addAttribute("roleId", roleId);
		    		map.addAttribute("roleAccessName", roleAccessName);
		    	}
		    }
		    
		    if(userMaster.getEntityType()!=1){
		    	System.out.println("inside if block userMaster.getDistrictId().getDistrictName() : "+userMaster.getDistrictId().getDistrictName());
		    	map.addAttribute("districtORSchoolName", userMaster.getDistrictId().getDistrictName());
		    	map.addAttribute("districtOrSchoolId", userMaster.getDistrictId().getDistrictId());
		    }else{
		    	map.addAttribute("districtOrSchoolName", null);
		    	//map.addAttribute("districtOrSchoolID",null);
		    	System.out.println("inside else block userMaster.getDistrictId().getDistrictName() : ");
		    }
		    
		        map.addAttribute("userMaster", userMaster);
		        map.addAttribute("servletResponse",response);
		        System.out.println("Entity Type ID: "+userMaster.getEntityType());
		
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return "candidateRecordDetatails";
	}
}
