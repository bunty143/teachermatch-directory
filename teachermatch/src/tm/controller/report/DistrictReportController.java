package tm.controller.report;


import java.util.HashMap;
import java.util.Map;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mysql.jdbc.util.TimezoneDump;

import tm.bean.master.DistrictMaster;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.UserFolderStructure;
import tm.bean.user.UserMaster;
import tm.dao.master.PanelAttendeesDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TimeZoneMasterDAO;
import tm.dao.master.UserFolderStructureDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.StringEncrypter;
import tm.utility.Utility;
import tm.utility.StringEncrypter.EncryptionException;

/*
 * District report page url
 */
@Controller
public class DistrictReportController 
{
   @RequestMapping(value="/districtReport.do", method=RequestMethod.GET)
	public String districtWeeklyReport(ModelMap map,HttpServletRequest request)
	{
	  	UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		
		if(userMaster.getEntityType()!=1){
			map.addAttribute("DistrictName",userMaster.getDistrictId().getDistrictName());
			map.addAttribute("DistrictId",userMaster.getDistrictId().getDistrictId());
		}else{
			map.addAttribute("DistrictName",null);
		}
		if(userMaster.getEntityType()==3){
			map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
			map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
		}else{
			map.addAttribute("SchoolName",null);
		}
		map.addAttribute("entityType",userMaster.getEntityType());
	
	return "districtReport";

	}
	
   @RequestMapping(value="/districtjobreport.do", method=RequestMethod.GET)
	public String districtJobReport(ModelMap map,HttpServletRequest request)
	{
	   System.out.println("????????????????????????????????????????????");
	  	UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		
		if(userMaster.getEntityType()!=1){
			map.addAttribute("DistrictName",userMaster.getDistrictId().getDistrictName());
			map.addAttribute("DistrictId",userMaster.getDistrictId().getDistrictId());
		}else{
			map.addAttribute("DistrictName",null);
		}
		if(userMaster.getEntityType()==3){
			map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
			map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
		}else{
			map.addAttribute("SchoolName",null);
		}
		map.addAttribute("entityType",userMaster.getEntityType());
	
	return "districtjobreport";

	}
   
   
}


