package tm.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;

import tm.utility.Utility;

public class MessageCGUploadServlet extends HttpServlet 
{

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println(">>>>>>> File Upload for Message :::>>>>>>>>>>");

		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");  

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List uploadedItems = null;
		FileItem fileItem = null;
		String ext="",filePath="";
		String teacherId="";
		
		try{
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			String fileName="";
			String dateTime=Utility.getDateTime();
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if(fileItem.getFieldName().equals("teacherIdForMessage")){
					teacherId=fileItem.getString();
				}
			}

			//System.out.println("teacherId:::::::::::"+teacherId);	
			String jobForTeacherId="";
			String[] jobForTeacherIds=teacherId.split(",");
			if(jobForTeacherIds.length>0)
				jobForTeacherId=jobForTeacherIds[0];
			
			if(jobForTeacherId!=null && !jobForTeacherId.equalsIgnoreCase(""))
			{
				filePath=Utility.getValueOfPropByKey("communicationRootPath")+"/message/"+jobForTeacherId+"/";
				//System.out.println("filePath:::::::"+filePath);

				File f=new File(filePath);
				if(!f.exists())
					f.mkdirs();

				if (fileItem.isFormField() == false){
					if (fileItem.getSize() > 0){
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						fileName="message"+dateTime+ext;
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
					}
				}
				fileItem=null;				
			}
			
			if(jobForTeacherIds.length > 1)
			{
				for(int j=1; j<jobForTeacherIds.length;j++)
				{
					jobForTeacherId="";
					jobForTeacherId=jobForTeacherIds[j];
					
					String source =filePath+"/"+fileName;

					String target=Utility.getValueOfPropByKey("communicationRootPath")+"/message/"+jobForTeacherId+"/";
					
					//System.out.println("Test "+filePath+" :: "+target);
					
					File sourceFile = new File(source);
					File targetDir = new File(target);
					if(!targetDir.exists())
						targetDir.mkdirs();
		
					File targetFile = new File(targetDir+"/"+sourceFile.getName());
					FileUtils.copyFile(sourceFile, targetFile);
				}
			}
			
			//System.out.println("fileName:::"+fileName);
			response.setContentType("text/html");  
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.saveMessageFile('"+dateTime+"');");
			pw.print("</script>");
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
