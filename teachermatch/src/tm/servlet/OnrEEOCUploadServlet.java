package tm.servlet;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import tm.services.clamav.ClamAVUtil;
import tm.utility.Utility;

public class OnrEEOCUploadServlet extends HttpServlet 
{
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		String msg="";
		PrintWriter pw = response.getWriter();
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		HttpSession session = request.getSession();
			if (isMultipart){
				FileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				String fileName="";
				try {
					List items = upload.parseRequest(request);
					Iterator iterator = items.iterator();
					String teacherIdEEOC = "";
					while (iterator.hasNext()) {
						FileItem item = (FileItem) iterator.next();
						if(item.getFieldName().equals("teacherIdEEOC")){
							teacherIdEEOC =item.getString();
							
						}
						if (!item.isFormField())
						{
							String root = getServletContext().getRealPath("/");
							File path = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherIdEEOC+"/onrDashboard");
							if (!path.exists()) {
								boolean status = path.mkdirs();
							}
							String myFullFileName = item.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
							int startIndex = myFullFileName.lastIndexOf(slashType);
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
							String ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
							fileName="ONR"+teacherIdEEOC+Utility.getDateTime()+ext;//session.getId()+ext;
							File uploadedFile = new File(path + "/" + fileName);
							item.write(uploadedFile);
							msg = ClamAVUtil.scanAndRemove(uploadedFile.toString());
						}
					}
				} catch (FileUploadException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				response.setContentType("text/html");  
				pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
				if(msg.equals(""))
					pw.print("window.top.saveEEOCDataUpload('"+fileName+"');");
				else
					pw.print("window.top.fileContainsVirusDiv('"+msg+"')");
				pw.print("</script>");
			}
	}

}
