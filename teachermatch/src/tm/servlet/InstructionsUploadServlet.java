package tm.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import tm.utility.Utility;

public class InstructionsUploadServlet extends HttpServlet 
{

	private static final long serialVersionUID = -4185540075178004639L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println("Calling Instructions Upload Servlet ...");

		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");  

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List uploadedItems = null;
		FileItem fileItem = null;
		String ext="",filePath="";
		String file="";
		
		String instructionFileId="";
		String secondaryStatusId="";
		
		
		try{
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			//String fileName="";
			String fullFileName="";
			int spacePost	=	-1;
			
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if(fileItem.getFieldName().equals("secondaryStatusId_INS")){
					secondaryStatusId=fileItem.getString();
				}
				if(fileItem.getFieldName().equals("instructionFileId")){
					instructionFileId=fileItem.getString();
				}
			}

			System.out.println("instructionFileId :: "+instructionFileId);
			System.out.println("secondaryStatusId :: "+secondaryStatusId);
			
			
			if(instructionFileId!=null ){
				filePath=Utility.getValueOfPropByKey("statusInstructionRootPath")+"/"+secondaryStatusId+"/";
				System.out.println("filePath :: "+filePath);

				File f=new File(filePath);
				if(!f.exists())
					f.mkdirs();
								
				if (fileItem.isFormField() == false){
					if (fileItem.getSize() > 0)
					{
						
						String[] oldFilePaths;
						oldFilePaths = f.list();
						for(String oldFileName:oldFilePaths)
				         {
				            File fileOld=new File(filePath+"/"+oldFileName);
							if(fileOld.exists()){
								fileOld.delete();
							}
				         }
						
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						
						//myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						//ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						//fileName="INS_"+secondaryStatusId+"_"+noteDateTime+ext;
						
						if(myFullFileName.length()>254)
						{
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.lastIndexOf("."));
							ext=myFullFileName.substring(myFullFileName.lastIndexOf("."),myFullFileName.length());
							myFileName = myFullFileName.substring(startIndex + 1, 250);
							spacePost=myFileName.lastIndexOf(" ");
							if(spacePost!=-1)
							{
								myFileName = myFullFileName.substring(startIndex + 1, spacePost);
							}
							myFileName=myFileName.replaceAll("[^\\w\\s]", "");
							fullFileName=myFileName+""+ext;
						}
						else
						{
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.lastIndexOf("."));
							ext=myFullFileName.substring(myFullFileName.lastIndexOf("."),myFullFileName.length());
							myFileName=myFileName.replaceAll("[^\\w\\s]", "");
							fullFileName=myFileName+""+ext;
						}
						
						
						uploadedFile = new File(filePath, fullFileName);
						fileItem.write(uploadedFile);
					}
				}
				fileItem=null;				
			}
			System.out.println("fileName:::"+fullFileName);
			response.setContentType("text/html");  
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.insertOrUpdateQuestionCallFromServlet('"+fullFileName+"');");
			pw.print("</script>");
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

}
