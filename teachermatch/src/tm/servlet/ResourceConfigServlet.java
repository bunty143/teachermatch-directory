package tm.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tm.utility.Utility;

import java.net.*; 
import java.io.*; 

public class ResourceConfigServlet extends HttpServlet {


	private static final long serialVersionUID = 1L;

	public ResourceConfigServlet() {
		super();
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	
		/*String line;
		try 
		{ 
			URL url = new URL(Utility.getValueOfPropByKey("basePath")+"/ResourceConfigServlet");
			//URL url = new URL("http://localhost:8080/teachermatch/ResourceConfigServlet"); 
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream())); 
			line = in.readLine(); 
			System.out.println( line );	
			in.close(); 
		}
		catch (Exception e)
		{ 
			e.printStackTrace(); 
		}*/
	}
	
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
	}

	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		try{  
			  
			ServletContext context=getServletContext();  
			
			ListFilesUtil listFilesUtil = new ListFilesUtil();
			Map<String,Long> resouceMap = listFilesUtil.getUpdatedFilesMap(context);
			
		    context.setAttribute("resouceMap",resouceMap);   
		    out.print("Done");
			}catch(Exception e){
				System.out.println(e);
			}  
			
		out.flush();
		out.close();
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		out.flush();
		out.close();
	}

	public void init() throws ServletException {
		// Put your code here
	}

}
