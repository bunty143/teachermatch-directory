package tm.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.controller.master.UploadXlsxController;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherUploadTempDAO;
import tm.dao.master.CityMasterDAO;
import tm.utility.Utility;

public class QuestionUploadServlet extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	 {
		System.out.println("QuestionUploadServlet :: doPost");
		PrintWriter pw = response.getWriter();
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		HttpSession session = request.getSession();
		String assessmentId = "";
		String sectionId    = "";
		String assesmentAndSection	=	"";

			if (isMultipart){
				FileItemFactory factory 	= 	new DiskFileItemFactory();
				ServletFileUpload upload 	= 	new ServletFileUpload(factory);
				String fileName				=	"";
				try {
					List items 			= 	upload.parseRequest(request);
					Iterator iterator 	= 	items.iterator();
					while (iterator.hasNext()) {
						
						FileItem item = (FileItem) iterator.next();
						
						if(item.getFieldName().equals("assessmentId")){
							assessmentId = item.getString();
						}

						if(item.getFieldName().equals("sectionId")){
							sectionId = item.getString();
						}

						if (!item.isFormField()) {
							String root = getServletContext().getRealPath("/");
							System.out.println("root::::::"+root);
							File path = new File(root + "/uploads");
							if (!path.exists()) {
								boolean status = path.mkdirs();
							}

							String myFullFileName = item.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
							int startIndex = myFullFileName.lastIndexOf(slashType);
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
							String ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
							fileName=session.getId()+ext;
							File uploadedFile = new File(path + "/" + fileName);
							//System.out.println(uploadedFile.length()+" uploadedFile "+uploadedFile.getAbsolutePath());
							item.write(uploadedFile);
						}
					}
					
					assesmentAndSection = assessmentId+"####"+sectionId;
				} catch (FileUploadException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				response.setContentType("text/html");  
				pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
				pw.print("window.top.uploadDataQuestion('"+fileName+"','"+session.getId()+"','"+assesmentAndSection+"');");
				pw.print("</script>");
			}
	}

}
