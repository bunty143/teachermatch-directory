package tm.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tm.utility.IPAddressUtility;


public class HeapTestServlet extends HttpServlet 
{
	private static final long serialVersionUID = 299184069682685833L;
	
	private static final long MEGABYTE = 1024L * 1024L;

	  public static long bytesToMegabytes(long bytes) {
	    return bytes / MEGABYTE;
	  }	
	  
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println("Heap Memory Test");
		PrintWriter pw = response.getWriter();
		HttpSession session = request.getSession();
		response.setContentType("text/html");
		System.out.println(IPAddressUtility.getIpAddress(request));
		String sReturnText="";
		try 
		{
			// Get the Java runtime
		    Runtime runtime = Runtime.getRuntime();
		    // Run the garbage collector
		    runtime.gc();
		    // Calculate the used memory
		    long totalMemory=runtime.totalMemory();
		    long freeMemory=runtime.freeMemory();
		    long usedmemory = totalMemory - freeMemory;
		    
		    System.out.println("Total Memory : " + totalMemory +" Byte "+bytesToMegabytes(totalMemory)+" MB");
		    System.out.println("Total Free Memory : " + freeMemory +" Byte "+bytesToMegabytes(freeMemory)+" MB");
		    System.out.println("Total Used Memory : " + usedmemory +" Byte "+bytesToMegabytes(usedmemory)+" MB");
			
		    String sTotalMemory=bytesToMegabytes(totalMemory)+" MB";
		    String sFreeMemory=bytesToMegabytes(freeMemory)+" MB";
		    String sUsedMemory =bytesToMegabytes(usedmemory)+" MB";
		    
		    sReturnText="Total Memory "+sTotalMemory +" <BR>Free Memory "+sFreeMemory +" <BR>Used Memory "+sUsedMemory;
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		pw.print("<P>"+sReturnText+"</P>");
			
	}

}
