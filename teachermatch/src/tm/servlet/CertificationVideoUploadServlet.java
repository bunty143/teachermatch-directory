package tm.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;
import tm.dao.master.DistrictAttachmentDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.utility.Utility;

public class CertificationVideoUploadServlet extends HttpServlet{
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private DistrictAttachmentDAO districtAttachmentDAO;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
			{	
			try {
				System.out.println("Video File Upload");
				PrintWriter pw = response.getWriter();
				response.setContentType("text/html");  

				request.setCharacterEncoding("UTF-8");
				response.setContentType("text/html");
				response.setCharacterEncoding("UTF-8");
				
				FileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				List uploadedItems = null;
				FileItem fileItem = null;
				String ext="",filePath="";
				String file="";
				HttpSession session=request.getSession();
				
				TeacherDetail teacherDetail=null;
				String teacherID= "";;
				if(session.getAttribute("teacherDetail")!=null)
				{
					 teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
					 teacherID=teacherDetail.getTeacherId().toString();
				}
				else
					teacherID= request.getParameter("teacherIdForVideo");
				
				uploadedItems = upload.parseRequest(request);
				System.out.println("1");
				upload.setSizeMax(10485760);
				Iterator i = uploadedItems.iterator();
				String fileName="",videoId="";
				
				videoId = String.valueOf(System.currentTimeMillis()).substring(6);
				if(request.getSession().getAttribute("previousVideoFile")!=null){
					String previousFileName=request.getSession().getAttribute("previousVideoFile").toString();
					System.out.println("from video servlet >> "+previousFileName);
					if(previousFileName!=null && !previousFileName.equals("")){
						String pFileName=Utility.getValueOfPropByKey("teacherRootPath")+teacherID+"/Video/"+previousFileName;
						File pFile=new File(pFileName);
						if(pFile.exists()){
							pFile.delete();
						}
						session.removeAttribute("previousVideoFile");
					}
				}
					
					
				String sbtsource_ref="";
				while (i.hasNext())	
				{
					fileItem = (FileItem) i.next();
					if(fileItem.getFieldName().equals("sbtsource_ref")){
						sbtsource_ref=fileItem.getString();
					}
					
					if(fileItem.getName()!=null && !fileItem.getName().equals("")){
					break;
					}
				}
					
					
				if(teacherID!=null && teacherID!="" ){
					filePath=Utility.getValueOfPropByKey("teacherRootPath")+teacherID+"/Video/";

					File f=new File(filePath);
					if(!f.exists())
						f.mkdirs();

					if (fileItem.isFormField() == false){
						if (fileItem.getSize() > 0){
							File uploadedFile = null; 
							String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
							int startIndex = myFullFileName.lastIndexOf(slashType);
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
							ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
							fileName="Video_"+videoId+ext;
							uploadedFile = new File(filePath, fileName);
							fileItem.write(uploadedFile);
						}
					}
					fileItem=null;				
				}
				response.setContentType("text/html");  
				pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
				pw.print("window.top.saveVideo('"+fileName+"');");
				pw.print("</script>");				
					
			
				} catch (FileUploadException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("districtId222===");
				/*
				 * 	Save Recortd Into Database
				 * */
				
			
	   }
 
}
