package tm.servlet;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

/**
 * Contains some methods to list files and folders from a directory
 *
 * @author Loiane Groner
 * http://loiane.com (Portuguese)
 * http://loianegroner.com (English)
 */
public class ListFilesUtil {
 
    /**
     * List all the files and folders from a directory
     * @param directoryName to be listed
     */
    public void listFilesAndFolders(String directoryName){
 
        File directory = new File(directoryName);
 
        //get all the files from a directory
        File[] fList = directory.listFiles();
 
        for (File file : fList){
        	 System.out.println(file.getName()+" --- "+file.lastModified());
        }
    }
 
    /**
     * List all the files under a directory
     * @param directoryName to be listed
     */
    public void listFiles(String directoryName){
 
        File directory = new File(directoryName);
 
        //get all the files from a directory
        File[] fList = directory.listFiles();
 
        for (File file : fList){
            if (file.isFile()){
                System.out.println(file.getName()+" --- "+file.lastModified());
            }
        }
    }
 
    /**
     * List all the folder under a directory
     * @param directoryName to be listed
     */
    public void listFolders(String directoryName){
 
        File directory = new File(directoryName);
 
        //get all the files from a directory
        File[] fList = directory.listFiles();
 
        for (File file : fList){
            if (file.isDirectory()){
            	 System.out.println(file.getName()+" --- "+file.lastModified());
            }
        }
    }
 
    /**
     * List all files from a directory and its subdirectories
     * @param directoryName to be listed
     */
    public void listFilesAndFilesSubDirectories(String directoryName,Map<String,Long> resouceMap,String nameToReplace){
 
        File directory = new File(directoryName);
 
        //get all the files from a directory
        File[] fList = directory.listFiles();
 
        for (File file : fList){
            if (file.isFile()){
            	//System.out.println(nameToReplace);
            	// System.out.println(file.getPath().replace(nameToReplace, "")+" --- "+file.lastModified());
            	// resouceMap.put(file.getName(), file.lastModified());
            	
            	//resouceMap.put(file.getPath().replace(nameToReplace, ""), file.lastModified());
            	 resouceMap.put(file.getPath().replace(nameToReplace, "").replace("\\", "/"), file.lastModified());
            } else if (file.isDirectory()){
                listFilesAndFilesSubDirectories(file.getAbsolutePath(),resouceMap,nameToReplace);
            }
        }
    }
 
    public void listFilesAndFilesSubDirectoriesForClasses(String directoryName,Map<String,Long> resouceMap){
    	 
        File directory = new File(directoryName);
 
        //get all the files from a directory
        File[] fList = directory.listFiles();
 
        for (File file : fList){
            if (file.isFile()){
            	resouceMap.put(file.getName().replace(".class", ".ajax"), file.lastModified());
            } else if (file.isDirectory()){
            	listFilesAndFilesSubDirectoriesForClasses(file.getAbsolutePath(),resouceMap);
            }
        }
    }
    //vishwanath 
    public Map<String,Long> getUpdatedFilesMap(ServletContext context)
    {
    	Map<String,Long> resouceMap = new HashMap<String, Long>(); 
		
		String basePath =context.getRealPath("/");
		System.out.println("-----------------------------------------");
		 
	        final String directoryLinuxMac ="/Users/loiane/test";
	 
	        //Windows directory example
	        String directoryWindows ="";
	        directoryWindows =  basePath+"js";
	        //System.out.println(directoryWindows.replace("/", "\\"));
	        listFilesAndFilesSubDirectories(directoryWindows,resouceMap,basePath);
	       // listFilesUtil.listFilesAndFilesSubDirectories(directoryWindows,resouceMap,"F:/projectwork/teachermatch/WebRoot".replace("/", "\\"));
	        directoryWindows =  basePath+"css";
	        listFilesAndFilesSubDirectories(directoryWindows,resouceMap,basePath);
	        System.out.println("basePath: "+basePath);
	       // directoryWindows =  basePath+"WEB-INF\\classes\\tm\\services";
	        directoryWindows =  basePath+"WEB-INF/classes/tm/services";
	       listFilesAndFilesSubDirectoriesForClasses(directoryWindows,resouceMap);
	       
	       directoryWindows =  basePath+"WEB-INF/classes/tm/service";
	       listFilesAndFilesSubDirectoriesForClasses(directoryWindows,resouceMap);
	       
	        //F:\projectwork\.metadata\.me_tcat\webapps\teachermatch\
	        //WEB-INF\classes\tm\services
	        System.out.println("resouceMap:::: "+resouceMap.size());
	        //System.out.println(file.getName()+" --- "+file.lastModified());
	        for (Map.Entry<String,Long> entry : resouceMap.entrySet())
	        {
	            System.out.println(entry.getKey() + " - " + entry.getValue());
	        }
	        System.out.println("-----------------------------------------");
	        return resouceMap;
    }
    public static void main (String[] args){
 
        ListFilesUtil listFilesUtil = new ListFilesUtil();
 
        final String directoryLinuxMac ="/Users/loiane/test";
 
        //Windows directory example
        String directoryWindows ="C://test";
        directoryWindows = "F://projectwork/teachermatch/WebRoot/js";
       // listFilesUtil.listFiles(directoryWindows);
        /*directoryWindows = "F://projectwork/teachermatch/WebRoot/js";
        listFilesUtil.listFilesAndFilesSubDirectories(directoryWindows);*/
        directoryWindows = "F://projectwork/teachermatch/WebRoot";
       // listFilesUtil.listFilesAndFilesSubDirectories(directoryWindows);
    }
}
