package tm.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.user.UserMaster;
import tm.dao.master.DistrictAttachmentDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.services.clamav.ClamAVUtil;
import tm.utility.Utility;

public class EventLogoUploadServlet extends HttpServlet{
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private DistrictAttachmentDAO districtAttachmentDAO;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{	System.out.println("EventFileUpload");
		PrintWriter pw = response.getWriter();
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		HttpSession session = request.getSession();
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		System.out.println("userSession::::::::"+userSession.getUserId());
		Integer userId = userSession.getUserId();
		String districtId = "";
		String headQuarterId="";
		String branchId="";
			if (isMultipart){
				FileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				String fileName="";
				String eventId = "";
				try {
					List items = upload.parseRequest(request);
					Iterator iterator = items.iterator();
					while (iterator.hasNext()) {
						FileItem item = (FileItem) iterator.next();
						if(item.getFieldName().equals("districtId")){
							districtId =item.getString();
							System.out.println(districtId);
						}
						if(item.getFieldName().equals("eventId")){
							eventId =item.getString();
							System.out.println("eventId"+eventId);
						}
						//HQ/BA
						if(item.getFieldName().equals("branchId")){
							branchId =item.getString();
							System.out.println("branchId"+branchId);
						}
						else if(item.getFieldName().equals("headQuarterId")){
							headQuarterId =item.getString();
							System.out.println("headQuarterId"+headQuarterId);
						}
						
						if (!item.isFormField())
						{
							String filepath="";
							String root = getServletContext().getRealPath("/");
							if(districtId!=null && districtId.length()>0)
							   filepath=Utility.getValueOfPropByKey("districtRootPath")+districtId+"/Event/"+eventId;
							
							//HQ/BA Root Path
							/*if(branchId!=null && branchId.length()>0)
								filepath=Utility.getValueOfPropByKey("branchRootPath")+branchId+"/Event/"+eventId;
							else */if(headQuarterId!=null && headQuarterId.length()>0)
								filepath=Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterId+"/Event/"+eventId;
							
							System.out.println("PATH :::::::::::::::: "+filepath);
						    File path=new File(filepath);
							if (!path.exists()) {
								boolean status = path.mkdirs();
							}
							String myFullFileName = item.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
							int startIndex = myFullFileName.lastIndexOf(slashType);
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
							String ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
							String strFileName=myFullFileName.substring(0,myFullFileName.lastIndexOf("."));
							//fileName=strFileName+Utility.getDateTime()+ext;
							fileName=strFileName+ext;
							File uploadedFile = new File(path + "/" + fileName);
							item.write(uploadedFile);
							
							//Scaning the File
							String msg="";
							msg = ClamAVUtil.scanAndRemove(uploadedFile.toString());
							if(!msg.equals(""))
								pw.print(msg);
						}
					}
				} catch (FileUploadException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("districtId212==="+districtId);
				/*
				 * 	Save Recortd Into Database
				 * */
				
			}
	   }
 
}
