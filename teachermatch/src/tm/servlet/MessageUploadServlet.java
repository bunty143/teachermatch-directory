package tm.servlet;

import java.io.File;



import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.services.EmailerService;
import tm.utility.Utility;

public class MessageUploadServlet extends HttpServlet 
{
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println(">>>>>>> File Upload for Message >>>>>>>>>>");

		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");  

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List uploadedItems = null;
		FileItem fileItem = null;
		String ext="",filePath="";
		String msgSubject="";
		String msg="";
		String file="";
		String to="";
		
		try{
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			String fileName="";

			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if(fileItem.getFieldName().equals("msgMailSubject")){
					msgSubject=fileItem.getString("UTF-8").trim();
				}
				if(fileItem.getFieldName().equals("msgMail")){
					msg=fileItem.getString("UTF-8").trim();
				}
				if(fileItem.getFieldName().equals("emailForTeacher")){
					to=fileItem.getString();
				}
				if(fileItem.getFieldName().equals("jobFile1")){
					file=fileItem.getString();
				}
			}

			System.out.println("msgSubject::::::::: "+msgSubject);
			System.out.println("msg::::::::: "+msg);
			System.out.println("To:::::::::::"+to);	

	
			if(to!=null ){
				filePath=Utility.getValueOfPropByKey("teachermessageRootPath")+to+"/";
				System.out.println("filePath:::::::"+filePath);

				File f=new File(filePath);
				if(!f.exists())
					f.mkdirs();

				if (fileItem.isFormField() == false){
					if (fileItem.getSize() > 0){
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						fileName="message"+ext;
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
					}
				}
				fileItem=null;				
			}

			ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
			Object myDao = context.getBean("myMailSender");
			EmailerService emailerService = (EmailerService)myDao;
			
			msg="<table><tr><td style='font-family: Tahoma;'>"+msg+"</td></tr></table>";
			
			if(!fileName.equals("")){
				System.out.println("with attachment: "+fileName);
				//sendMailWithAttachments(String to,String subject,String from, String text,String filePath)
				emailerService.sendMailWithAttachments(to, msgSubject,null,Utility.getUTFToHTML(msg),filePath+fileName);
			}else{
				System.out.println("without attachment: "+fileName);
				//sendMailAsHTMLText(String to,String subject, String text)
				emailerService.sendMailAsHTMLText(to, msgSubject,Utility.getUTFToHTML(msg));
			}

			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.hideMessage('"+1+"');");
			pw.print("</script>");
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

}
