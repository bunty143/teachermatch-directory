package tm.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherExperienceDAO;
import tm.utility.Utility;


public class ImpUploadServlet extends HttpServlet 
{
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		
		System.out.println("ImpUploadServlet : doGET");
		PrintWriter pw = response.getWriter();
		FileItemFactory factory = new DiskFileItemFactory();
		
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(10485760);
		
		
		HttpSession session = request.getSession();
		PrintWriter out = response.getWriter();

		List uploadedItems = null;
		FileItem fileItem = null;
		String filePath =Utility.getValueOfPropByKey("teacherRootPath")+1+"/";

		String ext="";
		
		try 
		{
			System.out.println("Enter ImpUploadServlet filePath::: "+filePath);
			uploadedItems = upload.parseRequest(request);
			Iterator i = uploadedItems.iterator();
			String fileName="";
			int id=0;
			int ids=0;
			int idCount=0;
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if(fileItem.getFieldName().equals("ids")){
					ids=Integer.parseInt(fileItem.getString());
				}
				if(fileItem.getFieldName().equals("idCount")){
					idCount=Integer.parseInt(fileItem.getString());
				}
			}
			
			System.out.println("ids:::::::::::"+ids);	
			System.out.println("idCount:::::::::::"+idCount);	
			String inputFilePath="";
			if(ids!=0 && idCount!=0 && fileItem.isFormField() == false)
			for(int c=0;c<idCount;c++){
				System.out.println("Created::::::"+c);
				id=ids+c;
				if(id!=0){
					filePath=Utility.getValueOfPropByKey("teacherRootPath")+id+"/";
					System.out.println("filePath:::::::"+filePath);
					
					if(c==0){
						File f=new File(filePath);
						if(!f.exists())
							f.mkdirs();
					
						//if (fileItem.isFormField() == false){
							if (fileItem.getSize() > 0){
								File uploadedFile = null; 
								String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
								int startIndex = myFullFileName.lastIndexOf(slashType);
								myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
								ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
								fileName="Resume"+ext;
								uploadedFile = new File(filePath, fileName);
								fileItem.write(uploadedFile);
							}
						//}
							inputFilePath=filePath+fileName;
							System.out.println("inputFilePath:::::"+inputFilePath);
					}else{	
						File f5=new File(filePath);
						if(!f5.exists())
							f5.mkdirs();
					
				        File f=new File(filePath+fileName);
				        
				        if(!f.exists())
				            f.createNewFile();
				        
				        if(f.canWrite())
				            System.out.println("Writable File");
				        
				        InputStream in = new FileInputStream(inputFilePath);
				        OutputStream output = new FileOutputStream(filePath+fileName);
				        
				        // Transfer bytes from in to output
				        byte[] buf = new byte[1024];
				        int len;
				        while ((len = in.read(buf)) > 0) {
				            output.write(buf, 0, len);
				        }
				        in.close();
				        output.close();
					}
									
				}
			}
			response.setContentType("text/html");
			System.out.println(":::::::::::::Complete:::::::::::::::::");
			pw.print("SuccessFully Uploaded Resume");
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	
	}

}
