package tm.mq.event;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import tm.bean.JobForTeacher;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.StatusNodeColorHistory;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.mq.MQEvent;
import tm.bean.mq.MQEventHistory;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.hqbranchesmaster.StatusNodeColorHistoryDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.mq.MQEventHistoryDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CommonService;
import tm.services.mq.MQService;
import tm.services.report.CandidateGridService;
import tm.utility.TMCommonUtil;


public class ActiveMQHandler  implements MessageListener {
		
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private MQEventHistoryDAO mqEventHistoryDAO;
	
	@Autowired
	private MQEventDAO mqEventDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private MQService mqService;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private StatusNodeColorHistoryDAO statusNodeColorHistoryDAO;
	
	@Autowired
	private CandidateGridService candidateGridService;
	
	@Override
	public void onMessage(Message message) {
		
		System.out.println("--------ActiveMQHandler--========---------");
		MapMessage map = (MapMessage) message;
		StatusNodeColorHistory stausNodeColorHistory =null;
		MQEvent mqEvent=null;
		String messageResponse="";
		Integer mqEventHistoryID = 0;
		Integer mqEventID = 0;
		try {
			
			Long jobForTeacherId = map.getLong("jobForTeacherId");
			String tmTransactionId = map.getString("TMTransactionID");
			mqEventHistoryID = map.getInt("MQEventHistoryID");
			mqEventID = map.getInt("MQEventID");
			String reqType=map.getString("reqType");
			String ksnId = map.getString("KSNID");
			MQService mqServiceObj=new MQService();
			
			Integer statusNodeColorHistoryId = map.getInt("statusNodeColorHistoryId");
			stausNodeColorHistory = statusNodeColorHistoryDAO.findById(statusNodeColorHistoryId,false,false);
			System.out.println(" ksnId ::: "+ksnId);
			JobForTeacher jobForTeacher=jobForTeacherDAO.findById(jobForTeacherId, false, false);
			System.out.println("jobForTeacherId:::: "+jobForTeacher);
			
			
			//StatusNodeColorHistory statusNodeColorHistory = null;
			// Map<String,MQEvent> mapNodeStatus= new HashMap<String, MQEvent>();

		   //   List<MQEvent> mqEventsList=new ArrayList<MQEvent>();
			String retrunValue=null;
			if(reqType!=null){
				if(reqType.equalsIgnoreCase("DC")){
					retrunValue=TMCommonUtil.updateKESCandidate(jobForTeacher,jobForTeacherDAO , tmTransactionId , mqEventHistoryID ,  mqEventHistoryDAO,ksnId);
				}else if(!reqType.equalsIgnoreCase("KSNID")){
					retrunValue=TMCommonUtil.updateKESCandidate(jobForTeacher,jobForTeacherDAO , tmTransactionId , mqEventHistoryID ,  mqEventHistoryDAO,ksnId);
				}
				
			}
			System.out.println("Response get on Handler  " + retrunValue);
			if(retrunValue == null)
			 return;
			//Checking for success and failure..........
			InputSource inputSource = new InputSource();
			inputSource.setCharacterStream(new StringReader(retrunValue));
			
			Document doc = null;
			DocumentBuilder docBuilder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
				
		    doc = docBuilder.newDocument();
			doc = docBuilder.parse(inputSource);
			NodeList CandidateDetailsTM = doc.getElementsByTagName("TeacherMatch");
			
			if (CandidateDetailsTM.getLength() > 0 && !reqType.equalsIgnoreCase("KSNID")) {
		        Element element = (Element) CandidateDetailsTM.item(0);
		        String status = element.getElementsByTagName("ResponseStatus").item(0).getTextContent();
	            messageResponse = element.getElementsByTagName("MessageDescription").item(0).getTextContent();
	            System.out.println("ResponseStatus: "+status);
	            System.out.println("MessageDescription:::= "+messageResponse);
		     
		        if(status.equalsIgnoreCase("Success")){
		        	mqEvent =	mqEventDAO.findById(mqEventID, false, false);
		        	 if(messageResponse.length()==0)
		        		  mqEvent.setAckStatus(null);
		        	  else
		        		  mqEvent.setAckStatus(status);
			        mqEvent.setMsgStatus(messageResponse);
			        mqEventDAO.makePersistent(mqEvent);
			        
			        MQEventHistory mqHistoryEvent =	mqEventHistoryDAO.findById(mqEventHistoryID, false, false);
			        mqHistoryEvent.setMqEvent(mqEvent);
			        mqEventHistoryDAO.makePersistent(mqHistoryEvent);
		        }else{
			         if(status!=null && (status.equalsIgnoreCase("Failure") || status.equalsIgnoreCase("Failed"))){	
				          mqEvent =	mqEventDAO.findById(mqEventID, false, false);
				          if(messageResponse.length()==0)
			        		  mqEvent.setAckStatus(null);
			        	  else
			        		  mqEvent.setAckStatus(status);
				          mqEvent.setStatus("R");
				          mqEvent.setMsgStatus(messageResponse);
				          mqEventDAO.makePersistent(mqEvent);
				          
				         try{ 
			        		HeadQuarterMaster headQuarterMaster=new HeadQuarterMaster();
			 		        headQuarterMaster.setHeadQuarterId(1);
			        		List<JobForTeacher> kellyAppliedJobs = jobForTeacherDAO.findByTeacherIdHQAndBranch_OP(mqEvent.getTeacherdetail(),headQuarterMaster, null);
			        		SecondaryStatus secondaryStatus=mqEvent.getSecondaryStatus();
			        		SessionFactory sessionFactory=assessmentDetailDAO.getSessionFactory();
							StatelessSession statelesSsession = sessionFactory.openStatelessSession();
							Transaction txOpen =statelesSsession.beginTransaction();
			        		for (JobForTeacher jobForTeacherObj : kellyAppliedJobs) {
			        			if(jobForTeacherObj.getSecondaryStatus()!=null && jobForTeacherObj.getSecondaryStatus().getSecondaryStatusId().equals(secondaryStatus.getSecondaryStatusId())){
			        				if(jobForTeacherObj.getStatusMaster()==null){
				        				StatusMaster statusMaster=null;				          
								        try{
				  							int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacherObj) ;
				  							statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacherObj,flagList);
				  						}catch(Exception ex){
				  						}
				  						if(statusMaster!=null){
				  							jobForTeacherObj.setApplicationStatus(statusMaster.getStatusId());
				  						}
				  						jobForTeacherObj.setStatus(statusMaster);
				  						jobForTeacherObj.setStatusMaster(statusMaster);
				  						jobForTeacherObj.setSecondaryStatus(null);
				  						statelesSsession.update(jobForTeacherObj);
			        				}
			        			}
			        			List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.getHistoryStatusList_OP(mqEvent.getTeacherdetail(),null,secondaryStatus);
			        			for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : lstTeacherStatusHistoryForJob) {
			        				teacherStatusHistoryForJob.setStatus("I");
			        				statelesSsession.update(teacherStatusHistoryForJob);
								}
			        			/*TeacherDetail teacherDetailObj=mqEvent.getTeacherdetail();
			        			teacherDetailObj.setKSNID(null);
		        				statelesSsession.update(teacherDetailObj);*/
							}
			        		txOpen.commit();
	        				statelesSsession.close();
				         }catch(Exception e){
				        	 e.printStackTrace();
				         }
				         
				         //sandeep 05-12-15
				         
				         if(mqEvent!=null){
								ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
								UserMaster userMaster=userMasterDAO.findById(mqEvent.getCreatedBy(),false,false);
								//mqService.statusUpdate(jobForTeacher,null,mqEvent.getSecondaryStatus(),userMaster,context0,messageResponse,false);
								String subject="";
								if(mqEvent.getSecondaryStatus()!=null){
									subject=mqEvent.getSecondaryStatus().getSecondaryStatusName();
								}else{
									subject=mqEvent.getEventType();
								}
								try {
									   mqServiceObj.saveTeacherStatusNotes(mqEvent, messageResponse, userMaster, context0, subject, jobForTeacher.getJobId());
									} catch (Exception e) {
										e.printStackTrace();
									}
							}
				         
				         
			         }else{
			        	  mqEvent =	mqEventDAO.findById(mqEventID, false, false);
			        	  if(messageResponse.length()==0)
			        		  mqEvent.setAckStatus(null);
			        	  else
			        		  mqEvent.setAckStatus(status);
				          mqEvent.setMsgStatus(messageResponse);
				          mqEventDAO.makePersistent(mqEvent);
			         }
		          
		          MQEventHistory mqHistoryEvent =	mqEventHistoryDAO.findById(mqEventHistoryID, false, false);
			      if(mqHistoryEvent != null){
			          mqHistoryEvent.setMqEvent(mqEvent);
				      mqEventHistoryDAO.makePersistent(mqHistoryEvent);
			      }    
		        }
		        
		        
		        /**
			        * add to track node color history
			        */
				try {
					if(stausNodeColorHistory!=null){
							String newColor ="";
						   /*if(messageResponse.length()==0){
							   newColor = "Yellow";
							   stausNodeColorHistory.setCurrentColor(newColor);
						   }
						   else*/{
							   newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
							   stausNodeColorHistory.setCurrentColor(newColor);
						   }
						   System.out.println(" newColor :: "+newColor);
						   stausNodeColorHistory.setUpdateDateTime(new Date());
						   statusNodeColorHistoryDAO.updatePersistent(stausNodeColorHistory);
					   }
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				/**
				 * end
				 */
		        
		        
			}    
			
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			
			mqEvent =	mqEventDAO.findById(mqEventID, false, false);
			mqEvent.setAckStatus(null);
	        mqEvent.setMsgStatus(messageResponse);
	        mqEventDAO.makePersistent(mqEvent);
	        
	        MQEventHistory mqHistoryEvent =	mqEventHistoryDAO.findById(mqEventHistoryID, false, false);
	        mqHistoryEvent.setMqEvent(mqEvent);
	        mqEventHistoryDAO.makePersistent(mqHistoryEvent);
	        
	        if(stausNodeColorHistory!=null){
	        	String newColor ="";
	        	newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
	        	stausNodeColorHistory.setCurrentColor(newColor);
	        	System.out.println(" newColor :: "+newColor);
	        	stausNodeColorHistory.setUpdateDateTime(new Date());
	        	statusNodeColorHistoryDAO.updatePersistent(stausNodeColorHistory);
	        }
		}
	}
	
}
