package tm.services.i4;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4DistrictAccount;
import tm.bean.i4.I4QuestionPool;
import tm.bean.i4.I4QuestionSetQuestions;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.i4.I4DistrictAccountDAO;
import tm.dao.i4.I4QuestionPoolDAO;
import tm.dao.i4.I4QuestionSetQuestionsDAO;
import tm.dao.i4.I4QuestionSetsDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.services.PaginationAndSorting;
import tm.utility.Utility;

public class I4QuestionsSetQuesAjax {

	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private I4QuestionPoolDAO i4QuestionPoolDAO;
	
	@Autowired
	private I4QuestionSetsDAO i4QuestionSetsDAO;
	
	@Autowired
	private I4QuestionSetQuestionsDAO i4QuestionSetQuestionsDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	//boolean pageNoFlag = false;
	@Autowired
	private I4DistrictAccountDAO i4DistrictAccountDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	
	// DIsplay Questions from question pool
	public String displayQuestions(String noOfRow, String pageNo,String sortOrder,String sortOrderType,String quesSetSearchText,String quesSetId,String districtId , String headQuarterId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			
			/*if(pageNoFlag==true)
			{
				System.out.println(" flag check :: "+pageNoFlag);
				pageNo = ""+(Integer.parseInt(pageNo)-1);
				pageNoFlag = false;
				
				System.out.println(" new Page  2 :::::: "+pageNo);
			}*/
			
			
			
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			System.out.println(" pgNo "+pgNo+" noOfRowInPage "+noOfRowInPage+" start "+start);
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			boolean allData	   	= false;
			boolean quesTextflag= false;
			
			List<I4QuestionPool> i4QuestionPools	  	=	new ArrayList<I4QuestionPool>();
			List<I4QuestionPool> filterData				=	new ArrayList<I4QuestionPool>();
			List<I4QuestionPool> finalDatalist			=	new ArrayList<I4QuestionPool>();
			List<I4QuestionPool> i4QuesForQues			=	new ArrayList<I4QuestionPool>();
			List<I4QuestionPool> repeatedQues			=   new ArrayList<I4QuestionPool>();
			
			List<I4QuestionSetQuestions> 	quesofQuesSetList	=   new ArrayList<I4QuestionSetQuestions>();
			
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"QuestionText";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			
			I4QuestionSets i4QuestionSets = i4QuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
			
			
			DistrictMaster districtMaster = null;;
			HeadQuarterMaster headQuarterMaster = null;
			
			if(districtId!=null && !districtId.equals(""))
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
		
			if(headQuarterId!=null && !headQuarterId.equals(""))
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
			
			if(districtMaster!=null)
				i4QuestionPools = i4QuestionPoolDAO.findByDistrict(districtMaster) ;
			
			if(headQuarterMaster!=null)
				i4QuestionPools = i4QuestionPoolDAO.findByHeadQuarter(headQuarterMaster) ;
				
			quesofQuesSetList	= i4QuestionSetQuestionsDAO.findByQuesSetId(i4QuestionSets,districtMaster,headQuarterMaster);
			
			for(I4QuestionSetQuestions i4QSQ:quesofQuesSetList)
			{
				repeatedQues.add(i4QSQ.getQuestionPool());
			}
			
			if(quesofQuesSetList.size()>0)
			{
				i4QuestionPools.removeAll(repeatedQues);
			}
			
			if(i4QuestionPools.size()>0)
			{
				filterData.addAll(i4QuestionPools);
				allData = true;
			}
			
			if(quesSetSearchText!=null && !quesSetSearchText.equalsIgnoreCase(""))
			{
				i4QuesForQues = i4QuestionPoolDAO.findi4QuesByQues(quesSetSearchText);
				quesTextflag = true;
			}
			
			if(quesTextflag==true)
				filterData.retainAll(i4QuesForQues);
			else
				filterData.addAll(i4QuesForQues);
			
			System.out.println((filterData.size()%10==0)+" pageNo "+pageNo+" filterData.size() "+filterData.size());
			
			//int newPageNo = 0;
			
			//System.out.println(" >>>>>>>>> "+!pageNo.equals("1"));
			
			
			/*if(!pageNo.equals("1") && filterData.size()%10==0)
			{
				System.out.println(" ################ ");
				int pageNoCount = 1;
				for(int i=1;i<filterData.size();i=i*10)
				{
					System.out.println("1"+i+"filterData.size() :: "+filterData.size());
					start = (pageNoCount-2)*10;
					newPageNo=pageNoCount;
					pageNoCount++;
				}
				pageNoFlag = true;
			}*/
			
			System.out.println(" filterData 1 :: "+filterData.size()+" pgNo :: "+pgNo);
			
			System.out.println(" Start :: "+start);
			
			totalRecord = filterData.size();
			if(totalRecord<end)
				end=totalRecord;
			
			finalDatalist	=	filterData.subList(start,end);
			
			dmRecords.append("<table  id='i4QuesTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");

			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblQues", locale),sortOrderFieldName,"QuestionText",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblcretedDate", locale),sortOrderFieldName,"DateCreated",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			dmRecords.append("<th width='15%'><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(finalDatalist.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoQuestionfound1", locale)+"</td></tr>" );
			System.out.println("No of Records "+i4QuestionPools.size());

			for (I4QuestionPool i4qp: finalDatalist) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+i4qp.getQuestionText()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(i4qp.getDateCreated())+"</td>");
				dmRecords.append("<td><a href='javascript:void(0);' onclick='addQuesFromQPtoQS("+i4qp.getID()+")'>"+Utility.getLocaleValuePropByKey("lnkAddQues", locale)+"</a></td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			
		//	if(newPageNo>0)
		//		pageNo = ""+newPageNo;
			
		//	System.out.println("  final page no ::::::: "+pageNo);
			
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	
	// Display Questions in Question Set
	public String displayQuesSetQues(String quesSetId,String districtId , String headQuarterId)
	{
		System.out.println(" =========== displayQuesSetQues ========="+quesSetId);
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			
			List<I4QuestionSetQuestions> i4QuestionSetQuestions = new ArrayList<I4QuestionSetQuestions>(); 
			
			I4QuestionSets i4QuestionSets = i4QuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
			
			DistrictMaster districtMaster = null;
			HeadQuarterMaster headQuarterMaster= null;			// @ Anurag
			if(districtId!=null && !districtId.equals(""))
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);

			if(headQuarterId !=null && !headQuarterId.equals(""))
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);

			
			i4QuestionSetQuestions = i4QuestionSetQuestionsDAO.findByQuesSetId(i4QuestionSets,districtMaster,headQuarterMaster);
		
			
			System.out.println(" i4QuestionSetQuestions size :: "+i4QuestionSetQuestions.size());
			
			dmRecords.append("<table  id='i4QuesSetQuesTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
		
			dmRecords.append("<th width='55%' valign='top'><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblQues", locale)+"</span></th>");
		
			dmRecords.append("<th width='15%'><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			
			/*================= Checking If Record Not Found ======================*/
			if(i4QuestionSetQuestions.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoQuestionfound1", locale)+"</td></tr>" );
			System.out.println("No of Records "+i4QuestionSetQuestions.size());

			//for (I4QuestionSetQuestions i4qp: i4QuestionSetQuestions)
			for (int i=0; i<i4QuestionSetQuestions.size();i++ )
			{
				String upButton ="";
				String downButton ="";
				
				System.out.println(" ");
				
				if(i==0)
					upButton = ""+Utility.getLocaleValuePropByKey("msgUp3", locale)+"";
				else
					upButton = "<a href='javascript:void(0);' disabled='disabled' onclick='moveUpQues("+i4QuestionSetQuestions.get(i).getID()+");'>"+Utility.getLocaleValuePropByKey("msgUp3", locale)+"</a>";
				
				if(i==i4QuestionSetQuestions.size()-1)
					downButton = ""+Utility.getLocaleValuePropByKey("msgDown3", locale)+"";
				else
					downButton = "<a href='javascript:void(0);' onclick='moveDownQues("+i4QuestionSetQuestions.get(i).getID()+");'>"+Utility.getLocaleValuePropByKey("msgDown3", locale)+"</a>";
				
				/*if(i==0 || i==i4QuestionSetQuestions.size()-1)
				{
					upButton = "Up";
					downButton = "Down";
				}
				else
				{
					upButton = "<a href='javascript:void(0);' disabled='disabled' onclick='moveUpQues("+i4QuestionSetQuestions.get(i).getID()+");'>Up</a>";
					downButton = "<a href='javascript:void(0);' onclick='moveDownQues("+i4QuestionSetQuestions.get(i).getID()+");'>Down</a>";
				}*/
				
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+i4QuestionSetQuestions.get(i).getQuestionPool().getQuestionText()+"</td>");
				dmRecords.append("<td>");
				
				dmRecords.append(upButton+" | "+downButton+" | <a href='javascript:void(0);' onclick='deleteQuesFromQuesSet("+i4QuestionSetQuestions.get(i).getID()+");'>"+Utility.getLocaleValuePropByKey("lnkRemo", locale)+"</a>");
				dmRecords.append("</td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	

	// Save New Question to question set
	@Transactional(readOnly=false)
	public int saveI4QuesInQuesSet(String quesSetId,String QuesText,String districtId , String headQuarterId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try
		{
			UserMaster userMaster = new UserMaster();
			
			if (session.getAttribute("userMaster") != null) 
			{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
			}
			
			I4QuestionSets i4QuestionSets = new I4QuestionSets();
			I4QuestionPool i4QuestionPool = new I4QuestionPool();
			I4QuestionSetQuestions i4QuestionSetQuestions = new I4QuestionSetQuestions();

			DistrictMaster districtMaster = null ;
			HeadQuarterMaster headQuarterMaster = null;				// @ Anurag
			if(districtId!=null && !districtId.equals(""))
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			
			if(headQuarterId!=null && !headQuarterId.equals(""))
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
			
			i4QuestionSets = i4QuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
			
			String i4QuestionId=null;
			String userName = "";
			String password = "";
			
			try {
				List<I4DistrictAccount> i4DistrictAccountlst = new ArrayList<I4DistrictAccount>();
				
				if(districtMaster!=null)
					i4DistrictAccountlst	=	i4DistrictAccountDAO.findByDistrict(districtMaster);
				
				if(headQuarterMaster!=null)
					i4DistrictAccountlst	=	i4DistrictAccountDAO.findByHeadQuarter(headQuarterMaster);
				
				if(i4DistrictAccountlst!=null && i4DistrictAccountlst.size()>0)
				{
					userName	=	i4DistrictAccountlst.get(0).getI4username();
					password    = 	i4DistrictAccountlst.get(0).getI4password();
				}
				
				System.out.println("======================================");
				i4QuestionId = JDigestI4API.I4_AddQuestion(userName,password,QuesText);
				System.out.println("======================================");
			
				System.out.println("i4QuestionId::: "+i4QuestionId);
				
				if(i4QuestionId!=null)
				{
					Date date = new Date();
					i4QuestionPool.setQuestionText(QuesText);
					i4QuestionPool.setStatus("A");
					i4QuestionPool.setDistrictMaster(districtMaster); 
					if(headQuarterMaster!=null)
					i4QuestionPool.setHeadQuarterId(headQuarterMaster.getHeadQuarterId());
					i4QuestionPool.setDateCreated(date);
					i4QuestionPool.setI4QuestionID(i4QuestionId);
					i4QuestionPoolDAO.makePersistent(i4QuestionPool);
				}
			
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String i4QuestionSetQuesId=null;
			try {
				System.out.println("======================================");
				//I4_AddQuestionToQuestionSet("Sanjeev_Arora1","teachermatch","950","3226");
				i4QuestionSetQuesId = JDigestI4API.I4_AddQuestionToQuestionSet(userName,password,i4QuestionSets.getI4QuestionSetID(),i4QuestionPool.getI4QuestionID());
				System.out.println("======================================");
			
				
				System.out.println("i4QuestionSetQuesId:: "+i4QuestionSetQuesId);
			
				if(i4QuestionSetQuesId!=null)
				{
					int quesSequence =0;
						List<I4QuestionSetQuestions> existQuesList = new ArrayList<I4QuestionSetQuestions>();
						existQuesList = i4QuestionSetQuestionsDAO.findByQuesSetId(i4QuestionSets,districtMaster,headQuarterMaster);
						
						if(existQuesList.size()==0)
						{
							quesSequence=1;
						}
						else
						{
							if(existQuesList.size()>0)
							{
								quesSequence = existQuesList.get(existQuesList.size()-1).getQuestionSequence()+1; 
							}
						}
						
						i4QuestionSetQuestions.setQuestionSequence(quesSequence);
						i4QuestionSetQuestions.setQuestionPool(i4QuestionPool);
						i4QuestionSetQuestions.setQuestionSets(i4QuestionSets);
						i4QuestionSetQuestionsDAO.makePersistent(i4QuestionSetQuestions);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return 1;
	}
	
	
	public int addQuesFromQPtoQS(String quesId,String quesSetId,String districtId , String headQuarterId)
	{
		System.out.println(" addQuesFromQPtoQS ");
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try
		{
			I4QuestionPool i4QuestionPool = i4QuestionPoolDAO.findById(Integer.parseInt(quesId), false, false);
			I4QuestionSets i4QuestionSets = i4QuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
			DistrictMaster districtMaster = null ;
			HeadQuarterMaster headQuarterMaster = null ;
			
			
			if(districtId!=null && !districtId.equals(""))
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			
			if(headQuarterId!=null && !headQuarterId.equals(""))
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);

			
			
			List<I4DistrictAccount> i4DistrictAccountlst = i4DistrictAccountDAO.findByDistrict(districtMaster);
			String userName = "";
			String password = "";

			if(i4DistrictAccountlst!=null && i4DistrictAccountlst.size()>0)
			{
				userName	=	i4DistrictAccountlst.get(0).getI4username();
				password    = 	i4DistrictAccountlst.get(0).getI4password();
			}
			
			String i4QuestionSetQuesId=null;
			try {
				System.out.println("======================================");
				//I4_AddQuestionToQuestionSet("Sanjeev_Arora1","teachermatch","950","3226");
				i4QuestionSetQuesId = JDigestI4API.I4_AddQuestionToQuestionSet(userName,password,i4QuestionSets.getI4QuestionSetID(),i4QuestionPool.getI4QuestionID());
				System.out.println("======================================");
				
				System.out.println("i4QuestionSetQuesId:: "+i4QuestionSetQuesId);
				
				I4QuestionSetQuestions i4QuestionSetQuestions = new I4QuestionSetQuestions();
				List<I4QuestionSetQuestions> existQuesList = new ArrayList<I4QuestionSetQuestions>();
				existQuesList = i4QuestionSetQuestionsDAO.findByQuesSetId(i4QuestionSets,districtMaster, headQuarterMaster);
				int quesSequence =0;
				
				if(i4QuestionSetQuesId!=null)
				{
					if(existQuesList.size()==0)
					{
						quesSequence=1;
					}
					else
					{
						if(existQuesList.size()>0)
						{
							quesSequence = existQuesList.get(existQuesList.size()-1).getQuestionSequence()+1; 
						}
					}
					i4QuestionSetQuestions.setQuestionSequence(quesSequence);
					i4QuestionSetQuestions.setQuestionPool(i4QuestionPool);
					i4QuestionSetQuestions.setQuestionSets(i4QuestionSets);
					i4QuestionSetQuestionsDAO.makePersistent(i4QuestionSetQuestions);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 1;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	//Delete Question from Question Set And display in Question Pool
	public int deleteQuesFromQuesSet(String quesSetQuesID)
	{
		 /*========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		String res = null;
		try
		{
			I4QuestionSetQuestions i4QuestionSetQuestions = new I4QuestionSetQuestions();
			
			i4QuestionSetQuestions = i4QuestionSetQuestionsDAO.findById(Integer.parseInt(quesSetQuesID), false, false);
			
			/*********** Delete from question set ***********************/
				List<I4DistrictAccount> i4DistrictAccountlst = i4DistrictAccountDAO.findByDistrict(i4QuestionSetQuestions.getQuestionSets().getDistrictMaster());
				
				String userName = "";
				String password = "";
	
				if(i4DistrictAccountlst!=null && i4DistrictAccountlst.size()>0)
				{
					userName	=	i4DistrictAccountlst.get(0).getI4username();
					password    = 	i4DistrictAccountlst.get(0).getI4password();
				}
				res = JDigestI4API.I4_DeleteQuestionToQuestionSet(userName, password, i4QuestionSetQuestions.getQuestionSets().getI4QuestionSetID(), i4QuestionSetQuestions.getQuestionPool().getI4QuestionID());
			/************************************************************/
			
			System.out.println("  question delete from question set response :: "+res);
			
			if(res!=null)
			{
				i4QuestionSetQuestionsDAO.makeTransient(i4QuestionSetQuestions);
			}
			return 1;
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return 0;
	}
	
	// Question Move Up in question set
	public int moveUpQues(String quesSetQuesID,String districtId , String headQuarterId)
	{
		 /*========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try
		{
			I4QuestionSetQuestions forMoveQues = new I4QuestionSetQuestions();
			
			if(quesSetQuesID!=null && !quesSetQuesID.equals(""))
				forMoveQues = i4QuestionSetQuestionsDAO.findById(Integer.parseInt(quesSetQuesID), false, false);
					
			I4QuestionSets i4QuestionSets = i4QuestionSetsDAO.findById(forMoveQues.getQuestionSets().getID(), false, false);
			
			DistrictMaster districtMaster = null;
			HeadQuarterMaster headQuarterMaster = null ;
			
			if(districtId!=null && !districtId.equals(""))
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);

			if(headQuarterId!=null && !headQuarterId.equals(""))
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);

			
			List<I4QuestionSetQuestions> existQuesList = new ArrayList<I4QuestionSetQuestions>();
			List<I4QuestionSetQuestions> updatedObjList = new ArrayList<I4QuestionSetQuestions>();
			
			existQuesList = i4QuestionSetQuestionsDAO.findByQuesSetId(i4QuestionSets,districtMaster ,headQuarterMaster);
			
			I4QuestionSetQuestions first = new I4QuestionSetQuestions();
			I4QuestionSetQuestions second = new I4QuestionSetQuestions();
			
			int first_QS = 0;
			int second_QS = 0;
			
			
			for(int i=existQuesList.size()-1; i>0;i--)
			{
				System.out.println(existQuesList.get(i).getID()+" -----"+i4QuestionSets.getID()+" ====================== >>>>>>>>>>>>>>> "+existQuesList.get(i).getID().equals(i4QuestionSets.getID()));
				
				if(existQuesList.get(i).getID().equals(forMoveQues.getID()))
				{
					first = existQuesList.get(i);
					second = existQuesList.get(i-1);
					break;
				}
			}
			first_QS = first.getQuestionSequence();
			second_QS = second.getQuestionSequence();
			
			//System.out.println("Before ::::::: first :: "+first.getQuestionSequence()+" second :: "+second.getQuestionSequence());
			
			first.setQuestionSequence(second_QS);
			second.setQuestionSequence(first_QS);
			
			//System.out.println("After ::::::: first :: "+first.getQuestionSequence()+" second :: "+second.getQuestionSequence());
			
			updatedObjList.add(first);
			updatedObjList.add(second);
			
			for(I4QuestionSetQuestions I4QSQ:updatedObjList)
			{
				i4QuestionSetQuestionsDAO.makePersistent(I4QSQ);
			}
			
			return 1;
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return 0;
	}
	
	// Question Move down in question set 
	public int moveDownQues(String quesSetQuesID, String districtId, String headQuarterId)
	{
		
		 /*========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try
		{
			I4QuestionSetQuestions forMoveQues = new I4QuestionSetQuestions();
			
			if(quesSetQuesID!=null && !quesSetQuesID.equals(""))
				forMoveQues = i4QuestionSetQuestionsDAO.findById(Integer.parseInt(quesSetQuesID), false, false);
					
			I4QuestionSets i4QuestionSets = i4QuestionSetsDAO.findById(forMoveQues.getQuestionSets().getID(), false, false);
			
			DistrictMaster districtMaster = null;
			HeadQuarterMaster headQuarterMaster = null ;	
			if(districtId!=null && !districtId.equals(""))
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);

			if(headQuarterId!=null && !headQuarterId.equals(""))
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);

			
			List<I4QuestionSetQuestions> existQuesList = new ArrayList<I4QuestionSetQuestions>();
			List<I4QuestionSetQuestions> updatedObjList = new ArrayList<I4QuestionSetQuestions>();
			
			existQuesList = i4QuestionSetQuestionsDAO.findByQuesSetId(i4QuestionSets,districtMaster,headQuarterMaster);
			
			I4QuestionSetQuestions first = new I4QuestionSetQuestions();
			I4QuestionSetQuestions second = new I4QuestionSetQuestions();
			
			int first_QS = 0;
			int second_QS = 0;
			
			
			for(int i=0;i<existQuesList.size()-1;i++)
			{
				if(existQuesList.get(i).getID().equals(forMoveQues.getID()))
				{
					first = existQuesList.get(i);
					second = existQuesList.get(i+1);
					break;
				}
			}
			first_QS = first.getQuestionSequence();
			second_QS = second.getQuestionSequence();
			
			System.out.println("Before ::::::: first :: "+first.getQuestionSequence()+" second :: "+second.getQuestionSequence());
			
			first.setQuestionSequence(second_QS);
			second.setQuestionSequence(first_QS);
			
			System.out.println("After ::::::: first :: "+first.getQuestionSequence()+" second :: "+second.getQuestionSequence());
			
			updatedObjList.add(first);
			updatedObjList.add(second);
			
			for(I4QuestionSetQuestions I4QSQ:updatedObjList)
			{
				i4QuestionSetQuestionsDAO.makePersistent(I4QSQ);
			}
			
			return 1;
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		
		return 0;
	}
}
