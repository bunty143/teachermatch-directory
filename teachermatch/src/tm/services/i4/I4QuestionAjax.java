package tm.services.i4;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.i4.I4DistrictAccount;
import tm.bean.i4.I4QuestionPool;
import tm.bean.i4.I4QuestionSetQuestions;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.i4.I4DistrictAccountDAO;
import tm.dao.i4.I4QuestionPoolDAO;
import tm.dao.i4.I4QuestionSetQuestionsDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.services.PaginationAndSorting;
import tm.utility.*;

public class I4QuestionAjax {
	 
	 String locale = Utility.getValueOfPropByKey("locale");
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lblQues=Utility.getLocaleValuePropByKey("lblQues", locale);
	 String lblcretedDate=Utility.getLocaleValuePropByKey("lblcretedDate", locale);
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String lblNoQuestionfound1=Utility.getLocaleValuePropByKey("lblNoQuestionfound1", locale);
	 String optAct=Utility.getLocaleValuePropByKey("optAct", locale);
	 String optInActiv=Utility.getLocaleValuePropByKey("optInActiv", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String lblDeactivate=Utility.getLocaleValuePropByKey("lblDeactivate", locale);
	 String lblActivate=Utility.getLocaleValuePropByKey("lblActivate", locale);
	 
	@Autowired
	private I4QuestionPoolDAO i4QuestionPoolDAO;
	
	@Autowired
	private  DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private I4DistrictAccountDAO i4DistrictAccountDAO;
	
	@Autowired
	private I4QuestionSetQuestionsDAO i4QuestionSetQuestionsDAO;
	
	public String displayDomainRecords(String noOfRow, String pageNo,String sortOrder,String sortOrderType,String i4QuesStatus,String quesText,String districtIdFilter)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			boolean statusflag 	= false;
			boolean districtFlag 	= false;
			boolean allData	   	= false;
			boolean quesTextflag= false;

			
			DistrictMaster districtMaster = new DistrictMaster();
			List<I4QuestionPool> i4QuestionPools	  	=	new ArrayList<I4QuestionPool>();
			List<I4QuestionPool> i4QuesForStatus		=	new ArrayList<I4QuestionPool>();
			List<I4QuestionPool> i4QuesForQues			=	new ArrayList<I4QuestionPool>();
			List<I4QuestionPool> filterData				=	new ArrayList<I4QuestionPool>();
			List<I4QuestionPool> filterByDistrict		=	new ArrayList<I4QuestionPool>();
			List<I4QuestionPool> finalDatalist			=	new ArrayList<I4QuestionPool>();
			
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"QuestionText";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			
			i4QuestionPools = i4QuestionPoolDAO.findByCriteria(sortOrderStrVal) ;
			
			if(i4QuestionPools.size()>0)
			{
				filterData.addAll(i4QuestionPools);
				allData = true;
			}
			
			if(quesText!=null && !quesText.equalsIgnoreCase(""))
			{
				i4QuesForQues = i4QuestionPoolDAO.findi4QuesByQues(quesText);
				quesTextflag = true;
			}
			
			if(quesTextflag==true)
				filterData.retainAll(i4QuesForQues);
			else
				filterData.addAll(i4QuesForQues);
			
			if(i4QuesStatus!=null && !i4QuesStatus.equalsIgnoreCase("") && !i4QuesStatus.equalsIgnoreCase("0"))
			{
				i4QuesForStatus = i4QuestionPoolDAO.findi4QuesByStatus(i4QuesStatus);
				statusflag = true;
			}
			
			
			if(districtIdFilter!=null && !districtIdFilter.equalsIgnoreCase("") && !districtIdFilter.equalsIgnoreCase("0"))
			{
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtIdFilter), false, false);	
				filterByDistrict = i4QuestionPoolDAO.findByDistrict(districtMaster);
				
				districtFlag = true;
			}
			
			if(districtFlag)
				filterData.retainAll(filterByDistrict);
			else
				filterData.addAll(filterByDistrict);
			
			
			if(statusflag==true)
				filterData.retainAll(i4QuesForStatus);
			else
				filterData.addAll(i4QuesForStatus);
				
			
			totalRecord = filterData.size();
			if(totalRecord<end)
				end=totalRecord;
			
			finalDatalist	=	filterData.subList(start,end);
			
			dmRecords.append("<table  id='i4QuesTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			//dmRecords.append("<th width='55%'>Domain Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblQues,sortOrderFieldName,"QuestionText",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblcretedDate,sortOrderFieldName,"DateCreated",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"Status",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			dmRecords.append("<th width='15%'><span style='color:#ffffff'>"+lblAct+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(finalDatalist.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+lblNoQuestionfound1+"</td></tr>" );
			System.out.println("No of Records "+i4QuestionPools.size());

			for (I4QuestionPool i4qp: finalDatalist) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+i4qp.getQuestionText()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(i4qp.getDateCreated())+"</td>");
				dmRecords.append("<td>");
				if(i4qp.getStatus().equalsIgnoreCase("A"))
					dmRecords.append(optAct);
				else
					dmRecords.append(optInActiv);
				dmRecords.append("</td>");
				
				if(i4qp.getStatus().equalsIgnoreCase("A"))
					dmRecords.append("<td><a href='javascript:void(0);' onclick='editI4Question("+i4qp.getID()+")'>"+lblEdit+"</a> | <a href='javascript:void(0);' onclick=\"activateDeactivateQues("+i4qp.getID()+",'I')\">"+lblDeactivate+"</a></td>");
				else if(i4qp.getStatus().equalsIgnoreCase("I"))
					dmRecords.append("<td><a href='javascript:void(0);' onclick='editI4Question("+i4qp.getID()+")'>"+lblEdit+"</a> | <a href='javascript:void(0);' onclick=\"activateDeactivateQues("+i4qp.getID()+",'A')\">Activate</a></td>");
			
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public int saveI4Question(Integer quesId,String txtQuestion,String quesStatus,String districtId)
	{
		System.out.println(" ==========saveI4Question ========");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		int res = 0;
		boolean chkDup = false;
		
		
		try
		{
			UserMaster userMaster = new UserMaster();
			DistrictMaster districtMaster = new DistrictMaster();
			
			if (session.getAttribute("userMaster") != null) 
				userMaster=	(UserMaster) session.getAttribute("userMaster");
			
			if(userMaster.getEntityType()==1)
			{
				if(districtId!=null && !districtId.equals(""))
					districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			}
			else if(userMaster.getEntityType()==2)
			{
				districtMaster = userMaster.getDistrictId();
			}
			
			if(quesId==null){
				if(checkDuplicateQues(txtQuestion, districtMaster))
					chkDup = true;
			}
			
			if(chkDup==false)
			{
				String i4QuestionId=null;
				try {
					System.out.println("======================================");

					List<I4DistrictAccount> i4DistrictAccountlst = i4DistrictAccountDAO.findByDistrict(districtMaster);
					String userName = "";
					String password = "";
					
					if(i4DistrictAccountlst!=null && i4DistrictAccountlst.size()>0)
					{
						userName	=	i4DistrictAccountlst.get(0).getI4username();
						password    = 	i4DistrictAccountlst.get(0).getI4password();
					}
					
					i4QuestionId = JDigestI4API.I4_AddQuestion(userName,password,txtQuestion);
					System.out.println("i4QuestionId::: "+i4QuestionId);
					System.out.println("======================================");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				I4QuestionPool i4QuestionPool = new I4QuestionPool();
			
				if(i4QuestionId!=null)
				{
					Date date = new Date();
					i4QuestionPool.setQuestionText(txtQuestion);
					i4QuestionPool.setStatus(quesStatus);
					i4QuestionPool.setDistrictMaster(districtMaster);
					i4QuestionPool.setDateCreated(date);
					i4QuestionPool.setI4QuestionID(i4QuestionId);
					if(quesId!=null)
					{
						i4QuestionPool.setID(quesId);
					}
					i4QuestionPoolDAO.makePersistent(i4QuestionPool);
				}
			}
			else
			{
				System.out.println(" question duplicate.");
				res = 1;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return res;
	}
	
	public I4QuestionPool editI4Question(String quesId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		I4QuestionPool i4QuestionPool = null;
		
		try
		{
			i4QuestionPool = i4QuestionPoolDAO.findById(Integer.parseInt(quesId), false, false);
			List<I4QuestionSetQuestions> attchedQuesWIthQuesSetList = new ArrayList<I4QuestionSetQuestions>();
			attchedQuesWIthQuesSetList = i4QuestionSetQuestionsDAO.findByQuesId(i4QuestionPool);
			
			System.out.println("  attchedQuesWIthQuesSetList   ::::::::: "+attchedQuesWIthQuesSetList.size());
			
			if(attchedQuesWIthQuesSetList!=null && attchedQuesWIthQuesSetList.size()>0)
				i4QuestionPool = null;
	
		}catch(Exception e){
			e.printStackTrace();
		}
		return i4QuestionPool;
	}
	
	
	public boolean activateDeactivateQuestion(String quesId,String status)
	{
		/* ========  For Session time Out Error =========*/
				WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				HttpSession session = request.getSession(false);
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException(msgYrSesstionExp);
			    }
				
				
				boolean existQuesInInaciveStatus = false;
				
				
				try{
					
					I4QuestionPool i4QuestionPool = i4QuestionPoolDAO.findById(Integer.parseInt(quesId), false, false);
					
					if(status.equalsIgnoreCase("I"))
					{
						List<I4QuestionSetQuestions> attchedQuesWIthQuesSetList = new ArrayList<I4QuestionSetQuestions>();
						
						attchedQuesWIthQuesSetList = i4QuestionSetQuestionsDAO.findByQuesId(i4QuestionPool);
						
						System.out.println("  attchedQuesWIthQuesSetList   ::::::::: "+attchedQuesWIthQuesSetList.size());
						
						if(attchedQuesWIthQuesSetList!=null && attchedQuesWIthQuesSetList.size()>0)
							existQuesInInaciveStatus = true;
						else
						{
							i4QuestionPool.setStatus(status);
							i4QuestionPoolDAO.makePersistent(i4QuestionPool);
						}
						
					}else
					{
						i4QuestionPool.setStatus(status);
						i4QuestionPoolDAO.makePersistent(i4QuestionPool);
					}
					
				}catch (Exception e) 
				{
					e.printStackTrace();
				}
				return existQuesInInaciveStatus;
	}
	
	public boolean checkDuplicateQues(String quesTxt,DistrictMaster districtMaster)
	{
		boolean chkDup = false;
		List<I4QuestionPool> existQuesList = new ArrayList<I4QuestionPool>();
		
		try
		{
			existQuesList = i4QuestionPoolDAO.findExistQuestion(quesTxt,districtMaster);
			
			if(existQuesList!=null && existQuesList.size()>0)
				chkDup = true;
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println(" chkDup question :: "+chkDup);
		
		return chkDup;
	}
	
	
	
}
