package tm.services.i4;

import javax.servlet.http.HttpServletRequest;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.i4.I4QuestionSets;
import tm.bean.user.UserMaster;
import tm.services.report.CGInviteInterviewAjax;



public class ScheduleI4Thread extends Thread{

	private CGInviteInterviewAjax cGInviteInterviewAjax;
	public void setCGInviteInterviewAjax(CGInviteInterviewAjax cGInviteInterviewAjax) {
		this.cGInviteInterviewAjax = cGInviteInterviewAjax;
	}
	private HttpServletRequest request;
	private UserMaster userMaster;
	
	private TeacherDetail  teacherDetail;
	private JobOrder jobOrder;
	private I4QuestionSets i4QuestionSets;  
	

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public I4QuestionSets getI4QuestionSets() {
		return i4QuestionSets;
	}

	public void setI4QuestionSets(I4QuestionSets i4QuestionSets) {
		this.i4QuestionSets = i4QuestionSets;
	}

	public ScheduleI4Thread() {
		super();
	}
	
	public void run()
	{
		try{
			System.out.println(":::::::::Call Here::::::::::::::");
			cGInviteInterviewAjax.saveIIStatusByThread(jobOrder,teacherDetail,i4QuestionSets,userMaster,request);
			System.out.println("Call Here::::::::::::::");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
