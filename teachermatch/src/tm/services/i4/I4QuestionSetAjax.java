package tm.services.i4;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.EventDetails;
import tm.bean.i4.I4DistrictAccount;
import tm.bean.i4.I4QuestionPool;
import tm.bean.i4.I4QuestionSetQuestions;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.EventDetailsDAO;
import tm.dao.i4.I4DistrictAccountDAO;
import tm.dao.i4.I4QuestionPoolDAO;
import tm.dao.i4.I4QuestionSetQuestionsDAO;
import tm.dao.i4.I4QuestionSetsDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.services.PaginationAndSorting;
import tm.utility.Utility;

public class I4QuestionSetAjax {

	
	String locale = Utility.getValueOfPropByKey("locale");
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
     String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
     String lblcretedDate=Utility.getLocaleValuePropByKey("lblcretedDate", locale);
     String lblQuesSet=Utility.getLocaleValuePropByKey("lblQuesSet", locale);
     String optAct=Utility.getLocaleValuePropByKey("optAct", locale);
     String optInActiv=Utility.getLocaleValuePropByKey("optInActiv", locale);
     String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
     String lblNoQuestionSetfound1=Utility.getLocaleValuePropByKey("lblNoQuestionSetfound1", locale);
     String lblManageQuestions=Utility.getLocaleValuePropByKey("lblManageQuestions", locale);
     String msgSelectDistQuesSetlist=Utility.getLocaleValuePropByKey("msgSelectDistQuesSetlist", locale);
     String msgThisDistrictNoQuesSet=Utility.getLocaleValuePropByKey("msgThisDistrictNoQuesSet", locale);
     
     
	@Autowired
	private I4QuestionPoolDAO i4QuestionPoolDAO;
	
	@Autowired
	private I4QuestionSetsDAO i4QuestionSetsDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private I4QuestionSetQuestionsDAO i4QuestionSetQuestionsDAO;
	
	@Autowired
	private I4DistrictAccountDAO i4DistrictAccountDAO;
	
	@Autowired
	private EventDetailsDAO eventDetailsDAO;
	
	public String displayQuestionSet(String noOfRow, String pageNo,String sortOrder,String sortOrderType,String i4QuesSetStatus,String quesSetSearchText,String districtIdFilter)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			boolean Statusflag 	= false;
			boolean districtFlag 	= false;
			boolean allData	   	= false;
			boolean quesTextflag= false;

			DistrictMaster districtMaster = new DistrictMaster();
			
			List<I4QuestionSets> i4QuestionSets  		=	new ArrayList<I4QuestionSets>();
			List<I4QuestionSets> i4QuesForStatus		=	new ArrayList<I4QuestionSets>();
			List<I4QuestionSets> i4QuesForQues			=	new ArrayList<I4QuestionSets>();
			List<I4QuestionSets> filterData				=	new ArrayList<I4QuestionSets>();
			List<I4QuestionSets> finalDatalist			=	new ArrayList<I4QuestionSets>();
			List<I4QuestionSets> filterByDistrict		=	new ArrayList<I4QuestionSets>();
			
			
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"QuestionSetText";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			
			i4QuestionSets = i4QuestionSetsDAO.findByCriteria(sortOrderStrVal) ;
			
			if(i4QuestionSets.size()>0)
			{
				filterData.addAll(i4QuestionSets);
				allData = true;
			}
			
			if(quesSetSearchText!=null && !quesSetSearchText.equalsIgnoreCase(""))
			{
				i4QuesForQues = i4QuestionSetsDAO.findi4QuesSetByQues(quesSetSearchText);
				quesTextflag = true;
			}
			
			if(quesTextflag==true)
				filterData.retainAll(i4QuesForQues);
			else
				filterData.addAll(i4QuesForQues);
			
			if(i4QuesSetStatus!=null && !i4QuesSetStatus.equalsIgnoreCase("") && !i4QuesSetStatus.equalsIgnoreCase("0"))
			{
				i4QuesForStatus = i4QuestionSetsDAO.findi4QuesSetByStatus(i4QuesSetStatus);
				Statusflag = true;
			}
			
			if(districtIdFilter!=null && !districtIdFilter.equalsIgnoreCase("") && !districtIdFilter.equalsIgnoreCase("0"))
			{
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtIdFilter), false, false);	
				filterByDistrict = i4QuestionSetsDAO.findByDistrict(districtMaster);
				
				districtFlag = true;
			}
			
			if(districtFlag)
				filterData.retainAll(filterByDistrict);
			else
				filterData.addAll(filterByDistrict);
			
			
			
			if(Statusflag==true)
				filterData.retainAll(i4QuesForStatus);
			else
				filterData.addAll(i4QuesForStatus);
				
			
			totalRecord = filterData.size();
			if(totalRecord<end)
				end=totalRecord;
			
			System.out.println(" start :: "+start+" end "+end);
			
			finalDatalist	=	filterData.subList(start,end);
			
			dmRecords.append("<table  id='i4QuesTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			//dmRecords.append("<th width='55%'>Domain Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblQuesSet,sortOrderFieldName,"QuestionSetText",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblcretedDate,sortOrderFieldName,"DateCreated",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"Status",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			dmRecords.append("<th width='15%'><span style='color:#ffffff'>"+lblAct+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(finalDatalist.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+lblNoQuestionSetfound1+"</td></tr>" );
			System.out.println("No of Records "+finalDatalist.size());

			for (I4QuestionSets i4qp: finalDatalist) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+i4qp.getQuestionSetText()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(i4qp.getDateCreated())+"</td>");
				dmRecords.append("<td>");
				if(i4qp.getStatus().equalsIgnoreCase("A"))
					dmRecords.append(optAct);
				else
					dmRecords.append(optInActiv);
				dmRecords.append("</td>");
				
				if(i4qp.getStatus().equalsIgnoreCase("A"))
					dmRecords.append("<td><a href='javascript:void(0);' onclick='editI4Question("+i4qp.getID()+")'>"+lblEdit+"</a> | <a href='javascript:void(0);' onclick=\"activateDeactivateQues("+i4qp.getID()+",'I')\">Deactivate</a> | <a href='virtualvideoquestionSetQuestion.do?quesSetId="+i4qp.getID()+"&&districtId="+i4qp.getDistrictMaster().getDistrictId()+"'>"+lblManageQuestions+"</a></td>");
				else if(i4qp.getStatus().equalsIgnoreCase("I"))
					dmRecords.append("<td><a href='javascript:void(0);' onclick='editI4Question("+i4qp.getID()+")'>"+lblEdit+"</a> | <a href='javascript:void(0);' onclick=\"activateDeactivateQues("+i4qp.getID()+",'A')\">Activate</a> | <a href='virtualvideoquestionSetQuestion.do?quesSetId="+i4qp.getID()+"&&districtId="+i4qp.getDistrictMaster().getDistrictId()+"'>"+lblManageQuestions+"</a></td>");
			
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public int saveI4QuestionSet(Integer quesSetId,String quesSetName,String quesSetStatus,String districtId)
	{
		System.out.println(" ==========saveI4Question ========");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		int res = 0;
		boolean chkDup = false;
		
		try
		{
			UserMaster userMaster = new UserMaster();
			
			if (session.getAttribute("userMaster") != null) 
			{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
			}
			
			DistrictMaster districtMaster = new DistrictMaster();
			
			if (session.getAttribute("userMaster") != null) 
				userMaster=	(UserMaster) session.getAttribute("userMaster");
			
			if(userMaster.getEntityType()==1)
			{
				if(districtId!=null && !districtId.equals(""))
					districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			}
			else if(userMaster.getEntityType()==2)
			{
				districtMaster = userMaster.getDistrictId();
			}
			
			if(quesSetId==null){
				if(checkDuplicateQuesSet(quesSetName, districtMaster))
					chkDup = true;
			}
			
			
			if(chkDup==false)
			{
				String i4QuestionSetId=null;
				try {
					List<I4DistrictAccount> i4DistrictAccountlst = i4DistrictAccountDAO.findByDistrict(districtMaster);
					String userName = "";
					String password = "";
					if(i4DistrictAccountlst!=null && i4DistrictAccountlst.size()>0)
					{
						userName	=	i4DistrictAccountlst.get(0).getI4username();
						password    = 	i4DistrictAccountlst.get(0).getI4password();
					}
					
					System.out.println("======================================");
					i4QuestionSetId = JDigestI4API.I4_AddQuestionSet(userName,password,quesSetName);
					System.out.println("======================================");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				I4QuestionSets i4QuestionSets = new I4QuestionSets();
				
				if(i4QuestionSetId!=null)
				{
					Date date = new Date();
					
					i4QuestionSets.setQuestionSetText(quesSetName);
					i4QuestionSets.setStatus(quesSetStatus);
					i4QuestionSets.setDistrictMaster(districtMaster);
					i4QuestionSets.setDateCreated(date);
					i4QuestionSets.setI4QuestionSetID(i4QuestionSetId);
					System.out.println("i4QuestionId::: "+i4QuestionSetId);
					
					if(quesSetId!=null)
					{
						i4QuestionSets.setID(quesSetId);
					}
					i4QuestionSetsDAO.makePersistent(i4QuestionSets);
				}
				
			}
			else
			{
				System.out.println(" Question set is duplicate.");
				res = 1;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return res;
	}
	
	public I4QuestionSets editI4QuestionSet(String quesId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		I4QuestionSets i4QuestionSets = null;
		
		try
		{
			i4QuestionSets = i4QuestionSetsDAO.findById(Integer.parseInt(quesId), false, false);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return i4QuestionSets;
	}
	
	
	public boolean activateDeactivateQuestionSet(String quesSetId,String status)
	{
		/* ========  For Session time Out Error =========*/
				WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				HttpSession session = request.getSession(false);
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException(msgYrSesstionExp);
			    }
				
				try{
					I4QuestionSets i4QuestionSets = i4QuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
					i4QuestionSets.setStatus(status);
					i4QuestionSetsDAO.makePersistent(i4QuestionSets);
					
				}catch (Exception e) 
				{
					e.printStackTrace();
					return false;
				}
				return true;
	}
	
	
	//Auto Suggest of Question Set 
	/*public List<I4QuestionSets> getFieldOfQuesList(String quesSetName)
	{
		System.out.println(" ============ getFieldOfQuesList ===========");
		
		 ========  For Session time Out Error =========
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<I4QuestionSets> questionSetsList =  new ArrayList<I4QuestionSets>();
		List<I4QuestionSets> fieldOfSchoolList1 = null;
		List<I4QuestionSets> fieldOfSchoolList2 = null;
		
		try 
		{
			Criterion criterion = Restrictions.like("QuestionSetText", quesSetName,MatchMode.START);
			
			questionSetsList = i4QuestionSetsDAO.findWithLimit(Order.asc("QuestionSetText"), 0, 25, criterion);
				if(schoolIds.size()>0){
					if(quesSetName.trim()!=null && quesSetName.trim().length()>0){
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterionSchoolIds);
						
						Criterion criterion2 = Restrictions.ilike("schoolName","% "+quesSetName.trim()+"%" );
						fieldOfSchoolList2 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion2,criterionSchoolIds);
						
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<SchoolMaster> setSchool = new LinkedHashSet<SchoolMaster>(schoolMasterList);
						schoolMasterList = new ArrayList<SchoolMaster>(new LinkedHashSet<SchoolMaster>(setSchool));
					}else{
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterionSchoolIds);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
			
			System.out.println(" question set size :: "+questionSetsList.size());
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return questionSetsList;
	}*/
	
	
	public List<I4QuestionSets> getFieldOfQuesList(String quesSetName,String districtId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		List<I4QuestionSets> districtMasterList =  new ArrayList<I4QuestionSets>();
		List<I4QuestionSets> fieldOfDistrictList1 = null;
		List<I4QuestionSets> fieldOfDistrictList2 = null;
		
		List i4QuestionSetQuestions = null;
		List<I4QuestionSets> quesSetInQSQ = new ArrayList<I4QuestionSets>();
		List<I4QuestionSets> finslList = new ArrayList<I4QuestionSets>();
		
		
		boolean flag_1=false;
		boolean flag_2=false;
		
		try{
			
			if(districtId!=null && !districtId.equals(""))
			{
				DistrictMaster districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
				Criterion crDistrict = Restrictions.eq("districtMaster",districtMaster);
				
				
				Criterion criterion = Restrictions.like("QuestionSetText", quesSetName.trim(),MatchMode.START);
				if(quesSetName.trim()!=null && quesSetName.trim().length()>0){
					fieldOfDistrictList1 = i4QuestionSetsDAO.findWithLimit(Order.asc("QuestionSetText"), 0, 25, criterion,crDistrict);
					
					Criterion criterion2 = Restrictions.ilike("QuestionSetText","% "+quesSetName.trim()+"%" );
					fieldOfDistrictList2 = i4QuestionSetsDAO.findWithLimit(Order.asc("QuestionSetText"), 0, 25, criterion2,crDistrict);
					
					districtMasterList.addAll(fieldOfDistrictList1);
					districtMasterList.addAll(fieldOfDistrictList2);
					Set<I4QuestionSets> setDistrict = new LinkedHashSet<I4QuestionSets>(districtMasterList);
					districtMasterList = new ArrayList<I4QuestionSets>(new LinkedHashSet<I4QuestionSets>(setDistrict));
					
				}else{
					fieldOfDistrictList1 = i4QuestionSetsDAO.findWithLimit(Order.asc("QuestionSetText"), 0, 25,crDistrict);
					districtMasterList.addAll(fieldOfDistrictList1);
				}
			}
			
			if(districtMasterList!=null && districtMasterList.size()>0)
				flag_1=true;

			
			
			ProjectionList projList = Projections.projectionList();
		    projList.add(Projections.groupProperty("questionSets"));
			
			i4QuestionSetQuestions = i4QuestionSetQuestionsDAO.findByCriteriaProjection(projList);
			
			for(Object IQSQ:i4QuestionSetQuestions)
			{
				I4QuestionSets i4QObj =  (I4QuestionSets)IQSQ;
				quesSetInQSQ.add(i4QObj);
			}
			
			if(quesSetInQSQ.size()>0)
				flag_2=true;
			
			if(flag_1)
				finslList.addAll(districtMasterList);
			
			if(flag_2)
				finslList.retainAll(quesSetInQSQ);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	
		return finslList;
	}
	
	public boolean checkDuplicateQuesSet(String quesTxt,DistrictMaster districtMaster)
	{
		boolean chkDup = false;
		List<I4QuestionSets> existQuesList = new ArrayList<I4QuestionSets>();
		
		try
		{
			existQuesList = i4QuestionSetsDAO.findExistQuestionSet(quesTxt,districtMaster);
			
			if(existQuesList!=null && existQuesList.size()>0)
				chkDup = true;
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return chkDup;
	}
	
	public String getQuestionSetByDistrict(String districtId,String eventId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer sb =	new StringBuffer();
		
		List<I4QuestionSets> i4QuestionSetsList = new ArrayList<I4QuestionSets>(); 
		EventDetails eventDetails = null;
		I4QuestionSets exitQuesSet = null;
		
		
		try
		{
			if(districtId!=null && !districtId.equals(""))
			{
				DistrictMaster districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
				
				if(eventId!=null && !eventId.equals("") && !eventId.equals("0"))
					eventDetails = eventDetailsDAO.findById(Integer.parseInt(eventId), false, false);
				
				if(eventDetails!=null)
					exitQuesSet = eventDetails.getI4QuestionSets();
				else if(districtMaster!=null)
					exitQuesSet = districtMaster.getI4QuestionSets();
				
				i4QuestionSetsList = i4QuestionSetsDAO.findByDistrict(districtMaster);
				
				if(i4QuestionSetsList.size()>0){
					sb.append("<option value=''>"+msgSelectDistQuesSetlist+"</option>");
					for(I4QuestionSets objQuestionSet:i4QuestionSetsList){
						if(exitQuesSet!=null && objQuestionSet.equals(exitQuesSet))
							sb.append("<option id='"+objQuestionSet.getID()+"' value='"+objQuestionSet.getID()+"' selected='selected'>"+objQuestionSet.getQuestionSetText()+"</option>");
						else
							sb.append("<option id='"+objQuestionSet.getID()+"' value='"+objQuestionSet.getID()+"' >"+objQuestionSet.getQuestionSetText()+"</option>");
					}
				}
				else
					sb.append("<option value=''>"+msgThisDistrictNoQuesSet+"</option>");
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	
}
