
package tm.services.i4;

import java.io.IOException;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import tm.utility.Utility;

public class JDigestI4API {

	public static String endpoint ="https://liveapi.interview4.com";
//	public static String endpoint ="https://teachermatch.interview4.com";
  //public static String callback_url = "http://122.160.116.109:8080";
	public static String callback_url = Utility.getValueOfPropByKey("basePath");//"https://titan.teachermatch.org";
//	public static String callback_url = "http://122.160.116.109:8080";
//	public static String callback_url = "https://titan.teachermatch.org";
	public static void main(String[] args) {

	//	I4_AddOrUpdateCandidate("Sanon", "Roj", "sanon_roj@netsutra.com", "new");
	//	I4_AddQuestion("Sanjeev_Arora1","teachermatch","State your qualifications?");
	//	I4_AddQuestion("Sanjeev_Arora1","teachermatch","Can you think of a situation where you would tell less than the whole truth?");
	//	I4_AddQuestionSet("Sanjeev_Arora1","teachermatch","Lab Codinator");
	//	I4_AddQuestionToQuestionSet("Sanjeev_Arora1","teachermatch","970","3226");
		
		///candidate/18455/interview/22958
		//inviteCandidateForInterview("22958","18455","966");
		
	//	inviteCandidateForInterviewResend("Noble_User1", "teachermatch", "23166", "18629");
		
	}

	public static String[] I4_AddOrUpdateCandidate(String username,String password,String firstname,String lastname,String email,String I4CandidateId) {
		/*
		firstname 
    		- string
    		- Required for 'new'
		lastname 
    		- string
    		- Required for 'new' 
		email 
    		- string
    		- Required for 'new'
		 */
		
		String[] res = new String[3];
		
		Map<String,String> map = new HashMap<String, String>();
		map.put("username", username);
		map.put("password", password);
		map.put("firstname", firstname);
		map.put("lastname", lastname);
		map.put("email", email);
		/*Pass "new" to add a new candidate
		 *Pass Numeric id of candidate to edit the candidate*/
		
		//String id = manageCandidate(map,"new");
		if(I4CandidateId==null || I4CandidateId.equals(""))
			I4CandidateId="new";
		
		res = manageCandidate(map,I4CandidateId);
		
		System.out.println("id:: "+res[0]+"uname:"+res[1]+"password:"+res[2]);
		
		return res;
	}

	public static String[] manageCandidate(Map<String,String> map,String candidateId)
	{
		//String urlStr = "https://liveapi.interview4.com/candidate/new";
		String urlStr = endpoint+"/candidate/"+candidateId;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPut httpPut = new HttpPut(urlStr);
		String res[] = new String[3];

		try {
			CloseableHttpResponse response1 = httpclient.execute(httpPut);
			System.out.println("response1.getStatusLine()::" + response1.getStatusLine());

			String authorizationHeader = getAuthHeader(response1, httpPut);

			System.out.println(authorizationHeader);
			httpPut.addHeader("Authorization", authorizationHeader);

			CloseableHttpClient httpclient1 = HttpClients.createDefault();
			List<NameValuePair> nmp = new ArrayList<NameValuePair>();

			/*nmp.add(new BasicNameValuePair("username", "Sanjeev_Arora1"));
			nmp.add(new BasicNameValuePair("password", "teachermatch"));*/
			for (Map.Entry<String, String> entry : map.entrySet())
				nmp.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));

			httpPut.setEntity(new UrlEncodedFormEntity(nmp));
			response1 = httpclient1.execute(httpPut);
			System.out.println("response1.getStatusLine()::" + response1.getStatusLine());

			HttpEntity entity = response1.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			// System.out.println(responseString);
			JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( responseString );
			
	/////////// Parse Json String and get candidate username, password ////////////////
			System.out.println(" JSON String :: "+jsonRequest.toString());
	//	String id = null;
	//	String username = null;
	//	String password = null;
		
		try
		{
			res[0] = jsonRequest.getString("id");
			res[1] = jsonRequest.getString("username");
			res[2] = jsonRequest.getString("password");
			
			System.out.println(">>>>>>>>>>>>>>>>>>id:"+res[0]+"uname:"+res[1]+"password:"+res[2]);
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
			
		///////////////////////////////////////////////////////////////////////////////////
			System.out.println("candidate_id:: "+res[0]);
			
			return res;
		
		} catch (IOException ex) {
			ex.printStackTrace();
			Logger.getLogger(JDigestI4API.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;

	}

	
	
	public static String inviteCandidateForInterview(String username,String password,String interviewId,String candidateId,String quesSetId,String expiryDate,String timeAllowedPerQuestion)
	{
		String urlStr = endpoint+"/candidate/"+candidateId+"/interview/"+interviewId;
		 //String urlStr = "https://liveapi.interview4.com/candidate/16354/interview/new";
		System.out.println("urlStr::: "+urlStr);
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPut httpPut = new HttpPut(urlStr);

		try {
			CloseableHttpResponse response1 = httpclient.execute(httpPut);
			System.out.println("response1.getStatusLine(): 1 :" + response1.getStatusLine());

			String authorizationHeader = getAuthHeader(response1, httpPut);

			System.out.println(authorizationHeader);
			httpPut.addHeader("Authorization", authorizationHeader);

			CloseableHttpClient httpclient1 = HttpClients.createDefault();
			List<NameValuePair> nmp = new ArrayList<NameValuePair>();

			
			
			System.out.println("final parameter expiryDate"+expiryDate+"timeAllowedPerQuestion"+timeAllowedPerQuestion+"quesSetId"+quesSetId);
			
			/*nmp.add(new BasicNameValuePair("username", "Sanjeev_Arora1"));
			nmp.add(new BasicNameValuePair("password", "teachermatch"));*/
			nmp.add(new BasicNameValuePair("username", username));
			nmp.add(new BasicNameValuePair("password", password));
	        nmp.add(new BasicNameValuePair("interview_type", "virtual"));
			nmp.add(new BasicNameValuePair("question_set_id", quesSetId));
			nmp.add(new BasicNameValuePair("send_email", "no"));
			System.out.println(" callback_url >>>>>>> "+callback_url+"/services/callback/postCandidateVideoDataFromI4ToTM");
			nmp.add(new BasicNameValuePair("callback_url", callback_url+"/services/callback/postCandidateVideoDataFromI4ToTM"));
			
			if(expiryDate!=null && !expiryDate.equals(""))
				nmp.add(new BasicNameValuePair("days_to_complete", expiryDate));
			
			if(timeAllowedPerQuestion!=null && !timeAllowedPerQuestion.equals("") && !timeAllowedPerQuestion.equals("0"))
				nmp.add(new BasicNameValuePair("max_question_length", timeAllowedPerQuestion));
			
			UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nmp);
			System.out.println("NMP:::: "+nmp.toString());
			httpPut.setEntity(urlEncodedFormEntity);
			response1 = httpclient1.execute(httpPut);
			System.out.println("HTTPPut:::: "+httpPut.getURI());
		
			System.out.println("response1.getStatusLine(): 2 :" + response1.getStatusLine());

			HttpEntity entity = response1.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			// System.out.println(responseString);
			JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( responseString ); 
			System.out.println(jsonRequest.toString());
			String url = null;
			try 
			{
				if(interviewId.equalsIgnoreCase("new")){
					if(jsonRequest.getString("message").equals("success"))
						url = jsonRequest.getString("location");
				}else{
					return null;
				}
				
			} catch (Exception e) {e.printStackTrace();}
			System.out.println(" invite url :: "+url);
			return url;
		} catch (IOException ex) {
			ex.printStackTrace();
			Logger.getLogger(JDigestI4API.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;

	}
	
	
	
	public static String inviteCandidateForInterviewResend(String username,String password,String interviewId,String candidateId)
	{
		String urlStr = endpoint+"/candidate/"+candidateId+"/interview/"+interviewId+"/resend";
		 //String urlStr = "https://liveapi.interview4.com/candidate/16354/interview/new";
		System.out.println("urlStr::: "+urlStr);
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPut httpPut = new HttpPut(urlStr);

		try {
			CloseableHttpResponse response1 = httpclient.execute(httpPut);
			System.out.println("response1.getStatusLine(): 1 :" + response1.getStatusLine());

			String authorizationHeader = getAuthHeader(response1, httpPut);

			System.out.println(authorizationHeader);
			httpPut.addHeader("Authorization", authorizationHeader);

			CloseableHttpClient httpclient1 = HttpClients.createDefault();
			List<NameValuePair> nmp = new ArrayList<NameValuePair>();

			/*nmp.add(new BasicNameValuePair("username", "Sanjeev_Arora1"));
			nmp.add(new BasicNameValuePair("password", "teachermatch"));*/
			nmp.add(new BasicNameValuePair("username", username));
			nmp.add(new BasicNameValuePair("password", password));
	        nmp.add(new BasicNameValuePair("interview_type", "virtual"));
			//nmp.add(new BasicNameValuePair("question_set_id", quesSetId));
			nmp.add(new BasicNameValuePair("send_email", "no"));
			//nmp.add(new BasicNameValuePair("callback_url", "http://192.168.1.165/"));
			System.out.println(" callback_url >>>>>>> "+callback_url+"/services/callback/postCandidateVideoDataFromI4ToTM");
			nmp.add(new BasicNameValuePair("callback_url", callback_url+"/services/callback/postCandidateVideoDataFromI4ToTM"));
			
			//nmp.add(new BasicNameValuePair("days_to_complete", expiryDate));
			//nmp.add(new BasicNameValuePair("max_question_length", timeAllowedPerQuestion));
			
			
			//nmp.add(new BasicNameValuePair("max_question_length", "200"));
			/*nmp.add(new BasicNameValuePair("interview_id",interviewId));
			nmp.add(new BasicNameValuePair("candidate_id", candidateId));
			nmp.add(new BasicNameValuePair("question_set_id", "8"));
			nmp.add(new BasicNameValuePair("days_to_complete", "8"));
			nmp.add(new BasicNameValuePair("max_question_length", "8"));
			nmp.add(new BasicNameValuePair("interview_type ", "virtual"));
			nmp.add(new BasicNameValuePair("send_email", "no"));*/
			
			/*for (Map.Entry<String, String> entry : map.entrySet())
				nmp.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));*/
			 
			httpPut.setEntity(new UrlEncodedFormEntity(nmp));
			response1 = httpclient1.execute(httpPut);
			System.out.println("response1.getStatusLine(): 2 :" + response1.getStatusLine());

			UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nmp);
			httpPut.setEntity(urlEncodedFormEntity);
			response1 = httpclient1.execute(httpPut);
			System.out.println("HTTPPut:::: "+httpPut.getURI());
			
			HttpEntity entity = response1.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			// System.out.println(responseString);
			JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( responseString ); 
			System.out.println(jsonRequest.toString());
			String url = null;
			try 
			{
				if(interviewId.equalsIgnoreCase("new"))
				{
				if(jsonRequest.getString("message").equals("success"))
					url = jsonRequest.getString("location");
				}else
				{
					return null;
				}
				
			} catch (Exception e) {e.printStackTrace();}
			System.out.println(" invite url :: "+url);
			return url;
		} catch (IOException ex) {
			ex.printStackTrace();
			Logger.getLogger(JDigestI4API.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;

	}
	
	
	
	
	public static String I4_AddQuestion(String username,String password,String QuestionText) {

		String urlStr = endpoint+"/question/new";
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPut httpPut = new HttpPut(urlStr);

		try {
			CloseableHttpResponse response1 = httpclient.execute(httpPut);
			System.out.println("response1.getStatusLine()::" + response1.getStatusLine());

			String authorizationHeader = getAuthHeader(response1, httpPut);

			System.out.println(authorizationHeader);
			httpPut.addHeader("Authorization", authorizationHeader);

			CloseableHttpClient httpclient1 = HttpClients.createDefault();
			List<NameValuePair> nmp = new ArrayList<NameValuePair>();

			nmp.add(new BasicNameValuePair("username", username));
			nmp.add(new BasicNameValuePair("password", password));
			nmp.add(new BasicNameValuePair("question_text", QuestionText));

			httpPut.setEntity(new UrlEncodedFormEntity(nmp));
			response1 = httpclient1.execute(httpPut);
			System.out.println("response1.getStatusLine()::" + response1.getStatusLine());

			HttpEntity entity = response1.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( responseString ); 
			System.out.println(jsonRequest.toString());
			//{"message":"success","location":"/question/3225","question":{"id":3225,"question_text":"State your address?"}}
			JSONObject jQuestion = null;
			String id = null;
			try {jQuestion = jsonRequest.getJSONObject("question");
			id = jQuestion.getString("id");} catch (Exception e) {e.printStackTrace();}
			System.out.println("question_id:: "+id);
			return id;
		} catch (IOException ex) {
			ex.printStackTrace();
			Logger.getLogger(JDigestI4API.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	public static String I4_AddQuestionSet(String username,String password,String QuestionSetName) {

		String urlStr = endpoint+"/question_set/new";
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPut httpPut = new HttpPut(urlStr);

		try {
			CloseableHttpResponse response1 = httpclient.execute(httpPut);
			System.out.println("response1.getStatusLine()::" + response1.getStatusLine());

			String authorizationHeader = getAuthHeader(response1, httpPut);
			System.out.println(authorizationHeader);
			httpPut.addHeader("Authorization", authorizationHeader);

			CloseableHttpClient httpclient1 = HttpClients.createDefault();
			List<NameValuePair> nmp = new ArrayList<NameValuePair>();
			nmp.add(new BasicNameValuePair("username", username));
			nmp.add(new BasicNameValuePair("password", password));
			nmp.add(new BasicNameValuePair("question_set_name", QuestionSetName));

			httpPut.setEntity(new UrlEncodedFormEntity(nmp));
			response1 = httpclient1.execute(httpPut);
			System.out.println("response1.getStatusLine()::" + response1.getStatusLine());

			HttpEntity entity = response1.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			//System.out.println(responseString);
			//{"message":"success","location":"/question_set/949","question_set":{"id":949,"name":"Java/J2EE position"}}
			JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( responseString ); 
			System.out.println(jsonRequest.toString());
			
			JSONObject jQuestion = null;
			String id = null;
			try {jQuestion = jsonRequest.getJSONObject("question_set");
			id = jQuestion.getString("id");} catch (Exception e) {e.printStackTrace();}
			System.out.println("question_set_id:: "+id);
			return id;
		} catch (IOException ex) {
			ex.printStackTrace();
			Logger.getLogger(JDigestI4API.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
	
	public static String I4_AddQuestionToQuestionSet(String username,String password,String questionSetId, String questionId) {
		
		String urlStr = endpoint+"/question_set/"+questionSetId+"/question/"+questionId;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPut httpPut = new HttpPut(urlStr);

		try {
			CloseableHttpResponse response1 = httpclient.execute(httpPut);
			System.out.println("response1.getStatusLine()::" + response1.getStatusLine());

			String authorizationHeader = getAuthHeader(response1, httpPut);

			System.out.println(authorizationHeader);
			httpPut.addHeader("Authorization", authorizationHeader);

			CloseableHttpClient httpclient1 = HttpClients.createDefault();
			List<NameValuePair> nmp = new ArrayList<NameValuePair>();
			nmp.add(new BasicNameValuePair("username", username));
			nmp.add(new BasicNameValuePair("password", password));

			httpPut.setEntity(new UrlEncodedFormEntity(nmp));
			response1 = httpclient1.execute(httpPut);
			System.out.println("response1.getStatusLine()::" + response1.getStatusLine());

			HttpEntity entity = response1.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			//System.out.println(responseString);
			//{"message":"success"}
			JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( responseString ); 
			System.out.println(jsonRequest.toString());
			
			String id = null;
			try {id = jsonRequest.getString("message");} catch (Exception e) {e.printStackTrace();}
			System.out.println("message:: "+id);
			return id;
		} catch (IOException ex) {
			ex.printStackTrace();
			Logger.getLogger(JDigestI4API.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	
	
	
	
	private static String getAuthHeader(CloseableHttpResponse response1,HttpRequestBase httpMethod)
	{
		String realm = "";
		String nonce = "";
		String qop = "";
		String opaque = "";

		Header wwAuthHeader = response1.getFirstHeader("WWW-Authenticate");

		for (HeaderElement element : wwAuthHeader.getElements()) {
			System.out.println(element.getName() + ": " + element.getValue());
			if (element.getName() != null) {
				if (element.getName().equals("Digest realm")) {
					realm = element.getValue();
				}
				if (element.getName().equals("nonce")) {
					nonce = element.getValue();
				}
				if (element.getName().equals("qop")) {
					qop = element.getValue();
				}
				if (element.getName().equals("opaque")) {
					opaque = element.getValue();
				}
			}
		}

		String uname = "sarora@teachermatch.org";
		String pswd = "fdf2c04b0a46311f8bce121c23cbdb10113ca35c";
		String uri = httpMethod.getURI().getPath();
		String A1 = uname + ":" + realm + ":" + pswd;
		System.out.println("A1::" + A1);
		A1 = toMD5(A1);
		String A2 = httpMethod.getMethod() + ":" + uri;
		System.out.println("A2::" + A2);
		A2 = toMD5(A2);
		String cnonce = Integer.toString(Math.abs(new Random().nextInt()));
		String ncvalue = "00000001";
		String response = A1 + ":" + nonce + ":" + ncvalue + ":" + cnonce + ":" + qop + ":" + A2;

		System.out.println("digest::" + response);
		response = toMD5(response);

		String authorizationHeader = "Digest username=\"" + uname + "\", realm=\"";
		authorizationHeader += realm + "\", nonce=\"" + nonce + "\",";
		authorizationHeader += " uri=\"" + uri + "\", cnonce=\"" + cnonce;
		authorizationHeader += "\", nc=" + ncvalue + ", response=\"" + response + "\", qop=" + qop;
		authorizationHeader += ", opaque=\"" + opaque + "\"";

		return authorizationHeader;
	}

	public static String toMD5(String data) {
		String ret = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data.getBytes());

			byte byteData[] = md.digest();
			//convert the byte to hex format method 1
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			ret = sb.toString();
		} catch (NoSuchAlgorithmException ex) {

		}
		return ret;
	}

	public static String toMD51(String string) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(string.getBytes());
			byte[] digest = md5.digest();
			string = byteArrToHexString(digest);
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		}
		return string;
	}

	private static String byteArrToHexString(byte[] bArr) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < bArr.length; i++) {
			int unsigned = bArr[i] & 0xff;
			if (unsigned < 0x10) {
				sb.append("0");
			}
			sb.append(Integer.toHexString((unsigned)));
		}
		return sb.toString();
	}
	
	public static String I4_DeleteQuestionToQuestionSet(String username,String password,String questionSetId, String questionId) {
		
		String urlStr = endpoint+"/question_set/"+questionSetId+"/question/"+questionId;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		//HttpPut httpPut = new HttpPut(urlStr);
		//HttpDelete httpDelete = new HttpDelete(urlStr);
		HttpDeleteWithBody httpDelete = new HttpDeleteWithBody(urlStr);

		try {
			CloseableHttpResponse response1 = httpclient.execute(httpDelete);
			System.out.println("response1.getStatusLine()::" + response1.getStatusLine());

			String authorizationHeader = getAuthHeader(response1, httpDelete);

			System.out.println(authorizationHeader);
			httpDelete.addHeader("Authorization", authorizationHeader);

			CloseableHttpClient httpclient1 = HttpClients.createDefault();
			List<NameValuePair> nmp = new ArrayList<NameValuePair>();
			nmp.add(new BasicNameValuePair("username", username));
			nmp.add(new BasicNameValuePair("password", password));

			System.out.println("username -----> "+username+" password -------> "+password);
	        
			//httpDelete.setEntity(input);
			httpDelete.setEntity(new UrlEncodedFormEntity(nmp));
			response1 = httpclient1.execute(httpDelete);
			System.out.println(" response1 :: "+response1);
			System.out.println("response1.getStatusLine()::" + response1.getStatusLine());

			HttpEntity entity = response1.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			//System.out.println(responseString);
			//{"message":"success"}
			JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( responseString ); 
			System.out.println(jsonRequest.toString());
			
			String id = null;
			try {id = jsonRequest.getString("message");} catch (Exception e) {e.printStackTrace();}
			System.out.println("message:: "+id);
			return id;
		} catch (IOException ex) {
			ex.printStackTrace();
			Logger.getLogger(JDigestI4API.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	
	
	/*public static String I4_AddQuestionToQuestionSetOrder(String username,String password,String questionSetId, String questionId, int questionOrder) {
		
		String urlStr = endpoint+"/question_set/"+questionSetId+"/question/"+questionId+"/question_order/"+questionOrder;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPut httpPut = new HttpPut(urlStr);

		try {
			CloseableHttpResponse response1 = httpclient.execute(httpPut);
			System.out.println("response1.getStatusLine()::" + response1.getStatusLine());

			String authorizationHeader = getAuthHeader(response1, httpPut);

			System.out.println(authorizationHeader);
			httpPut.addHeader("Authorization", authorizationHeader);

			CloseableHttpClient httpclient1 = HttpClients.createDefault();
			List<NameValuePair> nmp = new ArrayList<NameValuePair>();
			nmp.add(new BasicNameValuePair("username", username));
			nmp.add(new BasicNameValuePair("password", password));

			httpPut.setEntity(new UrlEncodedFormEntity(nmp));
			response1 = httpclient1.execute(httpPut);
			System.out.println("response1.getStatusLine()::" + response1.getStatusLine());

			HttpEntity entity = response1.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			//System.out.println(responseString);
			//{"message":"success"}
			JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( responseString ); 
			System.out.println(jsonRequest.toString());
			
			String id = null;
			try {id = jsonRequest.getString("message");} catch (Exception e) {e.printStackTrace();}
			System.out.println("message:: "+id);
			return id;
		} catch (IOException ex) {
			ex.printStackTrace();
			Logger.getLogger(JDigestI4API.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}*/
	
	

 static class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase {
    public static final String METHOD_NAME = "DELETE";
 
    public String getMethod() {
        return METHOD_NAME;
    }
 
    public HttpDeleteWithBody(final String uri) {
        super();
        setURI(URI.create(uri));
    }
 
    public HttpDeleteWithBody(final URI uri) {
        super();
        setURI(uri);
    }
 
    public HttpDeleteWithBody() {
        super();
    }
}

}
