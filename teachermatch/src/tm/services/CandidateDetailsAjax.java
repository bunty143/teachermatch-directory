package tm.services;

/* @Author: Gagan 
 * @Discription: It is used to displayTeacherGrid in teacherinfo page
 */
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.EmployeeMaster;
import tm.bean.JobCategoryTransaction;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.SchoolWiseCandidateStatus;
import tm.bean.TeacherAchievementScore;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentStatusDetail;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherEPIChangedStatusLog;
import tm.bean.TeacherExperience;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherSecondaryStatus;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.assessment.TeacherWiseAssessmentTime;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DegreeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.master.UniversityMaster;
import tm.bean.user.UserMaster;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobCategoryTransactionDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.SchoolWiseCandidateStatusDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAchievementScoreDAO;
import tm.dao.TeacherAssessmentDetailDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherEPIChagedStatusLogDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.TeacherSecondaryStatusDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.assessment.TeacherWiseAssessmentTimeDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DegreeMasterDAO;
import tm.dao.master.DemoClassScheduleDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.SecondaryStatusMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.TeacherAnswerDetailsForDistrictSpecificQuestionsDAO;
import tm.dao.master.UniversityMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.service.jobapplication.JobApplicationServiceAjax;
import tm.service.profile.SelfServiceCandidateProfileService;
import tm.services.report.CandidateGridService;
import tm.servlet.WorkThreadServlet;
import tm.utility.TeacherDetailComparatorLastNameAsc;
import tm.utility.TeacherDetailComparatorLastNameDesc;
import tm.utility.TeacherDetailComparatorStatusAsc;
import tm.utility.TeacherDetailComparatorStatusDesc;
import tm.utility.TestTool;
import tm.utility.Utility;

public class CandidateDetailsAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private CommonService commonService;
	
	@Autowired 
	private SchoolWiseCandidateStatusDAO schoolWiseCandidateStatusDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO;
	
	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;

	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;

	@Autowired
	private StateMasterDAO stateMasterDAO;

	@Autowired
	private StatusMasterDAO statusMasterDAO;

	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO; 

	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	@Autowired
	private SecondaryStatusMasterDAO secondaryStatusMasterDAO;
	
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO)
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	public void setJobCertificationDAO(JobCertificationDAO jobCertificationDAO) {
		this.jobCertificationDAO = jobCertificationDAO;
	}
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO) 
	{
		this.teacherExperienceDAO = teacherExperienceDAO;
	}	

	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) 
	{
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	@Autowired
	private DegreeMasterDAO degreeMasterDAO;
	public void setDegreeMasterDAO(DegreeMasterDAO degreeMasterDAO) {
		this.degreeMasterDAO = degreeMasterDAO;
	}

	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;
	public void setTeacherPreferenceDAO(
			TeacherPreferenceDAO teacherPreferenceDAO) {
		this.teacherPreferenceDAO = teacherPreferenceDAO;
	}
	@Autowired
	private UniversityMasterDAO universityMasterDAO;
	public void setUniversityMasterDAO(UniversityMasterDAO universityMasterDAO) {
		this.universityMasterDAO = universityMasterDAO;
	}
	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	public void setTeacherAcademicsDAO(TeacherAcademicsDAO teacherAcademicsDAO) {
		this.teacherAcademicsDAO = teacherAcademicsDAO;
	}

	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(
			JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	public void setSubjectMasterDAO(SubjectMasterDAO subjectMasterDAO) {
		this.subjectMasterDAO = subjectMasterDAO;
	}
	
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}
	
	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	public void setDistrictSpecificQuestionsDAO(
			DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO) {
		this.districtSpecificQuestionsDAO = districtSpecificQuestionsDAO;
	}
	
	@Autowired
	private TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	public void setTeacherAnswerDetailsForDistrictSpecificQuestionsDAO(
			TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO) {
		this.teacherAnswerDetailsForDistrictSpecificQuestionsDAO = teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	}
	
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	
	@Autowired
	private TeacherAchievementScoreDAO teacherAchievementScoreDAO;

	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;

	@Autowired
	private DemoClassScheduleDAO demoClassScheduleDAO;
	
	@Autowired
	private TeacherSecondaryStatusDAO teacherSecondaryStatusDAO;

	@Autowired
	private TeacherWiseAssessmentTimeDAO teacherWiseAssessmentTimeDAO;
	public void setTeacherWiseAssessmentTimeDAO(TeacherWiseAssessmentTimeDAO teacherWiseAssessmentTimeDAO)
	{
		this.teacherWiseAssessmentTimeDAO = teacherWiseAssessmentTimeDAO;
	}
	
	@Autowired
	private TeacherAssessmentDetailDAO teacherAssessmentDetailDAO;
	public void setTeacherAssessmentDetailDAO(TeacherAssessmentDetailDAO teacherAssessmentDetailDAO)
	{
		this.teacherAssessmentDetailDAO = teacherAssessmentDetailDAO;
	}
	
	@Autowired
	private TeacherEPIChagedStatusLogDAO teacherEPIChagedStatusLogDAO;
	
	@Autowired
	private SelfServiceCandidateProfileService selfServiceCandidateProfileService;
	
	//public String displayTeacherGrid(/*String firstName,String lastName, String emailAddress,*/String regionId,String degreeId,String universityId,String normScoreSelectVal,String 
	//		normScoreVal,String CGPA,String CGPASelectVal,String candidateStatus,String stateId,String internalCandidate,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String districtId,String schoolId,String fromDate,String toDate,String certType,String references,String resume,String fitScoreSelectVal,String YOTESelectVal,String AScoreSelectVal,String LRScoreSelectVal,String canServeAsSubTeacherYes,String canServeAsSubTeacherNo,String canServeAsSubTeacherDA,String TFAA,String TFAC,String TFAN,String fitScoreVal,String YOTE,String AScore,String LRScore,String jobAppliedFromDate,String jobAppliedToDate,int daysVal,int [] subjects,boolean newcandidatesonly,String teacherSSN,String stateForcandidate,String certTypeForCandidate)
	
	public String displayTeacherGrid(String firstName, String lastName,String emailAddress,String teacherSSN,String stateForcandidate,String certTypeForCandidate,String degreeId,String universityId,String normScoreSelectVal,String normScoreVal,String CGPASelectVal,String CGPA,String AScoreSelectVal,String AScore,String LRScoreSelectVal,String LRScore,String references,String resume,int [] subjects,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String districtId,String schoolId,String canServeAsSubTeacherYes,String canServeAsSubTeacherNo,String canServeAsSubTeacherDA,String TFAA,String TFAC,String TFAN,String empNmbr)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		//-- get no of record in grid,
		//-- set start and end position

		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start = ((pgNo-1)*noOfRowInPage);
		int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		
		//------------------------------------
		StringBuffer tmRecords =	new StringBuffer();
		CandidateGridService cgService=new CandidateGridService();
		try{
			List<TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();

			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"lastName";
			String sortOrderNoField		=	"lastName";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			sortOrderStrVal=Order.asc("lastName");
			/**End ------------------------------------**/
			int roleId=0;
			int entityID=0;
			boolean isMiami = false;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			UserMaster userMaster=null;
			int entityTypeId=1;
			int utype = 1;
			boolean writePrivilegeToSchool =false;
			boolean isManage = true;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
					if(districtMaster.getDistrictId().equals(1200390))
						isMiami = true;
					writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
				}
				if(userMaster.getSchoolId()!=null){
					schoolMaster =userMaster.getSchoolId();
				}	
				entityID=userMaster.getEntityType();
				utype = entityID;
			}
			
			if(isMiami && entityID==3)
				isManage=false;

			boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
			if(entityID==1)
			{
				achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
			}else
			{
				try{
					DistrictMaster districtMasterObj=districtMaster;
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
				}catch(Exception e){ e.printStackTrace();}
			}

			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,43,"teacherinfo.do",1);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			String fromDate = "";
			String toDate = "";
			Date fDate=null;
			Date tDate=null;
			Date appliedfDate=null;
			Date appliedtDate=null;
			boolean applieddateFlag=false;
			boolean dateFlag=false;
			try{
				if(!fromDate.equals("")){
					fDate=Utility.getCurrentDateFormart(fromDate);
					dateFlag=true;
				}
				if(!toDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(toDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					tDate=cal2.getTime();
					dateFlag=true;
				}

				// job Applied Filter
				String jobAppliedFromDate = null;
				String jobAppliedToDate = null;
				
				if(jobAppliedFromDate!=null && !jobAppliedFromDate.equals("")){
					appliedfDate=Utility.getCurrentDateFormart(jobAppliedFromDate);
					applieddateFlag=true;
				}
				if(jobAppliedToDate!=null && !jobAppliedToDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(jobAppliedToDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					appliedtDate=cal2.getTime();
					applieddateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
			List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();

			boolean certTypeFlag=false;
			String stateId = "0";
			StateMaster stateMaster=null;
			List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
			String certType = "0"; 
			if(!certType.equals("0") && !certType.equals("")||!stateId.equals("0")){
				certTypeFlag=true;
				if(!stateId.equals("0")){
					stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
				}
				certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
				//System.out.println("certificateTypeMasterList:: "+certificateTypeMasterList.size());
				if(certificateTypeMasterList.size()>0)
					certJobOrderList=jobCertificationDAO.findCertificationByJobWithState(certificateTypeMasterList);
				//System.out.println("certJobOrderList:: "+certJobOrderList.size());
				if(certJobOrderList.size()>0){
					for(JobOrder job: certJobOrderList){
						jobOrderIds.add(job);
					}
				}
				if(jobOrderIds.size()==0){
					entityTypeId=0;
				}
			}else if(certType.equals("")){
				entityTypeId=0;
				certTypeFlag=true;
			}
			
			List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
			List<TeacherDetail> NbyATeacherList = new ArrayList<TeacherDetail>();
			//List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean teacherFlag=false;
			
			List<TeacherDetail> regionTeacherList = new ArrayList<TeacherDetail>();
			boolean regionFlag=false;
			String regionId ="";
			if(regionId!=null && !regionId.equals("") && !regionId.equals("0")){
				regionFlag=true;
				regionTeacherList=teacherPreferenceDAO.findRegionByJobForTeacher(regionId);
			}
			if(regionId.equals("")){
				regionFlag=true;
			}
			if(regionFlag && regionTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(regionTeacherList);
				}else{
					filterTeacherList.addAll(regionTeacherList);
				}
			}
			if(regionFlag){
				teacherFlag=true;
			}
		
			/************* End **************/

			StatusMaster statusMaster = new StatusMaster();
			boolean status=false;
			String candidateStatus = "";
			if(!candidateStatus.equals("") && !candidateStatus.equals("0")){
				statusMaster= WorkThreadServlet.statusMap.get(candidateStatus);
				if(candidateStatus.equals("hird")|| candidateStatus.equals("rem") || candidateStatus.equals("widrw")){
					status=true;
				}else{
					List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
					try{
						if(candidateStatus.equals("icomp")){
							String[] statuss = {"comp","vlt"};
							lstStatusMasters = Utility.getStaticMasters(statuss);
						}else{
							String[] statuss = {candidateStatus};
							lstStatusMasters = Utility.getStaticMasters(statuss);
						}
					}catch (Exception e) {
						e.printStackTrace();
					}
					List<TeacherDetail> teacherStatusIcompList= new ArrayList<TeacherDetail>();
					List<TeacherDetail> teacherStatusList = new ArrayList<TeacherDetail>();
					if(candidateStatus.equals("icomp")){
						teacherStatusIcompList=teacherAssessmentStatusDAO.findBaseStatusByTeachers(lstStatusMasters);
						List teacherIds = new ArrayList();
						for(int e=0; e<teacherStatusIcompList.size(); e++) {
							teacherIds.add(teacherStatusIcompList.get(e).getTeacherId());
						}
						teacherStatusList=teacherDetailDAO.findIcompTeacherDetails(teacherIds);
					}else{
						teacherStatusList=teacherAssessmentStatusDAO.findBaseStatusByTeachers(lstStatusMasters);
					}

					if(teacherStatusList.size()>0){
						if(teacherFlag){
							filterTeacherList.retainAll(teacherStatusList);
						}else{
							filterTeacherList.addAll(teacherStatusList);
						}
						teacherFlag=true;
					}
				}
			}
			
		
			int internalCandVal=0;
			String internalCandidate = "false";
			if(internalCandidate.equalsIgnoreCase("true")){
				internalCandVal=1;
			}

			SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
			SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
			List<JobForTeacher> lstJobForTeacher =	 new ArrayList<JobForTeacher>();
			List<JobOrder> lstJobOrderList =new ArrayList<JobOrder>();
			List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
			int noOfRecordCheck=0;
			if(schoolId.equals("") || districtId.equals("")){
				entityTypeId=0;
			}
			List<TeacherDetail> lstTeacherDetailAll= new ArrayList<TeacherDetail>();
			if(entityTypeId!=0){
				if(entityID==3){
					// Nobal check
					if(!schoolId.equals("0") && writePrivilegeToSchool)
						schoolMaster.setSchoolId(Long.parseLong(schoolId));

					lstJobOrderSLList =schoolInJobOrderDAO.jobOrderPostedBySchool(schoolMaster);
					entityTypeId=3;
				}
				if(!schoolId.equals("0") && entityID!=3){
					SchoolMaster smaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
					lstJobOrderSLList =schoolInJobOrderDAO.jobOrderPostedBySchool(smaster);
					entityTypeId=3;
				}
				if(entityTypeId==3 && lstJobOrderSLList.size()>0 && certTypeFlag){
					lstJobOrderList.addAll(getFilterJobOrder(lstJobOrderSLList,jobOrderIds));
				}else if(entityTypeId!=3 && lstJobOrderSLList.size()==0 && certTypeFlag){
					lstJobOrderList.addAll(jobOrderIds);
					entityTypeId=3;
				}else{
					lstJobOrderList.addAll(lstJobOrderSLList);
				}
				if(entityID==2){
					entityTypeId=2;
				}
				if(!districtId.equals("0")  && entityID==1){
					districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId),false,false);
					entityTypeId=2;
				}
				if(!districtId.equals("0") && (!schoolId.equals("0")|| certTypeFlag) && entityID!=3){
					entityTypeId=4;
				}
			
				/*======================For Teachers Only =================================================================*/
				
				List<TeacherDetail> basicSearchList = new ArrayList<TeacherDetail>();
				List<TeacherDetail> finalTeacherList = new ArrayList<TeacherDetail>();
				List<Criterion> basicSearch = new ArrayList<Criterion>();
				boolean basicFlag = false;
				boolean searchFlag = false;
				
				/////////////////////// First Name, Last Name and Email Search ///////////////////////////////
				if(firstName!=null && !firstName.equals(""))
				{
					Criterion criterion = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
					
				if(lastName!=null && !lastName.equals(""))
				{
					Criterion criterion = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
				
				if(emailAddress!=null && !emailAddress.equals(""))
				{
					Criterion criterion = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
				
				if(basicFlag)
					basicSearchList  = teacherDetailDAO.getTeachersByNameAndEmail(basicSearch);
				
				lstTeacherDetailAll = teacherDetailDAO.getAllTeacherList();
				
				if(lstTeacherDetailAll!=null && lstTeacherDetailAll.size()>0)
				{
					finalTeacherList.addAll(lstTeacherDetailAll);
					searchFlag=true;
				}
				
				if(basicFlag){
					if(searchFlag){
						finalTeacherList.retainAll(basicSearchList);
					}else{
						finalTeacherList.addAll(basicSearchList);
					}
				}
				
				
				System.out.println("finalTeacherList    "+finalTeacherList.size()+"      "+basicFlag);
				/////////SSN//////////////////
				boolean ssnFlag=false;
				//	String teacherSSN = "";
				
				List<TeacherDetail> ssnTeacherList = new ArrayList<TeacherDetail>();
				if(teacherSSN!=null && !teacherSSN.trim().equals(""))
				{
					System.out.println("teacherSSN::: "+teacherSSN);
					teacherSSN = Utility.encodeInBase64(teacherSSN);
					System.out.println("teacherSSN decoded::: "+teacherSSN);
					ssnFlag = true;
					List<TeacherPersonalInfo> teacherPersonalInfosSSN = new ArrayList<TeacherPersonalInfo>();
					teacherPersonalInfosSSN = teacherPersonalInfoDAO.checkTeachersBySSN(teacherSSN);
					for (TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfosSSN) {
						TeacherDetail td = new TeacherDetail();
						td.setTeacherId(teacherPersonalInfo.getTeacherId());
						ssnTeacherList.add(td);
					}
				}
				if(ssnFlag)
				{
					if(searchFlag){
						finalTeacherList.retainAll(ssnTeacherList);
					}else{
						finalTeacherList.addAll(ssnTeacherList);
					}
				}
				/////////////////////////
				
				
				////////////////// Employee Number ////////////////////////////////////
				try
				{
					boolean empNmbrFlag = false;
					List<TeacherDetail> empNmbrThrList = new ArrayList<TeacherDetail>();
					if(empNmbr!=null && !empNmbr.trim().equals(""))
					{
						System.out.println("empNmbr::: "+empNmbr);
						empNmbrFlag = true;
						List<TeacherPersonalInfo> teacherPerInfoEmpNbr = new ArrayList<TeacherPersonalInfo>();
						teacherPerInfoEmpNbr = teacherPersonalInfoDAO.checkTeachersByEmpNmbr(empNmbr);
						for (TeacherPersonalInfo teacherPersonalInfo : teacherPerInfoEmpNbr) {
							TeacherDetail td = new TeacherDetail();
							td.setTeacherId(teacherPersonalInfo.getTeacherId());
							empNmbrThrList.add(td);
						}
					}
					if(empNmbrFlag)
					{
						if(searchFlag){
							finalTeacherList.retainAll(empNmbrThrList);
						}else{
							finalTeacherList.addAll(empNmbrThrList);
						}
					}
				}catch(Exception e)
				{
					e.printStackTrace();
				}
				///////////////////////////////////////////////////////////////////////
				
				
				
				// Start /////////////////////////////// candidate list By certificate ///////////////////////////////////////
				StateMaster stateMasterForCandidate=null;
				List<CertificateTypeMaster> certTypeMasterListForCndt =new ArrayList<CertificateTypeMaster>();
				List<TeacherDetail> tListByCertificateTypeId = new ArrayList<TeacherDetail>();
				boolean tdataByCertTypeIdFlag = false;
				
				
				
				if(certTypeForCandidate.equals(""))
					certTypeForCandidate="0";
				
				if(!certTypeForCandidate.equals("0") && !certTypeForCandidate.equals("")||!stateForcandidate.equals("0")){
					if(!stateForcandidate.equals("0")){
						stateMasterForCandidate=stateMasterDAO.findById(Long.valueOf(stateForcandidate), false,false);
					}
					certTypeMasterListForCndt=certificateTypeMasterDAO.findCertificationByJob(stateMasterForCandidate,certTypeForCandidate);
					tListByCertificateTypeId = teacherCertificateDAO.findCertificateByJobForTeacher(certTypeMasterListForCndt);
					tdataByCertTypeIdFlag=true;
				}
				if(tdataByCertTypeIdFlag){
					if(searchFlag){
						finalTeacherList.retainAll(tListByCertificateTypeId);
					}else{
						finalTeacherList.addAll(tListByCertificateTypeId);
					}
				}
				
				// End /////////////////////////////// candidate list By certificate ///////////////////////////////////////
				
				
				//////////////////////////////////////// Degree /////////////////////////////////
				List<TeacherDetail> degreeTeacherDetailList = new ArrayList<TeacherDetail>();
				boolean degreeIdFlag=false;
				
				if(degreeId!=null && !degreeId.equals("") && !degreeId.equals("0")){
					degreeIdFlag=true;
					DegreeMaster degreeMaster=degreeMasterDAO.findById(Long.valueOf(degreeId), false,false);
					degreeTeacherDetailList=teacherAcademicsDAO.findTeacherListByDegree(degreeMaster);
				}
				if(degreeId.equals("")){
					degreeIdFlag=true;
				}
				if(degreeIdFlag ){
					if(searchFlag){
						finalTeacherList.retainAll(degreeTeacherDetailList);
					}else{
						finalTeacherList.addAll(degreeTeacherDetailList);
					}
				}
				////////////////////////////////////////////////////////////////////////////////////
				
				
				
				/////////////////////////////University//////////////////////
				List<TeacherDetail> universityTeacherList = new ArrayList<TeacherDetail>();
				boolean universityFlag=false;
				if(universityId!=null && !universityId.equals("") && !universityId.equals("0")){
					universityFlag=true;
					UniversityMaster universityMaster=universityMasterDAO.findById(Integer.valueOf(universityId), false,false);
					universityTeacherList=teacherAcademicsDAO.findTeacherListByUniversity(universityMaster);
				}
				if(universityId.equals("")){
					universityFlag=true;
				}
				if(universityFlag){
					if(searchFlag){
						finalTeacherList.retainAll(universityTeacherList);
					}else{
						finalTeacherList.addAll(universityTeacherList);
					}
				}
				/////////////////////////////////////////////////////////////
				
				
				/////////////// Norm Score //////////////////////////////////
				boolean nByAflag=false;

				List<TeacherDetail> normTeacherList = new ArrayList<TeacherDetail>();
				boolean normFlag=false;
				
				if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") & !normScoreSelectVal.equals("6")){
					normFlag=true;
					normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore(normScoreVal,normScoreSelectVal);
				}
				if(normFlag ){
					if(searchFlag){
						finalTeacherList.retainAll(normTeacherList);
					}else{
						finalTeacherList.addAll(normTeacherList);
					}
				}
				
				/// N/A changes
				boolean normNa=false;
				if(!normScoreSelectVal.equals("") && normScoreSelectVal.equals("6")){
					normNa=true;
					normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore("0","5");
				}
				if(normNa)
				{
					if(nByAflag){
						NbyATeacherList.retainAll(normTeacherList);
					}else{
						NbyATeacherList.addAll(normTeacherList);
					}
				}
				
				if(normNa){
					nByAflag=true;
				}
				
				/*if(nByAflag)
				{
					finalTeacherList.removeAll(NbyATeacherList);
				}*/
				/////////////////////////////////////////////////////////////
				
				////////// CGPA ////////////////////////////////////////////
				List<TeacherDetail> cgpaTeacherList = new ArrayList<TeacherDetail>();
				boolean cgpaFlag=false;
				if(!CGPASelectVal.equals("") && !CGPASelectVal.equals("0") & !CGPASelectVal.equals("6")){
					cgpaFlag=true;
					cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA(CGPA,CGPASelectVal);
				}
				if(cgpaFlag){
					if(searchFlag){
						finalTeacherList.retainAll(cgpaTeacherList);
					}else{
						finalTeacherList.addAll(cgpaTeacherList);
					}
				}
				
				/// N/A changes
				boolean CGPANa=false;
				if(!CGPASelectVal.equals("") && CGPASelectVal.equals("6")){
					CGPANa=true;
					cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA("0","5");
				}
				if(CGPANa){
					if(nByAflag){
						NbyATeacherList.retainAll(cgpaTeacherList);
					}else{
						NbyATeacherList.addAll(cgpaTeacherList);
					}
				}
				
				if(CGPANa){
					nByAflag=true;
				}
				
				
				///////////////////////////////////////////////////////////
				
				////////// Ascore
				List<TeacherDetail> acheivTeacherList = new ArrayList<TeacherDetail>();
				boolean acheivFlag=false;
				if(!AScoreSelectVal.equals("") && !AScoreSelectVal.equals("0") & !AScoreSelectVal.equals("6")){
					acheivFlag=true;
					acheivTeacherList=teacherAchievementScoreDAO.findTeachersByAscoreOrLScore(AScore,AScoreSelectVal,districtMaster,true);
				}
				if(acheivFlag){
					if(searchFlag){
						finalTeacherList.retainAll(acheivTeacherList);
					}else{
						finalTeacherList.addAll(acheivTeacherList);
					}
				}
				
				/// N/A changes
				boolean acheivNa=false;
				if(!CGPASelectVal.equals("") && CGPASelectVal.equals("6")){
					acheivNa=true;
					acheivTeacherList=teacherAchievementScoreDAO.findTeachersByAscoreOrLScore("0","5",districtMaster,true);
				}
				
				if(acheivNa){
					if(nByAflag){
						NbyATeacherList.retainAll(acheivTeacherList);
					}else{
						NbyATeacherList.addAll(acheivTeacherList);
					}
				}
				if(acheivNa){
					nByAflag=true;
				}
				
				///////////////////////////////////////////////////////////
				
				/////////// L/R SCore
				List<TeacherDetail> LRScoreTeacherList = new ArrayList<TeacherDetail>();
				boolean lRScoreFlag=false;
				if(!LRScoreSelectVal.equals("") && !LRScoreSelectVal.equals("0") & !LRScoreSelectVal.equals("6") && districtMaster!=null){
					lRScoreFlag=true;
					LRScoreTeacherList=teacherAchievementScoreDAO.findTeachersByAscoreOrLScore(LRScore,LRScoreSelectVal,districtMaster,false);
				}
				if(lRScoreFlag){
					if(searchFlag){
						finalTeacherList.retainAll(LRScoreTeacherList);
					}else{
						finalTeacherList.addAll(LRScoreTeacherList);
					}
				}
				
				// N/A changes
				boolean LRScoreNa=false;
				if(!CGPASelectVal.equals("") && CGPASelectVal.equals("6")){
					LRScoreNa=true;
					LRScoreTeacherList=teacherAchievementScoreDAO.findTeachersByAscoreOrLScore("0","5",districtMaster,false);
				}	
				if(LRScoreNa){
					if(nByAflag){
						NbyATeacherList.retainAll(LRScoreTeacherList);
					}else{
						NbyATeacherList.addAll(LRScoreTeacherList);
					}
				}
				
				if(LRScoreNa)
					nByAflag=true;
				
				
				if(nByAflag)
				{
					finalTeacherList.removeAll(NbyATeacherList);
				}
				///////////////////////////////////////////////////////////
				
				
				
				////// Reference
				List<TeacherDetail> referencesTeacherList = new ArrayList<TeacherDetail>();
				boolean referFlag=false;
				if(references!=null && !references.equals("") && !references.equals("0")){
					referFlag=true;
					referencesTeacherList=teacherElectronicReferencesDAO.findTeacherReferences();
				}
				
				if(referFlag)
				{
					if(searchFlag)
					{
							if(references.equals("1"))
								finalTeacherList.retainAll(referencesTeacherList);
							else if(references.equals("2"))
								finalTeacherList.removeAll(referencesTeacherList);
					}else{
						if(references.equals("1"))
							finalTeacherList.addAll(referencesTeacherList);
						else
						{
							finalTeacherList.retainAll(referencesTeacherList);
						}
					}
					
				}
				////////////////////////////////////////////////////////////
				
				//// Resume
				List<TeacherDetail> nolstTeacherDetailAll= new ArrayList<TeacherDetail>();
				List<TeacherDetail> resumeTeacherList = new ArrayList<TeacherDetail>();
				boolean resumeFlag=false;
				if(resume!=null && !resume.equals("") && !resume.equals("0")){
					resumeFlag=true;
					resumeTeacherList=teacherExperienceDAO.findTeachersByResume(true);
				}
			
				if(resumeFlag)
				{
					if(searchFlag)
					{
							if(resume.equals("1"))
								finalTeacherList.retainAll(resumeTeacherList);
							else if(resume.equals("2"))
								finalTeacherList.removeAll(resumeTeacherList);
					}else{
						if(resume.equals("1"))
							finalTeacherList.addAll(resumeTeacherList);
						else
						{
							finalTeacherList.retainAll(resumeTeacherList);
						}
					}
				}
				
				///////////////////////////////////////////////////////////
				
				
				////// Subjects
				
				List<TeacherDetail> subjectTeacherList = new ArrayList<TeacherDetail>();
				boolean subjectFlag=false;
				if(subjects!=null && subjects.length>0){
					subjectFlag=true;
					SubjectMaster subjectMaster = null;
					List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();

					for(Integer subjId:subjects)
					{
						subjectMaster = new SubjectMaster();
						subjectMaster.setSubjectId(subjId);
						subjectMasters.add(subjectMaster);
					}

					subjectTeacherList=jobForTeacherDAO.findTeachersBySubjectList(subjectMasters);
				}
				if(subjectFlag){
					if(searchFlag){
						finalTeacherList.retainAll(subjectTeacherList);
					}else{
						finalTeacherList.addAll(subjectTeacherList);
					}
				}
				
				////////////////////////////////////////////////////////
				
				////////Willing to Substitute
				List<TeacherDetail> substitudeTeacherList = new ArrayList<TeacherDetail>();
				boolean substituteFlag=false;
				if(!canServeAsSubTeacherYes.equals("false") || !canServeAsSubTeacherNo.equals("false") || !canServeAsSubTeacherDA.equals("false")){
					substituteFlag=true;
					substitudeTeacherList=teacherExperienceDAO.findTeachersBySubstitute(canServeAsSubTeacherYes,canServeAsSubTeacherNo,canServeAsSubTeacherDA);
					System.out.println(" substitudeTeacherList ::::::::::::::: "+substitudeTeacherList.size());
				}
				if(substituteFlag){
					if(searchFlag){
						finalTeacherList.retainAll(substitudeTeacherList);
					}else{
						finalTeacherList.addAll(substitudeTeacherList);
					}
				}
				////////////////////////////////////////////////////////
				
				///////////TFA
				List<TeacherDetail> tfaTeacherList = new ArrayList<TeacherDetail>();
				boolean tfaFlag=false;
				
				if(!TFAA.equals("false") || !TFAC.equals("false") || !TFAN.equals("false")){
					tfaFlag=true;
					tfaTeacherList=teacherExperienceDAO.findTeachersByTFA(TFAA,TFAC,TFAN);
					System.out.println(" tfaTeacherList :::::::::::::::::::::: "+tfaTeacherList.size());
				}
				if(tfaFlag){
					if(searchFlag){
						finalTeacherList.retainAll(tfaTeacherList);
					}else{
						finalTeacherList.addAll(tfaTeacherList);
					}
				}
				////////////////////////////////////////////////////////
				
				totalRecord=finalTeacherList.size();
				
				
				
				
				System.out.println(" noOfRowInPage:"+noOfRowInPage+" pgNo:"+pgNo+" start:"+start+" end:"+end+" totalRecord:"+totalRecord);
				
				
				
				System.out.println(" sortOrderFieldName "+sortOrderFieldName);
				
				
				/*====================================================================================================*/
				
				if(totalRecord<end)
					end=totalRecord;

				
				// sorting on normscore, ascore, lrscore
				if(!sortOrder.equals("tLastName"))
				{
					System.out.println("  sort Order Is not Last Name .................. ");
					
					List<TeacherNormScore> teacherNormScoreList = teacherNormScoreDAO.findNormScoreByTeacherList(finalTeacherList);
					Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
					for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
						normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
					}
					Map<Integer,TeacherAchievementScore> mapAScore = null;

					if(entityID!=1 && achievementScore)
					{
						JobOrder jo=new JobOrder();
						jo.setDistrictMaster(districtMaster);
						List<TeacherAchievementScore> lstTeacherAchievementScore=teacherAchievementScoreDAO.getAchievementScore(finalTeacherList,jo);

						mapAScore = new HashMap<Integer, TeacherAchievementScore>();

						for(TeacherAchievementScore tAScore : lstTeacherAchievementScore){
							mapAScore.put(tAScore.getTeacherDetail().getTeacherId(),tAScore);
						}
					}
					TeacherAchievementScore tAScore=null;

					TeacherNormScore teacherNormScore = null;
					for(TeacherDetail teacherDetail : finalTeacherList)
					{
						teacherNormScore = normMap.get(teacherDetail.getTeacherId());
						if(teacherNormScore==null)
							teacherDetail.setNormScore(-0);
						else
							teacherDetail.setNormScore(teacherNormScore.getTeacherNormScore());

						if(entityID!=1 && achievementScore)
						{
							tAScore=mapAScore.get(teacherDetail.getTeacherId());
							if(tAScore!=null){
								if(tAScore.getAcademicAchievementScore()!=null)
									teacherDetail.setaScore(tAScore.getAcademicAchievementScore());

								if(tAScore.getLeadershipAchievementScore()!=null)
									teacherDetail.setlRScore(tAScore.getLeadershipAchievementScore());
							}else{
								teacherDetail.setaScore(-1);
								teacherDetail.setlRScore(-1);
							}
						}
					}

					if(sortOrderFieldName.equals("normScore") && sortOrderType.equals("0")){
						Collections.sort(finalTeacherList,TeacherDetail.teacherDetailComparatorNormScoreDesc );
					}else if(sortOrderFieldName.equals("normScore") && sortOrderType.equals("1")){
						Collections.sort(finalTeacherList,TeacherDetail.teacherDetailComparatorNormScore);
					}else if(sortOrderFieldName.equals("aScore") && sortOrderType.equals("1")){				
						Collections.sort(finalTeacherList,TeacherDetail.TeacherDetailComparatorAScore );
					}else if(sortOrderFieldName.equals("aScore") && sortOrderType.equals("0")){				
						Collections.sort(finalTeacherList,TeacherDetail.TeacherDetailComparatorAScoreDesc );				
					}else if(sortOrderFieldName.equals("lRScore") && sortOrderType.equals("1")){				
						Collections.sort(finalTeacherList,TeacherDetail.TeacherDetailComparatorLRScore );
					}else if(sortOrderFieldName.equals("lRScore") && sortOrderType.equals("0")){				
						Collections.sort(finalTeacherList,TeacherDetail.TeacherDetailComparatorLRScoreDesc );				
					}

					if(sortOrder.equals("tStatus")){
						for(TeacherDetail td:finalTeacherList)
						{
							if(td.getStatus().equalsIgnoreCase("I"))
								td.settStatus("Deactivate");
							else
								td.settStatus("Activate");
						}
						
						if(sortOrderType.equals("0"))
							Collections.sort(finalTeacherList,new TeacherDetailComparatorStatusAsc());
						else
							Collections.sort(finalTeacherList,new TeacherDetailComparatorStatusDesc());
					}
				
					lstTeacherDetail=finalTeacherList.subList(start,end);
				}
				else
				{
					if(sortOrder.equals("tLastName")){
						for(TeacherDetail td:finalTeacherList)
						{
								td.settLastName(td.getLastName());
						}
						
						if(sortOrderType.equals("0"))
							Collections.sort(finalTeacherList,new TeacherDetailComparatorLastNameAsc());
						else
							Collections.sort(finalTeacherList,new TeacherDetailComparatorLastNameDesc());
					}
				
					lstTeacherDetail=finalTeacherList.subList(start,end);
				}


				List<Integer> lstStatusMasters = new ArrayList<Integer>();
				String[] statuss = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
				try{
					lstStatusMasters = statusMasterDAO.findStatusIdsByStatusByShortNames(statuss);
				}catch (Exception e) {
					e.printStackTrace();
				}
				if(totalRecord!=0){
					lstJobForTeacher=jobForTeacherDAO.getJobForTeacherByDSTeachersList(lstStatusMasters,lstTeacherDetail,lstJobOrderSLList,null,null,districtMaster,entityID);
					for(JobForTeacher jobForTeacher : lstJobForTeacher){
						jftMap.put(jobForTeacher.getJobForTeacherId(),jobForTeacher);
					}
					
					System.out.println(lstStatusMasters.size()+" <--------> "+lstTeacherDetail.size()+" <--------> "+lstJobOrderSLList.size());
					lstJobForTeacher=jobForTeacherDAO.getJobForTeacherByDSTeachersList(lstStatusMasters,lstTeacherDetail,lstJobOrderSLList,null,null,districtMaster,1);
					for(JobForTeacher jobForTeacher : lstJobForTeacher){
						jftMapForTm.put(jobForTeacher.getJobForTeacherId(),jobForTeacher);
					}
				}
			}
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			List<TeacherNormScore> teacherNormScoreList=teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetail);
			Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
			for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
				normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
			}

			Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();	
			Map<Integer,Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
			List<TeacherExperience> lstTeacherExperience=teacherExperienceDAO.findExperienceForTeachers(lstTeacherDetail);

			// ************** Start ... IDENTIFY Internal Candidate *********************
			Map<Integer,Boolean> mapInternalCandidate = new HashMap<Integer, Boolean>();
			if(entityID!=1)
			{
				List<JobForTeacher> lstjobForTeachers=jobForTeacherDAO.findInternalCandidate(lstTeacherDetail);
				if(entityID==2)
				{
					for(JobForTeacher pojo:lstjobForTeachers){
						if(districtMaster.equals(pojo.getJobId().getDistrictMaster()))
						{
							mapInternalCandidate.put(pojo.getTeacherId().getTeacherId(), true);
						}
					}
				}
				else if(entityID==3)
				{
					Map<Integer,Boolean> mapJobOrder = new HashMap<Integer, Boolean>();
					List<JobOrder> lstJobOrders=new ArrayList<JobOrder>();
					for(JobForTeacher pojo:lstjobForTeachers){
						if(districtMaster.equals(pojo.getJobId().getDistrictMaster()))
							lstJobOrders.add(pojo.getJobId());
					}
					List<SchoolInJobOrder> lstSchoolInJobOrders= new ArrayList<SchoolInJobOrder>();
					if(schoolMaster!=null)
					 lstSchoolInJobOrders=schoolInJobOrderDAO.findInternalCandidate(lstJobOrders, schoolMaster);
					
					if(lstSchoolInJobOrders.size()>0){
						for(SchoolInJobOrder pojo:lstSchoolInJobOrders)
							mapJobOrder.put(pojo.getJobId().getJobId(), true);
					}

					for(JobForTeacher pojo:lstjobForTeachers){
						if(mapJobOrder.get(pojo.getJobId().getJobId())!=null && mapJobOrder.get(pojo.getJobId().getJobId()).equals(true)){
							mapInternalCandidate.put(pojo.getTeacherId().getTeacherId(), true);
						}
					}
				}
			}
			// ************** End ... IDENTIFY Internal Candidate *********************

			List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
			Integer isBase = null;
			try{
				if(lstTeacherDetail!=null && lstTeacherDetail.size()>0){
					teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(lstTeacherDetail);
					for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
						baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
						boolean cgflag = teacherAssessmentStatus2.getCgUpdated()==null?false:teacherAssessmentStatus2.getCgUpdated();
						if(cgflag && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
						{
							isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
						}
						else
						{
							if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
							{
								isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 2);
							}
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			
			//alok kumar			
			Map<Integer,List<Integer>> jsiCount	=	new HashMap<Integer, List<Integer>>();
			Map<Integer,List<Integer>> epiCount	=	new HashMap<Integer, List<Integer>>();
			
			List<TeacherAssessmentStatus> teacherAssessmentJsi = null;
			List<TeacherAssessmentStatus> teacherAssessmentEpi = null;
			List<Integer> teacherId = new ArrayList();
			List<Integer> matchTeacherId = null;
			try
			{
				if(lstTeacherDetail!=null && lstTeacherDetail.size()>0)
				{

					for(TeacherDetail t:lstTeacherDetail)
					{
						teacherId.add(t.getTeacherId());
					}	
					teacherAssessmentJsi = teacherAssessmentStatusDAO.findAssessmentJsiByTeachers(lstTeacherDetail);
					teacherAssessmentEpi=teacherAssessmentStatusDAO.findAssessmentEpiByTeachers(lstTeacherDetail);						
					if(teacherAssessmentJsi.size()>0)
					{
						for(Integer id :teacherId)
						{
							matchTeacherId=new ArrayList();
							for(TeacherAssessmentStatus assessmentteacherId:teacherAssessmentJsi)
							{								  
							  if(id.equals(assessmentteacherId.getTeacherDetail().getTeacherId()))
							  {
								  matchTeacherId.add(assessmentteacherId.getTeacherDetail().getTeacherId());
							  }
							}
							if(matchTeacherId.size()>0)
							jsiCount.put(id, matchTeacherId);							
						}
					}
					if(teacherAssessmentEpi.size()>0)
					{
						for(Integer id :teacherId)
						{
							matchTeacherId=new ArrayList();
							for(TeacherAssessmentStatus assessmentteacherId:teacherAssessmentEpi)
							{
							  if(id.equals(assessmentteacherId.getTeacherDetail().getTeacherId()))
							  {
								  matchTeacherId.add(assessmentteacherId.getTeacherDetail().getTeacherId());
							  }
							}
							if(matchTeacherId.size()>0)
							epiCount.put(id, matchTeacherId);							
						}
					}
				}				
			}			
			catch(Exception e)
			{e.printStackTrace();}
			
			
			
			String tFname="",tLname="";
			tmRecords.append("<table id='teacherTable' width='100%' class='tablecgtbl'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");

			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("Name",sortOrderFieldName,"tLastName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			String schoolDistrict="District";
			if(entityID==3){
				schoolDistrict="School";
			}

			//tmRecords.append("<th width='60' valign='top'>Norm Score</th>");
			responseText=PaginationAndSorting.responseSortingLink("Norm Score",sortOrderFieldName,"normScore",sortOrderTypeVal,pgNo);
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");

			if(entityID!=1 && achievementScore){
				//tmRecords.append("<th width='60' valign='top'>A Score</th>");
				//tmRecords.append("<th width='60' valign='top'>L/R Score</th>");
				System.out.println("achievementScore=="+achievementScore+"   tFA  "+tFA);

				responseText=PaginationAndSorting.responseSortingLink("A Score",sortOrderFieldName,"aScore",sortOrderTypeVal,pgNo);
				tmRecords.append("<th width='80' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink("L/R Score",sortOrderFieldName,"lRScore",sortOrderTypeVal,pgNo);
				tmRecords.append("<th width='80' valign='top'>"+responseText+"</th>");
			}
			if(tFA)
				tmRecords.append("<th width='60' valign='top'>TFA</th>");

			tmRecords.append("<th width='50' valign='top'>"+Utility.getLocaleValuePropByKey("lblOfJobs", locale)+"&nbsp;<a data-original-title='X/Y, X = Number of jobs applied at your "+schoolDistrict+" / Y = Number of Jobs applied at other "+schoolDistrict+"(s)' class='net-header-text ' rel='tooltip' id='jobApplied' href='javascript:void(0);' ><span class='icon-question-sign '></span></a></th>");

			if(teachingOfYear)
				tmRecords.append("<th width='60' valign='top'"+Utility.getLocaleValuePropByKey("lblOfYearsTeaching", locale)+"</th>");			
			if(expectedSalary)
				tmRecords.append("<th width='60' valign='top'>"+Utility.getLocaleValuePropByKey("ExptSalary", locale)+"</th>");

			if(entityID==1){
				/*responseText=PaginationAndSorting.responseSortingLink("User Type",sortOrderFieldName,"userType",sortOrderTypeVal,pgNo);
				tmRecords.append("<th width='80' valign='top'>"+responseText+"</th>");*/

				/*responseText=PaginationAndSorting.responseSortingLink("Registered On",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
				tmRecords.append("<th width='100' valign='top'>"+responseText+"</th>");*/

				responseText=PaginationAndSorting.responseSortingLink("Status",sortOrderFieldName,"tStatus",sortOrderTypeVal,pgNo);
				tmRecords.append("<th width='76' valign='top'>"+responseText+"</th>");
			}
			if(entityID!=1 && isManage)
			{
				tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblEngage", locale)+"</th>");
			}
			else if(entityID==1)
			{
				tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblEngage", locale)+"</th>");
			}
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			List<TeacherPersonalInfo> lstTeacherPersonalInfo = new ArrayList<TeacherPersonalInfo>();
			List<TeacherAchievementScore> lstTeacherAchievementScore = new ArrayList<TeacherAchievementScore>();
		
			List<DistrictSpecificQuestions> lstdistrictSpecificQuestions = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(userMaster.getDistrictId());
			boolean schoolCheckFlag=false;
			if(userMaster.getEntityType()==3)
			{
				try
				{
					schoolMaster = schoolMasterDAO.findById(userMaster.getSchoolId().getSchoolId(), false, false);
					DistrictSchools districtSchools = districtSchoolsDAO.findBySchoolMaster(schoolMaster);
					if(districtSchools.getDistrictMaster().getResetQualificationIssuesPrivilegeToSchool()==true)
					{
						schoolCheckFlag = true;
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			int counter=0;
			TeacherNormScore teacherNormScore = null;
			
			
			List<Integer>teacherIdLst=new ArrayList<Integer>();
			for (TeacherDetail teacherDetail : lstTeacherDetail) 
			{
				teacherIdLst.add(teacherDetail.getTeacherId());
			}
			Criterion criterionTeacherPersonalInfo=Restrictions.in("teacherId", teacherIdLst);
			List<TeacherPersonalInfo>teacherPersonalInfos=null;
			HashMap<Integer, TeacherPersonalInfo> map=new HashMap<Integer, TeacherPersonalInfo>();
			if(teacherIdLst!=null && teacherIdLst.size()>0){
				teacherPersonalInfos=teacherPersonalInfoDAO.findByCriteria(criterionTeacherPersonalInfo);
				for (TeacherPersonalInfo pojo : teacherPersonalInfos) 
				{
					map.put(pojo.getTeacherId(), pojo);
				}
			}	
			//alok
			String ssn="";				
			Map<Integer, TeacherPersonalInfo>tpInfomap=new  HashMap<Integer, TeacherPersonalInfo>();
			Map<Integer, EmployeeMaster> empMap = new HashMap<Integer, EmployeeMaster>();
			if(teacherPersonalInfos!=null && teacherPersonalInfos.size()>0){				
				for(TeacherPersonalInfo pojo :teacherPersonalInfos) {
					tpInfomap.put(pojo.getTeacherId(), pojo); 

					ssn = pojo.getSSN()==null?"":pojo.getSSN();
					String empNo = pojo.getEmployeeNumber()==null?"":pojo.getEmployeeNumber();

					if(!ssn.equals(""))
					{
							ssn = ssn.trim();
							try {
								ssn = Utility.decodeBase64(ssn);
							} catch (IOException e) {
								e.printStackTrace();
							}
							if(ssn.length()>4)
								ssn = ssn.substring(ssn.length() - 4, ssn.length());
							
							List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
							if(empNo.equals(""))// Not an employee
							{
								try {
									//System.out.println("SSN: "+ssn);
									employeeMasters =	employeeMasterDAO.checkEmployeeBySSN(ssn,pojo.getFirstName(),pojo.getLastName(),districtMaster);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							else 
								employeeMasters =	employeeMasterDAO.checkEmployee(ssn, empNo,pojo.getFirstName(),pojo.getLastName(),districtMaster);
							
							if(employeeMasters.size()>0)
							empMap.put(pojo.getTeacherId(), employeeMasters.get(0));
					}
				}
			}
			//Gourav  districtMaster
			
			List<SecondaryStatusMaster> lstSecondaryStatusMaster = secondaryStatusMasterDAO.findTagswithDistrictInCandidatePool(districtMaster);
			
			
			Criterion district_cri = Restrictions.eq("districtMaster", districtMaster);
			Criterion school_cri = Restrictions.isNull("schoolMaster");
			Criterion job_cri = Restrictions.isNull("jobOrder");
			
			List<TeacherSecondaryStatus> lstMltTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByCriteria(district_cri,school_cri,job_cri);
			Map<String, TeacherSecondaryStatus> teachersTagsExistMap = new HashMap<String, TeacherSecondaryStatus>();
			if(lstMltTeacherSecondaryStatus.size()>0){
				for (TeacherSecondaryStatus teacherSecondaryStatus : lstMltTeacherSecondaryStatus) {
					String teacherTagKey = teacherSecondaryStatus.getTeacherDetail().getTeacherId()+"##"+teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusId();
					teachersTagsExistMap.put(teacherTagKey, teacherSecondaryStatus);
				}
			}
			
			if(lstTeacherDetail.size()==0)
				tmRecords.append("<tr><td colspan='9'>"+Utility.getLocaleValuePropByKey("lblNoTeacher", locale)+"</td></tr>" );
			else
			{
				if(entityID!=1)
				{
					JobOrder jobOrder = new JobOrder();
					jobOrder.setDistrictMaster(districtMaster);
					lstTeacherAchievementScore=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetail,jobOrder);
				}
				lstTeacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPInfoForTeacher(lstTeacherDetail);
			}
			
			Map<Integer,Boolean> qqTeachersMap = new HashMap<Integer, Boolean>(); 
			if(lstTeacherDetail.size()>0 && entityTypeId!=1 && lstdistrictSpecificQuestions.size()>0)
			{
				List<TeacherDetail> qqAttemptedTeachers =  teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findQQStatusByTeacher(lstTeacherDetail,districtMaster,true);
				List<TeacherDetail> invalidQQTeachers = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findQQStatusByTeacher(lstTeacherDetail,districtMaster,false);
				for (TeacherDetail teacherDetail : qqAttemptedTeachers) {
					qqTeachersMap.put(teacherDetail.getTeacherId(), true);
				}
				for (TeacherDetail teacherDetail : invalidQQTeachers) {
					qqTeachersMap.put(teacherDetail.getTeacherId(), false);
				}
			}
			
			// teacherwiseassessmenttime
			Map<Integer, TeacherWiseAssessmentTime> teacherWMap = new HashMap<Integer, TeacherWiseAssessmentTime>();
			if(lstTeacherDetail.size()>0){
				Calendar cal = Calendar.getInstance();

				cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
				Date sDate=cal.getTime();
				cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
				Date eDate=cal.getTime();

				Criterion teacherListCR = Restrictions.in("teacherDetail", lstTeacherDetail);
				Criterion assessmenType		= Restrictions.eq("assessmentType", 1);
				Criterion currentYear = Restrictions.between("createdDateTime", sDate, eDate);
				
				List<TeacherWiseAssessmentTime> teacherWiseAssessmentTime = teacherWiseAssessmentTimeDAO.findByCriteria(Order.desc("assessmentTimeId"),teacherListCR,assessmenType);
				
				if(teacherWiseAssessmentTime.size()>0){
					for (TeacherWiseAssessmentTime teacherWiseAssessmentTime2 : teacherWiseAssessmentTime) {
						if(!teacherWMap.containsKey(teacherWiseAssessmentTime2.getTeacherDetail().getTeacherId())){
							teacherWMap.put(teacherWiseAssessmentTime2.getTeacherDetail().getTeacherId(), teacherWiseAssessmentTime2);
						}
					}
				}
			}
			//************************* Adding by deepak **************************************
			Map<Integer,String>  portfolioIdMap=new HashMap<Integer, String>();
			Boolean portfolioFlag=selfServiceCandidateProfileService.checkPortfolioAvaliablity(lstTeacherDetail,userMaster,districtMaster,portfolioIdMap);
			//***************************************************************
			for (TeacherDetail teacherDetail : lstTeacherDetail) 
			{
				teacherNormScore = normMap.get(teacherDetail.getTeacherId());
				int normScore = 0;
				String colorName = "";
				if(teacherNormScore!=null)
				{
					normScore = teacherNormScore.getTeacherNormScore();
					//percentile = teacherNormScore.getPercentile();
					colorName = teacherNormScore.getDecileColor();
				}
			
				noOfRecordCheck++;
				tFname=teacherDetail.getFirstName();
				tLname=teacherDetail.getLastName();
				counter++;
				String gridColor="class='bggrid'";
				if(counter%2==0){
					gridColor="style='background-color:white;'";
				}

				tmRecords.append("<tr "+gridColor+">");
				//tmRecords.append("<TD width='240'><a class='profile"+noOfRecordCheck+"' data-placement='above' href='javascript:void(0);' id='pro"+teacherDetail.getTeacherId()+"'>"+tFname+" "+tLname+"</a></BR>");

				
					tmRecords.append("<TD width='240'>");
				
				// ************* Start ... Add for Internal Candidate ICON
				if(entityID!=1)
				{
					if(mapInternalCandidate.get(teacherDetail.getTeacherId())!=null && mapInternalCandidate.get(teacherDetail.getTeacherId()))
						tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblIntrCand", locale)+"' rel='tooltip' id='tpInternalCandidate"+noOfRecordCheck+"'  ><span class='fa-random icon-large internalCandidateIconColor'></span></a> ");
					//else
					//tmRecords.append("<a data-original-title='No Internal Candidate' rel='tooltip' id='tpInternalCandidate"+noOfRecordCheck+"'  ><span class='fa-rocket icon-large iconcolorhover'></span></a> ");
				}

				//************************** Adding by deepak ***********************************
				System.out.println("Teacherid ::::::: "+teacherDetail.getTeacherId());
				//***********************************************************************************************
				//String sServerDomainName=request.getServerName();
				boolean bSelfService=false;
				if(portfolioFlag && portfolioIdMap.get(teacherDetail.getTeacherId())!=null)
				    {
				     bSelfService=true;
				    }
					if(bSelfService){
					String fullName=tFname+" "+tLname;
					fullName=fullName.replaceAll("'", "&#36;");
					tmRecords.append("<a class='profile' href='javascript:void(0);' onclick=\"showProfileContentNew(this,0,"+teacherDetail.getTeacherId()+",'0','0','',"+noOfRecordCheck+",event,'"+fullName+"','"+portfolioIdMap.get(teacherDetail.getTeacherId())+"','pool')\"   data-original-title=' "+tFname+" "+tLname+" ' rel='tooltip' id='teacherFnameAndLname"+noOfRecordCheck+"'  >"+tFname+" "+tLname+"</a>");
				}else{
					tmRecords.append("<a class='profile' href='javascript:void(0);' onclick=\"showProfileContentForTeacher(this,"+teacherDetail.getTeacherId()+","+noOfRecordCheck+",'Teacher Pool',event)\">"+tFname+" "+tLname+"</a>");
				}
			
				
				
				// ************* End ... Add for Internal Candidate ICON
				//tmRecords.append("<a class='profile' href='javascript:void(0);' onclick=\"showProfileContentForTeacher(this,"+teacherDetail.getTeacherId()+","+noOfRecordCheck+",'Teacher Pool',event)\">"+tFname+" "+tLname+"</a>");
				
				//gourav  tag loop start
				
				String cssheader="style='padding-left: 14px;padding-top: 15px; padding-bottom: 15px;'";
				String csscoloumns="style='padding-left: 14px;'";
				
				StringBuffer tagContent =	new StringBuffer();
				tagContent.append("<div>");
				tagContent.append("<table  width=\"50%\" border=\"0\" class=\"\">");				
				for(SecondaryStatusMaster secondaryStatusMaster : lstSecondaryStatusMaster){
					tagContent.append("<tr>");
					
					if(teachersTagsExistMap.containsKey(teacherDetail.getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId())){
						tagContent.append("<td width=\"10%\" style=\"padding-left:14px;line-height:0px;\"><input type=\"checkbox\" checked=\"checked\"  name=\"tagsValue\" id=\"tagsValue\" value="+secondaryStatusMaster.getSecStatusId()+"></td>");
					}else{
						tagContent.append("<td width=\"10%\" style=\"padding-left:14px;line-height:0px;\"><input type=\"checkbox\" name=\"tagsValue\" id=\"tagsValue\" value="+secondaryStatusMaster.getSecStatusId()+"></td>");
					}
					//tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\" class=\"tdAct\">"+secondaryStatusMaster.getSecStatusName()+"</td>");
					
					if(secondaryStatusMaster.getPathOfTagsIcon()!=null){
						tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\"><img src=\""+showTagIconFile(secondaryStatusMaster)+"\" width=\"20px\" height=\"20px\">&nbsp;&nbsp;"+secondaryStatusMaster.getSecStatusName()+"</td>");
						
					}else{
						tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\" class=\"tdAct\">"+secondaryStatusMaster.getSecStatusName()+"</td>");
					}
					tagContent.append("</tr>");
				}
				
				tagContent.append("</table>");
				tagContent.append("</div>");
				tagContent.append("<div class=\"modal-footer\">");
				tagContent.append("<span><button  class=\"btn btn-large btn-primary\" onclick=\"saveTags("+teacherDetail.getTeacherId()+");\"  >"+Utility.getLocaleValuePropByKey("btnSave", locale)+" <i class=\"icon\"></i></button>  <button id=\"closeTag"+noOfRecordCheck+"\" onclick=\"closeActAction("+noOfRecordCheck+");\" class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"  >"+Utility.getLocaleValuePropByKey("btnClose", locale)+"</button></span>");						 
				tagContent.append("</div>");
				
				StringBuffer tagNames =	new StringBuffer();
				for(SecondaryStatusMaster secondaryStatusMaster : lstSecondaryStatusMaster){
					if(teachersTagsExistMap.containsKey(teacherDetail.getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId())){						
						String tagName = teachersTagsExistMap.get(teacherDetail.getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId()).getSecondaryStatusMaster().getSecStatusName();						
						if(teachersTagsExistMap.get(teacherDetail.getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId()).getSecondaryStatusMaster().getPathOfTagsIcon()!=null){
							tagNames.append(" <img src='"+showTagIconFile(secondaryStatusMaster)+"' width=\"20px\" height=\"20px\">");
						}else{
							tagNames.append(" <span class=\"txtbgroundTagStatusCPool\" width=\"5%\">"+tagName+"</span>");
						}
					}
				}
				String tagString = tagContent.toString();
				if(roleId != 1){
					if(roleId != 3){
						
						
						
					tmRecords.append(" <a id='tagPreview"+noOfRecordCheck+"' class=\"allPopOver\" href='javascript:void(0);' rel='popover' data-content='"+tagContent.toString()+"'");
					//tmRecords.append(tagContent.toString());
					tmRecords.append(" data-html='true' onmouseover=\"closeProfileOpenTags("+noOfRecordCheck+");\">");
					tmRecords.append(tagNames.toString());
					tmRecords.append("</a>");
					
					tmRecords.append("<script type='text/javascript'>$('#tagPreview"+noOfRecordCheck+"').popover();</script>");
					}else{
						tmRecords.append(tagNames.toString());
					}
				}
				tmRecords.append("<br/>");
				//Add profile hover

				String profileNormScore="";
				if(normScore!=0){
					profileNormScore=normScore+"";
				}else{
					profileNormScore="N/A";
				}
				
			/* @Start
			 * @Ashish Kumar
			 * @Description :: Qualification Details Thumb 
			 * */
				
				tmRecords.append(teacherDetail.getEmailAddress());
				TeacherPersonalInfo tempTeacher=map.get(teacherDetail.getTeacherId());
				if(tempTeacher!=null && tempTeacher.getIsVateran()!=null && tempTeacher.getIsVateran()==1)
				{	
					tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblVeteranCandidate", locale)+"' rel='tooltip' id='teacher_"+teacherDetail.getTeacherId()+"' href='javascript:void(0);'>&nbsp;&nbsp;<img height='20px' width='20' src=\"images/Veteran.jpg\"></a>");
					tmRecords.append("<script type='text/javascript'>$('#teacher_"+teacherDetail.getTeacherId()+"').tooltip();</script>");
				}
				
				boolean CheckForIssue = false; 
				
				if(userMaster.getEntityType()==2)
				{
					CheckForIssue = true;
				}
				
				if(schoolCheckFlag)
					CheckForIssue = true;
				
				if(CheckForIssue == true)
				{
					if(lstdistrictSpecificQuestions.size()>0)
					{
						Boolean qqTaken = qqTeachersMap.get(teacherDetail.getTeacherId());
						if(qqTaken!=null)
						{
							if(qqTaken)
								tmRecords.append(" &nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("lblQualificationIssues", locale)+"' rel='tooltip' id='tpInv"+noOfRecordCheck+"' href='javascript:void(0);'><span class='icon-thumbs-up icon-large thumbUpIconColor' onclick='showQualificationDivForTP("+teacherDetail.getTeacherId()+","+districtId+");'></span></a>");
							else 
								tmRecords.append(" &nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("lblQualificationIssues", locale)+"' rel='tooltip' id='tpInv"+noOfRecordCheck+"' href='javascript:void(0);'><span class='fa-thumbs-down icon-large iconcolorRed' onclick='showQualificationDivForTP("+teacherDetail.getTeacherId()+","+districtId+");'></span></a>");
						}
					}
					
					/*if(lstdistrictSpecificQuestions.size()>0)
					{
						List<JobForTeacher> lstForTeachers = jobForTeacherDAO.findByTeacherIdDistrictId(teacherDetail, districtMaster);
						
						if(lstForTeachers!=null && lstForTeachers.size()>0)
						{
							if(lstForTeachers.get(0).getFlagForDistrictSpecificQuestions()!=null)
							{
								if(lstForTeachers.get(0).getFlagForDistrictSpecificQuestions()==false)
								{
									if(lstForTeachers.get(0).getIsDistrictSpecificNoteFinalize()==null || lstForTeachers.get(0).getIsDistrictSpecificNoteFinalize()==false)
									{
										tmRecords.append(" &nbsp;<a data-original-title='Qualification Issues' rel='tooltip' id='tpInv"+noOfRecordCheck+"' href='javascript:void(0);'><span class='fa-thumbs-down icon-large iconcolorRed' onclick='showQualificationDivForTP("+teacherDetail.getTeacherId()+","+districtId+");'></span></a>");
									}
									else
									{
										tmRecords.append(" &nbsp;<a data-original-title='Qualification Issues' rel='tooltip' id='tpInv"+noOfRecordCheck+"' href='javascript:void(0);'><span class='icon-thumbs-up icon-large thumbUpIconColor' onclick='showQualificationDivForTP("+teacherDetail.getTeacherId()+","+districtId+");'></span></a>");
									}
								}
								else if(lstForTeachers.get(0).getFlagForDistrictSpecificQuestions()==true)
								{
									tmRecords.append(" &nbsp;<a data-original-title='Qualification Issues' rel='tooltip' id='tpInv"+noOfRecordCheck+"' href='javascript:void(0);'><span class='icon-thumbs-up icon-large thumbUpIconColor' onclick='showQualificationDivForTP("+teacherDetail.getTeacherId()+","+districtId+");'></span></a>");
								}
							}else {
								int checkAnswerFlag = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.getFlagForAnswer(lstForTeachers.get(0).getTeacherId(), districtMaster);
								if(checkAnswerFlag>0)
								{
									if(checkAnswerFlag==1)
									{
										lstForTeachers.get(0).setFlagForDistrictSpecificQuestions(true);
										jobForTeacherDAO.makePersistent(lstForTeachers.get(0));
										tmRecords.append(" &nbsp;<a data-original-title='Qualification Issues' rel='tooltip' id='tpInv"+noOfRecordCheck+"' href='javascript:void(0);'><span class='icon-thumbs-up icon-large thumbUpIconColor' onclick='showQualificationDivForTP("+teacherDetail.getTeacherId()+","+districtId+");'></span></a>");
									
									}
									else if(checkAnswerFlag==2)
									{
										lstForTeachers.get(0).setFlagForDistrictSpecificQuestions(false);
										jobForTeacherDAO.makePersistent(lstForTeachers.get(0));
										tmRecords.append(" &nbsp;<a data-original-title='Qualification Issues' rel='tooltip' id='tpInv"+noOfRecordCheck+"' href='javascript:void(0);'><span class='fa-thumbs-down icon-large iconcolorRed' onclick='showQualificationDivForTP("+teacherDetail.getTeacherId()+","+districtId+");'></span></a>");
									}
								}
							}
						}
					}*/
				}
			/* @End
			 * @Ashish Kumar
			 * @Description :: Qualification Details Thumb 
			 * */
				//alok	
				if(entityID!=1)
				{
					EmployeeMaster employeeMaster = empMap.get(teacherDetail.getTeacherId());
					if(employeeMaster!=null)
					{ 
						System.out.println("value "+employeeMaster.getEmailAddress()+"ssn="+employeeMaster.getSSN()+"rrcode==="+employeeMaster.getRRCode()+"firsr name="+employeeMaster.getFirstName());
						if(employeeMaster.getRRCode()!=null && employeeMaster.getRRCode().equalsIgnoreCase("y"))
						{					
							tmRecords.append("<a data-original-title='RR' rel='tooltip' id='tprr"+noOfRecordCheck+"' href='javascript:void(0);'>&nbsp;<img  src=\"images/R-22.png\"></a> ");
							tmRecords.append("<script type='text/javascript'>$('#tprr"+noOfRecordCheck+"').tooltip();</script>");
						}
					}
				}										
				tmRecords.append("</td>");				
				int jobList=jobListByTeacher(teacherDetail.getTeacherId(),jftMap);
				int jobListForTm=jobListByTeacher(teacherDetail.getTeacherId(),jftMapForTm);

				if(normScore==0)
				{
					
						tmRecords.append("<td>N/A</td>");
										
				}
				else
				{
					String ccsName="";
					String normScoreLen=normScore+"";
					if(normScoreLen.length()==1){
						ccsName="nobground1";
					}else if(normScoreLen.length()==2){
						ccsName="nobground2";
					}else{
						ccsName="nobground3";
					}
					
						tmRecords.append("<td><span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>"+normScore+"</span></td>");
					
				
				}
				////////////// A score ////////////////////
				if(entityID!=1 && achievementScore){
					String aScore =cgService.getAchievementMaxScore(lstTeacherAchievementScore,teacherDetail,"A");
					if(aScore!=null){
						tmRecords.append("<td id='teacherPoolaScore_"+teacherDetail.getTeacherId()+"'>"+aScore+"</td>"); //@AShish Added ascore ID
					}else{
						tmRecords.append("<td id='teacherPoolaScore_"+teacherDetail.getTeacherId()+"'>N/A</td>"); //@AShish Added ascore ID
					}
					String lScore =cgService.getAchievementMaxScore(lstTeacherAchievementScore,teacherDetail,"L");
					if(lScore!=null){
						tmRecords.append("<td id='teacherPoollrScore_"+teacherDetail.getTeacherId()+"'>"+lScore+"</td>"); //@AShish Added lrscore ID
					}else{
						tmRecords.append("<td id='teacherPoollrScore_"+teacherDetail.getTeacherId()+"'>N/A</td>"); //@AShish Added lrscore ID
					}
				}
				//////////////////////////// TFA //////////////////////////////
				if(tFA){
					String tFAVal =cgService.getTFA(lstTeacherExperience,teacherDetail);
					if(tFAVal!=null){
						tmRecords.append("<td>"+tFAVal+"</td>");
					}else{
						tmRecords.append("<td>N/A</td>");
					}
				}
				tmRecords.append("<td>");
				if(jobList!=0){                                                                                                   //mukesh
					tmRecords.append("<a href='javascript:void(0);' onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" >"+jobList+"</a>");
				}else{
					tmRecords.append("0");
				}
				tmRecords.append("/"+(jobListForTm-jobList)+"</td>");

				if(teachingOfYear)
				{
					tmRecords.append("<td>");
					if(cgService.getTEYear(lstTeacherExperience,teacherDetail)!=0){
						tmRecords.append(cgService.getTEYear(lstTeacherExperience,teacherDetail));
					}else{
						tmRecords.append("N/A");
					}
					tmRecords.append("</td>");
				}

				if(expectedSalary){
					tmRecords.append("<td>");
					if(cgService.getSalary(lstTeacherPersonalInfo,teacherDetail)!=-1){
						tmRecords.append("$"+cgService.getSalary(lstTeacherPersonalInfo,teacherDetail));
					}else{
						tmRecords.append("N/A");
					}
					tmRecords.append("</td>");
				}

				if(entityID==1){

					tmRecords.append("<td>");
					if(roleAccess.indexOf("|7|")!=-1 && userMaster.getEntityType()==1){
						if(teacherDetail.getStatus().equalsIgnoreCase("A"))
							tmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateTeacher("+teacherDetail.getTeacherId()+",'I')\">Deactivate");
						else
							tmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateTeacher("+teacherDetail.getTeacherId()+",'A')\">Activate");
					}else{
						if(teacherDetail.getStatus().equals("A"))
							tmRecords.append(Utility.getLocaleValuePropByKey("optAct", locale));
						else
							tmRecords.append(Utility.getLocaleValuePropByKey("optInActiv", locale));
					}
					tmRecords.append("</td>");
				}

				tmRecords.append("<td  style='vertical-align: top;padding:0px;'>");
				tmRecords.append("<table border=0 >");
				tmRecords.append("<tr "+gridColor+">");

				tmRecords.append("<td  style='padding: 0px; padding-left:3px; margin: 0px; border: 0px;padding-top:5px;'>");
			    
				tmRecords.append("</td>");
				
				//if(entityID!=1 )
				
				String ccsName		=	"nobground1";
				String colorgray	=	"7F7F7F";	
				String colorblue	=	"007AB4";
				
				if(entityID!=1 && isManage)
				{
					//System.out.println(isManage+"================================="+entityID);
					tmRecords.append("<td  style='padding: 0px; padding-left:3px; margin: 0px; border: 0px;padding-top:5px;'>");
					// mukesh
					tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblApply/AddCandidate", locale)+"' rel='tooltip' id='tpJobApplied"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"getJobDetailList('"+teacherDetail.getTeacherId()+"');getZoneListAll('"+districtId+"')\"><span class='fa-plus-square icon-large iconcolor'></span></a>");
					tmRecords.append("</td>");
				}
				isBase = isbaseComplete.get(teacherDetail.getTeacherId());

				
				if(entityID==1 && roleAccess.indexOf("|7|")!=-1 && userMaster.getEntityType()==1){
					try {
						String IconColor="";
						if(teacherWMap.containsKey(teacherDetail.getTeacherId())){
							IconColor=colorblue;
						}else{
							IconColor=colorgray;
						}
						tmRecords.append("<td  style='padding: 0px; padding-left:0px; margin: 0px; border: 0px;padding-top:5px;'>");
						tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblEPIAccomodation", locale)+"' rel='tooltip' id='epiACC"+noOfRecordCheck+"'  href='javascript:void(0);' onclick=\"openEpiTime('"+teacherDetail.getTeacherId()+"','"+tFname+" "+tLname+"','"+isBase+"')\" ><span class='icon-time icon-large' id='timeIcon"+teacherDetail.getTeacherId()+"' style='color: #"+IconColor+";'></span></a>");
						tmRecords.append("</td>");
					} catch (Exception e) {}
					try
					{
						tmRecords.append("<td  style='padding: 0px; padding-left:0px; margin: 0px; border: 0px;padding-top:5px;'>");
				        
						if(jsiCount.containsKey(teacherDetail.getTeacherId()) || epiCount.containsKey(teacherDetail.getTeacherId()))
				        {
				        	tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblAssessmentDetails", locale)+"' rel='tooltip' id='epiAndJsi"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"showEpiAndJsi("+teacherDetail.getTeacherId()+")\"><span class='fa-file-text icon-large' style='color: #"+colorblue+";'></span></a>");			        	
				    	}
				        else
				        {
				        	tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblNoAssessment", locale)+"' rel='tooltip' id='epiAndJsi"+noOfRecordCheck+"' href='javascript:void(0);'><span class='fa-file-text icon-large' style='color: #"+colorgray+";'></span></a>");
				        }
				        tmRecords.append("</td>");	
					}
					catch(Exception e)
					{e.printStackTrace();}
					
					tmRecords.append("<td style='padding: 0px; padding-left:0px; margin: 0px;padding-top:5px;'>");
					try
					{
						if(isBase==2)
							tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblResetEPI", locale)+"' rel='tooltip' id='tpReset"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"checkBaseViolatedForEPIReset('"+teacherDetail.getTeacherId()+"')\"><span class='icon-refresh  icon-large iconcolor'></span></a>");
						else if(isBase==1)
							tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblEPICompleted", locale)+"' rel='tooltip' id='tpReset"+noOfRecordCheck+"' ><span class='icon-refresh  icon-large iconcolorhover'></span></a>");
						else
							tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("toolNoEPI", locale)+"' rel='tooltip' id='tpReset"+noOfRecordCheck+"'><span class='icon-refresh  icon-large iconcolorhover'></span></a>");
					}
					catch(NullPointerException e)
					{
						tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("toolNoEPI", locale)+"' rel='tooltip' id='tpReset"+noOfRecordCheck+"'><span class='icon-refresh  icon-large iconcolorhover'></span></a>");
					}
				}
				tmRecords.append("</td>");
				tmRecords.append("<td width=5>&nbsp;");
				tmRecords.append("</td>");
				tmRecords.append("</tr>");
				tmRecords.append("</table>");
				tmRecords.append("</td>");
				tmRecords.append("</tr>");
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForTeacherInfoAjax(request,totalRecord,noOfRow, pageNo));
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
	
	
	public String showTagIconFile(SecondaryStatusMaster secondaryStatusMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String path="";
		String source="";
		String target="";
		try 
		{
			if(secondaryStatusMaster.getDistrictMaster()!=null){
				
				source = Utility.getValueOfPropByKey("districtRootPath")+secondaryStatusMaster.getDistrictMaster().getDistrictId()+"/tags/"+secondaryStatusMaster.getPathOfTagsIcon();
				target = context.getServletContext().getRealPath("/")+"/district/"+secondaryStatusMaster.getDistrictMaster().getDistrictId()+"/tags/";
			}else{
				source = Utility.getValueOfPropByKey("rootPath")+"tags/"+secondaryStatusMaster.getPathOfTagsIcon();
				target = context.getServletContext().getRealPath("/")+"/tags/";
			}
			
			
		    File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        FileUtils.copyFile(sourceFile, targetFile);
	        
	        String ext = null;
	        String s = sourceFile.getName();
	        int i = s.lastIndexOf('.');

	        if (i > 0 &&  i < s.length() - 1) {
	            ext = s.substring(i+1).toLowerCase();
	        }
	        
	        /*if(ext.equals("jpeg") || ext.equals("png") || ext.equals("gif") || ext.equals("jpg")){
	        	System.out.println("tag path >>>>>> "+targetDir+"/"+sourceFile.getName());
	        	ImageResize.resizeTag(targetDir+"/"+sourceFile.getName());
	        }*/
	        if(secondaryStatusMaster.getDistrictMaster()!=null){
	        		path = Utility.getValueOfPropByKey("contextBasePath")+"district/"+secondaryStatusMaster.getDistrictMaster().getDistrictId()+"/tags/"+secondaryStatusMaster.getPathOfTagsIcon();	
	        }else{
	        	path = Utility.getValueOfPropByKey("contextBasePath")+"/tags/"+secondaryStatusMaster.getPathOfTagsIcon();
	        }
	       	 
		}catch (Exception e){
			//e.printStackTrace();
		}		
		return path;
	}
	
	public List<JobOrder> getFilterJobOrder(List<JobOrder> jobOrderSList, List<JobOrder> jobOrderCertList)
	{
		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		String jobIds="";

		try{
			for(JobOrder jobOrder: jobOrderSList){
				jobIds+="||"+jobOrder.getJobId()+"||";
			}
		}catch (Exception e) { }
		try{
			for(JobOrder jobOrder: jobOrderCertList){
				if(jobIds.contains("||"+jobOrder.getJobId()+"||")){
					jobOrderList.add(jobOrder);
				}
			}
		}catch (Exception e) {}
		return jobOrderList;
	}
	
	public String getJobListByTeacher(int teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,boolean showCommunication)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		return getJobListByTeacherByProfile(teacherId,noOfRow,pageNo,sortOrder,sortOrderType,showCommunication,"",0,0,null);
	}
	
	
	public String getJobListByTeacherByProfile(int teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,boolean showCommunication,String visitLocation,int jobCategoryId,int subjectId,String zoneIdd)
	{	
		Integer geoZoneId=0;
		if(zoneIdd!=null && zoneIdd!="")
			geoZoneId =  Integer.parseInt(zoneIdd);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		HeadQuarterMaster headQuarterMaster = null;
		BranchMaster branchMaster = null;
		SchoolMaster schoolMaster=null;
		int entityID=0;
		int roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getBranchMaster()!=null){
				branchMaster = userMaster.getBranchMaster();
			}
			if(userMaster.getHeadQuarterMaster()!=null){
				headQuarterMaster = userMaster.getHeadQuarterMaster();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}
			entityID = userMaster.getEntityType();
		}

		boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
		if(entityID==1)
		{
			achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
		}
		else if(entityID==5)
		{
			if(headQuarterMaster.getDisplayDemoClass()!=null && headQuarterMaster.getDisplayDemoClass()){
				demoClass=true;
			}
			if(headQuarterMaster.getDisplayJSI()!=null && headQuarterMaster.getDisplayJSI()){
				JSI=true;
			}
			if(headQuarterMaster.getDisplayFitScore()!=null && headQuarterMaster.getDisplayFitScore()){
				fitScore=true;
			}
		}
		else if(entityID==6)
		{
			if(branchMaster.getDisplayDemoClass()!=null && branchMaster.getDisplayDemoClass()){
				demoClass=true;
			}
			if(branchMaster.getDisplayJSI()!=null && branchMaster.getDisplayJSI()){
				JSI=true;
			}
			if(branchMaster.getDisplayFitScore()!=null && branchMaster.getDisplayFitScore()){
				fitScore=true;
			}			
		}
		else
		{
			try{
				DistrictMaster districtMasterObj=districtMaster;
				if(districtMasterObj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(districtMasterObj.getDisplayTFA()){
					tFA=true;
				}
				if(districtMasterObj.getDisplayDemoClass()){
					demoClass=true;
				}
				if(districtMasterObj.getDisplayJSI()){
					JSI=true;
				}
				if(districtMasterObj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				if(districtMasterObj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				if(districtMasterObj.getDisplayFitScore()){
					fitScore=true;
				}
			}catch(Exception e){ e.printStackTrace();}
		}

		StatusMaster status = null;
		StringBuffer sb = new StringBuffer();	
		int noOfRecordCheck = 0;
		try 
		{	

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------

			/** set default sorting fieldName **/
			String sortOrderFieldName="jobId";
			String sortOrderNoField="jobId";

			boolean deafultFlag=true;
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("JobType") && !sortOrder.equals("jobCategoryId")&& !sortOrder.equals("applicationStatus") && !sortOrder.equals("dateApplied") && !sortOrder.equals("subjectId") && !sortOrder.equals("geoZoneName")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("JobType")){
					sortOrderNoField="JobType";
				}
				if(sortOrder.equals("jobCategoryId")){
					sortOrderNoField="jobCategoryId";
				}
				if(sortOrder.equals("applicationStatus")){
					sortOrderNoField="applicationStatus";
				}
				if(sortOrder.equals("dateApplied")){
					sortOrderNoField="dateApplied";
				}
				if(sortOrder.equals("subjectId")){
					sortOrderNoField="subjectId";
				}
				if(sortOrder.equals("geoZoneName"))
				{
					sortOrderNoField="geoZoneName";
				}
			}
			if(sortOrderNoField.equals("demoClass") || sortOrderNoField.equals("fitScore")){
				sortOrderFieldName="jobId";
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}

			/**End ------------------------------------**/
			String roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			JobCategoryMaster jobCategoryMaster=null;
			SubjectMaster subjectMaster=null;
			if(jobCategoryId > 0)
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
			if(subjectId > 0)
				subjectMaster=subjectMasterDAO.findById(subjectId, false, false);
			
			List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
			TeacherDetail teacherDetail =teacherDetailDAO.findById(teacherId, false, false);
			List<JobForTeacher> jft= jobForTeacherDAO.findJobByTeacher(teacherDetail);
			Map<Integer,JobForTeacher> jftList = new HashMap<Integer, JobForTeacher>();
			for(JobForTeacher jftObj:jft){
				jftList.put(jftObj.getJobId().getJobId(),jftObj);
			}
			Criterion criterion4= Restrictions.eq("districtMaster",districtMaster);
			List<JobOrder> lstJobOrderList = new ArrayList<JobOrder>();//schoolInJobOrderDAO.jobOrderPostedBySchool(schoolMaster);
			Map<Integer,JobOrder> schoolJobMap = new HashMap<Integer, JobOrder>();  
			if(schoolMaster!=null)
			{
				List<JobOrder> lstSchoolJobOrderList = schoolInJobOrderDAO.jobOrderPostedBySchool(schoolMaster);
				for (JobOrder jobOrder : lstSchoolJobOrderList) {
					schoolJobMap.put(jobOrder.getJobId(), jobOrder);
				}
			}
			List<Integer> statusIdList = new ArrayList<Integer>();
			String[] statusShrotName ={"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
			try{
				statusIdList = statusMasterDAO.findStatusIdsByStatusByShortNames(statusShrotName);
			}catch (Exception e) {
				e.printStackTrace();
			}
			Map<Integer,StatusMaster> swcsMap= new HashMap<Integer,StatusMaster>();
			lstJobOrder=jobForTeacherDAO.getJobOrderByTeacher(statusIdList,sortOrderStrVal,teacherDetail,lstJobOrderList,headQuarterMaster,branchMaster,districtMaster,jobCategoryMaster,subjectMaster);
			///////////////Start show school panel status/////////////// 
			List<SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus= new ArrayList<SchoolWiseCandidateStatus>();
			if(userMaster!=null)
				if(userMaster.getEntityType()==3){	
					lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusListHistoryForSchoolByJob(teacherDetail,lstJobOrder,userMaster.getSchoolId());
				}else if(userMaster.getEntityType()==2){
					lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusListHistoryForUser(teacherDetail, lstJobOrder,userMaster);
				}
			if(lstSchoolWiseCandidateStatus.size()>0)
				for (SchoolWiseCandidateStatus schoolWiseCandidateStatus : lstSchoolWiseCandidateStatus) {
					swcsMap.put(schoolWiseCandidateStatus.getJobOrder().getJobId(),schoolWiseCandidateStatus.getStatusMaster());
				}
			///////////////End show school panel status///////////////			
			SortedMap<String,JobOrder>	sortedMap = new TreeMap<String,JobOrder>();
			List<JobOrder> lstJob=new ArrayList<JobOrder>();

			if(sortOrderNoField.equals("JobType")){
				sortOrderFieldName="JobType";
			}
			if(sortOrderNoField.equals("jobCategoryId")){
				sortOrderFieldName="jobCategoryId";
			}
			if(sortOrderNoField.equals("dateApplied")){
				sortOrderFieldName="dateApplied";
			}
			if(sortOrderNoField.equals("applicationStatus")){
				sortOrderFieldName="applicationStatus";
			}
			if(sortOrderNoField.equals("fitScore")){
				sortOrderFieldName="fitScore";
			}
			if(sortOrderNoField.equals("demoClass")){
				sortOrderFieldName="demoClass";
			}
			if(sortOrderNoField.equals("subjectId"))
			{
				sortOrderFieldName	="subjectId";
			}
			if(sortOrderNoField.equals("geoZoneName"))
			{
				sortOrderFieldName	=	"geoZoneName";
			}			
			List<DistrictMaster> districtMasterList= new ArrayList<DistrictMaster>();
			int mapFlag=2;
			try{
				for (JobOrder jobOrder : lstJobOrder){
					districtMasterList.add(jobOrder.getDistrictMaster());
					String orderFieldName=jobOrder.getCreatedDateTime()+"||";
					if(sortOrder!=null){
						if(sortOrderFieldName.equals("JobType")){
							deafultFlag=false;
							String jobType="DJO";
							if(jobOrder.getCreatedForEntity()==3){
								jobType="SJO";
							}
							orderFieldName=jobType+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}if(sortOrderFieldName.equals("jobCategoryId")){
							deafultFlag=false;
							orderFieldName=jobOrder.getJobCategoryMaster().getJobCategoryName()+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
						
						if(jobOrder.getSubjectMaster()!=null){
							orderFieldName=jobOrder.getSubjectMaster().getSubjectName();
							if(sortOrderFieldName.equals("subjectId")){
								orderFieldName=jobOrder.getSubjectMaster().getSubjectName()+"||"+jobOrder.getJobId();
								sortedMap.put(orderFieldName+"||",jobOrder);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
						}else{
							if(sortOrderFieldName.equals("subjectId")){
								orderFieldName="0||"+jobOrder.getJobId();
								sortedMap.put(orderFieldName+"||",jobOrder);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
						}						
						if(sortOrderFieldName.equals("dateApplied")){
							deafultFlag=false;
							//orderFieldName=getDateApplied(jobOrder.getJobId(),jft,teacherDetail)+jobOrder.getJobId();;
							JobForTeacher jftObj=jftList.get(jobOrder.getJobId());
							if(teacherDetail.equals(jftObj.getTeacherId())){
								orderFieldName=Utility.convertDateAndTimeToUSformatOnlyDate(jftObj.getCreatedDateTime())+jobOrder.getJobId();
							}
							sortedMap.put("||"+orderFieldName, jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}if(sortOrderFieldName.equals("applicationStatus")){
							deafultFlag=false;
							JobForTeacher jobFTObj=jftList.get(jobOrder.getJobId());
							status =jobFTObj.getStatusMaster();
							
							if(swcsMap.get(jobOrder.getJobId())!=null){
								status=swcsMap.get(jobOrder.getJobId());
							}
							String statusVal="";
							if(status!=null){	
								if(status.getStatusShortName().equalsIgnoreCase("comp")){
									statusVal=Utility.getLocaleValuePropByKey("lblCompleted", locale);
								}else if(status.getStatusShortName().equalsIgnoreCase("icomp")){
									statusVal=Utility.getLocaleValuePropByKey("msgIn-Complete", locale);
								}else if(status.getStatusShortName().equalsIgnoreCase("rem")){
									statusVal=status.getStatus();
								}else if(status.getStatusShortName().equalsIgnoreCase("hird")){
									statusVal=Utility.getLocaleValuePropByKey("lblHired", locale);
								}else if(status.getStatusShortName().equalsIgnoreCase("vlt")){
									statusVal=Utility.getLocaleValuePropByKey("optTOut", locale);
								}else if(status.getStatusShortName().equalsIgnoreCase("widrw")){
									statusVal= Utility.getLocaleValuePropByKey("lblWithdrew", locale) ; 
								}
							}else {
								if(jobFTObj.getSecondaryStatus()!=null){
									statusVal=jobFTObj.getSecondaryStatus().getSecondaryStatusName();
								}
							}
							
							orderFieldName=statusVal+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
						
						
						if(jobOrder.getGeoZoneMaster()!=null){
							orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName();
							if(sortOrderFieldName.equals("geoZoneName")){
								orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName()+"||"+jobOrder.getJobId();
								sortedMap.put(orderFieldName+"||",jobOrder);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
						}else{
							if(sortOrderFieldName.equals("geoZoneName")){
								orderFieldName="0||"+jobOrder.getJobId();
								sortedMap.put(orderFieldName+"||",jobOrder);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
						}
						
						
						
						
					}

				}
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						lstJob.add((JobOrder) sortedMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedMap.keySet().iterator();
					
					while (iterator.hasNext()) {
						Object key = iterator.next();
						lstJob.add((JobOrder) sortedMap.get(key));
					}
				}else{
					
					lstJob=lstJobOrder;
				}
			}catch(Exception e){
				e.printStackTrace();
			}

// zmukesh 
			
			List<JobOrder> listjobOrdersgeoZone = new ArrayList<JobOrder>();
			  if(geoZoneId>0)
				{
					GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
					listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);
					
				}
		
			  if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
			  {
				  if(deafultFlag==true){
					  lstJobOrder.retainAll(listjobOrdersgeoZone);
					}else{
						lstJobOrder.addAll(listjobOrdersgeoZone);
					}
	
			  }	
			
			  if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
			  {
				  lstJobOrder.addAll(listjobOrdersgeoZone);
			  
			  }
			  else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
			  {
				  lstJobOrder.retainAll(listjobOrdersgeoZone);
			  }
			  
			totalRecord =lstJob.size();

			if(totalRecord<end)
				end=totalRecord;

			Map<Integer,String> scoremap = new HashMap<Integer, String>();
			Map<Integer,String> demoClassmap = new HashMap<Integer, String>();
			if(fitScore)
			{
				scoremap =  jobWiseConsolidatedTeacherScoreDAO.getFitScoreTeacherWise(teacherDetail,lstJob);
				String fit = "0";
				for (JobOrder jobOrder2 : lstJob) {
					fit = scoremap.get(jobOrder2.getJobId());
					if(fit==null)
						jobOrder2.setFitScore(0);
					else
						try{
							String fitScoreVal=fit.split("/")[0];
							jobOrder2.setFitScore(Integer.parseInt(fitScoreVal.replace(".", "#").split("#")[0]));
						}catch(Exception e){
							jobOrder2.setFitScore(0);
							e.printStackTrace();
						}
				}
				if(sortOrderTypeVal.equalsIgnoreCase("1") && sortOrderNoField.equals("fitScore"))
					Collections.sort(lstJob,JobOrder.jobOrderComparatorFitScoreDesc );
				else if(sortOrderTypeVal.equalsIgnoreCase("0") && sortOrderNoField.equals("fitScore"))
					Collections.sort(lstJob,JobOrder.jobOrderComparatorFitScoreAsc );
			}

			if(demoClass)
			{
				demoClassmap = demoClassScheduleDAO.findDemoClassSchedulesTeacherWise(teacherDetail,lstJob);
				String demo = "";
				for (JobOrder jobOrder2 : lstJob) {
					demo = demoClassmap.get(jobOrder2.getJobId());
					if(demo==null)
						jobOrder2.setDemoClass("N/A");
					else
						jobOrder2.setDemoClass(demo);
				}
				if(sortOrderTypeVal.equalsIgnoreCase("1") && sortOrderNoField.equals("demoClass"))
					Collections.sort(lstJob,JobOrder.jobOrderComparatorDemoDesc );
				else if(sortOrderTypeVal.equalsIgnoreCase("0") && sortOrderNoField.equals("demoClass"))
					Collections.sort(lstJob,JobOrder.jobOrderComparatorDemoAsc );
			}


			List<JobOrder> jobLst=lstJob.subList(start,end);
			List<SecondaryStatus> lstSecondaryStatus =	secondaryStatusDAO.findSecondaryStatusWithDistrictList(districtMasterList);
			Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 
			List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobLst);
			List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
			if(assessmentJobRelations1.size()>0){
				for(AssessmentJobRelation ajr: assessmentJobRelations1){
					mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
					adList.add(ajr.getAssessmentId());
				}
				if(adList.size()>0)
				lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentList(teacherDetail,adList);
			}
			for(TeacherAssessmentStatus ts : lstJSI){
				mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
			}
			int[] headerwidth;
			
			if(entityID==1)
				headerwidth = new int[] {40,55,205,70,110,120,80,100,65,70,110};
			else if(fitScore && demoClass)
				headerwidth = new int[] {40,55,205,70,110,130,80,100,65,70,110}; // fitScore and demoClass
			else if(!fitScore && !demoClass)
				headerwidth = new int[] {40,55,310,70,110,130,80,110,0,0,120}; // ! fitScore and ! demoClass
			else{
				//headerwidth = new int[] {40,55,250,110,130,80,100,80,110}; // else
				headerwidth = new int[] {40,55,205,70,110,130,80,100,65,70,110};
			}						
			String responseText="";
			String tablePadding_Data="";
			if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
			{
				tablePadding_Data="padding-left:10px;vertical-align:top;text-align:left;font-weight:normal;line-height:30px;";

				String tablePadding="style='padding-left:10px;vertical-align:top;color:#FFFFFF;line-height:25px;'";
				sb.append("<table id='jobTable' border='0'  width='900' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('job')\">");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblID", locale),sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[0]+"' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTpe", locale),sortOrderNoField,"JobType",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[1]+"' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[2]+"' "+tablePadding+" >"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblZone", locale),sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[3]+"' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSub", locale),sortOrderFieldName,"subjectId",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[4]+"' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCategory", locale),sortOrderNoField,"jobCategoryId",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[5]+"' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDateApplied", locale),sortOrderNoField,"dateApplied",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[6]+"' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblApplicationbrStatus", locale),sortOrderNoField,"applicationStatus",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[7]+"' "+tablePadding+" >"+responseText+"</th>");

				if(fitScore)
				{
					responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblFitbrScore", locale),sortOrderNoField,"fitScore",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='"+headerwidth[8]+"' "+tablePadding+" >"+responseText+"</th>");
				}
				if(demoClass)
				{
					responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDemobrLesson", locale),sortOrderNoField,"demoClass",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='"+headerwidth[9]+"' "+tablePadding+" >"+responseText+"</th>");
				}

				sb.append("<th align=\"left\" width='"+headerwidth[10]+"' "+tablePadding+" >Details</th>");

			}
			else
			{
				tablePadding_Data="";
				sb.append("<table id='jobTable' width='100%' border='0' class='table table-striped'>");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblID", locale),sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
				sb.append("<th width='50px' valign='top'>"+responseText+"</th>");

				//responseText=PaginationAndSorting.responseSortingLink("Job Type",sortOrderNoField,"JobType",sortOrderTypeVal,pgNo);
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTpe", locale),sortOrderNoField,"JobType",sortOrderTypeVal,pgNo);
				sb.append("<th width='60px' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
				sb.append("<th width='140px' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblZone", locale),sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
				sb.append("<th width='70px' valign='top'>"+responseText+"</th>");
			
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSub", locale),sortOrderFieldName,"subjectId",sortOrderTypeVal,pgNo);
				sb.append("<th width='90px' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCategory", locale),sortOrderNoField,"jobCategoryId",sortOrderTypeVal,pgNo);
				sb.append("<th width='110px' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblAppliedbron", locale),sortOrderNoField,"dateApplied",sortOrderTypeVal,pgNo);
				sb.append("<th width='80px' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblApplicationbrStatus", locale),sortOrderNoField,"applicationStatus",sortOrderTypeVal,pgNo);
				sb.append("<th width='110px' valign='top'>"+responseText+"</th>");

				if(fitScore)
				{
					//sb.append("<th width='60px' valign='top'>Fit Score</th>");
					responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblFitbrScore", locale),sortOrderNoField,"fitScore",sortOrderTypeVal,pgNo);
					sb.append("<th width='60px' valign='top'>"+responseText+"</th>");
				}
				if(demoClass)
				{
					//sb.append("<th width='60px' valign='top'>Demo Class</th>");
					responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDemobrLesson", locale),sortOrderNoField,"demoClass",sortOrderTypeVal,pgNo);
					sb.append("<th width='80px' valign='top'>"+responseText+"</th>");
				}

				sb.append("<th width='100px;' valign='top'>");
				sb.append("Details");
				sb.append("</th>");	
			}

			sb.append("</tr>");
			sb.append("</thead>");

			TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			String fitScore1 = "";
			
			boolean isDisplayCheckBox=false;
			if((jobCategoryMaster!=null || jobLst.size()==1) && entityID==2 && visitLocation.equalsIgnoreCase("TeacherPool"))
			{
				isDisplayCheckBox=true;
			}
			sb.append("<input type='hidden' id='txtJobLstNo' value='"+jobLst.size()+"'>");
			
			int iRecordCount=1;
			CandidateGridService cgService= new CandidateGridService();
			boolean schoolFlag=false;
			int iDisplayCount=0;
			for(JobOrder jobOrder:jobLst)
			{
				String sDidplayStatusName="";
				JobForTeacher jobFTObj=jftList.get(jobOrder.getJobId());
				status =jobFTObj.getStatusMaster();
				if(swcsMap.get(jobOrder.getJobId())!=null){
					status=swcsMap.get(jobOrder.getJobId());
				}
				if(status!=null)
				{
					if(status.getStatusShortName().equalsIgnoreCase("comp"))
						sDidplayStatusName=Utility.getLocaleValuePropByKey("optAvble", locale); //sb.append("Available");
					else if(status.getStatusShortName().equalsIgnoreCase("rem") || status.getStatusShortName().equalsIgnoreCase("icomp") || status.getStatusShortName().equalsIgnoreCase("vlt"))
						sDidplayStatusName=status.getStatus(); //sb.append(status.getStatus()); 
					else if(status.getStatusShortName().equalsIgnoreCase("scomp") || status.getStatusShortName().equalsIgnoreCase("ecomp") || status.getStatusShortName().equalsIgnoreCase("vcomp"))
						sDidplayStatusName=cgService.getSecondaryStatusNameByDistrictAndJobCategory(lstSecondaryStatus, status,jobOrder); //sb.append(cgService.getSecondaryStatusNameByDistrictAndJobCategory(lstSecondaryStatus, status,jobOrder));
					else if(status.getStatusShortName().equalsIgnoreCase("dcln"))
						sDidplayStatusName= Utility.getLocaleValuePropByKey("lblDeclined", locale) ; 
					else if(status.getStatusShortName().equalsIgnoreCase("widrw"))
						sDidplayStatusName= Utility.getLocaleValuePropByKey("lblWithdrew", locale) ; 
					else if(status.getStatusShortName().equalsIgnoreCase("hird"))
						sDidplayStatusName=Utility.getLocaleValuePropByKey("lblHired", locale); 
				}
				else
				{
					if(jobFTObj.getSecondaryStatus()!=null)
						sDidplayStatusName=jobFTObj.getSecondaryStatus().getSecondaryStatusName(); //sb.append(jobFTObj.getSecondaryStatus().getSecondaryStatusName());
				}
				boolean bDisplayRow=true;
				
				
				if(bDisplayRow)
				{
					if(schoolMaster!=null)
					{
						if(schoolJobMap.get(jobOrder.getJobId())!=null)
							schoolFlag=false;
						else
							schoolFlag=true;
					}

					iRecordCount++;
					String gridColor="";
					if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
						gridColor="class='bggrid'";
					}

					teacherAssessmentStatusJSI = null;
					noOfRecordCheck++;
					sb.append("<tr "+gridColor+">");

					String jobType="DJO",jobTitle="",jobId="";
					if(jobOrder.getCreatedForEntity()==3){
						jobType="SJO";

						if(schoolFlag)
						{	
							jobTitle=jobOrder.getJobTitle();
							jobId=""+jobOrder.getJobId();
						}else
						{
							jobTitle="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewCG", locale)+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=3'>"+jobOrder.getJobTitle()+"</a>";
							jobId="<a target='blank'  data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewJO", locale)+"' rel='tooltip'  id='jobJobId"+noOfRecordCheck+"'  href='editjoborder.do?JobOrderType=3&JobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobId()+"</a>";
						}

					}else{

						if(schoolFlag)
						{	
							jobTitle=jobOrder.getJobTitle();
							jobId=""+jobOrder.getJobId();
						}else
						{
							jobTitle="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewCG", locale)+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=2'>"+jobOrder.getJobTitle()+"</a>";
							jobId="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewJO", locale)+"' rel='tooltip'  id='jobJobId"+noOfRecordCheck+"' href='editjoborder.do?JobOrderType=2&JobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobId()+"</a>";
						}
					}
					
					int iJobId=jobOrder.getJobId();
					String sCheckBox="<input type=\"checkbox\" name='chkjobId_msu' id='chkjobId_msu"+iJobId+"' value='"+iJobId+"'>&nbsp;";
					if(isDisplayCheckBox)
						jobId="<div>"+sCheckBox+"</div><div style='margin-left:20px;margin-top:-28px;'>"+jobId+"</div>";
					
					sb.append("<td style='text-align: left; vertical-align: middle;width:50px;"+tablePadding_Data+"'>"+jobId+"</td>");

					sb.append("<td style='text-align: left; vertical-align: middle;width:60px;"+tablePadding_Data+"'>"+jobType+"</td>");
					if(jobOrder.getIsPoolJob()==2)
					{
						jobTitle="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewCG", locale)+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=2'>"+jobOrder.getJobTitle()+"</a>";
						sb.append("<td style='text-align: left; vertical-align: middle;width:150px;'><span class='requiredlarge'>*</span>"+jobTitle+"</td>");						
					}
					else
					{	jobTitle="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewCG", locale)+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=2'>"+jobOrder.getJobTitle()+"</a>";
						sb.append("<td style='text-align: left; vertical-align: middle;width:150px;'>"+jobTitle+"</td>");
					}	
					String zone = "";
					if(jobOrder.getGeoZoneMaster()!=null)
					{
						if(jobOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
						{
							sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						}
						else if(jobOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
							//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							sb.append("<td style='text-align: left; vertical-align: middle;width:60px;"+tablePadding_Data+"'><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						}else{
							sb.append("<td style='text-align: left; vertical-align: middle;width:60px;"+tablePadding_Data+"'><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jobOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jobOrder.getDistrictMaster().getDistrictId()+")\";>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						}
					}
					else
					{
						sb.append("<td style='text-align: left; vertical-align: middle;width:60px;"+tablePadding_Data+"'>");
						sb.append(zone);
						sb.append("</td>");
					}					
					if(jobOrder.getSubjectMaster()!=null)
						sb.append("<td style='text-align: left; vertical-align: middle;width:90px;"+tablePadding_Data+"'>"+jobOrder.getSubjectMaster().getSubjectName()+"</td>");
					else
						sb.append("<td style='text-align: left; vertical-align: middle;width:90px;"+tablePadding_Data+"'>&nbsp;</td>");

					sb.append("<td style='text-align: left; vertical-align: middle;width:110px;"+tablePadding_Data+"'>"+jobOrder.getJobCategoryMaster().getJobCategoryName()+"</td>");
					JobForTeacher jftOb=jftList.get(jobOrder.getJobId());
					String dateVal="";
					if(teacherDetail.equals(jftOb.getTeacherId())){
						dateVal=Utility.convertDateAndTimeToUSformatOnlyDate(jftOb.getCreatedDateTime());
					}
					sb.append("<td style='text-align: left; vertical-align: middle;width:80px;"+tablePadding_Data+"' nowrap>"+dateVal+"</td>");
					sb.append("<td style='text-align: left; vertical-align: middle;width:110px;"+tablePadding_Data+"'>");				
					sb.append(sDidplayStatusName);					
					sb.append("</td>");
					if(fitScore)
					{
						fitScore1 = scoremap.get(jobOrder.getJobId());
						sb.append("<td style='text-align: left; vertical-align: middle;width:60px;"+tablePadding_Data+"'>"+(fitScore1==null?"N/A":fitScore1)+"</td>");
					}
					if(demoClass)
					{
						sb.append("<td style='text-align: left; vertical-align: middle;width:80px;"+tablePadding_Data+"'>"+(jobOrder.getDemoClass()==null?"N/A":jobOrder.getDemoClass())+"</td>");
					}

					sb.append("<td style='vertical-align: middle;width:110px;"+tablePadding_Data+"'>");				
					//school checking
					if(!schoolFlag)
					{
						sb.append("&nbsp;");
						if(JSI)
						{
							if(roleAccess.indexOf("|4|")!=-1){
								//teacherAssessmentStatusJSI =getJSIStatus(lstJSI,jobOrder);
								Integer assessmentId=mapAssess.get(jobOrder.getJobId());
								
								teacherAssessmentStatusJSI = mapJSI.get(assessmentId);
								if(teacherAssessmentStatusJSI!=null){
									if(teacherAssessmentStatusJSI.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")){
										sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblCandidateNotCompletedJSI", locale)+"' rel='tooltip' id='tpJSA"+noOfRecordCheck+"'><img src='images/jsi_hover.png'  width='24px'></a>");
									}
									else if(teacherAssessmentStatusJSI.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
										sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lbllblCandidateTimedOut", locale)+"' rel='tooltip' id='tpJSA"+noOfRecordCheck+"'><img src='images/jsi_hover.png'  width='24px'></a>");
									}
									else
									{
										sb.append("<a data-original-title='JSI' rel='tooltip' id='tpJSA"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"generatePilarReport('"+teacherId+"','"+jobOrder.getJobId()+"','tpJSA"+noOfRecordCheck+"');"+windowFunc+"\"><img src='images/jsi.png'  width='24px'></a>");
									}
								}
								else{
									sb.append("<a data-original-title='No JSI' rel='tooltip' id='tpJSA"+noOfRecordCheck+"'><img src='images/jsi_hover.png'  width='24px'></a>");
								}
							}else{
								sb.append("&nbsp;");
							}
						}
						sb.append("&nbsp;");

						if(roleAccess.indexOf("|4|")!=-1  && showCommunication){
							String coverLetter=null;
							JobForTeacher jftObj=jftList.get(jobOrder.getJobId());
							if(jftObj!=null){
								if(jftObj.getCoverLetter()!=null){
									coverLetter=jftObj.getCoverLetter();
								}
							}
							
							if(coverLetter==null || coverLetter.trim().equals("")){
								sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblNoCoverLetter", locale)+"' rel='tooltip' id='jobCL"+noOfRecordCheck+"'><span class='fa-file-text-o icon-large iconcolorhover'></span></a>");
							}
							else{
								sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='jobCL"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"showCoverLetter('"+teacherId+"','"+jobOrder.getJobId()+"');\"><span class='fa-file-text-o icon-large iconcolor'></span></span></a>");
							}
						}else{
							sb.append("&nbsp;");
						}
						sb.append("&nbsp;");
						sb.append("&nbsp;");
						if(roleAccess.indexOf("|4|")!=-1 && showCommunication){
							int jobforTeacherId=0;
							try{
								jobforTeacherId=Integer.parseInt(jftList.get(jobOrder.getJobId()).getJobForTeacherId()+"");
							}catch(Exception e){}
							sb.append("<a href='javascript:void(0);'data-original-title='"+Utility.getLocaleValuePropByKey("headCom", locale)+"' rel='tooltip'  id='jobCom"+noOfRecordCheck+"' onclick=\"getCommunicationsDiv(0,'"+jobforTeacherId+"','"+teacherDetail.getTeacherId()+"','"+jobOrder.getJobId()+"');\" ><span class='icon-dropbox icon-large iconcolor'></span></a>");
						}else{
							sb.append("&nbsp;");
						}
						sb.append("&nbsp;");
					}
					sb.append("</td>");
					sb.append("</tr>");
				}	
					
							
			}
			if(totalRecord>10 && (visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
				sb.append("<tr><td colspan='13' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;line-height:30px;'>");
				sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize1","950"));
				sb.append("</td></tr>");
			}
						
			if(jobLst.size()==0)
			{
				sb.append("<tr><td colspan='13' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;line-height:30px;'>");
				sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
				sb.append("</td></tr>");
			}
			
			sb.append("</table>");


			if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic"))
			{
				sb.append(PaginationAndSorting.getPaginationDoubleString(request,totalRecord,noOfRow, pageNo));
			}

		}catch (Exception e){
			e.printStackTrace();
		}

		return sb.toString();	
	}
	
	public int jobListByTeacher(int teacherId,SortedMap<Long,JobForTeacher> jftMap)
	{
		int jobList=0;
		try{
			Iterator iterator = jftMap.keySet().iterator();
			JobForTeacher jobForTeacher=null;
			while (iterator.hasNext()) {
				Object key = iterator.next();
				jobForTeacher=jftMap.get(key);
				int teacherKey=Integer.parseInt(jobForTeacher.getTeacherId().getTeacherId()+"");
				if(teacherKey==teacherId){
					jobList++;
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return jobList;
	}
	
	
	public String getJobDetailList(int teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{	
		//System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		int entityID=0;
		int roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}
			entityID = userMaster.getEntityType();
		}

		
		StatusMaster status = null;
		StringBuffer sb = new StringBuffer();	
		int noOfRecordCheck = 0;
		try 
		{	

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------

			/** set default sorting fieldName **/
			String sortOrderFieldName="jobId";
			String sortOrderNoField="jobId";

			boolean deafultFlag=true;
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("JobType") && !sortOrder.equals("jobCategoryId")&& !sortOrder.equals("applicationStatus") && !sortOrder.equals("dateApplied") && !sortOrder.equals("subjectId") && !sortOrder.equals("geoZoneName")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("JobType")){
					sortOrderNoField="JobType";
				}
				if(sortOrder.equals("jobCategoryId")){
					sortOrderNoField="jobCategoryId";
				}
				if(sortOrder.equals("applicationStatus")){
					sortOrderNoField="applicationStatus";
				}
				if(sortOrder.equals("dateApplied")){
					sortOrderNoField="dateApplied";
				}
				if(sortOrder.equals("subjectId")){
					sortOrderNoField="subjectId";
				}
				if(sortOrder.equals("geoZoneName"))
				{
					sortOrderNoField="geoZoneName";
				}
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}

			/**End ------------------------------------**/

			List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
			TeacherDetail teacherDetail =teacherDetailDAO.findById(teacherId, false, false);
			List<JobForTeacher> jft= jobForTeacherDAO.findJobByTeacher(teacherDetail);
				
			Map<Integer,JobForTeacher> jftList = new HashMap<Integer, JobForTeacher>();
			for(JobForTeacher jftObj:jft){
				jftList.put(jftObj.getJobId().getJobId(),jftObj);
			}
			Criterion criterion4= Restrictions.eq("districtMaster",districtMaster);
			List<JobOrder> lstJobOrderList = new ArrayList<JobOrder>();
			
			List<Integer> statusIdList = new ArrayList<Integer>();
			String[] statusShrotName = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
			try{
				statusIdList = statusMasterDAO.findStatusIdsByStatusByShortNames(statusShrotName);
			}catch (Exception e) {
				e.printStackTrace();
			}
			List<StatusMaster> lstStatusMaster = new ArrayList<StatusMaster>();
			List<JobOrder> lstAppliedJobOrders = jobForTeacherDAO.findDistrictOrSchoolAppliedJobByTeacherAll(teacherDetail,null,null, districtMaster, lstStatusMaster);
			List<JobOrder> jobOrders = new ArrayList<JobOrder>();
			List<Integer> jobs = new ArrayList<Integer>();
			
			for(JobOrder jb: lstAppliedJobOrders)
			{
				jobs.add(jb.getJobId());
			}
			List<Integer> schooljobs = new ArrayList<Integer>();
			
			if(schoolMaster!=null)
			{
				List<JobOrder> lstSchoolJobOrderList = schoolInJobOrderDAO.jobOrderPostedBySchool(schoolMaster);
				for (JobOrder jobOrder : lstSchoolJobOrderList) {
					schooljobs.add(jobOrder.getJobId());
				}
			}
			List<JobOrder> jobOrderList = null;
			List<JobOrder> jobOrderListAll = null;
			if(schoolMaster!=null && schooljobs.size()==0)
				jobOrderList = new ArrayList<JobOrder>();
			else
				{
					//jobOrderListAll = jobOrderDAO.findAllActiveJobOrderbyDistrict(sortOrderStrVal,start,end,districtMaster,jobs,schoolMaster,schooljobs,true);
					jobOrderList = jobOrderDAO.findAllActiveJobOrderbyDistrict(sortOrderStrVal,start,end,null,null,districtMaster,jobs,schoolMaster,schooljobs,true);
				}
		
			//lstJobOrder=jobForTeacherDAO.getJobOrderByTeacher(statusIdList,sortOrderStrVal,teacherDetail,lstJobOrderList,districtMaster);
			lstJobOrder = jobOrderList; 
			SortedMap<String,JobOrder>	sortedMap = new TreeMap<String,JobOrder>();
			List<JobOrder> lstJob=new ArrayList<JobOrder>();

			if(sortOrderNoField.equals("JobType")){
				sortOrderFieldName="JobType";
			}
			if(sortOrderNoField.equals("jobCategoryId")){
				sortOrderFieldName="jobCategoryId";
			}
			if(sortOrderNoField.equals("dateApplied")){
				sortOrderFieldName="dateApplied";
			}
			if(sortOrderNoField.equals("subjectId")){
				sortOrderFieldName="subjectId";
			}
			if(sortOrderNoField.equals("geoZoneName"))
			{
				sortOrderFieldName	=	"geoZoneName";
			}
			List<DistrictMaster> districtMasterList= new ArrayList<DistrictMaster>();
			int mapFlag=2;
			try{
				for (JobOrder jobOrder : lstJobOrder){
					districtMasterList.add(jobOrder.getDistrictMaster());
					String orderFieldName=jobOrder.getCreatedDateTime()+"||";
					if(sortOrder!=null){
						if(sortOrderFieldName.equals("JobType")){
							deafultFlag=false;
							String jobType="DJO";
							if(jobOrder.getCreatedForEntity()==3){
								jobType="SJO";
							}
							orderFieldName=jobType+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}if(sortOrderFieldName.equals("jobCategoryId")){
							deafultFlag=false;
							orderFieldName=jobOrder.getJobCategoryMaster().getJobCategoryName()+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
						if(jobOrder.getSubjectMaster()!=null){
							deafultFlag=false;
							orderFieldName=jobOrder.getSubjectMaster().getSubjectName();
							if(sortOrderFieldName.equals("subjectId")){
								orderFieldName=jobOrder.getSubjectMaster().getSubjectName()+"||"+jobOrder.getJobId();
								sortedMap.put(orderFieldName+"||",jobOrder);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
								
							}
					    }	
						else{
							if(sortOrderFieldName.equals("subjectId")){
								deafultFlag=false;
								orderFieldName="0||"+jobOrder.getJobId();
								sortedMap.put(orderFieldName+"||",jobOrder);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}								
							}				
						 }
						
						
						if(jobOrder.getGeoZoneMaster()!=null){
							orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName();
							if(sortOrderFieldName.equals("geoZoneName")){
								orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName()+"||"+jobOrder.getJobId();
								sortedMap.put(orderFieldName+"||",jobOrder);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
						}else{
							if(sortOrderFieldName.equals("geoZoneName")){
								orderFieldName="0||"+jobOrder.getJobId();
								sortedMap.put(orderFieldName+"||",jobOrder);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
						}
						
						 if(sortOrderFieldName.equals("dateApplied")){
							deafultFlag=false;
							JobForTeacher jftObj=jftList.get(jobOrder.getJobId());
							if(teacherDetail.equals(jftObj.getTeacherId())){
								orderFieldName=Utility.convertDateAndTimeToUSformatOnlyDate(jftObj.getCreatedDateTime())+jobOrder.getJobId();
							}
							sortedMap.put("||"+orderFieldName, jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
						 if(sortOrderFieldName.equals("applicationStatus")){
							deafultFlag=false;
							JobForTeacher jobFTObj=jftList.get(jobOrder.getJobId());
							status =jobFTObj.getStatusMaster();
							String statusVal="";
							if(status!=null){	
								if(status.getStatusShortName().equalsIgnoreCase("comp")){
									statusVal=Utility.getLocaleValuePropByKey("lblCompleted", locale);
								}else if(status.getStatusShortName().equalsIgnoreCase("icomp")){
									statusVal=Utility.getLocaleValuePropByKey("msgIn-Complete", locale);
								}else if(status.getStatusShortName().equalsIgnoreCase("rem")){
									statusVal=status.getStatus();
								}else if(status.getStatusShortName().equalsIgnoreCase("hird")){
									statusVal=Utility.getLocaleValuePropByKey("lblHired", locale);
								}else if(status.getStatusShortName().equalsIgnoreCase("vlt")){
									statusVal=Utility.getLocaleValuePropByKey("optTOut", locale);
								}
							}else {
								if(jobFTObj.getSecondaryStatus()!=null){
									statusVal=jobFTObj.getSecondaryStatus().getSecondaryStatusName();
								}
							}
							
							orderFieldName=statusVal+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
					}				
				}
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						lstJob.add((JobOrder) sortedMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedMap.keySet().iterator();
					
					while (iterator.hasNext()) {
						Object key = iterator.next();
						lstJob.add((JobOrder) sortedMap.get(key));
					}
				}else{
					
					lstJob=lstJobOrder;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
           // System.out.println("jobOrderList.size()=="+jobOrderList.size());
			totalRecord =lstJob.size();
			if(totalRecord<end)
				end=totalRecord;

			List<JobOrder> jobLst=lstJob.subList(start,end);

			String responseText="";
			sb.append("<table id='jobTableLst' width='100%' border='0'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th width='70px' valign='top'>&nbsp;</th>");
			
			responseText=PaginationAndSorting.responseSortingLink( Utility.getLocaleValuePropByKey("lblID", locale)  ,sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
			sb.append("<th width='70px' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTpe", locale),sortOrderNoField,"JobType",sortOrderTypeVal,pgNo);
			sb.append("<th width='70px' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
			sb.append("<th width='70px' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblZone", locale),sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
			sb.append("<th width='50px' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSub", locale),sortOrderFieldName,"subjectId",sortOrderTypeVal,pgNo);
			sb.append("<th width='80px' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCategory", locale),sortOrderFieldName,"jobCategoryId",sortOrderTypeVal,pgNo);
			sb.append("<th width='80px' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Created Date",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			sb.append("<th width='80px' valign='top'>"+responseText+"</th>");

			sb.append("</tr>");
			sb.append("</thead>");

			boolean schoolFlag=false;
			for(JobOrder jobOrder:jobLst)
			{	
				sb.append("<tr>");
				sb.append("<td style='text-align: left; vertical-align: middle;padding-left:5px;'><input type='checkbox' class='case' name='case' value='"+jobOrder.getJobId()+"'> </td>");
				String jobType="DJO",jobTitle="",jobId="";
				if(jobOrder.getCreatedForEntity()==3){
					jobType="SJO";

					if(schoolFlag)
					{	
						jobTitle=jobOrder.getJobTitle();
						jobId=""+jobOrder.getJobId();
					}else
					{
						jobTitle="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewCG", locale)+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=3'>"+jobOrder.getJobTitle()+"</a>";
						jobId="<a target='blank'  data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewJO", locale)+"' rel='tooltip'  id='jobJobId"+noOfRecordCheck+"'  href='editjoborder.do?JobOrderType=3&JobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobId()+"</a>";
					}

				}else{

					if(schoolFlag)
					{	
						jobTitle=jobOrder.getJobTitle();
						jobId=""+jobOrder.getJobId();
					}else
					{
						jobTitle="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewCG", locale)+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=2'>"+jobOrder.getJobTitle()+"</a>";
						jobId="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewJO", locale)+"' rel='tooltip'  id='jobJobId"+noOfRecordCheck+"' href='editjoborder.do?JobOrderType=2&JobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobId()+"</a>";
					}
				}
				sb.append("<td style='text-align: left; vertical-align: middle;padding-left:5px;'>"+jobId+"</td>");

				sb.append("<td style='text-align: left; vertical-align: middle;'>"+jobType+"</td>");
				if(jobOrder.getIsPoolJob()==2)
				{
					jobTitle="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewCG", locale)+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=3'>"+jobOrder.getJobTitle()+"</a>";
					sb.append("<td style='text-align: left; vertical-align: middle;'><span class='requiredlarge'>*</span>"+jobTitle+"</td>");						
				}
				else
				{
					jobTitle="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewCG", locale)+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=3'>"+jobOrder.getJobTitle()+"</a>";
					sb.append("<td style='text-align: left; vertical-align: middle;'>"+jobTitle+"</td>");
				}	
				if(jobOrder.getGeoZoneMaster()!=null && !jobOrder.getGeoZoneMaster().getGeoZoneName().equals(""))
				{	
					if(jobOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
					{
						sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}
					else if(jobOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
						//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}else{
						sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jobOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jobOrder.getDistrictMaster().getDistrictId()+")\";>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}
				}
				else
				{
					sb.append("<td>");							
					sb.append("</td>");
				}				
				
				if(jobOrder.getSubjectMaster()!=null)
					sb.append("<td style='text-align: left; vertical-align: middle;'>"+jobOrder.getSubjectMaster().getSubjectName()+"</td>");
				else
					sb.append("<td style='text-align: left; vertical-align: middle;'>&nbsp;</td>");

				sb.append("<td style='text-align: left; vertical-align: middle;'>"+jobOrder.getJobCategoryMaster().getJobCategoryName()+"</td>");
				
				String dateVal="";
			    dateVal=Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getCreatedDateTime());
				
				sb.append("<td style='text-align: left; vertical-align: middle;' nowrap>"+dateVal+"</td>");
				sb.append("</tr>");
			}
			// @AShish :: when records not found
			if(jobLst.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='6'>");
				
				sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
				sb.append("</td>");	   				
				sb.append("</tr>");
			}	
			sb.append("</table>");
			//sb.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
			sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize12","950"));

			
		}catch (Exception e){
			e.printStackTrace();
		}

		return sb.toString();	
	}
	
	public boolean activateDeactivateTeacher(int teacherId,String status)
	{
		System.out.println(" activateDeactivateTeacher ");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			teacherDetail.setStatus(status);
			teacherDetailDAO.makePersistent(teacherDetail);
		}catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public String getJobDescription(int jobId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String jobDesc="";
		try{
			JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
			jobDesc=jobOrder.getJobDescription();
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
		return jobDesc;
	}
	
	public String showPictureWeb(int teacherId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String path="";
		String source="";
		String target="";
		try 
		{
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			String  docFileName=teacherDetail.getIdentityVerificationPicture();
			source = Utility.getValueOfPropByKey("teacherRootPath")+teacherId+"/"+docFileName;
			target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherId+"/";
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherId+"/"+docFileName;

		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
		}

		return path;
	}
	
	
	public String applyJobs(int teacherId,Integer[] jobs)
	{	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		
	/*	System.out.println(":::::::::::::::::::applyJobs::::::::::::::::::::");
		int entityID;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}
			entityID = userMaster.getEntityType();
		}
		
		TeacherDetail teacherDetail =teacherDetailDAO.findById(teacherId, false, false);
		List<JobOrder> result = jobOrderDAO.findByAllJobOrders(jobs); 
		
		TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
		
		AssessmentDetail assessmentDetail = null;
		TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
		
		Map<String,StatusMaster> statusMap = new HashMap<String, StatusMaster>();
		List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
		String[] statuss = {"comp","icomp","vlt"};
		try{
			lstStatusMasters = Utility.getStaticMasters(statuss);
			for (StatusMaster statusMaster : lstStatusMasters) {
				statusMap.put(statusMaster.getStatusShortName(), statusMaster);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		List<JobOrder> jsiJobs = new ArrayList<JobOrder>();
		for (JobOrder jobOrder : result) {
			if(jobOrder.getIsJobAssessment())
				jsiJobs.add(jobOrder);
		}
		Map<Integer,AssessmentJobRelation> jsiARmap = new HashMap<Integer, AssessmentJobRelation>();
		if(jsiJobs.size()>0)
		{
			List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrdersNoDate(jsiJobs);
			for (AssessmentJobRelation assessmentJobRelation : lstAssessmentJobRelation) {
				jsiARmap.put(assessmentJobRelation.getJobId().getJobId(), assessmentJobRelation);
			}
		}
		
		String isAffilated ="0"; 
		JobForTeacher jobForTeacher = null;
		for (JobOrder jobOrder : result) {
			jobForTeacher = new JobForTeacher();
			jobForTeacher.setTeacherId(teacherDetail);
			jobForTeacher.setJobId(jobOrder);
			jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
			jobForTeacher.setCoverLetter("");
			jobForTeacher.setIsAffilated(Integer.parseInt(isAffilated));
			jobForTeacher.setCreatedDateTime(new Date());
			StatusMaster statusMaster = null;
			
			 int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
			 statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
			 boolean completeFlag=false;
			 try{
					String jftStatus=jobForTeacher.getStatus().getStatusShortName();
					System.out.println(statusMaster.getStatusShortName()+":::::jftStatus::::::::::::::7:"+jftStatus);
					if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
						completeFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			try {
				jobForTeacher.setStatus(statusMaster);
				jobForTeacher.setStatusMaster(statusMaster);
				jobForTeacherDAO.makePersistent(jobForTeacher);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
			 try{
				 if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
					System.out.println("::::::::::::::Update Status:::::::::::7:::::");
					commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
				 }
			 }catch(Exception e){
				 e.printStackTrace();
			 }
		
		}
		
		String JobBoardURL="";
		try{
			JobBoardURL=Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?districtId="+Utility.encryptNo(districtMaster.getDistrictId()));
		}catch (Exception e) {
			// TODO: handle exception
		}

		if(result.size()>0)
		{
			String emailto = teacherDetail.getEmailAddress();
			//emailto = "vishwanath@netsutra.com";
			String mailContent =null;
			DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
			
			districtMaster = result.get(0).getDistrictMaster();
			String districtName="";
			if(districtMaster!=null){
				if(districtMaster.getDisplayName()!=null && !districtMaster.getDisplayName().equals("")){
					districtName=districtMaster.getDisplayName();
				}else{
					districtName=districtMaster.getDistrictName();
				}
			}
			
			mailContent = MailText.notifyJobAppliedMailText(request,result,userMaster,teacherDetail,districtName,JobBoardURL,true);
			dsmt.setEmailerService(emailerService);
			dsmt.setMailfrom("noreply@teachermatch.net");
			dsmt.setMailto(emailto);
			dsmt.setMailsubject("Message from "+districtName);
			dsmt.setMailcontent(mailContent);
			dsmt.start();
			
			emailto = userMaster.getEmailAddress();
			//emailto = "vishwanath@netsutra.com";
			dsmt = new DemoScheduleMailThread();
			mailContent = MailText.notifyJobAppliedMailText(request,result,userMaster,teacherDetail,districtName,JobBoardURL,false);
			dsmt.setEmailerService(emailerService);
			dsmt.setMailfrom("noreply@teachermatch.net");
			dsmt.setMailto(emailto);
			dsmt.setMailsubject("Added an existing Candidate to other job(s)");
			dsmt.setMailcontent(mailContent);
			dsmt.start();
		}*/
		return "";
	}
	
	
	
	
	public String searchJobDetailList(int teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String schoolForSearch,String jobCategoryId,String jobIdForSearch,String subjectId,Integer geoZoneId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		int entityID=0;
		int roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}
			entityID = userMaster.getEntityType();
		}

		StatusMaster status = null;
		StringBuffer sb = new StringBuffer();	
		int noOfRecordCheck = 0;
		try 
		{	

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------

			/** set default sorting fieldName **/
			String sortOrderFieldName="jobId";
			String sortOrderNoField="jobId";

			boolean deafultFlag=true;
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("JobType") && !sortOrder.equals("jobCategoryId")&& !sortOrder.equals("applicationStatus") && !sortOrder.equals("dateApplied") && !sortOrder.equals("subjectId")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("JobType")){
					sortOrderNoField="JobType";
				}
				if(sortOrder.equals("jobCategoryId")){
					sortOrderNoField="jobCategoryId";
				}
				if(sortOrder.equals("applicationStatus")){
					sortOrderNoField="applicationStatus";
				}
				if(sortOrder.equals("dateApplied")){
					sortOrderNoField="dateApplied";
				}
				if(sortOrder.equals("subjectId")){
					sortOrderNoField="subjectId";
				}

			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}

			/**End ------------------------------------**/

			List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
			TeacherDetail teacherDetail =teacherDetailDAO.findById(teacherId, false, false);
			List<JobForTeacher> jft= jobForTeacherDAO.findJobByTeacher(teacherDetail);
				
			Map<Integer,JobForTeacher> jftList = new HashMap<Integer, JobForTeacher>();
			for(JobForTeacher jftObj:jft){
				jftList.put(jftObj.getJobId().getJobId(),jftObj);
			}
			Criterion criterion4= Restrictions.eq("districtMaster",districtMaster);
			List<JobOrder> lstJobOrderList = new ArrayList<JobOrder>();
			
			List<Integer> statusIdList = new ArrayList<Integer>();
			String[] statusShrotName = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
			try{
				statusIdList = statusMasterDAO.findStatusIdsByStatusByShortNames(statusShrotName);
			}catch (Exception e) {
				e.printStackTrace();
			}
			List<StatusMaster> lstStatusMaster = new ArrayList<StatusMaster>();
			List<JobOrder> lstAppliedJobOrders = jobForTeacherDAO.findDistrictOrSchoolAppliedJobByTeacherAll(teacherDetail,null,null,districtMaster, lstStatusMaster);
			List<JobOrder> jobOrders = new ArrayList<JobOrder>();
			List<Integer> jobs = new ArrayList<Integer>();
			
			for(JobOrder jb: lstAppliedJobOrders)
			{
				jobs.add(jb.getJobId());
			}
			List<Integer> schooljobs = new ArrayList<Integer>();
			
			if(schoolMaster!=null)
			{
				List<JobOrder> lstSchoolJobOrderList = schoolInJobOrderDAO.jobOrderPostedBySchool(schoolMaster);
				for (JobOrder jobOrder : lstSchoolJobOrderList) {
					schooljobs.add(jobOrder.getJobId());
				}
			}
			List<JobOrder> jobOrderList = null;
			List<JobOrder> jobOrderListAll = null;
			
			boolean flagJobOrderList = false; //@AShish
			List<JobOrder> jobOrderAllList = new ArrayList<JobOrder>(); //@Ashsih
			List<JobOrder> schoolMasterlst = new ArrayList<JobOrder>(); //@Ashsih
			List<JobOrder> jobCategoryList = new ArrayList<JobOrder>(); //@Ashsih
			List<JobOrder> jobIdList = new ArrayList<JobOrder>(); //@Ashsih
			List<JobOrder> subjectList = new ArrayList<JobOrder>(); //@Ashsih
			
			if(schoolMaster!=null && schooljobs.size()==0)
				jobOrderList = new ArrayList<JobOrder>();
			else
				{
					//jobOrderListAll = jobOrderDAO.findAllActiveJobOrderbyDistrict(sortOrderStrVal,start,end,districtMaster,jobs,schoolMaster,schooljobs,true);
					jobOrderList = jobOrderDAO.findAllActiveJobOrderbyDistrict(sortOrderStrVal,start,end,null,null,districtMaster,jobs,schoolMaster,schooljobs,true);
					jobOrderAllList.addAll(jobOrderList);
					flagJobOrderList = true;
					
					for(JobOrder j:jobOrderAllList){
					}
				}
		
			/* @ Start
			 * @ Ashish Kumar
			 * @ Description :: COde for search data
			 * */
				
				/*boolean flagSchoolForSearch = false;
				boolean flagJobCategory = false;
				boolean flagJobIdForSearch = false;
				boolean flagSubject = false;*/
			
				
				ArrayList<Integer> subjectIdList=  new ArrayList<Integer>();
				List<SubjectMaster> ObjSubjectList = new ArrayList<SubjectMaster>();	
				
				
				
				// For School Search
				if(!schoolForSearch.equals("0") && !schoolForSearch.equals(""))
				{
				//	flagSchoolForSearch = true;
					
					schoolMaster 	=	schoolMasterDAO.findById(new Long(schoolForSearch), false, false);
					schoolMasterlst.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, null,ObjSubjectList));
					
					if(flagJobOrderList==true){
						jobOrderAllList.retainAll(schoolMasterlst);
					}else{
						jobOrderAllList.addAll(schoolMasterlst);
					}
				}
		
				// For JobCategory Search
				if(!jobCategoryId.equals("0") && !jobCategoryId.equals(""))
				{
				//	flagJobCategory = true;
					
					JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(Integer.parseInt(jobCategoryId), false, false);
					jobCategoryList	=	jobOrderDAO.findJobOrderByJobCategoryMaster(jobCategoryMaster);
					
					if(flagJobOrderList==true){
						jobOrderAllList.retainAll(jobCategoryList);
					}else{
						jobOrderAllList.addAll(jobCategoryList);
					}
				}
				
				// For Job ID Search
				if(!jobIdForSearch.equals("0") && !jobIdForSearch.equals(""))
				{
				//	flagJobIdForSearch = true;
					
					JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(jobIdForSearch), false, false);
					jobIdList.add(jobOrder);
					
					if(flagJobOrderList==true){
						jobOrderAllList.retainAll(jobIdList);
					}else{
						jobOrderAllList.addAll(jobIdList);
					}
				
				}
				
				// For subjectList Search
				if(!subjectId.equals("0,") && !subjectId.equals(""))
				{
				//	flagSubject = true;
					
					String subIdList[] = subjectId.split(",");
					if(Integer.parseInt(subIdList[0])!=0)
					{
						for(int i=0;i<subIdList.length;i++)
						{
							subjectIdList.add(Integer.parseInt(subIdList[i]));
							
						}
						ObjSubjectList = subjectMasterDAO.getSubjectMasterlist(subjectIdList);
						
						subjectList = jobOrderDAO.findJobOrderBySubjectList(ObjSubjectList);
						
						if(flagJobOrderList==true){
							jobOrderAllList.retainAll(subjectList);
						}else{
							jobOrderAllList.addAll(jobIdList);
						}
					}
				}
				
				
				/* @ End
				 * @ Ashish Kumar
				 * @ Description :: COde for search data
				 * */
				
// filtering zone wise by mukesh
				
				List<JobOrder> listjobOrdersgeoZone = new ArrayList<JobOrder>();
				  if(geoZoneId>0)
					{
						GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
						listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);
						
					}
			
				  if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
				  {
					  if(flagJobOrderList==true){
							jobOrderAllList.retainAll(listjobOrdersgeoZone);
						}else{
							jobOrderAllList.addAll(listjobOrdersgeoZone);
						}
		          }					
				
				
				 
				lstJobOrder = jobOrderAllList;
				
			SortedMap<String,JobOrder>	sortedMap = new TreeMap<String,JobOrder>();
			List<JobOrder> lstJob=new ArrayList<JobOrder>();

			if(sortOrderNoField.equals("JobType")){
				sortOrderFieldName="JobType";
			}
			if(sortOrderNoField.equals("jobCategoryId")){
				sortOrderFieldName="jobCategoryId";
			}
			if(sortOrderNoField.equals("dateApplied")){
				sortOrderFieldName="dateApplied";
			}
			if(sortOrderNoField.equals("subjectId")){
				sortOrderFieldName="subjectId";
			}
			System.out.println("sortOrderFieldName"+sortOrderFieldName);
			List<DistrictMaster> districtMasterList= new ArrayList<DistrictMaster>();

			try{
				for (JobOrder jobOrder : lstJobOrder){
					districtMasterList.add(jobOrder.getDistrictMaster());
					String orderFieldName=jobOrder.getCreatedDateTime()+"||";
					if(sortOrder!=null){
						if(sortOrderFieldName.equals("JobType")){
							deafultFlag=false;
							String jobType="DJO";
							if(jobOrder.getCreatedForEntity()==3){
								jobType="SJO";
							}
							orderFieldName=jobType+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
						}else if(sortOrderFieldName.equals("jobCategoryId")){
							deafultFlag=false;
							orderFieldName=jobOrder.getJobCategoryMaster().getJobCategoryName()+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
						}
						else if(sortOrderFieldName.equals("subjectId")){
							deafultFlag=false;
							orderFieldName=jobOrder.getSubjectMaster().getSubjectName()+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
						}else if(sortOrderFieldName.equals("dateApplied")){
							deafultFlag=false;
							//orderFieldName=getDateApplied(jobOrder.getJobId(),jft,teacherDetail)+jobOrder.getJobId();;
							JobForTeacher jftObj=jftList.get(jobOrder.getJobId());
							if(teacherDetail.equals(jftObj.getTeacherId())){
								orderFieldName=Utility.convertDateAndTimeToUSformatOnlyDate(jftObj.getCreatedDateTime())+jobOrder.getJobId();
							}
							sortedMap.put("||"+orderFieldName, jobOrder);
						}else if(sortOrderFieldName.equals("applicationStatus")){
							deafultFlag=false;
							JobForTeacher jobFTObj=jftList.get(jobOrder.getJobId());
							status =jobFTObj.getStatusMaster();
							String statusVal="";
							if(status!=null){	
								if(status.getStatusShortName().equalsIgnoreCase("comp")){
									statusVal=Utility.getLocaleValuePropByKey("lblCompleted", locale);
								}else if(status.getStatusShortName().equalsIgnoreCase("icomp")){
									statusVal=Utility.getLocaleValuePropByKey("msgIn-Complete", locale);
								}else if(status.getStatusShortName().equalsIgnoreCase("rem")){
									statusVal=status.getStatus();
								}else if(status.getStatusShortName().equalsIgnoreCase("hird")){
									statusVal=Utility.getLocaleValuePropByKey("lblHired", locale);
								}else if(status.getStatusShortName().equalsIgnoreCase("vlt")){
									statusVal=Utility.getLocaleValuePropByKey("optTOut", locale);
								}
							}else {
								if(jobFTObj.getSecondaryStatus()!=null){
									statusVal=jobFTObj.getSecondaryStatus().getSecondaryStatusName();
								}
							}
							
							orderFieldName=statusVal+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
						}else{
							lstJob.add(jobOrder);
						}
					}

				}
				if(deafultFlag==false && sortOrderTypeVal.equals("1")){
					NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						lstJob.add((JobOrder) sortedMap.get(key));
					} 
				}else if(deafultFlag==false){
					Iterator iterator = sortedMap.keySet().iterator();
					while (iterator.hasNext()) {
						Object key = iterator.next();
						lstJob.add((JobOrder) sortedMap.get(key));
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			totalRecord =jobOrderAllList.size();
			if(totalRecord<end)
				end=totalRecord;

			List<JobOrder> jobLst=jobOrderAllList.subList(start,end);

			String responseText="";

			sb.append("<table id='jobTableLst' width='100%' border='0' class='table table-striped'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th width='70px' valign='top'>&nbsp;</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblID", locale),sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
			sb.append("<th width='70px' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTpe", locale),sortOrderNoField,"JobType",sortOrderTypeVal,pgNo);
			sb.append("<th width='70px' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
			sb.append("<th width='90px' valign='top'>"+responseText+"</th>");			

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblZone", locale),sortOrderFieldName,"geoZoneMaster",sortOrderTypeVal,pgNo);
			sb.append("<th width='50px' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSub", locale),sortOrderFieldName,"subjectId",sortOrderTypeVal,pgNo);
			sb.append("<th width='90px' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCategory", locale),sortOrderFieldName,"subjectId",sortOrderTypeVal,pgNo);
			sb.append("<th width='90px' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Created Date",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			sb.append("<th width='90px' valign='top'>"+responseText+"</th>");

			sb.append("</tr>");
			sb.append("</thead>");

			boolean schoolFlag=false;
			for(JobOrder jobOrder:jobLst)
			{	
				sb.append("<tr>");
				sb.append("<td style='text-align: left; vertical-align: middle;padding-left:5px;'><input type='checkbox' class='case' name='case' value='"+jobOrder.getJobId()+"'> </td>");
				String jobType="DJO",jobTitle="",jobId="";
				if(jobOrder.getCreatedForEntity()==3){
					jobType="SJO";

					if(schoolFlag)
					{	
						jobTitle=jobOrder.getJobTitle();
						jobId=""+jobOrder.getJobId();
					}else
					{
						jobTitle="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewCG", locale)+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=3'>"+jobOrder.getJobTitle()+"</a>";
						jobId="<a target='blank'  data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewJO", locale)+"' rel='tooltip'  id='jobJobId"+noOfRecordCheck+"'  href='editjoborder.do?JobOrderType=3&JobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobId()+"</a>";
					}

				}else{

					if(schoolFlag)
					{	
						jobTitle=jobOrder.getJobTitle();
						jobId=""+jobOrder.getJobId();
					}else
					{
						jobTitle="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewCG", locale)+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=2'>"+jobOrder.getJobTitle()+"</a>";
						jobId="<a target='blank' data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewJO", locale)+"' rel='tooltip'  id='jobJobId"+noOfRecordCheck+"' href='editjoborder.do?JobOrderType=2&JobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobId()+"</a>";
					}
				}
				sb.append("<td style='text-align: left; vertical-align: middle;padding-left:5px;'>"+jobId+"</td>");

				sb.append("<td style='text-align: left; vertical-align: middle;'>"+jobType+"</td>");
				if(jobOrder.getIsPoolJob()==2)
				{
					sb.append("<td style='text-align: left; vertical-align: middle;'><span class='requiredlarge'>*</span>"+jobOrder.getJobTitle()+"</td>");						
				}
				else
				{
					sb.append("<td style='text-align: left; vertical-align: middle;'>"+jobOrder.getJobTitle()+"</td>");
				}
				if(jobOrder.getGeoZoneMaster()!=null && !jobOrder.getGeoZoneMaster().getGeoZoneName().equals(""))
				{	
					if(jobOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
					{
						sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}
					else if(jobOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
						//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}else{
						sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jobOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jobOrder.getDistrictMaster().getDistrictId()+")\";>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}
				}
				else
				{
					sb.append("<td>");							
					sb.append("</td>");
				}	
								
				if(jobOrder.getSubjectMaster()!=null)
					sb.append("<td style='text-align: left; vertical-align: middle;'>"+jobOrder.getSubjectMaster().getSubjectName()+"</td>");
				else
					sb.append("<td style='text-align: left; vertical-align: middle;'>&nbsp;</td>");

				sb.append("<td style='text-align: left; vertical-align: middle;'>"+jobOrder.getJobCategoryMaster().getJobCategoryName()+"</td>");
				
				String dateVal="";
			    dateVal=Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getCreatedDateTime());
				
				sb.append("<td style='text-align: left; vertical-align: middle;' nowrap>"+dateVal+"</td>");
				sb.append("</tr>");
			}
			// @AShish :: when records not found
			if(jobLst.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='6'>");
				
				sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
				sb.append("</td>");	   				
				sb.append("</tr>");
			}	
			sb.append("</table>");
			//sb.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
			sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize12","950"));

			
		}catch (Exception e){
			e.printStackTrace();
		}

		return sb.toString();	
	}
	
	
public String saveTags(Integer districtId,Integer teacherId,String tagsId){
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		
		UserMaster userMaster=null;
		int roleId=0;
		int entityID=0;
		
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			
			if(userMaster.getRoleId().getEntityType()!=null){
				entityID=userMaster.getRoleId().getEntityType();
			}
		}

		List<SecondaryStatusMaster> lstSecondaryStatusMaster = secondaryStatusMasterDAO.findActiveSecStaus();
		Map<Integer, SecondaryStatusMaster>secStatusMap = new HashMap<Integer, SecondaryStatusMaster>();
		 for (SecondaryStatusMaster secStatus : lstSecondaryStatusMaster) {
			 secStatusMap.put(secStatus.getSecStatusId(), secStatus);
		}

		 DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
		 TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
		 
		 Criterion district_cri = Restrictions.eq("districtMaster", districtMaster);
		 Criterion school_cri = Restrictions.isNull("schoolMaster");
		 Criterion job_cri = Restrictions.isNull("jobOrder");
		 Criterion teacher_cri = Restrictions.eq("teacherDetail", teacherDetail);
			
		 List<TeacherSecondaryStatus> lstMltTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByCriteria(district_cri,school_cri,job_cri,teacher_cri);
		 
		 Map<Integer, TeacherSecondaryStatus> tagExist = new HashMap<Integer, TeacherSecondaryStatus>();
		 if(lstMltTeacherSecondaryStatus.size()>0){
			 for (TeacherSecondaryStatus teacherSecondaryStatus : lstMltTeacherSecondaryStatus) {
				 tagExist.put(teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusId(), teacherSecondaryStatus);
			}
		 }
		 
		 String[] tagsIds = tagsId.split(",");
			SessionFactory sessionFactory = teacherSecondaryStatusDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
		    Transaction txOpen =statelesSsession.beginTransaction();
		    
		   String tags="";
		   SecondaryStatusMaster secStatus = null;
		   for (String tagId : tagsIds) {
			  tags+="||"+tagId+"||";
			  
		if(!tagId.equalsIgnoreCase("")){
		  if(tagExist.get(Integer.parseInt(tagId))==null){
			
			  secStatus = secStatusMap.get(Integer.parseInt(tagId));
			  TeacherSecondaryStatus teacherSecondaryStatus = new TeacherSecondaryStatus();
			  teacherSecondaryStatus.setTeacherDetail(teacherDetail);
		 	  teacherSecondaryStatus.setSecondaryStatusMaster(secStatus);
		 	  teacherSecondaryStatus.setCreatedByUser(userMaster);
		 	  teacherSecondaryStatus.setCreatedByEntity(userMaster.getEntityType());
		 	  teacherSecondaryStatus.setDistrictMaster(districtMaster);
		 	  teacherSecondaryStatus.setCreatedDateTime(new Date());
		 	  statelesSsession.insert(teacherSecondaryStatus);
			  }
		   }
		   }
		    
		try{
		if(tagExist.size()!=0){
			for (Entry<Integer, TeacherSecondaryStatus> entry : tagExist.entrySet()){
				TeacherSecondaryStatus  teacherSecondaryStatus=entry.getValue();
				if(tags.contains("||"+teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusId()+"||")){
					System.out.println("No Add ::::::::::::::::::::::::::::::::::::::"+teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusId());
				}else{
					TeacherSecondaryStatus teacherSecondaryStatus2 = new TeacherSecondaryStatus();
					teacherSecondaryStatus2 = tagExist.get(teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusId());
					statelesSsession.delete(teacherSecondaryStatus2);
				}
			}
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		 txOpen.commit();
 	 	 statelesSsession.close();
		return "tagexist";
	}


public String showEpiAndJsi(int teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
{
	int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
	int pgNo 			= 	Integer.parseInt(pageNo);
	int start 			= 	((pgNo-1)*noOfRowInPage);
	int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
	int totalRecord		= 	0;
	String sortOrderFieldName	=	"assessmentType";
	Order  sortOrderStrVal		=	null;		
	if(!sortOrder.equals("") && !sortOrder.equals(null))
	{
		sortOrderFieldName		=	sortOrder;
	}
	
	String sortOrderTypeVal="0";
	if(!sortOrderType.equals("") && !sortOrderType.equals(null))
	{
		if(sortOrderType.equals("0"))
		{
			sortOrderStrVal		=	Order.asc(sortOrderFieldName);
		}
		else
		{
			sortOrderTypeVal	=	"1";
			sortOrderStrVal		=	Order.desc(sortOrderFieldName);
		}
	}
	else
	{
		sortOrderTypeVal		=	"0";
		sortOrderStrVal			=	Order.asc(sortOrderFieldName);
	}
	System.out.println("............."+sortOrderStrVal);
	List<TeacherAssessmentStatus> teacherAssessmentJsiEpi = null;
	List<TeacherEPIChangedStatusLog> teacherEPIChangedStatusLogList = null;
	
	StringBuffer sb = new StringBuffer();	
	String responseText="";
	try
	{		
			TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			teacherAssessmentJsiEpi = teacherAssessmentStatusDAO.jsiByTeachers(teacherDetail,sortOrderStrVal);
			teacherEPIChangedStatusLogList 	= 	teacherEPIChagedStatusLogDAO.epiByTeachers(teacherDetail);
			System.out.println("teacherEPIChangedStatusLogList::"+teacherEPIChangedStatusLogList.size());
			List<TeacherAssessmentStatusDetail> teacherDetailList	=	new ArrayList<TeacherAssessmentStatusDetail>();
			StatusMaster statusMaster = null;
			if(teacherEPIChangedStatusLogList.size()>0 || teacherEPIChangedStatusLogList!=null){
				for(TeacherEPIChangedStatusLog teacherEPIChangedStatusLog : teacherEPIChangedStatusLogList){
					TeacherAssessmentStatusDetail tecaherObj = new TeacherAssessmentStatusDetail();

					tecaherObj.setAssessmentName(teacherEPIChangedStatusLog.getAssessmentName());
					tecaherObj.setAssessmentType(1);
					tecaherObj.setStatusMaster(statusMaster);
					tecaherObj.setCreatedDateTime(teacherEPIChangedStatusLog.getCreatedDateTime());
					teacherDetailList.add(tecaherObj);
				}
			}

			for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentJsiEpi){
				TeacherAssessmentStatusDetail tecaherObj = new TeacherAssessmentStatusDetail();
				tecaherObj.setAssessmentName(teacherAssessmentStatus.getAssessmentDetail().getAssessmentName());
				tecaherObj.setAssessmentType(teacherAssessmentStatus.getAssessmentType());
				tecaherObj.setStatusMaster(teacherAssessmentStatus.getStatusMaster());
				tecaherObj.setCreatedDateTime(teacherAssessmentStatus.getCreatedDateTime());
				tecaherObj.setAssessmentCompletedDateTime(teacherAssessmentStatus.getAssessmentCompletedDateTime());
				teacherDetailList.add(tecaherObj);

			}
			//Collections.sort(teacherDetailList,TeacherAssessmentStatusDetail.createdDate);
			sb.append("<table id='epiAndJsiTable' width='100%'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgNameTheAssessment", locale),sortOrderFieldName,"teacherAssessmentdetail",sortOrderTypeVal,pgNo);
			sb.append("<th valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgAssessmentType", locale),sortOrderFieldName,"assessmentType",sortOrderTypeVal,pgNo);
			sb.append("<th valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgAssessmentStatus", locale),sortOrderFieldName,"statusMaster",sortOrderTypeVal,pgNo);
			sb.append("<th valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgCompletionTimed", locale) , sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			sb.append("<th valign='top'>"+responseText+"</th>");
		
			sb.append("</tr>");
		    sb.append("</thead>");			  
		    if(teacherAssessmentJsiEpi.size()>0)
		    {
		    	for(TeacherAssessmentStatusDetail assessmentStatus : teacherDetailList){
	    			sb.append("<tr>");
	    		    sb.append("<td>");
				    sb.append(assessmentStatus.getAssessmentName());
				    sb.append("</td>");

				    sb.append("<td>");
				    if(assessmentStatus.getAssessmentType()==2) {
				    	sb.append("JSI");
				    } else {
				    	 sb.append("EPI");
				    }
				    sb.append("</td>");

				    sb.append("<td>");
				    if(assessmentStatus.getStatusMaster() == null){
				    			sb.append(Utility.getLocaleValuePropByKey("lblReset", locale));
				    } else {
				    	 if(assessmentStatus.getStatusMaster().getStatusId()==3) {
						    	 sb.append(Utility.getLocaleValuePropByKey("optIncomlete", locale));
						    }
						    if(assessmentStatus.getStatusMaster().getStatusId()==4) {
						        sb.append(Utility.getLocaleValuePropByKey("btnComp", locale));
						    }
						    else if(assessmentStatus.getStatusMaster().getStatusId()==9) {
						    	 sb.append(Utility.getLocaleValuePropByKey("optTOut", locale));
						    }
				    }
				   						    
				    sb.append("</td>");

				    sb.append("<td>");
				    if(assessmentStatus.getAssessmentCompletedDateTime()!=null && !assessmentStatus.getAssessmentCompletedDateTime().equals(""))
				    	sb.append(Utility.convertDateAndTimeFormatForEpiAndJsi(assessmentStatus.getAssessmentCompletedDateTime()));
				    else
				    	sb.append(Utility.convertDateAndTimeFormatForEpiAndJsi(assessmentStatus.getCreatedDateTime()));
				    sb.append("</td>");
				    sb.append("</tr>");						  
	    	}			   
		    }		  
		    sb.append("</table>");		
	}	
	catch(Exception e)
	{e.printStackTrace();}
	
	return sb.toString();		
}

public String teacherAssessmentTime(Integer teacherId){
	System.out.println("========= teacherAssessmentTime ==============="+teacherId);
	
	Calendar cal = Calendar.getInstance();

	cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
	Date sDate=cal.getTime();
	cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
	Date eDate=cal.getTime();
	
	AssessmentDetail assessmentDetail = assessmentDetailDAO.findById(110, false, false);
	TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
	Criterion teacherCriterion 	= Restrictions.eq("teacherDetail", teacherDetail);
	Criterion assessmenType		= Restrictions.eq("assessmentType", 1);
	Criterion currentYear = Restrictions.between("createdDateTime", sDate, eDate);
	
	List<TeacherWiseAssessmentTime> teacherWiseAssessmentTime = teacherWiseAssessmentTimeDAO.findByCriteria(Order.desc("assessmentTimeId"),teacherCriterion,assessmenType);
	
	List<TeacherAssessmentdetail> teacherAssessmentdetails = teacherAssessmentDetailDAO.findByCriteria(teacherCriterion,assessmenType,currentYear);
	
	StringBuffer tmRecords =	new StringBuffer();
	String asssessper = Utility.getValueOfPropByKey("assessmentSessionTime");
	tmRecords.append("<input type='hidden' id='asssessper' value='"+asssessper+"'>");
	tmRecords.append("<input type='hidden' id='defQST' value='"+assessmentDetail.getQuestionSessionTime()+"'>");
	tmRecords.append("<input type='hidden' id='defAST' value='"+assessmentDetail.getAssessmentSessionTime()+"'>");
	tmRecords.append("<input type='hidden' id='teacherIdASS' value='"+teacherId+"'>");
	if(teacherWiseAssessmentTime.size()>0){
		//assessmentDetail
		if(teacherAssessmentdetails.size()>0 && teacherWiseAssessmentTime.get(0).getTeacherDetail().getTeacherId().equals(teacherAssessmentdetails.get(0).getTeacherDetail().getTeacherId())){
			tmRecords.append("<input type='hidden' id='updateBoth' value='1'>");
		}else{
			tmRecords.append("<input type='hidden' id='updateBoth' value='2'>");
		}
		
			tmRecords.append("<div class='span3'>");
			if(teacherWiseAssessmentTime.get(0).getAssessmentFlag().equalsIgnoreCase("U")){
				tmRecords.append("<input type='checkbox' id='accomodation' checked=checked onclick='return accomodation("+teacherWiseAssessmentTime.get(0).getQuestionSessionTime()+","+teacherWiseAssessmentTime.get(0).getAssessmentSessionTime()+");' name='accomodation'> Accomodation");
			}else{
				tmRecords.append("<input type='checkbox' id='accomodation' onclick='return accomodation("+teacherWiseAssessmentTime.get(0).getQuestionSessionTime()+","+teacherWiseAssessmentTime.get(0).getAssessmentSessionTime()+");' name='accomodation'> Accomodation");
			}
			tmRecords.append("</div>");			
			tmRecords.append("<div class='span3'>"+Utility.getLocaleValuePropByKey("msgQST", locale)+" <span  id='quesSessTime'>"+teacherWiseAssessmentTime.get(0).getQuestionSessionTime()+"</span> seconds </div>");
			tmRecords.append("<div class='span3'> "+Utility.getLocaleValuePropByKey("lblAST", locale)+" <span  id='assSessTime'>"+teacherWiseAssessmentTime.get(0).getAssessmentSessionTime()+"</span> seconds </div>");
		
	}else if(teacherAssessmentdetails.size()>0){
			tmRecords.append("<input type='hidden' id='updateBoth' value='3'>");		
			tmRecords.append("<div class='span3'>");
			tmRecords.append("<input type='checkbox' id='accomodation' onclick='return accomodation("+teacherAssessmentdetails.get(0).getQuestionSessionTime()+","+teacherAssessmentdetails.get(0).getAssessmentSessionTime()+");' name='accomodation'> Accomodation");
			tmRecords.append("</div>");
			tmRecords.append("<div class='span3'>"+Utility.getLocaleValuePropByKey("msgQST", locale)+"<span  id='quesSessTime'>"+teacherAssessmentdetails.get(0).getQuestionSessionTime()+"</span> seconds </div>");
			tmRecords.append("<div class='span3'>"+Utility.getLocaleValuePropByKey("lblAST", locale)+" <span  id='assSessTime'>"+teacherAssessmentdetails.get(0).getAssessmentSessionTime()+"</span> seconds </div>");
	}else{
			//assessmentDetail
			tmRecords.append("<input type='hidden' id='updateBoth' value='4'>");
			tmRecords.append("<div class='span3'>");
			tmRecords.append("<input type='checkbox' id='accomodation' onclick='return accomodation("+assessmentDetail.getQuestionSessionTime()+","+assessmentDetail.getAssessmentSessionTime()+");' name='accomodation'> Accomodation");
			tmRecords.append("</div>");
			tmRecords.append("<div class='span3'>"+Utility.getLocaleValuePropByKey("msgQST", locale)+" <span  id='quesSessTime'>"+assessmentDetail.getQuestionSessionTime()+"</span> seconds </div>");
			tmRecords.append("<div class='span3'>"+Utility.getLocaleValuePropByKey("lblAST", locale)+"<span  id='assSessTime'>"+assessmentDetail.getAssessmentSessionTime()+"</span> seconds </div>");
			
		}
	return tmRecords.toString();		
}

public boolean updateTeacherWiseAssessmentTime(Integer teacherId,Integer quesSessTime,Integer assSessTime,Integer updateBoth)
{
	System.out.println("============  updateTeacherWiseAssessmentTime ===================="+teacherId);
	WebContext context; 
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));		
	}
	
	UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
	
	Calendar cal = Calendar.getInstance();
	cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
	Date sDate=cal.getTime();
	cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
	Date eDate=cal.getTime();
	TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
	Criterion teacherCriterion 	= Restrictions.eq("teacherDetail", teacherDetail);
	Criterion assessmenType		= Restrictions.eq("assessmentType", 1);
	Criterion currentYear = Restrictions.between("createdDateTime", sDate, eDate);
	AssessmentDetail assessmentDetail = assessmentDetailDAO.findById(110, false, false);
	
	List<TeacherAssessmentdetail> teacherAssessmentdetails = teacherAssessmentDetailDAO.findByCriteria(teacherCriterion,assessmenType,currentYear);
	
	String defaultFlag="";
	if(assessmentDetail!=null && assessmentDetail.getAssessmentSessionTime().equals(assSessTime)){
			defaultFlag="D";
	}else{
		defaultFlag="U";
	}

	
	
	boolean status=false;
	if(updateBoth.equals(1)){
		
		TeacherWiseAssessmentTime teacherWiseAssessment = new TeacherWiseAssessmentTime();
		teacherWiseAssessment.setTeacherDetail(teacherDetail);
		teacherWiseAssessment.setAssessmentId(assessmentDetail);
		teacherWiseAssessment.setAssessmentType(assessmentDetail.getAssessmentType());
		teacherWiseAssessment.setQuestionSessionTime(quesSessTime);
		teacherWiseAssessment.setAssessmentSessionTime(assSessTime);
		teacherWiseAssessment.setAssessmentFlag(defaultFlag);
		teacherWiseAssessment.setTimeAllowedBy(userMaster);
		teacherWiseAssessment.setIpAddress("");
		teacherWiseAssessmentTimeDAO.makePersistent(teacherWiseAssessment);
		
		TeacherAssessmentdetail teacherAssessment		= teacherAssessmentdetails.get(0);			
		teacherAssessment.setTeacherAssessmentId(teacherAssessment.getTeacherAssessmentId());
		teacherAssessment.setQuestionSessionTime(quesSessTime);
		teacherAssessment.setAssessmentSessionTime(assSessTime);
		teacherAssessment.setUserMaster(userMaster);
		teacherAssessmentDetailDAO.updatePersistent(teacherAssessment);
		status=true;
	}else if(updateBoth.equals(2)){
		
		TeacherWiseAssessmentTime teacherWiseAssessment = new TeacherWiseAssessmentTime();
		teacherWiseAssessment.setTeacherDetail(teacherDetail);
		teacherWiseAssessment.setAssessmentId(assessmentDetail);
		teacherWiseAssessment.setAssessmentType(assessmentDetail.getAssessmentType());
		teacherWiseAssessment.setQuestionSessionTime(quesSessTime);
		teacherWiseAssessment.setAssessmentSessionTime(assSessTime);
		teacherWiseAssessment.setAssessmentFlag(defaultFlag);
		teacherWiseAssessment.setTimeAllowedBy(userMaster);
		teacherWiseAssessment.setIpAddress("");
		teacherWiseAssessmentTimeDAO.makePersistent(teacherWiseAssessment);
		status=true;
	}else if(updateBoth.equals(3)){
		
		TeacherWiseAssessmentTime teacherWiseAssessment = new TeacherWiseAssessmentTime();
		teacherWiseAssessment.setTeacherDetail(teacherDetail);
		teacherWiseAssessment.setAssessmentId(assessmentDetail);
		teacherWiseAssessment.setAssessmentType(assessmentDetail.getAssessmentType());
		teacherWiseAssessment.setQuestionSessionTime(quesSessTime);
		teacherWiseAssessment.setAssessmentSessionTime(assSessTime);
		teacherWiseAssessment.setAssessmentFlag(defaultFlag);
		teacherWiseAssessment.setTimeAllowedBy(userMaster);
		teacherWiseAssessment.setIpAddress("");
		teacherWiseAssessmentTimeDAO.makePersistent(teacherWiseAssessment);
		
		TeacherAssessmentdetail teacherAssessment		= teacherAssessmentdetails.get(0);
		teacherAssessment.setTeacherAssessmentId(teacherAssessment.getTeacherAssessmentId());
		teacherAssessment.setQuestionSessionTime(quesSessTime);
		teacherAssessment.setAssessmentSessionTime(assSessTime);
		teacherAssessment.setUserMaster(userMaster);
		teacherAssessmentDetailDAO.updatePersistent(teacherAssessment);
		status=true;
	}else if(updateBoth.equals(4)){
		
		TeacherWiseAssessmentTime teacherWiseAssessment = new TeacherWiseAssessmentTime();
		teacherWiseAssessment.setTeacherDetail(teacherDetail);
		teacherWiseAssessment.setAssessmentId(assessmentDetail);
		teacherWiseAssessment.setAssessmentType(assessmentDetail.getAssessmentType());
		teacherWiseAssessment.setQuestionSessionTime(quesSessTime);
		teacherWiseAssessment.setAssessmentSessionTime(assSessTime);
		teacherWiseAssessment.setAssessmentFlag(defaultFlag);
		teacherWiseAssessment.setTimeAllowedBy(userMaster);
		teacherWiseAssessment.setIpAddress("");
		teacherWiseAssessmentTimeDAO.makePersistent(teacherWiseAssessment);
		status=true;
	}else{
		status=false;
	}
	return status;
	
}

public String displayTeacherGrid_Op(String firstName, String lastName,String emailAddress,String teacherSSN,String stateForcandidate,String certTypeForCandidate,String degreeId,String universityId,String normScoreSelectVal,String normScoreVal,String CGPASelectVal,String CGPA,String AScoreSelectVal,String AScore,String LRScoreSelectVal,String LRScore,String references,String resume,int [] subjects,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String districtId,String schoolId,String canServeAsSubTeacherYes,String canServeAsSubTeacherNo,String canServeAsSubTeacherDA,String TFAA,String TFAC,String TFAN,String empNmbr)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	
	UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");    //Adding by deepak
	
	StringBuffer tmRecords =	new StringBuffer();	
	int noOfRowInPage = Integer.parseInt(noOfRow);
	int pgNo = Integer.parseInt(pageNo);
	int start = ((pgNo-1)*noOfRowInPage);
	int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
	int totalRecord = 0;
	
	
	String sortOrderFieldName	=	"td.lastName";
	String sortOrderNoField		=	"td.lastName";

	
	DistrictMaster districtMaster=null;
	/*====================================================================================================*/
	
	

	/**Start set dynamic sorting fieldName **/
	Order  sortOrderStrVal=null;

	if(sortOrder!=null){
		if(!sortOrder.equals("") && !sortOrder.equals(null)){
			sortOrderFieldName=sortOrder;
			sortOrderNoField=sortOrder;
		}
	}

	String sortOrderTypeVal="0";
	String sortOrderStr="";
	System.out.println("sortOrderFieldNamesortOrderFieldName    "+sortOrderFieldName);
	
	if(sortOrderFieldName.equals("tLastName")){
		sortOrderFieldName="td.lastName";
	}else if(sortOrderFieldName.equals("tStatus")){
		sortOrderFieldName="td.status";
	}
	if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
		if(sortOrderType.equals("0")){
			sortOrderStrVal=Order.asc(sortOrderFieldName);
			sortOrderStr = "order by "+sortOrderFieldName+" asc";
		}else{
			sortOrderTypeVal="1";
			//sortOrderStrVal=Order.desc(sortOrderFieldName);
			sortOrderStr = "order by "+sortOrderFieldName+" desc";
		}
	}else{
		sortOrderTypeVal="0";
		sortOrderStrVal=Order.asc(sortOrderFieldName);
		sortOrderStr = "order by "+sortOrderFieldName+" asc";
	}
	sortOrderStrVal=Order.asc("td.lastName");
	
	//String noOfRow, String pageNo
	SortedMap<Integer,Integer> jftMapForTm1 = new TreeMap<Integer,Integer>();
	
	List<Integer> searchTId=new ArrayList<Integer>();
	List<Integer> filterTeacherList = new ArrayList<Integer>();
	List<Integer> NbyATeacherList = new ArrayList<Integer>();
	boolean teacherFlag=false;
	// Start /////////////////////////////// candidate list By certificate ///////////////////////////////////////
	StateMaster stateMasterForCandidate=null;
	List<Integer> certTypeMasterListForCndt =new ArrayList<Integer>();
	List<Integer> tListByCertificateTypeId = new ArrayList<Integer>();
	boolean tdataByCertTypeIdFlag = false;
	
	if(!certTypeForCandidate.equals("0") && !certTypeForCandidate.equals("")||!stateForcandidate.equals("0") && !stateForcandidate.equals("")){
		if(!stateForcandidate.equals("0")){
		try{	stateMasterForCandidate=stateMasterDAO.loadById((Long.valueOf(stateForcandidate)));}catch(Exception e) {e.printStackTrace();}
		}
		certTypeMasterListForCndt=certificateTypeMasterDAO.findCertificationByJob_Op(stateMasterForCandidate,certTypeForCandidate);
		tListByCertificateTypeId = teacherCertificateDAO.findCertificateByJobForTeacherByCertIds_Op(certTypeMasterListForCndt);
		tdataByCertTypeIdFlag=true;
	}
	if(tdataByCertTypeIdFlag && tListByCertificateTypeId.size()>0){
		if(teacherFlag){
			filterTeacherList.retainAll(tListByCertificateTypeId);
		}else{
			filterTeacherList.addAll(tListByCertificateTypeId);
		}
	}
	if(tdataByCertTypeIdFlag){
		teacherFlag=true;
	}
	// End /////////////////////////////// candidate list By certificate ///////////////////////////////////////
	
	
	//////////////////////////////////////// Degree /////////////////////////////////
	List<Integer> degreeTeacherDetailList = new ArrayList<Integer>();  // change Anurag
	boolean degreeIdFlag=false;
	if(degreeId!=null && !degreeId.equals("") && !degreeId.equals("0")){
		degreeIdFlag=true;
		//DegreeMaster degreeMaster=degreeMasterDAO.findById(Long.valueOf(degreeId), false,false);
		DegreeMaster degreeMaster=new DegreeMaster();
		degreeMaster.setDegreeId(Long.valueOf(degreeId));
		degreeTeacherDetailList=teacherAcademicsDAO.findTeacherListByDegreeId_Op(degreeMaster);
	}
	if(degreeId.equals("")){
		degreeIdFlag=true;
	}
	if(degreeIdFlag && degreeTeacherDetailList.size()>0){
		if(teacherFlag){
			filterTeacherList.retainAll(degreeTeacherDetailList);
		}else{
			filterTeacherList.addAll(degreeTeacherDetailList);
		}
	}
	if(degreeIdFlag){
		teacherFlag=true;
	}
	////////////////////////////////////////////////////////////////////////////////////
	
	
	
	/////////////////////////////University//////////////////////
	List<Integer> universityTeacherList = new ArrayList<Integer>();  // change Anurag
	boolean universityFlag=false;
	if(universityId!=null && !universityId.equals("") && !universityId.equals("0")){
		universityFlag=true;
		//UniversityMaster universityMaster=universityMasterDAO.findById(Integer.valueOf(universityId), false,false);
		UniversityMaster universityMaster=new UniversityMaster();
		universityMaster.setUniversityId(Integer.valueOf(universityId));
		universityTeacherList=teacherAcademicsDAO.findTeacherListByUniversityId_Op(universityMaster);
	}
	if(universityId.equals("")){
		universityFlag=true;
	}
	if(universityFlag && universityTeacherList.size()>0){
		if(teacherFlag){
			filterTeacherList.retainAll(universityTeacherList);
		}else{
			filterTeacherList.addAll(universityTeacherList);
		}
	}
	if(universityFlag){
		teacherFlag=true;
	}
	/////////////////////////////////////////////////////////////
	
	
	/////////////// Norm Score //////////////////////////////////
	boolean nByAflag=false;

	List<Integer> normTeacherList = new ArrayList<Integer>();
	boolean normFlag=false;
	boolean normScoreFlag = false;
	
	if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") & !normScoreSelectVal.equals("6")){
		normFlag=true;
		
		normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore_Op(normScoreVal,normScoreSelectVal);
		
	}
	if(normFlag && normTeacherList.size()>0){
		if(teacherFlag){
			filterTeacherList.retainAll(normTeacherList);
		}else{
			filterTeacherList.addAll(normTeacherList);
		}
	}
	if(normFlag){
		teacherFlag=true;
	}
	

	/// N/A changes
	boolean normNa=false;
	if(!normScoreSelectVal.equals("") && normScoreSelectVal.equals("6")){
		normNa=true;
		normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore_Op("0","5");
		
		if(normNa && normTeacherList.size()>0){
			if(nByAflag){
				NbyATeacherList.retainAll(normTeacherList);
			}else{
				NbyATeacherList.addAll(normTeacherList);
			}
		}
	}
	if(normNa){
		nByAflag=true;
	}
	
	/*if(nByAflag)
	{
		finalTeacherList.removeAll(NbyATeacherList);
	}*/
	/////////////////////////////////////////////////////////////
	
	////////// CGPA ////////////////////////////////////////////
	List<Integer> cgpaTeacherList = new ArrayList<Integer>();
	boolean cgpaFlag=false;
	if(!CGPASelectVal.equals("") && !CGPASelectVal.equals("0") & !CGPASelectVal.equals("6")){
		cgpaFlag=true;
		cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA_Op(CGPA,CGPASelectVal);
	}
	if(cgpaFlag && cgpaTeacherList.size()>0){
		if(teacherFlag){
			filterTeacherList.retainAll(cgpaTeacherList);
		}else{
			filterTeacherList.addAll(cgpaTeacherList);
		}
	}
	if(cgpaFlag){
		teacherFlag=true;
	}
	
	/// N/A changes
	boolean CGPANa=false;
	if(!CGPASelectVal.equals("") && CGPASelectVal.equals("6")){
		CGPANa=true;
		cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA_Op("0","5");
	}
	if(CGPANa){
		if(nByAflag){
			NbyATeacherList.retainAll(cgpaTeacherList);
		}else{
			NbyATeacherList.addAll(cgpaTeacherList);
		}
	}
	
	if(CGPANa){
		nByAflag=true;
	}
	if(normFlag){
		teacherFlag=true;
	}
	
	///////////////////////////////////////////////////////////
	
	////////// Ascore
	List<Integer> acheivTeacherList = new ArrayList<Integer>();
	boolean acheivFlag=false;
	if(!AScoreSelectVal.equals("") && !AScoreSelectVal.equals("0") & !AScoreSelectVal.equals("6")){
		acheivFlag=true;
		acheivTeacherList=teacherAchievementScoreDAO.findTeachersByAscoreOrLScore_Op(AScore,AScoreSelectVal,districtMaster,true);
	}
	if(acheivFlag && acheivTeacherList.size()>0){
		if(teacherFlag){
			filterTeacherList.retainAll(acheivTeacherList);
		}else{
			filterTeacherList.addAll(acheivTeacherList);
		}
	}
	if(acheivFlag){
		teacherFlag=true;
	}
	
	/// N/A changes
	boolean acheivNa=false;
	if(!CGPASelectVal.equals("") && CGPASelectVal.equals("6")){
		acheivNa=true;
		acheivTeacherList=teacherAchievementScoreDAO.findTeachersByAscoreOrLScore_Op("0","5",districtMaster,true);
	}
	
	if(acheivNa){
		if(nByAflag){
			NbyATeacherList.retainAll(acheivTeacherList);
		}else{
			NbyATeacherList.addAll(acheivTeacherList);
		}
	}
	if(acheivNa){
		nByAflag=true;
	}
	
	///////////////////////////////////////////////////////////
	
	/////////// L/R SCore
	List<Integer> LRScoreTeacherList = new ArrayList<Integer>();
	boolean lRScoreFlag=false;
	if(!LRScoreSelectVal.equals("") && !LRScoreSelectVal.equals("0") & !LRScoreSelectVal.equals("6") && districtMaster!=null){
		lRScoreFlag=true;
		LRScoreTeacherList=teacherAchievementScoreDAO.findTeachersByAscoreOrLScore_Op(LRScore,LRScoreSelectVal,districtMaster,false);
	}
	if(lRScoreFlag && LRScoreTeacherList.size()>0){
		if(teacherFlag){
			filterTeacherList.retainAll(LRScoreTeacherList);
		}else{
			filterTeacherList.addAll(LRScoreTeacherList);
		}
	}
	if(lRScoreFlag){
		teacherFlag=true;
	}
	
	// N/A changes
	boolean LRScoreNa=false;
	if(!CGPASelectVal.equals("") && CGPASelectVal.equals("6")){
		LRScoreNa=true;
		LRScoreTeacherList=teacherAchievementScoreDAO.findTeachersByAscoreOrLScore_Op("0","5",districtMaster,false);
		
		if(LRScoreTeacherList.size()>0){
			if(nByAflag){
				NbyATeacherList.retainAll(LRScoreTeacherList);
			}else{
				NbyATeacherList.addAll(LRScoreTeacherList);
			}
		}
	}
	if(LRScoreNa)
		nByAflag=true;
	
	
	/*if(nByAflag)
	{
		finalTeacherList.removeAll(NbyATeacherList);
	}*/
	///////////////////////////////////////////////////////////
	
	
	
	////// Reference
	List<Integer> referencesTeacherList = new ArrayList<Integer>();
	boolean referFlag=false;
	if(references!=null && !references.equals("") && !references.equals("0")){
		referFlag=true;
		referencesTeacherList=teacherElectronicReferencesDAO.findTeacherReferences_Op();
	}
	
	if(referFlag && referencesTeacherList.size()>0){
		if(teacherFlag){
			{
				filterTeacherList.retainAll(referencesTeacherList);
			}
		}else{
			if(references.equals("1"))
				filterTeacherList.addAll(referencesTeacherList);
			else
			{
				filterTeacherList.retainAll(referencesTeacherList);
			}
		}
	}
	if(referFlag){
		teacherFlag=true;
	}
	////////////////////////////////////////////////////////////
	
	//// Resume
//////////////////////////////////Resume //////////////////////////////////////////
	List<Integer> nolstTeacherDetailAll= new ArrayList<Integer>();   // change Anurag
	List<Integer> resumeTeacherList = new ArrayList<Integer>();
	boolean resumeFlag=false;
	if(resume!=null && !resume.equals("") && !resume.equals("0")){
		resumeFlag=true;
		resumeTeacherList=teacherExperienceDAO.findTeachersByResume_Op(true);
	}
	if(resumeFlag && resumeTeacherList.size()>0){
		
		if(teacherFlag && resume.equals("1"))
		{
			filterTeacherList.retainAll(resumeTeacherList);
			teacherFlag=true;
		}
		else if(teacherFlag== false && resume.equals("1")){
			{
				filterTeacherList.addAll(resumeTeacherList);
				teacherFlag=true;
			}
		}else if(resume.equals("2"))
		{
			nolstTeacherDetailAll.addAll(resumeTeacherList);
			teacherFlag=false;
		}
		
	}
	
	///////////////////////////////////////////////////////////
	
	////////Willing to Substitute
	List<Integer> substitudeTeacherList = new ArrayList<Integer>();
	boolean substituteFlag=false;
	if(!canServeAsSubTeacherYes.equals("false") || !canServeAsSubTeacherNo.equals("false") || !canServeAsSubTeacherDA.equals("false")){
		substituteFlag=true;
		substitudeTeacherList=teacherExperienceDAO.findTeachersBySubstitute_Op(canServeAsSubTeacherYes,canServeAsSubTeacherNo,canServeAsSubTeacherDA);
	}
	if(substituteFlag && substitudeTeacherList.size()>0){
		if(teacherFlag){
			filterTeacherList.retainAll(substitudeTeacherList);
		}else{
			filterTeacherList.addAll(substitudeTeacherList);
		}
	}
	if(substituteFlag){
		teacherFlag=true;
	}
	////////////////////////////////////////////////////////
	
	///////////TFA
	List<Integer> tfaTeacherList = new ArrayList<Integer>();
	boolean tfaFlag=false;
	if(!TFAA.equals("false") || !TFAC.equals("false") || !TFAN.equals("false")){
		tfaFlag=true;
		tfaTeacherList=teacherExperienceDAO.findTeachersByTFA_Op(TFAA,TFAC,TFAN);
	}
	if(tfaFlag && tfaTeacherList.size()>0){
		if(teacherFlag){
			filterTeacherList.retainAll(tfaTeacherList);
		}else{
			filterTeacherList.addAll(tfaTeacherList);
		}
	}
	if(tfaFlag){
		teacherFlag=true;
	}
	////////////////////////////////////////////////////////
	
	TestTool.getTraceSecond("1 rec  ");
	StringBuilder tIds = new StringBuilder();

	   String sep = ", ";
	   //String sep = ", ";
	   int k=0;
	   for (Integer each : filterTeacherList) {
		   if(k==0){
			   tIds.append(each);
		   }else{
			   tIds.append(sep).append(each);
		   }
		   k++;
	   }
	       TestTool.getTraceSecond("2 rec");       
	
	
	try {
		JSONArray[]  gridJsonRecord = teacherDetailDAO.getProspectGrid(sortOrderStr,start,end,noOfRow,firstName,lastName,emailAddress,teacherSSN,empNmbr,tIds.toString());
	
		List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
		for (int i = 0; i < gridJsonRecord[0].size(); i++) {
			JSONObject jsonrec = new JSONObject();
			jsonrec = gridJsonRecord[0].getJSONObject(i);
			TeacherDetail tDetail = new TeacherDetail();
			tDetail.setTeacherId(Integer.parseInt(jsonrec.getString("teacherId").toString()));
			teacherDetailList.add(tDetail);
		}

		tmRecords.append("<table id='teacherTable' width='100%' class='tablecgtbl'>");
		tmRecords.append("<thead class='bg'>");
		tmRecords.append("<tr>");
		tmRecords.append("<tr>");

		String responseText="";
		responseText=PaginationAndSorting.responseSortingLink("Name",sortOrderFieldName,"tLastName",sortOrderTypeVal,pgNo);
		tmRecords.append("<th  valign='top'>"+responseText+"</th>");
		String schoolDistrict="District";
		responseText=PaginationAndSorting.responseSortingLink("Norm Score",sortOrderFieldName,"normScore",sortOrderTypeVal,pgNo);
		tmRecords.append("<th   valign='top'>"+responseText+"</th>");		
		tmRecords.append("<th width='60' valign='top'>TFA</th>");
		tmRecords.append("<th width='50' valign='top'>"+Utility.getLocaleValuePropByKey("lblOfJobs", locale)+"&nbsp;<a data-original-title='X/Y, X = Number of jobs applied at your "+schoolDistrict+" / Y = Number of Jobs applied at other "+schoolDistrict+"(s)' class='net-header-text ' rel='tooltip' id='jobApplied' href='javascript:void(0);' ><span class='icon-question-sign '></span></a></th>");		
		tmRecords.append("<th width='60' valign='top'>"+Utility.getLocaleValuePropByKey("lblOfYearsTeaching", locale)+"</th>");
		tmRecords.append("<th width='60' valign='top'>"+Utility.getLocaleValuePropByKey("ExptSalary", locale)+"</th>");
		responseText=PaginationAndSorting.responseSortingLink("Status",sortOrderFieldName,"tStatus",sortOrderTypeVal,pgNo);
		tmRecords.append("<th width='76' valign='top'>"+responseText+"</th>");
		tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblEngage", locale)+"</th>");
		tmRecords.append("</tr>");
		tmRecords.append("</thead>");
		int counter=0;
		int noOfRecordCheck=0;
		String teacherIdHidden ="";
		//************************* Adding by deepak **************************************
		Map<Integer,String>  portfolioIdMap=new HashMap<Integer, String>();
		Boolean portfolioFlag=selfServiceCandidateProfileService.checkPortfolioAvaliablity(teacherDetailList,userMaster,districtMaster,portfolioIdMap);
		//***************************************************************
		for (int i = 0; i < gridJsonRecord[0].size(); i++) {
			
			JSONObject jsonrec = new JSONObject();
			jsonrec = gridJsonRecord[0].getJSONObject(i);
			noOfRecordCheck++;
			counter++;
			String gridColor="class='bggrid'";
			if(counter%2==0){
			gridColor="style='background-color:white;'";
			}
			int jobListForTm=0;
			
			jobListForTm = Integer.parseInt(jsonrec.getString("jobApply").toString());
			tmRecords.append("<tr "+gridColor+">");
			tmRecords.append("<TD width='240'>");
			//************************** Adding by deepak ***********************************
			Integer teacherId=0;
			System.out.println("Teacher Id ::::::: "+jsonrec.getString("teacherId").toString());
			if(jsonrec.getString("teacherId").toString()!=null)
				teacherId=Utility.getIntValue(jsonrec.getString("teacherId").toString());
			//***********************************************************************************************
			//String sServerDomainName=request.getServerName();
			boolean bSelfService=false;
			if(portfolioFlag && portfolioIdMap.get(teacherId)!=null)
			    {
			     bSelfService=true;
			    }
				if(bSelfService){
				String fullName=jsonrec.getString("teacherName").toString();
				fullName=fullName.replaceAll("'", "&#36;");
				tmRecords.append("<a class='profile' href='javascript:void(0);' onclick=\"showProfileContentNew(this,0,"+teacherId+",'0','0','',"+noOfRecordCheck+",event,'"+fullName+"','"+portfolioIdMap.get(teacherId)+"','pool')\"   data-original-title=' "+fullName+" ' rel='tooltip' id='teacherFnameAndLname"+noOfRecordCheck+"'  >"+fullName+"</a>");
			}else{
				tmRecords.append("<a class='profile' href='javascript:void(0);' onclick=\"showProfileContentForTeacher(this,"+jsonrec.getString("teacherId")+","+noOfRecordCheck+",'Teacher Pool',event)\">"+jsonrec.getString("teacherName")+"</a>");
			}
			//tmRecords.append("<a class='profile' href='javascript:void(0);' onclick=\"showProfileContentForTeacher(this,"+jsonrec.getString("teacherId")+","+noOfRecordCheck+",'Teacher Pool',event)\">"+jsonrec.getString("teacherName")+"</a>");
			
			tmRecords.append("<br>"+jsonrec.get("emailAddress"));
			tmRecords.append("</td>");
			
			int normScore = 0;			
			if(!jsonrec.get("normScore").equals("N/A")){
				normScore=Integer.parseInt(jsonrec.get("normScore").toString());
			}
			String colorName = "";
			String ccsName="";
			String normScoreLen=normScore+"";
			if(normScoreLen.length()==1){
				ccsName="nobground1";
			}else if(normScoreLen.length()==2){
				ccsName="nobground2";
			}else{
				ccsName="nobground3";
			}
			
			if(jsonrec.get("decileColor")!=null){
				colorName=jsonrec.get("decileColor").toString();
			}
			if(normScore!=0){
				tmRecords.append("<td><span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>"+normScore+"</span></td>");
			}else{
				tmRecords.append("<td>N/A</td>");
			}
			
			tmRecords.append("<td>"+jsonrec.get("tfa")+"</td>");
			if(jobListForTm!=0){
				tmRecords.append("<td style='font-size:12px;'><a href='javascript:void(0);' onclick=\"getJobList('"+jsonrec.getString("teacherId").toString()+"')\" >"+jobListForTm+"</a>/0</td>");
			}else{
				tmRecords.append("<td style='font-size:12px;'>"+"0"+"/0</td>");
			}
			
			tmRecords.append("<td>"+jsonrec.get("teaExp")+"</td>");
			if(!jsonrec.get("teaExpSal").equals("N/A")){
				tmRecords.append("<td>$"+jsonrec.get("teaExpSal")+"</td>");
			}else{
				tmRecords.append("<td>"+jsonrec.get("teaExpSal")+"</td>");
			}
			
			tmRecords.append("<td>");
			
			if(jsonrec.get("status").toString().equalsIgnoreCase("Active"))
				tmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateTeacher("+jsonrec.getString("teacherId")+",'I')\">Deactivate");
			else
				tmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateTeacher("+jsonrec.getString("teacherId")+",'A')\">Activate");
			tmRecords.append("</td>");
			tmRecords.append("<td  style='vertical-align: top;padding:0px;'>");
			tmRecords.append("<table border=0 >");
			tmRecords.append("<tr "+gridColor+">");

			tmRecords.append("<td  style='padding: 0px; padding-left:3px; margin: 0px; border: 0px;padding-top:5px;'>");
		    
			tmRecords.append("</td>");
		
	
			String colorgray	=	"7F7F7F";	
			String colorblue	=	"007AB4";
						
			Integer isBase = null;

			if(jsonrec.getString("isBase")!=null && !jsonrec.getString("isBase").equals("")){
				isBase=Integer.parseInt(jsonrec.getString("isBase"));
			}
			

			try {
				String IconColor="";
				if(jsonrec.get("timeIcon").equals(1)){
					IconColor=colorblue;
				}else{
					IconColor=colorgray;
				}
				tmRecords.append("<td  style='padding: 0px; padding-left:0px; margin: 0px; border: 0px;padding-top:5px;'>");
				tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblEPIAccomodation", locale)+"' rel='tooltip' id='epiACC"+noOfRecordCheck+"'  href='javascript:void(0);' onclick=\"openEpiTime('"+jsonrec.getString("teacherId")+"','"+jsonrec.getString("teacherName")+"','"+isBase+"')\" ><span class='icon-time icon-large' id='timeIcon"+jsonrec.getString("teacherId")+"' style='color: #"+IconColor+";'></span></a>");
				tmRecords.append("</td>");
			} catch (Exception e) {}
			try
			{
				tmRecords.append("<td  style='padding: 0px; padding-left:0px; margin: 0px; border: 0px;padding-top:5px;'>");
		        
				if(Integer.parseInt(jsonrec.getString("jsiCount"))>0 || Integer.parseInt(jsonrec.getString("epiCount"))>0)
		        {
		        	tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblAssessmentDetails", locale)+"' rel='tooltip' id='epiAndJsi"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"showEpiAndJsi("+jsonrec.getString("teacherId")+")\"><span class='fa-file-text icon-large' style='color: #"+colorblue+";'></span></a>");			        	
		    	}
		        else
		        {
		        	tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblNoAssessment", locale)+"' rel='tooltip' id='epiAndJsi"+noOfRecordCheck+"' href='javascript:void(0);'><span class='fa-file-text icon-large' style='color: #"+colorgray+";'></span></a>");
		        }
		        tmRecords.append("</td>");	
			}
			catch(Exception e)
			{e.printStackTrace();}
			
			tmRecords.append("<td style='padding: 0px; padding-left:0px; margin: 0px;padding-top:5px;'>");
			try
			{
				if(isBase==2)
					tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblResetEPI", locale)+"' rel='tooltip' id='tpReset"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"checkBaseViolatedForEPIReset('"+jsonrec.getString("teacherId")+"')\"><span class='icon-refresh  icon-large iconcolor'></span></a>");
				else if(isBase==1)
					tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblEPICompleted", locale)+"' rel='tooltip' id='tpReset"+noOfRecordCheck+"' ><span class='icon-refresh  icon-large iconcolorhover'></span></a>");
				else
					tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("toolNoEPI", locale)+"' rel='tooltip' id='tpReset"+noOfRecordCheck+"'><span class='icon-refresh  icon-large iconcolorhover'></span></a>");
			}
			catch(NullPointerException e)
			{
				tmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("toolNoEPI", locale)+"' rel='tooltip' id='tpReset"+noOfRecordCheck+"'><span class='icon-refresh  icon-large iconcolorhover'></span></a>");
			}
		
			tmRecords.append("</td>");
			tmRecords.append("<td width=5>&nbsp;");
			tmRecords.append("</td>");
			tmRecords.append("</tr>");
			tmRecords.append("</table>");
			tmRecords.append("</td>");
			tmRecords.append("</tr>");
			
			}
		
			/*tmRecords.append("</td>");
			tmRecords.append("</tr>");*/
		
		tmRecords.append("</table>");
		//tmRecords.append("<div id='teacherIdsHiddenDiv' class='hide'>"+teacherIdHidden+"</div>");
		
		JSONObject jsonCount = new JSONObject();
		jsonCount = gridJsonRecord[1].getJSONObject(0);
		totalRecord = jsonCount.getInt("totalCount");
		
		
		if((gridJsonRecord[0].size() >= Integer.valueOf(noOfRow)) || totalRecord > Integer.valueOf(noOfRow))				
		tmRecords.append(PaginationAndSorting.getPaginationStringForTeacherInfoAjax(request,totalRecord,noOfRow, pageNo));
		
	} catch (Exception e) {
e.printStackTrace();
	}
	
	
	return tmRecords.toString();
}
}