package tm.services.social;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.swing.ImageIcon;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.FacebookConfig;
import tm.bean.master.LinkedInConfig;
import tm.bean.master.SchoolMaster;
import tm.bean.master.TwitterConfig;
import tm.bean.user.UserMaster;
import tm.controller.hqbranches.HqBranchController;
import tm.controller.teacher.BasicController;
import tm.dao.master.FacebookConfigDAO;
import tm.dao.master.LinkedInConfigDAO;
import tm.dao.master.TwitterConfigDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

/* @Author: Vishwanath Kumar
 * @Discription: 
 */
public class SocialService {

	String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	TwitterConfigDAO twitterConfigDAO;
	public void setTwitterConfigDAO(TwitterConfigDAO twitterConfigDAO) {
		this.twitterConfigDAO = twitterConfigDAO;
	}

	@Autowired
	FacebookConfigDAO facebookConfigDAO;
	public void setFacebookConfigDAO(FacebookConfigDAO facebookConfigDAO) {
		this.facebookConfigDAO = facebookConfigDAO;
	}

	@Autowired
	LinkedInConfigDAO linkedInConfigDAO;
	public void setLinkedInConfigDAO(LinkedInConfigDAO linkedInConfigDAO) {
		this.linkedInConfigDAO = linkedInConfigDAO;
	}

	@Autowired
	private UserMasterDAO userMasterDAO;

	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;

	@Autowired
	private BasicController basicController;
	
	@Autowired
	private HqBranchController hqBranchController;


	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String tweet(String message,UserMaster userMaster,JobOrder jobOrder,String baseUrl)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		
		Integer entityType = userMaster.getEntityType();
		DistrictMaster districtMaster = userMaster.getDistrictId();
		SchoolMaster schoolMaster = userMaster.getSchoolId();

		List<TwitterConfig> twitterConfigs = null;
		TwitterConfig twitterConfig = null;
		String jobTitle = Utility.LimitCharacter(jobOrder.getJobTitle(), 80, "...");
		
		String stateName = districtMaster.getStateId().getStateName().replace(" ", "");
		String logofileName=districtMaster.getLogoPath();		
		String imagePath=Utility.getBaseURL(request)+"district/"+districtMaster.getDistrictId()+"/"+districtMaster.getLogoPath();
		
		//String checkStateImage = Utility.getValueOfPropByKey("rootPath")+"States/"+stateName+".jpg";
		String checkStateImage = context.getServletContext().getRealPath("/")+"States/"+stateName+".jpg";
		File stateImageFile = new File(checkStateImage);
		
		//String checkDistrictImage = Utility.getValueOfPropByKey("rootPath")+"district/"+districtMaster.getDistrictId()+"/"+districtMaster.getLogoPath();
		String checkDistrictImage = context.getServletContext().getRealPath("/")+"district/"+districtMaster.getDistrictId()+"/"+districtMaster.getLogoPath();
		File districtImageFile = new File(checkDistrictImage);
		
		String defaultImg = context.getServletContext().getRealPath("/")+"States/Logo_Teacher.png";
		int width=0;
		int height=0;
		
		try {
			ImageIcon imageIcon = new ImageIcon(checkDistrictImage);
			height = imageIcon.getIconHeight();
			width = imageIcon.getIconWidth();
			
			/*BufferedImage bimg = ImageIO.read(new File(checkDistrictImage));
			width          = bimg.getWidth();
			height         = bimg.getHeight();*/
			
			System.out.println("width*height="+width+"x"+height);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		if(logofileName!=null && logofileName!="" && districtImageFile.exists() && width>=300 && height>=180)
			baseUrl = checkDistrictImage;
		else if(stateImageFile.exists())			
			baseUrl = checkStateImage;
		else
			baseUrl = defaultImg;
		
		System.out.println("Image Paths "+baseUrl);
		
		if(userMaster!=null)
		{
			Criterion criterion = Restrictions.eq("entityType",entityType);
			Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2 = Restrictions.eq("schoolMaster", schoolMaster);

			if(entityType==1)
			{
				twitterConfigs = twitterConfigDAO.findByCriteria(criterion);
				message = "New Job! "+jobTitle+" #education "+message;
			}
			else if(entityType==2)
			{
				twitterConfigs = twitterConfigDAO.findByCriteria(criterion,criterion1);
				message = "New Job! "+jobTitle+" #education "+message;
			}
			else if(entityType==3)
			{
				twitterConfigs = twitterConfigDAO.findByCriteria(criterion,criterion2);
				message = "New Job! "+jobTitle+" #education "+message;
			}

			if(twitterConfigs.size()>0)
				twitterConfig = twitterConfigs.get(0);

		}

		if(message!=null && twitterConfig!=null)
		{
			UpdateStatus us = new UpdateStatus();
			us.tweet(message, "", twitterConfig, baseUrl);
		}
		
		return null;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String postOnWall(String message,UserMaster userMaster,JobOrder jobOrder,String baseUrl)
	{
		System.out.println("Post on wall");
		System.out.println("message message"+message);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		
		Integer entityType = userMaster.getEntityType();
		DistrictMaster districtMaster = userMaster.getDistrictId();
		SchoolMaster schoolMaster = userMaster.getSchoolId();

		List<FacebookConfig> facebookConfigs = null;
		FacebookConfig facebookConfig = null;
	
		Integer jobId;	
		String tinnyUrl ="";
		if(jobOrder!=null)
		{			
			jobId=jobOrder.getJobId();	
			String jobUrl=Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobId+"";
			try {
				 tinnyUrl =	Utility.getShortURL(jobUrl);
				 System.out.println("base" +tinnyUrl);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}		
		
		String cityName = districtMaster.getCityName();
		String stateName = districtMaster.getStateId().getStateName().replace(" ", "");
		String logofileName=districtMaster.getLogoPath();		
		String imagePath=Utility.getBaseURL(request)+"district/"+districtMaster.getDistrictId()+"/"+districtMaster.getLogoPath();
		
		//String checkStateImage = Utility.getValueOfPropByKey("rootPath")+"States/"+stateName+".jpg";
		String checkStateImage = context.getServletContext().getRealPath("/")+"States/"+stateName+".jpg";
		File stateImageFile = new File(checkStateImage);
		
		//String checkDistrictImage = Utility.getValueOfPropByKey("rootPath")+"district/"+districtMaster.getDistrictId()+"/"+districtMaster.getLogoPath();
		String checkDistrictImage = context.getServletContext().getRealPath("/")+"district/"+districtMaster.getDistrictId()+"/"+districtMaster.getLogoPath();
		File districtImageFile = new File(checkDistrictImage);
		int width=0;
		int height=0;
		
		try {
			ImageIcon imageIcon = new ImageIcon(checkDistrictImage);
			height = imageIcon.getIconHeight();
			width = imageIcon.getIconWidth();
			System.out.println("width*height="+width+"x"+height);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		if(logofileName!=null && logofileName!="" && districtImageFile.exists() && width>=200 && height>=200)
			baseUrl = imagePath;
		else if(stateImageFile.exists())			
			baseUrl = Utility.getBaseURL(request)+"States/"+stateName+".jpg";
		else
			baseUrl = Utility.getBaseURL(request)+"States/Logo_Teacher.png";
		
		System.out.println("Image Paths "+baseUrl);
		String jobTitle = "";
		if(jobOrder!=null)
			jobTitle = jobOrder.getJobTitle();

		try {
			if(userMaster!=null)
			{
				Criterion criterion = Restrictions.eq("entityType",entityType);
				Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
				Criterion criterion2 = Restrictions.eq("schoolMaster", schoolMaster);

				if(entityType==1)
				{
					facebookConfigs = facebookConfigDAO.findByCriteria(criterion);
					if(schoolMaster!=null)
						message = "Job opportunity in "+stateName+"!\\n"+jobTitle+"\\n"+schoolMaster.getSchoolName()+"\\n"+cityName+", "+stateName+"\\n"+message;
					else
						message = "Job opportunity in "+stateName+"!\\n"+jobTitle+"\\n"+districtMaster.getDistrictName()+"\\n"+cityName+", "+stateName+"\\n"+message;
				}
				else if(entityType==2)
				{
					facebookConfigs = facebookConfigDAO.findByCriteria(criterion,criterion1);
					message = "New Job!\\n"+jobTitle+"\\n"+districtMaster.getDistrictName()+"\\n"+cityName+", "+stateName+"\\n"+message;
				}
				else if(entityType==3)
				{
					facebookConfigs = facebookConfigDAO.findByCriteria(criterion,criterion2);
					message = "New Job!\\n"+jobTitle+"\\n"+schoolMaster.getSchoolName()+"\\n"+cityName+", "+stateName+"\\n"+message;
				}

				if(facebookConfigs.size()>0)
					facebookConfig = facebookConfigs.get(0);
			}

			if(message!=null && facebookConfig!=null)
			{
				System.out.println("facebookConfig.getAccessToken()" +facebookConfig.getAccessToken());
				Boolean found = baseUrl.contains("localhost");
				Boolean found1 = baseUrl.contains(":8080");
				if(found!=true && found1!=true)
				Facebook.postOnReceiverWall(Utility.decodeBase64(facebookConfig.getAccessToken()), facebookConfig.getFacebookId(), "", message,baseUrl,tinnyUrl);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String shareOnLinkedIn(String message,UserMaster userMaster,JobOrder jobOrder,String baseUrl)
	{

		Integer entityType = userMaster.getEntityType();
		DistrictMaster districtMaster = userMaster.getDistrictId();
		SchoolMaster schoolMaster = userMaster.getSchoolId();

		List<LinkedInConfig> linkedInConfigs = null;
		LinkedInConfig linkedInConfig = null;

		String cityName = districtMaster.getCityName();
		String stateName = districtMaster.getStateId().getStateName();
		String jobTitle = "";
		if(jobOrder!=null)
			jobTitle = jobOrder.getJobTitle();
		String comment = "";
		String url = message;
		try {
			if(userMaster!=null)
			{
				Criterion criterion = Restrictions.eq("entityType",entityType);
				Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
				Criterion criterion2 = Restrictions.eq("schoolMaster", schoolMaster);

				if(entityType==1)
				{
					linkedInConfigs = linkedInConfigDAO.findByCriteria(criterion);
					if(schoolMaster!=null)
						message = schoolMaster.getSchoolName()+", "+cityName+", "+stateName;
					else
						message = districtMaster.getDistrictName()+", "+cityName+", "+stateName;

					comment="Job opportunity in "+stateName;
				}
				else if(entityType==2)
				{
					linkedInConfigs = linkedInConfigDAO.findByCriteria(criterion,criterion1);
					message = districtMaster.getDistrictName()+", "+cityName+", "+stateName;
					comment="We are hiring!";
				}
				else if(entityType==3)
				{
					linkedInConfigs = linkedInConfigDAO.findByCriteria(criterion,criterion2);
					message = schoolMaster.getSchoolName()+", "+cityName+", "+stateName;
					comment="We are hiring!";
				}

				if(linkedInConfigs.size()>0)
					linkedInConfig = linkedInConfigs.get(0);
			}

			if(message!=null && linkedInConfig!=null)
			{
				//sb.append(0x000A); // for line feed
				//sb.append(0x000D); //for carrage return
				String req="<share><comment>"+comment+"</comment>"+
				" <content>"+
				"  <title>"+jobTitle+"</title>"+
				" <description>"+message+"</description>"+
				" <submitted-url>"+url+"</submitted-url>"+
				//" <submitted-url>http://demo.teachermatch.org</submitted-url>"+
				"<submitted-image-url>http://demo.teachermatch.org/images/tmlogo.png</submitted-image-url>"+ 
				"</content>"+
				"<visibility>"+ 
				"<code>anyone</code>"+ 
				"</visibility>"+
				"</share>";

				String fff = Facebook.shareOnLinkedIn(Utility.decodeBase64(linkedInConfig.getAccess_token()),req);
				System.out.println("gg"+fff);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}


	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String getTwitterConfig()
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}


		String consumerKey = "";
		String consumerSecret = "";
		String accessToken = "";
		String accessTokenSecret = "";
		Integer twitterConfigId = 0;
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");

		Integer entityType = userMaster.getEntityType();
		DistrictMaster districtMaster = userMaster.getDistrictId();
		SchoolMaster schoolMaster = userMaster.getSchoolId();

		List<TwitterConfig> twitterConfigs = null;
		TwitterConfig twitterConfig = null;

		try {
			if(userMaster!=null)
			{
				Criterion criterion = Restrictions.eq("entityType",entityType);
				Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
				Criterion criterion2 = Restrictions.eq("schoolMaster", schoolMaster);

				if(entityType==1)
					twitterConfigs = twitterConfigDAO.findByCriteria(criterion);
				else if(entityType==2)
					twitterConfigs = twitterConfigDAO.findByCriteria(criterion,criterion1);
				else if(entityType==3)
					twitterConfigs = twitterConfigDAO.findByCriteria(criterion,criterion2);

				if(twitterConfigs.size()>0)
				{
					twitterConfig = twitterConfigs.get(0);
					twitterConfigId = twitterConfig.getTwitterConfigId();
				}
				else
					twitterConfig = new TwitterConfig();

				consumerKey = Utility.decodeBase64(twitterConfig.getConsumerKey());
				consumerSecret = Utility.decodeBase64(twitterConfig.getConsumerSecret());
				accessToken = Utility.decodeBase64(twitterConfig.getAccessToken());
				accessTokenSecret = Utility.decodeBase64(twitterConfig.getAccessTokenSecret());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//System.out.println("{ \"consumerKey\": \""+consumerKey+"\", \"consumerSecret\": \""+consumerSecret+"\",\"accessToken\": \""+accessToken+"\",\"accessTokenSecret\": \""+accessTokenSecret+"\" }");
		return "{\"twitterConfigId\":\""+twitterConfigId+"\", \"consumerKey\": \""+consumerKey+"\", \"consumerSecret\": \""+consumerSecret+"\",\"accessToken\": \""+accessToken+"\",\"accessTokenSecret\": \""+accessTokenSecret+"\" }";
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String saveTwiterConfig(Integer twitterConfigId,String consumerKey,String consumerSecret,String accessToken,String accessTokenSecret)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String ipAddress = IPAddressUtility.getIpAddress(request);

		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");

		Integer entityType = userMaster.getEntityType();
		DistrictMaster districtMaster = userMaster.getDistrictId();
		SchoolMaster schoolMaster = userMaster.getSchoolId();

		List<TwitterConfig> twitterConfigs = null;
		TwitterConfig twitterConfig = null;

		try {
			if(userMaster!=null)
			{
				Criterion criterion = Restrictions.eq("entityType",entityType);
				Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
				Criterion criterion2 = Restrictions.eq("schoolMaster", schoolMaster);

				if(entityType==1)
					twitterConfigs = twitterConfigDAO.findByCriteria(criterion);
				else if(entityType==2)
					twitterConfigs = twitterConfigDAO.findByCriteria(criterion,criterion1);
				else if(entityType==3)
					twitterConfigs = twitterConfigDAO.findByCriteria(criterion,criterion2);

				if(twitterConfigs.size()>0)
					twitterConfig = twitterConfigs.get(0);
				else
					twitterConfig = new TwitterConfig();
			}

			twitterConfig.setConsumerKey(Utility.encodeInBase64(consumerKey));
			twitterConfig.setConsumerSecret(Utility.encodeInBase64(consumerSecret));
			twitterConfig.setAccessToken(Utility.encodeInBase64(accessToken));
			twitterConfig.setAccessTokenSecret(Utility.encodeInBase64(accessTokenSecret));
			twitterConfig.setIpAddress(ipAddress);

			if(twitterConfigId==0)	
				twitterConfig.setCreatedDateTime(new Date());

			twitterConfig.setStatus("A");
			twitterConfig.setUserMaster(userMaster);
			twitterConfig.setEntityType(entityType);
			twitterConfig.setDistrictMaster(districtMaster);
			twitterConfig.setSchoolMaster(schoolMaster);


			twitterConfigDAO.makePersistent(twitterConfig);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return ""+entityType;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String saveFacebookConfig(String facebookId,String accessToken)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String ipAddress = IPAddressUtility.getIpAddress(request);

		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");

		Integer entityType = userMaster.getEntityType();
		DistrictMaster districtMaster = userMaster.getDistrictId();
		SchoolMaster schoolMaster = userMaster.getSchoolId();

		List<FacebookConfig> facebookConfigs = null;
		FacebookConfig facebookConfig = null;

		try {
			if(userMaster!=null)
			{
				Criterion criterion = Restrictions.eq("entityType",entityType);
				Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
				Criterion criterion2 = Restrictions.eq("schoolMaster", schoolMaster);

				if(entityType==1)
					facebookConfigs = facebookConfigDAO.findByCriteria(criterion);
				else if(entityType==2)
					facebookConfigs = facebookConfigDAO.findByCriteria(criterion,criterion1);
				else if(entityType==3)
					facebookConfigs = facebookConfigDAO.findByCriteria(criterion,criterion2);

				if(facebookConfigs.size()>0)
					facebookConfig = facebookConfigs.get(0);
				else
					facebookConfig = new FacebookConfig();
			}

			facebookConfig.setFacebookId(facebookId);
			//facebookConfig.setAccessToken(Utility.encodeInBase64(accessToken));
			facebookConfig.setIpAddress(ipAddress);

			if(facebookConfig.getFacebookId()==null)
				facebookConfig.setCreatedDateTime(new Date());

			facebookConfig.setStatus("A");
			facebookConfig.setUserMaster(userMaster);
			facebookConfig.setEntityType(entityType);
			facebookConfig.setDistrictMaster(districtMaster);
			facebookConfig.setSchoolMaster(schoolMaster);

			System.out.println("accessToken: "+accessToken);
			accessToken=Facebook.renewAccess_token(accessToken,Utility.getValueOfPropByKey("appId"),Utility.getValueOfPropByKey("appSecret"));
			facebookConfig.setAccessToken(Utility.encodeInBase64(accessToken));
			facebookConfigDAO.makePersistent(facebookConfig);
			String value = Facebook.getFacebookPages(accessToken);
			//postOnWall("Hi test message I am hiring",userMaster);
			return value;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		//return ""+entityType;
		return ""+accessToken;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String savePageId(String pageId)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String ipAddress = IPAddressUtility.getIpAddress(request);

		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");

		Integer entityType = userMaster.getEntityType();
		DistrictMaster districtMaster = userMaster.getDistrictId();
		SchoolMaster schoolMaster = userMaster.getSchoolId();

		List<FacebookConfig> facebookConfigs = null;
		FacebookConfig facebookConfig = null;

		try {
			if(userMaster!=null)
			{
				Criterion criterion = Restrictions.eq("entityType",entityType);
				Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
				Criterion criterion2 = Restrictions.eq("schoolMaster", schoolMaster);

				if(entityType==1)
					facebookConfigs = facebookConfigDAO.findByCriteria(criterion);
				else if(entityType==2)
					facebookConfigs = facebookConfigDAO.findByCriteria(criterion,criterion1);
				else if(entityType==3)
					facebookConfigs = facebookConfigDAO.findByCriteria(criterion,criterion2);

				if(facebookConfigs.size()>0)
					facebookConfig = facebookConfigs.get(0);
				else
					facebookConfig = new FacebookConfig();
			}

			String accessToken = pageId.split("@@@")[1];
			pageId = pageId.split("@@@")[0];

			System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkk pageId: "+pageId);
			facebookConfig.setFacebookId(pageId);
			facebookConfig.setAccessToken(Utility.encodeInBase64(accessToken));
			facebookConfig.setIpAddress(ipAddress);

			if(facebookConfig.getFacebookId()==null)
				facebookConfig.setCreatedDateTime(new Date());

			facebookConfig.setStatus("A");
			facebookConfig.setUserMaster(userMaster);
			facebookConfig.setEntityType(entityType);
			facebookConfig.setDistrictMaster(districtMaster);
			facebookConfig.setSchoolMaster(schoolMaster);

			facebookConfigDAO.makePersistent(facebookConfig);
			return "1";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		//return ""+entityType;
		return "0";
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String saveLinkedInConfig(Integer linkedConfigId,String access_token,String linkedInId,HttpServletRequest request)
	{

		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String ipAddress = IPAddressUtility.getIpAddress(request);

		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");

		Integer entityType = userMaster.getEntityType();
		DistrictMaster districtMaster = userMaster.getDistrictId();
		SchoolMaster schoolMaster = userMaster.getSchoolId();

		List<LinkedInConfig> linkedInConfigs = null;
		LinkedInConfig linkedInConfig = null;

		try {
			if(userMaster!=null)
			{
				Criterion criterion = Restrictions.eq("entityType",entityType);
				Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
				Criterion criterion2 = Restrictions.eq("schoolMaster", schoolMaster);

				if(entityType==1)
					linkedInConfigs = linkedInConfigDAO.findByCriteria(criterion);
				else if(entityType==2)
					linkedInConfigs = linkedInConfigDAO.findByCriteria(criterion,criterion1);
				else if(entityType==3)
					linkedInConfigs = linkedInConfigDAO.findByCriteria(criterion,criterion2);

				if(linkedInConfigs.size()>0)
					linkedInConfig = linkedInConfigs.get(0);
				else
					linkedInConfig = new LinkedInConfig();
			}

			linkedInConfig.setLinkedInId(linkedInId);
			linkedInConfig.setAccess_token(Utility.encodeInBase64(access_token));
			linkedInConfig.setIpAddress(ipAddress);

			if(linkedConfigId==0)	
			{
				linkedInConfig.setCreatedDateTime(new Date());
				linkedInConfig.setStatus("A");
			}

			linkedInConfig.setUserMaster(userMaster);
			linkedInConfig.setEntityType(entityType);
			linkedInConfig.setDistrictMaster(districtMaster);
			linkedInConfig.setSchoolMaster(schoolMaster);


			linkedInConfigDAO.makePersistent(linkedInConfig);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return ""+entityType;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public List<String> checkSocialConfigDetails(HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");

		List<String> socialList = new ArrayList<String>();
		Integer entityType = userMaster.getEntityType();
		
		DistrictMaster districtMaster =null;
		if(userMaster.getDistrictId()!=null)
		districtMaster = userMaster.getDistrictId();
		
		SchoolMaster schoolMaster = null;
		if(userMaster.getSchoolId()!=null)
		schoolMaster = userMaster.getSchoolId();

		List<TwitterConfig> twitterConfigs = null;
		List<FacebookConfig> facebookConfigs = null;
		List<LinkedInConfig> linkedInConfigs = null;

		if(userMaster!=null)
		{
			Criterion criterion = Restrictions.eq("entityType",entityType);
			
			Criterion criterion1 = null;
			if(districtMaster!=null)
			criterion1 = Restrictions.eq("districtMaster", districtMaster);
			
			Criterion criterion2 = null;
			if(schoolMaster!=null)
			criterion2 = Restrictions.eq("schoolMaster", schoolMaster);

			if(entityType==1 || entityType==5 || entityType==6)
				facebookConfigs = facebookConfigDAO.findByCriteria(criterion);
			else if(entityType==2)
				facebookConfigs = facebookConfigDAO.findByCriteria(criterion,criterion1);
			else if(entityType==3)
				facebookConfigs = facebookConfigDAO.findByCriteria(criterion,criterion2);

			if(facebookConfigs.size()>0)
				socialList.add("facebook");

			if(entityType==1 || entityType==5 || entityType==6)
				twitterConfigs = twitterConfigDAO.findByCriteria(criterion);
			else if(entityType==2)
				twitterConfigs = twitterConfigDAO.findByCriteria(criterion,criterion1);
			else if(entityType==3)
				twitterConfigs = twitterConfigDAO.findByCriteria(criterion,criterion2);

			if(twitterConfigs.size()>0)
				socialList.add("twitter");

			if(entityType==1 || entityType==5 || entityType==6)
				linkedInConfigs = linkedInConfigDAO.findByCriteria(criterion);
			else if(entityType==2)
				linkedInConfigs = linkedInConfigDAO.findByCriteria(criterion,criterion1);
			else if(entityType==3)
				linkedInConfigs = linkedInConfigDAO.findByCriteria(criterion,criterion2);

			if(linkedInConfigs.size()>0)
				socialList.add("linkedIn");
		}


		return socialList;
	}


	/**
	 * @author Amit
	 * @description it is used for change the location (Single Sign On For Philadelphia)
	 * @date 11-Mar-15
	 */
	@Transactional(readOnly=false)
	public String displayRecordsByDistrictAndEmail(String districtId, String emailAddress)
	{
		System.out.println("\n"+districtId+" :: districtId @@@@@@@@@@  AJAX  @@@@@@@@@@@@@ emailAddress :: "+emailAddress+"\n");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		StringBuffer tmRecords = new StringBuffer();
		try
		{
			boolean flagfordata=true;
			if(districtId.trim()==null && districtId.trim().equals("") && districtId.trim().equals("0"))
				flagfordata=false;
			if(emailAddress.trim()==null && emailAddress.trim().equals(""))
				flagfordata=false;
			UserMaster userMaster = null;
			Integer entityID = null;
			DistrictMaster districtMaster = null;
			HeadQuarterMaster headQuarterMaster = null;
			BranchMaster branchMaster = null;
			Integer currentUserId = null;
			if (session==null || session.getAttribute("userMaster")==null)
				return "false";
			else
			{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getUserId()!=null && userMaster.getUserId()!=0)
					currentUserId=userMaster.getUserId();
				if(userMaster.getHeadQuarterMaster()!=null)
					headQuarterMaster=userMaster.getHeadQuarterMaster();
				if(userMaster.getBranchMaster()!=null)
					branchMaster=userMaster.getBranchMaster();
				if(userMaster.getDistrictId()!=null)
					districtMaster=userMaster.getDistrictId();
				entityID=userMaster.getEntityType();
			}
			List<UserMaster> userMasterList = new ArrayList<UserMaster>();
			if(flagfordata)
				if(entityID!=1){
					if(districtMaster!=null)
						userMasterList = userMasterDAO.findByByDistrictAndEmail(districtMaster, emailAddress);
					else
						userMasterList = userMasterDAO.findByHQBRAndEmail(headQuarterMaster,branchMaster, emailAddress,entityID);
				}
			tmRecords.append("<table  id='tblGridList' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			tmRecords.append("<th valign='top'></th>");
			tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblNm", locale)+"</th>");
			tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblLocti", locale)+"</th>");
			tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblRole", locale)+"</th>");
			tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgTeacherController11", locale)+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			if(userMasterList.size()==0)
				tmRecords.append("<tr><td colspan='5' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			int checkBoxId=0;
			if(userMasterList.size()>0)
			{
				for(UserMaster um : userMasterList)
				{
					++checkBoxId;
					String location="";
					String role="";
					String employeeCode="";
					tmRecords.append("<tr>");
					if(um.getUserId().equals(currentUserId))
						tmRecords.append("<td style='margin-top: 6px;'><input type=\"radio\" name='cl' id='cl"+checkBoxId+"' value="+um.getUserId()+" checked><input type='hidden' id='"+um.getUserId()+"' value='CL'></td>");
					else
						tmRecords.append("<td style='margin-top: 6px;'><input type=\"radio\" name='cl' id='cl"+checkBoxId+"' value="+um.getUserId()+"><input type='hidden' id='"+um.getUserId()+"' value='CL'></td>");
					tmRecords.append("<td>"+um.getFirstName()+" "+um.getLastName()+"</td>");
					if(um.getSchoolId()!=null && !um.getSchoolId().equals("") && !um.getSchoolId().equals("0"))
					{
						location = um.getSchoolId().getSchoolName();
						if(um.getSchoolId().getLocationCode()!=null && !um.getSchoolId().getLocationCode().equals("") && !um.getSchoolId().getLocationCode().equals("0"))
						{
							location+=" ("+um.getSchoolId().getLocationCode()+")";
						}
						tmRecords.append("<td>"+location+"</td>");
					}
					else if(um.getEntityType()==6)
					{
						location = um.getBranchMaster().getBranchName();
						if(um.getBranchMaster().getBranchCode()!=null && !um.getBranchMaster().getBranchCode().equals("") && !um.getBranchMaster().getBranchCode().equals("0"))
						{
							location+=" ("+um.getBranchMaster().getBranchCode()+")";
						}
						tmRecords.append("<td>"+location+"</td>");
					}
					else
						tmRecords.append("<td>N/A</td>");
					if(um.getRoleId()!=null && !um.getRoleId().equals("") && !um.getRoleId().equals("0"))
					{
						role = um.getRoleId().getRoleName();
						tmRecords.append("<td>"+role+"</td>");
					}
					else
						tmRecords.append("<td>N/A</td>");
					if(um.getEmployeeMaster()!=null && !um.getEmployeeMaster().equals("") && !um.getEmployeeMaster().equals("0"))
					{
						employeeCode = um.getEmployeeMaster().getEmployeeCode();
						tmRecords.append("<td>"+employeeCode+"</td>");
					}
					else
						tmRecords.append("<td>N/A</td>");

					tmRecords.append("</tr>");
				}
			}
			tmRecords.append("</tbody>");
			tmRecords.append("</table>");
			if(userMasterList.size()>1)
			{
				tmRecords.append("<br><br><button class='btn btn-primary' type='button' onclick='changeLocation();'>&nbsp;Switch Location&nbsp;&nbsp;<i class='icon'></i></button>");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return tmRecords.toString();		
	}

	/**
	 * @author Amit
	 * @description it is used for change the location (Single Sign On For Philadelphia)
	 * @date 11-Mar-15
	 */
	@Transactional(readOnly=false)
	public String changeLocation(Integer userId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		StringBuffer tmRecords = new StringBuffer();
		try
		{
			boolean flagfordata=true;

			UserMaster userMaster = null;
			Integer entityID = null;
			DistrictMaster districtMaster = null;
			Integer currentUserId = null;
			if (session==null || session.getAttribute("userMaster")==null)
				return "false";
			else
			{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getUserId()!=null && userMaster.getUserId()!=0)
					currentUserId=userMaster.getUserId();
				if(userMaster.getDistrictId()!=null)
					districtMaster=userMaster.getDistrictId();
				entityID=userMaster.getEntityType();

				String email = userMaster.getEmailAddress();

				userMaster = userMasterDAO.findById(userId, false, false); 
				if(userMaster!=null)
				{
					if(email.equalsIgnoreCase(userMaster.getEmailAddress()))
					{
						session.setAttribute("userMaster", userMaster);
						session.setAttribute("lstMenuMaster", roleAccessPermissionDAO.getMenuList(request,userMaster));

						if(userMaster.getEntityType()==2)//District
						{
							tmRecords.append("true");
							int responseFolder =	 basicController.createHomeFolder(request);
							if(userMaster.getEntityType()!=1)
								basicController.createDistrictStatusHome(request);
						}
						else if(userMaster.getEntityType()==3)//School
						{
							tmRecords.append("true");
							int responseFolder =	 basicController.createHomeFolder(request);
							if(userMaster.getEntityType()!=1)
								basicController.createDistrictStatusHome(request);
						}
						else if(userMaster.getEntityType()==6)//Branch
						{
							tmRecords.append("true#Branch");
							int responseFolder =	 basicController.createHomeFolder(request);
							if(userMaster.getEntityType()!=1)
								hqBranchController.createDistrictStatusHeadQuarterHome(request,userMaster.getHeadQuarterMaster(),userMaster.getBranchMaster());
							System.out.println(" createDistrictStatusHeadQuarterHome :: called ");
						}

					}else
						tmRecords.append("false");
				}else
					tmRecords.append("false");

			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return tmRecords.toString();		
	}
}
