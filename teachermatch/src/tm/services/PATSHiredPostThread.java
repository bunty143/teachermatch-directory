package tm.services;

import java.util.Map;

import tm.bean.JobForTeacher;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.master.RaceMaster;
import tm.utility.TMCommonUtil;


public class PATSHiredPostThread extends Thread{

	private JobForTeacher jobForTeacher;
	private TeacherPersonalInfo teacherPersonalInfo;
	private Map<String,RaceMaster> raceMap;
	private String postingNo;
	
	public void setJobForTeacher(JobForTeacher jobForTeacher) {
		this.jobForTeacher = jobForTeacher;
	}
	public void setTeacherPersonalInfo(TeacherPersonalInfo teacherPersonalInfo) {
		this.teacherPersonalInfo = teacherPersonalInfo;
	}
	public void setRaceMap(Map<String, RaceMaster> raceMap) {
		this.raceMap = raceMap;
	}
	public String getPostingNo() {
		return postingNo;
	}
	public void setPostingNo(String postingNo) {
		this.postingNo = postingNo;
	}
	
	public PATSHiredPostThread() {
		super();
	}
	
	public void run()
	{
		try{
			System.out.println("==============PATSHiredPostThread===================");
			/*TeacherDetail teacherDetail = new TeacherDetail();
			teacherDetail.setTeacherId(jobForTeacher.getTeacherId().getTeacherId());
			if(jobForTeacher.getDistrictId().equals(806900))
			TMCommonUtil.startBackgroundCheck(teacherDetail, teacherPersonalInfo, raceMap, jobForTeacher.getJobId().getDistrictMaster());
			else*/
			TMCommonUtil.updateSDSCHiredCandidate(jobForTeacher, teacherPersonalInfo, raceMap, postingNo);
			System.out.println("==============PATSHiredPostThread===================");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
