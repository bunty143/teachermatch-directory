package tm.services;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.UserEmailNotifications;
import tm.bean.user.UserMaster;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.UserEmailNotificationsDAO;
import tm.utility.Utility;

public class UserEmailNotificationsAjax {

	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private UserEmailNotificationsDAO userEmailNotificationsDAO;
	public void setUserEmailNotificationsDAO(
			UserEmailNotificationsDAO userEmailNotificationsDAO) {
		this.userEmailNotificationsDAO = userEmailNotificationsDAO;
	}
	
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	
	public String saveOrUpdateNotification(String[] statusShortNamesMaster,String[] demoNamesMaster,String checked_chkcstatus,String checked_chkdemonoti){

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityID=0,roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}
		
		try {
			
		//System.out.println("checked_chkcstatus : "+checked_chkcstatus+"checked_chkdemonoti : "+checked_chkdemonoti);
		
		
		Criterion criterion_user = Restrictions.eq("userMaster",userMaster);
		Criterion criterion_notification_cs = Restrictions.eq("notificationTabName","cs");
		List<UserEmailNotifications> lstuserEmailNotifications_cs =userEmailNotificationsDAO.findByCriteria(criterion_user,criterion_notification_cs);
		
		Criterion criterion_notification_dn = Restrictions.eq("notificationTabName","dn");
		List<UserEmailNotifications> lstuserEmailNotifications_dn =userEmailNotificationsDAO.findByCriteria(criterion_user,criterion_notification_dn);
	
		List<UserEmailNotifications> lstuserEmailNotifications =userEmailNotificationsDAO.findByCriteria(criterion_user);
		
		if(checked_chkcstatus!=null && checked_chkcstatus.length()>1)
			checked_chkcstatus=checked_chkcstatus.substring(0, checked_chkcstatus.length()-1);
		String[] chkcstatus_arr=checked_chkcstatus.split(",");
		
		Map<String, Boolean> candidatemap = new HashMap<String, Boolean>();
		Map<String, Boolean> demomap = new HashMap<String, Boolean>();
		
		// Set default value false for all
		if(statusShortNamesMaster.length>0)
			for(int i=0;i<statusShortNamesMaster.length;i++)
				candidatemap.put(statusShortNamesMaster[i], new Boolean(false));
		
		// If selected then set default value true
		if(chkcstatus_arr.length>0)
			for(int i=0;i<chkcstatus_arr.length;i++)
				if(candidatemap.get(chkcstatus_arr[i])!=null)
					candidatemap.put(chkcstatus_arr[i],  new Boolean(true));
		
		
		if(checked_chkdemonoti!=null && checked_chkdemonoti.length()>1)
			checked_chkdemonoti=checked_chkdemonoti.substring(0, checked_chkdemonoti.length()-1);
		
		String[] chkdemonoti_arr=checked_chkdemonoti.split(",");
		
		// Set default value false for all
		if(demoNamesMaster.length>0)
			for(int i=0;i<demoNamesMaster.length;i++)
				demomap.put(demoNamesMaster[i], new Boolean(false));
		
		// If selected then set default value true
		if(chkdemonoti_arr.length>0)
			for(int i=0;i<chkdemonoti_arr.length;i++)
				if(demomap.get(chkdemonoti_arr[i])!=null)
					demomap.put(chkdemonoti_arr[i],  new Boolean(true));
		
		UserEmailNotifications emailNotifications=null;
		
		
		if(lstuserEmailNotifications.size()>0 && statusShortNamesMaster.length==lstuserEmailNotifications_cs.size())
		{
			Boolean bNotificationFlag=false;
			for(UserEmailNotifications pojo:lstuserEmailNotifications_cs)
			{
				bNotificationFlag=false;
				if(candidatemap.get(pojo.getNotificationShortName()).equals(new Boolean(true)))
					bNotificationFlag=true;
				pojo.setNotificationFlag(bNotificationFlag);
				userEmailNotificationsDAO.makePersistent(pojo);
			}
				
			
			for(UserEmailNotifications pojo:lstuserEmailNotifications_dn)
			{
				bNotificationFlag=false;
				if(demomap.get(pojo.getNotificationShortName()).equals(new Boolean(true)))
					bNotificationFlag=true;
				pojo.setNotificationFlag(bNotificationFlag);
				userEmailNotificationsDAO.makePersistent(pojo);
			}
			
		}
		else
		{
			
			if(lstuserEmailNotifications_cs.size()>0)
			{
				for(UserEmailNotifications pojo:lstuserEmailNotifications_cs)
				{
					userEmailNotificationsDAO.makeTransient(pojo);
				}
			}
			if(lstuserEmailNotifications_dn.size()>0)
			{
				for(UserEmailNotifications pojo:lstuserEmailNotifications_dn)
				{
					userEmailNotificationsDAO.makeTransient(pojo);
				}
			}
			
			Iterator iter = candidatemap.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry mEntry = (Map.Entry) iter.next();
				
				Boolean bNotificationFlag=false;
				if(mEntry.getValue().equals(new Boolean(true)))
					bNotificationFlag=new Boolean(true);
				
				emailNotifications=new UserEmailNotifications();
				emailNotifications.setUserMaster(userMaster);
				emailNotifications.setDistrictMaster(districtMaster);
				if(schoolMaster!=null)
					emailNotifications.setSchoolMaster(schoolMaster);
				emailNotifications.setNotificationTabName("cs");
				emailNotifications.setNotificationShortName(mEntry.getKey().toString());
				emailNotifications.setNotificationFlag(bNotificationFlag);
				emailNotifications.setCreatedDateTime(new Date());
				userEmailNotificationsDAO.makePersistent(emailNotifications);
				
			}
			
			// Part 02 :: Demo 
			
			Iterator iter_demo = demomap.entrySet().iterator();
			while (iter_demo.hasNext()) {
				Map.Entry mEntry = (Map.Entry) iter_demo.next();
				
				Boolean bNotificationFlag=false;
				if(mEntry.getValue().equals(new Boolean(true)))
					bNotificationFlag=new Boolean(true);
				
				emailNotifications=new UserEmailNotifications();
				emailNotifications.setUserMaster(userMaster);
				emailNotifications.setDistrictMaster(districtMaster);
				if(schoolMaster!=null)
					emailNotifications.setSchoolMaster(schoolMaster);
				emailNotifications.setNotificationTabName("dn");
				emailNotifications.setNotificationShortName(mEntry.getKey().toString());
				emailNotifications.setNotificationFlag(bNotificationFlag);
				emailNotifications.setCreatedDateTime(new Date());
				userEmailNotificationsDAO.makePersistent(emailNotifications);
				
			}
		}
		
		} catch (Exception e) {
			e.printStackTrace();
			return "2";
		}
		return "1";
	}
	
	
	
public String displayNotificationData(String[] statusShortNames)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityID=0,roleId=0;
		StringBuffer sb=new StringBuffer();
		try {
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}
		
		//System.out.println(" [ Method : displayNotificationData  ]"+statusShortNames);
		
		Criterion criterion_user = Restrictions.eq("userMaster",userMaster);
		List<UserEmailNotifications> lstuserEmailNotifications =userEmailNotificationsDAO.findByCriteria(criterion_user);
		
		Map<String, Boolean> dispmap = new HashMap<String, Boolean>();
		if(lstuserEmailNotifications.size()>0)
				for(UserEmailNotifications pojo:lstuserEmailNotifications)
					dispmap.put(pojo.getNotificationShortName(),pojo.getNotificationFlag());
		
		// Set default value false for all
		
		Criterion criterion = Restrictions.in("statusShortName",statusShortNames);
		List<StatusMaster> listStatusMasters=statusMasterDAO.findByCriteria(Order.asc("status"), criterion);
		
		//System.out.println(" Gagan : listStatusMasters : "+listStatusMasters.size());
		String isChecked="";
		
		sb.append("<form:form id='notificationform' name='notificationform' method='post'  onsubmit='return  validatenotification();'>");
		
		sb.append("<div class='row mt10'>"+    
	      "<div class='span4'>"+
		       "<p>"+Utility.getLocaleValuePropByKey("msgCandidateStatusChange", locale)+"</p>"+
	       "</div>"+
		"</div>");
		
		
		sb.append("<div class='row'>");
		if(listStatusMasters.size()>0)
		{
			for(StatusMaster pojo:listStatusMasters){
				sb.append("<div class='span10 paddingLeft20'>");
				sb.append("<label class='checkbox inline'>");
				isChecked="";
				if(lstuserEmailNotifications.size()>0){
					if(dispmap.get(pojo.getStatusShortName())!=null)
					{
						if(dispmap.get(pojo.getStatusShortName()).equals(new Boolean(true)))
							isChecked="checked";	
					}
				}
				else
					if(pojo.getStatusShortName().equalsIgnoreCase("hird"))	
						isChecked="checked";
				
				if(pojo.getStatusShortName().equalsIgnoreCase("apl"))
					pojo.setStatus(Utility.getLocaleValuePropByKey("toollJobApply", locale));
				
				sb.append("<input type='checkbox' name='chkcstatus' id='chkcstatus' "+isChecked+" value='"+pojo.getStatusShortName()+"'>&nbsp;"+pojo.getStatus());
				sb.append("</label>");
				sb.append("</div>");
			}
			
		}
		
		isChecked="";
		if(lstuserEmailNotifications.size()>0)
			if(dispmap.get("soth").equals(new Boolean(true)))
				isChecked="checked";
		
		sb.append("<div class='span10 paddingLeft20'>");
		sb.append("<label class='checkbox inline'>");
		sb.append("<input type='checkbox' name='chkcstatus' id='chkcstatus' "+isChecked+" value='soth'>&nbsp;"+Utility.getLocaleValuePropByKey("msgOtherStatusChanges", locale));
		sb.append("</label>");
		sb.append("</div>");
		
		sb.append("</div>");
		
		//*********************************** Demo 
		
		
		
		sb.append("<div class='row mt15'>"+    
	      "<div class='span4'>"+
		       "<p>"+Utility.getLocaleValuePropByKey("msgDemoNotifications", locale)+"</p>"+
	       "</div></div>");
  
		sb.append("<div class='row'>");
		
		isChecked="";
		if(lstuserEmailNotifications.size()>0)
			if(dispmap.get("sch").equals(new Boolean(true)))
				isChecked="checked";
		
		sb.append("<div class='span10 paddingLeft20'>"+
			"<label class='checkbox inline'>"+
		     "<input type='checkbox' name='chkdemonoti' id='chkdemonoti' "+isChecked+" value='sch'>&nbsp;"+Utility.getLocaleValuePropByKey("msgScheduled3", locale)+
		     "</label>"+	
		"</div>");
		
		isChecked="";
		if(lstuserEmailNotifications.size()>0)
			if(dispmap.get("can").equals(new Boolean(true)))
				isChecked="checked";
		
		sb.append("<div class='span10 paddingLeft20'>"+
			"<label class='checkbox inline'>"+
		     "<input type='checkbox' name='chkdemonoti' id='chkdemonoti' "+isChecked+" value='can'>&nbsp;"+Utility.getLocaleValuePropByKey("msgCancelled3", locale)+
		     "</label>"+	
		"</div>");
		
		isChecked="";
		if(lstuserEmailNotifications.size()>0)
			if(dispmap.get("doth").equals(new Boolean(true)))
				isChecked="checked";
		
		sb.append("<div class='span10 paddingLeft20' >"+
			"<label class='checkbox inline'>"+
		     "<input type='checkbox' name='chkdemonoti' id='chkdemonoti' "+isChecked+" value='doth'>&nbsp;"+Utility.getLocaleValuePropByKey("msgAttendeesAddedRemoved", locale)+
		     "</label>"+	
		"</div>");
		
		sb.append("</div>");
		
		sb.append("<div class='row mt10'>"+
		"<div class='span4'>"+
		       	"<span class=''>"+
		       		"<button class='btn btn-primary' type='button' onclick='return validatenotification();'>"+Utility.getLocaleValuePropByKey("btnSave", locale)+"&nbsp;<i class='icon' style='margin-top:5px!important;'></i></button>"+
		       	"</span>"+
	       "</div></div>");    
		
		sb.append("</form:form>");
			
			} catch (Exception e) {
				e.printStackTrace();
		}
	return sb.toString();	
	}
	
}
