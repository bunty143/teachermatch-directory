package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.api.PaginationAndSortingAPI;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherProfileVisitHistory;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherProfileVisitHistoryDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.services.quartz.FTPConnectAndLogin;
import tm.utility.DistrictNameCompratorASC;
import tm.utility.DistrictNameCompratorDESC;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
public class NobleStCandidateReportAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	

	public void setTeacherPersonalInfoDAO(
			TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}

	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	
	public String displayRecordsByEntityType(String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	 {
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=1;
			try{
				
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				//System.out.println("schoolMaster="+schoolMaster+"districtMaster="+districtMaster+"entityID="+entityID);
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position


				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
			 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"intenalteacherid";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<String[]> lstNobleCandidate =new ArrayList<String[]>();
				
				if(entityID==2||entityID==3)
				{
					lstNobleCandidate =jobForTeacherDAO.noblestcandidate(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
				  	totalRecord = lstNobleCandidate.size();
				   	System.out.println("totallllllllllll========="+totalRecord);
			    	 
				}
				else if(entityID==1) 
				{
					System.out.println("indside admin candidate ");
					
					lstNobleCandidate =jobForTeacherDAO.noblestcandidate(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
				  	totalRecord = lstNobleCandidate.size();
				   	System.out.println("totallllllllllll========="+totalRecord);
					
				
				}
				
				List<String[]> finallstNobleCandidate =new ArrayList<String[]>();
			     if(totalRecord<end)
						end=totalRecord;
			     finallstNobleCandidate=lstNobleCandidate.subList(start,end);
							
				String responseText="";
				
				tmRecords.append("<table  id='tblGridNobleSt' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				
				
				responseText=PaginationAndSorting.responseSortingLink("Internal Teacher ID",sortOrderFieldName,"intenalteacherid",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("First Name",sortOrderFieldName,"fname",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Last Name",sortOrderFieldName,"lname",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Middle Name",sortOrderFieldName,"mname",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				
				responseText=PaginationAndSorting.responseSortingLink("Email Address",sortOrderFieldName,"email",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink("Phone Number",sortOrderFieldName,"pno",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Mobile Number",sortOrderFieldName,"mno",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Address1",sortOrderFieldName,"add1",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Address2",sortOrderFieldName,"add2",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("City",sortOrderFieldName,"city",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("State",sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Zip Code",sortOrderFieldName,"zip",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Country",sortOrderFieldName,"country",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Gender",sortOrderFieldName,"gender",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Race",sortOrderFieldName,"race",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Internal Candidate",sortOrderFieldName,"ic",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Record Status",sortOrderFieldName,"record",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("District Name",sortOrderFieldName,"dname",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Affidavit Accepted",sortOrderFieldName,"affidavit",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Years of teaching",sortOrderFieldName,"yot",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				
				responseText=PaginationAndSorting.responseSortingLink("National Board ",sortOrderFieldName,"nb",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink("Teach for America",sortOrderFieldName,"teach",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Year at TFA",sortOrderFieldName,"yat",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("TFA Region",sortOrderFieldName,"tfa",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("EPI Norm Score",sortOrderFieldName,"epi",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Attitudinal Factors Score",sortOrderFieldName,"afs",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Cognitive Ability Score",sortOrderFieldName,"cas",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Teaching Skills Score",sortOrderFieldName,"tss",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Achievement Score",sortOrderFieldName,"ascore",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
			    responseText=PaginationAndSorting.responseSortingLink("L_R_Score",sortOrderFieldName,"lscore",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				
				responseText=PaginationAndSorting.responseSortingLink("MA_Score",sortOrderFieldName,"mas",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("NF_Score",sortOrderFieldName,"nfscore",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("PNQ",sortOrderFieldName,"pnq",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Other Languages",sortOrderFieldName,"ol",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Can_Sub",sortOrderFieldName,"cs",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				
				responseText=PaginationAndSorting.responseSortingLink("Doctorates Year",sortOrderFieldName,"docyear",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink("Doctorates Degree",sortOrderFieldName,"docdegree",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Doctorates School",sortOrderFieldName,"docschool",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Doctorates GPA",sortOrderFieldName,"dgpa",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				//add new
				responseText=PaginationAndSorting.responseSortingLink("Doctorates Field",sortOrderFieldName,"dfield",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Doctorates Transcript",sortOrderFieldName,"dt",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Masters Year",sortOrderFieldName,"my",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Masters Degree",sortOrderFieldName,"mdegree",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Masters School",sortOrderFieldName,"mastersschool",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Masters GPA",sortOrderFieldName,"mastergpa",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				//add new
				responseText=PaginationAndSorting.responseSortingLink("Masters Field",sortOrderFieldName,"masterfield",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Masters Transcript",sortOrderFieldName,"mastran",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Bachelors Year",sortOrderFieldName,"byear",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				
				responseText=PaginationAndSorting.responseSortingLink("Bachelors Degree",sortOrderFieldName,"bdegree",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink("Bachelors School",sortOrderFieldName,"bachsch",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Bachelors GPA",sortOrderFieldName,"bgpa",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				//add new
				responseText=PaginationAndSorting.responseSortingLink("Bachelors Field",sortOrderFieldName,"bfield",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Bachelors Transcript",sortOrderFieldName,"bachtrans",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Associates Year",sortOrderFieldName,"assoyear",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink("Associates Degree",sortOrderFieldName,"assodeg",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Associates School",sortOrderFieldName,"assosch",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Associates GPA",sortOrderFieldName,"assogpa",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				//add new
				responseText=PaginationAndSorting.responseSortingLink("Associates Field",sortOrderFieldName,"assofield",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Associates Transcript",sortOrderFieldName,"assotrans",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				
				
				
				
				
				
			
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				if(finallstNobleCandidate.size()==0){
					 tmRecords.append("<tr><td colspan='9' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
					}
				
				String sta="";
				  if(finallstNobleCandidate.size()>0){
	                   for (Iterator it = finallstNobleCandidate.iterator(); it.hasNext();)
	                   {
	                	   String[] row = (String[]) it.next(); 
	                	   
	                	   String  myVal1="";
	                	   String  myVal2="";
	                	  
	                	        tmRecords.append("<tr>");	
	                	        
	                            myVal1 = (row[0]==null)? "N/A" : row[0].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
	                            
			                    
			                    myVal1 = (row[1]==null)? "N/A" : row[1].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? "N/A" : row[2].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    
			                    myVal1 = (row[3]==null)? "N/A" : row[3].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[4]==null)? "N/A" : row[4].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[5]==null)? "N/A" : row[5].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[11]==null)||(row[11].toString().length()==0))? " " : row[11].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[12]==null)||(row[12].toString().length()==0))? " " : row[12].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[13]==null)? "N/A" : row[13].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[14]==null)? "N/A" : row[14].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[15]==null)? "N/A" : row[15].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[16]==null)||(row[16].toString().length()==0))? " " : row[16].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[17]==null)||(row[17].toString().length()==0))? " " : row[17].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[18]==null)||(row[18].toString().length()==0))? " " : row[18].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[19]==null)||(row[19].toString().length()==0))? " " : row[19].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[20]==null)||(row[20].toString().length()==0))? " " : row[20].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[21]==null)||(row[21].toString().length()==0))? " " : row[21].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[22]==null)||(row[22].toString().length()==0))? " " : row[22].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[23]==null)||(row[23].toString().length()==0))? " " : row[23].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[24]==null)||(row[24].toString().length()==0))? " " : row[24].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[25]==null)||(row[25].toString().length()==0))? " " : row[25].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[26]==null)||(row[26].toString().length()==0))? " " : row[26].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[27]==null)||(row[27].toString().length()==0))? " " : row[27].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[28]==null)||(row[28].toString().length()==0))? " " : row[28].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[29]==null)||(row[29].toString().length()==0))? " " : row[29].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[30]==null)||(row[30].toString().length()==0))? " " : row[30].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[31]==null)||(row[31].toString().length()==0))? " " : row[31].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[32]==null)||(row[32].toString().length()==0))? " " : row[32].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[33]==null)||(row[33].toString().length()==0))? " " : row[33].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[34]==null)||(row[34].toString().length()==0))? " " : row[34].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[35]==null)||(row[35].toString().length()==0))? " " : row[35].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[36]==null)||(row[36].toString().length()==0))? " " : row[36].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[37]==null)||(row[37].toString().length()==0))? " " : row[37].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[38]==null)||(row[38].toString().length()==0))? " " : row[38].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[39]==null)||(row[39].toString().length()==0))? " " : row[39].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[40]==null)||(row[40].toString().length()==0))? " " : row[40].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[41]==null)||(row[41].toString().length()==0))? " " : row[41].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[42]==null)||(row[42].toString().length()==0))? " " : row[42].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[43]==null)||(row[43].toString().length()==0))? " " : row[43].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[44]==null)||(row[44].toString().length()==0))? " " : row[44].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[45]==null)||(row[45].toString().length()==0))? " " : row[45].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[46]==null)||(row[46].toString().length()==0))? " " : row[46].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[47]==null)||(row[47].toString().length()==0))? " " : row[47].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[48]==null)||(row[48].toString().length()==0))? " " : row[48].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[49]==null)||(row[49].toString().length()==0))? " " : row[49].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[50]==null)||(row[50].toString().length()==0))? " " : row[50].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[51]==null)||(row[51].toString().length()==0))? " " : row[51].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[52]==null)||(row[52].toString().length()==0))? " " : row[52].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[53]==null)||(row[53].toString().length()==0))? " " : row[53].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[54]==null)||(row[54].toString().length()==0))? " " : row[54].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[55]==null)||(row[55].toString().length()==0))? " " : row[55].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[56]==null)||(row[56].toString().length()==0))? " " : row[56].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[57]==null)||(row[57].toString().length()==0))? " " : row[57].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[58]==null)||(row[58].toString().length()==0))? " " : row[58].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                  
			                    
			                    
			                    tmRecords.append("</tr>");
	               }               
	              }
				
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			

			}catch(Exception e){
				e.printStackTrace();
			}
			
		 return tmRecords.toString();
	  }
		
	/*public String displayRecordsByEntityTypeEXL(String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		int sortingcheck=1;
		try{
			
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
		}
		
		//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start =((pgNo-1)*noOfRowInPage);
		int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		
		 //** set default sorting fieldName **//*
		String  sortOrderStrVal		=	null;
		String sortOrderFieldName	=	"jobid";
		
		if(!sortOrder.equals("") && !sortOrder.equals(null))
		{
			sortOrderFieldName		=	sortOrder;
		}
		
		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null))
		{
			if(sortOrderType.equals("0"))
			{
				sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
			}
			else
			{
				sortOrderTypeVal	=	"1";
				sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
			}
		}
		else
		{
			sortOrderTypeVal		=	"0";
			sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
		}
		
		//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
			List<String[]> lstNobleCandidate =new ArrayList<String[]>();
			
			
			if(entityID==2||entityID==3)
			{
				lstNobleCandidate =jobOrderDAO.noblestjobs(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
			  	totalRecord = lstNobleCandidate.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
		    	 
			}
			else if(entityID==1) 
			{
				System.out.println("indside admin ");
				
				lstNobleCandidate =jobOrderDAO.noblestjobs(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
			  	totalRecord = lstNobleCandidate.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
				
			
			}
				
				List<String[]> finallstNobleCandidate =new ArrayList<String[]>();
			     if(totalRecord<end)
						end=totalRecord;
			     finallstNobleCandidate=lstNobleCandidate;


		 
		 
			 //  Excel   Exporting	
					
					String time = String.valueOf(System.currentTimeMillis()).substring(6);
					//String basePath = request.getRealPath("/")+"/candidate";
					String basePath = request.getSession().getServletContext().getRealPath ("/")+"/NobleStJobsExl";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="noblestjobs"+time+".xls";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

					WorkbookSettings wbSettings = new WorkbookSettings();

					wbSettings.setLocale(new Locale("en", "EN"));

					WritableCellFormat timesBoldUnderline;
					WritableCellFormat header;
					WritableCellFormat headerBold;
					WritableCellFormat times;

					WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
					workbook.createSheet("nobleapplication", 0);
					WritableSheet excelSheet = workbook.getSheet(0);

					WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
					// Define the cell format
					times = new WritableCellFormat(times10pt);
					// Lets automatically wrap the cells
					times.setWrap(true);

					// Create create a bold font with unterlines
					WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
					WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

					timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
					timesBoldUnderline.setAlignment(Alignment.CENTRE);
					// Lets automatically wrap the cells
					timesBoldUnderline.setWrap(true);

					header = new WritableCellFormat(times10ptBoldUnderline);
					headerBold = new WritableCellFormat(times10ptBoldUnderline);
					CellView cv = new CellView();
					cv.setFormat(times);
					cv.setFormat(timesBoldUnderline);
					cv.setAutosize(true);

					header.setBackground(Colour.GRAY_25);

					// Write a few headers
					excelSheet.mergeCells(0, 0, 15, 1);
					Label label;
					label = new Label(0, 0, "Noble St Application ", timesBoldUnderline);
					excelSheet.addCell(label);
					excelSheet.mergeCells(0, 3, 15, 3);
					label = new Label(0, 3, "");
					excelSheet.addCell(label);
					excelSheet.getSettings().setDefaultColumnWidth(18);
					
					int k=4;
					int col=1;
					label = new Label(0, k, "Internal Job ID",header); 
					excelSheet.addCell(label);
					
					label = new Label(1, k, "Job Title",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Job Posted Date",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Job Post End Date",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Immediate Hiring",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Hiring Season",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Job Category",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Job Subject",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Job Status",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Job Type",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Job Created By",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Job Record Date",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "No. of Positions",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "No. of Positions Filled",header); 
					excelSheet.addCell(label);
					 
					label = new Label(++col, k, " No. of Positions Filled(%)",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "No. of School for Job",header); 
					excelSheet.addCell(label);
					
					k=k+1;
					if(finallstNobleCandidate.size()==0)
					{	
						excelSheet.mergeCells(0, k, 15, k);
						label = new Label(0, k, "No Records Founds"); 
						excelSheet.addCell(label);
					}
					String sta="";
			if(finallstNobleCandidate.size()>0){
				 for (Iterator it = finallstNobleCandidate.iterator(); it.hasNext();) 					            
					{
					String[] row = (String[]) it.next();
				col=1;
			
						label = new Label(0, k,row[0].toString()); 
						excelSheet.addCell(label);
				
						label = new Label(1, k, row[1].toString()); 
						excelSheet.addCell(label);
						
						 String  myVal111 = (row[2]==null)? "N/A" : row[2].toString();
							label = new Label(++col, k, myVal111); 
							excelSheet.addCell(label);
						
						
					 String  myVal1 = (row[3]==null)? "N/A" : row[3].toString();
					label = new Label(++col, k, myVal1); 
					excelSheet.addCell(label);
					
					 String  myVal2 = ((row[4]==null)||(row[4].toString().length()==0))? " " : row[4].toString();
					label = new Label(++col, k, myVal2); 
					excelSheet.addCell(label);
					
					 String  myVal3 = ((row[5]==null)||(row[5].toString().length()==0))? "N/A" : row[5].toString();
					label = new Label(++col, k, myVal3); 
					excelSheet.addCell(label);
					
					 String  myVal4 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString();
					label = new Label(++col, k, myVal4); 
					excelSheet.addCell(label);
					
					String  myVal7 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString();
					label = new Label(++col, k, myVal7); 
					excelSheet.addCell(label);
					
					String  myVal8 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString();
					label = new Label(++col, k, myVal8); 
					excelSheet.addCell(label);
					
					String  myVal9 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString();
					label = new Label(++col, k, myVal9); 
					excelSheet.addCell(label);
					
					String  myVal10 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString();
					label = new Label(++col, k, myVal10); 
					excelSheet.addCell(label);
					
					String  myVal11 = ((row[11]==null)||(row[11].toString().length()==0))? " " : row[11].toString();
					label = new Label(++col, k, myVal11); 
					excelSheet.addCell(label);
					
					String  myVal12 = ((row[12]==null)||(row[12].toString().length()==0))? " " : row[12].toString();
					label = new Label(++col, k, myVal12); 
					excelSheet.addCell(label);
					
					String  myVal13 = ((row[13]==null)||(row[13].toString().length()==0))? " " : row[13].toString();
					label = new Label(++col, k, myVal13); 
					excelSheet.addCell(label);
					
					String  myVal14 = ((row[14]==null)||(row[14].toString().length()==0))? " " : row[14].toString();
					label = new Label(++col, k, myVal14); 
					excelSheet.addCell(label);
					
					String  myVal15 = ((row[15]==null)||(row[15].toString().length()==0))? " " : row[15].toString();
					label = new Label(++col, k, myVal15); 
					excelSheet.addCell(label);
					
									
								
				   k++; 
			 }
			}
	
			workbook.write();
			workbook.close();
		}catch(Exception e){}
		
	 return fileName;
  }*/
	
  
	
	/*public String displayRecordsByEntityTypePDF(String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport(  noOfRow, pageNo, sortOrder, sortOrderType,basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	public boolean generateCandidatePDReport(String noOfRow,String pageNo,String sortOrder,String sortOrderType,String path,String realPath)
	{
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			int sortingcheck=1;
			try{
				
				
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				
				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				 *//** set default sorting fieldName **//*
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"jobid";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<String[]> lstNobleCandidate =new ArrayList<String[]>();
					
					
					if(entityID==2||entityID==3)
					{
						lstNobleCandidate =jobOrderDAO.noblestjobs(sortingcheck,sortOrderStrVal,start, noOfRowInPage,false,sortOrderFieldName);
					  	totalRecord = lstNobleCandidate.size();
					    	System.out.println("totallllllllllll========="+totalRecord);
				    	 
					}
					else if(entityID==1) 
					{
						System.out.println("indside admin ");
						
						lstNobleCandidate =jobOrderDAO.noblestjobs(sortingcheck,sortOrderStrVal,start, noOfRowInPage,false,sortOrderFieldName);
					  	totalRecord = lstNobleCandidate.size();
					    	System.out.println("totallllllllllll========="+totalRecord);
						
					}
												
						List<String[]> finallstNobleCandidate =new ArrayList<String[]>();
					     if(totalRecord<end)
								end=totalRecord;
					     finallstNobleCandidate=lstNobleCandidate;
		
				 String fontPath = realPath;
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject("Noble St Jobs");
						document.addCreationDate();
						document.addTitle("Noble St Jobs");

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));
						
						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						
						para[0] = new Paragraph("Noble St Application",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
												
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph("Created By: "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+"Created on: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						document.add(new Phrase("\n"));


						float[] tblwidth={.06f,.10f,.07f,.06f,.06f,.06f,.07f,.05f,.06f,.05f,.06f,.06f,.04f,.05f,.05f,.04f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[16];
						cell = new PdfPCell[16];
				// header
						para[0] = new Paragraph("Internal Job ID",font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+"Job Title",font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(""+"Job Posted Date",font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[2]);
						
						para[3] = new Paragraph(""+"Job Post End Date",font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[3]);

						para[4] = new Paragraph(""+"Immediate Hiring",font10_10);
						cell[4]= new PdfPCell(para[4]);
						cell[4].setBackgroundColor(bluecolor);
						cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[4]);
						
						para[5] = new Paragraph(""+"Hiring Season",font10_10);
						cell[5]= new PdfPCell(para[5]);
						cell[5].setBackgroundColor(bluecolor);
						cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[5]);
						
						para[6] = new Paragraph(""+"Job Category",font10_10);
						cell[6]= new PdfPCell(para[6]);
						cell[6].setBackgroundColor(bluecolor);
						cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[6]);
						
						para[7] = new Paragraph(""+"Job Subject",font10_10);
						cell[7]= new PdfPCell(para[7]);
						cell[7].setBackgroundColor(bluecolor);
						cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[7]);
						
						para[8] = new Paragraph(""+"Job Status",font10_10);
						cell[8]= new PdfPCell(para[8]);
						cell[8].setBackgroundColor(bluecolor);
						cell[8].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[8]);
						
						para[9] = new Paragraph(""+"Job Type",font10_10);
						cell[9]= new PdfPCell(para[9]);
						cell[9].setBackgroundColor(bluecolor);
						cell[9].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[9]);
						
						para[10] = new Paragraph(""+"Job Created By",font10_10);
						cell[10]= new PdfPCell(para[10]);
						cell[10].setBackgroundColor(bluecolor);
						cell[10].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[10]);
												
						
						para[11] = new Paragraph(""+"Job Record Date",font10_10);
						cell[11]= new PdfPCell(para[11]);
						cell[11].setBackgroundColor(bluecolor);
						cell[11].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[11]);
						document.add(mainTable);
						
						para[12] = new Paragraph(""+"No. of Positions",font10_10);
						cell[12]= new PdfPCell(para[12]);
						cell[12].setBackgroundColor(bluecolor);
						cell[12].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[12]);
						document.add(mainTable);
						
						para[13] = new Paragraph(""+"No. of Positions Filled",font10_10);
						cell[13]= new PdfPCell(para[13]);
						cell[13].setBackgroundColor(bluecolor);
						cell[13].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[13]);
						document.add(mainTable);
						
						para[14] = new Paragraph(""+"% No. of Positions Filled",font10_10);
						cell[14]= new PdfPCell(para[14]);
						cell[14].setBackgroundColor(bluecolor);
						cell[14].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[14]);
						document.add(mainTable);
						
						para[15] = new Paragraph(""+"No. of School for Job",font10_10);
						cell[15]= new PdfPCell(para[15]);
						cell[15].setBackgroundColor(bluecolor);
						cell[15].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[15]);
						document.add(mainTable);
						
						if(finallstNobleCandidate.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph("No Record Found.",font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
				     String sta="";
						if(finallstNobleCandidate.size()>0){
							for (Iterator it = finallstNobleCandidate.iterator(); it.hasNext();) 					            
							 {
								String[] row = (String[]) it.next();
							  int index=0;
							 									
							  mainTable = new PdfPTable(tblwidth);
							  mainTable.setWidthPercentage(100);
							  para = new Paragraph[16];
							  cell = new PdfPCell[16];
							
                              String myVal1="";
                              String myVal2="";
                              
								 myVal1 = (row[0]==null)? "N/A" : row[0].toString();
								 para[index] = new Paragraph( row[0].toString(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
															 
								 myVal1 = (row[1]==null)? "N/A" : row[1].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? "N/A" : row[2].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 								 
								 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[4]==null)? "N/A" : row[4].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								
								 myVal1 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[11]==null)||(row[11].toString().length()==0))? " " : row[11].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[12]==null)||(row[12].toString().length()==0))? " " : row[12].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[13]==null)? "N/A" : row[13].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[14]==null)? "N/A" : row[14].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[15]==null)? "N/A" : row[15].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								

						document.add(mainTable);
				   }
				}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	
	public String displayRecordsByEntityTypePrintPreview(String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println("inside Print Preview");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
		
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 *//** set default sorting fieldName **//*
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"jobid";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<String[]> lstNobleCandidate =new ArrayList<String[]>();
				
				
				if(entityID==2||entityID==3)
				{
					System.out.println("inside district print");
					lstNobleCandidate =jobOrderDAO.noblestjobs(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
				  	totalRecord = lstNobleCandidate.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
			    	 
				}
				else if(entityID==1) 
				{
					System.out.println("indside admin print ");
					
					lstNobleCandidate =jobOrderDAO.noblestjobs(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
				  	totalRecord = lstNobleCandidate.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
				
				}
										
					List<String[]> finallstNobleCandidate =new ArrayList<String[]>();
				     if(totalRecord<end)
							end=totalRecord;
				     finallstNobleCandidate=lstNobleCandidate;
				
			

	        tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>Noble St Application</div><br/>");
			
		    	tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+"Created by: "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>Date of Print: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			    tmRecords.append("</div>");
			
			
			tmRecords.append("<table  id='tblGridNoblePrint' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Internal Job ID</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Title</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Posted Date</th>");
    
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Post End Date</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Immediate Hiring</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Hiring Season</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Category</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Subject</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Status</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Type</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Created By</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Record Date</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>No. of Positions</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>No. of Positions Filled</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>% No. of Positions Filled</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>No. of School for Job</th>");
			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(finallstNobleCandidate.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			}
		String sta="";
			if(finallstNobleCandidate.size()>0){
				 for (Iterator it = finallstNobleCandidate.iterator(); it.hasNext();) 					            
					{
					String[] row = (String[]) it.next();
					
					 String myVal1="";
                     String myVal2="";
					  
					 tmRecords.append("<tr>");
					 
					 myVal1 = (row[0]==null)? "N/A" : row[0].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
					 
						 myVal1 = (row[1]==null)? "N/A" : row[1].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = (row[2]==null)? "N/A" : row[2].toString();
							tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
														
							 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
								tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = ((row[4]==null)||(row[4].toString().length()==0))? "N/A" : row[4].toString();
			      		tmRecords.append("<td style='font-size:12px;'>"+ myVal1+"</td>");	
			      		
			      		 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString();
							tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
							
							 myVal1 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString();
								tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
								
								 myVal1 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString();
									tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
									
									 myVal1 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString();
										tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
										
										 myVal1 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString();
											tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
											
											 myVal1 = ((row[11]==null)||(row[11].toString().length()==0))? " " : row[11].toString();
												tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
												
												 myVal1 = ((row[12]==null)||(row[12].toString().length()==0))? " " : row[12].toString();
													tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
																										
													 myVal1 = (row[13]==null)? "N/A" : row[13].toString();
														tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
																												
														 myVal1 = (row[14]==null)? "N/A" : row[14].toString();
															tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
																														
															 myVal1 = (row[15]==null)? "N/A" : row[15].toString();
																tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
													
					   tmRecords.append("</tr>");
			   }
			}
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
  }*/
	 public String displayNobleCandidateCSV(String noOfRow,String pageNo,String sortOrder,String sortOrderType){
		 String basePath = null;
			
			String fileName = null;
		 WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=1;
			try{
				
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				//System.out.println("schoolMaster="+schoolMaster+"districtMaster="+districtMaster+"entityID="+entityID);
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position


				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
			 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"intenalteacherid";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
				
			/**end set dynamic sorting fieldName **/
                   List<String[]> lstNobleCandidate =new ArrayList<String[]>();
				
                   if(entityID==2||entityID==3)
   				{
   					lstNobleCandidate =jobForTeacherDAO.noblestcandidate(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
   				  	totalRecord = lstNobleCandidate.size();
   				   	System.out.println("totallllllllllll========="+totalRecord);
   			    	 
   				}
   				else if(entityID==1) 
   				{
   					System.out.println("indside admin candidate ");
   					
   					lstNobleCandidate =jobForTeacherDAO.noblestcandidate(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
   				  	totalRecord = lstNobleCandidate.size();
   				   	System.out.println("totallllllllllll========="+totalRecord);
   				
   				}
				
				List<String[]> finallstNobleCandidate =new ArrayList<String[]>();
			     if(totalRecord<end)
						end=totalRecord;
			     finallstNobleCandidate=lstNobleCandidate.subList(start,end);
			
			 
		   //Excel SmartFusion Exporting	

			     String time = String.valueOf(System.currentTimeMillis()).substring(6);
				
			     basePath = request.getSession().getServletContext().getRealPath ("/")+"/hiredapplicants";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="noblestcandidate"+time+".csv";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);
                    System.out.println("fileeeeeeeeeeee====="+file);
			 FileWriter writer = new FileWriter(file);
			 
		   //Delimiter used in CSV file
			final String COMMA_DELIMITER = ",";
			final String NEW_LINE_SEPARATOR = "\n";
			
			StringBuilder csvFileData = new StringBuilder();
			 
			csvFileData.append("Internal_Teacher_ID");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Candidate_First_Name");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Candidate_Last_Name");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Candidate_Middle_Name");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Teacher_Email");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Phone_Number");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Mobile_Number");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Address1");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Address2");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("City");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("State");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Zip_Code");
			csvFileData.append(COMMA_DELIMITER);
		
			csvFileData.append("Country");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Gender");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Race");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Internal_Candidate");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Record_Status");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("District_Name");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Affidavit_Accepted");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Years_of_Teaching");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Year_of_National_Board_Certified");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Teach_for_America");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Year_at_TFA");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("TFA_Region");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("EPI_Norm_Score");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Attitudinal_Factors_Score");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Cognitive_Ability_Score");
			csvFileData.append(COMMA_DELIMITER);
		
			csvFileData.append("Teaching_Skills_Score");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Achievement_Score");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("L_R_Score");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("MA_Score");
			csvFileData.append(COMMA_DELIMITER);
			 			 
			csvFileData.append("NF_Score");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("PNQ");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Other_Languages");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Can_Sub");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Doctorates_Year");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Doctorates_Degree");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Doctorates_School");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Doctorates_GPA");
			csvFileData.append(COMMA_DELIMITER);
			//add new
			csvFileData.append("Doctorates_field");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Doctorates_Transcript");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Masters_Year");
			csvFileData.append(COMMA_DELIMITER);
		
			csvFileData.append("Masters_Degree");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Masters_School");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Masters_GPA");
			csvFileData.append(COMMA_DELIMITER);
			
			//add new
			csvFileData.append("Masters_field");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Masters_Transcript");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Bachelors_Year");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Bachelors_Degree");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Bachelors_School");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Bachelors_GPA");
			csvFileData.append(COMMA_DELIMITER);
			
			//add new
			csvFileData.append("Bachelors_field");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Bachelors_Transcript");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Associates_Year");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Associates_Degree");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Associates_School");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Associates_GPA");
			csvFileData.append(COMMA_DELIMITER);
			
			//add new
			csvFileData.append("Associates_field");
			csvFileData.append(COMMA_DELIMITER);
			 						
			csvFileData.append("Associates_Transcript");
					 	

				if(lstNobleCandidate!=null)
				{
				 if(lstNobleCandidate.size()>0){
				 totalRecord =  lstNobleCandidate.size();
				 }
				
				 System.out.println("totalRecord: "+totalRecord+" end: "+end+" start: "+start+" sortOrderFieldName: "+sortOrderFieldName);
				 if(totalRecord>end){
					 end =totalRecord;
					 finallstNobleCandidate =lstNobleCandidate.subList(start, end);
				}else{
					 end =totalRecord;
					 finallstNobleCandidate =lstNobleCandidate.subList(start, end);
				}
				
				
				if(finallstNobleCandidate.size()==0){
					csvFileData.append(NEW_LINE_SEPARATOR);
					csvFileData.append("No Rcords Founds");
				}
			
				 if(finallstNobleCandidate.size()>0){
					// Integer count=1;
	                   for (Iterator it = finallstNobleCandidate.iterator(); it.hasNext();)
	                   {
	                	   String[] row = (String[]) it.next(); 

						
						
						String  myVal1="";
						
						   myVal1 = (row[0]==null)? "N/A" : row[0].toString();
						 csvFileData.append(NEW_LINE_SEPARATOR);
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						  myVal1 = (row[1]==null)? "N/A" : row[1].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[2]==null)? "N/A" : row[2].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[4]==null)? "N/A" : row[4].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[6]==null)? "N/A" : row[6].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[7]==null)? "N/A" : row[7].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[8]==null)? "N/A" : row[8].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[9]==null)? "N/A" : row[9].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[10]==null)? "N/A" : row[10].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[11]==null)? "N/A" : row[11].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[12]==null)? "N/A" : row[12].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[13]==null)? "N/A" : row[13].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[14]==null)? "N/A" : row[14].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[15]==null)? "N/A" : row[15].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						  myVal1 = (row[16]==null)? "N/A" : row[16].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[17]==null)? "N/A" : row[17].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[18]==null)? "N/A" : row[18].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[19]==null)? "N/A" : row[19].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[20]==null)? "N/A" : row[20].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[21]==null)? "N/A" : row[21].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[22]==null)? "N/A" : row[22].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[23]==null)? "N/A" : row[23].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[24]==null)? "N/A" : row[24].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[25]==null)? "N/A" : row[25].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[26]==null)? "N/A" : row[26].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[27]==null)? "N/A" : row[27].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[28]==null)? "N/A" : row[28].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[29]==null)? "N/A" : row[29].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[30]==null)? "N/A" : row[30].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[31]==null)? "N/A" : row[31].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[32]==null)? "N/A" : row[32].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[33]==null)? "N/A" : row[33].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[34]==null)? "N/A" : row[34].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							  myVal1 = (row[35]==null)? "N/A" : row[35].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[36]==null)? "N/A" : row[36].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[37]==null)? "N/A" : row[37].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[38]==null)? "N/A" : row[38].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[39]==null)? "N/A" : row[39].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[40]==null)? "N/A" : row[40].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[41]==null)? "N/A" : row[41].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[42]==null)? "N/A" : row[42].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[43]==null)? "N/A" : row[43].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[44]==null)? "N/A" : row[44].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[45]==null)? "N/A" : row[45].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[46]==null)? "N/A" : row[46].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[47]==null)? "N/A" : row[47].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[48]==null)? "N/A" : row[48].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[49]==null)? "N/A" : row[49].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[50]==null)? "N/A" : row[50].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[51]==null)? "N/A" : row[51].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[52]==null)? "N/A" : row[52].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[53]==null)? "N/A" : row[53].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[54]==null)? "N/A" : row[54].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[55]==null)? "N/A" : row[55].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[56]==null)? "N/A" : row[56].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[57]==null)? "N/A" : row[57].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[58]==null)? "N/A" : row[58].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 
														
						
						//count++;
					}
				}
			}else{
				
				csvFileData.append(NEW_LINE_SEPARATOR);
				csvFileData.append("No Rcords Founds");
					
			}
				  writer.append(csvFileData);
				  writer.flush();
				  writer.close();
				  
			}catch (Exception e) {
				e.printStackTrace();
			}
		  return fileName;
	  }	
	
	/*****************************/
	 public String displayNobleCandidateCSVRun(JobForTeacherDAO jobForTeacherDAO1,ServletContext servletContext){
		 
		 System.out.println("Method call from schudler jobForTeacherDAO1: "+jobForTeacherDAO1+" servletContext :"+servletContext);
		 String basePath = null;
		String fileName = null;
		try{
		
			/**end set dynamic sorting fieldName **/
                   List<String[]> lstNobleCandidate =new ArrayList<String[]>();
				
                   System.out.println("jobForTeacherDAO1 : "+jobForTeacherDAO1);
					lstNobleCandidate =jobForTeacherDAO1.noblestCandidateRun();
			  
			    	List<String[]> finallstNobleCandidate =new ArrayList<String[]>();
			   
			       finallstNobleCandidate=lstNobleCandidate;//.subList(start,end);
			System.out.println("finallstNobleCandidate===="+finallstNobleCandidate.size());
			    
			 String time = String.valueOf(System.currentTimeMillis()).substring(6);
			 /***************************creating directory inside  ***rootPath*******************************************/	
			    FileOutputStream outStream = null;
			    fileName ="noblestcandidate.csv";
			    String nobleCandidate = Utility.getValueOfPropByKey("rootPath"); 
				System.out.println("nobleCandidate : "+nobleCandidate);	
				
				String sourceDirName = nobleCandidate+"//"+"NobleCandidate"; 
				String targetFileName = sourceDirName+"/"+fileName; ;
				
				File nobleCandidateDir = new File(sourceDirName);
				
				File sourceFile= new File(targetFileName);
				System.out.println("sourceFile========"+sourceFile);
				System.out.println("ileFeedDir.exists() before : "+nobleCandidateDir.exists());
				
				if(!nobleCandidateDir.exists())
				{
					nobleCandidateDir.mkdir();
					outStream = new FileOutputStream(sourceFile);
					System.out.println("directory created succesffully");
				}
				if(sourceFile.exists())
				{
					outStream = new FileOutputStream(targetFileName);
					System.out.println("sourceFile.exists()writing Noble Candidate report : "+sourceFile.exists());
				}
				
		/********************************************************************/	
			 
			 Utility.deleteAllFileFromDir(sourceDirName);
			// file = new File(basePath+"/"+fileName);
			 
			 FileWriter writer = new FileWriter(sourceFile);
			 
		   //Delimiter used in CSV file
			 final String COMMA_DELIMITER = ",";
				final String NEW_LINE_SEPARATOR = "\n";
				
				StringBuilder csvFileData = new StringBuilder();
				 
				csvFileData.append("Internal_Teacher_ID");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Candidate_First_Name");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Candidate_Last_Name");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Candidate_Middle_Name");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Teacher_Email");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Phone_Number");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Mobile_Number");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Address1");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Address2");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("City");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("State");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Zip_Code");
				csvFileData.append(COMMA_DELIMITER);
			
				csvFileData.append("Country");
				csvFileData.append(COMMA_DELIMITER);
				
				csvFileData.append("Gender");
				csvFileData.append(COMMA_DELIMITER);
				
				csvFileData.append("Race");
				csvFileData.append(COMMA_DELIMITER);
				
				csvFileData.append("Internal_Candidate");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Record_Status");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("District_Name");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Affidavit_Accepted");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Years_of_Teaching");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Year_of_National_Board_Certified");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Teach_for_America");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Year_at_TFA");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("TFA_Region");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("EPI_Norm_Score");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Attitudinal_Factors_Score");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Cognitive_Ability_Score");
				csvFileData.append(COMMA_DELIMITER);
			
				csvFileData.append("Teaching_Skills_Score");
				csvFileData.append(COMMA_DELIMITER);
				
				csvFileData.append("Achievement_Score");
				csvFileData.append(COMMA_DELIMITER);
				
				csvFileData.append("L_R_Score");
				csvFileData.append(COMMA_DELIMITER);
				
				csvFileData.append("MA_Score");
				csvFileData.append(COMMA_DELIMITER);
				 			 
				csvFileData.append("NF_Score");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("PNQ");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Other_Languages");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Can_Sub");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Doctorates_Year");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Doctorates_Degree");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Doctorates_School");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Doctorates_GPA");
				csvFileData.append(COMMA_DELIMITER);
				//add new
				csvFileData.append("Doctorates_field");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Doctorates_Transcript");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Masters_Year");
				csvFileData.append(COMMA_DELIMITER);
			
				csvFileData.append("Masters_Degree");
				csvFileData.append(COMMA_DELIMITER);
				
				csvFileData.append("Masters_School");
				csvFileData.append(COMMA_DELIMITER);
				
				csvFileData.append("Masters_GPA");
				csvFileData.append(COMMA_DELIMITER);
				
				//add new
				csvFileData.append("Masters_field");
				csvFileData.append(COMMA_DELIMITER);
				
				csvFileData.append("Masters_Transcript");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Bachelors_Year");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Bachelors_Degree");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Bachelors_School");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Bachelors_GPA");
				csvFileData.append(COMMA_DELIMITER);
				
				//add new
				csvFileData.append("Bachelors_field");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Bachelors_Transcript");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Associates_Year");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Associates_Degree");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Associates_School");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Associates_GPA");
				csvFileData.append(COMMA_DELIMITER);
				
				//add new
				csvFileData.append("Associates_field");
				csvFileData.append(COMMA_DELIMITER);
				 						
				csvFileData.append("Associates_Transcript");
					 	

				if(lstNobleCandidate!=null)
				{
							
				if(finallstNobleCandidate.size()==0){
					csvFileData.append(NEW_LINE_SEPARATOR);
					csvFileData.append("No Rcords Founds");
				}
			
				 if(finallstNobleCandidate.size()>0){
					// Integer count=1;
	                   for (Iterator it = finallstNobleCandidate.iterator(); it.hasNext();)
	                   {
	                	   String[] row = (String[]) it.next(); 

	                	   String  myVal1="";
							
						   myVal1 = (row[0]==null)? "N/A" : row[0].toString();
						 csvFileData.append(NEW_LINE_SEPARATOR);
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						  myVal1 = (row[1]==null)? "N/A" : row[1].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[2]==null)? "N/A" : row[2].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[4]==null)? "N/A" : row[4].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[6]==null)? "N/A" : row[6].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[7]==null)? "N/A" : row[7].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[8]==null)? "N/A" : row[8].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[9]==null)? "N/A" : row[9].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[10]==null)? "N/A" : row[10].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[11]==null)? "N/A" : row[11].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[12]==null)? "N/A" : row[12].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[13]==null)? "N/A" : row[13].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[14]==null)? "N/A" : row[14].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[15]==null)? "N/A" : row[15].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						  myVal1 = (row[16]==null)? "N/A" : row[16].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[17]==null)? "N/A" : row[17].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[18]==null)? "N/A" : row[18].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[19]==null)? "N/A" : row[19].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[20]==null)? "N/A" : row[20].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[21]==null)? "N/A" : row[21].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[22]==null)? "N/A" : row[22].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[23]==null)? "N/A" : row[23].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[24]==null)? "N/A" : row[24].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[25]==null)? "N/A" : row[25].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[26]==null)? "N/A" : row[26].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[27]==null)? "N/A" : row[27].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[28]==null)? "N/A" : row[28].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[29]==null)? "N/A" : row[29].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[30]==null)? "N/A" : row[30].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[31]==null)? "N/A" : row[31].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[32]==null)? "N/A" : row[32].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[33]==null)? "N/A" : row[33].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[34]==null)? "N/A" : row[34].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							  myVal1 = (row[35]==null)? "N/A" : row[35].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[36]==null)? "N/A" : row[36].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[37]==null)? "N/A" : row[37].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[38]==null)? "N/A" : row[38].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[39]==null)? "N/A" : row[39].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[40]==null)? "N/A" : row[40].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[41]==null)? "N/A" : row[41].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[42]==null)? "N/A" : row[42].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[43]==null)? "N/A" : row[43].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[44]==null)? "N/A" : row[44].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[45]==null)? "N/A" : row[45].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[46]==null)? "N/A" : row[46].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[47]==null)? "N/A" : row[47].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[48]==null)? "N/A" : row[48].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[49]==null)? "N/A" : row[49].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[50]==null)? "N/A" : row[50].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[51]==null)? "N/A" : row[51].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[52]==null)? "N/A" : row[52].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[53]==null)? "N/A" : row[53].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[54]==null)? "N/A" : row[54].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[55]==null)? "N/A" : row[55].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[56]==null)? "N/A" : row[56].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[57]==null)? "N/A" : row[57].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
								 
								 myVal1 = (row[58]==null)? "N/A" : row[58].toString();
								 csvFileData.append("\""+myVal1+"\"");
								 csvFileData.append(COMMA_DELIMITER);
						
						
						//count++;
					}
				}
			}else{
				
				csvFileData.append(NEW_LINE_SEPARATOR);
				csvFileData.append("No Rcords Founds");
					
			}
				  writer.append(csvFileData);
				  writer.flush();
				  writer.close();
				  
				  writeNobleReportONServer(servletContext);
				  
			}catch (Exception e) {
				e.printStackTrace();
			}
		  return basePath+"/"+fileName;
	  }	
	 /*****************************/
	
	 
	 public void writeNobleReportONServer(ServletContext servletContext){
		  System.out.println("************writing Noble Candidate  on server inside ******************* servletContext: "+servletContext);
		    try{
		     String path="";
		   
		     String source = Utility.getValueOfPropByKey("rootPath")+"NobleCandidate/noblestcandidate.csv";
			 System.out.println("source Path "+source);

			 String target = servletContext.getRealPath("/")+"/"+"/NobleCandidate/";
			 System.out.println("Real target Path "+target);

			File sourceFile = new File(source);
			File targetDir = new File(target);
			
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());

			try {
				FileUtils.copyFile(sourceFile, targetFile);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			path = Utility.getValueOfPropByKey("contextBasePath")+"/NobleCandidate/noblestcandidate.csv";
			System.out.println("NobleApplication contextBasePath "+path);
		  
			
			FTPConnectAndLogin.dropFileOnFTPServerForNobleCandidate(target+"noblestcandidate.csv");
			
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
			
		}
	 
	
}


