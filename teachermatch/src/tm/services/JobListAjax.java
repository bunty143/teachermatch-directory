package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.lang.ArrayUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;




public class JobListAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;	
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	
	 public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	
	public String displayRecordsByEntityType(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String endfromDate, String endtoDate,String sfromDate, String stoDate,String jobOrderId,String normScoreSelectVal,String normScoreVal,String statusId,boolean exportcheck)
		{
			System.out.println( normScoreSelectVal+" @@ " + normScoreVal+" @@@@@@@@@@   AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=0;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position

					int noOfRowInPage = Integer.parseInt(noOfRow);
					int pgNo = Integer.parseInt(pageNo);
					int start =((pgNo-1)*noOfRowInPage);
					int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
					int totalRecord = 0;
					int totalRecords=0;
					//------------------------------------
				 /** set default sorting fieldName **/
					String sortOrderFieldName="jobId";
					String sortOrderNoField="jobId";

					if(sortOrder.equalsIgnoreCase("districtName"))
						sortingcheck=1;
					
					if(sortOrder.equalsIgnoreCase("normScore"))
						sortingcheck=2;
					
					/**Start set dynamic sorting fieldName **/
					
					Order  sortOrderStrVal=null;

					if(sortOrder!=null){
						if(!sortOrder.equals("") && !sortOrder.equals(null)){
							sortOrderFieldName=sortOrder;
							sortOrderNoField=sortOrder;
						}
					}

					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("0")){
							sortOrderStrVal=Order.asc(sortOrderFieldName);
						}else{
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
						}
					}else{
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<JobOrder> lstJobOrders =new ArrayList<JobOrder>();
					List<Integer> lstSchoolJobOrderIds = new ArrayList<Integer>();
					boolean checkflag = false;
					List<Integer> jobIDS= new ArrayList<Integer>();
					List<Integer> jobIDsByAvgNormScore= new ArrayList<Integer>();
					List<Integer> finaljobIDS= new ArrayList<Integer>();
					boolean jobIdsflag=false;
					boolean normScoreflag=false;
					boolean finaljobIdsflag=false;
					boolean statusflag=false;
					
					if(districtOrSchoolId!=0 && entityID==1)
						  districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
					
					//******************************Job Status***************************************
// filter of job status 
					String statusNames[]=null;
					List<Integer> jobOrdersByJobStatuses = new ArrayList<Integer>();
					List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
					  List <StatusMaster> statusMaster1st=new ArrayList<StatusMaster>();
					 if(!statusId.equals("") && !statusId.equals("0")){
						 System.out.println(" Filter StatusId Start ::  "+statusId);
						 statusNames = statusId.split("#@");
					      if(!ArrayUtils.contains(statusNames,"0"))
					      {	
					    	 statusflag=true;
							 lstSecondaryStatus = secondaryStatusDAO.findByMultipleStatusName(districtMaster, statusNames);
					
							  List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
							  Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
							  for (StatusMaster statusMaster1 : statusMasterList) {
								  mapStatus.put(statusMaster1.getStatusShortName(),statusMaster1);
							  }
							  StatusMaster stMaster =null;
							
							  
							  if(ArrayUtils.contains(statusNames,"Available")){
									  stMaster = mapStatus.get("comp");
									  statusMaster1st.add(stMaster);
							  }
							  if(ArrayUtils.contains(statusNames,"Rejected")){
								    stMaster = mapStatus.get("rem");
								    statusMaster1st.add(stMaster);
						     }
							  if(ArrayUtils.contains(statusNames,"Timed Out")){
								    stMaster = mapStatus.get("vlt");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Withdrew")){
								    stMaster = mapStatus.get("widrw");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Incomplete")){
								    stMaster = mapStatus.get("icomp");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Hired")){
								    stMaster = mapStatus.get("hird");
								    statusMaster1st.add(stMaster);
						     }   
							 for(SecondaryStatus ss: lstSecondaryStatus)
							  {
								 if(ss.getStatusMaster()!=null)
									 statusMaster1st.add(ss.getStatusMaster()); 
							  }
							 jobOrdersByJobStatuses = jobForTeacherDAO.findJobOrderByCandidtesJobStatus(districtMaster,statusMaster1st,lstSecondaryStatus);
					      }
					 }
			//################################################################################
				// Job ID Filter
					if(!jobOrderId.equals(""))
					{
						jobIdsflag=true;
						String strjobIds[]=jobOrderId.split(",");
						if(strjobIds.length>0)
					     for(String str : strjobIds){
					    	 if(!str.equals(""))
					    	 jobIDS.add(Integer.parseInt(str));
					     }
					}
					
				// Average Norm Score Filter
					boolean nbyAflag=false;
					if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") && !normScoreSelectVal.equals("6"))
					 {
						normScoreflag=true;
						List avgNormScore=  jobForTeacherDAO.avgFilterNormHQL(normScoreVal, normScoreSelectVal, districtMaster,jobOrdersByJobStatuses,statusflag);
						Iterator iter = avgNormScore.iterator();
						if(avgNormScore.size()>0)
						{
						  while(iter.hasNext()){
							  Object[] obj = (Object[]) iter.next();
							  jobIDsByAvgNormScore.add((Integer)obj[0]);
					  	  }
						//}
						}
					 }
					 List<Integer> nbyAJobIds= new ArrayList<Integer>();
					if(normScoreflag==true && jobIdsflag==false){
						finaljobIDS.addAll(jobIDsByAvgNormScore);
						finaljobIdsflag=true;
					}else if(normScoreflag==false && jobIdsflag==true){
						finaljobIDS.addAll(jobIDS);
						finaljobIdsflag=true;
					}else if(normScoreflag==true && jobIdsflag==true){
						jobIDS.retainAll(jobIDsByAvgNormScore);
						finaljobIDS.addAll(jobIDS);
						finaljobIdsflag=true;
					}
					
					//New Change 
					List<Integer> statusFilterJobIds= new  ArrayList<Integer>();
					boolean onlyStatus=false;
					if(normScoreflag==false && statusflag==true){
						onlyStatus=true;
						statusFilterJobIds = jobForTeacherDAO.findJobIdsbyJFTID(jobOrdersByJobStatuses);
					}
					if(onlyStatus==true && finaljobIdsflag==true){
						finaljobIDS.retainAll(statusFilterJobIds);
						
					}else if(onlyStatus==true && finaljobIdsflag==false){
						finaljobIDS.addAll(statusFilterJobIds);
						finaljobIdsflag=true;
					}
					
					
					/*if(finaljobIdsflag==true && statusflag==true){
						finaljobIDS.retainAll(jobOrdersByJobStatuses);
						finaljobIdsflag=true;
					}
					else if(finaljobIdsflag==false && statusflag==true){
						finaljobIDS.addAll(jobOrdersByJobStatuses);
						finaljobIdsflag=true;
					}*/
				
					//SchoolId Filter
				    boolean schoolFlag=false;
					if(schoolId!=0)
					{
						System.out.println("schoolId schoolId "+schoolId);
					  schoolFlag=true;
					  SchoolMaster  sclMaster=null;
					  sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
					  lstSchoolJobOrderIds = schoolInJobOrderDAO.findAllJobOrderBySchool(sclMaster);
					}	
					
					if(finaljobIdsflag==true && schoolFlag==true){
						finaljobIDS.retainAll(lstSchoolJobOrderIds);
						finaljobIdsflag=true;
					}
					else if(finaljobIdsflag==false && schoolFlag==true){
						finaljobIDS.addAll(lstSchoolJobOrderIds);
						finaljobIdsflag=true;
					}
					
					List<JobOrder> criteriaMixJobOrders=new ArrayList<JobOrder>();
					if(entityID==2 || entityID==1)
					{
					   if(finaljobIdsflag==true && finaljobIDS!=null &&  finaljobIDS.size()==0){
						  lstJobOrders=new ArrayList<JobOrder>();
						}
					  else
					    {
						  criteriaMixJobOrders = jobOrderDAO.findJobOrderbyWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start,noOfRowInPage,endfromDate,endtoDate,sfromDate, stoDate,finaljobIDS,nbyAflag,nbyAJobIds,exportcheck);						
						  totalRecords = jobOrderDAO.findTotalJobOrderbyWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start,noOfRowInPage,endfromDate,endtoDate,sfromDate, stoDate,finaljobIDS,nbyAflag,nbyAJobIds);						
						  
						  if(criteriaMixJobOrders!=null && criteriaMixJobOrders.size()>0)
						    {
						      if(checkflag){
		   						lstJobOrders.retainAll(criteriaMixJobOrders);
		   					  }else{
		   						lstJobOrders.addAll(criteriaMixJobOrders);
		   						checkflag=true;
		   					  }
						    }else{
						    	 lstJobOrders=new ArrayList<JobOrder>();
						    }
						 }
					 }
					
				List<DistrictMaster> districtMasters = new ArrayList<DistrictMaster>();
				for(JobOrder job : lstJobOrders){
					districtMasters.add(job.getDistrictMaster());
				}
					
				Map<Integer, Map<String,Integer>> mapsscountwithJob= new HashMap<Integer, Map<String,Integer>>();
				Map<Integer,String> mapSpecificStatus = new HashMap<Integer, String>();
				List<SecondaryStatus> secondaryStatusList = new ArrayList<SecondaryStatus>();
				
				String []statusArray={"scomp" , "ecomp" , "vcomp"};
			    List<StatusMaster> lstStatusMasters = Utility.getStaticMasters(statusArray);
			    if(districtMasters!=null && districtMasters.size()>0)
			     secondaryStatusList= secondaryStatusDAO.findSecondaryStatusforSpecificStatusListBYDistricts(districtMasters,lstStatusMasters);
			    if(secondaryStatusList!=null && secondaryStatusList.size()>0)
			    for(SecondaryStatus ss : secondaryStatusList)
			     {
			    	mapSpecificStatus.put(ss.getStatusMaster().getStatusId(), ss.getSecondaryStatusName());
			     }
					
				List list = new ArrayList();	
				if(lstJobOrders!=null && lstJobOrders.size()>0)
				{
				   list= jobForTeacherDAO.fintApplicntsStatusByJob(lstJobOrders);
				}
				 Iterator itr = list.iterator();
					if(list.size()>0){
						while(itr.hasNext()){
							Object[] obj = (Object[]) itr.next();
							JobOrder jobOrder=(JobOrder)obj[0];
							
							SecondaryStatus secondaryStatus= null;
							StatusMaster statusMaster= null;
							if(obj[1]!=null)
								secondaryStatus=(SecondaryStatus) obj[1];
							
							statusMaster=(StatusMaster) obj[2];
							Integer secCount= (Integer) obj[3];
							Integer statusCount = (Integer) obj[4];
							
							if(secondaryStatus!=null && statusMaster==null){
								Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
								if(countValue==null){
									Map<String,Integer> tempcountVal = new TreeMap<String, Integer>();
									tempcountVal.put(secondaryStatus.getSecondaryStatusName(), secCount);
									mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
								}else{
									if(countValue.get(secondaryStatus.getSecondaryStatusName())!=null){
										secCount=countValue.get(secondaryStatus.getSecondaryStatusName())+secCount;
									}
									countValue.put(secondaryStatus.getSecondaryStatusName(), secCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}
							}
							
							if(secondaryStatus==null && statusMaster!=null){
								String sDidplayStatusName="";
								if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
									sDidplayStatusName="Available"; 
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
									sDidplayStatusName=statusMaster.getStatus();
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
									sDidplayStatusName=statusMaster.getStatus();
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
									sDidplayStatusName="Timed Out"; 
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
									sDidplayStatusName="Declined"; 
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
									sDidplayStatusName="Withdrew"; 
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
									sDidplayStatusName="Hired";
								
								else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
							    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							    }
									 
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
									sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
								}
								  
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
									 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
								 }
								
								Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
								if(countValue==null){
									Map<String,Integer> tempcountVal = new TreeMap<String, Integer>();
									tempcountVal.put(sDidplayStatusName, statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
								}else{
									if(countValue.get(sDidplayStatusName)!=null){
										countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
										mapsscountwithJob.put(jobOrder.getJobId(), countValue);
									}else{
										countValue.put(sDidplayStatusName,statusCount);
										mapsscountwithJob.put(jobOrder.getJobId(), countValue);
									}
								}
							}
							if(secondaryStatus!=null && statusMaster!=null){
								String sDidplayStatusName="";
								if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
									sDidplayStatusName="Available"; 
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
									sDidplayStatusName=statusMaster.getStatus();
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
									sDidplayStatusName=statusMaster.getStatus();
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
									sDidplayStatusName="Timed Out"; 
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
									sDidplayStatusName="Declined"; 
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
									sDidplayStatusName="Withdrew"; 
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
									sDidplayStatusName="Hired";
								else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
							    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							    }
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
									sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
								}
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
									 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
								}
								Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
								if(countValue==null){
									Map<String,Integer> tempcountVal = new TreeMap<String, Integer>();
									tempcountVal.put(sDidplayStatusName, statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
								}else{
									if(countValue.get(sDidplayStatusName)!=null){
										countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
										mapsscountwithJob.put(jobOrder.getJobId(), countValue);
									}else{
										countValue.put(sDidplayStatusName,statusCount);
										mapsscountwithJob.put(jobOrder.getJobId(), countValue);
									}
								}
							}
						}
					}
				List avgListbyJobList=new ArrayList();
				Map<Integer,Double> mapAvgEPI = new HashMap<Integer, Double>();
				if(lstJobOrders!=null && lstJobOrders.size()>0)
				{
				  avgListbyJobList= jobForTeacherDAO.countCandidatesNormHQLJobListWise(lstJobOrders);
				}
				List<Integer> statusIds = new ArrayList<Integer>();
				List<Integer> secstatusIds = new ArrayList<Integer>();
				//Map<jobId,Map<StatusName,avgNormScore>>
				Map<Integer, Map<String,Double>> mapAvgNormScoreStatusAndJob = new HashMap<Integer, Map<String,Double>>();
				for(Object objLst : avgListbyJobList)
                {
                    Object[] obj = (Object[]) objLst;
                    int ds = Utility.getIntValue(String.valueOf(obj[4]));
                    int dss = Utility.getIntValue(String.valueOf(obj[3]));
                    if(ds!=0 && dss==0)
                    	statusIds.add(ds);
                    if(ds==0 && dss!=0)
                    	secstatusIds.add(dss);
                    if(ds!=0 && dss!=0)
                    	statusIds.add(ds);
                    
                }
				List<StatusMaster> statusMasters =new ArrayList<StatusMaster>();
				List<SecondaryStatus> secondaryStatus = new ArrayList<SecondaryStatus>();
				Map<Integer,StatusMaster> mapStatusMaster = new HashMap<Integer, StatusMaster>();
				Map<Integer,SecondaryStatus> mapSecondaryStatus =  new HashMap<Integer, SecondaryStatus>();
				
				if(statusIds!=null && statusIds.size()>0)
				{
					Criterion criterion = Restrictions.in("statusId", statusIds);
					statusMasters = statusMasterDAO.findByCriteria(criterion);
				}
				if(statusMasters!=null && statusMasters.size()>0){
					for(StatusMaster sm :statusMasters){
						mapStatusMaster.put(sm.getStatusId(), sm);
					}
				}
					
				if(secstatusIds!=null && secstatusIds.size()>0)
				{
					Criterion criterion = Restrictions.in("secondaryStatusId", secstatusIds);
					secondaryStatus = secondaryStatusDAO.findByCriteria(criterion);
				}
				if(secondaryStatus!=null && secondaryStatus.size()>0){
					for(SecondaryStatus ssm :secondaryStatus){
						mapSecondaryStatus.put(ssm.getSecondaryStatusId(), ssm);
					}
				}	
				for(Object objLst:avgListbyJobList)
                {
                    float avg;
                    Object[] obj = (Object[]) objLst;
                    int normSum=Integer.parseInt(String.valueOf(obj[1]));
                    int total =Integer.parseInt(String.valueOf(obj[2]));
                    avg  =(float)normSum/(float)total;
                    int jobid = Utility.getIntValue(String.valueOf(obj[0]));
                   
                    int ds = Utility.getIntValue(String.valueOf(obj[4]));
                    int dss = Utility.getIntValue(String.valueOf(obj[3]));
                    StatusMaster pojoS = new StatusMaster();
                    SecondaryStatus pojoSS = new SecondaryStatus();
                    if(ds!=0 && dss==0)
                    {
                    	pojoS=mapStatusMaster.get((Integer)ds);
                    	String sDidplayStatusName="";
						if(pojoS.getStatusShortName().equalsIgnoreCase("comp")){
							sDidplayStatusName="Available"; 
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("rem")){
							sDidplayStatusName=pojoS.getStatus();
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("icomp")){
							sDidplayStatusName=pojoS.getStatus();
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("vlt")){
							sDidplayStatusName="Timed Out"; 
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("dcln"))
							sDidplayStatusName="Declined"; 
						else if(pojoS.getStatusShortName().equalsIgnoreCase("widrw"))
							sDidplayStatusName="Withdrew"; 
						else if(pojoS.getStatusShortName().equalsIgnoreCase("hird"))
							sDidplayStatusName="Hired";
						
						else  if(pojoS.getStatusShortName().equalsIgnoreCase("scomp")){
					    	sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
					    }
						else if(pojoS.getStatusShortName().equalsIgnoreCase("ecomp")){
							sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
						}
						else if(pojoS.getStatusShortName().equalsIgnoreCase("vcomp")){
							 sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
						 }
						
						Map<String,Double> countValue = mapAvgNormScoreStatusAndJob.get(jobid);
						if(countValue==null){
							Map<String,Double> tempcountVal = new TreeMap<String, Double>();
							tempcountVal.put(sDidplayStatusName, ((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, tempcountVal);
						}else{
							countValue.put(sDidplayStatusName,((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, countValue);
							}
                    }
                    if(ds==0 && dss!=0)
                    {
                    	pojoSS=mapSecondaryStatus.get((Integer)dss);
                    	Map<String,Double> countValue = mapAvgNormScoreStatusAndJob.get(jobid);
						if(countValue==null){
							Map<String,Double> tempcountVal = new TreeMap<String, Double>();
							tempcountVal.put(pojoSS.getSecondaryStatusName(), ((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, tempcountVal);
						}else{
							countValue.put(pojoSS.getSecondaryStatusName(), ((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, countValue);
						}
                    }
                    if(ds!=0 && dss!=0)
                    {
                    	pojoS=mapStatusMaster.get((Integer)ds);
                    	String sDidplayStatusName="";
						if(pojoS.getStatusShortName().equalsIgnoreCase("comp")){
							sDidplayStatusName="Available"; 
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("rem")){
							sDidplayStatusName=pojoS.getStatus();
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("icomp")){
							sDidplayStatusName=pojoS.getStatus();
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("vlt")){
							sDidplayStatusName="Timed Out"; 
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("dcln"))
							sDidplayStatusName="Declined"; 
						else if(pojoS.getStatusShortName().equalsIgnoreCase("widrw"))
							sDidplayStatusName="Withdrew"; 
						else if(pojoS.getStatusShortName().equalsIgnoreCase("hird"))
							sDidplayStatusName="Hired";
						else  if(pojoS.getStatusShortName().equalsIgnoreCase("scomp")){
					    	sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
					    }
						else if(pojoS.getStatusShortName().equalsIgnoreCase("ecomp")){
							sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
						}
						else if(pojoS.getStatusShortName().equalsIgnoreCase("vcomp")){
							 sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
						 }
						
						Map<String,Double> countValue = mapAvgNormScoreStatusAndJob.get(jobid);
						if(countValue==null){
							Map<String,Double> tempcountVal = new TreeMap<String, Double>();
							tempcountVal.put(sDidplayStatusName, ((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, tempcountVal);
						}else{
							countValue.put(sDidplayStatusName,((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, countValue);
							}
                    }
                }

				totalRecord =totalRecords;
				if(totalRecord<end)
					end=totalRecord;
				
				//finalJorJobOrders	=	lstJobOrders.subList(start,end);
				
				String responseText="";
				
				tmRecords.append("<table  id='tblGridJobList' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDistrictName", locale),sortOrderNoField,"districtName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobId", locale),sortOrderNoField,"jobId",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderNoField,"jobTitle",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblPostingDate", locale),sortOrderNoField,"jobStartDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgExpirationDate", locale),sortOrderNoField,"jobEndDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgPositions", locale)+" </th>");
				
				tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblCandStatus", locale)+" </th>");
				
				tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgNumberofApplications", locale)+" </th>");
				
				tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblAvNoSco", locale)+" </th>");
				
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				tmRecords.append("<tbody>");
				if(lstJobOrders.size()==0){
				 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
				}
			
				Map<String, String> positionMap = getPositionsNumber(lstJobOrders);
				if(lstJobOrders.size()>0){
				 for(JobOrder job:lstJobOrders) 
				 {
				   tmRecords.append("<tr>");	
				   
				    tmRecords.append("<td>"+job.getDistrictMaster().getDistrictName()+"</td>");
					tmRecords.append("<td>"+job.getJobId()+"</td>");
					tmRecords.append("<td>"+job.getJobTitle()+"</td>");
					tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobStartDate())+",12:01 AM</td>");
					
					if(Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobEndDate()).equals("Dec 25, 2099"))
				      tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblUntilfilled", locale)+"</td>");
					else
					  tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobEndDate())+",11:59 PM</td>");
					 
					tmRecords.append("<td>"+(positionMap.get(""+job.getJobId()))+"</td>");
					//tmRecords.append("<td>"+job.getNoOfHires()+"</td>");
					
					//"+Utility.getLocaleValuePropByKey("lblCandStatus", locale)+" vs Number of Candidate 
					Map<String, Integer> mapsecoundryStatus = mapsscountwithJob.get(job.getJobId());
					 if(mapsecoundryStatus==null){
					   tmRecords.append("<td>-</td>");
					   tmRecords.append("<td>-</td>");
					 }else{
						  if(statusflag &&  statusNames!=null && !ArrayUtils.contains(statusNames,"0") ){
						    tmRecords.append("<td>");
							for(Map.Entry<String,Integer> entry : mapsecoundryStatus.entrySet())
							{
								tmRecords.append("<div>");
								if(ArrayUtils.contains(statusNames,entry.getKey()))
								   tmRecords.append(entry.getKey());
								 tmRecords.append("</div>");
								
							}
						  tmRecords.append("</td>");
						  tmRecords.append("<td>");
							for(Map.Entry<String,Integer> entry : mapsecoundryStatus.entrySet())
							{
							 tmRecords.append("<div>");
							 if(ArrayUtils.contains(statusNames,entry.getKey()))
								 tmRecords.append(entry.getValue());
							 tmRecords.append("</div>");
							}
						  tmRecords.append("</td>");
						 }else{
							 tmRecords.append("<td>");
								for(Map.Entry<String,Integer> entry : mapsecoundryStatus.entrySet())
								{
									tmRecords.append("<div>");
									   tmRecords.append(entry.getKey());
									 tmRecords.append("</div>");
									
								}
							  tmRecords.append("</td>");
							  tmRecords.append("<td>");
								for(Map.Entry<String,Integer> entry : mapsecoundryStatus.entrySet())
								{
								 tmRecords.append("<div>");
									 tmRecords.append(entry.getValue());
								 tmRecords.append("</div>");
								}
							  tmRecords.append("</td>");
							 
						 }
					}
				//for Average Norm Score 
						Map<String, Double> mapTempAvg = mapAvgNormScoreStatusAndJob.get(job.getJobId());
						if(mapTempAvg!=null && mapTempAvg.size()>0){
							tmRecords.append("<td>");
						   for(Map.Entry<String,Double> entry : mapTempAvg.entrySet()){
							   //if(entry.getValue()>)
							   tmRecords.append(entry.getKey()+ "&nbsp-&nbsp"+ entry.getValue()+"</br>");
						   }
						   tmRecords.append("</td>");
						}else{
							tmRecords.append("<td>-</td>");
						}
				   tmRecords.append("</tr>");
			   }
			}
				try{
					try{
						mapStatusMaster.clear();//map
						mapStatusMaster=null;
					}catch (Exception e) {}
					try {
						mapSecondaryStatus.clear();//map
						mapSecondaryStatus=null;
					} catch (Exception e) {}
					try {
						statusMasters.clear();//list
						statusMasters=null;
					} catch (Exception e) {}
					try {
						statusMasters.clear();//list
						statusMasters=null;
					} catch (Exception e) {}	
					try {
						secondaryStatus.clear();//list
						secondaryStatus=null;
					} catch (Exception e) {}	
					try {
						mapAvgNormScoreStatusAndJob .clear();//map
						mapAvgNormScoreStatusAndJob =null;
					} catch (Exception e) {}	
					try {
						secstatusIds.clear();//list
						secstatusIds=null;
					} catch (Exception e) {}	
					try {
						statusIds.clear();//list
						statusIds=null;
					} catch (Exception e) {}	
					try {
						mapAvgEPI.clear();//map
						mapAvgEPI=null;
					} catch (Exception e) {}	
					try {
						avgListbyJobList.clear();//list
						avgListbyJobList=null;
					} catch (Exception e) {}	
					try {
						lstStatusMasters.clear();//list
						lstStatusMasters=null;
					} catch (Exception e) {}	
					try {
						secondaryStatusList.clear();//list
						secondaryStatusList=null;
					} catch (Exception e) {}	
					try {
						mapSpecificStatus.clear();//map
						mapSpecificStatus=null;
					} catch (Exception e) {}	
					try {
						mapsscountwithJob.clear();//map
						mapsscountwithJob=null;
					} catch (Exception e) {}	
					try {
						districtMasters.clear();//list
						districtMasters=null;
					} catch (Exception e) {}	
					try {
						criteriaMixJobOrders.clear();//list
						criteriaMixJobOrders=null;
					} catch (Exception e) {}	
					try {
						statusFilterJobIds.clear();//list
						statusFilterJobIds=null;
					} catch (Exception e) {}
					try {
						lstSecondaryStatus.clear();//list
						lstSecondaryStatus=null;
					} catch (Exception e) {}
					try {
						jobOrdersByJobStatuses.clear();  //list
						jobOrdersByJobStatuses=null;
					} catch (Exception e) {}
					try {
						lstJobOrders.clear();//list
						lstJobOrders=null;
					} catch (Exception e) {}
					try {
						lstSchoolJobOrderIds.clear();//list
						lstSchoolJobOrderIds=null;
					} catch (Exception e) {}
					try {
						jobIDS.clear();//list
						jobIDS=null;
					} catch (Exception e) {}
					try {
						jobIDsByAvgNormScore.clear();//list
						jobIDsByAvgNormScore=null;
					} catch (Exception e) {}
					try {
						finaljobIDS.clear();//list
						finaljobIDS=null;
					} catch (Exception e) {}
					try {
						statusMaster1st.clear();//list
						statusMaster1st=null;
					} catch (Exception e) {}
			}catch(Exception ex){
				ex.printStackTrace();
			}
			
			tmRecords.append("</tbody>");
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));

			}catch(Exception e){
				e.printStackTrace();
			}
			
		 return tmRecords.toString();
	  }
		
	public String jobListExportEXL(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String endfromDate, String endtoDate,String sfromDate, String stoDate,String jobOrderId,String normScoreSelectVal,String normScoreVal,String statusId,boolean exportcheck)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		int sortingcheck=0;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="jobId";
				String sortOrderNoField="jobId";

				if(sortOrder.equalsIgnoreCase("districtName"))
					sortingcheck=1;
				
				if(sortOrder.equalsIgnoreCase("normScore"))
					sortingcheck=2;
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<JobOrder> lstJobOrders =new ArrayList<JobOrder>();
				List<Integer> lstSchoolJobOrderIds = new ArrayList<Integer>();
				boolean checkflag = false;
				List<Integer> jobIDS= new ArrayList<Integer>();
				List<Integer> jobIDsByAvgNormScore= new ArrayList<Integer>();
				List<Integer> finaljobIDS= new ArrayList<Integer>();
				boolean jobIdsflag=false;
				boolean normScoreflag=false;
				boolean finaljobIdsflag=false;
				boolean statusflag=false;
				
				if(districtOrSchoolId!=0 && entityID==1)
					  districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
				
				//******************************Job Status***************************************
//filter of job status 
				String statusNames[]=null;
				List<Integer> jobOrdersByJobStatuses = new ArrayList<Integer>();
				List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
				 if(!statusId.equals("") && !statusId.equals("0")){
					 System.out.println(" Filter StatusId Start ::  "+statusId);
					 statusNames = statusId.split("#@");
				      if(!ArrayUtils.contains(statusNames,"0"))
				      {	 
				    	 statusflag=true;
						 lstSecondaryStatus = secondaryStatusDAO.findByMultipleStatusName(districtMaster, statusNames);
				
						  List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
						  Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
						  for (StatusMaster statusMaster1 : statusMasterList) {
							  mapStatus.put(statusMaster1.getStatusShortName(),statusMaster1);
						  }
						  StatusMaster stMaster =null;
						  List <StatusMaster> statusMaster1st=new ArrayList<StatusMaster>();
						  
						  if(ArrayUtils.contains(statusNames,"Available")){
								  stMaster = mapStatus.get("comp");
								  statusMaster1st.add(stMaster);
						  }
						  if(ArrayUtils.contains(statusNames,"Rejected")){
							    stMaster = mapStatus.get("rem");
							    statusMaster1st.add(stMaster);
					     }
						  if(ArrayUtils.contains(statusNames,"Timed Out")){
							    stMaster = mapStatus.get("vlt");
							    statusMaster1st.add(stMaster);
					     }
						 if(ArrayUtils.contains(statusNames,"Withdrew")){
							    stMaster = mapStatus.get("widrw");
							    statusMaster1st.add(stMaster);
					     }
						 if(ArrayUtils.contains(statusNames,"Incomplete")){
							    stMaster = mapStatus.get("icomp");
							    statusMaster1st.add(stMaster);
					     }
						 if(ArrayUtils.contains(statusNames,"Hired")){
							    stMaster = mapStatus.get("hird");
							    statusMaster1st.add(stMaster);
					     }   
						 for(SecondaryStatus ss: lstSecondaryStatus)
						  {
							 if(ss.getStatusMaster()!=null)
								 statusMaster1st.add(ss.getStatusMaster()); 
						  }
						 jobOrdersByJobStatuses = jobForTeacherDAO.findJobOrderByCandidtesJobStatus(districtMaster,statusMaster1st,lstSecondaryStatus);
			         }
				 }
		//################################################################################
			// Job ID Filter
				if(!jobOrderId.equals(""))
				{
					jobIdsflag=true;
					String strjobIds[]=jobOrderId.split(",");
					if(strjobIds.length>0)
				     for(String str : strjobIds){
				    	 if(!str.equals(""))
				    	 jobIDS.add(Integer.parseInt(str));
				     }
				}
				
			// Average Norm Score Filter	
				boolean nbyAflag=false;
				if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") && !normScoreSelectVal.equals("6"))
				 {
					normScoreflag=true;
					List avgNormScore=  jobForTeacherDAO.avgFilterNormHQL(normScoreVal, normScoreSelectVal, districtMaster,jobOrdersByJobStatuses,statusflag);
					Iterator iter = avgNormScore.iterator();
					if(avgNormScore.size()>0)
					{
					  while(iter.hasNext()){
						  Object[] obj = (Object[]) iter.next();
						  jobIDsByAvgNormScore.add((Integer)obj[0]);
				  	  }
					}
				 }
				 List<Integer> nbyAJobIds= new ArrayList<Integer>();
				
				if(normScoreflag==true && jobIdsflag==false){
					finaljobIDS.addAll(jobIDsByAvgNormScore);
					finaljobIdsflag=true;
				}else if(normScoreflag==false && jobIdsflag==true){
					finaljobIDS.addAll(jobIDS);
					finaljobIdsflag=true;
				}else if(normScoreflag==true && jobIdsflag==true){
					jobIDS.retainAll(jobIDsByAvgNormScore);
					finaljobIDS.addAll(jobIDS);
					finaljobIdsflag=true;
				}
				
				//New Change 
				List<Integer> statusFilterJobIds= new  ArrayList<Integer>();
				boolean onlyStatus=false;
				if(normScoreflag==false && statusflag==true){
					onlyStatus=true;
					statusFilterJobIds = jobForTeacherDAO.findJobIdsbyJFTID(jobOrdersByJobStatuses);
				}
				if(onlyStatus==true && finaljobIdsflag==true){
					finaljobIDS.retainAll(statusFilterJobIds);
					
				}else if(onlyStatus==true && finaljobIdsflag==false){
					finaljobIDS.addAll(statusFilterJobIds);
					finaljobIdsflag=true;
				}
				
			/*	if(finaljobIdsflag==true && statusflag==true){
					finaljobIDS.retainAll(jobOrdersByJobStatuses);
					finaljobIdsflag=true;
				}
				else if(finaljobIdsflag==false && statusflag==true){
					finaljobIDS.addAll(jobOrdersByJobStatuses);
					finaljobIdsflag=true;
				}*/
			
				//SchoolId Filter
			    boolean schoolFlag=false;
				if(schoolId!=0)
				{
				  schoolFlag=true;
				  SchoolMaster  sclMaster=null;
				  sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
				  lstSchoolJobOrderIds = schoolInJobOrderDAO.findAllJobOrderBySchool(sclMaster);
				}	
				
				if(finaljobIdsflag==true && schoolFlag==true){
					finaljobIDS.retainAll(lstSchoolJobOrderIds);
					finaljobIdsflag=true;
				}
				else if(finaljobIdsflag==false && schoolFlag==true){
					finaljobIDS.addAll(lstSchoolJobOrderIds);
					finaljobIdsflag=true;
				}
				
				List<JobOrder> criteriaMixJobOrders=new ArrayList<JobOrder>();
				if(entityID==2 || entityID==1)
				{
				   if(finaljobIdsflag==true && finaljobIDS!=null &&  finaljobIDS.size()==0){
					  lstJobOrders=new ArrayList<JobOrder>();
					}
				  else
				    {
					  criteriaMixJobOrders = jobOrderDAO.findJobOrderbyWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start,noOfRowInPage,endfromDate,endtoDate,sfromDate, stoDate,finaljobIDS,nbyAflag,nbyAJobIds,exportcheck);						
					  
					  if(criteriaMixJobOrders!=null && criteriaMixJobOrders.size()>0)
					    {
					      if(checkflag){
	   						lstJobOrders.retainAll(criteriaMixJobOrders);
	   					  }else{
	   						lstJobOrders.addAll(criteriaMixJobOrders);
	   						checkflag=true;
	   					  }
					    }else{
					    	 lstJobOrders=new ArrayList<JobOrder>();
					    }
					 }
				 }
				
			List<DistrictMaster> districtMasters = new ArrayList<DistrictMaster>();
			for(JobOrder job : lstJobOrders){
				districtMasters.add(job.getDistrictMaster());
			}
				
			Map<Integer, Map<String,String>> mapsscountwithJob= new HashMap<Integer, Map<String,String>>();
			Map<Integer,String> mapSpecificStatus = new HashMap<Integer, String>();
			List<SecondaryStatus> secondaryStatusList = new ArrayList<SecondaryStatus>();
			
			String []statusArray={"scomp" , "ecomp" , "vcomp"};
		    List<StatusMaster> lstStatusMasters = Utility.getStaticMasters(statusArray);
		    if(districtMasters!=null && districtMasters.size()>0)
		     secondaryStatusList= secondaryStatusDAO.findSecondaryStatusforSpecificStatusListBYDistricts(districtMasters,lstStatusMasters);
		    if(secondaryStatusList!=null && secondaryStatusList.size()>0)
		    for(SecondaryStatus ss : secondaryStatusList)
		     {
		    	mapSpecificStatus.put(ss.getStatusMaster().getStatusId(), ss.getSecondaryStatusName());
		     }
				
			List list = new ArrayList();	
			if(lstJobOrders!=null && lstJobOrders.size()>0)
			{
			   list= jobForTeacherDAO.fintApplicntsStatusByJob(lstJobOrders);
			}
			 Iterator itr = list.iterator();
				if(list.size()>0){
					while(itr.hasNext()){
						Object[] obj = (Object[]) itr.next();
						JobOrder jobOrder=(JobOrder)obj[0];
						
						SecondaryStatus secondaryStatus= null;
						StatusMaster statusMaster= null;
						if(obj[1]!=null)
							secondaryStatus=(SecondaryStatus) obj[1];
						
						statusMaster=(StatusMaster) obj[2];
						Integer secCount= (Integer) obj[3];
						Integer statusCount = (Integer) obj[4];
						
						
						if(secondaryStatus!=null && statusMaster==null){
							Map<String,String> countValue = mapsscountwithJob.get(jobOrder.getJobId());
							if(countValue==null){
								Map<String,String> tempcountVal = new TreeMap<String, String>();
								tempcountVal.put(secondaryStatus.getSecondaryStatusName(), secCount+"");
								mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
							}else{
								if(countValue.get(secondaryStatus.getSecondaryStatusName())!=null){
									secCount = Integer.parseInt(countValue.get(secondaryStatus.getSecondaryStatusName()))+secCount;
								}
								countValue.put(secondaryStatus.getSecondaryStatusName(), secCount+"");
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							}
						}
						
						if(secondaryStatus==null && statusMaster!=null){
							String sDidplayStatusName="";
							if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
								sDidplayStatusName="Available"; 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
								sDidplayStatusName="Timed Out"; 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
								sDidplayStatusName="Declined"; 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
								sDidplayStatusName="Withdrew"; 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
								sDidplayStatusName="Hired";
							
							else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
						    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						    }
								 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
								sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							}
							  
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
								 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							 }
							
							Map<String,String> countValue = mapsscountwithJob.get(jobOrder.getJobId());
							if(countValue==null){
								Map<String,String> tempcountVal = new TreeMap<String, String>();
								tempcountVal.put(sDidplayStatusName, statusCount+"");
								mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
							}else{
								if(countValue.get(sDidplayStatusName)!=null){
									countValue.put(sDidplayStatusName,(Integer.parseInt(countValue.get(sDidplayStatusName))+statusCount)+"");
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}else{
									countValue.put(sDidplayStatusName,statusCount+"");
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}
							}
						}
						if(secondaryStatus!=null && statusMaster!=null){
							String sDidplayStatusName="";
							if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
								sDidplayStatusName="Available"; 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
								sDidplayStatusName="Timed Out"; 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
								sDidplayStatusName="Declined"; 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
								sDidplayStatusName="Withdrew"; 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
								sDidplayStatusName="Hired";
							else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
						    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						    }
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
								sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							}
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
								 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							}
							Map<String,String> countValue = mapsscountwithJob.get(jobOrder.getJobId());
							if(countValue==null){
								Map<String,String> tempcountVal = new TreeMap<String, String>();
								tempcountVal.put(sDidplayStatusName, statusCount+"");
								mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
							}else{
								if(countValue.get(sDidplayStatusName)!=null){
									countValue.put(sDidplayStatusName,(Integer.parseInt(countValue.get(sDidplayStatusName))+statusCount)+"");
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}else{
									countValue.put(sDidplayStatusName,statusCount+"");
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}
							}
						}
					}
				}
			List avgListbyJobList=new ArrayList();
			Map<Integer,Double> mapAvgEPI = new HashMap<Integer, Double>();
			if(lstJobOrders!=null && lstJobOrders.size()>0)
			{
			  avgListbyJobList= jobForTeacherDAO.countCandidatesNormHQLJobListWise(lstJobOrders);
			}
			
			List<Integer> statusIds = new ArrayList<Integer>();
			List<Integer> secstatusIds = new ArrayList<Integer>();
			//Map<jobId,Map<StatusName,avgNormScore>>
			Map<Integer, Map<String,Double>> mapAvgNormScoreStatusAndJob = new HashMap<Integer, Map<String,Double>>();
			for(Object objLst : avgListbyJobList)
            {
                Object[] obj = (Object[]) objLst;
                int ds = Utility.getIntValue(String.valueOf(obj[4]));
                int dss = Utility.getIntValue(String.valueOf(obj[3]));
                if(ds!=0 && dss==0)
                	statusIds.add(ds);
                if(ds==0 && dss!=0)
                	secstatusIds.add(dss);
                if(ds!=0 && dss!=0)
                	statusIds.add(ds);
                
            }
			List<StatusMaster> statusMasters =new ArrayList<StatusMaster>();
			List<SecondaryStatus> secondaryStatus = new ArrayList<SecondaryStatus>();
			Map<Integer,StatusMaster> mapStatusMaster = new HashMap<Integer, StatusMaster>();
			Map<Integer,SecondaryStatus> mapSecondaryStatus =  new HashMap<Integer, SecondaryStatus>();
			
			if(statusIds!=null && statusIds.size()>0)
			{
				Criterion criterion = Restrictions.in("statusId", statusIds);
				statusMasters = statusMasterDAO.findByCriteria(criterion);
			}
			if(statusMasters!=null && statusMasters.size()>0){
				for(StatusMaster sm :statusMasters){
					mapStatusMaster.put(sm.getStatusId(), sm);
				}
			}
				
			if(secstatusIds!=null && secstatusIds.size()>0)
			{
				Criterion criterion = Restrictions.in("secondaryStatusId", secstatusIds);
				secondaryStatus = secondaryStatusDAO.findByCriteria(criterion);
			}
			if(secondaryStatus!=null && secondaryStatus.size()>0){
				for(SecondaryStatus ssm :secondaryStatus){
					mapSecondaryStatus.put(ssm.getSecondaryStatusId(), ssm);
				}
			}
			
			try {
				for(Object objLst:avgListbyJobList)
				{
				    float avg;
				    Object[] obj = (Object[]) objLst;
				    int normSum=Integer.parseInt(String.valueOf(obj[1]));
				    int total =Integer.parseInt(String.valueOf(obj[2]));
				    avg  =(float)normSum/(float)total;
				    int jobid = Utility.getIntValue(String.valueOf(obj[0]));
				   
				    int ds = Utility.getIntValue(String.valueOf(obj[4]));
				    int dss = Utility.getIntValue(String.valueOf(obj[3]));
				    StatusMaster pojoS = new StatusMaster();
				    SecondaryStatus pojoSS = new SecondaryStatus();
				    if(ds!=0 && dss==0)
				    {
				    	pojoS=mapStatusMaster.get((Integer)ds);
				    	String sDidplayStatusName="";
						if(pojoS.getStatusShortName().equalsIgnoreCase("comp")){
							sDidplayStatusName="Available"; 
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("rem")){
							sDidplayStatusName=pojoS.getStatus();
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("icomp")){
							sDidplayStatusName=pojoS.getStatus();
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("vlt")){
							sDidplayStatusName="Timed Out"; 
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("dcln"))
							sDidplayStatusName="Declined"; 
						else if(pojoS.getStatusShortName().equalsIgnoreCase("widrw"))
							sDidplayStatusName="Withdrew"; 
						else if(pojoS.getStatusShortName().equalsIgnoreCase("hird"))
							sDidplayStatusName="Hired";
						
						else  if(pojoS.getStatusShortName().equalsIgnoreCase("scomp")){
					    	sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
					    }
						else if(pojoS.getStatusShortName().equalsIgnoreCase("ecomp")){
							sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
						}
						else if(pojoS.getStatusShortName().equalsIgnoreCase("vcomp")){
							 sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
						 }
						/*Map<String,String> countValue = mapsscountwithJob.get(jobOrder.getJobId());
						if(countValue==null){
							Map<String,String> tempcountVal = new TreeMap<String, String>();
							tempcountVal.put(sDidplayStatusName, statusCount+"");
							mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
						}else{
							if(countValue.get(sDidplayStatusName)!=null){
								countValue.put(sDidplayStatusName,(Integer.parseInt(countValue.get(sDidplayStatusName))+statusCount)+"");
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							}else{
								countValue.put(sDidplayStatusName,statusCount+"");
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							}
						}*/
						
						Map<String,String> countValue = mapsscountwithJob.get(jobid);
						if(countValue==null){
							/*Map<String,Double> tempcountVal = new TreeMap<String, Double>();
							tempcountVal.put(sDidplayStatusName, ((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, tempcountVal);*/
						}else{
							 if(countValue.get(sDidplayStatusName)!=null){
								   countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+"@@@"+((double) Math.round(avg * 100) / 100));
								   mapsscountwithJob.put(jobid, countValue);
							 }
							}
				    }
				    if(ds==0 && dss!=0)
				    {
				    	pojoSS=mapSecondaryStatus.get((Integer)dss);
				    	//Map<String,Double> countValue = mapAvgNormScoreStatusAndJob.get(jobid);
				    	Map<String,String> countValue = mapsscountwithJob.get(jobid);
						if(countValue==null){
							/*Map<String,Double> tempcountVal = new TreeMap<String, Double>();
							tempcountVal.put(pojoSS.getSecondaryStatusName(), ((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, tempcountVal);*/
						}else{
							if(countValue.get(pojoSS.getSecondaryStatusName())!=null){
							countValue.put(pojoSS.getSecondaryStatusName(),countValue.get(pojoSS.getSecondaryStatusName())+"@@@"+((double) Math.round(avg * 100) / 100));
							mapsscountwithJob.put(jobid, countValue);
							}
						}
				    }
				    if(ds!=0 && dss!=0)
				    {
				    	pojoS=mapStatusMaster.get((Integer)ds);
				    	String sDidplayStatusName="";
						if(pojoS.getStatusShortName().equalsIgnoreCase("comp")){
							sDidplayStatusName="Available"; 
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("rem")){
							sDidplayStatusName=pojoS.getStatus();
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("icomp")){
							sDidplayStatusName=pojoS.getStatus();
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("vlt")){
							sDidplayStatusName="Timed Out"; 
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("dcln"))
							sDidplayStatusName="Declined"; 
						else if(pojoS.getStatusShortName().equalsIgnoreCase("widrw"))
							sDidplayStatusName="Withdrew"; 
						else if(pojoS.getStatusShortName().equalsIgnoreCase("hird"))
							sDidplayStatusName="Hired";
						else  if(pojoS.getStatusShortName().equalsIgnoreCase("scomp")){
					    	sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
					    }
						else if(pojoS.getStatusShortName().equalsIgnoreCase("ecomp")){
							sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
						}
						else if(pojoS.getStatusShortName().equalsIgnoreCase("vcomp")){
							 sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
						 }
						
						//Map<String,Double> countValue = mapAvgNormScoreStatusAndJob.get(jobid);
						Map<String,String> countValue = mapsscountwithJob.get(jobid);
						if(countValue==null){
							/*Map<String,Double> tempcountVal = new TreeMap<String, Double>();
							tempcountVal.put(sDidplayStatusName, ((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, tempcountVal);*/
						}else{
							if(countValue.get(sDidplayStatusName)!=null){
								countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+"@@@"+((double) Math.round(avg * 100) / 100));
								mapsscountwithJob.put(jobid, countValue);
							}
							}
				    }
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			 //  Excel   Exporting	
					String time = String.valueOf(System.currentTimeMillis()).substring(6);
					//String basePath = request.getRealPath("/")+"/candidate";
					String basePath = request.getSession().getServletContext().getRealPath ("/")+"/joblist";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="joblist"+time+".xls";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

					WorkbookSettings wbSettings = new WorkbookSettings();

					wbSettings.setLocale(new Locale("en", "EN"));

					WritableCellFormat timesBoldUnderline;
					WritableCellFormat header;
					WritableCellFormat headerBold;
					WritableCellFormat times;
					WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
					workbook.createSheet("joblist", 0);
					WritableSheet excelSheet = workbook.getSheet(0);
					WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
					// Define the cell format
					times = new WritableCellFormat(times10pt);
					// Lets automatically wrap the cells
					times.setWrap(true);

					// Create create a bold font with unterlines
					WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
					WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

					timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
					timesBoldUnderline.setAlignment(Alignment.CENTRE);
					// Lets automatically wrap the cells
					timesBoldUnderline.setWrap(true);

					header = new WritableCellFormat(times10ptBoldUnderline);
					headerBold = new WritableCellFormat(times10ptBoldUnderline);
					CellView cv = new CellView();
					cv.setFormat(times);
					cv.setFormat(timesBoldUnderline);
					cv.setAutosize(true);
					header.setBackground(Colour.GRAY_25);

					// Write a few headers
					excelSheet.mergeCells(0, 0, 7, 1);
					Label label;
					label = new Label(0, 0, ""+Utility.getLocaleValuePropByKey("headJoLiWithAppCoun", locale)+"", timesBoldUnderline);
					excelSheet.addCell(label);
					excelSheet.mergeCells(0, 3, 7, 3);
					label = new Label(0, 3, "");
					excelSheet.addCell(label);
					excelSheet.getSettings().setDefaultColumnWidth(18);
					
					int k=4;
					int col=1;
					label = new Label(0, k, Utility.getLocaleValuePropByKey("lblDistrictName", locale),header); 
					excelSheet.addCell(label);
					label = new Label(1, k, Utility.getLocaleValuePropByKey("lblJobId", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJoTil", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblPostingDate", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgExpirationDate", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("msgPositions", locale)+"",header); 
					excelSheet.addCell(label); 
					label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("lblCandStatus", locale)+"",header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("msgNumberofApplications", locale)+"",header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("lblAvNoSco", locale)+"",header); 
					excelSheet.addCell(label);
					
					k=k+1;
					if(lstJobOrders.size()==0)
					{	
						excelSheet.mergeCells(0, k, 7, k);
						label = new Label(0, k, ""+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+""); 
						excelSheet.addCell(label);
					}
			
			Map<String, String> positionMap = getPositionsNumber(lstJobOrders);
			if(lstJobOrders.size()>0)
			 for(JobOrder job:lstJobOrders) 
			 {
				 Map<String, String> mapsecoundryStatus = mapsscountwithJob.get(job.getJobId());
				 String position = positionMap.get(""+job.getJobId());
				 if(mapsecoundryStatus==null){
					 col=1;
					    label = new Label(0, k, job.getDistrictMaster().getDistrictName()); 
						excelSheet.addCell(label);
						 
						label = new Label(1, k,job.getJobId()+""); 
						excelSheet.addCell(label);
						
						label = new Label(++col, k, job.getJobTitle()); 
						excelSheet.addCell(label);
						
						label = new Label(++col, k,Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobStartDate())+",12:01 AM"); 
						excelSheet.addCell(label);
						
						if(Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobEndDate()).equals("Dec 25, 2099"))
						  label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("lblUntilfilled", locale)+""); 
						else
						  label = new Label(++col, k, Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobEndDate())+",11:59 PM"); 
						excelSheet.addCell(label);
						//"+Utility.getLocaleValuePropByKey("lblCandStatus", locale)+" vs Number of Candidate 
						label = new Label(++col, k,position); 
					    excelSheet.addCell(label);
					    label = new Label(++col, k,"-"); 
					    excelSheet.addCell(label);
					    label = new Label(++col, k,"-"); 
					    excelSheet.addCell(label);
					    label = new Label(++col, k,"-"); 
					    excelSheet.addCell(label);
						
					   k++;
				 }else{
					   // String jobStatus="";
					    if(statusflag &&  statusNames!=null && !ArrayUtils.contains(statusNames,"0") )
					    {
							for(Map.Entry<String,String> entry : mapsecoundryStatus.entrySet())
							 {
								if(ArrayUtils.contains(statusNames,entry.getKey()))
								{
								 col=1;
							     label = new Label(0, k, job.getDistrictMaster().getDistrictName()); 
								 excelSheet.addCell(label);
								 
								 label = new Label(1, k,job.getJobId()+""); 
								 excelSheet.addCell(label);
								
								 label = new Label(++col, k, job.getJobTitle()); 
								 excelSheet.addCell(label);
								
								 label = new Label(++col, k,Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobStartDate())+",12:01 AM"); 
								 excelSheet.addCell(label);
								
								 if(Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobEndDate()).equals("Dec 25, 2099"))
								   label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("lblUntilfilled", locale)+""); 
								 else
								   label = new Label(++col, k, Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobEndDate())+",11:59 PM"); 
								   excelSheet.addCell(label);
								//"+Utility.getLocaleValuePropByKey("lblCandStatus", locale)+" vs Number of Candidate 
						         
								   label = new Label(++col, k,position); 
								   excelSheet.addCell(label); 
								    
								String  jobStatus1 = entry.getKey();
								 label = new Label(++col, k, jobStatus1); 
								 excelSheet.addCell(label);
								 
								 if(entry.getValue().matches("(.*)@@(.*)")){
										String countAndNorm[]=entry.getValue().split("@@@");
										label = new Label(++col, k, countAndNorm[0]); 
										excelSheet.addCell(label);
										
										label = new Label(++col, k, countAndNorm[1]); 
										excelSheet.addCell(label);
									}else{
										label = new Label(++col, k, entry.getValue()); 
										excelSheet.addCell(label);
										label = new Label(++col, k, "-"); 
										excelSheet.addCell(label);
									}
							  k++;
							}
						 }
				     }else{
							for(Map.Entry<String,String> entry : mapsecoundryStatus.entrySet())
							{
								 col=1;
							     label = new Label(0, k, job.getDistrictMaster().getDistrictName()); 
								 excelSheet.addCell(label);
								 
								 label = new Label(1, k,job.getJobId()+""); 
								 excelSheet.addCell(label);
								
								 label = new Label(++col, k, job.getJobTitle()); 
								 excelSheet.addCell(label);
								
								 label = new Label(++col, k,Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobStartDate())+",12:01 AM"); 
								 excelSheet.addCell(label);
								
								 if(Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobEndDate()).equals("Dec 25, 2099"))
								   label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("lblUntilfilled", locale)+""); 
								 else
								   label = new Label(++col, k, Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobEndDate())+",11:59 PM"); 
								   excelSheet.addCell(label);
								
								   label = new Label(++col, k,position); 
								   excelSheet.addCell(label); 
								    
								String jobStatus1=entry.getKey();
								label = new Label(++col, k, jobStatus1); 
								excelSheet.addCell(label);
									
								
								
								if(entry.getValue().matches("(.*)@@(.*)")){
									String countAndNorm[]=entry.getValue().split("@@@");
									label = new Label(++col, k, countAndNorm[0]); 
									excelSheet.addCell(label);
									
									label = new Label(++col, k, countAndNorm[1]); 
									excelSheet.addCell(label);
								}else{
									label = new Label(++col, k, entry.getValue()); 
									excelSheet.addCell(label);
									label = new Label(++col, k, "-"); 
									excelSheet.addCell(label);
								}
							k++;
							}
						}
				 }
				 
			}
			try{
				try{
					mapStatusMaster.clear();//map
					mapStatusMaster=null;
				}catch (Exception e) {}
				try {
					mapSecondaryStatus.clear();//map
					mapSecondaryStatus=null;
				} catch (Exception e) {}
				try {
					statusMasters.clear();//list
					statusMasters=null;
				} catch (Exception e) {}
				try {
					statusMasters.clear();//list
					statusMasters=null;
				} catch (Exception e) {}	
				try {
					secondaryStatus.clear();//list
					secondaryStatus=null;
				} catch (Exception e) {}	
				try {
					mapAvgNormScoreStatusAndJob .clear();//map
					mapAvgNormScoreStatusAndJob =null;
				} catch (Exception e) {}	
				try {
					secstatusIds.clear();//list
					secstatusIds=null;
				} catch (Exception e) {}	
				try {
					statusIds.clear();//list
					statusIds=null;
				} catch (Exception e) {}	
				try {
					mapAvgEPI.clear();//map
					mapAvgEPI=null;
				} catch (Exception e) {}	
				try {
					avgListbyJobList.clear();//list
					avgListbyJobList=null;
				} catch (Exception e) {}	
				try {
					lstStatusMasters.clear();//list
					lstStatusMasters=null;
				} catch (Exception e) {}	
				try {
					secondaryStatusList.clear();//list
					secondaryStatusList=null;
				} catch (Exception e) {}	
				try {
					mapSpecificStatus.clear();//map
					mapSpecificStatus=null;
				} catch (Exception e) {}	
				try {
					mapsscountwithJob.clear();//map
					mapsscountwithJob=null;
				} catch (Exception e) {}	
				try {
					districtMasters.clear();//list
					districtMasters=null;
				} catch (Exception e) {}	
				try {
					criteriaMixJobOrders.clear();//list
					criteriaMixJobOrders=null;
				} catch (Exception e) {}	
				try {
					statusFilterJobIds.clear();//list
					statusFilterJobIds=null;
				} catch (Exception e) {}
				try {
					lstSecondaryStatus.clear();//list
					lstSecondaryStatus=null;
				} catch (Exception e) {}
				try {
					jobOrdersByJobStatuses.clear();  //list
					jobOrdersByJobStatuses=null;
				} catch (Exception e) {}
				try {
					lstJobOrders.clear();//list
					lstJobOrders=null;
				} catch (Exception e) {}
				try {
					lstSchoolJobOrderIds.clear();//list
					lstSchoolJobOrderIds=null;
				} catch (Exception e) {}
				try {
					jobIDS.clear();//list
					jobIDS=null;
				} catch (Exception e) {}
				try {
					jobIDsByAvgNormScore.clear();//list
					jobIDsByAvgNormScore=null;
				} catch (Exception e) {}
				try {
					finaljobIDS.clear();//list
					finaljobIDS=null;
				} catch (Exception e) {}
				
		}catch(Exception ex){
			ex.printStackTrace();
		}
			
			workbook.write();
			workbook.close();
		}catch(Exception e){}
		
	 return fileName;
  }
	
  
	
	public String jobListExportPDF(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String endfromDate, String endtoDate,String sfromDate, String stoDate,String jobOrderId,String normScoreSelectVal,String normScoreVal,String statusId,boolean exportcheck)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport(districtOrSchoolId,schoolId,noOfRow,pageNo,sortOrder,sortOrderType,endfromDate, endtoDate,sfromDate,stoDate,jobOrderId,normScoreSelectVal,normScoreVal,statusId,exportcheck,basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	public boolean generateCandidatePDReport(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String endfromDate, String endtoDate,String sfromDate, String stoDate,String jobOrderId,String normScoreSelectVal,String normScoreVal,String statusId,boolean exportcheck,String path,String realPath){
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			String fileName = null;
			int sortingcheck=0;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position

					int noOfRowInPage = Integer.parseInt(noOfRow);
					int pgNo = Integer.parseInt(pageNo);
					int start =((pgNo-1)*noOfRowInPage);
					int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
					//------------------------------------
				 /** set default sorting fieldName **/
					String sortOrderFieldName="jobId";
					String sortOrderNoField="jobId";

					if(sortOrder.equalsIgnoreCase("districtName"))
						sortingcheck=1;
					
					if(sortOrder.equalsIgnoreCase("normScore"))
						sortingcheck=2;
					
					/**Start set dynamic sorting fieldName **/
					
					Order  sortOrderStrVal=null;

					if(sortOrder!=null){
						if(!sortOrder.equals("") && !sortOrder.equals(null)){
							sortOrderFieldName=sortOrder;
							sortOrderNoField=sortOrder;
						}
					}

					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("0")){
							sortOrderStrVal=Order.asc(sortOrderFieldName);
						}else{
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
						}
					}else{
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<JobOrder> lstJobOrders =new ArrayList<JobOrder>();
					List<Integer> lstSchoolJobOrderIds = new ArrayList<Integer>();
					boolean checkflag = false;
					List<Integer> jobIDS= new ArrayList<Integer>();
					List<Integer> jobIDsByAvgNormScore= new ArrayList<Integer>();
					List<Integer> finaljobIDS= new ArrayList<Integer>();
					boolean jobIdsflag=false;
					boolean normScoreflag=false;
					boolean finaljobIdsflag=false;
					boolean statusflag=false;
					
					if(districtOrSchoolId!=0 && entityID==1)
						  districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
					
					//******************************Job Status***************************************
	//filter of job status 
					String statusNames[]=null;
					List<Integer> jobOrdersByJobStatuses = new ArrayList<Integer>();
					List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
					 if(!statusId.equals("") && !statusId.equals("0")){
						 System.out.println(" Filter StatusId Start ::  "+statusId);
						  statusNames = statusId.split("#@");
					      if(!ArrayUtils.contains(statusNames,"0"))
					      {	 
					    	 statusflag=true;
							 lstSecondaryStatus = secondaryStatusDAO.findByMultipleStatusName(districtMaster, statusNames);
					
							  List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
							  Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
							  for (StatusMaster statusMaster1 : statusMasterList) {
								  mapStatus.put(statusMaster1.getStatusShortName(),statusMaster1);
							  }
							  StatusMaster stMaster =null;
							  List <StatusMaster> statusMaster1st=new ArrayList<StatusMaster>();
							  
							  if(ArrayUtils.contains(statusNames,"Available")){
									  stMaster = mapStatus.get("comp");
									  statusMaster1st.add(stMaster);
							  }
							  if(ArrayUtils.contains(statusNames,"Rejected")){
								    stMaster = mapStatus.get("rem");
								    statusMaster1st.add(stMaster);
						     }
							  if(ArrayUtils.contains(statusNames,"Timed Out")){
								    stMaster = mapStatus.get("vlt");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Withdrew")){
								    stMaster = mapStatus.get("widrw");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Incomplete")){
								    stMaster = mapStatus.get("icomp");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Hired")){
								    stMaster = mapStatus.get("hird");
								    statusMaster1st.add(stMaster);
						     }   
							 for(SecondaryStatus ss: lstSecondaryStatus)
							  {
								 if(ss.getStatusMaster()!=null)
									 statusMaster1st.add(ss.getStatusMaster()); 
							  }
							 jobOrdersByJobStatuses = jobForTeacherDAO.findJobOrderByCandidtesJobStatus(districtMaster,statusMaster1st,lstSecondaryStatus);
				         }
					 }
			//################################################################################
				// Job ID Filter
					if(!jobOrderId.equals(""))
					{
						jobIdsflag=true;
						String strjobIds[]=jobOrderId.split(",");
						if(strjobIds.length>0)
					     for(String str : strjobIds){
					    	 if(!str.equals(""))
					    	 jobIDS.add(Integer.parseInt(str));
					     }
					}
					
				// Average Norm Score Filter	
					boolean nbyAflag=false;
					if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") && !normScoreSelectVal.equals("6"))
					 {
						normScoreflag=true;
						List avgNormScore=  jobForTeacherDAO.avgFilterNormHQL(normScoreVal, normScoreSelectVal, districtMaster,jobOrdersByJobStatuses,statusflag);
						Iterator iter = avgNormScore.iterator();
						if(avgNormScore.size()>0)
						{
						  while(iter.hasNext()){
							  Object[] obj = (Object[]) iter.next();
							  jobIDsByAvgNormScore.add((Integer)obj[0]);
					  	  }
						}
					 }
					 List<Integer> nbyAJobIds= new ArrayList<Integer>();
					
					if(normScoreflag==true && jobIdsflag==false){
						finaljobIDS.addAll(jobIDsByAvgNormScore);
						finaljobIdsflag=true;
					}else if(normScoreflag==false && jobIdsflag==true){
						finaljobIDS.addAll(jobIDS);
						finaljobIdsflag=true;
					}else if(normScoreflag==true && jobIdsflag==true){
						jobIDS.retainAll(jobIDsByAvgNormScore);
						finaljobIDS.addAll(jobIDS);
						finaljobIdsflag=true;
					}
					
					
					//New Change 
					List<Integer> statusFilterJobIds= new  ArrayList<Integer>();
					boolean onlyStatus=false;
					if(normScoreflag==false && statusflag==true){
						onlyStatus=true;
						statusFilterJobIds = jobForTeacherDAO.findJobIdsbyJFTID(jobOrdersByJobStatuses);
					}
					if(onlyStatus==true && finaljobIdsflag==true){
						finaljobIDS.retainAll(statusFilterJobIds);
						
					}else if(onlyStatus==true && finaljobIdsflag==false){
						finaljobIDS.addAll(statusFilterJobIds);
						finaljobIdsflag=true;
					}
					/*if(finaljobIdsflag==true && statusflag==true){
						finaljobIDS.retainAll(jobOrdersByJobStatuses);
						finaljobIdsflag=true;
					}
					else if(finaljobIdsflag==false && statusflag==true){
						finaljobIDS.addAll(jobOrdersByJobStatuses);
						finaljobIdsflag=true;
					}*/
				
					//SchoolId Filter
				    boolean schoolFlag=false;
					if(schoolId!=0)
					{
					  schoolFlag=true;
					  SchoolMaster  sclMaster=null;
					  sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
					  lstSchoolJobOrderIds = schoolInJobOrderDAO.findAllJobOrderBySchool(sclMaster);
					}	
					
					if(finaljobIdsflag==true && schoolFlag==true){
						finaljobIDS.retainAll(lstSchoolJobOrderIds);
						finaljobIdsflag=true;
					}
					else if(finaljobIdsflag==false && schoolFlag==true){
						finaljobIDS.addAll(lstSchoolJobOrderIds);
						finaljobIdsflag=true;
					}
					
					List<JobOrder> criteriaMixJobOrders=new ArrayList<JobOrder>();
					if(entityID==2 || entityID==1)
					{
					   if(finaljobIdsflag==true && finaljobIDS!=null &&  finaljobIDS.size()==0){
						  lstJobOrders=new ArrayList<JobOrder>();
						}
					  else
					    {
						  criteriaMixJobOrders = jobOrderDAO.findJobOrderbyWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start,noOfRowInPage,endfromDate,endtoDate,sfromDate, stoDate,finaljobIDS,nbyAflag,nbyAJobIds,exportcheck);						
						  
						  if(criteriaMixJobOrders!=null && criteriaMixJobOrders.size()>0)
						    {
						      if(checkflag){
		   						lstJobOrders.retainAll(criteriaMixJobOrders);
		   					  }else{
		   						lstJobOrders.addAll(criteriaMixJobOrders);
		   						checkflag=true;
		   					  }
						    }else{
						    	 lstJobOrders=new ArrayList<JobOrder>();
						    }
						 }
					 }
					
				List<DistrictMaster> districtMasters = new ArrayList<DistrictMaster>();
				for(JobOrder job : lstJobOrders){
					districtMasters.add(job.getDistrictMaster());
				}
					
				Map<Integer, Map<String,Integer>> mapsscountwithJob= new HashMap<Integer, Map<String,Integer>>();
				Map<Integer,String> mapSpecificStatus = new HashMap<Integer, String>();
				List<SecondaryStatus> secondaryStatusList = new ArrayList<SecondaryStatus>();
				
				String []statusArray={"scomp" , "ecomp" , "vcomp"};
			    List<StatusMaster> lstStatusMasters = Utility.getStaticMasters(statusArray);
			    if(districtMasters!=null && districtMasters.size()>0)
			     secondaryStatusList= secondaryStatusDAO.findSecondaryStatusforSpecificStatusListBYDistricts(districtMasters,lstStatusMasters);
			    if(secondaryStatusList!=null && secondaryStatusList.size()>0)
			    for(SecondaryStatus ss : secondaryStatusList)
			     {
			    	mapSpecificStatus.put(ss.getStatusMaster().getStatusId(), ss.getSecondaryStatusName());
			     }
					
				List list = new ArrayList();	
				if(lstJobOrders!=null && lstJobOrders.size()>0)
				{
				   list= jobForTeacherDAO.fintApplicntsStatusByJob(lstJobOrders);
				}
				 Iterator itr = list.iterator();
					if(list.size()>0){
						while(itr.hasNext()){
							Object[] obj = (Object[]) itr.next();
							JobOrder jobOrder=(JobOrder)obj[0];
							
							SecondaryStatus secondaryStatus= null;
							StatusMaster statusMaster= null;
							if(obj[1]!=null)
								secondaryStatus=(SecondaryStatus) obj[1];
							
							statusMaster=(StatusMaster) obj[2];
							Integer secCount= (Integer) obj[3];
							Integer statusCount = (Integer) obj[4];
							
							
							if(secondaryStatus!=null && statusMaster==null){
								Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
								if(countValue==null){
									Map<String,Integer> tempcountVal = new TreeMap<String, Integer>();
									tempcountVal.put(secondaryStatus.getSecondaryStatusName(), secCount);
									mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
								}else{
									if(countValue.get(secondaryStatus.getSecondaryStatusName())!=null){
										secCount = countValue.get(secondaryStatus.getSecondaryStatusName())+secCount;
									}
									countValue.put(secondaryStatus.getSecondaryStatusName(), secCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}
							}
							
							if(secondaryStatus==null && statusMaster!=null){
								String sDidplayStatusName="";
								if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
									sDidplayStatusName="Available"; 
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
									sDidplayStatusName=statusMaster.getStatus();
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
									sDidplayStatusName=statusMaster.getStatus();
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
									sDidplayStatusName="Timed Out"; 
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
									sDidplayStatusName="Declined"; 
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
									sDidplayStatusName="Withdrew"; 
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
									sDidplayStatusName="Hired";
								
								else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
							    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							    }
									 
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
									sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
								}
								  
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
									 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
								 }
								
								Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
								if(countValue==null){
									Map<String,Integer> tempcountVal = new TreeMap<String, Integer>();
									tempcountVal.put(sDidplayStatusName, statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
								}else{
									if(countValue.get(sDidplayStatusName)!=null){
										countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
										mapsscountwithJob.put(jobOrder.getJobId(), countValue);
									}else{
										countValue.put(sDidplayStatusName,statusCount);
										mapsscountwithJob.put(jobOrder.getJobId(), countValue);
									}
								}
							}
							if(secondaryStatus!=null && statusMaster!=null){
								String sDidplayStatusName="";
								if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
									sDidplayStatusName="Available"; 
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
									sDidplayStatusName=statusMaster.getStatus();
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
									sDidplayStatusName=statusMaster.getStatus();
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
									sDidplayStatusName="Timed Out"; 
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
									sDidplayStatusName="Declined"; 
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
									sDidplayStatusName="Withdrew"; 
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
									sDidplayStatusName="Hired";
								else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
							    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							    }
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
									sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
								}
								else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
									 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
								}
								Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
								if(countValue==null){
									Map<String,Integer> tempcountVal = new TreeMap<String, Integer>();
									tempcountVal.put(sDidplayStatusName, statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
								}else{
									if(countValue.get(sDidplayStatusName)!=null){
										countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
										mapsscountwithJob.put(jobOrder.getJobId(), countValue);
									}else{
										countValue.put(sDidplayStatusName,statusCount);
										mapsscountwithJob.put(jobOrder.getJobId(), countValue);
									}
								}
							}
						}
					}
				List avgListbyJobList=new ArrayList();
				Map<Integer,Double> mapAvgEPI = new HashMap<Integer, Double>();
				if(lstJobOrders!=null && lstJobOrders.size()>0)
				{
				  avgListbyJobList= jobForTeacherDAO.countCandidatesNormHQLJobListWise(lstJobOrders);
				}
				
				
				List<Integer> statusIds = new ArrayList<Integer>();
				List<Integer> secstatusIds = new ArrayList<Integer>();
				//Map<jobId,Map<StatusName,avgNormScore>>
				Map<Integer, Map<String,Double>> mapAvgNormScoreStatusAndJob = new HashMap<Integer, Map<String,Double>>();
				for(Object objLst : avgListbyJobList)
	            {
	                Object[] obj = (Object[]) objLst;
	                int ds = Utility.getIntValue(String.valueOf(obj[4]));
	                int dss = Utility.getIntValue(String.valueOf(obj[3]));
	                if(ds!=0 && dss==0)
	                	statusIds.add(ds);
	                if(ds==0 && dss!=0)
	                	secstatusIds.add(dss);
	                if(ds!=0 && dss!=0)
	                	statusIds.add(ds);
	                
	            }
				List<StatusMaster> statusMasters =new ArrayList<StatusMaster>();
				List<SecondaryStatus> secondaryStatus = new ArrayList<SecondaryStatus>();
				Map<Integer,StatusMaster> mapStatusMaster = new HashMap<Integer, StatusMaster>();
				Map<Integer,SecondaryStatus> mapSecondaryStatus =  new HashMap<Integer, SecondaryStatus>();
				
				if(statusIds!=null && statusIds.size()>0)
				{
					Criterion criterion = Restrictions.in("statusId", statusIds);
					statusMasters = statusMasterDAO.findByCriteria(criterion);
				}
				if(statusMasters!=null && statusMasters.size()>0){
					for(StatusMaster sm :statusMasters){
						mapStatusMaster.put(sm.getStatusId(), sm);
					}
				}
					
				if(secstatusIds!=null && secstatusIds.size()>0)
				{
					Criterion criterion = Restrictions.in("secondaryStatusId", secstatusIds);
					secondaryStatus = secondaryStatusDAO.findByCriteria(criterion);
				}
				if(secondaryStatus!=null && secondaryStatus.size()>0){
					for(SecondaryStatus ssm :secondaryStatus){
						mapSecondaryStatus.put(ssm.getSecondaryStatusId(), ssm);
					}
				}	
				for(Object objLst:avgListbyJobList)
	            {
	                float avg;
	                Object[] obj = (Object[]) objLst;
	                int normSum=Integer.parseInt(String.valueOf(obj[1]));
	                int total =Integer.parseInt(String.valueOf(obj[2]));
	                avg  =(float)normSum/(float)total;
	                int jobid = Utility.getIntValue(String.valueOf(obj[0]));
	               
	                int ds = Utility.getIntValue(String.valueOf(obj[4]));
	                int dss = Utility.getIntValue(String.valueOf(obj[3]));
	                StatusMaster pojoS = new StatusMaster();
	                SecondaryStatus pojoSS = new SecondaryStatus();
	                if(ds!=0 && dss==0)
	                {
	                	pojoS=mapStatusMaster.get((Integer)ds);
	                	String sDidplayStatusName="";
						if(pojoS.getStatusShortName().equalsIgnoreCase("comp")){
							sDidplayStatusName="Available"; 
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("rem")){
							sDidplayStatusName=pojoS.getStatus();
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("icomp")){
							sDidplayStatusName=pojoS.getStatus();
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("vlt")){
							sDidplayStatusName="Timed Out"; 
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("dcln"))
							sDidplayStatusName="Declined"; 
						else if(pojoS.getStatusShortName().equalsIgnoreCase("widrw"))
							sDidplayStatusName="Withdrew"; 
						else if(pojoS.getStatusShortName().equalsIgnoreCase("hird"))
							sDidplayStatusName="Hired";
						
						else  if(pojoS.getStatusShortName().equalsIgnoreCase("scomp")){
					    	sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
					    }
						else if(pojoS.getStatusShortName().equalsIgnoreCase("ecomp")){
							sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
						}
						else if(pojoS.getStatusShortName().equalsIgnoreCase("vcomp")){
							 sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
						 }
						
						Map<String,Double> countValue = mapAvgNormScoreStatusAndJob.get(jobid);
						if(countValue==null){
							Map<String,Double> tempcountVal = new TreeMap<String, Double>();
							tempcountVal.put(sDidplayStatusName, ((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, tempcountVal);
						}else{
							countValue.put(sDidplayStatusName,((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, countValue);
							}
	                }
	                if(ds==0 && dss!=0)
	                {
	                	pojoSS=mapSecondaryStatus.get((Integer)dss);
	                	Map<String,Double> countValue = mapAvgNormScoreStatusAndJob.get(jobid);
						if(countValue==null){
							Map<String,Double> tempcountVal = new TreeMap<String, Double>();
							tempcountVal.put(pojoSS.getSecondaryStatusName(), ((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, tempcountVal);
						}else{
							countValue.put(pojoSS.getSecondaryStatusName(), ((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, countValue);
						}
	                }
	                if(ds!=0 && dss!=0)
	                {
	                	pojoS=mapStatusMaster.get((Integer)ds);
	                	String sDidplayStatusName="";
						if(pojoS.getStatusShortName().equalsIgnoreCase("comp")){
							sDidplayStatusName="Available"; 
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("rem")){
							sDidplayStatusName=pojoS.getStatus();
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("icomp")){
							sDidplayStatusName=pojoS.getStatus();
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("vlt")){
							sDidplayStatusName="Timed Out"; 
						}else if(pojoS.getStatusShortName().equalsIgnoreCase("dcln"))
							sDidplayStatusName="Declined"; 
						else if(pojoS.getStatusShortName().equalsIgnoreCase("widrw"))
							sDidplayStatusName="Withdrew"; 
						else if(pojoS.getStatusShortName().equalsIgnoreCase("hird"))
							sDidplayStatusName="Hired";
						else  if(pojoS.getStatusShortName().equalsIgnoreCase("scomp")){
					    	sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
					    }
						else if(pojoS.getStatusShortName().equalsIgnoreCase("ecomp")){
							sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
						}
						else if(pojoS.getStatusShortName().equalsIgnoreCase("vcomp")){
							 sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
						 }
						
						Map<String,Double> countValue = mapAvgNormScoreStatusAndJob.get(jobid);
						if(countValue==null){
							Map<String,Double> tempcountVal = new TreeMap<String, Double>();
							tempcountVal.put(sDidplayStatusName, ((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, tempcountVal);
						}else{
							countValue.put(sDidplayStatusName,((double) Math.round(avg * 100) / 100));
							mapAvgNormScoreStatusAndJob.put(jobid, countValue);
							}
	                }
	            }
				
					String fontPath = realPath;
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject(Utility.getLocaleValuePropByKey("headJoLiWithAppCoun", locale));
						document.addCreationDate();
						document.addTitle(Utility.getLocaleValuePropByKey("headJoLiWithAppCoun", locale));

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));

						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("headJoLiWithAppCoun", locale),font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						
						
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						
						
						document.add(new Phrase("\n"));


						//float[] tblwidth={.20f,.06f,.20f,.16f,.16f,.18f,.12f,.18f};
						float[] tblwidth={.20f,.06f,.20f,.16f,.16f,.12f,.18f,.12f,.18f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[tblwidth.length];
						cell = new PdfPCell[tblwidth.length];
				// header
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblDistrictName", locale),font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[0].setBorder(1);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJobId", locale),font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[1].setBorder(1);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJoTil", locale),font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
					//	cell[2].setBorder(1);
						mainTable.addCell(cell[2]);

						para[3] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblPostingDate", locale),font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[3].setBorder(1);
						mainTable.addCell(cell[3]);
						
						para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgExpirationDate", locale),font10_10);
						cell[4]= new PdfPCell(para[4]);
						cell[4].setBackgroundColor(bluecolor);
						cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[4]);
						
						para[5] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("msgPositions", locale)+"",font10_10);
						cell[5]= new PdfPCell(para[5]);
						cell[5].setBackgroundColor(bluecolor);
						cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[5]);
						
						para[6] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCandStatus", locale)+"",font10_10);
						cell[6]= new PdfPCell(para[6]);
						cell[6].setBackgroundColor(bluecolor);
						cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[6]);
						
						para[7] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("msgNumberofApplications", locale)+"",font10_10);
						cell[7]= new PdfPCell(para[7]);
						cell[7].setBackgroundColor(bluecolor);
						cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[7]);
						
						para[8] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblAvNoSco", locale)+"",font10_10);
						cell[8]= new PdfPCell(para[8]);
						cell[8].setBackgroundColor(bluecolor);
						cell[8].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[8]); 
						
						document.add(mainTable);
						
						if(lstJobOrders.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+".",font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 //cell[index].setBorder(1);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
						
						Map<String, String> positionMap = getPositionsNumber(lstJobOrders);
						if(lstJobOrders.size()>0){
							 for(JobOrder job:lstJobOrders) 
							 {
								 String position = positionMap.get(""+job.getJobId()); 
								int index=0;
									
								 mainTable = new PdfPTable(tblwidth);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[tblwidth.length];
								 cell = new PdfPCell[tblwidth.length];
								
								 
								 para[index] = new Paragraph(""+job.getDistrictMaster().getDistrictName(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 para[index] = new Paragraph(""+job.getJobId(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 para[index] = new Paragraph(""+job.getJobTitle(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobStartDate())+",12:01 AM",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 if(Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobEndDate()).equals("Dec 25, 2099"))
								   para[index] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblUntilfilled", locale)+"",font8bold);
								 else
								   para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobEndDate())+",11:59 PM",font8bold);
								 
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 para[index] = new Paragraph(position,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
									 
								//"+Utility.getLocaleValuePropByKey("lblCandStatus", locale)+" vs Number of Candidate 
								Map<String, Integer> mapsecoundryStatus = mapsscountwithJob.get(job.getJobId());
								 if(mapsecoundryStatus==null){
									 para[index] = new Paragraph("-",font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++;
									 para[index] = new Paragraph("-",font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++; 
									 
								 }else{
								  if(statusflag &&  statusNames!=null && !ArrayUtils.contains(statusNames,"0") ){
									String jobStatus="";
									 for(Map.Entry<String,Integer> entry : mapsecoundryStatus.entrySet())
									  {
										if(jobStatus==""){
											if(ArrayUtils.contains(statusNames,entry.getKey()))
											jobStatus = entry.getKey();
										}else{
											if(ArrayUtils.contains(statusNames,entry.getKey()))
											jobStatus=jobStatus+"\n"+entry.getKey();
										}
									  }
									 para[index] = new Paragraph(""+jobStatus,font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++; 
								   String numberOfApp="";
									for(Map.Entry<String,Integer> entry : mapsecoundryStatus.entrySet())
									{
										if(numberOfApp==""){
											if(ArrayUtils.contains(statusNames,entry.getKey()))
											numberOfApp = entry.getValue()+"";
										}else{
											if(ArrayUtils.contains(statusNames,entry.getKey()))
											numberOfApp=numberOfApp+"\n"+entry.getValue();
										}	
									}
									 para[index] = new Paragraph(""+numberOfApp,font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++; 
								}else{
									String jobStatus="";
									 for(Map.Entry<String,Integer> entry : mapsecoundryStatus.entrySet())
									  {
										if(jobStatus==""){
											jobStatus = entry.getKey();
										}else{
											jobStatus=jobStatus+"\n"+entry.getKey();
										}
									  }
									 para[index] = new Paragraph(""+jobStatus,font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++; 
								   String numberOfApp="";
									for(Map.Entry<String,Integer> entry : mapsecoundryStatus.entrySet())
									{
										if(numberOfApp==""){
											numberOfApp = entry.getValue()+"";
										}else{
											numberOfApp=numberOfApp+"\n"+entry.getValue();
										}	
									}
									 para[index] = new Paragraph(""+numberOfApp,font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++; 
								
								}
							  }
								//for Average Norm Score 
								 String avgNorm="";
								 Map<String, Double> mapTempAvg = mapAvgNormScoreStatusAndJob.get(job.getJobId());
									if(mapTempAvg!=null && mapTempAvg.size()>0){
									   for(Map.Entry<String,Double> entry : mapTempAvg.entrySet()){
										   if(avgNorm.length()==0)
											   avgNorm=entry.getKey()+ " - "+ entry.getValue();
										   else
											   avgNorm=avgNorm+"\n"+entry.getKey()+ " - "+ entry.getValue();
									   }
									}else{
										avgNorm="-";
									}
								  
									 para[index] = new Paragraph(""+avgNorm,font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++;
								  
								document.add(mainTable);
							 }
						}
						try{
							try{
								mapStatusMaster.clear();//map
								mapStatusMaster=null;
							}catch (Exception e) {}
							try {
								mapSecondaryStatus.clear();//map
								mapSecondaryStatus=null;
							} catch (Exception e) {}
							try {
								statusMasters.clear();//list
								statusMasters=null;
							} catch (Exception e) {}
							try {
								statusMasters.clear();//list
								statusMasters=null;
							} catch (Exception e) {}	
							try {
								secondaryStatus.clear();//list
								secondaryStatus=null;
							} catch (Exception e) {}	
							try {
								mapAvgNormScoreStatusAndJob .clear();//map
								mapAvgNormScoreStatusAndJob =null;
							} catch (Exception e) {}	
							try {
								secstatusIds.clear();//list
								secstatusIds=null;
							} catch (Exception e) {}	
							try {
								statusIds.clear();//list
								statusIds=null;
							} catch (Exception e) {}	
							try {
								mapAvgEPI.clear();//map
								mapAvgEPI=null;
							} catch (Exception e) {}	
							try {
								avgListbyJobList.clear();//list
								avgListbyJobList=null;
							} catch (Exception e) {}	
							try {
								lstStatusMasters.clear();//list
								lstStatusMasters=null;
							} catch (Exception e) {}	
							try {
								secondaryStatusList.clear();//list
								secondaryStatusList=null;
							} catch (Exception e) {}	
							try {
								mapSpecificStatus.clear();//map
								mapSpecificStatus=null;
							} catch (Exception e) {}	
							try {
								mapsscountwithJob.clear();//map
								mapsscountwithJob=null;
							} catch (Exception e) {}	
							try {
								districtMasters.clear();//list
								districtMasters=null;
							} catch (Exception e) {}	
							try {
								criteriaMixJobOrders.clear();//list
								criteriaMixJobOrders=null;
							} catch (Exception e) {}	
							try {
								statusFilterJobIds.clear();//list
								statusFilterJobIds=null;
							} catch (Exception e) {}
							try {
								lstSecondaryStatus.clear();//list
								lstSecondaryStatus=null;
							} catch (Exception e) {}
							try {
								jobOrdersByJobStatuses.clear();  //list
								jobOrdersByJobStatuses=null;
							} catch (Exception e) {}
							try {
								lstJobOrders.clear();//list
								lstJobOrders=null;
							} catch (Exception e) {}
							try {
								lstSchoolJobOrderIds.clear();//list
								lstSchoolJobOrderIds=null;
							} catch (Exception e) {}
							try {
								jobIDS.clear();//list
								jobIDS=null;
							} catch (Exception e) {}
							try {
								jobIDsByAvgNormScore.clear();//list
								jobIDsByAvgNormScore=null;
							} catch (Exception e) {}
							try {
								finaljobIDS.clear();//list
								finaljobIDS=null;
							} catch (Exception e) {}
							
					}catch(Exception ex){
						ex.printStackTrace();
					}
						
						
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	
	public String jobListPrintPreview(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String endfromDate, String endtoDate,String sfromDate, String stoDate,String jobOrderId,String normScoreSelectVal,String normScoreVal,String statusId,boolean exportcheck)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=0;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="jobId";
				String sortOrderNoField="jobId";

				if(sortOrder.equalsIgnoreCase("districtName"))
					sortingcheck=1;
				
				if(sortOrder.equalsIgnoreCase("normScore"))
					sortingcheck=2;
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<JobOrder> lstJobOrders =new ArrayList<JobOrder>();
				List<Integer> lstSchoolJobOrderIds = new ArrayList<Integer>();
				boolean checkflag = false;
				List<Integer> jobIDS= new ArrayList<Integer>();
				List<Integer> jobIDsByAvgNormScore= new ArrayList<Integer>();
				List<Integer> finaljobIDS= new ArrayList<Integer>();
				boolean jobIdsflag=false;
				boolean normScoreflag=false;
				boolean finaljobIdsflag=false;
				boolean statusflag=false;
				
				if(districtOrSchoolId!=0 && entityID==1)
					  districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
				
				//******************************Job Status***************************************
//filter of job status 
				String statusNames[]=null;
				List<Integer> jobOrdersByJobStatuses = new ArrayList<Integer>();
				List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
				 if(!statusId.equals("") && !statusId.equals("0")){
					 System.out.println(" Filter StatusId Start ::  "+statusId);
					  statusNames = statusId.split("#@");
				      if(!ArrayUtils.contains(statusNames,"0"))
				      {	 
				    	 statusflag=true;
						 lstSecondaryStatus = secondaryStatusDAO.findByMultipleStatusName(districtMaster, statusNames);
				
						  List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
						  Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
						  for (StatusMaster statusMaster1 : statusMasterList) {
							  mapStatus.put(statusMaster1.getStatusShortName(),statusMaster1);
						  }
						  StatusMaster stMaster =null;
						  List <StatusMaster> statusMaster1st=new ArrayList<StatusMaster>();
						  
						  if(ArrayUtils.contains(statusNames,"Available")){
								  stMaster = mapStatus.get("comp");
								  statusMaster1st.add(stMaster);
						  }
						  if(ArrayUtils.contains(statusNames,"Rejected")){
							    stMaster = mapStatus.get("rem");
							    statusMaster1st.add(stMaster);
					     }
						  if(ArrayUtils.contains(statusNames,"Timed Out")){
							    stMaster = mapStatus.get("vlt");
							    statusMaster1st.add(stMaster);
					     }
						 if(ArrayUtils.contains(statusNames,"Withdrew")){
							    stMaster = mapStatus.get("widrw");
							    statusMaster1st.add(stMaster);
					     }
						 if(ArrayUtils.contains(statusNames,"Incomplete")){
							    stMaster = mapStatus.get("icomp");
							    statusMaster1st.add(stMaster);
					     }
						 if(ArrayUtils.contains(statusNames,"Hired")){
							    stMaster = mapStatus.get("hird");
							    statusMaster1st.add(stMaster);
					     }   
						 for(SecondaryStatus ss: lstSecondaryStatus)
						  {
							 if(ss.getStatusMaster()!=null)
								 statusMaster1st.add(ss.getStatusMaster()); 
						  }
						 jobOrdersByJobStatuses = jobForTeacherDAO.findJobOrderByCandidtesJobStatus(districtMaster,statusMaster1st,lstSecondaryStatus);
			         }
				 }
		//################################################################################
			// Job ID Filter
				if(!jobOrderId.equals(""))
				{
					jobIdsflag=true;
					String strjobIds[]=jobOrderId.split(",");
					if(strjobIds.length>0)
				     for(String str : strjobIds){
				    	 if(!str.equals(""))
				    	 jobIDS.add(Integer.parseInt(str));
				     }
				}
				
			// Average Norm Score Filter	
				boolean nbyAflag=false;
				if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") && !normScoreSelectVal.equals("6"))
				 {
					normScoreflag=true;
					List avgNormScore=  jobForTeacherDAO.avgFilterNormHQL(normScoreVal, normScoreSelectVal, districtMaster,jobOrdersByJobStatuses,statusflag);
					Iterator iter = avgNormScore.iterator();
					if(avgNormScore.size()>0)
					{
					  while(iter.hasNext()){
						  Object[] obj = (Object[]) iter.next();
						  jobIDsByAvgNormScore.add((Integer)obj[0]);
				  	  }
					}
				 }
				 List<Integer> nbyAJobIds= new ArrayList<Integer>();
				
				if(normScoreflag==true && jobIdsflag==false){
					finaljobIDS.addAll(jobIDsByAvgNormScore);
					finaljobIdsflag=true;
				}else if(normScoreflag==false && jobIdsflag==true){
					finaljobIDS.addAll(jobIDS);
					finaljobIdsflag=true;
				}else if(normScoreflag==true && jobIdsflag==true){
					jobIDS.retainAll(jobIDsByAvgNormScore);
					finaljobIDS.addAll(jobIDS);
					finaljobIdsflag=true;
				}
				
				//New Change 
				List<Integer> statusFilterJobIds= new  ArrayList<Integer>();
				boolean onlyStatus=false;
				if(normScoreflag==false && statusflag==true){
					onlyStatus=true;
					statusFilterJobIds = jobForTeacherDAO.findJobIdsbyJFTID(jobOrdersByJobStatuses);
				}
				if(onlyStatus==true && finaljobIdsflag==true){
					finaljobIDS.retainAll(statusFilterJobIds);
					
				}else if(onlyStatus==true && finaljobIdsflag==false){
					finaljobIDS.addAll(statusFilterJobIds);
					finaljobIdsflag=true;
				}
				
				/*if(finaljobIdsflag==true && statusflag==true){
					finaljobIDS.retainAll(jobOrdersByJobStatuses);
					finaljobIdsflag=true;
				}
				else if(finaljobIdsflag==false && statusflag==true){
					finaljobIDS.addAll(jobOrdersByJobStatuses);
					finaljobIdsflag=true;
				}*/
			
				//SchoolId Filter
			    boolean schoolFlag=false;
				if(schoolId!=0)
				{
				  schoolFlag=true;
				  SchoolMaster  sclMaster=null;
				  sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
				  lstSchoolJobOrderIds = schoolInJobOrderDAO.findAllJobOrderBySchool(sclMaster);
				}	
				
				if(finaljobIdsflag==true && schoolFlag==true){
					finaljobIDS.retainAll(lstSchoolJobOrderIds);
					finaljobIdsflag=true;
				}
				else if(finaljobIdsflag==false && schoolFlag==true){
					finaljobIDS.addAll(lstSchoolJobOrderIds);
					finaljobIdsflag=true;
				}
				
				List<JobOrder> criteriaMixJobOrders=new ArrayList<JobOrder>();
				if(entityID==2 || entityID==1)
				{
				   if(finaljobIdsflag==true && finaljobIDS!=null &&  finaljobIDS.size()==0){
					  lstJobOrders=new ArrayList<JobOrder>();
					}
				  else
				    {
					  criteriaMixJobOrders = jobOrderDAO.findJobOrderbyWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start,noOfRowInPage,endfromDate,endtoDate,sfromDate, stoDate,finaljobIDS,nbyAflag,nbyAJobIds,exportcheck);						
					  
					  if(criteriaMixJobOrders!=null && criteriaMixJobOrders.size()>0)
					    {
					      if(checkflag){
	   						lstJobOrders.retainAll(criteriaMixJobOrders);
	   					  }else{
	   						lstJobOrders.addAll(criteriaMixJobOrders);
	   						checkflag=true;
	   					  }
					    }else{
					    	 lstJobOrders=new ArrayList<JobOrder>();
					    }
					 }
				 }
				
			List<DistrictMaster> districtMasters = new ArrayList<DistrictMaster>();
			for(JobOrder job : lstJobOrders){
				districtMasters.add(job.getDistrictMaster());
			}
				
			Map<Integer, Map<String,Integer>> mapsscountwithJob= new HashMap<Integer, Map<String,Integer>>();
			Map<Integer,String> mapSpecificStatus = new HashMap<Integer, String>();
			List<SecondaryStatus> secondaryStatusList = new ArrayList<SecondaryStatus>();
			
			String []statusArray={"scomp" , "ecomp" , "vcomp"};
		    List<StatusMaster> lstStatusMasters = Utility.getStaticMasters(statusArray);
		    if(districtMasters!=null && districtMasters.size()>0)
		     secondaryStatusList= secondaryStatusDAO.findSecondaryStatusforSpecificStatusListBYDistricts(districtMasters,lstStatusMasters);
		    if(secondaryStatusList!=null && secondaryStatusList.size()>0)
		    for(SecondaryStatus ss : secondaryStatusList)
		     {
		    	mapSpecificStatus.put(ss.getStatusMaster().getStatusId(), ss.getSecondaryStatusName());
		     }
				
			List list = new ArrayList();	
			if(lstJobOrders!=null && lstJobOrders.size()>0)
			{
			   list= jobForTeacherDAO.fintApplicntsStatusByJob(lstJobOrders);
			}
			 Iterator itr = list.iterator();
				if(list.size()>0){
					while(itr.hasNext()){
						Object[] obj = (Object[]) itr.next();
						JobOrder jobOrder=(JobOrder)obj[0];
						
						SecondaryStatus secondaryStatus= null;
						StatusMaster statusMaster= null;
						if(obj[1]!=null)
							secondaryStatus=(SecondaryStatus) obj[1];
						
						statusMaster=(StatusMaster) obj[2];
						Integer secCount= (Integer) obj[3];
						Integer statusCount = (Integer) obj[4];
						
						
						if(secondaryStatus!=null && statusMaster==null){
							Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
							if(countValue==null){
								Map<String,Integer> tempcountVal = new TreeMap<String, Integer>();
								tempcountVal.put(secondaryStatus.getSecondaryStatusName(), secCount);
								mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
							}else{
								if(countValue.get(secondaryStatus.getSecondaryStatusName())!=null){
									secCount = countValue.get(secondaryStatus.getSecondaryStatusName())+secCount;
								}
								countValue.put(secondaryStatus.getSecondaryStatusName(), secCount);
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							}
						}
						
						if(secondaryStatus==null && statusMaster!=null){
							String sDidplayStatusName="";
							if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
								sDidplayStatusName="Available"; 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
								sDidplayStatusName="Timed Out"; 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
								sDidplayStatusName="Declined"; 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
								sDidplayStatusName="Withdrew"; 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
								sDidplayStatusName="Hired";
							
							else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
						    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						    }
								 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
								sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							}
							  
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
								 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							 }
							
							Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
							if(countValue==null){
								Map<String,Integer> tempcountVal = new TreeMap<String, Integer>();
								tempcountVal.put(sDidplayStatusName, statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
							}else{
								if(countValue.get(sDidplayStatusName)!=null){
									countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}else{
									countValue.put(sDidplayStatusName,statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}
							}
						}
						if(secondaryStatus!=null && statusMaster!=null){
							String sDidplayStatusName="";
							if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
								sDidplayStatusName="Available"; 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
								sDidplayStatusName="Timed Out"; 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
								sDidplayStatusName="Declined"; 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
								sDidplayStatusName="Withdrew"; 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
								sDidplayStatusName="Hired";
							else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
						    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						    }
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
								sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							}
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
								 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							}
							Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
							if(countValue==null){
								Map<String,Integer> tempcountVal = new TreeMap<String, Integer>();
								tempcountVal.put(sDidplayStatusName, statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
							}else{
								if(countValue.get(sDidplayStatusName)!=null){
									countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}else{
									countValue.put(sDidplayStatusName,statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}
							}
						}
					}
				}
			List avgListbyJobList=new ArrayList();
			Map<Integer,Double> mapAvgEPI = new HashMap<Integer, Double>();
			if(lstJobOrders!=null && lstJobOrders.size()>0)
			{
			  avgListbyJobList= jobForTeacherDAO.countCandidatesNormHQLJobListWise(lstJobOrders);
			}
			
			List<Integer> statusIds = new ArrayList<Integer>();
			List<Integer> secstatusIds = new ArrayList<Integer>();
			//Map<jobId,Map<StatusName,avgNormScore>>
			Map<Integer, Map<String,Double>> mapAvgNormScoreStatusAndJob = new HashMap<Integer, Map<String,Double>>();
			for(Object objLst : avgListbyJobList)
            {
                Object[] obj = (Object[]) objLst;
                int ds = Utility.getIntValue(String.valueOf(obj[4]));
                int dss = Utility.getIntValue(String.valueOf(obj[3]));
                if(ds!=0 && dss==0)
                	statusIds.add(ds);
                if(ds==0 && dss!=0)
                	secstatusIds.add(dss);
                if(ds!=0 && dss!=0)
                	statusIds.add(ds);
                
            }
			List<StatusMaster> statusMasters =new ArrayList<StatusMaster>();
			List<SecondaryStatus> secondaryStatus = new ArrayList<SecondaryStatus>();
			Map<Integer,StatusMaster> mapStatusMaster = new HashMap<Integer, StatusMaster>();
			Map<Integer,SecondaryStatus> mapSecondaryStatus =  new HashMap<Integer, SecondaryStatus>();
			
			if(statusIds!=null && statusIds.size()>0)
			{
				Criterion criterion = Restrictions.in("statusId", statusIds);
				statusMasters = statusMasterDAO.findByCriteria(criterion);
			}
			if(statusMasters!=null && statusMasters.size()>0){
				for(StatusMaster sm :statusMasters){
					mapStatusMaster.put(sm.getStatusId(), sm);
				}
			}
				
			if(secstatusIds!=null && secstatusIds.size()>0)
			{
				Criterion criterion = Restrictions.in("secondaryStatusId", secstatusIds);
				secondaryStatus = secondaryStatusDAO.findByCriteria(criterion);
			}
			if(secondaryStatus!=null && secondaryStatus.size()>0){
				for(SecondaryStatus ssm :secondaryStatus){
					mapSecondaryStatus.put(ssm.getSecondaryStatusId(), ssm);
				}
			}	
			for(Object objLst:avgListbyJobList)
            {
                float avg;
                Object[] obj = (Object[]) objLst;
                int normSum=Integer.parseInt(String.valueOf(obj[1]));
                int total =Integer.parseInt(String.valueOf(obj[2]));
                avg  =(float)normSum/(float)total;
                int jobid = Utility.getIntValue(String.valueOf(obj[0]));
               
                int ds = Utility.getIntValue(String.valueOf(obj[4]));
                int dss = Utility.getIntValue(String.valueOf(obj[3]));
                StatusMaster pojoS = new StatusMaster();
                SecondaryStatus pojoSS = new SecondaryStatus();
                if(ds!=0 && dss==0)
                {
                	pojoS=mapStatusMaster.get((Integer)ds);
                	String sDidplayStatusName="";
					if(pojoS.getStatusShortName().equalsIgnoreCase("comp")){
						sDidplayStatusName="Available"; 
					}else if(pojoS.getStatusShortName().equalsIgnoreCase("rem")){
						sDidplayStatusName=pojoS.getStatus();
					}else if(pojoS.getStatusShortName().equalsIgnoreCase("icomp")){
						sDidplayStatusName=pojoS.getStatus();
					}else if(pojoS.getStatusShortName().equalsIgnoreCase("vlt")){
						sDidplayStatusName="Timed Out"; 
					}else if(pojoS.getStatusShortName().equalsIgnoreCase("dcln"))
						sDidplayStatusName="Declined"; 
					else if(pojoS.getStatusShortName().equalsIgnoreCase("widrw"))
						sDidplayStatusName="Withdrew"; 
					else if(pojoS.getStatusShortName().equalsIgnoreCase("hird"))
						sDidplayStatusName="Hired";
					
					else  if(pojoS.getStatusShortName().equalsIgnoreCase("scomp")){
				    	sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
				    }
					else if(pojoS.getStatusShortName().equalsIgnoreCase("ecomp")){
						sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
					}
					else if(pojoS.getStatusShortName().equalsIgnoreCase("vcomp")){
						 sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
					 }
					
					Map<String,Double> countValue = mapAvgNormScoreStatusAndJob.get(jobid);
					if(countValue==null){
						Map<String,Double> tempcountVal = new TreeMap<String, Double>();
						tempcountVal.put(sDidplayStatusName, ((double) Math.round(avg * 100) / 100));
						mapAvgNormScoreStatusAndJob.put(jobid, tempcountVal);
					}else{
						countValue.put(sDidplayStatusName,((double) Math.round(avg * 100) / 100));
						mapAvgNormScoreStatusAndJob.put(jobid, countValue);
						}
                }
                if(ds==0 && dss!=0)
                {
                	pojoSS=mapSecondaryStatus.get((Integer)dss);
                	Map<String,Double> countValue = mapAvgNormScoreStatusAndJob.get(jobid);
					if(countValue==null){
						Map<String,Double> tempcountVal = new TreeMap<String, Double>();
						tempcountVal.put(pojoSS.getSecondaryStatusName(), ((double) Math.round(avg * 100) / 100));
						mapAvgNormScoreStatusAndJob.put(jobid, tempcountVal);
					}else{
						countValue.put(pojoSS.getSecondaryStatusName(), ((double) Math.round(avg * 100) / 100));
						mapAvgNormScoreStatusAndJob.put(jobid, countValue);
					}
                }
                if(ds!=0 && dss!=0)
                {
                	pojoS=mapStatusMaster.get((Integer)ds);
                	String sDidplayStatusName="";
					if(pojoS.getStatusShortName().equalsIgnoreCase("comp")){
						sDidplayStatusName="Available"; 
					}else if(pojoS.getStatusShortName().equalsIgnoreCase("rem")){
						sDidplayStatusName=pojoS.getStatus();
					}else if(pojoS.getStatusShortName().equalsIgnoreCase("icomp")){
						sDidplayStatusName=pojoS.getStatus();
					}else if(pojoS.getStatusShortName().equalsIgnoreCase("vlt")){
						sDidplayStatusName="Timed Out"; 
					}else if(pojoS.getStatusShortName().equalsIgnoreCase("dcln"))
						sDidplayStatusName="Declined"; 
					else if(pojoS.getStatusShortName().equalsIgnoreCase("widrw"))
						sDidplayStatusName="Withdrew"; 
					else if(pojoS.getStatusShortName().equalsIgnoreCase("hird"))
						sDidplayStatusName="Hired";
					else  if(pojoS.getStatusShortName().equalsIgnoreCase("scomp")){
				    	sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
				    }
					else if(pojoS.getStatusShortName().equalsIgnoreCase("ecomp")){
						sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
					}
					else if(pojoS.getStatusShortName().equalsIgnoreCase("vcomp")){
						 sDidplayStatusName=mapSpecificStatus.get(pojoS.getStatusId());
					 }
					
					Map<String,Double> countValue = mapAvgNormScoreStatusAndJob.get(jobid);
					if(countValue==null){
						Map<String,Double> tempcountVal = new TreeMap<String, Double>();
						tempcountVal.put(sDidplayStatusName, ((double) Math.round(avg * 100) / 100));
						mapAvgNormScoreStatusAndJob.put(jobid, tempcountVal);
					}else{
						countValue.put(sDidplayStatusName,((double) Math.round(avg * 100) / 100));
						mapAvgNormScoreStatusAndJob.put(jobid, countValue);
						}
                }
            }
			tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>"+Utility.getLocaleValuePropByKey("headJoLiWithAppCoun", locale)+"</div><br/>");
			
			tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");
			
			tmRecords.append("<table  id='tblGridPrint' width='100%' border='1'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			tmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblDistrictName", locale)+"</th>");
    
			tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblJobId", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoTil", locale)+"</th>");
			
			tmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblPostingDate", locale)+"</th>");
			
			tmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("msgExpirationDate", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblCandStatus", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("msgNumberofApplications", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblAvNoSco", locale)+"</th>");
			
		
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(lstJobOrders.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='left' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			}
		
			if(lstJobOrders.size()>0){
			 for(JobOrder job:lstJobOrders) 
			 {
				 tmRecords.append("<tr>");	
				 
				
				 
				tmRecords.append("<td style='font-size:12px;'>"+job.getDistrictMaster().getDistrictName()+"</td>");
				 
				tmRecords.append("<td style='font-size:12px;'>"+job.getJobId()+"</td>");
				tmRecords.append("<td style='font-size:12px;'>"+job.getJobTitle()+"</td>");
				tmRecords.append("<td style='font-size:12px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobStartDate())+",12:01 AM</td>");
				
				if(Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobEndDate()).equals("Dec 25, 2099"))
				  tmRecords.append("<td style='font-size:12px;'>"+Utility.getLocaleValuePropByKey("lblUntilfilled", locale)+"</td>");
				else
				   tmRecords.append("<td style='font-size:12px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(job.getJobEndDate())+",11:59 PM</td>");
				 
				//"+Utility.getLocaleValuePropByKey("lblCandStatus", locale)+" vs Number of Candidate 
				Map<String, Integer> mapsecoundryStatus = mapsscountwithJob.get(job.getJobId());
				 if(mapsecoundryStatus==null){
				   tmRecords.append("<td style='font-size:12px;'>-</td>");
				   tmRecords.append("<td style='font-size:12px;'>-</td>");
				 }else{
					if(statusflag &&  statusNames!=null && !ArrayUtils.contains(statusNames,"0")){
				    tmRecords.append("<td style='font-size:12px;'>");
					for(Map.Entry<String,Integer> entry : mapsecoundryStatus.entrySet())
					{
					 tmRecords.append("<div style='font-size:12px;'>");
					  if(ArrayUtils.contains(statusNames,entry.getKey()))
						 tmRecords.append(entry.getKey());
					 tmRecords.append("</div>");
					}
				  tmRecords.append("</td>");
				  tmRecords.append("<td style='font-size:12px;'>");
					for(Map.Entry<String,Integer> entry : mapsecoundryStatus.entrySet())
					{
					 tmRecords.append("<div style='font-size:12px;'>");
					  if(ArrayUtils.contains(statusNames,entry.getKey()))
						tmRecords.append(entry.getValue());
					 tmRecords.append("</div>");
					}
				  tmRecords.append("</td>");
			 }else{
				    tmRecords.append("<td style='font-size:12px;'>");
					for(Map.Entry<String,Integer> entry : mapsecoundryStatus.entrySet())
					{
					 tmRecords.append("<div style='font-size:12px;'>");
						 tmRecords.append(entry.getKey());
					 tmRecords.append("</div>");
					}
				  tmRecords.append("</td>");
				  tmRecords.append("<td style='font-size:12px;'>");
					for(Map.Entry<String,Integer> entry : mapsecoundryStatus.entrySet())
					{
					 tmRecords.append("<div style='font-size:12px;'>");
						tmRecords.append(entry.getValue());
					 tmRecords.append("</div>");
					}
				  tmRecords.append("</td>");
				 
			 }
				}
			//for Average Norm Score 
					Map<String, Double> mapTempAvg = mapAvgNormScoreStatusAndJob.get(job.getJobId());
					if(mapTempAvg!=null && mapTempAvg.size()>0){
						tmRecords.append("<td>");
					   for(Map.Entry<String,Double> entry : mapTempAvg.entrySet()){
						   tmRecords.append(entry.getKey()+ "&nbsp-&nbsp"+ entry.getValue()+"</br>");
					   }
					   tmRecords.append("</td>");
					}else{
						tmRecords.append("<td>-</td>");
					}
				 
				 
			   tmRecords.append("</tr>");
			 }
			}
			try{
				try{
					mapStatusMaster.clear();//map
					mapStatusMaster=null;
				}catch (Exception e) {}
				try {
					mapSecondaryStatus.clear();//map
					mapSecondaryStatus=null;
				} catch (Exception e) {}
				try {
					statusMasters.clear();//list
					statusMasters=null;
				} catch (Exception e) {}
				try {
					statusMasters.clear();//list
					statusMasters=null;
				} catch (Exception e) {}	
				try {
					secondaryStatus.clear();//list
					secondaryStatus=null;
				} catch (Exception e) {}	
				try {
					mapAvgNormScoreStatusAndJob .clear();//map
					mapAvgNormScoreStatusAndJob =null;
				} catch (Exception e) {}	
				try {
					secstatusIds.clear();//list
					secstatusIds=null;
				} catch (Exception e) {}	
				try {
					statusIds.clear();//list
					statusIds=null;
				} catch (Exception e) {}	
				try {
					mapAvgEPI.clear();//map
					mapAvgEPI=null;
				} catch (Exception e) {}	
				try {
					avgListbyJobList.clear();//list
					avgListbyJobList=null;
				} catch (Exception e) {}	
				try {
					lstStatusMasters.clear();//list
					lstStatusMasters=null;
				} catch (Exception e) {}	
				try {
					secondaryStatusList.clear();//list
					secondaryStatusList=null;
				} catch (Exception e) {}	
				try {
					mapSpecificStatus.clear();//map
					mapSpecificStatus=null;
				} catch (Exception e) {}	
				try {
					mapsscountwithJob.clear();//map
					mapsscountwithJob=null;
				} catch (Exception e) {}	
				try {
					districtMasters.clear();//list
					districtMasters=null;
				} catch (Exception e) {}	
				try {
					criteriaMixJobOrders.clear();//list
					criteriaMixJobOrders=null;
				} catch (Exception e) {}	
				try {
					statusFilterJobIds.clear();//list
					statusFilterJobIds=null;
				} catch (Exception e) {}
				try {
					lstSecondaryStatus.clear();//list
					lstSecondaryStatus=null;
				} catch (Exception e) {}
				try {
					jobOrdersByJobStatuses.clear();  //list
					jobOrdersByJobStatuses=null;
				} catch (Exception e) {}
				try {
					lstJobOrders.clear();//list
					lstJobOrders=null;
				} catch (Exception e) {}
				try {
					lstSchoolJobOrderIds.clear();//list
					lstSchoolJobOrderIds=null;
				} catch (Exception e) {}
				try {
					jobIDS.clear();//list
					jobIDS=null;
				} catch (Exception e) {}
				try {
					jobIDsByAvgNormScore.clear();//list
					jobIDsByAvgNormScore=null;
				} catch (Exception e) {}
				try {
					finaljobIDS.clear();//list
					finaljobIDS=null;
				} catch (Exception e) {}
				
		}catch(Exception ex){
			ex.printStackTrace();
		}

		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){}
		
	 return tmRecords.toString();
  }
	
	private Map<String, String> getPositionsNumber(List<JobOrder> jobOrderList)
	{
		Map<String, String> jobPosition = new HashMap<String, String>();
		System.out.println("---------------------------Start getPositionsNumber-----------------------------");
		try
		{
	    	if(jobOrderList!=null && jobOrderList.size()>0)
	    	{
	    		Map<Integer, List<SchoolInJobOrder>> schoolInJobOrderWithJob = new HashMap<Integer, List<SchoolInJobOrder>>();
	    				    		
	    		//getting the schoolInJobOrder
	    		List<SchoolInJobOrder> schoolInJobOrderList = schoolInJobOrderDAO.findByCriteria(Restrictions.in("jobId", jobOrderList));
	    		if(schoolInJobOrderList!=null)
	    		{
	    			for(SchoolInJobOrder schoolInJobOrder : schoolInJobOrderList)
	    			{
	    				List<SchoolInJobOrder> sijo = schoolInJobOrderWithJob.get(schoolInJobOrder.getJobId().getJobId());
	    				if(sijo==null)
	    					sijo = new ArrayList<SchoolInJobOrder>();
	    				
	    				sijo.add(schoolInJobOrder);
	    				schoolInJobOrderWithJob.put(schoolInJobOrder.getJobId().getJobId(), sijo);
	    			}
	    		}
	    		
	    		//Now Calculating the position
	    		for(JobOrder jobOrder : jobOrderList)
	    		{
	    			float totalPosition=0;
	    			
	    			if(jobOrder.getNoOfExpHires()!=null)
	    				totalPosition+= jobOrder.getNoOfExpHires();
	    			else
	    			{
	    				List<SchoolInJobOrder> sijo = schoolInJobOrderWithJob.get(jobOrder.getJobId());
	    				if(sijo!=null)
	    					for(SchoolInJobOrder sjob : sijo)
	    						totalPosition+=	sjob.getNoOfSchoolExpHires()==null? 0 : sjob.getNoOfSchoolExpHires();
	    			}
	    			
	    			jobPosition.put(jobOrder.getJobId().toString(), ""+totalPosition);
	    			System.out.println("Job Id:- "+jobOrder.getJobId()+", totalPosition:- "+totalPosition+", jobOrder.getNoOfExpHires():- "+jobOrder.getNoOfExpHires()+", ");
	    		}
	    	}
	    }
		catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("---------------------------End getPositionsNumber-----------------------------");
		return jobPosition;
	}
	
}


