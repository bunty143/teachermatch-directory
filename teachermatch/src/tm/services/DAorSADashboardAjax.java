package tm.services;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.services.charts.ChartService;
import tm.utility.Utility;

public class DAorSADashboardAjax {
	
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;

	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
	{
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) 
	{
		this.schoolMasterDAO = schoolMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void SchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) 
	{
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;

	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired 
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO) {
		this.teacherExperienceDAO = teacherExperienceDAO;
	}

	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	public void setTeacherAcademicsDAO(TeacherAcademicsDAO teacherAcademicsDAO) {
		this.teacherAcademicsDAO = teacherAcademicsDAO;
	}
	
	
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String getDashboardSnapshot()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(cal.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(cal.YEAR), 11,31,0,0,0);
		Date eDate=cal.getTime();

		DistrictMaster districtMaster			=	null;
		SchoolMaster schoolMaster				=	null;
		List <JobForTeacher> jobForTeacher		=	null;
		Criterion criterion						=	Restrictions.eq("createdForEntity",userSession.getEntityType());
		Criterion criterion1					=	Restrictions.eq("status","A");
		Criterion criterion2					=	Restrictions.between("createdDateTime", sDate, eDate);
		
		StringBuilder sb = new StringBuilder();

		String avgTime = "NA";
		
		int totalJobOrdersPosted = 0;
		int totalApplicants = 0;
		int totalHired = 0;
		int averageTime = 0;
		int totalOpenJoborder = 0;
		int currentlyActiveApplicants = 0;
		int percentageHired					=	0;
		
		if(userSession.getDistrictId().getDistrictId()!=0 && userSession.getEntityType()==2)
		{
			/*=========== ============= Total District Job Orders Functionality============================================*/
			System.out.println("\n ======== District Id  from session "+userSession.getDistrictId().getDistrictId());
			districtMaster						=	districtMasterDAO.findById(userSession.getDistrictId().getDistrictId(), false, false);

			Criterion criterion3				=	Restrictions.eq("districtMaster",districtMaster);
			//List<JobOrder> jobOrder				=	jobOrderDAO.findByCriteria(criterion1,criterion2,criterion3);
			List<JobOrder> jobOrder				=	jobOrderDAO.findByCriteria(criterion2,criterion3);
			System.out.println("\n ============= Total District Job Orders Posted "+jobOrder.size());
			
			totalJobOrdersPosted = jobOrder.size();
			
			/*==================== Total Number of Applicants From Districts ==========================*/
			if(jobOrder.size()!=0)
			{
				jobForTeacher					=	jobForTeacherDAO.findJFTApplicantsbyJobOrder(jobOrder);
				System.out.println("\n ======= Total Number of Applicants From Districts  "+jobForTeacher.size());
				totalApplicants = jobForTeacher.size();

				/* ============== Total Number of Hires ==============*/
				int countForHiredApplicant			=	0;
				int sumOfdays						=	0;
				for(JobForTeacher jobForTeacherDetail:jobForTeacher)
				{
					if(jobForTeacherDetail.getStatus().getStatusId()==6)
					{
						countForHiredApplicant++;
						long days = 0;
						if(jobForTeacherDetail.getUpdatedDate()!=null)
						{
							days = (jobForTeacherDetail.getUpdatedDate().getTime()-jobForTeacherDetail.getJobId().getJobStartDate().getTime())/ (24 * 60 * 60 * 1000); 
							sumOfdays	+=	days;
						}	

					}
				}

				totalHired = countForHiredApplicant;

				/*=========== Hiring Selectivity ================= */
				if(countForHiredApplicant!=0)
				{
					percentageHired	=	((countForHiredApplicant*100)/jobForTeacher.size());
					averageTime	= sumOfdays/countForHiredApplicant;
				}
				else
				{
					percentageHired = 0;
					averageTime = 0;
				}
				
				Criterion criterion4						=	Restrictions.eq("jobStatus","O");
				Criterion criterion5						=	Restrictions.le("jobStartDate",new Date());
				Criterion criterion6						=	Restrictions.ge("jobEndDate",new Date());
				List<JobOrder> listCurrentlyOpenJobOrder	=	jobOrderDAO.findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);

				totalOpenJoborder = listCurrentlyOpenJobOrder.size();

				/*========= Currently Active Applicants =====================*/
				if(listCurrentlyOpenJobOrder.size()!=0)
				{
					List<JobForTeacher> activeApplicantjobForTeacher		=	jobForTeacherDAO.findJFTApplicantsbyJobOrder(listCurrentlyOpenJobOrder);
					currentlyActiveApplicants = activeApplicantjobForTeacher.size();
				}
				else
				{
					currentlyActiveApplicants = 0;
				}

			}
			else
			{
				totalApplicants = 0;
				totalHired = 0;
				averageTime = 0;
				totalOpenJoborder = 0;
				currentlyActiveApplicants = 0;
				percentageHired = 0;
			}
			
			
			if(averageTime!=0)
				avgTime=averageTime+" days";
			
		 	
		    
		}else
		{

			/*========= Checking School Entity Type 3 ================*/
			if(userSession.getEntityType()==3)
			{
				if(userSession.getSchoolId().getSchoolId()!=0)
				{
					/*=========== ============= Total School Job Orders Functionality============================================*/
					System.out.println("\n ======== School Id  from session "+userSession.getSchoolId().getSchoolId());
					schoolMaster										=	schoolMasterDAO.findById(userSession.getSchoolId().getSchoolId(), false, false);
					List<JobOrder> listTotalJoOrderPostedBySchool		=	schoolInJobOrderDAO.totalJobOrderPostedBySchool(schoolMaster);
					//System.out.println("\n ====== Total JoB Order Posted By School "+listTotalJoOrderPostedBySchool.size());
					totalJobOrdersPosted = listTotalJoOrderPostedBySchool.size();

					/*==================== Total Number of Applicants From School ==========================*/
					List<SchoolInJobOrder> listschoolInJobOrder			=	schoolInJobOrderDAO.findJobBySchool(schoolMaster);
					System.out.println("\n =======List School In Job Order size "+listschoolInJobOrder.size());

					List<JobOrder> jobOrders = new ArrayList<JobOrder>();
					List<JobOrder> listJobOrder	=new ArrayList<JobOrder>();
					if(listschoolInJobOrder.size()>0){
						for(SchoolInJobOrder scl : listschoolInJobOrder)
						{
							jobOrders.add(scl.getJobId());
							System.out.println("\n Job Id From SCHOOL In Job Order Dao "+scl.getJobId());
						}


						listJobOrder =	jobOrderDAO.findJobOrderlistFromJobId(jobOrders);
						for(JobOrder ljb : listJobOrder)
						{
							System.out.println("\n  Job Id From Job Order Dao"+ljb.getJobId());
						}
					}

					if(listJobOrder.size()!=0)
					{
						jobForTeacher									=	jobForTeacherDAO.findJFTApplicantsbyJobOrder(listJobOrder);
						System.out.println("\n ======= Total Number of Applicants From School  "+jobForTeacher.size());
						totalApplicants = jobForTeacher.size();
						/* ============== Total Number of School Hires ==============*/
						int countForHiredApplicant			=	0;
						int sumOfdays						=	0;
						long days							=	0;
						for(JobForTeacher jobForTeacherDetail:jobForTeacher)
						{
							if(jobForTeacherDetail.getStatus().getStatusId()==6)
							{
								countForHiredApplicant++;
								System.out.println("\n =========== countForHiredApplicant From School ===== "+countForHiredApplicant);
								/*========== Average time to Hire ================*/
								//long noOfDays	= Utility.getTimeDiff(jobForTeacherDetail.getUpdatedDate(),jobForTeacherDetail.getJobId().getJobStartDate());
								if(jobForTeacherDetail.getUpdatedDate()!=null)
								{
									days = (jobForTeacherDetail.getUpdatedDate().getTime()-jobForTeacherDetail.getJobId().getJobStartDate().getTime())/ (24 * 60 * 60 * 1000); 
									sumOfdays	+=	days;
								}
							}
						}
						
						totalHired = countForHiredApplicant;

						/*=========== Hiring Selectivity ================= */
						if(countForHiredApplicant!=0)
						{
							percentageHired	=	((countForHiredApplicant*100)/jobForTeacher.size());
							averageTime	= sumOfdays/countForHiredApplicant;
						}
						else
						{
							percentageHired = 0;
							averageTime = 0;
						}

						
						List<JobOrder> schoollistCurrentlyOpenJobOrder		=	jobOrderDAO.getCurrentlyOpenJobOrders(jobOrders);
						totalOpenJoborder = schoollistCurrentlyOpenJobOrder.size();
						/*========= Currently Schools Active Applicants =====================*/
						if(schoollistCurrentlyOpenJobOrder.size()!=0)
						{
							List<JobForTeacher> schoolsActiveApplicantjobForTeacher		=	jobForTeacherDAO.findJFTApplicantsbyJobOrder(schoollistCurrentlyOpenJobOrder);
							currentlyActiveApplicants = schoolsActiveApplicantjobForTeacher.size();
						}
						else
						{
							currentlyActiveApplicants = 0;
						}
					}
					else
					{
						totalApplicants=0;
						percentageHired=0;
						averageTime=0;
						currentlyActiveApplicants=0;
						totalOpenJoborder=0;
						totalHired=0;
					}
					
				}
			}
		
		}
	
		sb.append("<ul class='list1'>");
	    sb.append("<li><strong class='text-info'>"+Utility.getLocaleValuePropByKey("msgtoataljobOrderPosted", locale)+"</strong><span class='pull-right right_block'> "+totalJobOrdersPosted+"</span><br>");
	    sb.append(" <span>"+Utility.getLocaleValuePropByKey("msgToatlNoofjobinSys", locale)+"</span></li>");
		sb.append("<li><strong class='text-info'>"+Utility.getLocaleValuePropByKey("msgDAorSaDash3", locale)+"</strong><span class='pull-right right_block'> "+totalApplicants+"</span><br>");
		sb.append(" <span>"+Utility.getLocaleValuePropByKey("msgTotalNoofApplAcrJoborder", locale)+"</span></li>");
		sb.append("<li><strong class='text-info'>"+Utility.getLocaleValuePropByKey("msgDAorSaDash4", locale)+"</strong><span class='pull-right right_block'> "+totalHired+"</span><br>");
		sb.append(" <span>"+Utility.getLocaleValuePropByKey("msgTotalPeoplehired", locale)+"</span></li>");
		sb.append("<li><strong class='text-info'>"+Utility.getLocaleValuePropByKey("msgDAorSaDash5", locale)+"</strong><span class='pull-right right_block'> "+totalOpenJoborder+"</span><br>");
		sb.append(" <span>"+Utility.getLocaleValuePropByKey("msgTotalNoOfActiveJob", locale)+"</span></li>");
		sb.append("<li><strong class='text-info'>"+Utility.getLocaleValuePropByKey("msgDAorSaDash6", locale)+"</strong><span class='pull-right right_block'> "+currentlyActiveApplicants+"</span><br>");
		sb.append("<span>"+Utility.getLocaleValuePropByKey("msgDAorSaDash1", locale)+"</span></li>");
		sb.append("<li><strong class='text-info'>"+Utility.getLocaleValuePropByKey("msgDAorSaDash7", locale)+"</strong><span class='pull-right right_block'> "+percentageHired+"%</span><br>");
		sb.append("<span>"+Utility.getLocaleValuePropByKey("msgDAorSaDash2", locale)+"</span></li>");
		sb.append("<li><strong class='text-info'>"+Utility.getLocaleValuePropByKey("msgDAorSaDash8", locale)+"</strong><span class='pull-right right_block'> "+avgTime+"</span><br>");
		sb.append("<span>"+Utility.getLocaleValuePropByKey("msgDAorSaDash9", locale)+"</span></li>");
		sb.append("</ul>");
		sb.append("<br/>");
	    sb.append(" <br/>");
	
	    
		
		return sb.toString();
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String getDashboardCharts()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		StringBuilder sb = new StringBuilder();
		
		UserMaster userSession = (UserMaster) session.getAttribute("userMaster");
		String bottomLeftCornerImg ="";
		String topRightCornerImg ="";
		String bottomRightCornerImg ="";
		try 
		{
			
			/////////////////// charting started //////////////////////////////////
			try{
				int entityType = userSession.getEntityType();
				List<TeacherExperience> teacherExperienceList = null;
				List<JobOrder> jobOrders =new ArrayList<JobOrder>();

				if(entityType==3)
				{
					if(userSession.getSchoolId().getSchoolId()!=0)
					{
						List<SchoolInJobOrder> lsttotalJobOrderBySchool= null;
						lsttotalJobOrderBySchool = schoolInJobOrderDAO.totalJobOrdersForSchool(userSession.getSchoolId());
						if(lsttotalJobOrderBySchool.size()>0)
						{
							List<Integer> jobs = new ArrayList<Integer>();

							for(SchoolInJobOrder schoolInJobOrder : lsttotalJobOrderBySchool)
							{
								jobs.add(schoolInJobOrder.getJobId().getJobId());
							}
							jobOrders = jobOrderDAO.getAllJobOrdersOfTheYearForSchool(jobs);
						}

					}
				}else if(entityType==2)
				{
					jobOrders = jobOrderDAO.getAllJobOrdersOfTheYear(userSession.getDistrictId());
				}

				ChartService chartService =null;
				int withCertifictionPercentage = 0;
				int withOutCertifictionPercentage = 0;

				int bachelorPercentage = 0;
				int nonbachelorPercentage = 0;

				int le2Percent = 0;
				int gt2le3Percent = 0;
				int gt3le4Percent = 0;
				int gt4Percent = 0;

				if(jobOrders.size()>0)
				{
					List<JobForTeacher>  jobForTeachers =jobForTeacherDAO.findJFTApplicantsbyJobOrder(jobOrders);
					Set<JobForTeacher> s = new HashSet<JobForTeacher>();
					s.addAll(jobForTeachers);

					Set<TeacherDetail>  teachersHired = new HashSet<TeacherDetail>(); 
					List<TeacherDetail>  teachersHiredList = new ArrayList<TeacherDetail>(); 
					if(jobForTeachers.size()>0)
					{

						for(JobForTeacher jFt : jobForTeachers)
						{
							if(jFt.getStatus().getStatusShortName().equalsIgnoreCase("hird"))
								teachersHired.add(jFt.getTeacherId());

						}
						if(teachersHired.size()>0)
						{
							teachersHiredList.addAll(teachersHired);
							teacherExperienceList = teacherExperienceDAO.findExperienceForTeachers(teachersHiredList);
							int totalExpCont = 0;
							int nationalBoardCertHolderCount = 0;
							int nationalBoardCertNotHolderCont = 0;
							if(teacherExperienceList.size()>0)
							{
								for(TeacherExperience teacherExperience : teacherExperienceList)
								{
									if(teacherExperience.getNationalBoardCert())
										++nationalBoardCertHolderCount;
									else
										++nationalBoardCertNotHolderCont;
								}
							}

							totalExpCont = nationalBoardCertHolderCount + nationalBoardCertNotHolderCont;

							if(totalExpCont!=0)
							{
								if(nationalBoardCertHolderCount!=0)
								{
									float dd = ((float)nationalBoardCertHolderCount/(float)totalExpCont)*100;
									withCertifictionPercentage = Math.round(dd);
								}

								if(nationalBoardCertNotHolderCont!=0)
								{
									float dd = ((float)nationalBoardCertNotHolderCont/(float)totalExpCont)*100;
									withOutCertifictionPercentage = Math.round(dd);
								}
							}
							//System.out.println("PPPPPPPPPPPP nationalBoardCertHolderCount: "+ nationalBoardCertHolderCount);
						}
					}

					//System.out.println("PPPPPPPPPPPP withCertifictionPercentage: "+ withCertifictionPercentage);
					//System.out.println("PPPPPPPPPPPP withOutCertifictionPercentage: "+ withOutCertifictionPercentage);
					
					///////////////2 graph //////////////////////////////////////////
					
					if(jobForTeachers.size()>0)
					{
						Map<Integer,Double> tempmapGPA = new HashMap<Integer, Double>();

						Map<Integer,String> tempmap = new HashMap<Integer, String>();
						Set<TeacherDetail>  teachers = new HashSet<TeacherDetail>(); 
						List<TeacherDetail>  teachersList = new ArrayList<TeacherDetail>(); 


						for(JobForTeacher jFt : jobForTeachers)
						{
							teachers.add(jFt.getTeacherId());
							tempmap.put(jFt.getTeacherId().getTeacherId(), "");
						}

						teachersList.addAll(teachers);

						List<TeacherAcademics> teacherAcademicList = null;
						double cgpa = 0;
						if(teachers.size()>0)
						{
							teacherAcademicList = teacherAcademicsDAO.findAcademicsForTeachers(teachersList);

							int teacherId=0;
							String type="";
							String val="";
							String degreeType = "";
							Double cgpafrommap = null;
							double cgpafrommap1 = 0 ;
							for(TeacherAcademics teacherAcademic : teacherAcademicList)
							{
								teacherId = teacherAcademic.getTeacherId().getTeacherId();
								//System.out.println(teacherAcademic.getTeacherId().getTeacherId()+" getDegreeType "+teacherAcademic.getDegreeId().getDegreeType());
								degreeType = teacherAcademic.getDegreeId().getDegreeType();
								type = tempmap.get(teacherId);
								val=type==null?"":type;

								tempmap.put(teacherId, val+degreeType);

								if(degreeType.equalsIgnoreCase("B"))
								{
									cgpa = teacherAcademic.getGpaCumulative()==null?0:teacherAcademic.getGpaCumulative();
									cgpafrommap = tempmapGPA.get(teacherId);
									cgpafrommap1 = cgpafrommap==null?0:cgpafrommap;
									if(cgpa>=cgpafrommap1)
										tempmapGPA.put(teacherId,cgpa);
								}
							}
							int bachelorCount = 0;
							int nonbachelorCount = 0;
							int totalAcademicsCount = 0;

							String types = "";
							for(Map.Entry<Integer, String> entry : tempmap.entrySet())
							{
								//System.out.println(entry.getKey()+" "+entry.getValue());
								types = entry.getValue();
								if(!types.equals(""))
									if(types.contains("M") || types.contains("D"))
									{
										nonbachelorCount++;
									}else if(types.contains("B"))
										bachelorCount++;

							}
							totalAcademicsCount = bachelorCount+nonbachelorCount;

							if(totalAcademicsCount!=0)
							{
								if(bachelorCount!=0)
								{
									float dd =((float)bachelorCount/(float)totalAcademicsCount)*100;
									bachelorPercentage = Math.round(dd);
								}
								if(nonbachelorCount!=0)
								{
									float dd = ((float)nonbachelorCount/(float)totalAcademicsCount)*100;
									nonbachelorPercentage = Math.round(dd);
								}
							}
						}
						//System.out.println("PPPPPPPPPPPP withCertifictionPercentage: "+ withCertifictionPercentage);
						//System.out.println("PPPPPPPPPPPP withOutCertifictionPercentage: "+ withOutCertifictionPercentage);

						/////////////////////////////// 3rd graph ///////////////////////

						int le2Count = 0;
						int gt2le3Count = 0;
						int gt3le4Count = 0;
						int gt4Count = 0;

						int totalCGPA = 0;
						double cgpaval = 0;
						for(Map.Entry<Integer, Double> entry : tempmapGPA.entrySet())
						{
							//System.out.println(" kkk "+entry.getKey()+" "+entry.getValue());
							cgpaval = entry.getValue();
							if(cgpaval<=2)
								le2Count++;
							else if(cgpaval>2 && cgpaval<=3)
								gt2le3Count++;
							else if(cgpaval>3 && cgpaval<=4)
								gt3le4Count++;
							else if(cgpaval>4)
								gt4Count++;
						}

						totalCGPA = le2Count +  gt2le3Count + gt3le4Count + gt4Count;


						if(totalCGPA!=0)
						{
							if(le2Count!=0)
							{
								float dd = ((float)le2Count/(float)totalCGPA)*100;
								le2Percent = Math.round(dd);
							}
							if(gt2le3Count!=0)
							{
								float dd = ((float)gt2le3Count/(float)totalCGPA)*100;
								gt2le3Percent = Math.round(dd);
							}
							if(gt3le4Count!=0)
							{
								float dd = ((float)gt3le4Count/(float)totalCGPA)*100;
								gt3le4Percent = Math.round(dd);
							}
							if(gt4Count!=0)
							{
								float dd = ((float)gt4Count/(float)totalCGPA)*100;
								gt4Percent = Math.round(dd);
							}
						}

					}
				}
				//////////////////////////////1st graph ///////////////////////////////////////

				String time = String.valueOf(System.currentTimeMillis()).substring(6);
				String basePath = request.getRealPath("/")+"/dashboard/"+userSession.getDistrictId().getDistrictId();
				String fileName =time+"pieQual.png";

				File file = new File(basePath);
				if(!file.exists())
					file.mkdirs();

				chartService = new ChartService();
				String[] value = {Utility.getLocaleValuePropByKey("msgDAorSaDash11", locale), Utility.getLocaleValuePropByKey("msgDAorSaDash10", locale) };
				int[] values = {withCertifictionPercentage,(100 - withCertifictionPercentage)}; 
				
				if(withCertifictionPercentage==0 && withOutCertifictionPercentage==0)
				{
					int[] values1 = {0,0}; 
					chartService.createPieChart(value, values1, basePath+"/"+fileName,30);
				}else
					chartService.createPieChart(value, values, basePath+"/"+fileName,30);

				bottomLeftCornerImg="dashboard/"+userSession.getDistrictId().getDistrictId()+"/"+fileName;

				//////////////////////////////2nd graph ///////////////////////////////////////

				time = String.valueOf(System.currentTimeMillis()).substring(6);
				//request.getRealPath(path)
				basePath = request.getRealPath("/")+"/dashboard/"+userSession.getDistrictId().getDistrictId();
				//System.out.println("basePath : "+basePath);
				fileName =time+"pieAcad.png";

				file = new File(basePath);
				if(!file.exists())
					file.mkdirs();

				chartService = new ChartService();
				String[] value2 = {Utility.getLocaleValuePropByKey("msgDAorSaDash12", locale),Utility.getLocaleValuePropByKey("msgMastersorHigher", locale)};
				int[] values2 = {bachelorPercentage,(100 - bachelorPercentage)}; 
				
				if(bachelorPercentage==0 && nonbachelorPercentage==0)
				{
					int[] values1 = {0,0}; 
					chartService.createPieChart(value, values1, basePath+"/"+fileName,60);
				}else
					chartService.createPieChart(value2, values2, basePath+"/"+fileName,60);

				topRightCornerImg="dashboard/"+userSession.getDistrictId().getDistrictId()+"/"+fileName;

				//////////////////3rd graph ///////////////////////////////////////

				time = String.valueOf(System.currentTimeMillis()).substring(6);
				basePath = request.getRealPath("/")+"/dashboard/"+userSession.getDistrictId().getDistrictId();
				fileName =time+"pieCGPA.png";

				file = new File(basePath);
				if(!file.exists())
					file.mkdirs();

				chartService = new ChartService();
				String[] value3 = {""+Utility.getLocaleValuePropByKey("lblGPA", locale)+" <= 2.0",""+Utility.getLocaleValuePropByKey("lblGPA", locale)+" > 2.0 and <= 3.0",""+Utility.getLocaleValuePropByKey("lblGPA", locale)+" > 3.0 and <= 4.0",""+Utility.getLocaleValuePropByKey("lblGPA", locale)+" > 4.0"};

				int[] values3 = {le2Percent,gt2le3Percent,gt3le4Percent,gt4Percent}; 
				chartService.createPieChart(value3, values3, basePath+"/"+fileName,60);

				bottomRightCornerImg="dashboard/"+userSession.getDistrictId().getDistrictId()+"/"+fileName;
				////////////////////////////////////////////////////////////////
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		sb.append(topRightCornerImg+"###");
		sb.append(bottomLeftCornerImg+"###");
		sb.append(bottomRightCornerImg+"###");
		
		return sb.toString();
	}
	
}
