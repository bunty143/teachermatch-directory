package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

import tm.api.PaginationAndSortingAPI;
import tm.bean.JobForTeacher;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherSecondaryStatusDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificPortfolioAnswersDAO;
import tm.dao.master.DistrictSpecificPortfolioQuestionsDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.JFTEmailCompratorASC;
import tm.utility.JFTEmailCompratorDESC;
import tm.utility.JFTSSNCompratorASC;
import tm.utility.JFTSSNCompratorDESC;
import tm.utility.JobTitleCompratorASC;
import tm.utility.JobTitleCompratorDESC;
import tm.utility.LastNameCompratorASC;
import tm.utility.LastNameCompratorDESC;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;


public class ManageApplicantReportAjax 
{	
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private DistrictSpecificPortfolioQuestionsDAO districtSpecificPortfolioQuestionsDAO; 
	
	@Autowired
	private DistrictSpecificPortfolioAnswersDAO districtSpecificPortfolioAnswersDAO; 
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private TeacherSecondaryStatusDAO teacherSecondaryStatusDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO;
	
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	
	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	
	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	
public String displayApplicatRecords(Integer districtId,String noOfRow, String pageNo,String sortOrder,String sortOrderType, String sfromDate,String stoDate,String jobStatusAvail,String jobOrderId,boolean HNS,String empStatus)
{
	System.out.println("Job Status======"+jobStatusAvail);
	System.out.println("Job Id======"+jobOrderId);
	System.out.println("HNS======"+HNS);
	System.out.println("Employee status======"+empStatus);
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	StringBuffer dmRecords = new StringBuffer();
	int sortingcheck=0;
	
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			entityID=userMaster.getEntityType();
		}
		System.out.println(":::::::::districtId::::::::::::"+districtId);
		if(districtId!=null)
			 districtMaster = districtMasterDAO.findById(districtId, false, false);
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start =((pgNo-1)*noOfRowInPage);
		int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		int totalRecords=0;
		
		String responseText			=	"";
		
		 /** set default sorting fieldName **/
		String sortOrderFieldName="jobTitle";
		String sortOrderNoField="jobTitle";
		
		boolean deafultFlag=false;
		
		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal=null;

		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
		}

		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
	
		StatusMaster  statusMaster = WorkThreadServlet.statusMap.get("comp");
		StatusMaster  statusMasterHired = statusMasterDAO.findStatusByShortNameHired("hird");
		List<JobForTeacher> jobForTeacherList 		= 	new ArrayList<JobForTeacher>();
		List<TeacherDetail> teacherDetails			=	new ArrayList<TeacherDetail>();
		List<SecondaryStatus> secondaryStatusList	=	new ArrayList<SecondaryStatus>();
		List<JobForTeacher> jobForTeacherMainList	=	new ArrayList<JobForTeacher>();
		List<TeacherDetail> teacherDetailList		= 	new ArrayList<TeacherDetail>();
		List<TeacherPersonalInfo> teacherPersonalInfoList  = new ArrayList<TeacherPersonalInfo>();
		List<Integer> jobStatusList		= 	new ArrayList<Integer>();
		List<JobForTeacher> jobStatusList1		= 	new ArrayList<JobForTeacher>();

		
	if(districtMaster!=null){
		secondaryStatusList 	= 	secondaryStatusDAO.findSecondaryStatusListByStatusName("Ofcl Trans / Veteran Pref",districtMaster);
		
		Map<Integer, Boolean> mapTeacherHire=new HashMap<Integer, Boolean>();
		
		if(secondaryStatusList!=null && secondaryStatusList.size()>0)
		{
			try
			{
				jobStatusList1 	=  	jobForTeacherDAO.getApplicantByAvailableAndTransListHired(statusMasterHired,secondaryStatusList,districtMaster);
				
				for(JobForTeacher jobForTeacher:jobStatusList1)
					mapTeacherHire.put(jobForTeacher.getTeacherId().getTeacherId(), true);
				System.out.println("----------------Hired candidate list----------------"+ jobStatusList1.size());
				jobForTeacherList 	=  	jobForTeacherDAO.getApplicantByAvailableAndTransList(statusMaster,secondaryStatusList,districtMaster,sfromDate,stoDate,jobOrderId);
				System.out.println("----------------All candidate list----------------"+ jobForTeacherList.size());
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			if(jobForTeacherList.size()>0){
				try{
					for (JobForTeacher jt : jobForTeacherList) {
						teacherDetails.add(jt.getTeacherId());
					}
				}catch(Exception exception){
					exception.printStackTrace();
				}
			}
		
			if(teacherDetails!=null && teacherDetails.size()>0)
				teacherDetailList = teacherSecondaryStatusDAO.findTeacherListWithFilter(teacherDetails,districtMaster);
			
			Map<Integer,TeacherDetail> mapTeacher=new HashMap<Integer, TeacherDetail>();

			if(teacherDetailList.size()>0 && teacherDetailList!=null){
				try{
					for (TeacherDetail teacherDetail : teacherDetailList) {
						mapTeacher.put(teacherDetail.getTeacherId(),teacherDetail);
					}
				} catch(Exception exception){
					exception.printStackTrace();
				}
			}

			if(jobForTeacherList.size()>0 && jobForTeacherList!=null){
				try{
					for (JobForTeacher jobForTeacher : jobForTeacherList) {
						if(mapTeacher.get(jobForTeacher.getTeacherId().getTeacherId()) == null){
							jobForTeacherMainList.add(jobForTeacher);
						}
					}
				} catch(Exception exception){
					exception.printStackTrace();
				}
			}
		}

		Map<Integer, TeacherPersonalInfo> tpInfomap = new  HashMap<Integer, TeacherPersonalInfo>();
		if(teacherDetails.size()>0)
			teacherPersonalInfoList = teacherPersonalInfoDAO.findTeacherPInfoForTeacher(teacherDetails);
		try{
			for (TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList) {
				tpInfomap.put(teacherPersonalInfo.getTeacherId(), teacherPersonalInfo);
			}
		} catch(Exception exception){
			exception.printStackTrace();
		}
		
		//Manual Sorting
		if(sortOrder.equals("")){
	    	 for(JobForTeacher jf:jobForTeacherMainList) 
		    	 jf.setJobTitle(jf.getJobId().getJobTitle());
			
			  Collections.sort(jobForTeacherMainList, new JobTitleCompratorASC());
	     }
		if(sortOrder.equals("lastName")){    
		     for(JobForTeacher jf:jobForTeacherMainList) 
			 {
		    	 jf.setLastName(jf.getTeacherId().getLastName());
				
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherMainList, new LastNameCompratorASC());
			 else 
			  Collections.sort(jobForTeacherMainList, new LastNameCompratorDESC());
	     }
	     if(sortOrder.equals("jobTitle")){    
		     for(JobForTeacher jf:jobForTeacherMainList) 
			 {
		    	 jf.setJobTitle(jf.getJobId().getJobTitle());
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherMainList, new JobTitleCompratorASC());
			 else 
			  Collections.sort(jobForTeacherMainList, new JobTitleCompratorDESC());
	     }
	     
		if(sortOrder.equals("emailAddress")){    
		     for(JobForTeacher jf:jobForTeacherMainList) 
			 {
		    	 jf.setEmailId(jf.getTeacherId().getEmailAddress());
				
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherMainList, new JFTEmailCompratorASC());
			 else 
			  Collections.sort(jobForTeacherMainList, new JFTEmailCompratorDESC());
	     }
		
		if(sortOrder.equals("SSN")){
		     for(JobForTeacher jf:jobForTeacherMainList) {
		    	 TeacherPersonalInfo teacherPersonalInfo = 	tpInfomap.get(jf.getTeacherId().getTeacherId());
		    	 if(teacherPersonalInfo!=null){
			    	 if(teacherPersonalInfo.getSSN()!= null && !teacherPersonalInfo.getSSN().equals("")){
			    		jf.setSsn(teacherPersonalInfo.getSSN());
			    	 } else {
			    		jf.setSsn("");
			    	 }
		    	  } else {
			    		jf.setSsn("");
			      }
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherMainList, new JFTSSNCompratorASC());
			 else 
			  Collections.sort(jobForTeacherMainList, new JFTSSNCompratorDESC());
	     }

		// Map record for HNS (High Need School)
		Map<Integer, DistrictSpecificPortfolioAnswers> quesMap = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
		List<DistrictSpecificPortfolioAnswers> districtSpecificPortfolioAnswersList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions =  districtSpecificPortfolioQuestionsDAO.findById(1, false, false);
		
		Integer numberOfRecord = 0; 
		if(districtSpecificPortfolioQuestions != null){
			try{
				districtSpecificPortfolioAnswersList  =	 districtSpecificPortfolioAnswersDAO.findByDistrictAndQuestion(districtMaster, districtSpecificPortfolioQuestions);
				
				for(DistrictSpecificPortfolioAnswers allRecord : districtSpecificPortfolioAnswersList){
					quesMap.put(allRecord.getTeacherDetail().getTeacherId(), allRecord);
				}
				
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		
		dmRecords.append("<table id='applicantReportGridTable' border='0'>");
		dmRecords.append("<thead class='bg'>");
		
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgPoolNameGeozone", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
		dmRecords.append("<th valign='top'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblApplicantName", locale),sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
		dmRecords.append("<th valign='top'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgApplicantEmail", locale),sortOrderFieldName,"emailAddress",sortOrderTypeVal,pgNo);
		dmRecords.append("<th valign='top'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgApplicantSocial", locale),sortOrderFieldName,"SSN",sortOrderTypeVal,pgNo);
		dmRecords.append("<th valign='top' width='8%'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(""+Utility.getLocaleValuePropByKey("msgEmployee3", locale)+"&nbsp;#",sortOrderFieldName,"employeeNumber",sortOrderTypeVal,pgNo);
		dmRecords.append("<th valign='top' width='8%'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink("status",sortOrderFieldName,"candidateStatus",sortOrderTypeVal,pgNo);
		dmRecords.append("<th valign='top'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgActiveEmployee3", locale),sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgActiveEmployee3", locale)+"</th>");
		
		//responseText=PaginationAndSorting.responseSortingLink("Interested in working in HNS",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgInterestedHNS", locale)+"</th>");
		
		//responseText=PaginationAndSorting.responseSortingLink("Address",sortOrderFieldName,"addressLine1",sortOrderTypeVal,pgNo);
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblAdd", locale)+"</th>");
		
		//responseText=PaginationAndSorting.responseSortingLink("Phone&nbsp;#",sortOrderFieldName,"phoneNumber",sortOrderTypeVal,pgNo);
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("headPhone", locale)+"&nbsp;#</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgDateApplicantApplied", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgDateApplicantApplied", locale)+"</th>");
		dmRecords.append("</thead>");
		
		int iHire=0;
		
		if(jobForTeacherMainList.size()>0 && jobForTeacherMainList!=null){
			for(JobForTeacher allRecord : jobForTeacherMainList){
				if(mapTeacherHire==null || mapTeacherHire.get(allRecord.getTeacherId().getTeacherId())==null)
				{
					
				}
				else
				{
					iHire++;
				}
			}
			totalRecord =jobForTeacherMainList.size();
			
			totalRecord=totalRecord-iHire;
			}
		
		System.out.println(totalRecord+"::::::::::::::iHire::::::::::::"+iHire);
		List<JobForTeacher> finlalst=new ArrayList<JobForTeacher>();
		
		if(HNS)
		{
		for(JobForTeacher allRecord : jobForTeacherMainList){
			DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers = quesMap.get(allRecord.getTeacherId().getTeacherId());
			try{
					if(districtSpecificPortfolioAnswers!=null){
						if(districtSpecificPortfolioAnswers.getSelectedOptions().equals(1)){
							finlalst.add(allRecord);
						}
					}
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		jobForTeacherMainList.retainAll(finlalst);
		totalRecord =jobForTeacherMainList.size();
		//totalRecord=totalRecord-iHire;
		}
		
		List<JobForTeacher> finlalstStatus=new ArrayList<JobForTeacher>();
		List<JobForTeacher> finlalstStatusAvail=new ArrayList<JobForTeacher>();
		
		int ofcl=0;
		int avail=0;
		for(JobForTeacher allRecord : jobForTeacherMainList){
		if(allRecord.getSecondaryStatus()!=null){
			
			if((allRecord.getSecondaryStatus().getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref"))){
				finlalstStatus.add(allRecord);
				ofcl++;
			} else {
				finlalstStatusAvail.add(allRecord);
				avail++;
			}
		}else{
			finlalstStatusAvail.add(allRecord);
			avail++;
		}
		}
		if(jobStatusAvail.equalsIgnoreCase("Ofcl Trans / Veteran Pref"))			
		{
			System.out.println("finlalstStatus==="+finlalstStatus.size());
			jobForTeacherMainList.retainAll(finlalstStatus);
			totalRecord =jobForTeacherMainList.size();
		}
		if(jobStatusAvail.equalsIgnoreCase("Available"))
		{
			
			jobForTeacherMainList.retainAll(finlalstStatusAvail);
			totalRecord =jobForTeacherMainList.size();
		}
		
		List<JobForTeacher> empstatus=new ArrayList<JobForTeacher>();
		List<JobForTeacher> empstatusno=new ArrayList<JobForTeacher>();
		for(JobForTeacher allRecord : jobForTeacherMainList){
			
			try{
				empstatusno.add(allRecord);
				if(allRecord.getIsAffilated()!=null)
					if(allRecord.getIsAffilated().equals("1"))
					{
						empstatus.add(allRecord);
					}
						
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		if(empStatus.equalsIgnoreCase("yes"))
		{
			jobForTeacherMainList.retainAll(empstatus);	
			totalRecord =jobForTeacherMainList.size();
		}
		else if(empStatus.equalsIgnoreCase("no"))
		{
			jobForTeacherMainList.retainAll(empstatusno);
			totalRecord =jobForTeacherMainList.size();
		}
		
		if(totalRecord<end)
			end=totalRecord;
		
		int nn=0;
		List<JobForTeacher> activityRecordListMain = jobForTeacherMainList.subList(start,end);
		
		String fullName = "",teacherFname="",teacherLname="";
		int noOfRecordCheck=0;
		
		System.out.println(":::::::: jobForTeacherMainList ::::::::::"+jobForTeacherMainList.size());
		
		if(jobForTeacherMainList.size()>0 && jobForTeacherMainList!=null){
			for(JobForTeacher allRecord : activityRecordListMain){
				if(mapTeacherHire==null || mapTeacherHire.get(allRecord.getTeacherId().getTeacherId())==null)
				{
					String ssnumber			=	"";
					String employeeNumber	=	"";
					String status			=	"";
					String address			=	"";
					String phoneNumber		=	"";
					String activeEmployee	=	"";
					Integer answerId		=	0;
					String hns				=	"N/A";
					String jobStatus		=	"";
					teacherFname 	= 	allRecord.getTeacherId().getFirstName()==null?"":allRecord.getTeacherId().getFirstName();
					teacherLname 	= 	allRecord.getTeacherId().getLastName()==null?"":allRecord.getTeacherId().getLastName();
					fullName 		= 	teacherFname+" "+teacherLname;
					fullName		=	fullName.replace("'","\\'");
					
					TeacherPersonalInfo teacherPersonalInfo = 	tpInfomap.get(allRecord.getTeacherId().getTeacherId());
					try{ 
						if(teacherPersonalInfo!=null && teacherPersonalInfo.getSSN()!=null){
							ssnumber=Utility.decodeBase64(teacherPersonalInfo.getSSN());
							employeeNumber	= 	teacherPersonalInfo.getEmployeeNumber()==null?"":teacherPersonalInfo.getEmployeeNumber();
						}
						
						if(teacherPersonalInfo!=null){
							address			= 	teacherPersonalInfo.getAddressLine1();	
							address			=	address.replace("'","\\'");
							phoneNumber	= 	teacherPersonalInfo.getPhoneNumber()==null?"":teacherPersonalInfo.getPhoneNumber();
						}
						
						if(phoneNumber == "" || phoneNumber == null)
							phoneNumber = allRecord.getTeacherId().getPhoneNumber()==null?"":allRecord.getTeacherId().getPhoneNumber();
						
					}catch(Exception e){
						System.out.println(e);
					}
					
					if(allRecord.getTeacherId().getStatus().equals("1")){
						status = "Yes";
					} else if(allRecord.getTeacherId().getStatus().equals("0")){
						status = "No";
					}
					
					// Get Record for HNS (High Need School)
					DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers = quesMap.get(allRecord.getTeacherId().getTeacherId());
					try{
							if(districtSpecificPortfolioAnswers!=null){
								if(districtSpecificPortfolioAnswers.getSelectedOptions().equals(1)){
									hns = "Yes";
								} else if(districtSpecificPortfolioAnswers.getSelectedOptions().equals(2)){
									hns = "No";
								} else {
									hns = "N/A";
								}
							}
					} catch(Exception exception){
						exception.printStackTrace();
					}
					
					activeEmployee	=	"No";
					if(allRecord.getIsAffilated()!=null)
						if(allRecord.getIsAffilated().equals("1"))
							activeEmployee	=	"Yes";
					
						// Find Status
						if(allRecord.getSecondaryStatus()!=null){
							
							if(allRecord.getSecondaryStatus().getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref")){
								jobStatus = Utility.getLocaleValuePropByKey("msgOfclTransVeteranPref", locale);
							
							} else {
								
								jobStatus = Utility.getLocaleValuePropByKey("optAvble", locale);
							}
						}else{
						
							jobStatus = Utility.getLocaleValuePropByKey("optAvble", locale);
						}
					
										
					 	noOfRecordCheck++;
					 	nn++;
					 	dmRecords.append("<tr>");
						dmRecords.append("<td>");
						dmRecords.append(allRecord.getJobId().getJobTitle());
						dmRecords.append("</td>");
						
						dmRecords.append("<td>");
						dmRecords.append("<a class='profile' data-placement='above' href='javascript:void(0);' onclick=\"showProfileContentForTeacher(this,"+allRecord.getTeacherId().getTeacherId()+","+noOfRecordCheck+",'Applicant Report',event)\">");
						dmRecords.append(fullName);
						dmRecords.append("</a>");
						dmRecords.append("</td>");
					
						dmRecords.append("<td>");
						dmRecords.append("<a href='mailto:"+allRecord.getTeacherId().getEmailAddress()+"\'>");
						dmRecords.append(allRecord.getTeacherId().getEmailAddress());
						dmRecords.append("</a>");
						dmRecords.append("</td>");
							
						
						dmRecords.append("<td>");
						dmRecords.append(ssnumber);
						dmRecords.append("</td>");
						
						dmRecords.append("<td>");
						dmRecords.append(employeeNumber);
						dmRecords.append("</td>");
						
					
						dmRecords.append("<td>");
						dmRecords.append(jobStatus);
						dmRecords.append("</td>");
						
						dmRecords.append("<td>");
						dmRecords.append(activeEmployee);
						dmRecords.append("</td>");
						
						dmRecords.append("<td>");
						dmRecords.append(hns);
						dmRecords.append("</td>");
						
						dmRecords.append("<td>");
						dmRecords.append(address);
						dmRecords.append("</td>");
						
						dmRecords.append("<td>");
						dmRecords.append(phoneNumber);
						dmRecords.append("</td>");
						
						dmRecords.append("<td>");
						if(allRecord.getTeacherId().getCreatedDateTime() != null){
							dmRecords.append(Utility.convertDateAndTimeToUSformatOnlyDate(allRecord.getTeacherId().getCreatedDateTime()));
						}
						dmRecords.append("</td>");
					dmRecords.append("</tr>");
				
				}
				}
			System.out.println("nn="+nn);
		}else{
			dmRecords.append("<tr>");
			dmRecords.append("<td colspan='7'><B>");
			dmRecords.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
			dmRecords.append("</B></td>");
			dmRecords.append("</tr>");
		}
			dmRecords.append("<table width=\"945px;\">");
			dmRecords.append("<tr>");
			dmRecords.append("<td colspan=3>");
			dmRecords.append(PaginationAndSortingAPI.getPaginationString(request,totalRecord,noOfRow, pageNo));
			dmRecords.append("</td></tr>");
			dmRecords.append("</table>");
	   }
		
	} catch(Exception exception){
		exception.printStackTrace();
	}
	return dmRecords.toString();
 }


public String exportRecordIntoExcel(Integer districtId,String noOfRow, String pageNo,String sortOrder,String sortOrderType , String sfromDate,String stoDate,String jobStatusAvail,String jobOrderId,boolean HNS,String empStatus){
	
	System.out.println(":::::::::::::::::::::::::::: exportRecordIntoExcel ::::::::::::::::::::::::::::");

	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	StringBuffer dmRecords = new StringBuffer();
	int sortingcheck=0;
	String fileName = null;
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			entityID=userMaster.getEntityType();
		}
		
		System.out.println(":::::::::districtId::::::::::::"+districtId);
		if(districtId!=null)
			districtMaster = districtMasterDAO.findById(districtId, false, false);
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start =((pgNo-1)*noOfRowInPage);
		int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		int totalRecords=0;
		
		String responseText	=	"";
		
		 /** set default sorting fieldName **/
		String sortOrderFieldName="jobTitle";
		String sortOrderNoField="jobTitle";
		
		boolean deafultFlag=false;
		
		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal=null;

		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
		}

		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
	
		StatusMaster  statusMaster = WorkThreadServlet.statusMap.get("comp");
		StatusMaster  statusMasterHired = statusMasterDAO.findStatusByShortNameHired("hird");
		List<JobForTeacher> jobForTeacherList 		= 	new ArrayList<JobForTeacher>();
		List<TeacherDetail> teacherDetails			=	new ArrayList<TeacherDetail>();
		List<SecondaryStatus> secondaryStatusList	=	new ArrayList<SecondaryStatus>();
		List<JobForTeacher> jobForTeacherMainList	=	new ArrayList<JobForTeacher>();
		List<TeacherDetail> teacherDetailList		= 	new ArrayList<TeacherDetail>();
		List<TeacherPersonalInfo> teacherPersonalInfoList  = new ArrayList<TeacherPersonalInfo>();
		List<Integer> jobStatusList		= 	new ArrayList<Integer>();
		List<JobForTeacher> jobStatusList1		= 	new ArrayList<JobForTeacher>();
		
		
	if(districtMaster!=null){
		secondaryStatusList 	= 	secondaryStatusDAO.findSecondaryStatusListByStatusName("Ofcl Trans / Veteran Pref",districtMaster);
		Map<Integer, Boolean> mapTeacherHire=new HashMap<Integer, Boolean>();
		
		if(secondaryStatusList!=null && secondaryStatusList.size()>0)
		{
		
			try{
				
				jobStatusList1 	=  	jobForTeacherDAO.getApplicantByAvailableAndTransListHired(statusMasterHired,secondaryStatusList,districtMaster);  
				for(JobForTeacher jobForTeacher:jobStatusList1)
						mapTeacherHire.put(jobForTeacher.getTeacherId().getTeacherId(), true);
					System.out.println("----------------Hired candidate list----------------"+ jobStatusList1.size());
					jobForTeacherList 	=  	jobForTeacherDAO.getApplicantByAvailableAndTransList(statusMaster,secondaryStatusList,districtMaster,sfromDate,stoDate,jobOrderId);
					System.out.println("----------------All candidate list----------------"+ jobForTeacherList.size());
					//jobForTeacherList.removeAll(jobStatusList1);
				
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			if(jobForTeacherList.size()>0){
				try{
					for (JobForTeacher jt : jobForTeacherList) {
						teacherDetails.add(jt.getTeacherId());
					}
				}catch(Exception exception){
					exception.printStackTrace();
				}
			}
			
			if(teacherDetails!=null && teacherDetails.size()>0)
				teacherDetailList = teacherSecondaryStatusDAO.findTeacherListWithFilter(teacherDetails,districtMaster);
			
			Map<Integer,TeacherDetail> mapTeacher=new HashMap<Integer, TeacherDetail>();

			if(teacherDetailList.size()>0 && teacherDetailList!=null){
				try{
					for (TeacherDetail teacherDetail : teacherDetailList) {
						mapTeacher.put(teacherDetail.getTeacherId(),teacherDetail);
					}
				} catch(Exception exception){
					exception.printStackTrace();
				}
			}

			if(jobForTeacherList.size()>0 && jobForTeacherList!=null){
				try{
					for (JobForTeacher jobForTeacher : jobForTeacherList) {
						if(mapTeacher.get(jobForTeacher.getTeacherId().getTeacherId()) == null){
							jobForTeacherMainList.add(jobForTeacher);
						}
					}
				} catch(Exception exception){
					exception.printStackTrace();
				}
			}
		}

		Map<Integer, TeacherPersonalInfo> tpInfomap = new  HashMap<Integer, TeacherPersonalInfo>();
		
		if(teacherDetails.size()>0)
			teacherPersonalInfoList = teacherPersonalInfoDAO.findTeacherPInfoForTeacher(teacherDetails);
		try{
			for (TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList) {
				tpInfomap.put(teacherPersonalInfo.getTeacherId(), teacherPersonalInfo);
			}
		} catch(Exception exception){
			exception.printStackTrace();
		}
		
		//Manual Sorting
		if(sortOrder.equals("")){
	    	 for(JobForTeacher jf:jobForTeacherMainList) 
		    	 jf.setJobTitle(jf.getJobId().getJobTitle());
			
			  Collections.sort(jobForTeacherMainList, new JobTitleCompratorASC());
	     }
		if(sortOrder.equals("lastName")){    
		     for(JobForTeacher jf:jobForTeacherMainList) 
			 {
		    	 jf.setLastName(jf.getTeacherId().getLastName());
				
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherMainList, new LastNameCompratorASC());
			 else 
			  Collections.sort(jobForTeacherMainList, new LastNameCompratorDESC());
	     }
	     if(sortOrder.equals("jobTitle")){    
		     for(JobForTeacher jf:jobForTeacherMainList) 
			 {
		    	 jf.setJobTitle(jf.getJobId().getJobTitle());
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherMainList, new JobTitleCompratorASC());
			 else 
			  Collections.sort(jobForTeacherMainList, new JobTitleCompratorDESC());
	     }
	     
		if(sortOrder.equals("emailAddress")){    
		     for(JobForTeacher jf:jobForTeacherMainList) 
			 {
		    	 jf.setEmailId(jf.getTeacherId().getEmailAddress());
				
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherMainList, new JFTEmailCompratorASC());
			 else 
			  Collections.sort(jobForTeacherMainList, new JFTEmailCompratorDESC());
	     }
		
		if(sortOrder.equals("SSN")){
		     for(JobForTeacher jf:jobForTeacherMainList) {
		    	 TeacherPersonalInfo teacherPersonalInfo = 	tpInfomap.get(jf.getTeacherId().getTeacherId());
		    	 if(teacherPersonalInfo!=null){
			    	 if(teacherPersonalInfo.getSSN()!= null && !teacherPersonalInfo.getSSN().equals("")){
			    		jf.setSsn(teacherPersonalInfo.getSSN());
			    	 } else {
			    		jf.setSsn("");
			    	 }
		    	  } else {
			    		jf.setSsn("");
			      }
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherMainList, new JFTSSNCompratorASC());
			 else 
			  Collections.sort(jobForTeacherMainList, new JFTSSNCompratorDESC());
	     }

		// Map record for HNS (High Need School)
		Map<Integer, DistrictSpecificPortfolioAnswers> quesMap = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
		List<DistrictSpecificPortfolioAnswers> districtSpecificPortfolioAnswersList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions =  districtSpecificPortfolioQuestionsDAO.findById(1, false, false);
		
		Integer numberOfRecord = 0; 
		if(districtSpecificPortfolioQuestions != null){
			try{
				districtSpecificPortfolioAnswersList  =	 districtSpecificPortfolioAnswersDAO.findByDistrictAndQuestion(districtMaster, districtSpecificPortfolioQuestions);
				
				for(DistrictSpecificPortfolioAnswers allRecord : districtSpecificPortfolioAnswersList){
					quesMap.put(allRecord.getTeacherDetail().getTeacherId(), allRecord);
				}
				
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		/* EXPORT INTO EXCEL */
		
		String time = String.valueOf(System.currentTimeMillis()).substring(6);
		String basePath = request.getSession().getServletContext().getRealPath ("/")+"/applicantsNotContactedList";
		fileName ="applicantwithouttag"+time+".xls";
		
		File file = new File(basePath);
		if(!file.exists())
			file.mkdirs();
		
		Utility.deleteAllFileFromDir(basePath);
		file = new File(basePath+"/"+fileName);
		
		WorkbookSettings wbSettings = new WorkbookSettings();

		wbSettings.setLocale(new Locale("en", "EN"));

		WritableCellFormat timesBoldUnderline;
		WritableCellFormat header;
		WritableCellFormat headerBold;
		WritableCellFormat times;
		WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
		workbook.createSheet("notContactedList", 0);
		WritableSheet excelSheet = workbook.getSheet(0);
		WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
		// Define the cell format
		times = new WritableCellFormat(times10pt);
		// Lets automatically wrap the cells
		times.setWrap(true);

		// Create create a bold font with unterlines
		WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
		WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

		timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
		timesBoldUnderline.setAlignment(Alignment.CENTRE);
		// Lets automatically wrap the cells
		timesBoldUnderline.setWrap(true);

		header = new WritableCellFormat(times10ptBoldUnderline);
		headerBold = new WritableCellFormat(times10ptBoldUnderline);
		CellView cv = new CellView();
		cv.setFormat(times);
		cv.setFormat(timesBoldUnderline);
		cv.setAutosize(true);
		header.setBackground(Colour.GRAY_25);

		// Write a few headers
		excelSheet.mergeCells(0, 0, 3, 1);
		Label label;
		label = new Label(0, 0, Utility.getLocaleValuePropByKey("headAppNotCont", locale), timesBoldUnderline);
		excelSheet.addCell(label);
		excelSheet.mergeCells(0, 3, 3, 3);
		label = new Label(0, 3, "");
		excelSheet.addCell(label);
		excelSheet.getSettings().setDefaultColumnWidth(18);
		
		int k=11;
		int col=1;
		label = new Label(0, k, Utility.getLocaleValuePropByKey("msgPoolNameGeozone", locale),header); 
		excelSheet.addCell(label);
		label = new Label(1, k, Utility.getLocaleValuePropByKey("lblApplicantName", locale),header); 
		excelSheet.addCell(label);
		label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgApplicantEmail", locale),header); 
		excelSheet.addCell(label);
		label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgApplicantSocial", locale),header); 
		excelSheet.addCell(label);
		label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgEmployeeNo", locale),header); 
		excelSheet.addCell(label);
		label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblStatus", locale),header); 
		excelSheet.addCell(label);
		label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgActiveEmployee3", locale),header); 
		excelSheet.addCell(label);
		label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgInterestedHNS", locale),header); 
		excelSheet.addCell(label);
		label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblAdd", locale),header); 
		excelSheet.addCell(label);
		label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgPhoneNo", locale),header); 
		excelSheet.addCell(label);
		label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblDateApplied", locale),header); 
		excelSheet.addCell(label);
		
		k=k+1;
		if(jobForTeacherMainList.size()==0)
		{	
			excelSheet.mergeCells(0, k, 3, k);
			label = new Label(0, k, Utility.getLocaleValuePropByKey("msgNorecordfound", locale)); 
			excelSheet.addCell(label);
		}
		
		
List<JobForTeacher> finlalst=new ArrayList<JobForTeacher>();
		
		if(HNS)
		{
		for(JobForTeacher allRecord : jobForTeacherMainList){
			DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers = quesMap.get(allRecord.getTeacherId().getTeacherId());
			try{
					if(districtSpecificPortfolioAnswers!=null){
						if(districtSpecificPortfolioAnswers.getSelectedOptions().equals(1)){
							finlalst.add(allRecord);
						}
					}
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		jobForTeacherMainList.retainAll(finlalst);
		totalRecord =jobForTeacherMainList.size();
		//totalRecord=totalRecord-iHire;
		}
		
		List<JobForTeacher> finlalstStatus=new ArrayList<JobForTeacher>();
		List<JobForTeacher> finlalstStatusAvail=new ArrayList<JobForTeacher>();
		
		int ofcl=0;
		int avail=0;
		for(JobForTeacher allRecord : jobForTeacherMainList){
		if(allRecord.getSecondaryStatus()!=null){
			
			if((allRecord.getSecondaryStatus().getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref"))){
				finlalstStatus.add(allRecord);
				ofcl++;
			} else {
				finlalstStatusAvail.add(allRecord);
				avail++;
			}
		}else{
			finlalstStatusAvail.add(allRecord);
			avail++;
		}
		}
		if(jobStatusAvail.equalsIgnoreCase("Ofcl Trans / Veteran Pref"))			
		{
			jobForTeacherMainList.retainAll(finlalstStatus);
			totalRecord =jobForTeacherMainList.size();
		
		}
		if(jobStatusAvail.equalsIgnoreCase("Available"))
		{
			System.out.println("inside availbale");
			jobForTeacherMainList.retainAll(finlalstStatusAvail);
			totalRecord =jobForTeacherMainList.size();
			
		}
		
		List<JobForTeacher> empstatus=new ArrayList<JobForTeacher>();
		List<JobForTeacher> empstatusno=new ArrayList<JobForTeacher>();
		for(JobForTeacher allRecord : jobForTeacherMainList){
			
			try{
				empstatusno.add(allRecord);
				if(allRecord.getIsAffilated()!=null)
					if(allRecord.getIsAffilated().equals("1"))
					{
						empstatus.add(allRecord);
					}
						
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		if(empStatus.equalsIgnoreCase("yes"))
		{
			jobForTeacherMainList.retainAll(empstatus);	
			totalRecord =jobForTeacherMainList.size();
		}
		else if(empStatus.equalsIgnoreCase("no"))
		{
			jobForTeacherMainList.retainAll(empstatusno);
			totalRecord =jobForTeacherMainList.size();
		}
		
		String fullName = "",teacherFname="",teacherLname="";
		if(jobForTeacherMainList.size()>0 )
			for(JobForTeacher allRecord : jobForTeacherMainList){
				if(mapTeacherHire==null || mapTeacherHire.get(allRecord.getTeacherId().getTeacherId())==null)
				{
				
				String ssnumber			=	"";
				String employeeNumber	=	"";
				String status			=	"";
				String address			=	"";
				String phoneNumber		=	"";
				String activeEmployee	=	"";
				Integer answerId		=	0;
				String hns				=	"N/A";
				String jobStatus		=	"";
				teacherFname 	= 	allRecord.getTeacherId().getFirstName()==null?"":allRecord.getTeacherId().getFirstName();
				teacherLname 	= 	allRecord.getTeacherId().getLastName()==null?"":allRecord.getTeacherId().getLastName();
				fullName 		= 	teacherFname+" "+teacherLname;
				fullName		=	fullName.replace("'","\\'");
				
				TeacherPersonalInfo teacherPersonalInfo = 	tpInfomap.get(allRecord.getTeacherId().getTeacherId());
				try{ 
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getSSN()!=null){
						ssnumber=Utility.decodeBase64(teacherPersonalInfo.getSSN());
						employeeNumber	= 	teacherPersonalInfo.getEmployeeNumber()==null?"":teacherPersonalInfo.getEmployeeNumber();
					}
					
					if(teacherPersonalInfo!=null){
						address			= 	teacherPersonalInfo.getAddressLine1();	
						address			=	address.replace("'","\\'");
						phoneNumber		= 	teacherPersonalInfo.getPhoneNumber()==null?"":teacherPersonalInfo.getPhoneNumber();
					}
					
					if(phoneNumber == "" || phoneNumber == null)
						phoneNumber = allRecord.getTeacherId().getPhoneNumber()==null?"":allRecord.getTeacherId().getPhoneNumber();
					
				}catch(Exception e){
					System.out.println(e);
				}
				
				if(allRecord.getTeacherId().getStatus().equals("1")){
					status = "Yes";
				} else if(allRecord.getTeacherId().getStatus().equals("0")){
					status = "No";
				}
				
				// Get Record for HNS (High Need School)
				DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers = quesMap.get(allRecord.getTeacherId().getTeacherId());
				try{
						if(districtSpecificPortfolioAnswers!=null){
							if(districtSpecificPortfolioAnswers.getSelectedOptions().equals(1)){
								hns = "Yes";
							} else if(districtSpecificPortfolioAnswers.getSelectedOptions().equals(2)){
								hns = "No";
							} else {
								hns = "N/A";
							}
						}
				} catch(Exception exception){
					exception.printStackTrace();
				}
				
				activeEmployee	=	"No";
				if(allRecord.getIsAffilated()!=null)
					if(allRecord.getIsAffilated().equals("1"))
						activeEmployee	=	"Yes";
				
					// Find Status
					if(allRecord.getSecondaryStatus()!=null){
						if(allRecord.getSecondaryStatus().getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgOfclTransVeteranPref", locale))){
							jobStatus = Utility.getLocaleValuePropByKey("msgOfclTransVeteranPref", locale);
						} else {
							jobStatus = Utility.getLocaleValuePropByKey("optAvble", locale);
						}
					}else{
						jobStatus = Utility.getLocaleValuePropByKey("optAvble", locale);
					}
				
					col=1;
					label = new Label(0, k, allRecord.getJobId().getJobTitle());
					excelSheet.addCell(label);
					
					label = new Label(1, k,fullName); 
					excelSheet.addCell(label);
				
					label = new Label(++col, k, allRecord.getTeacherId().getEmailAddress()); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, ssnumber); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, employeeNumber); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, jobStatus); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, activeEmployee); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, hns);
					excelSheet.addCell(label);
					
					label = new Label(++col, k, address); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, phoneNumber); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.convertDateAndTimeToUSformatOnlyDate(allRecord.getTeacherId().getCreatedDateTime())); 
					excelSheet.addCell(label);

					 k++;
				}
			}
			workbook.write();
			workbook.close();
	 }
	}catch(Exception exception){
		exception.printStackTrace();
	}
	
	return fileName;
}

public String getRecordListPDF(Integer districtId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,boolean exportcheck, String sfromDate,String stoDate,String jobStatusAvail,String jobOrderId,boolean HNS,String empStatus)
{		
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	try
	{	
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
		}
		
		System.out.println(":::::::::districtId::::::::::::"+districtId);
		if(districtId!=null)
			districtMaster = districtMasterDAO.findById(districtId, false, false);
		
		String fontPath = request.getRealPath("/");
		BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

		int userId = userMaster.getUserId();
		
		String time 		= 	String.valueOf(System.currentTimeMillis()).substring(6);
		String basePath 	= 	context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
		String fileName 	=	time+"applicantsWithoutTag.pdf";

		File file = new File(basePath);
		if(!file.exists())
			file.mkdirs();

		Utility.deleteAllFileFromDir(basePath);
		System.out.println("user/"+userId+"/"+fileName);
		
		generateApplicantNotContactedPDReport(noOfRow,pageNo,sortOrder,sortOrderType,exportcheck,sfromDate,stoDate,jobStatusAvail, jobOrderId, HNS,empStatus,basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
		return "user/"+userId+"/"+fileName;
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return "ABV";
}

public boolean generateApplicantNotContactedPDReport(String noOfRow,String pageNo,String sortOrder,String sortOrderType,boolean exportcheck,String sfromDate,String stoDate,String jobStatusAvail,String jobOrderId,boolean HNS,String empStatus,String path,String realPath){
	
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
	
		int cellSize = 0;
		Document document			=	null;
		FileOutputStream fos 		= 	null;
		PdfWriter writer 			= 	null;
		Paragraph footerpara 		= 	null;
		HeaderFooter headerFooter 	= 	null;
		
		Font font8 			= 	null;
		Font font8Green 	= 	null;
		Font font8bold 		=	null;
		Font font9 			= 	null;
		Font font9bold 		= 	null;
		Font font10 		= 	null;
		Font font10_10 		= 	null;
		Font font10bold 	= 	null;
		Font font11 		= 	null;
		Font font11bold 	= 	null;
		Font font20bold 	=	null;
		Font font11b   		=	null;
		Color bluecolor 	=	null;
		Font font8bold_new 	= 	null;
		
		try{
			
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false" != null;
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				
				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}
				entityID=userMaster.getEntityType();
			}
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			int totalRecords=0;
			
			String responseText = "";
			
			 /** set default sorting fieldName **/
			String sortOrderFieldName=	"jobTitle";
			String sortOrderNoField	 =	"jobTitle";
			
			boolean deafultFlag=false;
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
		
			StatusMaster  statusMaster = WorkThreadServlet.statusMap.get("comp");
			StatusMaster  statusMasterHired = statusMasterDAO.findStatusByShortNameHired("hird");
			List<JobForTeacher> jobForTeacherList 		= 	new ArrayList<JobForTeacher>();
			List<TeacherDetail> teacherDetails			=	new ArrayList<TeacherDetail>();
			List<SecondaryStatus> secondaryStatusList	=	new ArrayList<SecondaryStatus>();
			List<JobForTeacher> jobForTeacherMainList	=	new ArrayList<JobForTeacher>();
			List<TeacherDetail> teacherDetailList		= 	new ArrayList<TeacherDetail>();
			List<TeacherPersonalInfo> teacherPersonalInfoList  = new ArrayList<TeacherPersonalInfo>();

			List<JobForTeacher> jobStatusList1		= 	new ArrayList<JobForTeacher>();
			
	if(districtMaster!=null){
			secondaryStatusList 	= 	secondaryStatusDAO.findSecondaryStatusListByStatusName("Ofcl Trans / Veteran Pref",districtMaster);
			Map<Integer, Boolean> mapTeacherHire=new HashMap<Integer, Boolean>();
			if(secondaryStatusList!=null && secondaryStatusList.size()>0)
			{
				
				try{
					jobStatusList1 	=  	jobForTeacherDAO.getApplicantByAvailableAndTransListHired(statusMasterHired,secondaryStatusList,districtMaster);
					
					for(JobForTeacher jobForTeacher:jobStatusList1)
						mapTeacherHire.put(jobForTeacher.getTeacherId().getTeacherId(), true);
					System.out.println("----------------Hired candidate list----------------"+ jobStatusList1.size());
				
					jobForTeacherList 	=  	jobForTeacherDAO.getApplicantByAvailableAndTransList(statusMaster,secondaryStatusList,districtMaster,sfromDate,stoDate,jobOrderId);
					System.out.println("----------------All candidate list----------------"+ jobForTeacherList.size());
					
					//jobForTeacherList.removeAll(jobStatusList1);
					
				}catch(Exception exception){
					exception.printStackTrace();
				}
				
				if(jobForTeacherList.size()>0){
					try{
						for (JobForTeacher jobForTeacher : jobForTeacherList) {
							teacherDetails.add(jobForTeacher.getTeacherId());
						}
					}catch(Exception exception){
						exception.printStackTrace();
					}
				}
				
				if(teacherDetails!=null && teacherDetails.size()>0)
					teacherDetailList = teacherSecondaryStatusDAO.findTeacherListWithFilter(teacherDetails,districtMaster);
				
				Map<Integer,TeacherDetail> mapTeacher=new HashMap<Integer, TeacherDetail>();

				if(teacherDetailList.size()>0 && teacherDetailList!=null){
					try{
						for (TeacherDetail teacherDetail : teacherDetailList) {
							mapTeacher.put(teacherDetail.getTeacherId(),teacherDetail);
						}
					} catch(Exception exception){
						exception.printStackTrace();
					}
				}

				if(jobForTeacherList.size()>0 && jobForTeacherList!=null){
					try{
						for (JobForTeacher jobForTeacher : jobForTeacherList) {
							if(mapTeacher.get(jobForTeacher.getTeacherId().getTeacherId()) == null){
								jobForTeacherMainList.add(jobForTeacher);
							}
						}
					} catch(Exception exception){
						exception.printStackTrace();
					}
				}
			}

			Map<Integer, TeacherPersonalInfo> tpInfomap = new  HashMap<Integer, TeacherPersonalInfo>();

			if(teacherDetails.size()>0)
				teacherPersonalInfoList = teacherPersonalInfoDAO.findTeacherPInfoForTeacher(teacherDetails);
			try{
				for (TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList) {
					tpInfomap.put(teacherPersonalInfo.getTeacherId(), teacherPersonalInfo);
				}
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			//Manual Sorting
			if(sortOrder.equals("")){
		    	 for(JobForTeacher jf:jobForTeacherMainList) 
			    	 jf.setJobTitle(jf.getJobId().getJobTitle());
				
				  Collections.sort(jobForTeacherMainList, new JobTitleCompratorASC());
		     }
			if(sortOrder.equals("lastName")){    
			     for(JobForTeacher jf:jobForTeacherMainList) 
				 {
			    	 jf.setLastName(jf.getTeacherId().getLastName());
					
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherMainList, new LastNameCompratorASC());
				 else 
				  Collections.sort(jobForTeacherMainList, new LastNameCompratorDESC());
		     }
		     if(sortOrder.equals("jobTitle")){    
			     for(JobForTeacher jf:jobForTeacherMainList) 
				 {
			    	 jf.setJobTitle(jf.getJobId().getJobTitle());
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherMainList, new JobTitleCompratorASC());
				 else 
				  Collections.sort(jobForTeacherMainList, new JobTitleCompratorDESC());
		     }
		     
			if(sortOrder.equals("emailAddress")){    
			     for(JobForTeacher jf:jobForTeacherMainList) 
				 {
			    	 jf.setEmailId(jf.getTeacherId().getEmailAddress());
					
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherMainList, new JFTEmailCompratorASC());
				 else 
				  Collections.sort(jobForTeacherMainList, new JFTEmailCompratorDESC());
		     }
			
			if(sortOrder.equals("SSN")){
			     for(JobForTeacher jf:jobForTeacherMainList) {
			    	 TeacherPersonalInfo teacherPersonalInfo = 	tpInfomap.get(jf.getTeacherId().getTeacherId());
			    	 if(teacherPersonalInfo!=null){
				    	 if(teacherPersonalInfo.getSSN()!= null && !teacherPersonalInfo.getSSN().equals("")){
				    		jf.setSsn(teacherPersonalInfo.getSSN());
				    	 } else {
				    		jf.setSsn("");
				    	 }
			    	  } else {
				    		jf.setSsn("");
				      }
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherMainList, new JFTSSNCompratorASC());
				 else 
				  Collections.sort(jobForTeacherMainList, new JFTSSNCompratorDESC());
		     }

			// Map record for HNS (High Need School)
			Map<Integer, DistrictSpecificPortfolioAnswers> quesMap = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
			List<DistrictSpecificPortfolioAnswers> districtSpecificPortfolioAnswersList = new ArrayList<DistrictSpecificPortfolioAnswers>();
			DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions =  districtSpecificPortfolioQuestionsDAO.findById(1, false, false);
			
			Integer numberOfRecord = 0; 
			if(districtSpecificPortfolioQuestions != null){
				try{
					districtSpecificPortfolioAnswersList  =	 districtSpecificPortfolioAnswersDAO.findByDistrictAndQuestion(districtMaster, districtSpecificPortfolioQuestions);
					
					for(DistrictSpecificPortfolioAnswers allRecord : districtSpecificPortfolioAnswersList){
						quesMap.put(allRecord.getTeacherDetail().getTeacherId(), allRecord);
					}
					
				} catch(Exception exception){
					exception.printStackTrace();
				}
			}
			String fontPath = realPath;
			
			try {
				
				BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
				font8 = new Font(tahoma, 8);

				font8Green = new Font(tahoma, 8);
				font8Green.setColor(Color.BLUE);
				font8bold = new Font(tahoma, 8, Font.NORMAL);

				font9 = new Font(tahoma, 9);
				font9bold = new Font(tahoma, 9, Font.BOLD);
				font10 = new Font(tahoma, 10);
				
				font10_10 = new Font(tahoma, 10);
				font10_10.setColor(Color.white);
				font10bold = new Font(tahoma, 10, Font.BOLD);
				font11 = new Font(tahoma, 11);
				font11bold = new Font(tahoma, 11,Font.BOLD);
				bluecolor =  new Color(0,122,180); 
				
				font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
				font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


			} 
			catch (DocumentException e1) 
			{
				e1.printStackTrace();
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}
			
			document = new Document(PageSize.A4,10f,10f,10f,10f);//PageSize.A4.rotate()		
			document.addAuthor("TeacherMatch");
			document.addCreator("TeacherMatch Inc.");
			document.addSubject(Utility.getLocaleValuePropByKey("headAppNotCont", locale));
			document.addCreationDate();
			document.addTitle(Utility.getLocaleValuePropByKey("headAppNotCont", locale));

			fos = new FileOutputStream(path);
			PdfWriter.getInstance(document, fos);
		
			document.open();
			
			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			Paragraph [] para 	= null;
			PdfPCell [] cell 	= null;

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20bold);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);
			mainTable.addCell(cell[0]);
			
			//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
			Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
			logo.scalePercent(75);

			//para[1] = new Paragraph(" ",font20bold);
			cell[1]= new PdfPCell(logo);
			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[1].setBorder(0);
			mainTable.addCell(cell[1]);

			document.add(new Phrase("\n"));
			
			para[2] = new Paragraph("",font20bold);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);
			mainTable.addCell(cell[2]);

			document.add(mainTable);
			
			document.add(new Phrase("\n"));

			float[] tblwidthz={.15f};
			
			mainTable = new PdfPTable(tblwidthz);
			mainTable.setWidthPercentage(100);
			para = new Paragraph[1];
			cell = new PdfPCell[1];

			
			para[0] = new Paragraph(Utility.getLocaleValuePropByKey("headAppNotCont", locale),font20bold);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[0].setBorder(0);
			mainTable.addCell(cell[0]);
			document.add(mainTable);

			document.add(new Phrase("\n"));
			
	        float[] tblwidths={.15f,.20f};
			
			mainTable = new PdfPTable(tblwidths);
			mainTable.setWidthPercentage(100);
			para = new Paragraph[2];
			cell = new PdfPCell[2];

			String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
			
			para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].setBorder(0);
			mainTable.addCell(cell[0]);
			
			para[1] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);
			mainTable.addCell(cell[1]);
			
			document.add(mainTable);
			
			document.add(new Phrase("\n"));

			float[] tblwidth={.36f,.36f,.34f,.20f,.36f,.36f,.34f,.20f,.20f,.20f,.20f};
			
			mainTable = new PdfPTable(tblwidth);
			mainTable.setWidthPercentage(100);
			para = new Paragraph[11];
			cell = new PdfPCell[11];
			
			// header
			para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgPoolNameGeozone", locale),font10_10);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setBackgroundColor(bluecolor);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			//cell[0].setBorder(1);
			mainTable.addCell(cell[0]);
			
			para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblApplicantName", locale),font10_10);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setBackgroundColor(bluecolor);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[1]);
			
			para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgApplicantEmail", locale),font10_10);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setBackgroundColor(bluecolor);
			cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[2]);

			para[3] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgApplicantSocial", locale),font10_10);
			cell[3]= new PdfPCell(para[3]);
			cell[3].setBackgroundColor(bluecolor);
			cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[3]);
			
			para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgEmployeeNo", locale),font10_10);
			cell[4]= new PdfPCell(para[4]);
			cell[4].setBackgroundColor(bluecolor);
			cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[4]);
			
			para[5] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblStatus", locale),font10_10);
			cell[5]= new PdfPCell(para[5]);
			cell[5].setBackgroundColor(bluecolor);
			cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[5]);
			
			para[6] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgActiveEmployee3", locale),font10_10);
			cell[6]= new PdfPCell(para[6]);
			cell[6].setBackgroundColor(bluecolor);
			cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[6]);
			
			para[7] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblHNS", locale),font10_10);
			cell[7]= new PdfPCell(para[7]);
			cell[7].setBackgroundColor(bluecolor);
			cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[7]);
			
			para[8] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblAdd", locale),font10_10);
			cell[8]= new PdfPCell(para[8]);
			cell[8].setBackgroundColor(bluecolor);
			cell[8].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[8]);
			
			para[9] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgPhoneNo", locale),font10_10);
			cell[9]= new PdfPCell(para[9]);
			cell[9].setBackgroundColor(bluecolor);
			cell[9].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[9]);
			
			para[10] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblDateApplied", locale),font10_10);
			cell[10]= new PdfPCell(para[10]);
			cell[10].setBackgroundColor(bluecolor);
			cell[10].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[10]);
			
			document.add(mainTable);
			
			if(jobForTeacherMainList.size()==0){
			    float[] tblwidth11={.10f};
				
				 mainTable = new PdfPTable(tblwidth11);
				 mainTable.setWidthPercentage(100);
				 para = new Paragraph[1];
				 cell = new PdfPCell[1];
			
				 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgNorecordfound", locale),font8bold);
				 cell[0]= new PdfPCell(para[0]);
				 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				 mainTable.addCell(cell[0]);
			
			document.add(mainTable);
		}
			
			List<JobForTeacher> finlalst=new ArrayList<JobForTeacher>();
			
			if(HNS)
			{
			for(JobForTeacher allRecord : jobForTeacherMainList){
				DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers = quesMap.get(allRecord.getTeacherId().getTeacherId());
				try{
						if(districtSpecificPortfolioAnswers!=null){
							if(districtSpecificPortfolioAnswers.getSelectedOptions().equals(1)){
								finlalst.add(allRecord);
							}
						}
				} catch(Exception exception){
					exception.printStackTrace();
				}
			}
			jobForTeacherMainList.retainAll(finlalst);
			totalRecord =jobForTeacherMainList.size();
			//totalRecord=totalRecord-iHire;
			}
			
			List<JobForTeacher> finlalstStatus=new ArrayList<JobForTeacher>();
			List<JobForTeacher> finlalstStatusAvail=new ArrayList<JobForTeacher>();
			
			int ofcl=0;
			int avail=0;
			for(JobForTeacher allRecord : jobForTeacherMainList){
			if(allRecord.getSecondaryStatus()!=null){
				
				if((allRecord.getSecondaryStatus().getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref"))){
					finlalstStatus.add(allRecord);
					ofcl++;
				} else {
					finlalstStatusAvail.add(allRecord);
					avail++;
				}
			}else{
				finlalstStatusAvail.add(allRecord);
				avail++;
			}
			}
			if(jobStatusAvail.equalsIgnoreCase("Ofcl Trans / Veteran Pref"))			
			{
				jobForTeacherMainList.retainAll(finlalstStatus);
				totalRecord =jobForTeacherMainList.size();
			
			}
			if(jobStatusAvail.equalsIgnoreCase("Available"))
			{
				System.out.println("inside availbale");
				jobForTeacherMainList.retainAll(finlalstStatusAvail);
				totalRecord =jobForTeacherMainList.size();
				
			}	
			List<JobForTeacher> empstatus=new ArrayList<JobForTeacher>();
			List<JobForTeacher> empstatusno=new ArrayList<JobForTeacher>();
			for(JobForTeacher allRecord : jobForTeacherMainList){
				
				try{
					empstatusno.add(allRecord);
					if(allRecord.getIsAffilated()!=null)
						if(allRecord.getIsAffilated().equals("1"))
						{
							empstatus.add(allRecord);
						}
							
				} catch(Exception exception){
					exception.printStackTrace();
				}
			}
			if(empStatus.equalsIgnoreCase("yes"))
			{
				jobForTeacherMainList.retainAll(empstatus);	
				totalRecord =jobForTeacherMainList.size();
			}
			else if(empStatus.equalsIgnoreCase("no"))
			{
				jobForTeacherMainList.retainAll(empstatusno);
				totalRecord =jobForTeacherMainList.size();
			}
			
		String fullName = "",teacherFname="",teacherLname="";

		if(jobForTeacherMainList.size()>0 ){
					for(JobForTeacher allRecord : jobForTeacherMainList){
						if(mapTeacherHire==null || mapTeacherHire.get(allRecord.getTeacherId().getTeacherId())==null)
						{

							String ssnumber			=	"";
							String employeeNumber	=	"";
							String status			=	"";
							String address			=	"";
							String phoneNumber		=	"";
							String activeEmployee	=	"";
							Integer answerId		=	0;
							String hns				=	"N/A";
							String jobStatus		=	"";
							teacherFname 	= 	allRecord.getTeacherId().getFirstName()==null?"":allRecord.getTeacherId().getFirstName();
							teacherLname 	= 	allRecord.getTeacherId().getLastName()==null?"":allRecord.getTeacherId().getLastName();
							fullName 		= 	teacherFname+" "+teacherLname;
							fullName		=	fullName.replace("'","\\'");
							
							TeacherPersonalInfo teacherPersonalInfo = 	tpInfomap.get(allRecord.getTeacherId().getTeacherId());
							try{ 
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getSSN()!=null){
									ssnumber=Utility.decodeBase64(teacherPersonalInfo.getSSN());
									employeeNumber	= 	teacherPersonalInfo.getEmployeeNumber()==null?"":teacherPersonalInfo.getEmployeeNumber();
								}
								
								if(teacherPersonalInfo!=null){
									address			= 	teacherPersonalInfo.getAddressLine1();	
									address			=	address.replace("'","\\'");
									phoneNumber	= 	teacherPersonalInfo.getPhoneNumber()==null?"":teacherPersonalInfo.getPhoneNumber();
								}
								
								if(phoneNumber == "" || phoneNumber == null)
									phoneNumber = allRecord.getTeacherId().getPhoneNumber()==null?"":allRecord.getTeacherId().getPhoneNumber();
								
							}catch(Exception e){
								System.out.println(e);
							}
							
							if(allRecord.getTeacherId().getStatus().equals("1")){
								status = "Yes";
							} else if(allRecord.getTeacherId().getStatus().equals("0")){
								status = "No";
							}
							
							// Get Record for HNS (High Need School)
							DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers = quesMap.get(allRecord.getTeacherId().getTeacherId());
							try{
									if(districtSpecificPortfolioAnswers!=null){
										if(districtSpecificPortfolioAnswers.getSelectedOptions().equals(1)){
											hns = "Yes";
										} else if(districtSpecificPortfolioAnswers.getSelectedOptions().equals(2)){
											hns = "No";
										} else {
											hns = "N/A";
										}
									}
							} catch(Exception exception){
								exception.printStackTrace();
							}
							
							activeEmployee	=	"No";
							if(allRecord.getIsAffilated()!=null)
								if(allRecord.getIsAffilated().equals("1"))
									activeEmployee	=	"Yes";
							
								// Find Status
								if(allRecord.getSecondaryStatus()!=null){
									if(allRecord.getSecondaryStatus().getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref")){
										jobStatus = "Ofcl Trans / Veteran Pref";
									} else {
										jobStatus = "Available";
									}
								}else{
									jobStatus = "Available";
								}
							
								 int index=0;
                                 mainTable = new PdfPTable(tblwidth);
                                 mainTable.setWidthPercentage(100);
                                 para = new Paragraph[11];
                                 cell = new PdfPCell[11];
                                 
                                 
                                 para[index] = new Paragraph(allRecord.getJobId().getJobTitle(),font8bold);
                                 cell[index]= new PdfPCell(para[index]);
                                 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
                                 mainTable.addCell(cell[index]);
                                 index++;
                                 
                                 para[index] = new Paragraph(fullName,font8bold);
                                 cell[index]= new PdfPCell(para[index]);
                                 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
                                 mainTable.addCell(cell[index]);
                                 index++;                                                             
                                 
                                 para[index] = new Paragraph(allRecord.getTeacherId().getEmailAddress(),font8bold);
                                 cell[index]= new PdfPCell(para[index]);
                                 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
                                 mainTable.addCell(cell[index]);
                                 index++;
                                 
                                 para[index] = new Paragraph(ssnumber,font8bold);
                                 cell[index]= new PdfPCell(para[index]);
                                 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
                                 mainTable.addCell(cell[index]);
                                 index++;
                                 
                                 para[index] = new Paragraph(employeeNumber,font8bold);
                                 cell[index]= new PdfPCell(para[index]);
                                 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
                                 mainTable.addCell(cell[index]);
                                 index++;
                                 
                                 para[index] = new Paragraph(jobStatus,font8bold);
                                 cell[index]= new PdfPCell(para[index]);
                                 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
                                 mainTable.addCell(cell[index]);
                                 index++;
                                 
                                 para[index] = new Paragraph(activeEmployee,font8bold);
                                 cell[index]= new PdfPCell(para[index]);
                                 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
                                 mainTable.addCell(cell[index]);
                                 index++;
                                 
                                 para[index] = new Paragraph(hns,font8bold);
                                 cell[index]= new PdfPCell(para[index]);
                                 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
                                 mainTable.addCell(cell[index]);
                                 index++;
                                 
                                 para[index] = new Paragraph(address,font8bold);
                                 cell[index]= new PdfPCell(para[index]);
                                 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
                                 mainTable.addCell(cell[index]);
                                 index++;
                                 
                                 para[index] = new Paragraph(phoneNumber,font8bold);
                                 cell[index]= new PdfPCell(para[index]);
                                 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
                                 mainTable.addCell(cell[index]);
                                 index++;
                                 
                                 para[index] = new Paragraph(Utility.convertDateAndTimeToUSformatOnlyDate(allRecord.getTeacherId().getCreatedDateTime()),font8bold);
                                 cell[index]= new PdfPCell(para[index]);
                                 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
                                 mainTable.addCell(cell[index]);
                                 index++;
                                 document.add(mainTable);
							
						
						}
					}
		     }
	     }
	}catch(Exception exception){
			exception.printStackTrace();
	}finally
	{
		if(document != null && document.isOpen())
			document.close();
		if(writer!=null){
			writer.flush();
			writer.close();
		}
	}
	
	return true;
}

public String printApplicantRecord(Integer districtId,String noOfRow, String pageNo,String sortOrder,String sortOrderType, String sfromDate,String stoDate,String jobStatusAvail,String jobOrderId,boolean HNS,String empStatus)
{
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	StringBuffer dmRecords = new StringBuffer();
	int sortingcheck=0;
	
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			entityID=userMaster.getEntityType();
		}
		System.out.println(":::::::::districtId::::::::::::"+districtId);
		if(districtId!=null)
			districtMaster = districtMasterDAO.findById(districtId, false, false);
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start =((pgNo-1)*noOfRowInPage);
		int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		int totalRecords=0;
		
		String responseText			=	"";
		
		 /** set default sorting fieldName **/
		String sortOrderFieldName="jobTitle";
		String sortOrderNoField="jobTitle";
		
		boolean deafultFlag=false;
		
		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal=null;

		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
		}

		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
	
		StatusMaster  statusMaster = WorkThreadServlet.statusMap.get("comp");
		StatusMaster  statusMasterHired = statusMasterDAO.findStatusByShortNameHired("hird");
		List<JobForTeacher> jobForTeacherList 		= 	new ArrayList<JobForTeacher>();
		List<TeacherDetail> teacherDetails			=	new ArrayList<TeacherDetail>();
		List<SecondaryStatus> secondaryStatusList	=	new ArrayList<SecondaryStatus>();
		List<JobForTeacher> jobForTeacherMainList	=	new ArrayList<JobForTeacher>();
		List<TeacherDetail> teacherDetailList		= 	new ArrayList<TeacherDetail>();
		List<TeacherPersonalInfo> teacherPersonalInfoList  = new ArrayList<TeacherPersonalInfo>();

		List<JobForTeacher> jobStatusList1		= 	new ArrayList<JobForTeacher>();
		
		Map<Integer, Boolean> mapTeacherHire=new HashMap<Integer, Boolean>();
  if(districtMaster!=null){
		secondaryStatusList 	= 	secondaryStatusDAO.findSecondaryStatusListByStatusName("Ofcl Trans / Veteran Pref",districtMaster);
		
		if(secondaryStatusList!=null && secondaryStatusList.size()>0)
		{
			
			try{
				jobStatusList1 	=  	jobForTeacherDAO.getApplicantByAvailableAndTransListHired(statusMasterHired,secondaryStatusList,districtMaster);
				for(JobForTeacher jobForTeacher:jobStatusList1)
					mapTeacherHire.put(jobForTeacher.getTeacherId().getTeacherId(), true);
					System.out.println("----------------Hired candidate list----------------"+ jobStatusList1.size());
				
					jobForTeacherList 	=  	jobForTeacherDAO.getApplicantByAvailableAndTransList(statusMaster,secondaryStatusList,districtMaster,sfromDate,stoDate,jobOrderId );
					
					System.out.println("----------------All candidate list----------------"+ jobForTeacherList.size());
					
					//jobForTeacherList.removeAll(jobStatusList1);
			}catch(Exception exception){
				exception.printStackTrace();
			}
						
			if(jobForTeacherList.size()>0){
				try{
					for (JobForTeacher jobForTeacher : jobForTeacherList) {
						teacherDetails.add(jobForTeacher.getTeacherId());
					}
				}catch(Exception exception){
					exception.printStackTrace();
				}
			}
			
			if(teacherDetails!=null && teacherDetails.size()>0)
				teacherDetailList = teacherSecondaryStatusDAO.findTeacherListWithFilter(teacherDetails,districtMaster);
			
			Map<Integer,TeacherDetail> mapTeacher=new HashMap<Integer, TeacherDetail>();

			if(teacherDetailList.size()>0 && teacherDetailList!=null){
				try{
					for (TeacherDetail teacherDetail : teacherDetailList) {
						mapTeacher.put(teacherDetail.getTeacherId(),teacherDetail);
					}
				} catch(Exception exception){
					exception.printStackTrace();
				}
			}

			if(jobForTeacherList.size()>0 && jobForTeacherList!=null){
				try{
					for (JobForTeacher jobForTeacher : jobForTeacherList) {
						if(mapTeacher.get(jobForTeacher.getTeacherId().getTeacherId()) == null){
							jobForTeacherMainList.add(jobForTeacher);
						}
					}
				} catch(Exception exception){
					exception.printStackTrace();
				}
			}
		}

		Map<Integer, TeacherPersonalInfo> tpInfomap = new  HashMap<Integer, TeacherPersonalInfo>();

		if(teacherDetails.size()>0)
			teacherPersonalInfoList = teacherPersonalInfoDAO.findTeacherPInfoForTeacher(teacherDetails);
		try{
			for (TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList) {
				tpInfomap.put(teacherPersonalInfo.getTeacherId(), teacherPersonalInfo);
			}
		} catch(Exception exception){
			exception.printStackTrace();
		}
		
		//Manual Sorting
		if(sortOrder.equals("")){
	    	 for(JobForTeacher jf:jobForTeacherMainList) 
		    	 jf.setJobTitle(jf.getJobId().getJobTitle());
			
			  Collections.sort(jobForTeacherMainList, new JobTitleCompratorASC());
	     }
		if(sortOrder.equals("lastName")){    
		     for(JobForTeacher jf:jobForTeacherMainList) 
			 {
		    	 jf.setLastName(jf.getTeacherId().getLastName());
				
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherMainList, new LastNameCompratorASC());
			 else 
			  Collections.sort(jobForTeacherMainList, new LastNameCompratorDESC());
	     }
	     if(sortOrder.equals("jobTitle")){    
		     for(JobForTeacher jf:jobForTeacherMainList) 
			 {
		    	 jf.setJobTitle(jf.getJobId().getJobTitle());
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherMainList, new JobTitleCompratorASC());
			 else 
			  Collections.sort(jobForTeacherMainList, new JobTitleCompratorDESC());
	     }
	     
		if(sortOrder.equals("emailAddress")){ 
		     for(JobForTeacher jf:jobForTeacherMainList) 
			 {
		    	 jf.setEmailId(jf.getTeacherId().getEmailAddress());
				
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherMainList, new JFTEmailCompratorASC());
			 else 
			  Collections.sort(jobForTeacherMainList, new JFTEmailCompratorDESC());
	     }
		
		if(sortOrder.equals("SSN")){
		     for(JobForTeacher jf:jobForTeacherMainList) {
		    	 TeacherPersonalInfo teacherPersonalInfo = 	tpInfomap.get(jf.getTeacherId().getTeacherId());
		    	 if(teacherPersonalInfo!=null){
			    	 if(teacherPersonalInfo.getSSN()!= null && !teacherPersonalInfo.getSSN().equals("")){
			    		jf.setSsn(teacherPersonalInfo.getSSN());
			    	 } else {
			    		jf.setSsn("");
			    	 }
		    	  } else {
			    		jf.setSsn("");
			      }
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherMainList, new JFTSSNCompratorASC());
			 else 
			  Collections.sort(jobForTeacherMainList, new JFTSSNCompratorDESC());
	     }

		// Map record for HNS (High Need School)
		Map<Integer, DistrictSpecificPortfolioAnswers> quesMap = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
		List<DistrictSpecificPortfolioAnswers> districtSpecificPortfolioAnswersList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions =  districtSpecificPortfolioQuestionsDAO.findById(1, false, false);
		
		Integer numberOfRecord = 0; 
		if(districtSpecificPortfolioQuestions != null){
			try{
				districtSpecificPortfolioAnswersList  =	 districtSpecificPortfolioAnswersDAO.findByDistrictAndQuestion(districtMaster, districtSpecificPortfolioQuestions);
				
				for(DistrictSpecificPortfolioAnswers allRecord : districtSpecificPortfolioAnswersList){
					quesMap.put(allRecord.getTeacherDetail().getTeacherId(), allRecord);
				}
				
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		
		dmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'> "+Utility.getLocaleValuePropByKey("headAppNotCont", locale)+"</div><br/>");
		
		dmRecords.append("<div style='width:100%'>");
			dmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
			dmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
			dmRecords.append("<br/><br/>");
		dmRecords.append("</div>");
		
		dmRecords.append("<table id='applicantReportGridTable' border='0'>");
		dmRecords.append("<thead class='bg'>");
		
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgPoolNameGeozone", locale)+"</th>");
		
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblApplicantName", locale)+"</th>");
		
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgApplicantEmail", locale)+"</th>");
		
		dmRecords.append("<th valign='top' width='8%'>"+Utility.getLocaleValuePropByKey("msgApplicantSocial", locale)+"</th>");
		
		dmRecords.append("<th valign='top' width='8%'>"+Utility.getLocaleValuePropByKey("msgEmployeeNo", locale)+"</th>");
		
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
		
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgActiveEmployee", locale)+"</th>");
		
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblHNS", locale)+"</th>");
		
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblAdd", locale)+"</th>");
		
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("headPhone", locale)+"&nbsp;#</th>");
		
		dmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblDateApplied", locale)+"</th>");
		dmRecords.append("</thead>");
		int iHire=0;
		if(jobForTeacherMainList.size()>0 && jobForTeacherMainList!=null){
			for(JobForTeacher allRecord : jobForTeacherMainList){
				if(mapTeacherHire==null || mapTeacherHire.get(allRecord.getTeacherId().getTeacherId())==null)
				{
					
				}
				else
				{
					iHire++;
				}
			}
		}
		
		totalRecord =jobForTeacherMainList.size();
List<JobForTeacher> finlalst=new ArrayList<JobForTeacher>();
		
		if(HNS)
		{
		for(JobForTeacher allRecord : jobForTeacherMainList){
			DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers = quesMap.get(allRecord.getTeacherId().getTeacherId());
			try{
					if(districtSpecificPortfolioAnswers!=null){
						if(districtSpecificPortfolioAnswers.getSelectedOptions().equals(1)){
							finlalst.add(allRecord);
						}
					}
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		jobForTeacherMainList.retainAll(finlalst);
		totalRecord =jobForTeacherMainList.size();
		//totalRecord=totalRecord-iHire;
		}
		
		List<JobForTeacher> finlalstStatus=new ArrayList<JobForTeacher>();
		List<JobForTeacher> finlalstStatusAvail=new ArrayList<JobForTeacher>();
		
		int ofcl=0;
		int avail=0;
		for(JobForTeacher allRecord : jobForTeacherMainList){
		if(allRecord.getSecondaryStatus()!=null){
			
			if((allRecord.getSecondaryStatus().getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref"))){
				finlalstStatus.add(allRecord);
				ofcl++;
			} else {
				finlalstStatusAvail.add(allRecord);
				avail++;
			}
		}else{
			finlalstStatusAvail.add(allRecord);
			avail++;
		}
		}
		if(jobStatusAvail.equalsIgnoreCase("Ofcl Trans / Veteran Pref"))			
		{
			jobForTeacherMainList.retainAll(finlalstStatus);
			totalRecord =jobForTeacherMainList.size();
		
		}
		if(jobStatusAvail.equalsIgnoreCase("Available"))
		{
			System.out.println("inside availbale");
			jobForTeacherMainList.retainAll(finlalstStatusAvail);
			totalRecord =jobForTeacherMainList.size();
			
		}
		List<JobForTeacher> empstatus=new ArrayList<JobForTeacher>();
		List<JobForTeacher> empstatusno=new ArrayList<JobForTeacher>();
		for(JobForTeacher allRecord : jobForTeacherMainList){
			
			try{
				empstatusno.add(allRecord);
				if(allRecord.getIsAffilated()!=null)
					if(allRecord.getIsAffilated().equals("1"))
					{
						empstatus.add(allRecord);
					}
						
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		if(empStatus.equalsIgnoreCase("yes"))
		{
			jobForTeacherMainList.retainAll(empstatus);	
			totalRecord =jobForTeacherMainList.size();
		}
		else if(empStatus.equalsIgnoreCase("no"))
		{
			jobForTeacherMainList.retainAll(empstatusno);
			totalRecord =jobForTeacherMainList.size();
		}
		//totalRecord=totalRecord-iHire;
		
		if(totalRecord<end)
			end=totalRecord;
		
		List<JobForTeacher> activityRecordListMain = jobForTeacherMainList.subList(start,end);
		
		String fullName = "",teacherFname="",teacherLname="";
		int noOfRecordCheck=0;
		if(jobForTeacherMainList.size()>0 && jobForTeacherMainList!=null){
			for(JobForTeacher allRecord : activityRecordListMain){
				if(mapTeacherHire==null || mapTeacherHire.get(allRecord.getTeacherId().getTeacherId())==null)
				{
				String ssnumber			=	"";
				String employeeNumber	=	"";
				String status			=	"";
				String address			=	"";
				String phoneNumber		=	"";
				String activeEmployee	=	"";
				Integer answerId		=	0;
				String hns				=	"N/A";
				String jobStatus		=	"";
				teacherFname 	= 	allRecord.getTeacherId().getFirstName()==null?"":allRecord.getTeacherId().getFirstName();
				teacherLname 	= 	allRecord.getTeacherId().getLastName()==null?"":allRecord.getTeacherId().getLastName();
				fullName 		= 	teacherFname+" "+teacherLname;
				fullName		=	fullName.replace("'","\\'");
				
				TeacherPersonalInfo teacherPersonalInfo = 	tpInfomap.get(allRecord.getTeacherId().getTeacherId());
				try{ 
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getSSN()!=null){
						ssnumber=Utility.decodeBase64(teacherPersonalInfo.getSSN());
						employeeNumber	= 	teacherPersonalInfo.getEmployeeNumber()==null?"":teacherPersonalInfo.getEmployeeNumber();
					}
					
					if(teacherPersonalInfo!=null){
						address			= 	teacherPersonalInfo.getAddressLine1();	
						address			=	address.replace("'","\\'");
						phoneNumber		= 	teacherPersonalInfo.getPhoneNumber()==null?"":teacherPersonalInfo.getPhoneNumber();
					}
					
					if(phoneNumber == "" || phoneNumber == null)
						phoneNumber = allRecord.getTeacherId().getPhoneNumber()==null?"":allRecord.getTeacherId().getPhoneNumber();
					
				}catch(Exception e){
					System.out.println(e);
				}
				
				if(allRecord.getTeacherId().getStatus().equals("1")){
					status = "Yes";
				} else if(allRecord.getTeacherId().getStatus().equals("0")){
					status = "No";
				}
				
				// Get Record for HNS (High Need School)
				DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers = quesMap.get(allRecord.getTeacherId().getTeacherId());
				try{
						if(districtSpecificPortfolioAnswers!=null){
							if(districtSpecificPortfolioAnswers.getSelectedOptions().equals(1)){
								hns = "Yes";
							} else if(districtSpecificPortfolioAnswers.getSelectedOptions().equals(2)){
								hns = "No";
							} else {
								hns = "N/A";
							}
						}
				} catch(Exception exception){
					exception.printStackTrace();
				}
				
				phoneNumber = allRecord.getTeacherId().getPhoneNumber()==null?"":allRecord.getTeacherId().getPhoneNumber();
				activeEmployee	=	"No";
				if(allRecord.getIsAffilated()!=null)
					if(allRecord.getIsAffilated().equals("1"))
						activeEmployee	=	"Yes";
				
					// Find Status
					if(allRecord.getSecondaryStatus()!=null){
						if(allRecord.getSecondaryStatus().getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgOfclTransVeteranPref", locale))){
							jobStatus = Utility.getLocaleValuePropByKey("msgOfclTransVeteranPref", locale);
						} else {
							jobStatus = Utility.getLocaleValuePropByKey("optAvble", locale);
						}
					}else{
						jobStatus = Utility.getLocaleValuePropByKey("optAvble", locale);
					}
				
				 	noOfRecordCheck++;
				 	dmRecords.append("<tr>");
					dmRecords.append("<td>");
					dmRecords.append(allRecord.getJobId().getJobTitle());
					dmRecords.append("</td>");
					
					dmRecords.append("<td>");
					dmRecords.append("<a class='profile' data-placement='above' href='javascript:void(0);' onclick=\"showProfileContentForTeacher(this,"+allRecord.getTeacherId().getTeacherId()+","+noOfRecordCheck+",'Applicant Report',event)\">");
					dmRecords.append(fullName);
					dmRecords.append("</a>");
					dmRecords.append("</td>");
				
					dmRecords.append("<td>");
					dmRecords.append("<a href='mailto:"+allRecord.getTeacherId().getEmailAddress()+"\'>");
					dmRecords.append(allRecord.getTeacherId().getEmailAddress());
					dmRecords.append("</a>");
					dmRecords.append("</td>");
					
					dmRecords.append("<td>");
					dmRecords.append(ssnumber);
					dmRecords.append("</td>");
					
					dmRecords.append("<td>");
					dmRecords.append(employeeNumber);
					dmRecords.append("</td>");
					
					dmRecords.append("<td>");
					dmRecords.append(jobStatus);
					dmRecords.append("</td>");
					
					dmRecords.append("<td>");
					dmRecords.append(activeEmployee);
					dmRecords.append("</td>");
					
					dmRecords.append("<td>");
					dmRecords.append(hns);
					dmRecords.append("</td>");
					
					dmRecords.append("<td>");
					dmRecords.append(address);
					dmRecords.append("</td>");
					
					dmRecords.append("<td>");
					dmRecords.append(phoneNumber);
					dmRecords.append("</td>");
					
					dmRecords.append("<td>");
					if(allRecord.getTeacherId().getCreatedDateTime() != null){
						dmRecords.append(Utility.convertDateAndTimeToUSformatOnlyDate(allRecord.getTeacherId().getCreatedDateTime()));
					}
					dmRecords.append("</td>");
				dmRecords.append("</tr>");
			  }
			}
		}else{
			dmRecords.append("<tr>");
			dmRecords.append("<td colspan='7'><B>");
			dmRecords.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
			dmRecords.append("</B></td>");
			dmRecords.append("</tr>");
		}
      }
	} catch(Exception exception){
		exception.printStackTrace();
	}
	return dmRecords.toString();
 }

}

