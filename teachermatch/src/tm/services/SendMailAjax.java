package tm.services;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import tm.utility.Utility;

import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.util.UidGenerator;

public class SendMailAjax
{
	
	String email;
	String subject;
	String location;
	String fromDate;
	String fromtime;
	String enddate;
	String endtime;
	String description;
	public String googleLink() throws ParseException
	{
			
		   SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
	       SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
	       Date sdate = parseFormat.parse(fromtime);
	       Date edate = parseFormat.parse(endtime);
	       String[] convertStartTime =displayFormat.format(sdate).split(":");
	       String[] convertEndTime =displayFormat.format(edate).split(":");
	        
	       
	      
	      String val1=  calculateTime(Integer.parseInt(convertStartTime[0]),Integer.parseInt(convertStartTime[1]));
	      String val2=  calculateTime(Integer.parseInt(convertEndTime[0]),Integer.parseInt(convertEndTime[1]));
	     
	       if(Integer.parseInt(convertStartTime[1]) ==30)
	       {
	    	   convertStartTime[1]="00";
	    	   convertStartTime[0]=String.valueOf((Integer.parseInt(convertStartTime[0])-5));
	    	  
	       }
	       else
	       {
	    	   convertStartTime[1]="30";
	    	   convertStartTime[0]=String.valueOf((Integer.parseInt(convertStartTime[0])-6));	    	   
	       }
	       
	       int Atime=Integer.parseInt(convertStartTime[0]);
	       int Btime=Integer.parseInt(convertStartTime[1]);
	       
	       String[] convertStartDates =fromDate.split("-");
	       String[] convertEndDates =enddate.split("-");	       
	     /////////Start Date
		if(Integer.parseInt(convertStartDates[0])==1)
		{			
			convertStartDates[0]="12";
			convertStartDates[2]=String.valueOf((Integer.parseInt(convertStartDates[2])-1));
		}
		else
		{
			convertStartDates[0]=String.valueOf((Integer.parseInt(convertStartDates[0])-1));			
		}
		int A=(Integer.parseInt(convertStartDates[0]));
		int B=(Integer.parseInt(convertStartDates[1]));
		int C=(Integer.parseInt(convertStartDates[2]));
		
		/////////////End Date		
		if(Integer.parseInt(convertEndDates[0])==1)
		{			
			convertEndDates[0]="12";
			convertEndDates[2]=String.valueOf((Integer.parseInt(convertEndDates[2])-1));
		}
		else
		{
			convertEndDates[0]=String.valueOf((Integer.parseInt(convertEndDates[0])-1));			
		}
		int D=(Integer.parseInt(convertEndDates[0]));
		int E=(Integer.parseInt(convertEndDates[1]));
		int F=(Integer.parseInt(convertEndDates[2]));
		
		//start time
		 java.util.Calendar startCal = java.util.Calendar.getInstance();
		  startCal.set(C, A, B, Atime, Btime, 00);
		  //end time
		  java.util.Calendar endCal = java.util.Calendar.getInstance();
		  endCal.set(F, D, E, 15, 00);
		  
		  SimpleDateFormat sdFormat =  new SimpleDateFormat("yyyyMMdd'T'hhmmss'Z'");
		  String strDate = sdFormat.format(startCal.getTime());		   
		  
		  String strDates = sdFormat.format(endCal.getTime());
		
		  String[] firstDate =strDate.split("T"); 
		  String[] lastDate =strDates.split("T");
		 
		/*String url=  "https://www.google.com/calendar/render?action=TEMPLATE&details="+description+"&text="+subject+"" +
 		"&add=sonu.gupta@netsutra.com&location="+location+"&dates="+(firstDate[0]+val1)+"%2F"+(lastDate[0]+val2)+"" +
			"&calendar-label=Outgoing#f";*/
		  String url=  "https://www.google.com/calendar/render?action=TEMPLATE&details="+description+"&text="+subject+"" +
	 		"&location="+location+"&dates="+(firstDate[0]+val1)+"%2F"+(lastDate[0]+val2)+"" +
				"&calendar-label=Outgoing#f";
		 
		return url;
	}
	public String calculateTime(int hour,int minute)
	{
		
		 int min=000000;
		 int hou=000000;
		 int mins=50000;
		 String finalTime="";
		  
	       if(minute ==30 && hour==5)
	       {
	    	   finalTime="000000";
	       }
	       else if(minute==30 && hour>5)
	       {
	    	   min=50000;
	    	   hour=hour*10000;
	    	   finalTime=""+(hour-min);
	       }
	       else if(minute ==30&&hour==0)
	       {
	    	   finalTime="190000";
	       }
	       else if(minute ==30&&hour<5)
	       {
	    	   min=190000;
	    	   hour=hour*10000;
	    	   finalTime=String.valueOf(min+hour);
	       }
	       else if(minute ==00&&hour==6)
	       {
	    	   
	    	   min=63000;
	    	   hour=hour*10000;
	    	   finalTime=String.valueOf(min-hour);
	       }
	       else if(minute ==00&&hour>6)
	       {
	    	   
	    	   min=57000;
	    	   hour=hour*10000;
	    	   finalTime=String.valueOf(hour-min);
	       }
	       else if(minute ==00&&hour<=5)
	       {
	    	   min=183000;
	    	   hour=hour*10000;
	    	   finalTime=String.valueOf(min+hour);
	       }
	       if(finalTime.length()==4)
	       {
	    	   finalTime="T00"+finalTime+"Z";
	       }
	       if(finalTime.length()==5)
	       {
	    	   finalTime="T0"+finalTime+"Z";
	       }
	       if(finalTime.length()==6)
	       {
	    	   finalTime="T"+finalTime+"Z";
	       }
	
		return finalTime;
	}
	public String sendMail(String email,String subject,String location,String fromdate,String fromtime,String enddate,String endtime,String description) throws ParseException 
	{
	
		this.email=email;
		this.subject=subject;
		this.location=location;
		this.fromDate=fromdate;
		this.fromtime=fromtime;
		this.enddate=enddate;
		this.endtime=endtime;
		this.description=description;	
		
		// Initilize values
		
		  
		//convert time AM/PM to 24 hour
		   SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
	       SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
	       Date sdate = parseFormat.parse(fromtime);
	       Date edate = parseFormat.parse(endtime);
	       String[] convertStartTime =displayFormat.format(sdate).split(":");
	       String[] convertEndTime =displayFormat.format(edate).split(":");
	       
	       if(Integer.parseInt(convertStartTime[1]) ==30)
	       {
	    	   convertStartTime[1]="00";
	    	   convertStartTime[0]=String.valueOf((Integer.parseInt(convertStartTime[0])+6));
	    	  
	       }
	       else
	       {
	    	   convertStartTime[1]="30";
	    	   convertStartTime[0]=String.valueOf((Integer.parseInt(convertStartTime[0])+5));	    	   
	       }
	       
	       int Atime=Integer.parseInt(convertStartTime[0]);
	       int Btime=Integer.parseInt(convertStartTime[1]);
	       
	       if(Integer.parseInt(convertEndTime[1]) ==30)
	       {
	    	   convertEndTime[1]="00";
	    	   convertEndTime[0]=String.valueOf((Integer.parseInt(convertEndTime[0])+6));
	    	  
	       }
	       else
	       {
	    	   convertEndTime[1]="30";
	    	   convertEndTime[0]=String.valueOf((Integer.parseInt(convertEndTime[0])+5));	    	   
	       }
	       
	       int Ctime=Integer.parseInt(convertEndTime[0]);
	       int Dtime=Integer.parseInt(convertEndTime[1]);
	       String[] convertStartDates =fromdate.split("-");
	       String[] convertEndDates =enddate.split("-");	       
	       
	     /////////Start Date
		if(Integer.parseInt(convertStartDates[0])==1)
		{			
			convertStartDates[0]="12";
			convertStartDates[2]=String.valueOf((Integer.parseInt(convertStartDates[2])-1));
		}
		else
		{
			convertStartDates[0]=String.valueOf((Integer.parseInt(convertStartDates[0])-1));			
		}
		int A=(Integer.parseInt(convertStartDates[0]));
		int B=(Integer.parseInt(convertStartDates[1]));
		int C=(Integer.parseInt(convertStartDates[2]));			
		/////////////End Date		
		if(Integer.parseInt(convertEndDates[0])==1)
		{			
			convertEndDates[0]="12";
			convertEndDates[2]=String.valueOf((Integer.parseInt(convertEndDates[2])-1));
		}
		else
		{
			convertEndDates[0]=String.valueOf((Integer.parseInt(convertEndDates[0])-1));			
		}
		int D=(Integer.parseInt(convertEndDates[0]));
		int E=(Integer.parseInt(convertEndDates[1]));
		int F=(Integer.parseInt(convertEndDates[2]));		
	
	
		System.out.println(A);
		// create a calendar object
		Calendar icsCalendar = new Calendar();

		//File calFile = new File("E:/inviteCalender.ics");
		File calFile = new File(Utility.getValueOfPropByKey("iclcalendarRootPath")+"/inviteCalender.ics");

		try {

		// Create a TimeZone
		TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
		TimeZone timezone = registry.getTimeZone("Australia/Melbourne");
		VTimeZone tz = ((net.fortuna.ical4j.model.TimeZone) timezone).getVTimeZone();

		// Start Date is on: July 17, 2013, 10:00 am
		java.util.Calendar startDate = new GregorianCalendar();
		startDate.setTimeZone(timezone);
		startDate.set(java.util.Calendar.MONTH, A);
		startDate.set(java.util.Calendar.DAY_OF_MONTH, B);
		startDate.set(java.util.Calendar.YEAR, C);
		startDate.set(java.util.Calendar.HOUR_OF_DAY, Atime);
		startDate.set(java.util.Calendar.MINUTE, Btime);
		startDate.set(java.util.Calendar.SECOND, 0);

		// End Date is on: July 17, 2013, 11:00 am
		java.util.Calendar endDate = new GregorianCalendar();
		endDate.setTimeZone(timezone);
		endDate.set(java.util.Calendar.MONTH, D);
		endDate.set(java.util.Calendar.DAY_OF_MONTH, E);
		endDate.set(java.util.Calendar.YEAR, F);
		endDate.set(java.util.Calendar.HOUR_OF_DAY, Ctime);
		endDate.set(java.util.Calendar.MINUTE, Dtime);	
		endDate.set(java.util.Calendar.SECOND, 0);

		// Create the event props
		String eventName = subject;
		DateTime start = new DateTime(startDate.getTime());
		DateTime end = new DateTime(endDate.getTime());

		// Create the event
		VEvent meeting = new VEvent(start, end, eventName);
		// create Organizer object and add it to vEvent
		//Organizer organizer = new Organizer(URI.create("mailto:someone@somethingsss"));
		//meeting.getProperties().getProperty(Property.DESCRIPTION).setValue(description);
		//meeting.getProperties().add(organizer);

		// add timezone to vEvent
		meeting.getProperties().add(tz.getTimeZoneId());
		Location loc = new Location("hjkj");
		meeting.getProperties().add(loc);
		
		
		Description sum = new Description("hjkhfhgj");
		meeting.getProperties().add(sum);

		// generate unique identifier and add it to vEvent
		UidGenerator ug;
		ug = new UidGenerator("uidGen");
		Uid uid = ug.generateUid();
		meeting.getProperties().add(uid);

		// add attendees..
		/*Attendee dev1 = new Attendee(URI.create("someone@something"));
		dev1.getParameters().add(Role.REQ_PARTICIPANT);
		dev1.getParameters().add(new Cn("Developer 1"));
		meeting.getProperties().add(dev1);*/

		// assign props to calendar object
		icsCalendar.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
		icsCalendar.getProperties().add(CalScale.GREGORIAN);

		// Add the event and print
		icsCalendar.getComponents().add(meeting);

		CalendarOutputter outputter = new CalendarOutputter();
		outputter.setValidating(false);

		FileOutputStream fout = new FileOutputStream(calFile);
		outputter.output(icsCalendar, fout);
		System.out.println(meeting);
		

		} catch (Exception e) {
		System.err.println("Error in method sendMail() " + e);		
		}
		String microsoftStartDate=C+"-"+A+"-"+B+"t"+Atime+":"+Btime+":00z";
		String microsoftEndDate=F+"-"+D+"-"+E+"t"+Ctime+":"+Dtime+":00z";
		String microsoftCalendar="";
		//microsoftCalendar="https://outlook.office365.com/api/v1.0/me/calendarview?startdatetime="+microsoftStartDate+"&enddatetime="+microsoftEndDate+"&location=";
		System.out.println("DDDDDDDDDDDDDD "+Utility.getValueOfPropByKey("iclcalendarRootPath"));
		String contents = googleLink();
		System.out.println("contents::: "+contents);
		  EmailerService emailerService=new EmailerService();		  
		 
		 //emailerService.sendMailWithAttachments(email, subject, "sonu.gupta@netsutra.com", googleLink(), "inviteCalender.ics", "InviteCalender");
		  
		 emailerService.sendMailWithAttachments(email, subject, "noreply@teachermatch.net", contents,Utility.getValueOfPropByKey("iclcalendarRootPath")+"/inviteCalender.ics");
		
		return "Submitted";
	}	

}
	
	
	