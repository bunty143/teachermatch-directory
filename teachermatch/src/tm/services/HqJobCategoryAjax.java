package tm.services;
import static tm.services.report.CandidateGridReportAjax.convertUSformatOnlyYear;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.HqBranchesDistricts;
import tm.bean.hqbranchesmaster.SpAssessmentConfig;
import tm.bean.hqbranchesmaster.TempDistricts;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.JobCategoryMasterHistory;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.ReferenceCheckQuestionSet;
import tm.bean.master.ReferenceQuestionSets;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.assessment.AssessmentGroupDetailsDAO;
import tm.dao.districtassessment.DistrictAssessmentDetailDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.hqbranchesmaster.HqBranchesDistrictsDAO;
import tm.dao.hqbranchesmaster.SpAssessmentConfigDAO;
import tm.dao.hqbranchesmaster.TempDistrictsDAO;
import tm.dao.i4.I4QuestionSetsDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificRefChkQuestionsDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.JobCategoryMasterHistoryDAO;
import tm.dao.master.QqQuestionSetsDAO;
import tm.dao.master.ReferenceCheckQuestionSetDAO;
import tm.dao.master.ReferenceQuestionSetsDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class HqJobCategoryAjax
{
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(
			JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	@Autowired 
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}	
	
	@Autowired 
	private BranchMasterDAO branchMasterDAO; 
	public void setBranchMasterDAO(BranchMasterDAO branchMasterDAO) {
		this.branchMasterDAO = branchMasterDAO;
	}
	
	@Autowired 
	private TempDistrictsDAO tempDistrictsDAO;
	public void settempDistrictsDAO(TempDistrictsDAO tempDistrictsDAO) {
		this.tempDistrictsDAO = tempDistrictsDAO;
	}
	
	@Autowired 
	private HeadQuarterMasterDAO headQuarterMasterDAO; 
	public void setHeadQuarterMasterDAO(HeadQuarterMasterDAO headQuarterMasterDAO) {
		this.headQuarterMasterDAO = headQuarterMasterDAO;
	}
	
	@Autowired 
	private HqBranchesDistrictsDAO hqBranchesDistrictsDAO; 
	public HqBranchesDistrictsDAO getHqBranchesDistrictsDAO() {
		return hqBranchesDistrictsDAO;
	}

	public void setHqBranchesDistrictsDAO(
			HqBranchesDistrictsDAO hqBranchesDistrictsDAO) {
		this.hqBranchesDistrictsDAO = hqBranchesDistrictsDAO;
	}
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private I4QuestionSetsDAO i4QuestionSetsDAO;

	@Autowired
	private DistrictAssessmentDetailDAO districtAssessmentDetailDAO;
	
	@Autowired
	private DistrictSpecificRefChkQuestionsDAO districtSpecificRefChkQuestionsDAO;
	
	@Autowired
	private ReferenceCheckQuestionSetDAO referenceCheckQuestionSetDAO;
	
	@Autowired
	private ReferenceQuestionSetsDAO referenceQuestionSetsDAO;
	
	@Autowired
	private QqQuestionSetsDAO qqQuestionSetsDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private SpAssessmentConfigDAO spAssessmentConfigDAO;
	
	@Autowired
	private JobCategoryMasterHistoryDAO jobCategoryMasterHistoryDAO;
	
	@Autowired
	private AssessmentGroupDetailsDAO assessmentGroupDetailsDAO;

	String locale = Utility.getValueOfPropByKey("locale");
	String lblNoRecord=Utility.getLocaleValuePropByKey("lblNoRecord", locale);
	String optStrEpiGroups=Utility.getLocaleValuePropByKey("optStrEpiGroups", locale);
	
	
	
	public String displayJobCategories(Boolean result,Integer searchEntityType,Integer entityType,Integer searchId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer branchId ,String status)
	{
		WebContext context;
 		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------

			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}

			List<JobCategoryMaster> jobCategoryMasters	  	=	null;
			List<JobCategoryMaster> jobCategoryMastersAll	  	=	null;
			String sortOrderFieldName	=	"jobCategoryName";
			String sortOrderNoField		=	"jobCategoryName";

			Order  sortOrderStrVal		=	null;
			
			Criterion statusCrit = Restrictions.eq("status", status);	
			if(status.equalsIgnoreCase("All"))
				statusCrit = Restrictions.ne("status", "");	
			
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("districtMaster")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("districtMaster"))
				{
					sortOrderNoField="districtMaster";
				}
			}

			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}

			List<JobCategoryMaster>sortedJobCatList=new ArrayList<JobCategoryMaster>();
			Criterion criterion=null; 
			DistrictMaster districtMaster=null;
			HeadQuarterMaster headQuarterMaster=null;
			BranchMaster branchMaster=null;
			if(userMaster.getEntityType()==2){
				districtMaster=userMaster.getDistrictId();
				criterion=Restrictions.eq("districtMaster", districtMaster);
				jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,statusCrit);
				jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion);
				totalRecord=jobCategoryMastersAll.size();
			}
			//HeadQuarter
			else if(userMaster.getEntityType()==5){
				//HeadQuarter
				if(searchEntityType==1)
				{
					headQuarterMaster=userMaster.getHeadQuarterMaster();
					criterion=Restrictions.eq("headQuarterMaster", headQuarterMaster);
					Criterion criterion2=Restrictions.isNotNull("branchMaster");
					jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,criterion2,statusCrit);
					jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion,criterion2,statusCrit);
					totalRecord=jobCategoryMastersAll.size();
				}
				//HeadQuarter >> Branch
				if(searchEntityType==2)
				{
					if(searchId!=0){
						branchMaster=branchMasterDAO.findById(searchId, false, false);
						criterion=Restrictions.eq("branchMaster", branchMaster);
					}
					else
					{		
						headQuarterMaster=userMaster.getHeadQuarterMaster();
						Criterion criterion2=Restrictions.isNotNull("branchMaster");
						Criterion criterion3= Restrictions.eq("headQuarterMaster", headQuarterMaster);
						criterion=Restrictions.and(criterion2, criterion3);
					}
					jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,statusCrit);
					jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion,statusCrit);
					totalRecord=jobCategoryMastersAll.size();
				}
				//HeadQuarter >> District
				if(searchEntityType==3)
				{
					if(searchId!=0){
						districtMaster=districtMasterDAO.findById(searchId, false, false);
						criterion=Restrictions.eq("districtMaster", districtMaster);
						
					}else{
						headQuarterMaster=userMaster.getHeadQuarterMaster();
						Criterion criterion2=Restrictions.isNotNull("districtMaster");
						Criterion criterion3= Restrictions.eq("headQuarterMaster", headQuarterMaster);						
						criterion=Restrictions.and(criterion2, criterion3);
					}
					jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,statusCrit);
					jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion,statusCrit);
					totalRecord=jobCategoryMastersAll.size();
					
				}
			}
			else if(userMaster.getEntityType()==6){
				if(searchEntityType==2)
				{
					branchMaster=userMaster.getBranchMaster();
					if(searchId!=0){
						districtMaster=districtMasterDAO.findById(searchId, false, false);
						criterion=Restrictions.eq("districtMaster", districtMaster);
						Criterion criterion2=Restrictions.eq("branchMaster", branchMaster);
						criterion=Restrictions.and(criterion, criterion2);
					}else{
						criterion=Restrictions.eq("branchMaster", branchMaster);
					}
					jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,statusCrit);
					jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion,statusCrit);
					totalRecord=jobCategoryMastersAll.size();
				}
				else
				{
					branchMaster=userMaster.getBranchMaster();				
					criterion=Restrictions.eq("branchMaster", branchMaster);
					Criterion criterion2=Restrictions.eq("status", "A");
					criterion=Restrictions.and(criterion, criterion2);
					jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,statusCrit);
					jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion,statusCrit);
					totalRecord=jobCategoryMastersAll.size();
				}
			}else if(userMaster.getEntityType()==1){
				//for Admin // HeadQuarter @auther -------- Anurag 
				if(searchEntityType==1)
				{
				
					
					Criterion branchCat= Restrictions.isNull("branchMaster");					 
					Criterion parentJobCat=null;// Restrictions.isNull("parentJobCategoryId");
					headQuarterMaster=userMaster.getHeadQuarterMaster();
					criterion=Restrictions.isNotNull("headQuarterMaster");
					if(searchId!=0) {
						headQuarterMaster=new HeadQuarterMaster();headQuarterMaster.setHeadQuarterId(searchId);
						criterion=Restrictions.eq("headQuarterMaster",headQuarterMaster);
 						if(branchId!=null){
 							branchCat=Restrictions.eq("branchMaster.branchId", branchId);
 							parentJobCat=Restrictions.isNotNull("parentJobCategoryId");
 							
 							jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,branchCat,parentJobCat,statusCrit);
 							jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion,branchCat,parentJobCat,statusCrit);
 							
 					     }else{
 					    	 
 					    	jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,branchCat,statusCrit);
 							jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion,branchCat,statusCrit);
 							
 					     }
					
					}
					else{
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,statusCrit);
						jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion,statusCrit);
						
						
					}
					totalRecord=jobCategoryMastersAll.size();
				}
			}else{
				if(result){
					System.out.println("searchId :"+searchId);
					if(searchId!=0){
						districtMaster=districtMasterDAO.findById(searchId, false, false);
						criterion=Restrictions.eq("districtMaster", districtMaster);	
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion);
					}else{
						criterion=Restrictions.isNotNull("districtMaster");
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion);
					}
					jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion);
					totalRecord=jobCategoryMastersAll.size();
				}else{
					if(searchEntityType==3){
						districtMaster=districtMasterDAO.findById(searchId, false, false);
						criterion=Restrictions.eq("districtMaster", districtMaster);
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,statusCrit);
						jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion,statusCrit);
					}else{
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,statusCrit);
						jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,statusCrit);
					}	
					totalRecord=jobCategoryMastersAll.size();
				}
			}

			SortedMap<String,JobCategoryMaster>	sortedMap = new TreeMap<String,JobCategoryMaster>();
			if(sortOrderNoField.equals("districtMaster"))
			{
				sortOrderFieldName	=	"districtMaster";
			}

			int mapFlag=2;
			for (JobCategoryMaster jbc : jobCategoryMastersAll){
				String orderFieldName=null;
				if(jbc.getDistrictMaster()!=null){
					orderFieldName=jbc.getDistrictMaster().getDistrictName();
					if(sortOrderFieldName.equals("districtMaster")){
						orderFieldName=jbc.getDistrictMaster().getDistrictName()+"||"+jbc.getJobCategoryId();
						sortedMap.put(orderFieldName+"||",jbc);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}else{
					if(sortOrderFieldName.equals("districtMaster")){
						orderFieldName="0||"+jbc.getJobCategoryId();
						sortedMap.put(orderFieldName+"||",jbc);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedJobCatList.add((JobCategoryMaster) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedJobCatList.add((JobCategoryMaster) sortedMap.get(key));
				}
			}else{
				sortedJobCatList=jobCategoryMasters;
			}

			List<JobCategoryMaster>sortedJBCList=new ArrayList<JobCategoryMaster>();

			if(totalRecord<end)
				end=totalRecord;

			if(mapFlag!=2)
				sortedJBCList=sortedJobCatList.subList(start,end);
			else
				sortedJBCList=sortedJobCatList;

			//List<JobCategoryMaster> lstsortedjobCategory		=	jobCategoryMasters.subList(start,end);
			dmRecords.append("<table  id='jobCategoryTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLink("JobCategory Name",sortOrderFieldName,"jobCategoryName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink("Branch",sortOrderFieldName,"epiForFullTimeTeachers",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("District",sortOrderFieldName,"districtMaster",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink("EPI Required",sortOrderFieldName,"baseStatus",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
									
			responseText=PaginationAndSorting.responseSortingLink("Prescreen Attachment",sortOrderFieldName,"assessmentDocument",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink("Status",sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			dmRecords.append("<th width='15%' valign='top'>Actions</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			//System.out.println(" jobCategoryMasters :   "+jobCategoryMasters.size()+" lstsortedjobCategory "+lstsortedjobCategory);

			if(sortedJobCatList.size()==0)
				dmRecords.append("<tr><td colspan='6'>No JobCategory found</td></tr>" );
			for (JobCategoryMaster jc : sortedJBCList) 
			{
				dmRecords.append("<tr>" );		
				dmRecords.append("<td>"+jc.getJobCategoryName()+"</td>");
				
				
				if(jc.getBranchMaster()!=null){
					dmRecords.append("<td>"+jc.getBranchMaster().getBranchName()+"</td>");
				}else{
					dmRecords.append("<td></td>");
				}		
				
				
				if(jc.getDistrictMaster()!=null){
					dmRecords.append("<td>"+jc.getDistrictMaster().getDistrictName()+"</td>");
				}else{
					dmRecords.append("<td></td>");
				}

				String bStatus=jc.getBaseStatus().toString();
				dmRecords.append("<td>"+Character.toUpperCase(bStatus.charAt(0)) + bStatus.substring(1)+"</td>");
				
				
				
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				dmRecords.append("<td>");
				dmRecords.append(jc.getAssessmentDocument()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to see assessment document !'  id='jsi"+jc.getJobCategoryId()+"' onclick=\"downloadJsi('"+jc.getJobCategoryId()+"','jsi"+jc.getJobCategoryId()+"');"+windowFunc+"\">"+jc.getAssessmentDocument()+"</a>");
				dmRecords.append("<script>$('#jsi"+jc.getJobCategoryId()+"').tooltip();</script>");
				dmRecords.append("</td>");

				dmRecords.append("<td>");
				if(jc.getStatus().equalsIgnoreCase("A"))
					dmRecords.append("Active");
				else
					dmRecords.append("Inactive");
				dmRecords.append("</td>");


				dmRecords.append("<td>");
				if((userMaster.getEntityType()==5 || userMaster.getEntityType()==1) && ! userMaster.getRoleId().getRoleId().equals(12))
				{
				 
					dmRecords.append("<a href='javascript:void(0);' onclick='return editJobCategory("+jc.getJobCategoryId()+")'><i class='fa fa-pencil-square-o fa-lg' /></a>&nbsp;|&nbsp;");
					dmRecords.append("<a href='javascript:void(0);' onclick='return showHistory("+jc.getJobCategoryId()+")'><i class='fa fa-history fa-lg'/></a>&nbsp;|&nbsp;");
					
					if(jc.getStatus().equalsIgnoreCase("A"))
						dmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateJobCategory("+jc.getJobCategoryId()+",'I')\"><i class='fa fa-times fa-lg' /></a> &nbsp;|&nbsp");
					else
						dmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateJobCategory("+jc.getJobCategoryId()+",'A')\"><i class='fa fa-check fa-lg' /></a> &nbsp;|&nbsp");
					
					dmRecords.append("<a href='javascript:void(0);' onclick='return cloneJobCategory("+jc.getJobCategoryId()+")'>Clone</a>");
				}
				else
				{
					dmRecords.append("<a href='javascript:void(0);' onclick='return editJobCategory("+jc.getJobCategoryId()+")'><i class='fa fa-eye fa-lg' /></a>");
				}
				
				dmRecords.append("</td>");

				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}

	public String saveJobCategory(Integer jobCategoryId,String jobCategoryName,Integer districtId,Integer branchId,Integer headQuarterId,boolean baseStatus,boolean epiForFullTimeTeachers,String assessmentDocument,Integer offerJSI,boolean offerDSPQ,boolean jobInviteOnly,boolean preHireSmartPractices,boolean qualificationQuestion,String hiddenJob,Integer qquestionSetId,Integer parentJobCategoryId , String questionSetVal,String preHireId,String primaryJobCode,Integer assessmentGroupId,boolean postHireSmartPractices)
	{
		System.out.println("::::::::::::::::::::::::::::::: saveJobCategory :::::::::::::::::::::::questionSetVal = :"+questionSetVal);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster 			= 	null;
		String ipAddress=null;
		List<JobCategoryMaster> masterobj=null;
		boolean isNew = true;
		ipAddress= IPAddressUtility.getIpAddress(request);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		} else {
			userMaster = (UserMaster)session.getAttribute("userMaster");
		}
		try
		{
			DistrictMaster districtMaster=null;
			JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();
			jobCategoryMaster.setPrimaryJobCode(primaryJobCode);
			JobCategoryMasterHistory jobCategoryMasterHistory  = new JobCategoryMasterHistory();
			//*********************...55555.....********************************
			/*Criterion primaryJobCodeCriteria=Restrictions.eq("primaryJobCode", primaryJobCode);
			masterobj=jobCategoryMasterDAO.findByCriteria(primaryJobCodeCriteria);
			if(masterobj!=null &&  masterobj.size()>0)
				return "true2";*/
			//*******************...55555.....************************8		
			if(assessmentDocument!=null && !assessmentDocument.equals(""))
				jobCategoryMaster.setAssessmentDocument(assessmentDocument);
			else
				jobCategoryMaster.setAssessmentDocument(null);
			
			if(assessmentGroupId!=null && assessmentGroupId!=0)
				jobCategoryMaster.setAssessmentGroupId(assessmentGroupId);
			else
				jobCategoryMaster.setAssessmentGroupId(null);
			
			HeadQuarterMaster headQuarterMaster = null;
			if(userMaster.getEntityType()==5)
			{
				 headQuarterMaster =headQuarterMasterDAO.findById(userMaster.getHeadQuarterMaster().getHeadQuarterId(), false, false); 
				jobCategoryMaster.setHeadQuarterMaster(headQuarterMaster);
			}
			
			if(headQuarterId!=null && headQuarterId!=0)
			{
				headQuarterMaster =headQuarterMasterDAO.findById(headQuarterId, false, false); 
				jobCategoryMaster.setHeadQuarterMaster(headQuarterMaster);
			}
			BranchMaster branchMaster =null;
			if(branchId!=null && branchId!=0)
			{
				branchMaster =branchMasterDAO.findById(branchId, false, false); 
				jobCategoryMaster.setBranchMaster(branchMaster);
			}
			/*if(headQuarterId!=null && headQuarterId!=0)
			{
				BranchMaster branchMaster =branchMasterDAO.findById(branchId, false, false); 
				jobCategoryMaster.setBranchMaster(branchMaster);
			}*/
			
			QqQuestionSets qqQuestionSets = null;
			
			if(qquestionSetId!=null && !qquestionSetId.equals(""))
				qqQuestionSets = qqQuestionSetsDAO.findById(qquestionSetId, false, false);
			if(qualificationQuestion)
				jobCategoryMaster.setQuestionSets(qqQuestionSets);
			else
				jobCategoryMaster.setQuestionSets(null);
			
			
			
			if(parentJobCategoryId!=null && !parentJobCategoryId.equals(0))
			 {
				 JobCategoryMaster parentCategory = jobCategoryMasterDAO.findById(parentJobCategoryId, false, false);
                jobCategoryMaster.setParentJobCategoryId(parentCategory);
			 }
			
			
			jobCategoryMaster.setJobCategoryName(jobCategoryName);
			jobCategoryMaster.setBaseStatus(baseStatus);
			jobCategoryMaster.setEpiForFullTimeTeachers(epiForFullTimeTeachers);
			if(jobCategoryId == null || jobCategoryId!=0)
			   jobCategoryMaster.setStatus("A");
			Boolean hideFlag=null;
			if(jobInviteOnly){
				if(hiddenJob.equalsIgnoreCase("0"))
					hideFlag=false;
				else if(hiddenJob.equalsIgnoreCase("1"))
						hideFlag=true;
				jobCategoryMaster.setHiddenJob(hideFlag);
				
			}
			
			
			jobCategoryMaster.setOfferJSI(offerJSI);
			jobCategoryMaster.setOfferDSPQ(offerDSPQ);
			jobCategoryMaster.setOfferPortfolioNeeded(false);
			jobCategoryMaster.setJobInviteOnly(jobInviteOnly);
			jobCategoryMaster.setPreHireSmartPractices(preHireSmartPractices);
			jobCategoryMaster.setPostHireSmartPractices(postHireSmartPractices);
			Integer spAssessmentId = 0;
			/*SpAssessmentConfig spAssessmentConfig = null;
			try {
				if(preHireId!=null && preHireId.length()>0)
					spAssessmentConfig = spAssessmentConfigDAO.findById(Integer.parseInt(preHireId), false, false);
			} catch (Exception e) {
				e.printStackTrace();
			}*/
			if(preHireId!=null && preHireId.length()>0)
				spAssessmentId = Integer.parseInt(preHireId);
			if(preHireSmartPractices)
				 jobCategoryMaster.setSpAssessmentId(spAssessmentId);
			else
				 jobCategoryMaster.setSpAssessmentId(spAssessmentId);
			
			jobCategoryMaster.setQualificationQuestion(qualificationQuestion);
			jobCategoryMaster.setJobCategoryId(jobCategoryId);
			
			jobCategoryMaster.setDistrictSpecificItemsForIMCandidates(false);
			jobCategoryMaster.setEpiForIMCandidates(false);
			jobCategoryMaster.setJsiForIMCandidates(0);
			jobCategoryMaster.setOfferDistrictSpecificItems(false);
			jobCategoryMaster.setOfferQualificationItems(false);
			jobCategoryMaster.setOfferVVIForInternalCandidates(false);
			jobCategoryMaster.setOfferVirtualVideoInterview(false);
			jobCategoryMaster.setPortfolioForIMCandidates(false);
			jobCategoryMaster.setQualificationItemsForIMCandidates(false);
			jobCategoryMaster.setSendAutoVVILink(false);
			jobCategoryMaster.setAttachDSPQFromJC(false);
			jobCategoryMaster.setAttachSLCFromJC(false);
			
			
			boolean duplicate=false;			
			String temp = session.getId();
			System.out.println(" session ::: "+temp);
			List<TempDistricts> tempDistrictsList=null;
			Criterion criterion=Restrictions.eq("tempId", temp);
			tempDistrictsList=tempDistrictsDAO.findByCriteria(criterion);
						
			 SessionFactory sessionFactory=jobCategoryMasterDAO.getSessionFactory();
			 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			 Transaction txOpen =statelesSsession.beginTransaction();
			 
			 // @ Anurag
			 
			 
			 
			if(tempDistrictsList!=null && tempDistrictsList.size()>0 && jobCategoryMaster.getJobCategoryId()==null  && (userMaster.getEntityType()==5 ||   userMaster.getEntityType()==1 ) )
			{
				for (TempDistricts tempDistricts : tempDistrictsList) {			
					jobCategoryMaster.setDistrictMaster(tempDistricts.getDistrictMaster());
					duplicate=jobCategoryMasterDAO.checkDuplicateJobCategoryWithBranch(null,branchMaster,jobCategoryName,tempDistricts.getDistrictMaster());
					if(!duplicate){
						
						statelesSsession.insert(jobCategoryMaster);
						isNew=true;
						
					}
				}
				
				txOpen.commit();
				statelesSsession.close();
				saveReferenceCheckQuestionSet(userMaster , headQuarterMaster, jobCategoryMaster,questionSetVal);  // @ Anurag
				saveToJobcategoryMasterHistory(jobCategoryMaster ,userMaster,ipAddress,isNew,questionSetVal);  // for saving data into jobcategorymastorhistory table
				  	
				  if(duplicate==true)
					 return "true1";
					 
				 return "false";
			}
			else if(tempDistrictsList!=null && tempDistrictsList.size()>0 && jobCategoryMaster.getJobCategoryId()!=null && ( userMaster.getEntityType()==5 || userMaster.getEntityType()==1))
			{
				for (TempDistricts tempDistricts : tempDistrictsList) {			
					jobCategoryMaster.setDistrictMaster(tempDistricts.getDistrictMaster());
					jobCategoryMaster.setJobCategoryId(null);
					duplicate=jobCategoryMasterDAO.checkDuplicateJobCategoryWithBranch(null,branchMaster,jobCategoryName,tempDistricts.getDistrictMaster());
					if(!duplicate){
						statelesSsession.insert(jobCategoryMaster);
						isNew=true;
					}
				}
				if(tempDistrictsList.size()>1)
				{
				 txOpen.commit();
				 statelesSsession.close();
				 saveReferenceCheckQuestionSet(userMaster , headQuarterMaster, jobCategoryMaster,questionSetVal);  // @ Anurag
				 saveToJobcategoryMasterHistory(jobCategoryMaster ,userMaster,ipAddress,isNew,questionSetVal);  // for saving data into jobcategorymastorhistory table
				 	
				}
				else
				{
					jobCategoryMaster.setJobCategoryId(jobCategoryId);	
					isNew = (jobCategoryMaster.getJobCategoryId()==null ||  jobCategoryMaster.getJobCategoryId()==0)?true : false;
					saveToJobcategoryMasterHistory(jobCategoryMaster ,userMaster,ipAddress,isNew,questionSetVal);  // for saving data into jobcategorymastorhistory table
					jobCategoryMasterDAO.makePersistent(jobCategoryMaster);
					saveReferenceCheckQuestionSet(userMaster , headQuarterMaster, jobCategoryMaster,questionSetVal);  // @ Anurag	
				}
				 return "false";
			}				
			else 
				if(jobCategoryMaster.getJobCategoryId()!=null && jobCategoryMaster.getJobCategoryId()!=0 )
			{					
				if(districtId!=null && districtId!=0){
					jobCategoryMaster.setJobCategoryId(jobCategoryId);						
					districtMaster=districtMasterDAO.findByDistrictId(districtId.toString());
					jobCategoryMaster.setDistrictMaster(districtMaster);
					}
				JobCategoryMaster job=jobCategoryMasterDAO.findById(jobCategoryMaster.getJobCategoryId(), false, false);
				jobCategoryMaster.setStatus(job.getStatus());  // @ Anurag
				if(!jobCategoryMaster.getJobCategoryName().toString().equalsIgnoreCase(job.getJobCategoryName().toString()))
				{
					Criterion criterion2=Restrictions.eq("jobCategoryName", jobCategoryMaster.getJobCategoryName());
					Criterion criterion3=Restrictions.eq("branchMaster", jobCategoryMaster.getBranchMaster());
					Criterion criterion4=Restrictions.eq("districtMaster", jobCategoryMaster.getDistrictMaster());
					List<JobCategoryMaster>categoryMastersList=null;
					categoryMastersList=jobCategoryMasterDAO.findByCriteria(criterion2,criterion3,criterion4);
					if(categoryMastersList!=null&& categoryMastersList.size()>0)
						return "true1";
					//duplicate=jobCategoryMasterDAO.checkDuplicateJobCategoryWithBranch(null,branchMaster,jobCategoryName,districtMaster);
				
				}
				isNew = (jobCategoryMaster.getJobCategoryId()==null ||  jobCategoryMaster.getJobCategoryId()==0)?true : false;
				saveToJobcategoryMasterHistory(jobCategoryMaster ,userMaster,ipAddress,isNew,questionSetVal);  // for saving data into jobcategorymastorhistory table
				jobCategoryMasterDAO.makePersistent(jobCategoryMaster);
				saveReferenceCheckQuestionSet(userMaster , headQuarterMaster, jobCategoryMaster,questionSetVal);  // @ Anurag
				return "false";
				
			}
			else
			{
				isNew = (jobCategoryMaster.getJobCategoryId()==null ||  jobCategoryMaster.getJobCategoryId()==0)?true : false;
				jobCategoryMasterDAO.makePersistent(jobCategoryMaster);
				saveToJobcategoryMasterHistory(jobCategoryMaster ,userMaster,ipAddress,isNew,questionSetVal);  // for saving data into jobcategorymastorhistory table
				saveReferenceCheckQuestionSet(userMaster , headQuarterMaster, jobCategoryMaster,questionSetVal);  // @ Anurag
				return "false";
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "";
	}
	public boolean activateDeactivateJobCategory(Integer jobcategoryId,String status)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false); 
		String ipAddress=null;
		boolean isNew = false;
		ipAddress= IPAddressUtility.getIpAddress(request);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try{
			UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
			JobCategoryMaster jobCategoryMaster=jobCategoryMasterDAO.findById(jobcategoryId, false, false);	
			
			if(userMaster.getEntityType()==5 || userMaster.getEntityType()==1)
			{				
				jobCategoryMaster.setStatus(status);
				saveToJobcategoryMasterHistory(jobCategoryMaster, userMaster, ipAddress, isNew, "nochange")	;
				jobCategoryMasterDAO.makePersistent(jobCategoryMaster);
			}
			
			/*if(userMaster.getEntityType()==6 && jobCategoryMaster.getParentCategoryId()==null)
			{
				jobCategoryMaster.setJobCategoryId(null);				
				jobCategoryMaster.setParentCategoryId(jobcategoryId);
				jobCategoryMaster.setBranchMaster(userMaster.getBranchMaster());
				jobCategoryMasterDAO.makePersistent(jobCategoryMaster);
			}
			else
			{				
				jobCategoryMaster.setStatus(status);
				jobCategoryMasterDAO.makePersistent(jobCategoryMaster);
			}*/
			
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Transactional(readOnly=false)
	public JobCategoryMaster getJobCategoryId(Integer jobCategoryId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();
		try{
			if(jobCategoryId!=null){
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				if(jobCategoryMaster!=null &&  jobCategoryMaster.getAssessmentDocument()!=null){
					session.setAttribute("previousJSIFileName", jobCategoryMaster.getAssessmentDocument());	
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return jobCategoryMaster;
	}


	/* ====== Gagan : This method is used for  download Jsi =======*/
	@Transactional(readOnly=false)
	public String downloadJsi(int jobCategoryId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		String path="";
		try 
		{
			String source="";
			String target="";
			JobCategoryMaster jc=new JobCategoryMaster();
			if(jobCategoryId!=0)
				jc=jobCategoryMasterDAO.findById(jobCategoryId, false, false);

			String fileName= jc.getAssessmentDocument();
			
			if(jc.getDistrictMaster()!=null)
				source = Utility.getValueOfPropByKey("districtRootPath")+jc.getDistrictMaster().getDistrictId()+"/jobcategory/"+jc.getAssessmentDocument();
			else
				source = Utility.getValueOfPropByKey("districtRootPath")+"/jobcategory/"+jc.getAssessmentDocument();

			if(jc.getDistrictMaster()!=null)
				target = context.getServletContext().getRealPath("/")+"/"+"/district/"+jc.getDistrictMaster().getDistrictId()+"/";
			else
				target = context.getServletContext().getRealPath("/")+"/"+"/district/"+"/";
			
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());

			FileUtils.copyFile(sourceFile, targetFile);
			
			if(jc.getDistrictMaster()!=null)
				path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+jc.getDistrictMaster().getDistrictId()+"/"+jc.getAssessmentDocument();
			else
				path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+"/"+jc.getAssessmentDocument();
			 	

			//if(districtORSchoool.equalsIgnoreCase("district")){
			//path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtId+"/"+docFileName; 	
			/* }else{
	        	  path = Utility.getValueOfPropByKey("contextBasePath")+"/school/"+districtORSchooolId+"/"+docFileName;
	        }*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;

	}
	
	/* ====== Gagan : This method is used for  download Jsi =======*/
	public String removeJsi(int jobCategoryId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try 
		{
			JobCategoryMaster jc=null;
			if(jobCategoryId!=0)
			{
				jc=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				SecondaryStatus  secondaryStatus = 	secondaryStatusDAO.findSecondaryStatusByDistrict(jc.getDistrictMaster(),jc);
				if(secondaryStatus!=null)
					return "3";
			}

			if(jc!=null ){
				File file=null;
				if(jc.getDistrictMaster()!=null)
					file = new File(Utility.getValueOfPropByKey("districtRootPath")+jc.getDistrictMaster().getDistrictId()+"/jobcategory/"+jc.getAssessmentDocument());
				else
					file = new File(Utility.getValueOfPropByKey("districtRootPath")+"/jobcategory/"+jc.getAssessmentDocument());
				//System.out.println("Remove Jsi 1111 "+file.exists()+"\n"+file.getPath());
				if(file.exists()){
					if(file.delete()){
						System.out.println(file.getName()+" deleted");
					}else{
						System.out.println("Delete operation for Jsi is failed.");
						return "2";
					}
					jc.setAssessmentDocument(null);
					jobCategoryMasterDAO.makePersistent(jc);
					return "1";
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return "2";
		}
		return "2";
	}

		
	// For VVI
	public String getStatusList(String districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		StringBuffer status=	new StringBuffer();
		
		List<StatusMaster> statusMasterList = null;
		List<StatusMaster> lstStatusMaster = null;
		
		try
		{
			
			Map<Integer,SecondaryStatus> mapSStatus = new HashMap<Integer, SecondaryStatus>();
			
			DistrictMaster districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			List<SecondaryStatus> lstSecStatus=	secondaryStatusDAO.findSecondaryStatusOnlyStatus(districtMaster);
			List<SecondaryStatus> lstsecondaryStatus=  secondaryStatusDAO.findSecondaryStatusOnly(districtMaster);
			
			
			if(lstSecStatus!=null)
			for(SecondaryStatus secStatus : lstSecStatus){
				mapSStatus.put(secStatus.getStatusMaster().getStatusId(),secStatus);
			}
			

			SecondaryStatus secondaryStatus=null;

			String[] statuss = {"hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
			lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statuss);
			
			statusMasterList = new ArrayList<StatusMaster>();
			
			for(StatusMaster sm: lstStatusMaster){
				if(sm.getBanchmarkStatus()==1){
					secondaryStatus=mapSStatus.get(sm.getStatusId());
					if(secondaryStatus!=null){
						sm.setStatus(secondaryStatus.getSecondaryStatusName());
					}
				}
				statusMasterList.add(sm);
			}
			
			status.append("<select class='form-control' id='slctStatusID'>");
			status.append("<option id='' value='' ></option>");
			
			if(statusMasterList!=null && statusMasterList.size()>0)
			{
				for(StatusMaster sm:statusMasterList)
				{
					status.append("<option value='"+sm.getStatusId()+"' >"+sm.getStatus()+"</option>");
				}
			}
			
			if(lstsecondaryStatus!=null && lstsecondaryStatus.size()>0)
			{
				for(SecondaryStatus ssm:lstsecondaryStatus)
				{
					status.append("<option value='SSID_"+ssm.getSecondaryStatusId()+"' >"+ssm.getSecondaryStatusName()+"</option>");
				}
			}
			
			
			status.append("</select>");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	
		return status.toString();
	}
	
	@Transactional(readOnly=false)
	public DistrictMaster getVVIDefaultFields(Integer districtId)
	{
		System.out.println(" ====== getVVIDefaultFields =======");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		DistrictMaster districtMaster	=	new DistrictMaster();
		try{
			if(districtId!=null){
				districtMaster = districtMasterDAO.findById(districtId, false, false);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtMaster;
	}
	
	public String displayAllDistrictAssessment(String districtId,Integer jobcategoryId)
	{
		System.out.println(" =================== displayAllDistrictAssessment ================== ");
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		StringBuffer sbAttachSchool=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		boolean selectedDAS = false;
		DistrictMaster districtMaster  = null;
		JobCategoryMaster jobCategoryMaster = null;
		try
		{
			districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			if(districtMaster!=null)
			{
				if(jobcategoryId!=null && !jobcategoryId.equals(""))
					jobCategoryMaster = jobCategoryMasterDAO.findById(jobcategoryId, false, false);
				
				//Job category Wise Data
				if(jobCategoryMaster!=null)
				{
					List<DistrictAssessmentDetail> AlldistrictAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
					List<DistrictAssessmentDetail> selecteddistAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
					
					AlldistrictAssessmentDetails = districtAssessmentDetailDAO.findByDistrict(districtMaster);
					
					
					String daIds = jobCategoryMaster.getDistrictAssessmentId();
					
					
					String arr[]=null;
					List multiDAIds = new ArrayList();
					
					if(daIds!=null && !daIds.equals(""))
					{
						if(daIds!="" && daIds.length() > 0)
						{
							arr=daIds.split("#");
						}
						
						if(arr!=null && arr.length > 0)
						{
							for(int i=0;i<arr.length;i++)
							{
								multiDAIds.add(Integer.parseInt((arr[i])));	
							}
						}
					}
					
					selecteddistAssessmentDetails = districtAssessmentDetailDAO.findByDistAssessmentIds(multiDAIds);
					
					System.out.println(" selecteddistAssessmentDetails :: "+selecteddistAssessmentDetails.size());
					
					AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails);

					System.out.println(" AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails) :: "+AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails));
					System.out.println(" AlldistrictAssessmentDetails :: "+AlldistrictAssessmentDetails.size());
					//Unselected DA List
					sb.append("<select multiple id=\"lstDistrictAdmins\" name=\"lstDistrictAdmins\" class=\"form-control\" style=\"height: 150px;\" >");
					if(AlldistrictAssessmentDetails.size()>0)
					{
						for (DistrictAssessmentDetail selectedDASMT : AlldistrictAssessmentDetails) {
							sb.append("<option value="+selectedDASMT.getDistrictAssessmentId()+">"+selectedDASMT.getDistrictAssessmentName()+"</option>");
						}
				//		selectedDAS = true;
					}
					sb.append("</select>");
					
					//Selected DA List
					sbAttachSchool.append("<select multiple class=\"form-control\" id=\"attachedDAList\" name=\"attachedDAList\" style=\"height: 150px;\">");
					if(selecteddistAssessmentDetails.size()>0)
					{
						for (DistrictAssessmentDetail selectdDASMT : selecteddistAssessmentDetails) {
							sbAttachSchool.append("<option value="+selectdDASMT.getDistrictAssessmentId()+">"+selectdDASMT.getDistrictAssessmentName()+"</option>");
						}
						System.out.println("01");
						selectedDAS = true;
					}
					sbAttachSchool.append("</select>");
				}else
				{
					List<DistrictAssessmentDetail> AlldistrictAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
					List<DistrictAssessmentDetail> selecteddistAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
					
					AlldistrictAssessmentDetails = districtAssessmentDetailDAO.findByDistrict(districtMaster);
					
					
				    String daIds = districtMaster.getDistrictAssessmentId();
					
					
					String arr[]=null;
					List multiDAIds = new ArrayList();
					
					if(daIds!=null && !daIds.equals(""))
					{
						if(daIds!="" && daIds.length() > 0)
						{
							arr=daIds.split("#");
						}
						
						if(arr!=null && arr.length > 0)
						{
							for(int i=0;i<arr.length;i++)
							{
								multiDAIds.add(Integer.parseInt((arr[i])));	
							}
						}
					}
					
					selecteddistAssessmentDetails = districtAssessmentDetailDAO.findByDistAssessmentIds(multiDAIds);
					
					System.out.println(" selecteddistAssessmentDetails :: "+selecteddistAssessmentDetails.size());
					
					AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails);

					System.out.println(" AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails) :: "+AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails));
					System.out.println(" AlldistrictAssessmentDetails :: "+AlldistrictAssessmentDetails.size());
					//Unselected DA List
					sb.append("<select multiple id=\"lstDistrictAdmins\" name=\"lstDistrictAdmins\" class=\"form-control\" style=\"height: 150px;\" >");
					if(AlldistrictAssessmentDetails.size()>0)
					{
						for (DistrictAssessmentDetail selectedDASMT : AlldistrictAssessmentDetails) {
							sb.append("<option value="+selectedDASMT.getDistrictAssessmentId()+">"+selectedDASMT.getDistrictAssessmentName()+"</option>");
						}
				//		selectedDAS = true;
					}
					sb.append("</select>");
					
					//Selected DA List
					sbAttachSchool.append("<select multiple class=\"form-control\" id=\"attachedDAList\" name=\"attachedDAList\" style=\"height: 150px;\">");
					if(selecteddistAssessmentDetails.size()>0)
					{
						for (DistrictAssessmentDetail selectdDASMT : selecteddistAssessmentDetails) {
							sbAttachSchool.append("<option value="+selectdDASMT.getDistrictAssessmentId()+">"+selectdDASMT.getDistrictAssessmentName()+"</option>");
						}
						selectedDAS = true;	
					}
					sbAttachSchool.append("</select>");
				}
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		System.out.println(" sb.toString() :: "+sb.toString());
		System.out.println(" sbAttachSchool.toString() :: "+sbAttachSchool.toString());
		System.out.println(" selectedDAS :: "+selectedDAS);

		return sb.toString()+"||"+sbAttachSchool.toString()+"||"+selectedDAS;
	}
	
	/*	
	 * 		Get Reference Check List
	 * 		Date	:	24 Feb 2015
	 * 		By		:	Hanzala Subhani
	 * 
	 */
	
	public String getReferenceByDistrictList(int districtId, int jobCategoryId){
		StringBuffer sb = new StringBuffer();
		DistrictMaster districtMaster							=	null;
		List<ReferenceCheckQuestionSet> refChkQuestionSet		= 	null;
		List<ReferenceQuestionSets>	referenceQuestionSetsList 	= 	null;
		JobCategoryMaster jobCategoryMaster						=	null;
		 
		List<Integer> ques = new ArrayList<Integer>();
		Integer counter = 1; 
		Integer questionId = null;
		try 
		{
			if(""+districtId!="")
			districtMaster			=	districtMasterDAO.findById(districtId, false, false);
			jobCategoryMaster		=	jobCategoryMasterDAO.findById(jobCategoryId, false, false);
			refChkQuestionSet 		= 	referenceCheckQuestionSetDAO.getReferenceByDistrictAndJobCategory(districtMaster, jobCategoryMaster);
			
			if(refChkQuestionSet.size()>0 && refChkQuestionSet !=null && refChkQuestionSet.get(0) != null)
				questionId	=	refChkQuestionSet.get(0).getReferenceQuestionSets().getID();
				/*	
					jobCategoryMaster	=	jobCategoryMasterDAO.findById(jobCategoryId, false, false);
					refChkSetList 		= 	referenceCheckQuestionSetDAO.getReferenceByDistrictAndJobCategory(districtMaster, jobCategoryMaster);
					for(ReferenceCheckQuestionSet qs: refChkSetList)
					{
						ques.add(qs.getDistrictSpecificRefChkQuestions().getQuestionId());
					}
					refChkList 	= 	districtSpecificRefChkQuestionsDAO.getDistrictRefChkQuestionByDistrictAndJoCategory(districtMaster,ques);
				*/
				referenceQuestionSetsList	=	referenceQuestionSetsDAO.findByDistrictActive(districtMaster);
				sb.append("<select id='avlbList' name='avlbList' class='span4'>");
				sb.append("<option value=''>Please Select Question Set</option>");
				if(referenceQuestionSetsList!=null && referenceQuestionSetsList.size() > 0)
					for(ReferenceQuestionSets refChkRecord : referenceQuestionSetsList)
						if(refChkQuestionSet!=null && refChkQuestionSet.size() > 0 && refChkQuestionSet.get(0) != null){
							if(questionId.equals(refChkRecord.getID())){
								sb.append("<option selected value='"+refChkRecord.getID()+"'>"+refChkRecord.getQuestionSetText()+"</option>");
							} else {
								sb.append("<option value='"+refChkRecord.getID()+"'>"+refChkRecord.getQuestionSetText()+"</option>");
							}
						} else {
							sb.append("<option value='"+refChkRecord.getID()+"'>"+refChkRecord.getQuestionSetText()+"</option>");
						}
						// sb.append("<option value='"+refChkRecord.getID()+"'>"+refChkRecord.getQuestionSetText()+"</option>");
			  sb.append("</select>");
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String getQQuestionSetByDistrictAndJobCategoryList(Integer districtId, Integer jobCategoryId){
        System.out.println("call");
		System.out.println(" ******* getQQuestionSetByDistrictAndJobCategoryList**** districtId: "+districtId+" jobCategoryId : "+jobCategoryId);
		
        WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		StringBuffer sb = new StringBuffer();
        DistrictMaster districtMaster = null;
        JobCategoryMaster jobCategoryMaster = null;
        
        List<QqQuestionSets> districtSpecificQQList = null;
		
	 try{
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		if(districtId!=null){			
		districtMaster = districtMasterDAO.findById(districtId, false, false);
	    Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
		districtSpecificQQList = qqQuestionSetsDAO.findByCriteria(criterion1);
	    }else{
		districtSpecificQQList = new ArrayList<QqQuestionSets>();
	    }
        
		if(jobCategoryId!=null){
			 jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
		}
		
		sb.append("<select id='qqAvlbList' name='qqAvlbList' class='span4'>");
		sb.append("<option value=''>Please Select QQ Set</option>");
		
		if(districtSpecificQQList!=null)
	    {
			Integer selectOpt=0;
			try {
				if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSets()!=null){
					selectOpt= jobCategoryMaster.getQuestionSets().getID();
				}else{
					selectOpt= districtMaster.getQqQuestionSets().getID();
				}
			} catch (Exception e) {}
		  for(QqQuestionSets list: districtSpecificQQList)
	       {
			  if(list.getID().equals(selectOpt))
	    		  sb.append("<option selected value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
			  else
				  sb.append("<option value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
	       }
	     }
		sb.append("</select>");
		
	 }catch(Exception e){
        	 e.printStackTrace();
         }
		return sb.toString();
	}
	
	public String getJobCategoryByDistrictId(int districtId)
	{
		StringBuffer sb	=	new StringBuffer();
		try {
			DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
			/*=== values to dropdown from jobCategoryMaster Table ===*/
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
			//sb.append("<select id='parentJobCategoryId' name='parentJobCategoryId' class='form-control'>");
			sb.append("<option value='0'>Select Job Category</option>");
			if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
				for(JobCategoryMaster jb: jobCategoryMasterlst)
				{
					if(jb.getParentJobCategoryId()==null){
						sb.append("<option value='"+jb.getJobCategoryId()+"'>"+jb.getJobCategoryName()+"</option>");
					}
				}
			}

			//sb.append("</select>");
		} catch (Exception e) 
		{
			e.printStackTrace();
		}

		return sb.toString();
	}	
	
	public List<BranchMaster> getFieldOfBranchList(String BranchName,int headQuarterId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		 
		userMaster = (UserMaster)session.getAttribute("userMaster");		
		List<BranchMaster> branchMasterList =  new ArrayList<BranchMaster>();
		List<BranchMaster> fieldOfbranchList1 = null;
		List<BranchMaster> fieldOfbranchList2 = null;			
		
		// for TMA
		Integer hqId = 0;
		if(userMaster!=null){
			if(userMaster.getEntityType()==1){
				hqId=headQuarterId;
			}
			else{
				hqId=userMaster.getHeadQuarterMaster().getHeadQuarterId();
			}
		}
		
		
		try{

			Criterion criterion = Restrictions.like("branchName", BranchName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "A");
			Criterion criterion3 = Restrictions.eq("headQuarterMaster.headQuarterId", hqId);
			if(BranchName.trim()!=null && BranchName.trim().length()>0){
				fieldOfbranchList1 = branchMasterDAO.findWithLimit(Order.asc("branchName"), 0, 25, criterion,criterion1,criterion3);

				Criterion criterion2 = Restrictions.ilike("branchName","% "+BranchName.trim()+"%" );
				fieldOfbranchList2 = branchMasterDAO.findWithLimit(Order.asc("branchName"), 0, 25, criterion2,criterion1,criterion3);

				branchMasterList.addAll(fieldOfbranchList1);
				branchMasterList.addAll(fieldOfbranchList2);
				Set<BranchMaster> setBranch = new LinkedHashSet<BranchMaster>(branchMasterList);
				branchMasterList = new ArrayList<BranchMaster>(new LinkedHashSet<BranchMaster>(setBranch));
				
			}else{
				fieldOfbranchList1 = branchMasterDAO.findWithLimit(Order.asc("branchName"), 0, 25, criterion1,criterion3);
				branchMasterList.addAll(fieldOfbranchList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return branchMasterList;
	}
	public List<DistrictMaster> getFieldOfDistrictList(String DistrictName,int branchId,int headQuarterId)
	{
		/* ========  For Session time Out Error =========*/
		System.out.println("headQuarterId " +headQuarterId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}

		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
		Criterion criterion1 = Restrictions.eq("status", "A");
		try{
			
			if(branchId!=0)
			{
				System.out.println("branchId " +branchId);
				if(DistrictName.trim()!=null && DistrictName.trim().length()>0)
				{
					List<HqBranchesDistricts> hqBranchesDistrictsList = new ArrayList<HqBranchesDistricts>();
					BranchMaster branchMaster=null;
					branchMaster=branchMasterDAO.findById(branchId, false, false);
					Criterion criterion3=Restrictions.eq("branchMaster", branchMaster);
					hqBranchesDistrictsList=hqBranchesDistrictsDAO.findByCriteria(criterion3);
					if(hqBranchesDistrictsList!=null && hqBranchesDistrictsList.size()>0 && hqBranchesDistrictsList.get(0).getDistrictId()!="")
					{
						String dist = "";
						if(hqBranchesDistrictsList!=null && hqBranchesDistrictsList.size()>0)
						{
							dist = hqBranchesDistrictsList.get(0).getDistrictId();
						}
						String[] distIds = null;
						if(dist!=null && dist.length()>0)
							distIds = dist.split(",");
							
						List<Integer> districtArray =new ArrayList<Integer>();
						for(int i=0;i<distIds.length;i++)
						{
							districtArray.add(Integer.parseInt(distIds[i]));
						}
						Criterion criterion2 = Restrictions.in("districtId", districtArray);				
						List<DistrictMaster> districtMasters = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1,criterion2);
						
						Criterion criterion4 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
						fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion4,criterion1,criterion2);
						
						districtMasterList.addAll(districtMasters);
						districtMasterList.addAll(fieldOfDistrictList2);
						Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
						districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
					}
				}
			}
			else if(headQuarterId!=0)
			{
				System.out.println("headQuarterId " +headQuarterId);
				if(DistrictName.trim()!=null && DistrictName.trim().length()>0)
				{
					List<HqBranchesDistricts> hqBranchesDistrictsList = new ArrayList<HqBranchesDistricts>();
					HeadQuarterMaster headQuarterMaster=null;
					headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
					Criterion criterion3=Restrictions.eq("headQuarterMaster", headQuarterMaster);
					
					hqBranchesDistrictsList=hqBranchesDistrictsDAO.findByCriteria(criterion3);
					if(hqBranchesDistrictsList!=null && hqBranchesDistrictsList.size()>0)
					{						
						String[] distIds = null;
						List<Integer> districtArray =new ArrayList<Integer>();
						if(hqBranchesDistrictsList!=null && hqBranchesDistrictsList.size()>0)
						{
							for (HqBranchesDistricts hqBranchesDistricts : hqBranchesDistrictsList) {
								if(hqBranchesDistricts.getDistrictId()!=null && hqBranchesDistricts.getDistrictId()!="")
								{
									distIds = hqBranchesDistricts.getDistrictId().split(",");
									for(int i=0;i<distIds.length;i++)
									{
										if(distIds[i].trim()!="")
										districtArray.add(Integer.parseInt(distIds[i]));
									}
								}
							}							
						}											
						
						System.out.println(districtArray.size());
						Criterion criterion2 = Restrictions.in("districtId", districtArray);				
						List<DistrictMaster> districtMasters = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1,criterion2);
						
						Criterion criterion4 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
						fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion4,criterion1,criterion2);
						
						districtMasterList.addAll(districtMasters);
						districtMasterList.addAll(fieldOfDistrictList2);
						Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
						districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
					}
				}
			}
			else
			{
				
				if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
					fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);

					Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
					fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);

					districtMasterList.addAll(fieldOfDistrictList1);
					districtMasterList.addAll(fieldOfDistrictList2);
					Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
					districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));

				}else{
					fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion1);
					districtMasterList.addAll(fieldOfDistrictList1);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return districtMasterList;
	}
	public String tempListSize()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuilder sb=new StringBuilder();
		String temp = session.getId();
		System.out.println(" session ::: "+temp);
		List<TempDistricts> tempDistrictsList=null;
		Criterion criterion=Restrictions.eq("tempId", temp);
		tempDistrictsList=tempDistrictsDAO.findByCriteria(criterion);
		if(tempDistrictsList!=null && tempDistrictsList.size()>0)
			for (TempDistricts tempDistricts : tempDistrictsList) {
				sb.append(tempDistricts.getDistrictMaster().getDistrictId()+",");
			}
		return sb.toString();
	}
	public String showEditDistricts(Integer jobCategoryId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;		
		
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		userMaster = (UserMaster)session.getAttribute("userMaster");
		JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();
		List<JobOrder> jobsByCategory =  new ArrayList<JobOrder>();
		Boolean jobExists = false;
		try{
			if(jobCategoryId!=null){
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				
				jobsByCategory = jobOrderDAO.findJobOrderByJobCategoryMasterAll(jobCategoryMaster);
				if(jobsByCategory!=null && jobsByCategory.size()>0){
					System.out.println(" jobsByCategory list size ::: "+jobsByCategory.size());
					jobExists = true;
				}
				System.out.println(" jobExists :: "+jobExists);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		StringBuffer sb = new StringBuffer();
				
		sb.append("<table id='tempDistrictTable' border='0' class='table table-bordered table-striped' >");
		sb.append("<thead class='bg'>");
		sb.append("<tr>");
		String responseText="";
		sb.append("<th width='40%' valign='top'>District Name</th>");
		
		//sb.append("<th width='25%' valign='top'>Added On</th>");
		
		sb.append("<th width='20%' valign='top'>Added By</th>");
		
		sb.append("<th width='15%'>Actions</th>");
		sb.append("</tr>");
		sb.append("</thead>");
		sb.append("<tr>" );
		if(jobCategoryMaster.getDistrictMaster()!=null){

			sb.append("<td>"+jobCategoryMaster.getDistrictMaster().getDistrictName()+"</td>");
			//sb.append("<td></td>");
			sb.append("<td>Head Quarter</td>");
			sb.append("<td>");
			if((userMaster.getEntityType()==5 || userMaster.getEntityType()==1) && !jobExists)
				sb.append("<a href='javascript:void(0);' onclick=\"return removeDistrictWithJobCategory("+jobCategoryMaster.getJobCategoryId()+")\">Remove</a>");
			sb.append("</td>");
		}
		else{
			sb.append("<td>No District Attached</td>");
		}
		sb.append("</tr>");
		sb.append("</table>");
		return sb.toString();
	}
	
	public String removeDistrictWithJobCategory(Integer jobCategoryId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		String result = "";
		JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();
		List<SecondaryStatus> secondaryStatus = null;
		DistrictMaster districtMaster = null;
		if(jobCategoryId!=null){
			jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
			districtMaster = jobCategoryMaster.getDistrictMaster();
			jobCategoryMaster.setDistrictMaster(null);
			jobCategoryMasterDAO.updatePersistent(jobCategoryMaster);
			
			Criterion criterion =	Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criterion criterion1 =	Restrictions.eq("districtMaster",districtMaster);
			secondaryStatus = secondaryStatusDAO.findByCriteria(criterion,criterion1);
			
			SessionFactory sessionFactory=secondaryStatusDAO.getSessionFactory();
			 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			 Transaction txOpen =statelesSsession.beginTransaction();
			if(secondaryStatus!=null && secondaryStatus.size()>0){
				for(SecondaryStatus secStatus : secondaryStatus){
					System.out.println("secStatus id inactivate ::  "+secStatus.getSecondaryStatusId());
					secStatus.setStatus("I");
					statelesSsession.update(secStatus);
				}
				txOpen.commit();
				statelesSsession.close();
			}
			result = "success";
		}
		
		return result;
	}
	
	
	public String getQQuestionSetByHeadQuarter(Integer headQuarterId){
        System.out.println("call");
		System.out.println(" ******* getQQuestionSetByDistrictAndJobCategoryList**** headQuarterId: "+headQuarterId);
		
        WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		StringBuffer sb = new StringBuffer();
       // DistrictMaster districtMaster = null;
       // JobCategoryMaster jobCategoryMaster = null;
        HeadQuarterMaster headQuarterMaster = null;
        
        
        List<QqQuestionSets> districtSpecificQQList = null;
		
	 try{
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		if(headQuarterId!=null){
			System.out.println("0");
		//districtMaster = districtMasterDAO.findById(districtId, false, false);
			headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
			Criterion criterion1 = Restrictions.eq("headQuarterMaster", headQuarterMaster);
			districtSpecificQQList = qqQuestionSetsDAO.findByCriteria(criterion1);
	    }else{
	    	System.out.println("0");
	    	districtSpecificQQList = new ArrayList<QqQuestionSets>();
	    }
        
		/*if(jobCategoryId!=null){
			 jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
		}*/
		
		sb.append("<select id='qqAvlbList' name='qqAvlbList' class='span4'>");
		sb.append("<option value=''>Please Select QQ Set</option>");
		
		System.out.println(" districtSpecificQQList >>>>>>>>>>>>>>>>>>>>>>> "+districtSpecificQQList.size());
		
		
		if(districtSpecificQQList!=null)
	    {
			Integer selectOpt=0;
			
			/*try {
				if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSets()!=null){
					selectOpt= jobCategoryMaster.getQuestionSets().getID();
				}else{
					selectOpt= districtMaster.getQqQuestionSets().getID();
				}
			} catch (Exception e) {}*/
			
		  for(QqQuestionSets list: districtSpecificQQList)
	       {
			  if(list.getID().equals(selectOpt))
	    		  sb.append("<option selected value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
			  else
				  sb.append("<option value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
	       }
	     }
		sb.append("</select>");
		
	 }catch(Exception e){
        	 e.printStackTrace();
         }
		return sb.toString();
	}
	
	@Transactional(readOnly=false)
	public String getJobCategoryByHQAndBranch(Integer headQuarterId,Integer branchId,Integer subCateId){
		StringBuilder sb = new StringBuilder();
		List<JobCategoryMaster> jobCategoryMasterlst = new ArrayList<JobCategoryMaster>();
		JobCategoryMaster jobSubCategory = null;
		
		try{
			
			HeadQuarterMaster headQuarterMaster = null;
			BranchMaster branchMaster = null;
			
			if(headQuarterId!=null && headQuarterId>0)
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
			
			if(branchId!=null && branchId>0)
				branchMaster = branchMasterDAO.findById(branchId, false, false);
			
			/*************************************************/
			Criteria criteria = jobCategoryMasterDAO.getSession().createCriteria(jobCategoryMasterDAO.getPersistentClass());
			Criterion criHQ = null;
			Criterion crBR	= null;
			Criterion crStatus = Restrictions.eq("status", "A");
			
			if(headQuarterMaster!=null)
				criHQ = Restrictions.eq("headQuarterMaster", headQuarterMaster);
		
			/*if(branchMaster!=null)
				crBR = Restrictions.eq("branchMaster", branchMaster);
			else*/
			crBR = Restrictions.isNull("branchMaster");
				
			criteria.add(criHQ);
			criteria.add(crBR);
			criteria.add(crStatus);
			
			jobCategoryMasterlst = criteria.list();
			/**************************************************/
			
			
			
			//jobCategoryMasterlst = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(headQuarterMaster, branchMaster, null);
			
			if(subCateId!=null && subCateId>0)
				jobSubCategory = jobCategoryMasterDAO.findById(subCateId, false, false);

			sb.append("<option value='0'>Select Job Category</option>");
			if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
				for(JobCategoryMaster jb: jobCategoryMasterlst)
				{
					if(jb.getParentJobCategoryId()==null){
						if(jobSubCategory!=null && jobSubCategory.getParentJobCategoryId()!=null && jb.getJobCategoryId().equals(jobSubCategory.getParentJobCategoryId().getJobCategoryId()))
							sb.append("<option value='"+jb.getJobCategoryId()+"' selected >"+jb.getJobCategoryName()+"</option>");
						else
							sb.append("<option value='"+jb.getJobCategoryId()+"'>"+jb.getJobCategoryName()+"</option>");
					}
				}
			}

		}catch(Exception e){e.printStackTrace();}
		return sb.toString();
	}
	
	// @ Anurag
	
	public String getReferenceByHeadQuarterList(int headQuarterId){
		StringBuffer sb = new StringBuffer();
		HeadQuarterMaster headQuarterMaster							=	null;
		List<ReferenceQuestionSets> result  =   new ArrayList<ReferenceQuestionSets>();
	   System.out.println("*********  getReferenceByHeadQuarterList from Ajax  *************");
		 
		try 
		{
			if(headQuarterId!=0)
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
			if(headQuarterMaster!=null){
				result 		= 	referenceCheckQuestionSetDAO.getReferenceByHeadQuarter(headQuarterMaster);
			
			if(  result !=null && result.size()>0){
				 /*	
					jobCategoryMaster	=	jobCategoryMasterDAO.findById(jobCategoryId, false, false);
					refChkSetList 		= 	referenceCheckQuestionSetDAO.getReferenceByDistrictAndJobCategory(districtMaster, jobCategoryMaster);
					for(ReferenceCheckQuestionSet qs: refChkSetList)
					{
						ques.add(qs.getDistrictSpecificRefChkQuestions().getQuestionId());
					}
					refChkList 	= 	districtSpecificRefChkQuestionsDAO.getDistrictRefChkQuestionByDistrictAndJoCategory(districtMaster,ques);
				*/
	  
				sb.append("<option value=''>Please Select Question Set</option>");
			 	 
			for(ReferenceQuestionSets qset : result)
			{
				sb.append("<option value='"+qset.getID()+"'>"+qset.getQuestionSetText()+"</option>");
				
			}
			  sb.append("</select>");
			  
			}
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public boolean saveReferenceCheckQuestionSet(UserMaster userMaster, HeadQuarterMaster headQuarterMaster, JobCategoryMaster jobCategoryMaster ,String questionSetVal){
		try{
			
			System.out.println("jobCategoryMaster  =  "+jobCategoryMaster+"*****************saveReferenceCheckQuestionSet**********************"+questionSetVal);
			
 		   Date currentDate	=	new Date();
			if(questionSetVal!="" && !questionSetVal.equalsIgnoreCase("")){
				ReferenceQuestionSets referenceQuestionSets = referenceQuestionSetsDAO.findById(Integer.parseInt(questionSetVal), false, false);
				List<ReferenceCheckQuestionSet> referenceCheckQuestionSetList = null;
				referenceCheckQuestionSetList	=	referenceCheckQuestionSetDAO.findByQuestionAndJobCategoryByHQ(headQuarterMaster,jobCategoryMaster);

				ReferenceCheckQuestionSet referenceCheckQuestionSet =  new ReferenceCheckQuestionSet();
                 if(referenceCheckQuestionSetList.size() == 0 || referenceCheckQuestionSetList == null){
               	  // Insert Record
               	  try{
	                  		  referenceCheckQuestionSet.setReferenceQuestionSets(referenceQuestionSets);
	                  		  referenceCheckQuestionSet.setJobCategoryMaster(jobCategoryMaster);
	                          referenceCheckQuestionSet.setHeadQuarterId(headQuarterMaster.getHeadQuarterId());
	                          referenceCheckQuestionSet.setUserMaster(userMaster);
	                          referenceCheckQuestionSet.setCreatedDateTime(currentDate);
	                          referenceCheckQuestionSetDAO.makePersistent(referenceCheckQuestionSet);
                     } catch(Exception exception){
                         exception.printStackTrace();
                     }
                 } else {
               	  if(referenceCheckQuestionSetList!=null)
               		  try{
               			  referenceCheckQuestionSet = referenceCheckQuestionSetDAO.findById(referenceCheckQuestionSetList.get(0).getReferenceQuestionId(), false, false);
               			  referenceCheckQuestionSet.setReferenceQuestionSets(referenceQuestionSets);
               			  referenceCheckQuestionSetDAO.makePersistent(referenceCheckQuestionSet);
               		  } catch(Exception exception){
               			  exception.printStackTrace();
               		  }
                 	}
				}else{
					List<ReferenceCheckQuestionSet> referenceCheckQuestionSetList = null;
					referenceCheckQuestionSetList	=	referenceCheckQuestionSetDAO.findByQuestionAndJobCategoryByHQ(headQuarterMaster,jobCategoryMaster);

					 if(referenceCheckQuestionSetList.size() == 1 ){
		               	  
		               	  try{

			  				ReferenceCheckQuestionSet referenceCheckQuestionSet =  new ReferenceCheckQuestionSet();
			           	   referenceCheckQuestionSet = referenceCheckQuestionSetDAO.findById(referenceCheckQuestionSetList.get(0).getReferenceQuestionId(), false, false);
			       			  referenceCheckQuestionSet.setReferenceQuestionSets(null);
			       			  referenceCheckQuestionSetDAO.makePersistent(referenceCheckQuestionSet);
		                     } catch(Exception exception){exception.printStackTrace();		}
					 }
				}
		    
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	public String getPreHiresList(){
		System.out.println(" ******* getPreHiresList ****");
		
        WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		StringBuffer sb = new StringBuffer();
        
        List<SpAssessmentConfig> spAssessmentConfigList = null;
		
	 try{
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		spAssessmentConfigList = spAssessmentConfigDAO.findAll();
		
		sb.append("<select id='preHireList' name='preHireList' class='span4'>");
		sb.append("<option value=''> Select Smart Practice Compliance </option>");
		
		if(spAssessmentConfigList!=null)
	    {
			Integer selectOpt=0;
			
		  for(SpAssessmentConfig list: spAssessmentConfigList)
	       {
				  sb.append("<option value='"+list.getSpAssessmentConfigId()+"'>"+list.getAssessmentName()+"</option>");
	       }
	     }
		sb.append("</select>");
		
	 }catch(Exception e){
        	 e.printStackTrace();
         }
		return sb.toString();
	}
	
	
	// save job category history for log maintain TPL- 3112
public void saveToJobcategoryMasterHistory(JobCategoryMaster jobCategoryMaster, UserMaster userMaster , String ipAddress ,boolean isNew, String... params)
{
	JobCategoryMasterHistory jobCategoryMasterHistory = new JobCategoryMasterHistory();
	BeanUtils.copyProperties(jobCategoryMaster, jobCategoryMasterHistory);
	String questionSetVal = null;
	

	try{	
		if(userMaster!=null){ 
		if(params.length>0)
		{
			questionSetVal = params[0];
					if(params[0].equalsIgnoreCase("nochange")){
						try{ 
							List<ReferenceCheckQuestionSet> qnSets = new ArrayList<ReferenceCheckQuestionSet>();
							qnSets = referenceCheckQuestionSetDAO.findByJobcategoryMaster(jobCategoryMaster);
							if(qnSets.size()>0){
							questionSetVal = qnSets.get(0).getReferenceQuestionSets() !=null ?qnSets.get(0).getReferenceQuestionSets().getQuestionSetText():null;
							}
							else{
								questionSetVal = "";
							}
						
					}catch (Exception e) {}
				
					}
			 
			jobCategoryMasterHistory.setQuestionSetVal(questionSetVal);
		}
		jobCategoryMasterHistory.setUserMaster(userMaster); 
		jobCategoryMasterHistory.setUpdateTime(new Date());
		jobCategoryMasterHistory.setIpAddress(ipAddress);
		} 
		if(jobCategoryMaster.getJobCategoryId()==null || jobCategoryMaster.getJobCategoryId()==0){
			jobCategoryMasterHistory.setUpdateAction("add");
			jobCategoryMasterHistoryDAO.makePersistent(jobCategoryMasterHistory); 
		}
		else{
			
			// add current data from job category master to history 
			int totalJobCat = jobCategoryMasterHistoryDAO.totalJobCategoryHistoryByPrevId(jobCategoryMaster.getJobCategoryId());
			if(totalJobCat==0){
				JobCategoryMaster prevJobCat = jobCategoryMasterDAO.findById(jobCategoryMaster.getJobCategoryId(), false, false);
				JobCategoryMasterHistory jobCategoryMasterHistory2 =new JobCategoryMasterHistory();
				BeanUtils.copyProperties(prevJobCat, jobCategoryMasterHistory2);				
				jobCategoryMasterHistory2.setQuestionSetVal(questionSetVal);
				jobCategoryMasterHistory2.setUserMaster(userMaster);
				
				if(isNew)
					jobCategoryMasterHistory2.setUpdateAction("add");
				else
				jobCategoryMasterHistory2.setUpdateAction("preAdd");
				
				jobCategoryMasterHistory2.setUpdateTime(new Date());
				jobCategoryMasterHistory2.setIpAddress(ipAddress);
				jobCategoryMasterHistoryDAO.makePersistent(jobCategoryMasterHistory2); 
			}
			 	jobCategoryMasterHistory.setUpdateAction("update");
		}

		if(!isNew)
		jobCategoryMasterHistoryDAO.makePersistent(jobCategoryMasterHistory); 
		System.out.println(" save data in JobCategoryHistory table");
		}
	catch(Exception e)
	{
		System.out.println("Failure to save data in JobCategoryHistory table"+e);
		e.printStackTrace();
	}
	
}

public String getDataFromJobcategoryMasterHistory(Integer jobCategoryId){
	StringBuffer sb =	new StringBuffer();
	String responseText="";
	//  map of table column name ( as key )  and UI label ( as value)
	LinkedHashMap<String, String> columnVsUI  = getColumnVsUIMap();
	List <JobCategoryMasterHistory> data= jobCategoryMasterHistoryDAO.listJobCategoryHistoryByPrevId(jobCategoryId);
	List<String> noCheckLabels=new ArrayList<String>();
		noCheckLabels.add(StringUtils.capitalize("JobCategoryHistoryId"));
		noCheckLabels.add(StringUtils.capitalize("updateTime"));
		noCheckLabels.add(StringUtils.capitalize("updateAction"));
	
	sb.append("<table border='0' width='750' class='tablecgtbl'>");
	if(data!=null && data.size()>0){
	 sb.append("<tr><td>");
	 
	 sb.append("<table border='0' style='padding:0px;' cellpadding=0 cellspacing=0  width='100%' class='tablecgtbl'>");
			
	 	int counter=0;
		String year="";
		String preYear="";
		//List<String> fields=new ArrayList<String>();
		String fields="";
	 if(data!=null && data.size()>0){
		//for(JobCategoryMasterHistory jobCatHis : data){
			for(int i=0;i<data.size();i++){
				JobCategoryMasterHistory jobCatHis = data.get(i);
				// hide preAdd Flag object from UI ( preAdd - while old job category is updated first time )
				if(jobCatHis.getUpdateAction()!=null && jobCatHis.getUpdateAction().equalsIgnoreCase("preAdd")) continue;
	 
				if(data.size()>i+1){
					try {
						Map<String,Object> first =ConvertObjectToMap(jobCatHis);
						Map<String,Object> second =ConvertObjectToMap(data.get(i+1));
						fields= getDiffValueKeys(first, second ,noCheckLabels , columnVsUI);
						if(fields == null) continue;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					if(jobCatHis.getUpdateAction().equalsIgnoreCase("add"))
					fields="<span style='color:green'>New job category created.</style>";
					else
						fields="N/A";						
					}
			 
		
		year = convertUSformatOnlyYear(jobCatHis.getUpdateTime());
		counter++;
		if(!preYear.equals(year)){
		sb.append("<tr>");
		sb.append("<td colspan=3 style='text-align:right;padding-right:8px;padding-bottom:5px;verical-align:bottom;'><span class=\"txtyearbg\">"+year+"</span></td><td>&nbsp;</td>");
		sb.append("<tr>");
		sb.append("<tr>");
		sb.append("<td width='15%' style='height:10px;'>&nbsp;</td>");
		sb.append("<td style='text-align:right;vertical-align:middle;padding-left:12px;'  height=10>");
		sb.append("<span class=\"vertical-line\"></span></td><td ></td>");
		sb.append("</tr>");
		}
		preYear=year;
		
		sb.append("<tr>");
		
		
				//sb.append("<td width='15%' style='text-align:right;vertical-align:middle;position:relative;'>"+convertUSformatOnlyMonthDate(commDetail.getCreatedDateTime()));
				sb.append("<td width='25%' style='text-align:right;vertical-align:middle;position:relative;'>"+Utility.convertDateAndTimeFormatForCommunication(jobCatHis.getUpdateTime()));
					if(jobCatHis.getUpdateAction().equalsIgnoreCase("add"))
						sb.append("<span class='note-com'><img src=\"images/add_circle.png\" style=\"width:20px;    margin-left: 3px; height:auto;\"></span>");
					else
					sb.append("<span class='note-com'><img src=\"images/edit_circle.png\" style=\"width:20px;    margin-left: 3px; height:auto;\"></span>");
					
				sb.append("</td><td style='text-align:right;vertical-align:middle;padding-left:12px;' width='8px;' height='13'>");
					sb.append("<span class=\"vertical-line\"></span></td>");
					sb.append("<td  style='text-align:right;vertical-align:middle;' width=20>");
				sb.append("<hr style=\"color: #f00; background:#007AB4; width: 100%; height: 3px;\"/></td>");
				sb.append("<td class='commbgcolor'  style='padding:5px;vertical-align:middle;'>");
				sb.append("<span class='com-user' style='line-height:14px;'>Action: "+(jobCatHis.getUpdateAction().equalsIgnoreCase("add")==true?"Create":"Update") +"</span><br/>");
				sb.append("<span class='com-user' style='line-height:14px;'>User: "+jobCatHis.getUserMaster().getFirstName()+" "+jobCatHis.getUserMaster().getLastName()+" ( "+jobCatHis.getUserMaster().getEmailAddress()+" )</span><br/>");
				sb.append("<span class='com-user' style='line-height:14px;'>Fields that were updated: "+fields.toString()+"</span>");
				sb.append("</tr>"); 
				sb.append("<tr>");
				sb.append("<td width='15%' style='height:20px;'>&nbsp;</td>");
				sb.append("<td style='text-align:right;vertical-align:middle;padding-left:12px;'  height=20>");
				sb.append("<span class=\"vertical-line\"></span></td><td ></td>");
				sb.append("</tr>");
				 
		//}
	 }
			if(counter==0){
					sb.append("<tr><td>"+lblNoRecord+"</td></tr>");
				}
	 }
			sb.append("</table></td></tr>");
	}else{
		sb.append("<tr><td>"+lblNoRecord+"</td></tr>");
		}
		sb.append("</table>");

	
	
	return sb.toString();
	
}

public static Map<String, Object> ConvertObjectToMap(Object obj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    Method[] methods = obj.getClass().getMethods();


    Map<String, Object> map = new LinkedHashMap<String, Object>();
    for (Method m : methods) {
       if (m.getName().startsWith("get") && !m.getName().startsWith("getClass")) {
          Object value = (Object) m.invoke(obj);
          map.put(m.getName().substring(3), (Object) value);   // here key is in 'capitalize' format means first letter is Capital.... 
       }
    }
return map;
}

public String getDiffValueKeys(Map<String,Object> first ,Map<String,Object> second ,List noCheckLabels , LinkedHashMap<String, String> columnVsUI){
	StringBuffer result =new StringBuffer();
	result.append("<table class='table table-hover top2' style='margin-bottom: 10px !important;'>");
	result.append("<thead style='background-color:#007AB4;'><tr><th>#</th><th>Field Name</th><th>Previous Value</th><th>Current Value</th></tr></thead>");
	int counter=0;
	String columnVsUIKey="";
	try{
		if(first!=null && second!=null){
			for(String key : columnVsUI.keySet()){
				columnVsUIKey = key;
				key = StringUtils.capitalize(key);        //  make key of columnVsUI map in Capitalize format.
				if(key.equalsIgnoreCase("jobCategoryHistoryId")){System.out.println("********************    equal  no check label *******************");};
				
				if(key.equalsIgnoreCase("updateTime")){
					result.append("<tr><th scope='' style='min-width:20px; color:#007AB4;'>"+(counter+1)+"</th><td>"+columnVsUI.get(columnVsUIKey)+"</td><td>"+getObjValue(second.get(key))+"</td><td>"+getObjValue(first.get(key))+"</td></tr>");
					counter++;
				}
				
				if(noCheckLabels.contains(key)) {continue;}  // for no check label
				if(first.get(key)==null || second.get(key)==null){
					if(!(second.get(key)==null && first.get(key)==null )){
						result.append("<tr><th scope='' style='min-width:20px; color:#007AB4;'>"+(counter+1)+"</th><td>"+columnVsUI.get(columnVsUIKey)+"</td><td>"+getObjValue(second.get(key))+"</td><td>"+getObjValue(first.get(key))+"</td></tr>");
						counter++;
						}
				}else{
					if(first.get(key).hashCode()!=second.get(key).hashCode()){
						result.append("<tr><th scope='' style='min-width:20px; color:#007AB4;'>"+(counter+1)+"</th><td>"+columnVsUI.get(columnVsUIKey)+"</td><td>"+getObjValue(second.get(key))+"</td><td>"+getObjValue(first.get(key))+"</td></tr>");
						counter++;
					}  
					}
			} 
		}
		 
		result.append("</table>");
	}
	catch (Exception e) {
		System.out.println("*****************************   Error while comparing two map for key******************************");
		e.printStackTrace();
	}
	if(counter<=1) result= null;
	return result !=null ? result.toString():null;
	
}

public String getObjValue(Object obj){
	String result ="";
 
	if(obj!=null){
	if(obj.getClass()== java.lang.Boolean.class){
		if(obj.equals(Boolean.TRUE)) result = "Required";
		else   result = "Not required";
	}
	else {
		result+=obj;
	}
	 
	} 
	
	return result;

}

public LinkedHashMap<String, String> getColumnVsUIMap(){
	LinkedHashMap<String, String> map  =  new LinkedHashMap<String, String>();
	map.put("updateTime", "Update Time");
	map.put("jobCategoryName", "Job Category Name");
	map.put("parentJobCategoryId", "Parent Job Category");
	map.put("branchMaster", "Branch Name");
	map.put("headQuarterMaster", "Headquarter Name");
	map.put("districtMaster", "District Name");
	map.put("questionSets", "Qualification Question Set Id");
	map.put("jobInviteOnly", "Job category is invite-only");
	map.put("offerDSPQ", "Offer DSPQ");
	map.put("hiddenJob", "Hidden Job");
	map.put("preHireSmartPractices", Utility.getLocaleValuePropByKey("lblSPStatus", locale));
	map.put("qualificationQuestion", "Qualification Question Set");
	map.put("baseStatus", "EPI Required (external candidate(s))");
	map.put("epiForFullTimeTeachers", "EPI Required (internal candidate(s))");
	map.put("offerDistrictSpecificItems", "Offer District Specific Mandatory Items (internal candidate(s))");
	map.put("offerQualificationItems", "Offer Qualification Items (internal candidate(s))");
	map.put("offerJSI", "Offer JSI (internal candidate(s))");
	map.put("offerPortfolioNeeded", "Offer Portfolio");
	map.put("assessmentDocument", "Attach JSI");
	map.put("status", "Status");
	map.put("offerVVIForInternalCandidates", "Offer Virtual Video Interview (Internal Movement Candidate(s))");
	map.put("offerVirtualVideoInterview", "Offer Virtual Video Interview");
	map.put("epiForIMCandidates", "EPI Required (Internal Movement Candidate(s))");
	map.put("portfolioForIMCandidates", "Offer Portfolio (Internal Movement Candidate(s))");
	map.put("districtSpecificItemsForIMCandidates", "Offer District Specific Mandatory Items (Internal Movement Candidate(s))");
	map.put("qualificationItemsForIMCandidates", "Offer Qualification Items	(Internal Movement Candidate(s))");
	map.put("jsiForIMCandidates", "Offer JSI (Internal Movement Candidate(s))");
	map.put("i4QuestionSets", "Virtual Video Interview Id");
	map.put("maxScoreForVVI", "Max Marks for VVI");
	map.put("sendAutoVVILink", "Auto Send VVI Link");
	map.put("timeAllowedPerQuestion", "Time Allowed Per Question");
	map.put("VVIExpiresInDays", "Virtual Video interview Expires In Days");
	map.put("minDaysJobWillDisplay", "Minimum Days of job display");
	map.put("offerAssessmentInviteOnly", "Offer Assessment to candidates on invite only");
	map.put("districtAssessmentId", "District Assesment Id");
	map.put("schoolSelection", "School Selection");
	map.put("attachDSPQFromJC", "District Specific Mandatory Items from parent Job Category");
	map.put("attachSLCFromJC", "Status Life Cycle from parent Job Category");
	map.put("questionSetsForOnboarding", "Qualification Question Set to Job Category For Onboarding");
	map.put("approvalBeforeGoLive", "Approval process for positions prior to them going live");
	map.put("noOfApprovalNeeded", "Number of Approvals Needed");
	map.put("buildApprovalGroup", "Build Approval Groups ");
	map.put("questionSetVal", "e-Reference Question Set Id");
	map.put("userMaster", "User");
	map.put("updateAction", "Update Action");
	map.put("ipAddress", "IpAddress");
	map.put("spAssessmentId", "Pre-Hire Smart Practices Assesment Id");
	
	return map;
}

	public String getEpiGroups()
	{
		System.out.println("::::::::::::::::: HqJobCategoryAjax : getEpiGroups :::::::::::::::::");
		StringBuffer sb	= new StringBuffer();
		try{
			List<AssessmentGroupDetails> assessmentGroupDetailList = null;
			assessmentGroupDetailList = assessmentGroupDetailsDAO.getAllAssessmentGroupDetailsList(1, 1);
			sb.append("<option value='0'>"+optStrEpiGroups+"</option>");
			if(assessmentGroupDetailList!=null && assessmentGroupDetailList.size()>0){
				for(AssessmentGroupDetails assessmentGroupDetails : assessmentGroupDetailList)
				{
					if(assessmentGroupDetails!=null && assessmentGroupDetails.getAssessmentGroupName()!=null && assessmentGroupDetails.getAssessmentGroupName().equalsIgnoreCase("Standard EPI"))
						sb.append("<option value='"+assessmentGroupDetails.getAssessmentGroupId()+"' selected>"+assessmentGroupDetails.getAssessmentGroupName()+"</option>");
					else
						sb.append("<option value='"+assessmentGroupDetails.getAssessmentGroupId()+"'>"+assessmentGroupDetails.getAssessmentGroupName()+"</option>");
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
}
