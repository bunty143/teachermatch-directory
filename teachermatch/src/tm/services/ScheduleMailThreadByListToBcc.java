package tm.services;

import java.util.List;

import tm.services.district.PrintOnConsole;


public class ScheduleMailThreadByListToBcc extends Thread{

	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) {
		this.emailerService = emailerService;
	}
	
	private List<String> lstMailTo;
	private List<String> lstMailBcc;
	private List<String> lstMailcc;
	private String mailSubject;
	private String mailContent;

	public void setLstMailTo(List<String> lstMailTo) {
		this.lstMailTo = lstMailTo;
	}

	public void setLstMailBcc(List<String> lstMailBcc) {
		this.lstMailBcc = lstMailBcc;
	}

	public void setLstMailcc(List<String> lstMailcc) {
		this.lstMailcc = lstMailcc;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}

	public ScheduleMailThreadByListToBcc() {
		super();
	}
	
	public void run()
	{
		try{
			emailerService.sendMailAsHTMLByListToBcc(lstMailTo,lstMailBcc,mailSubject,mailContent);
			PrintOnConsole.debugPrintln("ScheduleMailThreadByListToBcc", "Try to Send Email List To "+lstMailTo.size() +" Bcc List Size "+lstMailBcc.size());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
