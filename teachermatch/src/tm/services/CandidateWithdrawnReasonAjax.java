package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.api.PaginationAndSortingAPI;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherProfileVisitHistory;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherProfileVisitHistoryDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.services.quartz.FTPConnectAndLogin;
import tm.utility.DistrictNameCompratorASC;
import tm.utility.DistrictNameCompratorDESC;
import tm.utility.Utility;

import ch.qos.logback.core.status.StatusManager;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class CandidateWithdrawnReasonAjax {
	String locale = Utility.getValueOfPropByKey("locale");
	
	private String lblDistrictName = Utility.getLocaleValuePropByKey("lblDistrictName", locale);
	private String lblPlanning = Utility.getLocaleValuePropByKey("lblPlanning", locale);
	private String lbllerningEnvrmnt = Utility.getLocaleValuePropByKey("lbllerningEnvrmnt", locale);
	private String lblinstruct = Utility.getLocaleValuePropByKey("lblinstruct",locale);
	private String lblanalyzeandAdjust = Utility.getLocaleValuePropByKey("lblanalyzeandAdjust", locale);
	private String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord",locale);
	private String lblCandidateFirstName = Utility.getLocaleValuePropByKey("lblCandidateFirstName", locale);
	private String lblCandidateLastName = Utility.getLocaleValuePropByKey("lblCandidateLastName", locale);
	private String lblCandidateEmailAddress = Utility.getLocaleValuePropByKey("lblCandidateEmailAddress", locale);
	private String jobtitle = Utility.getLocaleValuePropByKey("lblJoTil", locale);
	private String lblWithdrawnBy = Utility.getLocaleValuePropByKey("lblWithdrawnBy", locale);
	private String lblWithdrawTime = Utility.getLocaleValuePropByKey("lblWithdrawTime", locale);
	private String lblWithdrawnReason = Utility.getLocaleValuePropByKey("lblWithdrawnReason", locale);
	private String lblNOfWithdraws = Utility.getLocaleValuePropByKey("lblNOfWithdraws", locale);
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;

	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;

	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;

	public void setJobCategoryMasterDAO(
			JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;

	public void setTeacherPersonalInfoDAO(
			TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}

	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;

	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,
			String SchoolName) {
		/* ======== For Session time Out Error ========= */
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if (session == null || (session.getAttribute("teacherDetail") == null && session.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<DistrictSchools> schoolMasterList = new ArrayList<DistrictSchools>();
		List<DistrictSchools> fieldOfSchoolList1 = null;
		List<DistrictSchools> fieldOfSchoolList2 = null;
		try {
			Criterion criterion = Restrictions.like("schoolName", SchoolName,
					MatchMode.START);
			if (SchoolName.length() > 0) {
				if (districtIdForSchool == 0) {
					if (SchoolName.trim() != null
							&& SchoolName.trim().length() > 0) {
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(
								null, 0, 10, criterion);
						Criterion criterion2 = Restrictions.ilike("schoolName",
								"% " + SchoolName.trim() + "%");
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(
								null, 0, 10, criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(
								schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(
								new LinkedHashSet<DistrictSchools>(setSchool));
					} else {
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				} else {
					DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);

					Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);

					if (SchoolName.trim() != null && SchoolName.trim().length() > 0) {
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
						Criterion criterion3 = Restrictions.ilike("schoolName","% " + SchoolName.trim() + "%");
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion3,criterion2);

						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					} else {
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return schoolMasterList;

	}

	public String displayRecordsByEntityType(String noOfRow, String pageNo,String sortOrder, String sortOrderType, String districtID,String jobTitle,String WithdrawFromId, String WithdrawToId) {
		System.out.println(":::::::::::::::::::::::::::::::::::::::in the displayRecordsByEntityType   method::::::::::::::::");
		System.out.println(noOfRow + "\t" + pageNo + "\t" + sortOrder + "\t"
				+ sortOrderType);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if (session == null || (session.getAttribute("teacherDetail") == null && session.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords = new StringBuffer();
		int sortingcheck = 1;
		try {

			UserMaster userMaster = null;
			Integer entityID = null;
			DistrictMaster districtMaster = null;
			SchoolMaster schoolMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			} else {
				userMaster = (UserMaster) session.getAttribute("userMaster");

				if (userMaster.getSchoolId() != null)
					schoolMaster = userMaster.getSchoolId();

				if (userMaster.getDistrictId() != null) {
					districtMaster = userMaster.getDistrictId();
				}

				entityID = userMaster.getEntityType();
			}

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo - 1) * noOfRowInPage);
			int end = ((pgNo - 1) * noOfRowInPage) + noOfRowInPage;
			int totalRecord = 0;

			// ------------------------------------
			/** set default sorting fieldName **/
			String sortOrderStrVal = null;
			String sortOrderFieldName = "districtname";

			if (!sortOrder.equals("") && !sortOrder.equals(null)) {
				sortOrderFieldName = sortOrder;
			}

			String sortOrderTypeVal = "0";
			if (!sortOrderType.equals("") && !sortOrderType.equals(null)) {
				if (sortOrderType.equals("0")) {
					sortOrderStrVal = "asc";// Order.asc(sortOrderFieldName);
					
				} else {
					sortOrderTypeVal = "1";
					sortOrderStrVal = "desc";// Order.desc(sortOrderFieldName);
				}
			} else {
				sortOrderTypeVal = "0";
				sortOrderStrVal = "asc";// Order.asc(sortOrderFieldName);
			}

			// @@@@@@@@@@@@@@@@@@@@@ end @@@@@@@@@@@@@@@@@@@@@@@@@
			List<String[]> lstCandidateWithdrawnReason = new ArrayList<String[]>();
			// int disID=Integer.parseInt(districtID);
			int disID = 0;
			String districtname="";

			if (entityID == 2 || entityID == 3) {
				disID = districtMaster.getDistrictId();
				districtname=districtMaster.getDistrictName();
				lstCandidateWithdrawnReason = teacherStatusHistoryForJobDAO.getCandidateWithdrawnReason(sortingcheck, sortOrderStrVal,start, noOfRowInPage, true, sortOrderFieldName,disID,jobTitle,WithdrawFromId, WithdrawToId);
				totalRecord = lstCandidateWithdrawnReason.size();
				System.out.println("totallllllllllll=========" + totalRecord);

			} else if (entityID == 1) {

				if (districtID != null && !districtID.equals(""))
					disID = Integer.parseInt(districtID);
				else
					disID = 0;

				lstCandidateWithdrawnReason = teacherStatusHistoryForJobDAO.getCandidateWithdrawnReason(sortingcheck, sortOrderStrVal,start, noOfRowInPage, true, sortOrderFieldName,disID, jobTitle,WithdrawFromId, WithdrawToId);

				if(lstCandidateWithdrawnReason!=null)
				totalRecord = lstCandidateWithdrawnReason.size();
				System.out.println("totallllllllllll=========" + totalRecord);

			}

			List<String[]> finallstNobleCandidate = new ArrayList<String[]>();
			if (totalRecord < end)
				end = totalRecord;
			if(lstCandidateWithdrawnReason!=null)
				finallstNobleCandidate = lstCandidateWithdrawnReason.subList(start, end);

			String responseText = "";

			tmRecords.append("<table  id='tblCndidtWithnReasn' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			if(entityID==1){
				responseText = PaginationAndSorting.responseSortingLink(lblDistrictName, sortOrderFieldName, "districtname",sortOrderTypeVal, pgNo);
				tmRecords.append("<th valign='top'>" + responseText + "</th>");
			}
			
			responseText = PaginationAndSorting.responseSortingLink(jobtitle, sortOrderFieldName, "jobtitle",sortOrderTypeVal, pgNo);
			tmRecords.append("<th  valign='top'>" + responseText + "</th>");

			responseText = PaginationAndSorting.responseSortingLink(lblWithdrawnBy, sortOrderFieldName, "WithdrawnBy",sortOrderTypeVal, pgNo);
			tmRecords.append("<th valign='top'>" + responseText + "</th>");
			
			responseText = PaginationAndSorting.responseSortingLink(lblWithdrawTime, sortOrderFieldName, "WithdrawTime",sortOrderTypeVal, pgNo);
			tmRecords.append("<th valign='top'>" + responseText + "</th>");

			responseText = PaginationAndSorting.responseSortingLink(lblWithdrawnReason, sortOrderFieldName, "WithdrawnReason",sortOrderTypeVal, pgNo);
			tmRecords.append("<th valign='top'>" + responseText + "</th>");
			
			responseText = PaginationAndSorting.responseSortingLink(lblNOfWithdraws, sortOrderFieldName, "N_Of_Withdraws",sortOrderTypeVal, pgNo);
			tmRecords.append("<th valign='top'>" + responseText + "</th>");
			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			if (finallstNobleCandidate.size() == 0) {
				tmRecords.append("<tr><td colspan='9' align='center' class='net-widget-content'>"+ Utility.getLocaleValuePropByKey("lblNoRecord", locale) + "</td></tr>");
			}

			String sta = "";
			if (finallstNobleCandidate.size() > 0) {
				for (Iterator it = finallstNobleCandidate.iterator(); it
						.hasNext();) {
					String[] row = (String[]) it.next();

					String myVal1 = "";
					String myVal2 = "";

					tmRecords.append("<tr>");
					if(entityID==1){
						myVal1 = (row[0] == null) ? "N/A" : row[0].toString();
						tmRecords.append("<td>" + myVal1 + "</td>");
					}
					myVal1 = (row[1] == null) ? "N/A" : row[1].toString();
					tmRecords.append("<td>" + myVal1 + "</td>");

					myVal1 = ((row[2] == null) || (row[2].toString().length() == 0)) ? "N/A" : row[2].	toString();
					tmRecords.append("<td>" + myVal1 + "</td>");

					myVal1 = (row[3] == null) ? "N/A" : row[3].toString();
					tmRecords.append("<td>" + myVal1 + "</td>");

					myVal1 = (row[4] == null) ? "N/A" : row[4].toString();
					tmRecords.append("<td>" + myVal1 + "</td>");

					myVal1 = (row[5] == null) ? "N/A" : row[5].toString();
					tmRecords.append("<td>" + myVal1 + "</td>");

					tmRecords.append("</tr>");
				}
			}

			tmRecords.append("</table>");

			System.out.println();
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request, totalRecord,noOfRow, pageNo));

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();
		return tmRecords.toString();
	}

public String displayRecordsByEntityTypeEXL(String noOfRow, String pageNo,
		String sortOrder, String sortOrderType, String districtID,
		String jobTitle,String WithdrawFromDate, String WithdrawToId) {

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if (session == null || (session.getAttribute("teacherDetail") == null && session.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		int sortingcheck = 1;
		try {

			UserMaster userMaster = null;
			Integer entityID = null;
			DistrictMaster districtMaster = null;
			SchoolMaster schoolMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			} else {
				userMaster = (UserMaster) session.getAttribute("userMaster");

				if (userMaster.getSchoolId() != null)
					schoolMaster = userMaster.getSchoolId();

				if (userMaster.getDistrictId() != null) {
					districtMaster = userMaster.getDistrictId();
				}

				entityID = userMaster.getEntityType();
			}

			// @@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo - 1) * noOfRowInPage);
			int end = ((pgNo - 1) * noOfRowInPage) + noOfRowInPage;
			int totalRecord = 0;

			// ** set default sorting fieldName **//*
			String sortOrderStrVal = null;
			String sortOrderFieldName = "districtname";

			if (!sortOrder.equals("") && !sortOrder.equals(null)) {
				sortOrderFieldName = sortOrder;
			}

			String sortOrderTypeVal = "0";
			if (!sortOrderType.equals("") && !sortOrderType.equals(null)) {
				if (sortOrderType.equals("0")) {
					sortOrderStrVal = "asc";// Order.asc(sortOrderFieldName);
				} else {
					sortOrderTypeVal = "1";
					sortOrderStrVal = "desc";// Order.desc(sortOrderFieldName);
				}
			} else {
				sortOrderTypeVal = "0";
				sortOrderStrVal = "asc";// Order.asc(sortOrderFieldName);
			}

			// @@@@@@@@@@@@@@@@@@@@@ end @@@@@@@@@@@@@@@@@@@@@@@@@
			List<String[]> lstNobleCandidate = new ArrayList<String[]>();

			int disID = 0;
			String districtname="";
			int teacherid =0;
			System.out.println("in the EXL method");
			if (entityID == 2 || entityID == 3) {
				disID = districtMaster.getDistrictId();
				districtname=districtMaster.getDistrictName();
				lstNobleCandidate = teacherStatusHistoryForJobDAO.getCandidateWithdrawnReason(sortingcheck, sortOrderStrVal,start, noOfRowInPage, true, sortOrderFieldName,disID, jobTitle,WithdrawFromDate, WithdrawToId);
				totalRecord = lstNobleCandidate.size();
				System.out.println("totallllllllllll=========" + totalRecord);

			} else if (entityID == 1) {
				if (districtID != null && !districtID.equals(""))
					disID = Integer.parseInt(districtID);
				else
					disID = 0;
				lstNobleCandidate = teacherStatusHistoryForJobDAO.getCandidateWithdrawnReason(sortingcheck, sortOrderStrVal,start, noOfRowInPage, true, sortOrderFieldName, disID, jobTitle,WithdrawFromDate, WithdrawToId);
				totalRecord = lstNobleCandidate.size();
				System.out.println("totallllllllllll=========" + totalRecord);

			}

			List<String[]> finallstNobleCandidate = new ArrayList<String[]>();
			finallstNobleCandidate = lstNobleCandidate;

			// Excel Exporting

			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			// String basePath = request.getRealPath("/")+"/candidate";
			String basePath = request.getSession().getServletContext().getRealPath("/")+ "/candidateWithdrawnReason";
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+ basePath);
			fileName = "candidateWithdrawnReason" + time + ".xls";

			File file = new File(basePath);
			if (!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);

			file = new File(basePath + "/" + fileName);

			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));

			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;

			WritableWorkbook workbook = Workbook.createWorkbook(file,wbSettings);
			workbook.createSheet("pdfReportForNCGrnlApplication", 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);

			// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			header.setBackground(Colour.GRAY_25);

			// Write a few headers
			excelSheet.mergeCells(0, 0, 6, 1);
			Label label;
			label = new Label(0, 0, "Candidate Withdrawn Reason", timesBoldUnderline);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 3, 6, 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			excelSheet.getSettings().setDefaultColumnWidth(18);

			int k = 4;
			int col = 1;
			int headercounter=-1;
			int datacounter=-1;
			if(entityID==1){
			label = new Label(++headercounter, k, lblDistrictName, header);
			excelSheet.addCell(label);
			}
			// label = new Label(1, k,lblJoTil,header);
			// excelSheet.addCell(label);

			label = new Label(++headercounter, k, jobtitle, header);
			excelSheet.addCell(label);
			label = new Label(++headercounter, k, lblWithdrawnBy, header);
			excelSheet.addCell(label);
			label = new Label(++headercounter, k, lblWithdrawTime, header);
			excelSheet.addCell(label);

			label = new Label(++headercounter, k, lblWithdrawnReason, header);
			excelSheet.addCell(label);

			label = new Label(++headercounter, k, lblNOfWithdraws, header);
			excelSheet.addCell(label);
			k = k + 1;

			
			if (finallstNobleCandidate.size() == 0) {
				excelSheet.mergeCells(0, k, 6, k);
				label = new Label(0, k, lblNoRecord);
				excelSheet.addCell(label);
			}
			String sta = "";
			if (finallstNobleCandidate.size() > 0) {
				for (Iterator it = finallstNobleCandidate.iterator(); it.hasNext();) {
					String[] row = (String[]) it.next();
					datacounter = -1;
					if(entityID==1){
						label = new Label(++datacounter, k, row[0].toString());
						excelSheet.addCell(label);
					}

					label = new Label(++datacounter, k, row[1].toString());
					excelSheet.addCell(label);

					String myVal111 = (row[2] == null) ? "N/A" : row[2]
							.toString();
					label = new Label(++datacounter, k, myVal111);
					excelSheet.addCell(label);

					String myVal1 = (row[3] == null) ? "N/A" : row[3]
							.toString();
					label = new Label(++datacounter, k, myVal1);
					excelSheet.addCell(label);

					String myVal2 = ((row[4] == null) || (row[4].toString().length() == 0)) ? "N/A" : row[4].toString();
					label = new Label(++datacounter, k, myVal2);
					excelSheet.addCell(label);

					String myVal3 = ((row[5] == null) || (row[5].toString().length() == 0)) ? "N/A" : row[5].toString();
					label = new Label(++datacounter, k, myVal3);
					excelSheet.addCell(label);
					
					k++;
				}
			}

			workbook.write();
			workbook.close();
		} catch (Exception e) {
		}

		return fileName;
	}

/////////////////////////////////shriram
public String displayRecordsByEntityTypePDF(String noOfRow, String pageNo,String sortOrder, String sortOrderType, String districtID,String jobTitle,String WithdrawFromId, String WithdrawToId) {
	System.out.println("in the pdf method");
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if (session == null || (session.getAttribute("teacherDetail") == null && session.getAttribute("userMaster") == null)) {
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	try {
		String fontPath = request.getRealPath("/");
		BaseFont.createFont(fontPath + "fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		BaseFont tahoma = BaseFont.createFont(fontPath + "fonts/tahoma.ttf", BaseFont.CP1252,BaseFont.NOT_EMBEDDED);

		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			// return "false";
		} else
			userMaster = (UserMaster) session.getAttribute("userMaster");

		int userId = userMaster.getUserId();

		String time = String.valueOf(System.currentTimeMillis()).substring(6);
		String basePath = context.getServletContext().getRealPath("/") + "/user/" + userId + "/";
		
		System.out.println(basePath);
		String fileName = time + "Report.pdf";

		File file = new File(basePath);
		if (!file.exists())
			file.mkdirs();

		Utility.deleteAllFileFromDir(basePath);
		System.out.println("user/" + userId + "/" + fileName);

		generateCandidatePDReport(noOfRow, pageNo, sortOrder,sortOrderType, basePath + "/" + fileName, context.getServletContext().getRealPath("/"), districtID,jobTitle,WithdrawFromId, WithdrawToId);
		return "user/" + userId + "/" + fileName;
	} catch (Exception e) {
		e.printStackTrace();
	}
	return "ABV";
}

// shriram
public boolean generateCandidatePDReport(String noOfRow, String pageNo,String sortOrder, String sortOrderType, String path,String realPath, String districtID,String jobTitle, String WithdrawFromdate, String WithdrawTodate) {
	int cellSize = 0;
	Document document = null;
	FileOutputStream fos = null;
	PdfWriter writer = null;
	Paragraph footerpara = null;
	HeaderFooter headerFooter = null;
	Font font8 = null;
	Font font8Green = null;
	Font font8bold = null;
	Font font9 = null;
	Font font9bold = null;
	Font font10 = null;
	Font font10_10 = null;
	Font font10bold = null;
	Font font11 = null;
	Font font11bold = null;
	Font font20bold = null;
	Font font11b = null;
	Color bluecolor = null;
	Font font8bold_new = null;

	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if (session == null || (session.getAttribute("teacherDetail") == null && session.getAttribute("userMaster") == null)) {
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	int sortingcheck = 1;
	try {

		UserMaster userMaster = null;
		Integer entityID = null;
		DistrictMaster districtMaster = null;
		SchoolMaster schoolMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			// return "false";
		} else {
			userMaster = (UserMaster) session.getAttribute("userMaster");

			if (userMaster.getSchoolId() != null)
				schoolMaster = userMaster.getSchoolId();

			if (userMaster.getDistrictId() != null) {
				districtMaster = userMaster.getDistrictId();
			}

			entityID = userMaster.getEntityType();
		}

		// @@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@

		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start = ((pgNo - 1) * noOfRowInPage);
		int end = ((pgNo - 1) * noOfRowInPage) + noOfRowInPage;
		int totalRecord = 0;
		// *//** set default sorting fieldName **//*
		String sortOrderStrVal = null;
		String sortOrderFieldName = "districtname";

		if (!sortOrder.equals("") && !sortOrder.equals(null)) {
			sortOrderFieldName = sortOrder;
		}

		String sortOrderTypeVal = "0";
		if (!sortOrderType.equals("") && !sortOrderType.equals(null)) {
			if (sortOrderType.equals("0")) {
				sortOrderStrVal = "asc";// Order.asc(sortOrderFieldName);
			} else {
				sortOrderTypeVal = "1";
				sortOrderStrVal = "desc";// Order.desc(sortOrderFieldName);
			}
		} else {
			sortOrderTypeVal = "0";
			sortOrderStrVal = "asc";// Order.asc(sortOrderFieldName);
		}

		// @@@@@@@@@@@@@@@@@@@@@ end @@@@@@@@@@@@@@@@@@@@@@@@@
		List<String[]> lstNobleCandidate = new ArrayList<String[]>();

		int disID = 0;
		String districtname="";
		// int jobCatID=Integer.parseInt(jobcategoryID);
		
		if (entityID == 2 || entityID == 3) {
			disID = districtMaster.getDistrictId();
			districtname=districtMaster.getDistrictName();
			lstNobleCandidate = teacherStatusHistoryForJobDAO.getCandidateWithdrawnReason(sortingcheck, sortOrderStrVal,start, noOfRowInPage, true, sortOrderFieldName,disID, jobTitle,WithdrawFromdate, WithdrawTodate);
			totalRecord = lstNobleCandidate.size();
			System.out.println("totallllllllllll=========" + totalRecord);

		} else if (entityID == 1) {
			if (districtID != null && !districtID.equals(""))
				disID = Integer.parseInt(districtID);
			else
				disID = 0;

			lstNobleCandidate = teacherStatusHistoryForJobDAO.getCandidateWithdrawnReason(sortingcheck, sortOrderStrVal,start, noOfRowInPage, true, sortOrderFieldName,disID, jobTitle,WithdrawFromdate, WithdrawTodate);
			totalRecord = lstNobleCandidate.size();
			System.out.println("totallllllllllll=========" + totalRecord);

		}

		List<String[]> finallstNobleCandidate = new ArrayList<String[]>();
		if (totalRecord < end)
			end = totalRecord;
		finallstNobleCandidate = lstNobleCandidate;

		String fontPath = realPath;
		try {

			BaseFont.createFont(fontPath + "fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath + "fonts/4637.ttf", BaseFont.CP1252,BaseFont.NOT_EMBEDDED);
			font8 = new Font(tahoma, 8);

			font8Green = new Font(tahoma, 8);
			font8Green.setColor(Color.BLUE);
			font8bold = new Font(tahoma, 8, Font.NORMAL);

			font9 = new Font(tahoma, 9);
			font9bold = new Font(tahoma, 9, Font.BOLD);
			font10 = new Font(tahoma, 10);

			font10_10 = new Font(tahoma, 10);
			font10_10.setColor(Color.white);
			font10bold = new Font(tahoma, 10, Font.BOLD);
			font11 = new Font(tahoma, 11);

			font11bold = new Font(tahoma, 11, Font.BOLD);
			bluecolor = new Color(0, 122, 180);

			// Color bluecolor = new Color(0,122,180);

			font20bold = new Font(tahoma, 20, Font.BOLD, bluecolor);
			// font20bold.setColor(Color.BLUE);
			font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);

		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		document = new Document(PageSize.A4.rotate(), 10f, 10f, 10f, 10f);
		document.addAuthor("TeacherMatch");
		document.addCreator("TeacherMatch Inc.");
		document.addSubject("Report for Candidate Withdrawn Reason");
		document.addCreationDate();
		document.addTitle(" Report for Candidate Withdrawn Reason");

		fos = new FileOutputStream(path);
		PdfWriter.getInstance(document, fos);

		document.open();

		PdfPTable mainTable = new PdfPTable(1);
		mainTable.setWidthPercentage(90);

		Paragraph[] para = null;
		PdfPCell[] cell = null;

		para = new Paragraph[3];
		cell = new PdfPCell[3];
		para[0] = new Paragraph(" ", font20bold);
		cell[0] = new PdfPCell(para[0]);
		cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell[0].setBorder(0);
		mainTable.addCell(cell[0]);

		// Image logo = Image.getInstance
		Image logo = Image.getInstance(fontPath+ "/images/Logo with Beta300.png");
		logo.scalePercent(75);

		cell[1] = new PdfPCell(logo);
		cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
		cell[1].setBorder(0);
		mainTable.addCell(cell[1]);

		document.add(new Phrase("\n"));

		para[2] = new Paragraph("", font20bold);
		cell[2] = new PdfPCell(para[2]);
		cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
		cell[2].setBorder(0);
		mainTable.addCell(cell[2]);

		document.add(mainTable);

		document.add(new Phrase("\n"));

		float[] tblwidthz = { .15f };

		mainTable = new PdfPTable(tblwidthz);
		mainTable.setWidthPercentage(100);
		para = new Paragraph[1];
		cell = new PdfPCell[1];

		para[0] = new Paragraph("Candidate Withdrawn Reason", font20bold);
		cell[0] = new PdfPCell(para[0]);
		cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
		cell[0].setBorder(0);
		mainTable.addCell(cell[0]);
		document.add(mainTable);

		document.add(new Phrase("\n"));

		float[] tblwidths = { .15f, .20f };

		mainTable = new PdfPTable(tblwidths);
		mainTable.setWidthPercentage(100);
		para = new Paragraph[2];
		cell = new PdfPCell[2];

		String name1 = userMaster.getFirstName() + " "+ userMaster.getLastName();

		para[0] = new Paragraph("Created By: " + name1, font10);
		cell[0] = new PdfPCell(para[0]);
		cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
		cell[0].setBorder(0);
		mainTable.addCell(cell[0]);

		para[1] = new Paragraph("" + "Created on: " + Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
		cell[1] = new PdfPCell(para[1]);
		cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell[1].setBorder(0);
		mainTable.addCell(cell[1]);

		document.add(mainTable);

		document.add(new Phrase("\n"));

		float[] tblwidth = { .06f, .10f, .07f, .06f, .06f, .06f };
		
		float[] tblwidth1 = { .06f, .10f, .07f, .06f, .06f };

		if(entityID==1)
			mainTable = new PdfPTable(tblwidth);
		else
			mainTable = new PdfPTable(tblwidth1);
			
		mainTable.setWidthPercentage(100);
		int datacounter=0;
		
		if(entityID==1){
		para = new Paragraph[6];
		cell = new PdfPCell[6];
		}else{
			para = new Paragraph[5];
			cell = new PdfPCell[5];
		}
		// header
		if(entityID==1){
			para[datacounter] = new Paragraph(lblDistrictName, font10_10);
			cell[datacounter] = new PdfPCell(para[datacounter]);
			cell[datacounter].setBackgroundColor(bluecolor);
			cell[datacounter].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[datacounter]);
		++datacounter;
		}
		/*
		 * para[1] = new Paragraph(""+lblJoTil,font10_10); cell[1]= new
		 * PdfPCell(para[1]); cell[1].setBackgroundColor(bluecolor);
		 * cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
		 * mainTable.addCell(cell[1]);
		 */
		// Shriram////
		para[datacounter] = new Paragraph("" + jobtitle, font10_10);
		cell[datacounter] = new PdfPCell(para[datacounter]);
		cell[datacounter].setBackgroundColor(bluecolor);
		cell[datacounter].setHorizontalAlignment(Element.ALIGN_LEFT);
		mainTable.addCell(cell[datacounter]);
		++datacounter;
		
		para[datacounter] = new Paragraph("" + lblWithdrawnBy, font10_10);
		cell[datacounter] = new PdfPCell(para[datacounter]);
		cell[datacounter].setBackgroundColor(bluecolor);
		cell[datacounter].setHorizontalAlignment(Element.ALIGN_LEFT);
		mainTable.addCell(cell[datacounter]);
		++datacounter;
		
		para[datacounter] = new Paragraph("" + lblWithdrawTime, font10_10);
		cell[datacounter] = new PdfPCell(para[datacounter]);
		cell[datacounter].setBackgroundColor(bluecolor);
		cell[datacounter].setHorizontalAlignment(Element.ALIGN_LEFT);
		mainTable.addCell(cell[datacounter]);
		++datacounter;

		para[datacounter] = new Paragraph("" + lblWithdrawnReason, font10_10);
		cell[datacounter] = new PdfPCell(para[datacounter]);
		cell[datacounter].setBackgroundColor(bluecolor);
		cell[datacounter].setHorizontalAlignment(Element.ALIGN_LEFT);
		mainTable.addCell(cell[datacounter]);
		++datacounter;

		para[datacounter] = new Paragraph("" + lblNOfWithdraws, font10_10);
		cell[datacounter] = new PdfPCell(para[datacounter]);
		cell[datacounter].setBackgroundColor(bluecolor);
		cell[datacounter].setHorizontalAlignment(Element.ALIGN_LEFT);
		mainTable.addCell(cell[datacounter]);
		++datacounter;

		

		document.add(mainTable);

		if (finallstNobleCandidate.size() == 0) {
			float[] tblwidth11 = { .10f };

			mainTable = new PdfPTable(tblwidth11);
			mainTable.setWidthPercentage(100);
			para = new Paragraph[1];
			cell = new PdfPCell[1];

			para[0] = new Paragraph(lblNoRecord, font8bold);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[0]);

			document.add(mainTable);
		}
		String sta = "";
		if (finallstNobleCandidate.size() > 0) {
			for (Iterator it = finallstNobleCandidate.iterator(); it
					.hasNext();) {
				String[] row = (String[]) it.next();
				int index = 0;
				
				
				
				if(entityID==1){
				para = new Paragraph[6];
				cell = new PdfPCell[6];
				mainTable = new PdfPTable(tblwidth);
				}else{
					para = new Paragraph[5];
					cell = new PdfPCell[5];
					mainTable = new PdfPTable(tblwidth1);
				}
				mainTable.setWidthPercentage(100);
				
				String myVal1 = "";
				String myVal2 = "";
				if(entityID==1){
				myVal1 = (row[0] == null) ? "N/A" : row[0].toString();
				para[index] = new Paragraph(row[0].toString(), font8bold);
				cell[index] = new PdfPCell(para[index]);
				cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[index]);
				index++;
				}

				myVal1 = (row[1] == null) ? "N/A" : row[1].toString();
				para[index] = new Paragraph(myVal1, font8bold);
				cell[index] = new PdfPCell(para[index]);
				cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[index]);
				index++;

				myVal1 = ((row[2] == null) || (row[2].toString().length() == 0)) ? "N/A" : row[2].toString();
				para[index] = new Paragraph(myVal1, font8bold);
				cell[index] = new PdfPCell(para[index]);
				cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[index]);
				index++;

				myVal1 = (row[3] == null) ? "N/A" : row[3].toString();
				para[index] = new Paragraph(myVal1, font8bold);
				cell[index] = new PdfPCell(para[index]);
				cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[index]);
				index++;

				myVal1 = (row[4] == null) ? "N/A" : row[4].toString();
				para[index] = new Paragraph(myVal1, font8bold);
				cell[index] = new PdfPCell(para[index]);
				cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[index]);
				index++;

				myVal1 = (row[5] == null) ? "N/A" : row[5].toString();
				para[index] = new Paragraph(myVal1, font8bold);
				cell[index] = new PdfPCell(para[index]);
				cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[index]);
				index++;
				
				index++;

				document.add(mainTable);
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		if (document != null && document.isOpen())
			document.close();
		if (writer != null) {
			writer.flush();
			writer.close();
		}
	}

	return true;
}

public String displayRecordsByEntityTypePrintPreview(String noOfRow, String pageNo,String sortOrder, String sortOrderType, String districtID,String jobTitle,String WithdrawFromId, String WithdrawToId) {
		System.out.println("inside Print Preview");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session
						.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey(
					"msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords = new StringBuffer();
		int sortingcheck = 1;
		try {

			UserMaster userMaster = null;
			Integer entityID = null;
			DistrictMaster districtMaster = null;
			SchoolMaster schoolMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			} else {
				userMaster = (UserMaster) session.getAttribute("userMaster");

				if (userMaster.getSchoolId() != null)
					schoolMaster = userMaster.getSchoolId();

				if (userMaster.getDistrictId() != null) {
					districtMaster = userMaster.getDistrictId();
				}

				entityID = userMaster.getEntityType();
			}

			// @@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			// -- get no of record in grid,
			// -- set start and end position

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo - 1) * noOfRowInPage);
			int end = ((pgNo - 1) * noOfRowInPage) + noOfRowInPage;
			int totalRecord = 0;
			// ------------------------------------
			// *//** set default sorting fieldName **//*
			String sortOrderStrVal = null;
			String sortOrderFieldName = "districtname";

			if (!sortOrder.equals("") && !sortOrder.equals(null)) {
				sortOrderFieldName = sortOrder;
			}

			String sortOrderTypeVal = "0";
			if (!sortOrderType.equals("") && !sortOrderType.equals(null)) {
				if (sortOrderType.equals("0")) {
					sortOrderStrVal = "asc";// Order.asc(sortOrderFieldName);
				} else {
					sortOrderTypeVal = "1";
					sortOrderStrVal = "desc";// Order.desc(sortOrderFieldName);
				}
			} else {
				sortOrderTypeVal = "0";
				sortOrderStrVal = "asc";// Order.asc(sortOrderFieldName);
			}

			// @@@@@@@@@@@@@@@@@@@@@ end @@@@@@@@@@@@@@@@@@@@@@@@@
			List<String[]> lstNobleCandidate = new ArrayList<String[]>();

			// int disID=Integer.parseInt(districtID);
			int disID = 0;
			String districtname="";
			// int jobCatID=Integer.parseInt(jobcategoryID);
			//int jobstatuss = Integer.parseInt(jobstatus);
//shriramprint
			
			/*int teacherid =0;
			if(teacheridd=="")
				teacherid=0;
			else*/
	//int	teacherid = 0;
			if (entityID == 2 || entityID == 3) {
				disID = districtMaster.getDistrictId();
				districtname=districtMaster.getDistrictName();
				lstNobleCandidate = teacherStatusHistoryForJobDAO
				.getCandidateWithdrawnReason(sortingcheck, sortOrderStrVal,
						start, noOfRowInPage, true, sortOrderFieldName,
						disID,jobTitle,WithdrawFromId, WithdrawToId);
			
				System.out.println("totallllllllllll=========" + totalRecord);

			} else if (entityID == 1) {
				if (districtID != null && !districtID.equals(""))
					disID = Integer.parseInt(districtID);
				else
					disID = 0;
				lstNobleCandidate = teacherStatusHistoryForJobDAO
				.getCandidateWithdrawnReason(sortingcheck, sortOrderStrVal,
						start, noOfRowInPage, true, sortOrderFieldName,
						disID,jobTitle,WithdrawFromId, WithdrawToId);
				totalRecord = lstNobleCandidate.size();
				System.out.println("totallllllllllll=========" + totalRecord);

			}

			List<String[]> finallstNobleCandidate = new ArrayList<String[]>();
			finallstNobleCandidate = lstNobleCandidate;

			tmRecords
					.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>Candidate Withdrawn Reason</div><br/>");

			tmRecords.append("<div style='width:100%'>");
			tmRecords
					.append("<div style='width:50%; float:left; font-size: 15px; '> "
							+ "Created by: "
							+ userMaster.getFirstName()
							+ " "
							+ userMaster.getLastName() + "</div>");
			tmRecords
					.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>Date of Print: "
							+ Utility
									.convertDateAndTimeToUSformatOnlyDate(new Date())
							+ "</div>");
			tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");

			tmRecords
					.append("<table  id='tblCndidtWithnReasn' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");

			tmRecords
					.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ lblDistrictName + "</th>");

			// tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+lblJoTil+"</th>");
			// shriram//Candidate First Name//
			tmRecords
					.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ jobtitle + "</th>");
			// Candidate Last Name
			tmRecords
					.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ lblWithdrawnBy + "</th>");
			// Candidate Email
			tmRecords
					.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ lblWithdrawTime + "</th>");

			tmRecords
					.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ lblWithdrawnReason + "</th>");

			tmRecords
					.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ lblNOfWithdraws + "</th>");


			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");

			if (finallstNobleCandidate.size() == 0) {
				tmRecords
						.append("<tr><td colspan='10' align='center' class='net-widget-content'>"
								+ Utility.getLocaleValuePropByKey(
										"lblNoRecord", locale) + "</td></tr>");
			}
			String sta = "";
			if (finallstNobleCandidate.size() > 0) {
				for (Iterator it = finallstNobleCandidate.iterator(); it
						.hasNext();) {
					String[] row = (String[]) it.next();
				
					String myVal1 = "";
					String myVal2 = "";

					tmRecords.append("<tr>");

					myVal1 = (row[0] == null) ? "N/A" : row[0].toString();
					tmRecords.append("<td>" + myVal1 + "</td>");

					myVal1 = (row[1] == null) ? "N/A" : row[1].toString();
					tmRecords.append("<td>" + myVal1 + "</td>");

					myVal1 = ((row[2] == null) || (row[2].toString().length() == 0)) ? "N/A"
							: row[2].toString();
					tmRecords.append("<td>" + myVal1 + "</td>");

					myVal1 = (row[3] == null) ? "N/A" : row[3].toString();
					tmRecords.append("<td>" + myVal1 + "</td>");

					myVal1 = (row[4] == null) ? "N/A" : row[4].toString();
					tmRecords.append("<td>" + myVal1 + "</td>");

					myVal1 = (row[5] == null) ? "N/A" : row[5].toString();
					tmRecords.append("<td>" + myVal1 + "</td>");
					
					tmRecords.append("</tr>");
				}

			}
			tmRecords.append("</tbody>");
			tmRecords.append("</table>");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return tmRecords.toString();
	}

	

	public String getJobStatusList() {
		StringBuffer sb = new StringBuffer();
		try {
			List<StatusMaster> allStatusMasterlist = statusMasterDAO
					.findAllStatusMaster();
			// set job status
			if (allStatusMasterlist.size() > 0) {
				sb.append("<option style='width:0px;' value=''>All</option>");
				for (StatusMaster jtle : allStatusMasterlist) {
					sb.append("<option style='width:0px;' value="
							+ jtle.getStatusId() + ">" + jtle.getStatus()
							+ "</option>");
				}
			} else {
				sb.append("<option style='width:0px;' value=''>"
						+ Utility.getLocaleValuePropByKey("lblNotAvailable",
								locale) + "</option>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	
	  public String getJobTitleList(String districtId,String jobTitle) { 
		  
		  Criterion districtCriteria=null;
		  if(!districtId.equalsIgnoreCase(""))
		  districtCriteria=Restrictions.eq("districtMaster.districtId",Integer.parseInt(districtId));
		 
		  System.out.println("districtMaster.districtMaster"+districtId);
		  Criterion jobTitleCretria=Restrictions.ilike("jobTitle","%"+jobTitle,MatchMode.START);
		  
		  List<JobOrder> jobOrder=null;
		  StringBuffer sb=new StringBuffer();
		  try{
			  if(districtCriteria!=null)
				  jobOrder =jobOrderDAO.findByJobTitle(districtCriteria,jobTitleCretria);
			  else
				  jobOrder =jobOrderDAO.findByJobTitle(jobTitleCretria);
			  if(jobOrder!=null)
			  {
				  System.out.println("jobList    "+jobOrder.size());
				  for(JobOrder jo:jobOrder){
					  String str1 = "onclick=\"onclick1(this,'"+jo.getJobTitle()+"')\"";
					  String str2 = "onmouseover=\"mouseover1(this,'"+jo.getJobTitle()+"\')\""; 
					  sb.append("<div class='test' style='padding-left: 2px;;background-color: white;border-top: 1px solid #AEAEAE;' id='jobTitleShow_"+jo.getJobId()+"' "+str1+" "+str2+">"+jo.getJobTitle()+"</div>");
				  }
				  
			  }  
			  
			  
			  
			  
			  
			  
		  }catch(Exception ex){}
		
	    return sb.toString();
	  }
	 

}
