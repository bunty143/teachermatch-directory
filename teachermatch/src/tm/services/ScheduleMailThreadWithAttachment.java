package tm.services;


public class ScheduleMailThreadWithAttachment extends Thread{

	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) {
		this.emailerService = emailerService;
	}
	
	private String mailto;
	public void setMailto(String mailto) {
		this.mailto = mailto;
	}
	
	private String mailfrom;
	public void setMailfrom(String mailfrom) {
		this.mailfrom = mailfrom;
	}
	
	private String mailsubject;
	public void setMailsubject(String mailsubject) {
		this.mailsubject = mailsubject;
	}
	
	private String mailcontent;
	public void setMailcontent(String mailcontent) {
		this.mailcontent = mailcontent;
	}
	
	private String fileName;
	private String filePath;
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public ScheduleMailThreadWithAttachment() {
		super();
	}
	
	public void run()
	{
		try{			
		
			if(fileName!=null && !fileName.equalsIgnoreCase(""))
			{
				System.out.println("Email ... With Attach");
				emailerService.sendMailWithAttachments(mailto, mailsubject, mailfrom, mailcontent,filePath,fileName);
			}
			else
			{
				System.out.println("Email ... No Attach");
				emailerService.sendMailAsHTMLText(mailto+"#"+mailfrom, mailsubject,mailcontent);
			}
			
			System.out.println("Mail set successfully to "+mailto);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
