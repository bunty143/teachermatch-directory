package tm.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.DistrictApprovalGroups;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictApprovalGroupsDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;

public class GroupsAjax {
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private DistrictApprovalGroupsDAO districtApprovalGroupsDAO;
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	
	public String displayGroups(String districtName,Integer districtId,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println("::::::::::::::::::::::::::::::::: displayGroups :::::::::::::::::::::::::::::::::");
		System.out.println("districtId : "+districtId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException("Your session has expired!");
		StringBuffer tmRecords = new StringBuffer();
		int sortingcheck=1;
		try
		{
			UserMaster userMaster = null;
			Integer entityID = null;
			DistrictMaster districtMaster = null;
			
			if (session==null || session.getAttribute("userMaster")==null)
				return "false";
			else
			{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getDistrictId()!=null)
					districtMaster=userMaster.getDistrictId();
				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
				
			/** set default sorting fieldName **/
			String sortOrderFieldName="groupName";
			String sortOrderNoField="groupName";

			if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("groupName"))
				sortingcheck=1;
				
			//System.out.println("Sort order :: "+sortingcheck);
			/**Start set dynamic sorting fieldName **/
				
			Order sortOrderStrVal=null;

			if(sortOrder!=null)
			{
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}				
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
			
		    if(totalRecord<end)
		    	end=totalRecord;
		    
			String responseText="";
			
			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			//responseText=PaginationAndSorting.responseSortingLink("Group Name",sortOrderNoField,"groupName",sortOrderTypeVal,pgNo);
			//tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			tmRecords.append("<th  valign='top'>Group Name</th>");
			//responseText=PaginationAndSorting.responseSortingLink("Members",sortOrderNoField,"",sortOrderTypeVal,pgNo);
			//tmRecords.append("<th valign='top'>"+responseText+"</th>");
			tmRecords.append("<th  valign='top'>Members Name</th>");
			tmRecords.append("<th  valign='top'>Actions</th>");
					
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(districtId!=null){
				try {
					if(districtMaster==null)
						districtMaster = districtMasterDAO.findById(districtId, false, false);
					
					List<DistrictApprovalGroups> districtApprovalGroupList = districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByDistrict(districtMaster);
					
					if(districtApprovalGroupList.size()==0)
						tmRecords.append("<tr><td colspan='5' align='center' class='net-widget-content'>No Record found</td></tr>" );
					
					if(districtApprovalGroupList!=null && districtApprovalGroupList.size()>0)
					{
						Set<Integer> membersId = new TreeSet<Integer>();
						for(DistrictApprovalGroups districtApprovalGroups :districtApprovalGroupList)
						{
							String[] membersName = (districtApprovalGroups.getGroupMembers()!=null)? districtApprovalGroups.getGroupMembers().split("#") : null;
							if(membersName!=null && membersName.length >0)
								for(String member : membersName)
									if(!member.equals("") || member==null)
										membersId.add(Integer.parseInt(member));
						}
						
						Map<String, DistrictKeyContact> membersMap = new HashMap<String, DistrictKeyContact>();
						if(membersId!=null && membersId.size()>0)
						{
							List<DistrictKeyContact> membersList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", membersId));
							for(DistrictKeyContact contact : membersList)
							{
								if(contact!=null)
								{
									membersMap.put(""+contact.getKeyContactId(), contact);
								}
							}
						}
						
						membersId=null;
						for(DistrictApprovalGroups districtApprovalGroups :districtApprovalGroupList)
						{
							String groupName = districtApprovalGroups.getGroupName();
							String[] membersIds = (districtApprovalGroups.getGroupMembers()!=null)? districtApprovalGroups.getGroupMembers().split("#") : null;
							String grpIds = "";
							tmRecords.append("<tr>");
				            tmRecords.append("<td>"+groupName+"</td>");
				            if(membersIds!=null && membersIds.length >0){
				            	tmRecords.append("<td>");
								for(String memberId : membersIds){
									if(!memberId.equals("")){
										if(membersMap.get(memberId) != null)
										{
											if(membersIds.length==2){
												tmRecords.append(membersMap.get(memberId).getKeyContactFirstName()+" "+membersMap.get(memberId).getKeyContactLastName());
												//+"&nbsp<a href='javascript:void(0);' data-toggle='tooltip' title='Remove Member' onclick='removeMemberConfirmation("+districtApprovalGroups.getDistrictApprovalGroupsId()+","+membersMap.get(memberId).getKeyContactId()+")' ><img width='15' height='15' class='can' src='images/can-icon.png'></a>&nbsp;&nbsp;");
											} else{
												tmRecords.append(membersMap.get(memberId).getKeyContactFirstName()+" "+membersMap.get(memberId).getKeyContactLastName()+"&nbsp<a href='javascript:void(0);' data-toggle='tooltip' title='Remove Member' onclick='removeMemberConfirmation("+districtApprovalGroups.getDistrictApprovalGroupsId()+","+membersMap.get(memberId).getKeyContactId()+")' ><img width='15' height='15' class='can' src='images/can-icon.png'></a>&nbsp;&nbsp;");
											}
											grpIds +=membersMap.get(memberId).getKeyContactId()+",";
										}
									}
								}
								tmRecords.append("</td>");
				            }
				            else
				            	tmRecords.append("<td></td>");
				            tmRecords.append("<td><a href='javascript:void(0);' data-toggle='tooltip' title='Edit Group' onclick=\"editGroup("+districtApprovalGroups.getDistrictApprovalGroupsId()+",'"+(!grpIds.trim().equals("")?grpIds.substring(0, grpIds.length()-1):"")+"');\"><i class='icon-edit'></i></a> | <a href='javascript:void(0);' data-toggle='tooltip' title='Remove Group' onclick='removeGroupConfirmation("+districtApprovalGroups.getDistrictApprovalGroupsId()+")' ><i class='icon-trash'></i></a></td>");
				            tmRecords.append("</tr>");			
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				tmRecords.append("<tr><td colspan='5' align='center' class='net-widget-content'>No Record found</td></tr>" );
			}
			tmRecords.append("</tbody>");
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return tmRecords.toString();
	}

}
