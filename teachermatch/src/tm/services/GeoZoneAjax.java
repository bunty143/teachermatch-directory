package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.utility.Utility;

public class GeoZoneAjax {
	
	
	 String locale = Utility.getValueOfPropByKey("locale");
	// String locale = Utility.getValueOfPropByKey("locale");
     String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
     String lblGeoZoneName1=Utility.getLocaleValuePropByKey("lblGeoZoneName1", locale);
     String lblDescription1=Utility.getLocaleValuePropByKey("lblDescription1", locale);
     String lblDistrictName=Utility.getLocaleValuePropByKey("lblDistrictName", locale);
     String lblofSchools=Utility.getLocaleValuePropByKey("lblofSchools", locale);
     String lblLastUpdated1=Utility.getLocaleValuePropByKey("lblLastUpdated1", locale);
     String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
     String msgNoGeoZonefound1=Utility.getLocaleValuePropByKey("msgNoGeoZonefound1", locale);
     String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
     String lnkDlt=Utility.getLocaleValuePropByKey("lnkDlt", locale);
     
     
	
	

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO;
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public List<DistrictMaster> getFieldOfDistrictList(String DistrictName)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		try{

			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "A");
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);

				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);

				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));

			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion1);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return districtMasterList;
	}
	
	@Transactional(readOnly=false)
	public String getSchoolByDistrict(Integer districtId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		List<SchoolMaster>finalDistrictSchoolMasters=null;
		List<SchoolMaster>finalSchoolMasters=null;
		StringBuffer stringBuffer=new StringBuffer();
		try{
			
			if( districtId!=null)
			{
				DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
				Criterion criterion11 = Restrictions.eq("districtId",districtMaster);
				Criterion criterion2 = Restrictions.isNull("geoZoneMaster");
				
				Order ascOrder = Order.asc("schoolName"); 
				finalDistrictSchoolMasters=districtSchoolsDAO.findZoneWiseSchools(districtId); 
				finalSchoolMasters=schoolMasterDAO.findByCriteria(ascOrder,criterion11,criterion2);
				
				finalDistrictSchoolMasters.retainAll(finalSchoolMasters);
				
				for(SchoolMaster pojo:finalDistrictSchoolMasters)
				{	
					stringBuffer.append("<option value='"+pojo.getSchoolId()+"'>"+pojo.getSchoolName()+"</option>");
				}
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return stringBuffer.toString();
	}
	
	public int saveZoneAndSchool(Integer geozoneId,String geozonename,String geozonecode,String discription,Integer noOfSchoolsInGeoZone, Integer districtId,String schoolIdStr)
	{
		try{
			GeoZoneMaster geoZoneMaster=null;
			Criterion criterion1=null;
			int addEditFlag=0;
			DistrictMaster districtMaster=null;
			
			StatelessSession statelessSession=schoolMasterDAO.getSessionFactory().openStatelessSession(); 
			Transaction transaction=statelessSession.beginTransaction();
			if(districtId>0)
			{
				districtMaster=districtMasterDAO.findById(districtId, false, false);
			}
			
			if(geozoneId==null || geozoneId==0)
			{
				geoZoneMaster=new GeoZoneMaster();
				geoZoneMaster.setCreatedDateTime(new Date()); 
			}
			else
			{
				addEditFlag=1;
				geoZoneMaster=geoZoneMasterDAO.findById(geozoneId, false, false);
				criterion1=Restrictions.eq("geoZoneMaster", geoZoneMaster);
				geoZoneMaster.setUpdatedDateTime(new Date());
			}
			geoZoneMaster.setDistrictMaster(districtMaster);
			geoZoneMaster.setGeoZoneName(geozonename);
			geoZoneMaster.setGeoZoneCode(geozonecode);
			geoZoneMaster.setGeoZoneDescription(discription);
			geoZoneMaster.setNoOfSchoolsInGeoZone(noOfSchoolsInGeoZone);
			geoZoneMaster.setStatus("A");
			int duplicateFlag=geoZoneMasterDAO.findDupicateGeoZone(geozoneId,districtId,geozonename,geozonecode); 
			if(duplicateFlag==1)
			{
				return duplicateFlag;
			}else if(duplicateFlag==2)
			{
				return duplicateFlag;
			}else
			{
				geoZoneMasterDAO.makePersistent(geoZoneMaster);
			}
			if(geoZoneMaster.getGeoZoneId()!=null && geoZoneMaster.getGeoZoneId()>0)
			{
				List<SchoolMaster>lstSchoolMasters=null;
				if(addEditFlag==1)
				{
					lstSchoolMasters=schoolMasterDAO.findByCriteria(criterion1);
					if(lstSchoolMasters!=null && lstSchoolMasters.size()>0)
					{	
						for(SchoolMaster pojo:lstSchoolMasters)
						{
							pojo.setGeoZoneMaster(null);
							statelessSession.update(pojo);
						}
					}
				}
				String[] schoolIdList=schoolIdStr.split("#");
				List<Long> listSchoolMasterIds=new ArrayList<Long>();
				for(String ids:schoolIdList)
				{	
					listSchoolMasterIds.add(Long.parseLong(ids));
				}
				Criterion criterion2=Restrictions.in("schoolId",listSchoolMasterIds); 
				lstSchoolMasters=schoolMasterDAO.findByCriteria(criterion2);
				for(SchoolMaster pojo:lstSchoolMasters)
				{
					pojo.setGeoZoneMaster(geoZoneMaster);
					statelessSession.update(pojo);
				}
			}
			transaction.commit();
			statelessSession.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	public String displayGeoZoneGrid(Integer districtId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
	    	WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(msgYrSesstionExp);
		    }
			StringBuffer gz=new StringBuffer();
			try{
				
				int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
				int pgNo 			= 	Integer.parseInt(pageNo);
				int start 			= 	((pgNo-1)*noOfRowInPage);
				int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord		= 	0;
				//------------------------------------
				
				UserMaster userMaster=null;
				int roleId=0;
				if (session == null || session.getAttribute("userMaster") == null) 
				{
					return "redirect:index.jsp";
				}else{
					userMaster=	(UserMaster) session.getAttribute("userMaster");
					if(userMaster.getRoleId().getRoleId()!=null){
						roleId=userMaster.getRoleId().getRoleId();
					}
				}
				List<GeoZoneMaster>geoZoneMasters=new ArrayList<GeoZoneMaster>();
				String sortOrderFieldName	=	"geoZoneName";
				
				Order  sortOrderStrVal		=	null;
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	Order.asc(sortOrderFieldName);
				}
				
				if(districtId!=0)
				{
					DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
					if(session.getAttribute("displayType").toString().equalsIgnoreCase("1")){
					session.setAttribute("districtMasterId", districtMaster.getDistrictId());
					session.setAttribute("districtMasterName",districtMaster.getDistrictName());
					}
					Criterion criterion=Restrictions.eq("districtMaster", districtMaster);
					geoZoneMasters=geoZoneMasterDAO.findByCriteria(sortOrderStrVal,criterion);
				}
				else
				{
					if(session.getAttribute("displayType").toString().equalsIgnoreCase("1")){
					session.setAttribute("districtMasterId", 0);
					session.setAttribute("districtMasterName", "");
					}
					geoZoneMasters=geoZoneMasterDAO.findByCriteria(sortOrderStrVal);
				}
				totalRecord=geoZoneMasters.size();
				
				if(totalRecord<end)
					end=totalRecord;
				List<GeoZoneMaster> lstsortedGeoZoneMaster		=	geoZoneMasters.subList(start,end);
				HashMap<Integer , Boolean>map=jobOrderDAO.getUsedZoneInJobOrder();
				
				gz.append("<table  id='geozoneTable' width='100%' border='0' >");
				gz.append("<thead class='bg'>");
				gz.append("<tr>");
				String responseText="";
				responseText=PaginationAndSorting.responseSortingLink(lblGeoZoneName1,sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
				gz.append("<th width='25%' valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(lblDescription1,sortOrderFieldName,"geoZoneDescription",sortOrderTypeVal,pgNo);
				gz.append("<th width='35%' valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(lblDistrictName,sortOrderFieldName,"districtMaster",sortOrderTypeVal,pgNo);
				gz.append("<th width='35%' valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(lblofSchools,sortOrderFieldName,"noOfSchoolsInGeoZone",sortOrderTypeVal,pgNo);
				gz.append("<th width='20%' valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(lblLastUpdated1,sortOrderFieldName,"updatedDateTime",sortOrderTypeVal,pgNo);
				gz.append("<th width='15%' valign='top'>"+responseText+"</th>");
				
				gz.append("<th width='15%' valign='top'>"+lblAct+"</th>");
				gz.append("</tr>");
				gz.append("</thead>");
				
				if(lstsortedGeoZoneMaster.size()==0)
					gz.append("<tr><td colspan='6'>"+msgNoGeoZonefound1+"</td></tr>");
				for (GeoZoneMaster pojo : lstsortedGeoZoneMaster) 
				{
					String desc="";
					if(pojo.getGeoZoneDescription()!=null){
						desc=pojo.getGeoZoneDescription();
					}
					
					gz.append("<tr>" );
					gz.append("<td>"+pojo.getGeoZoneName()+"</td>");
					gz.append("<td>"+desc+"</td>");
					gz.append("<td>"+pojo.getDistrictMaster().getDistrictName()+"</td>");
					gz.append("<td>"+(pojo.getNoOfSchoolsInGeoZone())+"</td>");
					try{
						if(pojo.getUpdatedDateTime()!=null)
						{
							gz.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(pojo.getUpdatedDateTime())+"</td>");
						}else
						{
							gz.append("<td></td>");
						}
					}catch(Exception e){
						gz.append("<td></td>");
					}
					gz.append("<td>");
						gz.append("<a href='javascript:void(0);' title='Edit' onclick='return editGeoZone("+pojo.getGeoZoneId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>");
						if(!(map.get(pojo.getGeoZoneId())!=null))
						{
							gz.append("&nbsp;|&nbsp;<a href='javascript:void(0);' title='Delete' onclick='return showDeleteModel("+pojo.getGeoZoneId()+")'\"><i class='fa fa-trash-o' fa-lg></i></a>");
						}
					gz.append("</td>");
					gz.append("</tr>");
				}
				gz.append("</table>");
				gz.append(PaginationAndSorting.getPaginationStringForPanelAjaxForDSPQ(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
	return gz.toString();
  }	
	@Transactional(readOnly=false)
	public GeoZoneMaster getZoneById(Integer geozoneId)
	{
		GeoZoneMaster  geoZoneMaster=null;
		try
		{
			if(geozoneId>0)
			{
				geoZoneMaster=geoZoneMasterDAO.findById(geozoneId, false, false);
				Criterion criterion1=Restrictions.eq("geoZoneMaster", geoZoneMaster);
				List<SchoolMaster>lstSchoolMasters=schoolMasterDAO.findByCriteria(criterion1);
				String[] schoolIds=new String[lstSchoolMasters.size()];
				String[] schoolNames=new String[lstSchoolMasters.size()];
				int i=0;
				for(SchoolMaster pojo:lstSchoolMasters)
				{
					schoolIds[i]=""+pojo.getSchoolId();
					schoolNames[i]=""+pojo.getSchoolName();
					i++;
				}
				geoZoneMaster.setSchoolIds(schoolIds);
				geoZoneMaster.setSchoolNames(schoolNames);
			}
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		return geoZoneMaster;
	}
	
	@Transactional(readOnly=false)
	public void deleteZoneById(Integer geozoneId)
	{
		if(geozoneId!=null || geozoneId==0)
		{
			GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geozoneId, false, false);
			Criterion criterion=Restrictions.eq("geoZoneMaster", geoZoneMaster);
			List<SchoolMaster>schoolMasters=schoolMasterDAO.findByCriteria(criterion);
			for(SchoolMaster pojo:schoolMasters)
			{
				pojo.setGeoZoneMaster(null);
				schoolMasterDAO.makePersistent(pojo);
			}
			geoZoneMasterDAO.makeTransient(geoZoneMaster);
		}
	}
}
