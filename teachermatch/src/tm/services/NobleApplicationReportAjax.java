package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.api.PaginationAndSortingAPI;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherProfileVisitHistory;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherProfileVisitHistoryDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.services.quartz.FTPConnectAndLogin;
import tm.utility.DistrictNameCompratorASC;
import tm.utility.DistrictNameCompratorDESC;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
public class NobleApplicationReportAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	

	public void setTeacherPersonalInfoDAO(
			TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}

	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	
	public String displayRecordsByEntityType(String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	 {
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=1;
			try{
				
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				//System.out.println("schoolMaster="+schoolMaster+"districtMaster="+districtMaster+"entityID="+entityID);
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position


				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
			 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"jobforteacherid";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<String[]> lstNobelApplication =new ArrayList<String[]>();
				
				if(entityID==2||entityID==3)
				{
					lstNobelApplication =jobForTeacherDAO.noblestapplication(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
			    totalRecord = lstNobelApplication.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
			    	 
				}
				else if(entityID==1) 
				{
					System.out.println("indside admin ");
					
					lstNobelApplication =jobForTeacherDAO.noblestapplication(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
				    	totalRecord = lstNobelApplication.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
					
				
				}
				
				List<String[]> finallstNobelApplication =new ArrayList<String[]>();
			     if(totalRecord<end)
						end=totalRecord;
			     finallstNobelApplication=lstNobelApplication.subList(start,end);
							
				String responseText="";
				
				tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
								
				responseText=PaginationAndSorting.responseSortingLink("Internal Record ID",sortOrderFieldName,"jobforteacherid",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink("Internal Teacher ID",sortOrderFieldName,"teacherid",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Internal Job ID",sortOrderFieldName,"jobid",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Job Title",sortOrderFieldName,"jobtitle",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Job Posted Date",sortOrderFieldName,"jobstartdate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Job Post End Date",sortOrderFieldName,"jobenddate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Job Category",sortOrderFieldName,"jobCategoryName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Job Subject",sortOrderFieldName,"subjectname",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Application Record Date",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Application Status",sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("SLC Status",sortOrderFieldName,"AmricanIndian",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Hired By School",sortOrderFieldName,"schoolname",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Have Cover Letter",sortOrderFieldName,"coverletter",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Is Affilated",sortOrderFieldName,"isAffilated",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Last Activity on Application",sortOrderFieldName,"lastactivity",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Last Activity Date",sortOrderFieldName,"lastActivityDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Demo Date Scheduled",sortOrderFieldName,"demodatescheduled",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Demo Date Cancelled",sortOrderFieldName,"demodatecancelled",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				
			
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				if(finallstNobelApplication.size()==0){
					 tmRecords.append("<tr><td colspan='9' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
					}
				
				String sta="";
				  if(finallstNobelApplication.size()>0){
	                   for (Iterator it = finallstNobelApplication.iterator(); it.hasNext();)
	                   {
	                	   String[] row = (String[]) it.next(); 
	                	   
	                	   String  myVal1="";
	                	   String  myVal2="";
	                	  
	                	        tmRecords.append("<tr>");	
	                	        
	                            myVal1 = (row[0]==null)? "N/A" : row[0].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
	                            
			                    
			                    myVal1 = (row[1]==null)? "N/A" : row[1].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? "N/A" : row[2].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    
			                    myVal1 = (row[3]==null)? "N/A" : row[3].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[4]==null)? "N/A" : row[4].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[5]==null)? "N/A" : row[5].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[11]==null)||(row[11].toString().length()==0))? " " : row[11].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[12]==null)||(row[12].toString().length()==0))? " " : row[12].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[13]==null)? "N/A" : row[13].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[14]==null)? "N/A" : row[14].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[15]==null)? "N/A" : row[15].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[16]==null)? "N/A" : row[16].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[17]==null)? "N/A" : row[17].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    
			                    tmRecords.append("</tr>");
	               }               
	              }
				
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			

			}catch(Exception e){
				e.printStackTrace();
			}
			
		 return tmRecords.toString();
	  }
		
	/*public String displayRecordsByEntityTypeEXL(String districtName,String jobStatus,int districtOrSchoolId,String fJobStatus,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate,String jobendstartDate,String jobendendDate)
	{
		System.out.println(jobStatus+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		int sortingcheck=1;
		try{
			
			
			
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
		}
		
		//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start =((pgNo-1)*noOfRowInPage);
		int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		
		 *//** set default sorting fieldName **//*
		String  sortOrderStrVal		=	null;
		String sortOrderFieldName	=	"jobforteacherid";
		
		if(!sortOrder.equals("") && !sortOrder.equals(null))
		{
			sortOrderFieldName		=	sortOrder;
		}
		
		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null))
		{
			if(sortOrderType.equals("0"))
			{
				sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
			}
			else
			{
				sortOrderTypeVal	=	"1";
				sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
			}
		}
		else
		{
			sortOrderTypeVal		=	"0";
			sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
		}
		
		//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
			List<String[]> lstNobelApplication =new ArrayList<String[]>();
			
			
				if(entityID==2||entityID==3)
				{
					//System.out.println("inside entity id=2");
					lstNobelApplication =teacherPersonalInfoDAO.hiredEeoc(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
			    	totalRecord = lstNobelApplication.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
			    	 
				}
				else if(entityID==1) 
				{
					System.out.println("indside admin ");
					if(districtOrSchoolId!=0)
					{
						lstNobelApplication =teacherPersonalInfoDAO.hiredEeoc(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
				    	totalRecord = lstNobelApplication.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
					}
					else
					{
					lstNobelApplication =teacherPersonalInfoDAO.hiredEeocForAdmin(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
			    	totalRecord = lstNobelApplication.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
					}
			    	
				}
				
				List<String[]> finallstNobelApplication =new ArrayList<String[]>();
			     if(totalRecord<end)
						end=totalRecord;
			     finallstNobelApplication=lstNobelApplication;


		 
		 
			 //  Excel   Exporting	
					
					String time = String.valueOf(System.currentTimeMillis()).substring(6);
					//String basePath = request.getRealPath("/")+"/candidate";
					String basePath = request.getSession().getServletContext().getRealPath ("/")+"/hiredapplicants";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="nobleapplication"+time+".xls";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

					WorkbookSettings wbSettings = new WorkbookSettings();

					wbSettings.setLocale(new Locale("en", "EN"));

					WritableCellFormat timesBoldUnderline;
					WritableCellFormat header;
					WritableCellFormat headerBold;
					WritableCellFormat times;

					WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
					workbook.createSheet("nobleapplication", 0);
					WritableSheet excelSheet = workbook.getSheet(0);

					WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
					// Define the cell format
					times = new WritableCellFormat(times10pt);
					// Lets automatically wrap the cells
					times.setWrap(true);

					// Create create a bold font with unterlines
					WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
					WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

					timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
					timesBoldUnderline.setAlignment(Alignment.CENTRE);
					// Lets automatically wrap the cells
					timesBoldUnderline.setWrap(true);

					header = new WritableCellFormat(times10ptBoldUnderline);
					headerBold = new WritableCellFormat(times10ptBoldUnderline);
					CellView cv = new CellView();
					cv.setFormat(times);
					cv.setFormat(timesBoldUnderline);
					cv.setAutosize(true);

					header.setBackground(Colour.GRAY_25);

					// Write a few headers
					excelSheet.mergeCells(0, 0, 12, 1);
					Label label;
					label = new Label(0, 0, "Noble St Application ", timesBoldUnderline);
					excelSheet.addCell(label);
					excelSheet.mergeCells(0, 3, 12, 3);
					label = new Label(0, 3, "");
					excelSheet.addCell(label);
					excelSheet.getSettings().setDefaultColumnWidth(18);
					
					int k=4;
					int col=1;
					label = new Label(0, k, "District Name",header); 
					excelSheet.addCell(label);
					
					label = new Label(1, k, "Job Title",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Job Status",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "No. of Applicant",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Responded",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "No. of Responded",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Black",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Asian",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Hispanic",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "White",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Amrican Indian",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Hawaiian",header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, "Multi",header); 
					excelSheet.addCell(label);
					 
					
					k=k+1;
					if(finallstNobelApplication.size()==0)
					{	
						excelSheet.mergeCells(0, k, 9, k);
						label = new Label(0, k, "No Records Founds"); 
						excelSheet.addCell(label);
					}
					String sta="";
			if(finallstNobelApplication.size()>0){
				 for (Iterator it = finallstNobelApplication.iterator(); it.hasNext();) 					            
					{
					String[] row = (String[]) it.next();
				col=1;
			
						label = new Label(0, k,row[1].toString()); 
						excelSheet.addCell(label);
				
						label = new Label(1, k, row[2].toString()); 
						excelSheet.addCell(label);
				
						
						if(row[13].toString().equalsIgnoreCase("A"))
						{
							sta="Active";
						}
						else
						{
							sta="Inactive";
						}
						
						 //String  myVal1 = (row[3]==null)? "N/A" : row[3].toString();
							label = new Label(++col, k, sta); 
							excelSheet.addCell(label);
				 
					 String  myVal1 = (row[3]==null)? "N/A" : row[3].toString();
					label = new Label(++col, k, myVal1); 
					excelSheet.addCell(label);
					
					 String  myVal2 = ((row[4]==null)||(row[4].toString().length()==0))? "0.00" : row[4].toString();
					label = new Label(++col, k, myVal2+"%"); 
					excelSheet.addCell(label);
					
					 String  myVal3 = ((row[5]==null)||(row[5].toString().length()==0))? "N/A" : row[5].toString();
					label = new Label(++col, k, myVal3); 
					excelSheet.addCell(label);
					
					 String  myVal4 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString()+"%";
					label = new Label(++col, k, myVal4); 
					excelSheet.addCell(label);
					
					String  myVal7 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString()+"%";
					label = new Label(++col, k, myVal7); 
					excelSheet.addCell(label);
					
					String  myVal8 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString()+"%";
					label = new Label(++col, k, myVal8); 
					excelSheet.addCell(label);
					
					String  myVal9 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString()+"%";
					label = new Label(++col, k, myVal9); 
					excelSheet.addCell(label);
					
					String  myVal10 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString()+"%";
					label = new Label(++col, k, myVal10); 
					excelSheet.addCell(label);
					
					String  myVal11 = ((row[11]==null)||(row[11].toString().length()==0))? " " : row[11].toString()+"%";
					label = new Label(++col, k, myVal11); 
					excelSheet.addCell(label);
					
					String  myVal12 = ((row[12]==null)||(row[12].toString().length()==0))? " " : row[12].toString()+"%";
					label = new Label(++col, k, myVal12); 
					excelSheet.addCell(label);
					
									
								
				   k++; 
			 }
			}
	
			workbook.write();
			workbook.close();
		}catch(Exception e){}
		
	 return fileName;
  }
	*/
  
	
	public String displayRecordsByEntityTypePDF(String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport(  noOfRow, pageNo, sortOrder, sortOrderType,basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	public boolean generateCandidatePDReport(String noOfRow,String pageNo,String sortOrder,String sortOrderType,String path,String realPath)
	{
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			int sortingcheck=1;
			try{
				
				
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				
				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"jobforteacherid";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<String[]> lstNobelApplication =new ArrayList<String[]>();
					
					
					if(entityID==2||entityID==3)
					{
						lstNobelApplication =jobForTeacherDAO.noblestapplication(sortingcheck,sortOrderStrVal,start, noOfRowInPage,false,sortOrderFieldName);
				    totalRecord = lstNobelApplication.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
				    	 
					}
					else if(entityID==1) 
					{
						System.out.println("indside admin ");
						
						lstNobelApplication =jobForTeacherDAO.noblestapplication(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
					    	totalRecord = lstNobelApplication.size();
					    	System.out.println("totallllllllllll========="+totalRecord);
						
					}
												
						List<String[]> finallstNobelApplication =new ArrayList<String[]>();
					     if(totalRecord<end)
								end=totalRecord;
					     finallstNobelApplication=lstNobelApplication;
		
				 String fontPath = realPath;
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject("Noble St Applications");
						document.addCreationDate();
						document.addTitle("Noble St Applications");

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));
						
						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						
						para[0] = new Paragraph("Noble St Application",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
												
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph("Created By: "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+"Created on: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						document.add(new Phrase("\n"));


						float[] tblwidth={.05f,.05f,.05f,.08f,.08f,.08f,.05f,.05f,.06f,.05f,.06f,.06f,.04f,.05f,.05f,.04f,.02f,.02f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[18];
						cell = new PdfPCell[18];
				// header
						para[0] = new Paragraph("Internal Record ID",font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+"Internal Teacher ID",font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(""+"Internal Job ID",font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[2]);
						
						para[3] = new Paragraph(""+"Job Title",font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[3]);

						para[4] = new Paragraph(""+"Job Posted Date",font10_10);
						cell[4]= new PdfPCell(para[4]);
						cell[4].setBackgroundColor(bluecolor);
						cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[4]);
						
						para[5] = new Paragraph(""+"Job Post End Date",font10_10);
						cell[5]= new PdfPCell(para[5]);
						cell[5].setBackgroundColor(bluecolor);
						cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[5]);
						
						para[6] = new Paragraph(""+"Job Category",font10_10);
						cell[6]= new PdfPCell(para[6]);
						cell[6].setBackgroundColor(bluecolor);
						cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[6]);
						
						para[7] = new Paragraph(""+"Job Subject",font10_10);
						cell[7]= new PdfPCell(para[7]);
						cell[7].setBackgroundColor(bluecolor);
						cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[7]);
						
						para[8] = new Paragraph(""+"Application Record Date",font10_10);
						cell[8]= new PdfPCell(para[8]);
						cell[8].setBackgroundColor(bluecolor);
						cell[8].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[8]);
						
						para[9] = new Paragraph(""+"Application Status",font10_10);
						cell[9]= new PdfPCell(para[9]);
						cell[9].setBackgroundColor(bluecolor);
						cell[9].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[9]);
						
						para[10] = new Paragraph(""+"SLC Status",font10_10);
						cell[10]= new PdfPCell(para[10]);
						cell[10].setBackgroundColor(bluecolor);
						cell[10].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[10]);
												
						
						para[11] = new Paragraph(""+"Hired By School",font10_10);
						cell[11]= new PdfPCell(para[11]);
						cell[11].setBackgroundColor(bluecolor);
						cell[11].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[11]);
						document.add(mainTable);
						
						para[12] = new Paragraph(""+"Have Cover Letter",font10_10);
						cell[12]= new PdfPCell(para[12]);
						cell[12].setBackgroundColor(bluecolor);
						cell[12].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[12]);
						document.add(mainTable);
						
						para[13] = new Paragraph(""+"Is Affilated",font10_10);
						cell[13]= new PdfPCell(para[13]);
						cell[13].setBackgroundColor(bluecolor);
						cell[13].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[13]);
						document.add(mainTable);
						
						para[14] = new Paragraph(""+"Last Activity on Application",font10_10);
						cell[14]= new PdfPCell(para[14]);
						cell[14].setBackgroundColor(bluecolor);
						cell[14].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[14]);
						document.add(mainTable);
						
						para[15] = new Paragraph(""+"Last Activity Date",font10_10);
						cell[15]= new PdfPCell(para[15]);
						cell[15].setBackgroundColor(bluecolor);
						cell[15].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[15]);
						document.add(mainTable);
						
						para[16] = new Paragraph(""+"Demo Scheduled",font10_10);
						cell[16]= new PdfPCell(para[16]);
						cell[16].setBackgroundColor(bluecolor);
						cell[16].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[16]);
						document.add(mainTable);
						
						para[17] = new Paragraph(""+"Demo Cancelled",font10_10);
						cell[17]= new PdfPCell(para[15]);
						cell[17].setBackgroundColor(bluecolor);
						cell[17].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[17]);
						document.add(mainTable);
						
						if(finallstNobelApplication.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph("No Record Found.",font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
				     String sta="";
						if(finallstNobelApplication.size()>0){
							for (Iterator it = finallstNobelApplication.iterator(); it.hasNext();) 					            
							 {
								String[] row = (String[]) it.next();
							  int index=0;
							 									
							  mainTable = new PdfPTable(tblwidth);
							  mainTable.setWidthPercentage(100);
							  para = new Paragraph[18];
							  cell = new PdfPCell[18];
							
                              String myVal1="";
                              String myVal2="";
                              
								 myVal1 = (row[0]==null)? "N/A" : row[0].toString();
								 para[index] = new Paragraph( row[0].toString(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
															 
								 myVal1 = (row[1]==null)? "N/A" : row[1].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? "N/A" : row[2].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 								 
								 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[4]==null)? "N/A" : row[4].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								
								 myVal1 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[11]==null)||(row[11].toString().length()==0))? " " : row[11].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[12]==null)||(row[12].toString().length()==0))? " " : row[12].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[13]==null)? "N/A" : row[13].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[14]==null)? "N/A" : row[14].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[15]==null)? "N/A" : row[15].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[16]==null)? "N/A" : row[16].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[17]==null)? "N/A" : row[17].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								

						document.add(mainTable);
				   }
				}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	
	public String displayRecordsByEntityTypePrintPreview(String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println("inside Print Preview");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
		
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"jobforteacherid";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<String[]> lstNobelApplication =new ArrayList<String[]>();
				
				
				if(entityID==2||entityID==3)
				{
					System.out.println("inside district print");
					lstNobelApplication =jobForTeacherDAO.noblestapplication(sortingcheck,sortOrderStrVal,start, noOfRowInPage,false,sortOrderFieldName);
			    totalRecord = lstNobelApplication.size();
			    	System.out.println("totallllllllllll records in print========="+totalRecord);
			    	 
				}
				else if(entityID==1) 
				{
					System.out.println("indside admin print ");
					
					lstNobelApplication =jobForTeacherDAO.noblestapplication(sortingcheck,sortOrderStrVal,start, noOfRowInPage,false,sortOrderFieldName);
				    	totalRecord = lstNobelApplication.size();
				    	System.out.println("totallllllllllll printtttttttt========="+totalRecord);
				
				}
										
					List<String[]> finallstNobelApplication =new ArrayList<String[]>();
				     if(totalRecord<end)
							end=totalRecord;
				     finallstNobelApplication=lstNobelApplication;
				
			

	        tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>Noble St Application</div><br/>");
			
		    	tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+"Created by: "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>Date of Print: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			    tmRecords.append("</div>");
			
			
			tmRecords.append("<table  id='tblGridHiredPrint' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Internal Record ID</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Internal Teacher ID</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Internal Job ID</th>");
    
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Title</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Posted Date</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Post End Date</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Category</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Job Subject</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Application Record Date</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Application Status</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>SLC Status</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Hired By School</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Have Cover Letter</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Is Affilated</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Last Activity on Application</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Last Activity Date</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Demo Date Scheduled</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Demo Date Cancelled</th>");
			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(finallstNobelApplication.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			}
		String sta="";
			if(finallstNobelApplication.size()>0){
				 for (Iterator it = finallstNobelApplication.iterator(); it.hasNext();) 					            
					{
					String[] row = (String[]) it.next();
					
					 String myVal1="";
                     String myVal2="";
					  
					 tmRecords.append("<tr>");
					 
					 myVal1 = (row[0]==null)? "N/A" : row[0].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
					 
						 myVal1 = (row[1]==null)? "N/A" : row[1].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = (row[2]==null)? "N/A" : row[2].toString();
							tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
														
							 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
								tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = ((row[4]==null)||(row[4].toString().length()==0))? "N/A" : row[4].toString();
			      		tmRecords.append("<td style='font-size:12px;'>"+ myVal1+"</td>");	
			      		
			      		 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString();
							tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
							
							 myVal1 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString();
								tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
								
								 myVal1 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString();
									tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
									
									 myVal1 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString();
										tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
										
										 myVal1 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString();
											tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
											
											 myVal1 = ((row[11]==null)||(row[11].toString().length()==0))? " " : row[11].toString();
												tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
												
												 myVal1 = ((row[12]==null)||(row[12].toString().length()==0))? " " : row[12].toString();
													tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
																										
													 myVal1 = (row[13]==null)? "N/A" : row[13].toString();
														tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
																												
														 myVal1 = (row[14]==null)? "N/A" : row[14].toString();
															tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
																														
															 myVal1 = (row[15]==null)? "N/A" : row[15].toString();
																tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
																
																 myVal1 = (row[16]==null)? "N/A" : row[16].toString();
																	tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
																																
																	 myVal1 = (row[17]==null)? "N/A" : row[17].toString();
																		tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
													
					   tmRecords.append("</tr>");
			   }
			}
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
  }
	 public String displayNobleApplicationCSV(String noOfRow,String pageNo,String sortOrder,String sortOrderType){
		 String basePath = null;
			
			String fileName = null;
		 WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=1;
			try{
				
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				//System.out.println("schoolMaster="+schoolMaster+"districtMaster="+districtMaster+"entityID="+entityID);
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position


				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
			 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"jobforteacherid";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
				
			/**end set dynamic sorting fieldName **/
                   List<String[]> lstNobelApplication =new ArrayList<String[]>();
				
				if(entityID==2||entityID==3)
				{
					lstNobelApplication =jobForTeacherDAO.noblestapplication(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
			    totalRecord = lstNobelApplication.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
			    	 
				}
				else if(entityID==1) 
				{
					System.out.println("indside admin ");
					
					lstNobelApplication =jobForTeacherDAO.noblestapplication(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
				    	totalRecord = lstNobelApplication.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
					
				
				}
				
				List<String[]> finallstNobelApplication =new ArrayList<String[]>();
			     if(totalRecord<end)
						end=totalRecord;
			     finallstNobelApplication=lstNobelApplication.subList(start,end);
			
			 
		   //Excel SmartFusion Exporting	

			     String time = String.valueOf(System.currentTimeMillis()).substring(6);
				
					 basePath = request.getSession().getServletContext().getRealPath ("/")+"/hiredapplicants";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="nobleapplication"+time+".csv";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

			 FileWriter writer = new FileWriter(file);
			 
		   //Delimiter used in CSV file
			final String COMMA_DELIMITER = ",";
			final String NEW_LINE_SEPARATOR = "\n";
			
			StringBuilder csvFileData = new StringBuilder();
			 
			csvFileData.append("Internal_Record_ID");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Internal_Teacher_ID");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Internal_Job_ID");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Job_Title");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Job_Posted_Date");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Job_Post_End_Date");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Job_Category");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Job_Subject");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Application_Record_date");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Application_Status");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Status_Life_Cycle_Status");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Hired_By_School");
			csvFileData.append(COMMA_DELIMITER);
		
			csvFileData.append("Have_Cover_Letter");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Is_Affilated");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Last_Activity_On_Application");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Last_Activity_Date");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Demo_Date_Schedule");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Demo_Date_Cancelled");
					 	

				if(lstNobelApplication!=null)
				{
				 if(lstNobelApplication.size()>0){
				 totalRecord =  lstNobelApplication.size();
				 }
				
				 System.out.println("totalRecord: "+totalRecord+" end: "+end+" start: "+start+" sortOrderFieldName: "+sortOrderFieldName);
				 if(totalRecord>end){
					 end =totalRecord;
					 finallstNobelApplication =lstNobelApplication.subList(start, end);
				}else{
					 end =totalRecord;
					 finallstNobelApplication =lstNobelApplication.subList(start, end);
				}
				
				
				if(finallstNobelApplication.size()==0){
					csvFileData.append(NEW_LINE_SEPARATOR);
					csvFileData.append("No Rcords Founds");
				}
			
				 if(finallstNobelApplication.size()>0){
					// Integer count=1;
	                   for (Iterator it = finallstNobelApplication.iterator(); it.hasNext();)
	                   {
	                	   String[] row = (String[]) it.next(); 

						
						
						String  myVal1="";
						
						   myVal1 = (row[0]==null)? "N/A" : row[0].toString();
						 csvFileData.append(NEW_LINE_SEPARATOR);
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						  myVal1 = (row[1]==null)? "N/A" : row[1].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[2]==null)? "N/A" : row[2].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[4]==null)? "N/A" : row[4].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[6]==null)? "N/A" : row[6].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[7]==null)? "N/A" : row[7].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[8]==null)? "N/A" : row[8].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[9]==null)? "N/A" : row[9].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[10]==null)? "N/A" : row[10].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[11]==null)? "N/A" : row[11].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[12]==null)? "N/A" : row[12].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[13]==null)? "N/A" : row[13].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[14]==null)? "N/A" : row[14].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[15]==null)? "N/A" : row[15].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[16]==null)? "N/A" : row[16].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[17]==null)? "N/A" : row[17].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						
						
						//count++;
					}
				}
			}else{
				
				csvFileData.append(NEW_LINE_SEPARATOR);
				csvFileData.append("No Rcords Founds");
					
			}
				  writer.append(csvFileData);
				  writer.flush();
				  writer.close();
				  
			}catch (Exception e) {
				e.printStackTrace();
			}
		  return fileName;
	  }	
	
	/*****************************/
	 public String displayNobleApplicationCSVRun(JobForTeacherDAO jobForTeacherDAO1,ServletContext servletContext){
		 
		 System.out.println("Method call from schudler jobForTeacherDAO1: "+jobForTeacherDAO1+" servletContext :"+servletContext);
		 String basePath = null;
		String fileName = null;
		try{
		
			/**end set dynamic sorting fieldName **/
                   List<String[]> lstNobelApplication =new ArrayList<String[]>();
				
                   System.out.println("jobForTeacherDAO1 : "+jobForTeacherDAO1);
					lstNobelApplication =jobForTeacherDAO1.noblestapplicationRun();
			  
			    	List<String[]> finallstNobelApplication =new ArrayList<String[]>();
			   
			       finallstNobelApplication=lstNobelApplication;//.subList(start,end);
			System.out.println("finallstNobelApplication===="+finallstNobelApplication.size());
			 
		   //Excel SmartFusion Exporting	 
			     
			 String time = String.valueOf(System.currentTimeMillis()).substring(6);
			// basePath = request.getSession().getServletContext().getRealPath("/")+"nobleApplicationReport";
			 
			basePath="D:/user";
			 fileName ="noblestapplication.csv";
			 
			 
			 /***************************creating directory inside  ***rootPath*******************************************/	
			    FileOutputStream outStream = null;
			    String jobFeedDir = Utility.getValueOfPropByKey("rootPath"); 
				System.out.println("jobFeedDir : "+jobFeedDir);	
				
				String sourceDirName = jobFeedDir+"//"+"NobleApplication"; 
				String targetFileName = sourceDirName+"/"+fileName; ;
				
				File fileFeedDir = new File(sourceDirName);
				
				File sourceFile= new File(targetFileName);
				System.out.println("ileFeedDir.exists() before : "+fileFeedDir.exists());
				
				if(!fileFeedDir.exists())
				{
					fileFeedDir.mkdir();
					outStream = new FileOutputStream(sourceFile);
					System.out.println("directory created succesffully");
				}
				if(sourceFile.exists())
				{
					outStream = new FileOutputStream(targetFileName);
					System.out.println("sourceFile.exists()writing Noble application report : "+sourceFile.exists());
				}
				
		/********************************************************************/	
			 
			/* 
			 File file = new File(basePath);
			 if(!file.exists()){
				 file.mkdir();
			 }*/
			 
			 Utility.deleteAllFileFromDir(sourceDirName);
			// file = new File(basePath+"/"+fileName);
			 
			 
			 FileWriter writer = new FileWriter(sourceFile);
			 
		   //Delimiter used in CSV file
			final String COMMA_DELIMITER = ",";
			final String NEW_LINE_SEPARATOR = "\n";
			
			StringBuilder csvFileData = new StringBuilder();
			 
			csvFileData.append("Internal_Record_ID");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Internal_Teacher_ID");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Internal_Job_ID");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Job_Title");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Job_Posted_Date");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Job_Post_End_Date");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Job_Category");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Job_Subject");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Application_Record_date");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Application_Status");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Status_Life_Cycle_Status");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Hired_By_School");
			csvFileData.append(COMMA_DELIMITER);
		
			csvFileData.append("Have_Cover_Letter");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Is_Affilated");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Last_Activity_On_Application");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Last_Activity_Date");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Demo_Date_Schedule");
			csvFileData.append(COMMA_DELIMITER);
			
			csvFileData.append("Demo_Date_Cancelled");
					 	

				if(lstNobelApplication!=null)
				{
							
				if(finallstNobelApplication.size()==0){
					csvFileData.append(NEW_LINE_SEPARATOR);
					csvFileData.append("No Rcords Founds");
				}
			
				 if(finallstNobelApplication.size()>0){
					// Integer count=1;
	                   for (Iterator it = finallstNobelApplication.iterator(); it.hasNext();)
	                   {
	                	   String[] row = (String[]) it.next(); 

						
						
						String  myVal1="";
						
						   myVal1 = (row[0]==null)? "N/A" : row[0].toString();
						 csvFileData.append(NEW_LINE_SEPARATOR);
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						  myVal1 = (row[1]==null)? "N/A" : row[1].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[2]==null)? "N/A" : row[2].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[4]==null)? "N/A" : row[4].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[6]==null)? "N/A" : row[6].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[7]==null)? "N/A" : row[7].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[8]==null)? "N/A" : row[8].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[9]==null)? "N/A" : row[9].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[10]==null)? "N/A" : row[10].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[11]==null)? "N/A" : row[11].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[12]==null)? "N/A" : row[12].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[13]==null)? "N/A" : row[13].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[14]==null)? "N/A" : row[14].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[15]==null)? "N/A" : row[15].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[16]==null)? "N/A" : row[16].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[17]==null)? "N/A" : row[17].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						
						
						//count++;
					}
				}
			}else{
				
				csvFileData.append(NEW_LINE_SEPARATOR);
				csvFileData.append("No Rcords Founds");
					
			}
				  writer.append(csvFileData);
				  writer.flush();
				  writer.close();
				  
				  writeNobleReportONServer(servletContext);
				  
			}catch (Exception e) {
				e.printStackTrace();
			}
		  return basePath+"/"+fileName;
	  }	
	 /*****************************/
	
	 
	 public void writeNobleReportONServer(ServletContext servletContext){
		  System.out.println("************writing Noble application  on server inside ******************* servletContext: "+servletContext);
		    try{
		     String path="";
		   
		     String source = Utility.getValueOfPropByKey("rootPath")+"NobleApplication/noblestapplication.csv";
			 System.out.println("source Path "+source);

			 String target = servletContext.getRealPath("/")+"/"+"/NobleApplication/";
			 System.out.println("Real target Path "+target);

			File sourceFile = new File(source);
			File targetDir = new File(target);
			
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());

			try {
				FileUtils.copyFile(sourceFile, targetFile);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			path = Utility.getValueOfPropByKey("contextBasePath")+"/NobleApplication/noblestapplication.csv";
			System.out.println("NobleApplication contextBasePath "+path);
		  
			
			FTPConnectAndLogin.dropFileOnFTPServer(target+"noblestapplication.csv");
			
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
			
		}
	 
	
}


