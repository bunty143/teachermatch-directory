package tm.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobCategoryTransaction;
import tm.bean.master.CertificationStatusMaster;
import tm.bean.master.CertificationTypeMaster;
import tm.bean.master.CityMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.DegreeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioOptions;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.bean.master.DspqFieldMaster;
import tm.bean.master.DspqGroupMaster;
import tm.bean.master.DspqPortfolioName;
import tm.bean.master.DspqRouter;
import tm.bean.master.DspqSectionMaster;
import tm.bean.master.EmpRoleTypeMaster;
import tm.bean.master.FieldMaster;
import tm.bean.master.FieldOfStudyMaster;
import tm.bean.master.GenderMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.OrgTypeMaster;
import tm.bean.master.PeopleRangeMaster;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.master.RaceMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SpokenLanguageMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.TFAAffiliateMaster;
import tm.bean.master.TFARegionMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobCategoryTransactionDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.master.CertificationStatusMasterDAO;
import tm.dao.master.CertificationTypeMasterDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.DegreeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificPortfolioOptionsDAO;
import tm.dao.master.DistrictSpecificPortfolioQuestionsDAO;
import tm.dao.master.DspqFieldMasterDAO;
import tm.dao.master.DspqGroupMasterDAO;
import tm.dao.master.DspqPortfolioNameDAO;
import tm.dao.master.DspqRouterDAO;
import tm.dao.master.DspqSectionMasterDAO;
import tm.dao.master.EmpRoleTypeMasterDAO;
import tm.dao.master.FieldMasterDAO;
import tm.dao.master.FieldOfStudyMasterDAO;
import tm.dao.master.GenderMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.OrgTypeMasterDAO;
import tm.dao.master.PeopleRangeMasterDAO;
import tm.dao.master.QuestionTypeMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SpokenLanguageMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.TFAAffiliateMasterDAO;
import tm.dao.master.TFARegionMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.Utility;

public class ManageDspqAjax {

	@Autowired
	RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	@Autowired
	DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private DistrictSpecificPortfolioQuestionsDAO districtSpecificPortfolioQuestionsDAO;
	
	@Autowired
	private DspqGroupMasterDAO dspqGroupMasterDAO;
	
	@Autowired 
	private DspqSectionMasterDAO dspqSectionMasterDAO;
	
	@Autowired 
	private DspqFieldMasterDAO dspqFieldMasterDAO;
	
	@Autowired 
	private DspqPortfolioNameDAO dspqPortfolioNameDAO;	
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private DspqRouterDAO dspqRouterDAO;
	
	@Autowired
	private DistrictSpecificPortfolioOptionsDAO districtSpecificPortfolioOptionsDAO;
	
	@Autowired
	private QuestionTypeMasterDAO questionTypeMasterDAO;
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private GenderMasterDAO genderMasterDAO;
	
	@Autowired
	private CountryMasterDAO countryMasterDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	
	@Autowired
	private DegreeMasterDAO degreeMasterDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	private FieldOfStudyMasterDAO fieldOfStudyMasterDAO;
	
	@Autowired
	private CertificationStatusMasterDAO certificationStatusMasterDAO;
	
	@Autowired
	private CertificationTypeMasterDAO certificationTypeMasterDAO;
	
	@Autowired
	private FieldMasterDAO fieldMasterDAO;
	
	@Autowired
	private EmpRoleTypeMasterDAO empRoleTypeMasterDAO;
	
	@Autowired
	private OrgTypeMasterDAO orgTypeMasterDAO;
	
	@Autowired
	private PeopleRangeMasterDAO peopleRangeMasterDAO;
	
	@Autowired
	private SpokenLanguageMasterDAO spokenLanguageMasterDAO;
	
	@Autowired
	private TFAAffiliateMasterDAO tfaAffiliateMasterDAO;
	
	@Autowired
	private TFARegionMasterDAO tfaRegionMasterDAO ;	
	
	@Autowired
	private JobCategoryTransactionDAO jobCategoryTransactionDAO ;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO ;
	
	String locale = Utility.getValueOfPropByKey("locale");
	public String displayPortfolioGrid(Integer districtId, String noOfRow, String pageNo)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			
			UserMaster userMaster=null;
			userMaster=(UserMaster)session.getAttribute("userMaster");
			List<DspqPortfolioName> dspqPortfolioNamesAll =null;
			List<DspqPortfolioName> dspqPortfolioNamesList =null;
			DistrictMaster districtMaster=userMaster.getDistrictId();
			Order  sortOrderStrVal		=	null;
			sortOrderStrVal=Order.asc("portfolioName");
			Criterion criterion=null;
			if(districtId!=null){
				if(districtId==0)
					districtId=null;
			}
			if(userMaster.getEntityType()==1)
			{
				if(districtId!=null && districtId!=0)
				{
					districtMaster=null;
					districtMaster=districtMasterDAO.findById(districtId, false, false);
					session.setAttribute("districtMasterId", districtMaster.getDistrictId());
					session.setAttribute("districtMasterName", districtMaster.getDistrictName());
					criterion=Restrictions.eq("districtMaster", districtMaster);
					Criterion criterionDistrict=Restrictions.isNull("districtMaster");
			        Criterion criterionStatus=Restrictions.eq("status", "A");
			        Criterion criterion2=Restrictions.and(criterionDistrict, criterionStatus);
					Criterion orCriteria = Restrictions.or(Restrictions.eq("districtMaster", districtMaster), criterion2);

					
					dspqPortfolioNamesList=dspqPortfolioNameDAO.findPortfolioList(sortOrderStrVal, start, noOfRowInPage,orCriteria);
					dspqPortfolioNamesAll=dspqPortfolioNameDAO.getDspqPortfolioNameByDistrictMaster(districtMaster);
					/*dspqPortfolioNamesList=dspqPortfolioNameDAO.findPortfolioList(sortOrderStrVal, start, noOfRowInPage,criterion);
					dspqPortfolioNamesAll=dspqPortfolioNameDAO.findPortfolioNameByDistrict(districtMaster);*/
				}
				else	
				{
					session.setAttribute("districtMasterId", 0);
					session.setAttribute("districtMasterName", "");
					criterion=Restrictions.isNotNull("status");				
					dspqPortfolioNamesList=dspqPortfolioNameDAO.findPortfolioList(sortOrderStrVal, start, noOfRowInPage,criterion);
			
					dspqPortfolioNamesAll=dspqPortfolioNameDAO.findAll();
				}
			}
			else
			{
				Criterion criterionDistrict=Restrictions.isNull("districtMaster");
		        Criterion criterionStatus=Restrictions.eq("status", "A");
		        Criterion criterion2=Restrictions.and(criterionDistrict, criterionStatus);
				Criterion orCriteria = Restrictions.or(Restrictions.eq("districtMaster", districtMaster), criterion2);

				
				dspqPortfolioNamesList=dspqPortfolioNameDAO.findPortfolioList(sortOrderStrVal, start, noOfRowInPage,orCriteria);
				dspqPortfolioNamesAll=dspqPortfolioNameDAO.getDspqPortfolioNameByDistrictMaster(districtMaster);
			}
			totalRecord=dspqPortfolioNamesAll.size();
		
			dmRecords.append("<div class='row' style='background-color:#007fb2; color:white; padding:7px 5px 7px 5px; width:100%; margin-left:0%;'>");
			if(userMaster.getEntityType()==1 && districtId==null){
				dmRecords.append("<div class='col-sm-3 col-md-3'>Candidate Portfolio Name</div>");
				dmRecords.append("<div class='col-sm-3 col-md-3'>District Name</div>");
			}
			else
				dmRecords.append("<div class='col-sm-6 col-md-6'>Candidate Portfolio Name</div>");
			dmRecords.append("<div class='col-sm-2 col-md-2'>TM Default</div>");
			
			//dmRecords.append("<div class='col-sm-5 col-md-5'>Applied At</div>");
			dmRecords.append("<div class='col-sm-2 col-md-2'>Activation Date</div>");
			dmRecords.append("<div class='col-sm-1 col-md-1'>Status</div>");
			dmRecords.append("<div class='col-sm-1 col-md-1' style='text-align:left; padding:0px;'>Actions</div>");
			dmRecords.append("</div>");
		if(dspqPortfolioNamesList.size()<=0 || dspqPortfolioNamesList.equals(null))
		{
			dmRecords.append("<div class='row'>");
			dmRecords.append("<div class='col-sm-8 col-md-8 left5'>No Portfolio found</div>");
			dmRecords.append("</div>");
		}		
		int colorCounter=0,count=0;
		String colorName="";
		for (DspqPortfolioName dspqPortfolioName : dspqPortfolioNamesList) 
		{
			if(colorCounter==0){
				colorName="white";
				colorCounter=1;
			}else{
				colorName="#dff0d8";
				colorCounter=0;
			}
			dmRecords.append("<div id='divId"+(count++)+"' class='row' style='width:100%;background-color:"+colorName+"; padding:2px 5px 2px 5px; margin-left:0px;' onmouseover='onHoverHighLight(this);' onmouseout='onOutHighLight(this);'>");				
			
			if(userMaster.getEntityType()==1 && districtId==null){
				dmRecords.append("<div class='col-sm-3 col-md-3'>"+dspqPortfolioName.getPortfolioName()+"</div>");
				if(dspqPortfolioName.getDistrictMaster()!=null)
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+dspqPortfolioName.getDistrictMaster().getDistrictName()+"</div>");
				else
					dmRecords.append("<div class='col-sm-3 col-md-3'></div>");
			}
			else
				dmRecords.append("<div class='col-sm-6 col-md-6'>"+dspqPortfolioName.getPortfolioName()+"</div>");			
			if(dspqPortfolioName.getDistrictMaster()==null)
				dmRecords.append("<div class='col-sm-2 col-md-2'>TM Default</div>");
			else
				dmRecords.append("<div class='col-sm-2 col-md-2'></div>");
						
			
			dmRecords.append("<div class='col-sm-2 col-md-2'>"+Utility.convertDateAndTimeToUSformatOnlyDate(dspqPortfolioName.getCreatedDateTime())+"</div>");
			
			dmRecords.append("<div class='col-sm-1 col-md-1'>");
			if(dspqPortfolioName.getStatus().equalsIgnoreCase("A"))
				dmRecords.append("Active");
			else
				dmRecords.append("Inactive");
			dmRecords.append("</div>");

			dmRecords.append("<div class='col-sm-1 col-md-1' style='text-align:left; padding:0px;'>");
			
			if(dspqPortfolioName.getDistrictMaster()!=null){
				dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Edit' onclick=\"return editDspq("+dspqPortfolioName.getDspqPortfolioNameId()+",2,"+dspqPortfolioName.getDistrictMaster().getDistrictId()+",'"+dspqPortfolioName.getDistrictMaster().getDistrictName().replace("'", "&#")+"');\"><i class='fa fa-pencil-square-o fa-lg'></i></a>&nbsp;&nbsp;");
			
				if(dspqPortfolioName.getStatus().equalsIgnoreCase("A")){
					if(dspqPortfolioName.getDistrictMaster()!=null)
						dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Deactivate' onclick=\"return activateDeactivateDspq("+dspqPortfolioName.getDistrictMaster().getDistrictId()+","+dspqPortfolioName.getDspqPortfolioNameId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>&nbsp;&nbsp;");
					else
						dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Deactivate' onclick=\"return activateDeactivateDspq(0,"+dspqPortfolioName.getDspqPortfolioNameId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>&nbsp;&nbsp;");
				}else{
					if(dspqPortfolioName.getDistrictMaster()!=null)
						dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Activate' onclick=\"return activateDeactivateDspq("+dspqPortfolioName.getDistrictMaster().getDistrictId()+","+dspqPortfolioName.getDspqPortfolioNameId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>&nbsp;&nbsp;");
					else
						dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Activate' onclick=\"return activateDeactivateDspq(0,"+dspqPortfolioName.getDspqPortfolioNameId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>&nbsp;&nbsp;");
				}
				dmRecords.append("<a href='javascript:void(0);'  data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Clone' onclick=\"return editDspq("+dspqPortfolioName.getDspqPortfolioNameId()+",1,"+dspqPortfolioName.getDistrictMaster().getDistrictId()+",'"+dspqPortfolioName.getDistrictMaster().getDistrictName().replace("'", "&#")+"');\"><i class='fa fa-files-o fa-lg'></i></a>");
			}
			else{
				if(userMaster.getEntityType()==1 && districtId==null){
				dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Edit' onclick=\"return editDspq("+dspqPortfolioName.getDspqPortfolioNameId()+",2,"+dspqPortfolioName.getDistrictMaster()+",'"+dspqPortfolioName.getDistrictMaster()+"');\"><i class='fa fa-pencil-square-o fa-lg'></i></a>&nbsp;&nbsp;");
				
				if(dspqPortfolioName.getStatus().equalsIgnoreCase("A")){
					if(dspqPortfolioName.getDistrictMaster()!=null)
						dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Deactivate' onclick=\"return activateDeactivateDspq("+dspqPortfolioName.getDistrictMaster().getDistrictId()+","+dspqPortfolioName.getDspqPortfolioNameId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>&nbsp;&nbsp;");
					else
						dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Deactivate' onclick=\"return activateDeactivateDspq(0,"+dspqPortfolioName.getDspqPortfolioNameId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>&nbsp;&nbsp;");
				}else{
					if(dspqPortfolioName.getDistrictMaster()!=null)
						dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Activate' onclick=\"return activateDeactivateDspq("+dspqPortfolioName.getDistrictMaster().getDistrictId()+","+dspqPortfolioName.getDspqPortfolioNameId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>&nbsp;&nbsp;");
					else
						dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Activate' onclick=\"return activateDeactivateDspq(0,"+dspqPortfolioName.getDspqPortfolioNameId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>&nbsp;&nbsp;");
				}
				dmRecords.append("<a href='javascript:void(0);'  data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Clone' onclick=\"return editDspq("+dspqPortfolioName.getDspqPortfolioNameId()+",1,"+dspqPortfolioName.getDistrictMaster()+",'"+dspqPortfolioName.getDistrictMaster()+"');\"><i class='fa fa-files-o fa-lg'></i></a>");
				}
				else{
					//dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Edit' onclick=\"return editDspq("+dspqPortfolioName.getDspqPortfolioNameId()+",2,"+dspqPortfolioName.getDistrictMaster()+",'"+dspqPortfolioName.getDistrictMaster()+"');\"><i class='fa fa-pencil-square-o fa-lg'></i></a>&nbsp;&nbsp;");
					
					if(dspqPortfolioName.getStatus().equalsIgnoreCase("A"))
						dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Activate' onclick=\"return createDefaultPortfolioConfirmation("+dspqPortfolioName.getDspqPortfolioNameId()+")\"><i class='fa fa-check fa-lg'></i></a>&nbsp;&nbsp;");
					//else
						//dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Activate' onclick=\"return activateDeactivateDspq("+dspqPortfolioName.getDspqPortfolioNameId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>&nbsp;&nbsp;");
				
					//dmRecords.append("<a href='javascript:void(0);'  data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Clone' onclick=\"return editDspq("+dspqPortfolioName.getDspqPortfolioNameId()+",1,"+dspqPortfolioName.getDistrictMaster()+",'"+dspqPortfolioName.getDistrictMaster()+"');\"><i class='fa fa-files-o fa-lg'></i></a>");
					
				}
			}
				dmRecords.append("</div>");

			dmRecords.append("</div>");
			dmRecords.append("<div class='row' style='width:100%;margin-left:1px;'>");
	          dmRecords.append("<div class='col-sm-12 col-md-12' style='margin-left:6px;'></div>");
	          dmRecords.append("</div>");
		}
		
		dmRecords.append(PaginationAndSorting.getPaginationStringForManageDSPQAjax(request,totalRecord,noOfRow, pageNo));
	}catch (Exception e) 
	{
		e.printStackTrace();
	}
	return dmRecords.toString();
	}

	public String getDspqAccordian(String groupId,Integer portfolioNameId,Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		
		boolean bEEOCEnable=false;
		if(request.getServerName().contains("canada-en-test.teachermatch.org"))
			bEEOCEnable=true;
		
		StringBuffer dmRecords = new StringBuffer();
		DistrictMaster districtMaster = null;
		try{
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			else{
				userMaster =	(UserMaster) session.getAttribute("userMaster");
				System.out.println("districtId" +districtId);
				if(districtId!=null)
					districtMaster=districtMasterDAO.findById(districtId, false, false);
			}
		
			String districtName1="";
			String stateName1="";
			if(districtMaster != null){
				districtName1=districtMaster.getDistrictName();
				stateName1=districtMaster.getStateId().getStateName();
			}
			List<DspqSectionMaster> dspqMasterList = null;
			List<DspqSectionMaster> dspqMasterListByOrder = null;
			List<DspqFieldMaster> dspqFieldMasters = null;
			List<DspqFieldMaster> dspqFieldMastersAll=null;
			Map<Integer,List<DspqFieldMaster>> sectionIds = new HashMap<Integer,List<DspqFieldMaster>>();
			
			DspqGroupMaster dspqGroupMaster = null;
			if(!groupId.equals("")){
				dspqGroupMaster= dspqGroupMasterDAO.findById(Integer.parseInt(groupId), false, false);
			}			
			StringBuilder sectionList=new StringBuilder();
			StringBuilder fieldList=new StringBuilder();
			
			try {
				dspqMasterList= dspqSectionMasterDAO.getDspqSectionListByGroup(dspqGroupMaster);
				dspqMasterListByOrder= dspqSectionMasterDAO.getDspqSectionListByGroupByOrder(dspqGroupMaster);
				
				for(DspqSectionMaster dspqSection : dspqMasterList){
					sectionList.append(","+dspqSection.getSectionId());
				}
				System.out.println("sectionList "+ sectionList.toString());				
				
				if(dspqMasterList!=null && dspqMasterList.size()>0)
					dspqFieldMasters	 = dspqFieldMasterDAO.getDspqFieldListBySections(dspqMasterList);
				dspqFieldMastersAll = dspqFieldMasterDAO.getDspqAllFieldListBySections(dspqMasterList);				
				
				Map<Integer,List<DspqFieldMaster>> SecMap = new HashMap<Integer,List<DspqFieldMaster>>();
				
				try {	
					if(dspqFieldMastersAll!=null && dspqFieldMastersAll.size()>0){
						List<DspqFieldMaster> tempList = null;

						for(DspqFieldMaster dspqAdditionalFields : dspqFieldMastersAll){
							
							if(SecMap==null || SecMap.size()==0){
								tempList = new ArrayList<DspqFieldMaster>();
								tempList.add(dspqAdditionalFields);
								SecMap.put(dspqAdditionalFields.getDspqSectionMaster().getSectionId(), tempList);
							}
							else
							{
								tempList = new ArrayList<DspqFieldMaster>();;
								tempList = SecMap.get(dspqAdditionalFields.getDspqSectionMaster().getSectionId());
								if(tempList==null){
									tempList = new ArrayList<DspqFieldMaster>();
									tempList.add(dspqAdditionalFields);
								}
								else{
									tempList.add(dspqAdditionalFields);
								}
								SecMap.put(dspqAdditionalFields.getDspqSectionMaster().getSectionId(), tempList);							
							}
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
				Boolean r=false;
				for(DspqSectionMaster dspqSectionMaster : dspqMasterList){					
					List<DspqFieldMaster> dspqFieldLi=null;
					dspqFieldLi= SecMap.get(dspqSectionMaster.getSectionId());
					 r=false;
					if(dspqFieldLi!=null && dspqFieldLi.size()!=0)
					{
						fieldList.append("##,");
						for(DspqFieldMaster dspqFieldMaster : dspqFieldLi)
						{
							if(r==false)
								fieldList.append(dspqFieldMaster.getDspqFieldId());
							else
								fieldList.append(","+dspqFieldMaster.getDspqFieldId());
							
							r=true;
						}			
					}
					else
					{
						fieldList.append("##,0");						
					}
				}
				System.out.println("fieldList " +fieldList.toString());				
			
				if(dspqFieldMasters!=null && dspqFieldMasters.size()>0){
					List<DspqFieldMaster> tempList = null;

					for(DspqFieldMaster dspqFields : dspqFieldMasters){
						
						if(sectionIds==null || sectionIds.size()==0){
							tempList = new ArrayList<DspqFieldMaster>();
							tempList.add(dspqFields);
							sectionIds.put(dspqFields.getDspqSectionMaster().getSectionId(), tempList);
						}
						else
						{
							tempList = new ArrayList<DspqFieldMaster>();;
							tempList = sectionIds.get(dspqFields.getDspqSectionMaster().getSectionId());
							if(tempList==null){
								tempList = new ArrayList<DspqFieldMaster>();
								tempList.add(dspqFields);
							}
							else{
								tempList.add(dspqFields);
							}
							sectionIds.put(dspqFields.getDspqSectionMaster().getSectionId(), tempList);							
						}
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			//TM Additional Fields
			Map<Integer,List<DspqFieldMaster>> sectionAdditionalIds = new HashMap<Integer,List<DspqFieldMaster>>();
			List<DspqFieldMaster> dspqAdditionalFieldMaster = null;
			try {
				
				if(dspqMasterList!=null && dspqMasterList.size()>0)
					dspqAdditionalFieldMaster	 = dspqFieldMasterDAO.getDspqAdditionalFieldListBySections(dspqMasterList);
				
				
				if(dspqAdditionalFieldMaster!=null && dspqAdditionalFieldMaster.size()>0){
					List<DspqFieldMaster> tempList = null;

					for(DspqFieldMaster dspqAdditionalFields : dspqAdditionalFieldMaster){
						
						if(sectionAdditionalIds==null || sectionAdditionalIds.size()==0){
							tempList = new ArrayList<DspqFieldMaster>();
							tempList.add(dspqAdditionalFields);
							sectionAdditionalIds.put(dspqAdditionalFields.getDspqSectionMaster().getSectionId(), tempList);
						}
						else
						{
							tempList = new ArrayList<DspqFieldMaster>();;
							tempList = sectionAdditionalIds.get(dspqAdditionalFields.getDspqSectionMaster().getSectionId());
							if(tempList==null){
								tempList = new ArrayList<DspqFieldMaster>();
								tempList.add(dspqAdditionalFields);
							}
							else{
								tempList.add(dspqAdditionalFields);
							}
							sectionAdditionalIds.put(dspqAdditionalFields.getDspqSectionMaster().getSectionId(), tempList);							
						}
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			// TM Custom Fields
			List<DistrictSpecificPortfolioQuestions> dspqQuestionList=null;
			List<DspqRouter> dspqRouterList=null;
			DspqPortfolioName dspqPortfolioName=null;	
			if(portfolioNameId!=null && portfolioNameId!=0)
			{
				dspqPortfolioName=dspqPortfolioNameDAO.findById(portfolioNameId, false, false);
				dspqQuestionList = districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestionBYPortfolioNameId(dspqPortfolioName);
				dspqRouterList=dspqRouterDAO.getDspqFieldListByDspqPortfolioNameId(dspqPortfolioName);
			}
			//List<DistrictSpecificPortfolioQuestions> dspqQuestionList = districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestionBYDistrictByAD(districtMaster,jobCategoryMaster);
			Map<Integer,List<DistrictSpecificPortfolioQuestions>> dspqQuestionsMap = new HashMap<Integer,List<DistrictSpecificPortfolioQuestions>>();
			try{
				if(dspqQuestionList!=null && dspqQuestionList.size()>0){
					List<DistrictSpecificPortfolioQuestions> tempQuesList = null;

					for(DistrictSpecificPortfolioQuestions dspqQuestion : dspqQuestionList){
						
						if(dspqQuestionsMap==null || dspqQuestionsMap.size()==0){
							tempQuesList = new ArrayList<DistrictSpecificPortfolioQuestions>();
							tempQuesList.add(dspqQuestion);
							dspqQuestionsMap.put(dspqQuestion.getDspqSectionMaster().getSectionId(), tempQuesList);
						}
						else
						{
							tempQuesList = new ArrayList<DistrictSpecificPortfolioQuestions>();
							tempQuesList = dspqQuestionsMap.get(dspqQuestion.getDspqSectionMaster().getSectionId());
							if(tempQuesList==null){
								tempQuesList = new ArrayList<DistrictSpecificPortfolioQuestions>();
								tempQuesList.add(dspqQuestion);
							}
							else{
								tempQuesList.add(dspqQuestion);
							}
							dspqQuestionsMap.put(dspqQuestion.getDspqSectionMaster().getSectionId(), tempQuesList);							
						}
					}
				}
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			int count = 0;
			String str ="";
			StringBuffer sb = new StringBuffer();
			StringBuilder TMAddFieldList=new StringBuilder();
			if(dspqMasterList!=null && dspqMasterList.size()>0)
			{	
				dmRecords.append("<div class='row'><div class='row'><div class='col-sm-12 col-md-12 mt10' style='padding-left: 25px;'> <img src='images/Experiencedpsq1.png'/>&nbsp;&nbsp;&nbsp;<label><b>INSTRUCTIONS</b><label></div></div>");
				dmRecords.append("<div class='row' style='margin-left: 0px;margin-right: 40px;'><div class='col-sm-12 col-md-12'><hr style='border-width: 1px;margin-bottom: 0px;margin-top: 6px;'></div></div><div style='margin-left:12px;'>");
				dmRecords.append("<div class='row' style='width:99%; font-size:11px;  padding-right:10px; padding-left:13px;'>This is the "+dspqGroupMaster.getGroupName()+" section of the candidate portfolio. Below are the configurations settings for  <b>�external�</b> candidates and by default, the <b>�internal�</b> and <b>�internal transfer�</b> candidates also share the same settings. If you would like to have different settings for <b>�internal�</b> and <b>�internal transfer�</b> candidates, please select the <b>�internal�</b> and/or <b>�internal transfer�</b> candidate type checkboxes below to do so as that will enable different settings for those candidate types.</div>");
				
				dmRecords.append("<div class='row top10'>");
				dmRecords.append("<div class='col-sm-2 col-md-2' style='height: 41.5px;margin-left: -3px;'><label style='margin-top: 10px;font-weight:bold;'>Candidate Type<label></div>");
				
				dmRecords.append("<div class='col-sm-10 col-md-10' style='width: 79%;'><div class='row'>");
				dmRecords.append("<div class='col-sm-4 col-md-4 checkbox' ><label class='checkbox inline' style='margin-top: 2px;margin-bottom: 2px;margin-left: -18px;' title='External'><input type='checkbox' id='sectionExternal' checked disabled onclick='hideOrShowApplicant()' style='margin-left:-8px;'>External</label><span href='#' id='tool1' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='External'> <img src='images/qua-icon.png' alt=''></span></div>");
				dmRecords.append("<div class='col-sm-4 col-md-4 checkbox' style='margin-top: 10px;'><label class='checkbox inline' style='margin-top: 2px;margin-bottom: 2px;margin-left: -25px;' title='Internal'><input type='checkbox' id='sectionInternal'  onclick='hideOrShowApplicant()'>Internal</label> <span href='#' id='tool1' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Internal'> <img src='images/qua-icon.png' alt=''></span></div>");
				dmRecords.append("<div class='col-sm-4 col-md-4 checkbox' style='margin-top: 10px;'><label class='checkbox inline' style='margin-top: 2px;margin-bottom: 2px;margin-left: -25px;' title='Internal Transfer'><input type='checkbox' id='sectionITransfer' onclick='hideOrShowApplicant()'>Internal Transfer</label> <span href='#' id='tool1' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Internal Transfer'> <img src='images/qua-icon.png' alt=''></span></div>");
				dmRecords.append("</div> </div>");
				dmRecords.append("</div>");			
			
				
				dmRecords.append("<div class='row' style='margin-bottom:10px;'>");
				dmRecords.append("<div class='col-sm-12 col-md-12'>Please select the subsections that are needed for this candidate portfolio");
				dmRecords.append("<span href='#' id='tool1' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Please select the subsections that are needed for this candidate portfolio'>	<img src='images/qua-icon.png' alt=''></span>");
				dmRecords.append("</div></div>");
				
				
				
				List<DspqFieldMaster> dspqFields = null;
				List<DspqFieldMaster> dspqAdditionalFields = null;
				
				
				int counterId=1;
				System.out.println("dspqMasterList "+dspqMasterList.size());
				boolean divOpen = false;
				for(DspqSectionMaster dspqSec : dspqMasterListByOrder)
				{
					String sKeyValue = propertyKey(dspqSec.getSectionName(),dspqSec.getSectionId(),"Section");
					//dmRecords.append("<input type='hidden' id='sectionIsGrid_"+dspqSec.getSectionId()+"'  value='"+dspqSec.getIsGrid()+"'>");
					if(counterId==1 || (!divOpen)){
						divOpen = true;
						dmRecords.append("<div class='row left1'>");						
					}
					if(dspqSec.getSectionId()==25)
						dmRecords.append("<div class='col-sm-3 col-md-3 checkbox hide' style='margin-top: 0px;'><input type='checkbox' checked  id='mainSection"+dspqSec.getSectionId()+"' onclick='hideOrShowMainSubsection("+dspqSec.getSectionId()+")' title='"+dspqSec.getSectionName()+"'>"+dspqSec.getSectionName()+" <span href='#' id='tool1' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+sKeyValue+"'> <img src='images/qua-icon.png' alt=''></span></div>");
					else if(dspqSec.getSectionId()==3)
					{
						if(bEEOCEnable)
							dmRecords.append("<div class='col-sm-3 col-md-3 checkbox' style='margin-top: 0px;'><input type='checkbox' name='topSection' id='mainSection"+dspqSec.getSectionId()+"' onclick='hideOrShowMainSubsection("+dspqSec.getSectionId()+")' title='"+dspqSec.getSectionName()+"'>"+dspqSec.getSectionName()+" <span href='#' id='tool1' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+sKeyValue+"'> <img src='images/qua-icon.png' alt=''></span></div>");
						else
							dmRecords.append("<div class='col-sm-3 col-md-3 checkbox' style='margin-top: 0px;'><input type='checkbox' name='topSection' disabled checked id='mainSection"+dspqSec.getSectionId()+"' onclick='hideOrShowMainSubsection("+dspqSec.getSectionId()+")' title='"+dspqSec.getSectionName()+"'>"+dspqSec.getSectionName()+" <span href='#' id='tool1' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+sKeyValue+"'> <img src='images/qua-icon.png' alt=''></span></div>");
					}
					else if(dspqSec.getSectionId()==29)
						dmRecords.append("<div class='col-sm-3 col-md-3 checkbox' style='margin-top: 0px;'><input type='checkbox' name='topSection' checked id='mainSection"+dspqSec.getSectionId()+"' onclick='hideOrShowMainSubsection("+dspqSec.getSectionId()+")' title='"+dspqSec.getSectionName()+"'>"+dspqSec.getSectionName()+" <span href='#' id='tool1' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+sKeyValue+"'> <img src='images/qua-icon.png' alt=''></span></div>");
					else
						dmRecords.append("<div class='col-sm-3 col-md-3 checkbox' style='margin-top: 0px;'><input type='checkbox' name='topSection' id='mainSection"+dspqSec.getSectionId()+"' onclick='hideOrShowMainSubsection("+dspqSec.getSectionId()+")' title='"+dspqSec.getSectionName()+"'>"+dspqSec.getSectionName()+" <span href='#' id='tool1' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+sKeyValue+"'> <img src='images/qua-icon.png' alt=''></span></div>");
					if(counterId%4==0){
						divOpen = false;
						dmRecords.append("</div>");
					}else if(counterId== dspqMasterList.size()){
						divOpen = false;
						dmRecords.append("</div>");
					}
					counterId++;					
				}
				dmRecords.append("<div class='row top10' style='margin-bottom:10px;'>");
				dmRecords.append("<div class='col-sm-12 col-md-12'><label class='circleInsHead' style='margin-left:0px;'>I</label> INSTRUCTIONS <label class='circleInsHead iconLabel'>L</label> LABEL NAME <label class='circleInsHead helper'>?</label> HELP TEXT <label class='circleInsHead iconRequired'>R</label> REQUIRED <label class='circleInsHead iconOptional'>O</label> OPTIONAL <label class='circleInsHead activeIcon'>A</label>  ACTIVE <label class='circleInsHead inactiveIcon'>X</label> INACTIVE");
				
				dmRecords.append("</div></div>");
				
				dmRecords.append("<div class='panel-group' id='accordion1' style='margin-top:5px;margin-bottom: 15px;'>");
				
				//*****************************Start For Section Ordering ******************************************
				
				Map<Integer, DspqSectionMaster> mapSec=new HashMap<Integer, DspqSectionMaster>();
				for(DspqSectionMaster dspqSec : dspqMasterList)
				{
					mapSec.put(dspqSec.getSectionId(), dspqSec);
				}
				DspqSectionMaster[] dspqSectionMastersList=new DspqSectionMaster[dspqMasterList.size()];
				Map<Integer,Integer> mapValue=new HashMap<Integer, Integer>();
				List<DspqRouter> dspqRouterssection= new ArrayList<DspqRouter>();
				if(dspqPortfolioName!=null)
					dspqRouterssection=dspqRouterDAO.getDspqSectionListByPortfolioId(dspqPortfolioName);
				String sectionOrderArray="";
				int count1=0;
				if(dspqRouterssection.size()==0){
					for(int i=0;i<dspqMasterList.size();i++){
						dspqSectionMastersList[i] = dspqMasterList.get(i);
						if(portfolioNameId==null && dspqMasterList.get(i).getSectionId()==3){
							if(count1==0)
								sectionOrderArray="3";
							
						}
					}
					
				}else{
					for(int i=0,k=0;i<dspqRouterssection.size();i++){
						
						for(DspqSectionMaster dspqSecM : dspqMasterList)
						{
							if(dspqSecM.getSectionId().equals(dspqRouterssection.get(i).getDspqSectionMaster().getSectionId())){
								mapValue.put(dspqRouterssection.get(i).getDspqSectionMaster().getSectionId(),k++);
							}
							if(dspqRouterssection.get(i).getDspqSectionMaster().getSectionId().equals(dspqSecM.getSectionId())){
								if(count1==0)
									sectionOrderArray+=dspqRouterssection.get(i).getDspqSectionMaster().getSectionId();
								else
									sectionOrderArray+=","+dspqRouterssection.get(i).getDspqSectionMaster().getSectionId();
								count1++;
							}
						}
						
					}					
					for(DspqSectionMaster dspqSec1 : dspqMasterList)					
					{
						if(mapValue.get(dspqSec1.getSectionId())!=null){
								dspqSectionMastersList[mapValue.get(dspqSec1.getSectionId())]= dspqSec1;								
						}
						
						
					}
					Integer mapSectionValue=mapValue.size();
					for(DspqSectionMaster dspqSec1 : dspqMasterList)					
					{
						if(mapValue.get(dspqSec1.getSectionId())==null){
							if(dspqSec1!=null)
								dspqSectionMastersList[mapSectionValue]= dspqSec1;
							mapSectionValue++;
							
						}
					}
				}				
				String[] orderArrayIndex =null;
				String sectionStartIndex="";
				String sectionEndIndex="";
				if(sectionOrderArray!=null && sectionOrderArray!=""){
					dmRecords.append("<input type='hidden' id='sectionOrderArray'  value='"+sectionOrderArray+"'>");
					
					orderArrayIndex	= sectionOrderArray.trim().split(",");
					sectionStartIndex=orderArrayIndex[0];
					sectionEndIndex=orderArrayIndex[orderArrayIndex.length-1];
					System.out.println("orderArrayIndex "+sectionOrderArray);
				}
				Integer orderCounter=0;
		
				for(DspqSectionMaster dspqSec : dspqSectionMastersList)
				{	if(dspqSec!=null)
						dmRecords.append("<div class='hide' id='orderSec"+dspqSec.getSectionId()+"'>"+dspqSec.getSectionId()+"</div>");
				}
				//*****************************End For Section Ordering ******************************************
				
				
				
				for(DspqSectionMaster dspqSec : dspqSectionMastersList)
				{
					String sKeyValue = propertyKey(dspqSec.getSectionName(),dspqSec.getSectionId(),"Section");
					String val="hide";
					if(dspqSec.getSectionId()==3 || dspqSec.getSectionId()==29)
					{
						if(bEEOCEnable)
							val="hide";
						else
							val="show";
					}
					else
						val="hide";
					if(dspqMasterList!=null && dspqMasterList.size()>0)
						{
						
						dmRecords.append("<div class='row " +val+"' style='width: 100%;' id='panel"+dspqSec.getSectionId()+"'><div class='col-sm-11 col-md-11 pright0' style='width: 100%;'>");
						
						dmRecords.append("<div style='width:95%; border:none;'>");
						
						//Row Start 
						dmRecords.append("<input type='hidden' id='section_Field_"+dspqSec.getSectionId()+"'  value='"+dspqSec.getSectionName()+"'>");
						dmRecords.append("<div class='row top20'  style='padding:0px 0px 0px 0px; width: 100%; margin-left:0px; height:37px; border-radius: 5px;  border: 1px rgb(213, 213, 213) solid;'>");
						dmRecords.append("<div class='col-sm-12 col-md-12 accordion-heading' style='background-color:White ;border: 1px rgb(213, 213, 213) solid; border: 1px; width:100%; padding-left:10px;'>");
						dmRecords.append("<div class='row'><div class='col-sm-10 col-md-10'  style='font-size: 12px; height: 26px;  line-height: 35px;color:#007AB4; padding-left:10px; font-weight:bold;'>Candidate Application Subsection: "+dspqSec.getSectionName()+" </div>");
						dmRecords.append("<div class='col-sm-2 col-md-2 marginpadding'>");
						
						
						//*******************
						dmRecords.append("<div class='sectionHideShow"+dspqSec.getSectionId()+" show'>");
						dspqFields = sectionAdditionalIds.get(dspqSec.getSectionId());
						
						
						/*if(dspqSec.getSectionId()==2 || dspqSec.getSectionId()==4 || dspqSec.getSectionId()==7 || dspqSec.getSectionId()==13)
						{*/
							dmRecords.append("<ul class='nav navbar-nav navbar-right' role='tablist'>     "+
									"      <li role='presentation' class='moreFileds1' id='moreSecId"+dspqSec.getSectionId()+"'>"+
									"        <a id='drop4' style=' color:white!important; width: 125px; background-color:#007AB4;'  class='flatbtn' href='javascript:void(0)' onclick='showDivMoreFields("+dspqSec.getSectionId()+");'>"+
									"          <i class='fa fa-cog'></i> More Fields "+
									"          <i class='caret top7'></i>"+
									"        </a>"+
									"        <ul id='menu1' class='dropdown-menu arrow_box' aria-labelledby='drop4' style='width: 320px;'>");
							
							
								
											dmRecords.append(" <li ><span class='moreSelectedColor' style='cursor: pointer; padding-left:5px; font-weight:bold;' id='additionalMore"+dspqSec.getSectionId()+"' href='javascript:void(0)' onclick='return showHideAdditionalFieldss("+dspqSec.getSectionId()+");'>TM Additional</span> <span class='show' style='cursor: pointer; font-weight:bold;' id='customMore"+dspqSec.getSectionId()+"' href='javascript:void(0)' onclick='return showHideCustomFieldss("+dspqSec.getSectionId()+");'>| Custom</span> <span style=' cursor: pointer;   float: right; padding-right: 5px; font-weight:bold;' href='javascript:void(0)' onclick='return openDspqQuestionDiv();'>+ Add Custom</span> </li> "+
							" <li class='divider'></li>");
							
							

								Integer	secId=dspqSec.getSectionId();
								/*for(Integer secId :lstSecrAddFld)
								{*/
									dspqAdditionalFields = sectionAdditionalIds.get(secId);
									DspqSectionMaster dspqSec1=mapSec.get(secId);
									////////////////////Custom Fields Start/////////////////////
									dmRecords.append("<div id='TMCustomFieldss"+dspqSec.getSectionId()+"' class='hide'>");
									//dmRecords.append("<div style='color:#007AB4; font-size: 12px; padding-left: 5px; font-weight: bold;'>Custom Fields</div>");
									int counterToolTip = 1;
									int quetionId=0;
									int quetionId1=1;
									List<DistrictSpecificPortfolioQuestions> dspqQuestionsList = null;
									dspqQuestionsList = dspqQuestionsMap.get(secId);
									if(dspqQuestionsList!=null)
									{
										for(DistrictSpecificPortfolioQuestions dspqQuestion : dspqQuestionsList){
											
											if(dspqQuestion.getParentQuestionId().equals(quetionId))
												quetionId1=0;
											else
												quetionId1=1;
											if(quetionId1==1)
											{
												dmRecords.append("<div class='row'>");
												dmRecords.append("<div class='col-sm-1 col-md-1' style=' margin:0px;padding:0px 0px 0px 20px;'>");
												quetionId=dspqQuestion.getParentQuestionId();
												dmRecords.append("<input type='checkbox' id='customFieldID"+dspqQuestion.getQuestionId()+"'   onclick='hideShowAdditionalFields("+dspqQuestion.getQuestionId()+","+dspqSec1.getSectionId()+",array"+dspqSec1.getSectionId()+");'>");
												dmRecords.append("</div>");
													dmRecords.append("<div class='col-sm-9 col-md-9' style='padding-left:10px; color: gray; font-weight: bold;'>");
														dmRecords.append(dspqQuestion.getQuestion());
													dmRecords.append("</div>");
													dmRecords.append("<div class='col-sm-2 col-md-2'>");
														dmRecords.append("<span style='cursor:pointer;' href='javascript:void(0);' data-original-title='Deactivate Question' rel='tooltip' id='dspqActiveDeactive"+counterToolTip+"' onclick='return editCustomField("+dspqQuestion.getQuestionId()+")')\"><span class='fa fa-pencil-square-o refChkIconGrayColor'></span></span>");
														//dmRecords.append("&nbsp;&nbsp;<span style='cursor:pointer;' href='javascript:void(0);' data-original-title='Remove Question' rel='tooltip' id='dspqActiveDeactive"+counterToolTip+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"I\")')\"><span class='fa fa-times refChkIconGrayColor'></span></span>");
														dmRecords.append("</div>");
												dmRecords.append("</div>");
												counterToolTip++;
											}
										}
								}
									else
									{
										dmRecords.append("<div class='row'>");
										dmRecords.append("<div class='col-sm-9 col-md-9' style=' margin:0px;padding:0px 0px 0px 20px;'>");
												dmRecords.append("Currently there are no Custom Fields defined for "+dspqSec.getSectionName()+"!</div>");
										dmRecords.append("</div>");
									}
								
									//dmRecords.append("<div style='cursor:pointer; padding-left:5px;'><span href='javascript:void(0)' onclick='cancelAdditionalFields();'>Cancel</span></div>");
									dmRecords.append("</div>");
									////////////////////Custom Fields End/////////////////////
									dmRecords.append("<div id='TMadditionalFieldss"+dspqSec.getSectionId()+"' class='show'>");
											if(dspqAdditionalFields!=null && dspqAdditionalFields.size()>0)
												dmRecords.append("<li style='font-size:13px; padding-left:5px; color:black; font-weight:bold;'>"+dspqSec1.getSectionName()+"</li>");
											if(dspqAdditionalFields!=null )
											{
												
												int i=0;
												for(DspqFieldMaster dspqfm : dspqAdditionalFields){	
													String fieldName="";
													if(dspqAdditionalFields.size()==1)
													{														
														
														dmRecords.append("<li style='font-size: 11px;  line-height: 20px; color: #707070;'>");
														dmRecords.append("<div class='row'>");
														
														dmRecords.append("<div class='col-sm-1 col-md-1' style='width:40px; margin:0px;padding:0px 0px 0px 20px;'>");										
														dmRecords.append("<input type='checkbox' id='additionalFieldID"+dspqfm.getDspqFieldId()+"'   onclick='hideShowAdditionalFields("+dspqfm.getDspqFieldId()+","+dspqSec1.getSectionId()+",array"+dspqSec1.getSectionId()+");'>");
														dmRecords.append("</div>");
														
														dmRecords.append("<div class='col-sm-9 col-md-9' style='margin:0px;padding:0px; color: gray; font-weight: bold;'>");										
														dmRecords.append(dspqfm.getDspqFieldName());
														dmRecords.append("</div>");	
														
														dmRecords.append("</div>");
														dmRecords.append("</li>");
														
													}
													else
													{
														if(dspqfm.getDspqFieldId()==154 || dspqfm.getDspqFieldId()==160 || dspqfm.getDspqFieldId()==161)
														{
															fieldName=dspqfm.getDspqFieldName()+" "+districtName1;
															
														}
														else
															fieldName=dspqfm.getDspqFieldName();
														if(i%2==0)
														{
															
															dmRecords.append("<li style='font-size: 11px;  line-height: 20px; color: #707070;'>");
															dmRecords.append("<div class='row'>");
															
															dmRecords.append("<div class='col-sm-1 col-md-1' style='width:40px; margin:0px;padding:0px 0px 0px 20px;'>");										
															dmRecords.append("<input type='checkbox' id='additionalFieldID"+dspqfm.getDspqFieldId()+"'   onclick='hideShowAdditionalFields("+dspqfm.getDspqFieldId()+","+dspqSec1.getSectionId()+",array"+dspqSec1.getSectionId()+");'>");
															dmRecords.append("</div>");
															
															dmRecords.append("<div class='col-sm-2 col-md-2' style='width:140px; margin:0px;padding:0px;  color: gray; font-weight: bold;'>");										
															dmRecords.append(fieldName);
															dmRecords.append("</div>");	
														}
														else
														{
															dmRecords.append("<div class='col-sm-1 col-md-1' style='width:30px; margin:0px;padding:0px 0px 0px 15px;'>");										
															dmRecords.append("<input type='checkbox' id='additionalFieldID"+dspqfm.getDspqFieldId()+"'   onclick='hideShowAdditionalFields("+dspqfm.getDspqFieldId()+","+dspqSec1.getSectionId()+",array"+dspqSec1.getSectionId()+");'>");
															dmRecords.append("</div>");
															
															dmRecords.append("<div class='col-sm-3 col-md-3' style='width:110px; margin:0px;padding:0px;  color: gray; font-weight: bold;'>");										
															dmRecords.append(fieldName);
															dmRecords.append("</div>");
															
															dmRecords.append("</div>");
															dmRecords.append("</li>");
														}
													}
													
													i++;
												}
											
											}
											else
											{
												dmRecords.append("<li style=' line-height: 20px; color: #707070;'>");
												dmRecords.append("<div class='row'>");													
												
												dmRecords.append("<div class='col-sm-9 col-md-9' style='margin:0px;padding:0px 0px 0px 20px;;'>");										
												dmRecords.append("Currently there are no TM Additional Fields defined for "+dspqSec.getSectionName()+"!");
												dmRecords.append("</div>");	
												
												dmRecords.append("</div>");
												dmRecords.append("</li>");
											}
								//}
								dmRecords.append("</div>");
								
								//dmRecords.append("<li style='padding-left:10px;'><span><a href='javascript:void(0)' onclick='cancelAdditionalFields();'>Cancel</a></span></li>");
							}
							else
							{

								
									dspqAdditionalFields = sectionAdditionalIds.get(dspqSec.getSectionId());
									DspqSectionMaster dspqSec1=mapSec.get(dspqSec.getSectionId());
									////////////////////Custom Fields Start/////////////////////
									dmRecords.append("<div id='TMCustomFieldss"+dspqSec.getSectionId()+"' class='hide'>");
									//dmRecords.append("<div style='color:#007AB4; font-size: 12px; padding-left: 5px; font-weight: bold;'>Custom Fields</div>");
									int counterToolTip = 1;
									int quetionId=0;
									int quetionId1=1;
									List<DistrictSpecificPortfolioQuestions> dspqQuestionsList = null;
									dspqQuestionsList = dspqQuestionsMap.get(dspqSec.getSectionId());
									if(dspqQuestionsList!=null)
									{
										for(DistrictSpecificPortfolioQuestions dspqQuestion : dspqQuestionsList){
											
											if(dspqQuestion.getParentQuestionId().equals(quetionId))
												quetionId1=0;
											else
												quetionId1=1;
											if(quetionId1==1)
											{
												dmRecords.append("<div class='row'>");
												dmRecords.append("<div class='col-sm-1 col-md-1' style=' margin:0px;padding:0px 0px 0px 20px;'>");
												quetionId=dspqQuestion.getParentQuestionId();
												dmRecords.append("<input type='checkbox' id='customFieldID"+dspqQuestion.getQuestionId()+"'   onclick='hideShowAdditionalFields("+dspqQuestion.getQuestionId()+","+dspqSec1.getSectionId()+",array"+dspqSec1.getSectionId()+");'>");
												dmRecords.append("</div>");
													dmRecords.append("<div class='col-sm-9 col-md-9' style='padding-left:10px; color: gray; font-weight: bold;'>");
														dmRecords.append(dspqQuestion.getQuestion());
													dmRecords.append("</div>");
													dmRecords.append("<div class='col-sm-2 col-md-2'>");
														dmRecords.append("<span style='cursor:pointer;' href='javascript:void(0);' data-original-title='Deactivate Question' rel='tooltip' id='dspqActiveDeactive"+counterToolTip+"' onclick='return editCustomField("+dspqQuestion.getQuestionId()+")')\"><span class='fa fa-pencil-square-o refChkIconGrayColor'></span></span>");
														//dmRecords.append("&nbsp;&nbsp;<span style='cursor:pointer;' href='javascript:void(0);' data-original-title='Remove Question' rel='tooltip' id='dspqActiveDeactive"+counterToolTip+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"I\")')\"><span class='fa fa-times refChkIconGrayColor'></span></span>");
														dmRecords.append("</div>");
												dmRecords.append("</div>");
												counterToolTip++;
											}
										}
								}
								else
								{
									dmRecords.append("<div class='row'>");
									dmRecords.append("<div class='col-sm-11 col-md-11' style=' margin:0px;padding:0px 0px 0px 20px;'>");
											dmRecords.append("Currently there are no Custom Fields defined for "+dspqSec.getSectionName()+"!</div>");
									dmRecords.append("</div>");
								}
								
									dmRecords.append("</div>");
									////////////////////Custom Fields End/////////////////////
									dmRecords.append("<div id='TMadditionalFieldss"+dspqSec.getSectionId()+"' class='show'>");
									
										dmRecords.append("<li style=' line-height: 20px; color: #707070;'>");
										dmRecords.append("<div class='row'>");													
										
										dmRecords.append("<div class='col-sm-12 col-md-12' style='margin:0px;padding:0px 16px 0px 20px;;'>");										
										dmRecords.append("Currently there are no TM Additional Fields defined for "+dspqSec.getSectionName()+"!");
										dmRecords.append("</div>");	
										
										dmRecords.append("</div>");
										dmRecords.append("</li>");
									
											
								
								dmRecords.append("</div>");
							
							}
							
						/*}*/
							
						
							
						   
								
								dmRecords.append("        </ul>"+
								"      </li>   "+
								"    </ul>");
								dmRecords.append("</div>");
						//********************
						
						
						
						dmRecords.append("</div></div></div>");
						
						dmRecords.append("</div>");
						
						//Row End
						dmRecords.append("<div  id='collapse"+count+"' style='height:auto;'>");
						dmRecords.append("<div  style='padding:0px 0px 0px 0px;'>");
						if(dspqSec.getSectionId()==9)
						{
							dmRecords.append("<div class='row' style='padding:5px 0px 0px 10px; padding-right: 10px; font-size:12px; margin-bottom:10px; '>");						
							dmRecords.append("<div class='col-sm-4 col-md-4'>");
							dmRecords.append("External <select name='References' id='Refrences_1' class='form-control'><option value='0'>References</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option></select>");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-4 col-md-4 sectionLabelInternal hide'>");
							dmRecords.append("Internal <select name='References' id='Refrences_2' class='form-control'><option value='0'>References</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option></select>");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-4 col-md-4 sectionLabelInternalTransfer hide'>");
							dmRecords.append("I Transfer <select name='References' id='Refrences_3' class='form-control'><option value='0'>References</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option></select>");
							dmRecords.append("</div>");
							dmRecords.append("</div>");
						}
						if(dspqSec.getSectionId()==6)
						{
							dmRecords.append("<div class='row' style='padding:5px 0px 0px 10px; padding-right: 10px; font-size:12px;'>");						
							dmRecords.append("<div class='col-sm-3 col-md-3 top20'>");
							dmRecords.append(Utility.getLocaleValuePropByKey("Selectatleastadegreetype", locale)+ " <span href='#'  data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+Utility.getLocaleValuePropByKey("RequireddegreetypehelperText", locale)+"'><img src='images/qua-icon.png' alt=''></span>");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-3 col-md-3'>");
							dmRecords.append("External <select name='degreeType1' id='degreeType_1' class='form-control'><option value='0'>"+Utility.getLocaleValuePropByKey("lblNone", locale)+"</option><option value='1'>"+Utility.getLocaleValuePropByKey("lblhighschool", locale)+"</option><option value='2'>"+Utility.getLocaleValuePropByKey("lblAssociateDegree", locale)+"</option><option value='3'>"+Utility.getLocaleValuePropByKey("lblBachlorDegree", locale)+"</option><option value='4'>"+Utility.getLocaleValuePropByKey("lblMasterDegree", locale)+"</option><option value='5'>"+Utility.getLocaleValuePropByKey("lblPhd", locale)+"</option></select>");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-3 col-md-3 sectionLabelInternal hide'>");
							dmRecords.append("Internal <select name='degreeType2' id='degreeType_2' class='form-control'><option value='0'>"+Utility.getLocaleValuePropByKey("lblNone", locale)+"</option><option value='1'>"+Utility.getLocaleValuePropByKey("lblhighschool", locale)+"</option><option value='2'>"+Utility.getLocaleValuePropByKey("lblAssociateDegree", locale)+"</option><option value='3'>"+Utility.getLocaleValuePropByKey("lblBachlorDegree", locale)+"</option><option value='4'>"+Utility.getLocaleValuePropByKey("lblMasterDegree", locale)+"</option><option value='5'>"+Utility.getLocaleValuePropByKey("lblPhd", locale)+"</option></select>");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-3 col-md-3 sectionLabelInternalTransfer hide'>");
							dmRecords.append("I Transfer <select name='degreeType3' id='degreeType_3' class='form-control'><option value='0'>"+Utility.getLocaleValuePropByKey("lblNone", locale)+"</option><option value='1'>"+Utility.getLocaleValuePropByKey("lblhighschool", locale)+"</option><option value='2'>"+Utility.getLocaleValuePropByKey("lblAssociateDegree", locale)+"</option><option value='3'>"+Utility.getLocaleValuePropByKey("lblBachlorDegree", locale)+"</option><option value='4'>"+Utility.getLocaleValuePropByKey("lblMasterDegree", locale)+"</option><option value='5'>"+Utility.getLocaleValuePropByKey("lblPhd", locale)+"</option></select>");
							dmRecords.append("</div>");
							dmRecords.append("</div>");
							
							
							dmRecords.append("<div class='row' style='padding:5px 0px 0px 10px; padding-right: 10px; font-size:12px;'>");						
							dmRecords.append("<div class='col-sm-3 col-md-3 top13'>");
							dmRecords.append(Utility.getLocaleValuePropByKey("Doyouwantdegreehierarchy", locale)+ " <span href='#'  data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+Utility.getLocaleValuePropByKey("DoyouwantdegreehierarchyhelperText", locale)+"'><img src='images/qua-icon.png' alt=''></span>");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-3 col-md-3'>");
							dmRecords.append("<label class='degreeLabel' style='padding-left: 0px; !important'><input type='radio' id='degreeHierarchy_1' name='degreeHierarchyExternal'  checked='checked'  value='0' /> No</label> <label class='degreeLabel'><input type='radio' id='degreeHierarchy_11' name='degreeHierarchyExternal' value='1' />  Yes</label>");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-3 col-md-3 sectionLabelInternal hide'>");
							dmRecords.append("<label class='degreeLabel' style='padding-left: 0px; !important'><input type='radio' id='degreeHierarchy_2' name='degreeHierarchyInternal'  checked='checked'  value='0' /> No</label> <label class='degreeLabel'><input type='radio' id='degreeHierarchy_22' name='degreeHierarchyInternal' value='1' />  Yes</label>");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-3 col-md-3 sectionLabelInternalTransfer hide'>");
							dmRecords.append("<label class='degreeLabel' style='padding-left: 0px; !important'><input type='radio' id='degreeHierarchy_3' name='degreeHierarchyITransfer' checked='checked'  value='0' /> No</label> <label class='degreeLabel'><input type='radio' id='degreeHierarchy_33' name='degreeHierarchyITransfer' value='1' />  Yes</label>");
							dmRecords.append("</div>");
							dmRecords.append("</div>");
						}
						
						
						dspqFields = sectionIds.get(dspqSec.getSectionId());
						if(dspqFields!=null && dspqFields.size()>0)
						{
							dmRecords.append("<div class='row' style='font-weight: bold; padding-top:5px; padding-right:15px;  color:#474747;'>");	
							
							dmRecords.append("<div  class='col-sm-9 col-md-9 top7 manageWidth'>");
							dmRecords.append("");
							dmRecords.append("</div>");			
							
							dmRecords.append("<div class='col-sm-1 col-md-1 top7'>");
							dmRecords.append("Edit");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-2 col-md-2 top7'>");
							dmRecords.append("External");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-2 col-md-2 top7 hide sectionLabelInternal' style='padding-left:30px;'>");
							dmRecords.append("Internal");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-2 col-md-2 top7 hide sectionLabelInternalTransfer' style='text-align:center;'>");
							dmRecords.append("Internal Transfer");
							dmRecords.append("</div>");
				
							dmRecords.append("</div>");
						}
						orderCounter++;
						dmRecords.append("<input type='hidden' id='allSectionChecked_"+dspqSec.getSectionId()+"'  value='0'>");
						dmRecords.append("<input type='hidden' id='sectionfield1_"+dspqSec.getSectionId()+"'  value='"+dspqSec.getSectionName()+"'>");
						dmRecords.append("<input type='hidden' id='sectionapplicantType_1_"+dspqSec.getSectionId()+"' value='E'>");
						dmRecords.append("<input type='hidden' id='sectionfield_1_"+dspqSec.getSectionId()+"'  value='"+dspqSec.getSectionName()+"'>");
						if(dspqSec.getSectionId()==25 || dspqSec.getSectionId()==3)
							dmRecords.append("<input type='hidden' id='sectionreq_1_"+dspqSec.getSectionId()+"' value='1'>");
						else
							dmRecords.append("<input type='hidden' id='sectionreq_1_"+dspqSec.getSectionId()+"' value='0'>");
						dmRecords.append("<input type='hidden' id='sectiontooltip_1_"+dspqSec.getSectionId()+"'>");
						dmRecords.append("<input type='hidden' id='sectionstatus_1_"+dspqSec.getSectionId()+"' value='A'>");
						dmRecords.append("<input type='hidden' id='sectionOrder_1_"+dspqSec.getSectionId()+"'  value="+orderCounter+">");
						///Internal hidden Filed
						dmRecords.append("<input type='hidden' id='sectionapplicantType_2_"+dspqSec.getSectionId()+"' value='I'>");
						dmRecords.append("<input type='hidden' id='sectionfield_2_"+dspqSec.getSectionId()+"'  value='"+dspqSec.getSectionName()+"'>");
						if(dspqSec.getSectionId()==25 || dspqSec.getSectionId()==3)
							dmRecords.append("<input type='hidden' id='sectionreq_2_"+dspqSec.getSectionId()+"' value='1'>");
						else
							dmRecords.append("<input type='hidden' id='sectionreq_2_"+dspqSec.getSectionId()+"' value='0'>");
						dmRecords.append("<input type='hidden' id='sectiontooltip_2_"+dspqSec.getSectionId()+"' >");
						dmRecords.append("<input type='hidden' id='sectionstatus_2_"+dspqSec.getSectionId()+"' value='A'>");
						dmRecords.append("<input type='hidden' id='sectionOrder_2_"+dspqSec.getSectionId()+"'  value="+orderCounter+">");
						///Internal Transfer hidden Filed
						dmRecords.append("<input type='hidden' id='sectionapplicantType_3_"+dspqSec.getSectionId()+"' value='T'>");
						dmRecords.append("<input type='hidden' id='sectionfield_3_"+dspqSec.getSectionId()+"'  value='"+dspqSec.getSectionName()+"'>");
						if(dspqSec.getSectionId()==25 || dspqSec.getSectionId()==3)
							dmRecords.append("<input type='hidden' id='sectionreq_3_"+dspqSec.getSectionId()+"' value='1'>");
						else
							dmRecords.append("<input type='hidden' id='sectionreq_3_"+dspqSec.getSectionId()+"' value='0'>");
						
						dmRecords.append("<input type='hidden' id='sectiontooltip_3_"+dspqSec.getSectionId()+"' >");
						dmRecords.append("<input type='hidden' id='sectionstatus_3_"+dspqSec.getSectionId()+"' value='A'/>");
						dmRecords.append("<input type='hidden' id='sectionOrder_3_"+dspqSec.getSectionId()+"'  value="+orderCounter+">");
						
						
						String EEOCInstructions=Utility.getLocaleValuePropByKey("instructionsForEEOC", locale);
						if( dspqSec.getSectionId()==3)
						{
							dmRecords.append("<input type='hidden' id='instruction_1_"+dspqSec.getSectionId()+"' value='"+EEOCInstructions+"'/>");
							dmRecords.append("<input type='hidden' id='instruction_2_"+dspqSec.getSectionId()+"' value='"+EEOCInstructions+"'/>");
							dmRecords.append("<input type='hidden' id='instruction_3_"+dspqSec.getSectionId()+"' value='"+EEOCInstructions+"'/>");
						}
						else
						{
							dmRecords.append("<input type='hidden' id='instruction_1_"+dspqSec.getSectionId()+"'/>");
							dmRecords.append("<input type='hidden' id='instruction_2_"+dspqSec.getSectionId()+"'/>");
							dmRecords.append("<input type='hidden' id='instruction_3_"+dspqSec.getSectionId()+"'/>");
						}
						dmRecords.append("<input type='hidden' id='sectionId"+dspqSec.getSectionId()+"' value='"+dspqSec.getSectionId()+"'><input type='hidden' id='sectionDisplay"+dspqSec.getSectionId()+"' value='"+dspqSec.getSectionName()+"'>");
						
						//Row Start
						dmRecords.append("<div class='row top5' style='padding: 5px 0px 1px 2px;  margin-left: 0px; border-radius: 5px; border: 1px rgb(213, 213, 213) solid; width: 100%;'>");						
						dmRecords.append("<div class='col-sm-9 col-md-9 manageWidth' style='padding-left:0px;'>");
						dmRecords.append( "<span id='sectionLabel_"+dspqSec.getSectionId()+"'>"+dspqSec.getSectionName()+ "</span> Sub-Section <span href='#' id='tool1' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+sKeyValue+"'><img src='images/qua-icon.png' alt=''></span>");						
						dmRecords.append("</div>");
						if(dspqSec.getSectionId()==3)
						{
							dmRecords.append("<div class='col-sm-1 col-md-1 top5 managePadding'>");												
								dmRecords.append("<i class='fa fa-pencil-square-o fa-lg tempToolTip' data-toggle='tooltip' data-placement='top' title='Edit'></i>");
							dmRecords.append("</div>");
							
							dmRecords.append("<div class='col-sm-2 col-md-2 manageExternal externalLeftPadding1' style='padding-right: 10px;'>");
							dmRecords.append("<label class='circleIns instructions tempToolTip' data-toggle='tooltip' data-placement='top' id='secInstruction_1_"+dspqSec.getSectionId()+"' title='"+EEOCInstructions+"'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='secLabel_1_"+dspqSec.getSectionId()+"' title='"+dspqSec.getSectionName()+"'>L</label> <label class='circleIns helper' id='secHelp_1_"+dspqSec.getSectionId()+"'>?</label> <label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' title='Required' style='cursor: default;' id='secReqOpt_1_"+dspqSec.getSectionId()+"'>R</label> <label class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' title='Active' style='cursor: default;' id='sectionRequired1_"+dspqSec.getSectionId()+"'>A</label>");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-2 col-md-2 hide sectionLabelInternal' style='padding-right: 2px; padding-left: 20px;'>");
							dmRecords.append("<label class='circleIns instructions tempToolTip' data-toggle='tooltip' data-placement='top' id='secInstruction_2_"+dspqSec.getSectionId()+"' title='"+EEOCInstructions+"'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='secLabel_2_"+dspqSec.getSectionId()+"' title='"+dspqSec.getSectionName()+"'>L</label> <label class='circleIns helper' id='secHelp_2_"+dspqSec.getSectionId()+"'>?</label> <label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' title='Required' style='cursor: default;' id='secReqOpt_2_"+dspqSec.getSectionId()+"'>R</label> <label class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' title='Active' style='cursor: default;' id='sectionRequired2_"+dspqSec.getSectionId()+"'>A</label>");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-2 col-md-2 hide sectionLabelInternalTransfer pleft20' style='padding-right: 0px;'>");
							dmRecords.append("<label class='circleIns instructions tempToolTip' data-toggle='tooltip' data-placement='top' id='secInstruction_3_"+dspqSec.getSectionId()+"' title='"+EEOCInstructions+"'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='secLabel_3_"+dspqSec.getSectionId()+"' title='"+dspqSec.getSectionName()+"'>L</label> <label class='circleIns helper' id='secHelp_3_"+dspqSec.getSectionId()+"'>?</label> <label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' title='Required' style='cursor: default;' id='secReqOpt_3_"+dspqSec.getSectionId()+"'>R</label> <label class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' title='Active' style='cursor: default;' id='sectionRequired3_"+dspqSec.getSectionId()+"'>A</label>");
							dmRecords.append("</div>");					
						}
						else
						{
							dmRecords.append("<div class='col-sm-1 col-md-1 managePadding' >");							
								dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top'  title='Edit' onclick='return showSectionDiv("+dspqSec.getSectionId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>&nbsp;&nbsp;");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-2 col-md-2 manageExternal externalLeftPadding1' style='padding-right: 10px;'>");
							dmRecords.append("<label class='circleIns instructions tempToolTip' data-toggle='tooltip' data-placement='top' id='secInstruction_1_"+dspqSec.getSectionId()+"'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='secLabel_1_"+dspqSec.getSectionId()+"' title='"+dspqSec.getSectionName()+"'>L</label> <label class='circleIns helper tempToolTip' data-toggle='tooltip' data-placement='top' id='secHelp_1_"+dspqSec.getSectionId()+"'>?</label> <label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' title='Optional' onclick='changeSectiondOptions("+dspqSec.getSectionId()+",1);' id='secReqOpt_1_"+dspqSec.getSectionId()+"'>O</label> <label class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='sectionRequired1_"+dspqSec.getSectionId()+"' title='Active' onclick='hideShowSubsection("+dspqSec.getSectionId()+",1);'>A</label>");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-2 col-md-2 hide sectionLabelInternal' style='padding-right: 2px; padding-left: 18px;'>");
							dmRecords.append("<label class='circleIns instructions tempToolTip' data-toggle='tooltip' data-placement='top' id='secInstruction_2_"+dspqSec.getSectionId()+"'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='secLabel_2_"+dspqSec.getSectionId()+"' title='"+dspqSec.getSectionName()+"'>L</label> <label class='circleIns helper tempToolTip' data-toggle='tooltip' data-placement='top' id='secHelp_2_"+dspqSec.getSectionId()+"'>?</label> <label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' title='Optional' onclick='changeSectiondOptions("+dspqSec.getSectionId()+",2);' id='secReqOpt_2_"+dspqSec.getSectionId()+"'>O</label> <label class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='sectionRequired2_"+dspqSec.getSectionId()+"' title='Active' onclick='hideShowSubsection("+dspqSec.getSectionId()+",2);' >A</label>");
							dmRecords.append("</div>");
							dmRecords.append("<div class='col-sm-2 col-md-2 hide sectionLabelInternalTransfer pleft20' style=' padding-right: 0px;'>");
							dmRecords.append("<label class='circleIns instructions tempToolTip' data-toggle='tooltip' data-placement='top' id='secInstruction_3_"+dspqSec.getSectionId()+"'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='secLabel_3_"+dspqSec.getSectionId()+"' title='"+dspqSec.getSectionName()+"'>L</label> <label class='circleIns helper tempToolTip' data-toggle='tooltip' data-placement='top' id='secHelp_3_"+dspqSec.getSectionId()+"'>?</label> <label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' title='Optional' onclick='changeSectiondOptions("+dspqSec.getSectionId()+",3);' id='secReqOpt_3_"+dspqSec.getSectionId()+"'>O</label> <label class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='sectionRequired3_"+dspqSec.getSectionId()+"' title='Active' onclick='hideShowSubsection("+dspqSec.getSectionId()+",3);' >A</label>");
							dmRecords.append("</div>");
						
						}
						dmRecords.append("</div>");
						
						dmRecords.append("<div class='row top5 left1'>");						
						dmRecords.append("<div class='col-sm-12 col-md-12'></div>");						
						dmRecords.append("</div>");
						//Row End
						/////////////////////////////////////////////////////hdhfdbfbffgbfffgfdgfgf////////////////////////////////////////
						/*if(dspqSec.getSectionId()==3)
						{
							String instruction="Government agencies require our partner districts and clients to collect information and file periodic reports regarding the gender, race and ethnicity of applicants. The definition for each category below has been established by the United States Federal Government. The data collected enables us to analyze hiring processes, and to ensure equal employment opportunity compliance.\n\nThe information requested below is voluntary and refusal to provide will not affect you adversely. This information is kept separate from your application.";
						}
						else
						{*/
							dmRecords.append("<div class='sectionHideShow"+dspqSec.getSectionId()+" show'>");
						//}
						//Row End
						//Row Start
						
					if(dspqFields!=null && dspqFields.size()>0)
					{
						for(DspqFieldMaster dspqfm : dspqFields){
							
							String ekeyValue = propertyKey(dspqfm.getDspqFieldName(),dspqfm.getDspqFieldId(),"Field");
							if(dspqfm.getDspqFieldId()==3)
								ekeyValue=Utility.getLocaleValuePropByKey("PortfolioField_3_IhavealreadysubmittedacoverletterforanotherjobatthisDistrictSchoolsopleaseusethat", locale);
							dmRecords.append("<div ><div class='row top5'>");	
							dmRecords.append("<input type='hidden' id='dspqFieldId"+dspqfm.getDspqFieldId()+"' value='"+dspqfm.getDspqFieldId()+"'><input type='hidden' id='dspqSecIdForSec"+dspqfm.getDspqFieldId()+"' value='"+dspqSec.getSectionId()+"'>");
							dmRecords.append("<input type='hidden' id='TMAddTMDef"+dspqfm.getDspqFieldId()+"' value='Default'>");
							String fieldName="";							
							if(dspqfm.getIsDependantField()==null)
							{
								dmRecords.append("<div class='col-sm-9 col-md-9  manageWidth' >");						
								
								
								if(dspqfm.getDspqFieldId()==121 || dspqfm.getDspqFieldId()==127 || dspqfm.getDspqFieldId()==132)
								{
									fieldName=dspqfm.getDspqFieldName()+" "+districtName1;
									ekeyValue=ekeyValue+" "+districtName1;
								}
								else if(dspqfm.getDspqFieldId()==123)
								{
									fieldName=dspqfm.getDspqFieldName()+" "+stateName1+" "+ Utility.getLocaleValuePropByKey("portfolioRetirementSystem", locale);
									ekeyValue=ekeyValue+" "+stateName1+" "+ Utility.getLocaleValuePropByKey("portfolioRetirementSystem", locale);
								}
								else if(dspqfm.getDspqFieldId()==125)
								{
									fieldName=dspqfm.getDspqFieldName()+" "+stateName1+" "+ Utility.getLocaleValuePropByKey("portfolioRetirementInvestmentPlanandIhavewithdrawnfunds", locale);
									ekeyValue=ekeyValue+" "+stateName1+" "+ Utility.getLocaleValuePropByKey("portfolioRetirementInvestmentPlanandIhavewithdrawnfunds", locale);
								}
								else if(dspqfm.getDspqFieldId()==4)
								{
									fieldName=dspqfm.getDspqFieldName()+" "+districtName1;
									//ekeyValue=ekeyValue+" "+userMaster.getDistrictId().getStateId().getStateName()+" "+ Utility.getLocaleValuePropByKey("portfolioRetirementInvestmentPlanandIhavewithdrawnfunds", locale);
								}
								else
								{
									fieldName=dspqfm.getDspqFieldName();									
								}
								
								dmRecords.append("<span  id='1displayField"+dspqfm.getDspqFieldId()+"'>"+fieldName +"</span> <span href='#'  data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+fieldName+"'><img src='images/qua-icon.png' alt=''></span>");
								dmRecords.append(" <span href='#'  data-toggle='tooltip' class='tempToolTip' data-placement='top' data-html=true' title=\"Data Type: "+dspqfm.getFieldDataType()+" "+loadDataTypeWithList(dspqfm.getDspqFieldId())+"\"><img src='images/qua-icon.png' alt=''></span>");
								dmRecords.append("</div>");
							}
							else
							{
								dmRecords.append("<div class='col-sm-9 col-md-9  manageWidth'><span class='left10' id='1displayField"+dspqfm.getDspqFieldId()+"'>");
																
								if(dspqfm.getDspqFieldId()==121 || dspqfm.getDspqFieldId()==127 || dspqfm.getDspqFieldId()==132)
									fieldName=dspqfm.getDspqFieldName()+" "+districtName1;
								else if(dspqfm.getDspqFieldId()==123)
									fieldName=dspqfm.getDspqFieldName()+" "+stateName1+" Retirement System.";								
								else if(dspqfm.getDspqFieldId()==125)
									fieldName=dspqfm.getDspqFieldName()+" "+stateName1+" Retirement Investment Plan and I have withdrawn funds.";							
								else
									fieldName=dspqfm.getDspqFieldName();
								
								dmRecords.append(fieldName +"</span> <span href='#'  data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+fieldName+"'><img src='images/qua-icon.png' alt=''></span>");
								dmRecords.append(" <span href='#'  data-toggle='tooltip' class='tempToolTip' data-placement='top' data-html='true' title=\"Data type: "+dspqfm.getFieldDataType()+" "+loadDataTypeWithList(dspqfm.getDspqFieldId())+"\"><img src='images/qua-icon.png' alt=''></span>");
									
								//<i class='fa fa-building'></i>
								dmRecords.append("</div>");
							}
							
							if(dspqSec.getSectionId()==3)
							{
								dmRecords.append("<div class='col-sm-1 col-md-1 top7' style='padding-left:8px;'>");
								dmRecords.append("<i class='fa fa-pencil-square-o fa-lg tempToolTip' data-toggle='tooltip' data-placement='top' title='Edit' style='text-align:center;'></i>");
								dmRecords.append("</div>");
								dmRecords.append("<div class='col-sm-2 col-md-2' style='padding-left: 1px;'>");
								dmRecords.append("<label class='circleInsWhite'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_1_"+dspqSec.getSectionId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper' style='cursor: default;'>?</label> <label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' title='It is a default Required field we can not change to optional' style='cursor: default;'>R</label> <label class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='1_"+dspqfm.getDspqFieldId()+"' title='It is a default activated field we can not change to deactivated' style='cursor: default;'>A</label>");
								dmRecords.append("</div>");
								dmRecords.append("<div class='col-sm-2 col-md-2 hide sectionLabelInternal pleft10'>");
								dmRecords.append("<label class='circleInsWhite'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_2_"+dspqSec.getSectionId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper' style='cursor: default;'>?</label> <label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' title='It is a default Required field we can not change to optional' style='cursor: default;'>R</label> <label class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='2_"+dspqfm.getDspqFieldId()+"' title='It is a default activated field we can not change to deactivated' style='cursor: default;'>A</label>");
								dmRecords.append("</div>");
								dmRecords.append("<div class='col-sm-2 col-md-2 hide sectionLabelInternalTransfer pleft10' >");
								dmRecords.append("<label class='circleInsWhite'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_3_"+dspqSec.getSectionId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper' style='cursor: default;'>?</label> <label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' title='It is a default Required field we can not change to optional' style='cursor: default;'>R</label> <label class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='3_"+dspqfm.getDspqFieldId()+"' title='It is a default activated field we can not change to deactivated' style='cursor: default;'>A</label>");
								dmRecords.append("</div>");
							}
							else
							{
								
								dmRecords.append("<div class='col-sm-1 col-md-1 top7' style='padding-left:8px;'>");
								dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Edit' onclick='showFieldDiv("+dspqfm.getDspqFieldId()+");'><i class='fa fa-pencil-square-o fa-lg' style='text-align:center;'></i></a>");
								dmRecords.append("</div>");
								dmRecords.append("<div class='col-sm-2 col-md-2' style='padding-left: 1px;'>");
								if(dspqfm.getIsRequired()==0)
									dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_1_"+dspqfm.getDspqFieldId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldHelp_1_"+dspqfm.getDspqFieldId()+"'>?</label> <label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' onclick='requireOptionalField("+dspqfm.getDspqFieldId()+",1);' id='reqOp_1_"+dspqfm.getDspqFieldId()+"'>O</label> <label title='Active' onclick='actDeacApplicant("+dspqfm.getDspqFieldId()+",1);' class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='1_"+dspqfm.getDspqFieldId()+"' >A</label>");
								else
									dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_1_"+dspqfm.getDspqFieldId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldHelp_1_"+dspqfm.getDspqFieldId()+"'>?</label> <label class='circleIns iconRequired tempToolTip' id='reqOp_1_"+dspqfm.getDspqFieldId()+"' data-toggle='tooltip' data-placement='top' title='It is a default Required field we can not change to optional' style='cursor:default;'>R</label> <label title='It is a default activated field we can not change to deactivated' style='cursor:default;' class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='1_"+dspqfm.getDspqFieldId()+"' >A</label>");
								dmRecords.append("</div>");
								dmRecords.append("<div class='col-sm-2 col-md-2 hide  sectionLabelInternal pleft10'>");
								if(dspqfm.getIsRequired()==0)
									dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_2_"+dspqfm.getDspqFieldId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldHelp_2_"+dspqfm.getDspqFieldId()+"'>?</label> <label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' onclick='requireOptionalField("+dspqfm.getDspqFieldId()+",2);' id='reqOp_2_"+dspqfm.getDspqFieldId()+"'>O</label> <label title='Active' onclick='actDeacApplicant("+dspqfm.getDspqFieldId()+",2);' class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='2_"+dspqfm.getDspqFieldId()+"'>A</label>");
								else
									dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_2_"+dspqfm.getDspqFieldId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldHelp_2_"+dspqfm.getDspqFieldId()+"'>?</label> <label class='circleIns iconRequired tempToolTip' id='reqOp_2_"+dspqfm.getDspqFieldId()+"' data-toggle='tooltip' data-placement='top' title='It is a default Required field we can not change to optional' style='cursor:default;'>R</label> <label title='It is a default activated field we can not change to deactivated' style='cursor:default;' class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='2_"+dspqfm.getDspqFieldId()+"'>A</label>");
								dmRecords.append("</div>");
								dmRecords.append("<div class='col-sm-2 col-md-2 hide  sectionLabelInternalTransfer pleft10' >");
								if(dspqfm.getIsRequired()==0)
									dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_3_"+dspqfm.getDspqFieldId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldHelp_3_"+dspqfm.getDspqFieldId()+"'>?</label> <label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' onclick='requireOptionalField("+dspqfm.getDspqFieldId()+",3);' id='reqOp_3_"+dspqfm.getDspqFieldId()+"'>O</label>  <label title='Active' onclick='actDeacApplicant("+dspqfm.getDspqFieldId()+",3);' class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='3_"+dspqfm.getDspqFieldId()+"' >A</label>");
								else
									dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_3_"+dspqfm.getDspqFieldId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldHelp_3_"+dspqfm.getDspqFieldId()+"'>?</label> <label class='circleIns iconRequired tempToolTip' id='reqOp_3_"+dspqfm.getDspqFieldId()+"' data-toggle='tooltip' data-placement='top' title='It is a default Required field we can not change to optional' style='cursor:default;'>R</label>  <label title='It is a default activated field we can not change to deactivated' style='cursor:default;' class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='3_"+dspqfm.getDspqFieldId()+"' >A</label>");
								dmRecords.append("</div>");
								
							}
							
							dmRecords.append("</div></div>");
							///External hidden Filed
							dmRecords.append("<input type='hidden' id='fieldIDD"+dspqfm.getDspqFieldId()+"' value='"+fieldName+"'>");
							dmRecords.append("<input type='hidden' id='applicantType_1_"+dspqfm.getDspqFieldId()+"' value='E'>");
							dmRecords.append("<input type='hidden' id='field_1_"+dspqfm.getDspqFieldId()+"'  value='"+fieldName+"'>");
							dmRecords.append("<input type='hidden' id='req_1_"+dspqfm.getDspqFieldId()+"' value="+dspqfm.getIsRequired()+">");
							dmRecords.append("<input type='hidden' id='tooltip_1_"+dspqfm.getDspqFieldId()+"'>");
							dmRecords.append("<input type='hidden' id='status_1_"+dspqfm.getDspqFieldId()+"' value='A'>");
							
							///Internal hidden Filed
							dmRecords.append("<input type='hidden' id='applicantType_2_"+dspqfm.getDspqFieldId()+"' value='E'>");
							dmRecords.append("<input type='hidden' id='field_2_"+dspqfm.getDspqFieldId()+"'  value='"+fieldName+"'>");
							dmRecords.append("<input type='hidden' id='req_2_"+dspqfm.getDspqFieldId()+"' value="+dspqfm.getIsRequired()+">");
							dmRecords.append("<input type='hidden' id='tooltip_2_"+dspqfm.getDspqFieldId()+"' >");
							dmRecords.append("<input type='hidden' id='status_2_"+dspqfm.getDspqFieldId()+"' value='A'>");
							
							///Internal Transfer hidden Filed
							dmRecords.append("<input type='hidden' id='applicantType_3_"+dspqfm.getDspqFieldId()+"' value='E'>");
							dmRecords.append("<input type='hidden' id='field_3_"+dspqfm.getDspqFieldId()+"'  value='"+fieldName+"'>");
							dmRecords.append("<input type='hidden' id='req_3_"+dspqfm.getDspqFieldId()+"' value="+dspqfm.getIsRequired()+">");
							dmRecords.append("<input type='hidden' id='tooltip_3_"+dspqfm.getDspqFieldId()+"' >");
							dmRecords.append("<input type='hidden' id='status_3_"+dspqfm.getDspqFieldId()+"' value='A'/>");
							
							//row end
							dmRecords.append("<input type='hidden' id='isRequiredTest"+dspqfm.getDspqFieldId()+"' value='"+dspqfm.getIsRequired()+"'>");
							
						
						}
					}
					////////////////////// TM Additional Fields///////////////////////////
					//dmRecords.append("<div class='row hide' id='TMAdditionalSec"+dspqSec.getSectionId()+"' style='font-weight:bold;'><div class='col-sm-12 col-md-12' style='padding:4px; padding-left:25px;'>TM Additional Fields</div></div>");
					dspqAdditionalFields = sectionAdditionalIds.get(dspqSec.getSectionId());
					if(dspqAdditionalFields!=null && dspqAdditionalFields.size()>0)
					{
					for(DspqFieldMaster dspqfm : dspqAdditionalFields){
						String ekeyValue = propertyKey(dspqfm.getDspqFieldName(),dspqfm.getDspqFieldId(),"Field");
						TMAddFieldList.append(","+dspqfm.getDspqFieldId());
						String fieldName="";
						if(dspqfm.getDspqFieldId()==154 || dspqfm.getDspqFieldId()==160 || dspqfm.getDspqFieldId()==161)
						{
							fieldName=dspqfm.getDspqFieldName()+" "+districtName1;
							ekeyValue=ekeyValue+" "+districtName1;
						}
						else
							fieldName=dspqfm.getDspqFieldName();
						
						dmRecords.append("<div ><div class='external show'><div class='externalAdditional"+dspqfm.getDspqFieldId()+" hide'><div class='row top5'>");	
						dmRecords.append("<input type='hidden' id='dspqFieldId"+dspqfm.getDspqFieldId()+"' value='"+dspqfm.getDspqFieldId()+"'><input type='hidden' id='dspqSecIdForSec"+dspqfm.getDspqFieldId()+"' value='"+dspqSec.getSectionId()+"'>");
						dmRecords.append("<input type='hidden' id='TMAddTMDef"+dspqfm.getDspqFieldId()+"' value='Additional'>");
						
						dmRecords.append("<div class='col-sm-9 col-md-9  manageWidth'>");
						dmRecords.append("<span id='1displayField"+dspqfm.getDspqFieldId()+"'>"+fieldName +"</span><span href='#'  data-toggle='tooltip' class='tempToolTip' data-placement='top' title=\""+ekeyValue+"\"> <img src='images/qua-icon.png' alt=''></span>");
						dmRecords.append(" <span href='#'  data-toggle='tooltip' class='tempToolTip' data-placement='top' title=\"Data Type: "+dspqfm.getFieldDataType()+" "+loadDataTypeWithList(dspqfm.getDspqFieldId())+"\"><img src='images/qua-icon.png' alt=''></span>");
						dmRecords.append("</div>");
						
						
						dmRecords.append("<div class='col-sm-1 col-md-1 top5' style='padding-left:8px;'><input type='hidden' id='AD"+dspqfm.getDspqFieldId()+"' value='A'/>");
						dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Edit' onclick='showFieldDiv("+dspqfm.getDspqFieldId()+");'><i class='fa fa-pencil-square-o fa-lg'></i></a>");
						dmRecords.append("</div>");				
						
						
						dmRecords.append("<div class='col-sm-2 col-md-2' style='padding-left:1px;'>");
						if(dspqfm.getIsRequired()==0)
							dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_1_"+dspqfm.getDspqFieldId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper'>?</label> <label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' onclick='requireOptionalField("+dspqfm.getDspqFieldId()+",1);' id='reqOp_1_"+dspqfm.getDspqFieldId()+"'>O</label> <label title='Active' onclick='actDeacApplicant("+dspqfm.getDspqFieldId()+",1);' class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='1_"+dspqfm.getDspqFieldId()+"' >A</label>");
						else
							dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_1_"+dspqfm.getDspqFieldId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper'>?</label> <label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' id='reqOp_1_"+dspqfm.getDspqFieldId()+"' title='Required' style='cursor:default;'>R</label> <label title='Active' style='cursor:default;' class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='1_"+dspqfm.getDspqFieldId()+"' >A</label>");
						dmRecords.append("</div>");
						dmRecords.append("<div class='col-sm-2 col-md-2 hide  sectionLabelInternal pleft10'>");
						if(dspqfm.getIsRequired()==0)
							dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_2_"+dspqfm.getDspqFieldId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper'>?</label> <label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' onclick='requireOptionalField("+dspqfm.getDspqFieldId()+",2);' id='reqOp_2_"+dspqfm.getDspqFieldId()+"'>O</label> <label title='Active' onclick='actDeacApplicant("+dspqfm.getDspqFieldId()+",2);' class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='2_"+dspqfm.getDspqFieldId()+"'>A</label>");
						else
							dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_2_"+dspqfm.getDspqFieldId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper'>?</label> <label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' id='reqOp_2_"+dspqfm.getDspqFieldId()+"' title='Required' style='cursor:default;'>R</label> <label title='Active' style='cursor:default;' class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='2_"+dspqfm.getDspqFieldId()+"'>A</label>");
						dmRecords.append("</div>");
						dmRecords.append("<div class='col-sm-2 col-md-2 hide  sectionLabelInternalTransfer pleft10' >");
						if(dspqfm.getIsRequired()==0)
							dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_3_"+dspqfm.getDspqFieldId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper'>?</label> <label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' onclick='requireOptionalField("+dspqfm.getDspqFieldId()+",3);' id='reqOp_3_"+dspqfm.getDspqFieldId()+"'>O</label>  <label title='Active' onclick='actDeacApplicant("+dspqfm.getDspqFieldId()+",3);' class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='3_"+dspqfm.getDspqFieldId()+"' >A</label>");
						else
							dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' id='fieldLabel_3_"+dspqfm.getDspqFieldId()+"' title='"+dspqfm.getDspqFieldName()+"'>L</label> <label class='circleIns helper'>?</label> <label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' id='reqOp_3_"+dspqfm.getDspqFieldId()+"' title='Required' style='cursor:default;'>R</label>  <label title='Active' style='cursor:default;' class='circleIns activeIcon tempToolTip' data-toggle='tooltip' data-placement='top' id='3_"+dspqfm.getDspqFieldId()+"' >A</label>");
						dmRecords.append("</div>");
						
					/*	dmRecords.append("<div class='col-sm-1 col-md-1 top10'><input type='hidden' id='AD"+dspqfm.getDspqFieldId()+"' value='A'/>");
					    dmRecords.append("<a href='javascript:void(0);' title='Active' value='0' id='additionalFieldAI"+dspqfm.getDspqFieldId()+"' onclick='activeInactiveAdditionalFields("+dspqfm.getDspqFieldId()+","+dspqSec.getSectionId()+",array"+dspqSec.getSectionId()+");' class='fa fa-check-circle fa-lg' style='color:green;width: 35px;'></i></a>");
						dmRecords.append("</div>");*/
						
						
						
						
						dmRecords.append("</div></div></div>");
						
						dmRecords.append("</div>");
						
						///External hidden Filed
						dmRecords.append("<input type='hidden' id='additionalFieldIDD"+dspqfm.getDspqFieldId()+"' value='0'>");
						dmRecords.append("<input type='hidden' id='fieldIDD"+dspqfm.getDspqFieldId()+"' value='"+dspqfm.getDspqFieldName()+"'>");
						dmRecords.append("<input type='hidden' id='applicantType_1_"+dspqfm.getDspqFieldId()+"' value='E'>");
						dmRecords.append("<input type='hidden' id='field_1_"+dspqfm.getDspqFieldId()+"'  value='"+dspqfm.getDspqFieldName()+"'>");
						dmRecords.append("<input type='hidden' id='req_1_"+dspqfm.getDspqFieldId()+"' value="+dspqfm.getIsRequired()+">");
						dmRecords.append("<input type='hidden' id='tooltip_1_"+dspqfm.getDspqFieldId()+"'>");
						dmRecords.append("<input type='hidden' id='status_1_"+dspqfm.getDspqFieldId()+"' value='A'>");
						
						///Internal hidden Filed
						dmRecords.append("<input type='hidden' id='applicantType_2_"+dspqfm.getDspqFieldId()+"' value='E'>");
						dmRecords.append("<input type='hidden' id='field_2_"+dspqfm.getDspqFieldId()+"'  value='"+dspqfm.getDspqFieldName()+"'>");
						dmRecords.append("<input type='hidden' id='req_2_"+dspqfm.getDspqFieldId()+"' value="+dspqfm.getIsRequired()+">");
						dmRecords.append("<input type='hidden' id='tooltip_2_"+dspqfm.getDspqFieldId()+"' >");
						dmRecords.append("<input type='hidden' id='status_2_"+dspqfm.getDspqFieldId()+"' value='A'>");
						
						///Internal Transfer hidden Filed
						dmRecords.append("<input type='hidden' id='applicantType_3_"+dspqfm.getDspqFieldId()+"' value='E'>");
						dmRecords.append("<input type='hidden' id='field_3_"+dspqfm.getDspqFieldId()+"'  value='"+dspqfm.getDspqFieldName()+"'>");
						dmRecords.append("<input type='hidden' id='req_3_"+dspqfm.getDspqFieldId()+"' value="+dspqfm.getIsRequired()+">");
						dmRecords.append("<input type='hidden' id='tooltip_3_"+dspqfm.getDspqFieldId()+"' >");
						dmRecords.append("<input type='hidden' id='status_3_"+dspqfm.getDspqFieldId()+"' value='A'/>");
						
						dmRecords.append("<input type='hidden' id='isRequiredTest"+dspqfm.getDspqFieldId()+"' value='"+dspqfm.getIsRequired()+"'>");
						dmRecords.append("<input type='hidden' id='dependantField_"+dspqfm.getDspqFieldId()+"' value='"+dspqfm.getIsDependantField()+"'>");
					  }
				    }
					
					// TeacherMatch Custom Fields
					List<DistrictSpecificPortfolioQuestions> dspqQuestionsList = null;
					dspqQuestionsList = dspqQuestionsMap.get(dspqSec.getSectionId());
					
					String questionInstruction = null;
					String question = null;
					Integer cnt = 0;
					int counter = 1;
					String shortName = null;
					dmRecords.append("<div id='New"+dspqSec.getSectionId()+"'>");
					if(dspqQuestionsList!=null && dspqQuestionsList.size()>0){
					//dmRecords.append("<div class='row' id='TMAdditionalSec"+dspqSec.getSectionId()+"' style='width:100%; font-weight:bold;'><div class='col-sm-12 col-md-12' style='padding:9px; padding-left:25px;'>TeacherMatch Custom Fields</div></div>");
					List<DistrictSpecificPortfolioOptions> questionOptionsList = null;					
					
					Integer valOld=0;
					Integer oldQuestionId=0;
					Integer closeDiv=0;
					String Custom1="",Custom2="",Custom3="",displayCustomField="";	
					
						for(DistrictSpecificPortfolioQuestions dspqQuestion : dspqQuestionsList){
							
						//boolean test=dspqRouterDAO.getExistsQuestionId(dspqQuestion.getParentQuestionId());
						
						if(oldQuestionId.equals(dspqQuestion.getParentQuestionId()))
							valOld=dspqQuestion.getParentQuestionId();
						else
						{
							List<DistrictSpecificPortfolioQuestions> dspqList=null;
							dspqList = districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestionByParentQuestionId(dspqQuestion.getParentQuestionId());
							if(dspqList!=null && dspqList.size()>0)
							{
								for(DistrictSpecificPortfolioQuestions dspqQuestion1 : dspqList){
									if(Custom1.equalsIgnoreCase(""))
										Custom1=dspqQuestion1.getQuestion();
									else if(Custom2.equalsIgnoreCase(""))
										Custom2=dspqQuestion1.getQuestion();
									else
										Custom3=dspqQuestion1.getQuestion();
								}
							}
							valOld=0;
						}
							
							if(valOld.equals(0))
							{
								if(Custom1.equalsIgnoreCase(Custom2) && Custom2.equalsIgnoreCase(Custom3) && Custom3.equalsIgnoreCase(Custom1))
									displayCustomField=Custom1;
								else 
									displayCustomField=Custom1+"/"+Custom2+"/"+Custom3;
								oldQuestionId=dspqQuestion.getParentQuestionId();
							shortName = dspqQuestion.getQuestionTypeMaster().getQuestionTypeShortName();
							
							questionInstruction = dspqQuestion.getQuestionInstructions()==null?"":dspqQuestion.getQuestionInstructions();
							question 			= dspqQuestion.getQuestion()==null?"":dspqQuestion.getQuestion();
							Custom1="";Custom2="";Custom3="";
							dmRecords.append("<input type='hidden' id='customField"+dspqQuestion.getQuestionId()+"' value='"+dspqQuestion.getQuestion()+"'>");
							
							dmRecords.append("<div style='margin-bottom:5px;'>");
							
							/*if(!questionInstruction.equals("")){
								dmRecords.append("<div class='row'>");
									dmRecords.append("<div class='col-sm-12 col-md-12'>");
									dmRecords.append("Instructions: ");
									dmRecords.append(""+Utility.getUTFToHTML(questionInstruction)+"");
									dmRecords.append("</div>");
								dmRecords.append("</div>");
							}*/
							
							if(!question.equals("")){
								String required="";
								if(dspqQuestion.getIsRequired()==1)
									required="<label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' title='Required' onclick='activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",0,"+dspqSec.getSectionId()+",\"Req\")'>R</label>";
								else
									required="<label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' title='Optional' onclick='activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",1,"+dspqSec.getSectionId()+",\"Req\")'>O</label>";
								dmRecords.append("<div class='row top5'>");
									dmRecords.append("<div class='col-sm-9 col-md-9 manageWidth'>");
									dmRecords.append(displayCustomField+" <span href='#'  data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+question+" '> <img src='images/qua-icon.png' alt=''></span>");
									dmRecords.append("</div>");
									
									dmRecords.append("<div class='col-sm-1 col-md-1 top5' style='padding-left:8px;'>");
									dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Edit' id='dspqActiveDeactive"+counter+"' onclick='return editCustomField("+dspqQuestion.getQuestionId()+")')\"><span class='fa fa-pencil-square-o fa-lg ' style='text-align:center;'></span></a>");
									dmRecords.append("</div>");
									
									dmRecords.append("<div class='col-sm-2 col-md-2' style='padding-left:1px;'>");
									if(dspqQuestion.getApplicantType().equalsIgnoreCase("E") && dspqQuestion.getStatus().equalsIgnoreCase("A"))
										dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' title='"+dspqQuestion.getQuestion()+"'>L</label> <label class='circleIns helper'>?</label> "+required+" <a href='javascript:void(0);' title='Active' class='tempToolTip' data-toggle='tooltip' data-placement='top' id='dspqActiveDeactive"+counter+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"I\","+dspqSec.getSectionId()+",\"Act\")')\"><label class='circleIns activeIcon'>A</label></a>");
									else
										dmRecords.append(" <label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' title='"+dspqQuestion.getQuestion()+"'>L</label> <label class='circleIns helper'>?</label> "+required+" <a href='javascript:void(0);' title='Inactive' class='tempToolTip' data-toggle='tooltip' data-placement='top' id='dspqActiveDeactive"+counter+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"A\","+dspqSec.getSectionId()+",\"Act\")')\"><label class='circleIns inactiveIcon'>X</label></a>");
									dmRecords.append("</div>");
							}
							}
							else
							{
								closeDiv++;
									
									if(dspqQuestion.getApplicantType().equalsIgnoreCase("I"))
									{
										String required="";
										if(dspqQuestion.getIsRequired()==1)
											required="<label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' title='Required' onclick='activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",0,"+dspqSec.getSectionId()+",\"Req\")'>R</label>";
										else
											required="<label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' title='Optional' onclick='activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",1,"+dspqSec.getSectionId()+",\"Req\")'>O</label>";
										dmRecords.append("<div class='col-sm-2 col-md-2 hide sectionLabelInternal pleft10' >");
										if(dspqQuestion.getApplicantType().equalsIgnoreCase("I") && dspqQuestion.getStatus().equalsIgnoreCase("A"))
											dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' title='"+dspqQuestion.getQuestion()+"'>L</label> <label class='circleIns helper'>?</label> "+required+"  <a href='javascript:void(0);' title='Active' class='tempToolTip' data-toggle='tooltip' data-placement='top' id='dspqActiveDeactive"+counter+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"I\","+dspqSec.getSectionId()+",\"Act\")')\"><label class='circleIns activeIcon'>A</label></a>");
										else
											dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' title='"+dspqQuestion.getQuestion()+"'>L</label> <label class='circleIns helper'>?</label> "+required+"  <a href='javascript:void(0);' title='Inactive' class='tempToolTip' data-toggle='tooltip' data-placement='top' id='dspqActiveDeactive"+counter+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"A\","+dspqSec.getSectionId()+",\"Act\")')\"><label class='circleIns inactiveIcon'>X</label></a>");
										dmRecords.append("</div>");
									}
									else
									{
										String required="";
										if(dspqQuestion.getIsRequired()==1)
											required="<label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' title='Required' onclick='activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",0,"+dspqSec.getSectionId()+",\"Req\")'>R</label>";
										else
											required="<label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' title='Optional' onclick='activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",1,"+dspqSec.getSectionId()+",\"Req\")'>O</label>";
										dmRecords.append("<div class='col-sm-2 col-md-2 hide sectionLabelInternalTransfer pleft10'>");
										if(dspqQuestion.getApplicantType().equalsIgnoreCase("T") && dspqQuestion.getStatus().equalsIgnoreCase("A"))
											dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' title='"+dspqQuestion.getQuestion()+"'>L</label> <label class='circleIns helper'>?</label> "+required+"  <a href='javascript:void(0);' title='Active' class='tempToolTip' data-toggle='tooltip' data-placement='top' id='dspqActiveDeactive"+counter+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"I\","+dspqSec.getSectionId()+",\"Act\")')\"><label class='circleIns activeIcon'>A</label></a>");
										else
											dmRecords.append("<label class='circleInsWhite tempToolTip' data-toggle='tooltip' data-placement='top'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' data-placement='top' title='"+dspqQuestion.getQuestion()+"'>L</label> <label class='circleIns helper'>?</label> "+required+"  <a href='javascript:void(0);' title='Inactive' class='tempToolTip' data-toggle='tooltip' data-placement='top' id='dspqActiveDeactive"+counter+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"A\","+dspqSec.getSectionId()+",\"Act\")')\"><label class='circleIns inactiveIcon'>X</label></a>");
										dmRecords.append("</div>");
									}
									if(closeDiv.equals(2))
									{
										closeDiv=0;
										dmRecords.append("</div>");
										dmRecords.append("</div>");
										dmRecords.append("<script>$('#dspqActiveDeactive"+counter+"').tooltip();</script>");
									}
								
							}														
							
							cnt++;
							counter++;
						}
						
					}
					// TeacherMatch Custom Fields Ends
					dmRecords.append("</div>");
					dmRecords.append("</div>");
					////////////////////////////////  Ends Hide Show Section///////////////////////////////////
					
					dmRecords.append("</div>");					
					dmRecords.append("</div>");
					dmRecords.append("</div>");
					dmRecords.append("</div>");
					if(userMaster.getEntityType()==1){
					dmRecords.append("<div class='col-sm-1 col-md-1 pleftright top20' style='width: 0%;  margin-left: -10px; '>");
					dmRecords.append("<div class='row'>");
					if(sectionStartIndex.equalsIgnoreCase(dspqSec.getSectionId().toString())){
						dmRecords.append("<div class='col-sm-12 col-md-12'><i id=up"+dspqSec.getSectionId()+" class='fa fa-chevron-up fa-2x disableArrow' onclick='upArrow("+dspqSec.getSectionId()+")'></i></div>");
					}
					else{
						dmRecords.append("<div class='col-sm-12 col-md-12'><i id=up"+dspqSec.getSectionId()+" class='fa fa-chevron-up fa-2x enableArrow' onclick='upArrow("+dspqSec.getSectionId()+")'></i></div>");
					}
					if(sectionEndIndex.equalsIgnoreCase(dspqSec.getSectionId().toString())){
						dmRecords.append("<div class='col-sm-12 col-md-12'><i id=down"+dspqSec.getSectionId()+" class='fa fa-chevron-down fa-2x disableArrow' onclick='downArrow("+dspqSec.getSectionId()+")'></i></div>");
					}						
					else{
						dmRecords.append("<div class='col-sm-12 col-md-12'><i id=down"+dspqSec.getSectionId()+" class='fa fa-chevron-down fa-2x enableArrow' onclick='downArrow("+dspqSec.getSectionId()+")'></i></div>");
					}						
					dmRecords.append("</div>");
					dmRecords.append("</div>");
					}
					dmRecords.append("</div>");
					
				//}
					
					
					
					sb.append(str).append("#collapse"+count);
					str =",";
					count++;
					
					dspqFields = new ArrayList<DspqFieldMaster>();
				}
				System.out.println("TMAddFieldList.toString() " +TMAddFieldList.toString());
				dmRecords.append("</div>");
				dmRecords.append("<input type='hidden' id='sectionList' value="+sectionList+">");
				dmRecords.append("<input type='hidden' id='fieldList' value="+fieldList+">");
				dmRecords.append("<input type='hidden' id='TMAddFieldList' value="+TMAddFieldList.toString()+">");
				dmRecords.append("@@@"+sb.toString());
				System.out.println(" sb.toString() :: "+sb.toString());
				sectionIds.clear();
			}
			
	}catch (Exception e) {
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	public List<DspqFieldMaster> getFieldIdBySec(String secId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }		
		List<DspqFieldMaster>dspqFieldMastersList =null;
		DspqSectionMaster dspqSectionMaster=dspqSectionMasterDAO.findById(Integer.parseInt(secId), false, false);
		Criterion criterion=Restrictions.eq("dspqSectionMaster", dspqSectionMaster);
		dspqFieldMastersList=dspqFieldMasterDAO.findByCriteria(criterion);
		return dspqFieldMastersList;
	}
	public String saveDspqFieldsValues(Integer districtId,DspqRouter[] dspqConf1,String portfolioName,Integer groupId,Integer dspqPortfolioNameId,String checkExistPortfolio)
	{
 		System.out.println("::::::::::::: saveDspqFieldsValues :::::::::::::::::: "); 		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }		
		SessionFactory factory = dspqRouterDAO.getSessionFactory();
	    StatelessSession statelessSession=factory.openStatelessSession();
	    Transaction transaction = statelessSession.beginTransaction();
	    String clonePortID="";
		try{			
			DistrictMaster districtMaster 	= null;	
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}			
			if(districtId !=null)
				districtMaster = districtMasterDAO.findById(districtId, false, false);
			DspqPortfolioName dspqPortfolioName2=new DspqPortfolioName();
			DspqPortfolioName dspqPortfolioName=new DspqPortfolioName();
			List<DspqRouter> routersList=null;
			Map<String,DspqRouter> dsqpRouListMap = new HashMap<String, DspqRouter>();
			Map<String,DspqRouter> dsqpRouSecListMap = new HashMap<String, DspqRouter>();
			System.out.println("portfolioName "+portfolioName);
			//System.out.println("districtMaster "+districtMaster.getDistrictName());
			dspqPortfolioName2=dspqPortfolioNameDAO.findByDistrictAndPortfolioName(districtMaster, portfolioName);
			
			if(!checkExistPortfolio.equalsIgnoreCase("clone"))
			{
				dspqPortfolioName2=new DspqPortfolioName();
				dspqPortfolioName2=null;
				dspqPortfolioName=dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
				routersList =dspqRouterDAO.getDspqFieldListByDspqPortfolioNameId(dspqPortfolioName);		
			
				if(routersList!=null && routersList.size()>0){
					for(DspqRouter dspqRouter : routersList){
						if(dspqRouter.getDspqFieldMaster()!=null)
							dsqpRouListMap.put(dspqRouter.getDspqPortfolioName().getDspqPortfolioNameId()+"##"+dspqRouter.getDspqFieldMaster().getDspqFieldId()+"##"+dspqRouter.getApplicantType(), dspqRouter);
						if(dspqRouter.getDspqSectionMaster()!=null)							
							dsqpRouSecListMap.put(dspqRouter.getDspqPortfolioName().getDspqPortfolioNameId()+"##"+dspqRouter.getDspqSectionMaster().getSectionId()+"##"+dspqRouter.getApplicantType(), dspqRouter);
					}
				}

				if(dspqPortfolioName!=null && !dspqPortfolioName.getPortfolioName().equalsIgnoreCase(portfolioName))
				{
					dspqPortfolioName2=new DspqPortfolioName();
					dspqPortfolioName2=dspqPortfolioNameDAO.findByDistrictAndPortfolioName(districtMaster, portfolioName);
				}
			}
			if(dspqPortfolioName2==null)
			{
				if(checkExistPortfolio.equalsIgnoreCase("clone"))
				{
					clonePortID =saveClone(dspqPortfolioNameId,portfolioName);
					dsqpRouListMap = new HashMap<String, DspqRouter>();
					dsqpRouSecListMap = new HashMap<String, DspqRouter>();
					dspqPortfolioName=new DspqPortfolioName();
					dspqPortfolioName=dspqPortfolioNameDAO.findById(Integer.parseInt(clonePortID), false, false);
					routersList =dspqRouterDAO.getDspqFieldListByDspqPortfolioNameId(dspqPortfolioName);
					if(routersList!=null && routersList.size()>0){
						for(DspqRouter dspqRouter : routersList){
							if(dspqRouter.getDspqFieldMaster()!=null)
								dsqpRouListMap.put(dspqRouter.getDspqPortfolioName().getDspqPortfolioNameId()+"##"+dspqRouter.getDspqFieldMaster().getDspqFieldId()+"##"+dspqRouter.getApplicantType(), dspqRouter);
							if(dspqRouter.getDspqSectionMaster()!=null)							
								dsqpRouSecListMap.put(dspqRouter.getDspqPortfolioName().getDspqPortfolioNameId()+"##"+dspqRouter.getDspqSectionMaster().getSectionId()+"##"+dspqRouter.getApplicantType(), dspqRouter);
						}
					}
				}				
				
					dspqPortfolioName.setPortfolioName(portfolioName);
					dspqPortfolioName.setDistrictMaster(districtMaster);
					dspqPortfolioName.setStatus("A");
					dspqPortfolioName.setCreatedDateTime(new Date());					
					transaction = statelessSession.beginTransaction();					
					statelessSession.update(dspqPortfolioName);
					
					DspqRouter dspqRouter = new DspqRouter();
					DspqRouter dspqRouterChk = new DspqRouter();
					for (DspqRouter dspqVal : dspqConf1) 
					{
						boolean chkDup=false;
						dspqRouter.setDspqSectionMaster(dspqVal.getDspqSectionMaster());
						dspqRouter.setDspqFieldMaster(dspqVal.getDspqFieldMaster());
						dspqRouter.setDspqPortfolioName(dspqPortfolioName);
						dspqRouter.setDisplayName(dspqVal.getDisplayName());
						dspqRouter.setIsRequired(dspqVal.getIsRequired());
						dspqRouter.setApplicantType(dspqVal.getApplicantType());
						//System.out.println("dspqVal.getNumberRequired() "+dspqVal.getNumberRequired());
						dspqRouter.setNumberRequired(dspqVal.getNumberRequired());
						dspqRouter.setInstructions(dspqVal.getInstructions());
						dspqRouter.setTooltip(dspqVal.getTooltip());
						dspqRouter.setStatus(dspqVal.getStatus());
						dspqRouter.setApplicantTypeStatus(dspqVal.getApplicantTypeStatus());
						dspqRouter.setOthersAttribute(dspqVal.getOthersAttribute());
						dspqRouter.setSectionOrder(dspqVal.getSectionOrder());
						dspqRouter.setCreatedDateTime(new Date());
						if(routersList!=null)
						{
							if(dspqVal.getDspqSectionMaster()!=null)
							{				
								System.out.println("dspqPortfolioName.()"+dspqPortfolioName.getDspqPortfolioNameId());
								dspqRouterChk = dsqpRouSecListMap.put(dspqPortfolioName.getDspqPortfolioNameId()+"##"+dspqVal.getDspqSectionMaster().getSectionId()+"##"+dspqRouter.getApplicantType(), dspqRouter);							
								if(dspqRouterChk!=null)
								{
									dspqRouter.setDspqRouterId(dspqRouterChk.getDspqRouterId());
									chkDup=true;
								}								
							}
							else if(dspqVal.getDspqFieldMaster()!=null)
							{
								dspqRouterChk = dsqpRouListMap.put(dspqPortfolioName.getDspqPortfolioNameId()+"##"+dspqVal.getDspqFieldMaster().getDspqFieldId()+"##"+dspqRouter.getApplicantType(), dspqRouter);
								if(dspqRouterChk!=null)
								{
									dspqRouter.setDspqRouterId(dspqRouterChk.getDspqRouterId());
									chkDup=true;
								}
							}						
						}
						transaction = statelessSession.beginTransaction();
						if(chkDup==true)
							statelessSession.update(dspqRouter);
						else
							statelessSession.insert(dspqRouter);
						}	
					}
			
			else
				return "Exists";
				
			transaction.commit();
		}
			catch (Exception e) {
			e.printStackTrace();
	}finally{
		statelessSession.close();
	}
		//return dmRecords.toString();
	return clonePortID;
}	
	public boolean activateDeactivateDspq(Integer dspqPortfolioNameId,String status)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try{
			DspqPortfolioName dspqPortfolioName=dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
			dspqPortfolioName.setStatus(status);			
			dspqPortfolioNameDAO.makePersistent(dspqPortfolioName);
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public List<DspqRouter> editDspq(Integer dspqPortfolioNameId)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		List<DspqRouter> dspqRouterList=null;
		DspqPortfolioName dspqPortfolioName=null;
		System.out.println("dspqPortfolioNameId " +dspqPortfolioNameId);
		if(dspqPortfolioNameId!=0 && dspqPortfolioNameId!=null)
		{			
			try{
				dspqPortfolioName=dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
				dspqRouterList = dspqRouterDAO.getDspqFieldListByDspqPortfolioNameId(dspqPortfolioName);
			}catch (Exception e) 
			{
				e.printStackTrace();				
			}
		}
		System.out.println("dspqRouterList.size "+dspqRouterList.size());
		return dspqRouterList;
	}
	public DspqPortfolioName getDspqPortfolioName(Integer dspqPortfolioNameId)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		DspqPortfolioName dspqPortfolioName=null;
		if(dspqPortfolioNameId!=0 && dspqPortfolioNameId!=null)
		{			
			try{
				dspqPortfolioName=dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);				
			}catch (Exception e) 
			{
				e.printStackTrace();				
			}
		}
		return dspqPortfolioName;
	}	
	public DistrictSpecificPortfolioQuestions saveDistrictQuestionFromDspq(DistrictSpecificPortfolioQuestions districtSpecificQuestion,DistrictMaster districtMaster,DistrictSpecificPortfolioOptions[] optionsForDistrictSpecificQuestions,Integer isRequiredVal,String multiSelectValueId,Integer portfolioNameId)
	{

		System.out.println("::::::::::::::::::::::::saveDistrictQuestionFromDspq ::::::::::"+districtMaster.getDistrictId());
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		
		UserMaster user = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestionsN1=null;
		districtSpecificPortfolioQuestionsN1=districtSpecificQuestion;
		SessionFactory factory = districtSpecificPortfolioQuestionsDAO.getSessionFactory();
	    StatelessSession statelessSession=factory.openStatelessSession();
	    Transaction transaction = statelessSession.beginTransaction();
	    transaction = statelessSession.beginTransaction();
	    Integer quesID=0;
		try{
	
		    List<DistrictSpecificPortfolioOptions> qopt = null;
		    	if(portfolioNameId!=null)
		    	{
		    		DspqPortfolioName dspqPortfolioName=dspqPortfolioNameDAO.findById(portfolioNameId, false, false);
		    	    JobCategoryMaster jobCategoryMaster=null;
		    	    /* String[] multiSelectId =null;
		    	    multiSelectId	= multiSelectValueId.trim().split(",");
				   for(int i=0;i<multiSelectId.length;i++)
					{*/
					   for(int j=1;j<=3;j++)
						{
						   String applicantType="";
						   if(j==1)
							   applicantType="E";
						   if(j==2)
							   applicantType="I";
						   if(j==3)
							   applicantType="T";
						//jobCategoryMaster = new JobCategoryMaster();
						//System.out.println("Catrgory Id <<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+ multiSelectId[i]);
						//jobCategoryMaster.setJobCategoryId(Integer.parseInt(multiSelectId[i]));
						districtSpecificQuestion.setIsRequired(isRequiredVal);
						districtSpecificQuestion.setUserMaster(user);
						districtSpecificQuestion.setJobCategoryMaster(null);
						districtSpecificQuestion.setIsCustom(true);
						districtSpecificQuestion.setDspqPortfolioName(dspqPortfolioName);
						districtSpecificQuestion.setApplicantType(applicantType);
						districtSpecificQuestion.setCreatedDateTime(new Date());	
						districtSpecificQuestion.setStatus("A");
						districtSpecificQuestion.setDistrictMaster(districtMaster);
						
						transaction = statelessSession.beginTransaction();
						statelessSession.insert(districtSpecificQuestion);
						if(j==1)
						{
							quesID=districtSpecificQuestion.getQuestionId();
							transaction = statelessSession.beginTransaction();
							districtSpecificQuestion.setParentQuestionId(districtSpecificQuestion.getQuestionId());
							statelessSession.update(districtSpecificQuestion);
						}
						else
						{
							transaction = statelessSession.beginTransaction();
							districtSpecificQuestion.setParentQuestionId(quesID);
							statelessSession.update(districtSpecificQuestion);
						}
						
						for (DistrictSpecificPortfolioOptions optionsForDistrictSpecificQuestions2 : optionsForDistrictSpecificQuestions) {
							optionsForDistrictSpecificQuestions2.setDistrictSpecificPortfolioQuestions(districtSpecificQuestion);
							optionsForDistrictSpecificQuestions2.setCreatedDateTime(new Date());
							optionsForDistrictSpecificQuestions2.setUserMaster(user);
							optionsForDistrictSpecificQuestions2.setOpenText(false);
							optionsForDistrictSpecificQuestions2.setStatus("A");
							transaction = statelessSession.beginTransaction();
							statelessSession.insert(optionsForDistrictSpecificQuestions2);
						}										
							
						/*}*/
					}
				   transaction.commit();
				   
		    	}
		    	else
		    	{	
		    		for(int j=1;j<=3;j++)
					{
		    			
					   String applicantType="";
					   if(j==1)
						   applicantType="E";
					   if(j==2)
						   applicantType="I";
					   if(j==3)
						   applicantType="T";
					districtSpecificQuestion=null;
					districtSpecificQuestion=districtSpecificPortfolioQuestionsN1;
					districtSpecificQuestion.setIsRequired(isRequiredVal);
					districtSpecificQuestion.setUserMaster(user);
					districtSpecificQuestion.setApplicantType(applicantType);
					districtSpecificQuestion.setIsCustom(true);
					districtSpecificQuestion.setStatus("A");
					districtSpecificQuestion.setCreatedDateTime(new Date());	
					districtSpecificQuestion.setDistrictMaster(districtMaster);
					transaction = statelessSession.beginTransaction();
					statelessSession.insert(districtSpecificQuestion);
					
					if(j==1)
					{
						quesID=districtSpecificQuestion.getQuestionId();
						transaction = statelessSession.beginTransaction();
						districtSpecificQuestion.setParentQuestionId(districtSpecificQuestion.getQuestionId());
						statelessSession.update(districtSpecificQuestion);
					}
					else
					{
						transaction = statelessSession.beginTransaction();
						districtSpecificQuestion.setParentQuestionId(quesID);
						statelessSession.update(districtSpecificQuestion);
					}

					for (DistrictSpecificPortfolioOptions optionsForDistrictSpecificQuestions2 : optionsForDistrictSpecificQuestions) {
						optionsForDistrictSpecificQuestions2.setDistrictSpecificPortfolioQuestions(districtSpecificQuestion);
						optionsForDistrictSpecificQuestions2.setCreatedDateTime(new Date());
						optionsForDistrictSpecificQuestions2.setUserMaster(user);
						optionsForDistrictSpecificQuestions2.setOpenText(false);
						optionsForDistrictSpecificQuestions2.setStatus("A");
						transaction = statelessSession.beginTransaction();
						statelessSession.insert(optionsForDistrictSpecificQuestions2);
					}
					}
		    		transaction.commit();
		    		
		    	}
		    }catch(Exception e)
		    {e.printStackTrace();}		   
		    finally{
				statelessSession.close();
			}	
		return districtSpecificQuestion;
	 
	}
	
	public String displayQuesList(Integer districtId)
	{
		System.out.println(":::::::::::: displayQuesList :::::::::::::"+districtId);
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		
		StringBuffer sb =new StringBuffer();
		try{
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,22,"assessmentquestions.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<DistrictSpecificPortfolioQuestions> districtSpecificQuestionList =null;
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			Criterion criterion = Restrictions.eq("districtMaster", districtMaster);
			districtSpecificQuestionList = districtSpecificPortfolioQuestionsDAO.findByCriteria(criterion);
			
			int i=0;
			List<DistrictSpecificPortfolioOptions> questionOptionsList = null;
			String questionInstruction = null;
			String shortName = "";
			
			sb.append("");
			sb.append("<table id='dspqQuestionTable' width='100%' border='0'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th valign='top'>Question</th>");
			sb.append("<th valign='top'>Is Required</th>");
			sb.append("<th valign='top'>Status</th>");	
			//sb.append("<th valign='top'>Options</th>");				
			sb.append("</tr>");
			sb.append("</thead>");
			int counter = 0;
			if(districtSpecificQuestionList!=null && districtSpecificQuestionList.size()>0){
				for (DistrictSpecificPortfolioQuestions districtSpecificQuestion : districtSpecificQuestionList) 
				{
					
					String isRequired = "Optional";
					String status = "Inactive";
					if(districtSpecificQuestion.getIsRequired()==1)
						isRequired = "Required";
					
					if(districtSpecificQuestion.getStatus().equals("A"))
						status = "Active";
					
					
					
					sb.append("<tr>");
					sb.append("<td>Question"+(++counter)+": "+districtSpecificQuestion.getQuestion()+"</td>");
					
					sb.append("<td>"+isRequired+"</td>");
					
					sb.append("<td>"+status+"</td>");
					
/*					sb.append("<td>");
					sb.append("<a href='javascript:void(0);' onclick=''>Edit</a>&nbsp;&nbsp;");
					sb.append("</td>");
*/					
					sb.append("</tr>");
					//sb.append("Question "+(++i)+": "+districtSpecificQuestion.getQuestion()+"");
					
				}
			}
			
			if(districtSpecificQuestionList.size()==0)
				sb.append("<tr id='tr1' ><td id='tr1' colspan=2>No Question found</td></tr>" );
			
	}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	
	}
	
	@Transactional(readOnly=false)
	public boolean activateDeactivateQuestion(Integer questionId,String status,String action)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try{
			DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions = districtSpecificPortfolioQuestionsDAO.findById(questionId, false, false);
			if(!action.equalsIgnoreCase("")&&action.equalsIgnoreCase("Req"))
				districtSpecificPortfolioQuestions.setIsRequired(Integer.parseInt(status));
			else
				districtSpecificPortfolioQuestions.setStatus(status);
			districtSpecificPortfolioQuestionsDAO.makePersistent(districtSpecificPortfolioQuestions);
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Transactional(readOnly=false)
	public DistrictSpecificPortfolioQuestions getDspqQuestionById(Integer questionId)
	{
		System.out.println(":::::::::::::::::::: getDspqQuestionById :::::::::::::::::::::"+questionId);
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions = new DistrictSpecificPortfolioQuestions();
		try{
			if(questionId!=null){
				districtSpecificPortfolioQuestions=districtSpecificPortfolioQuestionsDAO.findById(questionId, false, false);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("districtSpecificPortfolioQuestions "+districtSpecificPortfolioQuestions.getQuestion());
		return districtSpecificPortfolioQuestions;
	}
	
	public String getDspqQuestionTypeList(int questionId){
		StringBuffer sb = new StringBuffer();
		List<QuestionTypeMaster> questionTypeMastersList		= 	null;
		DistrictSpecificPortfolioQuestions dspqQuestion			=	null;
		List<Integer> ques = new ArrayList<Integer>();
		Integer counter = 1; 
		try 
		{
			if(""+questionId!="")
			dspqQuestion	=	districtSpecificPortfolioQuestionsDAO.findById(questionId, false, false);
			questionTypeMastersList = questionTypeMasterDAO.getActiveQuestionTypesByShortName(new String[]{"slsel","tf","ml","et","mlsel","mloet","sloet","dt","drsls","sl","UAT","UATEF","sswc"});
			System.out.println("questionTypeMastersList "+questionTypeMastersList);
				sb.append("<select id='avlbList' name='avlbList' class='span4'>");
				sb.append("<option value=''>Select Question Type</option>");
				if(questionTypeMastersList!=null && questionTypeMastersList.size() > 0)
					for(QuestionTypeMaster questionTypeLst : questionTypeMastersList){
						if(dspqQuestion!=null){
							if(dspqQuestion.getQuestionTypeMaster().getQuestionTypeId().equals(questionTypeLst.getQuestionTypeId())){
								sb.append("<option selected value='"+questionTypeLst.getQuestionTypeId()+"'>"+questionTypeLst.getQuestionType()+"</option>");
							} else {
								sb.append("<option value='"+questionTypeLst.getQuestionTypeId()+"'>"+questionTypeLst.getQuestionType()+"</option>");
							}
						} else {
							sb.append("<option value='"+questionTypeLst.getQuestionTypeId()+"'>"+questionTypeLst.getQuestionType()+"</option>");
						}
					}
			  sb.append("</select>");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String getSectionListByGroupId(int groupId){
		StringBuffer sb = new StringBuffer();
		List<DspqSectionMaster> DspqSectionMasterList	= 	null;
		DspqGroupMaster dspqGroupMaster = null;
		List<DspqSectionMaster> sectionMastersList = null;
		Integer counter = 1; 
		try 
		{
			if(""+groupId!="")
			dspqGroupMaster 	= dspqGroupMasterDAO.findById(groupId, false, false);
			sectionMastersList 	= dspqSectionMasterDAO.getDspqSectionListByGroup(dspqGroupMaster);
			
			sb.append("<select id='avlbList' name='avlbList' class='span4'>");
				sb.append("<option value=''>Select Sub Section</option>");
				if(sectionMastersList!=null && sectionMastersList.size() > 0)
					for(DspqSectionMaster sectionList : sectionMastersList){
						if(sectionList.getSectionId()!=25)
							sb.append("<option value='"+sectionList.getSectionId()+"'>"+sectionList.getSectionName()+"</option>");
					}
			  sb.append("</select>");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String getDspqGroup(int questionId){
		
		StringBuffer sb = new StringBuffer();
		List<DspqGroupMaster> dspqGroupMasterList		= 	null;
		DistrictSpecificPortfolioQuestions dspqQuestion	=	null;
		try 
		{
			if(""+questionId!="")
			dspqQuestion			=	districtSpecificPortfolioQuestionsDAO.findById(questionId, false, false);
			dspqGroupMasterList 	= 	dspqGroupMasterDAO.findAll();
				sb.append("<select id='avlbList' name='avlbList' class='span4'>");
				sb.append("<option value=''>Select Section</option>");
				if(dspqGroupMasterList!=null && dspqGroupMasterList.size() > 0)
					for(DspqGroupMaster dspqGrpList : dspqGroupMasterList){
						if(dspqQuestion!=null){
							if(dspqQuestion.getDspqGroupMaster().getGroupId().equals(dspqGrpList.getGroupId())){
								sb.append("<option selected value='"+dspqGrpList.getGroupId()+"'>"+dspqGrpList.getGroupName()+"</option>");
							} else {
								sb.append("<option value='"+dspqGrpList.getGroupId()+"'>"+dspqGrpList.getGroupName()+"</option>");
							}
						} else {
							sb.append("<option value='"+dspqGrpList.getGroupId()+"'>"+dspqGrpList.getGroupName()+"</option>");
						}
					}
			  sb.append("</select>");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String getDspqSection(int questionId,int groupId){
		StringBuffer sb = new StringBuffer();
		List<DspqSectionMaster> dspqSectionMasterList	= 	null;
		DistrictSpecificPortfolioQuestions dspqQuestion	=	null;
		DspqGroupMaster dspqGroupMaster = null;
		try 
		{
			if(""+questionId!="")
			dspqQuestion			=	districtSpecificPortfolioQuestionsDAO.findById(questionId, false, false);
			dspqGroupMaster 		= dspqGroupMasterDAO.findById(groupId, false, false);
			dspqSectionMasterList 	= dspqSectionMasterDAO.getDspqSectionListByGroup(dspqGroupMaster);
			
			sb.append("<select id='avlbList' name='avlbList' class='span4'>");
			sb.append("<option value=''>Select Section</option>");
			if(dspqSectionMasterList!=null && dspqSectionMasterList.size() > 0)
				for(DspqSectionMaster dspqSecList : dspqSectionMasterList){
					if(dspqQuestion!=null){
						if(dspqQuestion.getDspqSectionMaster().getSectionId().equals(dspqSecList.getSectionId())){
							sb.append("<option selected value='"+dspqSecList.getSectionId()+"'>"+dspqSecList.getSectionName()+"</option>");
						} else {
							sb.append("<option value='"+dspqSecList.getSectionId()+"'>"+dspqSecList.getSectionName()+"</option>");
						}
					} else {
						sb.append("<option value='"+dspqSecList.getSectionId()+"'>"+dspqSecList.getSectionName()+"</option>");
					}
				}
		  sb.append("</select>");
		} catch (Exception e) {
			e.printStackTrace();
		}
			return sb.toString();
	}
	
	@Transactional(readOnly=false)
	public JSONArray getDspqOptionById(Integer questionId)
	{
		System.out.println(":::::::::::::::::::: getDspqOptionById :::::::::::::::::::::"+questionId);
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions = new DistrictSpecificPortfolioQuestions();
		List<DistrictSpecificPortfolioOptions> districtSpecificPortfolioOptions = null;
		Criterion criteria = null;
		JSONArray dspqQuestionsArray = new JSONArray();
		JSONObject jsonQuestion = new JSONObject();
		try{
			if(questionId!=null){
				districtSpecificPortfolioQuestions=districtSpecificPortfolioQuestionsDAO.findById(questionId, false, false);
				criteria = Restrictions.eq("districtSpecificPortfolioQuestions", districtSpecificPortfolioQuestions);
				districtSpecificPortfolioOptions = districtSpecificPortfolioOptionsDAO.findByCriteria(criteria);
				if(districtSpecificPortfolioOptions.size()>0 && districtSpecificPortfolioOptions!=null){
					for (DistrictSpecificPortfolioOptions dspqOption : districtSpecificPortfolioOptions) {
						jsonQuestion.put("optionId", dspqOption.getOptionId());
						jsonQuestion.put("questionOption", dspqOption.getQuestionOption());
						jsonQuestion.put("validOption", dspqOption.getValidOption());
						dspqQuestionsArray.add(jsonQuestion);
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return dspqQuestionsArray;
	}
	public String getPortfolioIsEditable(String id){
	StringBuffer sb = new StringBuffer();
	/*List<JobOrder> jobOrderList=null;
	List<JobForTeacher> jobForTeacherList=null;
	JobForTeacher jobForTeacher=null;
	if(id!=null && id.length()>0){
		
		DspqPortfolioName dspq = dspqPortfolioNameDAO.findById(Integer.parseInt(id), false, false);			
		if(dspq!=null){
			
			if(dspq.getJobCategoryMaster()!=null)
			{
				System.out.println("dspq.getJobCategoryMaster()" +dspq.getJobCategoryMaster().getJobCategoryId());
				jobOrderList=jobOrderDAO.findByJobCategeryAndDistrict(dspq.getJobCategoryMaster(), dspq.getDistrictMaster());
				if(jobOrderList!=null && jobOrderList.size()>0)
				{
					System.out.println("jobOrderList " +jobOrderList.size());
					jobForTeacherList=jobForTeacherDAO.findJobByJobCategory(jobOrderList);
					System.out.println("jobForTeacherList "+jobForTeacherList.size());
					if(jobForTeacherList!=null && jobForTeacherList.size()>0)
						sb.append(dspq.getJobCategoryMaster().getJobCategoryName()+"##"+dspq.getJobCategoryMaster().getJobCategoryId());
					else
						return sb.append("0##"+dspq.getJobCategoryMaster().getJobCategoryId()).toString();					
				}
				else
					sb.append("0##"+dspq.getJobCategoryMaster().getJobCategoryId()).toString();
			}
			else
			{
				jobOrderList=jobOrderDAO.findByJobCategeryAndDistrict(dspq.getJobCategoryMaster(), dspq.getDistrictMaster());
				if(jobOrderList!=null && jobOrderList.size()>0)
				{
					jobForTeacherList=jobForTeacherDAO.findJobByJobCategory(jobOrderList);
					if(jobForTeacherList!=null && jobForTeacherList.size()>0)
						sb.append(dspq.getJobCategoryMaster().getJobCategoryName());
					else
						return sb.append("0##").toString();					
				}
				else
					sb.append("0##");
			}
		}
		else
			return sb.append("0##").toString();		
		
	}	*/
	return sb.toString();
	
	}
	public DistrictSpecificPortfolioQuestions saveEditDistrictQuestionFromDspq(DistrictSpecificPortfolioQuestions districtSpecificQuestion,DistrictMaster districtMaster,DistrictSpecificPortfolioOptions[] optionsForDistrictSpecificQuestions,Integer isRequiredVal,String multiSelectValueId,String applicantType,Integer parentID,Integer portfolioId)
	{

		System.out.println("::::::::::::::::::::::::saveEditDistrictQuestionFromDspq ::::::::::"+districtMaster.getDistrictId());
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		
		UserMaster user = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		if (districtSpecificQuestion.getQuestionId()==null)
		{
			districtSpecificQuestion.setCreatedDateTime(new Date());			
		}		
		try{
	
		    List<DistrictSpecificPortfolioOptions> qopt = null;
		    try
		    {
		    	if(multiSelectValueId!=null && !multiSelectValueId.equalsIgnoreCase("") && parentID ==null)
		    	{
		    		SessionFactory factory = districtSpecificPortfolioQuestionsDAO.getSessionFactory();
		    	    StatelessSession statelessSession=factory.openStatelessSession();
		    	    Transaction transaction = statelessSession.beginTransaction();
		    	    transaction = statelessSession.beginTransaction();
		    	    JobCategoryMaster jobCategoryMaster=null;
		    	    String[] multiSelectId =null;
		    	    multiSelectId	= multiSelectValueId.trim().split(",");
				   for(int i=0;i<multiSelectId.length;i++)
					{
						jobCategoryMaster = new JobCategoryMaster();
						System.out.println("Catrgory Id <<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+ multiSelectId[i]);
						jobCategoryMaster.setJobCategoryId(Integer.parseInt(multiSelectId[i]));
						districtSpecificQuestion.setIsRequired(isRequiredVal);
						districtSpecificQuestion.setUserMaster(user);
						districtSpecificQuestion.setJobCategoryMaster(jobCategoryMaster);
						districtSpecificQuestion.setApplicantType(applicantType);
						districtSpecificQuestion.setParentQuestionId(parentID);
						districtSpecificQuestion.setIsCustom(true);						
						districtSpecificQuestion.setStatus("A");
						districtSpecificQuestion.setDistrictMaster(districtMaster);
						DistrictSpecificPortfolioQuestions districtSpecificQuestionNew = null;
	
						if (districtSpecificQuestion.getQuestionId() != null) {
							System.out.println("districtSpecificQuestion.getQuestionId()::::=>"+ districtSpecificQuestion.getQuestionId());
							districtSpecificQuestionNew = districtSpecificPortfolioQuestionsDAO.findById(districtSpecificQuestion.getQuestionId(), false, false);
							if (districtSpecificQuestionNew != null) {
								qopt = districtSpecificQuestionNew.getQuestionOptions();
								districtSpecificQuestion.setStatus(districtSpecificQuestionNew.getStatus());
								districtSpecificQuestion.setCreatedDateTime(districtSpecificQuestionNew.getCreatedDateTime());
							}
							// districtSpecificQuestionsDAO.clear();
						}
						// districtSpecificPortfolioQuestionsDAO.makePersistent(districtSpecificQuestion);
						statelessSession.insert(districtSpecificQuestion);
					}
				   transaction.commit();
				   statelessSession.close();
		    	}else{	
					districtSpecificQuestion.setIsRequired(isRequiredVal);
					districtSpecificQuestion.setUserMaster(user);
					districtSpecificQuestion.setApplicantType(applicantType);
					districtSpecificQuestion.setParentQuestionId(parentID);					
					districtSpecificQuestion.setIsCustom(true);
					districtSpecificQuestion.setStatus("A");
					districtSpecificQuestion.setDistrictMaster(districtMaster);
					DistrictSpecificPortfolioQuestions districtSpecificQuestionNew = null;
					if (districtSpecificQuestion.getQuestionId() != null) {
						System.out.println("districtSpecificQuestion.getQuestionId()::::=>"+ districtSpecificQuestion.getQuestionId());
						districtSpecificQuestionNew = districtSpecificPortfolioQuestionsDAO.findById(districtSpecificQuestion.getQuestionId(), false, false);
						if (districtSpecificQuestionNew != null) {
							qopt = districtSpecificQuestionNew.getQuestionOptions();
							districtSpecificQuestion.setDspqPortfolioName(districtSpecificQuestionNew.getDspqPortfolioName());
							districtSpecificQuestion.setStatus(districtSpecificQuestionNew.getStatus());
							districtSpecificQuestion.setCreatedDateTime(districtSpecificQuestionNew.getCreatedDateTime());
						}
						// districtSpecificQuestionsDAO.clear();
					}
					 districtSpecificPortfolioQuestionsDAO.makePersistent(districtSpecificQuestion);
		    	}
		    }catch(Exception e)
		    {e.printStackTrace();}		   
			Map<Integer,DistrictSpecificPortfolioOptions> map = new HashMap<Integer,DistrictSpecificPortfolioOptions>();
			if(qopt!=null)
				for (DistrictSpecificPortfolioOptions questionOptions2 : qopt) {
					map.put(questionOptions2.getOptionId(), questionOptions2);
				}
			for (DistrictSpecificPortfolioOptions optionsForDistrictSpecificQuestions2 : optionsForDistrictSpecificQuestions) {
				optionsForDistrictSpecificQuestions2.setDistrictSpecificPortfolioQuestions(districtSpecificQuestion);
				optionsForDistrictSpecificQuestions2.setCreatedDateTime(new Date());
				optionsForDistrictSpecificQuestions2.setUserMaster(user);
				optionsForDistrictSpecificQuestions2.setOpenText(false);
				optionsForDistrictSpecificQuestions2.setStatus("A");
				districtSpecificPortfolioOptionsDAO.makePersistent(optionsForDistrictSpecificQuestions2);
				if(qopt!=null)
					map.remove(optionsForDistrictSpecificQuestions2.getOptionId());
				optionsForDistrictSpecificQuestions2=null;
			}
			if(qopt!=null)
				for(Integer aKey : map.keySet()) {
					districtSpecificPortfolioOptionsDAO.makeTransient(map.get(aKey));
				}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtSpecificQuestion;
	}
	
	public String getCustomFieldGrid(String id){
	StringBuffer dmRecords = new StringBuffer();
	List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsList=null;
	if(id!=null && id.length()>0){
		Criterion criterion=Restrictions.eq("parentQuestionId", Integer.parseInt(id));
		districtSpecificPortfolioQuestionsList=districtSpecificPortfolioQuestionsDAO.findByCriteria(criterion);				
		if(districtSpecificPortfolioQuestionsList!=null && districtSpecificPortfolioQuestionsList.size()>0){
			
			dmRecords.append("<div class='row' style='background-color:#007fb2; color:white; padding:7px 5px 7px 5px; width:100%; margin-left:0%; margin-bottom:10px;'>");
			dmRecords.append("<div class='col-sm-2 col-md-2'>Candidate Type</div>");
			dmRecords.append("<div class='col-sm-2 col-md-2'>Display Label</div>");
			dmRecords.append("<div class='col-sm-3 col-md-3'>Helper Text</div>");
			dmRecords.append("<div class='col-sm-2 col-md-2'>Field Type</div>");
			//dmRecords.append("<div class='col-sm-2 col-md-2'>Instructions</div>");
			dmRecords.append("<div class='col-sm-2 col-md-2'>Mandatory</div>");
			dmRecords.append("<div class='col-sm-1 col-md-1' style='text-align:center;'>Actions</div>");
			dmRecords.append("</div>");   
			
			for (DistrictSpecificPortfolioQuestions districtQuestion  : districtSpecificPortfolioQuestionsList) 
			{
				dmRecords.append("<div class='row' style='width:100%; padding:2px 5px 2px 5px; margin-left:1px;'>");
					String candidateType="";
					if(districtQuestion.getApplicantType().equalsIgnoreCase("E"))
						candidateType="External";
					if(districtQuestion.getApplicantType().equalsIgnoreCase("I"))
						candidateType="Internal";
					if(districtQuestion.getApplicantType().equalsIgnoreCase("T"))
						candidateType="I Transfer";
				
						dmRecords.append("<div class='col-sm-2 col-md-2'>"+candidateType+"</div>");	
						dmRecords.append("<div class='col-sm-2 col-md-2'>"+districtQuestion.getQuestion()+"</div>");
						dmRecords.append("<div class='col-sm-3 col-md-3'>"+districtQuestion.getTooltip()+"</div>");
						dmRecords.append("<div class='col-sm-2 col-md-2'>"+districtQuestion.getQuestionTypeMaster().getQuestionType()+"</div>");
						//dmRecords.append("<div class='col-sm-2 col-md-2'>"+districtQuestion.getQuestionInstructions()+"</div>");
						if(districtQuestion.getIsRequired()==0)
							dmRecords.append("<div class='col-sm-2 col-md-2'>Optional</div>");
						else
							dmRecords.append("<div class='col-sm-2 col-md-2'>Required</div>");
						dmRecords.append("<div class='col-sm-1 col-md-1 top7' style='text-align:center;'>");
							dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Edit' onclick='return editDspqQuestion("+districtQuestion.getQuestionId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>");
						dmRecords.append("</div>");
						dmRecords.append("</div>");
						
						dmRecords.append("<div class='row' style='width:100%; padding:2px 5px 2px 5px; margin-left:1px;'>");
						dmRecords.append("</div>");
				
			}
		}
		else		
			return dmRecords.toString();
		
	}	
	return dmRecords.toString();

}
	
	public String savePortfolioName(Integer districtId,String portfolioName)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		
		StringBuffer sbExistsRecord = new StringBuffer();		
		SessionFactory factory = jobCategoryMasterDAO.getSessionFactory();
	    StatelessSession statelessSession=factory.openStatelessSession();
	    Transaction transaction = statelessSession.beginTransaction();
		try{
			DistrictMaster districtMaster 	= null;			
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			if(districtId!=null && districtId!=0){
				districtMaster = districtMasterDAO.findById(districtId, false, false);			
				DspqPortfolioName dspqPortfolioNameDuplicate=dspqPortfolioNameDAO.findByDistrictAndPortfolioName(districtMaster, portfolioName);
				DspqPortfolioName dspqPortfolioName  = new DspqPortfolioName();
				if(dspqPortfolioNameDuplicate!=null && dspqPortfolioNameDuplicate.getPortfolioName().equalsIgnoreCase(portfolioName))
				{
					return "###Candidate portfolio name already exists!";
				}
				else
				{
					dspqPortfolioName.setPortfolioName(portfolioName);
					dspqPortfolioName.setDistrictMaster(districtMaster);
					dspqPortfolioName.setStatus("A");
					dspqPortfolioName.setCreatedDateTime(new Date());					
					transaction = statelessSession.beginTransaction();					
					statelessSession.insert(dspqPortfolioName);
					transaction.commit();
					sbExistsRecord.append("NewPortNew"+"###"+dspqPortfolioName.getDspqPortfolioNameId());
				}	
			}
			else{							
				DspqPortfolioName dspqPortfolioNameDuplicate=dspqPortfolioNameDAO.findByPortfolioNameIsNULL(portfolioName);
				DspqPortfolioName dspqPortfolioName  = new DspqPortfolioName();
				if(dspqPortfolioNameDuplicate!=null && dspqPortfolioNameDuplicate.getPortfolioName().equalsIgnoreCase(portfolioName))
				{
					return "###Candidate portfolio name already exists!";
				}
				else
				{
					dspqPortfolioName.setPortfolioName(portfolioName);
					dspqPortfolioName.setDistrictMaster(districtMaster);
					dspqPortfolioName.setStatus("A");
					dspqPortfolioName.setCreatedDateTime(new Date());					
					transaction = statelessSession.beginTransaction();					
					statelessSession.insert(dspqPortfolioName);
					transaction.commit();
					sbExistsRecord.append("NewPortNew"+"###"+dspqPortfolioName.getDspqPortfolioNameId());
				}	
			}
				
		}
			catch (Exception e) {
			e.printStackTrace();
	}finally{
		statelessSession.close();
	}		
	return sbExistsRecord.toString();

	}
	public String propertyKey(String fieldName,Integer fieldId,String type)
	{
		String sKey="";
		if(type=="Section")
			sKey="PortfolioSection_"+fieldId+"_";
		else			
			sKey="PortfolioField_"+fieldId+"_";
		
		fieldName=fieldName.replace("'", "");
		fieldName=fieldName.replace(" ", "");
		fieldName=fieldName.replace("(", "");
		fieldName=fieldName.replace(")", "");
		fieldName=fieldName.replace("/", "");
		fieldName=fieldName.replace("$", "");
		fieldName=fieldName.replace("&", "");
		fieldName=fieldName.replace(":", "");
		fieldName=fieldName.replace(".", "");
		fieldName=fieldName.replace("?", "");
		fieldName=fieldName.replace("*", "");
		fieldName=fieldName.replace("-", "");
		sKey=sKey+fieldName;
		String sKeyValue=Utility.getLocaleValuePropByKey(sKey, locale);
		//System.out.println("sKey "+sKey +" sKeyValue "+sKeyValue);		
		return sKeyValue;
	}
	public String updatePortfolioName(Integer districtId,String portfolioName,Integer dspqPortfolioNameId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		
		StringBuffer sbExistsRecord = new StringBuffer();
		//Stateles Session 
		SessionFactory factory = jobCategoryMasterDAO.getSessionFactory();
	    StatelessSession statelessSession=factory.openStatelessSession();
	    Transaction transaction = statelessSession.beginTransaction();
		try{
			UserMaster userMaster=null;
			DistrictMaster districtMaster 	= null;
			int roleId=0;
			
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}			
			districtMaster = districtMasterDAO.findById(districtId, false, false);			
			
			List<JobCategoryMaster> jobCategoryMasterList = null;
			JobCategoryMaster jobCategoryMaster = null;
			DspqPortfolioName dspqPortfolioName =null;
			if(dspqPortfolioNameId !=null && dspqPortfolioNameId !=0)
			{
				dspqPortfolioName=dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
				//String[] temps = dspqPortfolioName.getJobCategory().split(",");				
				//if(formsJobCtaIdss.contains(o))
			}
			
			if(jobCategoryMasterList!=null && jobCategoryMasterList.size()>0){
				System.out.println("dspqPortfolioNameId "+dspqPortfolioNameId);
				if(dspqPortfolioNameId==null)
					dspqPortfolioName=new DspqPortfolioName();				
					
				dspqPortfolioName.setPortfolioName(portfolioName);
				dspqPortfolioName.setDistrictMaster(districtMaster);				
				dspqPortfolioName.setStatus("A");
				dspqPortfolioName.setCreatedDateTime(new Date());
				transaction = statelessSession.beginTransaction();
				if(dspqPortfolioName.getDspqPortfolioNameId()!=null)
					statelessSession.update(dspqPortfolioName);
				else
					statelessSession.insert(dspqPortfolioName);
				transaction.commit();	
				
				System.out.println("dspqPortfolioName.getDspqPortfolioNameId()11111111 "+dspqPortfolioName.getDspqPortfolioNameId());
				sbExistsRecord.append(dspqPortfolioName.getDspqPortfolioNameId().toString());
				return sbExistsRecord.toString();
			}
			else
			{		
				DspqPortfolioName dspqPortfolioNameDuplicate=dspqPortfolioNameDAO.findByDistrictAndJobCategory(districtMaster, jobCategoryMaster);
				//DspqPortfolioName dspqPortfolioName  = dspqPortfolioNameDuplicate.getd
				if(dspqPortfolioNameDuplicate!=null)
				{
					dspqPortfolioNameDuplicate.setPortfolioName(portfolioName);
					dspqPortfolioNameDuplicate.setDistrictMaster(districtMaster);					
					dspqPortfolioNameDuplicate.setStatus("A");
					dspqPortfolioNameDuplicate.setCreatedDateTime(new Date());
					transaction = statelessSession.beginTransaction();
					statelessSession.update(dspqPortfolioNameDuplicate);
					transaction.commit();
					sbExistsRecord.append(dspqPortfolioNameDuplicate.getDspqPortfolioNameId());
				}
			}
		}
			catch (Exception e) {
			e.printStackTrace();
	}finally{
		statelessSession.close();
	}		
	return sbExistsRecord.toString();

	}
	
	public String getFieldNameUpdate(DspqRouter[] dspqConf1,Integer dspqPortfolioNameId)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }	
		Integer count=0;
		//Stateles Session 
		SessionFactory factory = dspqRouterDAO.getSessionFactory();
	    StatelessSession statelessSession=factory.openStatelessSession();
	    Transaction transaction = statelessSession.beginTransaction();
		try{
		UserMaster userMaster=null;		
		int roleId=0;
		
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		DspqPortfolioName dspqPortfolioName=dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);		
		
		Map<String,DspqRouter> dsqpRouListMap = new HashMap<String, DspqRouter>();
		Map<String,DspqRouter> dsqpRouSecListMap = new HashMap<String, DspqRouter>();
		Map<Integer,DspqRouter> dsqpRouOld = new HashMap<Integer, DspqRouter>();
		
		List<DspqRouter> dspqRouterLists = dspqRouterDAO.getDspqFieldListByDspqPortfolioNameId(dspqPortfolioName);		
		if(dspqRouterLists!=null && dspqRouterLists.size()>0){
			for(DspqRouter dspqRouter : dspqRouterLists){
				if(dspqRouter.getDspqFieldMaster()!=null)
				{
					dsqpRouListMap.put(dspqRouter.getDspqPortfolioName().getDspqPortfolioNameId()+"##"+dspqRouter.getDspqFieldMaster().getDspqFieldId()+"##"+dspqRouter.getApplicantType(), dspqRouter);
					dsqpRouOld.put(dspqRouter.getDspqFieldMaster().getDspqFieldId(), dspqRouter);
				}
				if(dspqRouter.getDspqSectionMaster()!=null)							
					dsqpRouSecListMap.put(dspqRouter.getDspqPortfolioName().getDspqPortfolioNameId()+"##"+dspqRouter.getDspqSectionMaster().getSectionId()+"##"+dspqRouter.getApplicantType(), dspqRouter);
				
				
			}
		}
		
			if(dspqPortfolioName!=null)
			{
				DspqRouter dspqRouter = new DspqRouter();
				DspqRouter dspqRouterChk = new DspqRouter();
				Boolean chkDup=false;
				for (DspqRouter dspqVal : dspqConf1) 
				{		
					DspqRouter dspqRouterOld = new DspqRouter();
					dspqRouterChk = dsqpRouListMap.put(dspqPortfolioName.getDspqPortfolioNameId()+"##"+dspqVal.getDspqFieldMaster().getDspqFieldId()+"##"+dspqRouter.getApplicantType(), dspqRouter);
					
					System.out.println("DisplayName() " +dspqVal.getDisplayName());
					System.out.println("status() " +dspqVal.getStatus());
					System.out.println("ApplicantType()" +dspqVal.getApplicantType());
					
					dspqRouter.setDspqSectionMaster(dspqRouterOld.getDspqSectionMaster());
					dspqRouter.setDspqFieldMaster(dspqVal.getDspqFieldMaster());
					dspqRouter.setDspqPortfolioName(dspqPortfolioName);
					dspqRouter.setDisplayName(dspqVal.getDisplayName());
					dspqRouter.setIsRequired(dspqVal.getIsRequired());
					dspqRouter.setApplicantType(dspqVal.getApplicantType());
					//System.out.println("dspqVal.getNumberRequired() "+dspqVal.getNumberRequired());
					dspqRouter.setNumberRequired(dspqVal.getNumberRequired());
					dspqRouter.setInstructions(dspqRouterOld.getInstructions());
					dspqRouter.setTooltip(dspqVal.getTooltip());
					dspqRouter.setStatus(dspqVal.getStatus());
					dspqRouter.setApplicantTypeStatus(dspqRouterOld.getApplicantTypeStatus());
					dspqRouter.setCreatedDateTime(new Date());
					transaction = statelessSession.beginTransaction();	
					
					/*if(dspqVal.getDspqSectionMaster()!=null)
					{							
						dspqRouterChk = dsqpRouSecListMap.put(dspqPortfolioName.getDspqPortfolioNameId()+"##"+dspqVal.getDspqSectionMaster().getSectionId()+"##"+dspqRouter.getApplicantType(), dspqRouter);							
						if(dspqRouterChk!=null)
						{
							dspqRouter.setDspqRouterId(dspqRouterChk.getDspqRouterId());
							chkDup=true;
						}							
					}*/
					if(dspqVal.getDspqFieldMaster()!=null)
					{
						//dspqRouterChk = dsqpRouListMap.put(dspqPortfolioName.getDspqPortfolioNameId()+"##"+dspqVal.getDspqFieldMaster().getDspqFieldId()+"##"+dspqRouter.getApplicantType(), dspqRouter);
						if(dspqRouterChk!=null)
						{
							dspqRouter.setDspqRouterId(dspqRouterChk.getDspqRouterId());
							chkDup=true;
						}
					}
					transaction = statelessSession.beginTransaction();
					if(chkDup==true)
						statelessSession.update(dspqRouter);
					else
						statelessSession.insert(dspqRouter);
					
					
				}
			}
			transaction.commit();
		
		
	}
		catch (Exception e) {
		e.printStackTrace();
}finally{
	statelessSession.close();
}
	//return dmRecords.toString();
return "";
	}
	
	
	
	
	
	
	
	
	
	
	public String getLoadSectionDiv(String sectionID,Integer portfolioNameId)
	{
		
		System.out.println("sectionID "+sectionID);
		System.out.println("portfolioNameId "+portfolioNameId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
	
		
		StringBuffer dmRecords = new StringBuffer();
		DistrictMaster districtMaster = null;
		try{
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster =	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				if(userMaster.getDistrictId()!=null){
	        		districtMaster = userMaster.getDistrictId();
	        	}
			}
			Map<Integer,List<DspqFieldMaster>> sectionIds = new HashMap<Integer,List<DspqFieldMaster>>();
			
			DspqSectionMaster dspqSectionMasterID = null;
			if(!sectionID.equals("")){
				dspqSectionMasterID= dspqSectionMasterDAO.findById(Integer.parseInt(sectionID),false, false);
			}			
			
				
			// TM Custom Fields
			List<DistrictSpecificPortfolioQuestions> dspqQuestionList=null;
			if(portfolioNameId!=null && portfolioNameId!=0)
			{
				DspqPortfolioName dspqPortfolioName=null;
				dspqPortfolioName=dspqPortfolioNameDAO.findById(portfolioNameId, false, false);
				dspqQuestionList = districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioBySecID(dspqPortfolioName,dspqSectionMasterID);
			}
			Map<Integer,List<DistrictSpecificPortfolioQuestions>> dspqQuestionsMap = new HashMap<Integer,List<DistrictSpecificPortfolioQuestions>>();
			try{
				if(dspqQuestionList!=null && dspqQuestionList.size()>0){
					List<DistrictSpecificPortfolioQuestions> tempQuesList = null;

					for(DistrictSpecificPortfolioQuestions dspqQuestion : dspqQuestionList){
						
						if(dspqQuestionsMap==null || dspqQuestionsMap.size()==0){
							tempQuesList = new ArrayList<DistrictSpecificPortfolioQuestions>();
							tempQuesList.add(dspqQuestion);
							dspqQuestionsMap.put(dspqQuestion.getDspqSectionMaster().getSectionId(), tempQuesList);
						}
						else
						{
							tempQuesList = new ArrayList<DistrictSpecificPortfolioQuestions>();
							tempQuesList = dspqQuestionsMap.get(dspqQuestion.getDspqSectionMaster().getSectionId());
							if(tempQuesList==null){
								tempQuesList = new ArrayList<DistrictSpecificPortfolioQuestions>();
								tempQuesList.add(dspqQuestion);
							}
							else{
								tempQuesList.add(dspqQuestion);
							}
							dspqQuestionsMap.put(dspqQuestion.getDspqSectionMaster().getSectionId(), tempQuesList);							
						}
					}
				}
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
		
			if(dspqSectionMasterID!=null)
			{	
					// TeacherMatch Custom Fields
					List<DistrictSpecificPortfolioQuestions> dspqQuestionsList = null;
					dspqQuestionsList = dspqQuestionsMap.get(dspqSectionMasterID.getSectionId());
					
					String questionInstruction = null;
					String question = null;
					Integer cnt = 0;
					int counter = 1;
					String shortName = null;
					
					if(dspqQuestionsList!=null && dspqQuestionsList.size()>0){
					//dmRecords.append("<div class='row' id='TMAdditionalSec"+dspqSec.getSectionId()+"' style='width:100%; font-weight:bold;'><div class='col-sm-12 col-md-12' style='padding:9px; padding-left:25px;'>TeacherMatch Custom Fields</div></div>");
					List<DistrictSpecificPortfolioOptions> questionOptionsList = null;					
					
					Integer valOld=0;
					Integer oldQuestionId=0;
					Integer closeDiv=0;
					String Custom1="",Custom2="",Custom3="",displayCustomField="";
					
						for(DistrictSpecificPortfolioQuestions dspqQuestion : dspqQuestionsList){
							
						//boolean test=dspqRouterDAO.getExistsQuestionId(dspqQuestion.getParentQuestionId());
						
						if(oldQuestionId.equals(dspqQuestion.getParentQuestionId()))
							valOld=dspqQuestion.getParentQuestionId();
						else
						{
							List<DistrictSpecificPortfolioQuestions> dspqList=null;
							dspqList = districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestionByParentQuestionId(dspqQuestion.getParentQuestionId());
							if(dspqList!=null && dspqList.size()>0)
							{
								for(DistrictSpecificPortfolioQuestions dspqQuestion1 : dspqList){
									if(Custom1.equalsIgnoreCase(""))
										Custom1=dspqQuestion1.getQuestion();
									else if(Custom2.equalsIgnoreCase(""))
										Custom2=dspqQuestion1.getQuestion();
									else
										Custom3=dspqQuestion1.getQuestion();
								}
							}
							valOld=0;
						}
							
							if(valOld.equals(0))
							{
								if(Custom1.equalsIgnoreCase(Custom2) && Custom2.equalsIgnoreCase(Custom3) && Custom3.equalsIgnoreCase(Custom1))
									displayCustomField=Custom1;
								else 
									displayCustomField=Custom1+"/"+Custom2+"/"+Custom3;
								oldQuestionId=dspqQuestion.getParentQuestionId();
							shortName = dspqQuestion.getQuestionTypeMaster().getQuestionTypeShortName();
							
							questionInstruction = dspqQuestion.getQuestionInstructions()==null?"":dspqQuestion.getQuestionInstructions();
							question 			= dspqQuestion.getQuestion()==null?"":dspqQuestion.getQuestion();
							Custom1="";Custom2="";Custom3="";
							dmRecords.append("<input type='hidden' id='customField"+dspqQuestion.getQuestionId()+"' value='"+dspqQuestion.getQuestion()+"'>");
							dmRecords.append("<div style='margin-bottom:5px;'>");
																					
							if(!question.equals("")){
								String required="";
								if(dspqQuestion.getIsRequired()==1)
									required="<label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' title='Required' onclick='activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",0,"+dspqSectionMasterID.getSectionId()+",\"Req\")'>R</label>";
								else
									required="<label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' title='Optional' onclick='activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",1,"+dspqSectionMasterID.getSectionId()+",\"Req\")'>O</label>";
								dmRecords.append("<div class='row top5'>");
									dmRecords.append("<div class='col-sm-9 col-md-9 manageWidth'>");
									dmRecords.append(displayCustomField+" <span href='#'  data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+question+" '> <img src='images/qua-icon.png' alt=''></span>");
									dmRecords.append("</div>");
									
									dmRecords.append("<div class='col-sm-1 col-md-1 top5' style='padding-left:8px;'>");
									dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Edit' id='dspqActiveDeactive"+counter+"' onclick='return editCustomField("+dspqQuestion.getQuestionId()+")')\"><span class='fa fa-pencil-square-o fa-lg ' style='text-align:center;'></span></a>");
									dmRecords.append("</div>");
									
									dmRecords.append("<div class='col-sm-2 col-md-2' style='padding-left:1px;'>");
									if(dspqQuestion.getApplicantType().equalsIgnoreCase("E") && dspqQuestion.getStatus().equalsIgnoreCase("A"))
										dmRecords.append("<label class='circleInsWhite'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' title='"+dspqQuestion.getQuestion()+"'>L</label> <label class='circleIns helper'>?</label> "+required+" <a href='javascript:void(0);' class='tempToolTip' data-toggle='tooltip' data-placement='top' title='Active'  id='dspqActiveDeactive"+counter+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"I\","+dspqSectionMasterID.getSectionId()+",\"Act\")')\"><label class='circleIns activeIcon'>A</label></a>");
									else
										dmRecords.append(" <label class='circleInsWhite'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' title='"+dspqQuestion.getQuestion()+"'>L</label> <label class='circleIns helper'>?</label> "+required+" <a href='javascript:void(0);' class='tempToolTip' data-toggle='tooltip' data-placement='top' title='Inactive'  id='dspqActiveDeactive"+counter+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"A\","+dspqSectionMasterID.getSectionId()+",\"Act\")')\"><label class='circleIns inactiveIcon'>X</label></a>");
									dmRecords.append("</div>");
							}
							}
							else
							{
								closeDiv++;
									
									if(dspqQuestion.getApplicantType().equalsIgnoreCase("I"))
									{
										String required="";
										if(dspqQuestion.getIsRequired()==1)
											required="<label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' title='Required' onclick='activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",0,"+dspqSectionMasterID.getSectionId()+",\"Req\")'>R</label>";
										else
											required="<label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' title='Optional' onclick='activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",1,"+dspqSectionMasterID.getSectionId()+",\"Req\")'>O</label>";
										dmRecords.append("<div class='col-sm-2 col-md-2 hide sectionLabelInternal pleft10' >");
										if(dspqQuestion.getApplicantType().equalsIgnoreCase("I") && dspqQuestion.getStatus().equalsIgnoreCase("A"))
											dmRecords.append("<label class='circleInsWhite'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' title='"+dspqQuestion.getQuestion()+"'>L</label> <label class='circleIns helper'>?</label> "+required+" <a href='javascript:void(0);' title='Active' class='tempToolTip' data-toggle='tooltip' data-placement='top'  id='dspqActiveDeactive"+counter+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"I\","+dspqSectionMasterID.getSectionId()+",\"Act\")')\"><label class='circleIns activeIcon '>A</label></a>");
										else
											dmRecords.append("<label class='circleInsWhite'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' title='"+dspqQuestion.getQuestion()+"'>L</label> <label class='circleIns helper'>?</label> "+required+"  <a href='javascript:void(0);' title='Inactive' class='tempToolTip' data-toggle='tooltip' data-placement='top' id='dspqActiveDeactive"+counter+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"A\","+dspqSectionMasterID.getSectionId()+",\"Act\")')\"><label class='circleIns inactiveIcon'>X</label></a>");
										dmRecords.append("</div>");
									}
									else
									{
										String required="";
										if(dspqQuestion.getIsRequired()==1)
											required="<label class='circleIns iconRequired tempToolTip' data-toggle='tooltip' data-placement='top' title='Required' onclick='activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",0,"+dspqSectionMasterID.getSectionId()+",\"Req\")'>R</label>";
										else
											required="<label class='circleIns iconOptional tempToolTip' data-toggle='tooltip' data-placement='top' title='Optional' onclick='activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",1,"+dspqSectionMasterID.getSectionId()+",\"Req\")'>O</label>";
										dmRecords.append("<div class='col-sm-2 col-md-2 hide sectionLabelInternalTransfer pleft10'>");
										if(dspqQuestion.getApplicantType().equalsIgnoreCase("T") && dspqQuestion.getStatus().equalsIgnoreCase("A"))
											dmRecords.append("<label class='circleInsWhite'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' title='"+dspqQuestion.getQuestion()+"'>L</label> <label class='circleIns helper'>?</label> "+required+"  <a href='javascript:void(0);' title='Active' class='tempToolTip' data-toggle='tooltip' data-placement='top'  id='dspqActiveDeactive"+counter+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"I\","+dspqSectionMasterID.getSectionId()+",\"Act\")')\"><label class='circleIns activeIcon'>A</label></a>");
										else
											dmRecords.append("<label class='circleInsWhite'>I</label> <label class='circleIns iconLabel tempToolTip' data-toggle='tooltip' title='"+dspqQuestion.getQuestion()+"'>L</label> <label class='circleIns helper'>?</label> "+required+"  <a href='javascript:void(0);' title='Inactive' class='tempToolTip' data-toggle='tooltip' data-placement='top'  id='dspqActiveDeactive"+counter+"' onclick='return activateDeactivateQuestion("+dspqQuestion.getQuestionId()+",\"A\","+dspqSectionMasterID.getSectionId()+",\"Act\")')\"><label class='circleIns inactiveIcon'>X</label></a>");
										dmRecords.append("</div>");
									}
									if(closeDiv.equals(2))
									{
										closeDiv=0;
										dmRecords.append("</div>");
										dmRecords.append("</div>");
										dmRecords.append("<script>$('#dspqActiveDeactive"+counter+"').tooltip();</script>");
									}
								
							}														
							
							cnt++;
							counter++;
						}
						
					}					
					// TeacherMatch Custom Fields Ends
					
				sectionIds.clear();
			}
			
	}catch (Exception e) {
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	public String saveClone(Integer dspqPortfolioNameId,String portfolioName)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }		
		SessionFactory factory = dspqRouterDAO.getSessionFactory();
	    StatelessSession statelessSession=factory.openStatelessSession();
	    Transaction transaction = statelessSession.beginTransaction();
	    String portfolioID="";
	    try{
			DspqPortfolioName dspqPortfolioName=new DspqPortfolioName();
			DspqPortfolioName dspqPortfolioNameCopy=new DspqPortfolioName();
			dspqPortfolioName=dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
			
			dspqPortfolioNameCopy.setPortfolioName(portfolioName);
			dspqPortfolioNameCopy.setDistrictMaster(dspqPortfolioName.getDistrictMaster());
			dspqPortfolioNameCopy.setStatus("A");
			dspqPortfolioNameCopy.setCreatedDateTime(new Date());					
			transaction = statelessSession.beginTransaction();
			
			statelessSession.insert(dspqPortfolioNameCopy);
			
			List<DspqRouter> routersList=null;
			routersList =dspqRouterDAO.getDspqFieldListByDspqPortfolioNameId(dspqPortfolioName);
			for (DspqRouter dspqVal : routersList) 
			{
				dspqVal.setDspqPortfolioName(dspqPortfolioNameCopy);				
				dspqVal.setCreatedDateTime(new Date());				
				transaction = statelessSession.beginTransaction();				
				statelessSession.insert(dspqVal);
			}
			List<DistrictSpecificPortfolioQuestions> customQuesList=new ArrayList<DistrictSpecificPortfolioQuestions>();
			DistrictSpecificPortfolioQuestions customQuesCopy=null;
			customQuesList=districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestionBYPortfolioNameId(dspqPortfolioName);
			Integer applicantCounter=0;
			Integer parentQuestionId=0;
			Integer quesID=0;
			List<DistrictSpecificPortfolioOptions> customOptionListCopy=new ArrayList<DistrictSpecificPortfolioOptions>();
			Map<Integer,List<DistrictSpecificPortfolioOptions>> mapOptions = new HashMap<Integer,List<DistrictSpecificPortfolioOptions>>();
			if(customQuesList!=null && customQuesList.size()>0){
				List<DistrictSpecificPortfolioOptions> customOptionList=new ArrayList<DistrictSpecificPortfolioOptions>();
				
				
				customOptionList=districtSpecificPortfolioOptionsDAO.getCustumFieldOptionByQuestionLst(customQuesList);
				for (DistrictSpecificPortfolioOptions customOptions : customOptionList) 
				{
					Integer questionId = customOptions.getDistrictSpecificPortfolioQuestions().getQuestionId();
					List<DistrictSpecificPortfolioOptions> tempList = new ArrayList<DistrictSpecificPortfolioOptions>();
					
					if(mapOptions.containsKey(questionId)){
						tempList = mapOptions.get(questionId);
						tempList.add(customOptions);
						mapOptions.put(questionId, tempList);
					}else{
						tempList.add(customOptions);
						mapOptions.put(questionId, tempList);
					}
					
				}
			}
			if(customQuesList!=null && customQuesList.size()>0){
				for (DistrictSpecificPortfolioQuestions customQues : customQuesList) 
				{
					quesID=customQues.getQuestionId();
					if(applicantCounter==0){
						customQuesCopy=customQues;
						customQuesCopy.setDspqPortfolioName(dspqPortfolioNameCopy);
						customQuesCopy.setCreatedDateTime(new Date());
						statelessSession.insert(customQuesCopy);
						customQuesCopy.setParentQuestionId(customQuesCopy.getQuestionId());
						statelessSession.update(customQuesCopy);
						parentQuestionId=customQuesCopy.getQuestionId();
					}
					else{
						customQuesCopy=customQues;
						customQuesCopy.setDspqPortfolioName(dspqPortfolioNameCopy);
						customQuesCopy.setCreatedDateTime(new Date());
						customQuesCopy.setParentQuestionId(parentQuestionId);
						statelessSession.insert(customQuesCopy);
						if(applicantCounter==2)
							applicantCounter=-1;
					}
					applicantCounter++;
					customOptionListCopy = new ArrayList<DistrictSpecificPortfolioOptions>();
					customOptionListCopy=mapOptions.get(quesID);
					DistrictSpecificPortfolioOptions customOptions=new DistrictSpecificPortfolioOptions();
					if(customOptionListCopy!=null && customOptionListCopy.size()>0)
					{
						for(DistrictSpecificPortfolioOptions cusOptions: customOptionListCopy){
							customOptions=cusOptions;
							customOptions.setDistrictSpecificPortfolioQuestions(customQuesCopy);
							customOptions.setCreatedDateTime(new Date());
							statelessSession.insert(customOptions);
						}
					}
				}
			}
			portfolioID=dspqPortfolioNameCopy.getDspqPortfolioNameId()+"";
			transaction.commit();
	    }
	    catch (Exception e) {
			e.printStackTrace();
	}finally{
		statelessSession.close();
	}
	return portfolioID;
}
	public String loadDataTypeWithList(Integer dspqFieldId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuilder sbAppend=new StringBuilder();
		Criterion criterion=Restrictions.eqProperty("status", "A");
	    try{
	    	if(dspqFieldId==5 || dspqFieldId==45)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("optDr", locale)+"<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("optMiss", locale)+"<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("optMr", locale)+"<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("optMrs", locale)+"<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("optMs", locale));
	    	}
	    	if(dspqFieldId==10)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<RaceMaster> raceMaster=raceMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		System.out.println("raceMaster "+raceMaster.size());
	    		if(raceMaster!=null && raceMaster.size()!=0)
	    		{
	    			for (RaceMaster raceMaster2 : raceMaster) {
	    				sbAppend.append(raceMaster2.getRaceName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==11)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<GenderMaster> genderMaster=genderMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(genderMaster!=null && genderMaster.size()!=0)
	    		{
	    			for (GenderMaster genderMaster2 : genderMaster) {
	    				sbAppend.append(genderMaster2.getGenderName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==14 || dspqFieldId==91 || dspqFieldId==34 || dspqFieldId==99 || dspqFieldId==105)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<StateMaster> stateMaster=stateMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(stateMaster!=null && stateMaster.size()!=0)
	    		{
	    			for (StateMaster stateMaster2 : stateMaster) {
	    				sbAppend.append(stateMaster2.getStateName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==16 || dspqFieldId==89)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<CountryMaster> countryMaster=countryMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(countryMaster!=null && countryMaster.size()!=0)
	    		{
	    			for (CountryMaster countryMaster2 : countryMaster) {
	    				sbAppend.append(countryMaster2.getName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==106)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<DistrictMaster> districtMaster=districtMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(districtMaster!=null && districtMaster.size()!=0)
	    		{
	    			for (DistrictMaster districtMaster2 : districtMaster) {
	    				sbAppend.append(districtMaster2.getDistrictName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==17 || dspqFieldId==92)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<CityMaster> cityMaster=cityMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(cityMaster!=null && cityMaster.size()!=0)
	    		{
	    			for (CityMaster cityMaster2 : cityMaster) {
	    				sbAppend.append(cityMaster2.getCityName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==20)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<DegreeMaster> degreeMaster=degreeMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(degreeMaster!=null && degreeMaster.size()!=0)
	    		{
	    			for (DegreeMaster degreeMaster2 : degreeMaster) {
	    				sbAppend.append(degreeMaster2.getDegreeName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==21)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<SchoolMaster> schoolMaster=schoolMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(schoolMaster!=null && schoolMaster.size()!=0)
	    		{
	    			for (SchoolMaster schoolMaster2 : schoolMaster) {
	    				sbAppend.append(schoolMaster2.getSchoolName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==22)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<FieldOfStudyMaster> fieldOfStudyMaster=fieldOfStudyMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(fieldOfStudyMaster!=null && fieldOfStudyMaster.size()!=0)
	    		{
	    			for (FieldOfStudyMaster fieldOfStudyMaster2 : fieldOfStudyMaster) {
	    				sbAppend.append(fieldOfStudyMaster2.getFieldName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==23 || dspqFieldId==30 || dspqFieldId==35 || dspqFieldId==36 || dspqFieldId==86 || dspqFieldId==102 || dspqFieldId==119)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		if(dspqFieldId==36)
	    			sbAppend.append("Does Not Expire<br>");
	    		List<String> lstYear=Utility.getLasterYearByYear(2006);
	    		for(String year : lstYear){
	    			sbAppend.append(year+"<br>");
				}
	    	}
	    	if(dspqFieldId==31 || dspqFieldId==53 || dspqFieldId==76 || dspqFieldId==97 || dspqFieldId==98)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("lblYes", locale)+"<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("lblNo", locale));	    		
	    	}
	    	if(dspqFieldId==32)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<CertificationStatusMaster> certificationStatusMaster=certificationStatusMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(certificationStatusMaster!=null && certificationStatusMaster.size()!=0)
	    		{
	    			for (CertificationStatusMaster certificationStatusMaster2 : certificationStatusMaster) {
	    				sbAppend.append(certificationStatusMaster2.getCertificationStatusName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==33)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<CertificationTypeMaster> certificationTypeMaster=certificationTypeMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(certificationTypeMaster!=null && certificationTypeMaster.size()!=0)
	    		{
	    			for (CertificationTypeMaster certificationTypeMaster2 : certificationTypeMaster) {
	    				sbAppend.append(certificationTypeMaster2.getCertificationType()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==40 || dspqFieldId==85)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		if(dspqFieldId==40)
	    		{
		    		sbAppend.append(Utility.getLocaleValuePropByKey("lblPK", locale)+ "<br>");
		    		sbAppend.append(Utility.getLocaleValuePropByKey("lblKG", locale)+ "<br>");
	    		}
	    		sbAppend.append("1<br>");
	    		sbAppend.append("2<br>");
	    		sbAppend.append("3<br>");
	    		sbAppend.append("4<br>");
	    		sbAppend.append("5<br>");
	    		sbAppend.append("6<br>");
	    		sbAppend.append("7<br>");
	    		sbAppend.append("8<br>");
	    		sbAppend.append("9<br>");
	    		sbAppend.append("10");
	    	}
	    	if(dspqFieldId==59)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("lblStudentTeaching", locale)+ "<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("lblFullTimeTeaching", locale)+ "<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("lblSubstituteTeaching", locale)+ "<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("lblOtherWorkExperience", locale));
	    	}
	    	if(dspqFieldId==61)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<FieldMaster> fieldMaster=fieldMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(fieldMaster!=null && fieldMaster.size()!=0)
	    		{
	    			for (FieldMaster fieldMaster2 : fieldMaster) {
	    				sbAppend.append(fieldMaster2.getFieldName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==66 || dspqFieldId==67 || dspqFieldId==84)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<String> lstMonth= Utility.getMonthList();
	    		for(String month : lstMonth){
	    			sbAppend.append(month+"<br>");
				}
	    	}
	    	if(dspqFieldId==69)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<EmpRoleTypeMaster> empRoleTypeMaster=empRoleTypeMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(empRoleTypeMaster!=null && empRoleTypeMaster.size()!=0)
	    		{
	    			for (EmpRoleTypeMaster empRoleTypeMaster2 : empRoleTypeMaster) {
	    				sbAppend.append(empRoleTypeMaster2.getEmpRoleTypeName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==74)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<OrgTypeMaster> orgTypeMaster=orgTypeMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(orgTypeMaster!=null && orgTypeMaster.size()!=0)
	    		{
	    			for (OrgTypeMaster orgTypeMaster2 : orgTypeMaster) {
	    				sbAppend.append(orgTypeMaster2.getOrgType()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==75)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<PeopleRangeMaster> peopleRangeMaster=peopleRangeMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(peopleRangeMaster!=null && peopleRangeMaster.size()!=0)
	    		{
	    			for (PeopleRangeMaster peopleRangeMaster2 : peopleRangeMaster) {
	    				sbAppend.append(peopleRangeMaster2.getRange()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==95)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<SpokenLanguageMaster> spokenLanguageMaster=spokenLanguageMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(spokenLanguageMaster!=null && spokenLanguageMaster.size()!=0)
	    		{
	    			for (SpokenLanguageMaster spokenLanguageMaster2 : spokenLanguageMaster) {
	    				sbAppend.append(spokenLanguageMaster2.getLanguageName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==117 || dspqFieldId==118)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("lblPolite", locale)+ "<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("lblLiterate", locale)+ "<br>");
	    		sbAppend.append(Utility.getLocaleValuePropByKey("lblFluent", locale));
	    	}
	    	if(dspqFieldId==101)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<TFAAffiliateMaster> tFAAffiliateMaster=tfaAffiliateMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(tFAAffiliateMaster!=null && tFAAffiliateMaster.size()!=0)
	    		{
	    			for (TFAAffiliateMaster tFAAffiliateMaster2 : tFAAffiliateMaster) {
	    				sbAppend.append(tFAAffiliateMaster2.getTfaAffiliateName()+"<br>");
					}
	    		}
	    	}
	    	if(dspqFieldId==103)
	    	{
	    		sbAppend.append("<br>Top Ten Values:<br>");
	    		List<TFARegionMaster> tFARegionMaster=tfaRegionMasterDAO.findByCriteriaTopTen(new Criterion[]{});
	    		if(tFARegionMaster!=null && tFARegionMaster.size()!=0)
	    		{
	    			for (TFARegionMaster tFARegionMaster2 : tFARegionMaster) {
	    				sbAppend.append(tFARegionMaster2.getTfaRegionName()+"<br>");
					}
	    		}
	    	}	    	
	    }
	    catch (Exception e) {
			e.printStackTrace();
	}
	return sbAppend.toString();
}
	public String createDefaultPortfolio(Integer dspqPortfolioNameId, Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		int roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		else{
			userMaster =	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getEntityType().equals(2))
				districtMaster=userMaster.getDistrictId();	
			else
				districtMaster=districtMasterDAO.findById(districtId, false, false);
		}
		SessionFactory factory = dspqRouterDAO.getSessionFactory();
	    StatelessSession statelessSession=factory.openStatelessSession();
	    Transaction transaction = statelessSession.beginTransaction();
	    String portfolioID="";
	    DspqPortfolioName dspqPortfolioNameCheck=new DspqPortfolioName();
	    try{
	    	if(dspqPortfolioNameId!=null && dspqPortfolioNameId!=0){
				DspqPortfolioName dspqPortfolioName=new DspqPortfolioName();
				DspqPortfolioName dspqPortfolioNameCopy=new DspqPortfolioName();
				dspqPortfolioName=dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
				DateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");
				Date date = new Date();
				System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48
				dspqPortfolioNameCheck=dspqPortfolioNameDAO.findByDistrictAndPortfolioName(districtMaster, dspqPortfolioName.getPortfolioName());
				if(dspqPortfolioNameCheck!=null)				
					dspqPortfolioNameCopy.setPortfolioName(dspqPortfolioNameCheck.getPortfolioName()+" "+dateFormat.format(date));
				else
					dspqPortfolioNameCopy.setPortfolioName(dspqPortfolioName.getPortfolioName());
				dspqPortfolioNameCopy.setDistrictMaster(districtMaster);
				dspqPortfolioNameCopy.setStatus("A");
				dspqPortfolioNameCopy.setCreatedDateTime(new Date());					
				transaction = statelessSession.beginTransaction();				
				statelessSession.insert(dspqPortfolioNameCopy);
				
				List<DspqRouter> routersList=null;
				routersList =dspqRouterDAO.getDspqFieldListByDspqPortfolioNameId(dspqPortfolioName);
				for (DspqRouter dspqVal : routersList) 
				{
					dspqVal.setDspqPortfolioName(dspqPortfolioNameCopy);				
					dspqVal.setCreatedDateTime(new Date());				
					transaction = statelessSession.beginTransaction();				
					statelessSession.insert(dspqVal);
				}
				portfolioID=dspqPortfolioNameCopy.getDspqPortfolioNameId()+"";
				transaction.commit();
	    	}
	    }
	    catch (Exception e) {
			e.printStackTrace();
	}finally{
		statelessSession.close();
	}
	return portfolioID;
}

public String findJobcategoryListByPortfolioId(Integer portfolioid,Integer districtId)
{
	    WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb = new StringBuffer();
		try
		{
			if(session == null ||session.getAttribute("userMaster")==null) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}	
			JobCategoryMaster jobCategoryMaster=null;
			List<JobCategoryTransaction> jobCategorytransactionList=null;
			List<JobCategoryMaster> jobCategoryMasterList=new ArrayList<JobCategoryMaster>();
			jobCategorytransactionList=jobCategoryTransactionDAO.getJobCategoryByPortfolioId(portfolioid);
			if((jobCategorytransactionList!=null && jobCategorytransactionList.size()>0))
			{
				if(jobCategorytransactionList.size()==1){
					jobCategoryMaster=new JobCategoryMaster();
					jobCategoryMaster.setJobCategoryId(jobCategorytransactionList.get(0).getJobCategoryId());
					jobCategoryMasterList.add(jobCategoryMaster);
				}else{
					for(JobCategoryTransaction rec: jobCategorytransactionList){
						jobCategoryMaster=new JobCategoryMaster();
						jobCategoryMaster.setJobCategoryId(rec.getJobCategoryId());
						jobCategoryMasterList.add(jobCategoryMaster);
					}
				}
				
				List<Object> lstObjects=jobForTeacherDAO.getJobStatusByJobCategorys(jobCategoryMasterList,districtId);
				 //Collections.sort(lstObjects);		
				
				if(lstObjects!=null && lstObjects.size()>0)
				{
					sb.append("<label class='mb10'>The following jobs using this portfolio already have applications:</label>");
					sb.append("<table id='portfolioChngTblJob' width='100%' border='0' >");
					sb.append("<thead class='bg'>");
			        sb.append("<tr>");
			        sb.append("<th valign='top' style='color :white;'>Job Category Name</th><th valign='top' style='color :white;'>Job Id</th><th valign='top' style='color :white;'>Job Title</th><th valign='top' style='color :white;'>No of Candidate</th>");
			        sb.append("</tr>");
			        sb.append("</thead>");	
			        String tempName="";
			        String colorName="white";
			        int checkCounter=0;
			        int colorCount=0;
					for (Object row:lstObjects){
						Object[] arrObj = (Object[])row;
						String jobCategoryName=arrObj[3].toString();
			            Integer iJobId=Utility.getIntValue(arrObj[0].toString());
			            String jobTitle=arrObj[1].toString();
			            Integer iJobCount=Utility.getIntValue(arrObj[2].toString());
			            if(tempName.equalsIgnoreCase(jobCategoryName)){
			            	checkCounter=1;
			            }
			            if(checkCounter==0){
			            	 if(colorCount==0){
									colorName="white";
									colorCount=1;
								}else{
									colorName="#dff0d8";
									colorCount=0;
								}
			            	 sb.append("<tr style='background-color:"+colorName+"'>");
			            	tempName=jobCategoryName;
			            	sb.append("<td>"+tempName+"</td>");
			            checkCounter=1;
			            }else{
			            	sb.append("<tr style='background-color:"+colorName+"'>");
			            	 sb.append("<td></td>");
					            checkCounter=0;
			            }
			            sb.append("<td>"+iJobId+"</td>");
			            sb.append("<td>"+jobTitle+"</td>");
			            sb.append("<td>"+iJobCount+"</td>");
			            sb.append("</tr>");
					}
					sb.append("</table>");
					sb.append("<DIV class='mt30' id='promptMsg'>Updating the portfolio will only affect future job applications. Existing job applications will maintain the portfolio requirements prior to this change.<br>Would you like to continue?</DIV>");
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
	    }
		return sb.toString();
		}
}

