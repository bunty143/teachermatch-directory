package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.api.PaginationAndSortingAPI;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherProfileVisitHistory;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherProfileVisitHistoryDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.DistrictSpecificPortfolioAnswersDAO;
import tm.services.quartz.FTPConnectAndLogin;
import tm.utility.DistrictNameCompratorASC;
import tm.utility.DistrictNameCompratorDESC;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
public class NobleStSchoolPostionReportAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO  schoolInJobOrderDAO )
	{
		this.schoolInJobOrderDAO=schoolInJobOrderDAO;
	}

	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	
	public String displayRecordsByEntityType(String districtName,String schoolName,int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	 {
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=1;
			try{
				boolean flagfordata=true;
				if(!districtName.trim().equals("") && districtOrSchoolId==0)
					flagfordata=false;
				else if(!schoolName.equals("") && schoolId==0)
					flagfordata=false;
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				//System.out.println("schoolMaster="+schoolMaster+"districtMaster="+districtMaster+"entityID="+entityID);
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position


				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="noOfSchoolExpHires";
				String sortOrderNoField="noOfSchoolExpHires";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("noOfSchoolExpHires"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("jobId11"))
					sortingcheck=2;
				else if(sortOrder.equalsIgnoreCase("schoolId"))
					sortingcheck=3;
				else if(sortOrder.equalsIgnoreCase("schoolName"))
				{
					sortingcheck=4;				
				}else
					sortingcheck=5;
				
				//System.out.println("Sort order :: "+sortingcheck);
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<String[]> lstNobleSchoolPostion =new ArrayList<String[]>();
				
				
			
				if(flagfordata)
				if((entityID==2)||(entityID==3))
				{
					System.out.println("inside entity id=2");
					lstNobleSchoolPostion =schoolInJobOrderDAO.getNobleStSchoolPostion(districtOrSchoolId,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false);
			    	totalRecord = lstNobleSchoolPostion.size();
			    	 
				}
				else if(entityID==1) 
				{
					System.out.println("indside admin ");
					//if(districtOrSchoolId!=0)
						// districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
						lstNobleSchoolPostion =schoolInJobOrderDAO.getNobleStSchoolPostion(districtOrSchoolId,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false);
				    	totalRecord = lstNobleSchoolPostion.size();
				}
				
				if(flagfordata)
				{
					if(schoolId!=0)
					{
						//lstNobleSchoolPostion =teacherProfileVisitHistoryDAO.getSearchedData(districtOrSchoolId, schoolId,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false);
						//totalRecord = lstNobleSchoolPostion.size();
					}
				}
				
				List<String[]> finallstNobleSchoolPostion =new ArrayList<String[]>();
			     if(totalRecord<end)
						end=totalRecord;
			     finallstNobleSchoolPostion=lstNobleSchoolPostion.subList(start,end);
				
								
				String responseText="";
				
				tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingLink("Internal School ID",sortOrderNoField,"schoolId",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				        
				responseText=PaginationAndSorting.responseSortingLink("School Name",sortOrderNoField,"schoolName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Internal Job Id",sortOrderNoField,"jobId11",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Number of Postion",sortOrderNoField,"noOfSchoolExpHires",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
							
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				if(finallstNobleSchoolPostion.size()==0){
					 tmRecords.append("<tr><td colspan='9' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
					}
				
				  if(finallstNobleSchoolPostion.size()>0){
	                   for (Iterator it = finallstNobleSchoolPostion.iterator(); it.hasNext();)
	                   {
	                	   Object[] row = (Object[]) it.next(); 
	                	   
	                	   String  myVal1="";
	                	   String  myVal2="";
	                	                   	  
	                	        tmRecords.append("<tr>");	
	                	        	                            
	                            myVal1 = ((row[0]==null)||(row[0].toString().length()==0))? "0" : row[0].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
	                            
			                    
			                    myVal1 = (row[1]==null)? " " : row[1].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? "0" : row[2].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[3]==null)||(row[3].toString().length()==0))? "0" : row[3].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    			                  
			                    tmRecords.append("</tr>");
	               }               
	              }
				
			tmRecords.append("</table>");
			System.out.println("request="+request+"totalRecord="+totalRecord+"noOfRow="+noOfRow+"pageNo"+pageNo);
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			

			}catch(Exception e){
				e.printStackTrace();
			}
			
		 return tmRecords.toString();
	  }
		
		
	public String displayRecordsByEntityTypePDF(String districtName,String schoolName,int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport( districtName, schoolName, districtOrSchoolId, schoolId, noOfRow, pageNo, sortOrder, sortOrderType,basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	public boolean generateCandidatePDReport(String districtName,String schoolName,int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String path,String realPath)
	{
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			int sortingcheck=1;
			try{
				boolean flagfordata=true;
				if(!districtName.trim().equals("") && districtOrSchoolId==0)
					flagfordata=false;
				else if(!schoolName.equals("") && schoolId==0)
					flagfordata=false;
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				
				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				 /** set default sorting fieldName **/
				
				String sortOrderFieldName="noOfSchoolExpHires";
				String sortOrderNoField="noOfSchoolExpHires";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("noOfSchoolExpHires"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("jobId11"))
					sortingcheck=2;
				else if(sortOrder.equalsIgnoreCase("schoolId"))
					sortingcheck=3;
				else if(sortOrder.equalsIgnoreCase("schoolName"))
				{
					sortingcheck=4;				
				}else
					sortingcheck=5;
				
				//System.out.println("Sort order :: "+sortingcheck);
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<String[]> lstNobleSchoolPostion =new ArrayList<String[]>();
					
					if(flagfordata)
						if((entityID==2)||(entityID==3))
						{
							System.out.println("inside entity id=2");
							lstNobleSchoolPostion =schoolInJobOrderDAO.getNobleStSchoolPostion(districtOrSchoolId,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false);
					    	totalRecord = lstNobleSchoolPostion.size();
					    	 
						}
						else if(entityID==1) 
						{
							System.out.println("indside admin ");
							//if(districtOrSchoolId!=0)
								// districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
								lstNobleSchoolPostion =schoolInJobOrderDAO.getNobleStSchoolPostion(districtOrSchoolId,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false);
						    	totalRecord = lstNobleSchoolPostion.size();
						}
						
						if(flagfordata)
						{
							if(schoolId!=0)
							{
								//lstNobleSchoolPostion =teacherProfileVisitHistoryDAO.getSearchedData(districtOrSchoolId, schoolId,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false);
								//totalRecord = lstNobleSchoolPostion.size();
							}
						}
						
						List<String[]> finallstNobleSchoolPostion =new ArrayList<String[]>();
					     if(totalRecord<end)
								end=totalRecord;
					     finallstNobleSchoolPostion=lstNobleSchoolPostion;

								
			 // System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
				 String fontPath = realPath;
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject("Noble St School Postion");
						document.addCreationDate();
						document.addTitle("Noble St School Postion");

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						

						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						
						para[0] = new Paragraph("Noble St School Postion",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						
						
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph("Created By: "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+"Created on: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						document.add(new Phrase("\n"));


						float[] tblwidth={.16f,.18f,.16f,.08f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[8];
						cell = new PdfPCell[8];
				// header
						para[0] = new Paragraph("User Name",font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+"District Name",font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(""+"School Name",font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[2]);

						para[3] = new Paragraph(""+"User Title",font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[3]);
						
																	
						document.add(mainTable);
						
						if(finallstNobleSchoolPostion.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph("No Record Found.",font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
				     
						if(finallstNobleSchoolPostion.size()>0){
							for (Iterator it = finallstNobleSchoolPostion.iterator(); it.hasNext();) 					            
							 {
								Object[] row = (Object[]) it.next();
							  int index=0;
							 									
							  mainTable = new PdfPTable(tblwidth);
							  mainTable.setWidthPercentage(100);
							  para = new Paragraph[8];
							  cell = new PdfPCell[8];
							
                              String myVal1="";
                              String myVal2="";
                              
                                 
								 myVal1 = (row[0]==null)? " " : row[0].toString();
								 para[index] = new Paragraph( myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								
								 
								 myVal1 = (row[1]==null)? " " : row[1].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? " " : row[2].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[3]==null)? " " : row[3].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
										 
								

						document.add(mainTable);
				   }
				}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	
	public String displayRecordsByEntityTypePrintPreview(String districtName,String schoolName,int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
			boolean flagfordata=true;
			if(!districtName.trim().equals("") && districtOrSchoolId==0)
				flagfordata=false;
			else if(!schoolName.equals("") && schoolId==0)
				flagfordata=false;
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
				 /** set default sorting fieldName **/
				String sortOrderFieldName="noOfSchoolExpHires";
				String sortOrderNoField="noOfSchoolExpHires";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("noOfSchoolExpHires"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("jobId11"))
					sortingcheck=2;
				else if(sortOrder.equalsIgnoreCase("schoolId"))
					sortingcheck=3;
				else if(sortOrder.equalsIgnoreCase("schoolName"))
				{
					sortingcheck=4;				
				}else
					sortingcheck=5;
				
				//System.out.println("Sort order :: "+sortingcheck);
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<String[]> lstNobleSchoolPostion =new ArrayList<String[]>();
				
				if(flagfordata)
					if((entityID==2)||(entityID==3))
					{
						System.out.println("inside entity id=2");
						lstNobleSchoolPostion =schoolInJobOrderDAO.getNobleStSchoolPostion(districtOrSchoolId,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false);
				    	totalRecord = lstNobleSchoolPostion.size();
				    	 
					}
					else if(entityID==1) 
					{
						System.out.println("indside admin ");
						//if(districtOrSchoolId!=0)
							// districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
							lstNobleSchoolPostion =schoolInJobOrderDAO.getNobleStSchoolPostion(districtOrSchoolId,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false);
					    	totalRecord = lstNobleSchoolPostion.size();
					}
					
					if(flagfordata)
					{
						if(schoolId!=0)
						{
							//lstNobleSchoolPostion =teacherProfileVisitHistoryDAO.getSearchedData(districtOrSchoolId, schoolId,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false);
							//totalRecord = lstNobleSchoolPostion.size();
						}
					}
					
					List<String[]> finallstNobleSchoolPostion =new ArrayList<String[]>();
				     if(totalRecord<end)
							end=totalRecord;
				     finallstNobleSchoolPostion=lstNobleSchoolPostion;
				
			

	        tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>Noble St School Postion</div><br/>");
			
			tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+"Created by: "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>Date of Print: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");
			
			
			tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Internal School ID</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>School Name</th>");
    
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Intenal Job ID</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Number of Postion</th>");
			
					
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(finallstNobleSchoolPostion.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			}
		
			if(finallstNobleSchoolPostion.size()>0){
				 for (Iterator it = finallstNobleSchoolPostion.iterator(); it.hasNext();) 					            
					{
					Object[] row = (Object[]) it.next();
					
					 String myVal1="";
                     String myVal2="";
					  
                   
					 tmRecords.append("<tr>");
					 
					 
					     myVal1 = (row[0]==null)? " " : row[0].toString();
					   	tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
					 
						 myVal1 = (row[1]==null)? " " : row[1].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? " " : row[2].toString();
			      		tmRecords.append("<td style='font-size:12px;'>"+ myVal1+"</td>");	
			      		
			      		 myVal1 = (row[3]==null)? " " : row[3].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
												
													
					   tmRecords.append("</tr>");
			   }
			}
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
  }
	
	public String displayNobleStSchoolCSV(String districtName,String schoolName,int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		 System.out.println("inside displayNobleStSchoolCSV ");
		 String basePath = null;
			
			String fileName = null;
		 WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=1;
			try{
				
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				//System.out.println("schoolMaster="+schoolMaster+"districtMaster="+districtMaster+"entityID="+entityID);
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position


				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
				 /** set default sorting fieldName **/
				String sortOrderFieldName="noOfSchoolExpHires";
				String sortOrderNoField="noOfSchoolExpHires";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("noOfSchoolExpHires"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("jobId11"))
					sortingcheck=2;
				else if(sortOrder.equalsIgnoreCase("schoolId"))
					sortingcheck=3;
				else if(sortOrder.equalsIgnoreCase("schoolName"))
				{
					sortingcheck=4;				
				}else
					sortingcheck=5;
				
				//System.out.println("Sort order :: "+sortingcheck);
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				
			/**end set dynamic sorting fieldName **/
                  List<String[]> lstNobleSchoolPostion =new ArrayList<String[]>();
				
                  if((entityID==2)||(entityID==3))
					{
						System.out.println("inside entity id=2");
						lstNobleSchoolPostion =schoolInJobOrderDAO.getNobleStSchoolPostion(districtOrSchoolId,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false);
				    	totalRecord = lstNobleSchoolPostion.size();
				    	 
					}
					else if(entityID==1) 
					{
						System.out.println("indside admin ");
						//if(districtOrSchoolId!=0)
							// districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
							lstNobleSchoolPostion =schoolInJobOrderDAO.getNobleStSchoolPostion(districtOrSchoolId,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false);
					    	totalRecord = lstNobleSchoolPostion.size();
					}
					
					
					{
						if(schoolId!=0)
						{
							//lstNobleSchoolPostion =teacherProfileVisitHistoryDAO.getSearchedData(districtOrSchoolId, schoolId,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false);
							//totalRecord = lstNobleSchoolPostion.size();
						}
					}
				
				List<String[]> finallstNobleSchoolPostion =new ArrayList<String[]>();
			     if(totalRecord<end)
						end=totalRecord;
			     finallstNobleSchoolPostion=lstNobleSchoolPostion.subList(start,end);
			
			 
		   //Excel SmartFusion Exporting	

			     String time = String.valueOf(System.currentTimeMillis()).substring(6);
				
					 basePath = request.getSession().getServletContext().getRealPath ("/")+"/nobleschoolpostion";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="nobleschoolpostion"+time+".csv";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);
                   System.out.println("fileeeeeeeeeeee====="+file);
			 FileWriter writer = new FileWriter(file);
			 
		   //Delimiter used in CSV file
			final String COMMA_DELIMITER = ",";
			final String NEW_LINE_SEPARATOR = "\n";
			
			StringBuilder csvFileData = new StringBuilder();
			 
			csvFileData.append("Internal_School_ID");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("School_Name");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Internal_Job_ID");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Number_of_Positions");
					 	

				if(lstNobleSchoolPostion!=null)
				{
				 if(lstNobleSchoolPostion.size()>0){
				 totalRecord =  lstNobleSchoolPostion.size();
				 }
				
				 if(totalRecord>end){
					 end =totalRecord;
					 finallstNobleSchoolPostion =lstNobleSchoolPostion.subList(start, end);
				}else{
					 end =totalRecord;
					 finallstNobleSchoolPostion =lstNobleSchoolPostion.subList(start, end);
				}
				
				
				if(finallstNobleSchoolPostion.size()==0){
					csvFileData.append(NEW_LINE_SEPARATOR);
					csvFileData.append("No Rcords Founds");
				}
			
				 if(finallstNobleSchoolPostion.size()>0){
					// Integer count=1;
	                   for (Iterator it = finallstNobleSchoolPostion.iterator(); it.hasNext();)
	                   {
	                	   Object[] row = (Object[]) it.next();

						String  myVal1="";
						
						 myVal1 = (row[0]==null)? "0" : row[0].toString();
						 csvFileData.append(NEW_LINE_SEPARATOR);
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[1]==null)? " " : row[1].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[2]==null)? " " : row[2].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = (row[3]==null)? " " : row[3].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 					
						//count++;
					}
				}
			}else{
				
				csvFileData.append(NEW_LINE_SEPARATOR);
				csvFileData.append("No Rcords Founds");
					
			}
				  writer.append(csvFileData);
				  writer.flush();
				  writer.close();
				  
			}catch (Exception e) {
				e.printStackTrace();
			}
		  return basePath+"/"+fileName;
	  }	
	
	/*****************************/
	/*****************************/
	 public String displayNobleStSchoolCSVRun(SchoolInJobOrderDAO schoolInJobOrderDAO,ServletContext servletContext){
		 
		 System.out.println("Method call from schudler DSPA: "+schoolInJobOrderDAO+" servletContext :"+servletContext);
		 String basePath = null;
		String fileName = null;
		try{
				
			/**end set dynamic sorting fieldName **/
                  List lstNobleSchoolPostion =new ArrayList();
				
                  System.out.println("schoolInJobOrderDAO : "+schoolInJobOrderDAO);
                  lstNobleSchoolPostion =schoolInJobOrderDAO.getNobleStSchoolPostionCSV();
			    	
			    	List<String[]> finallstNobleSchoolPostion =new ArrayList<String[]>();
			   
			       finallstNobleSchoolPostion=lstNobleSchoolPostion;//.subList(start,end);
			System.out.println("finallstNobleSchoolPostion===="+finallstNobleSchoolPostion.size());
			    
			 String time = String.valueOf(System.currentTimeMillis()).substring(6);
			 /***************************creating directory inside  ***rootPath*******************************************/	
			    FileOutputStream outStream = null;
			    fileName ="nobleschoolpostion.csv";
			    String schoolPostion = Utility.getValueOfPropByKey("rootPath"); 
				System.out.println("school Postion : "+schoolPostion);	
				
				String sourceDirName = schoolPostion+"//"+"NobleSchoolPostion"; 
				String targetFileName = sourceDirName+"/"+fileName; ;
				
				File fileSchoolPostion = new File(sourceDirName);
				
				File sourceFile= new File(targetFileName);
				System.out.println("sourceFile========"+sourceFile);
				System.out.println("schoolPostion.exists() before : "+fileSchoolPostion.exists());
				
				if(!fileSchoolPostion.exists())
				{
					fileSchoolPostion.mkdir();
					outStream = new FileOutputStream(sourceFile);
					System.out.println("directory created succesffully");
				}
				if(sourceFile.exists())
				{
					outStream = new FileOutputStream(targetFileName);
					System.out.println("sourceFile.exists()writing Noble School Postion report : "+sourceFile.exists());
				}
				
		/********************************************************************/	
			 
			 Utility.deleteAllFileFromDir(sourceDirName);
			// file = new File(basePath+"/"+fileName);
			 
			 FileWriter writer = new FileWriter(sourceFile);
			 
		   //Delimiter used in CSV file
			 final String COMMA_DELIMITER = ",";
				final String NEW_LINE_SEPARATOR = "\n";
				
				StringBuilder csvFileData = new StringBuilder();
				 
				csvFileData.append("Internal_School_ID");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("School_Name");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Internal_Job_ID");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Number_of_Positions");
					 	

				if(lstNobleSchoolPostion!=null)
				{
							
				if(finallstNobleSchoolPostion.size()==0){
					csvFileData.append(NEW_LINE_SEPARATOR);
					csvFileData.append("No Rcords Founds");
				}
			
				 if(finallstNobleSchoolPostion.size()>0){
					// Integer count=1;
	                   for (Iterator it = finallstNobleSchoolPostion.iterator(); it.hasNext();)
	                   {
	                	   Object[] row = (Object[]) it.next();

							String  myVal1="";
							
							 myVal1 = (row[0]==null)? "0" : row[0].toString();
							 csvFileData.append(NEW_LINE_SEPARATOR);
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[1]==null)? " " : row[1].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[2]==null)? " " : row[2].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = (row[3]==null)? " " : row[3].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
						
						
						//count++;
					}
				}
			}else{
				
				csvFileData.append(NEW_LINE_SEPARATOR);
				csvFileData.append("No Rcords Founds");
					
			}
				  writer.append(csvFileData);
				  writer.flush();
				  writer.close();
				  
				  writeNobleReportONServer(servletContext);
				  
			}catch (Exception e) {
				e.printStackTrace();
			}
		  return basePath+"/"+fileName;
	  }	
	 /*****************************/
	
	 
	 public void writeNobleReportONServer(ServletContext servletContext){
		  System.out.println("************writing Noble School Postion on server inside ******************* servletContext: "+servletContext);
		    try{
		     String path="";
		   
		     String source = Utility.getValueOfPropByKey("rootPath")+"NobleSchoolPostion/nobleschoolpostion.csv";
			 System.out.println("source Path "+source);

			 String target = servletContext.getRealPath("/")+"/"+"/NobleSchoolPostion/";
			 System.out.println("Real target Path "+target);

			File sourceFile = new File(source);
			File targetDir = new File(target);
			
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());

			try {
				FileUtils.copyFile(sourceFile, targetFile);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			path = Utility.getValueOfPropByKey("contextBasePath")+"/NobleSchoolPostion/nobleschoolpostion.csv";
			System.out.println("NobleSchoolPostion contextBasePath "+path);
		  
			
			FTPConnectAndLogin.dropFileOnFTPServerForNobleSchoolPostion(target+"nobleschoolpostion.csv");
			
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
			
		}
	
}


