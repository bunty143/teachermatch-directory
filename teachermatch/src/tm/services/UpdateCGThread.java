package tm.services;

import tm.dao.cgreport.PercentileCalculationDAO;
import tm.dao.cgreport.PercentileNationalCompositeTScoreDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.services.quartz.DataProcessService;
import tm.services.report.CGReportService;


public class UpdateCGThread extends Thread{

	private CGReportService cGReportService;
	public void setcGReportService(CGReportService cGReportService) {
		this.cGReportService = cGReportService;
	}
	
	private PercentileNationalCompositeTScoreDAO percentileNationalCompositeTScoreDAO;
	public void setPercentileNationalCompositeTScoreDAO(PercentileNationalCompositeTScoreDAO percentileNationalCompositeTScoreDAO) {
		this.percentileNationalCompositeTScoreDAO = percentileNationalCompositeTScoreDAO;
	}
	
	private PercentileCalculationDAO percentileCalculationDAO;
	public void setPercentileCalculationDAO(PercentileCalculationDAO percentileCalculationDAO){
		this.percentileCalculationDAO = percentileCalculationDAO;
	}
	
	private RawDataForDomainDAO rawDataForDomainDAO;
	public void setRawDataForDomainDAO(RawDataForDomainDAO rawDataForDomainDAO) {
		this.rawDataForDomainDAO = rawDataForDomainDAO;
	}
	
	public UpdateCGThread() {
		super();
	}
	
	public void run()
	{
		try {
			
			//cGReportService.updateRawDataByUser();
			try {
				System.gc();				
			}catch (Exception e){
				e.printStackTrace();
			}
			
			try {/*
				percentileNationalCompositeTScoreDAO.truncate();			
				System.out.println("updateCgDataForNewApplyJob.do");

				Map<String, PercentileCalculation> mapNationalPercentile = new HashMap<String, PercentileCalculation>();
				List<PercentileCalculation> lstPercentileCalculation = percentileCalculationDAO.findAllInCurrenctYear();
				for(PercentileCalculation percentileCalculation : lstPercentileCalculation){
					mapNationalPercentile.put(percentileCalculation.getDomainMaster().getDomainId()+"##"+percentileCalculation.getScore(), percentileCalculation);
				}			
				List<RawDataForDomain> lstRawDataForDomains = rawDataForDomainDAO.findAllInCurrentYear();
				System.out.println("lstRawDataForDomains.size()"+lstRawDataForDomains.size());
				
				Collections.sort(lstRawDataForDomains,RawDataForDomain.comparatorRawDataForDomainByTeacherId );
				
				PercentileNationalCompositeTScore percentileNationalCompositeTScore = null;
				List<Double> lstCompositTScore = new LinkedList<Double>();
				PercentileCalculation percentileCalculation = null;
				double compositeTScore = 0.00;
				Integer teacherIdTemp = lstRawDataForDomains.get(0).getTeacherDetail().getTeacherId();
				DecimalFormat oneDForm = new DecimalFormat("###,###.0");
				for(RawDataForDomain rawDataForDomain : lstRawDataForDomains){				
					if(!rawDataForDomain.getTeacherDetail().getTeacherId().equals(teacherIdTemp)){					
						lstCompositTScore.add(Double.valueOf(oneDForm.format(compositeTScore)));
						teacherIdTemp=rawDataForDomain.getTeacherDetail().getTeacherId();					
						compositeTScore = 0.0;
					}
					percentileCalculation = mapNationalPercentile.get(rawDataForDomain.getDomainMaster().getDomainId()+"##"+Math.round(rawDataForDomain.getScore()));
					compositeTScore = compositeTScore + percentileCalculation.gettValue()*percentileCalculation.getDomainMaster().getMultiplier();				
				}			
				lstCompositTScore.add(Double.valueOf(oneDForm.format(compositeTScore)));
				
				cGReportService.insertorUpdateCompositeTValue(lstCompositTScore);
				
				System.out.println(lstCompositTScore);
				System.out.println(lstCompositTScore.size());
			*/} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				System.gc();				
			} catch (Exception e) {
				e.printStackTrace();
			}
			//cGReportService.updateJobPercentileByUserOrThread();	
			
			//System.out.println("CG Updated Successfully...............");
			
			DataProcessService dataProcessService = new DataProcessService();
			dataProcessService.execute(null);
			
			System.out.println("Mosaic Updated........................");
		}
		catch (Exception e){
			e.printStackTrace();
		}		
	}
	
}
