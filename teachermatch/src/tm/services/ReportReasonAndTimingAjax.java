package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.DistrictSpecificReason;
import tm.bean.DistrictSpecificTiming;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.SchoolSelectedByCandidate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.DistrictSpecificReasonDAO;
import tm.dao.DistrictSpecificTimingDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.StatusMasterDAO;
import tm.utility.DistrictNameCompratorASC;
import tm.utility.DistrictNameCompratorDESC;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
public class ReportReasonAndTimingAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	
	@Autowired
	private DistrictSpecificTimingDAO districtSpecificTimingDAO;
	
	@Autowired
	private DistrictSpecificReasonDAO districtSpecificReasonDAO;

	public void setDistrictSpecificTimingDAO(
			DistrictSpecificTimingDAO districtSpecificTimingDAO) {
		this.districtSpecificTimingDAO = districtSpecificTimingDAO;
	}

	public void setDistrictSpecificReasonDAO(
			DistrictSpecificReasonDAO districtSpecificReasonDAO) {
		this.districtSpecificReasonDAO = districtSpecificReasonDAO;
	}
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;

	public void setTeacherStatusHistoryForJobDAO(
			TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO) {
		this.teacherStatusHistoryForJobDAO = teacherStatusHistoryForJobDAO;
	}

	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	public String displayRecordsByEntityType(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String reason,String timing,String searchbystatus)
	 {
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=1;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}

					int noOfRowInPage = Integer.parseInt(noOfRow);
					int pgNo = Integer.parseInt(pageNo);
					int start =((pgNo-1)*noOfRowInPage);
					int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
					int totalRecord = 0;
					
					List<JobForTeacher> lstJobForTeachers =new ArrayList<JobForTeacher>();
					
					if(entityID==2)
					{
					 lstJobForTeachers = jobForTeacherDAO.getReasonAndTiming(districtMaster,searchbystatus); 
					}
					else if(entityID==1) 
					{
						if(districtOrSchoolId!=0)
							 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
						 lstJobForTeachers = jobForTeacherDAO.getReasonAndTiming(districtMaster,searchbystatus);	 
					}
				
				 List<JobForTeacher> finalJobForTeachers =new ArrayList<JobForTeacher>(); 
				 if(sortingcheck==1)
				 {
					 for(JobForTeacher jft : lstJobForTeachers){
						 jft.setDistName(jft.getTeacherId().getLastName());
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
					 else 
					      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
				 }
				 
				 List<TeacherDetail> teacherDetail=new ArrayList<TeacherDetail>();
				 List<JobOrder> jobOrder=new ArrayList<JobOrder>();
				 totalRecord=lstJobForTeachers.size();
				
				 for (JobForTeacher jobForTeacher : lstJobForTeachers) {
					teacherDetail.add(jobForTeacher.getTeacherId());
					jobOrder.add(jobForTeacher.getJobId());
				 }
				
					System.out.println("=====================lstForTeachers.size() :: "+lstJobForTeachers.size()+"     ::  "+sortOrderType);

					//sorting to check
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorTeacherId);
					 else 
					      Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorTeacherId);
				 
				 
				 
				 
				List<TeacherStatusHistoryForJob> teacherStatusHistoryForJob= new ArrayList<TeacherStatusHistoryForJob>();
				
				try {
					teacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findReasonAndTiming(teacherDetail,jobOrder,reason,timing,searchbystatus);
				} catch (Exception e) {
				}
				System.out.println("????>>>>>>>>>>>>>>>>>>>>>>>>>>>>   "+teacherStatusHistoryForJob.size());

				Map<String,String> teacherStatusMap=new HashMap<String,String>();
				
				if (teacherStatusHistoryForJob.size()>0) {
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob2 : teacherStatusHistoryForJob) {
					String string=teacherStatusHistoryForJob2.getTimingforDclWrdw().getTiming().toString().concat("~").concat(teacherStatusHistoryForJob2.getReasonforDclWrdw().getReason().toString());
					teacherStatusMap.put(teacherStatusHistoryForJob2.getTeacherDetail().getTeacherId().toString().concat("~").concat(teacherStatusHistoryForJob2.getJobOrder().getJobId().toString()),string );
				}
				}
				List<JobForTeacher> jobForTeachers=new ArrayList<JobForTeacher>();
				 for (JobForTeacher jobForTeacher : lstJobForTeachers) {
					 for (Map.Entry<String, String> entry : teacherStatusMap.entrySet()) {
						    if(entry.getKey().equals(jobForTeacher.getTeacherId().getTeacherId().toString().concat("~").concat(jobForTeacher.getJobId().getJobId().toString()))){
						    	String string[]=entry.getValue().split("~");
						    	System.out.println("Key = " + entry.getKey()+"\tmail id   "+jobForTeacher.getTeacherId().getTeacherId() );
						    	jobForTeacher.setDistName(string[0]);
						    	jobForTeacher.setStafferName(string[1]);
						    	jobForTeachers.add(jobForTeacher);
						    }
						}
				 }
				 totalRecord=jobForTeachers.size();
				 if(totalRecord<end)
						end=totalRecord;
					 finalJobForTeachers	=	jobForTeachers.subList(start,end);
					 
					 /** set default sorting fieldName **/
						String sortOrderFieldName="lastName";
						String sortOrderNoField="lastName";

						if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("lastName"))
							sortingcheck=1;
						else if(sortOrder.equalsIgnoreCase("jobId"))
							sortingcheck=2;
						else  if(sortOrder.equalsIgnoreCase("requisitionNumber")){
							sortingcheck=3;
						}else
							sortingcheck=4;
						
						/**Start set dynamic sorting fieldName **/
						
						Order  sortOrderStrVal=null;

						if(sortOrder!=null){
							if(!sortOrder.equals("") && !sortOrder.equals(null)){
								sortOrderFieldName=sortOrder;
								sortOrderNoField=sortOrder;
							}
						}

						String sortOrderTypeVal="0";
						if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
							if(sortOrderType.equals("0")){
								sortOrderStrVal=Order.asc(sortOrderFieldName);
							}else{
								sortOrderTypeVal="1";
								sortOrderStrVal=Order.desc(sortOrderFieldName);
							}
						}else{
							sortOrderTypeVal="0";
							sortOrderStrVal=Order.asc(sortOrderFieldName);
						}
					 
					 
				// SORT by JobId
				 if(sortOrder.equals("jobId"))
				 {
					 if(sortOrderType.equals("0"))
						  Collections.sort(finalJobForTeachers, JobForTeacher.jobForTeacherComparatorId);
					 else 
					      Collections.sort(finalJobForTeachers, JobForTeacher.jobForTeacherComparatorIdDesc);
				 }
				
				 
					// SORT by Status
					 if(sortOrder.equals("status"))
					 {
						 if(sortOrderType.equals("0"))
							  Collections.sort(finalJobForTeachers, JobForTeacher.jobForTeacherStatusComparatorId);
						 else 
						      Collections.sort(finalJobForTeachers, JobForTeacher.jobForTeacherStatusComparatorIdDesc);
					 }
				 
					// SORT by Timing
					 if(sortOrder.equals("timing"))
					 {
						 if(sortOrderType.equals("0"))
							  Collections.sort(finalJobForTeachers, JobForTeacher.jobForTeacherTimingComparatorId);
						 else 
						      Collections.sort(finalJobForTeachers, JobForTeacher.jobForTeacherTimingComparatorIdDesc);
					 }
					 
					// SORT by Reason
					 if(sortOrder.equals("reason"))
					 {
						 if(sortOrderType.equals("0"))
							  Collections.sort(finalJobForTeachers, JobForTeacher.jobForTeacherReasonComparatorId);
						 else 
						      Collections.sort(finalJobForTeachers, JobForTeacher.jobForTeacherReasonComparatorIdDesc);
					 }
				//System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::     "+string2.length);
				String responseText="";
				
				tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblApplicantName", locale),sortOrderNoField,"lastName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobId", locale),sortOrderNoField,"jobId",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Date of Application",sortOrderNoField,"dateofapp",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblStatus", locale),sortOrderNoField,"status",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Date of Decline or Withdraw",sortOrderNoField,"requisitionNumber",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Timing Selection",sortOrderNoField,"timing",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Reason Selection",sortOrderNoField,"reason",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				tmRecords.append("<tbody>");
				
				if(finalJobForTeachers.size()==0){
				 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
				}

				if(finalJobForTeachers.size()>0){
				 for(JobForTeacher jft:finalJobForTeachers) 
				 {
				  tmRecords.append("<tr>");	
					
					tmRecords.append("<td>"+jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")"+"</td>");
					tmRecords.append("<td>"+jft.getJobId().getJobId()+"</td>");
					
					if(jft.getCreatedDateTime()!=null){
					 tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getCreatedDateTime())+"</td>");
					}else{
						tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					}
					
					if(jft.getStatus()!=null)
					{
					tmRecords.append("<td>"+jft.getStatus().getStatus()+"</td>");
					}else{
						tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					}
					
					 if(jft.getStatus()!=null)
					 {
						 if(jft.getStatus().getStatusId().equals(19)&&jft.getUpdatedDate()!=null)
					  tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getUpdatedDate())+"</td>");
						 else if(jft.getStatus().getStatusId().equals(7)&&jft.getWithdrwanDateTime()!=null)
							 tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getWithdrwanDateTime())+"</td>");
						 else
							 tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					 }else{
						tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					  }
					 
					 tmRecords.append("<td>"+jft.getDistName()+"</td>");
					 tmRecords.append("<td>"+jft.getStafferName()+"</td>");
					 
				  tmRecords.append("</tr>");
				 }
				}

			tmRecords.append("</tbody>");
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));

			}catch(Exception e){
				e.printStackTrace();
			}
			
		 return tmRecords.toString();
	  }
		
	public String displayRecordsByEntityTypeEXL(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String reason,String timing,String searchbystatus)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@EXL "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		int sortingcheck=1;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				List<JobForTeacher> lstJobForTeachers =new ArrayList<JobForTeacher>();
				
				if(entityID==2)
				{
				 lstJobForTeachers = jobForTeacherDAO.getReasonAndTiming(districtMaster,searchbystatus); 
				}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0)
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					lstJobForTeachers = jobForTeacherDAO.getReasonAndTiming(districtMaster,searchbystatus);
				}
			
				System.out.println("=====================lstForTeachers.size() :: "+lstJobForTeachers.size());
			 
			 
			 List<TeacherDetail> teacherDetail=new ArrayList<TeacherDetail>();
			 List<JobOrder> jobOrder=new ArrayList<JobOrder>();
			
			 for (JobForTeacher jobForTeacher : lstJobForTeachers) {
				teacherDetail.add(jobForTeacher.getTeacherId());
				jobOrder.add(jobForTeacher.getJobId());
			 }
			//sorting to check
			 if(sortOrderType.equals("0"))
				  Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorTeacherId);
			 else 
			      Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorTeacherId);
			 
			List<TeacherStatusHistoryForJob> teacherStatusHistoryForJob= new ArrayList<TeacherStatusHistoryForJob>();
			teacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findReasonAndTiming(teacherDetail,jobOrder,reason,timing,searchbystatus);
			
			Map<String,String> teacherStatusMap=new HashMap<String,String>();
			
			if (teacherStatusHistoryForJob.size()>0) {
			for (TeacherStatusHistoryForJob teacherStatusHistoryForJob2 : teacherStatusHistoryForJob) {
				String string=teacherStatusHistoryForJob2.getTimingforDclWrdw().getTiming().toString().concat("~").concat(teacherStatusHistoryForJob2.getReasonforDclWrdw().getReason().toString());
				teacherStatusMap.put(teacherStatusHistoryForJob2.getTeacherDetail().getTeacherId().toString().concat("~").concat(teacherStatusHistoryForJob2.getJobOrder().getJobId().toString()),string );
			}
			}
			List<JobForTeacher> jobForTeachers=new ArrayList<JobForTeacher>();
			 for (JobForTeacher jobForTeacher : lstJobForTeachers) {
				 for (Map.Entry<String, String> entry : teacherStatusMap.entrySet()) {
					 if(entry.getKey().equals(jobForTeacher.getTeacherId().getTeacherId().toString().concat("~").concat(jobForTeacher.getJobId().getJobId().toString()))){
					    	String string[]=entry.getValue().split("~");
					    	jobForTeacher.setDistName(string[0]);
					    	jobForTeacher.setStafferName(string[1]);
					    	jobForTeachers.add(jobForTeacher);
					    }
					}
			 }
			 /** set default sorting fieldName **/
				String sortOrderFieldName="lastName";
				String sortOrderNoField="lastName";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("lastName"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("jobId"))
					sortingcheck=2;
				else  if(sortOrder.equalsIgnoreCase("requisitionNumber")){
					sortingcheck=3;
				}else
					sortingcheck=4;
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			  
			// SORT by JobId
			 if(sortOrder.equals("jobId"))
			 {
				 for(JobForTeacher jft : jobForTeachers){
					 jft.setJobId(jft.getJobId());
				 }
				 if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherComparatorId);
				 else 
				      Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherComparatorIdDesc);
			 }
			// SORT by Status
			 if(sortOrder.equals("status"))
			 {
				 if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherStatusComparatorId);
				 else 
				      Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherStatusComparatorIdDesc);
			 }
		 
			// SORT by Timing
			 if(sortOrder.equals("timing"))
			 {
				 if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherTimingComparatorId);
				 else 
				      Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherTimingComparatorIdDesc);
			 }
			 
			// SORT by Reason
			 if(sortOrder.equals("reason"))
			 {
				 if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherReasonComparatorId);
				 else 
				      Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherReasonComparatorIdDesc);
			 }
			 //  Excel   Exporting	
					
					String time = String.valueOf(System.currentTimeMillis()).substring(6);
					//String basePath = request.getRealPath("/")+"/candidate";
					String basePath = request.getSession().getServletContext().getRealPath ("/")+"/hiredapplicants";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="hiredapplicants"+time+".xls";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

					WorkbookSettings wbSettings = new WorkbookSettings();

					wbSettings.setLocale(new Locale("en", "EN"));

					WritableCellFormat timesBoldUnderline;
					WritableCellFormat header;
					WritableCellFormat headerBold;
					WritableCellFormat times;

					WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
					workbook.createSheet("reasonandtiming", 0);
					WritableSheet excelSheet = workbook.getSheet(0);

					WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
					// Define the cell format
					times = new WritableCellFormat(times10pt);
					// Lets automatically wrap the cells
					times.setWrap(true);

					// Create create a bold font with unterlines
					WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
					WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

					timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
					timesBoldUnderline.setAlignment(Alignment.CENTRE);
					// Lets automatically wrap the cells
					timesBoldUnderline.setWrap(true);

					header = new WritableCellFormat(times10ptBoldUnderline);
					headerBold = new WritableCellFormat(times10ptBoldUnderline);
					CellView cv = new CellView();
					cv.setFormat(times);
					cv.setFormat(timesBoldUnderline);
					cv.setAutosize(true);

					header.setBackground(Colour.GRAY_25);

					// Write a few headers
					excelSheet.mergeCells(0, 0, 6, 1);
					Label label;
					label = new Label(0, 0,Utility.getLocaleValuePropByKey("lblReasonTiming", locale) , timesBoldUnderline);
					excelSheet.addCell(label);
					excelSheet.mergeCells(0, 3, 5, 3);
					label = new Label(0, 3, "");
					excelSheet.addCell(label);
					excelSheet.getSettings().setDefaultColumnWidth(18);
					
					int k=4;
					int col=1;
					label = new Label(0, k, Utility.getLocaleValuePropByKey("lblApplicantName", locale),header); 
					excelSheet.addCell(label);
					label = new Label(1, k, Utility.getLocaleValuePropByKey("lbljobid", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, "Date of Application",header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, "Status",header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, "Date of Decline or Withdraw",header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, "Timing Selection",header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, "Reason Selection",header); 
					excelSheet.addCell(label);
					
					k=k+1;
					if(jobForTeachers.size()==0)
					{	
						excelSheet.mergeCells(0, k, 7, k);
						label = new Label(0, k, Utility.getLocaleValuePropByKey("msgNorecordfound", locale)); 
						excelSheet.addCell(label);
					}
					
			if(jobForTeachers.size()>0){
			 for(JobForTeacher jft :jobForTeachers) 
			 {
				col=1;
			
			    label = new Label(0, k, jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")"); 
				excelSheet.addCell(label);
				
				
				label = new Label(1, k, jft.getJobId().getJobId().toString()); 
				excelSheet.addCell(label);
				
				//Norm Score Sorting
				if(jft.getCreatedDateTime()!=null){
					label = new Label(++col, k, Utility.convertDateAndTimeToUSformatOnlyDate(jft.getCreatedDateTime())+""); 
					excelSheet.addCell(label);
				}else{
					label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("optN/A", locale)+""); 
					excelSheet.addCell(label);
				}
				//HIRED BY DATE
				if(jft.getStatus()!=null)
				 {
					 label = new Label(++col, k,jft.getStatus().getStatus()); 
						excelSheet.addCell(label);
				 }else{
					 label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("optN/A", locale)+""); 
						excelSheet.addCell(label);
				  }
				//for position Number
				 if(jft.getStatus()!=null)
				 {
					 if(jft.getStatus().getStatusId().equals(19)&&jft.getUpdatedDate()!=null)
					 label = new Label(++col, k,Utility.convertDateAndTimeToUSformatOnlyDate(jft.getUpdatedDate())); 
					 else if(jft.getStatus().getStatusId().equals(7)&&jft.getWithdrwanDateTime()!=null)
						 label = new Label(++col, k,Utility.convertDateAndTimeToUSformatOnlyDate(jft.getWithdrwanDateTime())); 
					 else
						 label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("optN/A", locale)+""); 
						excelSheet.addCell(label);
				 }else{
					 label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("optN/A", locale)+""); 
						excelSheet.addCell(label);
				  }
				//for School
				 
					label = new Label(++col, k, ""+jft.getDistName()+"");
					excelSheet.addCell(label);
					label = new Label(++col, k, ""+jft.getStafferName()+""); 
					excelSheet.addCell(label);
					
				 k++;
			 }
			}
			workbook.write();
			workbook.close();
		}catch(Exception e){}
		
	 return fileName;
  }
	
  
	
	public String displayRecordsByEntityTypePDF(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String reason,String timing,String searchbystatus)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport( districtOrSchoolId, schoolId, noOfRow, pageNo, sortOrder, sortOrderType,basePath+"/"+fileName,context.getServletContext().getRealPath("/"),reason,timing,searchbystatus);
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	public boolean generateCandidatePDReport(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String path,String realPath,String reason,String timing,String searchbystatus)
	{
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			int sortingcheck=1;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return false;
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}

					int noOfRowInPage = Integer.parseInt(noOfRow);
					int pgNo = Integer.parseInt(pageNo);
					int start =((pgNo-1)*noOfRowInPage);
					int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
					int totalRecord = 0;
					
					
					List<JobForTeacher> lstJobForTeachers =new ArrayList<JobForTeacher>();
					
					if(entityID==2)
					{
					 lstJobForTeachers = jobForTeacherDAO.getReasonAndTiming(districtMaster,searchbystatus); 
					}
					else if(entityID==1) 
					{
						if(districtOrSchoolId!=0)
							 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
						lstJobForTeachers = jobForTeacherDAO.getReasonAndTiming(districtMaster,searchbystatus);	
					}
				
					System.out.println("=====================lstForTeachers.size() :: "+lstJobForTeachers.size());

				 
				 List<TeacherDetail> teacherDetail=new ArrayList<TeacherDetail>();
				 List<JobOrder> jobOrder=new ArrayList<JobOrder>();
				 totalRecord=lstJobForTeachers.size();
				
				 for (JobForTeacher jobForTeacher : lstJobForTeachers) {
					teacherDetail.add(jobForTeacher.getTeacherId());
					jobOrder.add(jobForTeacher.getJobId());
				 }
				 //sorting to check
				 if(sortOrderType.equals("0"))
					  Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorTeacherId);
				 else 
				      Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorTeacherId);
				
				List<TeacherStatusHistoryForJob> teacherStatusHistoryForJob= new ArrayList<TeacherStatusHistoryForJob>();
				teacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findReasonAndTiming(teacherDetail,jobOrder,reason,timing,searchbystatus);
				
				Map<String,String> teacherStatusMap=new HashMap<String,String>();
				
				if (teacherStatusHistoryForJob.size()>0) {
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob2 : teacherStatusHistoryForJob) {
					String string=teacherStatusHistoryForJob2.getTimingforDclWrdw().getTiming().toString().concat("~").concat(teacherStatusHistoryForJob2.getReasonforDclWrdw().getReason().toString());
					teacherStatusMap.put(teacherStatusHistoryForJob2.getTeacherDetail().getTeacherId().toString().concat("~").concat(teacherStatusHistoryForJob2.getJobOrder().getJobId().toString()),string );
				}
				}
				List<JobForTeacher> jobForTeachers=new ArrayList<JobForTeacher>();
				 for (JobForTeacher jobForTeacher : lstJobForTeachers) {
					 for (Map.Entry<String, String> entry : teacherStatusMap.entrySet()) {
						 if(entry.getKey().equals(jobForTeacher.getTeacherId().getTeacherId().toString().concat("~").concat(jobForTeacher.getJobId().getJobId().toString()))){
						    	String string[]=entry.getValue().split("~");
						    	jobForTeacher.setDistName(string[0]);
						    	jobForTeacher.setStafferName(string[1]);
						    	jobForTeachers.add(jobForTeacher);
						    }
						}
				 }
				 
				 /** set default sorting fieldName **/
					String sortOrderFieldName="lastName";
					String sortOrderNoField="lastName";

					if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("lastName"))
						sortingcheck=1;
					else if(sortOrder.equalsIgnoreCase("jobId"))
						sortingcheck=2;
					else  if(sortOrder.equalsIgnoreCase("requisitionNumber")){
						sortingcheck=3;
					}else
						sortingcheck=4;
					
					/**Start set dynamic sorting fieldName **/
					
					Order  sortOrderStrVal=null;

					if(sortOrder!=null){
						if(!sortOrder.equals("") && !sortOrder.equals(null)){
							sortOrderFieldName=sortOrder;
							sortOrderNoField=sortOrder;
						}
					}

					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("0")){
							sortOrderStrVal=Order.asc(sortOrderFieldName);
						}else{
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
						}
					}else{
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}
					
				 
				// SORT by JobId
				 if(sortOrder.equals("jobId"))
				 {
					 for(JobForTeacher jft : jobForTeachers){
						 jft.setJobId(jft.getJobId());
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherComparatorId);
					 else 
					      Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherComparatorIdDesc);
				 }
				// SORT by Status
				 if(sortOrder.equals("status"))
				 {
					 if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherStatusComparatorId);
					 else 
					      Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherStatusComparatorIdDesc);
				 }
			 
				// SORT by Timing
				 if(sortOrder.equals("timing"))
				 {
					 if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherTimingComparatorId);
					 else 
					      Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherTimingComparatorIdDesc);
				 }
				 
				// SORT by Reason
				 if(sortOrder.equals("reason"))
				 {
					 if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherReasonComparatorId);
					 else 
					      Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherReasonComparatorIdDesc);
				 }
			 // System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
				 String fontPath = realPath;
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject(Utility.getLocaleValuePropByKey("headAppHir", locale));
						document.addCreationDate();
						document.addTitle(Utility.getLocaleValuePropByKey("headAppHir", locale));

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						

						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblReasonTiming", locale),font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						
						
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						document.add(new Phrase("\n"));


						float[] tblwidth={.22f,.22f,.08f,.08f,.08f,.24f,.09f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[7];
						cell = new PdfPCell[7];
				// header
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblApplicantName", locale),font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph("Job Id",font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph("Date of Application",font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[2]);
						
						//Hired by date 
						para[3] = new Paragraph("Status",font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[3]);

						para[4] = new Paragraph("Date of Decline or Withdraw",font10_10);
						cell[4]= new PdfPCell(para[4]);
						cell[4].setBackgroundColor(bluecolor);
						cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[4]);
						
						para[5] = new Paragraph("Timing Selection",font10_10);
						cell[5]= new PdfPCell(para[5]);
						cell[5].setBackgroundColor(bluecolor);
						cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[5]);
						
						para[6] = new Paragraph("Reason Selection",font10_10);
						cell[6]= new PdfPCell(para[6]);
						cell[6].setBackgroundColor(bluecolor);
						cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[6]);
						
						document.add(mainTable);
						
						if(jobForTeachers.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgNorecordfound", locale),font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
				     
						if(jobForTeachers.size()>0){
							 for(JobForTeacher jft :jobForTeachers) 
							 {
							  int index=0;
							  float[] tblwidth1={.22f,.22f,.08f,.08f,.08f,.24f,.09f};
									
							  mainTable = new PdfPTable(tblwidth1);
							  mainTable.setWidthPercentage(100);
							  para = new Paragraph[7];
							  cell = new PdfPCell[7];

							 para[index] = new Paragraph(""+jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")",font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
							 
							 para[index] = new Paragraph(""+jft.getJobId().getJobId(),font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
								
								//Norm Score Sorting
							if(jft.getCreatedDateTime()!=null){
								para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getCreatedDateTime()),font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
							}else{
								para[index] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("optN/A", locale)+"",font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
							}
							
							//for hired by date SWADESH
							if(jft.getStatus()!=null){
								para[index] = new Paragraph(""+jft.getStatus().getStatus(),font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
							}else{
								para[index] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("optN/A", locale)+"",font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
							}
								
							//for position Number
							 if(jft.getStatus()!=null)
							 {
								 if(jft.getStatus().getStatusId().equals(19)&&jft.getUpdatedDate()!=null)
								 para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getUpdatedDate()),font8bold);
								 else if(jft.getStatus().getStatusId().equals(7)&&jft.getWithdrwanDateTime()!=null)
								 para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getWithdrwanDateTime()),font8bold);
								 else
								 para[index] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("optN/A", locale)+"",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							 }else{
								 para[index] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("optN/A", locale)+"",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							  }
							//for School
									para[index] = new Paragraph(""+""+jft.getDistName()+"",font8bold);
									cell[index]= new PdfPCell(para[index]);
									cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									mainTable.addCell(cell[index]);
									index++;
									para[index] = new Paragraph(""+""+jft.getStafferName()+"",font8bold);
									cell[index]= new PdfPCell(para[index]);
									cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									mainTable.addCell(cell[index]);
									index++;
						document.add(mainTable);
				   }
				}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	
	public String displayRecordsByEntityTypePrintPreview(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String reason,String timing,String searchbystatus)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				List<JobForTeacher> lstJobForTeachers =new ArrayList<JobForTeacher>();
				
				if(entityID==2)
				{
				 lstJobForTeachers = jobForTeacherDAO.getReasonAndTiming(districtMaster,searchbystatus); 
				}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0)
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					lstJobForTeachers = jobForTeacherDAO.getReasonAndTiming(districtMaster,searchbystatus);
				}
			
				System.out.println("=====================lstForTeachers.size() :: "+lstJobForTeachers.size());
			 
			 
			 List<TeacherDetail> teacherDetail=new ArrayList<TeacherDetail>();
			 List<JobOrder> jobOrder=new ArrayList<JobOrder>();
			 totalRecord=lstJobForTeachers.size();
			
			 for (JobForTeacher jobForTeacher : lstJobForTeachers) {
				teacherDetail.add(jobForTeacher.getTeacherId());
				jobOrder.add(jobForTeacher.getJobId());
			 }
			 
			//sorting to check
			 if(sortOrderType.equals("0"))
			  Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorTeacherId);
			 else 
		      Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorTeacherId);
			
			List<TeacherStatusHistoryForJob> teacherStatusHistoryForJob= new ArrayList<TeacherStatusHistoryForJob>();
			teacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findReasonAndTiming(teacherDetail,jobOrder,reason,timing,searchbystatus);
			
			Map<String,String> teacherStatusMap=new HashMap<String,String>();
			
			if (teacherStatusHistoryForJob.size()>0) {
			for (TeacherStatusHistoryForJob teacherStatusHistoryForJob2 : teacherStatusHistoryForJob) {
				String string=teacherStatusHistoryForJob2.getTimingforDclWrdw().getTiming().toString().concat("~").concat(teacherStatusHistoryForJob2.getReasonforDclWrdw().getReason().toString());
				teacherStatusMap.put(teacherStatusHistoryForJob2.getTeacherDetail().getTeacherId().toString().concat("~").concat(teacherStatusHistoryForJob2.getJobOrder().getJobId().toString()),string );
			}
			}
			List<JobForTeacher> jobForTeachers=new ArrayList<JobForTeacher>();
			 for (JobForTeacher jobForTeacher : lstJobForTeachers) {
				 for (Map.Entry<String, String> entry : teacherStatusMap.entrySet()) {
					 if(entry.getKey().equals(jobForTeacher.getTeacherId().getTeacherId().toString().concat("~").concat(jobForTeacher.getJobId().getJobId().toString()))){
					    	String string[]=entry.getValue().split("~");
					    	jobForTeacher.setDistName(string[0]);
					    	jobForTeacher.setStafferName(string[1]);
					    	jobForTeachers.add(jobForTeacher);
					    }
					}
			 }
			 
			 /** set default sorting fieldName **/
				String sortOrderFieldName="lastName";
				String sortOrderNoField="lastName";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("lastName"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("jobId"))
					sortingcheck=2;
				else  if(sortOrder.equalsIgnoreCase("requisitionNumber")){
					sortingcheck=3;
				}else
					sortingcheck=4;
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			 
			// SORT by JobId
			 if(sortOrder.equals("jobId"))
			 {
				 for(JobForTeacher jft : jobForTeachers){
					 jft.setJobId(jft.getJobId());
				 }
				 if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherComparatorId);
				 else 
				      Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherComparatorIdDesc);
			 }

			// SORT by Status
			 if(sortOrder.equals("status"))
			 {
				 if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherStatusComparatorId);
				 else 
				      Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherStatusComparatorIdDesc);
			 }
		 
			// SORT by Timing
			 if(sortOrder.equals("timing"))
			 {
				 if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherTimingComparatorId);
				 else 
				      Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherTimingComparatorIdDesc);
			 }
			 
			// SORT by Reason
			 if(sortOrder.equals("reason"))
			 {
				 if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherReasonComparatorId);
				 else 
				      Collections.sort(jobForTeachers, JobForTeacher.jobForTeacherReasonComparatorIdDesc);
			 }
	        tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>"+Utility.getLocaleValuePropByKey("lblReasonTiming", locale)+"</div><br/>");
			
			tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");
			
			
			tmRecords.append("<table  id='tblGridHired1' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblApplicantName", locale)+"</th>");
    
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>job Id</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Date of Application</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Status</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Date of Decline or Withdraw</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Timing Selection</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Reason Selection</th>");
			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(jobForTeachers.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			}
		
			if(jobForTeachers.size()>0){
			 for(JobForTeacher jft:jobForTeachers) 
			 {
				   tmRecords.append("<tr>");	
					
					tmRecords.append("<td style='font-size:11px;'>"+jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")"+"</td>");
					tmRecords.append("<td style='font-size:11px;'>"+jft.getJobId().getJobId()+"</td>");
					
					//Norm Score 
					if(jft.getCreatedDateTime()!=null){
					 tmRecords.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getCreatedDateTime())+"</td>");
					}else{
						tmRecords.append("<td style='font-size:11px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					}
					
					//Hired By Date 
					
					if(jft.getStatus()!=null)
					{
					  tmRecords.append("<td style='font-size:11px;'>"+jft.getStatus().getStatus()+"</td>");
					 }else{
						tmRecords.append("<td style='font-size:11px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					  }
					
					//for position Number
					
					 if(jft.getStatus()!=null)
					 {
						 if(jft.getStatus().getStatusId().equals(19)&&jft.getUpdatedDate()!=null)
					  tmRecords.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getUpdatedDate())+"</td>");
						 else if(jft.getStatus().getStatusId().equals(7)&&jft.getWithdrwanDateTime()!=null)
							 tmRecords.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getWithdrwanDateTime())+"</td>");
						 else
							 tmRecords.append("<td style='font-size:11px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					 }else{
						tmRecords.append("<td style='font-size:11px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					  }
					
					//for School
						   tmRecords.append("<td style='font-size:11px;'>"+jft.getDistName()+"</td>");
						   tmRecords.append("<td style='font-size:11px;'>"+jft.getStafferName()+"</td>");
			   }
			}
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
  }
	
	public String displayReasonAndTiming(Integer districtID)
	{
		StringBuffer sb =	new StringBuffer();
		DistrictMaster districtMaster=new DistrictMaster();
		if(districtID!=null)
		districtMaster.setDistrictId(districtID);
		
		try{	
	List<DistrictSpecificTiming> districtSpecificTiming=null;
	List<DistrictSpecificReason> districtSpecificReason=null;
	if(districtMaster!=null){
	districtSpecificTiming=districtSpecificTimingDAO.getAllTimingForDistrict(districtMaster);
	districtSpecificReason=districtSpecificReasonDAO.getAllReasonForDistrict(districtMaster);
	}
	if (districtSpecificTiming.size()==0) {
		districtSpecificTiming=districtSpecificTimingDAO.getAllTimingDefault();
	}
	if (districtSpecificReason.size()==0) {
		districtSpecificReason=districtSpecificReasonDAO.getAllReasonDefault();
	}
	sb.append("<div class='col-sm-3'>");
	sb.append("<label>Timing:</label>");
	sb.append("<select id=\"timingforDclWrdwsearch\" class='form-control'>");
	sb.append("<option value='0'>Select time</option>");
	if(districtSpecificTiming!=null){
	for (DistrictSpecificTiming districtSpecificTiming2 : districtSpecificTiming) {
		sb.append("<option value='"+districtSpecificTiming2.getTimingId()+"'>"+districtSpecificTiming2.getTiming()+"</option>");
	}}
	sb.append("</select>");
	sb.append("</div>");
	sb.append("<div class='col-sm-3'>");
	sb.append("<label>Reason:</label>");
	sb.append("<select id=\"reasonforDclWrdwrearch\" class='form-control'>");
	sb.append("<option value='0'>Select time</option>");
	if(districtSpecificReason!=null){
	for (DistrictSpecificReason districtSpecificReason2 : districtSpecificReason) {
		sb.append("<option value='"+districtSpecificReason2.getReasonId()+"'>"+districtSpecificReason2.getReason()+"</option>");
	}
	}
	sb.append("</select>");
	sb.append("</div>");
	
		}catch(Exception e){
			e.printStackTrace();
		}
	return sb.toString();
	}
}


