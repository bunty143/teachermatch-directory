package tm.services;

import static tm.services.district.GlobalServices.DATE_TIME_FORMAT;
import static tm.services.district.GlobalServices.IS_PRINT;
import static tm.services.district.GlobalServices.println;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictPanelMaster;
import tm.bean.DistrictPortfolioConfig;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.EligibilityStatusMaster;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.PanelMembers;
import tm.bean.SchoolInJobOrder;
import tm.bean.SchoolWiseCandidateStatus;
import tm.bean.StafferMaster;
import tm.bean.StatusWiseAutoEmailSend;
import tm.bean.StrengthOpportunitySummary;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherAssessmentOption;
import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherElectronicReferencesHistory;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.cgreport.JobCategoryWiseStatusPrivilege;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.cgreport.TeacherStatusNotesHistory;
import tm.bean.cgreport.TeacherStatusScores;
import tm.bean.cgreport.UserMailSend;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.WorkFlowOptions;
import tm.bean.hqbranchesmaster.WorkFlowOptionsForStatus;
import tm.bean.i4.I4InterviewInvites;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificRefChkQuestions;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.master.RaceMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusSpecificQuestions;
import tm.bean.master.StatusSpecificScore;
import tm.bean.teacher.OnlineActivityQuestionSet;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.controller.teacher.BasicController;
import tm.controller.teacher.UserDashboardController;
import tm.dao.DistrictPanelMasterDAO;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.EligibilityStatusMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.PanelMembersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.SchoolWiseCandidateStatusDAO;
import tm.dao.StafferMasterDAO;
import tm.dao.StatusWiseAutoEmailSendDAO;
import tm.dao.StrengthOpportunitySummaryDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAnswerDetailDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherElectronicReferencesHistoryDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.cgreport.JobCategoryWiseStatusPrivilegeDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.cgreport.TeacherStatusNotesHistoryDAO;
import tm.dao.cgreport.TeacherStatusScoresDAO;
import tm.dao.cgreport.TeacherStatusScoresHistoryDAO;
import tm.dao.hqbranchesmaster.WorkFlowOptionsDAO;
import tm.dao.hqbranchesmaster.WorkFlowOptionsForStatusDAO;
import tm.dao.i4.I4InterviewInvitesDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificRefChkQuestionsDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.dao.master.PanelAttendeesDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.StatusSpecificQuestionsDAO;
import tm.dao.master.StatusSpecificScoreDAO;
import tm.dao.master.UserEmailNotificationsDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.teacher.OnlineActivityQuestionSetDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.district.ManageStatusAjax;
import tm.services.report.CandidateGridService;
import tm.services.report.CandidateGridSubAjax;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

public class PanelAjax {
	
	 String locale = Utility.getValueOfPropByKey("locale");
	 //String locale = Utility.getValueOfPropByKey("locale");
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lblPanelName=Utility.getLocaleValuePropByKey("lblPanelName", locale);
	 String lblPanelMembers=Utility.getLocaleValuePropByKey("lblPanelMembers", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String lblNoPanelfound1=Utility.getLocaleValuePropByKey("lblNoPanelfound1", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String lnkDlt=Utility.getLocaleValuePropByKey("lnkDlt", locale);
	 String btnAddMembers=Utility.getLocaleValuePropByKey("btnAddMembers", locale);
	 String msgNoMemberfound1=Utility.getLocaleValuePropByKey("msgNoMemberfound1", locale);
	 String lnkRmovS=Utility.getLocaleValuePropByKey("lnkRmovS", locale);
	 String optionSelectMember=Utility.getLocaleValuePropByKey("optionSelectMember", locale);
	 
	 
	
	@Autowired
	private DistrictPanelMasterDAO districtPanelMasterDAO;
	
	@Autowired
	private PanelMembersDAO panelMembersDAO;

	////////////////////////////////////////
	@Autowired
	private ManageStatusAjax manageStatusAjax;
	    
	    

		@Autowired
		private WorkFlowOptionsForStatusDAO flowOptionsForStatusDAO;
		
		@Autowired
		private WorkFlowOptionsDAO flowOptionsDAO;
		
		
		
		@Autowired
		private CandidateGridSubAjax candidateGridSubAjax; 

		@Autowired
		private CommonService commonService;


		@Autowired
		private OnlineActivityQuestionSetDAO onlineActivityQuestionSetDAO;

		

		
		
		

	

		@Autowired
		private I4InterviewInvitesDAO I4InterviewInvitesDAO;



		@Autowired
		private SchoolWiseCandidateStatusDAO schoolWiseCandidateStatusDAO;

		@Autowired
		private StafferMasterDAO stafferMasterDAO;

		@Autowired
		private AssessmentDetailDAO assessmentDetailDAO;


		@Autowired
		private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;

		@Autowired
		private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;


		@Autowired
		private UserEmailNotificationsDAO userEmailNotificationsDAO;

		@Autowired
		private EligibilityStatusMasterDAO eligibilityStatusMasterDAO;

		@Autowired
		private EmailerService emailerService;
		public void setEmailerService(EmailerService emailerService) 
		{
			this.emailerService = emailerService;
		}

		@Autowired
		private TeacherStatusScoresDAO teacherStatusScoresDAO;

		@Autowired
		private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;

		@Autowired
		private TeacherStatusNotesDAO teacherStatusNotesDAO;

		@Autowired
		private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;

		@Autowired
		private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;

		@Autowired
		private StatusMasterDAO statusMasterDAO;

		@Autowired
		private SchoolInJobOrderDAO schoolInJobOrderDAO;

		@Autowired
		private RoleAccessPermissionDAO roleAccessPermissionDAO;

		@Autowired
		private JobOrderDAO jobOrderDAO;

		@Autowired
		private JobForTeacherDAO jobForTeacherDAO;

		@Autowired
		private TeacherDetailDAO teacherDetailDAO;

		@Autowired
		private SecondaryStatusDAO secondaryStatusDAO;

		@Autowired
		private UserMasterDAO userMasterDAO;



		@Autowired
		private DistrictMasterDAO districtMasterDAO;
		@Autowired
		private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
		
		
		@Autowired
		private StatusSpecificQuestionsDAO statusSpecificQuestionsDAO;
		public void setStatusSpecificQuestionsDAO(
				StatusSpecificQuestionsDAO statusSpecificQuestionsDAO) {
			this.statusSpecificQuestionsDAO = statusSpecificQuestionsDAO;
		}


		@Autowired
		private StatusSpecificScoreDAO statusSpecificScoreDAO;
		public void setStatusSpecificScoreDAO(
				StatusSpecificScoreDAO statusSpecificScoreDAO) {
			this.statusSpecificScoreDAO = statusSpecificScoreDAO;
		}

		@Autowired
		private JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO;
		public void setJobCategoryWiseStatusPrivilegeDAO(
				JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO) {
			this.jobCategoryWiseStatusPrivilegeDAO = jobCategoryWiseStatusPrivilegeDAO;
		}

		@Autowired
		private ContactTypeMasterDAO contactTypeMasterDAO;
		public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
			this.contactTypeMasterDAO = contactTypeMasterDAO;
		}

		@Autowired
		private DistrictKeyContactDAO districtKeyContactDAO;
		public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) {
			this.districtKeyContactDAO = districtKeyContactDAO;
		}

		
		@Autowired
		private TeacherAnswerDetailDAO teacherAnswerDetailDAO;

		@Autowired
		private SchoolMasterDAO schoolMasterDAO;

		@Autowired
		private PanelScheduleDAO panelScheduleDAO;
		@Autowired
		private PanelAttendeesDAO panelAttendeesDAO;

		@Autowired
		private JobWisePanelStatusDAO jobWisePanelStatusDAO;

		@Autowired
		private AssessmentJobRelationDAO assessmentJobRelationDAO;

		@Autowired
		private TeacherAcademicsDAO teacherAcademicsDAO;

		@Autowired
		private StatusWiseAutoEmailSendDAO statusWiseAutoEmailSendDAO;

		@Autowired
		private RaceMasterDAO raceMasterDAO;

		@Autowired
		private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;

		@Autowired
		private TeacherElectronicReferencesHistoryDAO teacherElectronicReferencesHistoryDAO;

		@Autowired
		private DistrictSpecificRefChkQuestionsDAO districtSpecificRefChkQuestionsDAO;

		@Autowired
		private StrengthOpportunitySummaryDAO strengthOpportunitySummaryDAO;
		
		@Autowired
		TeacherStatusScoresHistoryDAO teacherStatusScoresHistoryDAO;
		
		@Autowired
		TeacherStatusNotesHistoryDAO teacherStatusNotesHistoryDAO;
		
		@Autowired
		private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
		
	/* @Author: Gagan */
	public String displayPanel(int districtId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer pnRecords =	new StringBuffer();
		String panelMembers="";
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,17,"domain.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<DistrictPanelMaster> lstdistrictPanelMaster	  	=	null;
			DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
 			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"panelName";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			Criterion criterion1	=	Restrictions.eq("districtMaster", districtMaster);
			lstdistrictPanelMaster	= districtPanelMasterDAO.findByCriteria(sortOrderStrVal,criterion1);
			
			totalRecord =lstdistrictPanelMaster.size();

			if(totalRecord<end)
				end=totalRecord;
			List<DistrictPanelMaster> lstsortedDistrictPanelMaster		=	lstdistrictPanelMaster.subList(start,end);
			
			
			
			//totaRecord = panelmasterdao.getRowCountWithSort(sortOrderStrVal);
			//districtPanelMaster = panelmasterdao.findWithLimit(sortOrderStrVal,0,100);
			pnRecords.append("<table  id='panelTable' width='100%' border='0' >");
			pnRecords.append("<thead class='bg'>");
			pnRecords.append("<tr>");
			//pnRecords.append("<th width='55%'>Panel Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblPanelName,sortOrderFieldName,"panelName",sortOrderTypeVal,pgNo);
			pnRecords.append("<th width='55%' valign='top'>"+responseText+"</th>");
			
			pnRecords.append("<th>"+lblPanelMembers+"</th>");
			
			pnRecords.append("<th width='15%'>"+lblAct+"</th>");
			pnRecords.append("</tr>");
			pnRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(lstsortedDistrictPanelMaster.size()==0)
				pnRecords.append("<tr><td colspan='6'>"+lblNoPanelfound1+"</td></tr>" );
			System.out.println("No of Records "+lstsortedDistrictPanelMaster.size());

			for (DistrictPanelMaster districtPanelMasterDetail : lstsortedDistrictPanelMaster) 
			{
				panelMembers= getMembersByPanel(districtPanelMasterDetail);
				
				
				pnRecords.append("<tr>" );
				pnRecords.append("<td>"+districtPanelMasterDetail.getPanelName()+"</td>");
				pnRecords.append("<td>");
					pnRecords.append(panelMembers);
				pnRecords.append("</td>");
				
				pnRecords.append("<td>");
				
				pnRecords.append("<a href=\"javascript:void(0);\"  onclick='return editPanel("+districtPanelMasterDetail.getPanelId()+")'>"+lblEdit+"</a> | ");
				pnRecords.append("<a href='javascript:void(0);'  onclick=\"return deletePanel("+districtPanelMasterDetail.getPanelId()+",'A')\">"+lnkDlt+"</a> | ");
				pnRecords.append("&nbsp;&nbsp; <a href='panelmembers.do?&panelId="+districtPanelMasterDetail.getPanelId()+"'>"+btnAddMembers+"</a>");
				pnRecords.append("</td>");
				pnRecords.append("</tr>");
			}
			pnRecords.append("</table>");
			pnRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			//System.out.println(pnRecords.toString());
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return pnRecords.toString();
	}
	/* ===============      savePanel        =========================*/
	public int savePanel(Integer panelId,Integer districtId,String panelName)
	{
		System.out.println(" panelId  "+panelId+" panelName:  "+panelName);
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		DistrictPanelMaster districtPanelMaster=new DistrictPanelMaster();
		DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
		try
		{
			districtPanelMaster.setDistrictMaster(districtMaster);
			districtPanelMaster.setPanelName(panelName);
			districtPanelMaster.setStatus("A");
			districtPanelMaster.setCreatedDateTime(new Date());

			if(panelId	!=	null)
			{	
				districtPanelMaster.setPanelId(panelId);
			}
			/*======= Check For Unic Panel ==========*/
			List<DistrictPanelMaster> lstdupdistrictPanelMaster=districtPanelMasterDAO.checkDuplicatePanelName(panelName,panelId,districtMaster);
			int size = lstdupdistrictPanelMaster.size();
			if(size>0)
				return 3;
			
			districtPanelMasterDAO.makePersistent(districtPanelMaster);
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}

		return 1;
	}
	/* ===============      Activate Deactivate Panel        =========================*/
	@Transactional(readOnly=false)
	public boolean deletePanel(int panelId)
	{
		System.out.println(" Panel is going to be deleted ");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		try{
			/* ==== First delete its Panel Member ===============*/
			DistrictPanelMaster districtPanelMaster	=	districtPanelMasterDAO.findById(panelId, false, false);

			
			Criterion criterion1 =Restrictions.eq("districtPanelMaster", districtPanelMaster);
			List<PanelMembers> lstPanelMembers = panelMembersDAO.findByCriteria(criterion1);
			
			for(PanelMembers lstpm : lstPanelMembers)
			{
				panelMembersDAO.makeTransient(lstpm);
			}
			
			districtPanelMasterDAO.makeTransient(districtPanelMaster);
			
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/* ===============    Edit Panel Functionality        =========================*/
	@Transactional(readOnly=false)
	public DistrictPanelMaster editPanel(int panelId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		DistrictPanelMaster districtPanelMaster =null;
		try
		{
			districtPanelMaster	=	districtPanelMasterDAO.findById(panelId, false, false);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		return districtPanelMaster;
	}
	
	public String getMembersByPanel(DistrictPanelMaster districtPanelMaster)
	{
		//System.out.println("getMembersByPanel Method : DistrictPanelMaster Panel Id : "+districtPanelMaster.getPanelId());
		StringBuffer sb = new StringBuffer();
		try
		{
			List<PanelMembers> lstPanelMembers = new ArrayList<PanelMembers>();
			Criterion criterion1 = Restrictions.eq("districtPanelMaster",districtPanelMaster);
			lstPanelMembers = panelMembersDAO.findByCriteria(criterion1);
			
			//System.out.println(" lstPanelMembers size :  "+lstPanelMembers.size());
			
			for(PanelMembers pnm :lstPanelMembers)
			{
				sb.append(pnm.getUserMaster().getFirstName()+" "+pnm.getUserMaster().getLastName());
				sb.append(" | ");
			}
			
			sb.deleteCharAt(sb.length()-2); 
			 
		}
		catch (Exception e) {
			//System.out.println(" "+e);
			//e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	/* ============================= Gagan : PanelMembers Ajax Method [Start] =====================================*/
	
	/* @Author: Gagan */
	public String displayPanelMember(int panelId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer pnmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,17,"domain.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			//List<DistrictPanelMaster> lstdistrictPanelMaster	  	=	null;
			//DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			DistrictPanelMaster districtPanelMaster	=	 districtPanelMasterDAO.findById(panelId, false, false);
			
			Criterion criterion1 =Restrictions.eq("districtPanelMaster", districtPanelMaster);
			//PanelMembers panelMembers	= panelMembersDAO.findById(id, false, false);
			
			List<PanelMembers> lstPanelMembers = new ArrayList<PanelMembers>();
			List<PanelMembers> sortedlstPanelMembers		=	new ArrayList<PanelMembers>();
			
			
 			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"panelMemberId";
			String sortOrderNoField		=	"userName";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("userName")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("userName"))
				 {
					 sortOrderNoField="userName";
				 }
				 else
				{
					sortOrderNoField="userName";
				}
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			lstPanelMembers =panelMembersDAO.findByCriteria(sortOrderStrVal,criterion1);
			
		/*	Criterion criterion1	=	Restrictions.eq("districtMaster", districtMaster);
			lstdistrictPanelMaster	= districtPanelMasterDAO.findByCriteria(sortOrderStrVal,criterion1);*/
			
			SortedMap<String,PanelMembers>	sortedMap = new TreeMap<String,PanelMembers>();
			if(sortOrderNoField.equals("userName"))
			{
				sortOrderFieldName	=	"userName";
			}
			int mapFlag=2;
			for (PanelMembers panelMembers : lstPanelMembers){
				String orderFieldName=panelMembers.getUserMaster().getFirstName().toLowerCase()+" "+panelMembers.getUserMaster().getLastName();
				if(sortOrderFieldName.equals("userName")){
					orderFieldName=panelMembers.getUserMaster().getFirstName().toLowerCase()+"||"+panelMembers.getPanelMemberId();
					sortedMap.put(orderFieldName+"||",panelMembers);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstPanelMembers.add((PanelMembers) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstPanelMembers.add((PanelMembers) sortedMap.get(key));
				}
			}else{
				sortedlstPanelMembers=lstPanelMembers;
			}
			
			totalRecord =sortedlstPanelMembers.size();

			if(totalRecord<end)
				end=totalRecord;
			List<PanelMembers> lstsortedPanelMembers		=	sortedlstPanelMembers.subList(start,end);
			
			
			
			//totaRecord = panelMembermasterdao.getRowCountWithSort(sortOrderStrVal);
			//districtPanelMaster = panelMembermasterdao.findWithLimit(sortOrderStrVal,0,100);
			pnmRecords.append("<table  id='panelMemberTable' width='100%' border='0' >");
			pnmRecords.append("<thead class='bg'>");
			pnmRecords.append("<tr>");
			//pnmRecords.append("<th width='55%'>Panel Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblPanelMembers,sortOrderFieldName,"userName",sortOrderTypeVal,pgNo);
			pnmRecords.append("<th width='55%' valign='top'>"+responseText+"</th>");
			
			//pnmRecords.append("<th>Panel Members</th>");
			
			pnmRecords.append("<th width='15%'>"+lblAct+"</th>");
			pnmRecords.append("</tr>");
			pnmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(lstsortedPanelMembers.size()==0)
				pnmRecords.append("<tr><td colspan='6'>"+msgNoMemberfound1+"</td></tr>" );
			System.out.println("No of Records "+lstsortedPanelMembers.size());

			for (PanelMembers pm : lstsortedPanelMembers) 
			{
				pnmRecords.append("<tr>" );
				pnmRecords.append("<td>"+pm.getUserMaster().getFirstName()+" "+pm.getUserMaster().getLastName()+"</td>");
				//pnmRecords.append("<td>"+pm.getDistrictPanelMaster().getPanelName()+"</td>");
				/*pnmRecords.append("<td>");
					pnmRecords.append("");
				pnmRecords.append("</td>");
				*/
				pnmRecords.append("<td>");
				
				//pnmRecords.append("<a href=\"javascript:void(0);\"  onclick='return editPanelMember("+pm.getPanelMemberId()+")'>Edit</a> | ");
				pnmRecords.append("<a href='javascript:void(0);'  onclick=\"return deletePanelMember("+pm.getPanelMemberId()+",'A')\">"+lnkRmovS+"</a>");
				//pnmRecords.append("&nbsp;&nbsp; <a href='panelMembermembers.do?&panelMemberId="+districtPanelMasterDetail.getPanelId()+"'>Add Members</a>");
				pnmRecords.append("</td>");
				pnmRecords.append("</tr>");
			}
			pnmRecords.append("</table>");
			pnmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			//System.out.println(pnmRecords.toString());
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return pnmRecords.toString();
	}
	/* ===============      savePanel        =========================*/
	public int savePanelMember(Integer panelMemberId,int panelId,int districtId,long schoolId,int memberId)
	{
		System.out.println("savePanelMember : panelMemberId  "+panelMemberId+" panelId:  "+panelId+" districtId : "+districtId+" schoolId : "+schoolId+" memberId : "+memberId);
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		PanelMembers panelMembers   = new PanelMembers();
		SchoolMaster schoolMaster	= new SchoolMaster();
		DistrictPanelMaster districtPanelMaster =districtPanelMasterDAO.findById(panelId, false, false);
		DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
		if(schoolId>0)
			schoolMaster	=	schoolMasterDAO.findById(schoolId, false, false);
		UserMaster userMaster	=	userMasterDAO.findById(memberId, false, false);
		try
		{
			panelMembers.setDistrictMaster(districtMaster);
			panelMembers.setDistrictPanelMaster(districtPanelMaster);
			panelMembers.setDistrictMaster(districtMaster);
			if(schoolId>0)
				panelMembers.setSchoolMaster(schoolMaster);
			else
				panelMembers.setSchoolMaster(null);
			
			panelMembers.setUserMaster(userMaster);
			

			if(panelMemberId	!=	null)
			{	
				districtPanelMaster.setPanelId(panelMemberId);
			}
			/*======= Check For Unic Panel ==========*/
			List<PanelMembers> lstPanelMembers	=	panelMembersDAO.checkDuplicatePanelMember(userMaster,districtPanelMaster);
			int size = lstPanelMembers.size();
			
			System.out.println(" lstPanelMembers size : "+lstPanelMembers.size());
			if(size>0)
				return 3;
			
			panelMembersDAO.makePersistent(panelMembers);
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}

		return 1;
	}
	/* ===============      DeletePanelMember      =========================*/
	@Transactional(readOnly=false)
	public boolean deletePanelMember(int panelMemberId)
	{
		//System.out.println(" PanelMember is going to be deleted ");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		try{
			PanelMembers panelMembers	=	panelMembersDAO.findById(panelMemberId, false, false);
			panelMembersDAO.makeTransient(panelMembers);
			
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/* ===============    Edit Panel Functionality        =========================*/
	@Transactional(readOnly=false)
	public DistrictPanelMaster editPanelMember(int panelMemberId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		DistrictPanelMaster districtPanelMaster =null;
		try
		{
			districtPanelMaster	=	districtPanelMasterDAO.findById(panelMemberId, false, false);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		return districtPanelMaster;
	}
	
	
	/* ===============      DeletePanelMember      =========================*/
	@Transactional(readOnly=false)
	public String getUserBySchool(int districtId,long schoolId)
	{
		//System.out.println(" PanelMember is going to be deleted ");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer sb = new StringBuffer();
		DistrictMaster districtMaster = new DistrictMaster();
		SchoolMaster schoolMaster =new SchoolMaster();
		List<UserMaster> lstschollUser = new ArrayList<UserMaster>();
		
		try{
			if(schoolId>0)
			{
				System.out.println(" [ If Block in case of School ] : "+schoolId);
				schoolMaster = schoolMasterDAO.findById(schoolId, false, false);
				lstschollUser =userMasterDAO.getUserByOnlySchool(schoolMaster);
			}
			else
			{
				System.out.println(" [ ELSe Block in case of DistrictId ] : "+districtId);
				districtMaster=districtMasterDAO.findById(districtId, false, false);
				lstschollUser =userMasterDAO.getUserByDistrict(districtMaster);
			}
			
			System.out.println(" lstschollUser Size "+lstschollUser.size());
			
			sb.append("<select id='memberId' name='memberId' selected=\"selected\" class='span7'>");
			sb.append("<option value='' id='memberOption' >"+optionSelectMember+"</option>");
			for(UserMaster lum : lstschollUser)
			{
				//System.out.println(" "+lum.getFirstName()+" "+lum.getLastName());
				sb.append("<option id="+lum.getUserId()+" value="+lum.getUserId()+">"+lum.getFirstName()+" "+lum.getLastName()+"</option>");
			}
			sb.append("</select>");
			
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	/* ============================= Gagan : PanelMembers Ajax Method [End] =====================================*/
	public String  saveStatusNoteForPanel(String jobForTeacherId,String fitScore,String teacherStatusNoteId,String teacherId,String jobId,String statusId,String secondaryStatusId,String setHiredDate,String statusNotes,String statusNoteFileName,String emailSentToD,String emailSentToS,String scoreProvided,String finalizeStatus,boolean bEmailSendFor_DA_SA,boolean bEmailSendFor_CA,Integer[] answerId_array,Integer[] score_array,int isEmailTemplateChanged, String hiddenStatusShortName,String msgSubject,String mailBody,Integer txtschoolCount,Long schoolIdInputFromUser,int isEmailTemplateChangedTeacher,String msgSubjectTeacher,String mailBodyTeacher,boolean chkOverridetSatus,String requisitionNumberId,String qqNoteIdsvalue_str,String questionAssessmentIds,int jobCategoryFlagValue,String rdReqSel,String txtNewReqNo, String[] strength,String[] growthOpp,String recomreason,String remquestion,Integer minOneScoreSliderSetOrNotFlag,Integer superAdminClickOnEditNote,String flowOptionIds, String startDate){
		System.out.println(":::::::::::::::::::::::::::::::::saveStatusNoteForPanel::::::::::::::::::::::::::::::");
		String returnStr="";
		try{
			returnStr=saveStatusNoteMainForPanel(jobForTeacherId,fitScore,teacherStatusNoteId,teacherId,jobId,statusId,secondaryStatusId,setHiredDate,statusNotes,statusNoteFileName,emailSentToD,emailSentToS,scoreProvided,finalizeStatus,bEmailSendFor_DA_SA,bEmailSendFor_CA,answerId_array,score_array,isEmailTemplateChanged,hiddenStatusShortName,msgSubject,mailBody,txtschoolCount,schoolIdInputFromUser,isEmailTemplateChangedTeacher,msgSubjectTeacher,mailBodyTeacher,chkOverridetSatus,requisitionNumberId,qqNoteIdsvalue_str,questionAssessmentIds,jobCategoryFlagValue,rdReqSel,txtNewReqNo,strength,growthOpp,recomreason,remquestion,minOneScoreSliderSetOrNotFlag,superAdminClickOnEditNote,flowOptionIds,startDate);
		}catch(Exception e){
			e.printStackTrace();
		}
		return returnStr;
	}
	
	@Transactional(readOnly=false)
	public String  saveStatusNoteMainForPanel(String jobForTeacherId,String fitScore,String teacherStatusNoteId,String teacherId,String jobId,String statusId,String secondaryStatusId,String setHiredDate,String statusNotes,String statusNoteFileName,String emailSentToD,String emailSentToS,String scoreProvided,String finalizeStatus,boolean bEmailSendFor_DA_SA,boolean bEmailSendFor_CA,Integer[] answerId_array,Integer[] score_array,int isEmailTemplateChanged, String hiddenStatusShortName,String msgSubject,String mailBody,Integer txtschoolCount,Long schoolIdInputFromUser,int isEmailTemplateChangedTeacher,String msgSubjectTeacher,String mailBodyTeacher,boolean chkOverridetSatus,String requisitionNumberId,String qqNoteIdsvalue_str,String questionAssessmentIds,int jobCategoryFlagValue,String rdReqSel,String txtNewReqNo, String[] strength,String[] growthOpp,String recomreason,String remquestion,Integer minOneScoreSliderSetOrNotFlag,Integer superAdminClickOnEditNote,String flowOptionIds, String startDate)
	{
		System.out.println(":::::::::::::::::::::::::::::::::::::::::::saveStatusNoteMainForPanel:::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		//System.out.println(fitScore+" :::::"+minOneScoreSliderSetOrNotFlag+"::>>>>>>>>>>>>saveStatusNote::::::"+qqNoteIdsvalue_str+" ....... questionAssessmentIds :: "+questionAssessmentIds+" secondaryStatusId "+secondaryStatusId+"       superAdminClickOnEditNote:::::::"+superAdminClickOnEditNote);
		StringBuffer sb = new StringBuffer("");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		boolean canHire = true;
		boolean canUnhire=true;
		boolean mailSend=false;
		boolean overrideAll=false;
		String errorFlag="1";
		int multiJobReturn=0;
		String secondaryStatusNames="";
		String statusUpdateHDR=null;	
		TeacherDetail teacherDetail =null;
		DistrictMaster districtMaster=null;
		HeadQuarterMaster headQuarterMaster=null;
		JobCategoryMaster jobCategoryMaster=null;
		SecondaryStatus secondaryStatus=null;
		boolean hourlyCategory=false;
		SecondaryStatus secondaryStatusVVI=null;
		StatusMaster statusMasterVVI=null;
		SecondaryStatus secondaryStatusCC=null;
		StatusMaster statusMasterCC=null;
		String statusMasterSecondaryName=null;
		String statusName = "";
		boolean isSuperAdminUser=false;// added by 04-04-2015
		List<TeacherDetail> lstTDetails=new ArrayList<TeacherDetail>();
		BranchMaster branchMaster=null;
		boolean statusWaived=false;
		try {
			HttpSession session = request.getSession();
			UserMaster userMaster =null;
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}else{
				userMaster = (UserMaster) session.getAttribute("userMaster");
			}
			//added by 03-04-2015
			List<DistrictKeyContact> notesKeys=districtKeyContactDAO.findByContactType(userMaster, "Super Administrator"); 
			if(notesKeys.size()>0)
				isSuperAdminUser=true;
			//ended by 03-04-2015
			int roleId=0;
			int entityType=0;
			SchoolMaster userSchoolMaster=null; 
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
				entityType=userMaster.getEntityType();
			}
			if(entityType==3){
				if(userMaster.getSchoolId()!=null){
					userSchoolMaster=userMaster.getSchoolId();
				}
			}
			List<StatusMaster> lstStatusMaster=null;
			String roleAccess=null;
			try{
				lstStatusMaster=WorkThreadServlet.statusMasters;
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			List<Long> qAnssementQuesIds = new ArrayList<Long>();
			String qqNoteIdsvalue_str_array[]=null;
			String qAssessmentIds_array[]=null;
			try{
				if(questionAssessmentIds!=null && !questionAssessmentIds.equals("")){
					qqNoteIdsvalue_str_array= qqNoteIdsvalue_str.split("#");
					qAssessmentIds_array= questionAssessmentIds.split("#");
					for(int i=0;i<qAssessmentIds_array.length;i++)
					{
						qAnssementQuesIds.add(Long.parseLong(qAssessmentIds_array[i]));
					}
				}
			}catch(Exception e){
				//e.printStackTrace();
			}

			CandidateGridService cgService=new CandidateGridService();
			teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			JobOrder jobOrder =jobOrderDAO.findById(new Integer(jobId), false, false);


			//Start ... New Requistion

			if(finalizeStatus!=null && finalizeStatus.equals("1") && rdReqSel!=null && rdReqSel.equals("2") && txtNewReqNo!=null && !txtNewReqNo.equals(""))
			{
				DistrictMaster dmTemp=jobOrder.getDistrictMaster();
				SchoolMaster schoolMaster=null;
				DistrictRequisitionNumbers districtRequisitionNumber=districtRequisitionNumbersDAO.getDistrictRequisitionNumber(dmTemp, txtNewReqNo);
				if( districtRequisitionNumber==null || (districtRequisitionNumber!=null && !districtRequisitionNumber.getIsUsed()))
				{

					if(txtschoolCount>0)
						schoolMaster=schoolMasterDAO.findById(schoolIdInputFromUser, false, false);

					SchoolInJobOrder schoolInJobOrder=null;
					List<SchoolInJobOrder> lstSchoolInJobOrder= new ArrayList<SchoolInJobOrder>();
					lstSchoolInJobOrder=schoolInJobOrderDAO.getSIJO(jobOrder, schoolMaster);
					if(lstSchoolInJobOrder!=null && lstSchoolInJobOrder.size()==1)
						schoolInJobOrder=lstSchoolInJobOrder.get(0);

					if(txtschoolCount==0 && schoolInJobOrder!=null)
						schoolMaster=schoolInJobOrder.getSchoolId();


					if(districtRequisitionNumber==null)
					{
						districtRequisitionNumber=new DistrictRequisitionNumbers();
						districtRequisitionNumber.setCreatedDateTime(new Date());
						districtRequisitionNumber.setDistrictMaster(dmTemp);
						districtRequisitionNumber.setIsUsed(true);
						districtRequisitionNumber.setUserMaster(userMaster);

						if(jobOrder.getJobType()!=null && !jobOrder.getJobType().equals("") && jobOrder.getJobType().equals("P"))
							districtRequisitionNumber.setPosType("P");
						else
							districtRequisitionNumber.setPosType("F");

						districtRequisitionNumber.setRequisitionNumber(txtNewReqNo);
						districtRequisitionNumbersDAO.makePersistent(districtRequisitionNumber);
					}

					JobRequisitionNumbers jobRequisitionNumbers=new JobRequisitionNumbers();
					jobRequisitionNumbers.setJobOrder(jobOrder);
					jobRequisitionNumbers.setDistrictRequisitionNumbers(districtRequisitionNumber);
					jobRequisitionNumbers.setSchoolMaster(schoolMaster);
					jobRequisitionNumbers.setStatus(0);
					jobRequisitionNumbersDAO.makePersistent(jobRequisitionNumbers);

					if(schoolInJobOrder!=null)
					{
						int noOfSchoolExpHires=schoolInJobOrder.getNoOfSchoolExpHires();
						noOfSchoolExpHires=noOfSchoolExpHires+1;
						schoolInJobOrder.setNoOfSchoolExpHires(noOfSchoolExpHires);
						schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
					}
					else
					{
						int noOfExpHires=0;
						if(jobOrder.getNoOfExpHires()!=null )
							noOfExpHires=jobOrder.getNoOfExpHires();
						noOfExpHires=noOfExpHires+1;
						jobOrder.setNoOfExpHires(noOfExpHires);
						jobOrderDAO.makePersistent(jobOrder);
					}

					requisitionNumberId=jobRequisitionNumbers.getJobRequisitionId().toString();

				}
				else
				{
					errorFlag="99";
					return errorFlag+"##0##0##0";
				}
			}

			//End ... New Requistion
			jobCategoryMaster=jobOrder.getJobCategoryMaster();
			try{
				if(jobCategoryMaster!=null && jobCategoryMaster.getJobCategoryName().contains(Utility.getLocaleValuePropByKey("msgHourlyTeacher", locale))){
					hourlyCategory=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			//System.out.println("hourlyCategory:::::::::::::::::::::::::::::>"+hourlyCategory);
			JobForTeacher jobForTeacherObj =new JobForTeacher();
			if(jobForTeacherId!=null){
				JobForTeacher jobForTeacherForResend=jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
				if(finalizeStatus.equalsIgnoreCase("6")){
					jobForTeacherForResend.setOfferReady(false);
					jobForTeacherForResend.setOfferMadeDate(null);
					jobForTeacherDAO.updatePersistent(jobForTeacherForResend);
					finalizeStatus="1";				
				}
				jobForTeacherObj=jobForTeacherForResend;
			}
			if(finalizeStatus!=null && finalizeStatus.equalsIgnoreCase("7")){
				finalizeStatus="1";	
				statusWaived=true;
			}
			SchoolInJobOrder schoolInJobOrder = null;
			boolean outerJob=true;
			if(userMaster.getEntityType()==3){
				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, userMaster.getSchoolId());
				if(schoolInJobOrder!=null){
					outerJob=false;
				}
			}else{
				outerJob=false;
			}
			boolean isUpdateStatus=cgService.isUpdateStatus(jobForTeacherObj,userMaster,schoolInJobOrder);

			System.out.println("isUpdateStatus::::::::::::::::::::::::::::::::::::>>>"+isUpdateStatus);

			if(jobOrder.getCreatedForEntity()!=1){
				try{
					districtMaster=jobOrder.getDistrictMaster();
				}catch(Exception e){}
			}
			if(jobOrder.getCreatedForEntity()==5 || jobOrder.getCreatedForEntity()==6 || jobOrder.getHeadQuarterMaster()!=null){
				try{
					headQuarterMaster=jobOrder.getHeadQuarterMaster();
				}catch(Exception e){}
			}
			
			boolean isMiami = false;
			boolean isPhiladelphia = false;//added by 21-03-2015
			try{
				//added by 21-03-2015
				if(districtMaster!=null && districtMaster.getDistrictId().equals(00000000))
					isPhiladelphia=true;
				//end
				if(districtMaster!=null && districtMaster.getDistrictId().equals(1200390))
					isMiami=true;
			}catch(Exception e){}

			List<I4InterviewInvites> I4InterviewInvites=new ArrayList<I4InterviewInvites>();
			boolean vviPrivilege=false;
			try{
				if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview())
				{
					if(jobOrder.getStatusMaster()!=null && jobOrder.getSecondaryStatus()==null){
						statusMasterVVI=jobOrder.getStatusMaster();
						vviPrivilege=true;
					}else if(jobOrder.getStatusMaster()==null && jobOrder.getSecondaryStatus()!=null){
						secondaryStatusVVI=jobOrder.getSecondaryStatus();
						vviPrivilege=true;
					}
				}else if(jobOrder.getJobCategoryMaster().getOfferVirtualVideoInterview()!=null && jobOrder.getJobCategoryMaster().getOfferVirtualVideoInterview()){

					if(jobOrder.getJobCategoryMaster().getStatusIdForAutoVVILink()!=null && jobOrder.getJobCategoryMaster().getSecondaryStatusIdForAutoVVILink()==null){
						statusMasterVVI= WorkThreadServlet.statusIdMap.get(jobOrder.getJobCategoryMaster().getStatusIdForAutoVVILink());//jobOrder.getJobCategoryMaster().getStatusMaster();
						vviPrivilege=true;
					}else if(jobOrder.getJobCategoryMaster().getStatusIdForAutoVVILink()==null && jobOrder.getJobCategoryMaster().getSecondaryStatusIdForAutoVVILink()!=null){
						secondaryStatusVVI= secondaryStatusDAO.findById(jobOrder.getJobCategoryMaster().getSecondaryStatusIdForAutoVVILink(), false, false);
						vviPrivilege=true;
					}
				}else if(districtMaster!=null && districtMaster.getOfferVirtualVideoInterview()!=null && districtMaster.getOfferVirtualVideoInterview()){
					if(districtMaster.getStatusMasterForVVI()!=null && districtMaster.getSecondaryStatusForVVI()==null){
						statusMasterVVI=districtMaster.getStatusMasterForVVI();
						vviPrivilege=true;
					}else if(districtMaster.getStatusMasterForVVI()==null && districtMaster.getSecondaryStatusForVVI()!=null){
						secondaryStatusVVI=districtMaster.getSecondaryStatusForVVI();
						vviPrivilege=true;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(vviPrivilege){
				I4InterviewInvites=I4InterviewInvitesDAO.getListbyTeacherAndjobId(jobOrder,teacherDetail);
			}
			boolean ccPrivilege=false;
			try{
				if(districtMaster!=null && districtMaster.getStatusMasterForCC()!=null && districtMaster.getSecondaryStatusForCC()==null){
					statusMasterCC=districtMaster.getStatusMasterForCC();
					ccPrivilege=true;
				}else if(districtMaster!=null && districtMaster.getStatusMasterForCC()==null && districtMaster.getSecondaryStatusForCC()!=null){
					secondaryStatusCC=districtMaster.getSecondaryStatusForCC();
					ccPrivilege=true;

				}
			}catch(Exception e){
				e.printStackTrace();
			}

			SchoolMaster schoolMaster =null;
			if(jobOrder.getCreatedForEntity()==3){
				try{
					schoolMaster=jobOrder.getSchool().get(0);
				}catch(Exception e){}
			}
			
			System.out.println("statusId::::::------------------------:::::"+statusId+"     "+secondaryStatusId);
			StatusMaster statusMaster=null;
			SecondaryStatus secondaryStatusForPanel=null;
			String statusShortName="soth";
			if(statusId!=null && !statusId.equals("0")){
				statusMaster=WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
			}


			if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
				secondaryStatus=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
				secondaryStatusForPanel=secondaryStatus;
			}
			if(statusMaster!=null)
			{
				statusName = statusMaster.getStatus();	
			}
			if(secondaryStatus!=null)
			{
				statusName = secondaryStatus.getSecondaryStatusName();
			}
			
			try{
				String secondaryStatusName = secondaryStatus.getSecondaryStatusName();
				if(districtMaster!=null && districtMaster.getDistrictId() == 5510470 && secondaryStatusName.equalsIgnoreCase("conditional offer")){
					emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Conditional Offer",MailText.getFPRejectMail(request, teacherDetail));
				}
			}catch(Exception e){}

			SecondaryStatus secondaryStatusForJobs=secondaryStatus;
			StatusMaster statusMasterForJobs=statusMaster;


			boolean selectedstatus=false;
			boolean selectedSecondaryStatus=false;
			try{
				if(jobForTeacherObj.getStatus()!=null && statusMaster!=null){
					selectedstatus=cgService.selectedStatusCheck(statusMaster,jobForTeacherObj.getStatus());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			try{
				if(secondaryStatus!=null && jobForTeacherObj.getStatus()!=null &&  statusMaster!=null){// && (jobForTeacherObj.getStatus().getStatusShortName().equals("scomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("ecomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("vcomp"))
					if(jobForTeacherObj.getSecondaryStatus()!=null){
						selectedSecondaryStatus=cgService.selectedPriSecondaryStatusCheck(secondaryStatus,jobForTeacherObj.getSecondaryStatus());
						if(selectedSecondaryStatus==false){
							selectedstatus=false;
						}
					}
				}else if(jobForTeacherObj.getSecondaryStatus()!=null && secondaryStatus!=null && statusMaster==null){
					if(jobForTeacherObj.getStatus()!=null){
						if(cgService.priSecondaryStatusCheck(jobForTeacherObj.getStatus(),secondaryStatus)){
							selectedSecondaryStatus=cgService.selectedNotPriSecondaryStatusCheck(secondaryStatus,jobForTeacherObj.getSecondaryStatus());
						}
					}
				}else if(jobForTeacherObj.getSecondaryStatus()==null && secondaryStatus!=null && statusMaster==null){
					if(jobForTeacherObj.getStatus()!=null){
						if(cgService.priSecondaryStatusCheck(jobForTeacherObj.getStatus(),secondaryStatus)){
							selectedSecondaryStatus=true;
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("::::::selectedstatus::::::::;;>>>>>>>>>>>>>>>>>>>>>>>>>>>;>>"+selectedstatus);
			System.out.println(":::::::;;selectedSecondaryStatus::::::::;;;>>"+selectedSecondaryStatus);
 
			if(statusId!=null && !statusId.equals("0")){
				secondaryStatusId="0";
				secondaryStatus=null;
			}
			List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryActive(teacherDetail,jobOrder);
			TeacherStatusHistoryForJob statusHistory=teacherStatusHistoryForJobDAO.findByTeacherStatus(teacherDetail,jobOrder,statusMaster,secondaryStatus);
			boolean  finalizeStatusFlag=false;
			try{
				
				if(headQuarterMaster!=null && headQuarterMaster.getStatusNotes()!=null){
					if(headQuarterMaster.getStatusNotes()==false){
						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								finalizeStatusFlag=true;
							}
						}
					}
				}else if(districtMaster!=null && districtMaster.getStatusNotes()!=null){
					if(districtMaster.getStatusNotes()==false){
						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								finalizeStatusFlag=true;
							}
						}
					}
				}
				if(statusName.equalsIgnoreCase("Asst Supe Interview"))
				{
					finalizeStatusFlag=true;
				}
			}catch(Exception e){}
			System.out.println("statusName::::::::::"+statusName);
			System.out.println("finalizeStatusFlag::::::::::"+finalizeStatusFlag);
			try{

				if(teacherStatusNoteId.equals("0"))
				{

					if((statusNoteFileName!=null && !statusNoteFileName.equals("")) || (statusNotes!=null && !statusNotes.equals("")))
					{
						TeacherStatusNotes tsDetailsObj = new TeacherStatusNotes();
						if(teacherStatusNoteId!=null){
							if(!teacherStatusNoteId.equals("0")){
								tsDetailsObj =teacherStatusNotesDAO.findById(Integer.parseInt(teacherStatusNoteId),false,false);
							}
						}
						tsDetailsObj.setTeacherDetail(teacherDetail);
						tsDetailsObj.setUserMaster(userMaster);
						tsDetailsObj.setJobOrder(jobOrder);
						if(districtMaster!=null){
							tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
						}
						if(statusId!=null && !statusId.equals("0")){
							tsDetailsObj.setStatusMaster(statusMaster);
						}
						if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
							tsDetailsObj.setSecondaryStatus(secondaryStatus);
						}
						if(statusNotes!=null){
							tsDetailsObj.setStatusNotes(statusNotes);
						}
						if(statusNoteFileName!=null){
							if(tsDetailsObj.getStatusNoteFileName()!=null){
								try{
									File file = new File(Utility.getValueOfPropByKey("communicationRootPath")+"statusNote/"+tsDetailsObj.getTeacherDetail().getTeacherId()+"/"+tsDetailsObj.getStatusNoteFileName());
									if(file.delete()){
										System.out.println(file.getName() + " is deleted!");
									}else{
										System.out.println("Delete operation is failed.");
									}
								}catch(Exception e){}
							}
							tsDetailsObj.setStatusNoteFileName(statusNoteFileName);
						}
						if(schoolMaster!=null){
							tsDetailsObj.setSchoolId(schoolMaster.getSchoolId());	
						}
						int emailSentTo=0;
						if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
							emailSentTo=2;
						}
						if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
							emailSentTo=1;
						}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
							emailSentTo=3;
						}
						tsDetailsObj.setEmailSentTo(emailSentTo);

						tsDetailsObj.setTeacherAssessmentQuestionId(null);

						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								tsDetailsObj.setFinalizeStatus(true);
								finalizeStatusFlag=true;
							}
						}
						teacherStatusNotesDAO.makePersistent(tsDetailsObj);
					}

					// Set ReferenceCheckHistory Table

					List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistoryList = null;
					teacherElectronicReferencesHistoryList	=	teacherElectronicReferencesHistoryDAO.countContactStatusRefChkByHBD(jobOrder, jobForTeacherObj.getTeacherId());;	
					Integer elerefHAutoId = null;
					try{
						if(teacherElectronicReferencesHistoryList != null){
							for(TeacherElectronicReferencesHistory objRefHistory : teacherElectronicReferencesHistoryList){
								elerefHAutoId	=	objRefHistory.getElerefHAutoId();
								TeacherElectronicReferencesHistory objEl = teacherElectronicReferencesHistoryDAO.findById(elerefHAutoId, false, false);	
								objEl.setReplyStatus(1);
								teacherElectronicReferencesHistoryDAO.makePersistent(objEl);
							}
						}
					} catch(Exception e){}

					//mukesh
					if((statusNotes==null || statusNotes.equals("")) && fitScore.equals("0.0")){
						if(districtMaster!=null && districtMaster.getStatusNotes()==false){
							if(finalizeStatus!=null){
								if(!finalizeStatus.equals("0")){
									finalizeStatusFlag=true;
								}
							}
						}
						/*if(statusName.equalsIgnoreCase("Asst Supe Interview"))
						{
							finalizeStatusFlag=true;
						}*/
					}
					TeacherStatusNotes checkForFinalize = teacherStatusNotesDAO.chkQuestionNotesStatusByUserMasterAndHBD(teacherDetail, jobOrder,qAnssementQuesIds,userMaster);

					if(checkForFinalize!=null && checkForFinalize.isFinalizeStatus()){
						System.out.println(" Data is finalize.......");
					}else{
						// For Question Status Notes
						if(qAssessmentIds_array!=null)
							for(int i=0;i<qAssessmentIds_array.length;i++)
							{
								TeacherStatusNotes tsDetailsObj = teacherStatusNotesDAO.getQuestionNoteObjByUserMasterAndHBD(teacherDetail, jobOrder,Long.parseLong(qAssessmentIds_array[i]),userMaster);
								if(tsDetailsObj==null)
								{
									tsDetailsObj = new TeacherStatusNotes();
								}


								tsDetailsObj.setTeacherDetail(teacherDetail);
								tsDetailsObj.setUserMaster(userMaster);
								tsDetailsObj.setJobOrder(jobOrder);
								if(districtMaster!=null){
									tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
								}
								if(statusId!=null && !statusId.equals("0")){
									tsDetailsObj.setStatusMaster(statusMaster);
								}
								if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
									tsDetailsObj.setSecondaryStatus(secondaryStatus);
								}
								tsDetailsObj.setStatusNoteFileName(null);
								if(schoolMaster!=null){
									tsDetailsObj.setSchoolId(schoolMaster.getSchoolId());
								}
								int emailSentTo=0;
								if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
									emailSentTo=2;
								}
								if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
									emailSentTo=1;
								}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
									emailSentTo=3;
								}
								tsDetailsObj.setEmailSentTo(emailSentTo);

								if(qAssessmentIds_array[i]!=null && !qAssessmentIds_array[i].equals(""))
									tsDetailsObj.setTeacherAssessmentQuestionId(Long.parseLong(qAssessmentIds_array[i]));
								else
									tsDetailsObj.setTeacherAssessmentQuestionId(null);

								boolean noQuestion=false;
								if(qqNoteIdsvalue_str_array!=null && qqNoteIdsvalue_str_array.length>i){
									if(qqNoteIdsvalue_str_array[i]!=null && !qqNoteIdsvalue_str_array[i].equals("")){
										tsDetailsObj.setStatusNotes(qqNoteIdsvalue_str_array[i]);
										noQuestion=true;
									}
								}
								if(finalizeStatus!=null){
									if(!finalizeStatus.equals("0")){
										tsDetailsObj.setFinalizeStatus(true);
										finalizeStatusFlag=true;
									}
								}
								if(noQuestion){
									teacherStatusNotesDAO.makePersistent(tsDetailsObj);
								}
							}
					}
				}
				else
				{
					if((statusNoteFileName!=null && !statusNoteFileName.equals("")) || (statusNotes!=null && !statusNotes.equals("")))
					{
						TeacherStatusNotes tsDetailsObj = new TeacherStatusNotes();
						boolean updatedbyuser=true;
						if(teacherStatusNoteId!=null){
							if(!teacherStatusNoteId.equals("0")){
								tsDetailsObj =teacherStatusNotesDAO.findById(Integer.parseInt(teacherStatusNoteId),false,false);
								TeacherStatusNotesHistory copyjTSNH=new TeacherStatusNotesHistory();
		 			 			 try {
		 							BeanUtils.copyProperties(copyjTSNH, tsDetailsObj);
		 							copyjTSNH.setTeacherStatusNoteId(null);
		 							copyjTSNH.setUpdatedByUserId(userMaster.getUserId());
		 							copyjTSNH.setStatusUpdatedNotes(statusNotes);
		 							teacherStatusNotesHistoryDAO.makePersistent(copyjTSNH);
		 							tsDetailsObj.setUserMaster(copyjTSNH.getUserMaster());
		 							if(isSuperAdminUser)
		 							updatedbyuser=false;
		 						}catch(Exception e){
		 							e.printStackTrace();
		 						}
		 						//ended by 01-04-2015
							}
						}
						tsDetailsObj.setTeacherDetail(teacherDetail);
						
						if(updatedbyuser)//added by 04-04-2015
						tsDetailsObj.setUserMaster(userMaster);
						tsDetailsObj.setJobOrder(jobOrder);
						if(districtMaster!=null){
							tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
						}
						if(statusId!=null && !statusId.equals("0")){
							tsDetailsObj.setStatusMaster(statusMaster);
						}
						if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
							tsDetailsObj.setSecondaryStatus(secondaryStatus);
						}
						if(statusNotes!=null){
							tsDetailsObj.setStatusNotes(statusNotes);
						}
						if(statusNoteFileName!=null){
							if(tsDetailsObj.getStatusNoteFileName()!=null){
								try{
									File file = new File(Utility.getValueOfPropByKey("communicationRootPath")+"statusNote/"+tsDetailsObj.getTeacherDetail().getTeacherId()+"/"+tsDetailsObj.getStatusNoteFileName());
									if(file.delete()){
										System.out.println(file.getName() + " is deleted!");
									}else{
										System.out.println("Delete operation is failed.");
									}
								}catch(Exception e){}
							}
							tsDetailsObj.setStatusNoteFileName(statusNoteFileName);
						}
						if(schoolMaster!=null){
							tsDetailsObj.setSchoolId(schoolMaster.getSchoolId());
						}
						int emailSentTo=0;
						if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
							emailSentTo=2;
						}
						if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
							emailSentTo=1;
						}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
							emailSentTo=3;
						}
						tsDetailsObj.setEmailSentTo(emailSentTo);

						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								tsDetailsObj.setFinalizeStatus(true);
								finalizeStatusFlag=true;
							}
						}
						teacherStatusNotesDAO.makePersistent(tsDetailsObj);
					}

					TeacherStatusNotes checkForFinalize = teacherStatusNotesDAO.chkQuestionNotesStatusByUserMasterAndHBD(teacherDetail, jobOrder, qAnssementQuesIds,userMaster);

					if(checkForFinalize!=null && checkForFinalize.isFinalizeStatus()){
						System.out.println(" Data is finalize.......");
					}else{
						// For Question Status Notes in Edit Mode
						if(qAssessmentIds_array!=null)
							for(int i=0;i<qAssessmentIds_array.length;i++)
							{
								TeacherStatusNotes tsDetailsObj = teacherStatusNotesDAO.getQuestionNoteObjByUserMasterAndHBD(teacherDetail, jobOrder,Long.parseLong(qAssessmentIds_array[i]),userMaster);
								if(tsDetailsObj==null)
								{
									tsDetailsObj = new TeacherStatusNotes();
								}


								tsDetailsObj.setTeacherDetail(teacherDetail);
								tsDetailsObj.setUserMaster(userMaster);
								tsDetailsObj.setJobOrder(jobOrder);
								if(districtMaster!=null){
									tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
								}
								if(statusId!=null && !statusId.equals("0")){
									tsDetailsObj.setStatusMaster(statusMaster);
								}
								if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
									tsDetailsObj.setSecondaryStatus(secondaryStatus);
								}
								tsDetailsObj.setStatusNoteFileName(null);
								if(schoolMaster!=null){
									tsDetailsObj.setSchoolId(schoolMaster.getSchoolId());
								}
								int emailSentTo=0;
								if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
									emailSentTo=2;
								}
								if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
									emailSentTo=1;
								}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
									emailSentTo=3;
								}
								tsDetailsObj.setEmailSentTo(emailSentTo);

								if(qAssessmentIds_array[i]!=null && !qAssessmentIds_array[i].equals(""))
									tsDetailsObj.setTeacherAssessmentQuestionId(Long.parseLong(qAssessmentIds_array[i]));
								else
									tsDetailsObj.setTeacherAssessmentQuestionId(null);

								boolean noQuestion=false;
								if(qqNoteIdsvalue_str_array!=null && qqNoteIdsvalue_str_array.length>i){
									if(qqNoteIdsvalue_str_array[i]!=null && !qqNoteIdsvalue_str_array[i].equals("")){
										tsDetailsObj.setStatusNotes(qqNoteIdsvalue_str_array[i]);
										noQuestion=true;
									}
								}

								if(finalizeStatus!=null){
									if(!finalizeStatus.equals("0")){
										tsDetailsObj.setFinalizeStatus(true);
										finalizeStatusFlag=true;
									}
								}
								if(noQuestion){
									teacherStatusNotesDAO.makePersistent(tsDetailsObj);
								}
							}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}	

			//Override
			if(statusHistory!=null && finalizeStatusFlag)
			{
				if(chkOverridetSatus){
					statusHistory.setOverride(chkOverridetSatus);
					statusHistory.setOverrideBy(userMaster);
				}
				try {
					teacherStatusHistoryForJobDAO.makePersistent(statusHistory);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			try{
				TeacherStatusHistoryForJob statusHistoryForWaived=teacherStatusHistoryForJobDAO.findWaivedStatusByTeacherAndJob(teacherDetail,jobOrder,statusMaster,secondaryStatus);
				System.out.println(statusWaived+":::::::::::statusHistoryForWaived:::::::::::::::"+statusHistoryForWaived);
				if(statusHistoryForWaived!=null && statusWaived==false && finalizeStatusFlag){
					statusHistoryForWaived.setStatus("S");
					statusHistoryForWaived.setUserMaster(userMaster);
					statusHistoryForWaived.setUpdatedDateTime(new Date());
					teacherStatusHistoryForJobDAO.updatePersistent(statusHistoryForWaived);
				}
				//return null;
			}catch(Exception e){
				e.printStackTrace();
			}
			//add by Ram Nath for JSI Score
			String jsiStatusName="";
			if(statusMasterForJobs!=null)
			jsiStatusName=statusMasterForJobs.getStatus();
			else if(secondaryStatusForJobs!=null)
			jsiStatusName=secondaryStatusForJobs.getSecondaryStatusName();
			//System.out.println("status==="+jsiStatusName);				
			//end by Ram Nath
			
			// Start Update Answer Score
			int iReturnValue=0;
			Double inputSumAnswerScore=0.0;
			Double inputSumAnswerMaxScore=0.0; 
			if(answerId_array!=null && answerId_array.length>0)
			{
				List<StatusSpecificScore> StatusSpecificScoreList=statusSpecificScoreDAO.getStatusSpecificScoreList(answerId_array);
				Map<Integer,StatusSpecificScore> sSFSMap= new HashMap<Integer, StatusSpecificScore>();
				for(StatusSpecificScore sssOBJ : StatusSpecificScoreList ){
					sSFSMap.put(sssOBJ.getAnswerId(),sssOBJ);
				}

				for(int i=0; i<answerId_array.length;i++)	
				{
					try {
						StatusSpecificScore statusSpecificScore=null;
						if(sSFSMap!=null){
							statusSpecificScore=sSFSMap.get(answerId_array[i]);
						}
						if(statusSpecificScore!=null)
						{
							//start add by ram nath for JSI Score
							if(jsiStatusName.equalsIgnoreCase("jsi")){
								statusSpecificScore.setFinalizeStatus(Integer.parseInt(finalizeStatus));
								statusSpecificScore.setScoreProvided(score_array[i]);
								statusSpecificScoreDAO.makePersistent(statusSpecificScore);
								iReturnValue++;
								inputSumAnswerScore=inputSumAnswerScore+statusSpecificScore.getScoreProvided();
								inputSumAnswerMaxScore=inputSumAnswerMaxScore+statusSpecificScore.getMaxScore();
							}else//end by ram nath	
							if(score_array[i]!=0){
								statusSpecificScore.setFinalizeStatus(Integer.parseInt(finalizeStatus));
								statusSpecificScore.setScoreProvided(score_array[i]);
								statusSpecificScoreDAO.makePersistent(statusSpecificScore);
								iReturnValue++;
								inputSumAnswerScore=inputSumAnswerScore+statusSpecificScore.getScoreProvided();
								inputSumAnswerMaxScore=inputSumAnswerMaxScore+statusSpecificScore.getMaxScore();
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			// End Update Answer Score

			boolean  jobWiseScoreFlag=false;
			List<TeacherStatusScores> teacherStatusScoresList=new ArrayList<TeacherStatusScores>();
			List<TeacherStatusNotes> teacherStatusNotesList=new ArrayList<TeacherStatusNotes>();
			try{
				//added by 10-04-2015
				//System.out.println("minOneScoreSliderSetOrNotFlag==="+minOneScoreSliderSetOrNotFlag+" inputSumAnswerScore==="+inputSumAnswerScore+"outerJob============="+outerJob);
				boolean isInsertZeroScore=false;
				//if(minOneScoreSliderSetOrNotFlag==1)
				//isInsertZeroScore=true;
				//ended by 10-04-2015
				if(outerJob==false && (( scoreProvided!=null && !scoreProvided.equals("0") && !scoreProvided.equals("")) || (inputSumAnswerScore>0) || isInsertZeroScore)){ //added by 10-04-2015 || isInsertZeroScore
					int fStatus=0;
					teacherStatusScoresList= teacherStatusScoresDAO.getStatusScore(teacherDetail,jobOrder,statusMaster,secondaryStatus,userMaster);
					TeacherStatusScores statusScores = new TeacherStatusScores();
					if(teacherStatusScoresList!=null && teacherStatusScoresList.size()> 0)
					{
						statusScores =teacherStatusScoresList.get(0);
						if(statusScores.isFinalizeStatus())
							fStatus=1;
					}
					if(fStatus==0){
						statusScores.setTeacherDetail(teacherDetail);
						statusScores.setUserMaster(userMaster);
						statusScores.setJobOrder(jobOrder);
						if(districtMaster!=null){
							statusScores.setDistrictId(districtMaster.getDistrictId());
						}
						if(statusId!=null && !statusId.equals("0")){
							statusScores.setStatusMaster(statusMaster);
						}
						if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
							statusScores.setSecondaryStatus(secondaryStatus);
						}

						if(schoolMaster!=null){
							statusScores.setSchoolId(schoolMaster.getSchoolId());	
						}
						int emailSentTo=0;
						if(emailSentToD!=null && emailSentToD.equalsIgnoreCase("true")){
							emailSentTo=2;
						}
						if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true") && emailSentTo==2){
							emailSentTo=1;
						}else if(emailSentToS!=null && emailSentToS.equalsIgnoreCase("true")){
							emailSentTo=3;
						}
						statusScores.setEmailSentTo(emailSentTo);

						if(inputSumAnswerScore>0)
						{
							statusScores.setScoreProvided(inputSumAnswerScore);
						}
						else{
							if(scoreProvided!=null && !scoreProvided.trim().equals("")){
								System.out.println(" scoreProvided :: "+scoreProvided);
								statusScores.setScoreProvided(Double.parseDouble(scoreProvided));
							}
						}

						if(inputSumAnswerScore>0)
						{
							statusScores.setMaxScore(inputSumAnswerMaxScore);
						}
						else
						{
							if(fitScore!=null){
								statusScores.setMaxScore(Double.parseDouble(fitScore));
							}
						}

						if(finalizeStatus!=null){
							if(!finalizeStatus.equals("0")){
								statusScores.setFinalizeStatus(true);
								jobWiseScoreFlag=true;
							}
						}
						teacherStatusScoresDAO.makePersistent(statusScores);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			///////////////////////Auto Panel Creation//////////////////////

			try{
				if(entityType==3 && userSchoolMaster!=null){
					JobWisePanelStatus jobWisePanelStatusObj=null;
					if(isPhiladelphia)
					jobWisePanelStatusObj =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacherObj.getJobId(),"No Evaluation Complete");	//added by ram nath
					else
					jobWisePanelStatusObj =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacherObj.getJobId(),"Offer Ready");
					if(jobWisePanelStatusObj!=null){
						List<PanelSchedule> pSList=panelScheduleDAO.findPanelSchedules(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId());
						List<StafferMaster>	sList=stafferMasterDAO.findStafferMasterList(districtMaster,userSchoolMaster);
						List<DistrictKeyContact> dKCList=new ArrayList<DistrictKeyContact>();
						List<UserMaster> userMasters=new ArrayList<UserMaster>();
						UserMaster stafferUser=null;
						if(sList.size()==0){
							try{
								dKCList=districtKeyContactDAO.findByContactType(districtMaster,"Staffer");	
								if(dKCList.size()>0){
									userMasters=userMasterDAO.findByEmailAndDate(dKCList.get(0).getKeyContactEmailAddress());
									if(userMasters.size()>0){
										for (UserMaster userMasterObj : userMasters) {
											stafferUser=userMasterObj;
											break;
										}
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
						//added by 21-03-2015
						if(((isPhiladelphia && secondaryStatusForPanel.getSecondaryStatusName().equals("No Evaluation Complete"))||(isMiami && secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready"))) && districtMaster!=null && userSchoolMaster!=null && pSList.size()==0 && (sList.size()>0 || stafferUser!=null ) ){
							UserMaster[] userMasterArray=new UserMaster[2];
							try{
								userMasterArray[0]=userMaster;
								if(sList.size()>0){
									userMasterArray[1]=sList.get(0).getUserMaster();
								}else{
									userMasterArray[1]=stafferUser;
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
				}
			}catch(Exception e){}
			////////////////////Auto Panel Ceration End///////////////////////////////////

			//System.out.println(jobOrder.getDistrictMaster()+"!=null && "+statusMaster+"!=null && ("+jobWiseScoreFlag+" || "+finalizeStatusFlag+" ) && ("+statusMaster.getStatusShortName()+" jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()="+jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints());
			if(jobOrder.getDistrictMaster()!=null && statusMaster!=null && (jobWiseScoreFlag || finalizeStatusFlag ) && (statusMaster.getStatusShortName().equalsIgnoreCase("scomp") || statusMaster.getStatusShortName().equalsIgnoreCase("ecomp") || statusMaster.getStatusShortName().equalsIgnoreCase("vcomp"))){
				if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()!=null){
					if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()){
						String accessDPoints="";
						if(jobOrder.getDistrictMaster().getAccessDPoints()!=null)
							accessDPoints=jobOrder.getDistrictMaster().getAccessDPoints();

						if(accessDPoints.contains(""+statusMaster.getStatusId())){
							List<TeacherStatusHistoryForJob> lstTSHJob =teacherStatusHistoryForJobDAO.findByTeacherStatusHistorySelected(teacherDetail,jobOrder);
							List<SecondaryStatus> listSecondaryStatus = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithStatusAndHBD(jobOrder,statusMaster);
							int isInternalCandidate=jobForTeacherObj.getIsAffilated()==0?3:2;
							Map<Integer,SecondaryStatus> secMap=new HashMap<Integer, SecondaryStatus>();
							for(TeacherStatusHistoryForJob tSHObj : lstTSHJob){
								if(tSHObj.getSecondaryStatus()!=null)
									secMap.put(tSHObj.getSecondaryStatus().getSecondaryStatusId(),tSHObj.getSecondaryStatus());
							}
							if(listSecondaryStatus.size()>0){
								List<SecondaryStatus> lstSecondaryStatus=listSecondaryStatus.get(0).getChildren();
								List<JobCategoryWiseStatusPrivilege> lstJCWSP= jobCategoryWiseStatusPrivilegeDAO.getStatus(jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster(),lstSecondaryStatus);
								List<SecondaryStatus> lstSS=new ArrayList<SecondaryStatus> ();
								for(JobCategoryWiseStatusPrivilege jcwspObj:lstJCWSP){
									if(jcwspObj.getInternalExternalOrBothCandidate()!=null){
									if(jcwspObj.getInternalExternalOrBothCandidate()==2 && isInternalCandidate!=2)
										lstSS.add(jcwspObj.getSecondaryStatus());
									else if(jcwspObj.getInternalExternalOrBothCandidate()==3 && isInternalCandidate!=3)
										lstSS.add(jcwspObj.getSecondaryStatus());
									}else{
										lstSS.add(jcwspObj.getSecondaryStatus());
									}
								}
								lstSecondaryStatus.removeAll(lstSS);
								int counter=0;
								for(SecondaryStatus  obj : listSecondaryStatus.get(0).getChildren()){
									if(obj.getStatus().equalsIgnoreCase("A") && obj.getStatusMaster()==null){
										SecondaryStatus sec=secMap.get(obj.getSecondaryStatusId());
										if(sec==null){
											boolean isJobAssessment=false;
											if(jobOrder.getIsJobAssessment()!=null){
												if(jobOrder.getIsJobAssessment()){
													isJobAssessment=true;
												}
											}
											if((isJobAssessment && obj.getSecondaryStatusName().equalsIgnoreCase("JSI"))|| !obj.getSecondaryStatusName().equalsIgnoreCase("JSI")){
												if(counter==1){
													secondaryStatusNames+=", ";
												}
												counter=0;
												secondaryStatusNames+=obj.getSecondaryStatusName();
												counter++;
											}
										}
									}
								}
							}
							if(!secondaryStatusNames.equals("")){
								errorFlag="5";
							}
							//********************************************End TPL 2024 (Internal And External Candidate) ***************************************************//
		
						}
					}
				}
			}
			
			if((headQuarterMaster!=null || branchMaster!=null || districtMaster!=null) && jobCategoryMaster!=null && secondaryStatusForJobs!=null && finalizeStatus!=null && finalizeStatus.equals("1"))
            {
                List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivilegeList = jobCategoryWiseStatusPrivilegeDAO.getStatusByHBD(jobOrder, secondaryStatusForJobs);
                if(jobCategoryWiseStatusPrivilegeList.size()>0)
                {
                        if(jobCategoryWiseStatusPrivilegeList.get(0).isOverrideUserSettings())
                             overrideAll=true;
                }
            }

			
			
			
			////////////////////////Start multiplejob Add one time //////////////////////
			if(jobCategoryFlagValue!=2 && entityType==2 && secondaryStatusNames.equals("")){
				List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=jobCategoryWiseStatusPrivilegeDAO.getFilterStatusByHBD(jobOrder, secondaryStatusForJobs);
				System.out.println("jobCategoryWiseStatusPrivileges:::::::::::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+jobCategoryWiseStatusPrivileges.size());
				int jobCategoryFlag=3;
				if(superAdminClickOnEditNote!=1)//added by 03-04-2015
				try{
					if(jobCategoryWiseStatusPrivileges.size()>0){
						if(jobCategoryWiseStatusPrivileges.get(0).isAutoUpdateStatus()){
							if(jobCategoryWiseStatusPrivileges.get(0).getUpdateStatusOption()!=null){
								if(jobCategoryWiseStatusPrivileges.get(0).getUpdateStatusOption()==0){
									jobCategoryFlag=0;
								}else if(jobCategoryWiseStatusPrivileges.get(0).getUpdateStatusOption()==1){
									jobCategoryFlag=1;
								}else if(jobOrder.getGeoZoneMaster()!=null){
									jobCategoryFlag=2;	
								}
							}
						}
					//	if(jobCategoryWiseStatusPrivileges.get(0).isOverrideUserSettings())
					//		overrideAll=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				Map<Integer,SecondaryStatus> sStatusMap= new HashMap<Integer, SecondaryStatus>();
				//System.out.println("::::::::::::::::::::jobCategoryFlag:::::::::::::::::::::::"+jobCategoryFlag);
				try{
					if(statusMasterForJobs!=null)
						if(statusMasterForJobs.getStatusShortName().equalsIgnoreCase("widrw") || statusMasterForJobs.getStatusShortName().equalsIgnoreCase("hird")|| statusMasterForJobs.getStatusShortName().equalsIgnoreCase("dcln")|| statusMasterForJobs.getStatusShortName().equalsIgnoreCase("rem")){
							jobCategoryFlag=3;
						}
				}catch(Exception e){}
				try{
					if(jobCategoryFlag!=3){
						List<JobCategoryMaster> jobCategoryMasters= new ArrayList<JobCategoryMaster>();
						List<JobForTeacher> jobForTeachers=new ArrayList<JobForTeacher>();
						List<SecondaryStatus> secondaryStatusList= new ArrayList<SecondaryStatus>();
						if(jobCategoryFlag==0){
							jobForTeachers= jobForTeacherDAO.findJobByTeacherAndJobCategoryHBD(teacherDetail,jobOrder,jobForTeacherId);
							jobCategoryMasters.add(jobCategoryMaster);
						}else if(jobCategoryFlag==1){
							jobForTeachers= jobForTeacherDAO.findJobByTeacherAndDistrictHBD(teacherDetail,jobOrder,jobForTeacherId);
							if(jobForTeachers.size()>0)
								for (JobForTeacher jobForTeacher : jobForTeachers) {
									if(jobForTeacher.getJobId().getHeadQuarterMaster()==null && jobForTeacher.getJobId().getDistrictMaster()!=null){
										jobCategoryMasters.add(jobForTeacher.getJobId().getJobCategoryMaster());
									}
									else{
										if(jobForTeacher.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null)
											jobCategoryMasters.add(jobForTeacher.getJobId().getJobCategoryMaster().getParentJobCategoryId());
										else
											jobCategoryMasters.add(jobForTeacher.getJobId().getJobCategoryMaster());
									}
								}
							secondaryStatusList=secondaryStatusDAO.findSecondaryStatusByJobCategoryAndDistrictHBD(jobCategoryMasters,jobOrder,secondaryStatusForJobs);

							if(secondaryStatusList.size()>0)
								for (SecondaryStatus secondaryStatus2 : secondaryStatusList) {
									sStatusMap.put(secondaryStatus2.getJobCategoryMaster().getJobCategoryId(),secondaryStatus2);
								}
						}else if(jobCategoryFlag==2){

							String jobTitle=jobOrder.getJobTitle();
							if(jobTitle.indexOf("(")>0){
								jobTitle=jobTitle.substring(0,jobTitle.lastIndexOf("(")).trim();
								//System.out.println("jobTitle:::::::::"+jobTitle);
							}
							//System.out.println("jobTitle::::::"+jobTitle);
							jobForTeachers= jobForTeacherDAO.findJobByTeacherAndJobTitleHBD(teacherDetail,jobOrder,jobForTeacherId,jobTitle);
							jobCategoryMasters.add(jobCategoryMaster);
						}
						System.out.println("jobForTeachers::::::"+jobForTeachers.size());
						List<JobOrder> jobOrdersLst=new ArrayList<JobOrder>();
						for (JobForTeacher jobForTeacher : jobForTeachers) {
							jobOrdersLst.add(jobForTeacher.getJobId());
						}
						List<StatusSpecificQuestions> lstStatusSpecificQuestions = new ArrayList<StatusSpecificQuestions>();
						lstStatusSpecificQuestions=statusSpecificQuestionsDAO.findQuestions_msuByHBD(jobOrder, secondaryStatusForJobs, statusMasterForJobs);
						MassStatusUpdateService msusObj= new MassStatusUpdateService();
						List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
						Map<JobOrder,List<StatusSpecificScore>> mapStatusScoreDetailsByJobOrder= new HashMap<JobOrder,List<StatusSpecificScore>>();
						Map<String,StatusSpecificScore> ssfMap=new HashMap<String, StatusSpecificScore>();
						Map<String,StatusSpecificScore> ssfMapfoCategory=new HashMap<String, StatusSpecificScore>();
						Map<String,StatusSpecificScore> questionTxtMap= new HashMap<String, StatusSpecificScore>();
						Map<Integer,StatusSpecificScore> questionMap= new HashMap<Integer, StatusSpecificScore>();

						//System.out.println("lstStatusSpecificQuestions.size()::::"+lstStatusSpecificQuestions.size());
						String recentStatusName=null;
						try{
							if(secondaryStatusForJobs!=null){
								recentStatusName=secondaryStatusForJobs.getSecondaryStatusName();
							}else{
								recentStatusName=statusMasterForJobs.getStatus();
							}
						}catch(Exception e){}
						if(iReturnValue!=0){
							try{
								if(lstStatusSpecificQuestions.size() > 0 && jobOrdersLst.size()>0)
								{
									lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList_msuByHBD(jobOrder,jobOrdersLst,statusMasterForJobs,secondaryStatusForJobs,teacherDetail,userMaster);

									if(lstStatusSpecificScore.size() == 0)
									{
										if(jobOrder.getJobCategoryMaster()!=null)
											statusSpecificScoreDAO.copyQuestionAnswersForAllJobCategoryLst_msuByHBD(jobCategoryMasters,jobOrdersLst, statusMasterForJobs,secondaryStatusForJobs, secondaryStatusList, userMaster, teacherDetail);
									}
									else{
										mapStatusScoreDetailsByJobOrder=msusObj.getStatusScoreDetailsByJobOrder(lstStatusSpecificScore);

										List<JobOrder> jobOrdersLst_Partial=new ArrayList<JobOrder>();
										jobOrdersLst_Partial=msusObj.getJobOrderListWithNoQuestion(jobOrdersLst, mapStatusScoreDetailsByJobOrder);

										if(jobOrdersLst_Partial.size() > 0)
										{
											if(jobOrder.getJobCategoryMaster()!=null)
												statusSpecificScoreDAO.copyQuestionAnswersForAllJobCategoryLst_msuByHBD(jobCategoryMasters,jobOrdersLst_Partial, statusMasterForJobs,secondaryStatusForJobs, secondaryStatusList, userMaster, teacherDetail);
										}
									}

								}
							}catch(Exception e){}
							List<StatusSpecificScore> statusSpecificScoreList=statusSpecificScoreDAO.getStatusSpecificScoreList(answerId_array);

							for(StatusSpecificScore sssOBJ : statusSpecificScoreList ){
								if(sssOBJ.getStatusSpecificQuestions()!=null){
									if(sssOBJ.getSkillAttributesMaster()!=null){
										questionTxtMap.put(sssOBJ.getStatusSpecificQuestions().getQuestion()+"##"+sssOBJ.getSkillAttributesMaster().getSkillAttributeId(),sssOBJ);
									}else{
										questionTxtMap.put(sssOBJ.getStatusSpecificQuestions().getQuestion(),sssOBJ);
									}
									questionMap.put(sssOBJ.getStatusSpecificQuestions().getQuestionId(),sssOBJ);
								}else{
									questionMap.put(sssOBJ.getTeacherAnswerDetail().getAnswerId(),sssOBJ);
								}
							}
							if(jobOrdersLst.size()>0)
								lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionAllJobCategoryList_msuBYHBD(jobOrder,jobCategoryMasters,jobOrdersLst,statusMasterForJobs,secondaryStatusForJobs,secondaryStatusList,teacherDetail,userMaster);
							for (StatusSpecificScore statusSpecificScore : lstStatusSpecificScore) {
								if(statusSpecificScore.getStatusSpecificQuestions()!=null){
									if(statusSpecificScore.getJobCategoryId().equals(jobCategoryMaster.getJobCategoryId())){
										String ssfValue=statusSpecificScore.getStatusSpecificQuestions().getQuestionId()+"##"+statusSpecificScore.getJobOrder().getJobId();
										ssfMap.put(ssfValue,statusSpecificScore);	
									}else{
										String ssfCValue=null;
										if(statusSpecificScore.getSkillAttributesMaster()!=null){
											ssfCValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+statusSpecificScore.getSkillAttributesMaster().getSkillAttributeId()+"##"+statusSpecificScore.getJobOrder().getJobId();
										}else{
											ssfCValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getStatusSpecificQuestions().getQuestion()+"##"+statusSpecificScore.getJobOrder().getJobId();
										}
										ssfMapfoCategory.put(ssfCValue,statusSpecificScore);	
									}
								}else{
									if(statusSpecificScore.getJobCategoryId().equals(jobCategoryMaster.getJobCategoryId())){
										String ssfValue=statusSpecificScore.getTeacherAnswerDetail().getAnswerId()+"##"+statusSpecificScore.getJobOrder().getJobId();
										ssfMap.put(ssfValue,statusSpecificScore);
									}else{
										String ssfCValue=statusSpecificScore.getMaxScore()+"##"+statusSpecificScore.getTeacherAnswerDetail().getAnswerId()+"##"+statusSpecificScore.getJobOrder().getJobId();
										ssfMapfoCategory.put(ssfCValue,statusSpecificScore);	
									}
								}
							}
						}
						String saveMultiReturnVal="";
					}
				}catch(Exception e){}
			}
			//System.out.println("Final multiJobReturn::::::"+multiJobReturn);
			//////////////////////End multiplejob Add one time //////////////////////

			System.out.println("------------------------------------------End Multiple Jobs-------------------------------------------------------------------------------");
			String hireText="";
			if(headQuarterMaster!=null){
				hireText="Active";
			}else{
				hireText="Hire";
			}


			//System.out.println("statusUpdateHDR:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::>>>"+statusUpdateHDR);
			String schoolLocation="";
			String locationCode="";
			if(isMiami || isPhiladelphia){ //added by 21-03-2015
				JobForTeacher jobForTeacherForisMiami = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);


				List<SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus= new ArrayList<SchoolWiseCandidateStatus>();
				if(userMaster!=null)
					if(userMaster.getEntityType()==3){	
						lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusHistoryForSchoolByHBD(jobForTeacherForisMiami.getTeacherId(),jobForTeacherForisMiami.getJobId(),userMaster.getSchoolId());
					}else if(userMaster.getEntityType()==2){
						lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusHistoryByUserAndHBD(jobForTeacherForisMiami.getTeacherId(), jobForTeacherForisMiami.getJobId(),userMaster);
					}
			}

			// Start ... Panel
			List<UserMaster> userMasters_PanelAttendees=new ArrayList<UserMaster>();
			List<UserMaster> panelAttendees_ForEmail=new ArrayList<UserMaster>();
			List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
			Map<Integer,UserMaster> panelMemberMap=new HashMap<Integer, UserMaster>();

			SecondaryStatus secondaryStatus_temp=null;
			StatusMaster statusMaster_temp=null;
			List<SecondaryStatus> lstSecondaryStatus_temp=new ArrayList<SecondaryStatus>();

			if(statusMaster!=null)
			{
				lstSecondaryStatus_temp=secondaryStatusDAO.getSecondaryStatusForPanelByHBD(jobOrder, statusMaster);
				if(lstSecondaryStatus_temp.size() ==1)
				{
					statusMasterSecondaryName=lstSecondaryStatus_temp.get(0).getSecondaryStatusName();//added
					secondaryStatus_temp=lstSecondaryStatus_temp.get(0);
					statusMaster_temp=null;
				}
				else
				{
					statusMaster_temp=statusMaster;
				}
			}
			else

			{
				secondaryStatus_temp=secondaryStatus;
			}
			try{
				if(statusMasterSecondaryName==null){
					statusMasterSecondaryName=statusName;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("statusMasterSecondaryName::::::-:>>>>>:"+statusMasterSecondaryName);
			jobWisePanelStatusList=jobWisePanelStatusDAO.getPanel(jobOrder, secondaryStatus_temp, statusMaster_temp);
			boolean smartPractices=false;
			if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
					smartPractices=true;
				}
			}
			//System.out.println("flowOptionIds::::::::::::::::::::::::::::::::::::::>>>>>>>>>>>>>>>>"+flowOptionIds);
			if(smartPractices){
				List<WorkFlowOptions> flowOptions=flowOptionsDAO.getFlowOptions();
				Map<Integer,String> flowMap=new HashMap<Integer, String>();
				for (WorkFlowOptions flowOptionsObj : flowOptions) {
					flowMap.put(flowOptionsObj.getFlowOptionId(),flowOptionsObj.getFlowOptionName());
				}
				//System.out.println("flowMap::::Size::::::::::::::::::::::::::::::"+flowMap.size());
				WorkFlowOptionsForStatus flowOptionsForStatus=flowOptionsForStatusDAO.findOptionsByTeacherStatus(teacherDetail, jobOrder, statusMaster, secondaryStatus);
				//System.out.println("flowOptionsForStatus::::::::::::::::::::::::"+flowOptionsForStatus);
				String flowOptionsName="";
				try{
					String flowOptionsId[]=null;
					try{
						if(flowOptionIds!=null && !flowOptionIds.equals("")){
							flowOptionsId= flowOptionIds.split("#");
							int countFlow=0;
							for(int i=1;i<flowOptionsId.length;i++)
							{
								boolean existFlowOptions=false;
								if(flowOptionsForStatus!=null){
									if(flowOptionsForStatus.getFlowOptionIds().contains("#"+flowOptionsId[i]+"#")){
										existFlowOptions=true;
									}
								}
								
								if(existFlowOptions==false){
									if(countFlow!=0){
										flowOptionsName	 =	flowOptionsName+"<br/>"+flowMap.get(Integer.parseInt(flowOptionsId[i]));
									}else{
										flowOptionsName	 =	flowOptionsName+flowMap.get(Integer.parseInt(flowOptionsId[i]));
									}
									countFlow++;
								}
							}
						}
					}catch(Exception e){
						//e.printStackTrace();
					}
					//System.out.println("flowOptionsName:===============>===============:::::::"+flowOptionsName);
				}catch(Exception  e){
					e.printStackTrace();
				}
			}
			boolean bPanel=false;
			int iPanelAttendeeCount=0;
			StatusMaster statusMasterPanel=null;
			SecondaryStatus secondaryStatusPanel=null;
			statusMasterPanel=statusMaster;
			secondaryStatusPanel=secondaryStatus;
			boolean flagforpanelAttendees=true;//added by 21-03-2015

			List<PanelSchedule> panelScheduleList=new ArrayList<PanelSchedule>();
			if(jobWisePanelStatusList.size() == 1)
			{

				bPanel=true;
				panelScheduleList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWisePanelStatusList,teacherDetail);
				if(panelScheduleList.size()==1)
				{
					PanelSchedule panelSchedule=null;
					if(panelScheduleList.get(0)!=null)
					{
						panelSchedule=panelScheduleList.get(0);
						List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
						panelAttendees=panelAttendeesDAO.getPanelAttendees(panelSchedule);
						iPanelAttendeeCount=panelAttendees.size();

						if(panelAttendees.size() > 0)
						{
							for(PanelAttendees attendees:panelAttendees)
							{
								if(attendees!=null)
								{
									if(isPhiladelphia && userMaster.getEntityType()==3 && statusMaster!=null && statusMaster.getStatus().equals("No Evaluation Complete")) //added by 21-03-2015
									flagforpanelAttendees=false;
									userMasters_PanelAttendees.add(attendees.getPanelInviteeId());
									if(!attendees.getPanelInviteeId().getUserId().equals(userMaster.getUserId()))
									{
										panelAttendees_ForEmail.add(attendees.getPanelInviteeId());
									}
									try{
										if(attendees.getPanelInviteeId()!=null){
											panelMemberMap.put(attendees.getPanelInviteeId().getUserId(),attendees.getPanelInviteeId());
										}
									}catch(Exception e){							
									}
								}
							}
						}

					}
				}
			}
			// End ... Panel

			if(!errorFlag.equals("5")){

				teacherStatusScoresList= teacherStatusScoresDAO.getFinalizeStatusScore(teacherDetail,jobOrder,statusMaster,secondaryStatus);
				teacherStatusNotesList= teacherStatusNotesDAO.getFinalizeStatusScore(teacherDetail,jobOrder,statusMaster,secondaryStatus);

				Map<Integer,Integer> noteScoreMap= new HashMap<Integer, Integer>();
				try{
					for (TeacherStatusScores teacherStatusScores: teacherStatusScoresList) {
						int count =0;
						try{
							count=noteScoreMap.get(teacherStatusScores.getUserMaster().getUserId());
						}catch(Exception e){

						}
						if(count==0){
							noteScoreMap.put(teacherStatusScores.getUserMaster().getUserId(), 1);
						}else{
							noteScoreMap.put(teacherStatusScores.getUserMaster().getUserId(), 1+count);
						}
					}
					for (TeacherStatusNotes teacherStatusNotes: teacherStatusNotesList) {
						int count =0;
						try{
							count=noteScoreMap.get(teacherStatusNotes.getUserMaster().getUserId());
						}catch(Exception e){

						}
						if(count==0){
							noteScoreMap.put(teacherStatusNotes.getUserMaster().getUserId(), 1);
						}else{
							noteScoreMap.put(teacherStatusNotes.getUserMaster().getUserId(), 1+count);
						}

					}
				}catch(Exception e){}

				TeacherStatusHistoryForJob canNotHireObj= new TeacherStatusHistoryForJob();
				boolean noInsert=true;
				if(statusMaster!=null && (statusMaster.getStatusShortName().equals("widrw")|| statusMaster.getStatusShortName().equals("hird")||statusMaster.getStatusShortName().equals("rem")|| statusMaster.getStatusShortName().equals("dcln"))){
					noInsert=false;
				}
				//System.out.println(outerJob+" "+noInsert+" && ("+selectedstatus+"|| "+selectedSecondaryStatus+") && ("+jobWiseScoreFlag+" || "+finalizeStatusFlag);
				//System.out.println(outerJob+"==false && "+statusHistory+"==null && "+noInsert+" && ("+selectedstatus+"==false|| "+selectedSecondaryStatus+"==false) && ("+jobWiseScoreFlag+" || "+finalizeStatusFlag+")");
				if(outerJob==false && noInsert && (selectedstatus|| selectedSecondaryStatus) && (jobWiseScoreFlag || finalizeStatusFlag)){//Ramnnath  && flagforpanelAttendees
					boolean histCheck=false;
					try{

						TeacherStatusHistoryForJob statusHistoryObj=teacherStatusHistoryForJobDAO.findByTeacherStatusAndUserMaster(teacherDetail,jobOrder,statusMaster,secondaryStatus,userMaster);
						if(statusHistoryObj==null){
							histCheck=true;
							Calendar c = Calendar.getInstance();
							c.add(Calendar.SECOND,01);
							Date date = c.getTime();
							TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
							tSHJ.setTeacherDetail(teacherDetail);
							tSHJ.setJobOrder(jobOrder);
							if(statusId!=null && !statusId.equals("0")){
								tSHJ.setStatusMaster(statusMaster);
							}
							if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
								tSHJ.setSecondaryStatus(secondaryStatus);
							}
							if(statusWaived){
								tSHJ.setStatus("W");
							}else{
								tSHJ.setStatus("S");
							}
							tSHJ.setCreatedDateTime(date);
							tSHJ.setUserMaster(userMaster);
							if(chkOverridetSatus){
								tSHJ.setOverride(chkOverridetSatus);
								tSHJ.setOverrideBy(userMaster);
							}
							teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
						}

					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
						if(jobForTeacher!=null && secondaryStatusId!=null && !secondaryStatusId.equals("0")){
							if(!bPanel)
							{
								//System.out.println("::::::::interviewInvites::::::::1");
								lstTDetails.clear();lstTDetails.add(teacherDetail);
								jobForTeacher.setSecondaryStatus(secondaryStatus);
								jobForTeacher.setStatusMaster(null);
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
								jobForTeacher.setUpdatedDate(new Date());
								jobForTeacher.setLastActivity(secondaryStatus.getSecondaryStatusName());
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);
								jobForTeacherDAO.makePersistent(jobForTeacher);
							}
						}else{
							try{
								if(jobForTeacher!=null && histCheck && statusId!=null && !statusId.equals("0") && selectedstatus && flagforpanelAttendees ){//added 21-03-2015
									if(statusMaster!=null && (statusMaster.getStatusShortName().equalsIgnoreCase("scomp") || statusMaster.getStatusShortName().equalsIgnoreCase("ecomp") || statusMaster.getStatusShortName().equalsIgnoreCase("vcomp"))){
										jobForTeacher.setStatus(statusMaster);
										if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
											jobForTeacher.setStatusMaster(null);
										}else{
											jobForTeacher.setStatusMaster(statusMaster);
										}
										if(statusMaster!=null){
											jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
										}
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMaster.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);
										jobForTeacherDAO.makePersistent(jobForTeacher);
										//System.out.println(":::::::::::::::Interview Check :::::::::::");
										mailSend = true;
										
										lstTDetails.clear();lstTDetails.add(teacherDetail);
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(outerJob==false && statusHistory==null && noInsert && (selectedstatus==false|| selectedSecondaryStatus==false) && (jobWiseScoreFlag || finalizeStatusFlag)){
					try{
						System.out.println(" Status Check :::::::::::::::::::::: 8");
						TeacherStatusHistoryForJob statusHistoryObj=teacherStatusHistoryForJobDAO.findByTeacherStatusAndUserMaster(teacherDetail,jobOrder,statusMaster,secondaryStatus,userMaster);
						if(statusHistoryObj==null){
							Calendar c = Calendar.getInstance();
							c.add(Calendar.SECOND,01);
							Date date = c.getTime();
							TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
							tSHJ.setTeacherDetail(teacherDetail);
							tSHJ.setJobOrder(jobOrder);
							if(statusId!=null && !statusId.equals("0")){
								tSHJ.setStatusMaster(statusMaster);
							}
							if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
								tSHJ.setSecondaryStatus(secondaryStatus);
							}
							if(statusWaived){
								tSHJ.setStatus("W");
							}else{
								tSHJ.setStatus("S");
							}
							tSHJ.setCreatedDateTime(date);
							tSHJ.setUserMaster(userMaster);
							if(chkOverridetSatus){
								tSHJ.setOverride(chkOverridetSatus);
								tSHJ.setOverrideBy(userMaster);
							}
							teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
						}

						if(secondaryStatus!=null){
							if(jobForTeacherObj.getSecondaryStatus()!=null){
								if(cgService.selectedNotPriSecondaryStatusCheck(secondaryStatus,jobForTeacherObj.getSecondaryStatus())){
									try{
										JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
										if(jobForTeacher!=null && secondaryStatusId!=null && !secondaryStatusId.equals("0")){
											if(!bPanel)
											{
												lstTDetails.clear();lstTDetails.add(teacherDetail);
												jobForTeacher.setSecondaryStatus(secondaryStatus);
												jobForTeacher.setUpdatedBy(userMaster);
												jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
												jobForTeacher.setUpdatedDate(new Date());
												jobForTeacher.setLastActivity(secondaryStatus.getSecondaryStatusName());
												jobForTeacher.setLastActivityDate(new Date());
												jobForTeacher.setUserMaster(userMaster);
												jobForTeacherDAO.makePersistent(jobForTeacher);
												//System.out.println("::::::::interviewInvites::::::::2");
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
								}
							}else{
								try{
									JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
									if(jobForTeacher!=null && secondaryStatusId!=null && !secondaryStatusId.equals("0")){
										if(!bPanel)
										{
											lstTDetails.clear();lstTDetails.add(teacherDetail);
											jobForTeacher.setSecondaryStatus(secondaryStatus);
											jobForTeacher.setUpdatedBy(userMaster);
											jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
											jobForTeacher.setUpdatedDate(new Date());
											jobForTeacher.setLastActivity(secondaryStatus.getSecondaryStatusName());
											jobForTeacher.setLastActivityDate(new Date());
											jobForTeacher.setUserMaster(userMaster);
											jobForTeacherDAO.makePersistent(jobForTeacher);
											//System.out.println("::::::::interviewInvites::::::::3");
										}
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}

				if(inputSumAnswerScore>0){
					scoreProvided=""+inputSumAnswerScore;
					fitScore=""+inputSumAnswerMaxScore;
				}

				if(outerJob==false && jobWiseScoreFlag ){
						try{
						Double cScore=0.0;
						Double maxScore=0.0;
						JobWiseConsolidatedTeacherScore jwScoreObj=null;
						jwScoreObj=jobWiseConsolidatedTeacherScoreDAO.getJWCTScore(teacherDetail, jobOrder);
						if(jwScoreObj==null){
							cScore=Double.parseDouble(scoreProvided);
							maxScore=Double.parseDouble(fitScore);
						}else{
							if(teacherStatusScoresList.size()>1){
								try{
									Double[] teacherStatusScoresAvg= teacherStatusScoresDAO.getAllFinalizeStatusScoreAvg(teacherDetail,jobOrder);
									cScore=teacherStatusScoresAvg[0];
									maxScore=Double.parseDouble(teacherStatusScoresAvg[1]+"");
								}catch(Exception e){ e.printStackTrace();}
							}else{
								cScore=Double.parseDouble(scoreProvided)+jwScoreObj.getJobWiseConsolidatedScore();	
								maxScore=Double.parseDouble(fitScore)+jwScoreObj.getJobWiseMaxScore();
							}
						}
						JobWiseConsolidatedTeacherScore jWScore= new JobWiseConsolidatedTeacherScore();
						if(jwScoreObj!=null){
							jWScore=jwScoreObj;
						}
						jWScore.setTeacherDetail(teacherDetail);
						jWScore.setJobOrder(jobOrder);
						jWScore.setJobWiseConsolidatedScore(cScore);
						if(maxScore!=0){
							jWScore.setJobWiseMaxScore(maxScore);
						}
						jobWiseConsolidatedTeacherScoreDAO.makePersistent(jWScore);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				List<SchoolMaster> lstSchoolMasters = null;

				if(statusUpdateHDR==null && (jobWiseScoreFlag || finalizeStatusFlag)){
					errorFlag="3";
				}
				List<TeacherStatusNotes> statusNoteList=new ArrayList<TeacherStatusNotes>();
				try{
					statusNoteList=teacherStatusNotesDAO.getStatusNoteAllList(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId());
				}catch(Exception e){
					e.printStackTrace();
				}

				/******* Update Status *******/
				if(finalizeStatus!=null)
					if(outerJob==false && statusMaster!=null && statusMaster.getStatusShortName().equals("hird") && finalizeStatus.equals("2")){
						TeacherStatusHistoryForJob lastSelectedObj=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryLastSelected(teacherDetail,jobOrder);
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
						canUnhire=isUpdateStatus;

						if(canUnhire){
							int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
							statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);

							if(lastSelectedObj!=null && !lastSelectedObj.getStatusMaster().getStatusShortName().equals("hird")){
								statusMaster=lastSelectedObj.getStatusMaster();
							}
							if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
								try{
									if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null){
										List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobForTeacher.getJobId(),jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
										if(jobReqList.size()>0){
											boolean updateFlag=false;
											for (JobRequisitionNumbers jobReqObj : jobReqList) {
												try{
													if(jobForTeacher.getSchoolMaster()!=null && jobReqObj.getSchoolMaster().getSchoolId().equals(jobForTeacher.getSchoolMaster().getSchoolId()) && jobReqObj.getStatus()==1){
														updateFlag=true;
														jobReqObj.setStatus(0);
														jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
														break;
													}
												}catch(Exception e){
													e.printStackTrace();
												}
											}
											if(updateFlag==false){
												for (JobRequisitionNumbers jobReqObj : jobReqList){
													try{
														if(jobReqObj.getStatus()==1){
															updateFlag=true;
															jobReqObj.setStatus(0);
															jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
															break;
														}
													}catch(Exception e){
														e.printStackTrace();
													}
												}
											}
										}
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								errorFlag="3";
								jobForTeacher.setStatus(statusMaster);
								if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
									jobForTeacher.setStatusMaster(null);
								}else{
									jobForTeacher.setStatusMaster(statusMaster);
								}
								if(statusMaster!=null){
									jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
								}
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());	
								jobForTeacher.setLastActivity("UnHired");
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);
								jobForTeacher.setUpdatedDate(new Date());
								jobForTeacher.setSchoolMaster(null);
								jobForTeacher.setRequisitionNumber(null);
								jobForTeacherDAO.makePersistent(jobForTeacher);
								//System.out.println("W 04");

								try{
									TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
									StatusMaster statusMasterObj  = WorkThreadServlet.statusMap.get("hird");
									tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterObj,"A");
									tSHJ.setJobOrder(jobOrder);
									if(tSHJ!=null){
										tSHJ.setStatus("I");
										tSHJ.setHiredByDate(null);
										tSHJ.setUpdatedDateTime(new Date());
										tSHJ.setUserMaster(userMaster);
										teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}else{
								canUnhire=true;
							}
						}
						if(canUnhire==false){
							errorFlag="4";
						}
					}else if(outerJob==false && isUpdateStatus && statusMaster!=null && statusMaster.getStatusShortName().equals("rem") && finalizeStatus.equals("4")){
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
						TeacherStatusHistoryForJob lastSelectedObj=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryLastSelected(teacherDetail,jobOrder);

						int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
						statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);

						if(lastSelectedObj!=null){
							statusMaster=lastSelectedObj.getStatusMaster();
						}
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
							errorFlag="3";
							try{
								TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
								StatusMaster statusMasterObj  = WorkThreadServlet.statusMap.get("rem");
								tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterObj,"A");
								if(tSHJ!=null){
									tSHJ.setStatus("I");
									tSHJ.setUpdatedDateTime(new Date());
									tSHJ.setUserMaster(userMaster);
									teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							jobForTeacher.setStatus(statusMaster);
							if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
								jobForTeacher.setStatusMaster(null);
							}else{
								jobForTeacher.setStatusMaster(statusMaster);
							}
							if(statusMaster!=null){
								jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
							}
							jobForTeacher.setUpdatedBy(userMaster);
							jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
							jobForTeacher.setUpdatedDate(new Date());
							jobForTeacher.setLastActivity("UnRejected");
							jobForTeacher.setLastActivityDate(new Date());
							jobForTeacher.setUserMaster(userMaster);
							jobForTeacherDAO.makePersistent(jobForTeacher);
						}
					}else if(outerJob==false && isUpdateStatus && statusMaster!=null && statusMaster.getStatusShortName().equals("widrw") && finalizeStatus.equals("8")){
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
						TeacherStatusHistoryForJob lastSelectedObj=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryLastSelected(teacherDetail,jobOrder);

						int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
						statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);

						if(lastSelectedObj!=null){
							statusMaster=lastSelectedObj.getStatusMaster();
						}
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
							errorFlag="3";
							try{
								TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
								StatusMaster statusMasterObj  = WorkThreadServlet.statusMap.get("widrw");
								tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterObj,"A");
								if(tSHJ!=null){
									tSHJ.setStatus("I");
									tSHJ.setUpdatedDateTime(new Date());
									tSHJ.setUserMaster(userMaster);
									teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							jobForTeacher.setStatus(statusMaster);
							if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
								jobForTeacher.setStatusMaster(null);
							}else{
								jobForTeacher.setStatusMaster(statusMaster);
							}
							if(statusMaster!=null){
								jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
							}
							jobForTeacher.setUpdatedBy(userMaster);
							jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
							jobForTeacher.setUpdatedDate(new Date());
							jobForTeacher.setLastActivity("UnWithdrew");
							jobForTeacher.setLastActivityDate(new Date());
							jobForTeacher.setUserMaster(userMaster);
							jobForTeacherDAO.makePersistent(jobForTeacher);
						}
					}else if(outerJob==false && isUpdateStatus && statusMaster!=null && statusMaster.getStatusShortName().equals("dcln") && finalizeStatus.equals("3")){
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
						TeacherStatusHistoryForJob lastSelectedObj=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryLastSelected(teacherDetail,jobOrder);
						int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
						statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);

						if(lastSelectedObj!=null){
							statusMaster=lastSelectedObj.getStatusMaster();
						}
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
							errorFlag="3";
							try{
								TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
								StatusMaster statusMasterObj  = WorkThreadServlet.statusMap.get("dcln");
								tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterObj,"A");
								if(tSHJ!=null){
									tSHJ.setStatus("I");
									tSHJ.setHiredByDate(null);
									tSHJ.setUpdatedDateTime(new Date());
									tSHJ.setUserMaster(userMaster);
									teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							jobForTeacher.setStatus(statusMaster);
							if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
								jobForTeacher.setStatusMaster(null);
							}else{
								jobForTeacher.setStatusMaster(statusMaster);
							}
							if(statusMaster!=null){
								jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
							}
							jobForTeacher.setUpdatedBy(userMaster);
							jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
							jobForTeacher.setUpdatedDate(new Date());
							jobForTeacher.setLastActivity("UnDecline");
							jobForTeacher.setLastActivityDate(new Date());
							jobForTeacher.setOfferAccepted(null);
							jobForTeacher.setOfferAcceptedDate(null);
							jobForTeacher.setUserMaster(userMaster);
							jobForTeacherDAO.makePersistent(jobForTeacher);

						}
					}else if(outerJob==false && isUpdateStatus && finalizeStatus.equals("5")){ /*********Undo Status ************/
						//System.out.println("::::::::::::::::=Undo status:::::::::::");
						StatusMaster masterForDPoint=null;
						try{
							if(statusMaster!=null && (statusMaster.getStatusShortName().equalsIgnoreCase("scomp") || statusMaster.getStatusShortName().equalsIgnoreCase("ecomp") || statusMaster.getStatusShortName().equalsIgnoreCase("vcomp"))){
								masterForDPoint=statusMaster;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
						errorFlag="3";
						SchoolMaster panelSchool=null;
						boolean panelFlag=false;

						try{
							JobWisePanelStatus jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId());
							if(jobWisePanelStatus!=null){
								if(secondaryStatusForPanel!=null && secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready")){ 
									PanelSchedule panelSchedule=panelScheduleDAO.findPanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
									if(panelSchedule!=null){
										List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
										if(panelAttendeesList.size()>0){
											for(PanelAttendees panelAttendees :panelAttendeesList){
												if(panelAttendees.getPanelInviteeId()!=null){
													UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
													if(panelUserMaster.getEntityType()==3){
														panelSchool=panelUserMaster.getSchoolId();
														panelFlag=true;
													}
												}
											}
										}
										candidateGridSubAjax.cancelPanelEvent(panelSchedule.getPanelId());
									}
								}
							}else{
								//System.out.println(":::::::::::::Else::::::::::::");
								List<JobWisePanelStatus> jobWisePanelStatusWiseList = jobWisePanelStatusDAO.getPanel(jobForTeacher.getJobId(),secondaryStatus,masterForDPoint);
								//System.out.println("::::::::::::::::::::jobWisePanelStatusWiseList:::::::::"+jobWisePanelStatusWiseList.size());
								List <PanelSchedule> panelScheduleStatusList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWisePanelStatusWiseList,jobForTeacher.getTeacherId());
								//System.out.println("::::::::::::::::::::panelScheduleStatusList:::::::::"+panelScheduleStatusList.size());
								try{
									if(panelScheduleStatusList.size()==1){
										if(panelScheduleStatusList.get(0)!=null){
											List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelScheduleStatusList.get(0));
											//System.out.println("panelAttendeesList::::::"+panelAttendeesList.size());
											if(panelAttendeesList.size()>0){
												for(PanelAttendees panelAttendees :panelAttendeesList){
													if(panelAttendees.getPanelInviteeId()!=null){
														UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
														if(panelUserMaster.getUserId().equals(userMaster.getUserId())){
															panelFlag=true;
														}
													}
												}
											}
										}

									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}catch(Exception e){e.printStackTrace();}

						//System.out.println(panelFlag+":::panelFlag panelSchool::::::::"+panelSchool);

						List<TeacherStatusScores> teacherStatusScoresForUndoList= teacherStatusScoresDAO.getFinalizeStatusScore(teacherDetail,jobOrder,masterForDPoint,secondaryStatus);
						//System.out.println("teacherStatusScoresForUndoList:::::::::"+teacherStatusScoresForUndoList.size());
						try{
							if(teacherStatusScoresForUndoList!=null && teacherStatusScoresForUndoList.size()>0){
								for (TeacherStatusScores teacherStatusScores : teacherStatusScoresForUndoList) {
									teacherStatusScoresDAO.makeTransient(teacherStatusScores);
								}
								JobWiseConsolidatedTeacherScore jwScoreObj=jobWiseConsolidatedTeacherScoreDAO.getJWCTScore(teacherDetail, jobOrder);
								if(jwScoreObj!=null){
									try{
										Double[] teacherStatusScoresAvg= teacherStatusScoresDAO.getAllFinalizeStatusScoreAvg(teacherDetail,jobOrder);
										jwScoreObj.setJobWiseConsolidatedScore(teacherStatusScoresAvg[0]);
										jwScoreObj.setJobWiseMaxScore(Double.parseDouble(teacherStatusScoresAvg[1]+""));
									}catch(Exception e){ e.printStackTrace();}
									jobWiseConsolidatedTeacherScoreDAO.updatePersistent(jwScoreObj);
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						if(panelFlag){
							try{
								List<TeacherStatusNotes> statusNoteUndoList=new ArrayList<TeacherStatusNotes>();
								try{
									statusNoteUndoList=teacherStatusNotesDAO.getFinalizeStatusScore(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId(),masterForDPoint,secondaryStatus);
								}catch(Exception e){
									e.printStackTrace();
								}
								//System.out.println("statusNoteUndoList:::::::::"+statusNoteUndoList.size());
								if(statusNoteUndoList.size()>0)
									for (TeacherStatusNotes teacherStatusNotes : statusNoteUndoList) {
										teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
									}
								jobForTeacher.setOfferReady(null);
								jobForTeacher.setOfferMadeDate(null);
								jobForTeacher.setRequisitionNumber(null);
								jobForTeacher.setOfferAcceptedDate(null);
								jobForTeacher.setOfferAccepted(null);
							}catch(Exception e){
								e.printStackTrace();
							}
							try{
								if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null){
									List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobForTeacher.getJobId(),jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
									if(jobReqList.size()>0){
										for (JobRequisitionNumbers jobReqObj : jobReqList) {
											try{
												if(panelSchool!=null && jobReqObj.getSchoolMaster().getSchoolId().equals(panelSchool.getSchoolId()) && jobReqObj.getStatus()==1){
													jobReqObj.setStatus(0);
													jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
													break;
												}
											}catch(Exception e){
												e.printStackTrace();
											}
										}
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
						try{
							List<TeacherStatusHistoryForJob>  statusHistoryObjList=teacherStatusHistoryForJobDAO.findByTeacherStatusForUndo(teacherDetail,jobOrder,masterForDPoint,secondaryStatus);
							//System.out.println("statusHistoryObjList::::::::"+statusHistoryObjList.size());
							for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : statusHistoryObjList) {
								teacherStatusHistoryForJob.setStatus("I");
								teacherStatusHistoryForJob.setHiredByDate(null);
								teacherStatusHistoryForJob.setUpdatedDateTime(new Date());
								teacherStatusHistoryForJob.setUserMaster(userMaster);
								teacherStatusHistoryForJobDAO.updatePersistent(teacherStatusHistoryForJob);
							}

						}catch(Exception e){
							e.printStackTrace();
						}
						commonService.undoJobStatus(jobForTeacher);
					}
				/*********End Update Status ************/
				if(outerJob==false && statusUpdateHDR==null && isUpdateStatus && selectedstatus)
					if(statusMaster!=null && statusMaster.getStatusShortName().equals("hird") && (jobWiseScoreFlag || finalizeStatusFlag)){
						int noOfHire = 0;
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
							if(userMaster.getEntityType()==2){//District - 2 user login type
								noOfHire = (jobForTeacherDAO.findHireByJobForDistrict(jobOrder, userMaster.getDistrictId())).size();
								canHire = isUpdateStatus;;
								if(jobOrder.getWritePrivilegeToSchool()!=null && jobOrder.getWritePrivilegeToSchool()){
									List<SchoolMaster> schoolMasterList=new ArrayList<SchoolMaster>();
									schoolMasterList = jobOrder.getSchool();
									if(schoolMasterList.size()>0){
										try{
											SchoolMaster schoolMasterForJFT=null;
											if(txtschoolCount > 1){
												if(schoolIdInputFromUser!=null && schoolIdInputFromUser!=0){
													schoolMasterForJFT=schoolMasterDAO.findById(schoolIdInputFromUser, false, false);
													noOfHire = (jobForTeacherDAO.findHireBySchool(jobOrder, schoolMasterForJFT)).size();
													schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMasterForJFT);
												}
											}else if(jobOrder.getCreatedForEntity()==2 && jobOrder.getNoSchoolAttach()==2){
												schoolMasterForJFT=jobOrder.getSchool().get(0);
												noOfHire = (jobForTeacherDAO.findHireBySchool(jobOrder, schoolMasterForJFT)).size();
												schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMasterForJFT);
											}
										}catch(Exception e){
											e.printStackTrace();
										}
									}
									if(schoolInJobOrder==null || schoolInJobOrder.getNoOfSchoolExpHires()==null || noOfHire>=schoolInJobOrder.getNoOfSchoolExpHires())
									{	
										canHire = false;
									}
								}else{
									if(jobOrder.getNoOfExpHires()!=null && jobOrder.getNoOfExpHires()<=noOfHire){	
										canHire = false;
									}
								}
							}else{
								if(isUpdateStatus){
									if(userMaster.getSchoolId()!=null){
										schoolMaster = userMaster.getSchoolId();				
										statusMaster= WorkThreadServlet.statusMap.get("hird");
										noOfHire = (jobForTeacherDAO.findHireBySchool(jobOrder, schoolMaster)).size();
										lstSchoolMasters = jobOrder.getSchool();
	
										schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMaster);
	
										if(schoolInJobOrder==null || schoolInJobOrder.getNoOfSchoolExpHires()==null || noOfHire>=schoolInJobOrder.getNoOfSchoolExpHires())
										{	
											canHire = false;
										}
									}
								}else{
									canHire = false;
								}
							}
							if(canHire){
								//Start ... update for hiredBySchool
								SchoolMaster schoolMasterForJFT=null;
								try{
									if(txtschoolCount > 1){
										if(schoolIdInputFromUser!=null && schoolIdInputFromUser!=0)
											schoolMasterForJFT=schoolMasterDAO.findById(schoolIdInputFromUser, false, false);
									}else{
										if(jobOrder.getCreatedForEntity()==2 && jobOrder.getNoSchoolAttach()==2){
											schoolMasterForJFT=jobOrder.getSchool().get(0);
										}else{
											if(schoolMaster!=null){
												schoolMasterForJFT=schoolMaster;
											}
										}
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								//End ... update for hiredBySchool
								String requisitionNumber=null;
								if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
									try{
										if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
											JobRequisitionNumbers jRNOb=jobRequisitionNumbersDAO.findById(Integer.parseInt(requisitionNumberId),false,false);
											if(jRNOb!=null){
												requisitionNumber=jRNOb.getDistrictRequisitionNumbers().getRequisitionNumber();
												List<JobRequisitionNumbers> jobReqNumbers=jobRequisitionNumbersDAO.findJobReqNumbers(jRNOb.getDistrictRequisitionNumbers());
												if(jobReqNumbers.size()==1){
													boolean multiHired=false;
													try{
														if(jRNOb.getJobOrder().getIsExpHireNotEqualToReqNo()){
															multiHired=true;
														}	
													}catch(Exception e){
														e.printStackTrace();
													}
													if(multiHired==false){
														jRNOb.setStatus(1);
														jobRequisitionNumbersDAO.updatePersistent(jRNOb);
													}
												}
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
								}
								////System.out.println("requisitionNumber::::"+requisitionNumber);
								//System.out.println(":::::::::::::::::setHiredDate::::::::::::::::::"+setHiredDate);

								errorFlag="3";
								mailSend=true;
								statusShortName=statusMaster.getStatusShortName();
								jobForTeacher.setStatus(statusMaster);
								jobForTeacher.setStatusMaster(statusMaster);
								if(statusMaster!=null){
									jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
								}
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
								jobForTeacher.setUpdatedDate(new Date());
								jobForTeacher.setLastActivity("Hired");
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);
								if(requisitionNumber!=null){
									jobForTeacher.setRequisitionNumber(requisitionNumber);
								}
								if(schoolMasterForJFT!=null)
									jobForTeacher.setSchoolMaster(schoolMasterForJFT);
								jobForTeacherDAO.makePersistent(jobForTeacher);
								//System.out.println("::::::::interviewInvites::::::::4");

								lstTDetails.clear();lstTDetails.add(teacherDetail);
								int live=Integer.parseInt(Utility.getValueOfPropByKey("isAPILive"));
								if(districtMaster!=null &&  districtMaster.getDistrictId().equals(3628590) && live==1)
								{
									//// Vishwanath (For Hired Status SCSD candidates)
									try {
										TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(jobForTeacher.getTeacherId().getTeacherId(), false, false);

										List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>();
										String requisitionNumber1 = jobForTeacher.getRequisitionNumber();
										
										Criterion criteria5 = Restrictions.eq("requisitionNumber", requisitionNumber1);
										lstDistrictRequisitionNumbers = districtRequisitionNumbersDAO.findByCriteria(criteria5);
										
										//System.out.println("lstDistrictRequisitionNumbers**inside ManageStatusAjax*******************: "+lstDistrictRequisitionNumbers.size());
										
										String postingNo = "";
										if(lstDistrictRequisitionNumbers.size()>0)
										{
											postingNo = lstDistrictRequisitionNumbers.get(0).getPostingNo();
										}
										
										List<RaceMaster> lstRace= null;
										lstRace = raceMasterDAO.findAllRaceByOrder();
										Map<String,RaceMaster> raceMap = new HashMap<String, RaceMaster>();
										for (RaceMaster raceMaster : lstRace) {
											raceMap.put(""+raceMaster.getRaceId(), raceMaster);
										}

										if(teacherPersonalInfo!=null)
										{
											PATSHiredPostThread phpt = new PATSHiredPostThread();
											phpt.setRaceMap(raceMap);
											phpt.setJobForTeacher(jobForTeacher);
											phpt.setTeacherPersonalInfo(teacherPersonalInfo);
											phpt.setPostingNo(postingNo);
											//TMCommonUtil.updateSDSCHiredCandidate(jobForTeacher, teacherPersonalInfo, raceMap);
											phpt.start();
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}
						}else{
							canHire=true;
						}
						if(canHire==false){
							errorFlag="2";
						}
					}else if(outerJob==false && selectedstatus && statusMaster!=null && statusMaster.getStatusShortName().equals("rem") && (jobWiseScoreFlag || finalizeStatusFlag)){
						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){

							boolean staffer=false;
							SchoolMaster panelSchool=null;
							if(isMiami || isPhiladelphia){ //added by 21-03-2015
								try{
									JobWisePanelStatus jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId());
									if(jobWisePanelStatus!=null){
										//System.out.println("jobWisePanelStatus:::"+jobWisePanelStatus.getJobPanelStatusId());
										PanelSchedule panelSchedule=panelScheduleDAO.findPanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
										if(panelSchedule!=null){
											List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
											if(panelAttendeesList.size()>0)
												for(PanelAttendees panelAttendees :panelAttendeesList){
													if(panelAttendees.getPanelInviteeId()!=null){
														UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
														if(panelUserMaster.getEntityType()==3){
															panelSchool=panelUserMaster.getSchoolId();
														}
														if(panelUserMaster.getUserId().equals(userMaster.getUserId())){
															staffer=true;
														}
													}
												}
										}
									}
								}catch(Exception e){e.printStackTrace();}
							}
							////////////////////////////////////////////////////////////////////////////////
							try{
								Map<Integer,List<String>> statusHistoryMap=new HashMap<Integer, List<String>>();
								Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
								Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
								List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId());

								List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
								LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
								Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
								if(staffer){
									try{
										int counter=0; 
										for(SecondaryStatus tree: lstTreeStructure)
										{
											if(tree.getSecondaryStatus()==null)
											{
												if(tree.getChildren().size()>0){
													counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
												}
											}
										}
									}catch(Exception e){}

									if(statusNoteList.size()>0)
										for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
											if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready"))){
												teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
											}
										}
									boolean vComp=false;
									if(historyList.size()>0)
										for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
											if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready"))){
												if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp")){
													vComp=true;	
												}
												teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
											}

										}

									try{		
										historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId());
										//System.out.println("historyList::::::::::"+historyList.size());

										List<String> statusList=new ArrayList<String>();
										if(historyList.size()>0)
											for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
												if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
													statusList=new ArrayList<String>();
													if(teacherStatusHistoryForJob.getStatusMaster()!=null){
														statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
													}else{
														statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
													}
													statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
												}else{
													statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
													if(teacherStatusHistoryForJob.getStatusMaster()!=null){
														statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
													}else{
														statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
													}
													statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
												}
											}
									}catch(Exception e){}
									//System.out.println("statusHistoryMap:::::::::::"+statusHistoryMap.size());
									List<String> statusIds=statusHistoryMap.get(jobForTeacherObj.getTeacherId().getTeacherId());
									String schoolStatusId=cgService.getSchoolStatusId(statusIds, statusHistMap, statusNameMap);
									SecondaryStatus secondaryStatusObj=null;
									StatusMaster statusMasterObj=null;
									try{
										String statusIdMapValue[]=schoolStatusId.split("#");
										if(statusIdMapValue[0].equals("0")){
											secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
											jobForTeacher.setSecondaryStatus(secondaryStatusObj);
											if(vComp){
												jobForTeacher.setStatus(findStatusByShortName(lstStatusMaster,"ecomp"));
												jobForTeacher.setStatusMaster(null);
											}
										}else{
											statusMasterObj=statusMasterMap.get(schoolStatusId);
										}
									}catch(Exception e){e.printStackTrace();}

									try{
										if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null){
											List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobForTeacher.getJobId(),jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
											if(jobReqList.size()>0){
												for (JobRequisitionNumbers jobReqObj : jobReqList) {
													try{
														if(panelSchool!=null && jobReqObj.getSchoolMaster().getSchoolId().equals(panelSchool.getSchoolId()) && jobReqObj.getStatus()==1){
															jobReqObj.setStatus(0);
															jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
															break;
														}
													}catch(Exception e){
														e.printStackTrace();
													}
												}
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									jobForTeacher.setOfferReady(null);
									jobForTeacher.setOfferMadeDate(new Date());
									jobForTeacher.setRequisitionNumber(null);
									jobForTeacher.setOfferAcceptedDate(new Date());
									jobForTeacher.setOfferAccepted(null);
									jobForTeacherDAO.updatePersistent(jobForTeacher);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							if(entityType==3 && (isMiami || isPhiladelphia)){ //added by 21-03-2015
								try{
									//System.out.println("<<<<<<<<<::::::::::::: Inside School Reject :::::::::::::::>>>>>>>>>>>");
									SchoolWiseCandidateStatus swcsObj= new SchoolWiseCandidateStatus();
									swcsObj.setStatusMaster(statusMaster);
									swcsObj.setJobOrder(jobForTeacher.getJobId());
									swcsObj.setDistrictId(userMaster.getDistrictId().getDistrictId());
									swcsObj.setSchoolId(userSchoolMaster.getSchoolId());
									swcsObj.setUserMaster(userMaster);		
									swcsObj.setCreatedDateTime(new Date());
									swcsObj.setTeacherDetail(jobForTeacher.getTeacherId());
									swcsObj.setStatus("A");
									swcsObj.setJobForTeacher(jobForTeacher);
									schoolWiseCandidateStatusDAO.makePersistent(swcsObj);
									errorFlag="3";
									mailSend=true;
									statusShortName=statusMaster.getStatusShortName();
								}catch(Exception e){e.printStackTrace();}
							}else{
								//////////////Add Zone Wise Rejected By Sekhar////////////
								if(entityType==2 && staffer==false && (isMiami)){
									//System.out.println("::::::::::::::::::::::::::::No Staffer Rejected:::::::::::");
									try{
										String jobTitle=jobOrder.getJobTitle();
										if(jobTitle.indexOf("(")>0){
											jobTitle=jobTitle.substring(0,jobTitle.lastIndexOf("(")).trim();
										}
								
										List<JobForTeacher> jobForTeachers= jobForTeacherDAO.findJobByTeacherAndJobTitle(teacherDetail,districtMaster,jobCategoryMaster,jobForTeacherId,jobTitle);

										List<JobOrder> joblist=new ArrayList<JobOrder>();
										List<TeacherDetail> teacherList =new ArrayList<TeacherDetail>();
										try{
											for (JobForTeacher jobObj : jobForTeachers) {
												joblist.add(jobObj.getJobId());
												teacherList.add(jobObj.getTeacherId());
											}
										}catch(Exception e){
											e.printStackTrace();
										}
										List<TeacherStatusHistoryForJob> forJobs=teacherStatusHistoryForJobDAO.findByTeachersAndJobsStatus(teacherList, joblist);

										Map<String,TeacherStatusHistoryForJob> mapTSHFJ=new HashMap<String, TeacherStatusHistoryForJob>();
										if(forJobs!=null && forJobs.size() > 0)
										{
											for(TeacherStatusHistoryForJob tSHObj : forJobs){
												mapTSHFJ.put(tSHObj.getTeacherDetail().getTeacherId()+"#"+tSHObj.getJobOrder().getJobId(),tSHObj);
											}
										}
										TeacherStatusHistoryForJob teacherHistoryJobs=teacherStatusHistoryForJobDAO.findByTeacherStatusHistory(teacherDetail, jobOrder, statusMasterPanel, secondaryStatusPanel);
										TeacherStatusNotes noteObj=teacherStatusNotesDAO.getFinalizeNotesByOrder(teacherDetail, jobOrder, statusMaster, secondaryStatus, userMaster);
										for (JobForTeacher jobForObj : jobForTeachers) {
											TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
											String sTID_JID=jobForObj.getTeacherId().getTeacherId()+"#"+jobForObj.getJobId().getJobId();
											try{
												if(mapTSHFJ.get(sTID_JID)!=null){
													teacherStatusHistoryForJob=mapTSHFJ.get(sTID_JID);
												}
												if(teacherStatusHistoryForJob!=null)
												{
													teacherStatusHistoryForJob.setStatus("I");
													teacherStatusHistoryForJob.setHiredByDate(null);
													teacherStatusHistoryForJob.setUpdatedDateTime(new Date());
													teacherStatusHistoryForJob.setUserMaster(userMaster);
													teacherStatusHistoryForJobDAO.makePersistent(teacherStatusHistoryForJob);
												}
											}catch(Exception e){
												e.printStackTrace();
											}
											if(teacherHistoryJobs!=null){
												try{
													if(teacherHistoryJobs!=null){
														TeacherStatusHistoryForJob forJob=new TeacherStatusHistoryForJob();
														forJob.setTeacherDetail(teacherHistoryJobs.getTeacherDetail());
														forJob.setJobOrder(jobForObj.getJobId());
														if(teacherHistoryJobs.getStatusMaster()!=null)
															forJob.setStatusMaster(teacherHistoryJobs.getStatusMaster());
														if(teacherHistoryJobs.getSecondaryStatus()!=null){
															forJob.setSecondaryStatus(teacherHistoryJobs.getSecondaryStatus());
														}
														forJob.setStatus(teacherHistoryJobs.getStatus());
														forJob.setUserMaster(teacherHistoryJobs.getUserMaster());
														forJob.setCreatedDateTime(teacherHistoryJobs.getCreatedDateTime());
														teacherStatusHistoryForJobDAO.makePersistent(forJob);
													}
												}catch(Exception e){
													e.printStackTrace();
												}
											}else{
												try{
													TeacherStatusHistoryForJob forJob=new TeacherStatusHistoryForJob();
													forJob.setTeacherDetail(jobForObj.getTeacherId());
													forJob.setJobOrder(jobForObj.getJobId());
													forJob.setStatusMaster(statusMaster);
													forJob.setStatus("A");
													if(statusMaster.getStatusShortName().equals("hird")){
														try{
															forJob.setHiredByDate(Utility.getCurrentDateFormart(setHiredDate));
														}catch(Exception e){
															e.printStackTrace();
														}

													}

													forJob.setUserMaster(userMaster);
													forJob.setCreatedDateTime(new Date());
													teacherStatusHistoryForJobDAO.makePersistent(forJob);
												}catch(Exception e){
													e.printStackTrace();
												}	
											}
											if(noteObj!=null){
												try{
													TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
													teacherStatusNoteObj.setTeacherDetail(noteObj.getTeacherDetail());
													teacherStatusNoteObj.setJobOrder(jobForObj.getJobId());
													if(noteObj.getStatusMaster()!=null)
														teacherStatusNoteObj.setStatusMaster(noteObj.getStatusMaster());
													if(noteObj.getSecondaryStatus()!=null){
														teacherStatusNoteObj.setSecondaryStatus(noteObj.getSecondaryStatus());
													}
													teacherStatusNoteObj.setUserMaster(noteObj.getUserMaster());
													teacherStatusNoteObj.setDistrictId(districtMaster.getDistrictId());
													teacherStatusNoteObj.setStatusNotes(noteObj.getStatusNotes());
													teacherStatusNoteObj.setStatusNoteFileName(noteObj.getStatusNoteFileName());
													teacherStatusNoteObj.setEmailSentTo(noteObj.getEmailSentTo());
													teacherStatusNoteObj.setFinalizeStatus(noteObj.isFinalizeStatus());
													teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
													//System.out.println(teacherStatusNoteObj.getTeacherStatusNoteId()+":::::::::AfterInsert Note::::::::"+noteObj.getTeacherStatusNoteId());
												}catch(Exception ee){
													ee.printStackTrace();
												}
											}else{
												try{
													TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
													teacherStatusNoteObj.setTeacherDetail(jobForObj.getTeacherId());
													teacherStatusNoteObj.setJobOrder(jobForObj.getJobId());
													teacherStatusNoteObj.setStatusMaster(statusMaster);
													teacherStatusNoteObj.setUserMaster(userMaster);
													teacherStatusNoteObj.setDistrictId(districtMaster.getDistrictId());
													teacherStatusNoteObj.setStatusNotes("Rejected");
													teacherStatusNoteObj.setEmailSentTo(1);
													teacherStatusNoteObj.setFinalizeStatus(true);
													teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
												}catch(Exception ee){
													ee.printStackTrace();
												}
											}
											jobForObj.setOfferReady(null);
											jobForObj.setOfferAccepted(null);
											jobForObj.setRequisitionNumber(null);
											jobForObj.setStatus(statusMaster);
											jobForObj.setStatusMaster(statusMaster);
											jobForObj.setUpdatedBy(userMaster);
											jobForObj.setUpdatedByEntity(userMaster.getEntityType());		
											jobForObj.setUpdatedDate(new Date());
											jobForObj.setLastActivity("Rejected");
											jobForObj.setLastActivityDate(new Date());
											jobForObj.setUserMaster(userMaster);
											jobForTeacherDAO.makePersistent(jobForObj);
										}
										jobForTeacher.setStatus(statusMaster);
										jobForTeacher.setStatusMaster(statusMaster);
										if(statusMaster!=null){
											jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
										}
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity("Rejected");
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);
										jobForTeacherDAO.makePersistent(jobForTeacher);
										errorFlag="3";
										mailSend=true;
										statusShortName=statusMaster.getStatusShortName();
										lstTDetails.clear();lstTDetails.add(teacherDetail);
										//System.out.println("::::::::interviewInvites::::::::5");
									}catch(Exception e){
										e.printStackTrace();
									}
								}else{ /////////End Zone Wise Rejected/////////////////////
									//System.out.println("::::::::Rejected Else::::::::::::::::::::");
									jobForTeacher.setOfferReady(null);
									jobForTeacher.setOfferAccepted(null);
									jobForTeacher.setRequisitionNumber(null);
									jobForTeacher.setStatus(statusMaster);
									jobForTeacher.setStatusMaster(statusMaster);
									if(statusMaster!=null){
										jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
									}
									jobForTeacher.setUpdatedBy(userMaster);
									jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
									jobForTeacher.setUpdatedDate(new Date());
									jobForTeacher.setLastActivity("Rejected");
									jobForTeacher.setLastActivityDate(new Date());
									jobForTeacher.setUserMaster(userMaster);
									jobForTeacherDAO.makePersistent(jobForTeacher);
									errorFlag="3";
									mailSend=true;
									statusShortName=statusMaster.getStatusShortName();
									lstTDetails.clear();lstTDetails.add(teacherDetail);
								}
							}
						}
					}else if(outerJob==false && selectedstatus && statusMaster!=null && statusMaster.getStatusShortName().equals("widrw") && (jobWiseScoreFlag || finalizeStatusFlag)){

						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
								//System.out.println("Else::::::::::::::::::::");
								jobForTeacher.setStatus(statusMaster);
								jobForTeacher.setStatusMaster(statusMaster);
								if(statusMaster!=null){
									jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
								}
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
								jobForTeacher.setUpdatedDate(new Date());
								jobForTeacher.setLastActivity("Withdrew");
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);
								jobForTeacherDAO.makePersistent(jobForTeacher);
								errorFlag="3";
								mailSend=true;
								statusShortName="soth";
								lstTDetails.clear();lstTDetails.add(teacherDetail);
								//System.out.println("::::::::interviewInvites::::::::7");
						}////////sekhar
					}else if(outerJob==false && selectedstatus && statusMaster!=null && statusMaster.getStatusShortName().equals("dcln") && (jobWiseScoreFlag || finalizeStatusFlag)){

						JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){

							boolean staffer=false;
							SchoolMaster panelSchool=null;
							if(isMiami || isPhiladelphia){ //added by 21-03-2015
								try{
									JobWisePanelStatus jobWisePanelStatus =null;
									if(isPhiladelphia)//added by 21-03-2015
										jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"No Evaluation Complete");
									else							
										jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"Offer Ready");
									if(jobWisePanelStatus!=null){
										//System.out.println("jobWisePanelStatus:::"+jobWisePanelStatus.getJobPanelStatusId());
										PanelSchedule panelSchedule=panelScheduleDAO.findPanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
										if(panelSchedule!=null){
											List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
											if(panelAttendeesList.size()>0)
												for(PanelAttendees panelAttendees :panelAttendeesList){
													if(panelAttendees.getPanelInviteeId()!=null){
														UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
														if(panelUserMaster.getEntityType()==3){
															panelSchool=panelUserMaster.getSchoolId();
														}
														if(panelUserMaster.getUserId().equals(userMaster.getUserId())){
															staffer=true;
														}
													}
												}
										}
									}
								}catch(Exception e){e.printStackTrace();}
							}

							/////////////////////////////////////QQQ///////////////////////////////////////////////

							try{
								Map<Integer,List<String>> statusHistoryMap=new HashMap<Integer, List<String>>();
								Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
								Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
								List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId());
																							
								List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
								LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
								Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
								if(staffer){
									try{
										int counter=0; 
										for(SecondaryStatus tree: lstTreeStructure)
										{
											if(tree.getSecondaryStatus()==null)
											{
												if(tree.getChildren().size()>0){
													counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
												}
											}
										}
									}catch(Exception e){}

									if(statusNoteList.size()>0)
										for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
											if(isPhiladelphia){ //added by 21-03-2015
												if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("ecomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("No Evaluation Complete"))){
												teacherStatusNotesDAO.makeTransient(teacherStatusNotes);	
												}
											}
											else{//ended by 21-03-2015
											if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready"))){
											teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
											}
											}
										}
									boolean vComp=false;
									if(historyList.size()>0)
										for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
											
											if(isPhiladelphia){ //added by 21-03-2015
												if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("ecomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("No Evaluation Complete"))){
													if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp")){
														vComp=true;	
													}
													teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
												}
											}
											else{//ended by 21-03-2015
											if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready"))){
												if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp")){
													vComp=true;	
												}
												teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
											}
											}

										}

									try{		
										historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId());
										//System.out.println("historyList::::::::::"+historyList.size());

										List<String> statusList=new ArrayList<String>();
										if(historyList.size()>0)
											for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
												if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
													statusList=new ArrayList<String>();
													if(teacherStatusHistoryForJob.getStatusMaster()!=null){
														statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
													}else{
														statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
													}
													statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
												}else{
													statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
													if(teacherStatusHistoryForJob.getStatusMaster()!=null){
														statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
													}else{
														statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
													}
													statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
												}
											}
									}catch(Exception e){}
									//System.out.println("statusHistoryMap:::::::::::"+statusHistoryMap.size());
									List<String> statusIds=statusHistoryMap.get(jobForTeacherObj.getTeacherId().getTeacherId());
									String schoolStatusId=cgService.getSchoolStatusId(statusIds, statusHistMap, statusNameMap);
									SecondaryStatus secondaryStatusObj=null;
									StatusMaster statusMasterObj=null;
									
									//added by 21-03-2015
									if(isPhiladelphia){
									try{
									int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
				                    StatusMaster statusMasterMain = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
				                    //System.out.println("statusMasterMain=="+statusMasterMain.getStatus());
				                    String statusIdMapValue[]=schoolStatusId.split("#");
									//System.out.println("statusIdMapValue[]==="+statusIdMapValue);
									if(statusIdMapValue[0].equals("0") && statusIdMapValue[1].equals("0")){
											jobForTeacher.setStatusMaster(statusMasterMain);												
											jobForTeacher.setStatus(statusMasterMain);
											jobForTeacher.setSecondaryStatus(null);
									}else if(statusIdMapValue[0].equals("0")){
											secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
											jobForTeacher.setSecondaryStatus(secondaryStatusObj);												
											jobForTeacher.setStatus(statusMasterMain);
											jobForTeacher.setStatusMaster(null);
									}else if(!statusIdMapValue[0].equals("0")){
											statusMasterObj=statusMasterMap.get(schoolStatusId);
											jobForTeacher.setStatusMaster(statusMasterObj);												
											jobForTeacher.setStatus(statusMasterObj);
											jobForTeacher.setSecondaryStatus(null);																								
									}
									}catch(Exception e){e.printStackTrace();}
									}else{
									//ended by 21-03-2015
									
									try{
										String statusIdMapValue[]=schoolStatusId.split("#");
										if(statusIdMapValue[0].equals("0")){
											secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
											jobForTeacher.setSecondaryStatus(secondaryStatusObj);
											if(vComp){
												jobForTeacher.setStatus(findStatusByShortName(lstStatusMaster,"ecomp"));
												jobForTeacher.setStatusMaster(null);
											}
										}else{
											statusMasterObj=statusMasterMap.get(schoolStatusId);
										}
									}catch(Exception e){e.printStackTrace();}
									}

									try{
										if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null){
											List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobForTeacher.getJobId(),jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
											if(jobReqList.size()>0){
												for (JobRequisitionNumbers jobReqObj : jobReqList) {
													try{
														if(panelSchool!=null && jobReqObj.getSchoolMaster().getSchoolId().equals(panelSchool.getSchoolId()) && jobReqObj.getStatus()==1){
															jobReqObj.setStatus(0);
															jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
															break;
														}
													}catch(Exception e){
														e.printStackTrace();
													}
												}
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									jobForTeacher.setOfferReady(null);
									jobForTeacher.setOfferMadeDate(new Date());
									jobForTeacher.setRequisitionNumber(null);
									jobForTeacher.setOfferAcceptedDate(new Date());
									jobForTeacher.setOfferAccepted(false);
									jobForTeacherDAO.updatePersistent(jobForTeacher);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							if(entityType==3 && (isMiami || isPhiladelphia)){//added by 21-03-2015
								try{
									//System.out.println("<<<<<<<<<::::::::::::: Inside School Decline :::::::::::::::>>>>>>>>>>>");
									SchoolWiseCandidateStatus swcsObj= new SchoolWiseCandidateStatus();
									swcsObj.setStatusMaster(statusMaster);
									swcsObj.setJobOrder(jobForTeacher.getJobId());
									swcsObj.setDistrictId(userMaster.getDistrictId().getDistrictId());
									swcsObj.setSchoolId(userSchoolMaster.getSchoolId());
									swcsObj.setUserMaster(userMaster);		
									swcsObj.setCreatedDateTime(new Date());
									swcsObj.setTeacherDetail(jobForTeacher.getTeacherId());
									swcsObj.setStatus("A");
									swcsObj.setJobForTeacher(jobForTeacher);
									schoolWiseCandidateStatusDAO.makePersistent(swcsObj);
									errorFlag="3";
									mailSend=true;
									statusShortName="soth";
								}catch(Exception e){e.printStackTrace();}

							}else{
								//System.out.println("Else::::::::::::::::::::");
								jobForTeacher.setStatus(statusMaster);
								jobForTeacher.setStatusMaster(statusMaster);
								if(statusMaster!=null){
									jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
								}
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
								jobForTeacher.setUpdatedDate(new Date());
								jobForTeacher.setLastActivity("Declined");
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);
								jobForTeacherDAO.makePersistent(jobForTeacher);
								errorFlag="3";
								mailSend=true;
								statusShortName="soth";
								lstTDetails.clear();lstTDetails.add(teacherDetail);
								//System.out.println("::::::::interviewInvites::::::::7");
							}
						}
					}else{
						if(outerJob==false && selectedstatus ){
							if(statusMaster!=null && statusMaster.getStatusShortName().equals("scomp")&& (jobWiseScoreFlag || finalizeStatusFlag)){
								String statuss ="|hird|dcln|rem|vcomp|ecomp|";
								JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
								if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())&& !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
									if(!bPanel)
									{
										jobForTeacher.setStatus(statusMaster);
										jobForTeacher.setStatusMaster(statusMaster);
										if(statusMaster!=null){
											jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
										}
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMaster.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);
										jobForTeacherDAO.makePersistent(jobForTeacher);
										lstTDetails.clear();lstTDetails.add(teacherDetail);
										//System.out.println("::::::::interviewInvites::::::::8");
									}
									errorFlag="3";
									mailSend=true;
									statusShortName="soth";
								}
							}else if(statusMaster!=null && statusMaster.getStatusShortName().equals("ecomp")&& (jobWiseScoreFlag || finalizeStatusFlag)){
								String statuss = "|hird|dcln|rem|vcomp|";

								JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
								if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())&& !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
									if(!bPanel)
									{
										jobForTeacher.setStatus(statusMaster);
										jobForTeacher.setStatusMaster(statusMaster);
										if(statusMaster!=null){
											jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
										}
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMaster.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);
										jobForTeacherDAO.makePersistent(jobForTeacher);
										lstTDetails.clear();lstTDetails.add(teacherDetail);
										//System.out.println("::::::::interviewInvites::::::::9");
									}
									errorFlag="3";
									mailSend=true;
									statusShortName="soth";
								}
							}else if(statusMaster!=null && statusMaster.getStatusShortName().equals("vcomp")&& (jobWiseScoreFlag || finalizeStatusFlag)){
								//System.out.println("::::::::::::::::::::::VCOM::::::::::::::::::::::::");
								JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
								String statuss = "|hird|dcln|rem|";
								if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId()) && !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
									if(!bPanel)
									{
										jobForTeacher.setStatus(statusMaster);
										jobForTeacher.setStatusMaster(statusMaster);
										if(statusMaster!=null){
											jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
										}
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMaster.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);

										jobForTeacherDAO.makePersistent(jobForTeacher);
										//System.out.println("vcomp 01");
										lstTDetails.clear();lstTDetails.add(teacherDetail);
										//System.out.println("::::::::interviewInvites::::::::10");
									}
									errorFlag="3";
									mailSend=true;
									statusShortName="soth";
								}
							}
						}
					}
				if(outerJob==false && isMiami && statusMasterPanel!=null && statusMasterPanel.getStatusShortName().equals("vcomp")&& (jobWiseScoreFlag || finalizeStatusFlag)){ 
					//System.out.println("::::::::::::::::::::::VCOM 3rdndddddd::::::::::::::::::::::::");
					JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
					UserMaster schoolUser=null;
					try{
						List<JobWisePanelStatus> jobWiseList=new ArrayList<JobWisePanelStatus>();
						SecondaryStatus  secondaryStatusObj =secondaryStatusDAO.findSecondaryStatusByJobOrder(jobForTeacher.getJobId(),"Offer Ready");
						jobWiseList=jobWisePanelStatusDAO.getPanel(jobOrder,secondaryStatusObj,null);

						//System.out.println("jobWiseList:::::>>>>>>>>>>>>>>>>>:"+jobWiseList.size());
						List<PanelSchedule> panelSList= new ArrayList<PanelSchedule>();
						if(jobWiseList.size() == 1){
							panelSList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWiseList,teacherDetail);
							if(panelSList.size()==1){
								PanelSchedule panelSchedule=null;
								if(panelSList.get(0)!=null){
									panelSchedule=panelSList.get(0);
									List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
									panelAttendees=panelAttendeesDAO.getPanelAttendees(panelSchedule);
									if(panelAttendees.size() > 0){
										for(PanelAttendees attendees:panelAttendees){
											if(attendees!=null){
												if(attendees.getPanelInviteeId().getEntityType()==3){
													schoolUser=attendees.getPanelInviteeId();
												}
											}
										}
									}
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}								int offerAccepted=1;
					//System.out.println("userMaster:::"+userMaster.getUserId());

					try{
						if(jobForTeacher!=null){
							if(jobForTeacher.getOfferAccepted()!=null){
								if(!jobForTeacher.getOfferAccepted()){
									offerAccepted=0;
								}
							}
						}
					}catch(Exception e){}
					//System.out.println("offerAccepted:::"+offerAccepted);

					int offerReady=2;
					try{
						if(jobForTeacher.getOfferReady()!=null){
							if(jobForTeacher.getOfferReady()){
								offerReady=1;
							}else{
								offerReady=0;
							}
						}
					}catch(Exception e){}

					try{
						String statuss = "|hird|dcln|rem|";
						if(!jobForTeacher.getStatus().getStatusId().equals(statusMasterPanel.getStatusId()) && !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
							if(!bPanel)
							{
								jobForTeacher.setStatus(statusMasterPanel);
								jobForTeacher.setStatusMaster(statusMasterPanel);
								if(statusMasterPanel!=null){
									jobForTeacher.setApplicationStatus(statusMasterPanel.getStatusId());
								}
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
								jobForTeacher.setUpdatedDate(new Date());
								jobForTeacher.setLastActivity(statusMaster.getStatus());
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);

								jobForTeacherDAO.makePersistent(jobForTeacher);
								lstTDetails.clear();lstTDetails.add(teacherDetail);
								//System.out.println("::::::::interviewInvites::::::::11");
							}
						}
					}catch(Exception e){

					}
					if(offerReady==1){
						try{
							List<UserMailSend> userMailSendListFotTeacher =new ArrayList<UserMailSend>();
							UserMailSend userMailSendForTeacher= new UserMailSend();
							userMailSendForTeacher.setEmail(teacherDetail.getEmailAddress());
							userMailSendForTeacher.setTeacherDetail(teacherDetail);

							String bccAndSchoolUser[]=new String[1];
							try{
								if(schoolUser!=null){
									bccAndSchoolUser[0]=schoolUser.getEmailAddress();
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							boolean isSubstituteInstructionalJob = false;
							try{
								if(jobForTeacher!=null && jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Substitute Instructional Positions"))
									isSubstituteInstructionalJob=true;
							}catch(Exception e){}
							try{
								if(isSubstituteInstructionalJob){
									if(userMaster!=null && userMaster.getDistrictId()!=null){
										bccAndSchoolUser[0]=userMaster.getEmailAddress();
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							userMailSendForTeacher.setBccEmail(bccAndSchoolUser);
							userMailSendForTeacher.setSubject("Offer Accepted");
							userMailSendForTeacher.setIsUserOrTeacherFlag(6);
							userMailSendForTeacher.setPanelExist("1");
							try{
								if(jobForTeacher.getIsAffilated()!=null){
									userMailSendForTeacher.setInternal(jobForTeacher.getIsAffilated());
								}
							}catch(Exception e){}
							userMailSendForTeacher.setSchoolLocation(schoolLocation);
							userMailSendForTeacher.setLocationCode(locationCode);// Set school location code

							userMailSendForTeacher.setJobTitle(jobOrder.getJobTitle());
							userMailSendForTeacher.setUserMaster(userMaster);
							userMailSendListFotTeacher.add(userMailSendForTeacher);

							Integer teacherId1 = teacherDetail.getTeacherId();
							TeacherPersonalInfo teacherPersonalInfos = teacherPersonalInfoDAO.findById(teacherId1, false, false);							
							TeacherDetail teacherPersonalInfoTemp = teacherDetail;							
							teacherPersonalInfoTemp.setFgtPwdDateTime(teacherPersonalInfos.getDob());//Dob		

							String teacherAddress="";
							try{
								if(teacherPersonalInfos.getAddressLine1()!=null){
									teacherAddress=teacherPersonalInfos.getAddressLine1().trim();
								}
								if(teacherPersonalInfos.getAddressLine2()!=null && !teacherPersonalInfos.getAddressLine2().equals("")){
									teacherAddress+=" "+teacherPersonalInfos.getAddressLine2().trim();
								}
								if(teacherPersonalInfos.getCityId()!=null){
									teacherAddress+=", "+teacherPersonalInfos.getCityId().getCityName();
								}
								if(teacherPersonalInfos.getStateId()!=null){
									teacherAddress+=", "+teacherPersonalInfos.getStateId().getStateName();
								}
								if(teacherPersonalInfos.getCountryId()!=null){
									teacherAddress+=", "+teacherPersonalInfos.getCountryId().getName();
								}
								if(teacherPersonalInfos.getZipCode()!=null){
									teacherAddress+=", "+teacherPersonalInfos.getZipCode();
								}
							}catch(Exception e){}
							//System.out.println("teacherAddress::::::::::>>>>"+teacherAddress);
							teacherPersonalInfoTemp.setAuthenticationCode(teacherAddress);//Address
							teacherPersonalInfoTemp.setPhoneNumber(teacherPersonalInfos.getPhoneNumber());//Phone

							String emailAddresses="";

							List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findOfferAccepted(districtMaster);
							if(districtKeyContactList!=null){
								//System.out.println("districtKeyContactList::::::::::::::::"+districtKeyContactList.size());
								int count=0;
								for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
									if(count==1){
										emailAddresses+=",";
										count=0;
									}
									emailAddresses+=districtKeyContact.getKeyContactEmailAddress();
									count++;
								}
								//System.out.println("emailAddresses:::::::->>>>>"+emailAddresses);
								UserMailSend userMailSendForTeacher3= new UserMailSend();
								userMailSendForTeacher3.setEmail(emailAddresses);
								userMailSendForTeacher3.setTeacherDetail(teacherPersonalInfoTemp);
								userMailSendForTeacher3.setSubject(Utility.getLocaleValuePropByKey("msgNotificationOfferAccepted", locale));
								userMailSendForTeacher3.setIsUserOrTeacherFlag(7);
								userMailSendForTeacher.setPanelExist("1");
								userMailSendForTeacher3.setSchoolLocation(schoolLocation);
								userMailSendForTeacher3.setLocationCode(locationCode); //Set school  location code
								userMailSendForTeacher3.setJobTitle(jobOrder.getJobTitle());
								userMailSendListFotTeacher.add(userMailSendForTeacher3);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				//System.out.println("RJ>>>>>>>>>>>>>>>"+outerJob+" "+selectedstatus+" "+isUpdateStatus+"  "+statusUpdateHDR+" "+errorFlag);
				if(outerJob==false && selectedstatus && isUpdateStatus && isUpdateStatus && statusUpdateHDR==null && !errorFlag.equals("2") && !errorFlag.equals("4"))
						if(statusMaster!=null && (statusMaster.getStatusShortName().equals("widrw")|| statusMaster.getStatusShortName().equals("hird")|| statusMaster.getStatusShortName().equals("rem")||statusMaster.getStatusShortName().equals("dcln"))){
						if((jobWiseScoreFlag || finalizeStatusFlag) && lstTeacherStatusHistoryForJob.size()==0){
							JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
							boolean staffer=false;
							SchoolMaster panelSchool=null;
							if(isMiami || isPhiladelphia){//added by 21-03-2015
								try{
									JobWisePanelStatus jobWisePanelStatus =null;
									if(isPhiladelphia)//added by 21-03-2015
										jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"No Evaluation Complete");
									else
										jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"Offer Ready");
									if(jobWisePanelStatus!=null){
										//System.out.println("jobWisePanelStatus:::"+jobWisePanelStatus.getJobPanelStatusId());
										PanelSchedule panelSchedule=panelScheduleDAO.findPanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
										if(panelSchedule!=null){
											List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
											if(panelAttendeesList.size()>0)
												for(PanelAttendees panelAttendees :panelAttendeesList){
													if(panelAttendees.getPanelInviteeId()!=null){
														UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
														if(panelUserMaster.getEntityType()==3){
															panelSchool=panelUserMaster.getSchoolId();
														}
														if(panelUserMaster.getUserId().equals(userMaster.getUserId())){
															staffer=true;
														}
													}
												}
										}
									}
								}catch(Exception e){e.printStackTrace();}
							}
							if(staffer && !statusMaster.getStatusShortName().equals("hird")){
								try{
									JobWisePanelStatus jobWisePanelStatus =null;
									if(isPhiladelphia) //added by 21-03-2015
									jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"No Evaluation Complete");
									else
									jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"Offer Ready");
									if(jobWisePanelStatus!=null){
										panelScheduleDAO.updatePanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
									}
								}catch(Exception e){e.printStackTrace();}
							}
							//System.out.println("isMiami::;"+isMiami+" isPhiladelphia :::"+isPhiladelphia +" staffer :::"+staffer+" "+statusMaster.getStatusShortName());
							if(((isMiami || isPhiladelphia) && staffer==false && entityType==2) ||( (isMiami || isPhiladelphia) && staffer && (statusMaster.getStatusShortName().equals("widrw")|| statusMaster.getStatusShortName().equals("hird")|| (statusMaster.getStatusShortName().equals("dcln")&& entityType==2) ||(statusMaster.getStatusShortName().equals("rem")&& entityType==2))) || (isMiami==false && isPhiladelphia==false)){//added by 21-03-2015
								try{
									Calendar c = Calendar.getInstance();
									c.add(Calendar.SECOND,01);
									Date date = c.getTime();
									TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
									tSHJ.setTeacherDetail(teacherDetail);
									tSHJ.setJobOrder(jobOrder);
									if(statusId!=null && !statusId.equals("0")){
										tSHJ.setStatusMaster(statusMaster);
									}
									if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
										tSHJ.setSecondaryStatus(secondaryStatus);
									}
									tSHJ.setStatus("A");

									try{
										if(statusMaster.getStatusShortName().equals("hird")){
											tSHJ.setHiredByDate(Utility.getCurrentDateFormart(setHiredDate));
										}
									} catch(Exception e){

									}

									tSHJ.setCreatedDateTime(date);
									tSHJ.setUserMaster(userMaster);
									teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
									canNotHireObj=tSHJ;
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}else if((jobWiseScoreFlag || finalizeStatusFlag) && jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId()==1){
							//System.out.println("::::::::::::::::::::Status Else RJ::::::::::::::::");
								try{
									Calendar c = Calendar.getInstance();
									c.add(Calendar.SECOND,01);
									Date date = c.getTime();
									TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
									tSHJ.setTeacherDetail(teacherDetail);
									tSHJ.setJobOrder(jobOrder);
									if(statusId!=null && !statusId.equals("0")){
										tSHJ.setStatusMaster(statusMaster);
									}
									if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
										tSHJ.setSecondaryStatus(secondaryStatus);
									}
									tSHJ.setStatus("A");

									try{
										if(statusMaster.getStatusShortName().equals("hird")){
											tSHJ.setHiredByDate(Utility.getCurrentDateFormart(setHiredDate));
										}
									} catch(Exception e){

									}

									tSHJ.setCreatedDateTime(date);
									tSHJ.setUserMaster(userMaster);
									teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
									canNotHireObj=tSHJ;
								}catch(Exception e){
									e.printStackTrace();
								}
						}
					}
				// Update 
				int offerReadyMailFlag=0;
				if(outerJob==false && bPanel && !finalizeStatus.equals("5")){
					JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
					boolean isPanelMember=false;
					boolean panelMailSend=false;
					//System.out.println("panelMemberMap::"+panelMemberMap.size());
					try{
						if(panelMemberMap.get(userMaster.getUserId())!=null){
							isPanelMember=true;
						}
					}catch(Exception e){}


					try{
						if(jobForTeacher.getOfferReady()!=null){
							if(!jobForTeacher.getOfferReady()){
								panelMailSend=true;
							}
						}else{
							panelMailSend=true;
						}
					}catch(Exception e){}
					//System.out.println("panelMailSend:::"+panelMailSend);
					//Is Already Override
					TeacherStatusHistoryForJob teacherStatusHistoryForJob= teacherStatusHistoryForJobDAO.getOverride(teacherDetail, jobOrder,statusMasterPanel,secondaryStatusPanel);
					boolean isAlreadyOverride=false;
					if(teacherStatusHistoryForJob!=null && teacherStatusHistoryForJob.getOverride().equals(new Boolean(true)))
					{
						isAlreadyOverride=true;
						if(teacherStatusHistoryForJob.getStatusMaster()!=null)
							statusMasterPanel=teacherStatusHistoryForJob.getStatusMaster();
						else if(teacherStatusHistoryForJob.getSecondaryStatus()!=null)
							secondaryStatusPanel=teacherStatusHistoryForJob.getSecondaryStatus();

					}
					String sStatusName="";
					if(statusMasterPanel!=null)
						sStatusName=statusMasterPanel.getStatus();
					else if(secondaryStatusPanel!=null){
						sStatusName=secondaryStatusPanel.getSecondaryStatusName();
					}
					/////////////////////////////////////
					int offerReady=2;
					try{
						if(jobForTeacher.getOfferReady()!=null){
							if(jobForTeacher.getOfferReady()){
								offerReady=1;
							}else{
								offerReady=0;
							}
						}
					}catch(Exception e){}

					SchoolMaster panelSchool=null;
					try{
						JobWisePanelStatus jobWisePanelStatus =null;
						if(isPhiladelphia)//added by 21-03-2015
							jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"No Evaluation Complete");
						else							
							jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"Offer Ready");
						if(jobWisePanelStatus!=null){
							//System.out.println("jobWisePanelStatus:::"+jobWisePanelStatus.getJobPanelStatusId());
							PanelSchedule panelSchedule=panelScheduleDAO.findPanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
							if(panelSchedule!=null){
								List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
								if(panelAttendeesList.size()>0)
									for(PanelAttendees panelAttendees :panelAttendeesList){
										if(panelAttendees.getPanelInviteeId()!=null){
											UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
											if(panelUserMaster.getEntityType()==3){
												panelSchool=panelUserMaster.getSchoolId();
											}
										}
									}
							}
						}
					}catch(Exception e){e.printStackTrace();}

					boolean positonNumberFlag=false;
					String requisitionNumber=null;
					
					try{
						if(isMiami && positonNumberFlag==false && (isPanelMember)){
							
							try{
								if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
									JobRequisitionNumbers jRNOb=jobRequisitionNumbersDAO.findById(Integer.parseInt(requisitionNumberId),false,false);
									if(jRNOb!=null){
										requisitionNumber=jRNOb.getDistrictRequisitionNumbers().getRequisitionNumber();
										List<JobRequisitionNumbers> jobReqNumbers=jobRequisitionNumbersDAO.findJobReqNumbers(jRNOb.getDistrictRequisitionNumbers());
										if(jobReqNumbers.size()==1){
											boolean multiHired=false;
											try{
												if(jRNOb.getJobOrder().getIsExpHireNotEqualToReqNo()){
													multiHired=true;
												}	
											}catch(Exception e){
												e.printStackTrace();
											}
											if(multiHired==false){
												jRNOb.setStatus(1);
												jobRequisitionNumbersDAO.updatePersistent(jRNOb);
											}
										}
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					//	//System.out.println("requisitionNumber:::><::::::::::::"+requisitionNumber +" secon==="+secondaryStatusForPanel+" sta=="+statusMaster);
						if((requisitionNumber!=null || isPhiladelphia) && isPanelMember && offerReady==2 && entityType==3){//added by 21-03-2015 (isPhiladelphia)
							if(secondaryStatusForPanel!=null && !secondaryStatusForPanel.getSecondaryStatusName().equals("") && (secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready") || secondaryStatusForPanel.getSecondaryStatusName().equals("No Evaluation Complete"))){
								offerReadyMailFlag=1;
								jobForTeacher.setRequisitionNumber(requisitionNumber);
								jobForTeacher.setOfferReady(false);
								jobForTeacher.setOfferMadeDate(new Date());
								try {
									jobForTeacherDAO.updatePersistent(jobForTeacher);
								} catch (Exception e) {
									e.printStackTrace();
								}	
							}
						}else if((isMiami || isPhiladelphia) && isPanelMember && offerReady==0 && entityType==2 && finalizeStatus!=null && finalizeStatus.equals("1")){ //added by 21-03-2015
							if(secondaryStatusForPanel!=null && !secondaryStatusForPanel.getSecondaryStatusName().equals("") && (secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready")||secondaryStatusForPanel.getSecondaryStatusName().equals("No Evaluation Complete") )){//added by 21-03-2015
								offerReadyMailFlag=2;
								jobForTeacher.setOfferReady(true);
								jobForTeacher.setOfferMadeDate(new Date());
								if(requisitionNumber!=null){
									jobForTeacher.setRequisitionNumber(requisitionNumber);
								}
								try {
									jobForTeacherDAO.updatePersistent(jobForTeacher);
								} catch (Exception e) {
									e.printStackTrace();
								}	
							}
						}else if(isMiami  && offerReady==2 && entityType==2){ /// Special case Maimi (Substitute position)
							if(secondaryStatusForPanel!=null && !secondaryStatusForPanel.getSecondaryStatusName().equals("") && secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready")){ 
								offerReadyMailFlag=2;
								jobForTeacher.setOfferReady(false);
								jobForTeacher.setOfferMadeDate(new Date());
								if(requisitionNumber!=null){
									jobForTeacher.setRequisitionNumber(requisitionNumber);
								}
								try {
									jobForTeacherDAO.updatePersistent(jobForTeacher);
								} catch (Exception e) {
									e.printStackTrace();
								}	
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}

					int offerReadyForInProgress=0;
					try{
						if(statusNoteList.size()>0){
							for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
								try{
									if((teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready")) && teacherStatusNotes.isFinalizeStatus() && finalizeStatus!=null && finalizeStatus.equals("1") && secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready")){
										offerReadyForInProgress++;
									}
									//added by 21-03-2015
									if((teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("No Evaluation Complete")) && teacherStatusNotes.isFinalizeStatus() && finalizeStatus!=null && finalizeStatus.equals("1") && secondaryStatusForPanel.getSecondaryStatusName().equals("No Evaluation Complete")){
										offerReadyForInProgress++;
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					//System.out.println("offerReadyForInProgress=="+offerReadyForInProgress+" offerReady=="+offerReady);
					if(offerReadyForInProgress==1 && offerReady==0){
						offerReadyMailFlag=1;
					}
					//System.out.println("offerReadyForInProgress:::::::"+offerReadyForInProgress);

					//System.out.println("::::::::::::::::offerReadyMailFlag:::::::::::::::::::::::"+offerReadyMailFlag);
					/////////////////////////////////////////////
					List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobs=new ArrayList<TeacherStatusHistoryForJob>();
					if(userMasters_PanelAttendees.size() > 0)
					{
						teacherStatusHistoryForJobs=teacherStatusHistoryForJobDAO.findByTID_JID_Status(teacherDetail, jobOrder, statusMaster, secondaryStatus,userMasters_PanelAttendees);
					}
					if( ( iPanelAttendeeCount > 0 && iPanelAttendeeCount==teacherStatusHistoryForJobs.size() ) || isAlreadyOverride)
					{

						if(jobForTeacher!=null)
						{
							if(secondaryStatusPanel!=null)
							{
								jobForTeacher.setSecondaryStatus(secondaryStatusPanel);
								jobForTeacher.setStatusMaster(null);

							}

							if(statusMasterPanel!=null)
							{
								jobForTeacher.setStatus(statusMasterPanel);
								jobForTeacher.setStatusMaster(statusMasterPanel);
							}

							jobForTeacher.setUpdatedBy(userMaster);
							jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
							jobForTeacher.setUpdatedDate(new Date());
							if(sStatusName!=null && !sStatusName.equalsIgnoreCase(""))
								jobForTeacher.setLastActivity(sStatusName);
							jobForTeacher.setLastActivityDate(new Date());
							jobForTeacher.setUserMaster(userMaster);
							try {
								jobForTeacherDAO.updatePersistent(jobForTeacher);
							} catch (Exception e) {
								System.out.println("00.00 Error in jobForTeacherDAO");
								e.printStackTrace();
							}	
						}

						//System.out.println("4.06 Update jobForTeacherId "+jobForTeacherId);
					}
					else
					{
						System.out.println("4.06.01 Not Update jobForTeacherId "+jobForTeacherId);
					}

					
					//Start ... Email for panel Member
					if(finalizeStatus!=null && finalizeStatus.equals("1"))
					{
						///////////////////////Start Send To Teacher///////////////////////
						//System.out.println("=========================================offerReadyMailFlag:: "+offerReadyMailFlag);

						if((offerReadyMailFlag==2 && (isPanelMember) && (isMiami || isPhiladelphia) && panelMailSend)){//added 21-03-2015

							try{
								if(isPhiladelphia){ //added 21-03-2015
									//System.out.println("===================Offer Letter send to teacher ========================entityType========="+entityType);
									String statusIds="";
									try{
										if(statusMaster_temp!=null){
											statusIds="s##"+statusMaster_temp.getStatusId();
										}else{
											statusIds="ss##"+secondaryStatus_temp.getSecondaryStatusId();
										}
									}catch(Exception e){}
									//System.out.println("statusIds==Offer Letter send to teacher==="+statusIds);
								}
								else{
								List<UserMailSend> userMailSendListFotTeacher =new ArrayList<UserMailSend>();
								UserMailSend userMailSendForTeacher= new UserMailSend();
								userMailSendForTeacher.setEmail(teacherDetail.getEmailAddress());
								userMailSendForTeacher.setTeacherDetail(teacherDetail);

								userMailSendForTeacher.setSubject(Utility.getLocaleValuePropByKey("msgOfferLetter", locale));
								userMailSendForTeacher.setIsUserOrTeacherFlag(5);
								userMailSendForTeacher.setPanelExist("1");
								String statusIds="";
								try{
									if(statusMaster_temp!=null){
										statusIds="s##"+statusMaster_temp.getStatusId();
									}else{
										statusIds="ss##"+secondaryStatus_temp.getSecondaryStatusId();
									}
								}catch(Exception e){}

								int userId=0;
								try{
									if(userMaster!=null){
										userId=userMaster.getUserId();
									}
								}catch(Exception e){}

								String decline = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+"0##"+statusIds+"##"+userId);
								String accept = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+"1##"+statusIds+"##"+userId);
								userMailSendForTeacher.setOfferAcceptURL(Utility.getValueOfPropByKey("basePath")+"/verifyoffer.do?key="+accept);
								userMailSendForTeacher.setOfferDeclineURL(Utility.getValueOfPropByKey("basePath")+"/verifyoffer.do?key="+decline);

								userMailSendForTeacher.setSchoolLocation(schoolLocation);
								userMailSendForTeacher.setLocationCode(locationCode); // set school location code

								userMailSendForTeacher.setJobTitle(jobOrder.getJobTitle());
								userMailSendForTeacher.setUserMaster(userMaster);
								userMailSendForTeacher.setJobOrder(jobOrder);
								userMailSendListFotTeacher.add(userMailSendForTeacher);
								System.out.println("===================Offer Letter send to teacher =================================");
								//System.out.println("===================Offer Letter mailed =================================");
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
						//////////////////End Send To Teacher////////////////////


						if(offerReadyMailFlag==1){ // || offerReadyMailFlag==0 
							//System.out.println("===================Offer Letter send to District nnnnnnnnnnnn ========================entityType========="+entityType);
							if(isPhiladelphia){ //added by 21-2015
								//System.out.println("===================Offer Letter send to District nnnnnnnnnnnn ========================entityType========="+entityType);
								String statusIds="";
								try{
									if(statusMaster_temp!=null){
										statusIds="s##"+statusMaster_temp.getStatusId();
									}else{
										statusIds="ss##"+secondaryStatus_temp.getSecondaryStatusId();
									}
								}catch(Exception e){}
								//System.out.println("statusIds==Offer Letter send to District==="+statusIds);
							}
							else{
							List<UserMailSend> userMailSendListForPanelMember =new ArrayList<UserMailSend>();
							for(UserMaster userMaster2 : panelAttendees_ForEmail)
							{
								String sPanelUserName="";
								if(userMaster.getLastName()!=null)
									sPanelUserName=userMaster.getFirstName()+" "+userMaster.getLastName();	 
								else
									sPanelUserName=userMaster.getFirstName();

								String sPanelUserNameTo="";
								if(userMaster2.getLastName()!=null)
									sPanelUserNameTo=userMaster2.getFirstName()+" "+userMaster2.getLastName();	 
								else
									sPanelUserNameTo=userMaster2.getFirstName();
								String sTeacheName="";
								if(teacherDetail.getLastName()!=null)
									sTeacheName=teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
								else
									sTeacheName=teacherDetail.getFirstName();

								UserMailSend userMailSendForPanelMember= new UserMailSend();
								try{
									if(jobForTeacher.getRequisitionNumber()!=null){
										userMailSendForPanelMember.setRequisitionNumber(jobForTeacher.getRequisitionNumber());
									}
								}catch(Exception e){}

								try{
									if(userMaster.getEntityType()!=null && userMaster.getEntityType()==3){
										userMailSendForPanelMember.setSchoolLocation(userMaster.getSchoolId().getSchoolName());
										userMailSendForPanelMember.setLocationCode(locationCode); //Set school location code
									}else if(schoolLocation!=null){
										userMailSendForPanelMember.setSchoolLocation(schoolLocation);
										userMailSendForPanelMember.setLocationCode(locationCode);
									}
								}catch(Exception e){}
								userMailSendForPanelMember.setSubject(Utility.getLocaleValuePropByKey("msgOfferReadyNotice", locale));
								userMailSendForPanelMember.setEmail(userMaster2.getEmailAddress());
								userMailSendForPanelMember.setFromUserName(sPanelUserName);
								userMailSendForPanelMember.setToUserName(sPanelUserNameTo);
								userMailSendForPanelMember.setTeacherName(sTeacheName);
								userMailSendForPanelMember.setJobTitle(jobOrder.getJobTitle());
								userMailSendForPanelMember.setStatusName(sStatusName);
								userMailSendForPanelMember.setUserMaster(userMaster);
								userMailSendForPanelMember.setIsUserOrTeacherFlag(offerReadyMailFlag);
								userMailSendForPanelMember.setPanelExist("1");
								userMailSendListForPanelMember.add(userMailSendForPanelMember);
							}
							}//added by 21-2015 one line
						}
					}
					//End
				}else if(!finalizeStatus.equals("5")){ //Start Without Panel
					////System.out.println("::::::::::::::Without Panel:::::::::");
					JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);

					TeacherStatusHistoryForJob teacherStatusHistoryForJob= teacherStatusHistoryForJobDAO.getOverride(teacherDetail, jobOrder,statusMasterPanel,secondaryStatusPanel);
					if(teacherStatusHistoryForJob!=null && teacherStatusHistoryForJob.getOverride().equals(new Boolean(true)))
					{
						if(teacherStatusHistoryForJob.getStatusMaster()!=null)
							statusMasterPanel=teacherStatusHistoryForJob.getStatusMaster();
						else if(teacherStatusHistoryForJob.getSecondaryStatus()!=null)
							secondaryStatusPanel=teacherStatusHistoryForJob.getSecondaryStatus();

					}
					String sStatusName="";
					if(statusMasterPanel!=null)
						sStatusName=statusMasterPanel.getStatus();
					else if(secondaryStatusPanel!=null){
						sStatusName=secondaryStatusPanel.getSecondaryStatusName();
					}
					//System.out.println("::::sStatusName:::: "+sStatusName);
					if(sStatusName!=null && sStatusName.equals("Offer Ready")){
						String requisitionNumber=null;
						try{
							if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
								JobRequisitionNumbers jRNOb=jobRequisitionNumbersDAO.findById(Integer.parseInt(requisitionNumberId),false,false);
								if(jRNOb!=null){
									requisitionNumber=jRNOb.getDistrictRequisitionNumbers().getRequisitionNumber();
									List<JobRequisitionNumbers> jobReqNumbers=jobRequisitionNumbersDAO.findJobReqNumbers(jRNOb.getDistrictRequisitionNumbers());
									if(jobReqNumbers.size()==1){
										boolean multiHired=false;
										try{
											if(jRNOb.getJobOrder().getIsExpHireNotEqualToReqNo()){
												multiHired=true;
											}	
										}catch(Exception e){
											e.printStackTrace();
										}
										if(multiHired==false){
											jRNOb.setStatus(1);
											jobRequisitionNumbersDAO.updatePersistent(jRNOb);
										}
									}
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						try{	
							//System.out.println("requisitionNumber:::::::::::::::"+requisitionNumber);
							if(finalizeStatus!=null && finalizeStatus.equals("1")){
								if(secondaryStatusForPanel!=null && !secondaryStatusForPanel.getSecondaryStatusName().equals("") && secondaryStatusForPanel.getSecondaryStatusName().equals("Offer Ready")){
									jobForTeacher.setOfferReady(true);
									jobForTeacher.setOfferMadeDate(new Date());
									if(requisitionNumber!=null){
										jobForTeacher.setRequisitionNumber(requisitionNumber);
									}
									try {
										jobForTeacherDAO.updatePersistent(jobForTeacher);
									} catch (Exception e) {
										e.printStackTrace();
									}	
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						try{
							//System.out.println(" jobForTeacher >>>>>>> "+jobForTeacher);
							if(jobForTeacher!=null){
								int statusFlag=0;
								if(secondaryStatusPanel!=null && jobForTeacher.getSecondaryStatus()!=null && !secondaryStatusPanel.getSecondaryStatusName().equalsIgnoreCase(jobForTeacher.getSecondaryStatus().getSecondaryStatusName())){
									jobForTeacher.setSecondaryStatus(secondaryStatusPanel);
									if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
										jobForTeacher.setStatusMaster(null);
									}else{
										jobForTeacher.setStatusMaster(statusMaster);
										lstTDetails.clear();lstTDetails.add(teacherDetail);
										statusFlag=1;
										//System.out.println("::::::::interviewInvites::::::::15");
									}
								}
								//System.out.println(" statusMasterPanel ::::: >>>>> "+statusMasterPanel);
								if(statusMasterPanel!=null){
									jobForTeacher.setStatus(statusMasterPanel);
									jobForTeacher.setStatusMaster(statusMasterPanel);
									//System.out.println(" >>>> interview method call >>>>");
									lstTDetails.clear();lstTDetails.add(teacherDetail);
									statusFlag=2;
									//System.out.println("::::::::interviewInvites::::::::16");
								}
								//System.out.println("::::::::interviewInvites::::::::Both");
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
								jobForTeacher.setUpdatedDate(new Date());
								if(sStatusName!=null && !sStatusName.equalsIgnoreCase(""))
									jobForTeacher.setLastActivity(sStatusName);
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);
								try {
									jobForTeacherDAO.updatePersistent(jobForTeacher);
								} catch (Exception e) {
									System.out.println("Error in jobForTeacherDAO");
									e.printStackTrace();
								}	
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						if(finalizeStatus!=null && finalizeStatus.equals("1")){
							try{
								List<UserMailSend> userMailSendListFotTeacher =new ArrayList<UserMailSend>();
								UserMailSend userMailSendForTeacher= new UserMailSend();
								userMailSendForTeacher.setEmail(teacherDetail.getEmailAddress());
								userMailSendForTeacher.setTeacherDetail(teacherDetail);
								userMailSendForTeacher.setSubject(Utility.getLocaleValuePropByKey("msgSubstituteOfferLetter", locale));
								userMailSendForTeacher.setIsUserOrTeacherFlag(5);
								userMailSendForTeacher.setPanelExist("0");
								String statusIds="";
								try{
									if(statusMaster_temp!=null){
										statusIds="s##"+statusMaster_temp.getStatusId();
									}else{
										statusIds="ss##"+secondaryStatus_temp.getSecondaryStatusId();
									}
								}catch(Exception e){}

								int userId=0;
								try{
									if(userMaster!=null){
										userId=userMaster.getUserId();
									}
								}catch(Exception e){}

								String decline = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+"0##"+statusIds+"##"+userId);
								String accept = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+"1##"+statusIds+"##"+userId);
								userMailSendForTeacher.setOfferAcceptURL(Utility.getValueOfPropByKey("basePath")+"/verifyoffer.do?key="+accept);
								userMailSendForTeacher.setOfferDeclineURL(Utility.getValueOfPropByKey("basePath")+"/verifyoffer.do?key="+decline);

								userMailSendForTeacher.setSchoolLocation(schoolLocation);
								userMailSendForTeacher.setLocationCode(locationCode); // set school location code

								userMailSendForTeacher.setJobTitle(jobOrder.getJobTitle());
								userMailSendForTeacher.setUserMaster(userMaster);
								userMailSendForTeacher.setJobOrder(jobOrder);
								userMailSendListFotTeacher.add(userMailSendForTeacher);
								System.out.println("===================Offer Letter send to teacher =================================");
								//System.out.println("===================Offer Letter mailed =================================");
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}//End Offer Ready Mail
				}//End  Without Panel
				/*
				 * outerJob: false, bEmailSendFor_DA_SA: false
				   bEmailSendFor_CA: false, offerReadyMailFlag: 0
				 */


				try
				{
					//System.out.println("4.20 ************ { End Panel }  *************");
					System.out.println("========================================================== ");
					System.out.println("outerJob: "+selectedstatus+", bEmailSendFor_DA_SA: "+bEmailSendFor_DA_SA);
					System.out.println("bEmailSendFor_CA: "+bEmailSendFor_CA+", offerReadyMailFlag: "+offerReadyMailFlag);
					System.out.println("========================================================== ");
				}catch(Exception e){e.printStackTrace();}

				if(outerJob==false && (bEmailSendFor_DA_SA || bEmailSendFor_CA)&& offerReadyMailFlag==0) // Email Send only for Selected CkeckBox for All DA/SA or a Teacher
				{
					try
					{
						System.out.println("bEmailSendFor_DA_SA  Or bEmailSendFor_CA");
						System.out.println(" selectedstatus : "+selectedstatus+" \n isUpdateStatus : "+isUpdateStatus+" \n mailSend : "+mailSend+"\n statusUpdateHDR "+statusUpdateHDR+"\n errorFlag :"+errorFlag+"\n statusHistory :"+statusHistory+"\n teacherDetail :"+teacherDetail.getEmailAddress()+" \n jobOrder "+jobOrder.getJobTitle()+" \n jobWiseScoreFlag "+jobWiseScoreFlag+"\n finalizeStatusFlag "+finalizeStatusFlag+"\n\n" );
					}catch(Exception e){e.printStackTrace();}

					if(selectedSecondaryStatus)
						mailSend=true;
					try
					{
						System.out.println("========================================================== ");
						System.out.println("selectedstatus: "+selectedstatus+", selectedSecondaryStatus: "+selectedSecondaryStatus);
						System.out.println("isUpdateStatus: "+isUpdateStatus+", mailSend: "+mailSend);
						System.out.println("statusUpdateHDR: "+statusUpdateHDR+", errorFlag: "+errorFlag);
						System.out.println("statusHistory: "+statusHistory+", teacherDetail: "+teacherDetail);
						System.out.println("jobWiseScoreFlag: "+jobWiseScoreFlag+", finalizeStatusFlag: "+finalizeStatusFlag);
						System.out.println("jobOrder: "+jobOrder+", mailSend: "+mailSend);
						System.out.println("========================================================== ");

					}catch(Exception e)
					{
						e.printStackTrace();
					}


					if((selectedstatus|| selectedSecondaryStatus)  && isUpdateStatus && mailSend && statusUpdateHDR==null && !errorFlag.equals("2") && !errorFlag.equals("4") &&  statusHistory==null && teacherDetail!=null && jobOrder!=null && (jobWiseScoreFlag || finalizeStatusFlag )){
						System.out.println(":::::>mail Send Now<:::::");
						String url = Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+jobOrder.getCreatedForEntity();
						List<SecondaryStatus> lstSecondaryStatus =	secondaryStatusDAO.findSecondaryStatus(jobOrder.getDistrictMaster());
						Map<Integer,SecondaryStatus> mapSecStatus = new HashMap<Integer, SecondaryStatus>();
						for(SecondaryStatus sec:lstSecondaryStatus ){
							if(sec.getStatusMaster()!=null)
								mapSecStatus.put(sec.getStatusMaster().getStatusId(),sec);
						}
						List<UserMaster> userMasters=new ArrayList<UserMaster>();
						List<UserMaster> userMastersEmailNotification=new ArrayList<UserMaster>();
						List<UserMaster> AllDAuserMasters=new ArrayList<UserMaster>();

						List<RoleMaster> roleMasters=new ArrayList<RoleMaster>();
						RoleMaster roleMaster=new RoleMaster();
						roleMaster.setRoleId(2);
						roleMasters.add(roleMaster);
						roleMaster=new RoleMaster();
						roleMaster.setRoleId(3);
						roleMasters.add(roleMaster);

							if(statusMasterPanel!=null && (statusMasterPanel.getStatusShortName().equalsIgnoreCase("hird") || statusMasterPanel.getStatusShortName().equalsIgnoreCase("rem") || statusMasterPanel.getStatusShortName().equalsIgnoreCase("dcln"))){
								try{
									boolean isSchool=false;
									if(jobOrder.getSelectedSchoolsInDistrict()!=null){
										if(jobOrder.getSelectedSchoolsInDistrict()==1){
											isSchool=true;
										}
									}
									if(jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2 && isSchool==false){
										userMasters=userEmailNotificationsDAO.getUserByDistrict_slcByHBD(jobOrder,statusShortName,roleMasters);
									}else if(jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2 && isSchool){
										List<SchoolMaster>  schoolMasters= new ArrayList<SchoolMaster>();
										if(jobOrder.getSchool()!=null){
											if(jobOrder.getSchool().size()!=0){
												for(SchoolMaster sMaster : jobOrder.getSchool()){
													schoolMasters.add(sMaster);
												}
											}
										}
										userMasters=userEmailNotificationsDAO.getUserByDistrictAndSchoolList_slc(districtMaster, schoolMasters,jobOrder,statusShortName,roleMasters);
									}else if(jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==3 && isSchool){
										userMasters=userEmailNotificationsDAO.getUserByDistrictListOnlySchool_slc(districtMaster,userMaster.getSchoolId(),jobOrder,statusShortName,roleMasters);
									}else if(jobOrder.getCreatedForEntity()==3 && userMaster.getEntityType()==3){
										userMasters=userEmailNotificationsDAO.getUserByOnlySchool_slc(userMaster.getSchoolId(),statusShortName,roleMasters);
									}
									
									System.out.println(" userMasters >>>>>>>>>>>> <<<<<<<<<<<<<<<<<<< "+userMasters.size());
									
								}catch(Exception e){
									e.printStackTrace();
								}
						}else{
							try{
								//Get Selected District----------------------------------------------------
								List<JobCategoryWiseStatusPrivilege> jobCateWSPList = new ArrayList<JobCategoryWiseStatusPrivilege>(); 
								try{
									//System.out.println(" >>>> jobOrder "+jobOrder);
									//System.out.println(" >>>> secondaryStatusPanel "+secondaryStatusPanel);
									//System.out.println(" >>>> statusMasterPanel "+statusMasterPanel);
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
								System.out.println(" Condiation 2");
								
								//For Selected DAs/SAa
								jobCateWSPList = jobCategoryWiseStatusPrivilegeDAO.getFilterStatusWithDPoint(jobOrder.getDistrictMaster(), jobOrder.getJobCategoryMaster(), secondaryStatusPanel,statusMasterPanel);

								if(jobCateWSPList!=null && jobCateWSPList.size()>0)
								{
									System.out.println(" jobCateWSPList ::::::::: >>>>>>>>> "+jobCateWSPList.get(0).getJobCategoryStatusPrivilegeId()+" userMasters "+userMasters);
								}
								String userDaIdsStr = ""; 
								boolean allSchoolFlag = false;

								if(jobCateWSPList.size()>0)
								{
									userDaIdsStr = jobCateWSPList.get(0).getEmailNotificationToSelectedDistrictAdmins();
									allSchoolFlag = jobCateWSPList.get(0).isEmailNotificationToAllSchoolAdmins();

									//System.out.println(" userDaIdsStr "+userDaIdsStr+" allSchoolFlag :: "+allSchoolFlag);

									if(userDaIdsStr==null || userDaIdsStr.equals(""))
									{
										AllDAuserMasters = userMasterDAO.getActiveDAUserByDistrict(districtMaster);
									}


									if((userDaIdsStr!=null && !userDaIdsStr.equals("")) || allSchoolFlag)
									{
										System.out.println(" Condiation 1");
										List<Integer> daIdList = new ArrayList<Integer>();
										if(userDaIdsStr!=null && !userDaIdsStr.equals(""))
										{
											String[] userDaIds = userDaIdsStr.split("#");

											if((userDaIdsStr!=null && !userDaIdsStr.equals("")))
											{
												for(int i=0;i<userDaIds.length;i++)
												{
													daIdList.add(Integer.parseInt(userDaIds[i]));
												}
											}
										}
										//get Schools
										if(allSchoolFlag)
										{
											if(userDaIdsStr==null || userDaIdsStr.equals(""))
											{
												AllDAuserMasters = userMasterDAO.getActiveDAUserByDistrictHBD(jobOrder);
											}

											System.out.println(" jobOrder.getCreatedForEntity() "+jobOrder.getCreatedForEntity()+" userMaster.getEntityType() "+userMaster.getEntityType());


											if(jobOrder.getCreatedForEntity()==2 && (userMaster.getEntityType()==2 || userMaster.getEntityType()==3)){
												List<SchoolMaster>  schoolMasters= new ArrayList<SchoolMaster>();
												if(jobOrder.getSchool()!=null){
													if(jobOrder.getSchool().size()!=0){
														for(SchoolMaster sMaster : jobOrder.getSchool()){
															schoolMasters.add(sMaster);
														}
													}
												}
												if(schoolMasters!=null && schoolMasters.size()>0)
												{
													if(daIdList!=null && daIdList.size()>0)
														userMasters	= userMasterDAO.getUserIDSBySchoolsAndDistrict(schoolMasters,daIdList);
													else
														userMasters	= userMasterDAO.getUserIDSBySchools(schoolMasters);

													if(AllDAuserMasters.size()>0)
														userMasters.addAll(AllDAuserMasters);
												}
												else
												{
													if(userDaIdsStr!=null && !userDaIdsStr.equals(""))
														userMasters = userMasterDAO.getUsersByUserIds(daIdList);
													else
													{
														if(AllDAuserMasters.size()>0)
															userMasters.addAll(AllDAuserMasters);
													}
												}
											}
										}else{
											userMasters = userMasterDAO.getUsersByUserIds(daIdList);
										}

										if(userMasters!=null && userMasters.size()>0)
											System.out.println(" >>>>>>>>>>> final user list :: "+userMasters.size());
										else
											System.out.println(" >>>>>>>>>>> final user list is 0 ");
									}
									else
									{
										System.out.println(" Condidation 1.1 ");
										System.out.println("AllDAuserMasters size :: "+AllDAuserMasters.size());
										if(AllDAuserMasters.size()>0)
											userMasters.addAll(AllDAuserMasters);
										System.out.println(" userMasters size :::::: "+userMasters.size());
									}
									System.out.println(" overrideAll "+overrideAll);
									//For All selected users which have opted notification on
									if(overrideAll==false)
									{
										if(userMasters.size()>0)
											userMasters = userEmailNotificationsDAO.getUserByNotificationOn_slc(userMasters, statusShortName, roleMasters);
									}
									System.out.println(" userMasters final>>>>>>>>>>>>>>>>>>>>>>>size :::::"+userMasters.size());
								}
								
								
							}catch(Exception e)
							{
								e.printStackTrace();
							}
						}
					}
					else
					{
						System.out.println("Could not Send email to DA/SA/Teacher");
					}
				}
				else
				{
					System.out.println(" No bEmailSendFor_DA_SA  Or bEmailSendFor_CA");
				}
			}
			
			/*
			 * 	Purpose	:	Remove dtata from table "sapcandidatedetails" and copy to "sapcandidatedetailshistory" 
			 * 	By		:	Hanzala Subhani
			 *  Date	:	23 Feb 2015
			 * 
			 */
			
			//if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
			
			SessionFactory sessionFactory=jobForTeacherDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			/*** Auto Status Change by Sekhar***/
			System.out.println("**********************************************************************************************************************");
		    System.out.println("statusMasterSecondaryNameL::::::::::::::::"+statusMasterSecondaryName);
		    try{
			    if(jobOrder.getHeadQuarterMaster()!=null && statusMasterSecondaryName!=null && statusMasterSecondaryName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale))){
			    	commonService.autoStatusForSPJobs(jobOrder,teacherDetail,null);
			    }
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		    System.out.println("|*************************************************************************************************************************");
		    /***End Auto Status Change by Sekhar***/
		}catch (Exception e) {
			e.printStackTrace();
		}catch (Throwable ee) {
			ee.printStackTrace();
		}
		/*
		 * 		Send e_Reference
		 * 		By Hanzala Subhani
		 * 		Dated : 05 May 2015
		 * 
		 * */
		Integer referenceStatusId 		= 	null;
		Integer referenceSecStatusId	=	null;
		String referenceSecStatusName	=	"";
		String secondaryStatusName		=	"";
		try{
			if(districtMaster!=null && districtMaster.getStatusMasterForReference()!=null && districtMaster.getStatusMasterForReference().getStatusId() !=null){
				referenceStatusId = districtMaster.getStatusMasterForReference().getStatusId();
			}
			
			if(districtMaster!=null && districtMaster.getSecondaryStatusForReference() != null && districtMaster.getSecondaryStatusForReference().getSecondaryStatusId() != null ){
				referenceSecStatusId 	=	districtMaster.getSecondaryStatusForReference().getSecondaryStatusId();
				referenceSecStatusName	=	districtMaster.getSecondaryStatusForReference().getSecondaryStatusName();
			}
			if(secondaryStatus!=null)
				secondaryStatusName			=	secondaryStatus.getSecondaryStatusName();	
			
			
			if((referenceStatusId !=null && referenceStatusId!=0) || (referenceSecStatusName!=null && referenceSecStatusName!="")){
				if(statusId.equals(referenceStatusId) || secondaryStatusName.equalsIgnoreCase(referenceSecStatusName)){					
					System.out.println("e-Reference Sent Success");
				}
			}
			
		} catch(Exception exception){
			exception.printStackTrace();
		}
		
		/* End Reference Check */
		System.out.println("errorFlag::::::::::::::::::::;:"+errorFlag);
		return errorFlag+"##"+statusUpdateHDR+"##"+secondaryStatusNames+"##"+multiJobReturn;

	}	

	public StatusMaster findStatusByShortName(List <StatusMaster> statusMasterList,String statusShortName)
	{
		StatusMaster status= null;
		try 
		{   if(statusMasterList!=null)
			for(StatusMaster statusMaster: statusMasterList){
				if(statusMaster.getStatusShortName().equalsIgnoreCase(statusShortName)){
					status=statusMaster;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(status==null)
			return null;
		else
			return status;
	}	
	public String getStatusNoteForPanel(String jobForTeacherId,String fitScore,String teacherId,String jobId,String statusId,String secondaryStatusId, String pageNo, String noOfRow,String sortOrder,String sortOrderType,int doNotShowPanelinst)
	{
		System.out.println("::::::::::::::::::::::::::::::getStatusNoteForPanel:::::::::::::::::::::::::::::");
		boolean flag=false;
		StringBuffer subs = new StringBuffer();
		StringBuffer sb = new StringBuffer("");
		String schoolName = "";
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		String statusHDRFlag=null;
		String statusJSIFlag=null;
		String statusHDRReq=null;
		String statusResend=null;
		String statusWaived=null;
		Date hiredByDate = null;
		int jobCategoryFlag=2;
		boolean isSuperAdminUser=false; //added by 04-04-2015
		try {
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			//added by 03-04-2015

			List<DistrictKeyContact> notesKeys=districtKeyContactDAO.findByContactType(userMaster, "Super Administrator"); //added by 01-04-2015
			if(notesKeys.size()>0)
				isSuperAdminUser=true;
			//ended by 03-04-2015
			int roleId=0;
			int entityType=0;
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
				entityType=userMaster.getEntityType();
			}
			/*String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}*/
			DistrictMaster districtMaster = userMaster.getDistrictId();
			HeadQuarterMaster headQuarterMaster = userMaster.getHeadQuarterMaster();

			//-- set start and end position			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord = 0;


			String qqNote = ""; //@Ashish :: for Get Qualification Question Note Ids
			//	String questionDetails = "";
			String questionAssessmentIds = "";


			String sortOrderFieldName	=	"createdDateTime";
			Order  sortOrderStrVal		=	null;

			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"1";
				sortOrderStrVal			=	Order.desc(sortOrderFieldName);
			}
			//------------------------------------
			CandidateGridService cgService=new CandidateGridService();
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			JobOrder jobOrder =jobOrderDAO.findById(new Integer(jobId), false, false);
			JobForTeacher jobForTeacherObj =new JobForTeacher();
			if(jobForTeacherId!=null){
				jobForTeacherObj=jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
				System.out.println("jobForTeacherObj Id::"+jobForTeacherObj.getJobForTeacherId());
			}
			boolean isMiami = false;
			try{
				if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1200390))
					isMiami=true;
			}catch(Exception e){}

			try{
				if(jobForTeacherObj!=null){
					if(jobForTeacherObj.getRequisitionNumber()!=null){
						statusHDRReq=jobForTeacherObj.getRequisitionNumber();	
					}
				}
			}catch(Exception e){}


			SchoolInJobOrder schoolInJobOrder = null;
			if(userMaster.getEntityType()==3){
				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, userMaster.getSchoolId());
			}
			boolean isUpdateStatus=cgService.isUpdateStatus(jobForTeacherObj,userMaster,schoolInJobOrder);
			String recentStatusName="";
			boolean isJobAssessment =false;
			if(jobOrder.getIsJobAssessment()!=null){
				if(jobOrder.getIsJobAssessment()){
					isJobAssessment=true;
				}
			}

			List<TeacherStatusNotes> lstTeacherStatusNotes = null;
			List<TeacherStatusNotes> lstTeacherStatusNotesAll = null;
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3=null; 
			StatusMaster statusMaster=null;
			SecondaryStatus secondaryStatus=null;
			SecondaryStatus secondaryStatusForScore=null;
			if(statusId!=null && !statusId.equals("0")){
				statusMaster=WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
				recentStatusName=statusMaster.getStatus();
				criterion3 = Restrictions.eq("statusMaster", statusMaster);
				if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
					SecondaryStatus secondaryStatusObj=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
					secondaryStatusForScore=secondaryStatusObj;
					recentStatusName=secondaryStatusObj.getSecondaryStatusName();
					if(secondaryStatusObj.getSecondaryStatusName().equalsIgnoreCase("jsi")){
						recentStatusName="JSI";
					}else if(secondaryStatusObj.getSecondaryStatusName().equalsIgnoreCase("Offer Ready") && isMiami){
						statusHDRFlag="Offer Ready";
					}
				}
			}else if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
				secondaryStatus=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
				
				/*//added by 21-03-2015
				if(jobOrder.getDistrictMaster().getDistrictId().equals(4218990) && secondaryStatus.getSecondaryStatusName().equals("No Evaluation Complete")) //added for philadelphia
				{
					criterion3=Restrictions.eq("statusMaster", secondaryStatus.getStatusMaster());
				}else//end by 21-03-2015					
*/				criterion3 = Restrictions.eq("secondaryStatus", secondaryStatus);
				secondaryStatusForScore=secondaryStatus;
				recentStatusName=secondaryStatus.getSecondaryStatusName();
				if(secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("jsi")){
					recentStatusName="JSI";
				}else if(secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("Offer Ready") && isMiami){
					statusHDRFlag="Offer Ready";
				}else if(secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("No Evaluation Complete") && jobOrder.getDistrictMaster().getDistrictId().equals(4218990)){
					statusHDRFlag="No Evaluation Complete";
				}
			}
	
			List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivilegeList=jobCategoryWiseStatusPrivilegeDAO.getStatus(districtMaster,jobOrder.getJobCategoryMaster(), secondaryStatusForScore); 
			try{
				if(jobCategoryWiseStatusPrivilegeList.size()>0 && entityType==2){
					if(jobCategoryWiseStatusPrivilegeList.get(0).isAutoUpdateStatus()){
						if(jobCategoryWiseStatusPrivilegeList.get(0).getUpdateStatusOption()!=null){
							if(jobCategoryWiseStatusPrivilegeList.get(0).getUpdateStatusOption()==0){
								jobCategoryFlag=0;
							}else{
								jobCategoryFlag=1;	
							}
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			//System.out.println("recentStatusName:=:::::::::"+recentStatusName);
			//System.out.println("isJobAssessment::::::::::"+isJobAssessment);
			Map<String,String> mapHistrory = new HashMap<String,String>();
			if((isJobAssessment && recentStatusName.equalsIgnoreCase("JSI"))|| !recentStatusName.equalsIgnoreCase("JSI")){

				if(userMaster.getEntityType()==1){
					Criterion criterion4 = Restrictions.eq("finalizeStatus",true);
					lstTeacherStatusNotes=teacherStatusNotesDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion1,criterion2,criterion3,criterion4);
					lstTeacherStatusNotesAll=teacherStatusNotesDAO.findWithAll(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4);
				}else{
					Criterion criterion4 = Restrictions.eq("finalizeStatus",false);
					Criterion criterion5 = Restrictions.eq("userMaster",userMaster);
					Criterion criterion6=Restrictions.and(criterion4, criterion5); 
					Criterion criterion7 = Restrictions.eq("finalizeStatus",true);
					Criterion criterion8=Restrictions.or(criterion6, criterion7); 
					lstTeacherStatusNotes=teacherStatusNotesDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion1,criterion2,criterion3,criterion8);
					lstTeacherStatusNotesAll=teacherStatusNotesDAO.findWithAll(sortOrderStrVal,criterion1,criterion2,criterion3,criterion8);
				}
				List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findByTeacherAndJob(teacherDetail,jobOrder);
				try{
					for(TeacherStatusHistoryForJob teacherStatusHistoryForJob : lstTeacherStatusHistoryForJob){
						
						if(teacherStatusHistoryForJob.getStatusMaster()!=null){
							String statusM=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##"+teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0"; 
							mapHistrory.put(statusM, teacherStatusHistoryForJob.getStatus());
						}else{
							String sStatus=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##0"+"##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId();
							mapHistrory.put(sStatus, teacherStatusHistoryForJob.getStatus());
						}
						if(teacherStatusHistoryForJob.getStatusMaster() != null && teacherStatusHistoryForJob.getStatus().equalsIgnoreCase("A") && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("hird")){
							hiredByDate = teacherStatusHistoryForJob.getHiredByDate();
						}
					}
				}catch(Exception exception){
					exception.printStackTrace();
				}

				//System.out.println(":::::::::::: hiredByDate1 ::::::::::::::::::::"+hiredByDate);

				boolean isStatus=manageStatusAjax.getStatusCheck(lstTeacherStatusHistoryForJob,statusMaster,secondaryStatus);
				try{
					if(statusMaster!=null)
						if(isMiami && (statusMaster.getStatusShortName().equalsIgnoreCase("rem")|| statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))){
							List<SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus=new ArrayList<SchoolWiseCandidateStatus>();
							if(userMaster!=null)
								if(userMaster.getEntityType()==3){
									lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusHistoryForSchool(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId(),userMaster.getSchoolId());
								}else if(userMaster.getEntityType()==2){
									lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusHistoryByUser(jobForTeacherObj.getTeacherId(),jobForTeacherObj.getJobId(),userMaster);
								}
							if(lstSchoolWiseCandidateStatus.size()>0){
								if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln") && lstSchoolWiseCandidateStatus.get(0).getStatusMaster().getStatusShortName().equalsIgnoreCase("dcln")){
									statusHDRFlag="D";
								}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem") && lstSchoolWiseCandidateStatus.get(0).getStatusMaster().getStatusShortName().equalsIgnoreCase("rem")){
									statusHDRFlag="R";
								}
							}
						}
				}catch(Exception e){
					e.printStackTrace();
				}
				/****************Undo Status*******************/
				try{
					if(userMaster.getEntityType()==2){
						if(jobForTeacherObj!=null && jobForTeacherObj.getSecondaryStatus()!=null &&  jobForTeacherObj.getSecondaryStatus().getSecondaryStatusName().equalsIgnoreCase("Offer Ready")){
							statusResend="resend";
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				try{
					if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==1){
						String statusStr=null;
						if(statusMaster!=null){
							statusStr=teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+statusMaster.getStatusId()+"##0";
						}else if(secondaryStatus!=null){
							statusStr=teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##0##"+secondaryStatus.getSecondaryStatusId();
						}
						String waived=mapHistrory.get(statusStr);
						System.out.println(statusStr+":::::waived::::::"+waived);
						if(waived==null){ 
							statusWaived="waived";	
						}
						System.out.println("statusWaived ::::>>>>>>>>>>>>>>>>>>>>>>>>>>: "+statusWaived);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				boolean undoForStatus=false;
				try{
					if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()!=null && jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints() && secondaryStatus!=null && userMaster.getEntityType()==2){
						if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()){
							String accessDPoints="";
							if(jobOrder.getDistrictMaster().getAccessDPoints()!=null)
								accessDPoints=jobOrder.getDistrictMaster().getAccessDPoints();

							List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
							Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
							for (StatusMaster statusMasterObj : statusMasterList) {
								mapStatus.put(statusMasterObj.getStatusShortName(),statusMasterObj);
							}

							if(accessDPoints!=null && (accessDPoints.contains("16")|| accessDPoints.contains("17") || accessDPoints.contains("18"))){
								List<SecondaryStatus> listSecondaryStatus = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithStatuss(jobOrder,mapStatus);

								Map<Integer,SecondaryStatus> secMap=new HashMap<Integer, SecondaryStatus>();
								boolean nodeScomp=false;
								boolean nodeEcomp=false;
								boolean nodeVcomp=false;
								for(TeacherStatusHistoryForJob tSHObj : lstTeacherStatusHistoryForJob){
									if(tSHObj.getStatusMaster()!=null){
										if(tSHObj.getStatusMaster().getStatusShortName().equals("scomp")){
											nodeScomp=true;
										}else if(tSHObj.getStatusMaster().getStatusShortName().equals("ecomp")){
											nodeEcomp=true;
										}else if(tSHObj.getStatusMaster().getStatusShortName().equals("vcomp")){
											nodeVcomp=true;
										}
									}
									if(tSHObj.getSecondaryStatus()!=null)
										secMap.put(tSHObj.getSecondaryStatus().getSecondaryStatusId(),tSHObj.getSecondaryStatus());
								}
								//System.out.println("nodescomp::::::"+nodeScomp);
								//System.out.println("nodeecomp::::::"+nodeEcomp);
								//System.out.println("nodeVcomp::::::"+nodeVcomp);
								//System.out.println("listSecondaryStatus::::"+listSecondaryStatus.size());
								if(nodeScomp || nodeEcomp || nodeVcomp){
									if(listSecondaryStatus.size()>0){
										for(SecondaryStatus secondaryStatusObj :listSecondaryStatus){
											for(SecondaryStatus  obj : secondaryStatusObj.getChildren()){
												if(obj.getStatus().equalsIgnoreCase("A") && obj.getStatusMaster()==null){

													SecondaryStatus sec=secMap.get(obj.getSecondaryStatusId());
													if(sec!=null){
														////System.out.println("StatusNodeId:::::"+secondaryStatusObj.getStatusNodeMaster().getStatusNodeId());
														////System.out.println("loop Id::::"+sec.getSecondaryStatusId()+":::::Status Id::::"+secondaryStatus.getSecondaryStatusId());
														if(nodeScomp && secondaryStatusObj.getStatusNodeMaster().getStatusNodeId()==1){
															if(secondaryStatus.getSecondaryStatusId().equals(sec.getSecondaryStatusId())){
																undoForStatus=true;
																break;
															}
														}else if(nodeEcomp && secondaryStatusObj.getStatusNodeMaster().getStatusNodeId()==2){
															if(secondaryStatus.getSecondaryStatusId().equals(sec.getSecondaryStatusId())){
																undoForStatus=true;
																break;
															}
														}else if(nodeVcomp && secondaryStatusObj.getStatusNodeMaster().getStatusNodeId()==3){
															if(secondaryStatus.getSecondaryStatusId().equals(sec.getSecondaryStatusId())){
																undoForStatus=true;
																break;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}else if(userMaster.getEntityType()!=2){
						undoForStatus=true;
					}

					if(userMaster.getEntityType()==2 || userMaster.getRoleId().getRoleId()==10 || userMaster.getRoleId().getRoleId()==11){
						undoForStatus=false;
						/*int panelFlag=0;
						try{
							List<JobWisePanelStatus> jobWisePanelStatusWiseList = jobWisePanelStatusDAO.getPanel(jobOrder,secondaryStatus,statusMaster);
							//System.out.println(":::::::::Get:::::::::::jobWisePanelStatusWiseList:::::::::"+jobWisePanelStatusWiseList.size());
							List <PanelSchedule> panelScheduleStatusList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWisePanelStatusWiseList,teacherDetail);
							//System.out.println("::::::::::::::::::::panelScheduleStatusList:::::::::"+panelScheduleStatusList.size());
							if(panelScheduleStatusList.size()==1){
								panelFlag=1;
								if(panelScheduleStatusList.get(0)!=null){
									List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelScheduleStatusList.get(0));
									//System.out.println("panelAttendeesList::::::"+panelAttendeesList.size());
									if(panelAttendeesList.size()>0){
										for(PanelAttendees panelAttendees :panelAttendeesList){
											if(panelAttendees.getPanelInviteeId()!=null){
												UserMaster panelUserMaster=panelAttendees.getPanelInviteeId();
												if(panelUserMaster.getUserId().equals(userMaster.getUserId()) && userMaster.getEntityType()==2){
													panelFlag=2;
												}
											}
										}
									}
								}

							}
						}catch(Exception e){
							e.printStackTrace();
						}
						if(panelFlag==1){
							undoForStatus=true;
						}*/
						/////System.out.println("panelFlag:::::"+panelFlag);
					}
				}catch(Exception e){
					e.printStackTrace();
				}

				System.out.println("undoForStatus=::::::::>>>>>>::"+undoForStatus);
				/*********End Undo Status**************/
				if(statusMaster!=null && isStatus){
					if(statusMaster.getStatusShortName().equalsIgnoreCase("hird")){
						if(!isMiami){
							statusHDRFlag="H";
						}
					}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln")){
						statusHDRFlag="D";
					}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
						statusHDRFlag="R";
					}else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw")){
						statusHDRFlag="W";
					}else{
						if(!undoForStatus){
							statusHDRFlag="S";
						}
					}
				}else if(isStatus && secondaryStatus!=null){
					String statusStr=teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##0"+"##"+secondaryStatus.getSecondaryStatusId();
					String waived=mapHistrory.get(statusStr);
					if(waived!=null && waived.equalsIgnoreCase("W")){
						
					}else{
						if(!undoForStatus){
							statusHDRFlag="S";
						}
					}
				}
				//System.out.println("statusHDRFlag::::>>>>>>:::::::::"+statusHDRFlag);
				totaRecord=lstTeacherStatusNotesAll.size();
				int topSliderScore_isFinalizeStatus=0;
				Double scoreProvided=0.0;
				Double scoreMaxSccore=0.0;
				int dslider=1;
				List<TeacherStatusScores> teacherStatusScoresList= new ArrayList<TeacherStatusScores>();
				try{
					teacherStatusScoresList= teacherStatusScoresDAO.getStatusScoreAll(teacherDetail,jobOrder,statusMaster,secondaryStatus);
					if(teacherStatusScoresList.size()!=0){
						for(TeacherStatusScores teacherStatusScores : teacherStatusScoresList){
							if(userMaster.getUserId().equals(teacherStatusScores.getUserMaster().getUserId()))
							{
								scoreProvided=teacherStatusScores.getScoreProvided();
								scoreMaxSccore=teacherStatusScores.getMaxScore();

								if(teacherStatusScores.isFinalizeStatus())
								{
									dslider=0;
									topSliderScore_isFinalizeStatus=1;
								}
								else{
									//scoreInProgress=true;
								}
								break;
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}

				boolean isQuestionEnable=true;
				List<TeacherStatusScores> teacherStatusScore_InProcess= new ArrayList<TeacherStatusScores>();
				teacherStatusScore_InProcess=teacherStatusScoresDAO.getStatusScore(teacherDetail, jobOrder, statusMaster, secondaryStatus, userMaster);
				if(teacherStatusScore_InProcess.size()==0)
					isQuestionEnable=true;
				else if(teacherStatusScore_InProcess.size()==1)
				{
					if(teacherStatusScore_InProcess.get(0).isFinalizeStatus())
						isQuestionEnable=false;
				}

				List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
				jobCategoryWiseStatusPrivileges=jobCategoryWiseStatusPrivilegeDAO.getJobCWSP(jobOrder, secondaryStatus,statusMaster);

				TeacherAssessmentStatus teacherAssessmentStatus = null;
				TeacherAssessmentdetail teacherAssessmentdetail = null;
				java.util.List<TeacherAnswerDetail> teacherAnswerDetails =null;
				java.util.List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
				TeacherAssessmentQuestion teacherAssessmentQuestion = null;
				java.util.List<TeacherAssessmentOption> teacherQuestionOptionsList = null;

				if(recentStatusName.equalsIgnoreCase("jsi"))
				{
					JobOrder jobOrderForJSI=null;
					AssessmentJobRelation assessmentJobRelation=null;
					assessmentJobRelation=assessmentJobRelationDAO.getAssessmentJobRelationByJobOrder(jobOrder);
					if(assessmentJobRelation!=null)
					{
						AssessmentDetail assessmentDetailForJSI=null;
						if(assessmentJobRelation.getAssessmentId()!=null)
						{
							//teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail, jobOrder);
							assessmentDetailForJSI=assessmentJobRelation.getAssessmentId();
							teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentByTeacher(teacherDetail, assessmentDetailForJSI);
							if(teacherAssessmentStatusList.size() == 1){
								teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
								if(teacherAssessmentStatusList.get(0).getJobOrder()!=null)
									jobOrderForJSI=teacherAssessmentStatusList.get(0).getJobOrder();
							}
						}
					}


					String status = "";
					if(teacherAssessmentStatus==null)
					{
						return "1";
					}
					else
					{

						status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();
						teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();
						if(status.equalsIgnoreCase("icomp")){
							return "1";
						}else if(status.equalsIgnoreCase("vlt")){
							return "2";
						}
					}
					Criterion criterion = Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail);
					Criterion criterionTech = Restrictions.eq("teacherDetail", teacherDetail);
					//Criterion criterionZJob = Restrictions.eq("jobOrder", jobOrder);
					Criterion criterionZJob = Restrictions.eq("jobOrder", jobOrderForJSI);
					teacherAnswerDetails = teacherAnswerDetailDAO.findByCriteria(criterion,criterionTech,criterionZJob);
				}

				// Start ... Copy Question in statusspecificscore table
				int iScoreSum=0;
				Double iScoreMaxSum=0.0;
				int answerSliderCount=0;
				List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
				Map<Integer,StatusSpecificScore> stausSCore= new HashMap<Integer, StatusSpecificScore>();
				if(userMaster.getEntityType()!=1)
				{
					if((secondaryStatusId!=null && !secondaryStatusId.equals("0")) || (statusId!=null && !statusId.equals("0")) )
					{
						boolean flagForSuperUser=false;
						if((isSuperAdminUser || userMaster.getEntityType()==2) && !recentStatusName.equalsIgnoreCase("JSI")){
							lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList(jobOrder,statusMaster,secondaryStatus,teacherDetail,userMaster);
							if(lstStatusSpecificScore.size()==0)flagForSuperUser=true;
							lstStatusSpecificScore.clear();
							lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionListForSuperAdministration(jobOrder,statusMaster,secondaryStatus,teacherDetail);
						}else{
							lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList(jobOrder,statusMaster,secondaryStatus,teacherDetail,userMaster);
						}	
						if(lstStatusSpecificScore.size()==0 || flagForSuperUser){
							//System.out.println("==============================================enter----------------------------------------------------------------");
							if(recentStatusName.equalsIgnoreCase("jsi")){
								/**** Session dynamic ****/
								SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
								StatelessSession statelesSsession = sessionFactory.openStatelessSession();
								org.hibernate.Transaction txOpen =statelesSsession.beginTransaction();
								for(TeacherAnswerDetail teacherAnswerDetail : teacherAnswerDetails) 
								{
									StatusSpecificScore statusSpecificScore=new StatusSpecificScore();
									statusSpecificScore.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());

									statusSpecificScore.setJobCategoryId(jobOrder.getJobCategoryMaster().getJobCategoryId());
									statusSpecificScore.setJobOrder(jobOrder);

									if(statusMaster!=null)
										statusSpecificScore.setStatusId(statusMaster.getStatusId());
									if(secondaryStatus!=null)
										statusSpecificScore.setSecondaryStatusId(secondaryStatus.getSecondaryStatusId());

									statusSpecificScore.setTeacherAnswerDetail(teacherAnswerDetail);

									statusSpecificScore.setScoreProvided(0);
									if(jobCategoryWiseStatusPrivileges.size()>0){
										statusSpecificScore.setMaxScore(jobCategoryWiseStatusPrivileges.get(0).getMaxValuePerJSIQuestion());
									}
									statusSpecificScore.setUserMaster(userMaster);
									statusSpecificScore.setCreatedDateTime(new Date());

									statusSpecificScore.setTeacherDetail(teacherDetail);
									statusSpecificScore.setFinalizeStatus(0);

									if(userMaster.getSchoolId()!=null)
										statusSpecificScore.setSchoolId(userMaster.getSchoolId().getSchoolId());

									statelesSsession.insert(statusSpecificScore);
								}
								statusSpecificScoreDAO.copyQuestionAnswer(jobOrder, statusMaster,secondaryStatus,userMaster,teacherDetail);//add by Ram Nath for Jsi question
								txOpen.commit();
								statelesSsession.close();
							}else{
								statusSpecificScoreDAO.copyQuestionAnswer(jobOrder, statusMaster,secondaryStatus,userMaster,teacherDetail);
							}
							if((isSuperAdminUser || userMaster.getEntityType()==2) && !recentStatusName.equalsIgnoreCase("JSI")){
							lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionListForSuperAdministration(jobOrder,statusMaster,secondaryStatus,teacherDetail);
							}else{
							lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList(jobOrder,statusMaster,secondaryStatus,teacherDetail,userMaster);
							}
						}
						for(StatusSpecificScore pojo:lstStatusSpecificScore)
						{
							if(userMaster.getUserId().toString().equalsIgnoreCase(pojo.getUserMaster().getUserId().toString()))
							if(pojo!=null)
							{
								if(pojo.getTeacherAnswerDetail()!=null)
									stausSCore.put(pojo.getTeacherAnswerDetail().getAnswerId(),pojo);
								if(pojo.getScoreProvided()!=null){
									iScoreSum=iScoreSum+pojo.getScoreProvided();
								}
								if(pojo.getMaxScore()!=null && pojo.getMaxScore()>0){
									iScoreMaxSum=iScoreMaxSum+pojo.getMaxScore();
									answerSliderCount++;
								}

							}
						}
					}
				}
				// End ... Copy Question in statusspecificscore table

				// Start ... ShowInstructionAttacheFileName 
				boolean bShowInstructionAttacheFileName=false;
				String strShowInstructionAttacheFileName="";
				//if(lstStatusSpecificScore.size()>0)
				//{
				if(jobCategoryWiseStatusPrivileges.size()>0){
					strShowInstructionAttacheFileName=jobCategoryWiseStatusPrivileges.get(0).getInstructionAttachFileName();
					if(userMaster.getEntityType()!=1 && strShowInstructionAttacheFileName!=null && !strShowInstructionAttacheFileName.equalsIgnoreCase(""))
						bShowInstructionAttacheFileName=true;
				}

				//}
				// End ... ShowInstructionAttacheFileName


				//Is Already Override
				TeacherStatusHistoryForJob teacherStatusHistoryForJob= teacherStatusHistoryForJobDAO.getOverride(teacherDetail, jobOrder,statusMaster,secondaryStatus);
				boolean isAlreadyOverride=false;
				if(teacherStatusHistoryForJob!=null && teacherStatusHistoryForJob.getOverride().equals(new Boolean(true)))
					isAlreadyOverride=true;

				//Active District User
				Map<String , Integer> mapUser=new HashMap<String, Integer>();
				Map<String , Boolean> mapHR=new HashMap<String, Boolean>();
				List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
				lstUserMaster=userMasterDAO.getActiveUserByDistrict(userMaster.getDistrictId());
				if(lstUserMaster.size() > 0)
				{
					for(UserMaster pojo:lstUserMaster)
						mapUser.put(pojo.getEmailAddress(), pojo.getEntityType());
				}
				//HR List
				boolean bActiveHR=false;
				boolean bOverride=false;
				List<DistrictKeyContact> districtKeyContacts=new ArrayList<DistrictKeyContact>();
				ContactTypeMaster contactTypeMaster=contactTypeMasterDAO.getActiveMaster(1);
				if(contactTypeMaster!=null && userMaster.getEntityType()!=1)
				{
					if(userMaster.getDistrictId()!=null)
						districtKeyContacts=districtKeyContactDAO.getHRList(contactTypeMaster, userMaster.getDistrictId().getDistrictId());
					if(districtKeyContacts.size() > 0)
						for(DistrictKeyContact pojo:districtKeyContacts)
							if(mapUser.get(pojo.getKeyContactEmailAddress())!=null)
							{
								bActiveHR=true;
								mapHR.put(pojo.getKeyContactEmailAddress(), new Boolean(true));
							}
							else
								mapHR.put(pojo.getKeyContactEmailAddress(), new Boolean(false));
				}

				if(bActiveHR)
				{
					if(mapHR.get(userMaster.getEmailAddress())!=null && mapHR.get(userMaster.getEmailAddress()).equals(new Boolean(true)))
						bOverride=true;
				}
				else
				{
					if(userMaster.getEntityType()==2)
						bOverride=true;
				}

				List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();

				SecondaryStatus secondaryStatus_temp=null;
				StatusMaster statusMaster_temp=null;
				List<SecondaryStatus> lstSecondaryStatus_temp=new ArrayList<SecondaryStatus>();

				if(statusMaster!=null)
				{
					lstSecondaryStatus_temp=secondaryStatusDAO.getSecondaryStatusForPanel(jobOrder.getDistrictMaster(), jobOrder.getJobCategoryMaster(), statusMaster);
					if(lstSecondaryStatus_temp.size() ==1)
					{
						secondaryStatus_temp=lstSecondaryStatus_temp.get(0);
						statusMaster_temp=null;
					}
					else
					{
						statusMaster_temp=statusMaster;
					}
				}
				else
				{
					secondaryStatus_temp=secondaryStatus;
				}

				jobWisePanelStatusList=jobWisePanelStatusDAO.getPanel(jobOrder,secondaryStatus_temp,statusMaster_temp);
				
				boolean bPanelForCG=false;
				boolean bPanelMember=false;
				boolean bLoggedInUserPanelAttendees=false;
				List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();

				if(jobWisePanelStatusList.size()==1)
				{
					panelScheduleList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWisePanelStatusList,teacherDetail);
					if(jobWisePanelStatusList.get(0).getPanelStatus())
					{
						/*if(panelScheduleList.size()==1)
							if(panelScheduleList.get(0).getTeacherDetail().getTeacherId().equals(teacherDetail.getTeacherId()))*/
						bPanelForCG=true;
					}

				}
				Map<Integer, UserMaster> mapUserScoreOrNote=new HashMap<Integer, UserMaster>();	
				List<UserMaster> lstUserMasterForScoresAndNotes = new ArrayList<UserMaster>();
				List<TeacherStatusNotes> lstTeacherStatusNotesAllUsers = new ArrayList<TeacherStatusNotes>();
				lstTeacherStatusNotesAllUsers=teacherStatusNotesDAO.findWithAll(sortOrderStrVal,criterion1,criterion2,criterion3);
				if(lstTeacherStatusNotesAllUsers.size()>0){
					for(TeacherStatusNotes notes:lstTeacherStatusNotesAllUsers)
						if(mapUserScoreOrNote.get(notes.getUserMaster().getUserId())==null && notes.isFinalizeStatus()){
							mapUserScoreOrNote.put(notes.getUserMaster().getUserId(), notes.getUserMaster());
							lstUserMasterForScoresAndNotes.add(notes.getUserMaster());
						}
				}

				if(teacherStatusScoresList.size()>0){
					for(TeacherStatusScores teacherStatusScores : teacherStatusScoresList)
						if(mapUserScoreOrNote.get(teacherStatusScores.getUserMaster().getUserId())==null && teacherStatusScores.isFinalizeStatus()){
							mapUserScoreOrNote.put(teacherStatusScores.getUserMaster().getUserId(), teacherStatusScores.getUserMaster());
							lstUserMasterForScoresAndNotes.add(teacherStatusScores.getUserMaster());
						}
				}				
				List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
				if(bPanelForCG)
				{
					//panelScheduleList=panelScheduleDAO.findPanelSchedules(teacherDetail, jobOrder);
					//panelScheduleList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusList(jobWisePanelStatusList);

					if(panelScheduleList.size()==1 && doNotShowPanelinst==0)
					{
						PanelSchedule panelSchedule= panelScheduleList.get(0);
						panelAttendees=panelAttendeesDAO.getPanelAttendees(panelSchedule);
						if(panelAttendees.size()>0)
						{
							bPanelMember=true;
							Collections.sort(panelAttendees,PanelAttendees.panelAttendeesUserNameAsc);
							sb.append("<div class='row mt10' style='margin-left:0px;'>");
							int i=0;
							for(PanelAttendees pojo:panelAttendees)
							{
								if(pojo.getPanelInviteeId().getUserId().equals(userMaster.getUserId()))
									bLoggedInUserPanelAttendees=true;

								i++;
								String sName="";
								if(pojo.getPanelInviteeId().getFirstName()!=null)
									sName=pojo.getPanelInviteeId().getFirstName();
								if(pojo.getPanelInviteeId().getLastName()!=null)
									sName=sName+" "+pojo.getPanelInviteeId().getLastName();
								sName.trim();

								String userAddress="";
								try{
									UserDashboardController udc=new UserDashboardController();
									BasicController bc= new BasicController();
									if(pojo.getPanelInviteeId()!=null){
										if(pojo.getPanelInviteeId().getEntityType()==3){
											userAddress=pojo.getPanelInviteeId().getSchoolId().getSchoolName()+"&#13;"+udc.getSchoolAddress(jobOrder);
										}else{
											userAddress=pojo.getPanelInviteeId().getDistrictId().getDistrictName()+"&#13;"+bc.getDistrictFullAddress(jobOrder);
										}
									}
									if(userAddress.equals("")){
										userAddress="Unknown";
									}
								}catch(Exception e){}


								if(mapUserScoreOrNote.get(pojo.getPanelInviteeId().getUserId())==null)									
									sb.append("<div class='row col-sm-4' style='padding-left:20px;'><div class='span_20'>&nbsp;<span class='fa-circle icon-large iconcolorRed' title='"+userAddress+"' style='cursor:default'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+sName+"</span></div></div>");
								else
									sb.append("<div class='row col-sm-4' style='padding-left:20px;'><div class='span_20'>&nbsp;<span class='fa-circle icon-large iconcolorGreen' title='"+userAddress+"' style='cursor:default'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+sName+"</span></div></div>");

								if(i%3==0)
									sb.append("<div class='row mt10' style='margin-left:-35px;'>&nbsp;</div>");							}
							sb.append("</div>");

							sb.append("<div class='row' style='margin-left:-35px;'>&nbsp;</div>");
						}
					}
				}
				else
				{
					if(lstUserMasterForScoresAndNotes.size()>0){
						Collections.sort(lstUserMasterForScoresAndNotes,UserMaster.userNameAsc);
						sb.append("<div class='row mt10' style='margin-left:-38px;'>");
						for(UserMaster userMaster2:lstUserMasterForScoresAndNotes)
						{
							if(userMaster2!=null)
							{
								String sName="";
								if(userMaster2.getFirstName()!=null)
									sName=userMaster2.getFirstName();
								if(userMaster2.getLastName()!=null)
									sName=sName+" "+userMaster2.getLastName();
								sName.trim();

								String userAddress="";
								try{
									UserDashboardController udc=new UserDashboardController();
									BasicController bc= new BasicController();
									if(userMaster2!=null){
										if(userMaster2.getEntityType()==3){
											userAddress=userMaster2.getSchoolId().getSchoolName()+"&#13;"+udc.getSchoolAddress(jobOrder);
										}else{
											userAddress=userMaster2.getDistrictId().getDistrictName()+"&#13;"+bc.getDistrictFullAddress(jobOrder);
										}
									}
									if(userAddress.equals("")){
										userAddress="Unknown";
									}
								}catch(Exception e){}
								sb.append("<div class='span3'><div class='status-notes-image'><span class='fa-circle icon-large iconcolorGreen' title='"+userAddress+"'></span></div><div class='status-notes-text' style='cursor:default' title='"+userAddress+"'>"+sName+"</div></div>");
							}
						}
						sb.append("</div>");
						sb.append("<div class='row' style='margin-left:-35px;'>&nbsp;</div>");
					}
				}

				List<TeacherStatusNotes> lstTeacherStatusNotesForPanel =new ArrayList<TeacherStatusNotes>();
				lstTeacherStatusNotesForPanel=teacherStatusNotesDAO.getFinalizeNotes(teacherDetail, jobOrder, statusMaster, secondaryStatus, userMaster); 

				boolean isFilaliseForPanelArray[]=new boolean[2];
				isFilaliseForPanelArray=manageStatusAjax.isVisiableScoreSliderAndIcons(lstTeacherStatusHistoryForJob, jobWisePanelStatusList, panelAttendees, statusMaster, secondaryStatus, userMaster, teacherStatusScore_InProcess,lstTeacherStatusNotesForPanel,bPanelForCG,bLoggedInUserPanelAttendees,bOverride);
				boolean isFilaliseForPanel=isFilaliseForPanelArray[0];
				boolean isViewScoreIcon=isFilaliseForPanelArray[1];
				//End ... Panel



				if(userMaster.getEntityType()==3 && isUpdateStatus==false){
					dslider=0;
				}

				// Info message for If Panel have and Does not have panel member
				/*if(bPanelForCG && !bPanelMember)
				{
					sb.append("<div style='color: red;' id='noPanelMember'>No Panel Member is attached with panel schedule.</div>");
					sb.append("<input type='hidden' name='txtPanelMember' id='txtPanelMember'  value='0'/>");
				}
				else
				{
					sb.append("<input type='hidden' name='txtPanelMember' id='txtPanelMember'  value='1'/>");
				}*/
				if(true)
				{
				sb.append("<div class='row left10' id='candidateDetails' style='display:none;'><div class='control-group span16'>");
				sb.append("<label><strong>"+Utility.getLocaleValuePropByKey("lblCandidateName", locale)+":</strong></label>&nbsp;&nbsp;&nbsp;&nbsp;<span>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"</span><br/>");
				sb.append("<label><strong>"+Utility.getLocaleValuePropByKey("lblCandidatePosition", locale)+":</strong></label><span>&nbsp;&nbsp;"+jobOrder.getJobTitle()+"</span><br/></div></div>");
				}
				
				sb.append("<table border='0' style='width:670px;'>");
				if(fitScore!=null && !fitScore.isEmpty() && !fitScore.equals("0.0"))
				{
					
					sb.append("<tr><td align=left id='sliderStatusNote'><input type='hidden' name='noteDivChk' id='noteDivChk'  value='10'/>");
					sb.append("<table border=0><tr><td style='vertical-align:top;height:33px;padding-left:8px;'><label >"+Utility.getLocaleValuePropByKey("lblScore", locale)+" </label></td>");
					if(fitScore.equals("0.0")){
						dslider=0;
					}

					dslider=manageStatusAjax.sliderDisplayStatus(dslider, bPanelForCG, bLoggedInUserPanelAttendees, bOverride);
				

					if(answerSliderCount>0)
						dslider=0;

					if(!isQuestionEnable)
						dslider=0;

					Double topMaxScore=0.0;
					if(scoreMaxSccore>0)
						topMaxScore=scoreMaxSccore;
					else
						topMaxScore=Double.parseDouble(fitScore);

					if(scoreProvided==0 && iScoreMaxSum>0.0)
						topMaxScore=iScoreMaxSum;

					int interval=10;
					if(topMaxScore%10==0)
						interval=10;
					else if(topMaxScore%5==0)
						interval=5;
					else if(topMaxScore%3==0)
						interval=3;
					else if(topMaxScore%2==0)
						interval=2;

					if(jobForTeacherObj!=null && jobForTeacherObj.getDistrictId()!=null &&  (jobForTeacherObj.getDistrictId()==7800047))
						sb.append("<td style='padding:5px;vertical-align:bottom;height:33px;'>&nbsp;<iframe id=\"ifrmStatusNote\"  src=\"slideractForDecimal.do?name=statusNoteFrm&tickInterval="+interval+"&max="+topMaxScore+"&swidth=536&dslider="+dslider+"&svalue="+scoreProvided+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:580px;margin-top:-11px;\"></iframe></td>");
					else
						sb.append("<td style='padding:5px;vertical-align:bottom;height:33px;'>&nbsp;<iframe id=\"ifrmStatusNote\"  src=\"slideract.do?name=statusNoteFrm&tickInterval="+interval+"&max="+topMaxScore+"&swidth=536&dslider="+dslider+"&svalue="+scoreProvided+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:580px;margin-top:-11px;\"></iframe></td>");
					
					Double sliderVal=0.0;
					if(userMaster.getEntityType()==3 && isUpdateStatus==false){
						sliderVal=-2.0;
					}else if(dslider==0){
						sliderVal=-1.0;
					}else{
						sliderVal=scoreProvided;
					}
					sb.append("<td valign=top style='padding-top:4px; padding-left: 17px;'><input type='hidden' name='dsliderchk' id='dsliderchk'  value='"+sliderVal+"'/>");
					if(manageStatusAjax.getScore(teacherStatusScoresList, statusMaster, secondaryStatus)){
						if(isViewScoreIcon)
							sb.append("<a class='statusscore' data-placement='above' href='javascript:void(0);' id='statusscoreYes'><span class='fa-crosshairs icon-large iconcolor'></span></a>");
					}else{
						if(fitScore.equals("0.0")){
							sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblPlzAskDistrictAdmin", locale)+"' rel='tooltip' id='noFitScore' href='javascript:void(0);' ><img src=\"images/qua-icon.png\" width=\"15\" height=\"15\"></a>");
						}else{
							sb.append("&nbsp;");
						}
					}

					if(bShowInstructionAttacheFileName)
					{
						String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
						String fileName=strShowInstructionAttacheFileName;
						String filePath=""+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusId();
						sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewInst", locale)+"' rel='tooltip' id='attachInstructionFileName' href='javascript:void(0);' onclick=\"downloadAttachInstructionFileName('"+filePath+"','"+fileName+"','"+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusName()+"');"+windowFunc+"\"><span class='icon-info-sign icon-large iconcolor'></span></a>");
					}

					sb.append("</td></tr>");
					sb.append("</table>");
					sb.append("</td></tr>");
					/*}
					else
					{
						sb.append(OnlyShowInstructionAttacheFileNameWithOutSlider(bShowInstructionAttacheFileName, strShowInstructionAttacheFileName, jobCategoryWiseStatusPrivileges));
					}*/
				}
				else
				{
					sb.append(manageStatusAjax.OnlyShowInstructionAttacheFileNameWithOutSlider(bShowInstructionAttacheFileName, strShowInstructionAttacheFileName, jobCategoryWiseStatusPrivileges));
				}

				int iCountScoreSlider=0;
				/*******Start JSI*********/
				sb.append("<tr><td align=left>");
				int iQdslider=0;
				if(isQuestionEnable)
					iQdslider=1;

				else if(userMaster.getEntityType()==1)
					iQdslider=0;
				if(userMaster.getEntityType()==3 && isUpdateStatus==false)
					iQdslider=0;

				iQdslider=manageStatusAjax.sliderDisplayStatus(iQdslider, bPanelForCG, bLoggedInUserPanelAttendees, bOverride);
				int defauntQuestionNote=0;
				String clickable="class='notclickable'";				
				if(recentStatusName.equalsIgnoreCase("JSI")&& userMaster.getEntityType()!=1){
					
					//iQdslider,iCountScoreSlider,qqNote,defauntQuestionNote,questionAssessmentIds,html  //added date 30-04-2015
					/****************************call new method********************************/
					//array return value :iQdslider,iCountScoreSlider,qqNote,defauntQuestionNote,html
					String[] allValue=callOnlyForJSIJQuestionAndAnswer(teacherDetail, jobOrder, statusMaster_temp, secondaryStatus_temp, userMaster, teacherAssessmentdetail, stausSCore, teacherAnswerDetails, teacherAssessmentQuestion, teacherQuestionOptionsList, questionAssessmentIds, iQdslider, iCountScoreSlider, qqNote, defauntQuestionNote,isSuperAdminUser,clickable);
					iQdslider=Integer.parseInt(allValue[0]);
					iCountScoreSlider=Integer.parseInt(allValue[1]);
					qqNote=allValue[2];
					defauntQuestionNote=Integer.parseInt(allValue[3]);
					questionAssessmentIds=allValue[4];
					sb.append(allValue[5]);
					/****************************end call new method********************************/
				}
				sb.append("</td></tr>");
				/*******End JSI*********/

				//System.out.println(" >>>>***************** qqNote ******************* >>>>>>> "+qqNote);

				//************* Start ... Display Question and List
				int requiredNotesForUno=0; //added by 04-04-2015
				int isSchoolOnJobId=0;
				List<SchoolInJobOrder> listSIJO=new ArrayList<SchoolInJobOrder>();
				Boolean checkSchoolOnJobIdSetOrNot=true;
				
				if(checkSchoolOnJobIdSetOrNot!=null && checkSchoolOnJobIdSetOrNot){
				try{
					listSIJO=schoolInJobOrderDAO.findByCriteria(Restrictions.eq("jobId", jobOrder));
					if(listSIJO.size()>0)
						isSchoolOnJobId=1;					
				}catch(Exception e){e.printStackTrace();}	
				}
				
				Map<Integer,String> alreadyShowQuestion=new TreeMap<Integer, String>();
				Map<Integer,String> alreadyShowAttribute=new TreeMap<Integer, String>();
				if(userMaster.getEntityType()!=1 && userMaster.getEntityType()!=5 && userMaster.getEntityType()!=6 && !recentStatusName.equalsIgnoreCase("JSI"))
				{		
					//added by 04-04-2015
					if(userMaster.getDistrictId().getDistrictId().equals(7800040) || userMaster.getDistrictId().getDistrictId().equals(614730)){
						requiredNotesForUno=1;
					}
					//ended by 04-04-2015
					iCountScoreSlider=0;
					if(lstStatusSpecificScore.size()>0)
					{
						int scoreWidth=525,scoreWidth1=565;
						if(userMaster.getEntityType()==3){
							scoreWidth=575;scoreWidth1=615;
						}
						Map<String,String> getScoreHtmlTableMap= manageStatusAjax.getAllStatusSpecificScoreByQuestionIdAndAnswerId(teacherDetail, jobOrder, statusMaster, secondaryStatus);
						println("========================================================================================================");
						Map<String,StatusSpecificScore> mapSSS=new LinkedHashMap<String,StatusSpecificScore>();
						for(StatusSpecificScore score:lstStatusSpecificScore){
							if(score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString()) && score.getQuestion()!=null && !score.getQuestion().equals("")){
								mapSSS.put(score.getUserMaster().getUserId().toString()+"#"+score.getStatusSpecificQuestions().getQuestionId(),score);
							}else if(score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString()) && score.getSkillAttributesMaster()!=null)	{
								mapSSS.put(score.getUserMaster().getUserId().toString()+"#"+score.getStatusSpecificQuestions().getQuestionId(),score);
							}
							}							
						for(StatusSpecificScore score:lstStatusSpecificScore){println("--user id======"+score.getUserMaster().getUserId()+" question ===="+score.getQuestion());}
						
						Map<UserMaster,List<TeacherStatusNotes>> getAllNotesByUserMap=getAllNotesByUserMap(lstStatusSpecificScore);
						
						println("========================================================================================================");
						if(isSuperAdminUser){
							alreadyShowQuestion.clear();
							/*****************************************************************YYYYYYYYYYYYYY******************************************************/														
													sb.append("<tr><td align=left>");
														int questionNumber=1;
														int iQuestionCount=0;	
														boolean questionShowOrNot=false;
														boolean flagforOneTimeShow=false;
														//Map<Integer, Boolean> attMap = new HashMap<Integer, Boolean>();
														for(StatusSpecificScore score:lstStatusSpecificScore)
														{
															boolean firtQustionSliderShow=false;
															
															boolean thisUser=score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString());
															if(score.getQuestion()!=null && !score.getQuestion().equalsIgnoreCase("") && alreadyShowQuestion.get(score.getStatusSpecificQuestions().getQuestionId())==null)
															{
																questionShowOrNot=manageStatusAjax.showOrNot(lstStatusSpecificScore,userMaster,score.getStatusSpecificQuestions(),getAllNotesByUserMap);
																if(questionShowOrNot){
																iQuestionCount++;
																sb.append("<div class='row question_padding_left '><span><label class='labletxt'><B>"+Utility.getLocaleValuePropByKey("lblQues", locale)+" "+iQuestionCount+":</B> "+score.getQuestion()+"</label></span></div>");
																if(score.getQuestion()!=null && !score.getQuestion().equals("") && score.getSkillAttributesMaster()!=null){
																	sb.append("<div class='row question_padding_left'><span><label  class='lableAtxt'><B>Attribute:</B> "+score.getSkillAttributesMaster().getSkillName()+"</label></span></div>");
																	alreadyShowAttribute.put(score.getStatusSpecificQuestions().getQuestionId(), score.getSkillAttributesMaster().getSkillName());
																}
																alreadyShowQuestion.put(score.getStatusSpecificQuestions().getQuestionId(), score.getQuestion());																
																firtQustionSliderShow=true;
																flagforOneTimeShow=true;
																}
															}
															
															
															int queId=0;
															
															if(score.getQuestion()!=null && !score.getQuestion().equals("") && score.getStatusSpecificQuestions()!=null){
																queId=score.getStatusSpecificQuestions().getQuestionId();
															}
															boolean scoreShowOrNot=false;
															try{
																println("thisUser=================yyyyyyyyyyy============================="+thisUser);
															if(!thisUser){
															List<TeacherStatusNotes> listTSN=teacherStatusNotesDAO.getQuestionNoteObjBySuperUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), Long.parseLong(queId+""),score.getUserMaster());
															println("listTSN==========hhhhhhhhhhhhhhhhhhhhh======:::::::::"+listTSN);
															if(listTSN!=null && listTSN.size()>0)
																scoreShowOrNot=listTSN.get(0).isFinalizeStatus();
															}
															}catch(Exception e){e.printStackTrace();}
															//boolean showOrHide=alreadyShowQuestion.get(score.getQuestion())!=null?true:false;															
															Double iQuestionMaxScore=0.0;
															if(score.getMaxScore()!=null){
																iQuestionMaxScore=score.getMaxScore();
															}
															int tickInterval=1;
							
															if(iQuestionMaxScore%10==0)
																tickInterval=10;
															else if(iQuestionMaxScore%5==0)
																tickInterval=5;
															else if(iQuestionMaxScore%3==0)
																tickInterval=3;
															else if(iQuestionMaxScore%2==0)
																tickInterval=2;
																
																if(alreadyShowAttribute.get(score.getStatusSpecificQuestions().getQuestionId())==null && (score.getQuestion()==null || score.getQuestion().equals("")) && score.getSkillAttributesMaster()!=null && thisUser ){
																	sb.append("<div class='row question_padding_left'><span><label  class='lableAtxt'><B>"+Utility.getLocaleValuePropByKey("lnkAtt", locale)+":</B> "+score.getSkillAttributesMaster().getSkillName()+"</label></span></div>");
																	firtQustionSliderShow=true;																	
																}
																														
															if(firtQustionSliderShow){
																StatusSpecificScore sss=mapSSS.get(userMaster.getUserId().toString()+"#"+score.getStatusSpecificQuestions().getQuestionId());
																//System.out.println("teacherDetail=="+teacherDetail.getTeacherId()+" jobOrder==="+jobOrder.getJobId()+"  jobOrder.getDistrictMaster()====="+jobOrder.getDistrictMaster().getDistrictId()+"  userMaster===="+score.getUserMaster().getUserId() +"   Long.parseLong(queId===="+Long.parseLong(queId+""));
																//System.out.println("sss-------"+sss.getMaxScore()+"-------------------------"+sss.getScoreProvided()+"----------------------==="+sss.getUserMaster().getUserId()+"    "+sss.getQuestion());
															if(sss!=null && sss.getMaxScore()!=null && sss.getMaxScore()>0){
																iCountScoreSlider++;
																if(sss.getFinalizeStatus()==1)
																	iQdslider=0;
																else
																	iQdslider=1;
																sb.append("<div class='row mt10' style='padding-left: 22px;'>");
																sb.append("<div class='span5 slider_question' style='float:left;'><iframe id=\"ifrmQuestion_"+iCountScoreSlider+"\" src=\"slideract.do?name=questionFrm&tickInterval="+tickInterval+"&max="+sss.getMaxScore()+"&dslider="+iQdslider+"&swidth="+scoreWidth+"&step=1&answerId="+sss.getAnswerId()+"&svalue="+sss.getScoreProvided()+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:"+scoreWidth1+"px;margin-top:-11px;\"></iframe></div>");
																if(userMaster.getEntityType()==2){
																	sb.append("<div style='float:right;'><a class='questionScore' id='question"+score.getStatusSpecificQuestions().getQuestionId()+"' title='' data-original-title='' data-placement='above' href='javascript:void(0);' onclick='clickOnQuestionScore(this);'><span  class='fa-crosshairs icon-large iconcolor' ></span></a></div>");
																	//for all User Score
																	sb.append("<div id='scoreTablequestion"+score.getStatusSpecificQuestions().getQuestionId()+"' style='display:none;'>");
																	////System.out.println("teacherDetail=="+teacherDetail+" jobOrder=="+jobOrder+" statusMaster=="+statusMaster+" secondaryStatus=="+secondaryStatus);
																	println("qId=--------------------------------==="+score.getStatusSpecificQuestions().getQuestionId()+"    value===---------------------=="+getScoreHtmlTableMap.get(String.valueOf(score.getStatusSpecificQuestions().getQuestionId())));
																	sb.append(getScoreHtmlTableMap.get(String.valueOf(score.getStatusSpecificQuestions().getQuestionId())));
																	sb.append("</div>");
																	//end all User Score
																	}
																sb.append("</div>");
															}
															}
															queId=0;
							
															println(" >>>>>>>>>>>>>>> Question >>>>>>>>>>>>>>>>> "+score.getQuestion());
															boolean flagforfilledQuestionOrNot=false;
															if(score.getQuestion()!=null && !score.getQuestion().equals(""))
															{
																List<TeacherStatusNotes> listTeacherStatusNotes = new ArrayList<TeacherStatusNotes>();
																
																String statusNote = "";
																//int queId=0;
																if(score.getStatusSpecificQuestions()!=null){
																	queId=score.getStatusSpecificQuestions().getQuestionId();
																}
																println("hhhhh====================================================="+Long.parseLong(0+""));
																//teacherStatusNotes = teacherStatusNotesDAO.getQuestionNoteObjByUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), Long.parseLong(queId+""),userMaster);
																listTeacherStatusNotes = teacherStatusNotesDAO.getQuestionNoteObjBySuperUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), Long.parseLong(queId+""),score.getUserMaster());
							
																//@Ashish :: add Note for every question 
																if(thisUser || scoreShowOrNot){
																sb.append("<br/><div class='row mt5'>");
																sb.append("<div class='inner_disable_jqte' style='margin-left:22px;' id='questionNotes"+queId+"' >");																
																sb.append("<label >Note: </label>");
																}
																if(listTeacherStatusNotes!=null)
																println("listTeacherStatusNotes====================="+listTeacherStatusNotes.size());
																if(listTeacherStatusNotes!=null && listTeacherStatusNotes.size()>0){
																for(TeacherStatusNotes teacherStatusNotes:listTeacherStatusNotes){
																if(teacherStatusNotes!=null)
																	statusNote = teacherStatusNotes.getStatusNotes();
																sb.append("<br/><label>("+score.getUserMaster().getFirstName()+" "+score.getUserMaster().getLastName()+","+DATE_TIME_FORMAT.format(teacherStatusNotes.getCreatedDateTime())+")</label>");
																sb.append("<div>");								
																	if(teacherStatusNotes.isFinalizeStatus()){
																		sb.append("<textarea class='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='1' style='pointer-event:none;'>"+statusNote+"</textarea>");
																		if(thisUser){flagforfilledQuestionOrNot=true;}
																	}else
																		sb.append("<textarea class='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='1' style='pointer-event:none;'>"+statusNote+"</textarea>");									
																sb.append("<div id='main"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'>");
																sb.append("<a "+clickable+" class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"' rel='tooltip' href='javascript:void(0);' id='edit_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' onclick='editTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/edit-notes.png' width='25px' height='30px;'/></a>");
																sb.append("<br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("lblSaveNote", locale)+"' rel='tooltip' href='javascript:void(0);' id='save_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='saveTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/save-notes.png' width='25px' height='30px;'/></a>");
																sb.append("<br/><br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("btnClr", locale)+"' rel='tooltip' href='javascript:void(0);' id='cancel_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='cancelTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/cancel-notes.jpg' width='25px' height='30px;'/></a><br/>");
																sb.append("</div>");
																sb.append("<br/></div>");
																}
																}
																if(thisUser){																	
																int isJSI=0;
																String allInfoAboutNote="'"+teacherDetail.getTeacherId()+","+jobOrder.getJobId()+","+statusId+","+secondaryStatusId+","+userMaster.getUserId()+","+jobOrder.getDistrictMaster().getDistrictId()+","+ queId+","+isJSI+"'";
																sb.append("<label id='label"+queId+"' style='width:100%;'>("+score.getUserMaster().getFirstName()+" "+score.getUserMaster().getLastName()+")</label>");
																sb.append("<div class='inner_enable_jqte'>");
																sb.append("<textarea id='questionNotes"+queId+"' name='questionNotes"+queId+"' class='span6' rows='2'></textarea>");									
																sb.append("<div style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'><a "+clickable+" class='tooltipforicon' data-original-title='Add' rel='tooltip' href='javascript:void(0);' onclick=\"addTeacherStatusNotes(this,"+queId+","+allInfoAboutNote+");\"><img src='images/add_notes.png' width='25px' height='30px;'/></a></div>");
																sb.append("<br/></div>");
																}																
																sb.append("</div>");
																sb.append("</div>");
																if(thisUser){
																if(!flagforfilledQuestionOrNot)
																if(!qqNote.equals(""))
																	qqNote = qqNote+"#"+"questionNotes"+queId;
																else
																	qqNote = "questionNotes"+queId;
																}
															}
															if(thisUser){
															if(!flagforfilledQuestionOrNot && !(""+queId).equals("0"))
															if(questionAssessmentIds!=null && !questionAssessmentIds.equals(""))
																questionAssessmentIds = questionAssessmentIds+"#"+queId+"";
															else
																questionAssessmentIds = queId+"";
							
															questionNumber++;	
															}
														}
														
														if(questionNumber>1)
														{
															sb.append("<div class='row mt10'></div>");
														}
														sb.append("</td></tr>");
					/*****************************************************************YYYYYYYYYYYYYY******************************************************/								
						}else{
							alreadyShowQuestion.clear();
							sb.append("<tr><td align=left>");
							int questionNumber=1;
							int iQuestionCount=0;		
							boolean questionShowOrNot=false;
							for(StatusSpecificScore score:lstStatusSpecificScore)
							{
								boolean firtQustionSliderShow=false;								
								boolean thisUser=score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString());
								if(score.getQuestion()!=null && !score.getQuestion().equalsIgnoreCase("") && alreadyShowQuestion.get(score.getStatusSpecificQuestions().getQuestionId())==null)
								{
									questionShowOrNot=manageStatusAjax.showOrNot(lstStatusSpecificScore,userMaster,score.getStatusSpecificQuestions(),getAllNotesByUserMap);
									if(questionShowOrNot){
									iQuestionCount++;
									sb.append("<div class='row question_padding_left '><span><label class='labletxt'><B>"+Utility.getLocaleValuePropByKey("headQues", locale)+" "+iQuestionCount+":</B> "+score.getQuestion()+"</label></span></div>");
									if(score.getQuestion()!=null && !score.getQuestion().equals("") && score.getSkillAttributesMaster()!=null){
										sb.append("<div class='row question_padding_left'><span><label  class='lableAtxt'><B>"+Utility.getLocaleValuePropByKey("lnkAtt", locale)+":</B> "+score.getSkillAttributesMaster().getSkillName()+"</label></span></div>");
										alreadyShowAttribute.put(score.getStatusSpecificQuestions().getQuestionId(), score.getSkillAttributesMaster().getSkillName());
									}
									alreadyShowQuestion.put(score.getStatusSpecificQuestions().getQuestionId(), score.getQuestion());
									firtQustionSliderShow=true;
									}									
								}
								println("--------------------------------------------------"+questionShowOrNot+"---------------------------------------------");
								Double iQuestionMaxScore=0.0;
								if(score.getMaxScore()!=null){
									iQuestionMaxScore=score.getMaxScore();
								}
								int tickInterval=1;

								if(iQuestionMaxScore%10==0)
									tickInterval=10;
								else if(iQuestionMaxScore%5==0)
									tickInterval=5;
								else if(iQuestionMaxScore%3==0)
									tickInterval=3;
								else if(iQuestionMaxScore%2==0)
									tickInterval=2;
								
								
								if(alreadyShowAttribute.get(score.getStatusSpecificQuestions().getQuestionId())==null && (score.getQuestion()==null || score.getQuestion().equals("")) && score.getSkillAttributesMaster()!=null && thisUser){
									sb.append("<div class='row question_padding_left'><span><label  class='lableAtxt'><B>"+Utility.getLocaleValuePropByKey("lblAttribute1", locale)+":</B> "+score.getSkillAttributesMaster().getSkillName()+"</label></span></div>");
									firtQustionSliderShow=true;
								}
								
								
								int queId=0;
								if(score.getQuestion()!=null && !score.getQuestion().equals("") && score.getStatusSpecificQuestions()!=null){
									queId=score.getStatusSpecificQuestions().getQuestionId();
								}
								
								boolean scoreShowOrNot=false;
								try{
								if(!thisUser){
								println("teacherDetail=="+teacherDetail.getTeacherId()+" jobOrder==="+jobOrder.getJobId()+"  jobOrder.getDistrictMaster()====="+jobOrder.getDistrictMaster().getDistrictId()+"  userMaster===="+score.getUserMaster().getUserId() +"   Long.parseLong(queId===="+Long.parseLong(queId+""));
								List<TeacherStatusNotes> listTSN=teacherStatusNotesDAO.getQuestionNoteObjBySuperUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), Long.parseLong(queId+""),score.getUserMaster());
								println("listTSN==========hhhhhhhhhhhhhhhhhhhhh======:::::::::"+listTSN);
								if(listTSN!=null && listTSN.size()>0){
									println("listTSN==========hhhhhhhhhhhhhhhhhhhhh======:::::::::"+listTSN.size());
									scoreShowOrNot=listTSN.get(0).isFinalizeStatus();
								}
								}
								}catch(Exception e){e.printStackTrace();}
								
								if(firtQustionSliderShow){
									StatusSpecificScore sss=mapSSS.get(userMaster.getUserId().toString()+"#"+score.getStatusSpecificQuestions().getQuestionId());
									//System.out.println("teacherDetail=="+teacherDetail.getTeacherId()+" jobOrder==="+jobOrder.getJobId()+"  jobOrder.getDistrictMaster()====="+jobOrder.getDistrictMaster().getDistrictId()+"  userMaster===="+score.getUserMaster().getUserId() +"   Long.parseLong(queId===="+Long.parseLong(queId+""));
									//System.out.println("sss-------"+sss.getMaxScore()+"-------------------------"+sss.getScoreProvided()+"----------------------==="+sss.getUserMaster().getUserId()+"    "+sss.getQuestion());
								if(sss!=null && sss.getMaxScore()!=null && sss.getMaxScore()>0){
									iCountScoreSlider++;
									if(sss.getFinalizeStatus()==1)
										iQdslider=0;
									else
										iQdslider=1;									
									sb.append("<div class='row mt10' style='padding-left: 22px;'>");
									sb.append("<div class='span5 slider_question' style='float:left;'><iframe id=\"ifrmQuestion_"+iCountScoreSlider+"\" src=\"slideract.do?name=questionFrm&tickInterval="+tickInterval+"&max="+sss.getMaxScore()+"&dslider="+iQdslider+"&swidth="+scoreWidth+"&step=1&answerId="+sss.getAnswerId()+"&svalue="+sss.getScoreProvided()+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:"+scoreWidth1+"px;margin-top:-11px;\"></iframe></div>");
									if(userMaster.getEntityType()==2){
										sb.append("<div style='float:right;'><a class='questionScore' id='question"+score.getStatusSpecificQuestions().getQuestionId()+"' title='' data-original-title='' data-placement='above' href='javascript:void(0);' onclick='clickOnQuestionScore(this);'><span  class='fa-crosshairs icon-large iconcolor' ></span></a></div>");
										//for all User Score
										sb.append("<div id='scoreTablequestion"+score.getStatusSpecificQuestions().getQuestionId()+"' style='display:none;'>");
										//System.out.println("teacherDetail=="+teacherDetail+" jobOrder=="+jobOrder+" statusMaster=="+statusMaster+" secondaryStatus=="+secondaryStatus);
										println("qId=--------------------------------==="+score.getStatusSpecificQuestions().getQuestionId()+"    value===---------------------=="+getScoreHtmlTableMap.get(String.valueOf(score.getStatusSpecificQuestions().getQuestionId())));
										sb.append(getScoreHtmlTableMap.get(String.valueOf(score.getStatusSpecificQuestions().getQuestionId())));
										sb.append("</div>");
										//end all User Score
										}
									sb.append("</div>");									
								}
								}
								 queId=0;

								println(" >>>>>>>>>>>>>>> Question >>>>>>>>>>>>>>>>> "+score.getQuestion());
								boolean flagforfilledQuestionOrNot=false;
								if(score.getQuestion()!=null && !score.getQuestion().equals(""))
								{
									println("Entry ----------------->>>>>>>> score==="+score.getQuestion());
									//TeacherStatusNotes teacherStatusNotes = new TeacherStatusNotes();
									List<TeacherStatusNotes> listTeacherStatusNotes = null;

									String statusNote = "";
									//int queId=0;
									if(score.getStatusSpecificQuestions()!=null){
										queId=score.getStatusSpecificQuestions().getQuestionId();
									}									
									println("teacherDetail=="+teacherDetail.getTeacherId()+" jobOrder==="+jobOrder.getJobId()+"  jobOrder.getDistrictMaster()====="+jobOrder.getDistrictMaster().getDistrictId()+"  userMaster===="+score.getUserMaster().getUserId() +"   Long.parseLong(queId===="+Long.parseLong(queId+""));
									//teacherStatusNotes = teacherStatusNotesDAO.getQuestionNoteObjByUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), Long.parseLong(queId+""),score.getUserMaster());
									listTeacherStatusNotes = teacherStatusNotesDAO.getQuestionNoteObjBySuperUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), Long.parseLong(queId+""),score.getUserMaster());
									
									//@Ashish :: add Note for every question 
									sb.append("<div class='row mt5'>");
									sb.append("<div style='margin-left:22px;' id='questionNotes"+queId+"' >");
									if(questionShowOrNot)
									if(thisUser || scoreShowOrNot){
									sb.append("<label >"+Utility.getLocaleValuePropByKey("blNote", locale)+"</label>");
									}
									
									if(listTeacherStatusNotes!=null){
									for(TeacherStatusNotes teacherStatusNotes:listTeacherStatusNotes){
									if(teacherStatusNotes!=null)
									println(teacherStatusNotes.isFinalizeStatus()+"teacherStatusNotes===yyyyyyyyyyyyyyyyyyyyyyyyyy==="+teacherStatusNotes.getStatusNotes());
									if(teacherStatusNotes!=null)
										statusNote = teacherStatusNotes.getStatusNotes();
									sb.append("<br/><label>("+score.getUserMaster().getFirstName()+" "+score.getUserMaster().getLastName()+","+DATE_TIME_FORMAT.format(teacherStatusNotes.getCreatedDateTime())+")</label>");
									println("finialae=================================="+teacherStatusNotes.isFinalizeStatus());
									if(teacherStatusNotes.isFinalizeStatus()){
										sb.append("<div class='' style='background-color:#EEEEEE; width:585px; height:50px; border:1px solid #CCCCCC; overflow:scroll; '>"+statusNote+"</div>");
										if(thisUser){flagforfilledQuestionOrNot=true;}
									}else
										if(thisUser){
											sb.append("<textarea id='questionNotes"+queId+"' name='questionNotes"+queId+"' class='span6' rows='2'>"+statusNote+"</textarea>");
											}																
									}
									}
									else{
										if(thisUser){
										sb.append("<br/><label>("+score.getUserMaster().getFirstName()+" "+score.getUserMaster().getLastName()+")</label>");
										sb.append("<textarea id='questionNotes"+queId+"' name='questionNotes"+queId+"' class='span6' rows='2'>"+statusNote+"</textarea>");
										}
									}
									

									sb.append("</div>");
									sb.append("</div>");
									if(thisUser){
									if(!flagforfilledQuestionOrNot)
									if(!qqNote.equals(""))
										qqNote = qqNote+"#"+"questionNotes"+queId;
									else
										qqNote = "questionNotes"+queId;
									}
								}
								//System.out.println("queId=========          "+queId);
								if(thisUser){
								if(!flagforfilledQuestionOrNot && !(""+queId).equals("0"))								
								if(questionAssessmentIds!=null && !questionAssessmentIds.equals(""))									
									questionAssessmentIds = questionAssessmentIds+"#"+queId+"";
								else									
									questionAssessmentIds = queId+"";

								questionNumber++;
								}
							}
							if(questionNumber>1)
							{
								sb.append("<div class='row mt10'></div>");
							}
							sb.append("</td></tr>");
							
						}					
					}
				}
				int isRequiredSchoolIdCheck=0;
				int alreadyExistsSchoolInStatus=0;
				String schoolNameValue="";
				String schoolIdValue="";
				println("listSIJO size========================================================================="+listSIJO.size());
				Boolean checkFlagForOFFER_READY=null;
				if(jobOrder.getDistrictMaster()!=null){
				}
				if(checkFlagForOFFER_READY!=null && checkFlagForOFFER_READY){
					/*Boolean schoolInputBoxHideOnStatus=SCH_INPUTBOX_HIDE.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+recentStatusName.trim());
					if(schoolInputBoxHideOnStatus!=null && schoolInputBoxHideOnStatus){
						isRequiredSchoolIdCheck=0;
						}else*/
					isRequiredSchoolIdCheck=1;
					println("Enter================");
					if(listSIJO!=null && listSIJO.size()==1){
						alreadyExistsSchoolInStatus=1;
						SchoolInJobOrder sijo=listSIJO.get(0);
						schoolNameValue = sijo.getSchoolId().getSchoolName().replace(" ", "!!");
						schoolIdValue=sijo.getSchoolId().getSchoolId().toString();
						println(schoolIdValue+"================nnnnnnn============================="+schoolNameValue);
					}
					if(jobForTeacherObj.getSchoolMaster()!=null){
						alreadyExistsSchoolInStatus=1;
						schoolNameValue = jobForTeacherObj.getSchoolMaster().getSchoolName().replace(" ", "!!");
						schoolIdValue=jobForTeacherObj.getSchoolMaster().getSchoolId().toString();
						println(schoolIdValue+"===================yyyyyyy=========================="+schoolNameValue);
					}
				}
				Boolean isAlwaysRequiredSchool=null;
				if(isAlwaysRequiredSchool!=null && isAlwaysRequiredSchool){
				isSchoolOnJobId=1;
				}
				
				sb.append("<input type='hidden' id='qqNoteIds' value='"+qqNote+"'>");
				//sb.append("<input type='hidden' id='questionDetails' value='"+questionDetails+"'>");
				sb.append("<input type='hidden' id='questionAssessmentIds' value='"+questionAssessmentIds+"'>");
				sb.append("<input type='hidden' name='iQdslider' id='iQdslider' value="+iQdslider+">");

				sb.append("<input type='hidden' name='iCountScoreSlider' id='iCountScoreSlider' value="+iCountScoreSlider+">");
				sb.append("<input type='hidden' name='isQuestionEnable' id='isQuestionEnable' value="+isQuestionEnable+">");
				sb.append("<input type='hidden' name='defauntQuestionNote' id='defauntQuestionNote' value="+defauntQuestionNote+">");
				sb.append("<input type='hidden' name='requiredNotesForUno' id='requiredNotesForUno' value="+requiredNotesForUno+">");
				sb.append("<input type='hidden' name='isSchoolOnJobId' id='isSchoolOnJobId' value="+isSchoolOnJobId+">");
				
				sb.append("<input type='hidden' name='isRequiredSchoolIdCheck' id='isRequiredSchoolIdCheck' value="+isRequiredSchoolIdCheck+">");
				sb.append("<input type='hidden' name='alreadyExistsSchoolInStatus' id='alreadyExistsSchoolInStatus' value="+alreadyExistsSchoolInStatus+">");
				sb.append("<input type='hidden' name='schoolNameValue' id='schoolNameValue' value="+schoolNameValue+">");
				sb.append("<input type='hidden' name='schoolIdValue' id='schoolIdValue' value="+schoolIdValue+">");
				sb.append("<input type='hidden' name='evaluationCompleteDiv' id='evaluationCompleteDiv' value="+(jobForTeacherObj.getOfferReady()!=null?1:0)+">");
				sb.append("<input type='hidden' name='isSuperAdministration' value="+(isSuperAdminUser?1:0)+">");
				sb.append("<input type='hidden' name='slcStartDateOnOrOff' value='ON'>");
				try{
				sb.append("<input type='hidden' name='isStartDate' value="+(jobForTeacherObj.getStartDate()!=null?new SimpleDateFormat("MM-dd-yyyy").format(jobForTeacherObj.getStartDate()):"")+">");
				System.out.println("starut============="+jobForTeacherObj.getStartDate()+"    "+jobForTeacherObj.getJobForTeacherId());
				}catch(Exception e){e.printStackTrace();}
				
				
				
				//System.out.println("isRequiredSchoolIdCheck==="+isSchoolOnJobId+"====yyyyyyyyyyyyyyyyy========"+isRequiredSchoolIdCheck);
				// End ... Display Question list from statusSpecificScore

				/*
				 * 		Dated	:	28-07-2014
				 * 		Purpose	:	Add Transcript Portion.
				 * 		By		:	Hanzala Subhani
				 * 		Version	:	1.0
				 * 
				 * */

				Integer tId = null;
				Integer eId = null;
				Integer jId = null;
				if(jobForTeacherObj!=null){
					tId		=	jobForTeacherObj.getTeacherId().getTeacherId();
					eId		=	5;
					jId		=	jobForTeacherObj.getJobId().getJobId();
				}
				boolean smartPractices=false;
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
					if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
						smartPractices=true;
					}
				}
				if(smartPractices && recentStatusName != null && recentStatusName.equalsIgnoreCase("Restart Workflow")){
						System.out.println("::::::::::::::::::::::::::Restart Workflow::::::::::::::::::::::::");
						WorkFlowOptionsForStatus flowOptionsForStatus=flowOptionsForStatusDAO.findOptionsByTeacherStatus(teacherDetail, jobOrder, statusMaster, secondaryStatus);
						String flowOptionIds="";
						if(flowOptionsForStatus!=null){
							flowOptionIds=flowOptionsForStatus.getFlowOptionIds();
						}
						
						sb.append("<tr><td>");
						sb.append("<div id='restartWorkflowGrid'>");
						sb.append("<table border='0' id='restartWorkflowTable' cellpadding=5>");
						sb.append("<tr>");
						sb.append("<th colspan=2 valign='top'>Which items which you like to restart?</th>");
						sb.append("</tr>");
						
						List<WorkFlowOptions> flowOptionsList=flowOptionsDAO.getFlowOptions();
							for (WorkFlowOptions flowOptions : flowOptionsList){
								boolean optionShow=false;
								if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferJSI()==0){
									if(flowOptions.getFlowOptionName().equalsIgnoreCase("Prescreen")){
										optionShow=false;	
									}else{
										optionShow=true;
									}
								}else{
									optionShow=true;
								}
								if(optionShow){
									String selectedFlow="";
									if(flowOptionIds!=null && flowOptionIds.contains("#"+flowOptions.getFlowOptionId()+"#")){
										selectedFlow="disabled checked";
									}
									sb.append("<tr>");
									sb.append("<td align=center valign=top>");
									sb.append("<input "+selectedFlow+" type=\"checkbox\" name=\"restartWorkflow[]\" value="+flowOptions.getFlowOptionId()+">");	
									sb.append("</td>");
									sb.append("<td valign=middle>"+flowOptions.getFlowOptionName());
									sb.append("</td>");
									sb.append("</tr>");
								}
							}
						sb.append("</table>");
						sb.append("</div>");
						sb.append("</td>");
						sb.append("</tr>");
				}else if(secondaryStatus != null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref") && isMiami){

					String StatusMaster3			=	"";
					String teacherAcademicsStatus 	= 	"";

					try{
						List<EligibilityStatusMaster> eligibilityStatusMasterList = null;
						Criterion criteria = null;
						if(eId.equals(5)){
							criteria= Restrictions.eq("formCode", "TR");
						}

						eligibilityStatusMasterList 	= 	eligibilityStatusMasterDAO.findByCriteria(criteria);
						for(EligibilityStatusMaster allEligibility:eligibilityStatusMasterList){
							StatusMaster3	=	StatusMaster3+"#$#"+allEligibility.getValidateStatus();
						}
					} catch(Exception e){
						e.printStackTrace();
					}

					try{

						/*============= For Sorting ===========*/
						int totalRecord		= 	0;
						//------------------------------------
						/*====== set default sorting fieldName ====== **/
						String sortOrderNoField		=	"attendedInYear";

						/**Start set dynamic sorting fieldName **/
						if(sortOrder!=null){
							if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("degree") && !sortOrder.equals("fieldOfStudy") && !sortOrder.equals("university")){
								sortOrderFieldName=sortOrder;
								sortOrderNoField=sortOrder;
							}
							if(sortOrder.equals("degree")){
								sortOrderNoField="degree";
							}
							if(sortOrder.equals("fieldOfStudy")){
								sortOrderNoField="fieldOfStudy";
							}
							if(sortOrder.equals("university")){
								sortOrderNoField="university";
							}
						}
						if(!sortOrderType.equals("") && !sortOrderType.equals(null))
						{
							if(sortOrderType.equals("0"))
							{
								sortOrderStrVal		=	Order.asc(sortOrderFieldName);
							}
							else
							{
								sortOrderTypeVal	=	"1";
								sortOrderStrVal		=	Order.desc(sortOrderFieldName);
							}
						}
						else
						{
							sortOrderTypeVal		=	"0";
							sortOrderStrVal			=	Order.asc(sortOrderFieldName);
						}
						/*=============== End ======================*/	

						TeacherDetail teacherDetailTrans = teacherDetail;//teacherDetailDAO.findById(tId, false, false);

						List<TeacherAcademics> listTeacherAcademics = null;
						listTeacherAcademics = teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetailTrans);
						listTeacherAcademics = teacherAcademicsDAO.findSortedAcadamicDetailByTeacher(sortOrderStrVal,teacherDetailTrans);

						List<TeacherAcademics> sortedlistTeacherAcademics	=	new ArrayList<TeacherAcademics>();

						SortedMap<String,TeacherAcademics>	sortedMap = new TreeMap<String,TeacherAcademics>();
						if(sortOrderNoField.equals("degree"))
						{
							sortOrderFieldName	=	"degree";
						}
						if(sortOrderNoField.equals("fieldOfStudy"))
						{
							sortOrderFieldName	=	"fieldOfStudy";
						}
						if(sortOrderNoField.equals("university"))
						{
							sortOrderFieldName	=	"university";
						}
						int mapFlag=2;
						for (TeacherAcademics trAcademics : listTeacherAcademics){
							String orderFieldName=trAcademics.getAttendedInYear()+"";
							if(sortOrderFieldName.equals("degree")){
								orderFieldName=trAcademics.getDegreeId().getDegreeName()+"||"+trAcademics.getAcademicId();
								sortedMap.put(orderFieldName+"||",trAcademics);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
							if(sortOrderFieldName.equals("fieldOfStudy")){
								orderFieldName=trAcademics.getFieldId().getFieldName()+"||"+trAcademics.getAcademicId();
								sortedMap.put(orderFieldName+"||",trAcademics);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
							if(sortOrderFieldName.equals("university")){
								orderFieldName=trAcademics.getUniversityId().getUniversityName()+"||"+trAcademics.getAcademicId();
								sortedMap.put(orderFieldName+"||",trAcademics);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
						}
						if(mapFlag==1){
							NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
							for (Iterator iter=navig.iterator();iter.hasNext();) {  
								Object key = iter.next(); 
								sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
							} 
						}else if(mapFlag==0){
							Iterator iterator = sortedMap.keySet().iterator();
							while (iterator.hasNext()) {
								Object key = iterator.next();
								sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
							}
						}else{
							sortedlistTeacherAcademics=listTeacherAcademics;
						}

						totalRecord = sortedlistTeacherAcademics.size();

						if(totalRecord<end)
							end=totalRecord;
						List<TeacherAcademics> listsortedTeacherCertificates	=	sortedlistTeacherAcademics.subList(start,end);

						sb.append("<tr><td>");
						sb.append("<div id='academicGridAcademicGrid'>");
						sb.append("<table border='0' id='academicGridAcademic' style=\"margin-right:0px;\">");
						sb.append("<thead class='bg'>");

						sb.append("<tr>");
						sb.append("<th width='20%' valign='top'>&nbsp;</th>");

						sb.append("<th width='20%' valign='top'>"+Utility.getLocaleValuePropByKey("lblSchool", locale)+"</th>");

						sb.append("<th width='14%' valign='top'>"+Utility.getLocaleValuePropByKey("lblDegreeConferredDate", locale)+"</th>"); 

						sb.append("<th width='15%' valign='top'>"+Utility.getLocaleValuePropByKey("lblDegree", locale)+"</th>");

						sb.append("<th width='12%' valign='top'>"+Utility.getLocaleValuePropByKey("lblFieldOfStudy", locale)+"</th>");

						sb.append("<th width='10%' class='net-header-text' valign='top'>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");

						sb.append("</tr>");

						sb.append("</thead>");
						if(listTeacherAcademics!=null)
							for(TeacherAcademics ta:listsortedTeacherCertificates)
							{

								Long academicId	=	ta.getAcademicId();
								sb.append("<tr>");

								sb.append("<td>");
								sb.append("<input "+clickable+" type=\"checkbox\" name=\"transcript_data[]\" onclick='return showAcademicQues()' value="+academicId+">");	
								sb.append("</td>");			

								sb.append("<td>");
								if(ta.getUniversityId() != null){
									sb.append(ta.getUniversityId().getUniversityName());
								} else {
									sb.append("");
								}
								sb.append("</td>");

								sb.append("<td>");
								String status	=	ta.getStatus()==null?"":ta.getStatus();
								String dateShow	=	Utility.convertDateAndTimeToDatabaseformatOnlyDate(ta.getDegreeConferredDate());
								dateShow		=	dateShow==null?"":dateShow;
								if(ta.getDegreeConferredDate() != null){

									if(status.equals("V") && ta.getDegreeConferredDate() != null){
										sb.append("<input style=\"width:82px;\" type=\"text\" placeholder='mm-dd-yyyy' id=\"graduationDate_"+ta.getAcademicId()+"\" name=\"graduationDate_"+ta.getAcademicId()+"\"  maxlength=\"10\" value="+dateShow+" readonly=\"readonly\" class=\"help-inline form-control_1\">");
									} else {
										sb.append("<input style=\"width:82px;\" type=\"text\" placeholder='mm-dd-yyyy' id=\"graduationDate_"+ta.getAcademicId()+"\" name=\"graduationDate_"+ta.getAcademicId()+"\"  maxlength=\"10\" value="+dateShow+" class=\"help-inline form-control_1\">");
									}
								} else {
									if(status.equals("V") && ta.getDegreeConferredDate() != null){
										sb.append("<input style=\"width:82px;\" type=\"text\" placeholder='mm-dd-yyyy' id=\"graduationDate_"+ta.getAcademicId()+"\" name=\"graduationDate_"+ta.getAcademicId()+"\"  maxlength=\"10\" readonly=\"readonly\" class=\"help-inline form-control_1\">");
									} else {
										sb.append("<input style=\"width:82px;\" type=\"text\" placeholder='mm-dd-yyyy' id=\"graduationDate_"+ta.getAcademicId()+"\" name=\"graduationDate_"+ta.getAcademicId()+"\"  maxlength=\"10\" class=\"help-inline form-control_1\">");
									}
								}
								if(!status.equals("V")){
									sb.append("&nbsp;&nbsp;<a "+clickable+" href=\"#\" onClick=\"canleGraduationDate("+ta.getAcademicId()+")\"><img src=\"images/red_cancel.png\"></a>");
								}

								if(!status.equals("V") || ta.getDegreeConferredDate() == null){
								}
								sb.append("</td>");

								if(ta.getDegreeId()!=null && ta.getDegreeId().getDegreeName()!=null){
									sb.append("<td>");
									sb.append(ta.getDegreeId().getDegreeName());
									sb.append("</td>");
								} else {
									sb.append("<td>&nbsp;</td>");
								}
								
								if(ta.getFieldId()!=null && ta.getFieldId().getFieldName()!=null){
									sb.append("<td>");
									sb.append(ta.getFieldId().getFieldName());	
									sb.append("</td>");
								} else {
									sb.append("<td>&nbsp;</td>");
								}

								sb.append("<td>");
								teacherAcademicsStatus	=	teacherAcademicsStatus+","+status;
								if(status == null || status.equals("") || status.equals("P")){
									status = Utility.getLocaleValuePropByKey("lblPending", locale);
								} else if(status.equals("V")){
									status = Utility.getLocaleValuePropByKey("lblVerified", locale);
								} else if(status.equals("F")){
									status = Utility.getLocaleValuePropByKey("lblFailed", locale);
								} else if(status.equals("W")){
									status = Utility.getLocaleValuePropByKey("lblWaived", locale);
								}
								sb.append(status);
								//System.out.println(" ::::: H ::::: "+teacherAcademicsStatus);
								sb.append("</td>");
								sb.append("</tr>");
							}
						if(listTeacherAcademics==null || listTeacherAcademics.size()==0) {
							sb.append("<tr>");
							sb.append("<td colspan='7'>");
							sb.append(""+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"");
							sb.append("</td>");
							sb.append("</tr>");
						}
						sb.append("</table>");
						sb.append("</div>");

						String teacherFName			= 	jobForTeacherObj.getTeacherId().getFirstName()==null?"":jobForTeacherObj.getTeacherId().getFirstName();
						String teacherLName 		= 	jobForTeacherObj.getTeacherId().getLastName()==null?"":jobForTeacherObj.getTeacherId().getLastName();
						String secondaryStatusName	=	secondaryStatus.getSecondaryStatusName()==null?"":secondaryStatus.getSecondaryStatusName();


						System.out.println("::: fitScore :::::"+fitScore);
						String teacherName	=	teacherFName+teacherLName;
						teacherName			=	teacherName.replace("'","\\'");
						secondaryStatusName	=	secondaryStatusName.replace("'","\\'");

						sb.append("<table border='0' id='academicGridAcademicNotes' cellspacing='3' cellpadding='3' style='display:none;'>");
						sb.append("<tr><td colspan=\"2\">&nbsp;</td></tr>");

						sb.append("<input type=\"hidden\" id=\"jsiFileNameTrans\" name=\"jsiFileNameTrans\" value=\"\" />");
						sb.append("<input type=\"hidden\" id=\"teacherIdTrans\" name=\"teacherIdTrans\" value='"+tId+"' />");
						sb.append("<input type=\"hidden\" id=\"eligibilityIdTrans\" name=\"eligibilityIdTrans\" value='"+eId+"' />");
						sb.append("<input type=\"hidden\" id=\"jobIdTrans\" name=\"jobIdTrans\" value='"+jId+"'/>");
						sb.append("<input type=\"hidden\" id=\"formId\" name=\"formId\" value=\"\" />");
						sb.append("<input type=\"hidden\" name=\"StatusMaster3\" id=\"StatusMaster3\" value="+StatusMaster3+">");
						sb.append("<input type=\"hidden\" name=\"teacherFName\" id=\"teacherFName\" value="+teacherFName+">");
						sb.append("<input type=\"hidden\" name=\"teacherLName\" id=\"teacherLName\" value="+teacherLName+">");
						sb.append("<input type=\"hidden\" name=\"secondaryStatus\" id=\"secondaryStatus\" value="+secondaryStatusName+">");
						sb.append("<input type=\"hidden\" name=\"fitScore\" id=\"fitScore\" value="+fitScore+">");
						sb.append("<input type=\"hidden\" name=\"statusId\" id=\"statusId\" value="+statusId+">");
						sb.append("<input type=\"hidden\" name=\"secondaryStatusId\" id=\"secondaryStatusId\" value="+secondaryStatusId+">");

						sb.append("<input type=\"hidden\" name=\"teacherAcademicsStatus\" id=\"teacherAcademicsStatus\" value="+teacherAcademicsStatus+">");

						sb.append("<tr>");

						sb.append("<td>&nbsp;</td>");
						sb.append("<td>");

						sb.append("<button class='btn btn-primary' type='button' onclick='return saveTransAcademic(22)'>"+Utility.getLocaleValuePropByKey("lblVerified", locale)+"</button>&nbsp;&nbsp;");
						sb.append("<button class='btn btn-primary' type='button' onclick='return saveTransAcademic(23)'>"+Utility.getLocaleValuePropByKey("lblFailed", locale)+"</button>&nbsp;&nbsp;");
						sb.append("<button class='btn btn-primary' type='button' onclick='return saveTransAcademic(24)'>"+Utility.getLocaleValuePropByKey("lblWaived", locale)+"</button>&nbsp;&nbsp;");
						sb.append("</td>");

						sb.append("</tr>");

						sb.append("</table>");

					}
					catch (Exception e){
						e.printStackTrace();
					}
					/*	End Transcript Section */
				} else if(secondaryStatus != null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("e-References")){
					// Reference Check Grid
					try{

						districtMaster	=	districtMaster == null?jobForTeacherObj.getJobId().getDistrictMaster():districtMaster;
						headQuarterMaster = headQuarterMaster ==null ? jobForTeacherObj.getJobId().getHeadQuarterMaster():headQuarterMaster;
						List<DistrictSpecificRefChkQuestions> districtSpecificQuestionList	=  null;
						Integer numberOfQuestion = null;
						try{
							 
							districtSpecificQuestionList	=	districtSpecificRefChkQuestionsDAO.getDistrictRefChkQuestionBYDistrict(districtMaster,headQuarterMaster);
							numberOfQuestion 				=	districtSpecificQuestionList.size();
						} catch(Exception exception){
							exception.printStackTrace();
						}

							
						List<DistrictPortfolioConfig> districtPortfolioConfig 	= 	null;
						DistrictPortfolioConfig portConfig 						= 	null;
						districtPortfolioConfig = 	districtPortfolioConfigDAO.getPortfolioConfigsByDistrictAndCandidate(districtMaster,"I");
						Integer requiredReference = 0;
						try{
							if(districtPortfolioConfig!=null && districtPortfolioConfig.size()>0){
								portConfig = districtPortfolioConfig.get(0);
								requiredReference = portConfig.getReference();
							}
						}catch(Exception exception){
							exception.printStackTrace();
						}
						
						
						/*============= For Sorting ===========*/
						int totalRecord		= 	0;
						int topMaxScore		=	0;
						//------------------------------------
						/*====== set default sorting fieldName ====== **/
						String sortOrderNoField		=	"createdDateTime";
						String sortOrderFieldName1	=	"createdDateTime";

						/**Start set dynamic sorting fieldName **/
						if(sortOrder!=null){
							if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("firstName") && !sortOrder.equals("email") && !sortOrder.equals("contactnumber")){
								sortOrderFieldName1=sortOrder;
								sortOrderNoField=sortOrder;
							}
							if(sortOrder.equals("firstName")){
								sortOrderNoField="firstName";
							}
							if(sortOrder.equals("email")){
								sortOrderNoField="email";
							}
							if(sortOrder.equals("contactnumber")){
								sortOrderNoField="contactnumber";
							}
						}
						if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
							if(sortOrderType.equals("0")) {
								sortOrderStrVal		=	Order.asc(sortOrderFieldName1);
							} else {
								sortOrderTypeVal	=	"1";
								sortOrderStrVal		=	Order.desc(sortOrderFieldName1);
							}
						} else {
							sortOrderTypeVal		=	"0";
							sortOrderStrVal			=	Order.asc(sortOrderFieldName1);
						}
						/*=============== End ======================*/	

						Criterion criteria= Restrictions.eq("teacherDetail", jobForTeacherObj.getTeacherId());
						//List<TeacherElectronicReferences> teacherElectronicReferenceList = teacherElectronicReferencesDAO.findByCriteria(criteria);
						//List<TeacherElectronicReferences> teacherElectronicReferenceList = teacherElectronicReferencesDAO.findByTeacher(jobForTeacherObj.getTeacherId());
						List<TeacherElectronicReferences> teacherElectronicReferenceList = teacherElectronicReferencesDAO.findActiveContactedReferencesByTeacher(teacherDetail);
						totalRecord =teacherElectronicReferenceList.size();
						if(totalRecord<end)
							end=totalRecord;

						List<TeacherElectronicReferences> teacherElectronicReferenceListMain = teacherElectronicReferenceList.subList(start,end);
						Map<String,TeacherElectronicReferencesHistory> mapRefHistory = new HashMap<String, TeacherElectronicReferencesHistory>();
						// Create Map
						List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistoryList = null;
						List<TeacherElectronicReferencesHistory> countContactStatusRefChkCount 			= null;
						List<TeacherElectronicReferencesHistory> countReplyStatusRefChkCount 			= null;
						Integer refChkSize 	= 	0;
						Integer contactSize	=	0;
						Integer replySize	=	0;

						try{
							teacherElectronicReferencesHistoryList	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByTeacherId(districtMaster, jobForTeacherObj.getTeacherId());
							countContactStatusRefChkCount			=	teacherElectronicReferencesHistoryDAO.countContactStatusRefChk(headQuarterMaster , districtMaster, jobForTeacherObj.getTeacherId());
							countReplyStatusRefChkCount				=	teacherElectronicReferencesHistoryDAO.countReplyStatusRefChk(headQuarterMaster ,districtMaster, jobForTeacherObj.getTeacherId());

							if(teacherElectronicReferencesHistoryList != null){
								refChkSize	=	teacherElectronicReferencesHistoryList.size();
								for (TeacherElectronicReferencesHistory refRecord : teacherElectronicReferencesHistoryList) {
									String key = refRecord.getTeacherElectronicReferences().getElerefAutoId()+"";
									mapRefHistory.put(key, refRecord);
								}
							}

							if(countContactStatusRefChkCount != null){
								contactSize = 	countContactStatusRefChkCount.size();
							}

							if(countReplyStatusRefChkCount != null){
								replySize	=	countReplyStatusRefChkCount.size();
							}

						} catch(Exception e){}
						int actionAdd=0;
						String responseText = "";
						sb.append("<tr><td>");
						sb.append("<div id='referenceCheckGrid'>");
						sb.append("<table border='0' id='referenceCheckGrid' style=\"margin-right:0px;\">");
						sb.append("<thead class='bg'>");

						sb.append("<tr>");

						sb.append("<th valign='top'>&nbsp;</th>");
						//responseText=PaginationAndSorting.responseSortingLink("Name",sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo);

						sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblName", locale)+"</th>");
						sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblEmail", locale)+"</th>");
						sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblContNum", locale)+"</th>");
						sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
						sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblScore", locale)+"</th>");
						sb.append("<th valign='top'>Created Date Time</th>");
						if(userMaster.getEntityType()==2 || userMaster.getEntityType()==3){
							sb.append("<th valign='top'>Action</th>");
							actionAdd=1;
						}
						sb.append("</tr>");
						sb.append("</thead>");

						Map<String,TeacherElectronicReferencesHistory> mapPosNumber = new HashMap<String, TeacherElectronicReferencesHistory>();
						Integer elerefAutoId 	= 0;
						Integer questionMaxScore= 0;	
						String techerFullName	=	null;
						String candidateFullName = "";
						if(totalRecord > 0) {

							for(TeacherElectronicReferences referenceDetail:teacherElectronicReferenceListMain) {
								Integer contactStatus 	= 0;
								elerefAutoId = referenceDetail.getElerefAutoId();
								techerFullName = referenceDetail.getFirstName()+" "+referenceDetail.getLastName();
								candidateFullName = teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
								try{
									techerFullName=techerFullName.replace("'","\\'");
									candidateFullName=candidateFullName.replace("'","\\'");
								}catch(Exception e){
									e.printStackTrace();
								}

								TeacherElectronicReferencesHistory referenceHistory = 	mapRefHistory.get(elerefAutoId.toString());
								try{
									if(referenceHistory != null){
										contactStatus	=	referenceHistory.getContactStatus();
										questionMaxScore=	referenceHistory.getQuestionMaxScore();
									}
								}catch(Exception exception){
									exception.printStackTrace();
								}

								sb.append("<tr>");

								sb.append("<td class='td1'>");
								if(contactStatus != null){
									if(contactStatus == 1){
										sb.append("<input "+clickable+" disabled type=\"checkbox\" name=\"referenceData[]\" value="+elerefAutoId+">");
									} else {
										sb.append("<input "+clickable+" type=\"checkbox\" name=\"referenceData[]\" onclick='return showReferenceCheckSLC()' value="+elerefAutoId+">");
									}
								} else {
										sb.append("<input "+clickable+" type=\"checkbox\" name=\"referenceData[]\" onclick='return showReferenceCheckSLC()' value="+elerefAutoId+">");
								}
								sb.append("</td>");

								sb.append("<td class='td2'>");
								sb.append(referenceDetail.getFirstName()+" "+referenceDetail.getLastName());
								sb.append("</td>");

								sb.append("<td class='td3'>");
								sb.append(referenceDetail.getEmail());
								sb.append("</td>");

								sb.append("<td class='td4'>");
								sb.append(referenceDetail.getContactnumber());
								sb.append("</td>");

								if(contactStatus != null){
									if(contactStatus == 1){
										sb.append("<td class='td5'>" +
												"<a "+clickable+" href=\"#\" onclick=\"showReferenceDetailSLC('"+candidateFullName+"','"+techerFullName+"',"+elerefAutoId+","+districtMaster.getDistrictId()+","+jobId+","+teacherId+")\">" +
												"<span class=\"icon-download-alt icon-large iconcolorBlue\"></span>" +
												"</a>" +
										"</td>");
									} else {
										sb.append("<td class='td5'></td>");
									}
								} else {
									sb.append("<td class='td6'>"+questionMaxScore+"</td>");
								}

								if(questionMaxScore != null){
									sb.append("<td class='td6'>"+questionMaxScore+"</td>");
								} else {
									sb.append("<td class='td6'></td>");
								}
								
								
								

								sb.append("<td class='td7'>");
								sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(referenceDetail.getCreatedDateTime()));
								sb.append("</td>");
								
								if(actionAdd == 1){
								String elerefAutoId1="",eId1="",jId1="",tId1="",uId="",urlString="";
								{									
								eId1		=	referenceDetail.getElerefAutoId().toString();																
								tId1		=	referenceDetail.getTeacherDetail().getTeacherId().toString();
								uId			=	userMaster.getUserId().toString();
								urlString 		= 	eId1+","+tId1+","+uId;
								}	
								
								sb.append("<td class='td8' style='text-align:center;'>");
								if( contactStatus != 1){//referenceHistory != null &&
									sb.append("<a "+clickable.substring(0,(clickable.length()-1))+" districtTooltip'"+" href='javascript:void(0)' data-original-title='District Entry' rel='tooltip' onclick=\"openQuestionEReference(\'"+urlString+"\');\"><img class='addDistrictEntry' src='images/add_notes.png' width='17px' height='20px;'/></a>");
								}else{
									sb.append("&nbsp;");
								}
								sb.append("</td>");								
								}
								
								sb.append("</tr>");
								
							}
						} else {
							sb.append("<tr>");
							sb.append("<td colspan=\"5\"><B>");
							sb.append(Utility.getLocaleValuePropByKey("lblNoRecord", locale));
							sb.append("</B></td>");
							sb.append("</tr>");
						}
						sb.append("</table>");
						sb.append(PaginationAndSorting.getPaginationTrippleGrid(request,totalRecord,noOfRow, pageNo));

						sb.append("</div>");

						Integer referenceTeacherId	=	jobForTeacherObj.getTeacherId().getTeacherId();
						Integer referenceJobId		=	jobForTeacherObj.getJobId().getJobId();
						String secondaryStatusName	=	secondaryStatus.getSecondaryStatusName()==null?"":secondaryStatus.getSecondaryStatusName();
						secondaryStatusName			=	secondaryStatusName.replace("'","\\'");
						sb.append("<table border='0' id='referenceGridSendEmail' cellspacing='3' cellpadding='3' style='display:none;'>");
						sb.append("<tr><td colspan=\"2\">&nbsp;</td></tr>");
						sb.append("<input type=\"hidden\" id=\"referenceTeacherId\" name=\"referenceTeacherId\" value='"+referenceTeacherId+"'/>");
						sb.append("<input type=\"hidden\" id=\"referenceJobId\" name=\"referenceJobId\" value='"+referenceJobId+"'/>");
						sb.append("<input type=\"hidden\" id=\"secondaryStatus\" name=\"secondaryStatus\" value="+secondaryStatusName+">");
						sb.append("<input type=\"hidden\" id=\"fitScore\" name=\"fitScore\" value="+fitScore+">");
						sb.append("<input type=\"hidden\" id=\"statusId\" name=\"statusId\" value="+statusId+">");
						sb.append("<input type=\"hidden\" id=\"secondaryStatusId\" name=\"secondaryStatusId\" value="+secondaryStatusId+">");
						sb.append("<input type=\"hidden\" id=\"referenceListSize\" name=\"referenceListSize\" value="+totalRecord+">");
						sb.append("<input type=\"hidden\" id=\"contactSize\" name=\"contactSize\" value="+contactSize+">");
						sb.append("<input type=\"hidden\" id=\"contactSize\" name=\"contactSize\" value="+refChkSize+">");
						sb.append("<input type=\"hidden\" id=\"numberOfQuestion\" name=\"numberOfQuestion\" value="+numberOfQuestion+">");
						sb.append("<input type=\"hidden\" id=\"topMaxScore\" name=\"topMaxScore\" value="+topMaxScore+">");
						sb.append("<input type=\"hidden\" id=\"requiredReference\" name=\"requiredReference\" value="+requiredReference+">");
						sb.append("<input type=\"hidden\" id=\"actionAdd\" name=\"actionAdd\" value="+actionAdd+">");
						sb.append("<tr>");

						sb.append("<td>&nbsp;</td>");
						sb.append("<td>");

						if(replySize != totalRecord){
							if(refChkSize != 0 && (totalRecord == refChkSize)){
								sb.append("<button class='btn btn-primary' type='button' onclick='return sendReferenceMail()'>"+Utility.getLocaleValuePropByKey("btnResend", locale)+"</button>&nbsp;&nbsp;");
							} else {
								sb.append("<button class='btn btn-primary' type='button' onclick='return sendReferenceMail()'>"+Utility.getLocaleValuePropByKey("btnSend", locale)+"</button>&nbsp;&nbsp;");
							}
							sb.append("</td>");
							sb.append("</tr>");
							sb.append("</table>");
						}
					} catch (Exception e){
						e.printStackTrace();
					}
				}		

				sb.append("</td></tr>");
				sb.append("<tr><td align=right style='padding-right:15px;'>");
				sb.append("<div id=\"divTransAcademic\" style=\"margin-top: 10px;\"></div>");
				sb.append("</td></tr>");

				/********************************* Online Activity ****************************************/
				List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSetList(districtMaster,jobOrder.getJobCategoryMaster());
				if(recentStatusName.equalsIgnoreCase("Online Activity")&& userMaster.getEntityType()!=1 && onlineActivityQuestionSetList.size()>0){
					sb.append("<div id=\"divOnlineActivityForCG\" style=\"margin-top: 10px;\"></div>");
				}
				/*********************************End Online Activity ****************************************/

				if(userMaster.getEntityType()!=1){
					sb.append("<tr><td align=right>");
					sb.append("<a data-original-title='Export Notes in PDF' rel='tooltip' id='expNotePdf' href='javascript:void(0);'  onclick='downloadSLCReport();'><span class='icon-print icon-large iconcolor'></span></a>&nbsp;&nbsp;");
					sb.append("<span id=\"statusAddNote\"><a "+clickable+" href=\"#\" onclick=\"openNoteDiv("+userMaster.getUserId()+")\">"+Utility.getLocaleValuePropByKey("lnkAddNote", locale)+"</a></span>");
					sb.append("</td></tr>");
				}
				sb.append("<tr><td>");
				try{
					sb.append("<div class='hide'id=\"scoreList\">");
					sb.append("<table border=0 class='table' >");
					sb.append("<thead class='bg'>");
					sb.append("<tr><th width=290>Name</td><th  width=30>Score</td><th  width=80>"+Utility.getLocaleValuePropByKey("lblCreateddate", locale)+"</th>");
					if(isSuperAdminUser){sb.append("<th>"+Utility.getLocaleValuePropByKey("lblActions", locale)+"</th>");	}//added by 04-04-2015
					sb.append("</tr>");
					sb.append("</thead>");
					//added by 04-04-2015
					String bothStausString="";
					if(statusMaster!=null && secondaryStatus!=null)
						bothStausString="sm#"+statusMaster.getStatusId()+"##ss#"+secondaryStatus.getSecondaryStatusId();
					else if(statusMaster!=null)
						bothStausString="sm#"+statusMaster.getStatusId();
					else if(secondaryStatus!=null)
						bothStausString="ss#"+secondaryStatus.getSecondaryStatusId();
					String teacherStatusScoresInfo=","+teacherDetail.getTeacherId()+","+jobOrder.getJobId()+","+bothStausString;
					//ended by 04-04-2015
					for(TeacherStatusScores teacherStatusScores : teacherStatusScoresList){
						if(teacherStatusScores.isFinalizeStatus()){
							sb.append("<tr><td>"+teacherStatusScores.getUserMaster().getFirstName()+" "+teacherStatusScores.getUserMaster().getLastName()+"</td>");
							sb.append("<td>"+teacherStatusScores.getScoreProvided()+"</td>");
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(teacherStatusScores.getCreatedDateTime())+"</td>");
							String allInfo="'"+teacherStatusScores.getTeacherStatusScoreId()+","+teacherStatusScores.getMaxScore()+","+teacherStatusScores.getScoreProvided()+teacherStatusScoresInfo+"'";
							////System.out.println("allInfo========="+allInfo);
							if(isSuperAdminUser){
							sb.append("<td><a "+clickable+" href='javascript:void(0)'  onclick=\"editteacherStatusScores("+allInfo+");\">"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a></td>");
							}
							sb.append("</tr>"); //added by 04-04-2015
						}
					}
					sb.append("</table>");
					sb.append("<table id='openEditTeacherStatusScore' style='display:none;width:100%;'></table>"); //added by 04-04-2015
					sb.append("</div>");
				}catch(Exception e){
					e.printStackTrace();
				}
				String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				sb.append("<table border='0' class='table' id='tblStatusNote'>");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				sb.append("<th valign=top>"+PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblNotes", locale),sortOrderFieldName,"statusNotes",sortOrderTypeVal,pgNo)+"</th>");
				sb.append("<th valign=top>"+Utility.getLocaleValuePropByKey("hdAttachment", locale)+"</th>");
				sb.append("<th valign=top>"+PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCreatedBy", locale),sortOrderFieldName,"userMaster",sortOrderTypeVal,pgNo)+"</th>");
				sb.append("<th valign=top>"+PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCreatedDate", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo)+"</th>");
				sb.append("<th valign=top>"+Utility.getLocaleValuePropByKey("lblActions", locale)+"</th>");
				sb.append("</tr>");
				sb.append("</thead>");
				int counter=0,countSave=0;
				if(lstTeacherStatusNotes !=null)
				{
					for(TeacherStatusNotes tsDetails : lstTeacherStatusNotes){
						sb.append("<tr>");
						sb.append("<td>");
						String statusNote=tsDetails.getStatusNotes();
						if(statusNote!=null){
							String statusNotes=statusNote.replaceAll("\\<[^>]*>","");
							int iTurncateIndex=90;
							if(statusNotes.length()>iTurncateIndex)
							{
								int sSpaceIndex=statusNotes.indexOf(" ", iTurncateIndex);
								if(sSpaceIndex >0)
								{
									statusNotes=statusNotes.substring(0,sSpaceIndex);
									sb.append("<a href='javascript:void(0);' id='teacherNotesDetails"+tsDetails.getTeacherStatusNoteId()+"' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgClickviewdetails", locale)+"' onclick='viewCompleteTeacherStatusNotes("+tsDetails.getTeacherStatusNoteId()+");'>"+statusNotes+"</a>");
									sb.append("<script>$('#teacherNotesDetails"+tsDetails.getTeacherStatusNoteId()+"').tooltip({placement : 'right'});</script>");
								}
								else
									sb.append(statusNotes);
							}
							else
								sb.append(statusNotes);
						}
						sb.append("</td>");
						sb.append("<td>");
						String fileName=tsDetails.getStatusNoteFileName();
						String filePath="statusNote/"+teacherDetail.getTeacherId();
						if(fileName!=null && fileName!=""){
							counter++;
							sb.append("<a href='javascript:void(0);' id='commfile"+counter+"' onclick=\"downloadCommunication('"+filePath+"','"+fileName+"','commfile"+counter+"');"+windowFunc+"\"><span class='icon-paper-clip icon-large iconcolor'></span></a>");
						}
						sb.append("</td>");
						String name="";
						if(tsDetails.getUserMaster()!=null){
							UserMaster um=tsDetails.getUserMaster();
							if(um.getMiddleName()!=null){
								name=um.getFirstName()+" "+um.getMiddleName()+" "+um.getLastName();
							}else{
								name=um.getFirstName()+" "+um.getLastName();
							}
						}
						sb.append("<td>"+name+"</td>");
						sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(tsDetails.getCreatedDateTime())+"</td>");
						sb.append("<td>");
						//if(!tsDetails.isFinalizeStatus()&& userMaster.getEntityType()!=1){
						if((!tsDetails.isFinalizeStatus()|| isSuperAdminUser)&& userMaster.getEntityType()!=1){
							countSave++;
							if(tsDetails.getTeacherAssessmentQuestionId()!=null){
								if(isSuperAdminUser)
								sb.append("<a "+clickable+" href='javascript:void(0);' onclick=\"return editShowStatusNote('"+tsDetails.getTeacherStatusNoteId()+"')\" >"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a> | <a "+clickable+" href='javascript:void(0);' onclick=\"return deleteStatusNoteChk('"+tsDetails.getTeacherStatusNoteId()+"','note')\" >"+Utility.getLocaleValuePropByKey("lnkDlt", locale)+"</a>");
								else
								sb.append("<a "+clickable+" href='javascript:void(0);' onclick=\"return deleteStatusNoteChk('"+tsDetails.getTeacherStatusNoteId()+"','note')\" >"+Utility.getLocaleValuePropByKey("lnkDlt", locale)+"</a>");
							}else
								sb.append("<a "+clickable+" href='javascript:void(0);' onclick=\"return editShowStatusNote('"+tsDetails.getTeacherStatusNoteId()+"')\" >"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a> | <a "+clickable+" href='javascript:void(0);' onclick=\"return deleteStatusNoteChk('"+tsDetails.getTeacherStatusNoteId()+"','note')\" >"+Utility.getLocaleValuePropByKey("lnkDlt", locale)+"</a>");

						}else{
							sb.append("&nbsp");
						}
						sb.append("</td>");
						sb.append("</tr>");
					}
				}

				if(lstTeacherStatusNotes == null || lstTeacherStatusNotes.size()==0){
					sb.append("<tr>");				
					sb.append("<td colspan=4>"+Utility.getLocaleValuePropByKey("lblNoNoteAdd", locale)+"</td>");
					sb.append("</tr>");
				}
				sb.append("</table>");
				sb.append(PaginationAndSorting.getPaginationTrippleGrid(request,totaRecord,noOfRow, pageNo));
				sb.append("</td></tr></table>");


				if(bOverride && bPanelForCG && !isAlreadyOverride)
				{
					//sb.append("<div class='row mt10' style='margin-left:-20px;'>");
					//sb.append("<div class='span10'><label class='checkbox inline'><input type='checkbox' name='chkOverride' id='chkOverride' value='1'/>Override the Status</label></div>");
					//sb.append("</div>");
					sb.append("<input type='hidden' id='txtOverride' value='1'/>");
				}
				else
				{
					sb.append("<input type='hidden' id='txtOverride' value='0'/>");
				}

				if(bPanelForCG)
					sb.append("<input type='hidden' id='txtPanel' value='1'/>");
				else
					sb.append("<input type='hidden' id='txtPanel' value='0'/>");

				if(bLoggedInUserPanelAttendees)
					sb.append("<input type='hidden' id='txtLoggedInUserPanelAttendees' value='1'/>");
				else
					sb.append("<input type='hidden' id='txtLoggedInUserPanelAttendees' value='0'/>");


				// Start ... Read / Write privilege for school
				if(userMaster.getEntityType()!=3 && jobOrder!=null && ((statusMaster!=null && statusMaster.getStatusShortName().equalsIgnoreCase("hird")) || (secondaryStatus!=null && secondaryStatus.getSecondaryStatusName().equals("Offer Ready"))))
				{
					List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findJobOrder(jobOrder);
					if(listSchoolInJobOrder!=null && listSchoolInJobOrder.size() > 1)
						sb.append("<input type='hidden' id='txtschoolCount' value='"+listSchoolInJobOrder.size()+"'/>");
				}
				// End ... Read / Write privilege for school

			}else{
				statusJSIFlag="JSI";
				sb.append("<table>");	
				sb.append("<tr>");				
				sb.append("<td colspan=6>"+Utility.getLocaleValuePropByKey("lblNoAttachJSI", locale)+"</td>");
				sb.append("</tr>");
				sb.append("<table>");	
			}
			
			List<StrengthOpportunitySummary> strengthOpportunitySummaries = strengthOpportunitySummaryDAO.findStrengthOpportunityByTeacherIdandJobId(teacherDetail, jobOrder);
			List<String> competencyList = strengthOpportunitySummaryDAO.getAllCompetencies();
			
			subs.append("<div id='summaryDiv' style='margin-top:10px;margin:auto;padding-top:15px;'>");
			
			subs.append("<table style='border:1px solid black;text-align:center;width:670px;'>");
			subs.append("<th style='border:1px solid black;text-align:center;background-color:#AEAEAE;'><b>"+Utility.getLocaleValuePropByKey("lblCompetencies", locale)+"</b></th>");
			subs.append("<th style='border:1px solid black;text-align:center;background-color:#AEAEAE;'><b>"+Utility.getLocaleValuePropByKey("strengths", locale)+"</b></th>");
			subs.append("<th style='border:1px solid black;text-align:center;background-color:#AEAEAE;'><b>"+Utility.getLocaleValuePropByKey("lblGrowthOpportunities", locale)+"</b></th>");
			
			int count = 0;
			String strength = "strength";
			String growth = "growth";
			String recommendReason = "";
			String remainingQuestions = "";
			if(strengthOpportunitySummaries!=null && strengthOpportunitySummaries.size()>0)
			{
				recommendReason = strengthOpportunitySummaries.get(0).getRecommendSchoolReason();
				remainingQuestions = strengthOpportunitySummaries.get(0).getQuestionForInterview();
				if(strengthOpportunitySummaries.get(0).getSchoolMaster()!=null)
				{
				//	subs.append("<script type='text/javascript'>document.getElementById('schoolIdCG').value='"+strengthOpportunitySummaries.get(0).getSchoolMaster().getSchoolName()+"'; $('#schoolIdCG').css('disabled','true');</script>");
					schoolName = strengthOpportunitySummaries.get(0).getSchoolMaster().getSchoolName()+"@@@"+strengthOpportunitySummaries.get(0).getSchoolMaster().getSchoolId();
				}
				
				for(String s : competencyList)
				{
					strength = strengthOpportunitySummaries.get(count).getStrength()!=null?strengthOpportunitySummaries.get(count).getStrength():"";
					growth = strengthOpportunitySummaries.get(count).getGrowthOpportunity()!=null?strengthOpportunitySummaries.get(count).getGrowthOpportunity():"";
					subs.append("<tr style='border:1px solid black;'>");
					subs.append("<td><b>"+s+"</b></td>");
					if(strength.length()>0)
					{
						//subs.append("<td id='td"+count+"'><div id='strength"+count+"' style='width:240px;overflow:auto;height:120px;text-align:center;'>"+strength+"</div></td>");
						subs.append("<td id='td"+count+"'><textarea name='strength"+count+"' id='strength"+count+"' class='form-control' maxlength='100' rows='5' style='width:240px;resize: none;' readonly>"+strength+"</textarea></td>");
					}
					else
					{
						subs.append("<td id='td"+count+"'><textarea name='strength"+count+"' id='strength"+count+"' class='form-control' maxlength='100' rows='5' style='width:240px;resize: none;'>"+strength+"</textarea></td>");
					}
					
					if(growth.length()>0)
					{
						//subs.append("<td id='td"+count+"'><div id='growth"+count+"' style='width:240px;overflow:auto;height:120px;text-align:center;'>"+growth+"</div></td>");
						subs.append("<td id='td"+count+"'><textarea name='growth"+count+"' id='growth"+count+"' class='form-control' maxlength='100' rows='5' style='width:240px;resize: none;' readonly>"+growth+"</textarea></td>");
					}
					else
					{
						subs.append("<td id='td"+count+"'><textarea name='growth"+count+"' id='growth"+count+"' class='form-control' maxlength='100' rows='5' style='width:240px;resize: none;'>"+growth+"</textarea></td>");
					}
					subs.append("</tr>");
					count++;
				}
			}
			else
			{
				count = 0;
				for(String s : competencyList)
				{
					subs.append("<tr style='border:1px solid black;'>");
					subs.append("<td><b>"+s+"</b></td>");
					subs.append("<td id='td"+count+"'><textarea name='strength"+count+"' id='strength"+count+"' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
					subs.append("<td id='td"+count+"'><textarea name='growth"+count+"' id='growth"+count+"' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
					subs.append("</tr>");
					count++;
				}
			}
			
			
			/*subs.append("<tr style='border:1px solid black;'>");
			subs.append("<td><b>Instructional Leadership</b></td>");
			subs.append("<td id='td3'><textarea name='strength1' id='strength1' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("<td id='td4'><textarea name='growth1' id='growth1' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("</tr>");
			
			subs.append("<tr style='border:1px solid black;'>");
			subs.append("<td><b>Personal Leadership</b></td>");
			subs.append("<td id='td5'><textarea name='strength2' id='strength2' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("<td id='td6'><textarea name='growth2' id='growth2' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("</tr>");
			
			subs.append("<tr style='border:1px solid black;'>");
			subs.append("<td><b>Cultural and  Community Leadership</b></td>");
			subs.append("<td id='td7'><textarea name='strength3' id='strength3' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("<td id='td8'><textarea name='growth3' id='growth3' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("</tr>");
			
			subs.append("<tr style='border:1px solid black;'>");
			subs.append("<td><b>Operational Management<b></td>");
			subs.append("<td id='td9'><textarea name='strength4' id='strength4' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("<td id='td10'><textarea name='growth4' id='growth4' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("</tr>");
			
			subs.append("<tr style='border:1px solid black;'>");
			subs.append("<td><b>Results Driven<b></td>");
			subs.append("<td id='td11'><textarea name='strength5' id='strength5' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("<td id='td12'><textarea name='growth5' id='growth5' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;'></textarea></td>");
			subs.append("</tr>");
			 */			
			
			subs.append("</table>");
			
			
			/*subs.append("<table><tr>");
			subs.append("<td>");
			
		 
			// Recommended for <INSERT SCHOOL> for the following key reasons:
			subs.append("<div id='recommendSchool' style='margin-top:10px;width:670px;'>");
			sb.append("<div id='recomDiv' class='control-group' style='width:670px;margin-top:10px;margin-left:1px;'>");
			sb.append("<label><strong>"+Utility.getLocaleValuePropByKey("lblRecommendedForSchool", locale)+":</strong></label><br/>");
			sb.append("<style>#recomreason .jqte{width:670px !important;}</style>");
			sb.append("<style>#remquestion .jqte{width:670px !important;}</style>");
			if(isSuperAdminUser) sb.append("<style>#jWTeacherStatusNotesDiv .jqte{height:150px !important;width:80% !important;float:left;padding-top:2px;padding-bottom:10px;} #statusNotes .jqte{width:96% !important;height:100% !important;padding-top:0px;padding-bottom:0px;}</style>");
			if(recommendReason.length()>0)
			{
			sb.append("<div id='recomreason'  style='pointer-events: none;width:670px;'><textarea name='text1' id='text1' class='form-control' maxlength='500' rows='5' style='width:670px;resize: none;' readonly>"+recommendReason+"</textarea></div></div>");
			}
			else
			{
			sb.append("<div id='recomreason' style='width:670px;'><textarea name='text1' id='text1' class='form-control' maxlength='500' rows='5' style='width:670px;resize: none;'>"+recommendReason+"</textarea></div></div>");
			}
			
			sb.append("<div id='remQuesDiv' class='control-group' style='width:670px;margin-top:10px;margin-left:1px;'>");
			sb.append("<label><strong>"+Utility.getLocaleValuePropByKey("lblRemainingQuestion", locale)+":</strong></label><br/>");
			if(remainingQuestions.length()>0)
			{
			sb.append("<div id='remquestion'  style='pointer-events: none;width:670px;'><textarea name='text2' id='text2' class='form-control' maxlength='500' rows='5' style='width:670px;resize: none;' readonly>"+remainingQuestions+"</textarea></div></div>");
			}
			else
			{
				sb.append("<div id='remquestion' style='width:670px;'><textarea name='text2' id='text2' class='form-control' maxlength='500' rows='5' style='width:670px;resize: none;'>"+remainingQuestions+"</textarea></div></div>");
			}
			//sb.append("<style> .jqte{width:670px !important;}</style>");
			subs.append("</div>");
			subs.append("</td>");
			subs.append("</tr></table>");*/
			
			subs.append("</div>");
			
			Integer autoEmailTemp=null;

			if(!statusId.equals("0") ){
				//System.out.println("**** status ID****** 1111 :: "+statusId);
				autoEmailTemp       =  Integer.parseInt(statusId);
				StatusWiseAutoEmailSend s =  statusWiseAutoEmailSendDAO.findAutoEmailStatusByStatusIdDistrict(autoEmailTemp, districtMaster);

				if(s!=null){
					flag=true;
				}
			}

			if(!secondaryStatusId.equals("0")){
				//System.out.println("**********secondaryStatus**********222 "+secondaryStatusId);
				autoEmailTemp      =  Integer.parseInt(secondaryStatusId);
				
				List<JobCategoryWiseStatusPrivilege> jcwpsList = new ArrayList<JobCategoryWiseStatusPrivilege>();
				SecondaryStatus secondaryStatus2 = secondaryStatusDAO.findById(autoEmailTemp, false, false);
				
				jcwpsList = jobCategoryWiseStatusPrivilegeDAO.getFilterStatusByName(districtMaster, jobOrder.getJobCategoryMaster(), secondaryStatus2);

				if(jcwpsList!=null && jcwpsList.size()>0){
					//System.out.println("01"+jcwpsList.get(0).getSecondaryStatus().getSecondaryStatusId());
					if(jcwpsList.get(0).getAutoNotifyAll()!=null){
						if(jcwpsList.get(0).getAutoNotifyAll())
							flag = true;
					}else{
						System.out.println("<<<<<<<<<<< AutoNotifyAll is NULL >>>>>>>>>>>>>>>>");
					}
				}
				
				
				/*List<Integer>  secodryIds      =  secondaryStatusDAO.findBySecondryStatusName(districtMaster, autoEmailTemp);      
				if(secodryIds.size()>0){
					if(flag!=true)
						flag = statusWiseAutoEmailSendDAO.findBySSIDs(secodryIds);
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//System.out.println(" flag :>>>>>>>>>>>>>>>>>>>: "+flag);
		
		return sb.toString()+"##"+statusHDRFlag+"##"+statusHDRReq+"##"+flag+"##"+jobCategoryFlag+"##"+hiredByDate+"##"+statusJSIFlag+"##"+statusResend+"##"+subs.toString()+"##"+schoolName+"##"+statusWaived;
		}	
	private String[] callOnlyForJSIJQuestionAndAnswer(TeacherDetail teacherDetail, JobOrder jobOrder,StatusMaster statusMaster, SecondaryStatus secondaryStatus, UserMaster userMaster,TeacherAssessmentdetail teacherAssessmentdetail,Map<Integer,StatusSpecificScore> stausSCore, List<TeacherAnswerDetail> teacherAnswerDetails,TeacherAssessmentQuestion teacherAssessmentQuestion,List<TeacherAssessmentOption> teacherQuestionOptionsList,String questionAssessmentIds, int iQdslider,int iCountScoreSlider,String qqNote,int defauntQuestionNote, boolean isSuperAdminUser, String clickable)
	{
		IS_PRINT=false;
		StringBuffer sb=new StringBuffer();
		//set width of slider
		int scoreWidth=525,scoreWidth1=565;
		if(userMaster.getEntityType()==3){
			scoreWidth=575;scoreWidth1=615;
		}
		//for return all value in which value change in this method
		String[] allValue=new String[6];
		// for all user available on this district in StatusSpecifictScore
		Map<Integer,UserMaster>  mapUserMaster=new LinkedHashMap<Integer,UserMaster>();
		Map<Integer,UserMaster>  mapExtraUserMaster=new LinkedHashMap<Integer,UserMaster>();
		
		// for all user by district StatusSpecificScore AnswerDetailId
		Map<String,List<StatusSpecificScore>> getUserIdAndAnswerIdSSSMap=new HashMap<String,List<StatusSpecificScore>>();
		// for all user by district StatusSpecificScore QuestionId
		Map<String,List<StatusSpecificScore>> getUserIdAndQuestionIdSSSMap=new HashMap<String,List<StatusSpecificScore>>();
		
		// for all notes by district  teacherStatusNotes
		Map<String,List<TeacherStatusNotes>> getUserIdAndAnswerIdTSNMap=new HashMap<String,List<TeacherStatusNotes>>();
		Map<String,List<TeacherStatusNotes>> getExtraUserIdAndAnswerIdTSNMap=new HashMap<String,List<TeacherStatusNotes>>();
		
		// for all user by district StatusSpecificScore of this user
		Map<Integer,StatusSpecificScore> mapScoreFirstSSS=new LinkedHashMap<Integer,StatusSpecificScore>();
		Map<Integer,StatusSpecificScore> mapExtraScoreFirstSSS=new LinkedHashMap<Integer,StatusSpecificScore>();
		
		// for all extra Question for jsi
		Map<Integer,StatusSpecificQuestions> allExtraQuestionMap=new TreeMap<Integer,StatusSpecificQuestions>();			
		
		//for all assesment question
		Set<Long> teacherAssessmentQuestionLst=new TreeSet<Long>();
		Set<Long> extraStatusSpecificQuestionLst=new TreeSet<Long>();
		mapUserMaster.put(userMaster.getUserId(),userMaster);
		
		//get All Notes of all user in one district
		Map<UserMaster,List<TeacherStatusNotes>>  getAllNotesByUserMap=new HashMap<UserMaster, List<TeacherStatusNotes>>();
		
		for (TeacherAnswerDetail teacherAnswerDetail : teacherAnswerDetails) {
			teacherAssessmentQuestionLst.add(teacherAnswerDetail.getTeacherAssessmentQuestion().getTeacherAssessmentQuestionId());
		}
		
		List<StatusSpecificScore> lstStatusSpecificScore=null;
		if(userMaster.getEntityType()==2){			
			lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionListForSuperAdministration(jobOrder,statusMaster,secondaryStatus,teacherDetail);
		}else{
			lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList(jobOrder,statusMaster,secondaryStatus,teacherDetail,userMaster);
		}
		getAllNotesByUserMap=getAllNotesByUserMap(lstStatusSpecificScore);
		println("Size===============22222222================="+lstStatusSpecificScore.size());
		for(StatusSpecificScore score:lstStatusSpecificScore){
			if(score.getTeacherAnswerDetail()!=null){
				try{
					if(score.getUserMaster()!=null)
						mapUserMaster.put(score.getUserMaster().getUserId(),score.getUserMaster());
					}catch(Exception e){e.printStackTrace();}
			if(score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString())){
				mapScoreFirstSSS.put(score.getTeacherAnswerDetail().getAnswerId(),score);
			}
			/*else if(score.getSkillAttributesMaster()!=null){
				mapScoreFirstSSS.put(score.getStatusSpecificQuestions().getQuestionId(),score);
			}*/
			}
			if(score.getStatusSpecificQuestions()!=null){
				Long qId=Long.parseLong(""+score.getStatusSpecificQuestions().getQuestionId());
				extraStatusSpecificQuestionLst.add(qId);
				try{
					if(score.getUserMaster()!=null)
						mapExtraUserMaster.put(score.getUserMaster().getUserId(),score.getUserMaster());
					}catch(Exception e){e.printStackTrace();}
				if(score.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString())){
					mapExtraScoreFirstSSS.put(score.getStatusSpecificQuestions().getQuestionId(),score);
					allExtraQuestionMap.put(score.getStatusSpecificQuestions().getQuestionId(),score.getStatusSpecificQuestions());
				}else if(manageStatusAjax.showOrNot(lstStatusSpecificScore, userMaster, score.getStatusSpecificQuestions(), getAllNotesByUserMap)){
					mapExtraScoreFirstSSS.put(score.getStatusSpecificQuestions().getQuestionId(),score);
					allExtraQuestionMap.put(score.getStatusSpecificQuestions().getQuestionId(),score.getStatusSpecificQuestions());
				}
			}
		}
		
		println("mapScoreFirstSSS==="+mapScoreFirstSSS.size()+"     mapExtraScoreFirstSSS=="+mapExtraScoreFirstSSS.size());
		
		
		for(StatusSpecificScore sps:lstStatusSpecificScore){
			println("userId===hhhhhh==="+sps+"======="+sps.getUserMaster().getFirstName()+" "+sps.getUserMaster().getLastName());
			if(sps.getTeacherAnswerDetail()!=null){					
				if(getUserIdAndAnswerIdSSSMap.get(sps.getTeacherAnswerDetail().getAnswerId()+"#"+sps.getUserMaster().getUserId())==null){
					List<StatusSpecificScore> lst=new ArrayList<StatusSpecificScore>();
					lst.add(sps);
					getUserIdAndAnswerIdSSSMap.put(sps.getTeacherAnswerDetail().getAnswerId()+"#"+sps.getUserMaster().getUserId(), lst);						
				}else{
					List<StatusSpecificScore> lst=getUserIdAndAnswerIdSSSMap.get(sps.getTeacherAnswerDetail().getAnswerId()+"#"+sps.getUserMaster().getUserId());
					lst.add(sps);
					getUserIdAndAnswerIdSSSMap.put(sps.getTeacherAnswerDetail().getAnswerId()+"#"+sps.getUserMaster().getUserId(), lst);
				}					
			}
			if(sps.getStatusSpecificQuestions()!=null){					
				if(getUserIdAndQuestionIdSSSMap.get(sps.getStatusSpecificQuestions().getQuestionId()+"#"+sps.getUserMaster().getUserId())==null){
					List<StatusSpecificScore> lst=new ArrayList<StatusSpecificScore>();
					lst.add(sps);
					getUserIdAndQuestionIdSSSMap.put(sps.getStatusSpecificQuestions().getQuestionId()+"#"+sps.getUserMaster().getUserId(), lst);						
				}else{
					List<StatusSpecificScore> lst=getUserIdAndQuestionIdSSSMap.get(sps.getStatusSpecificQuestions().getQuestionId()+"#"+sps.getUserMaster().getUserId());
					lst.add(sps);
					getUserIdAndQuestionIdSSSMap.put(sps.getStatusSpecificQuestions().getQuestionId()+"#"+sps.getUserMaster().getUserId(), lst);
				}					
			}				
		}
		
		List<TeacherStatusNotes> lstAllUserTeacherStatusNotes=new ArrayList<TeacherStatusNotes>();
		List<TeacherStatusNotes> lstExtraAllUserTeacherStatusNotes=new ArrayList<TeacherStatusNotes>();
		Criterion criterionStatus=null;
		if(statusMaster!=null){
			criterionStatus = Restrictions.eq("statusMaster", statusMaster);
		}else{
			criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
		}
		if(userMaster.getEntityType()==2){
			lstAllUserTeacherStatusNotes=teacherStatusNotesDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherDetail),Restrictions.eq("jobOrder", jobOrder),Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId()),Restrictions.in("teacherAssessmentQuestionId", teacherAssessmentQuestionLst),criterionStatus);
			if(extraStatusSpecificQuestionLst.size()>0)
			lstExtraAllUserTeacherStatusNotes=teacherStatusNotesDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherDetail),Restrictions.eq("jobOrder", jobOrder),Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId()),Restrictions.in("teacherAssessmentQuestionId", extraStatusSpecificQuestionLst),criterionStatus);
			}else{
				lstAllUserTeacherStatusNotes=teacherStatusNotesDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherDetail),Restrictions.eq("jobOrder", jobOrder),Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId()),Restrictions.in("teacherAssessmentQuestionId", teacherAssessmentQuestionLst),criterionStatus,Restrictions.eq("userMaster", userMaster));
				if(extraStatusSpecificQuestionLst.size()>0)
				lstExtraAllUserTeacherStatusNotes=teacherStatusNotesDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherDetail),Restrictions.eq("jobOrder", jobOrder),Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId()),Restrictions.in("teacherAssessmentQuestionId", extraStatusSpecificQuestionLst),criterionStatus,Restrictions.eq("userMaster", userMaster));
			}
		
		println("size 9of notes======================="+lstAllUserTeacherStatusNotes.size());
		println("size extra 9of notes======================="+lstExtraAllUserTeacherStatusNotes.size());
		for(TeacherStatusNotes tsn:lstAllUserTeacherStatusNotes){
			if(getUserIdAndAnswerIdTSNMap.get(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId())==null){
				List<TeacherStatusNotes> lst=new ArrayList<TeacherStatusNotes>();
				lst.add(tsn);
				getUserIdAndAnswerIdTSNMap.put(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId(), lst);						
			}else{
				List<TeacherStatusNotes> lst=getUserIdAndAnswerIdTSNMap.get(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId());
				lst.add(tsn);
				getUserIdAndAnswerIdTSNMap.put(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId(), lst);
			}
		}
		//for exta question
		for(TeacherStatusNotes tsn:lstExtraAllUserTeacherStatusNotes){
			if(getExtraUserIdAndAnswerIdTSNMap.get(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId())==null){
				List<TeacherStatusNotes> lst=new ArrayList<TeacherStatusNotes>();
				lst.add(tsn);
				getExtraUserIdAndAnswerIdTSNMap.put(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId(), lst);						
			}else{
				List<TeacherStatusNotes> lst=getExtraUserIdAndAnswerIdTSNMap.get(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId());
				lst.add(tsn);
				getExtraUserIdAndAnswerIdTSNMap.put(tsn.getTeacherAssessmentQuestionId()+"#"+tsn.getUserMaster().getUserId(), lst);
			}
		}
		
		Map<String,String> getScoreHtmlTableMap= manageStatusAjax.getAllStatusSpecificScoreByQuestionIdAndAnswerId(teacherDetail, jobOrder, statusMaster, secondaryStatus);
		
		//for status
		int statusId=0,secondaryStatusId=0;			
		if(statusMaster!=null){
			statusId=statusMaster.getStatusId();
		}else if(secondaryStatus!=null) {
			secondaryStatusId=secondaryStatus.getSecondaryStatusId();
		}				
		//end

		if(teacherAssessmentdetail!=null)
		{
			String selectedOptions = "",insertedRanks="";
			String questionTypeShortName = "";
			long optionId = 0;
			String answerText = "";
			int questionCounter = 0;
			sb.append("<table>");

			for(TeacherAnswerDetail teacherAnswerDetail : teacherAnswerDetails){
				++questionCounter;
				teacherAssessmentQuestion = teacherAnswerDetail.getTeacherAssessmentQuestion();
				selectedOptions = teacherAnswerDetail.getSelectedOptions();

				sb.append("<tr>");
				sb.append("<td width='5%' valign='top'><label  class='labletxt'><b>Q. "+questionCounter+"</b></label></td>");

				sb.append("<td width='95%'><label   class='labletxt'>"+teacherAssessmentQuestion.getQuestion());
				sb.append("</label></td>");
				sb.append("</tr>");
				Double maxScore=0.0;
				int aScoreProvided=0;
				int scoreAnswerId=0;

				questionTypeShortName = teacherAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();
				teacherQuestionOptionsList = teacherAssessmentQuestion.getTeacherAssessmentOptions();
				if(questionTypeShortName.equalsIgnoreCase("tf") || questionTypeShortName.equalsIgnoreCase("slsel") || questionTypeShortName.equalsIgnoreCase("lkts"))
				{
					if(selectedOptions!=null && !selectedOptions.equals(""))
					{ 
						try{optionId = Long.parseLong(selectedOptions);}catch(Exception e){optionId = 0;}
					}
					else
						optionId = 0;

					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {

						sb.append("<tr>");
						sb.append("<td width='5%' valign='top'>&nbsp;</td>");
						sb.append("<td width='95%'><label    class='labletxt'>"+teacherQuestionOption.getQuestionOption());
						sb.append("</label></td>");
						sb.append("</tr>");

						if(optionId==teacherQuestionOption.getTeacherAssessmentOptionId())
						{
							answerText=teacherQuestionOption.getQuestionOption();
						}
					}

					sb.append("<tr>");
					sb.append("<td width='5%' valign='top'><label><B>"+Utility.getLocaleValuePropByKey("  msgAnswer", locale)+"</B></label></td>");
					sb.append("<td width='95%'><label>"+answerText);
					sb.append("</label></td>");
					sb.append("</tr>");

					answerText="";
				}else if(questionTypeShortName.equalsIgnoreCase("rt")){

					for(TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {

						sb.append("<tr>");
						sb.append("<td width='5%' valign='top'>&nbsp;</td>");
						sb.append("<td width='95%'><label    class='labletxt'>"+teacherQuestionOption.getQuestionOption());
						sb.append("</label></td>");
						sb.append("</tr>");
					}
					answerText=insertedRanks;
					insertedRanks="";

					insertedRanks = teacherAnswerDetail.getInsertedRanks()==null?"":teacherAnswerDetail.getInsertedRanks();
					selectedOptions = selectedOptions==null?"":selectedOptions;
					String[] ranks = insertedRanks.split("\\|");

					String ans = "";
					int rank = 0;
					sb.append("<table>");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {

						try{ans = ranks[rank];}catch(Exception e){ans = "";}
						sb.append("<tr>");
						sb.append("<td><label>");
						sb.append(ans);
						sb.append("</label></td>");

						sb.append("<td><label class='labletxt'>");
						sb.append(teacherQuestionOption.getQuestionOption());
						sb.append("</label></td>");
						sb.append("</tr>");
						rank++;
					}
					sb.append("<table>");

					sb.append("<tr>");
					sb.append("<td width='5%' valign='top'><b>"+Utility.getLocaleValuePropByKey("  msgAnswer", locale)+"</></td>");
					sb.append("<td width='95%'><label>"+answerText);
					sb.append("</label></td>");
					sb.append("</tr>");

					answerText="";
				}else if(questionTypeShortName.equalsIgnoreCase("sl")){
					sb.append("<tr>");
					sb.append("<td width='5%' valign='top'><b>"+Utility.getLocaleValuePropByKey("  msgAnswer", locale)+"</b></td>");
					sb.append("<td width='95%'><label>"+teacherAnswerDetail.getInsertedText());
					sb.append("</label></td>");
					sb.append("</tr>");
				}
				else if(questionTypeShortName.equalsIgnoreCase("ml")){

					sb.append("<tr>");
					sb.append("<td width='5%' valign='top'><label><b>"+Utility.getLocaleValuePropByKey("  msgAnswer", locale)+"</b></label></td>");
					sb.append("<td width='95%'><label>"+teacherAnswerDetail.getInsertedText());
					sb.append("</label></td>");
					sb.append("</tr>");
				}
				
				boolean flagforScoreShow=true;	
				boolean increaseOrNot=false;
				for(Map.Entry<Integer, UserMaster> entry:mapUserMaster.entrySet()){	
					UserMaster um=entry.getValue();
					println("key---->::"+teacherAnswerDetail.getAnswerId()+"#"+um.getUserId());
				//StatusSpecificScore SSObj=stausSCore.get(teacherAnswerDetail.getAnswerId());
				List<StatusSpecificScore> lstSSS=getUserIdAndAnswerIdSSSMap.get(teacherAnswerDetail.getAnswerId()+"#"+um.getUserId());
				for(StatusSpecificScore SSObj:lstSSS){
				boolean thisUser=SSObj.getUserMaster().getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString());
				int tickInterval=1;
				if(flagforScoreShow){
					StatusSpecificScore SSObj1=mapScoreFirstSSS.get(SSObj.getTeacherAnswerDetail().getAnswerId());						
				if(SSObj1!=null){
					if(SSObj1.getMaxScore()!=null){
						maxScore=SSObj1.getMaxScore();
					}
					if(SSObj1.getScoreProvided()!=null){
						aScoreProvided=SSObj1.getScoreProvided();
					}
					if(SSObj1!=null)
						scoreAnswerId=SSObj1.getAnswerId();
				}
				
				if(maxScore%10==0)
					tickInterval=10;
				else if(maxScore%5==0)
					tickInterval=5;
				else if(maxScore%3==0)
					tickInterval=3;
				else if(maxScore%2==0)
					tickInterval=2;					
				}
				
				if(maxScore>0){
					if(flagforScoreShow)
					iCountScoreSlider++;

					//Add Code for JSI
					if(SSObj.getFinalizeStatus()==1)
						iQdslider=0;
					else
						iQdslider=1;

					sb.append("<tr>");
					sb.append("<td width='5%' valign='top'>&nbsp;</td>");
					sb.append("<td width='95%'>");		
					if(flagforScoreShow){
					sb.append("<div class='row ' style='padding-left: 12px;'>");
					sb.append("<div class='span5 slider_question' style='float:left;'><iframe id=\"ifrmQuestion_"+iCountScoreSlider+"\" src=\"slideract.do?name=questionFrm&tickInterval="+tickInterval+"&max="+maxScore+"&dslider="+iQdslider+"&swidth="+scoreWidth+"&step=1&answerId="+scoreAnswerId+"&svalue="+aScoreProvided+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:"+scoreWidth1+"px;margin-top:-11px;\"></iframe></div>");
					if(userMaster.getEntityType()==2){
					sb.append("<div style='float:right;'><a id='question"+SSObj.getTeacherAnswerDetail().getAnswerId()+"' class='questionScore' title='' data-original-title='' data-placement='above' href='javascript:void(0);' onclick='clickOnQuestionScore(this);'><span  class='fa-crosshairs icon-large iconcolor' ></span></a></div>");
					//for all User Score
					sb.append("<div id='scoreTablequestion"+SSObj.getTeacherAnswerDetail().getAnswerId()+"' style='display:none;'>");
					////logger.info("teacherDetail=="+teacherDetail+" jobOrder=="+jobOrder+" statusMaster=="+statusMaster+" secondaryStatus=="+secondaryStatus);
					println("qId=--------------------------------==="+SSObj.getTeacherAnswerDetail().getAnswerId()+"    value===---------------------=="+getScoreHtmlTableMap.get(String.valueOf(SSObj.getTeacherAnswerDetail().getAnswerId())));
					sb.append(getScoreHtmlTableMap.get(String.valueOf(SSObj.getTeacherAnswerDetail().getAnswerId())));
					sb.append("</div>");
					//end all User Score
					}
					sb.append("</div>");
					flagforScoreShow=false;
					}
					sb.append("</div>");


					List<TeacherStatusNotes> lstTeacherStatusNotes = null;

					lstTeacherStatusNotes = getUserIdAndAnswerIdTSNMap.get(teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"#"+um.getUserId());
						//teacherStatusNotesDAO.getQuestionNoteObjByUserMaster(teacherDetail, jobOrder, jobOrder.getDistrictMaster(), teacherAssessmentQuestion.getTeacherAssessmentQuestionId(),userMaster);
					String statusNote = "";
					
					boolean flagforShowAddNote=false;
					if(lstTeacherStatusNotes!=null && lstTeacherStatusNotes.size()>0){
						for(TeacherStatusNotes teacherStatusNotes:lstTeacherStatusNotes){
							
					if(teacherStatusNotes!=null)
						statusNote = teacherStatusNotes.getStatusNotes();


					//@Ashish :: add Note for every question 
					sb.append("<div  class='row mt5'>");
					sb.append("<div class='left16 inner_disable_jqte' id='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' >");
					sb.append("<label >Note</label>&nbsp;&nbsp;&nbsp;&nbsp;");
					
						sb.append("<label>("+um.getFirstName()+" "+um.getLastName()+", "+DATE_TIME_FORMAT.format(teacherStatusNotes.getCreatedDateTime())+")</label>");
						if(isSuperAdminUser){
							sb.append("<div>");
							if(teacherStatusNotes.isFinalizeStatus()){
								sb.append("<textarea id='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='2'>"+statusNote+"</textarea>");
								increaseOrNot=false;
							}else{
								if(thisUser){
								sb.append("<textarea id='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='2'>"+statusNote+"</textarea>");
								increaseOrNot=false;
								}
							}
							sb.append("<div id='main"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'>");
							sb.append("<a "+clickable+" class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"' rel='tooltip' href='javascript:void(0);' id='edit_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' onclick='editTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/edit-notes.png' width='25px' height='30px;'/></a>");
							sb.append("<br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("  lblSaveNote", locale)+"' rel='tooltip' href='javascript:void(0);' id='save_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='saveTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/save-notes.png' width='25px' height='30px;'/></a>");
							sb.append("<br/><br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("  btnClr", locale)+"' rel='tooltip' href='javascript:void(0);' id='cancel_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='cancelTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/cancel-notes.jpg' width='25px' height='30px;'/></a><br/>");
							sb.append("</div>");
							sb.append("<br/></div>");
						}else{
						if(teacherStatusNotes.isFinalizeStatus()){
							sb.append("<div class='' style='background-color:#EEEEEE; width:585px; height:80px; border:1px solid #CCCCCC; overflow:scroll; '>"+statusNote+"</div>");
							increaseOrNot=false;
						}else{
							if(thisUser){
							sb.append("<textarea id='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' name='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' class='span6' rows='2'>"+statusNote+"</textarea>");
							increaseOrNot=true;
							}
						}
						}
					
					sb.append("</div>");
					sb.append("</div>");											
						}
					}
					if(thisUser){
						int isJSI=1;
						String allInfoAboutNote="'"+teacherDetail.getTeacherId()+","+jobOrder.getJobId()+","+statusId+","+secondaryStatusId+","+userMaster.getUserId()+","+jobOrder.getDistrictMaster().getDistrictId()+","+ teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+","+isJSI+"'";
						if(lstTeacherStatusNotes==null || lstTeacherStatusNotes.size()==0){
						sb.append("<div class='row mt5'>");
						sb.append("<div class='left16' id='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' >");
						sb.append("<label id='label"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' style='width:100%;'>Note&nbsp;&nbsp;&nbsp;&nbsp;");
						sb.append("("+um.getFirstName()+" "+um.getLastName()+")</label>");
						if(isSuperAdminUser){
						sb.append("<div class='inner_enable_jqte'>");
						}
						sb.append("<textarea id='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' name='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' class='span6' rows='2'></textarea>");
						if(isSuperAdminUser){
						sb.append("<div style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'><a "+clickable+" class='tooltipforicon' data-original-title='Add' rel='tooltip' href='javascript:void(0);' onclick=\"addTeacherStatusNotes(this,"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+","+allInfoAboutNote+");\"><img src='images/add_notes.png' width='25px' height='30px;'/></a></div>");
						sb.append("<br/></div>");
						}
						sb.append("</div>");
						sb.append("</div>");
						sb.append("</td>");
						sb.append("</tr>");					
						increaseOrNot=true;
						println("--------------------------------------------------------------------------------------");
						}
						if(isSuperAdminUser && !increaseOrNot){	
							sb.append("<div class='row mt5'>");
							sb.append("<div class='left16' id='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' >");
							sb.append("<label id='label"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' style='width:100%;'>Note&nbsp;&nbsp;&nbsp;&nbsp;");
							sb.append("("+um.getFirstName()+" "+um.getLastName()+")</label>");
							if(isSuperAdminUser){
							sb.append("<div class='inner_enable_jqte'>");
							}
							sb.append("<textarea id='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' name='questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"' class='span6' rows='2'></textarea>");
							if(isSuperAdminUser){
							sb.append("<div style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'><a "+clickable+" class='tooltipforicon' data-original-title='Add' rel='tooltip' href='javascript:void(0);' onclick=\"addTeacherStatusNotes(this,"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+","+allInfoAboutNote+");\"><img src='images/add_notes.png' width='25px' height='30px;'/></a></div>");
							sb.append("<br/></div>");
							}
							sb.append("</div>");
							sb.append("</div>");
							sb.append("</td>");
							sb.append("</tr>");																					
						}
					}

					if(increaseOrNot){
						println("user id=="+um.getUserId()+" "+teacherAssessmentQuestion.getTeacherAssessmentQuestionId());
					if(!qqNote.equals(""))
						qqNote = qqNote+"#"+"questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId();
					else
						qqNote = "questionNotes"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId();
					}
				}
				}
					if(increaseOrNot){
					if(questionAssessmentIds!=null && !questionAssessmentIds.equals("")){
						questionAssessmentIds = questionAssessmentIds+"#"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId().toString();
						defauntQuestionNote++;
						increaseOrNot=false;
					}else{
						questionAssessmentIds = teacherAssessmentQuestion.getTeacherAssessmentQuestionId().toString();
						defauntQuestionNote++;
						increaseOrNot=false;
					}
					}
				}
			}
			
			
			
			
			// Second part of question
			
			//Extra Question 
			for(Map.Entry<Integer, StatusSpecificQuestions> entryQuestion:allExtraQuestionMap.entrySet()){
				Integer qId=entryQuestion.getKey();
				StatusSpecificQuestions question=entryQuestion.getValue();
				boolean flagForAttribute=false;
				if(question.getQuestion()!=null && !question.getQuestion().equalsIgnoreCase(""))
				{		
					sb.append("<tr>");
					sb.append("<td width='5%' valign='top'><label  class='labletxt'><b>Q. "+(++questionCounter)+"</b></label></td>");
					sb.append("<td width='95%'><label   class='labletxt'>"+teacherAssessmentQuestion.getQuestion());
					sb.append("</label></td>");
					sb.append("</tr>");											
					if(question.getQuestion()!=null && !question.getQuestion().equals("") && question.getSkillAttributesMaster()!=null){
						sb.append("<tr>");
						sb.append("<td width='5%' valign='top'><label  class='lableAtxt'><b>"+Utility.getLocaleValuePropByKey("  lblAttribute1", locale)+":</b></label></td>");
						sb.append("<td width='95%'><label   class='lableAtxt'>"+question.getSkillAttributesMaster().getSkillName());
						sb.append("</label></td>");
						sb.append("</tr>");														
					}						
				}else if((question.getQuestion()==null || question.getQuestion().equals("")) && question.getSkillAttributesMaster()!=null){
					{
						sb.append("<tr>");
						sb.append("<td width='5%' valign='top'><label  class='lableAtxt'><b>"+Utility.getLocaleValuePropByKey("  lblAttribute1", locale)+":</b></label></td>");
						sb.append("<td width='95%'><label   class='lableAtxt'>"+question.getSkillAttributesMaster().getSkillName());
						sb.append("</label></td>");
						sb.append("</tr>");	
						flagForAttribute=true;													
					}	
				}
				Double maxScore=0.0;
				int aScoreProvided=0;
				int scoreQuestionId=0;
			boolean flagforScoreShow=true;	
			boolean increaseOrNot=false;
			boolean thisUser=false;
			for(Map.Entry<Integer, UserMaster> entry:mapUserMaster.entrySet())
			{	
				UserMaster um=entry.getValue();
				println("key--------extra-------->::"+qId+"#"+um.getUserId());				//
			List<StatusSpecificScore> lstSSS=getUserIdAndQuestionIdSSSMap.get(qId+"#"+um.getUserId());
			if(lstSSS!=null)
			for(StatusSpecificScore SSObj:lstSSS){
			boolean flagforDisableOrEnableScoreBecauseAnotherDAScore=true;
			int tickInterval=1;
			if(flagforScoreShow){
				StatusSpecificScore SSObj1=mapExtraScoreFirstSSS.get(SSObj.getStatusSpecificQuestions().getQuestionId());						
			if(SSObj1!=null){
				if(!SSObj1.getUserMaster().getUserId().toString().equals(userMaster.getUserId().toString()))
				flagforDisableOrEnableScoreBecauseAnotherDAScore=false;
				else{					
				if(SSObj1.getScoreProvided()!=null){
					aScoreProvided=SSObj1.getScoreProvided();
				}					
				}
				if(SSObj1.getMaxScore()!=null){
					maxScore=SSObj1.getMaxScore();
				}
				if(SSObj1!=null)
					scoreQuestionId=SSObj1.getAnswerId();
			}
			
			if(maxScore%10==0)
				tickInterval=10;
			else if(maxScore%5==0)
				tickInterval=5;
			else if(maxScore%3==0)
				tickInterval=3;
			else if(maxScore%2==0)
				tickInterval=2;					
			}

			if(maxScore>0){
				if(flagforScoreShow)
				iCountScoreSlider++;

				//Add Code for JSI
				if(SSObj.getFinalizeStatus()==1)
					iQdslider=0;
				else
					if(flagforDisableOrEnableScoreBecauseAnotherDAScore)
					iQdslider=1;
					else
						iQdslider=0;

				sb.append("<tr>");
				sb.append("<td width='5%' valign='top'>&nbsp;</td>");
				sb.append("<td width='95%'>");		
				if(flagforScoreShow){
				sb.append("<div class='row ' style='padding-left: 12px;'>");
				sb.append("<div style='float:left;' class='span5 slider_question'><iframe id=\"ifrmQuestion_"+iCountScoreSlider+"\" src=\"slideract.do?name=questionFrm&tickInterval="+tickInterval+"&max="+maxScore+"&dslider="+iQdslider+"&swidth="+scoreWidth+"&step=1&answerId="+scoreQuestionId+"&svalue="+aScoreProvided+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:"+scoreWidth1+"px;margin-top:-11px;\"></iframe></div>");
				if(userMaster.getEntityType()==2){
				sb.append("<div style='float:right;'><a class='questionScore' id='question"+qId+"' title='' data-original-title='' data-placement='above' href='javascript:void(0);' onclick='clickOnQuestionScore(this);'><span  class='fa-crosshairs icon-large iconcolor' ></span></a></div>");
				//for all User Score
				sb.append("<div id='scoreTablequestion"+qId+"' style='display:none;'>");
				//logger.info("teacherDetail=="+teacherDetail+" jobOrder=="+jobOrder+" statusMaster=="+statusMaster+" secondaryStatus=="+secondaryStatus);
				println("qId=--------------------------------==="+qId+"    value===---------------------=="+getScoreHtmlTableMap.get(String.valueOf(qId)));
				sb.append(getScoreHtmlTableMap.get(String.valueOf(qId)));
				sb.append("</div>");
				//end all User Score
				}
				sb.append("</div>");
				flagforScoreShow=false;
				}
				sb.append("</div>");
				List<TeacherStatusNotes> lstTeacherStatusNotes = null;

				lstTeacherStatusNotes = getExtraUserIdAndAnswerIdTSNMap.get(qId+"#"+um.getUserId());					
				String statusNote = "";
				println("user check====out===="+um.getUserId() +"      ==="+userMaster.getUserId());
				thisUser=um.getUserId().toString().equalsIgnoreCase(userMaster.getUserId().toString());
				
				if(!flagForAttribute){
				if(lstTeacherStatusNotes!=null && lstTeacherStatusNotes.size()>0){
					for(TeacherStatusNotes teacherStatusNotes:lstTeacherStatusNotes){
						
				if(teacherStatusNotes!=null)
					statusNote = teacherStatusNotes.getStatusNotes();


				//add Note for every question 
				sb.append("<div class='row mt5'>");
				sb.append("<div class='left16 inner_disable_jqte' id='questionNotes"+qId+"' >");
				sb.append("<label >Note</label>&nbsp;&nbsp;&nbsp;&nbsp;");
				
					sb.append("<label>("+um.getFirstName()+" "+um.getLastName()+", "+DATE_TIME_FORMAT.format(teacherStatusNotes.getCreatedDateTime())+")</label>");
					if(isSuperAdminUser){
						sb.append("<div>");
						if(teacherStatusNotes.isFinalizeStatus()){
							sb.append("<textarea id='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='2'>"+statusNote+"</textarea>");
							increaseOrNot=false;
						}else{								
							sb.append("<textarea id='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' name='questionSuperNotes"+teacherStatusNotes.getTeacherStatusNoteId()+"' class='span6' rows='2'>"+statusNote+"</textarea>");
							increaseOrNot=false;								
						}
						sb.append("<div id='main"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'>");
						sb.append("<a "+clickable+" class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"' rel='tooltip' href='javascript:void(0);' id='edit_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' onclick='editTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/edit-notes.png' width='25px' height='30px;'/></a>");
						sb.append("<br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("  lblSaveNote", locale)+"' rel='tooltip' href='javascript:void(0);' id='save_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='saveTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/save-notes.png' width='25px' height='30px;'/></a>");
						sb.append("<br/><br/><a class='tooltipforicon' data-original-title='"+Utility.getLocaleValuePropByKey("  btnClr", locale)+"' rel='tooltip' href='javascript:void(0);' id='cancel_notes"+teacherStatusNotes.getTeacherStatusNoteId()+"' style='display:none;' onclick='cancelTeacherStatusNotes(this,"+teacherStatusNotes.getTeacherStatusNoteId()+");'><img src='images/cancel-notes.jpg' width='25px' height='30px;'/></a><br/>");
						sb.append("</div>");
						sb.append("<br/></div>");
					}else{
					if(teacherStatusNotes.isFinalizeStatus()){
						sb.append("<div class='' style='background-color:#EEEEEE; width:585px; height:80px; border:1px solid #CCCCCC; overflow:scroll; '>"+statusNote+"</div>");
						increaseOrNot=false;
					}else{
						if(thisUser){
						sb.append("<textarea id='questionNotes"+qId+"' name='questionNotes"+qId+"' class='span6' rows='2'>"+statusNote+"</textarea>");
						increaseOrNot=true;
						}
					}
					}
				
					sb.append("</div>");
					sb.append("</div>");									
					}						
				}
				if(thisUser){
					int isJSI=1;
					String allInfoAboutNote="'"+teacherDetail.getTeacherId()+","+jobOrder.getJobId()+","+statusId+","+secondaryStatusId+","+userMaster.getUserId()+","+jobOrder.getDistrictMaster().getDistrictId()+","+ qId+","+isJSI+"'";
					if(flagforDisableOrEnableScoreBecauseAnotherDAScore && (lstTeacherStatusNotes==null || lstTeacherStatusNotes.size()==0)){											
					sb.append("<div class='row mt5'>");
					sb.append("<div class='left16' id='questionNotes"+qId+"' >");
					sb.append("<label id='label"+qId+"' style='width:100%;'>"+Utility.getLocaleValuePropByKey("  blNote", locale)+"&nbsp;&nbsp;&nbsp;&nbsp;");
					sb.append("("+um.getFirstName()+" "+um.getLastName()+")</label>");
					if(isSuperAdminUser){
						sb.append("<div class='inner_enable_jqte'>");
						}
					sb.append("<textarea id='questionNotes"+qId+"' name='questionNotes"+qId+"' class='span6' rows='2'></textarea>");
					if(isSuperAdminUser){
						sb.append("<div style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'><a "+clickable+" class='tooltipforicon' data-original-title='Add' rel='tooltip' href='javascript:void(0);' onclick=\"addTeacherStatusNotes(this,"+qId+","+allInfoAboutNote+");\"><img src='images/add_notes.png' width='25px' height='30px;'/></a></div>");
						sb.append("</div>");
						}
					sb.append("</div>");
					sb.append("</div>");
					sb.append("</td>");
					sb.append("</tr>");
					increaseOrNot=true;
					}
					if(flagforDisableOrEnableScoreBecauseAnotherDAScore && isSuperAdminUser && !increaseOrNot){							
						sb.append("<div class='row mt5'>");
						sb.append("<div class='left16' id='questionNotes"+qId+"' >");
						sb.append("<label id='label"+qId+"' style='width:100%;'>Note&nbsp;&nbsp;&nbsp;&nbsp;");
						sb.append("("+um.getFirstName()+" "+um.getLastName()+")</label>");
						if(isSuperAdminUser){
							sb.append("<div class='inner_enable_jqte'>");
							}
						sb.append("<textarea id='questionNotes"+qId+"' name='questionNotes"+qId+"' class='span6' rows='2'></textarea>");
						if(isSuperAdminUser){
							sb.append("<div style='float:right;padding-right:50px;vertical-align:middle;padding-top:65px;'><a "+clickable+" class='tooltipforicon' data-original-title='Add' rel='tooltip' href='javascript:void(0);' onclick=\"addTeacherStatusNotes(this,"+qId+","+allInfoAboutNote+");\"><img src='images/add_notes.png' width='25px' height='30px;'/></a></div>");
							sb.append("</div>");
							}
						sb.append("</div>");
						sb.append("</div>");
						sb.append("</td>");
						sb.append("</tr>");															
					}
				}
				}

				if(increaseOrNot){
					println("user id=="+um.getUserId()+" "+qId);
				if(!qqNote.equals(""))
					qqNote = qqNote+"#"+"questionNotes"+qId;
				else
					qqNote = "questionNotes"+qId;
				}
			}else{	
				if(thisUser){
				defauntQuestionNote++;						
				}						
				}
			}
				if(increaseOrNot){
				if(questionAssessmentIds!=null && !questionAssessmentIds.equals("")){
					questionAssessmentIds = questionAssessmentIds+"#"+qId;
					increaseOrNot=false;
				}else{
					questionAssessmentIds = ""+qId;
					increaseOrNot=false;
				}
				}					
			}
			}
			sb.append("</table>");			
		}
		//iQdslider,iCountScoreSlider,qqNote,defauntQuestionNote,questionAssessmentIds,html
		allValue[0]=""+iQdslider;
		allValue[1]=""+iCountScoreSlider;
		allValue[2]=qqNote;
		allValue[3]=""+defauntQuestionNote;
		allValue[4]=questionAssessmentIds;
		allValue[5]=sb.toString();
		return allValue;
	}
	 private Map<UserMaster,List<TeacherStatusNotes>> getAllNotesByUserMap(List<StatusSpecificScore> lstStatusSpecificScore){
		 List<UserMaster> listUserMaster =new ArrayList<UserMaster>();
		 Map<UserMaster,List<TeacherStatusNotes>> getAllNotesByUserMap=new HashMap<UserMaster, List<TeacherStatusNotes>>();
		 for(StatusSpecificScore score:lstStatusSpecificScore){listUserMaster.add(score.getUserMaster());}
		 StatusSpecificScore sss=null;
		 if(lstStatusSpecificScore.size()>0){
			 sss=lstStatusSpecificScore.get(0);
		 }
		 List<TeacherStatusNotes> lstTSN=null;
		 if(sss!=null){
			 lstTSN=teacherStatusNotesDAO.findByCriteria(Restrictions.eq("teacherDetail", sss.getTeacherDetail()),Restrictions.eq("jobOrder", sss.getJobOrder()),Restrictions.in("userMaster", listUserMaster),Restrictions.eq("districtId", sss.getDistrictId()),Restrictions.isNotNull("teacherAssessmentQuestionId"));
			 for(TeacherStatusNotes tsn:lstTSN){
				 if(getAllNotesByUserMap.get(tsn.getUserMaster())==null) {
					 List<TeacherStatusNotes> lstAllTSN=new ArrayList<TeacherStatusNotes>();
					 lstAllTSN.add(tsn);
					 getAllNotesByUserMap.put(tsn.getUserMaster(),lstAllTSN );					 
				 }else{
					 getAllNotesByUserMap.get(tsn.getUserMaster()).add(tsn);
				 }
			 }
		 }
		 return getAllNotesByUserMap;
	 }		
}
