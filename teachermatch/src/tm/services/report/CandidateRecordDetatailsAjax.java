package tm.services.report;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.services.PaginationAndSorting;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class CandidateRecordDetatailsAjax {

	String locale = Utility.getValueOfPropByKey("locale");
	String lblApplicantName=Utility.getLocaleValuePropByKey("lblApplicantName", locale);
	String lblPresentPhone1=Utility.getLocaleValuePropByKey("lblPresentPhone1", locale);
	String lblCellPhone1=Utility.getLocaleValuePropByKey("lblCellPhone1", locale);
	String lblPremanentAddress1=Utility.getLocaleValuePropByKey("lblPremanentAddress1", locale);
	String lblPremanentCity1=Utility.getLocaleValuePropByKey("lblPremanentCity1", locale);
	String lblPremanentState1=Utility.getLocaleValuePropByKey("lblPremanentState1", locale);
	String lblPremanentZip1=Utility.getLocaleValuePropByKey("lblPremanentZip1", locale);
	String lblPremanentPhone1=Utility.getLocaleValuePropByKey("lblPremanentPhone1", locale);
	String lblNA=Utility.getLocaleValuePropByKey("lblNA", locale);
	String lblNoRecordsFound1=Utility.getLocaleValuePropByKey("lblNoRecordsFound1", locale);
	String lblApplicantNumber1=Utility.getLocaleValuePropByKey("lblApplicantNumber1", locale);
	String lblEmailAddress=Utility.getLocaleValuePropByKey("lblEmailAddress", locale);
	String lblFname=Utility.getLocaleValuePropByKey("lblFname", locale);
	String lblMiddleName=Utility.getLocaleValuePropByKey("lblMiddleName", locale);
	String lblLastName=Utility.getLocaleValuePropByKey("lblLastName", locale);
	String lblHomeCellPhone1=Utility.getLocaleValuePropByKey("lblHomeCellPhone1", locale);
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

    @Autowired
    private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
    public void setTeacherPersonalInfoDAO(
			TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
  


	public String displayAllCandidateRecordsByEntityType(Integer districtID,String noOfRow,String pageNo,String sortOrder,String sortOrderType){
		
		System.out.println("***********displayAllCandidateRecordsByEntityType****************"+districtID);
		
		StringBuffer sb = new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		DistrictMaster districtMaster = null;
        List<JobForTeacher>  lstJobForTeachers = null;
        List<JobForTeacher>  finalJobForTeachers = null;
        
		TeacherPersonalInfo teacherPersonalInfo = null;
		TeacherDetail teacherDetail = null;
		Integer teacherID = null;
		
		try{
			
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start = ((pgNo-1)*noOfRowInPage);
		int end = ((pgNo-1)*noOfRowInPage)+noOfRowInPage;
		int totalRecord = 0;
		boolean teacherDetailFlag =false;
		
		/** set default sorting fieldName **/
		String sortOrderFieldName = "firstName";
		
		/**Start set dynamic sorting fieldName **/
		 
		Order sortOrderStrVal = null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName = sortOrder;
			}
		}
		
		String sortOrderTypeVal = "0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal = Order.asc(sortOrderFieldName);
			}
			else{
				sortOrderTypeVal = "1";
				sortOrderStrVal = Order.desc(sortOrderFieldName);
			}
		}
		else{
			sortOrderTypeVal = "0";
			sortOrderStrVal = Order.asc(sortOrderFieldName);
		}
		/**end set dynamic sorting fieldName **/
		
		
		if(districtID!=null)
		{
			districtMaster=new DistrictMaster();
			districtMaster.setDistrictId(districtID);
		}
			//districtMaster = districtMasterDAO.findById(districtID, false, false);
		
		if(sortOrderStrVal.toString().contains("firstName")||sortOrderStrVal.toString().contains("phoneNumber"))
			teacherDetailFlag = true;
		
		lstJobForTeachers = jobForTeacherDAO.hiredCandidateListByDistrict(districtMaster,sortOrderStrVal,teacherDetailFlag);
		 		
		sb.append("<table  id='tblGridHired' width='100%' border='0'  >");
		sb.append("<thead class='bg'>");
		sb.append("<tr>");
		String responseText="";
		
		responseText=PaginationAndSorting.responseSortingLink(lblApplicantName,sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo);
		sb.append("<th width='15%' valign='top'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(lblPresentPhone1,sortOrderFieldName,"phoneNumber",sortOrderTypeVal,pgNo);
		sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(lblCellPhone1,sortOrderFieldName,"mobileNumber",sortOrderTypeVal,pgNo);
		sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(lblPremanentAddress1,sortOrderFieldName,"addressLine1",sortOrderTypeVal,pgNo);
		sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(lblPremanentCity1,sortOrderFieldName,"cityId",sortOrderTypeVal,pgNo);
		sb.append("<th width='10%' valign='top'>"+responseText+"</th>");

		responseText=PaginationAndSorting.responseSortingLink(lblPremanentState1,sortOrderFieldName,"stateId",sortOrderTypeVal,pgNo);
		sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(lblPremanentZip1,sortOrderFieldName,"zipCode",sortOrderTypeVal,pgNo);
		sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(lblPremanentPhone1,sortOrderFieldName,"Premanent Phone",sortOrderTypeVal,pgNo);
		sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
		sb.append("</tr>");
		sb.append("</thead>");
		
		sb.append("<tbody>");
		
		if(lstJobForTeachers!=null){
		 if(lstJobForTeachers.size()>0){
			 totalRecord =  lstJobForTeachers.size();
			 }
			 
			 if(totalRecord>end){
				 end =totalRecord;
				 finalJobForTeachers =lstJobForTeachers.subList(start, end);
			}else{
				 end =totalRecord;
				 finalJobForTeachers =lstJobForTeachers.subList(start, end);
				System.out.println("finalJobForTeachers : "+finalJobForTeachers);
			}
			 
			 List<TeacherPersonalInfo> teacherPersonalInfoList  = new ArrayList<TeacherPersonalInfo>();
			 Map<Integer, TeacherPersonalInfo> mapTInfo=new TreeMap<Integer, TeacherPersonalInfo>();
			 ArrayList<Integer> teacherIdlst=new ArrayList<Integer>();
			 if(finalJobForTeachers!=null && finalJobForTeachers.size()>0)
			 {
				 for(JobForTeacher list:finalJobForTeachers)
					 teacherIdlst.add(list.getTeacherId().getTeacherId());
				 
				 if(teacherIdlst!=null && teacherIdlst.size()>0)
				 {
					 teacherPersonalInfoList=teacherPersonalInfoDAO.findByTeacherIds(teacherIdlst);
					 if(teacherPersonalInfoList!=null && teacherPersonalInfoList.size()>0)
						 for(TeacherPersonalInfo personalInfo:teacherPersonalInfoList)
							 mapTInfo.put(personalInfo.getTeacherId(), personalInfo);
				 }
			 }
			 
		for(JobForTeacher list:finalJobForTeachers)
		{
			teacherID = list.getTeacherId().getTeacherId();
			teacherDetail =list.getTeacherId();
			if(mapTInfo!=null && mapTInfo.get(list.getTeacherId().getTeacherId())!=null)
				teacherPersonalInfo=mapTInfo.get(list.getTeacherId().getTeacherId());
			
			sb.append("<tr>");
			
			if((teacherPersonalInfo!=null && teacherPersonalInfo.getFirstName()!=null) || (teacherDetail.getEmailAddress()!=null))
			{
				sb.append("<td width='15%'>");
				if(teacherPersonalInfo!=null && teacherPersonalInfo.getFirstName()!=null)
					sb.append(teacherPersonalInfo.getFirstName()+" "+teacherPersonalInfo.getLastName()+"\n");
					sb.append("("+teacherDetail.getEmailAddress()+")");
				sb.append("</td>");
			}
			else
				sb.append("<td width='15%'>"+lblNA+"</td>");
			
			if(teacherPersonalInfo!=null && teacherPersonalInfo.getPhoneNumber()!=null)
				sb.append("<td width='10%'>"+teacherPersonalInfo.getPhoneNumber()+"</td>");
			else
				sb.append("<td width='10%'>"+lblNA+"</td>");
			
			if(teacherPersonalInfo!=null && teacherPersonalInfo.getMobileNumber()!=null && !teacherPersonalInfo.getMobileNumber().equalsIgnoreCase(""))
				sb.append("<td width='10%'>"+teacherPersonalInfo.getMobileNumber()+"</td>");
			else
				sb.append("<td width='10%'>"+lblNA+"</td>");
			
			if(teacherPersonalInfo!=null && teacherPersonalInfo.getAddressLine1()!=null)
				sb.append("<td width='20%'>"+teacherPersonalInfo.getAddressLine1()+"</td>");
			else
				sb.append("<td width='20%'>"+lblNA+"</td>");
			
			if(teacherPersonalInfo!=null && teacherPersonalInfo.getCityId()!=null)
			{
				if(teacherPersonalInfo.getCityId().getCityName()!=null)
					sb.append("<td width='10%'>"+teacherPersonalInfo.getCityId().getCityName()+"</td>");
				else
				sb.append("<td width='10%'>"+lblNA+"</td>");
			}
			else
			{
				sb.append("<td width='10%'>"+lblNA+"</td>");
			}
			
			if(teacherPersonalInfo!=null && teacherPersonalInfo.getStateId()!=null)
			{
				if(teacherPersonalInfo.getStateId().getStateName()!=null)
					sb.append("<td width='10%'>"+teacherPersonalInfo.getStateId().getStateName()+"</td>");
				else
					sb.append("<td width='10%'>"+lblNA+"</td>");
			}
			else
			{
				sb.append("<td width='10%'>"+lblNA+"</td>");
			}
			
			if(teacherPersonalInfo!=null && teacherPersonalInfo.getZipCode()!=null)
				sb.append("<td width='10%'>"+teacherPersonalInfo.getZipCode()+"</td>");
			else
				sb.append("<td width='10%'>"+lblNA+"</td>");

			if(teacherPersonalInfo!=null && teacherDetail.getPhoneNumber()!=null && !teacherDetail.getPhoneNumber().equalsIgnoreCase(""))
				sb.append("<td width='10%'>"+teacherDetail.getPhoneNumber()+"</td>");
			else
				sb.append("<td width='10%'>"+lblNA+"</td>");
			
			sb.append("</tr>");
     	
		}
		
		
		
		if(finalJobForTeachers!=null && finalJobForTeachers.size()==0){
			sb.append("<tr id='tr1'><td id='tr1'>"+lblNoRecordsFound1+"</td></tr>");
		}
		try {
			finalJobForTeachers=null;
			lstJobForTeachers=null;
			teacherPersonalInfoList=null;
			teacherIdlst=null;
			mapTInfo=null;
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	 }else{
		 sb.append("<tr id='tr1'><td id='tr1'>"+lblNoRecordsFound1+"</td></tr>");
	 }	
		sb.append("</tbody>");
		sb.append("</table>");
		sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		
		}catch(Exception e){
			e.printStackTrace();
		    return null;
		}
	return sb.toString();
	}
	
  public String displaySmartFusionRecordsByEntityTypeEXL(Integer districtID,String noOfRow,String pageNo,String sortOrder,String sortOrderType){
	  
	  System.out.println("********displaySmartFusionRecordsByEntityTypeEXL*******");
	  
	  WebContext context;
	  context = WebContextFactory.get();
	  HttpServletRequest request = context.getHttpServletRequest();
	  HttpSession session = request.getSession(false);
	  
	  DistrictMaster districtMaster = null;
      List<JobForTeacher>  lstJobForTeachers = null;
      List<JobForTeacher>  finalJobForTeachers = null;
      
		TeacherPersonalInfo teacherPersonalInfo = null;
		TeacherDetail teacherDetail = null;
		Integer teacherID = null;
		boolean teacherDetailFlag =false;
		WritableSheet excelSheet = null;
		WritableWorkbook workbook = null;
		Label label;
		
		String fileName = null;
		
		try{
			
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start = ((pgNo-1)*noOfRowInPage);
		int end = ((pgNo-1)*noOfRowInPage)+noOfRowInPage;
		int totalRecord = 0;
		
		/** set default sorting fieldName **/
		String sortOrderFieldName = "firstName";
		
		/**Start set dynamic sorting fieldName **/
		 
		Order sortOrderStrVal = null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName = sortOrder;
			}
		}
		
		String sortOrderTypeVal = "0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal = Order.asc(sortOrderFieldName);
			}
			else{
				sortOrderTypeVal = "1";
				sortOrderStrVal = Order.desc(sortOrderFieldName);
			}
		}
		else{
			sortOrderTypeVal = "0";
			sortOrderStrVal = Order.asc(sortOrderFieldName);
		}
		/**end set dynamic sorting fieldName **/
		if(districtID!=null)
		{
			districtMaster=new DistrictMaster();
			districtMaster.setDistrictId(districtID);
		}
		
		if(sortOrderStrVal.toString().contains("firstName")||sortOrderStrVal.toString().contains("phoneNumber")){
			teacherDetailFlag = true;
		}
		
		lstJobForTeachers = jobForTeacherDAO.hiredCandidateListByDistrict(districtMaster,sortOrderStrVal,teacherDetailFlag);
		// System.out.println("lstJobForTeachers size : "+lstJobForTeachers);
		
		 
	   //Excel SmartFusion Exporting	 
		 String time = String.valueOf(System.currentTimeMillis()).substring(6);
		 String basePath = request.getSession().getServletContext().getRealPath("/")+"smartFusionReport";
		 
		 fileName ="smartFusionReport"+time+".xls";
		 
		 File file = new File(basePath);
		 if(!file.exists()){
			 file.mkdir();
		 }
		 
		 Utility.deleteAllFileFromDir(basePath);
		 file = new File(basePath+"/"+fileName);
		 
		 WorkbookSettings wbSettings = new WorkbookSettings();
		 wbSettings.setLocale(new Locale("en","EN"));
		 
		    WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;
		 
		 workbook = Workbook.createWorkbook(file,wbSettings);
		 workbook.createSheet("smartFustionReport", 0);
		 
		 excelSheet = workbook.getSheet(0);
		 
		 WritableFont times10pt = new WritableFont(WritableFont.ARIAL,10);
		 //define the cell formate
		 times = new WritableCellFormat(times10pt);
		 
		 //lets automatically wrap the cells
		 times.setWrap(true);
		 
		// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);
			
			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);
			
			header.setBackground(Colour.GRAY_25);
			
			// write headers
			
			//excelSheet.mergeCells(0, 0, 6, 1);
			
			
			/*label = new Label(0, 0, "Smart Fusion Report",timesBoldUnderline);
			excelSheet.addCell(label);
			
			//excelSheet.mergeCells(0, 3, 6, 3);*/
			
			excelSheet.getSettings().setDefaultColumnWidth(18);
			
			int k=0;
			int col=1;
			
			label = new Label(0, k, "Applicant Number",header);
			excelSheet.addCell(label);
			
			label = new Label(1, k, "Email Address",header);
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "First Name",header);
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "Middle Name",header);
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "Last Name",header);
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "Present Phone", header);
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "Home/CellPhone", header);
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "Permanent Phone", header);
			excelSheet.addCell(label);
		 
			label = new Label(++col, k, "Permanent Address", header);
			excelSheet.addCell(label);
		 
			label = new Label(++col, k, "Permanent City", header);
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "Permanent State", header);
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "Permanent Zip", header);
			excelSheet.addCell(label);
			
			k=k+1;
			
			

			if(lstJobForTeachers!=null)
			{
			 if(lstJobForTeachers.size()>0){
			 totalRecord =  lstJobForTeachers.size();
			 }
			
			 System.out.println("totalRecord: "+totalRecord+" end: "+end+" start: "+start+" sortOrderFieldName: "+sortOrderFieldName);
			 if(totalRecord>end){
				 end =totalRecord;
				 finalJobForTeachers =lstJobForTeachers.subList(start, end);
			}else{
				 end =totalRecord;
				 finalJobForTeachers =lstJobForTeachers.subList(start, end);
			}
			
			
			if(finalJobForTeachers.size()==0){
				excelSheet.mergeCells(0, k, 9, k);
				label = new Label(0, k, "No Rcords Founds");
				excelSheet.addCell(label);
			}
		
			 List<TeacherPersonalInfo> teacherPersonalInfoList  = new ArrayList<TeacherPersonalInfo>();
			 Map<Integer, TeacherPersonalInfo> mapTInfo=new TreeMap<Integer, TeacherPersonalInfo>();
			 ArrayList<Integer> teacherIdlst=new ArrayList<Integer>();
			 
			 if(finalJobForTeachers!=null && finalJobForTeachers.size()>0)
			 {
				 for(JobForTeacher list:finalJobForTeachers)
					 teacherIdlst.add(list.getTeacherId().getTeacherId());
				 
				 if(teacherIdlst!=null && teacherIdlst.size()>0)
				 {
					 teacherPersonalInfoList=teacherPersonalInfoDAO.findByTeacherIds(teacherIdlst);
					 if(teacherPersonalInfoList!=null && teacherPersonalInfoList.size()>0)
						 for(TeacherPersonalInfo personalInfo:teacherPersonalInfoList)
							 mapTInfo.put(personalInfo.getTeacherId(), personalInfo);
				 }
			 }
		
			 if(finalJobForTeachers!=null && finalJobForTeachers.size()>0)
			 {
				 
			 
		for(JobForTeacher list:finalJobForTeachers)
		{
			teacherID = list.getTeacherId().getTeacherId();
			teacherDetail =list.getTeacherId();
			if(mapTInfo!=null && mapTInfo.get(list.getTeacherId().getTeacherId())!=null)
				teacherPersonalInfo=mapTInfo.get(list.getTeacherId().getTeacherId());
			
					Integer count=1;
					col = 1;
					
					label = new Label(0, k, String.valueOf(count));
					excelSheet.addCell(label);
					
					if(teacherDetail.getEmailAddress()!=null && !teacherDetail.getEmailAddress().equalsIgnoreCase("")){
					    label = new Label(1, k, teacherDetail.getEmailAddress());
					    excelSheet.addCell(label);
					}else{
						label = new Label(1, k, "N/A");
						excelSheet.addCell(label);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getFirstName()!=null && !teacherPersonalInfo.getFirstName().equalsIgnoreCase("")){
						label = new Label(++col, k, teacherPersonalInfo.getFirstName());
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, "N/A");
						excelSheet.addCell(label);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getMiddleName()!=null && !teacherPersonalInfo.getMiddleName().equalsIgnoreCase("")){
						label = new Label(++col, k, teacherPersonalInfo.getMiddleName());
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, "N/A");
						excelSheet.addCell(label);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getLastName()!=null && !teacherPersonalInfo.getLastName().equalsIgnoreCase("")){
						label = new Label(++col, k, teacherPersonalInfo.getLastName());
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, "N/A");
						excelSheet.addCell(label);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getPhoneNumber()!=null && !teacherPersonalInfo.getPhoneNumber().equalsIgnoreCase("")){
						label = new Label(++col, k, teacherPersonalInfo.getPhoneNumber());
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, "N/A");
						excelSheet.addCell(label);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getMobileNumber()!=null && !teacherPersonalInfo.getMobileNumber().equalsIgnoreCase("")){
						label = new Label(++col, k, teacherPersonalInfo.getMobileNumber());
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, "N/A");
						excelSheet.addCell(label);
					}
					
					if(teacherDetail.getPhoneNumber()!=null && !teacherDetail.getPhoneNumber().equalsIgnoreCase("")){
						label = new Label(++col, k, teacherDetail.getPhoneNumber());
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, "N/A");
						excelSheet.addCell(label);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getAddressLine1()!=null && !teacherPersonalInfo.getAddressLine1().equalsIgnoreCase("")){
						label = new Label(++col, k, teacherPersonalInfo.getAddressLine1());
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, "N/A");
						excelSheet.addCell(label);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getCityId()!=null){
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getCityId().getCityName()!=null && !teacherPersonalInfo.getCityId().getCityName().equalsIgnoreCase("")){
						label = new Label(++col, k, teacherPersonalInfo.getCityId().getCityName());
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, "N/A");
						excelSheet.addCell(label);
					 }
					}else{
						label = new Label(++col, k, "N/A");
						excelSheet.addCell(label);
					 }
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getStateId()!=null){
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getStateId().getStateName()!=null && !teacherPersonalInfo.getStateId().getStateName().equalsIgnoreCase("")){
						label = new Label(++col, k, teacherPersonalInfo.getStateId().getStateName());
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, "N/A");
						excelSheet.addCell(label);
					 }
					}else{
						label = new Label(++col, k, "N/A");
						excelSheet.addCell(label);
					}
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getZipCode()!=null && !teacherPersonalInfo.getZipCode().equalsIgnoreCase("")){
						label = new Label(++col, k, teacherPersonalInfo.getZipCode());
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, "N/A");
						excelSheet.addCell(label);
					}
					
					k++;
					count++;
				}
		
		try {
			finalJobForTeachers=null;
			lstJobForTeachers=null;
			teacherPersonalInfoList=null;
			teacherIdlst=null;
			mapTInfo=null;
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
			}
		}else{
			
				excelSheet.mergeCells(0, 1, 9, 1);
				label = new Label(0, 1, "No Rcords Founds");
				excelSheet.addCell(label);
			
		}
			workbook.write();
			workbook.close();
			
	  
		}catch (Exception e) {
			e.printStackTrace();
		}
	  
	  return fileName;
  }	
  
  
  public String displaySmartFusionRecordsByEntityTypeCVS(Integer districtID,String noOfRow,String pageNo,String sortOrder,String sortOrderType){
	  
	  System.out.println("********displaySmartFusionRecordsByEntityTypeCVS*******");
	  
	  WebContext context;
	  context = WebContextFactory.get();
	  HttpServletRequest request = context.getHttpServletRequest();
	  HttpServletResponse response = context.getHttpServletResponse();	
	  HttpSession session = request.getSession(false);
	  ServletOutputStream  outputStream = null;
	 
	 
	  
	  DistrictMaster districtMaster = null;
      List<JobForTeacher>  lstJobForTeachers = null;
      List<JobForTeacher>  finalJobForTeachers = null;
      
		TeacherPersonalInfo teacherPersonalInfo = null;
		TeacherDetail teacherDetail = null;
		Integer teacherID = null;
		boolean teacherDetailFlag =false;
		String basePath = null;
		
		String fileName = null;
		
		try{
			
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start = ((pgNo-1)*noOfRowInPage);
		int end = ((pgNo-1)*noOfRowInPage)+noOfRowInPage;
		int totalRecord = 0;
		
		/** set default sorting fieldName **/
		String sortOrderFieldName = "firstName";
		
		/**Start set dynamic sorting fieldName **/
		 
		Order sortOrderStrVal = null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName = sortOrder;
			}
		}
		
		String sortOrderTypeVal = "0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal = Order.asc(sortOrderFieldName);
			}
			else{
				sortOrderTypeVal = "1";
				sortOrderStrVal = Order.desc(sortOrderFieldName);
			}
		}
		else{
			sortOrderTypeVal = "0";
			sortOrderStrVal = Order.asc(sortOrderFieldName);
		}
		/**end set dynamic sorting fieldName **/
		if(districtID!=null)
		{
			districtMaster=new DistrictMaster();
			districtMaster.setDistrictId(districtID);
		}
		
		if(sortOrderStrVal.toString().contains("firstName")||sortOrderStrVal.toString().contains("phoneNumber")){
			teacherDetailFlag = true;
		}
		
		lstJobForTeachers = jobForTeacherDAO.hiredCandidateListByDistrict(districtMaster,sortOrderStrVal,teacherDetailFlag);
		// System.out.println("lstJobForTeachers size : "+lstJobForTeachers);
		
		 
	   //Excel SmartFusion Exporting	 
		 String time = String.valueOf(System.currentTimeMillis()).substring(6);
		 basePath = request.getSession().getServletContext().getRealPath("/")+"smartFusionReport";
		 
		 fileName ="smartFusionReport"+time+".csv";
		
		 
		 File file = new File(basePath);
		 if(!file.exists()){
			 file.mkdir();
		 }
		 
		 Utility.deleteAllFileFromDir(basePath);
		 file = new File(basePath+"/"+fileName);
		 
		 
		 FileWriter writer = new FileWriter(file);
		 
	   //Delimiter used in CSV file
		final String COMMA_DELIMITER = ",";
		final String NEW_LINE_SEPARATOR = "\n";
		
		StringBuilder csvFileData = new StringBuilder();
		 
		csvFileData.append(lblApplicantNumber1);
		csvFileData.append(COMMA_DELIMITER);
		 
		csvFileData.append(lblEmailAddress);
		csvFileData.append(COMMA_DELIMITER);
		 
		csvFileData.append(lblFname);
		csvFileData.append(COMMA_DELIMITER);
		 
		csvFileData.append(lblMiddleName);
		csvFileData.append(COMMA_DELIMITER);
		 
		csvFileData.append(lblLastName);
		csvFileData.append(COMMA_DELIMITER);
		 
		csvFileData.append(lblPresentPhone1);
		csvFileData.append(COMMA_DELIMITER);
		 
		csvFileData.append(lblHomeCellPhone1);
		csvFileData.append(COMMA_DELIMITER);
		 
		csvFileData.append(lblPremanentPhone1);
		csvFileData.append(COMMA_DELIMITER);
		 
		csvFileData.append(lblPremanentAddress1);
		csvFileData.append(COMMA_DELIMITER);
		 
		csvFileData.append(lblPremanentCity1);
		csvFileData.append(COMMA_DELIMITER);
		 
		csvFileData.append(lblPremanentState1);
		csvFileData.append(COMMA_DELIMITER);
		 
		csvFileData.append(lblPremanentZip1);
	
		 	

			if(lstJobForTeachers!=null)
			{
			 if(lstJobForTeachers.size()>0){
			 totalRecord =  lstJobForTeachers.size();
			 }
			
			 System.out.println("totalRecord: "+totalRecord+" end: "+end+" start: "+start+" sortOrderFieldName: "+sortOrderFieldName);
			 if(totalRecord>end){
				 end =totalRecord;
				 finalJobForTeachers =lstJobForTeachers.subList(start, end);
			}else{
				 end =totalRecord;
				 finalJobForTeachers =lstJobForTeachers.subList(start, end);
			}
			
			
			if(finalJobForTeachers.size()==0){
				csvFileData.append(NEW_LINE_SEPARATOR);
				csvFileData.append(lblNoRecordsFound1);
			}
		
			
			 List<TeacherPersonalInfo> teacherPersonalInfoList  = new ArrayList<TeacherPersonalInfo>();
			 Map<Integer, TeacherPersonalInfo> mapTInfo=new TreeMap<Integer, TeacherPersonalInfo>();
			 ArrayList<Integer> teacherIdlst=new ArrayList<Integer>();
			 if(finalJobForTeachers!=null && finalJobForTeachers.size()>0)
			 {
				 for(JobForTeacher list:finalJobForTeachers)
					 teacherIdlst.add(list.getTeacherId().getTeacherId());
				 
				 if(teacherIdlst!=null && teacherIdlst.size()>0)
				 {
					 teacherPersonalInfoList=teacherPersonalInfoDAO.findByTeacherIds(teacherIdlst);
					 if(teacherPersonalInfoList!=null && teacherPersonalInfoList.size()>0)
						 for(TeacherPersonalInfo personalInfo:teacherPersonalInfoList)
							 mapTInfo.put(personalInfo.getTeacherId(), personalInfo);
				 }
			 }
			 
			 if(finalJobForTeachers!=null && finalJobForTeachers.size()>0)
			 {
				 
			 
		for(JobForTeacher list:finalJobForTeachers)
		{
			teacherID = list.getTeacherId().getTeacherId();
			teacherDetail =list.getTeacherId();
			if(mapTInfo!=null && mapTInfo.get(list.getTeacherId().getTeacherId())!=null)
				teacherPersonalInfo=mapTInfo.get(list.getTeacherId().getTeacherId());
			
					Integer count=1;
				
					csvFileData.append(NEW_LINE_SEPARATOR);
					csvFileData.append(count);
					csvFileData.append(COMMA_DELIMITER);
					
					
					if(teacherDetail.getEmailAddress()!=null && !teacherDetail.getEmailAddress().equalsIgnoreCase("")){
						csvFileData.append(teacherDetail.getEmailAddress());
						csvFileData.append(COMMA_DELIMITER);
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getFirstName()!=null && !teacherPersonalInfo.getFirstName().equalsIgnoreCase("")){
						csvFileData.append(teacherPersonalInfo.getFirstName());
						csvFileData.append(COMMA_DELIMITER);
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getMiddleName()!=null && !teacherPersonalInfo.getMiddleName().equalsIgnoreCase("")){
						csvFileData.append(teacherPersonalInfo.getMiddleName());
						csvFileData.append(COMMA_DELIMITER);
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getLastName()!=null && !teacherPersonalInfo.getLastName().equalsIgnoreCase("")){
						csvFileData.append(teacherPersonalInfo.getLastName());
						csvFileData.append(COMMA_DELIMITER);
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getPhoneNumber()!=null && !teacherPersonalInfo.getPhoneNumber().equalsIgnoreCase("")){
						csvFileData.append(teacherPersonalInfo.getPhoneNumber());
						csvFileData.append(COMMA_DELIMITER);
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getMobileNumber()!=null && !teacherPersonalInfo.getMobileNumber().equalsIgnoreCase("")){
						csvFileData.append(teacherPersonalInfo.getMobileNumber());
						csvFileData.append(COMMA_DELIMITER);
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					}
					
					if(teacherDetail.getPhoneNumber()!=null && !teacherDetail.getPhoneNumber().equalsIgnoreCase("")){
						csvFileData.append(teacherDetail.getPhoneNumber());
						csvFileData.append(COMMA_DELIMITER);
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getAddressLine1()!=null && !teacherPersonalInfo.getAddressLine1().equalsIgnoreCase("")){
						csvFileData.append(teacherPersonalInfo.getAddressLine1());
						csvFileData.append(COMMA_DELIMITER);
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					}
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getCityId()!=null){
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getCityId().getCityName()!=null && !teacherPersonalInfo.getCityId().getCityName().equalsIgnoreCase("")){
						csvFileData.append(teacherPersonalInfo.getCityId().getCityName());
						csvFileData.append(COMMA_DELIMITER);
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					 }
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getStateId()!=null){
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getStateId().getStateName()!=null && !teacherPersonalInfo.getStateId().getStateName().equalsIgnoreCase("")){
						csvFileData.append(teacherPersonalInfo.getStateId().getStateName());
						csvFileData.append(COMMA_DELIMITER);
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					 }
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getZipCode()!=null && !teacherPersonalInfo.getZipCode().equalsIgnoreCase("")){
						csvFileData.append(teacherPersonalInfo.getZipCode());
						
					}else{
						csvFileData.append(lblNA);
						csvFileData.append(COMMA_DELIMITER);
					}
					
					count++;
				}
		
		try {
			finalJobForTeachers=null;
			lstJobForTeachers=null;
			teacherPersonalInfoList=null;
			teacherIdlst=null;
			mapTInfo=null;
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
			}
		}else{
			
			csvFileData.append(NEW_LINE_SEPARATOR);
			csvFileData.append(lblNoRecordsFound1);
				
		}
			  writer.append(csvFileData);
			  writer.flush();
			  writer.close();
			  
		}catch (Exception e) {
			e.printStackTrace();
		}
	  return basePath+"/"+fileName;
  }	
  
  
  public String displaySmartFusionRecordsByEntityTypePDF(Integer districtID,String noOfRow,String pageNo,String sortOrder,String sortOrderType){
	  
	System.out.println("***********displaySmartFusionRecordsByEntityTypePDF****************"+districtID);
		
		StringBuffer sb = new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		
		String fileName ="";
		Integer userId=null;
		
		try{
			
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster = (UserMaster)session.getAttribute("userMaster");
			userId = userMaster.getUserId();
		}
		
		 
		// System.out.println("finalJobForTeachers: "+finalJobForTeachers.size());
	  
		    String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
		 
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			fileName =time+"SmartFusionReport.pdf";
			System.out.println("basePath : "+basePath);

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("Real Path : "+"user/"+userId+"/"+fileName);
		    
			generateSmartFusionPDFReport(districtID,noOfRow, pageNo, sortOrder, sortOrderType,basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
			return "user/"+userId+"/"+fileName;
		 
		}catch(Exception e){
			e.printStackTrace();
		}
	   return "DON";
  }


  	public boolean generateSmartFusionPDFReport(Integer districtID, String noOfRow,String pageNo,String sortOrder,String sortOrderType,String path,String realPath) {
  		
  		System.out.println("*******generateSmartFusionPDFReport******");
  		
  		WebContext context;
  		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster = (UserMaster)session.getAttribute("userMaster");
		}
  		
  		DistrictMaster districtMaster = null;
		List<JobForTeacher>  lstJobForTeachers = null;
        List<JobForTeacher>  finalJobForTeachers = null;
        
		TeacherPersonalInfo teacherPersonalInfo = null;
		TeacherDetail teacherDetail = null;
		Integer teacherID = null;
		boolean teacherDetailFlag =false;
  		
  		
  		Document document = null;
  		FileOutputStream fos = null;
  		PdfWriter writer = null;
  		
  		Font font8 = null;
		Font font8Green = null;
		Font font8bold = null;
		Font font9 = null;
		Font font9bold = null;
		Font font10 = null;
		Font font10_10 = null;
		Font font10bold = null;
		Font font11 = null;
		Font font11bold = null;
		Font font20bold = null;
		Font font11b   =null;
		Color bluecolor =null;
		Font font8bold_new = null;
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start = ((pgNo-1)*noOfRowInPage);
		int end = ((pgNo-1)*noOfRowInPage)+noOfRowInPage;
		int totalRecord = 0;
		
		/** set default sorting fieldName **/
		String sortOrderFieldName = "firstName";
		
		/**Start set dynamic sorting fieldName **/
		 
		Order sortOrderStrVal = null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName = sortOrder;
			}
		}
		
		String sortOrderTypeVal = "0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal = Order.asc(sortOrderFieldName);
			}
			else{
				sortOrderTypeVal = "1";
				sortOrderStrVal = Order.desc(sortOrderFieldName);
			}
		}
		else{
			sortOrderTypeVal = "0";
			sortOrderStrVal = Order.asc(sortOrderFieldName);
		}

		if(sortOrderStrVal.toString().contains("firstName")||sortOrderStrVal.toString().contains("phoneNumber")){
			teacherDetailFlag = true;
		}
		
		/**end set dynamic sorting fieldName **/

		if(districtID!=null)
		{
			districtMaster=new DistrictMaster();
			districtMaster.setDistrictId(districtID);
		}
		
		lstJobForTeachers = jobForTeacherDAO.hiredCandidateListByDistrict(districtMaster,sortOrderStrVal,teacherDetailFlag);
	
		// System.out.println("lstJobForTeachers size : "+lstJobForTeachers);
		 
		 
		 try{
			 
			 String fontPath = realPath;
			 try {
				 System.out.println("before :"+realPath);
					BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
					
					BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
					
					font8 = new Font(tahoma, 8);

					font8Green = new Font(tahoma, 8);
					font8Green.setColor(Color.BLUE);
					font8bold = new Font(tahoma, 8, Font.NORMAL);


					font9 = new Font(tahoma, 9);
					font9bold = new Font(tahoma, 9, Font.BOLD);
					font10 = new Font(tahoma, 10);
					
					font10_10 = new Font(tahoma, 10);
					font10_10.setColor(Color.white);
					font10bold = new Font(tahoma, 10, Font.BOLD);
					font11 = new Font(tahoma, 11);
					font11bold = new Font(tahoma, 11,Font.BOLD);
					bluecolor =  new Color(0,122,180); 
					
					//Color bluecolor =  new Color(0,122,180); 
					
					font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
					//font20bold.setColor(Color.BLUE);
					font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


				} 
				catch (DocumentException e1) 
				{
					e1.printStackTrace();
				} 
				catch (IOException e1) 
				{
					e1.printStackTrace();
				}
			 
			    document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
				document.addAuthor("TeacherMatch");
				document.addCreator("TeacherMatch Inc.");
				document.addSubject("Smart Fusion Report");
				document.addCreationDate();
				document.addTitle("Smart Fusion Report");
			 
				fos = new FileOutputStream(path);
				PdfWriter.getInstance(document, fos);
			
				document.open();
				
				PdfPTable mainTable = new PdfPTable(1);
				mainTable.setWidthPercentage(90);

				Paragraph [] para = null;
				PdfPCell [] cell = null;


				para = new Paragraph[3];
				cell = new PdfPCell[3];
				para[0] = new Paragraph(" ",font20bold);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell[0].setBorder(0);
				mainTable.addCell(cell[0]);
				
				//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
				Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
				logo.scalePercent(75);

				//para[1] = new Paragraph(" ",font20bold);
				cell[1]= new PdfPCell(logo);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				mainTable.addCell(cell[1]);

				document.add(new Phrase("\n"));
				
				
				para[2] = new Paragraph("",font20bold);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[2].setBorder(0);
				mainTable.addCell(cell[2]);

				document.add(mainTable);
				
				document.add(new Phrase("\n"));
				//document.add(new Phrase("\n"));
				

				float[] tblwidthz={.15f};
				
				mainTable = new PdfPTable(tblwidthz);
				mainTable.setWidthPercentage(100);
				para = new Paragraph[1];
				cell = new PdfPCell[1];

				
				para[0] = new Paragraph("Smart Fusion Report",font20bold);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[0].setBorder(0);
				mainTable.addCell(cell[0]);
				document.add(mainTable);

				document.add(new Phrase("\n"));
				//document.add(new Phrase("\n"));
				
				
		        float[] tblwidths={.15f,.20f};
				
				mainTable = new PdfPTable(tblwidths);
				mainTable.setWidthPercentage(100);
				para = new Paragraph[2];
				cell = new PdfPCell[2];

				String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
				
				para[0] = new Paragraph("Created By: "+name1,font10);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].setBorder(0);
				mainTable.addCell(cell[0]);
				
				para[1] = new Paragraph(""+"Created on: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell[1].setBorder(0);
				mainTable.addCell(cell[1]);
				
				document.add(mainTable);
				
				document.add(new Phrase("\n"));
         	
         	document.add(new Phrase("\n"));
         	
         	float[] tblwidth = {.12f,.20f,.12f,.12f,.12f,.12f,.12f,.12f,.20f,.12f,.12f,.12f};
         	
         	mainTable = new PdfPTable(tblwidth);
         	mainTable.setWidthPercentage(100);
         	
         	para = new Paragraph[12];
			cell = new PdfPCell[12];
			
			// writing Header......
			
			para[0] = new Paragraph("Applicant Number",font10_10);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setBackgroundColor(bluecolor);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[0]);
			
			para[1] = new Paragraph("Email Address",font10_10);
			cell[1] = new PdfPCell(para[1]);
			cell[1].setBackgroundColor(bluecolor);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[1]);
			
			para[2] = new Paragraph("First Name",font10_10);
			cell[2] = new PdfPCell(para[2]);
			cell[2].setBackgroundColor(bluecolor);
			cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[2]);
			
			para[3] = new Paragraph("Middle Name",font10_10);
			cell[3] = new PdfPCell(para[3]);
			cell[3].setBackgroundColor(bluecolor);
			cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[3]);
			
			para[4] = new Paragraph("Last Name",font10_10);
			cell[4] = new PdfPCell(para[4]);
			cell[4].setBackgroundColor(bluecolor);
			cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[4]);
			
			para[5] = new Paragraph("Present Phone",font10_10);
			cell[5] = new PdfPCell(para[5]);
			cell[5].setBackgroundColor(bluecolor);
			cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[5]);
			
			para[6] = new Paragraph("Home/CellPhone",font10_10);
			cell[6] = new PdfPCell(para[6]);
			cell[6].setBackgroundColor(bluecolor);
			cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[6]);
			
			para[7] = new Paragraph("Permanent Phone",font10_10);
			cell[7] = new PdfPCell(para[7]);
			cell[7].setBackgroundColor(bluecolor);
			cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[7]);
			
			para[8] = new Paragraph("Permanent Address",font10_10);
			cell[8] = new PdfPCell(para[8]);
			cell[8].setBackgroundColor(bluecolor);
			cell[8].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[8]);
			
			para[9] = new Paragraph("Permanent City",font10_10);
			cell[9] = new PdfPCell(para[9]);
			cell[9].setBackgroundColor(bluecolor);
			cell[9].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[9]);
			
			para[10] = new Paragraph("Permanent State",font10_10);
			cell[10] = new PdfPCell(para[10]);
			cell[10].setBackgroundColor(bluecolor);
			cell[10].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[10]);
			
			para[11] = new Paragraph("Permanent Zip",font10_10);
			cell[11] = new PdfPCell(para[11]);
			cell[11].setBackgroundColor(bluecolor);
			cell[11].setHorizontalAlignment(Element.ALIGN_LEFT);
			mainTable.addCell(cell[11]);
         	
         	document.add(mainTable);
         	
         	
       if(lstJobForTeachers!=null){
	   		 if(lstJobForTeachers.size()>0){
	   		 totalRecord =  lstJobForTeachers.size();
	   		 }
	   		
	   		 System.out.println("sortOrderType: "+sortOrderType+"sortOrderStrVal: "+sortOrderStrVal+"totalRecord: "+totalRecord+" end: "+end+" start: "+start+" sortOrderFieldName: "+sortOrderFieldName);
	   		 
	   		 if(totalRecord>end){
	   			 end =totalRecord;
	   			 finalJobForTeachers =lstJobForTeachers.subList(start, end);
	   		}else{
	   			 end =totalRecord;
	   			 finalJobForTeachers =lstJobForTeachers.subList(start, end);
	   		}
       }	
       if(lstJobForTeachers!=null){
         	if(finalJobForTeachers.size()==0){
         		float[] tablwidth={.10f};
         		mainTable = new PdfPTable(tablwidth);
                mainTable.setWidthPercentage(100);
                para = new Paragraph[1];
                cell = new PdfPCell[1];
                
                para[0] = new Paragraph("No Record Found.",font8bold);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[0]);
			
			    document.add(mainTable);
           }
       }
			 Integer count=1;
			 List<TeacherPersonalInfo> teacherPersonalInfoList  = new ArrayList<TeacherPersonalInfo>();
			 Map<Integer, TeacherPersonalInfo> mapTInfo=new TreeMap<Integer, TeacherPersonalInfo>();
			 ArrayList<Integer> teacherIdlst=new ArrayList<Integer>();
			 if(finalJobForTeachers!=null && finalJobForTeachers.size()>0)
			 {
				 for(JobForTeacher list:finalJobForTeachers)
					 teacherIdlst.add(list.getTeacherId().getTeacherId());
				 
				 if(teacherIdlst!=null && teacherIdlst.size()>0)
				 {
					 teacherPersonalInfoList=teacherPersonalInfoDAO.findByTeacherIds(teacherIdlst);
					 if(teacherPersonalInfoList!=null && teacherPersonalInfoList.size()>0)
						 for(TeacherPersonalInfo personalInfo:teacherPersonalInfoList)
							 mapTInfo.put(personalInfo.getTeacherId(), personalInfo);
				 }
			 }
			 
		
			 if(finalJobForTeachers!=null && finalJobForTeachers.size()>0)
			 {
				 
			 
		for(JobForTeacher list:finalJobForTeachers)
		{
			teacherID = list.getTeacherId().getTeacherId();
			teacherDetail =list.getTeacherId();
			if(mapTInfo!=null && mapTInfo.get(list.getTeacherId().getTeacherId())!=null)
				teacherPersonalInfo=mapTInfo.get(list.getTeacherId().getTeacherId());
			
         			
         			Integer index=0;
         			float[] tblwidth1={.12f,.20f,.12f,.12f,.12f,.12f,.12f,.12f,.20f,.12f,.12f,.12f};
         			
         			mainTable = new PdfPTable(tblwidth1);
         			mainTable.setWidthPercentage(100);
         			para = new Paragraph[12];
         			cell = new PdfPCell[12];
         			
         			para[index] = new Paragraph(""+count,font8bold);
         			cell[index] = new PdfPCell(para[index]);
         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
         			mainTable.addCell(cell[index]);
         			index++;
         			
         			
         			if(teacherDetail.getEmailAddress()!=null && !teacherDetail.getEmailAddress().equalsIgnoreCase("")){
         				para[index] = new Paragraph(""+teacherDetail.getEmailAddress(),font8bold);
             			cell[index] = new PdfPCell(para[index]);
             			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
             			mainTable.addCell(cell[index]);
             			index++;
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getFirstName()!=null && !teacherPersonalInfo.getFirstName().equalsIgnoreCase("")){
						para[index] = new Paragraph(""+teacherPersonalInfo.getFirstName(),font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getMiddleName()!=null && !teacherPersonalInfo.getMiddleName().equalsIgnoreCase("")){
						para[index] = new Paragraph(""+teacherPersonalInfo.getMiddleName(),font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getLastName()!=null && !teacherPersonalInfo.getLastName().equalsIgnoreCase("")){
						para[index] = new Paragraph(""+teacherPersonalInfo.getLastName(),font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getPhoneNumber()!=null && !teacherPersonalInfo.getPhoneNumber().equalsIgnoreCase("")){
						para[index] = new Paragraph(""+teacherPersonalInfo.getPhoneNumber(),font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getMobileNumber()!=null && !teacherPersonalInfo.getMobileNumber().equalsIgnoreCase("")){
						para[index] = new Paragraph(""+teacherPersonalInfo.getMobileNumber(),font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}
					
					if(teacherDetail.getPhoneNumber()!=null && !teacherDetail.getPhoneNumber().equalsIgnoreCase("")){
						para[index] = new Paragraph(""+teacherDetail.getPhoneNumber(),font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getAddressLine1()!=null && !teacherPersonalInfo.getAddressLine1().equalsIgnoreCase("")){
						para[index] = new Paragraph(""+teacherPersonalInfo.getAddressLine1(),font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getCityId()!=null){
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getCityId().getCityName()!=null && !teacherPersonalInfo.getCityId().getCityName().equalsIgnoreCase("")){
						para[index] = new Paragraph(""+teacherPersonalInfo.getCityId().getCityName(),font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;	
					 }
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;	
					 }
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getStateId()!=null){
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getStateId().getStateName()!=null && !teacherPersonalInfo.getStateId().getStateName().equalsIgnoreCase("")){
						para[index] = new Paragraph(""+teacherPersonalInfo.getStateId().getStateName(),font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					 }
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					 }
					
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getZipCode()!=null && !teacherPersonalInfo.getZipCode().equalsIgnoreCase("")){
						para[index] = new Paragraph(""+teacherPersonalInfo.getZipCode(),font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}else{
						para[index] = new Paragraph(""+"N/A",font8bold);
	         			cell[index] = new PdfPCell(para[index]);
	         			cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
	         			mainTable.addCell(cell[index]);
	         			index++;
					}
         			count++;
         			document.add(mainTable);
         		}
		
		try {
			finalJobForTeachers=null;
			lstJobForTeachers=null;
			teacherPersonalInfoList=null;
			teacherIdlst=null;
			mapTInfo=null;
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
         	
		 }else{
			 float[] tablwidth={.10f};
      		 mainTable = new PdfPTable(tablwidth);
             mainTable.setWidthPercentage(100);
             para = new Paragraph[1];
             cell = new PdfPCell[1];
             
                para[0] = new Paragraph("No Records Found.",font8bold);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[0]);
			
			    document.add(mainTable);
		 }
			 
		 }catch(Exception e){
			 e.printStackTrace();
		 }
  		finally{
  			if(document !=null && document.isOpen()){
  				document.close();
  			}
  			if(writer!=null){
  				writer.flush();
  				writer.close();
  			}
  		}
			
	return true;
	
   }
	
}
