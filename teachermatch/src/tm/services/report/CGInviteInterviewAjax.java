package tm.services.report;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.jclouds.ContextBuilder;
import org.jclouds.openstack.swift.v1.domain.SwiftObject;
import org.jclouds.openstack.swift.v1.features.ObjectApi;
import org.jclouds.rackspace.cloudfiles.v1.CloudFilesApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.common.io.ByteStreams;
import com.sun.star.io.IOException;

import tm.bean.EventParticipantsList;
import tm.bean.EventSchedule;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4Candidate;
import tm.bean.i4.I4DistrictAccount;
import tm.bean.i4.I4InterviewInvites;
import tm.bean.i4.I4QuestionPool;
import tm.bean.i4.I4QuestionSetQuestions;
import tm.bean.i4.I4QuestionSets;
import tm.bean.i4.VVIResponse;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.EventParticipantsListDAO;
import tm.dao.EventScheduleDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.i4.I4CandidateDAO;
import tm.dao.i4.I4DistrictAccountDAO;
import tm.dao.i4.I4InterviewInvitesDAO;
import tm.dao.i4.I4QuestionPoolDAO;
import tm.dao.i4.I4QuestionSetQuestionsDAO;
import tm.dao.i4.I4QuestionSetsDAO;
import tm.dao.i4.VVIResponseDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.services.district.GlobalServices;
import tm.services.i4.JDigestI4API;
import tm.services.rc.cloudfiles.RCCloudFileServices;
import tm.services.rc.cloudfiles.RCConfig;
import tm.utility.Utility;

public class CGInviteInterviewAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	
	String toolClickToInvite1=Utility.getLocaleValuePropByKey("toolClickToInvite1", locale);
	String toolVirtualVideoInte1=Utility.getLocaleValuePropByKey("toolVirtualVideoInte1", locale);
	String toolVirtualVideoInte2=Utility.getLocaleValuePropByKey("toolVirtualVideoInte2", locale);
	String toolVirtualVideoInterviewCompleted1=Utility.getLocaleValuePropByKey("toolVirtualVideoInterviewCompleted1", locale);
	String msgClickOKButtonToSendInvite1=Utility.getLocaleValuePropByKey("msgClickOKButtonToSendInvite1", locale);
	String msgClickOKButtonToSendInvite2=Utility.getLocaleValuePropByKey("msgClickOKButtonToSendInvite2", locale);
	String msgClickOKButtonToSendInvite3=Utility.getLocaleValuePropByKey("msgClickOKButtonToSendInvite3", locale);
	
	@Autowired
	private I4InterviewInvitesDAO i4InterviewInvitesDAO;
	
	@Autowired
	private I4QuestionSetQuestionsDAO i4QuestionSetQuestionsDAO;
	
	@Autowired
	private I4QuestionSetsDAO i4QuestionSetsDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private I4DistrictAccountDAO i4DistrictAccountDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private I4CandidateDAO i4CandidateDAO;
	
	@Autowired
	private EventParticipantsListDAO eventParticipantsListDAO;
	
	@Autowired
	private EventScheduleDAO eventScheduleDAO;
	
	@Autowired
	private I4QuestionPoolDAO i4QuestionPoolDAO;
	
	@Autowired
	private VVIResponseDAO vviResponseDAO;
	
	
	//I4 Interview Icon according condition
	public String getInterview(Map<Integer,I4InterviewInvites> mapInviteInterview,JobForTeacher jft,Integer noOfRecordCheck,String windowFunc)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		//System.out.println("Calling getInterview");
		
		StringBuffer sb=new StringBuffer();
		
		int chk=0;
		
		try
		{
			if(mapInviteInterview.get(jft.getTeacherId().getTeacherId())!=null)
				chk = mapInviteInterview.get(jft.getTeacherId().getTeacherId()).getStatus();
			
			if(chk==0)
				sb.append("&nbsp;<a data-original-title='"+toolClickToInvite1+"' rel='tooltip' id='i4Ques"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"displayIIQuesForRed('"+jft.getJobId().getJobId()+"','"+jft.getTeacherId().getTeacherId()+"');"+windowFunc+"\" >&nbsp;<img height='20px' width='20' src=\"images/RedVideo.png\"></a>");	
			else if(chk==1)
				sb.append("&nbsp;<a data-original-title='"+toolVirtualVideoInte1+"' rel='tooltip' id='i4Ques"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"displayIIQuesForGray('"+jft.getJobId().getJobId()+"','"+jft.getTeacherId().getTeacherId()+"');"+windowFunc+"\" >&nbsp;<img height='20px' width='20' src=\"images/GreyVideo.png\"></a>");
			else if(chk==2)
				sb.append("&nbsp;<a data-original-title='"+toolVirtualVideoInte2+"' rel='tooltip' id='i4Ques"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"displayIIQuesForQrange('"+jft.getJobId().getJobId()+"','"+jft.getTeacherId().getTeacherId()+"');"+windowFunc+"\" >&nbsp;<img height='20px' width='20' src=\"images/OrangeVideo.png\"></a>");
			else if(chk==3)
			{
				//sb.append("&nbsp;<a data-original-title='Virtual Video Interview completed. Click to view the interview.' rel='tooltip' id='i4Ques"+noOfRecordCheck+"' href='https://google.com' onclick='' target='_blank'>&nbsp;<img height='20px' width='20' src=\"images/BlueVideo.png\"></a>");
				sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+toolVirtualVideoInterviewCompleted1+"'  id='i4Ques"+noOfRecordCheck+"' onclick=\"openInterViewLink('"+jft.getJobId().getJobId()+"','"+jft.getTeacherId().getTeacherId()+"','trans"+jft.getJobForTeacherId()+"');\">&nbsp;<img height='20px' width='20' src=\"images/BlueVideo.png\"></a>");
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	
	
	public String displayInviteInterviewQues(String jobId,String teacherId,String iiFlag)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		StringBuffer sb=new StringBuffer();
		List<I4QuestionSetQuestions> lstQuesBySet = new ArrayList<I4QuestionSetQuestions>();
		I4QuestionSets i4QuestionSets = new I4QuestionSets();
		
		try
		{
			JobOrder joborder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
		
			String quesSetName = "";
			String quesSetId ="";
			
			if(joborder.getOfferVirtualVideoInterview())
			{
				//For QuesSetName
				if(joborder.getI4QuestionSets()!=null && !joborder.getI4QuestionSets().getQuestionSetText().equals(""))
					quesSetName = joborder.getI4QuestionSets().getQuestionSetText();
					
				if(joborder.getI4QuestionSets()!=null)
					quesSetId = joborder.getI4QuestionSets().getID().toString();
				
			}
			else {
				// no VVI is setup at any level
				System.out.println("  No question Set attached.");
			}
			
			if(quesSetId!=null && !quesSetId.equals(""))
			{
				i4QuestionSets = i4QuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
				lstQuesBySet = i4QuestionSetQuestionsDAO.findByQuesSetId(i4QuestionSets,joborder.getDistrictMaster() , joborder.getHeadQuarterMaster());  // @ Anurag add HQ in param
			}
			
			int qno=1;
			
			sb.append("<table>");
			if(lstQuesBySet.size()>0)
			{
				sb.append("<tr>");
				
				if(iiFlag.equalsIgnoreCase("red"))
				{
					sb.append("<td style='font-size: 15px;font-weight: normal; line-height: 16px;' ><div class='mt10'>"+msgClickOKButtonToSendInvite1+"</div></td>");
				}else if(iiFlag.equalsIgnoreCase("grey"))
				{
					sb.append("<td style='font-size: 15px;font-weight: normal; line-height: 16px;' ><div class='mt10'>"+msgClickOKButtonToSendInvite2+"</div></td>");
				}else if(iiFlag.equalsIgnoreCase("orange"))
				{
					sb.append("<td style='font-size: 15px;font-weight: normal; line-height: 16px;' ><div class='mt10'>"+msgClickOKButtonToSendInvite3+"</div></td>");
				}
				sb.append("</tr>");
			}
			
			sb.append("<tr>");
			sb.append("<td style='font-size: 15px;font-weight: normal; line-height: 16px;'><div class='mt10'>" +
					"<input type='hidden' id='quesSetId' name='quesSetId' value='"+quesSetId+"'/>" +
							"<input type='hidden' id='teacherIdForII' name='teacherIdForII' value='"+teacherId+"'/>" +
									"<input type='hidden' id='jobIdForII' name='jobIdForII' value='"+jobId+"'/>" +
									"<input type='hidden' id='quesSize' name='quesSize' value='"+lstQuesBySet.size()+"'/>" +
							"<b>"+quesSetName+"</b></div></td>");
			sb.append("</td>");
			sb.append("</tr>");
			
			System.out.println(" <<<<<<<< lstQuesBySet >>>>>> "+lstQuesBySet.size());
			if(lstQuesBySet.size()>0)
			{
				for(I4QuestionSetQuestions i4qsq:lstQuesBySet)
				{
					sb.append("<tr>");
					sb.append("<td style='font-size: 15px;font-weight: normal; line-height: 16px;'><div class='mt10'><b>Q"+qno+".&nbsp;</b>"+i4qsq.getQuestionPool().getQuestionText()+"</div></td>");
					sb.append("</tr>");
					qno++;
				}
			}
			
			if(lstQuesBySet.size()==0)
				sb.append("<td style='font-size: 15px;font-weight: normal; line-height: 16px;'><div class='mt10'>No question set attached to this job order.</div></td>");
			
			sb.append("</table>");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	
	public int saveIIStatus(String jobId, String teacherId, String  quesSetId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request;
		HttpSession session = null;
		if(context!=null){
			request = context.getHttpServletRequest();
			session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(msgYrSesstionExp);
		    }
		}
		
		try
		{
			String userEmail = "";
			UserMaster userMaster = new UserMaster();
			if(session!=null && session.getAttribute("userMaster")!=null){
				userMaster=(UserMaster)session.getAttribute("userMaster");
				userEmail = userMaster.getEmailAddress();
			}
			
			List<I4InterviewInvites> existI4InterviewInvites = new ArrayList<I4InterviewInvites>();
			I4InterviewInvites objInviteInterview = null;
			
			JobOrder joborder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
			TeacherDetail teacherDetail = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
			
			I4QuestionSets i4QuestionSets = new I4QuestionSets();
			
			if(quesSetId!=null && !quesSetId.equals(""))
				i4QuestionSets = i4QuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false); 
			
			
			boolean sendInviteOrNot = inviteAlreadySentInSameQuestionSet(i4QuestionSets.getID(),teacherDetail.getTeacherId(),joborder.getDistrictMaster().getDistrictId());			
			if(sendInviteOrNot){
				return 0;
			}
			/////////////////////////////////////////////////////////
			List<I4DistrictAccount> i4DistrictAccountlst = i4DistrictAccountDAO.findByDistrict(joborder.getDistrictMaster());
			String userName = "";
			String password = "";
			
			if(i4DistrictAccountlst!=null && i4DistrictAccountlst.size()>0)
			{
				userName	=	i4DistrictAccountlst.get(0).getI4username();
				password    = 	i4DistrictAccountlst.get(0).getI4password();
			}
			System.out.println(" username :: "+userName+" pass :: "+password);
			////////////////////// Add candidate in i4 candidate table /////////////////////
			String i4CandidateId = null; // null for create candidate
			String apiI4CandidateId = null;
			String[] apiCandidateStr	=	new String[3];
			I4Candidate i4Candidate = null;
			
			String i4Inviteinterview = "new"; // null for create are candidate
			String apiIIID= null;
			String apiI4InviteinterviewURL= null;
			boolean i4CandiateFlag = false;
			boolean i4IFlag = false;
			
			try
			{
				List<I4Candidate> lstI4CandidateExist = new ArrayList<I4Candidate>();
				lstI4CandidateExist = i4CandidateDAO.findByteacherDetail(teacherDetail,joborder.getDistrictMaster());
				
				if(lstI4CandidateExist!=null && lstI4CandidateExist.size()>0)
				{
					i4CandidateId = lstI4CandidateExist.get(0).getI4CandidateID();
					i4Candidate = lstI4CandidateExist.get(0);
					
					System.out.println("  candidate already exist= "+i4Candidate+" lstI4CandidateExist.get(0) "+lstI4CandidateExist.get(0).getI4CandidateID());
					apiI4CandidateId = lstI4CandidateExist.get(0).getI4CandidateID(); 
					
				}
				else
				{
					i4Candidate = new I4Candidate();
					apiCandidateStr = JDigestI4API.I4_AddOrUpdateCandidate(userName, password,teacherDetail.getFirstName(), teacherDetail.getLastName(), teacherDetail.getEmailAddress(), i4CandidateId);
					apiI4CandidateId = apiCandidateStr[0];
					
					System.out.println("username:"+apiCandidateStr[1]+"password:"+apiCandidateStr[2]);
					
					// Save Data in I4Candidate Table
					if(apiCandidateStr[0]!=null)
					{	
						i4Candidate.setDistrictMaster(joborder.getDistrictMaster());
						i4Candidate.setTeacherDetail(teacherDetail);
						i4Candidate.setDateCreated(new Date());
						i4Candidate.setI4CandidateID(apiI4CandidateId);
						i4Candidate.setUserName(apiCandidateStr[1]);
						i4Candidate.setPassword(apiCandidateStr[2]);
						
						i4CandidateDAO.makePersistent(i4Candidate);
					}else
					{
						System.out.println("Candidate I4 ID is NULL");
					}
					
				//	i4CandiateFlag=true;
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			//////////////////////////////////////////////////////////////////////////
						
			//////////////////////////// Invite candidate for interview //////////////////////
			try
				{
					existI4InterviewInvites = i4InterviewInvitesDAO.getListbyTeacherAndjobId(joborder, teacherDetail);
					
					if(existI4InterviewInvites!=null && existI4InterviewInvites.size()>0)
					{
						i4Inviteinterview = existI4InterviewInvites.get(0).getI4inviteid().toString();
						objInviteInterview = existI4InterviewInvites.get(0);
					}
				//else
				//	
					//i4IIForApi = new I4InterviewInvites();
						
				
						String expiryDate = "";
						String timeAllowedPerQuestion = "";
						String MaxScore = "";
						System.out.println("i4Inviteinterview :: "+i4Inviteinterview+" apiI4CandidateId :: "+apiI4CandidateId+" i4QuestionSets.getI4QuestionSetID() :: "+i4QuestionSets.getI4QuestionSetID());
						try
						{
							
							if(joborder.getOfferVirtualVideoInterview())
							{
								if(joborder.getVVIExpiresInDays()!=null)
									expiryDate = joborder.getVVIExpiresInDays().toString();
								
								if(joborder.getTimeAllowedPerQuestion()!=null)
									timeAllowedPerQuestion = joborder.getTimeAllowedPerQuestion().toString();
								
								if(joborder.getMaxScoreForVVI()!=null)
									MaxScore = joborder.getMaxScoreForVVI().toString();;
							}
								
						}catch(Exception e)
						{
							e.printStackTrace();
						}
						System.out.println("userName: "+userName+" password: "+password+" i4Inviteinterview: "+i4Inviteinterview+" apiI4CandidateId "+apiI4CandidateId+" i4QuestionSets.getI4QuestionSetID():"+i4QuestionSets.getI4QuestionSetID()+"expiryDate:"+expiryDate+"timeAllowedPerQuestion:"+timeAllowedPerQuestion+"....");
						
						if(objInviteInterview!=null && objInviteInterview.getStatus()<3){
							apiI4InviteinterviewURL = JDigestI4API.inviteCandidateForInterviewResend(userName, password, i4Inviteinterview, apiI4CandidateId);
						
							//Email For Resend Invite
							createAndSendVVIMail(joborder, teacherDetail, i4Candidate, i4QuestionSets,userEmail,objInviteInterview.getDatetimeset());
							
							System.out.println(" resend message :: "+apiI4InviteinterviewURL);
						}
						
						if(objInviteInterview==null)
						{
							objInviteInterview = new I4InterviewInvites();
							
							apiI4InviteinterviewURL = JDigestI4API.inviteCandidateForInterview(userName,password,i4Inviteinterview, apiI4CandidateId,i4QuestionSets.getI4QuestionSetID(),expiryDate,timeAllowedPerQuestion);
						
							if(apiI4InviteinterviewURL!=null)
							{
								String apiInterViewUrl[] = apiI4InviteinterviewURL.split("/");
								if(apiInterViewUrl.length==5)
									apiIIID = apiInterViewUrl[4];
								objInviteInterview.setDatetimeset(new Date());
								objInviteInterview.setJobOrder(joborder);
								objInviteInterview.setTeacherDetail(teacherDetail);
								objInviteInterview.setDistrictId(joborder.getDistrictMaster().getDistrictId());
								objInviteInterview.setI4QuestionSets(i4QuestionSets);
								objInviteInterview.setI4inviteid(Integer.parseInt(apiIIID));
								if(apiI4InviteinterviewURL!=null)
									objInviteInterview.setInterviewurl(apiI4InviteinterviewURL);
								
								objInviteInterview.setStatus(1);
								if(MaxScore!=null && !MaxScore.equals(""))
									objInviteInterview.setI4VideoInterviewMaxScore(Integer.parseInt(MaxScore));
								
								i4InterviewInvitesDAO.makePersistent(objInviteInterview);
								
								//Send mail
								createAndSendVVIMail(joborder, teacherDetail, i4Candidate, i4QuestionSets,userEmail,null);
								
							}
							else
								System.out.println(" Interview URl is NULL ");	
						
						}
						//	i4IFlag=true;			
				}catch(Exception e)
				{
					e.printStackTrace();
				}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public static String getTeacherName(TeacherDetail teacherDetail){
		String teacherName="";
		try{
			if(teacherDetail.getFirstName()!=null){
				teacherName=teacherDetail.getFirstName();
			}
			if(teacherDetail.getFirstName()!=null && teacherDetail.getLastName()!=null){
				teacherName=teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
			}
		}catch(Exception e){	e.printStackTrace(); }
		return teacherName;
	}
	
	/*public String[] openInterviewURL(String jobId, String teacherId)
	{
		
		System.out.println("jobId : "+jobId+"teacherId : "+teacherId);
		StringBuffer sb = new StringBuffer();
		String[] ReturnArray = new String[2];
		
		try
		{
			JobOrder jobOrder = null;
			TeacherDetail teacherDetail = null;
			List<I4InterviewInvites> i4InterviewInvites = new ArrayList<I4InterviewInvites>(); 
			int  maxScore = 0;
			int SliderScore = 0;
			boolean scoreflag = false;
			
			
			
			if(jobId!=null && !jobId.equals("") && teacherId!=null && !teacherId.equals(""))
			{
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
				teacherDetail = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
				
				System.out.println(" jobOrder "+jobOrder+" teacherDetail "+teacherDetail);
				
				if(jobOrder!=null && teacherDetail!=null)
				{
					i4InterviewInvites = i4InterviewInvitesDAO.getListbyTeacherAndjobId(jobOrder, teacherDetail);
				}
				
				System.out.println("i4InterviewInvites.get(0).getVideoUrl()+ "+i4InterviewInvites.get(0).getVideoUrl());
				
				sb.append("<div class=row'>");
					sb.append("<div class='col-sm-12 col-md-12  top10 mt15'>");
						sb.append("<iframe src='"+i4InterviewInvites.get(0).getVideoUrl()+"' id='ifrmTrans' width='100%' height='300px'></iframe>");
					sb.append("</div>");
				sb.append("</div>");
						
		
				if(i4InterviewInvites!=null && i4InterviewInvites.size()>0)
				{
					if(i4InterviewInvites.get(0).getI4VideoInterviewMaxScore()!=null)
					{
						maxScore = i4InterviewInvites.get(0).getI4VideoInterviewMaxScore();
					
						if( maxScore>0)
						{
							scoreflag=true;
							int iTickInterval=maxScore/10;
							
							if(i4InterviewInvites.get(0).getI4VideoInterviewScore()!=null)
								SliderScore = i4InterviewInvites.get(0).getI4VideoInterviewScore();
						
							sb.append("<div class='row'>");
							sb.append("<div class='col-sm-9 col-md-9  top20'>");
							sb.append("<input type='hidden' id='inviteInterviewID' value='"+i4InterviewInvites.get(0).getId()+"'/>");
							sb.append("<iframe id='ifrmForVideoII'  src='slideract.do?name=normScoreFrm&tickInterval="+iTickInterval+"&max="+maxScore+"&swidth=650&svalue="+SliderScore+"' scrolling='no' frameBorder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:670px;'></iframe>");
							sb.append("</div>");
							sb.append("</div>");
						}
					}
				}
			}
			
			ReturnArray[0]=sb.toString();
			ReturnArray[1]=""+scoreflag;
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return ReturnArray;
	}*/
	
	public String[] openInterviewURL(String jobId, String teacherId)
	{
		System.out.println("jobId : "+jobId+"teacherId : "+teacherId);
		StringBuffer sb = new StringBuffer();
		String[] ReturnArray = new String[2];
		
		try
		{
			JobOrder jobOrder = null;
			TeacherDetail teacherDetail = null;
			List<I4InterviewInvites> i4InterviewInvites = new ArrayList<I4InterviewInvites>(); 
			int  maxScore = 0;
			int SliderScore = 0;
			boolean scoreflag = false;
			
			//getPathOfVideoUrl("Mateo.Curtis.41548.testing.interview_aphone.webm");
			
			if(jobId!=null && !jobId.equals("") && teacherId!=null && !teacherId.equals(""))
			{
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
				teacherDetail = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
				
				System.out.println(" :: jobOrder "+jobOrder+" teacherDetail "+teacherDetail);
				
				if(jobOrder!=null && teacherDetail!=null)
				{
					i4InterviewInvites = i4InterviewInvitesDAO.getListbyTeacherAndjobId(jobOrder, teacherDetail);
				}
				
				System.out.println(" >>>>  i4InterviewInvites >>>>>> list >>>> "+i4InterviewInvites.size());
				
				for(I4InterviewInvites i4vvi:i4InterviewInvites){
					System.out.println(" ////////////// "+i4vvi.getInterviewurl());	
				}
				
				
			//	System.out.println("i4InterviewInvites.get(0).getVideoUrl()+ "+i4InterviewInvites.get(0).getVideoUrl());
				
				///////////////////////////////////////////////////////// UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
				String videoFIleName = "";
				String playerUrl = "";
				
			//	if(i4InterviewInvites.get(0).getVideoUrl()!=null)
			//		videoFIleName = i4InterviewInvites.get(0).getVideoUrl();
				
				/////////////////////////////////////////////////// for getting questions and video urls ////////////////////////////
				
				Map<String,String> vviResponseMap = new HashMap<String,String>();
				List<VVIResponse> vviQuestionLst = new ArrayList<VVIResponse>();
				//List<VVIResponse> vviQuestionLstForQuestion = new ArrayList<VVIResponse>();
				
				List<String> i4QuesIdList = new ArrayList<String>();
				List<I4QuestionPool> questionList = new ArrayList<I4QuestionPool>();
				if(i4InterviewInvites!=null && i4InterviewInvites.get(0).getI4inviteid()!=null)
				{
						System.out.println(" i4InterviewInvites >>>>>>>>>>>>>>>>>>>> "+i4InterviewInvites.get(0).getI4inviteid());
						vviQuestionLst = vviResponseDAO.findByI4InviteId(i4InterviewInvites.get(0).getI4inviteid());
				}
				
				System.out.println(" vviQuestionLst "+vviQuestionLst);
				
				
				if(vviQuestionLst!=null && vviQuestionLst.size()>0)
				{
					for(VVIResponse vviResList:vviQuestionLst)
					{
						i4QuesIdList.add(vviResList.getInterviewQuestionId());
						vviResponseMap.put(vviResList.getInterviewQuestionId(), vviResList.getWebmLink());
					}
					
					questionList = i4QuestionPoolDAO.findByI4QuestionIds(i4QuesIdList);
				}
				
				
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				
				
				
				if(videoFIleName!=null && !videoFIleName.equals(""))
					//playerUrl = getPathOfVideoUrl(videoFIleName);
				
				System.out.println(" Final player url :::::::::::: "+playerUrl+" vviQuestionLst "+vviQuestionLst.size());
				
				
				
				
				
				sb.append("<div class=row'>");
					sb.append("<div class='col-sm-6 col-md-6  top10 mt15'>");
						if(questionList!=null && questionList.size()>0)
						{
							if(vviResponseMap!=null && vviResponseMap.size()>0){
								for(I4QuestionPool vvir:questionList){
									if(vviResponseMap.get(vvir.getI4QuestionID()) != null){
										
										String str = vviResponseMap.get(vvir.getI4QuestionID());
										
										String[] str1 = str.split("\\?");
										String str2 = str1[0].replace("http://i4videos.s3.amazonaws.com/", "");
										
										System.out.println("  str2 >>>>>>>>>>>>>>>>>>>>>>>> "+str2);
										
										
										sb.append("<div id='vviUrlId' class='col-sm-12 col-md-12' style='background-color: #0078b4; color:#ffffff; border:2px; border-color:#ffffff; padding:5px; cursor:pointer' onclick='getVideoURL(\""+str2.trim()+"\")'>");
										//	sb.append("<button style='background-color: #0078b4; color:#ffffff;  onclick='getVideoURL("+str2.trim()+")'>");	
												sb.append(vvir.getQuestionText());
										//	sb.append("</button>");
										sb.append("</div>");
									}
								}
							}
						}
						
					sb.append("</div>");
					sb.append("<div class='col-sm-6 col-md-6  top10 mt15'>");
						sb.append("Please click on question for getting video interview");
						sb.append("<iframe src='"+playerUrl+"' id='ifrmTrans' width='100%' height='300px'></iframe>");
					sb.append("</div>");
				sb.append("</div>");
						
		
				if(i4InterviewInvites!=null && i4InterviewInvites.size()>0)
				{
					if(i4InterviewInvites.get(0).getI4VideoInterviewMaxScore()!=null)
					{
						maxScore = i4InterviewInvites.get(0).getI4VideoInterviewMaxScore();
					
						if( maxScore>0)
						{
							scoreflag=true;
							int iTickInterval=maxScore/10;
							
							if(i4InterviewInvites.get(0).getI4VideoInterviewScore()!=null)
								SliderScore = i4InterviewInvites.get(0).getI4VideoInterviewScore();
						
							sb.append("<div class='row'>");
							sb.append("<div class='col-sm-9 col-md-9  top20'>");
							sb.append("<input type='hidden' id='inviteInterviewID' value='"+i4InterviewInvites.get(0).getId()+"'/>");
							sb.append("<iframe id='ifrmForVideoII'  src='slideract.do?name=normScoreFrm&tickInterval="+iTickInterval+"&max="+maxScore+"&swidth=650&svalue="+SliderScore+"' scrolling='no' frameBorder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:670px;'></iframe>");
							sb.append("</div>");
							sb.append("</div>");
						}
					}
				}
			}
			
			ReturnArray[0]=sb.toString();
			ReturnArray[1]=""+scoreflag;
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return ReturnArray;
	}
	
	
	public String saveInterviewValue(String inviteInterviewId,String interviewMarks)
	{
		try
		{
			
			System.out.println(" interviewMarks ::::: "+interviewMarks);
			
			if(interviewMarks!=null && !interviewMarks.equals(""))
			{
				I4InterviewInvites i4InterviewInvites = new I4InterviewInvites();
				i4InterviewInvites = i4InterviewInvitesDAO.findById(Integer.parseInt(inviteInterviewId), false, false);
				i4InterviewInvites.setI4VideoInterviewScore(Integer.parseInt(interviewMarks));
				i4InterviewInvitesDAO.makePersistent(i4InterviewInvites);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return "0";
	}
	
	
	/*** Add by Sekhar, This method using for send automatically mail ( Staus Updated )***/
	public int saveIIStatusByThread(JobOrder jobOrder, TeacherDetail teacherDetail, I4QuestionSets  i4QuestionSets,UserMaster userMaster,HttpServletRequest request)
	{
		System.out.println("========== saveIIStatusByThread ============");
		try{
			////////////////////// Add candidate in i4 candidate table /////////////////////
		    String i4CandidateId = null; // null for create candidate
		    String apiI4CandidateId = null;
		    String[] apiCandidateStr	=	new String[3];
		    I4Candidate i4Candidate = null;
		    String i4Inviteinterview = "new"; // null for create are candidate
		    String apiIIID= null;
		    String apiI4InviteinterviewURL= null;
			String userName = "";
		    String password = "";
		    I4InterviewInvites objInviteInterview = null;
		    
		    boolean sendInviteOrNot = inviteAlreadySentInSameQuestionSet(i4QuestionSets.getID(),teacherDetail.getTeacherId(),jobOrder.getDistrictMaster().getDistrictId());			
			if(sendInviteOrNot){
				return 0;
			}
		    List<I4InterviewInvites> existI4InterviewInvites = new ArrayList<I4InterviewInvites>();
		 
		    List<I4DistrictAccount> i4DistrictAccountlst = i4DistrictAccountDAO.findByDistrict(jobOrder.getDistrictMaster());
		    if(i4DistrictAccountlst!=null && i4DistrictAccountlst.size()>0){
		        userName    =    i4DistrictAccountlst.get(0).getI4username();
		        password    =     i4DistrictAccountlst.get(0).getI4password();
		    }
		    System.out.println(" username :: "+userName+" pass :: "+password);
		    try{
		        List<I4Candidate> lstI4CandidateExist =i4CandidateDAO.findByteacherDetail(teacherDetail,jobOrder.getDistrictMaster());
		        if(lstI4CandidateExist.size()>0){
		            i4CandidateId = lstI4CandidateExist.get(0).getI4CandidateID();
		            i4Candidate = lstI4CandidateExist.get(0);
		            System.out.println("  candidate already exist= "+i4Candidate+" lstI4CandidateExist.get(0) "+lstI4CandidateExist.get(0).getI4CandidateID());
		            
		            apiI4CandidateId = lstI4CandidateExist.get(0).getI4CandidateID(); 
		        }else{
		            i4Candidate = new I4Candidate();
		            apiCandidateStr = JDigestI4API.I4_AddOrUpdateCandidate(userName, password,teacherDetail.getFirstName(), teacherDetail.getLastName(), teacherDetail.getEmailAddress(), i4CandidateId);
		            
		            apiI4CandidateId = apiCandidateStr[0];
		            
		            System.out.println("username:"+apiCandidateStr[1]+"password:"+apiCandidateStr[2]);
		            
		            // Save Data in I4Candidate Table
		            if(apiCandidateStr[0]!=null)
		            {
			            i4Candidate.setDistrictMaster(jobOrder.getDistrictMaster());
			            i4Candidate.setTeacherDetail(teacherDetail);
			            i4Candidate.setDateCreated(new Date());
			            i4Candidate.setI4CandidateID(apiI4CandidateId);
			            i4Candidate.setUserName(apiCandidateStr[1]);
			            i4Candidate.setPassword(apiCandidateStr[2]);
			            
			            i4CandidateDAO.makePersistent(i4Candidate);
		            }else{
		            	System.out.println("Candidate I4 ID is NULL");
		            }
		        }
		    }catch(Exception e){
		        e.printStackTrace();
		    }
		                
		    //////////////////////////// Invite candidate for interview //////////////////////
			try{
		        existI4InterviewInvites = i4InterviewInvitesDAO.getListbyTeacherAndjobId(jobOrder, teacherDetail);
		        if(existI4InterviewInvites.size()>0){
		            i4Inviteinterview = existI4InterviewInvites.get(0).getI4inviteid().toString();
		            objInviteInterview = existI4InterviewInvites.get(0);
		        }else{
		            objInviteInterview = new I4InterviewInvites();
		        }
		        String expiryDate = "";
		        String timeAllowedPerQuestion = "";
		        String MaxScore = "";
		        
		        System.out.println("i4Inviteinterview :: "+i4Inviteinterview+" apiI4CandidateId :: "+apiI4CandidateId+" i4QuestionSets.getI4QuestionSetID() :: "+i4QuestionSets.getI4QuestionSetID());
		        try{
		            if(jobOrder.getOfferVirtualVideoInterview()){
		                if(jobOrder.getVVIExpiresInDays()!=null)
		                    expiryDate = jobOrder.getVVIExpiresInDays().toString();
		                
		                if(jobOrder.getTimeAllowedPerQuestion()!=null)
		                    timeAllowedPerQuestion = jobOrder.getTimeAllowedPerQuestion().toString();
		                
		                if(jobOrder.getMaxScoreForVVI()!=null)
		                    MaxScore = jobOrder.getMaxScoreForVVI().toString();;
		            }
		                
		        }catch(Exception e){
		            e.printStackTrace();
		        }
		        
		        System.out.println("userName: "+userName+" password: "+password+" i4Inviteinterview: "+i4Inviteinterview+" apiI4CandidateId "+apiI4CandidateId+" i4QuestionSets.getI4QuestionSetID(): "+i4QuestionSets.getI4QuestionSetID()+" expiryDate: "+expiryDate+" timeAllowedPerQuestion: "+timeAllowedPerQuestion);
		        
		        apiI4InviteinterviewURL = JDigestI4API.inviteCandidateForInterview(userName,password,i4Inviteinterview, apiI4CandidateId,i4QuestionSets.getI4QuestionSetID(),expiryDate,timeAllowedPerQuestion);
		        if(apiI4InviteinterviewURL!=null){
		        	String apiInterViewUrl[] = apiI4InviteinterviewURL.split("/");
		            if(apiInterViewUrl.length==5)
		                apiIIID = apiInterViewUrl[4];
		            objInviteInterview.setDatetimeset(new Date());
		            objInviteInterview.setJobOrder(jobOrder);
		            objInviteInterview.setTeacherDetail(teacherDetail);
		            objInviteInterview.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
		            objInviteInterview.setI4QuestionSets(i4QuestionSets);
		            objInviteInterview.setI4inviteid(Integer.parseInt(apiIIID));
		            if(apiI4InviteinterviewURL!=null)
		                objInviteInterview.setInterviewurl(apiI4InviteinterviewURL);
		            
		            objInviteInterview.setStatus(1);
		            objInviteInterview.setI4VideoInterviewMaxScore(Integer.parseInt(MaxScore));
		            i4InterviewInvitesDAO.makePersistent(objInviteInterview);
		            
		            //Send mail
		            createAndSendVVIMail(jobOrder, teacherDetail, i4Candidate, i4QuestionSets,userMaster.getEmailAddress(),null);
		        
		        }else{
		            System.out.println(" Interview URl is NULL ");  
		        }
		    }catch(Exception e){
		        e.printStackTrace();
		    }
		}catch(Exception e)
		{
		    e.printStackTrace();
		}
		return 0;
	}
	
	
	
	
	
	/* Invite Interview for Event management system */
	public int saveIIStatusForEvent(String participantId )
	{
		/* ========  For Session time Out Error =========*/
		
		System.out.println(" ======= saveIIStatusForEvent  ============== ");
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		String teacherId = "";
		DistrictMaster districtMaster = null;
		I4QuestionSets i4QuestionSets = null;
		List<EventSchedule> eventScheduleList = new ArrayList<EventSchedule>();
		EventParticipantsList eventParticipantsList = null;
	//	String quesSetId = "";
		String expiryDate = "";
		String timeAllowedPerQuestion = "";
		String MaxScore = "";
		
		GlobalServices.print("========= Start ============");
		
		try
		{
			
			if(participantId!=null && !participantId.equals(""))
				eventParticipantsList = eventParticipantsListDAO.findById(Integer.parseInt(participantId), false, false);
			
			if(eventParticipantsList!=null)
			{
				eventScheduleList = eventScheduleDAO.findByEventDetail(eventParticipantsList.getEventDetails());
				if(eventParticipantsList.getTeacherId()!=null)
					teacherId = eventParticipantsList.getTeacherId().toString();
				
				districtMaster = eventParticipantsList.getDistrictMaster();
				
				if(districtMaster!=null)
					System.out.println(" eventParticipantsList.getDistrictMaster() :: "+eventParticipantsList.getDistrictMaster().getDistrictId());
				
				if(eventParticipantsList.getEventDetails()!=null)
					if(eventParticipantsList.getEventDetails().getI4QuestionSets()!=null)
						i4QuestionSets = eventParticipantsList.getEventDetails().getI4QuestionSets();
				
				if(eventScheduleList!=null && eventScheduleList.size()>0)
					expiryDate = ""+eventScheduleList.get(0).getValidity();
				else
					expiryDate = Utility.getValueOfPropByKey("ExpiryDaysForVVI");
					
				if(districtMaster.getTimeAllowedPerQuestion()!=null && !districtMaster.getTimeAllowedPerQuestion().equals(""))
					timeAllowedPerQuestion = ""+districtMaster.getTimeAllowedPerQuestion();
				else
					timeAllowedPerQuestion = Utility.getValueOfPropByKey("TimeAllowedPerQuestionForVVI");
				
				if(districtMaster.getMaxScoreForVVI()!=null && !districtMaster.getMaxScoreForVVI().equals(""))
					MaxScore = ""+districtMaster.getMaxScoreForVVI();
				/*else
					MaxScore = Utility.getValueOfPropByKey("MaxScore");*/
				
				System.out.println(" ======= saveIIStatusForEvent  0============== ");
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		System.out.println(" ======= saveIIStatusForEvent  1============== ");
		try
		{
			System.out.println(" teacherId "+teacherId+" districtMaster "+districtMaster+" i4QuestionSets "+i4QuestionSets);
			
			if(teacherId!=null && !teacherId.equals("") && districtMaster!=null && i4QuestionSets!=null)
			{
				boolean sendInviteOrNot = inviteAlreadySentInSameQuestionSet(i4QuestionSets.getID(),Integer.parseInt(teacherId),districtMaster.getDistrictId());			
				if(sendInviteOrNot){
					return 0;
				}
				
					UserMaster userMaster = new UserMaster();
					userMaster=(UserMaster)session.getAttribute("userMaster");
					
					List<I4InterviewInvites> existI4InterviewInvites = new ArrayList<I4InterviewInvites>();
					I4InterviewInvites objInviteInterview = null;
					
					TeacherDetail teacherDetail = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
					
					System.out.println(" Teacher Email :: "+teacherDetail.getEmailAddress());
					
					
					List<I4DistrictAccount> i4DistrictAccountlst = i4DistrictAccountDAO.findByDistrict(districtMaster);
					String userName = "";
					String password = "";
					
					if(i4DistrictAccountlst!=null && i4DistrictAccountlst.size()>0)
					{
						userName	=	i4DistrictAccountlst.get(0).getI4username();
						password    = 	i4DistrictAccountlst.get(0).getI4password();
					}
					
					System.out.println(" username :: "+userName+" pass :: "+password);
					
					////////////////////// Add candidate in i4 candidate table /////////////////////
					String i4CandidateId = null; // null for create candidate
					String apiI4CandidateId = null;
					String[] apiCandidateStr	=	new String[3];
					I4Candidate i4Candidate = null;
					
					String i4Inviteinterview = "new"; // null for create are candidate
					String apiIIID= null;
					String apiI4InviteinterviewURL= null;
					//I4InterviewInvites i4IIForApi = null;
					boolean i4CandiateFlag = false;
					boolean i4IFlag = false;
					
					try
					{
						List<I4Candidate> lstI4CandidateExist = new ArrayList<I4Candidate>();
						lstI4CandidateExist = i4CandidateDAO.findByteacherDetail(teacherDetail,districtMaster);
						
						if(lstI4CandidateExist!=null && lstI4CandidateExist.size()>0)
						{
							i4CandidateId = lstI4CandidateExist.get(0).getI4CandidateID();
							i4Candidate = lstI4CandidateExist.get(0);
							
							System.out.println("  candidate already exist= "+i4Candidate+" lstI4CandidateExist.get(0) "+lstI4CandidateExist.get(0).getI4CandidateID());
							apiI4CandidateId = lstI4CandidateExist.get(0).getI4CandidateID(); 
							
						}
						else
						{
							i4Candidate = new I4Candidate();
							apiCandidateStr = JDigestI4API.I4_AddOrUpdateCandidate(userName, password,teacherDetail.getFirstName(), teacherDetail.getLastName(), teacherDetail.getEmailAddress(), i4CandidateId);
							apiI4CandidateId = apiCandidateStr[0];
							
							System.out.println("username:"+apiCandidateStr[1]+"password:"+apiCandidateStr[2]);
							
							// Save Data in I4Candidate Table
							if(apiCandidateStr[0]!=null)
							{
								i4Candidate.setDistrictMaster(districtMaster);
								i4Candidate.setTeacherDetail(teacherDetail);
								i4Candidate.setDateCreated(new Date());
								i4Candidate.setI4CandidateID(apiI4CandidateId);
								i4Candidate.setUserName(apiCandidateStr[1]);
								i4Candidate.setPassword(apiCandidateStr[2]);
								
								i4CandidateDAO.makePersistent(i4Candidate);
							}
							else
							{
								System.out.println("Candidate I4 ID is NULL");
							}
							
						//	i4CandiateFlag=true;
						}
					}catch(Exception e)
					{
						e.printStackTrace();
					}
								
					//////////////////////////// Invite candidate for interview //////////////////////
					try
						{
						existI4InterviewInvites = i4InterviewInvitesDAO.getListbyTeacherAndDistForEventManagement(districtMaster.getDistrictId(), teacherDetail);
						
						System.out.println(" existI4InterviewInvites "+existI4InterviewInvites);
						
						if(existI4InterviewInvites!=null && existI4InterviewInvites.size()>0)
						{
							i4Inviteinterview = existI4InterviewInvites.get(0).getI4inviteid().toString();
							objInviteInterview = existI4InterviewInvites.get(0);
						}
								
								System.out.println("userName: "+userName+" password: "+password+" i4Inviteinterview: "+i4Inviteinterview+" apiI4CandidateId "+apiI4CandidateId+" i4QuestionSets.getI4QuestionSetID(): "+i4QuestionSets.getI4QuestionSetID()+" expiryDate: "+expiryDate+" timeAllowedPerQuestion: "+timeAllowedPerQuestion);
								
								if(objInviteInterview!=null && objInviteInterview.getStatus()<3){
									apiI4InviteinterviewURL = JDigestI4API.inviteCandidateForInterviewResend(userName, password, i4Inviteinterview, apiI4CandidateId);
									
									//Email For Resend Invite
									//createAndSendVVIMail(districtMaster, teacherDetail, i4Candidate, i4QuestionSets,userMaster.getEmailAddress());
									
									System.out.println(" resend message :: "+apiI4InviteinterviewURL);
								}
								
								if(objInviteInterview==null)
								{
									objInviteInterview = new I4InterviewInvites();
									
									System.out.println(" expiryDate :"+expiryDate+" timeAllowedPerQuestion:"+timeAllowedPerQuestion+" MaxScore:"+MaxScore);
									
									apiI4InviteinterviewURL = JDigestI4API.inviteCandidateForInterview(userName,password,i4Inviteinterview, apiI4CandidateId,i4QuestionSets.getI4QuestionSetID(),expiryDate,timeAllowedPerQuestion);
								
									if(apiI4InviteinterviewURL!=null)
									{
										String apiInterViewUrl[] = apiI4InviteinterviewURL.split("/");
										if(apiInterViewUrl.length==5)
											apiIIID = apiInterViewUrl[4];
										objInviteInterview.setDatetimeset(new Date());
										objInviteInterview.setJobOrder(null);
										objInviteInterview.setTeacherDetail(teacherDetail);
										objInviteInterview.setDistrictId(districtMaster.getDistrictId());
										objInviteInterview.setI4QuestionSets(i4QuestionSets);
										objInviteInterview.setI4inviteid(Integer.parseInt(apiIIID));
										if(apiI4InviteinterviewURL!=null)
											objInviteInterview.setInterviewurl(apiI4InviteinterviewURL);
										
										objInviteInterview.setStatus(1);
										try{
											if(MaxScore!=null && !MaxScore.equals(""))
												objInviteInterview.setI4VideoInterviewMaxScore(Integer.parseInt(MaxScore));
										}catch(Exception e){
											objInviteInterview.setI4VideoInterviewMaxScore(0);
											e.printStackTrace();
										}
										i4InterviewInvitesDAO.makePersistent(objInviteInterview);
										
										eventParticipantsList.setI4InterviewInvites(objInviteInterview);
										eventParticipantsListDAO.makePersistent(eventParticipantsList);
										
										//Send Email
										createAndSendVVIMail(districtMaster, teacherDetail, i4Candidate, i4QuestionSets,userMaster.getEmailAddress(),null);
									}
									else
										System.out.println(" Interview URl is NULL ");	
								
								}else{
									System.out.println("You can not resend the invitation");
									return 2;
								}
								//	i4IFlag=true;			
						}catch(Exception e)
						{
							e.printStackTrace();
						}
				}
				else
				{
					System.out.println(" Invite not Send ");
					return 1;
				}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public void createAndSendVVIMail(JobOrder joborder,TeacherDetail teacherDetail,I4Candidate i4Candidate,I4QuestionSets i4QuestionSets,String userEmail,Date sendInvitedate)
	{
		/*WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }*/
		
		try
		{
			
		//	UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");
			
			String[] emailDetails = new String[10]; 
			List<I4QuestionSetQuestions> questionList = new ArrayList<I4QuestionSetQuestions>();
			questionList = i4QuestionSetQuestionsDAO.findByQuesSetId(i4QuestionSets, joborder.getDistrictMaster() , joborder.getHeadQuarterMaster());   // @ Anurag add HQ in param
			int timePerQues = 0;
			int maxTimePerQues = 0;

			if(questionList.size()>0)
			{
				if(joborder.getTimeAllowedPerQuestion()!=null && !joborder.getTimeAllowedPerQuestion().equals("") && !joborder.getTimeAllowedPerQuestion().equals("0"))
					timePerQues	=	joborder.getTimeAllowedPerQuestion();
				else
					timePerQues	= Integer.parseInt(Utility.getValueOfPropByKey("TimeAllowedPerQuestionForVVI"));
				
				if(timePerQues>0)
				{
					if((timePerQues*questionList.size())/60==0)
						maxTimePerQues = (timePerQues*questionList.size())/60;
					else
						maxTimePerQues = ((timePerQues*questionList.size())/60)+1;
				}
			}else{System.out.println("Question size is 0");}
			
			
			emailDetails[0] = getTeacherName(teacherDetail);
			emailDetails[1] = joborder.getDistrictMaster().getDistrictName();
			emailDetails[2] = ""+questionList.size();
			emailDetails[3]	=	timePerQues+"";
			emailDetails[4] = ""+maxTimePerQues;
			emailDetails[5] = i4Candidate.getUserName();
			emailDetails[6]	= i4Candidate.getPassword();
			if(sendInvitedate!=null)
				emailDetails[7] = getDateANdTimeWithTimeFormatForReSendVVI(joborder.getVVIExpiresInDays(),sendInvitedate);
			else
				emailDetails[7]	= getDateANdTimeWithTimeFormatForVVI(joborder.getVVIExpiresInDays());
			
			emailDetails[8] = teacherDetail.getEmailAddress();
			emailDetails[9] = userEmail;
			
			//Email For Resend Invite
			sendVVIInviteInterviewEmail(emailDetails);
			
			
		}catch(Exception e){e.printStackTrace();}
	}
	
	
	public void createAndSendVVIMail(DistrictMaster districtMaster,TeacherDetail teacherDetail,I4Candidate i4Candidate,I4QuestionSets i4QuestionSets,String userEmail,Date sendInvitedate)
	{
		/*WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }*/
		try
		{
			/*UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");*/
			
			String[] emailDetails = new String[10]; 
			List<I4QuestionSetQuestions> questionList = new ArrayList<I4QuestionSetQuestions>();
			
			HeadQuarterMaster headQuarterMaster = null;
			
			questionList = i4QuestionSetQuestionsDAO.findByQuesSetId(i4QuestionSets, districtMaster , headQuarterMaster);	// @ Anurag add HQ in param
			int timePerQues = 0;
			int maxTimePerQues = 0;

			if(questionList.size()>0)
			{
				if(districtMaster.getTimeAllowedPerQuestion()!=null && !districtMaster.getTimeAllowedPerQuestion().equals("") && !districtMaster.getTimeAllowedPerQuestion().equals("0"))
					timePerQues	=	districtMaster.getTimeAllowedPerQuestion();
				else
					timePerQues	= Integer.parseInt(Utility.getValueOfPropByKey("TimeAllowedPerQuestionForVVI"));
				
				if(timePerQues>0)
				{
					if((timePerQues*questionList.size())/60==0)
						maxTimePerQues = (timePerQues*questionList.size())/60;
					else
						maxTimePerQues = ((timePerQues*questionList.size())/60)+1;
				}
			}else{System.out.println("Question size is 0");}
			
			
			emailDetails[0] = getTeacherName(teacherDetail);
			emailDetails[1] = districtMaster.getDistrictName();
			emailDetails[2] = ""+questionList.size();
			emailDetails[3]	=	timePerQues+"";
			emailDetails[4] = ""+maxTimePerQues;
			emailDetails[5] = i4Candidate.getUserName();
			emailDetails[6]	= i4Candidate.getPassword();
			if(sendInvitedate!=null)
				emailDetails[7] = getDateANdTimeWithTimeFormatForReSendVVI(districtMaster.getVVIExpiresInDays(),sendInvitedate);
			else
				emailDetails[7]	= getDateANdTimeWithTimeFormatForVVI(districtMaster.getVVIExpiresInDays());
			emailDetails[8] = teacherDetail.getEmailAddress();
			emailDetails[9] = userEmail;

			//Email For Resend Invite
			sendVVIInviteInterviewEmail(emailDetails);
			
			
		}catch(Exception e){e.printStackTrace();}
	}
	
	
	public void sendVVIInviteInterviewEmail(String[] emailDetails)
	{
		String mailContent ="" ;
		
		DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
		dsmt.setEmailerService(emailerService);
		dsmt.setMailfrom(emailDetails[9]);
		dsmt.setMailto(emailDetails[8]);
		dsmt.setMailsubject("Virtual video interview invitation");
		//mailContent = MailText.emailToSchoolAdmins(request,saObj,userMaster,districtMaster,jobTitle);
		mailContent = MailText.sendVVIInviteInterviewEmail(emailDetails);
		dsmt.setMailcontent(mailContent);
		System.out.println(emailDetails[9]+"from     to"+emailDetails[8]);
		
		dsmt.start();
	}
	
	public String getDateANdTimeWithTimeFormatForVVI(Integer days)
	{
		String res = "";
		Date resDate = null;
		
		try
		{
			Calendar cal = Calendar.getInstance();

			System.out.println(days);
			
			if(days!=null && !days.equals("") && days>0)
				cal.add(Calendar.DATE, days);
			else
				cal.add(Calendar.DATE, Integer.parseInt(Utility.getValueOfPropByKey("ExpiryDaysForVVI")));
				
			resDate = cal.getTime();
			
			SimpleDateFormat ft = new SimpleDateFormat ("EEEE, MMMM dd, yyyy 'at' hh:mm a zzz");
			res = ft.format(resDate);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return res;
	}
	
	public String getDateANdTimeWithTimeFormatForReSendVVI(Integer days,Date inviteDate)
	{
		String res = "";
		Date resDate = null;
		
		try
		{
			Calendar cal = Calendar.getInstance();
			cal.setTime(inviteDate);
			
			System.out.println(days);
			
			if(days!=null && !days.equals("") && days>0){
				cal.add(Calendar.DATE, days);
				System.out.println(" >>>>>>>>111>>>"+cal.getTime());
			}else
				cal.add(Calendar.DATE, Integer.parseInt(Utility.getValueOfPropByKey("ExpiryDaysForVVI")));
				
			resDate = cal.getTime();
			
			SimpleDateFormat ft = new SimpleDateFormat ("EEEE, MMMM dd, yyyy 'at' hh:mm a zzz");
			res = ft.format(resDate);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return res;
	}
	
	// Download Files from DWF Server
	private CloudFilesApi cloudFiles;

	public void getAccessOfServer(){
		cloudFiles = ContextBuilder.newBuilder(RCConfig.RC_CLOUDFILE_PROVIDER).credentials(RCConfig.RC_CLOUDFILE_USERNAME, RCConfig.RC_CLOUDFILE_APIKEY).buildApi(CloudFilesApi.class);
	}
	
	 /*public void close() throws IOException {
	      Closeables.close(cloudFiles, true);
	   }*/
	
	public SwiftObject getObject(String sFileName) {
	      System.out.format("Get Object%n");

	      ObjectApi objectApi = cloudFiles.getObjectApi("DFW", "Virtual_Video_Interviews");
	      
	      SwiftObject swiftObject = objectApi.get(sFileName);

	      System.out.format("  %s%n", swiftObject);

	      return swiftObject;
	   }

	 public String writeObject(SwiftObject swiftObject,String sFileName,String sSourcePath,String sTargetPath) throws IOException {
		 
		 String sSavedFileName="";
		 String filePath ="";
		 try
		 {
			 System.out.format("Write Object RC %n");
		     
		      InputStream inputStream = swiftObject.getPayload().openStream();
		      
		      String slashType = (sFileName.lastIndexOf("/") > 0) ? "/" : "/";
			  int startIndex = sFileName.lastIndexOf(slashType);
		      String extention=sFileName.substring(sFileName.lastIndexOf("."),sFileName.length());
		      System.out.println("extention "+extention);
		      String sFileNameWithOutExt = sFileName.substring(startIndex + 1, sFileName.lastIndexOf("."));
		      System.out.println("sFileNameWithOutExt "+sFileNameWithOutExt +" sTargetPath "+sTargetPath);
		      
		      filePath = sTargetPath+"/"+"vviVideos";
		      File FileDirectory=new File(filePath); 
		      
		      if(!FileDirectory.exists())
		    	  FileDirectory.mkdirs();
		      
		      /*try{
		    	// In case File Name has "'" Sign
			      if(sFileNameWithOutExt.contains("'")){
			    	  sFileNameWithOutExt = sFileNameWithOutExt.replace("'", "");
			      }
		      }catch(Exception e){
		    	  e.printStackTrace();
		      }*/
		      
		      
		      File file = File.createTempFile(sFileNameWithOutExt,extention,FileDirectory);
		      
		      BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));

		      try {
		         ByteStreams.copy(inputStream, outputStream);
		      }
		      finally {
		         inputStream.close();
		         outputStream.close();
		      }

		      System.out.format("  %s%n", file.getAbsolutePath());
		      sSavedFileName=file.getName();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 
	      
	      return sSavedFileName;
	   }
	
	public String getPathOfVideoUrl(String fileName)
	 {
		 System.out.println(new Date()+" Calling downloadResume method for Porfilio Resume ");

			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException("Your session has expired!");
			}
		 String videoPlayerURL = "";
		 
		 try
		 {
			 	String source = Utility.getValueOfPropByKey("teacherRootPath")+"";
				System.out.println("Porfilio Resume source "+source);

				String target = context.getServletContext().getRealPath("/")+"/";
				System.out.println("Porfilio Resume target "+target);

				File sourceFile = new File(source);
				File targetDir = new File(target);
				if(!targetDir.exists())
					targetDir.mkdirs();

				File targetFile = new File(targetDir+"/"+sourceFile.getName());
			 
				// Start ... read from Rackspace cloud file
				System.out.println("Read file By Rackspace cloud file");
				String sSourcePath="";
				String sTargetPath=target;
			 
				 RCCloudFileServices cloudFileServices = new RCCloudFileServices(RCConfig.RC_CLOUDFILE_USERNAME, RCConfig.RC_CLOUDFILE_APIKEY);
			 
				try {
					
					System.out.println(" sSourcePath :: "+sSourcePath+" fileName :: "+fileName);
					
			         SwiftObject swiftObject = getObject(sSourcePath+""+fileName);
			         String sSavedFileName=writeObject(swiftObject, fileName, sSourcePath, sTargetPath);
			         videoPlayerURL = Utility.getValueOfPropByKey("contextBasePath")+"/vviVideos/"+sSavedFileName;
			         
			         System.out.println(" videoPlayerURL ::::: >>>>>>>>>>>>>>>>>> "+videoPlayerURL);
			      }
			      catch (Exception e) {
			         e.printStackTrace();
			      }
			      finally {
			    	  cloudFileServices.close();
			      }
			 getAccessOfServer();
		//	 SwiftObject swiftObject = getObject(fileName);
		//	 videoPlayerURL =  writeObject(swiftObject,fileName,sSourcePath,sTargetPath);
			 
		 }catch(Exception e)
		 {
			e.printStackTrace(); 
		 }
		 
		 System.out.println(" videoPlayerURL final ::::: "+videoPlayerURL);
		 
		 
		 return videoPlayerURL;
	 }
	
	public boolean inviteAlreadySentInSameQuestionSet(Integer questionSetId,Integer teacherId,Integer districtId){
		boolean returnStatus=false;
		try {
			returnStatus = i4InterviewInvitesDAO.inviteAlreadySentInSameQuestionSet(questionSetId,teacherId,districtId);			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return returnStatus;
	}
	
	public String getInterviewNew(Map<Integer,Integer> mapInviteInterview,JobForTeacher jft,Integer noOfRecordCheck,String windowFunc)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		//System.out.println("Calling getInterview");
		
		StringBuffer sb=new StringBuffer();
		
		int chk=0;
		
		try
		{
			if(mapInviteInterview.get(jft.getTeacherId().getTeacherId())!=null)
				chk = mapInviteInterview.get(jft.getTeacherId().getTeacherId());
			
			System.out.println(chk+" ==test== "+jft.getTeacherId().getTeacherId());
			if(chk==0)
				sb.append("&nbsp;<a data-original-title='"+toolClickToInvite1+"' rel='tooltip' id='i4Ques"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"displayIIQuesForRed('"+jft.getJobId().getJobId()+"','"+jft.getTeacherId().getTeacherId()+"');"+windowFunc+"\" >&nbsp;<img height='20px' width='20' src=\"images/RedVideo.png\"></a>");	
			else if(chk==1)
				sb.append("&nbsp;<a data-original-title='"+toolVirtualVideoInte1+"' rel='tooltip' id='i4Ques"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"displayIIQuesForGray('"+jft.getJobId().getJobId()+"','"+jft.getTeacherId().getTeacherId()+"');"+windowFunc+"\" >&nbsp;<img height='20px' width='20' src=\"images/GreyVideo.png\"></a>");
			else if(chk==2)
				sb.append("&nbsp;<a data-original-title='"+toolVirtualVideoInte2+"' rel='tooltip' id='i4Ques"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"displayIIQuesForQrange('"+jft.getJobId().getJobId()+"','"+jft.getTeacherId().getTeacherId()+"');"+windowFunc+"\" >&nbsp;<img height='20px' width='20' src=\"images/OrangeVideo.png\"></a>");
			else if(chk==3)
			{
				//sb.append("&nbsp;<a data-original-title='Virtual Video Interview completed. Click to view the interview.' rel='tooltip' id='i4Ques"+noOfRecordCheck+"' href='https://google.com' onclick='' target='_blank'>&nbsp;<img height='20px' width='20' src=\"images/BlueVideo.png\"></a>");
				sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+toolVirtualVideoInterviewCompleted1+"'  id='i4Ques"+noOfRecordCheck+"' onclick=\"openInterViewLink('"+jft.getJobId().getJobId()+"','"+jft.getTeacherId().getTeacherId()+"','trans"+jft.getJobForTeacherId()+"');\">&nbsp;<img height='20px' width='20' src=\"images/BlueVideo.png\"></a>");
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return sb.toString();
	}
}