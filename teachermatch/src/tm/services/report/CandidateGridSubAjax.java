package tm.services.report;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.activemq.command.ActiveMQQueue;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.CGStatusEventTemp;
import tm.bean.DistrictPanelMaster;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.PanelMembers;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherSecondaryStatus;
import tm.bean.hqbranchesmaster.StatusNodeColorHistory;
import tm.bean.hqbranchesmaster.WorkFlowStatus;
import tm.bean.master.DemoClassAttendees;
import tm.bean.master.DemoClassSchedule;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.bean.master.DistrictSpecificPortfolioOptions;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.TimeZoneMaster;
import tm.bean.master.UserEmailNotifications;
import tm.bean.mq.MQEvent;
import tm.bean.mq.MQEventHistory;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.CGStatusEventTempDAO;
import tm.dao.DistrictPanelMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.PanelMembersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherSecondaryStatusDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.hqbranchesmaster.StatusNodeColorHistoryDAO;
import tm.dao.hqbranchesmaster.WorkFlowStatusDAO;
import tm.dao.master.DemoClassAttendeesDAO;
import tm.dao.master.DemoClassNotesDetailsDAO;
import tm.dao.master.DemoClassScheduleDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificPortfolioAnswersDAO;
import tm.dao.master.DistrictSpecificPortfolioOptionsDAO;
import tm.dao.master.DistrictSpecificPortfolioQuestionsDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.dao.master.PanelAttendeesDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TimeZoneMasterDAO;
import tm.dao.master.UserEmailNotificationsDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.mq.MQEventHistoryDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.services.district.ManageStatusAjax;
import tm.utility.IPAddressUtility;
import tm.utility.TMCommonUtil;
import tm.utility.Utility;


public class CandidateGridSubAjax 
{
	@Autowired
	private StatusNodeColorHistoryDAO statusNodeColorHistoryDAO;
	
	@Autowired
	private WorkFlowStatusDAO workFlowStatusDAO;
	private static final Logger logger = Logger.getLogger(CandidateGridSubAjax.class.getName());
	@Autowired
	private ManageStatusAjax manageStatusAjax;
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	@Autowired
	private TMCommonUtil tmCommonUtil;
	@Autowired
	private CandidateGridService candidateGridService;
	
	private static final int MYTHREADS = 1000;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private DemoClassScheduleDAO demoClassScheduleDAO;
	public void setDemoClassScheduleDAO(
			DemoClassScheduleDAO demoClassScheduleDAO) {
		this.demoClassScheduleDAO = demoClassScheduleDAO;
	}
	@Autowired
	private DemoClassAttendeesDAO demoClassAttendeesDAO; 
	public void setDemoClassAttendeesDAO(
			DemoClassAttendeesDAO demoClassAttendeesDAO) {
		this.demoClassAttendeesDAO = demoClassAttendeesDAO;
	}
	@Autowired
	private TimeZoneMasterDAO timeZoneMasterDAO;
	public void setTimeZoneMasterDAO(TimeZoneMasterDAO timeZoneMasterDAO) {
		this.timeZoneMasterDAO = timeZoneMasterDAO;
	}

	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO)
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private EmailerService emailerService;


	@Autowired
	private DemoClassNotesDetailsDAO demoClassNotesDetailsDAO;

	@Autowired
	private DistrictMasterDAO districtMasterDAO;

	@Autowired
	private UserEmailNotificationsDAO userEmailNotificationsDAO;

	@Autowired
	private PanelScheduleDAO panelScheduleDAO;

	@Autowired
	private PanelAttendeesDAO panelAttendeesDAO;

	@Autowired
	private DistrictPanelMasterDAO districtPanelMasterDAO;

	@Autowired
	private PanelMembersDAO panelMembersDAO;

	@Autowired
	private JobWisePanelStatusDAO jobWisePanelStatusDAO;
	@Autowired
	private DistrictSpecificPortfolioQuestionsDAO districtSpecificPortfolioQuestionsDAO;
	@Autowired
	private DistrictSpecificPortfolioAnswersDAO districtSpecificPortfolioAnswersDAO;
	@Autowired
	private TeacherSecondaryStatusDAO teacherSecondaryStatusDAO;
	@Autowired
	private DistrictSpecificPortfolioOptionsDAO districtSpecificPortfolioOptionsDAO;
	
	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;
	
	@Autowired
    private MQEventDAO mqEventDAO;
    
    @Autowired
    private MQEventHistoryDAO mqEventHistoryDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private CGStatusEventTempDAO cGStatusEventTempDAO;
	
	String locale = Utility.getValueOfPropByKey("locale");	 
	/*==============================show all attendee ==============================================*/
	public JSONObject displayAttendee(TeacherDetail teacherDetail,JobOrder jobOrder,boolean isDemo)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		JSONObject jsonOutput = new JSONObject();
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}
		System.out.println("inside displayAttendee");
		StringBuffer dmRecords = new StringBuffer();
		try{

			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);

			List<DemoClassSchedule> demoClassScheduleList = demoClassScheduleDAO.findDemoClassSchedulesByTeacherAndJob(teacherDetail,jobOrder,districtMaster,schoolMaster);
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+demoClassScheduleList.size());
			DemoClassSchedule demoClassSchedule = null;

			if(demoClassScheduleList.size()>0)
			{
				demoClassSchedule = demoClassScheduleList.get(0);

				jsonOutput.put("demoId", demoClassSchedule.getDemoId());
				jsonOutput.put("districtId", demoClassSchedule.getDistrictId().getDistrictId());
				if(demoClassSchedule.getSchoolId()==null)
					jsonOutput.put("schoolId", 0);
				else
					jsonOutput.put("schoolId", demoClassSchedule.getSchoolId().getSchoolId());
				jsonOutput.put("demoDate", Utility.convertDateAndTimeToDatabaseformatOnlyDate(demoClassSchedule.getDemoDate()));
				jsonOutput.put("demoTime", demoClassSchedule.getDemoTime());
				jsonOutput.put("timeFormat", demoClassSchedule.getTimeFormat());
				jsonOutput.put("timeZoneId", demoClassSchedule.getTimeZoneId().getTimeZoneId());
				jsonOutput.put("demoClassAddress", demoClassSchedule.getDemoClassAddress());
				jsonOutput.put("demoDescription", demoClassSchedule.getDemoDescription());
				jsonOutput.put("demoStatus", demoClassSchedule.getDemoStatus());
				JSONArray dataArray = new JSONArray();

				List<DemoClassAttendees> demoClassAttendees=demoClassAttendeesDAO.getDemoClassAttendees(demoClassSchedule);
				if(demoClassAttendees.size()<1){
					dmRecords.append("<div class='span3'>No Attendee Found</div>");
				}else {
					for(DemoClassAttendees demo : demoClassAttendees){
						UserMaster inviteeId = demo.getInviteeId();
						dmRecords.append("<div id='"+inviteeId.getUserId()+"' class='row col-sm-4 col-md-4'>"+
								""+inviteeId.getFirstName()+" "+inviteeId.getLastName()
								+"<a href='javascript:void(0);' onclick='deleteAttendee(\""+inviteeId.getUserId()+"\");'><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>");

						dataArray.add(demo.getInviteeId().getUserId());
					}
				}
				jsonOutput.put("attendeeList", dataArray);
				jsonOutput.put("attendees", dmRecords.toString());
				Boolean demoNoteStatus=demoClassNotesDetailsDAO.getDemoClassNotesByDemoSchedule(demoClassSchedule);
				jsonOutput.put("demoNoteStatus", demoNoteStatus);

			}

		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		//return dmRecords.toString();
		//System.out.println(jsonOutput.toString());
		return jsonOutput;
	}
	/*======================save saveDemoSchedule ===========================*/ 
	public void saveDemoSchedule(String demoDateVal,String demoTime,String meridiem,Integer	timezoneId,
			String location,String disSchCmnt,Integer teacherId,Integer jobId,boolean demoStatus,Integer[] arr,Integer demoId){

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		//11-18-2013
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
		//String dateInString = "7-Jun-2013";
		Date demoDate = null;
		try {

			demoDate = formatter.parse(demoDateVal);
			System.out.println(demoDate);
			System.out.println(formatter.format(demoDate));

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("demoId: lllllllllll "+demoId);
		String mailContent = "";
		int demochangedcnt = 0;
		DemoClassSchedule demoClassSchedule=new DemoClassSchedule();
		if(demoId!=0)
		{
			//demoClassSchedule.setDemoId(demoId);
			demoClassSchedule = demoClassScheduleDAO.findById(demoId, false, false);

			demoClassSchedule.setDemoClassAddress(location);
			if(!demoClassSchedule.getDemoDate().equals(demoDate))
				demochangedcnt++;
			if(!demoClassSchedule.getDemoTime().equals(demoTime))
				demochangedcnt++;
			if(!demoClassSchedule.getTimeFormat().equals(meridiem))
				demochangedcnt++;
			if(!demoClassSchedule.getTimeZoneId().getTimeZoneId().equals(timezoneId))
				demochangedcnt++;
			if(!demoClassSchedule.getDemoClassAddress().equals(location))
				demochangedcnt++;
		}else
		{
			demoClassSchedule.setDistrictId(districtMaster);
			demoClassSchedule.setSchoolId(schoolMaster);

			TeacherDetail teacherDetail =new TeacherDetail();
			teacherDetail.setTeacherId(teacherId);
			JobOrder jobOrder=new JobOrder();
			jobOrder.setJobId(jobId);
			demoClassSchedule.setTeacherId(teacherDetail);
			demoClassSchedule.setJobOrder(jobOrder);
			demoClassSchedule.setCreatedDateTime(new Date());
		}

		TimeZoneMaster timeZoneMaster= new TimeZoneMaster();
		timeZoneMaster.setTimeZoneId(timezoneId);

		String dStatus=null;
		if(demoStatus){
			dStatus="Completed";
		}else{
			dStatus="Scheduled";
		}
		System.out.println("demoDate::::::::::::::::::::::::::::::::::::::: "+demoDate);
		demoClassSchedule.setDemoDate(demoDate);
		demoClassSchedule.setDemoTime(demoTime);
		demoClassSchedule.setTimeFormat(meridiem);
		demoClassSchedule.setTimeZoneId(timeZoneMaster);
		demoClassSchedule.setDemoClassAddress(location);
		demoClassSchedule.setDemoDescription(disSchCmnt);
		demoClassSchedule.setCreatedBy(userMaster);
		demoClassSchedule.setDemoStatus(dStatus);

		demoClassScheduleDAO.makePersistent(demoClassSchedule);

		String distributionEmail=districtMasterDAO.getDistributionEmail(districtMaster);
		demoClassSchedule = demoClassScheduleDAO.findById(demoClassSchedule.getDemoId(), false, false);
		//distributionEmail = "vishwanath@netsutra.com";
		boolean isDistributionMail = true;
		if(distributionEmail==null || distributionEmail.equals(""))
			isDistributionMail = false;

		String emailto = "vishwanath@netsutra.com";
		if(arr.length>0 && demoId==0)
		{
			for(int i=0;i<arr.length;i++)
			{	
				DemoClassAttendees demoClassAttendees = new DemoClassAttendees();
				demoClassAttendees.setDemoDate(demoClassSchedule.getCreatedDateTime());
				demoClassAttendees.setDemoId(demoClassSchedule);
				demoClassAttendees.setDemoStatus(dStatus);
				demoClassAttendees.setHostId(userMaster);
				UserMaster invitee = userMasterDAO.findById(arr[i], false, false);
				//invitee.setUserId(arr[i]);
				demoClassAttendees.setInviteeId(invitee);
				demoClassAttendeesDAO.makePersistent(demoClassAttendees);
			}

			//Mailing start to new candidates
			List<DemoClassAttendees> demoClassAttendeesList=demoClassAttendeesDAO.getDemoClassAttendees(demoClassSchedule);
			for (DemoClassAttendees demoClassAttendees : demoClassAttendeesList) {
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				mailContent = MailText.getNewAttendeeMailText(request,demoClassSchedule,demoClassAttendees,demoClassAttendeesList,true);

				emailto = demoClassAttendees.getInviteeId().getEmailAddress();
				List<UserEmailNotifications> userEmailNotifications = userEmailNotificationsDAO.getUserEmailNotifications(demoClassAttendees.getInviteeId(), new String[]{"sch"},"dn");
				//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>> userEmailNotifications.size(): "+userEmailNotifications.size());
				if(userEmailNotifications.size()>0)
				{
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(emailto);
					dsmt.setMailsubject("Demo Lesson Scheduled");
					dsmt.setMailcontent(mailContent);
					dsmt.start();
				}
				if(isDistributionMail)
				{
					dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(distributionEmail);
					dsmt.setMailsubject("Demo Lesson Scheduled");
					dsmt.setMailcontent(mailContent);
					dsmt.start();
				}
			}
		}else if(arr.length>0 && demoId>0)
		{
			List<DemoClassAttendees> demoClassAttendeesList=demoClassAttendeesDAO.getDemoClassAttendees(demoClassSchedule);
			Map<Integer,DemoClassAttendees> attendeeMap = new HashMap<Integer, DemoClassAttendees>();
			for (DemoClassAttendees demoClassAttendees : demoClassAttendeesList) {
				attendeeMap.put(demoClassAttendees.getInviteeId().getUserId(), demoClassAttendees);
			}
			DemoClassAttendees demoClassAttendees1 = null;
			DemoClassAttendees demoClassAttendees = null;
			ArrayList<Integer> matchedAttendeeList = new ArrayList<Integer>();
			List<DemoClassAttendees> newInviteeList = new ArrayList<DemoClassAttendees>();
			List<DemoClassAttendees> rescheduleInviteeList = new ArrayList<DemoClassAttendees>();

			for(int i=0;i<arr.length;i++)
			{	
				demoClassAttendees1 = attendeeMap.get(arr[i]);

				if(demoClassAttendees1!=null)
				{
					demoClassAttendees = demoClassAttendees1;
					matchedAttendeeList.add(arr[i]);
					demoClassAttendeesDAO.makePersistent(demoClassAttendees);
					if(demochangedcnt>0)
					{
						rescheduleInviteeList.add(demoClassAttendees);
					}

				}else
				{
					demoClassAttendees = new DemoClassAttendees();
					demoClassAttendees.setDemoDate(demoClassSchedule.getCreatedDateTime());
					demoClassAttendees.setDemoId(demoClassSchedule);
					demoClassAttendees.setDemoStatus(dStatus);

					demoClassAttendees.setHostId(userMaster);
					UserMaster invitee = userMasterDAO.findById(arr[i], false, false);

					//invitee.setUserId(arr[i]);
					demoClassAttendees.setInviteeId(invitee);
					demoClassAttendeesDAO.makePersistent(demoClassAttendees);
					newInviteeList.add(demoClassAttendees);
				}

			}


			Set<Integer> removeAttendeeSet = attendeeMap.keySet();
			removeAttendeeSet.removeAll(matchedAttendeeList);
			System.out.println("LLLLLLLLLLLLLLLLLLLLLL:: "+removeAttendeeSet.size());

			/// Removed Attendees
			if(removeAttendeeSet.size()>0)
			{
				for (Integer integer : removeAttendeeSet) {
					demoClassAttendees1 = attendeeMap.get(integer);
					demoClassAttendeesList.remove(demoClassAttendees1);

					demoClassAttendeesDAO.makeTransient(demoClassAttendees1);
				}

				//Now removed mail
				for (Integer integer : removeAttendeeSet) {
					demoClassAttendees1 = attendeeMap.get(integer);
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					mailContent = MailText.getRemovedAttendeeMailText(request,demoClassSchedule,demoClassAttendees1,demoClassAttendeesList);
					emailto = demoClassAttendees1.getInviteeId().getEmailAddress();
					List<UserEmailNotifications> userEmailNotifications = userEmailNotificationsDAO.getUserEmailNotifications(demoClassAttendees1.getInviteeId(), new String[]{"doth"},"dn");
					//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>> userEmailNotifications.size(): "+userEmailNotifications.size());
					if(userEmailNotifications.size()>0)
					{
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("vishwanath@netsutra.com");
						dsmt.setMailto(emailto);
						dsmt.setMailsubject("Demo Lesson Invite Cancelled");
						dsmt.setMailcontent(mailContent);
						dsmt.start();
					}
					if(isDistributionMail)
					{
						dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("vishwanath@netsutra.com");
						dsmt.setMailto(distributionEmail);
						dsmt.setMailsubject("Demo Lesson Invite Cancelled");
						dsmt.setMailcontent(mailContent);
						dsmt.start();
					}
				}

			}



			demoClassAttendeesList=demoClassAttendeesDAO.getDemoClassAttendees(demoClassSchedule);
			//new Invitee
			for (DemoClassAttendees demoClassAttendeesnew : newInviteeList) {
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				mailContent = MailText.getNewAttendeeMailText(request,demoClassSchedule,demoClassAttendeesnew,demoClassAttendeesList,false);
				emailto = demoClassAttendeesnew.getInviteeId().getEmailAddress();
				List<UserEmailNotifications> userEmailNotifications = userEmailNotificationsDAO.getUserEmailNotifications(demoClassAttendeesnew.getInviteeId(), new String[]{"sch"},"dn");
				//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>> userEmailNotifications.size(): "+userEmailNotifications.size());
				if(userEmailNotifications.size()>0)
				{
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(emailto);
					dsmt.setMailsubject("Demo Lesson Invite");
					dsmt.setMailcontent(mailContent);
					dsmt.start();
				}
				if(isDistributionMail)
				{
					dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(distributionEmail);
					dsmt.setMailsubject("Demo Lesson Invite");
					dsmt.setMailcontent(mailContent);
					dsmt.start();
				}
			}

			// Reschedule
			for (DemoClassAttendees demoClassAttendeesResch : rescheduleInviteeList) {
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				mailContent = MailText.getExistingAttendeeMailText(request,demoClassSchedule,demoClassAttendeesResch,demoClassAttendeesList);
				emailto = demoClassAttendeesResch.getInviteeId().getEmailAddress();
				List<UserEmailNotifications> userEmailNotifications = userEmailNotificationsDAO.getUserEmailNotifications(demoClassAttendeesResch.getInviteeId(), new String[]{"doth"},"dn");
				//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>> userEmailNotifications.size(): "+userEmailNotifications.size());
				if(userEmailNotifications.size()>0)
				{
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(emailto);
					dsmt.setMailsubject("Changes to Scheduled Demo Lesson");
					dsmt.setMailcontent(mailContent);
					dsmt.start();
				}
				if(isDistributionMail)
				{
					dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(distributionEmail);
					dsmt.setMailsubject("Changes to Scheduled Demo Lesson");
					dsmt.setMailcontent(mailContent);
					dsmt.start();
				}
			}

		}
		System.out.println("demo scheduled");
	}

	/*======================save saveDemoSchedule ===========================*/	
	public String getUsersBySchool(Long schoolId,boolean isPanel){
		StringBuffer sb=new StringBuffer();
		SchoolMaster schoolMaster=schoolMasterDAO.findById(schoolId, false, false);
		List<UserMaster> userMasters=new ArrayList<UserMaster>();

		if(isPanel)
		{
			userMasters=userMasterDAO.getUserBySchoolWithoutAnalist(schoolMaster);
			sb.append("<option>Select Panelist</option>");
		}
		else
		{	userMasters=userMasterDAO.getUserBySchool(schoolMaster);
			sb.append("<option>Select Attendee</option>");
		}

		for(UserMaster master: userMasters){
			sb.append("<option value="+master.getUserId()+">"+master.getFirstName()+" "+master.getLastName()+"</option>");
		}
		return sb.toString();
	}
	public String getUsersByDistrict(Long districtId,boolean isPanel){
		StringBuffer sb=new StringBuffer();

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		List<UserMaster> userMasters=new ArrayList<UserMaster>();
		if(isPanel)
		{
			userMasters=userMasterDAO.getUserByDistrictWithoutAnalist(districtMaster);
			sb.append("<option>Select Panelist</option>");
		}
		else
		{
			userMasters=userMasterDAO.getUserByDistrict(districtMaster);
			sb.append("<option>Select Attendee</option>");
		}

		for(UserMaster master: userMasters){
			sb.append("<option value="+master.getUserId()+">"+master.getFirstName()+" "+master.getLastName()+"</option>");
		}
		return sb.toString();
	}
	public String cancelEvent(Integer demoId){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		System.out.println("demoId: lllllllllll "+demoId);
		DemoClassSchedule demoClassSchedule = null;
		if(demoId!=0)
		{
			demoClassSchedule = demoClassScheduleDAO.findById(demoId, false, false);
			demoClassSchedule.setDemoStatus("Cancelled");
			demoClassScheduleDAO.makePersistent(demoClassSchedule);

			List<DemoClassAttendees> demoClassAttendeesList=demoClassAttendeesDAO.getDemoClassAttendees(demoClassSchedule);
			String distributionEmail=districtMasterDAO.getDistributionEmail(districtMaster);
			demoClassSchedule = demoClassScheduleDAO.findById(demoClassSchedule.getDemoId(), false, false);
			//distributionEmail = "vishwanath@netsutra.com";

			boolean isDistributionMail = true;
			if(distributionEmail==null || distributionEmail.equals(""))
				isDistributionMail = false;

			String emailto = "vishwanath@netsutra.com";
			String mailContent = "";
			for (DemoClassAttendees demoClassAttendees : demoClassAttendeesList) {

				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				mailContent = MailText.getCancelledEvntMailText(request,demoClassSchedule,demoClassAttendees,demoClassAttendeesList);
				emailto = demoClassAttendees.getInviteeId().getEmailAddress();
				List<UserEmailNotifications> userEmailNotifications = userEmailNotificationsDAO.getUserEmailNotifications(demoClassAttendees.getInviteeId(), new String[]{"can"},"dn");
				//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>> userEmailNotifications.size(): "+userEmailNotifications.size());
				if(userEmailNotifications.size()>0)
				{
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(emailto);
					dsmt.setMailsubject("Demo Lesson Cancelled");
					dsmt.setMailcontent(mailContent);
					dsmt.start();
				}
				if(isDistributionMail)
				{
					dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(distributionEmail);
					dsmt.setMailsubject("Demo Lesson Cancelled");
					dsmt.setMailcontent(mailContent);
					dsmt.start();
				}
			}

			return "1";
		}

		return "0";
	}
	/*==============================show all attendee ==============================================*/
	public JSONObject displayPannel(TeacherDetail teacherDetail,JobOrder jobOrder,boolean isDemo,Integer jobPanelStatusId)
	{
		/* ========  For Session time Out Error =========*/
		System.out.println("displayPannel.............................");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		JSONObject jsonOutput = new JSONObject();
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}
		System.out.println("inside displayPanelAttendee");
		StringBuffer dmRecords = new StringBuffer();
		try{

			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
			JobWisePanelStatus jobWisePanelStatus = new JobWisePanelStatus();
			jobWisePanelStatus.setJobPanelStatusId(jobPanelStatusId);
			//List<PanelSchedule> panelScheduleList = panelScheduleDAO.findPanelSchedulesByTeacherAndJob(teacherDetail,jobOrder,districtMaster,schoolMaster);
			List<PanelSchedule> panelScheduleList = panelScheduleDAO.findPanelSchedulesByTeacherJobAndStatus(teacherDetail,jobOrder,districtMaster,schoolMaster,jobWisePanelStatus);
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+panelScheduleList.size());
			PanelSchedule panelSchedule = null;

			if(panelScheduleList.size()>0)
			{
				panelSchedule = panelScheduleList.get(0);

				jsonOutput.put("panelId", 	panelSchedule.getPanelId());
				jsonOutput.put("districtId", panelSchedule.getDistrictId());
				if(panelSchedule.getSchoolId()==null)
					jsonOutput.put("schoolId", 0);
				else
					jsonOutput.put("schoolId", panelSchedule.getSchoolId().getSchoolId());
				
				jsonOutput.put("panelDate", Utility.convertDateAndTimeToDatabaseformatOnlyDate(panelSchedule.getPanelDate()));
				jsonOutput.put("panelTime", panelSchedule.getPanelTime());
				jsonOutput.put("timeFormat", panelSchedule.getTimeFormat());
				jsonOutput.put("timeZoneId", panelSchedule.getTimeZoneMaster().getTimeZoneId());
				jsonOutput.put("panelAddress", panelSchedule.getPanelAddress());
				jsonOutput.put("panelDescription", panelSchedule.getPanelDescription());
				jsonOutput.put("panelStatus", panelSchedule.getPanelStatus());
				JSONArray dataArray = new JSONArray();

				List<PanelAttendees> panelAttendees=panelAttendeesDAO.getPanelAttendees(panelSchedule);
				if(panelAttendees.size()<1){
					dmRecords.append("<div class='span3'>No Panelist Found</div>");
				}else {
					for(PanelAttendees panel : panelAttendees){
						UserMaster inviteeId = panel.getPanelInviteeId();
						dmRecords.append("<div id='P"+inviteeId.getUserId()+"' class='span3'>"+
								""+inviteeId.getFirstName()+" "+inviteeId.getLastName()
								+"&nbsp;<a href='javascript:void(0);' onclick='deletePanelist(\""+inviteeId.getUserId()+"\");'><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>");

						dataArray.add(panel.getPanelInviteeId().getUserId());
					}
				}
				jsonOutput.put("panelAttendeeList", dataArray);
				jsonOutput.put("panelAttendees", dmRecords.toString());
				/*Boolean demoNoteStatus=demoClassNotesDetailsDAO.getDemoClassNotesByDemoSchedule(demoClassSchedule);
				jsonOutput.put("demoNoteStatus", demoNoteStatus);*/

			}

		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		//return dmRecords.toString();
		//System.out.println(jsonOutput.toString());
		return jsonOutput;
	}

	/*======================save savePanel ===========================*/ 
	public void savePanel(String panelDateVal,String panelTime,String meridiem,Integer	timezoneId,
			String location,String disSchCmnt,Integer teacherId,Integer jobId,boolean panelStatus,Integer[] arr,Integer panelId,Integer jobPanelStatusId){

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
		}

		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
		Date panelDate = null;
		try {

			panelDate = formatter.parse(panelDateVal);
			System.out.println(panelDate);
			System.out.println(formatter.format(panelDate));

		} catch (Exception e) {
			e.printStackTrace();
		}

		String mailContent = "";
		int panelchangedcnt = 0;
		PanelSchedule panelSchedule=new PanelSchedule();
		if(panelId!=0)
		{
			panelSchedule = panelScheduleDAO.findById(panelId, false, false);

			panelSchedule.setPanelAddress(location);
			if(!panelSchedule.getPanelDate().equals(panelDate))
				panelchangedcnt++;
			if(!panelSchedule.getPanelTime().equals(panelTime))
				panelchangedcnt++;
			if(!panelSchedule.getTimeFormat().equals(meridiem))
				panelchangedcnt++;
			if(!panelSchedule.getTimeZoneMaster().getTimeZoneId().equals(timezoneId))
				panelchangedcnt++;
			if(!panelSchedule.getPanelAddress().equals(location))
				panelchangedcnt++;
		}else
		{
			panelSchedule.setDistrictId(districtMaster.getDistrictId());
			panelSchedule.setSchoolId(schoolMaster);

			TeacherDetail teacherDetail =new TeacherDetail();
			teacherDetail.setTeacherId(teacherId);
			JobOrder jobOrder=new JobOrder();
			jobOrder.setJobId(jobId);
			panelSchedule.setTeacherDetail(teacherDetail);
			panelSchedule.setJobOrder(jobOrder);
			panelSchedule.setCreatedDateTime(new Date());
		}

		TimeZoneMaster timeZoneMaster= new TimeZoneMaster();
		timeZoneMaster.setTimeZoneId(timezoneId);

		String dStatus=null;
		if(panelStatus){
			dStatus="Completed";
		}else{
			dStatus="Scheduled";
		}
		System.out.println("panelDate::::::::::::::::::::::::::::::::::::::: "+panelDate);
		panelSchedule.setPanelDate(panelDate);
		panelSchedule.setPanelTime(panelTime);
		panelSchedule.setTimeFormat(meridiem);
		panelSchedule.setTimeZoneMaster(timeZoneMaster);
		panelSchedule.setPanelAddress(location);
		panelSchedule.setPanelDescription(disSchCmnt);
		panelSchedule.setCreatedBy(userMaster);
		panelSchedule.setPanelStatus(dStatus);
		JobWisePanelStatus jobWisePanelStatus = new JobWisePanelStatus();
		jobWisePanelStatus.setJobPanelStatusId(jobPanelStatusId);
		panelSchedule.setJobWisePanelStatus(jobWisePanelStatus);
		
		panelScheduleDAO.makePersistent(panelSchedule);

		String distributionEmail=districtMasterDAO.getDistributionEmail(districtMaster);
		panelSchedule = panelScheduleDAO.findById(panelSchedule.getPanelId(), false, false);
		//distributionEmail = "vishwanath@netsutra.com";
		boolean isDistributionMail = true;
		if(distributionEmail==null || distributionEmail.equals(""))
			isDistributionMail = false;
		String baseURL=Utility.getBaseURL(request);
		String emailto = "vishwanath@netsutra.com";
		if(arr.length>0 && panelId==0)
		{
			for(int i=0;i<arr.length;i++)
			{	
				PanelAttendees paneAttendees = new PanelAttendees();
				paneAttendees.setPanelDate(panelSchedule.getCreatedDateTime());
				paneAttendees.setPanelSchedule(panelSchedule);
				paneAttendees.setPanelStatus(dStatus);
				paneAttendees.setHostId(userMaster);
				UserMaster invitee = userMasterDAO.findById(arr[i], false, false);
				paneAttendees.setPanelInviteeId(invitee);
				panelAttendeesDAO.makePersistent(paneAttendees);
			}
			boolean mailSend=true;
			try{
				if(districtMaster.getDistrictId()==1200390){
					mailSend=false;
				}
			}catch(Exception e){}
			if(mailSend){
				//Mailing start to new candidates
				List<PanelAttendees> paneAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
				for (PanelAttendees paneAttendees : paneAttendeesList) {
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					String forMated = null;
					if(paneAttendees.getPanelInviteeId().getIsExternal()!=null && paneAttendees.getPanelInviteeId().getIsExternal()==true)
					{
						//send a link
						String pnlId=Utility.encryptNo(panelSchedule.getPanelId());
						String usrId=Utility.encryptNo(paneAttendees.getPanelInviteeId().getUserId());
						String linkIds= pnlId+"###"+usrId;
						try {
							forMated = Utility.encodeInBase64(linkIds);
							forMated = baseURL+"panelscore.do?id="+forMated;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					mailContent = MailText.getNewPanelAttendeeMailText(request,panelSchedule,paneAttendees,paneAttendeesList,true,forMated);
					emailto = paneAttendees.getPanelInviteeId().getEmailAddress();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(emailto);
					dsmt.setMailsubject("Panel Scheduled");
					dsmt.setMailcontent(mailContent);
					dsmt.start();
					if(isDistributionMail)
					{
						dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("vishwanath@netsutra.com");
						dsmt.setMailto(distributionEmail);
						dsmt.setMailsubject("Panel Scheduled");
						dsmt.setMailcontent(mailContent);
						dsmt.start();
					}
				}
			}
		}else if(arr.length>0 && panelId>0)
		{
			List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
			Map<Integer,PanelAttendees> attendeeMap = new HashMap<Integer, PanelAttendees>();
			for (PanelAttendees panelAttendees : panelAttendeesList) {
				attendeeMap.put(panelAttendees.getPanelInviteeId().getUserId(), panelAttendees);
			}
			PanelAttendees paneAttendees1 = null;
			PanelAttendees paneAttendees = null;
			ArrayList<Integer> matchedAttendeeList = new ArrayList<Integer>();
			List<PanelAttendees> newInviteeList = new ArrayList<PanelAttendees>();
			List<PanelAttendees> rescheduleInviteeList = new ArrayList<PanelAttendees>();

			for(int i=0;i<arr.length;i++)
			{	
				paneAttendees1 = attendeeMap.get(arr[i]);

				if(paneAttendees1!=null)
				{
					paneAttendees = paneAttendees1;
					matchedAttendeeList.add(arr[i]);
					panelAttendeesDAO.makePersistent(paneAttendees);
					if(panelchangedcnt>0)
					{
						rescheduleInviteeList.add(paneAttendees);
					}

				}else
				{
					paneAttendees = new PanelAttendees();
					paneAttendees.setPanelDate(panelSchedule.getCreatedDateTime());
					paneAttendees.setPanelSchedule(panelSchedule);
					paneAttendees.setPanelStatus(dStatus);

					paneAttendees.setHostId(userMaster);
					UserMaster invitee = userMasterDAO.findById(arr[i], false, false);

					paneAttendees.setPanelInviteeId(invitee);
					panelAttendeesDAO.makePersistent(paneAttendees);
					newInviteeList.add(paneAttendees);
				}
			}


			Set<Integer> removeAttendeeSet = attendeeMap.keySet();
			removeAttendeeSet.removeAll(matchedAttendeeList);
			/// Removed Attendees
			if(removeAttendeeSet.size()>0)
			{
				for (Integer integer : removeAttendeeSet) {
					paneAttendees1 = attendeeMap.get(integer);
					panelAttendeesList.remove(paneAttendees1);

					panelAttendeesDAO.makeTransient(paneAttendees1);
				}
				boolean mailSend=true;
				try{
					if(districtMaster.getDistrictId()==1200390){
						mailSend=false;
					}
				}catch(Exception e){}
				if(mailSend){
					//Now removed mail
					for (Integer integer : removeAttendeeSet) {
						paneAttendees1 = attendeeMap.get(integer);
						DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
						mailContent = MailText.getRemovedPanelAttendeeMailText(request,panelSchedule,paneAttendees1,panelAttendeesList);
						emailto = paneAttendees1.getPanelInviteeId().getEmailAddress();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("vishwanath@netsutra.com");
						dsmt.setMailto(emailto);
						dsmt.setMailsubject("Panel Invite Cancelled");
						dsmt.setMailcontent(mailContent);
						dsmt.start();
						if(isDistributionMail)
						{
							dsmt = new DemoScheduleMailThread();
							dsmt.setEmailerService(emailerService);
							dsmt.setMailfrom("vishwanath@netsutra.com");
							dsmt.setMailto(distributionEmail);
							dsmt.setMailsubject("Panel Invite Cancelled");
							dsmt.setMailcontent(mailContent);
							dsmt.start();
						}
					}
				}

			}

			panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
			boolean mailSend=true;
			try{
				if(districtMaster.getDistrictId()==1200390){
					mailSend=false;
				}
			}catch(Exception e){}
			if(mailSend){
				//new Invitee
				for (PanelAttendees paneAttendeesnew : newInviteeList) {
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					String forMated = null;
					if(paneAttendeesnew.getPanelInviteeId().getIsExternal()!=null && paneAttendeesnew.getPanelInviteeId().getIsExternal()==true)
					{
						//send a link
						String pnlId=Utility.encryptNo(panelSchedule.getPanelId());
						String usrId=Utility.encryptNo(paneAttendeesnew.getPanelInviteeId().getUserId());
						String linkIds= pnlId+"###"+usrId;
						try {
							forMated = Utility.encodeInBase64(linkIds);
							forMated = baseURL+"panelscore.do?id="+forMated;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					mailContent = MailText.getNewPanelAttendeeMailText(request,panelSchedule,paneAttendeesnew,panelAttendeesList,false,forMated);
					emailto = paneAttendeesnew.getPanelInviteeId().getEmailAddress();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(emailto);
					dsmt.setMailsubject("Panel Invite");
					dsmt.setMailcontent(mailContent);
					dsmt.start();
					if(isDistributionMail)
					{
						dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("vishwanath@netsutra.com");
						dsmt.setMailto(distributionEmail);
						dsmt.setMailsubject("Panel Invite");
						dsmt.setMailcontent(mailContent);
						dsmt.start();
					}
				}
	
				// Reschedule
				for (PanelAttendees panelAttendeesResch : rescheduleInviteeList) {
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					mailContent = MailText.getExistingPanelAttendeeMailText(request,panelSchedule,panelAttendeesResch,panelAttendeesList);
					emailto = panelAttendeesResch.getPanelInviteeId().getEmailAddress();
					System.out.println("emailto: "+emailto);
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("vishwanath@netsutra.com");
						dsmt.setMailto(emailto);
						dsmt.setMailsubject("Changes to Scheduled Panel");
						dsmt.setMailcontent(mailContent);
						dsmt.start();
					if(isDistributionMail)
					{
						dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("vishwanath@netsutra.com");
						dsmt.setMailto(distributionEmail);
						dsmt.setMailsubject("Changes to Scheduled Panel");
						dsmt.setMailcontent(mailContent);
						dsmt.start();
					}
				}
			}
		}
		System.out.println("panel scheduled");
	}
	public String cancelPanelEvent(Integer panelId){
		System.out.println("::::::::::::cancelPanelEvent:::::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		System.out.println("panelId: lllllllllll "+panelId);
		PanelSchedule panelSchedule = null;
		if(panelId!=0)
		{
			panelSchedule = panelScheduleDAO.findById(panelId, false, false);
			panelSchedule.setPanelStatus("Cancelled");
			panelScheduleDAO.makePersistent(panelSchedule);

			List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
			String distributionEmail=districtMasterDAO.getDistributionEmail(districtMaster);
			panelSchedule = panelScheduleDAO.findById(panelSchedule.getPanelId(), false, false);
			
			boolean mailSend=true;
			try{
				if(districtMaster.getDistrictId()==1200390){
					mailSend=false;
				}
			}catch(Exception e){}

			if(mailSend){
				boolean isDistributionMail = true;
				if(distributionEmail==null || distributionEmail.equals(""))
					isDistributionMail = false;
				
				String emailto = "vishwanath@netsutra.com";
				String mailContent = "";
				for (PanelAttendees panelAttendees : panelAttendeesList) {
	
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					mailContent = MailText.getCancelledPanelEvntMailText(request,panelSchedule,panelAttendees,panelAttendeesList);
					emailto = panelAttendees.getPanelInviteeId().getEmailAddress();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(emailto);
					dsmt.setMailsubject("Panel Cancelled");
					dsmt.setMailcontent(mailContent);
					dsmt.start();
					if(isDistributionMail)
					{
						dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("vishwanath@netsutra.com");
						dsmt.setMailto(distributionEmail);
						dsmt.setMailsubject("Panel Cancelled");
						dsmt.setMailcontent(mailContent);
						dsmt.start();
					}
				}
			}
			return "1";
		}

		return "0";
	}
	public String showPanels(Long districtId){
		StringBuffer sb=new StringBuffer();

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		List<DistrictPanelMaster> districtPanelMasters =districtPanelMasterDAO.getPanelsByDistrict(districtMaster);
		sb.append("<option>Select Panel</option>");

		for(DistrictPanelMaster master: districtPanelMasters){
			sb.append("<option value="+master.getPanelId()+">"+master.getPanelName()+"</option>");
		}
		return sb.toString();
	}

	public String viewPanelMembers(Integer panelsId){
		StringBuffer sb=new StringBuffer();

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictMaster districtMaster=null;
		UserMaster userMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");

		}
		DistrictPanelMaster districtPanelMaster = new DistrictPanelMaster();
		districtPanelMaster.setPanelId(panelsId);
		List<PanelMembers> panelMembers =panelMembersDAO.getPanelMembersByDistrictPanelMaster(districtPanelMaster);
		if(panelMembers.size()>0)
		{
			for(PanelMembers master: panelMembers){
				sb.append("<div class='row col-sm-4 col-md-4'><div class='checkbox inline'>"+
						"<input type='checkbox' class='case' name='case' checked value='"+master.getUserMaster().getUserId()+"'>&nbsp;<span id='u"+master.getUserMaster().getUserId()+"'>"+master.getUserMaster().getFirstName()+" "+master.getUserMaster().getLastName()
						+"</div></div>");
			}
			if(panelMembers.size()%3==1)
				sb.append("<div class='checkbox inline'>&nbsp;</div>");
			
			
			sb.append("<div class='row col-sm-4 col-md-4' style='padding-top:10px;'><a href='javascript:void(0);' onclick='checkAll(true);'>Check&nbsp;All</a>&nbsp;|&nbsp;<a href='javascript:void(0);' onclick='checkAll(false);'>Uncheck&nbsp;All</a></div>");
		}else
			sb.append("0");
		
		return sb.toString();
	}
	
	public String[] addNewPanelMember(String firstName,String lastName,String email)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityID=0,roleId=0;
		String[] dataArray = new String[6];
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}
		
		List<UserMaster> userMasters = userMasterDAO.checkUserEmail(email);
		if(userMasters!=null && userMasters.size()>0)
		{
			System.out.println("ddfdf"+userMasters.size());
			UserMaster uMaster = userMasters.get(0);
			dataArray[0]=""+uMaster.getUserId();
			dataArray[1]=""+uMaster.getFirstName();
			dataArray[2]=""+uMaster.getLastName();
			dataArray[3]=""+uMaster.getEmailAddress();
			
			uMaster.setIsQuestCandidate(false);
			uMaster.setIsExternal(true);
			userMasterDAO.makePersistent(uMaster);
		}else
		{
			try {
				UserMaster uMaster = new UserMaster();
				uMaster.setFirstName(firstName);
				uMaster.setLastName(lastName);
				uMaster.setEmailAddress(email);
				uMaster.setDistrictId(districtMaster);
				Utility utility= new Utility();
				String authenticationCode =	utility.getUniqueCodebyId((Integer.parseInt("1000")+1));
				int verificationCode=(int) Math.round(Math.random() * 2000000);
				
				uMaster.setAuthenticationCode(authenticationCode);
				uMaster.setSalutation(null);
				uMaster.setStatus("I");
				uMaster.setVerificationCode(""+verificationCode);
				uMaster.setIsExternal(true);
				try {
					uMaster.setPassword(MD5Encryption.toMD5("teachermatch"));
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				
				uMaster.setEntityType(2);
				RoleMaster roleMaster = new RoleMaster();
				roleMaster.setRoleId(5);
				uMaster.setRoleId(roleMaster);
				
				uMaster.setForgetCounter(0);
				uMaster.setCreatedDateTime(new Date());
				uMaster.setIsQuestCandidate(false);
				uMaster.setPhotoPathFile(null); 
				uMaster.setPhotoPath(null);
				uMaster.setIsQuestCandidate(false);
				userMasterDAO.makePersistent(uMaster);
				
				//emailerService.sendMailAsHTMLText(uMaster.getEmailAddress(), "I Have Added You As a User",MailText.createUserPwdMailToOtUser(request,uMaster));
				
				dataArray[0]=""+uMaster.getUserId();
				dataArray[1]=""+uMaster.getFirstName();
				dataArray[2]=""+uMaster.getLastName();
				dataArray[3]=""+uMaster.getEmailAddress();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return dataArray;
	}
	public String[] checkPanelStatus(Integer jobId){
		StringBuffer sb=new StringBuffer();

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");

		}
		return null;
	}
	
	
	public String[] getPanelByJobWiseStatus(Integer jobId)
	{
		System.out.println("::::::::::::::getPanelByJobWiseStatus::::::::::::::");
		StringBuffer sb=new StringBuffer();
		JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
		String[] data=new String[2];
		try{
			data[0]="1";
			List<JobWisePanelStatus> jobWisePanelStatusList = jobWisePanelStatusDAO.getPanelByJob(jobOrder);
			if(jobWisePanelStatusList.size()==1)
			{
				data[0]="0";
				data[1]=""+jobWisePanelStatusList.get(0).getJobPanelStatusId();
				return data;
			}
			
			if(jobWisePanelStatusList.size()>0)
			{
				sb.append("<option value='0'>Select Status</option>");
				for(JobWisePanelStatus master: jobWisePanelStatusList)
				{
					String status="";
					if(master.getStatusMaster()!=null)
						status=master.getStatusMaster().getStatus();
					else
						status=master.getSecondaryStatus().getSecondaryStatusName();
					
					sb.append("<option value="+master.getJobPanelStatusId()+">"+status+"</option>");
				}
				data[1]=sb.toString();
	
				return data;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return data;
		
	}
	
	public String[] addAllPanelMebersFromPanel(Integer panelsId){

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		DistrictPanelMaster districtPanelMaster = new DistrictPanelMaster();
		districtPanelMaster.setPanelId(panelsId);
		List<PanelMembers> panelMembers =panelMembersDAO.getPanelMembersByDistrictPanelMaster(districtPanelMaster);
		String[] members = new String[panelMembers.size()]; 
		int i=0;
		for(PanelMembers master: panelMembers){
			members[i]=master.getUserMaster().getUserId()+"#@"+master.getUserMaster().getFirstName()+" "+master.getUserMaster().getLastName();
			i++;
		}
		return members;
	}
	
	public String getDistrictSpecificPortfolioQuestions(Integer jobId,Integer teacherId)
	{
		System.out.println("*****:::::::::::::::||getDistrictSpecificPortfolioQuestions||:::::::::::::::::::;; ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
		}
		
		StringBuffer sb =new StringBuffer();
		try{
			TeacherDetail teacherDetail = null;
			DistrictMaster districtMaster = userMaster.getDistrictId();
			if(teacherId!=0){
				teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			}
			int jobCategoryId=0;
			JobOrder jobOrder=null;
			if(jobId!=0)
			{
				jobOrder=jobOrderDAO.findById(jobId, false, false);
			}
			
			boolean bPortfolioNewQuestion=false;
			List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsList=new ArrayList<DistrictSpecificPortfolioQuestions>();
			List<DistrictSpecificPortfolioAnswers> lastList = new ArrayList<DistrictSpecificPortfolioAnswers>();
			
			if(jobOrder!=null)
			{
				
				boolean SelfServicePortfolioStatus=false;
				if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getSelfServicePortfolioStatus()!=null && jobOrder.getDistrictMaster().getSelfServicePortfolioStatus())
					SelfServicePortfolioStatus=jobOrder.getDistrictMaster().getSelfServicePortfolioStatus();

				if(!SelfServicePortfolioStatus)
				{
					lastList = districtSpecificPortfolioAnswersDAO.findSpecificPortfolioAnswers(teacherDetail,jobOrder,0);
					if(lastList!=null && lastList.size()>0)
					{
						for(DistrictSpecificPortfolioAnswers answers :lastList)
						{
							districtSpecificPortfolioQuestionsList.add(answers.getDistrictSpecificPortfolioQuestions());
						}
						bPortfolioNewQuestion=true;
					}
					
				}

				///////////
			}
			int dspqType=0;
			if(!bPortfolioNewQuestion)
			{
				Map<Integer,List<DistrictSpecificPortfolioQuestions>> dSPQ = new HashMap<Integer, List<DistrictSpecificPortfolioQuestions>>();
				if(jobId!=0)
				{
					
					jobCategoryId=jobOrder.getJobCategoryMaster().getJobCategoryId();
					dSPQ=districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestion(jobOrder,false);
				}else if(districtMaster!=null){
					
					//getDistrictSpecificPortfolioBYDistrict
					dSPQ=districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioBYDistrict(districtMaster);
					
				}
					
					try{
						for(int i=0;i<5;i++){
							if(dSPQ.get(i)!=null){
								districtSpecificPortfolioQuestionsList=dSPQ.get(i);
								dspqType=i;
								break;
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
			}
			
				
				System.out.println("districtSpecificPortfolioQuestionsList:::::::::::"+districtSpecificPortfolioQuestionsList.size());
				int totalQuestions = districtSpecificPortfolioQuestionsList.size();
				int cnt = 0;
				if(districtSpecificPortfolioQuestionsList.size()>0){
					//System.out.println("dspqType:::::::::::"+dspqType);
					
					/*lastList= districtSpecificPortfolioAnswersDAO.findSpecificPortfolioAnswers(teacherDetail,jobOrder,dspqType);
					System.out.println("dspqType   "+dspqType);*/
					if(!bPortfolioNewQuestion)
					{
						if(dspqType==1 || dspqType==0){
							lastList= districtSpecificPortfolioAnswersDAO.findSpecificPortfolioAnswers(teacherDetail,jobOrder,dspqType);
						}else{				
							lastList= districtSpecificPortfolioAnswersDAO.findSpecificPortfolioAnswersOnProfile(teacherDetail,userMaster,dspqType);
						}
					}
					
					Map<Integer, DistrictSpecificPortfolioAnswers> map = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
	
					for(DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : lastList)
						map.put(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId(), districtSpecificPortfolioAnswers);
	
					DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers = null;
					List<DistrictSpecificPortfolioOptions> questionOptionsList = null;
					String questionInstruction = null;
					String shortName = "";
				
					String tbleDisabl="";
					
					if(districtMaster!=null && districtMaster.getDistrictId()!=7800038){
						tbleDisabl = "style='pointer-events: none;'";
					}
					String selectedOptions = "",insertedRanks="";
						for (DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions : districtSpecificPortfolioQuestionsList) 
						{
							shortName = districtSpecificPortfolioQuestions.getQuestionTypeMaster().getQuestionTypeShortName();
							sb.append("<input type='hidden' name='dspqType' id='dspqType' value='"+dspqType+"'  />");
							sb.append("<input type='hidden' name='jobCategoryIdForDSPQ'   id='jobCategoryIdForDSPQ' value='"+jobCategoryId+"'  />");
							sb.append("<tr>");
							sb.append("<td width='90%'>");
							sb.append("<div ><b>Question "+(++cnt)+" of "+totalQuestions+"</b>");
							if(districtSpecificPortfolioQuestions.getIsRequired()==1){
								sb.append("<span class=\"required\" >*</span>");
							}
							sb.append("</div>");
							sb.append("<input type='hidden' name='o_maxMarksS' value='"+(districtSpecificPortfolioQuestions.getMaxMarks()==null?0:districtSpecificPortfolioQuestions.getMaxMarks())+"'  />");
							//questionInstruction = districtSpecificPortfolioQuestions.getQuestionInstructions()==null?"":districtSpecificPortfolioQuestions.getQuestionInstructions();
							//if(!questionInstruction.equals(""))
								//sb.append(""+Utility.getUTFToHTML(questionInstruction)+"");
	
							sb.append("<div id='QS"+cnt+"question'>"+Utility.getUTFToHTML(districtSpecificPortfolioQuestions.getQuestion())+"</div>");
	
							questionOptionsList = districtSpecificPortfolioQuestions.getQuestionOptions();
							districtSpecificPortfolioAnswers = map.get(districtSpecificPortfolioQuestions.getQuestionId());
	
							List<DistrictSpecificPortfolioOptions> tempDspqOptionsList = new ArrayList<DistrictSpecificPortfolioOptions>(questionOptionsList);
							List<DistrictSpecificPortfolioOptions> optionsWithOrderList = new ArrayList<DistrictSpecificPortfolioOptions>();
							
							for(DistrictSpecificPortfolioOptions districtSpecificPortfolioOptions : questionOptionsList){
								if(districtSpecificPortfolioOptions!=null && districtSpecificPortfolioOptions.getOptionOrder()!=null && districtSpecificPortfolioOptions.getStatus()!=null && districtSpecificPortfolioOptions.getStatus().equalsIgnoreCase("A"))
									optionsWithOrderList.add(districtSpecificPortfolioOptions);
							}
							System.out.println(" "+districtSpecificPortfolioQuestions.getQuestionTypeMaster().getQuestionTypeShortName()+" tempDspqOptionsList.size() with questionOptionsList : "+tempDspqOptionsList.size());
							if(optionsWithOrderList!=null && optionsWithOrderList.size()>0){
								java.util.Collections.sort(optionsWithOrderList, new Comparator<DistrictSpecificPortfolioOptions>(){
									 public int compare(DistrictSpecificPortfolioOptions f1, DistrictSpecificPortfolioOptions f2){
										 if(f1!=null && f1.getOptionOrder()!=null && f2!=null && f2.getOptionOrder()!=null)
											 return f1.getOptionOrder().compareTo(f2.getOptionOrder());
										 else return 0;
									 }
								});
								tempDspqOptionsList = new ArrayList<DistrictSpecificPortfolioOptions>(optionsWithOrderList);
								System.out.println(" "+districtSpecificPortfolioQuestions.getQuestionTypeMaster().getQuestionTypeShortName()+" tempDspqOptionsList.size() with optionsWithOrderList : "+tempDspqOptionsList.size());
							}
							
							String checked = "";
							Integer optId = 0;
							String insertedText = "";
							int sliderScore=0;
							
							if(districtSpecificPortfolioAnswers!=null)
							{
								if(!shortName.equalsIgnoreCase("OSONP") && !shortName.equalsIgnoreCase("mloet") && !shortName.equalsIgnoreCase("DD") && !shortName.equalsIgnoreCase("mlsel") && !shortName.equalsIgnoreCase("rt") && !shortName.equalsIgnoreCase("sscb") && shortName.equalsIgnoreCase(districtSpecificPortfolioAnswers.getQuestionType()))
								{
									if(districtSpecificPortfolioAnswers.getSelectedOptions()!="" && districtSpecificPortfolioAnswers.getSelectedOptions()!=null)
										optId = Integer.parseInt(districtSpecificPortfolioAnswers.getSelectedOptions());
	
									insertedText = districtSpecificPortfolioAnswers.getInsertedText();
								}
								if(shortName.equalsIgnoreCase("mloet") || shortName.equalsIgnoreCase("OSONP") || shortName.equalsIgnoreCase("sscb"))
								{
									insertedText = districtSpecificPortfolioAnswers.getInsertedText();
								}
								
								if(districtSpecificPortfolioAnswers.getSliderScore()!=null && !districtSpecificPortfolioAnswers.getSliderScore().equals(0))
									sliderScore=districtSpecificPortfolioAnswers.getSliderScore();
							}
							if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel")|| shortName.equalsIgnoreCase("lkts"))
							{
								sb.append("<table "+tbleDisabl+">");
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									if(optId.equals(questionOptions.getOptionId()))
										checked = "checked";
									else
										checked = "";
	
									sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
											" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
											"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
									sb.append("</td></tr>");
								}
								sb.append("</table>");
							}
							if(shortName.equalsIgnoreCase("rt")){	
									if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getInsertedRanks()!=null){
										insertedRanks = districtSpecificPortfolioAnswers.getInsertedRanks()==null?"":districtSpecificPortfolioAnswers.getInsertedRanks();
									}
									String[] ranks = insertedRanks.split("\\|");
									int rank=1;
									int count=0;
									String ans = "";
									sb.append("<table "+tbleDisabl+">");
									Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
									for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
										optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
									}
									for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
										try{ans =ranks[count];}catch(Exception e){}
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnkS"+rank+"' name='rankS' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRankForPortfolio(this);' value='"+ans+"'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
										sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"' /><input type='hidden' name='optS' value='"+teacherQuestionOption.getOptionId()+"' />");
										sb.append("<input type='hidden' name='o_rankS' value='"+(teacherQuestionOption.getRank()==null?0:teacherQuestionOption.getRank())+"' />");
										sb.append("<script type=\"text/javascript\" language=\"javascript\">");
										//sb.append("document.getElementById('rnkS1').focus();");
										sb.append("</script></td></tr>");
										rank++;
										count++;
									}
									sb.append("</table >");
							}
							if(shortName.equalsIgnoreCase("mlsel")){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table "+tbleDisabl+">");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								String ansId="";
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											//System.out.println("ansId:::"+ansId);
											if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
												//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
												if(ansId!=null && !ansId.equals("")){
													ansId="checked";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									
									sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
									sb.append("</td></tr>");
									multiCount++;
									
								}
								sb.append("</table >");
							}
													
							if(shortName.equalsIgnoreCase("mloet")){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table "+tbleDisabl+">");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								String ansId="";
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : tempDspqOptionsList) {
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
												if(ansId!=null && !ansId.equals("")){
													ansId="checked";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									
									sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
									sb.append("</td></tr>");
									multiCount++;
									
								}
								sb.append("</table >");
								String questionInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								sb.append(questionInstructions+"</br><span style=''><textarea  name='QS"+cnt+"optmloet' id='QS"+cnt+"optmloet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('QS"+cnt+"optmloet').focus();");
								sb.append("</script>");
							}
							if(shortName.equalsIgnoreCase("sloet")){
								String textValue="";
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
									textValue=districtSpecificPortfolioAnswers.getInsertedText();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table "+tbleDisabl+">");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								String ansId="";
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : tempDspqOptionsList) {
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
												if(ansId!=null && !ansId.equals("")){
													ansId="checked";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									
									sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 0px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
									sb.append("</td></tr>");
									multiCount++;
									
								}
								sb.append("</table >");
								String questionInstructions="";
								try{
									if(districtSpecificPortfolioQuestions!=null){
										questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								sb.append(questionInstructions+"</br><span style=''><textarea  name='QS"+cnt+"optmloet' id='QS"+cnt+"optmloet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+textValue+"</textarea><span><br/>");
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('QS"+cnt+"optmloet').focus();");
								sb.append("</script>");
							}
							
							
							if(shortName.equalsIgnoreCase("it"))
							{
								sb.append("<table "+tbleDisabl+">");
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='optS' value='"+teacherQuestionOption.getOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherQuestionOption.getQuestionOption()+"'> ");
									sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
								}
								sb.append("</table>");
							}
							if(shortName.equalsIgnoreCase("et"))
							{
								sb.append("<table "+tbleDisabl+">");
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									if(optId.equals(questionOptions.getOptionId()))
										checked = "checked";
									else
										checked = "";
	
									sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
											" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
											"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
									sb.append("</td></tr>");
								}
								sb.append("</table>");
	
								String questionInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getQuestionInstructions()+"</br>";	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								sb.append(questionInstructions+"&nbsp;&nbsp;<span style=''><textarea  name='QS"+cnt+"optet' id='QS"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('QS"+cnt+"optet').focus();");
								sb.append("</script>");
							}
							if(shortName.equalsIgnoreCase("sl"))
							{
								//Years of certified teaching experience
								if(districtSpecificPortfolioQuestions.getQuestion().contains("Years of certified teaching experience")){									
									String uniqueChkId ="QS"+cnt+"opt";
									String checkedCheckBox="";
									String fieldDisable="";
									if(districtSpecificPortfolioAnswers==null){
										checkedCheckBox="";
										fieldDisable="";
									}else{
										if(insertedText.equalsIgnoreCase("0.0")){
											checkedCheckBox="checked";
											fieldDisable="disabled";
										}
									}
									
									String questionCustomInstructions="";
									try{
										if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
											questionCustomInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getQuestionInstructions();	
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									sb.append("&nbsp;&nbsp;<div class='row'><div class='col-sm-4 col-md-4'><input type='text' "+tbleDisabl+" name='QS"+cnt+"opt' "+fieldDisable+" onkeypress='return checkForDecimalTwo(event);' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' class='form-control'/></div>");
									sb.append("<div class='col-sm-4 col-md-4'><input type='checkbox' "+tbleDisabl+" id='QS"+cnt+"exp' "+checkedCheckBox+" style='margin-top: 10px;'>"+questionCustomInstructions+"</div></div><br>");
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
										sb.append("$('#QS"+cnt+"exp').change(function(){ $(this).each(function(){if($(this).prop('checked')){$('#"+uniqueChkId+"').val('0.0');$('#"+uniqueChkId+"').prop('disabled',true);}else{$('#"+uniqueChkId+"').val('');$('#"+uniqueChkId+"').prop('disabled',false);}});});");
									sb.append("</script>");
								}else{
									sb.append("&nbsp;&nbsp;<input type='text' "+tbleDisabl+" name='QS"+cnt+"opt' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' style='width:98%;margin:5px;'/><br/>");
								}								
							}else if(shortName.equalsIgnoreCase("ml"))
							{
								if(insertedText==null)
									insertedText="";
								sb.append("&nbsp;&nbsp;<textarea "+tbleDisabl+" name='QS"+cnt+"opt' id='QS"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");					
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('QS"+cnt+"opt').focus();");
								sb.append("</script>");
								if(districtSpecificPortfolioQuestions.getQuestionId().equals(8)){
									String dbScName= "";
									String dbScId="";
									try{
										if(districtSpecificPortfolioAnswers.getSchoolMaster()!=null && !districtSpecificPortfolioAnswers.getSchoolMaster().equals("")){
											dbScName=districtSpecificPortfolioAnswers.getSchoolMaster().getSchoolName();
											dbScId=Long.toString(districtSpecificPortfolioAnswers.getSchoolMaster().getSchoolId());
										}
									}catch (Exception e) {
										// TODO: handle exception
									}
									sb.append("<label>School Name</label><br><input type='text' value='"+dbScName+"' id='schoolName' maxlength='100' name='schoolName' class='form-control' placeholder='' onfocus=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onkeyup=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onblur=\"hideSchoolMasterDivQuestion(this,'QS"+cnt+"schoolId','divTxtShowData2');\" "+tbleDisabl+"  style='width: 50%;'/>	");
									sb.append("<input type='hidden' id='schoolName' name='schoolName'/><input type='hidden' id='QS"+cnt+"schoolId' class='school"+cnt+"' name='schoolId' value='"+dbScId+"'/>");
									sb.append("<div id='divTxtShowData2'  onmouseover=\"mouseOverChk('divTxtShowData2','schoolName')\" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>");
								}
							}
							if(shortName.equalsIgnoreCase("OSONP"))
							{
								String optionClass="OSONP"+cnt;
								sb.append("<table "+tbleDisabl+">");
								Integer p1=0;
								
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table "+tbleDisabl+">");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								String ansId="";
								boolean openOther=false;
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											if(ansId!=null && !ansId.equals("") && questionOptions.getOptionId()==Integer.parseInt(ansId)){
												if(ansId!=null && !ansId.equals("")){
													ansId="checked";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
																				
											if(questionOptions.getOpenText() && ansId.equalsIgnoreCase("checked")){
												sb.append("<script>onSelectOpenOptions('show','"+cnt+"');</script>");
											}
											
											String checkBox="";
											
											if(questionOptions.getOpenText()){
												checkBox="<input type='radio' "+tbleDisabl+" name='"+optionClass+"' class='"+optionClass+"' value='"+questionOptions.getOptionId()+"' "+ansId+" onclick=\"onSelectOpenOptions('show','"+cnt+"');\" />";	
											}else{
												checkBox="<input type='radio' "+tbleDisabl+" name='"+optionClass+"' class='"+optionClass+"' value='"+questionOptions.getOptionId()+"' "+ansId+" onclick=\"onSelectOpenOptions('hide','"+cnt+"');\"  />";
											}
											
											sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'>"+checkBox+"</div>" +
													" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
													"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
											sb.append("</td></tr>");
								}
								sb.append("</table>");
								String questionCustomInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionCustomInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
									sb.append("<span class='hide textareaOp"+cnt+"' style='margin-left: 10px;'>"+questionCustomInstructions+"<textarea  name='QS"+cnt+"OSONP' id='QS"+cnt+"OSONP' class='form-control "+cnt+"OSONPtext' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea></span>");
								
							}
							
							if(shortName.equalsIgnoreCase("DD") || (shortName.equalsIgnoreCase("drsls"))){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								String questionInstructions="&nbsp;";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionInstructions=districtSpecificPortfolioQuestions.getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								sb.append(questionInstructions.trim());
								sb.append("<table "+tbleDisabl+">");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
								String ansId="";
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><select name='dropdown"+cnt+"' id='dropdown"+cnt+"' class='form-control'><option value=''>Select</option>");
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											//System.out.println("ansId:::"+ansId);
											if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
												//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
												if(ansId!=null && !ansId.equals("")){
													ansId="selected";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									sb.append("<option "+ansId+" value='"+teacherQuestionOption.getOptionId()+"'>"+teacherQuestionOption.getQuestionOption()+"</option>");
									//sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
									
									multiCount++;
									
								}
								sb.append("</select></td></tr>");
								//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
								sb.append("</table><br>");
																
								
								
							}
							if(shortName.equalsIgnoreCase("sswc"))
							{
								sb.append("<table style='margin-bottom:10px' "+tbleDisabl+">");
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									if(optId.equals(questionOptions.getOptionId()))
										checked = "checked";
									else
										checked = "";
	
									sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
											" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
											"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
									sb.append("</td></tr>");
								}
								sb.append("</table>");
	
								String questionInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								sb.append(questionInstructions+"<br>");
								
								if(districtSpecificPortfolioQuestions.getQuestion().equalsIgnoreCase("Have you previously worked for UCSN?")){
									
									System.out.println("insertedText.trim()   "+insertedText.trim());
									String[] arr = new String[2]; 
									if(insertedText.equalsIgnoreCase("##")){
										arr[0]="";
										arr[1]="";
									}else if(insertedText.contains("##")){
										arr = insertedText.split("##");
									}else{
										arr[0]=insertedText;
										arr[1]="";
									}
									
									sb.append("<div class='row'><div class='col-sm-3 col-md-3'><input type='text' name='QS"+cnt+"optet' id='QS"+cnt+"optet1' class='form-control' value='"+arr[0].trim()+"'></div>");
									sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet1', 'QS"+cnt+"optet1', '%m-%d-%Y');</script>");									
									sb.append("<div class='col-sm-3 col-md-3'><input type='text'  name='QS"+cnt+"optet' id='QS"+cnt+"optet2' class='form-control' value='"+arr[1].trim()+"'></div>");
									sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet2', 'QS"+cnt+"optet2', '%m-%d-%Y');</script>");									
								}else{
									sb.append("<div class='row'><div class='col-sm-3 col-md-3'><input type='text' name='QS"+cnt+"optet' id='QS"+cnt+"optet' class='form-control' value='"+insertedText+"'></div>");
									sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet', 'QS"+cnt+"optet', '%m-%d-%Y');</script>");								
									
								}
																
								sb.append("</div>");
								sb.append("</br>");
							}
							
							if(shortName.equalsIgnoreCase("sscb")){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								String ansId="";
								int subsecCount=0; 
								
								String classSubSec="";
								for (DistrictSpecificPortfolioOptions teacherQuestionOptionHD : questionOptionsList) {
									if(teacherQuestionOptionHD.getSubSectionTitle()!=null && teacherQuestionOptionHD.getSubSectionTitle().equalsIgnoreCase("Y")){
										subsecCount++;
										classSubSec="sectionCnt"+subsecCount+"";
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;' class='secHeading' id='subSection"+cnt+"'>"+teacherQuestionOptionHD.getQuestionOption()+"");
										sb.append("</td></tr>");
									//}
									
									for (DistrictSpecificPortfolioOptions teacherQuestionOption : tempDspqOptionsList) {
											if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
											try{
												ansId="";
												for(int i=0;i<multiCounts.length;i++){
													try{ansId =multiCounts[i];}catch(Exception e){}
													//System.out.println("ansId:::"+ansId);
													if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
														//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
														if(ansId!=null && !ansId.equals("")){
															ansId="checked";
														}
														break;
													}
												}
											}catch(Exception e){
												e.printStackTrace();
											}
										
											if(teacherQuestionOption.getSubSectionTitle()!=null && teacherQuestionOption.getSubSectionTitle().equalsIgnoreCase(subsecCount+"")){
												/*subsecCount++;
												classSubSec="sectionCnt"+subsecCount+"";
												sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;' class='secHeading' id='subSection"+cnt+"'>"+teacherQuestionOption.getQuestionOption()+"");
												sb.append("</td></tr>");
											}else{*/
												sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='"+classSubSec+"' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'>&nbsp;&nbsp;&nbsp;"+teacherQuestionOption.getQuestionOption()+"<input type='hidden' id='QS"+teacherQuestionOption.getOptionId()+"validQuestion' value='"+teacherQuestionOption.getValidOption()+"'>");
												if(districtSpecificPortfolioQuestions.getDistrictMaster().getDistrictId().equals(7800040) && teacherQuestionOption.getQuestionOption().equalsIgnoreCase("College or University if selected, please choose the name of the school from the drop down"))
												{
													String dbUName= "";
													String dbUId="";
													try{
														if(districtSpecificPortfolioAnswers.getUniversityId()!=null && !districtSpecificPortfolioAnswers.getUniversityId().equals("")){
															dbUName=districtSpecificPortfolioAnswers.getUniversityId().getUniversityName();
															dbUId=districtSpecificPortfolioAnswers.getUniversityId().getUniversityId()+"";
														}
													}catch (Exception e) {
														// TODO: handle exception
													}											
													sb.append("<br><input type='text' value='"+dbUName+"' id='schoolName' maxlength='100' name='schoolName' class='form-control' placeholder='' onfocus=\"getUniversityAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onkeyup=\"getUniversityAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onblur=\"hideSchoolMasterDivQuestion(this,'QS"+cnt+"schoolId','divTxtShowData2');\" style='width:58%;display:inline;margin-left:5px;'/>");
													//sb.append("<label>School Name</label><br><input type='text' value='"+dbScName+"' id='schoolName' maxlength='100' name='schoolName' class='form-control' placeholder='' onfocus=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onkeyup=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onblur=\"hideSchoolMasterDivQuestion(this,'QS"+cnt+"schoolId','divTxtShowData2');\" style='width:58%;display:inline;'/>	");
													sb.append("<input type='hidden' id='schoolName' name='schoolName'/><input type='hidden' id='QS"+cnt+"schoolId' class='school"+cnt+"' name='schoolId' value='"+dbUId+"'/>");
													sb.append("<div id='divTxtShowData2'  onmouseover=\"mouseOverChk('divTxtShowData2','schoolName')\" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>");
													
												}
												if(districtSpecificPortfolioQuestions.getDistrictMaster().getDistrictId().equals(7800040) && teacherQuestionOption.getQuestionOption().contains("UNO Charter School Network Employee"))
												{
													sb.append("<input type='text'  name='QS"+cnt+"multiselectText' id='QS"+cnt+"multiselectText' class='form-control' style='width:35%;margin-left:5px;' value='"+insertedText+"'>");
												}
												//UNO Charter School Network Employee 
												sb.append("</td></tr>");
												multiCount++;										
											}		
										}
										
									}
								}
							}//main loop
								sb.append("</table >");
								
							}
							if(shortName.equalsIgnoreCase("UAT"))
							{
								//sb.append("&nbsp;&nbsp;<input type='file' name='QS"+cnt+"opt' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' style='width:98%;margin:5px;'/><br/>");
								
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								String questionInstructions="&nbsp;";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionInstructions=districtSpecificPortfolioQuestions.getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								//sb.append(questionInstructions.trim());
								
								if(questionOptionsList!=null && questionOptionsList.size()>0){
										sb.append("<table>");
										Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
										for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
											optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
										}
										//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
										String ansId="";
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><select "+tbleDisabl+" name='dropdown"+cnt+"' id='dropdown"+cnt+"' class='form-control'><option value=''>Select</option>");
										for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
											try{
												ansId="";
												for(int i=0;i<multiCounts.length;i++){
													try{ansId =multiCounts[i];}catch(Exception e){}
													//System.out.println("ansId:::"+ansId);
													if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
														//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
														if(ansId!=null && !ansId.equals("")){
															ansId="selected";
														}
														break;
													}
												}
											}catch(Exception e){
												e.printStackTrace();
											}
											sb.append("<option "+ansId+" value='"+teacherQuestionOption.getOptionId()+"'>"+teacherQuestionOption.getQuestionOption()+"</option>");
											//sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
											
											multiCount++;
											
										}
										sb.append("</select></td></tr>");
										//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
										sb.append("</table>");
								}	
								sb.append("<div id='answerDiv'>");								
									sb.append("<iframe id='uploadFrameAnswerId' name='uploadFrameAnswer' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
									sb.append("<form id='frmAnswerUpload' enctype='multipart/form-data' method='post' target='uploadFrameAnswer' action='answerUploadServlet.do' class='form-inline'>");
										sb.append("<div class='row'>");
											sb.append("<div class='col-sm-12 col-md-12'>");
												sb.append("<div class='divErrorMsg' id='errAnswer' style='display: block;'></div>");
											sb.append("</div>");
											
											sb.append("<div class='col-sm-12 col-md-12'>");
												if(questionInstructions!="" && !questionInstructions.equals("&nbsp;"))
													sb.append("<span>"+questionInstructions.trim()+"</span><br/>");
												sb.append("<div class='col-sm-6 col-md-6 top5'>");
													sb.append("<input "+tbleDisabl+" name='QS"+cnt+"File' id='QS"+cnt+"File' type='file'/>");
												sb.append("</div>");
												sb.append("<div class='col-sm-6 col-md-6 top5' id='divAnswerTxt'>");
													String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
													String fileName="";
													if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getFileName()!=null && !districtSpecificPortfolioAnswers.getFileName().equalsIgnoreCase(""))
													{
														fileName=districtSpecificPortfolioAnswers.getFileName();
														sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view Answer !' id='hrefAnswer' onclick=\"downloadAnswer(0,'"+districtSpecificPortfolioAnswers.getAnswerId()+"');"+windowFunc+"\">View</a>");
														sb.append("<script>$('#hrefAnswer').tooltip();</script>");
													}
												sb.append("</div>");

											sb.append("</div>");
											sb.append("<input type='hidden' name='answerFileName' id='answerFileName' value='"+fileName+"'>");
											sb.append("<input type='hidden' name='editCaseFile' id='editCaseFile' value='"+fileName+"'>");
											sb.append("<input type='hidden' name='answerFilePath' id='answerFilePath'>");
											sb.append("<input type='hidden' name='hdnQues' id='hdnQues' value='QS"+cnt+"File'>");										
										sb.append("</div>");
									sb.append("</form>");
								sb.append("</div>");
								if(districtSpecificPortfolioQuestions.getDistrictMaster().getDistrictId()==1302010 && districtSpecificPortfolioQuestions.getQuestion().equalsIgnoreCase("List sports clubs/activities you would be willing to coach/sponsor (attach coaching experience resume to profile)."))
								{
									sb.append("</br><span style=''><textarea  name='QS"+cnt+"Text' id='QS"+cnt+"Text' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
								}
							}

							//if(!jobId.equals(0)){								
								if(districtSpecificPortfolioQuestions.getMaxSliderScore()!=null && !districtSpecificPortfolioQuestions.getMaxSliderScore().equals(0)){										
									sb.append("<div><iframe id='ifrmQ"+cnt+"'  src='slideract.do?name=sliderQ"+cnt+"&tickInterval=1&max=20&swidth=500&svalue="+sliderScore+"' scrolling='no' frameBorder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:540px;'></iframe></div>");									
								}
							//}
							sb.append("<input type='hidden' id='dstQuesSlid"+cnt+"' value='"+sliderScore+"'>");
							sb.append("<input type='hidden' id='QS"+cnt+"isRequired' value='"+districtSpecificPortfolioQuestions.getIsRequired()+"'/>" );
							sb.append("<input type='hidden' id='QS"+cnt+"questionId' value='"+districtSpecificPortfolioQuestions.getQuestionId()+"'/>" );
							sb.append("<input type='hidden' id='QS"+cnt+"questionTypeId' value='"+districtSpecificPortfolioQuestions.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
							sb.append("<input type='hidden' id='QS"+cnt+"questionTypeShortName' value='"+shortName+"'/>" );
	
							sb.append("</td>");
							sb.append("</tr>");
							insertedText = "";
						}
				    }
				if(cnt > 0)
					sb.append("<input type='hidden' name='totalQuestionsList' id='totalQuestionsList' value='"+cnt+"'  />");
				//sb.append("@##@"+cnt+"@##@"+jobOrder.getDistrictMaster().getTextForDistrictSpecificQuestions());

			//////////////////////	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	
	}
	public String setDistrictQPortfoliouestions(DistrictSpecificPortfolioAnswers[] districtSpecificPortfolioAnswersList,int dspqType,int teacherId)
	{
		System.out.println(":::::::::::::::setDistrictQPortfoliouestions:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
		}

		TeacherDetail teacherDetail = null;

		if(teacherId!=0){
			teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
		}

		try {

			int inValidCount = 0;
			
			JobOrder jobOrder=null;
			
			if(dspqType!=2){
				
				jobOrder = districtSpecificPortfolioAnswersList[0].getJobOrder();
				jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
			}

			Map<Integer,Integer> map = new HashMap<Integer, Integer>();
			Map<String, String> questionOptions = new HashMap<String, String>();
			Map<String, TeacherSecondaryStatus> teacherTags = new HashMap<String, TeacherSecondaryStatus>();
			Map<Integer, DistrictSpecificPortfolioQuestions> questTags = new HashMap<Integer, DistrictSpecificPortfolioQuestions>();
			Map<Long, SchoolMaster> schoolMap = new HashMap<Long, SchoolMaster>();
			try {				
				List<SchoolMaster> schList = schoolMasterDAO.findDistrictSchoolId(userMaster.getDistrictId());
				for (SchoolMaster schoolMaster : schList) {
					schoolMap.put(schoolMaster.getSchoolId(), schoolMaster);
				}
				
				List<DistrictSpecificPortfolioAnswers> dspqList = new ArrayList<DistrictSpecificPortfolioAnswers>();				
				if(dspqType==1 || dspqType==0){
					dspqList= districtSpecificPortfolioAnswersDAO.findSpecificPortfolioAnswers(teacherDetail,jobOrder,dspqType);
				}else{				
					dspqList= districtSpecificPortfolioAnswersDAO.findSpecificPortfolioAnswersOnProfile(teacherDetail,userMaster,dspqType);
				}
				List<SecondaryStatusMaster> tagIds= new ArrayList<SecondaryStatusMaster>();
				for (DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : dspqList) {
					map.put(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId(), districtSpecificPortfolioAnswers.getAnswerId());					
					
				}
					
				Criterion criterionDis = Restrictions.eq("districtMaster", userMaster.getDistrictId());
				List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsLst = districtSpecificPortfolioQuestionsDAO.findByCriteria(criterionDis);
				
				for (DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions : districtSpecificPortfolioQuestionsLst) {
					questTags.put(districtSpecificPortfolioQuestions.getQuestionId(), districtSpecificPortfolioQuestions);					
					if(districtSpecificPortfolioQuestions.getProvideTag()){					
						tagIds.add(districtSpecificPortfolioQuestions.getSecondaryStatusMaster());
					}
				}
				
				Criterion criterion1 = Restrictions.in("districtSpecificPortfolioQuestions", districtSpecificPortfolioQuestionsLst);
				List<DistrictSpecificPortfolioOptions> listPortfolioOptions = districtSpecificPortfolioOptionsDAO.findByCriteria(criterion1);
				
				for (DistrictSpecificPortfolioOptions districtSpecificPortfolioOptions : listPortfolioOptions) {
					String questionOption = districtSpecificPortfolioOptions.getDistrictSpecificPortfolioQuestions().getQuestionId()+"##"+districtSpecificPortfolioOptions.getOptionId();
					questionOptions.put(questionOption, districtSpecificPortfolioOptions.getQuestionOption());
				}			
				
				Criterion criterion2 = Restrictions.in("secondaryStatusMaster", tagIds);
				Criterion criterion3 = Restrictions.eq("teacherDetail",teacherDetail);
				List<TeacherSecondaryStatus> tSecStatusList = new ArrayList<TeacherSecondaryStatus>();
				
				if(tagIds.size()>0){
					tSecStatusList = teacherSecondaryStatusDAO.findByCriteria(criterion3,criterion2);
				}
				if(tSecStatusList.size()>0)
				for (TeacherSecondaryStatus teacherSecondaryStatus : tSecStatusList) {
					teacherTags.put(teacherSecondaryStatus.getTeacherDetail().getTeacherId()+"##"+teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusId(), teacherSecondaryStatus);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			List<Integer> answers = new ArrayList<Integer>();
			Integer ansId = null;
			for (DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : districtSpecificPortfolioAnswersList) {
				districtSpecificPortfolioAnswers.setTeacherDetail(teacherDetail);
				
				districtSpecificPortfolioAnswers.setDistrictMaster(userMaster.getDistrictId());
				
				districtSpecificPortfolioAnswers.setCreatedDateTime(new Date());

				ansId = map.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId());
				if(ansId!=null)
				{
					districtSpecificPortfolioAnswers.setAnswerId(ansId);
				}
				if(!districtSpecificPortfolioAnswers.getQuestionType().equalsIgnoreCase("ml"))
				{
					if(districtSpecificPortfolioAnswers.getIsValidAnswer()!=null && districtSpecificPortfolioAnswers.getIsValidAnswer()==false)
						inValidCount++;
				}
				System.out.println("dspqType:::::11:::::::::"+dspqType);
				if(districtSpecificPortfolioAnswers.getSchoolIdTemp()!=null && schoolMap.containsKey(districtSpecificPortfolioAnswers.getSchoolIdTemp())){
					districtSpecificPortfolioAnswers.setSchoolMaster(schoolMap.get(districtSpecificPortfolioAnswers.getSchoolIdTemp()));
				}
				if(dspqType==1){
					districtSpecificPortfolioAnswers.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
					districtSpecificPortfolioAnswers.setJobOrder(null);
				}else if(dspqType==0){
					districtSpecificPortfolioAnswers.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
					districtSpecificPortfolioAnswers.setJobOrder(jobOrder);
				}else{
					districtSpecificPortfolioAnswers.setJobOrder(null);
					districtSpecificPortfolioAnswers.setJobCategoryMaster(null);
				}
				
				if(districtSpecificPortfolioAnswers.getSliderScore()!=null && districtSpecificPortfolioAnswers.getSliderScore()>0){
					try {
						districtSpecificPortfolioAnswers.setScoreBy(userMaster.getUserId());
						districtSpecificPortfolioAnswers.setScoreDateTime(new Date());
				
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}

				districtSpecificPortfolioAnswers.setIsActive(true);
				districtSpecificPortfolioAnswersDAO.makePersistent(districtSpecificPortfolioAnswers);
				answers.add(districtSpecificPortfolioAnswers.getAnswerId());
								
				if(questTags.containsKey(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()) && questTags.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()).getProvideTag()){
					DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions = questTags.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId());
					String quesOpt = (districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()+"##"+districtSpecificPortfolioAnswers.getSelectedOptions()).replace("|", "");
					
					if(questionOptions.get(quesOpt).equalsIgnoreCase("Yes") && !teacherTags.containsKey(teacherDetail.getTeacherId()+"##"+districtSpecificPortfolioQuestions.getSecondaryStatusMaster().getSecStatusId())){
						TeacherSecondaryStatus teacherSecondaryStatus = new TeacherSecondaryStatus();					
						teacherSecondaryStatus.setTeacherDetail(teacherDetail);
						teacherSecondaryStatus.setJobOrder(jobOrder);
						teacherSecondaryStatus.setSecondaryStatusMaster(districtSpecificPortfolioQuestions.getSecondaryStatusMaster());
						teacherSecondaryStatus.setCreatedByUser(userMaster);
						teacherSecondaryStatus.setCreatedByEntity(userMaster.getEntityType());
						teacherSecondaryStatus.setDistrictMaster(userMaster.getDistrictId());
						teacherSecondaryStatus.setCreatedDateTime(new Date());
						teacherSecondaryStatusDAO.makePersistent(teacherSecondaryStatus);					
					}
					if((questionOptions.get(quesOpt).equalsIgnoreCase("No") || questionOptions.get(quesOpt).equalsIgnoreCase("Prefer Not To Answer")) && teacherTags.containsKey(teacherDetail.getTeacherId()+"##"+districtSpecificPortfolioQuestions.getSecondaryStatusMaster().getSecStatusId())){
							TeacherSecondaryStatus teacherSecondaryStatus = teacherTags.get(teacherDetail.getTeacherId()+"##"+districtSpecificPortfolioQuestions.getSecondaryStatusMaster().getSecStatusId());				
							teacherSecondaryStatusDAO.makeTransient(teacherSecondaryStatus);					
					}
				}
				
			}
			return "1";

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

/*======================== comon panel=======================*/
	public void saveCommonPanel(String panelDateVal,String panelTime,String meridiem,Integer	timezoneId,
			String location,String disSchCmnt,Integer[] teacherIdArr,Integer jobId,boolean panelStatus,Integer[] arr,Integer panelId,Integer jobPanelStatusId){

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
		}

		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
		Date panelDate = null;
		try {

			panelDate = formatter.parse(panelDateVal);
			System.out.println(panelDate);
			System.out.println(formatter.format(panelDate));

		} catch (Exception e) {
			e.printStackTrace();
		}

		String mailContent = "";
		int panelchangedcnt = 0;
		/*  =\================    processing ===================*/
		List<TeacherDetail> tIdLst= new ArrayList<TeacherDetail>();
		List<Integer> teIdLst= new ArrayList<Integer>();
		for (int i = 0; i < teacherIdArr.length; i++) {
			TeacherDetail teacherDetail =new TeacherDetail();
			teacherDetail.setTeacherId(teacherIdArr[i]);
			tIdLst.add(teacherDetail);
			teIdLst.add(teacherIdArr[i]);
		}
		List<TeacherDetail> teacherList = teacherDetailDAO.findByTeacherIds(teIdLst);
		Map<Integer, TeacherDetail> tMap = new HashMap<Integer, TeacherDetail>();
		if(teacherList!=null && teacherList.size()>0){
			for (TeacherDetail teacherDetail : teacherList) {
				tMap.put(teacherDetail.getTeacherId(), teacherDetail);
			}
		}
		
		JobOrder jobOrder= jobOrderDAO.findById(jobId, false, false);
		jobOrder.setJobId(jobId);
		
		JobWisePanelStatus jobWisePanelStatus = new JobWisePanelStatus();
		jobWisePanelStatus.setJobPanelStatusId(jobPanelStatusId);
		
		List<PanelSchedule> panelTeacherSchList = panelScheduleDAO.findPanelScheduleList(tIdLst, jobOrder, jobWisePanelStatus);
		Map<Integer, PanelSchedule> mapAlreadyPanel = new HashMap<Integer, PanelSchedule>();
		
		if(panelTeacherSchList!=null && panelTeacherSchList.size()>0){
			for (PanelSchedule panelSchedule : panelTeacherSchList) {
				mapAlreadyPanel.put(panelSchedule.getTeacherDetail().getTeacherId(), panelSchedule);
			}
		}
		
		List<UserMaster> inviteeList = new ArrayList<UserMaster>();		
		if(arr.length>0){
			List<Integer> daIdList = new ArrayList<Integer>();
			for(int i=0;i<arr.length;i++)
			{
				daIdList.add(arr[i]);
			}			
			inviteeList = userMasterDAO.getUsersByUserIdLst(daIdList);
		}

			List<TimeZoneMaster> timeZoneLIs = timeZoneMasterDAO.getAllTimeZone();
			Map<Integer, TimeZoneMaster> timeZoneMap= new HashMap<Integer, TimeZoneMaster>();
			if(timeZoneLIs!=null && timeZoneLIs.size()>0){
				for (TimeZoneMaster timeZoneMaster : timeZoneLIs) {
					timeZoneMap.put(timeZoneMaster.getTimeZoneId(), timeZoneMaster);
				}
			}	
		
		 	SessionFactory sessionFactory=panelScheduleDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	 	    Transaction txOpen =statelesSsession.beginTransaction();
	 	    			
			String dStatus=null;
			if(panelStatus){
				dStatus="Completed";
			}else{
				dStatus="Scheduled";
			}
			
			List<PanelSchedule> panelschdList = new ArrayList<PanelSchedule>();				
	 	   for (int i = 0; i < teacherIdArr.length; i++) {
	 		   try {
	 		   	if(!mapAlreadyPanel.containsKey(teacherIdArr[i])){
					PanelSchedule panelSchedule=new PanelSchedule();
					panelSchedule.setDistrictId(districtMaster.getDistrictId());
					panelSchedule.setSchoolId(schoolMaster);
					TeacherDetail teacherDetail =new TeacherDetail();
					teacherDetail.setTeacherId(teacherIdArr[i]);				
					panelSchedule.setTeacherDetail(teacherDetail);
					panelSchedule.setJobOrder(jobOrder);
					panelSchedule.setCreatedDateTime(new Date());
					panelSchedule.setPanelDate(panelDate);
					panelSchedule.setPanelTime(panelTime);
					panelSchedule.setTimeFormat(meridiem);
					if(timeZoneMap.get(timezoneId) != null);
						panelSchedule.setTimeZoneMaster(timeZoneMap.get(timezoneId));
					panelSchedule.setPanelAddress(location);
					panelSchedule.setPanelDescription(disSchCmnt);
					panelSchedule.setCreatedBy(userMaster);
					panelSchedule.setPanelStatus(dStatus);
					jobWisePanelStatus.setJobPanelStatusId(jobPanelStatusId);
					panelSchedule.setJobWisePanelStatus(jobWisePanelStatus);					
					txOpen =statelesSsession.beginTransaction();
            	 	statelesSsession.insert(panelSchedule);
            	 	txOpen.commit();
            	 	panelschdList.add(panelSchedule);
	 	    }
	 		  } catch (Exception e) {
				e.printStackTrace();
				}
	 	   }
	 	   
	 	   String baseURL=Utility.getBaseURL(request);	 	   
	 	   Map<Integer, String> externalUserLink = new HashMap<Integer, String>();
	 	  List<PanelAttendees> paneAttendeesList = new ArrayList<PanelAttendees>();
	 	   if(panelschdList.size()>0){
	 		   Integer psCount=1;
		 		  for (PanelSchedule panelSchedule : panelschdList) {
		 			 if(inviteeList.size()>0){		 				
		 				 int inviteCount=1;
		 				for (UserMaster invitee : inviteeList) {
		 					PanelAttendees paneAttendees = new PanelAttendees();
			 				paneAttendees.setPanelDate(panelSchedule.getCreatedDateTime());
			 				paneAttendees.setPanelSchedule(panelSchedule);
			 				paneAttendees.setPanelStatus(dStatus);
			 				paneAttendees.setHostId(userMaster);			 			
			 				paneAttendees.setPanelInviteeId(invitee);

			 				txOpen =statelesSsession.beginTransaction();
		            	 	statelesSsession.insert(paneAttendees);
		            	 	txOpen.commit();
		            	 	
			            	 	/*Link Code Start*/		            	 	
			            	 	
			            	 	
			        			String candidateNamesAndLink="";
			        			if(paneAttendees.getPanelInviteeId().getIsExternal()!=null && paneAttendees.getPanelInviteeId().getIsExternal()==true)
			    	 			{
			        				String pnlId=Utility.encryptNo(panelSchedule.getPanelId());
				        			String usrId=Utility.encryptNo(paneAttendees.getPanelInviteeId().getUserId());
				        			String linkIds= pnlId+"###"+usrId;
				        			String forMated = null;
				        			try {
				        				forMated = Utility.encodeInBase64(linkIds);
				        				forMated = Utility.getShortURL(baseURL+"panelscore.do?id="+forMated);		        				
				        			} catch (Exception e) {
				        				e.printStackTrace();
				        			}
				        			
			        				candidateNamesAndLink ="<tr><td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>"+psCount+". "+getTeacherName(tMap,panelSchedule.getTeacherDetail())+"</td><td><a href='"+forMated+"'>"+forMated+"</a></td></tr>";
			    	 			}else{
			    	 				candidateNamesAndLink ="<tr><td colspan='2' style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>"+psCount+". "+getTeacherName(tMap,panelSchedule.getTeacherDetail())+"</td></tr>"; 
			    	 			}
			        			
			        			if(!externalUserLink.containsKey(invitee.getUserId())){
			        				externalUserLink.put(invitee.getUserId(), candidateNamesAndLink);
			        			}
			        			else{
			        				String prvLink = externalUserLink.get(invitee.getUserId());
			        				externalUserLink.put(invitee.getUserId(), prvLink+candidateNamesAndLink);	
			        			}
			        			
			        			/*Link Code End*/
		            	 	if(psCount.equals(1)){
		            	 		paneAttendeesList.add(paneAttendees);
		            	 	}
		            	 	inviteCount++;
						}			 					 			
		 			}
		 			psCount++;
				} 
	 	   }
	 	   
	 	  boolean mailSend=true;
			try{
				if(districtMaster.getDistrictId()==1200390){
					mailSend=false;
				}
			}catch(Exception e){}
	 	  String distributionEmail=districtMasterDAO.getDistributionEmail(districtMaster);
	 	  
	 	boolean isDistributionMail = true;
	 	if(distributionEmail==null || distributionEmail.equals(""))
	 	isDistributionMail = false;
	 	
	 	String emailto = "vishwanath@netsutra.com";
	 		for (PanelAttendees paneAttendees : paneAttendeesList) {
	 			DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
	 			PanelSchedule panelSchedule = paneAttendees.getPanelSchedule();
	 			
	 			String cNContent = externalUserLink.get(paneAttendees.getPanelInviteeId().getUserId());
	 			
	 			mailContent = MailText.getNewPanelComonAttendeeMailText(request,panelSchedule,paneAttendees,paneAttendeesList,cNContent);
	 			
	 			emailto = paneAttendees.getPanelInviteeId().getEmailAddress();
	 			dsmt.setEmailerService(emailerService);
	 			dsmt.setMailfrom("vishwanath@netsutra.com");
	 			dsmt.setMailto(emailto);
	 			dsmt.setMailsubject("Panel Scheduled");
	 			dsmt.setMailcontent(mailContent);
	 			dsmt.start();
	 			if(isDistributionMail)
	 			{
	 				dsmt = new DemoScheduleMailThread();
	 				dsmt.setEmailerService(emailerService);
	 				dsmt.setMailfrom("vishwanath@netsutra.com");
	 				dsmt.setMailto(distributionEmail);
	 				dsmt.setMailsubject("Panel Scheduled");
	 				dsmt.setMailcontent(mailContent);
	 				dsmt.start();
	 			}
	 		}
	}
	
	public static String getTeacherName(Map<Integer, TeacherDetail> tMap,TeacherDetail teacherDetail){
		String teacherName="";
		try{
			if(tMap.size()>0 && tMap.containsKey(teacherDetail.getTeacherId())){
				teacherDetail= tMap.get(teacherDetail.getTeacherId());
				if(teacherDetail.getFirstName()!=null){
					teacherName=teacherDetail.getFirstName();
				}
				if(teacherDetail.getFirstName()!=null && teacherDetail.getLastName()!=null){
					teacherName=teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
				}
			}
		}catch(Exception e){	e.printStackTrace(); }
		return teacherName;
	}
	//Send Once Again Invitationb to the hide Job Candidates
	public void sendAgainInviteToCandidates(String jobId,String teacherIds)
	{
		System.out.println(teacherIds+" teacherIds ::::::::::::::::::::::::::::::::::::::::::::::::::::::jobId "+jobId);

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
		}
		 SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
		 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
    	 Transaction txOpen =statelesSsession.beginTransaction();
		try{
			DemoScheduleMailThread dsmt=null;
			JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
			List<Integer> teacherIdList= Utility.convertStringIntoIntegerList(teacherIds,",");
			if(teacherIdList!=null && teacherIdList.size()>0)
			{
				 List<TeacherDetail> lstTeacherDetails =teacherDetailDAO.findByTeacherIds(teacherIdList);
				 
					List<MessageToTeacher> messageToTeacherList=new ArrayList<MessageToTeacher>();
					Map<Integer,String> passwordMap=new HashMap<Integer, String>();
					try{
						if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId()==1){
							messageToTeacherList=messageToTeacherDAO.findByMailTextByTeacherAndJob(lstTeacherDetails,jobOrder);
							System.out.println("messageToTeacherList::::::::::::::::::::::::::::::::::"+messageToTeacherList.size());
							if(messageToTeacherList.size()>0){
								for (MessageToTeacher messageToTeacher : messageToTeacherList) {
									if(passwordMap.get(messageToTeacher.getTeacherId().getTeacherId())==null){
										passwordMap.put(messageToTeacher.getTeacherId().getTeacherId(),messageToTeacher.getMessageSend());
									}
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					System.out.println("passwordMap:::::::::::::::::"+passwordMap.size());
				 
				 
				 if(lstTeacherDetails!=null && lstTeacherDetails.size()>0){
					 String distrcitSchoolName="";
				       	for (TeacherDetail teacherDetail : lstTeacherDetails) {
				       		String password=null;
				       		if(passwordMap.get(teacherDetail.getTeacherId())!=null){
				       			password=getPasswordFromMailText(passwordMap.get(teacherDetail.getTeacherId()));
				       		}
							System.out.println("password:::::::::::::>>>>"+password);
				       		
				       		if(jobOrder.getHeadQuarterMaster()!=null){
				       			distrcitSchoolName=jobOrder.getHeadQuarterMaster().getHeadQuarterName();
		        			}else{
		        				distrcitSchoolName=jobOrder.getDistrictMaster().getDistrictName();	
		        			}
				       		dsmt = new DemoScheduleMailThread();
							dsmt.setEmailerService(emailerService);
							dsmt.setMailfrom("admin@netsutra.com");
							dsmt.setMailto(teacherDetail.getEmailAddress());
							dsmt.setMailsubject(Utility.getLocaleValuePropByKey("msgInvitedApplyJob", locale)+" "+distrcitSchoolName);
							//dsmt.setMailcontent(MailText.sendInviteForInviteOnlyCandidatesMailText(jobOrder,teacherDetail2));
							if(jobOrder.getHeadQuarterMaster()!=null){
								dsmt.setMailcontent(MailText.sendInviteForInviteOnlyCandidatesMailTextForKelly(password,jobOrder,teacherDetail,new ArrayList<TeacherDetail>()));
							}else{
							  dsmt.setMailcontent(MailText.sendInviteForInviteOnlyCandidatesMailText(jobOrder,teacherDetail));
							}
							try {
								dsmt.start();	
							} catch (Exception e) {}
							
							try{
								MessageToTeacher  messageToTeacher= new MessageToTeacher();
								messageToTeacher.setTeacherId(teacherDetail);
								messageToTeacher.setJobId(jobOrder);
								messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
								messageToTeacher.setMessageSubject(Utility.getLocaleValuePropByKey("msgInvitedApplyJob", locale)+" "+distrcitSchoolName);
								if(jobOrder.getHeadQuarterMaster()!=null){
									messageToTeacher.setMessageSend(MailText.sendInviteForInviteOnlyCandidatesMailTextForKelly(password,jobOrder,teacherDetail,new ArrayList<TeacherDetail>()));
								}else{
									messageToTeacher.setMessageSend(MailText.sendInviteForInviteOnlyCandidatesMailText(jobOrder,teacherDetail));
								}
								messageToTeacher.setSenderId(userMaster);
								messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
								messageToTeacher.setEntityType(userMaster.getEntityType());
								messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
								if(jobOrder.getIsInviteOnly()!=null && jobOrder.getIsInviteOnly())
									messageToTeacher.setImportType("HR");	
								else
									messageToTeacher.setImportType("NR");
								statelesSsession.insert(messageToTeacher);
							}catch(Exception e){
								e.printStackTrace();
							}
						}
				 }
			}
			txOpen.commit();
			statelesSsession.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		//return null;
	}
	public String getPasswordFromMailText(String mailText){
		String password=null;
		try{
			if(mailText.indexOf("Password:")>1){
				mailText = mailText.substring(mailText.indexOf("Password:") + 9);
				password = mailText.substring(0,mailText.indexOf("<br/><br/>")).trim();
				System.out.println("password::::"+password);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return password;
	}
	
	
	public void reSendTalentSignUp(String jobId,String teacherIds)
	{
		System.out.println(teacherIds+" teacherIds ::::::::::::::reSendTalentSignUp::::::::::::::::::::::::::::::::::::::::jobId "+jobId);

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
		}
		 SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
		 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
    	 Transaction txOpen =statelesSsession.beginTransaction();
		try{
			DemoScheduleMailThread dsmt=null;
			JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
 			List<Integer> teacherIdList= Utility.convertStringIntoIntegerList(teacherIds,",");
 		
			if(teacherIdList!=null && teacherIdList.size()>0)
			{
					List<TeacherDetail> lstTeacherDetails =teacherDetailDAO.findByTeacherIds(teacherIdList);
					List<MessageToTeacher> messageToTeacherList=new ArrayList<MessageToTeacher>();
					Map<Integer,MessageToTeacher> msgMap=new HashMap<Integer, MessageToTeacher>();
					try{
						if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId()==1){
							//messageToTeacherList=messageToTeacherDAO.findByAuthMailTextByTeacherAndJob(lstTeacherDetails,jobOrder);
							messageToTeacherList=messageToTeacherDAO.findByAuthMailTextByTeacher(lstTeacherDetails);
							System.out.println("messageToTeacherList::::::::::::::::::::::::::::::::::"+messageToTeacherList.size());
							for (MessageToTeacher messageToTeacher : messageToTeacherList) {
								msgMap.put(messageToTeacher.getTeacherId().getTeacherId(),messageToTeacher);
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				 if(lstTeacherDetails!=null && lstTeacherDetails.size()>0){
				       	for (TeacherDetail teacherDetail : lstTeacherDetails) {
				       		MessageToTeacher messageToTeacherObj= msgMap.get(teacherDetail.getTeacherId());
				       		MessageToTeacher  messageToTeacher= new MessageToTeacher();
							messageToTeacher.setTeacherId(teacherDetail);
							if(jobOrder!=null){
								messageToTeacher.setJobId(jobOrder);
							}
							messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
				       		if(messageToTeacherObj!=null){
								
								Properties  properties = new Properties();  
								InputStream inputStream = new Utility().getClass().getClassLoader().getResourceAsStream("smtpmail.properties");
								properties.load(inputStream);
					       		dsmt = new DemoScheduleMailThread();
					       		dsmt.setEmailerService(emailerService);
					       		dsmt.setMailfrom(properties.getProperty("smtphost.noreplyusername"));
					       		dsmt.setMailto(teacherDetail.getEmailAddress());
					       		dsmt.setMailsubject(messageToTeacherObj.getMessageSubject());
								dsmt.setMailcontent(messageToTeacherObj.getMessageSend());
					       		try {
									dsmt.start();	
								} catch (Exception e) {}
									
								try{
									messageToTeacher.setMessageSubject(messageToTeacherObj.getMessageSubject());
									messageToTeacher.setMessageSend(messageToTeacherObj.getMessageSend());
									messageToTeacher.setSenderId(userMaster);
									messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
									messageToTeacher.setEntityType(userMaster.getEntityType());
									messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
									messageToTeacher.setImportType("AR");
									statelesSsession.insert(messageToTeacher);
								}catch(Exception e){
									e.printStackTrace();
								}
				       		}
						}
				 }
			}
			txOpen.commit();
			statelesSsession.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		//return null;
	}
	
	 public String getSchoolListByJob(Integer JobId,String noOfRow,String page,String sortOrder,String sortOrderType)
	  {
		  StringBuffer tmRecords =	new StringBuffer();
	      WebContext context;
	      context = WebContextFactory.get();
	      HttpServletRequest request = context.getHttpServletRequest();
	      HttpSession session = request.getSession(false);
	      if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	      {
	            throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	      }
		    try{
		    	int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(page);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="schoolName";
				String sortOrderNoField="schoolName";
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
		    	
		    	
		    	List<SchoolInJobOrder> listSchoolInJobOrders=new ArrayList<SchoolInJobOrder>();
		    	
		    	listSchoolInJobOrders=schoolInJobOrderDAO.findJobListByJob(jobOrderDAO.findById(JobId, false,false),sortOrderStrVal,start,end);
		    	
		    	tmRecords.append("<table  class='' id='schoolListGrid' width='100%' border='0' >");
		    	tmRecords.append("<thead class='bg' style='color:#ffffff;text-align:left;'>");
				tmRecords.append("<tr>");
				String responseText="";
				
				//responseText=PaginationAndSorting.responseSortingLink("School Name",sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th width='45%' valign='top'>School Name</th>");
				
				tmRecords.append("<th width='55%' valign='top'>Address</th>");
				
				tmRecords.append("</thead>");
					tmRecords.append("<tbody>");
				
					if(listSchoolInJobOrders.size()==0){
						 tmRecords.append("<tr><td colspan='10' align='left' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
					}
				    for(SchoolInJobOrder schoolJob : listSchoolInJobOrders){
				    SchoolMaster lst=schoolJob.getSchoolId();
				    	
				    	String schoolAddress="";
						String sAddress="";
						String sstate="";
						String scity = "";
						String szipcode="";
						
							if(lst.getAddress()!=null && !lst.getAddress().equals(""))
							{
								if(!lst.getCityName().equals(""))
								{
									schoolAddress = lst.getAddress()+", ";
								}
								else
								{
									schoolAddress = lst.getAddress();
								}
							}
							if(!lst.getCityName().equals(""))
							{
								if(lst.getStateMaster()!=null && !lst.getStateMaster().getStateName().equals(""))
								{
									scity = lst.getCityName()+", ";
								}else{
									scity = lst.getCityName();
								}
							}
							if(lst.getStateMaster()!=null && !lst.getStateMaster().getStateName().equals(""))
							{
								if(!lst.getZip().equals(""))
								{
									sstate = lst.getStateMaster().getStateName()+", ";
								}
								else
								{
									sstate = lst.getStateMaster().getStateName();
								}	
							}
							if(!lst.getZip().equals(""))
							{
									szipcode = lst.getZip().toString();
							}
							
							if(schoolAddress !="" || scity!="" ||sstate!="" || szipcode!=""){
								sAddress = schoolAddress+scity+sstate+szipcode;
							}else{
								sAddress="";
							}
						
							tmRecords.append("<tr>");
							tmRecords.append("<td>"+lst.getSchoolName()+"</td>");
							tmRecords.append("<td>"+sAddress+"</td>");
							tmRecords.append("</tr>");
				    }
				tmRecords.append("</tbody>");
				tmRecords.append("</table>");
			}catch (Exception e) {
			   e.printStackTrace();
			}
		  return tmRecords.toString();
	 }
	
	public String[] updateAppliedJobsonCG(String teacherIds)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		String[] jobCount = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
		}
		
		Map<Integer,String> teacherJobs = new HashMap<Integer, String>();
		List<Integer> candidatesIds= new ArrayList<Integer>();
		
		if(teacherIds!=null && teacherIds.length()>0){
			String[] candidates = teacherIds.split(",");
			if(candidates!=null && candidates.length>0)
			{
				for(String s : candidates)
				{
					candidatesIds.add(Integer.parseInt(s));
				}
			}

			try {
				List<TeacherDetail> listteacherDetail = teacherDetailDAO.findByTeacherIds(candidatesIds);
				teacherJobs = jobForTeacherDAO.countJobsByTeachers(listteacherDetail, userMaster);

				if(teacherJobs.size()>0){

					jobCount = new String[teacherJobs.size()];

					for(int tId=0;tId<candidatesIds.size();tId++){
						jobCount[tId] = teacherJobs.get(candidatesIds.get(tId));
					}
				}		

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return jobCount;
	}
	
	public String saveTempCGEventDetail(String jobId,String teacherIds)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
		}
		String sessionId=session.getId();
		System.out.println("TTTTTTTTTTTTTTTTTTTTTTTTTTTT :: "+teacherIds);
		if(teacherIds!=null && teacherIds.length()>0)
		{
		  try
		  {
			  List<TeacherDetail> listteacherDetail = teacherDetailDAO.findByTeacherIds(Utility.convertStringIntoIntegerList(teacherIds, ","));
			  if(listteacherDetail!=null && listteacherDetail.size()>0)
			  {
				  
				  SessionFactory sessionFactory = cGStatusEventTempDAO.getSessionFactory();
				  StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				  Transaction txOpen = statelesSsession.beginTransaction();
				  JobOrder jobOrder=new JobOrder();
				  jobOrder.setJobId(Integer.parseInt(jobId));
				  
				  for(TeacherDetail teacherDetail:  listteacherDetail)
				  {
					  CGStatusEventTemp cgevent= new CGStatusEventTemp();
					  cgevent.setJobOrder(jobOrder);
					  cgevent.setTeacherDetail(teacherDetail);
					  cgevent.setSessionId(sessionId);
					  //cgevent.setDate(new Date());
					  statelesSsession.insert(cgevent);	
					  
				  }
				  txOpen.commit();
				  statelesSsession.close();
			  }

		   } catch (Exception e) {
			 e.printStackTrace();
		   }
		}
		return "success";
	}
	
	public String callMessageQueueAPI(String[] teacher, Integer callerId)
	{
		System.out.println("::::::::::::callMessageQueueAPI:::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
		List<TeacherDetail> teacherDetailList=new ArrayList<TeacherDetail>();
		Set<Integer> teacherIds=new HashSet<Integer>();
		List<Long> jftIds=new ArrayList<Long>();
		List<MQEvent> mqEvents = new ArrayList<MQEvent>();
		Map<String,String> mqMap = new HashMap<String, String>();
		String msg="";
		List<JobForTeacher> jfteacher=null;
	
		if(teacher.length>0)
		{
			for(String id:teacher)
			{		
				jftIds.add(Long.parseLong(id));
			}			
		    jfteacher =jobForTeacherDAO.getJobForTeacherList(jftIds);
			for(JobForTeacher jftn:jfteacher){
				teacherIds.add(jftn.getTeacherId().getTeacherId());				
				}				
		if(teacherIds.size()>0)
			teacherDetailList=teacherDetailDAO.getActiveKellyTeacherList(teacherIds);
		}	
		
		boolean ksnFlag=false;
		boolean wf1Flag=false;
		boolean wf2Flag=false;
		boolean wf3Flag=false;
		boolean ksnFlag1=false;
		boolean wf1Flag1=false;
		boolean wf2Flag1=false;
		boolean wf3Flag1=false;
		String LinkToKsn = Utility.getLocaleValuePropByKey("lblLinkToKsn", locale);
		String INVWF1 = Utility.getLocaleValuePropByKey("lblINVWF1", locale);
		String INVWF2 = Utility.getLocaleValuePropByKey("lblINVWF2", locale);
		String INVWF3 = Utility.getLocaleValuePropByKey("lblINVWF3", locale);
		List<String> statusList = new ArrayList<String>();
		
		
		Map<JobCategoryMaster,JobCategoryMaster> allCategMap = new HashMap<JobCategoryMaster,JobCategoryMaster>();
		Map<String,SecondaryStatus> secStatusMap = new HashMap<String, SecondaryStatus>();
		Map<TeacherDetail,JobForTeacher> jftMap = new HashMap<TeacherDetail, JobForTeacher>();
		StatusMaster statusMaster=null;
		List<SecondaryStatus> secondaryStatuses =null;
 		SecondaryStatus secondaryStatus = null;
		try{
				if(LinkToKsn!=null)
					statusList.add(LinkToKsn);
				if(INVWF1!=null)
					statusList.add(INVWF1);
				if(INVWF2!=null)
					statusList.add(INVWF2);
				if(INVWF3!=null)
					statusList.add(INVWF3);
				
				if(teacherDetailList.size()>0){
					WorkFlowStatus workFlowStatus = workFlowStatusDAO.findById(2, false, false);
					mqEvents = mqEventDAO.findWFStatusByTeacherList(teacherDetailList,workFlowStatus);
				
					if(mqEvents!=null && mqEvents.size()>0){
						for(MQEvent mqevent : mqEvents){
							mqMap.put(mqevent.getTeacherdetail().getTeacherId()+"-"+mqevent.getEventType(), mqevent.getStatus());
						}
				    }
					if(jfteacher.size()>0){
						for(JobForTeacher jobForTeacher : jfteacher){	
							jftMap.put(jobForTeacher.getTeacherId(), jobForTeacher);
							allCategMap.put(jobForTeacher.getJobId().getJobCategoryMaster(), jobForTeacher.getJobId().getJobCategoryMaster().getParentJobCategoryId());
						}				
					}
					List<JobCategoryMaster> allCategory = new ArrayList<JobCategoryMaster>();
					if(allCategMap.size()>0)
					{
					   allCategory = new ArrayList<JobCategoryMaster>(allCategMap.values());
					}
					if(allCategory!=null && allCategory.size()>0)
						   secondaryStatuses = secondaryStatusDAO.findByNameListAndJobCats(statusList,allCategory);
					if(secondaryStatuses!=null && secondaryStatuses.size()>0){
						   for(SecondaryStatus sec : secondaryStatuses){
							   if(sec.getJobCategoryMaster()!=null){
								   secStatusMap.put(sec.getJobCategoryMaster().getJobCategoryId()+"+"+sec.getSecondaryStatusName(), sec);
								   logger.info("KEY::"+sec.getJobCategoryMaster().getJobCategoryId()+"+"+sec.getSecondaryStatusName());
							   }
						   }
					  }
					  logger.info("secStatusMap:"+secStatusMap.size()); 
					  Map<String,Date> latestNodeColorDateMap = new HashMap<String, Date>();
					  List<StatusNodeColorHistory> listStatusClr = new ArrayList<StatusNodeColorHistory>();
				      listStatusClr = statusNodeColorHistoryDAO.getStatusColorHistoryByTeacherList(teacherDetailList);
				      if(listStatusClr.size()>0){
				    	   for(StatusNodeColorHistory s :listStatusClr)
				    		   if(s.getMqEvent()!=null)
				    		   latestNodeColorDateMap.put(s.getTeacherDetail().getTeacherId()+"##"+s.getMqEvent().getEventType(), s.getUpdateDateTime());
				      }			      		    
				      List<MQEvent> mqEventList=null;
				      if(secondaryStatus!=null){
				    	  mqEventList = mqEventDAO.findEventByEventTypeList(teacherDetailList,secondaryStatus.getSecondaryStatusName());
				      }else{
				    	   mqEventList = mqEventDAO.findLinkToKSNByTeacherList(teacherDetailList);
				      }
				      List<MQEvent> mqEventsList=new ArrayList<MQEvent>();
				      if(mqEventList!=null&&mqEventList.size()>0){			    	  
							 mqEventsList=mqEventDAO.findMQEventByTeacherListt(teacherDetailList);  
				      }
					  System.out.println("mqEventsList ::::::"+mqEventsList.size());
				  
					  for(TeacherDetail td : teacherDetailList){
						if(td.getKSNID()!=null){
							ksnFlag=true;		    
						}else{
							ksnFlag1=true;
						}
						if(mqMap.containsKey(td.getTeacherId()+"-"+INVWF1)){
							wf1Flag=true;				
						}else{
							wf1Flag1=true;
						}
						if(mqMap.containsKey(td.getTeacherId()+"-"+INVWF2)){
							wf2Flag=true;				
						}else{
							wf2Flag1=true;
						}
						if(mqMap.containsKey(td.getTeacherId()+"-"+INVWF3)){
							wf3Flag=true;				
						}else{
							wf3Flag1=true;
						}
					 }				
	
				if(callerId==1){
					if(ksnFlag){
						msg="1"; //one or more talent Already Link to ksn
					}else{				
						if(jfteacher!=null && jfteacher.size()>0){
							   for(JobForTeacher jft : jfteacher){					
								   if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
									   logger.info(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+LinkToKsn);
									   if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()!=null){
										   secondaryStatus = secStatusMap.get(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+LinkToKsn);
										   if(secondaryStatus!=null)
										   logger.info("callerId:"+callerId+"----secondaryStatus:"+secondaryStatus.getSecondaryStatusId());
										   break;
									   }
								   }
							   }
						   }
						for(TeacherDetail td : teacherDetailList){
						    Boolean message=messageQueueForOnboarding(latestNodeColorDateMap,mqEventList,mqEventsList,statusMaster,secondaryStatus,jftMap.get(td),userMaster,"","");
						    System.out.println("boolean message status is:::"+message);
						}
						msg="11";
					}
			    }else if(callerId==2){
			    	if(wf1Flag){    		
				    		msg="2"; //one or more talent is already completed WF1 Registration
			    	}else{
			    		if(ksnFlag1){
			    			msg="3";//one or more talent is not linked to KSN
			    		}else{
							if(jfteacher!=null && jfteacher.size()>0){
								   for(JobForTeacher jft : jfteacher){
									   if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
										   logger.info(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+INVWF1);
										   if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()!=null){
											   secondaryStatus = secStatusMap.get(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+INVWF1);
											   if(secondaryStatus!=null)
											   logger.info("callerId:"+callerId+"----secondaryStatus:"+secondaryStatus.getSecondaryStatusId());
											   break;
										   }
									   }
								   }
							   }
							   for(TeacherDetail td : teacherDetailList){
								    Boolean message=messageQueueForOnboarding(latestNodeColorDateMap,mqEventList,mqEventsList,statusMaster,secondaryStatus,jftMap.get(td),userMaster,"","");
								    System.out.println("boolean message status is:::"+message);
								  }
							msg="22";
			    		}
			    	}
			    	
			    }else if(callerId==3){
				    if(wf2Flag){    		
				    		msg="4"; //one or more talent is already completed WF2 Registration
			    	}else if(ksnFlag1){
			    		msg="3";//one or more talent is not linked to KSN
			    	}else if(wf1Flag1){
			    		msg="5"; //one or more talent is not completed WF1 Registration
			    	}else{		
						if(jfteacher!=null && jfteacher.size()>0){
							   for(JobForTeacher jft : jfteacher){
								   if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
									   logger.info(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+INVWF2);
									   if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()!=null){
										   secondaryStatus = secStatusMap.get(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+INVWF2);
										   if(secondaryStatus!=null)
										   logger.info("callerId:"+callerId+"----secondaryStatus:"+secondaryStatus.getSecondaryStatusId());
										   break;
									   }
								   }
							   }
						   }
						   for(TeacherDetail td : teacherDetailList){
							    Boolean message=messageQueueForOnboarding(latestNodeColorDateMap,mqEventList,mqEventsList,statusMaster,secondaryStatus,jftMap.get(td),userMaster,"","");
							    System.out.println("boolean message status is:::"+message);
							  }
						msg="33";
					
			    	}
	    	
			    }else if(callerId==4){
			    	if(wf3Flag){    		
			    		msg="6"; //one or more talent is already completed WF3 Registration
			    	}else if(ksnFlag1){
			    		msg="3";//one or more talent is not linked to KSN
			    	}else if(wf1Flag1){
			    		msg="5"; //one or more talent is not completed WF1 Registration
			    	}else if(wf2Flag1){
			    		msg="7"; //one or more talent is not completed WF2 Registration
			    	}else{
						if(jfteacher!=null && jfteacher.size()>0){
							   for(JobForTeacher jft : jfteacher){
								   if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
									   logger.info(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+INVWF3);
									   if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()!=null){
										   secondaryStatus = secStatusMap.get(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+INVWF3);
										   if(secondaryStatus!=null)
										   logger.info("callerId:"+callerId+"----secondaryStatus:"+secondaryStatus.getSecondaryStatusId());
										   break;
									   }
								   }
							   }
						   }
						   for(TeacherDetail td : teacherDetailList){
							    Boolean message=messageQueueForOnboarding(latestNodeColorDateMap,mqEventList,mqEventsList,statusMaster,secondaryStatus,jftMap.get(td),userMaster,"","");
							    System.out.println("boolean message status is:::"+message);
							  }
						msg="44";
			    	}
			    }
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return msg;
	}
	@Transactional
	public boolean messageQueueForOnboarding(Map<String,Date> latestNodeColorDateMap,List <MQEvent> mqEventList,List<MQEvent> mqEventsList,StatusMaster statusMaster,SecondaryStatus secondaryStatus,JobForTeacher jobForTeacher,UserMaster userMaster,String reqType,String ksnId){
		boolean queueFlag=false;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
			logger.info("messageQueueForOnboarding method called");
			logger.info("Actime MQ Process Start with jobforteacherid -  "+ jobForTeacher.getJobForTeacherId());
			try {					
				 /**
			        * add to track node color history
			        */
			       StatusNodeColorHistory statusNodeColorHistory = null;
			       Map<String,MQEvent> mapNodeStatus= new HashMap<String, MQEvent>();			     
			       
				   SessionFactory sessionFactory=statusNodeColorHistoryDAO.getSessionFactory();
				   StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				   Transaction txOpen =statelesSsession.beginTransaction();
				   
				   Map<Object, Object> mapData = new HashMap<Object, Object>();
				   mapData.put("jobForTeacherId", jobForTeacher.getJobForTeacherId());
				   
				   if(secondaryStatus!=null)
					   mapData.put("secondaryStatusId",secondaryStatus.getSecondaryStatusId());
				   else
					   mapData.put("secondaryStatusId",null);
				   
				   mapData.put("userId",userMaster.getUserId());
				   String TMTransactionID="TM-"+(Utility.randomString(8)+Utility.getDateTime());
				   mapData.put("TMTransactionID", TMTransactionID);
				   mapData.put("KSNID", ksnId);
				   MQEvent mqEvent = null;
				   
		           if(mqEventList != null && mqEventList.size() >0){
				      mqEvent = mqEventList.get(0);
				      mapData.put("TMTransactionID", mqEvent.getTmTransId());
				      /**
				        * add to track node color history
				        */
				     System.out.println("mqEventsList    "+mqEventsList.size());
					 if(mqEventsList.size()>0){
						 for (MQEvent mqEv : mqEventsList) {
							 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
						 }
					 }
			      
					 try {
						 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
					 } catch (Exception e) {
						 e.printStackTrace();
					 }
					 /**
				       * end
				       */
				      
				   }else{
				       mqEvent = new MQEvent();
					   mqEvent.setTeacherdetail(jobForTeacher.getTeacherId());
					   if(secondaryStatus==null && statusMaster!=null){
						   mqEvent.setStatusMaster(statusMaster);
						   mqEvent.setEventType(statusMaster.getStatus());
					   }else if(secondaryStatus!=null){
						   mqEvent.setSecondaryStatus(secondaryStatus);
						   mqEvent.setEventType(secondaryStatus.getSecondaryStatusName());
					   }else{
						   mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
					   }
					   mqEvent.setTmTransId(TMTransactionID);
					   mqEvent.setStatus("R");
					   mqEvent.setCreatedBy(userMaster.getUserId());
					   mqEvent.setCreatedDateTime(new Date());
					   mqEvent.setIpAddress(request.getRemoteAddr());
					  // statelesSsession.insert(mqEvent);
					   mqEventDAO.makePersistent(mqEvent);
					   
					   
					   /**
				        * add to track node color history
				        */
					   try{
						   statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
					   }catch(Exception ex){
						   ex.printStackTrace();
					   }
					   /**
				        * end
				        */
					   
		           }
				   if(mqEvent != null)
				   mapData.put("MQEventID", mqEvent.getEventId());
					   
				   MQEventHistory mqhEventHistory = new MQEventHistory();
				   mqhEventHistory.setMqEvent(mqEvent);
				   mqhEventHistory.setCreatedBy(userMaster.getUserId());
				   mqhEventHistory.setCreatedDateTime(new Date());
				   mqhEventHistory.setIpAddress(request.getRemoteAddr());
				   //statelesSsession.insert(mqhEventHistory);
				   mqEventHistoryDAO.makePersistent(mqhEventHistory);
				   
				   
				   if(mqhEventHistory != null)
				   mapData.put("MQEventHistoryID", mqhEventHistory.getEventHistoryId());
				   
				   ApplicationContext APPLICATIONCONTEXT = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
				   JmsTemplate jmsTemplate =  (JmsTemplate) APPLICATIONCONTEXT.getBean("JMSTemplate");
				   String activeMQ=null;
				   mapData.put("reqType",reqType);
				   mapData.put("statusNodeColorHistoryId", statusNodeColorHistory.getStatusNodeColorHistoryId());
				   if((secondaryStatus==null && statusMaster==null) || (secondaryStatus!=null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)))){
					  
					   if(!reqType.equalsIgnoreCase("KSNID")){
						   mqEvent.setCheckEmailAPI(false);
						  // statelesSsession.update(mqEvent);
						   mqEventDAO.makePersistent(mqEvent);
						   activeMQ = "ActiveMQDestination";
					   }else{
						   mqEvent.setCheckEmailAPI(true);
						   //statelesSsession.update(mqEvent);
						   mqEventDAO.makePersistent(mqEvent);
						   logger.info(":::::::::::::::::::sendCheckEmailTalent : ::::::::::::::::::::::");
						   tmCommonUtil.checkForTalentEmailAddress(jobForTeacher, TMTransactionID , mqhEventHistory.getEventHistoryId(),statusNodeColorHistory);   
					   }
					   
					   mapData.put("WorkFlowID",null);
				   }else if(secondaryStatus!=null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF1", locale))){
					   activeMQ="ERegistration";
					   mapData.put("WorkFlowID",Utility.getLocaleValuePropByKey("msgINITIALAPP", locale));
				   }else if(secondaryStatus!=null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF2", locale))){
					   activeMQ="ERegistration";
					   mapData.put("WorkFlowID",Utility.getLocaleValuePropByKey("msgCONDOFF", locale));
				   }else if(secondaryStatus!=null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF3", locale))){
					   activeMQ="ERegistration";
					   mapData.put("WorkFlowID",Utility.getLocaleValuePropByKey("msgCONDFORMS", locale));
				   }
				   ActiveMQQueue eventSend =  (ActiveMQQueue)  APPLICATIONCONTEXT.getBean(activeMQ);
				   logger.info("-----------Before sending active mq of "+activeMQ+"-------------");
				   jmsTemplate.convertAndSend(eventSend,mapData);
				   logger.info("-----------After sending active mq  "+activeMQ+"-------------");
				   txOpen.commit();
				   statelesSsession.close();				   
			  } catch (BeansException e) {
			   e.printStackTrace();
			  } catch (JmsException e) {
			   e.printStackTrace();
			  } catch (Exception e) {
			   e.printStackTrace();
			  }
		return queueFlag;
}
	
	
	public String getJobInformationHeaderCG(Integer jobId){
		
		try {
		
		String lblSchoolName = Utility.getLocaleValuePropByKey("lblSchoolName", locale);
		String lblExpire = Utility.getLocaleValuePropByKey("lblExpire", locale);
		String lblActive    = Utility.getLocaleValuePropByKey("lblActive", locale);
		String lblInactive    = Utility.getLocaleValuePropByKey("lblInactive", locale);
		String lblJobTitle     = Utility.getLocaleValuePropByKey("lblJobTitle", locale);
		String lblTotalApplicant = Utility.getLocaleValuePropByKey("lblTotalApplicant", locale);
		String lblExpectedHires     = Utility.getLocaleValuePropByKey("lblExpectedHires", locale);
		String lblHiredTillDate     = Utility.getLocaleValuePropByKey("lblHiredTillDate", locale);
		String lblJobIdJobCode     = Utility.getLocaleValuePropByKey("lblJobIdJobCode", locale);
		
		JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
		
		StringBuffer sb = new StringBuffer();
		String jobType="",jobStatus="",DASALable="",DASAName="",jobTitle="";
		int noOfExpHires=0;
		if(jobOrder.getCreatedForEntity()==2){
			jobType="DJO";
			try{
				if(jobOrder.getSchool()!=null && jobOrder.getSchool().size()!=0){
					noOfExpHires=schoolInJobOrderDAO.noOfExpHiresSql(jobOrder,null,1);
				}else{
					if(jobOrder.getNoOfExpHires()!=null){
						noOfExpHires=jobOrder.getNoOfExpHires();
					}
				}
			}catch(Exception e){}
			if(jobOrder.getDistrictMaster()!=null){
				DASAName=jobOrder.getDistrictMaster().getDistrictName();				
				DASALable="District Name";
			}else{
				DASAName="";				
				DASALable="";	
			}
			int ShowJobId=jobOrder.getJobId();
			jobTitle="<a target='blank' href='editjoborder.do?JobOrderType=2&JobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobTitle()+"</a>";
		}else if(jobOrder.getCreatedForEntity()==5){
			jobType="HJO";
			try{
				if(jobOrder.getSchool()!=null && jobOrder.getSchool().size()!=0){
					noOfExpHires=schoolInJobOrderDAO.noOfExpHiresSql(jobOrder,null,1);
				}else{
					if(jobOrder.getNoOfExpHires()!=null){
						noOfExpHires=jobOrder.getNoOfExpHires();
					}
				}
			}catch(Exception e){}
			DASAName=jobOrder.getHeadQuarterMaster().getHeadQuarterName();				
			DASALable="Head Quarter Name";
			
			jobTitle="<a target='blank' href='hqbrjoborder.do?jobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobTitle()+"</a>";
		}else{
			jobTitle="<a target='blank' href='editjoborder.do?JobOrderType=3&JobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobTitle()+"</a>";
			try{
				DASAName=jobOrder.getSchool().get(0).getSchoolName();
				noOfExpHires=schoolInJobOrderDAO.noOfExpHiresSql(jobOrder,jobOrder.getSchool().get(0),0);
			}catch(Exception e){}
			jobType="SJO";
			DASALable=lblSchoolName;
		}
		if(jobOrder.getJobEndDate().compareTo(new Date())<0){
			jobStatus=lblExpire;
		}else{
			if(jobOrder.getStatus().equalsIgnoreCase("A")){
				jobStatus=lblActive;
			}else{
				jobStatus=lblInactive;
			}
		}
		List<StatusMaster> statusHird = new ArrayList<StatusMaster>();
		String[] statusH= {"hird"};
		
		int iTotalTeachers = jobForTeacherDAO.getApplicatCount(jobOrder.getJobId());
		try{
			statusHird = Utility.getStaticMasters(statusH);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		int noOfHiresJft = jobForTeacherDAO.noOfHiresSql(jobOrder,statusHird); 
		sb.append("<table border='0' cellspacing=0 cellpadding=0 width='960'>");
		sb.append("<tr>");
		sb.append("<td style='vertical-align: middle;' width='100%'><b>"+lblJobTitle+" </b>"+jobTitle +" ("+jobType+", "+jobStatus+", "+lblTotalApplicant+": "+iTotalTeachers+", "+lblExpectedHires+": "+noOfExpHires+", "+lblHiredTillDate+": "+noOfHiresJft+")");
		sb.append("</td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("<td style='vertical-align: middle;'><b>"+DASALable+": </b>"+DASAName+"<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+lblJobIdJobCode+"</b>&nbsp;"+jobOrder.getJobId());
		sb.append("</td>");			
		sb.append("</tr>");
		if(jobOrder.getCreatedForEntity()==2){					
			if(jobOrder.getSchool()!=null && jobOrder.getSchool().size()>0 && jobOrder.getSchool().size()<=1){
				String schoolName = jobOrder.getSchool().get(0).getSchoolName();
				sb.append("<tr>");
				sb.append("<td style='vertical-align: middle;'><b>"+"School Name"+": </b>"+schoolName);
				sb.append("</td>");			
				sb.append("</tr>");
				 
			}
		
			//shadab
			if(jobOrder.getSchool()!=null && jobOrder.getSchool().size()>1)
			{

				String schoolListIds = jobOrder.getSchool().get(0).getSchoolId().toString();
				for(int i=1;i<jobOrder.getSchool().size();i++)
				{
					schoolListIds = schoolListIds+","+jobOrder.getSchool().get(i).getSchoolId().toString();
				}
				sb.append("<tr>");
				sb.append("<td style='vertical-align: middle;'><b>"+"School Name"+": </b>"+"<a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a>");
				sb.append("</td>");			
				sb.append("</tr>");
				
				//sb.append("<td> <a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
			}
			//end
		}
		sb.append("<tr>");
		sb.append("</tr>");
		sb.append("</table>");
		return sb.toString();
		
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "";
	}	
	
}