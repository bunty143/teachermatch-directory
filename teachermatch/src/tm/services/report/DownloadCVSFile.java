package tm.services.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownloadCVSFile extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		try{
		String filePath=request.getParameter("path");
		
		System.out.println("**************downloading cvs file ******************"+filePath);
		
		File downloadFile=new File(filePath);
		FileInputStream fis=new FileInputStream(downloadFile);
		
		String mimeType=getServletContext().getMimeType(filePath);
		System.out.println("mimeType: : "+mimeType);
		
		if(mimeType==null)
		{
			mimeType="application/octet-stream";
		}
		response.setContentType(mimeType);
		response.setContentLength((int)downloadFile.length());
		
		//force download
		String headerKey="Content-Disposition";
		String headerValue=String.format("attachment; filename=\"%s\"", downloadFile.getName());
		
		response.setHeader(headerKey, headerValue);
		
		OutputStream outStream=response.getOutputStream();
		byte [] buffer=new byte[4096];
		int byteRead=-1;
		
		while((byteRead=fis.read(buffer))!= -1)
		{
			outStream.write(buffer,0,byteRead);
		}
		fis.close();
		outStream.close();
	  }catch(Exception e){
		  e.printStackTrace();
	  }
	}
}
