package tm.services.report;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.EmployeeMaster;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.SchoolSelectedByCandidate;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAchievementScore;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherExperience;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.StatusNodeColorHistory;
import tm.bean.i4.I4InterviewInvites;
import tm.bean.master.DemoClassSchedule;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.bean.master.DistrictSpecificPortfolioOptions;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.mq.MQEvent;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.hqbranchesmaster.StatusNodeColorHistoryDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.mq.MQEventDAO;
import tm.utility.IPAddressUtility;
import tm.utility.TeacherStatusHistoryForJobComparatorDesc;
import tm.utility.Utility;

public class CandidateGridService {
	private static final Logger logger = Logger.getLogger(CandidateGridService.class.getName());
	String locale = Utility.getValueOfPropByKey("locale");
	String lblResume=Utility.getLocaleValuePropByKey("lblResume", locale);
	String lblNotAvailable=Utility.getLocaleValuePropByKey("lblNotAvailable", locale);
	String lblFormerEmployee1=Utility.getLocaleValuePropByKey("lblFormerEmployee1", locale);
	String lblDateHiring1=Utility.getLocaleValuePropByKey("lblDateHiring1", locale);
	String lblSchoolName=Utility.getLocaleValuePropByKey("lblSchoolName", locale);
	String lblPotentialNobleQuality=Utility.getLocaleValuePropByKey("lblPotentialNobleQuality", locale);
	
	@Autowired
	private MQEventDAO mqEventDAO;
	
	@Autowired
	private StatusNodeColorHistoryDAO statusNodeColorHistoryDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired 
	private SecondaryStatusDAO secondaryStatusDAO;
	
	
	public Map<Integer, Boolean> findActiveExpStatus(List<TeacherExperience> lstTeacherExp)
	{
		Map<Integer,Boolean> mapTeacherCertificate= new TreeMap<Integer, Boolean>();
		try 
		{
			if(lstTeacherExp.size()>0)
			{
				for(TeacherExperience tExp : lstTeacherExp){
					if(tExp.getExpCertTeacherTraining()!=null && tExp.getExpCertTeacherTraining()>0)
						mapTeacherCertificate.put(tExp.getTeacherId().getTeacherId(), true);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		return mapTeacherCertificate;
	}
	public Map<Integer, Boolean> findActiveAcademicsStatus(List<TeacherAcademics> lstTeacherAcademics)
	{
		Map<Integer,Boolean> mapTeacherAcademics = new TreeMap<Integer, Boolean>();
		try 
		{
			if(lstTeacherAcademics.size()>0)
			{
				for(TeacherAcademics tAcademics: lstTeacherAcademics){
					mapTeacherAcademics.put(tAcademics.getTeacherId().getTeacherId(), true);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		return mapTeacherAcademics;
	}
	public Map<Integer, Boolean> findActiveCertificateStatus(List<TeacherCertificate> lstTeacherCertificate)
	{
		Map<Integer,Boolean> mapTeacherCertificate= new TreeMap<Integer, Boolean>();
		try 
		{
			if(lstTeacherCertificate.size()>0)
			{
				for(TeacherCertificate tCertificate : lstTeacherCertificate){
					mapTeacherCertificate.put(tCertificate.getTeacherDetail().getTeacherId(), true);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		return mapTeacherCertificate;
	}
	public Map<Integer, Boolean> findActiveResume(List<TeacherExperience> teacherExperienceList)
	{
		Map<Integer,Boolean> mapResume = new TreeMap<Integer, Boolean>();
		try{
			if(teacherExperienceList.size()>0)
			{
				String resume = null;
				for(TeacherExperience teacherExperience: teacherExperienceList ){
					resume = teacherExperience.getResume();
					if(resume==null || resume.trim().equals("")){
						mapResume.put(teacherExperience.getTeacherId().getTeacherId(), false);
					}else{
						mapResume.put(teacherExperience.getTeacherId().getTeacherId(), true);
					}
				}
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}		

		return mapResume;
	}
	
	public Map<Integer, TeacherAssessmentStatus> findActiveJSIStatus(List<TeacherAssessmentStatus> teacherAssessmentStatusList, JobOrder jobOrder)
	{	
		Map<Integer,TeacherAssessmentStatus> mapJSI = new TreeMap<Integer, TeacherAssessmentStatus>();		
		try{
			if(teacherAssessmentStatusList.size()>0){
				for(TeacherAssessmentStatus  teacherAssessmentStatus : teacherAssessmentStatusList){
					if(teacherAssessmentStatus.getJobOrder()!=null)
					if(teacherAssessmentStatus.getJobOrder().equals(jobOrder)){
						mapJSI.put(teacherAssessmentStatus.getTeacherDetail().getTeacherId() , teacherAssessmentStatus);
					}
				}
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return mapJSI;
	}
	public TeacherExperience getTeacherExperience(List<TeacherExperience> teacherExperienceList,TeacherDetail teacherDetail)
	{
		TeacherExperience teacherExperience =new TeacherExperience();
		try {
			if(teacherExperienceList!=null)
			for(TeacherExperience teacherExperienceObj: teacherExperienceList){
				if(teacherExperienceObj.getTeacherId().equals(teacherDetail)){
					teacherExperience=teacherExperienceObj;
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherExperience;
	}
	
	public double getTEYear(List<TeacherExperience> teacherExperienceList,TeacherDetail teacherDetail)
	{
		double tEYear=0;
		try {
			if(teacherExperienceList!=null)
			for(TeacherExperience teacherExperienceObj: teacherExperienceList){
				if(teacherExperienceObj.getTeacherId().equals(teacherDetail)){
					if(teacherExperienceObj.getExpCertTeacherTraining()!=null)
						tEYear=teacherExperienceObj.getExpCertTeacherTraining();
					else
						tEYear=0;
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tEYear;
	}
	
	public TeacherAcademics getTeacherAcademics(List<TeacherAcademics> teacherAcademicsList,TeacherDetail teacherDetail)
	{
		TeacherAcademics teacherAcademics =new TeacherAcademics();
		try {
			if(teacherAcademicsList!=null)
			for(TeacherAcademics teacherAcademicsObj: teacherAcademicsList){
				if(teacherAcademicsObj.getTeacherId().equals(teacherDetail)){
					if(teacherAcademicsObj.getDegreeId()!=null){
						if(teacherAcademicsObj.getDegreeId().getDegreeType().equals("B")){
							teacherAcademics=teacherAcademicsObj;
							break;
						}
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAcademics;
	}
	public int getSalary(List<TeacherPersonalInfo> lstTeacherPersonalInfo,TeacherDetail teacherDetail)
	{
		int salary=-1;
		try {
			if(lstTeacherPersonalInfo!=null)
			for(TeacherPersonalInfo teacherPersonalInfo: lstTeacherPersonalInfo){
				if(teacherPersonalInfo.getTeacherId().equals(teacherDetail.getTeacherId())){
					if(teacherPersonalInfo.getExpectedSalary()!=null)
						salary=teacherPersonalInfo.getExpectedSalary();
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return salary;
	}
	
	public DemoClassSchedule getDemoClassSchedule(List<DemoClassSchedule> demoClassScheduleList,TeacherDetail teacherDetail)
	{
		DemoClassSchedule demoClassSchedule =null;
		try {
			if(demoClassScheduleList!=null)
			for(DemoClassSchedule demoClassScheduleObj: demoClassScheduleList){
				if(demoClassScheduleObj.getTeacherId().equals(teacherDetail)){
					demoClassSchedule=demoClassScheduleObj;
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return demoClassSchedule;
	}
	public String getDemoClassScheduleStatus(List<DemoClassSchedule> demoClassScheduleList,TeacherDetail teacherDetail)
	{
		String demoStatus=null;
		try {
			if(demoClassScheduleList!=null)
			for(DemoClassSchedule demoClassScheduleObj: demoClassScheduleList){
				if(demoClassScheduleObj.getTeacherId().equals(teacherDetail)){
					if(demoClassScheduleObj.getDemoStatus().equalsIgnoreCase("Scheduled")){
						demoStatus=Utility.convertDateAndTimeToUSformatOnlyDate(demoClassScheduleObj.getDemoDate())+" "+demoClassScheduleObj.getDemoTime()+" "+demoClassScheduleObj.getTimeFormat();	
					}else if(demoClassScheduleObj.getDemoStatus().equalsIgnoreCase("Completed")){
						demoStatus="Completed";
					}else{
						demoStatus="N/A";
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return demoStatus;
	}
	public String getAchievementScore(List<TeacherAchievementScore> teacherAchievementScoreList,TeacherDetail teacherDetail)
	{
		String achievementScore=null;
		try {
			if(teacherAchievementScoreList!=null)
			for(TeacherAchievementScore teacherAchievementScoreObj: teacherAchievementScoreList){
				if(teacherAchievementScoreObj.getTeacherDetail().equals(teacherDetail)){
					achievementScore=teacherAchievementScoreObj.getTotalAchievementScore()+"/"+teacherAchievementScoreObj.getAchievementMaxScore();
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return achievementScore;
	}
	public String getAchievementMaxScore(List<TeacherAchievementScore> teacherAchievementScoreList,TeacherDetail teacherDetail,String achieveType)
	{
		String achievementScore=null;
		try {
			if(teacherAchievementScoreList!=null)
			for(TeacherAchievementScore teacherAchievementScoreObj: teacherAchievementScoreList){
				if(teacherAchievementScoreObj.getTeacherDetail().equals(teacherDetail)){
					if(achieveType.equalsIgnoreCase("A")){
						achievementScore=teacherAchievementScoreObj.getAcademicAchievementScore()+"/"+teacherAchievementScoreObj.getAcademicAchievementMaxScore();
					}else{
						achievementScore=teacherAchievementScoreObj.getLeadershipAchievementScore()+"/"+teacherAchievementScoreObj.getLeadershipAchievementMaxScore();	
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return achievementScore;
	}
	public String getFitScore(List<JobWiseConsolidatedTeacherScore> jobWiseConsolidatedTeacherScoreList,TeacherDetail teacherDetail)
	{
		String fitScore=null;
		try {
			if(jobWiseConsolidatedTeacherScoreList!=null)
			for(JobWiseConsolidatedTeacherScore jobWiseConsolidatedTeacherScoreObj: jobWiseConsolidatedTeacherScoreList){
				if(jobWiseConsolidatedTeacherScoreObj.getTeacherDetail().equals(teacherDetail)){
					fitScore=jobWiseConsolidatedTeacherScoreObj.getJobWiseConsolidatedScore()+"/"+jobWiseConsolidatedTeacherScoreObj.getJobWiseMaxScore();
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fitScore;
	}
	public String getTFA(List<TeacherExperience> teacherExperienceList,TeacherDetail teacherDetail)
	{
		String tFA=null;
		try {
			if(teacherExperienceList!=null)
			for(TeacherExperience teacherExperienceObj: teacherExperienceList){
				if(teacherExperienceObj.getTeacherId().equals(teacherDetail)){
					if(teacherExperienceObj.getTfaAffiliateMaster()!=null)
					tFA=teacherExperienceObj.getTfaAffiliateMaster().getTfaShortAffiliateName();
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tFA;
	}
	public StatusMaster findStatusByShortName(List <StatusMaster> statusMasterList,String statusShortName)
	{
		StatusMaster status= null;
		try 
		{   if(statusMasterList!=null)
			for(StatusMaster statusMaster: statusMasterList){
				if(statusMaster.getStatusShortName().equalsIgnoreCase(statusShortName)){
					status=statusMaster;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(status==null)
			return null;
		else
			return status;
	}
	public String getSecondaryStatusName(List<SecondaryStatus> secondaryStatusList,StatusMaster statusMaster)
	{
		String sStatusName=null;
		try {
			if(secondaryStatusList!=null)
			for(SecondaryStatus secondaryStatusObj: secondaryStatusList){
				if(secondaryStatusObj.getStatusMaster()!=null){
					if(secondaryStatusObj.getStatusMaster().getStatusId().equals(statusMaster.getStatusId())){
						
						sStatusName=secondaryStatusObj.getSecondaryStatusName();
						break;
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sStatusName;
	}
	public boolean  isVisible(JobOrder jobOrder,UserMaster userMaster,SchoolInJobOrder schoolInJobOrder){
		boolean doActivity = false;
		try{
			boolean isSchool=false;
			if(jobOrder.getSelectedSchoolsInDistrict()!=null){
				if(jobOrder.getSelectedSchoolsInDistrict()==1){
					isSchool=true;
				}
			}
			if(userMaster.getRoleId()!=null)
			if(userMaster.getRoleId().getRoleId()!=4 && userMaster.getRoleId().getRoleId()!=5 && userMaster.getRoleId().getRoleId()!=6  && userMaster.getRoleId().getRoleId()!=8){
				if((userMaster.getEntityType()==5 || userMaster.getEntityType()==6) && (userMaster.getRoleId().getRoleId()!=12 && userMaster.getRoleId().getRoleId()!=13)){	
					doActivity=true;
				}else if(jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2){	
					doActivity=true;
				}else if(jobOrder.getWritePrivilegeToSchool()!=null && jobOrder.getWritePrivilegeToSchool() && jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2 && isSchool){
					doActivity=true;
				}else if(jobOrder.getWritePrivilegeToSchool()!=null && jobOrder.getWritePrivilegeToSchool() && jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==3){
					doActivity=true;
				}else if(userMaster.getEntityType()==3){
					if(schoolInJobOrder==null){					
						doActivity = false;
					}else{
						doActivity=true;
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return doActivity;
	}
	public boolean  isUpdateStatus(JobForTeacher jobForTeacher,UserMaster userMaster,SchoolInJobOrder schoolInJobOrder){
		boolean doChange = false;
		JobOrder jobOrder =null;
		if(jobForTeacher!=null){
			jobOrder =jobForTeacher.getJobId();
		}
		try{
			boolean isSchoolIn=false;
			if(jobOrder.getSelectedSchoolsInDistrict()!=null){
				if(jobOrder.getSelectedSchoolsInDistrict()==1){
					isSchoolIn=true;
				}
			}
			boolean isHead=false;
			if(userMaster.getHeadQuarterMaster()!=null &&  jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId().equals(userMaster.getHeadQuarterMaster().getHeadQuarterId())){
				isHead=true;
			}
			boolean isBranch=false;
			if(userMaster.getBranchMaster()!=null && jobOrder.getBranchMaster()!=null &&  jobOrder.getBranchMaster().getBranchId().equals(userMaster.getBranchMaster().getBranchId())){
				isBranch=true;
			}
			boolean isDistrict=false;
			if(userMaster.getDistrictId()!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(userMaster.getDistrictId().getDistrictId())){
				isDistrict=true;
			}
			boolean isSchool=false;
			if(schoolInJobOrder!=null)
			if(userMaster.getSchoolId()!=null && schoolInJobOrder.getSchoolId()!=null && schoolInJobOrder.getSchoolId().equals(userMaster.getSchoolId())){
				isSchool=true;
			}
			
			if(isHead && userMaster.getEntityType()==5){
				doChange=true;
			}else if(isBranch && userMaster.getEntityType()==6){
				doChange=true;
			}else if(isDistrict && jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2){
				doChange=true;
			}else if(jobOrder.getWritePrivilegeToSchool()!=null && jobOrder.getWritePrivilegeToSchool() && isDistrict && jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2 && isSchoolIn){
				doChange=true;
			}else if(jobOrder.getWritePrivilegeToSchool()!=null && jobOrder.getWritePrivilegeToSchool() && isDistrict && isSchool && jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==3){
				doChange=true;
			}else if(userMaster.getEntityType()==3){
				if(schoolInJobOrder==null){					
					doChange = false;
				}else{
					doChange=true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return doChange;
	}
	public boolean  selectedStatusCheck(StatusMaster statusMaster,StatusMaster statusMasterPrev){
		boolean selectedStatus = false;
		try{
			int statusCounter=5;
			if(statusMasterPrev.getStatusShortName().equals("hird")){
				statusCounter=1;
			}else if(statusMasterPrev.getStatusShortName().equals("dcln")){
				statusCounter=1;
			}else if(statusMasterPrev.getStatusShortName().equals("rem")){
				statusCounter=1;
			}else if(statusMasterPrev.getStatusShortName().equals("widrw")){
				statusCounter=1;
			}else if(statusMasterPrev.getStatusShortName().equals("scomp")){
				statusCounter=2;
			}else if(statusMasterPrev.getStatusShortName().equals("ecomp")){
				statusCounter=3;
			}else if(statusMasterPrev.getStatusShortName().equals("vcomp")){
				statusCounter=4;
			}  
			if(statusMaster.getStatusShortName().equals("hird")&& statusCounter>0){
				selectedStatus=true; 
			}else if(statusMaster.getStatusShortName().equals("dcln")&& statusCounter>0){
				selectedStatus=true; 
			}else if(statusMaster.getStatusShortName().equals("rem")&& statusCounter>0){
				selectedStatus=true; 
			}else if(statusMaster.getStatusShortName().equals("widrw")&& statusCounter>0){
				selectedStatus=true; 
			}else if(statusMaster.getStatusShortName().equals("scomp")&& statusCounter==5){
				selectedStatus=true; 
			}else if(statusMaster.getStatusShortName().equals("ecomp")&& (statusCounter==5 || statusCounter==2)){
				selectedStatus=true; 
			}else if(statusMaster.getStatusShortName().equals("vcomp")&& (statusCounter==5 || statusCounter==3 || statusCounter==2)){
				selectedStatus=true; 
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return selectedStatus;
	}
	public boolean  selectedSecondaryStatusCheck(SecondaryStatus secondaryStatus,StatusMaster statusMasterPrev){
		boolean selectedStatus = false;
		try{
			int statusCounter=5;
			if(statusMasterPrev.getStatusShortName().equals("hird")){
				statusCounter=1;
			}else if(statusMasterPrev.getStatusShortName().equals("dcln")){
				statusCounter=1;
			}else if(statusMasterPrev.getStatusShortName().equals("rem")){
				statusCounter=1;
			}else if(statusMasterPrev.getStatusShortName().equals("widrw")){
				statusCounter=1;
			}else if(statusMasterPrev.getStatusShortName().equals("scomp")){
				statusCounter=2;
			}else if(statusMasterPrev.getStatusShortName().equals("ecomp")){
				statusCounter=3;
			}else if(statusMasterPrev.getStatusShortName().equals("vcomp")){
				statusCounter=4;
			}  
			if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==1 && statusCounter==5){
				selectedStatus=true; 
			}else if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==2 && (statusCounter==5 || statusCounter==2)){
				selectedStatus=true; 
			}else if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==3 && (statusCounter==5 || statusCounter==3 || statusCounter==2)){
				selectedStatus=true; 
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return selectedStatus;
	}
	public String getSecondaryStatusNameByDistrict(List<SecondaryStatus> secondaryStatusList,StatusMaster statusMaster,DistrictMaster districtMaster)
	{
		String sStatusName=null;
		try {
			if(secondaryStatusList!=null)
			for(SecondaryStatus secondaryStatusObj: secondaryStatusList){
				if(secondaryStatusObj.getStatusMaster()!=null){
					if(secondaryStatusObj.getStatusMaster().getStatusId().equals(statusMaster.getStatusId())&& secondaryStatusObj.getDistrictMaster().getDistrictId().equals(districtMaster.getDistrictId())){
						sStatusName=secondaryStatusObj.getSecondaryStatusName();
						break;
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sStatusName;
	}
	
	public Map<Integer, Boolean> getReferenceStatus(List<TeacherElectronicReferences> lstReferences)
	{
		Map<Integer,Boolean> mapReference = new TreeMap<Integer, Boolean>();
		try{
			if(lstReferences.size()>0)
			{
				for(TeacherElectronicReferences pojo: lstReferences ){													        			        
					if(pojo!=null)
						mapReference.put(pojo.getTeacherDetail().getTeacherId(), new Boolean(true));
				}
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return mapReference;
	}
	
	
	public String getResumeData(Map<Integer,Boolean> mapResume,Integer teacherId,Integer noOfRecordCheck,String windowFunc){
		StringBuffer sb=new StringBuffer();
		boolean isResume=false;
		try {
			isResume=mapResume.get(teacherId);
		} catch (Exception e) {
			
		}
		if(isResume)
			sb.append("&nbsp;<a data-original-title='"+lblResume+"' rel='tooltip' id='tpResume"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"downloadResume('"+teacherId+"','tpResume"+noOfRecordCheck+"');"+windowFunc+"\" ><span class='icon-briefcase icon-large iconcolor'></span></a>");
		else
			sb.append("&nbsp;<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpResume"+noOfRecordCheck+"' ><span class='icon-briefcase icon-large iconcolorhover'></span></a>");
		
		
		return sb.toString();
	}
	public boolean  selectedNotPriSecondaryStatusCheck(SecondaryStatus secondaryStatus,SecondaryStatus secondaryStatusPrev){
		boolean selectedStatus = false;
		try{
			if(secondaryStatus.getDistrictMaster()!=null){
				if(secondaryStatus.getDistrictMaster().equals(secondaryStatusPrev.getDistrictMaster()) && secondaryStatus.getJobCategoryMaster()!=null && secondaryStatus.getJobCategoryMaster().equals(secondaryStatusPrev.getJobCategoryMaster())){
					
					int statusCounterPrev=0;
					if(secondaryStatusPrev.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==1){
						statusCounterPrev=1; 
					}else if(secondaryStatusPrev.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==2){
						statusCounterPrev=2; 
					}else if(secondaryStatusPrev.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==3){
						statusCounterPrev=3; 
					}
					if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==1 && statusCounterPrev==1){
						if(secondaryStatus.getCreatedDateTime().after(secondaryStatusPrev.getCreatedDateTime())){
							selectedStatus=true; 
						}
						if(secondaryStatus.getDistrictMaster()!=null && secondaryStatus.getDistrictMaster().getHeadQuarterMaster()!=null && secondaryStatus.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2){
							if(selectedStatus==false){
								if(secondaryStatus.getSecondaryStatusId()>secondaryStatusPrev.getSecondaryStatusId()){
									selectedStatus=true; 
								}
							}
						}
					}else if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==2 && (statusCounterPrev==1 || statusCounterPrev==2)){
						if(statusCounterPrev==2 && secondaryStatus.getCreatedDateTime().after(secondaryStatusPrev.getCreatedDateTime())){
							selectedStatus=true; 
						}
						if(statusCounterPrev==1){
							selectedStatus=true; 
						}
						if(secondaryStatus.getDistrictMaster()!=null && secondaryStatus.getDistrictMaster().getHeadQuarterMaster()!=null && secondaryStatus.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2){
							if(selectedStatus==false){
								if(secondaryStatus.getSecondaryStatusId()>secondaryStatusPrev.getSecondaryStatusId()){
									selectedStatus=true; 
								}
							}
						}
					}else if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==3 && (statusCounterPrev==1 || statusCounterPrev==2 || statusCounterPrev==3 )){
						if(statusCounterPrev==3 && secondaryStatus.getCreatedDateTime().after(secondaryStatusPrev.getCreatedDateTime())){
							selectedStatus=true; 
						}
						if(statusCounterPrev!=3){
							selectedStatus=true; 
						}
						if(secondaryStatus.getDistrictMaster()!=null && secondaryStatus.getDistrictMaster().getHeadQuarterMaster()!=null && secondaryStatus.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2){
							if(selectedStatus==false){
								if(secondaryStatus.getSecondaryStatusId()>secondaryStatusPrev.getSecondaryStatusId()){
									selectedStatus=true; 
								}
							}
						}
					}
				}
			}else{
				if((secondaryStatus.getJobCategoryMaster()==null && secondaryStatusPrev.getJobCategoryMaster()==null && secondaryStatus.getHeadQuarterMaster().equals(secondaryStatusPrev.getHeadQuarterMaster())) || (secondaryStatus.getHeadQuarterMaster().equals(secondaryStatusPrev.getHeadQuarterMaster()) && secondaryStatus.getJobCategoryMaster().equals(secondaryStatusPrev.getJobCategoryMaster()))){
					
					int statusCounterPrev=0;
					if(secondaryStatusPrev.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==1){
						statusCounterPrev=1; 
					}else if(secondaryStatusPrev.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==2){
						statusCounterPrev=2; 
					}else if(secondaryStatusPrev.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==3){
						statusCounterPrev=3; 
					}
					if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==1 && statusCounterPrev==1){
						if(secondaryStatus.getCreatedDateTime().after(secondaryStatusPrev.getCreatedDateTime())){
							selectedStatus=true; 
						}
						if(secondaryStatus.getDistrictMaster()!=null && secondaryStatus.getDistrictMaster().getHeadQuarterMaster()!=null && secondaryStatus.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2){
							if(selectedStatus==false){
								if(secondaryStatus.getSecondaryStatusId()>secondaryStatusPrev.getSecondaryStatusId()){
									selectedStatus=true; 
								}
							}
						}
					}else if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==2 && (statusCounterPrev==1 || statusCounterPrev==2)){
						if(statusCounterPrev==2 && secondaryStatus.getCreatedDateTime().after(secondaryStatusPrev.getCreatedDateTime())){
							selectedStatus=true; 
						}
						if(statusCounterPrev==1){
							selectedStatus=true; 
						}
						if(secondaryStatus.getDistrictMaster()!=null && secondaryStatus.getDistrictMaster().getHeadQuarterMaster()!=null && secondaryStatus.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2){
							if(selectedStatus==false){
								if(secondaryStatus.getSecondaryStatusId()>secondaryStatusPrev.getSecondaryStatusId()){
									selectedStatus=true; 
								}
							}
						}
					}else if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==3 && (statusCounterPrev==1 || statusCounterPrev==2 || statusCounterPrev==3 )){
						if(statusCounterPrev==3 && secondaryStatus.getCreatedDateTime().after(secondaryStatusPrev.getCreatedDateTime())){
							selectedStatus=true; 
						}
						if(statusCounterPrev!=3){
							selectedStatus=true; 
						}
						if(secondaryStatus.getDistrictMaster()!=null && secondaryStatus.getDistrictMaster().getHeadQuarterMaster()!=null && secondaryStatus.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2){
							if(selectedStatus==false){
								if(secondaryStatus.getSecondaryStatusId()>secondaryStatusPrev.getSecondaryStatusId()){
									selectedStatus=true; 
								}
							}
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return selectedStatus;
	}
	public boolean  selectedPriSecondaryStatusCheck(SecondaryStatus secondaryStatus,SecondaryStatus secondaryStatusPrev){
		boolean selectedStatus = false;
		try{
			if(secondaryStatus.getDistrictMaster()!=null && secondaryStatus.getDistrictMaster().equals(secondaryStatusPrev.getDistrictMaster()) && secondaryStatus.getJobCategoryMaster().equals(secondaryStatusPrev.getJobCategoryMaster())){
				
				int statusCounterPrev=0;
				if(secondaryStatusPrev.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==1){
					statusCounterPrev=1; 
				}else if(secondaryStatusPrev.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==2){
					statusCounterPrev=2; 
				}else if(secondaryStatusPrev.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==3){
					statusCounterPrev=3; 
				}
				if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==1 && statusCounterPrev==1){
						selectedStatus=true; 
				}else if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==2 && (statusCounterPrev==1 || statusCounterPrev==2)){
						selectedStatus=true; 
				}else if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==3 && (statusCounterPrev==1 || statusCounterPrev==2 || statusCounterPrev==3 )){
						selectedStatus=true; 
				}
			}else if((secondaryStatus.getJobCategoryMaster()==null && secondaryStatusPrev.getJobCategoryMaster()==null && secondaryStatus.getHeadQuarterMaster().equals(secondaryStatusPrev.getHeadQuarterMaster())) || (secondaryStatus.getHeadQuarterMaster().equals(secondaryStatusPrev.getHeadQuarterMaster()) && secondaryStatus.getJobCategoryMaster().equals(secondaryStatusPrev.getJobCategoryMaster()))){
				
				int statusCounterPrev=0;
				if(secondaryStatusPrev.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==1){
					statusCounterPrev=1; 
				}else if(secondaryStatusPrev.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==2){
					statusCounterPrev=2; 
				}else if(secondaryStatusPrev.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==3){
					statusCounterPrev=3; 
				}
				if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==1 && statusCounterPrev==1){
						selectedStatus=true; 
				}else if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==2 && (statusCounterPrev==1 || statusCounterPrev==2)){
						selectedStatus=true; 
				}else if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==3 && (statusCounterPrev==1 || statusCounterPrev==2 || statusCounterPrev==3 )){
						selectedStatus=true; 
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return selectedStatus;
	}
	
	public boolean  priSecondaryStatusCheck(StatusMaster statusMaster,SecondaryStatus secondaryStatus){
		boolean selectedStatus = true;
		try{
			if(statusMaster!=null){
				if(statusMaster.getStatusShortName().equals("scomp")){
					if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==1){
						selectedStatus=false; 
					}
				}
				if(statusMaster.getStatusShortName().equals("ecomp")){
					if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==1 || secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==2){
						selectedStatus=false; 
					}
				}
				if(statusMaster.getStatusShortName().equals("vcomp") || statusMaster.getStatusShortName().equals("hird") || statusMaster.getStatusShortName().equals("dcln") || statusMaster.getStatusShortName().equals("rem") || statusMaster.getStatusShortName().equals("widrw")){
					selectedStatus=false; 
				}
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return selectedStatus;
	}
	public String getPanelScheduleStatus(List<PanelSchedule> panelScheduleList,TeacherDetail teacherDetail)
	{
		String panelStatus=null;
		List<PanelSchedule> tpanelScheduleList = new ArrayList<PanelSchedule>(); 
		try {
			if(panelScheduleList!=null)
			for(PanelSchedule panelScheduleObj: panelScheduleList){
				if(panelScheduleObj.getTeacherDetail().equals(teacherDetail)){
					tpanelScheduleList.add(panelScheduleObj);
				}
			}	
			int s=0;
			int c=0;
			int na=0;
			int size =tpanelScheduleList.size();
			for(PanelSchedule tpanelScheduleObj: tpanelScheduleList){
				if(tpanelScheduleObj.getPanelStatus().equalsIgnoreCase("Scheduled"))
					s++;
				else if(tpanelScheduleObj.getPanelStatus().equalsIgnoreCase("Completed"))
					c++;
				else
					na++;
			}
			if(size>0)
			{
				if(na==size){
					panelStatus="N/A";
				}else if(c==size){
					panelStatus="Completed";
				}else if(s==1){
					PanelSchedule tpanelSchedule = tpanelScheduleList.get(0);
					panelStatus=Utility.convertDateAndTimeToUSformatOnlyDate(tpanelSchedule.getPanelDate())+" "+tpanelSchedule.getPanelTime()+" "+tpanelSchedule.getTimeFormat();
				}else
					panelStatus="Scheduled";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return panelStatus;
	}
	public List<TeacherAssessmentStatus> findTASByTeacherAndAssessment(List<TeacherAssessmentStatus> teacherAssessmentStatusLst,TeacherDetail teacherDetail, AssessmentDetail assessmentDetail)
	{
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = new ArrayList<TeacherAssessmentStatus>();
		try 
		{
			if(teacherAssessmentStatusLst!=null)
			for(TeacherAssessmentStatus teacherAssessmentStatus: teacherAssessmentStatusLst){
				if(teacherAssessmentStatus.getTeacherDetail().equals(teacherDetail) && teacherAssessmentStatus.getAssessmentDetail().equals(assessmentDetail)){
					lstTeacherAssessmentStatus.add(teacherAssessmentStatus);
				}
			}	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherAssessmentStatus;
	}
	public List<AssessmentJobRelation> findRelationByJobOrder(List<AssessmentJobRelation> lstAssessmentJobRelation,JobOrder jobOrder) 
	{
		List<AssessmentJobRelation> assessmentJobRelations = new ArrayList<AssessmentJobRelation>();
		try {
			if(lstAssessmentJobRelation!=null)
			for(AssessmentJobRelation assessmentJobRelation: lstAssessmentJobRelation){
				if(assessmentJobRelation.getJobId().equals(jobOrder) && assessmentJobRelation.getStatus().equalsIgnoreCase("A")){
						if(jobOrder.getIsJobAssessment() && jobOrder.getStatus().equalsIgnoreCase("A") && jobOrder.getJobStatus().equalsIgnoreCase("o")){
							if(Utility.isDateValidGtLt(getJobFormatDate(jobOrder.getJobStartDate()),getJobFormatDate(new Date()), 0, 1)!=1 && Utility.isDateValidGtLt(getJobFormatDate(jobOrder.getJobEndDate()),getJobFormatDate(new Date()), 1, 1)!=1){
								assessmentJobRelations.add(assessmentJobRelation);
							}
						}
					
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentJobRelations;
	}
	public static String getJobFormatDate(Date dat)
	{
		try 
		{
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			return sdf.format(dat);
		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
			return "";
		}
	}
	public List<TeacherAssessmentStatus> findAssessmentTaken(List<TeacherAssessmentStatus> teacherAssessmentStatusLst,TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			if(teacherAssessmentStatusLst!=null)
			for(TeacherAssessmentStatus teacherAssessmentStatus: teacherAssessmentStatusLst){
				JobOrder jobOrderObj=teacherAssessmentStatus.getJobOrder();
				if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0){
					
					if(teacherDetail==null){
						if(jobOrderObj==null){
							teacherAssessmentStatusList.add(teacherAssessmentStatus);
						}
					}
					else{
						if(teacherAssessmentStatus.getTeacherDetail().equals(teacherDetail) && jobOrderObj==null){
							teacherAssessmentStatusList.add(teacherAssessmentStatus);
						}
					}
				}else{
					if(teacherDetail==null){
						if(jobOrderObj.equals(jobOrder)){
							teacherAssessmentStatusList.add(teacherAssessmentStatus);
						}
					}
					else{
						if(teacherAssessmentStatus.getTeacherDetail().equals(teacherDetail) && jobOrderObj.equals(jobOrder)){
							teacherAssessmentStatusList.add(teacherAssessmentStatus);
						}
					}
					
				}
			}	
		}catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	
	public String getSecondaryStatusNameByDistrictAndJobCategory(List<SecondaryStatus> secondaryStatusList,StatusMaster statusMaster,JobOrder jobOrder)
	{
		String sStatusName="";
		try {
			if(secondaryStatusList!=null)
			{
				for(SecondaryStatus secondaryStatusObj: secondaryStatusList)
				{
					if(secondaryStatusObj.getStatusMaster()!=null && secondaryStatusObj.getJobCategoryMaster()!=null)
					{
						if(jobOrder.getHeadQuarterMaster()!=null){
							if(secondaryStatusObj.getStatusMaster().getStatusId().equals(statusMaster.getStatusId())&& secondaryStatusObj.getJobCategoryMaster().equals(jobOrder.getJobCategoryMaster().getParentJobCategoryId()))
							{
								sStatusName=secondaryStatusObj.getSecondaryStatusName();
								break;
							}
						}else{
							if(secondaryStatusObj.getStatusMaster().getStatusId().equals(statusMaster.getStatusId())&& secondaryStatusObj.getJobCategoryMaster().equals(jobOrder.getJobCategoryMaster()))
							{
								sStatusName=secondaryStatusObj.getSecondaryStatusName();
								break;
							}
						}
					}
				}
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sStatusName;
	}
	public int getAllStatusCG(List<SecondaryStatus> lstuserChildren,Map<String,Integer> statusMap,Map<String,String> statusNameMap,int counter)
	{
		Collections.sort(lstuserChildren,SecondaryStatus.secondaryStatusOrder);
		int banchmarkStatus=0;
		try{
			for(SecondaryStatus subfL: lstuserChildren ){
				if(subfL.getStatus().equalsIgnoreCase("A")){
					if(subfL.getStatusMaster()!=null){
						if(subfL.getStatusMaster().getBanchmarkStatus()!=null){
							banchmarkStatus=subfL.getStatusMaster().getBanchmarkStatus();
						}
					}
					if(banchmarkStatus==0){	
						if(subfL.getStatusMaster()!=null){
							counter++;
							statusMap.put(subfL.getStatusMaster().getStatusId()+"##0",counter);
							statusNameMap.put(subfL.getStatusMaster().getStatusId()+"##0",subfL.getSecondaryStatusName());
						}
					}else if(subfL.getStatusMaster()==null)
					{
						counter++;
						statusMap.put("0##"+subfL.getSecondaryStatusId(),counter);
						statusNameMap.put("0##"+subfL.getSecondaryStatusId(),subfL.getSecondaryStatusName());
					}
				}
			}
			
			for(SecondaryStatus subfL: lstuserChildren){
				if(subfL.getStatus().equalsIgnoreCase("A"))
					if(subfL.getStatusMaster()!=null){
						if(subfL.getStatusMaster().getStatusShortName().equals("vcomp")||subfL.getStatusMaster().getStatusShortName().equals("ecomp")||subfL.getStatusMaster().getStatusShortName().equals("scomp"))
						{
							counter++;
							statusMap.put(subfL.getStatusMaster().getStatusId()+"##0",counter);
							statusNameMap.put(subfL.getStatusMaster().getStatusId()+"##0",subfL.getSecondaryStatusName());
						} 
					}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return counter;
	}
	public String getSchoolStatus(List<String> statusIds,Map<String,Integer> statusMap,Map<String,String> statusNameMap)
	{
		int count=0;
		String statusIdforSchool=null;
		String statusNameforSchool=null;
		try{
			for (String statusId : statusIds) {
				if(statusMap.get(statusId)>count){
					count=statusMap.get(statusId);
					statusIdforSchool=statusId;
				}
			}
			if(statusIdforSchool!=null){
				statusNameforSchool=statusNameMap.get(statusIdforSchool);
			}
		}catch(Exception e){}
		return statusNameforSchool;
	}
	
	public String[] getStatusForFitScore(List objectList,List objectListOnlineActivity,TeacherDetail teacherDetailObj)
	{
		String[] arrMix=new String[1];
		try{
			List aList = (List)objectList.get(0);
			List bList = (List)objectList.get(1);
			List cList = new ArrayList();//(List) objectListOnlineActivity.get(0);
			if(objectListOnlineActivity!=null && objectListOnlineActivity.size()>0)
				cList = (List) objectListOnlineActivity.get(0);
			arrMix[0]+="<table>";
			Iterator itr = aList.iterator();
			if(aList.size()>0){
				while(itr.hasNext()){
					Object[] obj = (Object[]) itr.next();
					StatusMaster sMaster=(StatusMaster)obj[0];
					TeacherDetail teacherDetail=(TeacherDetail)obj[1];
					Double val1= (Double) obj[2];
					Double val2= (Double) obj[3];
					if(teacherDetail.getTeacherId().equals(teacherDetailObj.getTeacherId())){
						if(sMaster.getStatus().contains("Online Activity"))
						{
							arrMix[0]+="<tr><td>Online Activity Status: "+Math.round(val1)+"/"+Math.round(val2)+"</td></tr>";
						}
						else
						{
							arrMix[0]+="<tr><td>"+sMaster.getStatus()+": "+Math.round(val1)+"/"+Math.round(val2)+"</td></tr>";
						}
					}
				}
			}
			if(bList.size()>0){
				itr = bList.iterator();
				while(itr.hasNext()){
					Object[] obj = (Object[]) itr.next();
					SecondaryStatus secondaryStatus=(SecondaryStatus)obj[0];
					TeacherDetail teacherDetail=(TeacherDetail)obj[1];
					Double val1= (Double) obj[2];
					Double val2= (Double) obj[3];
					if(teacherDetail.getTeacherId().equals(teacherDetailObj.getTeacherId())){
						if(secondaryStatus.getSecondaryStatusName().contains("Online Activity"))
							arrMix[0]+="<tr><td>Online Activity Status: "+Math.round(val1)+"/"+Math.round(val2)+"</td></tr>";
						else
							arrMix[0]+="<tr><td>"+secondaryStatus.getSecondaryStatusName()+": "+Math.round(val1)+"/"+Math.round(val2)+"</td></tr>";
					}
				}
			}
			if(cList.size()>0){
				itr = cList.iterator();
				while(itr.hasNext()){
					Object[] obj = (Object[]) itr.next();
					//SecondaryStatus secondaryStatus=(SecondaryStatus)obj[0];
					TeacherDetail teacherDetail=(TeacherDetail)obj[0];
					Double val1= (Double) obj[1];
					Double val2= (Double) obj[2];
					if(teacherDetail.getTeacherId().equals(teacherDetailObj.getTeacherId())){
						arrMix[0]+="<tr><td>Online Activity Score: "+Math.round(val1)+"/"+Math.round(val2)+"</td></tr>";
					}
				}
			}
			arrMix[0]+="</table>";
		}catch(Exception e){e.printStackTrace();}
		return arrMix;
	}
	public int getAllStatusForHistory(Map<String,StatusMaster> statusMasterMap,Map<String ,SecondaryStatus> secondaryStatusMap,List<SecondaryStatus> lstuserChildren,Map<String,Integer> statusMap,LinkedHashMap<String,String> statusNameMap,int counter)
	{
		Collections.sort(lstuserChildren,SecondaryStatus.secondaryStatusOrder);
		int banchmarkStatus=0;
		try{
			for(SecondaryStatus subfL: lstuserChildren ){
				if(subfL.getStatus().equalsIgnoreCase("A")){
					if(subfL.getStatusMaster()!=null){
						if(subfL.getStatusMaster().getBanchmarkStatus()!=null){
							banchmarkStatus=subfL.getStatusMaster().getBanchmarkStatus();
						}
					}
					if(banchmarkStatus==0){	
						if(subfL.getStatusMaster()!=null){
							counter++;
							statusMap.put(subfL.getStatusMaster().getStatusId()+"##0",counter);
							statusMasterMap.put(subfL.getStatusMaster().getStatusId()+"##0",subfL.getStatusMaster());
							statusNameMap.put(subfL.getStatusMaster().getStatusId()+"##0",subfL.getSecondaryStatusName());
						}
					}else if(subfL.getStatusMaster()==null)
					{
						counter++;
						statusMap.put("0##"+subfL.getSecondaryStatusId(),counter);
						statusNameMap.put("0##"+subfL.getSecondaryStatusId(),subfL.getSecondaryStatusName());
						secondaryStatusMap.put("0##"+subfL.getSecondaryStatusId(),subfL);
					}
					
				}
			}
			
			for(SecondaryStatus subfL: lstuserChildren){
				if(subfL.getStatus().equalsIgnoreCase("A"))
					if(subfL.getStatusMaster()!=null){
						if(subfL.getStatusMaster().getStatusShortName().equals("vcomp")||subfL.getStatusMaster().getStatusShortName().equals("ecomp")||subfL.getStatusMaster().getStatusShortName().equals("scomp"))
						{
							counter++;
							statusMap.put(subfL.getStatusMaster().getStatusId()+"##0",counter);
							statusMasterMap.put(subfL.getStatusMaster().getStatusId()+"##0",subfL.getStatusMaster());
							statusNameMap.put(subfL.getStatusMaster().getStatusId()+"##0",subfL.getSecondaryStatusName());
						} 
					}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return counter;
	}
	public int getAllStatusForHistoryStatus(Map<String,String> statusSecMap,Map<String,StatusMaster> statusMasterMap,Map<String ,SecondaryStatus> secondaryStatusMap,List<SecondaryStatus> lstuserChildren,Map<String,Integer> statusMap,LinkedHashMap<String,String> statusNameMap,int counter)
	{
		Collections.sort(lstuserChildren,SecondaryStatus.secondaryStatusOrder);
		int banchmarkStatus=0;
		try{
			for(SecondaryStatus subfL: lstuserChildren ){
				if(subfL.getStatus().equalsIgnoreCase("A")){
					if(subfL.getStatusMaster()!=null){
						if(subfL.getStatusMaster().getBanchmarkStatus()!=null){
							banchmarkStatus=subfL.getStatusMaster().getBanchmarkStatus();
						}
					}
					if(banchmarkStatus==0){	
						if(subfL.getStatusMaster()!=null){
							if(statusSecMap.get(subfL.getStatusMaster().getStatusId()+"##0")!=null){
								counter++;
								statusMap.put(subfL.getStatusMaster().getStatusId()+"##0",counter);
								statusMasterMap.put(subfL.getStatusMaster().getStatusId()+"##0",subfL.getStatusMaster());
								statusNameMap.put(subfL.getStatusMaster().getStatusId()+"##0",subfL.getSecondaryStatusName());
							}
						}
					}else if(subfL.getStatusMaster()==null)
					{
						if(statusSecMap.get("0##"+subfL.getSecondaryStatusId())!=null){
							counter++;
							statusMap.put("0##"+subfL.getSecondaryStatusId(),counter);
							statusNameMap.put("0##"+subfL.getSecondaryStatusId(),subfL.getSecondaryStatusName());
							secondaryStatusMap.put("0##"+subfL.getSecondaryStatusId(),subfL);
						}
					}
					
				}
			}
			
			for(SecondaryStatus subfL: lstuserChildren){
				if(subfL.getStatus().equalsIgnoreCase("A"))
					if(subfL.getStatusMaster()!=null){
						if(subfL.getStatusMaster().getStatusShortName().equals("vcomp")||subfL.getStatusMaster().getStatusShortName().equals("ecomp")||subfL.getStatusMaster().getStatusShortName().equals("scomp"))
						{
							if(statusSecMap.get(subfL.getStatusMaster().getStatusId()+"##0")!=null){
								counter++;
								statusMap.put(subfL.getStatusMaster().getStatusId()+"##0",counter);
								statusMasterMap.put(subfL.getStatusMaster().getStatusId()+"##0",subfL.getStatusMaster());
								statusNameMap.put(subfL.getStatusMaster().getStatusId()+"##0",subfL.getSecondaryStatusName());
							}
						} 
					}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return counter;
	}
	public String getSchoolStatusId(List<String> statusIds,Map<String,Integer> statusMap,Map<String,String> statusNameMap)
	{
		int count=0;
		String statusIdforSchool=null;
		try{
			for (String statusId : statusIds) {
				if(statusMap.get(statusId)>count){
					count=statusMap.get(statusId);
					statusIdforSchool=statusId;
				}
			}
			
		}catch(Exception e){}
		return statusIdforSchool;
	}
	public Map<Integer,List<TeacherStatusHistoryForJob>> getHiredList(Map<String,String> reqMap,Map<String,DistrictRequisitionNumbers> dReqNoMap,List<TeacherDetail> lstTeacherDetails,List<TeacherStatusHistoryForJob> historyForJobList,JobOrder jobOrder){
		Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();	
		try{
			Map<Integer, List<TeacherStatusHistoryForJob>> historyForJobMap = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
			
			Map<String,List<Integer>> teacherWiseMap=new HashMap<String, List<Integer>>();
			for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobList) {
				int fullAndPartTimeJob=0;
				if(teacherStatusHistoryForJob.getJobOrder()!=null){
					String key=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"";
					String keyJT=teacherStatusHistoryForJob.getJobOrder().getJobId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId();
					List<Integer> tAlist = teacherWiseMap.get(key);
					if(tAlist==null ){
						try{
							String reqNo=reqMap.get(keyJT);
							if(reqNo!=null){
								DistrictRequisitionNumbers drnObj=dReqNoMap.get(reqNo);
								if(drnObj!=null && drnObj.getPosType()!=null){
									if(drnObj.getPosType().equalsIgnoreCase("F")){
										fullAndPartTimeJob=1;
									}else if(drnObj.getPosType().equalsIgnoreCase("P")){
										fullAndPartTimeJob=2;
									}
								}
							}else{
								fullAndPartTimeJob=1;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						List<Integer> jobs = new ArrayList<Integer>();
						jobs.add(fullAndPartTimeJob);
						teacherWiseMap.put(key, jobs);
					}else{
						try{
							String reqNo=reqMap.get(keyJT);
							if(reqNo!=null){
								DistrictRequisitionNumbers drnObj=dReqNoMap.get(reqNo);
								if(drnObj!=null && drnObj.getPosType()!=null){
									if(drnObj.getPosType().equalsIgnoreCase("F")){
										fullAndPartTimeJob=1;
									}else if(drnObj.getPosType().equalsIgnoreCase("P")){
										fullAndPartTimeJob=2;
									}
								}
							}else{
								fullAndPartTimeJob=1;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						tAlist.add(fullAndPartTimeJob);
						teacherWiseMap.put(key, tAlist);
					}
				}
			}
			
			for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobList) {
				int fullAndPartTimeJob=0;
				Integer teacherId = teacherStatusHistoryForJob.getTeacherDetail().getTeacherId();
				try{
					List<Integer> fullPartAlist=teacherWiseMap.get(teacherId+"");
					if(fullPartAlist.size()>0){
						for (int integerVal : fullPartAlist) {
							if(integerVal==1){
								fullAndPartTimeJob=1;
								break;
							}else{
								fullAndPartTimeJob=2;
							}
							
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(fullAndPartTimeJob==1){
					List<TeacherStatusHistoryForJob> tAlist = historyForJobMap.get(teacherId);
					if(tAlist==null){
						List<TeacherStatusHistoryForJob> jobs = new ArrayList<TeacherStatusHistoryForJob>();
						jobs.add(teacherStatusHistoryForJob);
						historyForJobMap.put(teacherId, jobs);
					}else{
						tAlist.add(teacherStatusHistoryForJob);
						historyForJobMap.put(teacherId, tAlist);
					}
				}else if(fullAndPartTimeJob==2){
					DistrictRequisitionNumbers drnObj=null;
					try{
						String reqNo=reqMap.get(teacherStatusHistoryForJob.getJobOrder().getJobId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
						if(reqNo!=null){
							drnObj=dReqNoMap.get(reqNo);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					if((drnObj!=null && drnObj.getPosType().equals("P")) && (jobOrder!=null && teacherStatusHistoryForJob.getJobOrder().getJobId().equals(jobOrder.getJobId()))){
						List<TeacherStatusHistoryForJob> tAlist = historyForJobMap.get(teacherId);
						if(tAlist==null){
							List<TeacherStatusHistoryForJob> jobs = new ArrayList<TeacherStatusHistoryForJob>();
							jobs.add(teacherStatusHistoryForJob);
							historyForJobMap.put(teacherId, jobs);
						}else{
							tAlist.add(teacherStatusHistoryForJob);
							historyForJobMap.put(teacherId, tAlist);
						}
					}
				}
			}
			for(TeacherDetail teacherDetailObj:lstTeacherDetails){
				if(historyForJobMap.get(teacherDetailObj.getTeacherId())!=null){
					mapForHiredTeachers.put(teacherDetailObj.getTeacherId(),historyForJobMap.get(teacherDetailObj.getTeacherId()));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return mapForHiredTeachers;
	}
	
	public Map<String, Boolean> getMapAlum(List<EmployeeMaster> employeeMasters,DistrictMaster districtMaster)
	{
		Map<String,Boolean> mapAlum = new HashMap<String, Boolean>();
		try{
			if(employeeMasters!=null && employeeMasters.size()>0 && districtMaster!=null)
			{
				for(EmployeeMaster employeeMaster: employeeMasters)
				{
					String sMapAlumKey="";
					if(employeeMaster.getFirstName()!=null && !employeeMaster.getFirstName().equalsIgnoreCase(""))
						sMapAlumKey=employeeMaster.getFirstName();
					if(employeeMaster.getLastName()!=null && !employeeMaster.getLastName().equalsIgnoreCase(""))
						sMapAlumKey=sMapAlumKey+"_"+employeeMaster.getLastName();
					if(employeeMaster.getSSN()!=null && !employeeMaster.getSSN().equalsIgnoreCase(""))
						sMapAlumKey=sMapAlumKey+"_"+employeeMaster.getSSN();
					
					sMapAlumKey=sMapAlumKey.toUpperCase();
					if(employeeMaster!=null && districtMaster!=null && employeeMaster.getDistrictMaster()!=null && employeeMaster.getDistrictMaster().getDistrictId().equals(districtMaster.getDistrictId()) && employeeMaster.getAlum()!=null && employeeMaster.getAlum().equalsIgnoreCase("Y") && sMapAlumKey!=null && !sMapAlumKey.equalsIgnoreCase(""))
						mapAlum.put(sMapAlumKey,true);
				}
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}		
		return mapAlum;
	}
	
	public String getAlum(TeacherPersonalInfo teacherPersonalInfo,Map<String,Boolean> mapAlum, int noOfRecordCheck)
	{
		String sReturnValue="";
		String sMapAlumKey="";
		if(teacherPersonalInfo!=null && mapAlum!=null)
		{	
			if(teacherPersonalInfo.getFirstName()!=null && !teacherPersonalInfo.getFirstName().equalsIgnoreCase(""))
				sMapAlumKey=teacherPersonalInfo.getFirstName();
			if(teacherPersonalInfo.getLastName()!=null && !teacherPersonalInfo.getLastName().equalsIgnoreCase(""))
				sMapAlumKey=sMapAlumKey+"_"+teacherPersonalInfo.getLastName();
			
			if(teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().equalsIgnoreCase(""))
			{
				String sSSN=teacherPersonalInfo.getSSN();
				try {
					sSSN = Utility.decodeBase64(sSSN);
				} catch (Exception e) {}
				sSSN = sSSN.substring(sSSN.length() - 4, sSSN.length());
				sMapAlumKey=sMapAlumKey+"_"+sSSN;
			}
			
			sMapAlumKey=sMapAlumKey.toUpperCase();
			if(!sMapAlumKey.equalsIgnoreCase(""))
				if(mapAlum!=null && mapAlum.get(sMapAlumKey)!=null)
				{
					sReturnValue="<a data-original-title='"+lblFormerEmployee1+"' rel='tooltip' id='alum"+noOfRecordCheck+"' href='javascript:void(0);'>&nbsp;Alum</a>";
					sReturnValue="&nbsp;<a data-original-title='"+lblFormerEmployee1+"' rel='tooltip' id='alum"+noOfRecordCheck+"' href='javascript:void(0);'><span class='fa-external-link-square icon-large iconcolorRed'></span></a>";
				}
		}
		return sReturnValue;
	}
	//Display Hired Teachers
	public String displayHiredTeacherList(Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers,Integer teacherId)
	{
		StringBuffer sb = new StringBuffer();

		try{
			List<TeacherStatusHistoryForJob> teacherStatusList = new ArrayList<TeacherStatusHistoryForJob>();
			teacherStatusList = mapForHiredTeachers.get(teacherId);
			
			sb.append("<table>");
			if(teacherStatusList.size()>0)
			{
				Collections.sort(teacherStatusList,new TeacherStatusHistoryForJobComparatorDesc());
				Map<Integer,Boolean> map = new HashMap<Integer, Boolean>(); 
				for(TeacherStatusHistoryForJob tdhfj:teacherStatusList)
				{	
					if(map.get(tdhfj.getJobOrder().getJobId())==null)
					{
						sb.append("<tr>");
						sb.append("<td>"+lblDateHiring1+" : "+Utility.convertDateAndTimeToUSformatOnlyDate(tdhfj.getHiredByDate())+"<br>Job Order : "+tdhfj.getJobOrder().getJobTitle()+"</td>");
						sb.append("</tr>");
					}
					map.put(tdhfj.getJobOrder().getJobId(), true);
				}
				map=null;
			}
			sb.append("</table>");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	//Display Hired Teachers school
	public String displayHiredTeacherListToolTip(Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers,Integer teacherId,Map<String,List<SchoolMaster>> hiredSchol)
	{
		StringBuffer sb = new StringBuffer();

		try{
			List<TeacherStatusHistoryForJob> teacherStatusList = new ArrayList<TeacherStatusHistoryForJob>();
			teacherStatusList = mapForHiredTeachers.get(teacherId);
			
			sb.append("<table>");
			if(teacherStatusList.size()>0)
			{
				Collections.sort(teacherStatusList,new TeacherStatusHistoryForJobComparatorDesc());
				Map<Integer,Boolean> map = new HashMap<Integer, Boolean>(); 
				for(TeacherStatusHistoryForJob tdhfj:teacherStatusList)
				{
					if(tdhfj.getStatus()!=null && tdhfj.getStatus().equalsIgnoreCase("A")){
						String schoolAndLoc="";
						if(hiredSchol.containsKey(tdhfj.getTeacherDetail().getTeacherId()+"#"+tdhfj.getJobOrder().getJobId())){
							List<SchoolMaster> schoolMaster=hiredSchol.get(tdhfj.getTeacherDetail().getTeacherId()+"#"+tdhfj.getJobOrder().getJobId());
							schoolAndLoc = "<BR>"+lblSchoolName+": "+schoolMaster.get(0).getSchoolName()+"<Br>Location Id: "+schoolMaster.get(0).getLocationCode();
						}
						
						if(map.get(tdhfj.getJobOrder().getJobId())==null)
						{
							sb.append("<tr>");
							sb.append("<td>"+lblDateHiring1+": "+Utility.convertDateAndTimeToUSformatOnlyDate(tdhfj.getHiredByDate())+"<br>Job Order: "+tdhfj.getJobOrder().getJobTitle()+schoolAndLoc+"</td>");
							sb.append("</tr>");
						}
						map.put(tdhfj.getJobOrder().getJobId(), true);
					}else{
						String schoolAndLoc="";
						if(hiredSchol.containsKey(tdhfj.getTeacherDetail().getTeacherId()+"#"+tdhfj.getJobOrder().getJobId())){
							List<SchoolMaster> schoolMaster=hiredSchol.get(tdhfj.getTeacherDetail().getTeacherId()+"#"+tdhfj.getJobOrder().getJobId());
							schoolAndLoc = "<BR>"+lblSchoolName+": "+schoolMaster.get(0).getSchoolName()+"<Br>Location Id: "+schoolMaster.get(0).getLocationCode();
						}
						
						if(map.get(tdhfj.getJobOrder().getJobId())==null)
						{
							sb.append("<tr>");
							sb.append("<td>Date of Offer Accepted: "+Utility.convertDateAndTimeToUSformatOnlyDate(tdhfj.getCreatedDateTime())+"<br>Job Order: "+tdhfj.getJobOrder().getJobTitle()+schoolAndLoc+"</td>");
							sb.append("</tr>");
						}
						map.put(tdhfj.getJobOrder().getJobId(), true);
					}
				}
				map=null;
			}
			sb.append("</table>");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	//Display Hired Teachers school
	public String displayHiredVcompTeacherListToolTip(Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers,Integer teacherId,Map<String,List<SchoolMaster>> hiredSchol,String offerText)
	{
		StringBuffer sb = new StringBuffer();

		try{
			List<TeacherStatusHistoryForJob> teacherStatusList = new ArrayList<TeacherStatusHistoryForJob>();
			teacherStatusList = mapForHiredTeachers.get(teacherId);
			
			sb.append("<table>");
			if(teacherStatusList.size()>0)
			{
				Collections.sort(teacherStatusList,new TeacherStatusHistoryForJobComparatorDesc());
				Map<Integer,Boolean> map = new HashMap<Integer, Boolean>(); 
				for(TeacherStatusHistoryForJob tdhfj:teacherStatusList)
				{
					if(tdhfj.getStatus()!=null && tdhfj.getStatus().equalsIgnoreCase("A")){
						String schoolAndLoc="";
						if(hiredSchol.containsKey(tdhfj.getTeacherDetail().getTeacherId()+"#"+tdhfj.getJobOrder().getJobId())){
							List<SchoolMaster> schoolMaster=hiredSchol.get(tdhfj.getTeacherDetail().getTeacherId()+"#"+tdhfj.getJobOrder().getJobId());
							schoolAndLoc = "<BR>"+lblSchoolName+": "+schoolMaster.get(0).getSchoolName()+"<Br>Location Id: "+schoolMaster.get(0).getLocationCode();
						}
						
						if(map.get(tdhfj.getJobOrder().getJobId())==null)
						{
							sb.append("<tr>");
							sb.append("<td>"+lblDateHiring1+": "+Utility.convertDateAndTimeToUSformatOnlyDate(tdhfj.getHiredByDate())+"<br>Job Order: "+tdhfj.getJobOrder().getJobTitle()+schoolAndLoc+"</td>");
							sb.append("</tr>");
						}
						map.put(tdhfj.getJobOrder().getJobId(), true);
					}else{
						String schoolAndLoc="";
						if(hiredSchol.containsKey(tdhfj.getTeacherDetail().getTeacherId()+"#"+tdhfj.getJobOrder().getJobId())){
							List<SchoolMaster> schoolMaster=hiredSchol.get(tdhfj.getTeacherDetail().getTeacherId()+"#"+tdhfj.getJobOrder().getJobId());
							schoolAndLoc = "<BR>"+lblSchoolName+": "+schoolMaster.get(0).getSchoolName()+"<Br>Location Id: "+schoolMaster.get(0).getLocationCode();
						}
						
						if(map.get(tdhfj.getJobOrder().getJobId())==null)
						{
							sb.append("<tr>");
							sb.append("<td>Date of "+offerText+": "+Utility.convertDateAndTimeToUSformatOnlyDate(tdhfj.getCreatedDateTime())+"<br>Job Order: "+tdhfj.getJobOrder().getJobTitle()+schoolAndLoc+"</td>");
							sb.append("</tr>");
						}
						map.put(tdhfj.getJobOrder().getJobId(), true);
					}
				}
				map=null;
			}
			sb.append("</table>");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	//Display Offer Accepted
	public boolean displayOfferAccepted(Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers,Integer teacherId,Map<String,List<SchoolMaster>> hiredSchol)
	{
		boolean offerAccepted=false;
		try{
			List<TeacherStatusHistoryForJob> teacherStatusList = new ArrayList<TeacherStatusHistoryForJob>();
			teacherStatusList = mapForHiredTeachers.get(teacherId);
			
			if(teacherStatusList.size()>0)
			{
				Collections.sort(teacherStatusList,new TeacherStatusHistoryForJobComparatorDesc());
				for(TeacherStatusHistoryForJob tdhfj:teacherStatusList)
				{
					if(tdhfj.getStatus()!=null && tdhfj.getStatus().equalsIgnoreCase("A")){
						offerAccepted=false;
						break;
					}else{
						offerAccepted=true;
					}
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return offerAccepted;
	}
	public Map<Integer,List<DistrictSpecificPortfolioAnswers>> findDistrictSpecificPortfolioAnswers(List<DistrictSpecificPortfolioAnswers> districtSpecificPortfolioAnswersList, JobOrder jobOrder)
	{	
		Map<Integer,List<DistrictSpecificPortfolioAnswers>> mapAnswers = new TreeMap<Integer, List<DistrictSpecificPortfolioAnswers>>();		
		try{
			if(districtSpecificPortfolioAnswersList.size()>0){
				for(DistrictSpecificPortfolioAnswers  districtSpecificPortfolioAnswers : districtSpecificPortfolioAnswersList){
					
					if(districtSpecificPortfolioAnswers.getDistrictMaster().getDistrictId()==7800038){
						if(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions()!=null && (districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()==3 || districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()==4)){
							int techerId=districtSpecificPortfolioAnswers.getTeacherDetail().getTeacherId();
							List<DistrictSpecificPortfolioAnswers> answers=mapAnswers.get(techerId);
							boolean teacherAnswerFlag=false;
							if(districtSpecificPortfolioAnswers.getJobOrder()!=null){
								if(districtSpecificPortfolioAnswers.getJobOrder().getJobId().equals(jobOrder.getJobId())){
									teacherAnswerFlag=true;
								}
							}else if(districtSpecificPortfolioAnswers.getJobCategoryMaster()!=null){
								if(districtSpecificPortfolioAnswers.getJobCategoryMaster().getJobCategoryId().equals(jobOrder.getJobCategoryMaster().getJobCategoryId())){
									teacherAnswerFlag=true;
								}
							}else if(districtSpecificPortfolioAnswers.getDistrictMaster()!=null){
								if(districtSpecificPortfolioAnswers.getDistrictMaster().getDistrictId().equals(jobOrder.getDistrictMaster().getDistrictId())){
									teacherAnswerFlag=true;	
								}
							} 
							//logger.info("teacherAnswerFlag::::::"+teacherAnswerFlag);
							if(teacherAnswerFlag){
								if(mapAnswers.get(techerId)==null){
									List<DistrictSpecificPortfolioAnswers> newList=new ArrayList<DistrictSpecificPortfolioAnswers>();
									newList.add(districtSpecificPortfolioAnswers);
									mapAnswers.put(techerId, newList);
								}else{
									answers.add(districtSpecificPortfolioAnswers);
									mapAnswers.put(techerId, answers);
								}
							}
						}
					}else{
						int techerId=districtSpecificPortfolioAnswers.getTeacherDetail().getTeacherId();
						List<DistrictSpecificPortfolioAnswers> answers=mapAnswers.get(techerId);
						boolean teacherAnswerFlag=false;
						if(districtSpecificPortfolioAnswers.getJobOrder()!=null){
							if(districtSpecificPortfolioAnswers.getJobOrder().getJobId().equals(jobOrder.getJobId())){
								teacherAnswerFlag=true;
							}
						}else if(districtSpecificPortfolioAnswers.getJobCategoryMaster()!=null){
							if(districtSpecificPortfolioAnswers.getJobCategoryMaster().getJobCategoryId().equals(jobOrder.getJobCategoryMaster().getJobCategoryId())){
								teacherAnswerFlag=true;
							}
						}else if(districtSpecificPortfolioAnswers.getDistrictMaster()!=null){
							if(districtSpecificPortfolioAnswers.getDistrictMaster().getDistrictId().equals(jobOrder.getDistrictMaster().getDistrictId())){
								teacherAnswerFlag=true;	
							}
						} 
						//logger.info("teacherAnswerFlag::::::"+teacherAnswerFlag);
						if(teacherAnswerFlag){
							if(mapAnswers.get(techerId)==null){
								List<DistrictSpecificPortfolioAnswers> newList=new ArrayList<DistrictSpecificPortfolioAnswers>();
								newList.add(districtSpecificPortfolioAnswers);
								mapAnswers.put(techerId, newList);
							}else{
								answers.add(districtSpecificPortfolioAnswers);
								mapAnswers.put(techerId, answers);
							}
						}
					}
					
				}
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return mapAnswers;
	}
	
	public String findCGDistrictSpecificPortfolioAnswers(List<DistrictSpecificPortfolioAnswers> districtSpecificPortfolioAnswersList,int questionId)
	{	
		String answerDetails="";	
		String selectedOptions = null;
		List<DistrictSpecificPortfolioOptions> questionOptionsList=new ArrayList<DistrictSpecificPortfolioOptions>();
		try{
			if(districtSpecificPortfolioAnswersList!=null){
				for(DistrictSpecificPortfolioAnswers  districtSpecificPortfolioAnswers : districtSpecificPortfolioAnswersList){
					if(districtSpecificPortfolioAnswers.getDistrictMaster().getDistrictId()==1200390){
						if(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()==2 && questionId==2){
							if(districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
								questionOptionsList=districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionOptions();
								selectedOptions=districtSpecificPortfolioAnswers.getSelectedOptions();
								String[] multiCounts = selectedOptions.split("\\|");
								String ansId="";
								boolean ansFlag=false;
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									try{
										if(ansFlag==false){
											for(int i=0;i<multiCounts.length;i++){
													try{ansId =multiCounts[i];}catch(Exception e){}
													if(ansId!=null && !ansId.equals("") && questionOptions.getOptionId()==Integer.parseInt(ansId)){
														if(ansId!=null && !ansId.equals("")){
															ansId=questionOptions.getQuestionOption();
														}
														ansFlag=true;
														break;
													}
											}
											if(ansId.equals("")){
												ansId="No";
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
								}
								if(!answerDetails.equals("") && !ansId.equals("")){
									answerDetails+=","+ansId;
								}else{
									answerDetails+=ansId;
								}
							}
						}
					}else if(districtSpecificPortfolioAnswers.getDistrictMaster().getDistrictId()==7800038){
						if(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()==3 && questionId==3){
							if(districtSpecificPortfolioAnswers.getSliderScore()!=null){
								answerDetails=districtSpecificPortfolioAnswers.getSliderScore().toString();
								break;
							}else{
								answerDetails="N/A";
							}
						}else if(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()==4 && questionId==4){
							if(districtSpecificPortfolioAnswers.getSliderScore()!=null){
								answerDetails=districtSpecificPortfolioAnswers.getSliderScore().toString();
								break;
							}else{
								answerDetails="N/A";
							}
						}
					}
				}
			}else{
				answerDetails="N/A";
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return answerDetails;
	}
	public String getPNQ(List<TeacherExperience> teacherExperienceList,TeacherDetail teacherDetail)
	{
		String tPNQ="<a data-original-title='"+lblPotentialNobleQuality+"' rel='tooltip' id='teacher_PNQ"+teacherDetail.getTeacherId()+"'><span class='fa-send icon-large iconcolorhover'></span></a> ";
		try {
			if(teacherExperienceList!=null)
			for(TeacherExperience teacherExperienceObj: teacherExperienceList){
				if(teacherExperienceObj.getTeacherId().getTeacherId().equals(teacherDetail.getTeacherId())){
					Integer pnqVal=teacherExperienceObj.getPotentialQuality();
					
					if(pnqVal!=null){
						if(pnqVal.equals(1)){
							tPNQ="<a data-original-title='"+lblPotentialNobleQuality+"' rel='tooltip' id='teacher_PNQ"+teacherDetail.getTeacherId()+"'><span class='fa-send icon-large iconcolor'></span></a> ";
						}else if(pnqVal.equals(2)){
							tPNQ="<a data-original-title='"+lblPotentialNobleQuality+"' rel='tooltip' id='teacher_PNQ"+teacherDetail.getTeacherId()+"'><span class='fa-send icon-large required'></span></a> ";
						}else{
							tPNQ="<a data-original-title='"+lblPotentialNobleQuality+"' rel='tooltip' id='teacher_PNQ"+teacherDetail.getTeacherId()+"'><span class='fa-send icon-large iconcolorhover'></span></a> ";
						}
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		tPNQ+="<script type='text/javascript'>$('#teacher_PNQ"+teacherDetail.getTeacherId()+"').tooltip();</script>";
		
		
		
		tPNQ+="<script type='text/javascript'>$('#teacher_PNQ"+teacherDetail.getTeacherId()+"').tooltip();</script>";
		
		return tPNQ;
	}
	
	
	public String getPNQForCG(List<TeacherExperience> teacherExperienceList,TeacherDetail teacherDetail)
	{
		String tPNQ="<span class='fa-send icon-large iconcolorhover'></span> ";
		try {
			if(teacherExperienceList!=null)
			for(TeacherExperience teacherExperienceObj: teacherExperienceList){
				if(teacherExperienceObj.getTeacherId().getTeacherId().equals(teacherDetail.getTeacherId())){
					Integer pnqVal=teacherExperienceObj.getPotentialQuality();
					
					if(pnqVal!=null){
						if(pnqVal.equals(1)){
							tPNQ="<span class='fa-send icon-large iconcolor'></span> ";
						}else if(pnqVal.equals(2)){
							tPNQ="<span class='fa-send icon-large required'></span>";
						}else{
							tPNQ="<span class='fa-send icon-large iconcolorhover'></span> ";
						}
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return tPNQ;
	}
	
	
	
	public Map<Integer,Integer> findVVIScore(List<I4InterviewInvites> i4InterviewInvitesList)
	{	
		Map<Integer,Integer> mapVVI = new HashMap<Integer, Integer>();		
		try{
			if(i4InterviewInvitesList.size()>0){
				for (I4InterviewInvites i4InterviewInvites : i4InterviewInvitesList) {
					if(i4InterviewInvites.getI4VideoInterviewScore()!=null){
						mapVVI.put(i4InterviewInvites.getTeacherDetail().getTeacherId(),i4InterviewInvites.getI4VideoInterviewScore());
					}
				}
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return mapVVI;
	}
	
	public Map<String,Integer> findSchoolSelection(List<SchoolSelectedByCandidate> schoolSelectedByCandidateList)
	{	
		Map<String,Integer> mapSS = new HashMap<String, Integer>();		
		try{
			if(schoolSelectedByCandidateList.size()>0){
				for (SchoolSelectedByCandidate schoolSelectedByCandidate : schoolSelectedByCandidateList) {
					String key=schoolSelectedByCandidate.getJobOrder().getJobId()+"##"+schoolSelectedByCandidate.getTeacherDetail().getTeacherId();
					if(mapSS.get(key)!=null){
						int keyVal=mapSS.get(key);
						mapSS.put(key,new Integer(++keyVal));
					}else{
						mapSS.put(key,new Integer(1));
					}
				}
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return mapSS;
	}
	public int getStatusForUndoHistoryStatus(List<SecondaryStatus> lstuserChildren,Map<String,Integer> statusHistMap,int counter)
	{
		//logger.info(":::::::::::getStatusForUndoHistoryStatus::::::::::");
		Collections.sort(lstuserChildren,SecondaryStatus.secondaryStatusOrder);
		int banchmarkStatus=0;
		try{
			for(SecondaryStatus subfL: lstuserChildren ){
				if(subfL.getStatus().equalsIgnoreCase("A")){
					if(subfL.getStatusMaster()!=null){
						if(subfL.getStatusMaster().getBanchmarkStatus()!=null){
							banchmarkStatus=subfL.getStatusMaster().getBanchmarkStatus();
						}
					}
					if(banchmarkStatus==0){	
						if(subfL.getStatusMaster()!=null){
								counter++;
								statusHistMap.put(subfL.getStatusMaster().getStatusId()+"##0",counter);
						}
					}else if(subfL.getStatusMaster()==null)
					{
							counter++;
							statusHistMap.put("0##"+subfL.getSecondaryStatusId(),counter);
					}
					
				}
			}
			
			for(SecondaryStatus subfL: lstuserChildren){
				if(subfL.getStatus().equalsIgnoreCase("A"))
					if(subfL.getStatusMaster()!=null){
						if(subfL.getStatusMaster().getStatusShortName().equals("vcomp")||subfL.getStatusMaster().getStatusShortName().equals("ecomp")||subfL.getStatusMaster().getStatusShortName().equals("scomp"))
						{
								counter++;
								statusHistMap.put(subfL.getStatusMaster().getStatusId()+"##0",counter);
						} 
					}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return counter;
	}
	
	public String displayPC(int entityID,EmployeeMaster employeeMaster,int noOfRecordCheck)
	{
		String sReturnValue="";
		if(entityID==2 && employeeMaster.getPCCode()!=null && employeeMaster.getPCCode().equalsIgnoreCase("Y"))
		{
			sReturnValue="<a data-original-title='PC' rel='tooltip' id='pc"+noOfRecordCheck+"' href='javascript:void(0);'>&nbsp;<img  src=\"images/PC-22.png\"></div></a> ";
		}
		return sReturnValue;
	}
	
	public String getStatusNodeColor(String statusName,TeacherDetail teacherDetail,Map<String,MQEvent> mapNodeStatus){
		
		MQEvent mqEventKSN = null;
		MQEvent mqEventKSNtemp = null;
		
		//node color
		
		String nodeColor = "Grey"; // 7F7F7F
		if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
			if(mqEventKSN!=null){
				/*if(mqEventKSN.getCheckEmailAPI()!=null && mqEventKSN.getCheckEmailAPI()){
					nodeColor = "Grey";
			    }else*/ if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && (mqEventKSN.getAckStatus()==null)){
					nodeColor="Yellow"; // fcd914
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
					nodeColor="Red"; // ff0000
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))){
					nodeColor="Blue"; // 243a6a
				}
			}
		}
				//INV WF1
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF1", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
			nodeColor = "Grey"; // 7F7F7F
			mqEventKSNtemp = mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
			if(mqEventKSN!=null){
				if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
					nodeColor="Red"; //red
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
					nodeColor="Red"; // ff0000
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getAckStatus()!=null && mqEventKSN.getAckStatus().contains("Success") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
					nodeColor="Yellow"; // fcd914
				}

			}
		}
				//WF1 COM
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblWF1COM", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
			mqEventKSNtemp = mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
			nodeColor = "Grey"; // 7F7F7F
			if(mqEventKSNtemp!=null){
				if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSNtemp.getWorkFlowStatusId()!=null  && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
					nodeColor = "Grey"; // 7F7F7F
				else if(mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null &&  mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
					nodeColor="Green"; // 009900
				}
				else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
					nodeColor="Green"; // 009900
				}
				else if(mqEventKSNtemp.getStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
					nodeColor="Red"; //red
				}
			}
			else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
				nodeColor="Green"; // 009900
			}
		}
				//INV WF2
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF2", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
			nodeColor = "Grey"; // 7F7F7F
			mqEventKSNtemp = mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
			if(mqEventKSN!=null){
				if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
					nodeColor="Green"; // 009900
				}/*else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
					nodeColor="Grey"; //gray
				}*/else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null &&  mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
					nodeColor="Red"; //red
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
					nodeColor="Red"; // ff0000
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getAckStatus()!=null && mqEventKSN.getAckStatus().contains("Success") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
					nodeColor="Yellow"; // fcd914
				}
			}
		}
				//WF2 COM
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblWF2COM", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
			mqEventKSNtemp = mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
			nodeColor = "Grey"; // 7F7F7F
			if(mqEventKSNtemp!=null){
				if(mqEventKSN!=null && mqEventKSN.getStatus()!=null  && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
					nodeColor = "Grey"; // 7F7F7F
				else if(mqEventKSNtemp.getStatus()!=null  && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSNtemp.getStatus()!=null && mqEventKSNtemp.getStatus().equalsIgnoreCase("R") && mqEventKSNtemp.getWorkFlowStatusId()!=null &&  mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
					nodeColor="Red"; //red
				}
				else if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
					nodeColor = "Green"; // 7F7F7F
				else if(mqEventKSNtemp.getStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
					nodeColor="Red"; //red
				}
			}
			else if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
				nodeColor = "Green"; // 7F7F7F
		}
				//INV WF3
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF3", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
			nodeColor = "Grey"; // 7F7F7F
			mqEventKSNtemp = mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
			if(mqEventKSN!=null){
				if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
					nodeColor="Red"; //red
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
					nodeColor="Red"; // ff0000
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getAckStatus()!=null && mqEventKSN.getAckStatus().contains("Success") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
					nodeColor="Yellow"; // fcd914
				}

			}
		}
				//WF3 COM
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblWF3COM", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
			mqEventKSNtemp = mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
			nodeColor = "Grey"; // 7F7F7F
			if(mqEventKSNtemp!=null){
				if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
					nodeColor = "Grey"; // 7F7F7F
				else if(mqEventKSNtemp!=null && mqEventKSNtemp.getStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
					nodeColor="Green"; // 009900
				}
				else if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
					nodeColor = "Green"; // 7F7F7F
				else if(mqEventKSNtemp.getStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
					nodeColor="Red"; //red
				}
			}
			else if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
				nodeColor = "Green"; // 7F7F7F
		}
		
		//SCR
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblScreening", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblScreening", locale));
			nodeColor = "Grey"; // 7F7F7F
			if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null){
				if(mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINPR", locale))){
					nodeColor="Yellow"; // fcd914
				}else if(mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblRERE", locale))){
					nodeColor="Blue"; // 243a6a
				}else if(mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))){
					nodeColor="Red"; // ff0000
				}
			}
		}
		
		//TAL TYP
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgHired2", locale))){
			nodeColor = "Grey"; // 7F7F7F
			if(mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale))!=null && !mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)).getStatus().equalsIgnoreCase("R"))
			{
				mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale));
				if(mqEventKSN!=null)
				{
					if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
						nodeColor="Green"; // 009900
					}else{
						nodeColor="Yellow"; // fcd914
					}
				}
			}
		}
				//WTHD
				/*nodeColor = "Grey"; // 7F7F7F
				if(teacher.getStatus().getStatusShortName().equals("widrw") && teacher.getStatusMaster().getStatusShortName().equals("widrw")){
				nodeColor="Green"; // 009900
				}*/

				//IN-E
				/*nodeColor = "Grey"; // 7F7F7F
				if(teacher.getStatus().getStatusShortName().equals("ielig") && teacher.getStatusMaster().getStatusShortName().equals("ielig")){
				nodeColor="Green"; // 009900
				}*/
			else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgTalentStatus", locale))){
				nodeColor = "Grey"; // 7F7F7F
				if(mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale))!=null && !mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)).getStatus().equalsIgnoreCase("R"))
				{
					mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
					if(mqEventKSN!=null)
					{
						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
							nodeColor="Orange"; // 009900
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R")){
							nodeColor="Grey"; // fcd914
						}
					}
				}
			}

				return nodeColor;
	}
	
	@Transactional(readOnly=false)
	public StatusNodeColorHistory saveStatusNodeColor(JobForTeacher jobForTeacher,MQEvent mqEvent,Map<String,MQEvent> mapNodeStatus,Map<String,Date> latestNodeColorDateMap){
		logger.info("<<<<<<<<<<<<<<<< saveStatusNodeColor >>>>>>>>>>>>>>>");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = null;
		if(context!=null)
			request = context.getHttpServletRequest();
		
		String color = "Grey";
		StatusNodeColorHistory statusNodeColorHistory = new StatusNodeColorHistory();
		try {
			String key = "";
			   if(jobForTeacher!=null){
				   
				   color = getStatusNodeColor(mqEvent.getEventType(),jobForTeacher.getTeacherId(),mapNodeStatus);
				   if(jobForTeacher.getJobId().getHeadQuarterMaster()!=null){
					   statusNodeColorHistory.setHeadQuarterMaster(jobForTeacher.getJobId().getHeadQuarterMaster());
					   statusNodeColorHistory.setBranchMaster(jobForTeacher.getJobId().getBranchMaster());
				   }
				   else{
					   statusNodeColorHistory.setDistrictMaster(jobForTeacher.getJobId().getDistrictMaster());
				   }
				   
				   statusNodeColorHistory.setTeacherDetail(jobForTeacher.getTeacherId());
			   }
			   if(mqEvent!=null)
				   key = mqEvent.getTeacherdetail().getTeacherId()+"##"+mqEvent.getEventType();
			   
			   if(latestNodeColorDateMap.size()>0 && latestNodeColorDateMap.get(key)!=null){
				   statusNodeColorHistory.setCreatedDateTime(latestNodeColorDateMap.get(key));
			   }
			   else{
				   statusNodeColorHistory.setCreatedDateTime(new Date());
			   }
			   
			   if(mqEvent.getStatusMaster()!=null)
				   statusNodeColorHistory.setStatusId(mqEvent.getStatusMaster().getStatusId());
			   else if(mqEvent.getSecondaryStatus()!=null)
				   statusNodeColorHistory.setSecondaryStatusId(mqEvent.getSecondaryStatus().getSecondaryStatusId());
			  
			   logger.info(" prev color :  "+color);
			   statusNodeColorHistory.setPrevColor(color);
			   statusNodeColorHistory.setCurrentColor(color);
			   
			   if(request!=null)
				   statusNodeColorHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
			   
			   if(mqEvent.getCreatedBy()!=null){
				   UserMaster userMaster = new UserMaster();
				   userMaster.setUserId(mqEvent.getCreatedBy());
				   statusNodeColorHistory.setUserMaster(userMaster);
			   }
			   
			   statusNodeColorHistory.setMqEvent(mqEvent);
			   
			   try {
				   ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());   
				   statusNodeColorHistoryDAO = (StatusNodeColorHistoryDAO)context0.getBean("statusNodeColorHistoryDAO");
				   statusNodeColorHistoryDAO.makePersistent(statusNodeColorHistory);
			} catch (Exception e) {
				e.printStackTrace();
			}
		   } catch (Exception e) {
			e.printStackTrace();
		}
		   return statusNodeColorHistory;
	}
	
	public String getNodeColorWithMqEvent(MQEvent mqEvent,String invWflsStatus){
		MQEvent mqEventKSN = null;
		//node color
		String nodeColor = "Grey"; // 7F7F7F
		ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());   
		mqEventDAO = (MQEventDAO)context0.getBean("mqEventDAO");
		try {
		if(mqEvent != null){	
			if(mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale))){
				if(mqEvent!=null){
					/*if(mqEvent.getCheckEmailAPI()!=null && mqEvent.getCheckEmailAPI()){
						nodeColor = "Grey";
				    }else*/ if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C")){
						nodeColor="Green"; // 009900
					}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && (mqEvent.getAckStatus()==null)){
						nodeColor="Yellow"; // fcd914
					}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().equalsIgnoreCase("Failed") || mqEvent.getAckStatus().equalsIgnoreCase("Failure"))){
						nodeColor="Red"; // ff0000
					}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().equalsIgnoreCase("Success") || mqEvent.getAckStatus().equalsIgnoreCase("Successful"))){
						nodeColor="Blue"; // 243a6a
					}
				}
			}
				//INV WF1
				else if(mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF1", locale))){
					nodeColor = "Grey"; // 7F7F7F
					List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEvent.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblWF1COM", locale));
					if(mqEvent!=null){
						if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C")){
							nodeColor="Green"; // 009900
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEventtemp!=null && mqEventtemp.size()>0 &&  mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							nodeColor="Red"; //red
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().equalsIgnoreCase("Failed") || mqEvent.getAckStatus().equalsIgnoreCase("Failure"))){
							nodeColor="Red"; // ff0000
						}else if(mqEvent.getStatus()!=null && mqEvent.getAckStatus()!=null && mqEvent.getAckStatus().contains("Success") && ((mqEvent.getWorkFlowStatusId()==null) || (mqEvent.getTmInitiate()!=null && mqEvent.getTmInitiate()))){
							nodeColor="Yellow"; // fcd914
						}

					}
				}
				//WF1 COM
				else if(mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblWF1COM", locale))){
					nodeColor = "Grey"; // 7F7F7F
					List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEvent.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblINVWF1", locale));
					if(mqEventtemp!=null && mqEventtemp.size()>0){
						if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
							nodeColor = "Grey"; // 7F7F7F
						else if(mqEventtemp.get(0).getStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equals(Utility.getLocaleValuePropByKey("lblCOM", locale)) && invWflsStatus!=null && invWflsStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="Green"; // 009900
						}
						else if(mqEvent!=null && mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C") && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
							nodeColor = "Green"; // 7F7F7F
						else if(mqEventtemp.get(0).getStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
							nodeColor="Red"; //red
						}
					}
					else if(mqEvent!=null && mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C") && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
						nodeColor = "Green"; // 7F7F7F
				}
				//INV WF2
				else if(mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF2", locale))){
					nodeColor = "Grey"; // 7F7F7F
					List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEvent.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblWF2COM", locale));
					if(mqEvent!=null){
						if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C")){
							nodeColor="Green"; // 009900
						}/*else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
							nodeColor="Grey"; //gray
						}*/else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEventtemp!=null && mqEventtemp.size()>0 &&  mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							nodeColor="Red"; //red
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().equalsIgnoreCase("Failed") || mqEvent.getAckStatus().equalsIgnoreCase("Failure"))){
							nodeColor="Red"; // ff0000
						}else if(mqEvent.getStatus()!=null && mqEvent.getAckStatus()!=null && mqEvent.getAckStatus().contains("Success") && ((mqEvent.getWorkFlowStatusId()==null) || (mqEvent.getTmInitiate()!=null && mqEvent.getTmInitiate()))){
							nodeColor="Yellow"; // fcd914
						}
					}
				}
				//WF2 COM
				else if(mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblWF2COM", locale))){
					nodeColor = "Grey"; // 7F7F7F
					List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEvent.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblINVWF2", locale));
					if(mqEvent!=null && mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C") && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus()!=null &&  mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						nodeColor = "Green"; // 7F7F7F
					}else if(mqEventtemp!=null && mqEventtemp.size()>0){
						if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus()!=null &&  mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
							nodeColor = "Grey"; // 7F7F7F
						else if(mqEventtemp.get(0).getStatus()!=null && invWflsStatus!=null && invWflsStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getStatus()!=null && mqEventtemp.get(0).getStatus().equalsIgnoreCase("R") && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
							nodeColor="Red"; //red
						}
						else if(mqEventtemp.get(0).getStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
							nodeColor="Red"; //red
						}
					}
					
				}
				//INV WF3
				else if(mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF3", locale))){
					nodeColor = "Grey"; // 7F7F7F
					List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEvent.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblWF3COM", locale));
					if(mqEvent!=null){
						if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C")){
							nodeColor="Green"; // 009900
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEventtemp!=null && mqEventtemp.size()>0 &&  mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							nodeColor="Red"; //red
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().equalsIgnoreCase("Failed") || mqEvent.getAckStatus().equalsIgnoreCase("Failure"))){
							nodeColor="Red"; // ff0000
						}else if(mqEvent.getStatus()!=null && mqEvent.getAckStatus()!=null && mqEvent.getAckStatus().contains("Success") && ((mqEvent.getWorkFlowStatusId()==null) || (mqEvent.getTmInitiate()!=null && mqEvent.getTmInitiate()))){
							nodeColor="Yellow"; // fcd914
						}

					}
				}
				//WF3 COM
				else if(mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblWF3COM", locale))){
					nodeColor = "Grey"; // 7F7F7F
					List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEvent.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblINVWF3", locale));
					if(mqEventtemp!=null && mqEventtemp.size()>0){
						if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
							nodeColor = "Grey"; // 7F7F7F
						else if(mqEventtemp.get(0).getStatus()!=null && invWflsStatus!=null && invWflsStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="Green"; // 009900
						}
						else if(mqEvent!=null && mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C") && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
							nodeColor = "Green"; // 7F7F7F
						else if(mqEventtemp.get(0).getStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
							nodeColor="Red"; //red
						}
					}
					else if(mqEvent!=null && mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C") && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
						nodeColor = "Green"; // 7F7F7F
				}

				//SCR
				else if(mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblScreening", locale))){
					nodeColor = "Grey"; // 7F7F7F
					if(mqEvent!=null && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus()!=null){
						if(mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINPR", locale))){
							nodeColor="Yellow"; // fcd914
						}else if(mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblRERE", locale))){
							nodeColor="Blue"; // 243a6a
						}else if(mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))){
							nodeColor="Red"; // ff0000
						}
					}
				}

				//TAL TYP
				else if(mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgHired2", locale))){
					nodeColor = "Grey"; // 7F7F7F
					if(mqEvent!=null)
					{
						if(mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
							nodeColor="Green"; // 009900
						}else{
							nodeColor="Yellow"; // fcd914
						}
					}
				}
				//WTHD
				/*nodeColor = "Grey"; // 7F7F7F
					if(teacher.getStatus().getStatusShortName().equals("widrw") && teacher.getStatusMaster().getStatusShortName().equals("widrw")){
					nodeColor="Green"; // 009900
					}*/

				//IN-E
				else if(mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgTalentStatus", locale))){
					nodeColor = "Grey"; // 7F7F7F
					if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C")){
						nodeColor="Orange"; 
					}
					else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R")){
						nodeColor="Grey"; 
					}
				}
				//IN-IE
				/*else if(mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgTalentStatus", locale))){
					nodeColor = "Grey"; // 7F7F7F
					if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C")){
					nodeColor="Orange"; 
					}
				}*/
		}	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return nodeColor;
	}
public synchronized static String getStatusNodeColor(String statusName,TeacherDetail teacherDetail,Map<String,MQEvent> mapNodeStatus,String locale){
		
		MQEvent mqEventKSN = null;
		MQEvent mqEventKSNtemp = null;
		
		//node color
		
		String nodeColor = "Grey"; // 7F7F7F
		if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
			if(mqEventKSN!=null){
				/*if(mqEventKSN.getCheckEmailAPI()!=null && mqEventKSN.getCheckEmailAPI()){
					nodeColor = "Grey";
			    }else*/ if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && (mqEventKSN.getAckStatus()==null)){
					nodeColor="Yellow"; // fcd914
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
					nodeColor="Red"; // ff0000
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))){
					nodeColor="Blue"; // 243a6a
				}
			}
		}
				//INV WF1
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF1", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
			nodeColor = "Grey"; // 7F7F7F
			mqEventKSNtemp = mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
			if(mqEventKSN!=null){
				if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
					nodeColor="Red"; //red
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
					nodeColor="Red"; // ff0000
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getAckStatus()!=null && mqEventKSN.getAckStatus().contains("Success") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
					nodeColor="Yellow"; // fcd914
				}

			}
		}
				//WF1 COM
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblWF1COM", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
			mqEventKSNtemp = mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
			nodeColor = "Grey"; // 7F7F7F
			if(mqEventKSNtemp!=null){
				if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSNtemp.getWorkFlowStatusId()!=null  && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
					nodeColor = "Grey"; // 7F7F7F
				else if(mqEventKSNtemp!=null  && mqEventKSNtemp.getWorkFlowStatusId()!=null &&  mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
					nodeColor="Green"; // 009900
				}
				else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
					nodeColor="Green"; // 009900
				}
				else if(mqEventKSNtemp.getStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
					nodeColor="Red"; //red
				}
			}
			else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
				nodeColor="Green"; // 009900
			}
		}
				//INV WF2
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF2", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
			nodeColor = "Grey"; // 7F7F7F
			mqEventKSNtemp = mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
			if(mqEventKSN!=null){
				if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
					nodeColor="Green"; // 009900
				}/*else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
					nodeColor="Grey"; //gray
				}*/else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null &&  mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
					nodeColor="Red"; //red
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
					nodeColor="Red"; // ff0000
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getAckStatus()!=null && mqEventKSN.getAckStatus().contains("Success") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
					nodeColor="Yellow"; // fcd914
				}
			}
		}
				//WF2 COM
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblWF2COM", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
			mqEventKSNtemp = mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
			nodeColor = "Grey"; // 7F7F7F
			if(mqEventKSNtemp!=null){
				if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
					nodeColor="Green"; // 009900
				else if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
					nodeColor = "Grey"; // 7F7F7F
				else if(mqEventKSNtemp.getStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSNtemp.getStatus()!=null && mqEventKSNtemp.getStatus().equalsIgnoreCase("R") && mqEventKSNtemp.getWorkFlowStatusId()!=null &&  mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
					nodeColor="Red"; //red
				}
				else if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
					nodeColor = "Green"; // 7F7F7F
				else if(mqEventKSNtemp.getStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
					nodeColor="Red"; //red
				}
			}
			else if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
				nodeColor = "Green"; // 7F7F7F
		}
				//INV WF3
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF3", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
			nodeColor = "Grey"; // 7F7F7F
			mqEventKSNtemp = mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
			if(mqEventKSN!=null){
				if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSNtemp!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
					nodeColor="Red"; //red
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
					nodeColor="Red"; // ff0000
				}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getAckStatus()!=null && mqEventKSN.getAckStatus().contains("Success") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
					nodeColor="Yellow"; // fcd914
				}

			}
		}
				//WF3 COM
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblWF3COM", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
			mqEventKSNtemp = mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
			nodeColor = "Grey"; // 7F7F7F
			if(mqEventKSNtemp!=null){
				if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
					nodeColor = "Grey"; // 7F7F7F
				else if(mqEventKSNtemp!=null && mqEventKSNtemp.getStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
					nodeColor="Green"; // 009900
				}
				else if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
					nodeColor = "Green"; // 7F7F7F
				else if(mqEventKSNtemp.getStatus()!=null && mqEventKSNtemp.getWorkFlowStatusId()!=null && mqEventKSNtemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
					nodeColor="Red"; //red
				}
			}
			else if(mqEventKSN!=null && mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
				nodeColor = "Green"; // 7F7F7F
		}
		
		//SCR
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblScreening", locale))){
			mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblScreening", locale));
			nodeColor = "Grey"; // 7F7F7F
			if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null){
				if(mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINPR", locale))){
					nodeColor="Yellow"; // fcd914
				}else if(mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblRERE", locale))){
					nodeColor="Blue"; // 243a6a
				}else if(mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))){
					nodeColor="Green"; // 009900
				}else if(mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))){
					nodeColor="Red"; // ff0000
				}
			}
		}
		
		//TAL TYP
		else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgHired2", locale))){
			nodeColor = "Grey"; // 7F7F7F
			if(mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale))!=null && !mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)).getStatus().equalsIgnoreCase("R"))
			{
				mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale));
				if(mqEventKSN!=null)
				{
					if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
						nodeColor="Green"; // 009900
					}else{
						nodeColor="Yellow"; // fcd914
					}
				}
			}
		}
				//WTHD
				/*nodeColor = "Grey"; // 7F7F7F
				if(teacher.getStatus().getStatusShortName().equals("widrw") && teacher.getStatusMaster().getStatusShortName().equals("widrw")){
				nodeColor="Green"; // 009900
				}*/

				//IN-E
				/*nodeColor = "Grey"; // 7F7F7F
				if(teacher.getStatus().getStatusShortName().equals("ielig") && teacher.getStatusMaster().getStatusShortName().equals("ielig")){
				nodeColor="Green"; // 009900
				}*/
			else if(statusName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgTalentStatus", locale))){
				nodeColor = "Grey"; // 7F7F7F
				if(mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale))!=null && !mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)).getStatus().equalsIgnoreCase("R"))
				{
					mqEventKSN=mapNodeStatus.get(teacherDetail.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
					if(mqEventKSN!=null)
					{
						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
							nodeColor="Orange"; // 009900
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R")){
							nodeColor="Grey"; // fcd914
						}
					}
				}
			}
				return nodeColor;
	}



///////////////////////////////////// Start :: Check Link To KSN , WF1, WF2 ////////////////////////////////////
	public boolean checkLinkToKSN(List<MQEvent> mqList)
	{
		boolean chk = true;
		try{
			if(mqList.size()>0){
				for(MQEvent mqEvent:mqList)
				{
					/*if(mqEvent!=null && mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)))
					{*/
							if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C")){
								//nodeColor="Green"; // 009900
							}else{
								chk = false;
								break;
								//No link to ksn
							}
					/*}else{
						chk = false;
						break;
					}*/
				}
			}else{
				chk = false;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return chk;
	}

	public boolean chkWF1(List<MQEvent> mqList)
	{
		boolean chk = true;
		try{
			if(mqList.size()>0)
			{
				for(MQEvent mqEvent:mqList)
				{
						/*if(mqEvent!=null && mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF1", locale))){*/
							//nodeColor = "Grey"; // 7F7F7F
							//List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEvent.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblWF1COM", locale));
								if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C")){
									//nodeColor="Green"; // 009900
								}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
									//nodeColor="Green"; // 009900
								}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
									//nodeColor="Green"; // 009900
								}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getWorkFlowStatusId()==null && (mqEvent.getAckStatus()==null || (mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().equalsIgnoreCase("Success") || mqEvent.getAckStatus().equalsIgnoreCase("Successful"))))){
									//nodeColor="Yellow"; // fcd914
								}else{
									chk = false;
									break;
									//No WF1
								}
						/*}else{
							chk = false;
							break;
						}*/
				}
			}else{
				chk = false;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return chk;
	}
	
	/*public boolean chkWF2(List<MQEvent> mqList)
	{
		boolean chk = true;
		try{
			for(MQEvent mqEvent:mqList)
			{
				if(mqEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF2", locale))){
					//nodeColor = "Grey"; // 7F7F7F
					//List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEvent.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblWF2COM", locale));
					if(mqEvent!=null){
						if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C")){
							//nodeColor="Green"; // 009900
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
							nodeColor="Grey"; //gray
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							//nodeColor="Green"; // 009900
						}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							//nodeColor="Green"; // 009900
						}if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getWorkFlowStatusId()==null && (mqEvent.getAckStatus()==null || (mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().equalsIgnoreCase("Success") || mqEvent.getAckStatus().equalsIgnoreCase("Successful"))))){
							//nodeColor="Yellow"; // fcd914
						}else{
							chk = false;
							break;
							//No WF2
						}
					}
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return chk;
	}*/
	

	public String getNodeColorWithMqEventforFlag(List<TeacherDetail> teacherList,int callerid)
	{
		boolean mainFlag = false;
		boolean ksnFlag = true;
		boolean wf1Flag = true;
		boolean wf2Flag = true;
		String msg = "no";
		
		
		try{
			
			ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());   
			mqEventDAO = (MQEventDAO)context0.getBean("mqEventDAO");
			
			List<MQEvent> mqList = mqEventDAO.findMQeacherList(teacherList);
			Map<String,List<MQEvent>> mapMq = new HashMap<String, List<MQEvent>>();
			List<MQEvent> mqKSN = new ArrayList<MQEvent>();
			List<MQEvent> wf1KSN = new ArrayList<MQEvent>();
			List<MQEvent> wf2KSN = new ArrayList<MQEvent>();
			
			for(MQEvent mq:mqList)
			{
				if(mq!=null && mq.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)))
					mqKSN.add(mq);
				if(mq!=null && mq.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF1", locale)))
					wf1KSN.add(mq);
				if(mq!=null && mq.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF2", locale)))
					wf2KSN.add(mq);
			}
			
			
			
			if(callerid==2)//For WF1
			{
				ksnFlag = checkLinkToKSN(mqKSN); // Check for Link to KSN
				
				if(ksnFlag){
					if(wf1KSN.size()>0){
						for(MQEvent mqEvent:wf1KSN)
						{
									//nodeColor = "Grey"; // 7F7F7F
									//List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEvent.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblWF1COM", locale));
										if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C")){
											//nodeColor="Green"; // 009900
											wf1Flag = false;
											break;
										}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
											//nodeColor="Green"; // 009900
											wf1Flag = false;
											break;
										}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
											//nodeColor="Green"; // 009900
											wf1Flag = false;
											break;
										}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getWorkFlowStatusId()==null && (mqEvent.getAckStatus()==null || (mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().equalsIgnoreCase("Success") || mqEvent.getAckStatus().equalsIgnoreCase("Successful"))))){
											//nodeColor="Yellow"; // fcd914
											wf1Flag = false;
											break;
										}else{
											//No WF1
										}
						}
					}
					
					if(wf1Flag)
						mainFlag = true;
				}else{
					msg = "3";
				}
			}
			
			if(callerid==3)//For WF2
			{
				ksnFlag = checkLinkToKSN(mqKSN); // Check for Link to KSN
				
				if(ksnFlag)
				{
					wf1Flag = chkWF1(wf1KSN); // Check for WF1
					
					if(wf1Flag)
					{
						if(wf2KSN.size()>0)
						{
							for(MQEvent mqEvent:wf2KSN)
							{
									//nodeColor = "Grey"; // 7F7F7F
									//List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEvent.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblWF2COM", locale));
										if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("C")){
											//nodeColor="Green"; // 009900
											wf2Flag = false;
											break;
										}/*else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
											nodeColor="Grey"; //gray
										}*/else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().contains("Success")) && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
											//nodeColor="Green"; // 009900
											wf2Flag = false;
											break;
										}else if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
											//nodeColor="Green"; // 009900
											wf2Flag = false;
											break;
										}if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R") && mqEvent.getWorkFlowStatusId()==null && (mqEvent.getAckStatus()==null || (mqEvent.getAckStatus()!=null && (mqEvent.getAckStatus().equalsIgnoreCase("Success") || mqEvent.getAckStatus().equalsIgnoreCase("Successful"))))){
											//nodeColor="Yellow"; // fcd914
											wf2Flag = false;
											break;
										}else{
											//No WF2
										}
							}
						}
						if(wf2Flag)
							mainFlag = true;
					}
				}else{
					msg = "3";
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	
		System.out.println(">>>>>>>>>> ::"+mainFlag+"##"+msg);
		
		return mainFlag+"##"+msg;
}
///////////////////////////////////// END :: Check Link To KSN , WF1, WF2 ////////////////////////////////////
	@Transactional(readOnly=false)
	public StatusNodeColorHistory saveStatusNodeColorForLinkToKSN(MQEvent mqEvent,String prevColor){
		logger.info("<<<<<<<<<<<<<<<< saveStatusNodeColorForLinkToKSN >>>>>>>>>>>>>>>");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = null;
		if(context!=null)
			request = context.getHttpServletRequest();
		
		StatusNodeColorHistory statusNodeColorHistory = new StatusNodeColorHistory();
		try {
				   HeadQuarterMaster headQuarterMaster=new HeadQuarterMaster();
				   headQuarterMaster.setHeadQuarterId(1);
				   statusNodeColorHistory.setHeadQuarterMaster(headQuarterMaster);
				   statusNodeColorHistory.setTeacherDetail(mqEvent.getTeacherdetail());
				   statusNodeColorHistory.setCreatedDateTime(new Date());
				   statusNodeColorHistory.setUpdateDateTime(new Date());
			   
			   if(mqEvent.getStatusMaster()!=null)
				   statusNodeColorHistory.setStatusId(mqEvent.getStatusMaster().getStatusId());
			   else if(mqEvent.getSecondaryStatus()!=null)
				   statusNodeColorHistory.setSecondaryStatusId(mqEvent.getSecondaryStatus().getSecondaryStatusId());
			  
			   if(prevColor!=null){
			      statusNodeColorHistory.setPrevColor(prevColor);
			   }else{
				   statusNodeColorHistory.setPrevColor(getNodeColorWithMqEvent(mqEvent, null));
			   }
			   	  statusNodeColorHistory.setCurrentColor("Yellow");
			   
			   if(request!=null)
				   statusNodeColorHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
			   
			   if(mqEvent.getCreatedBy()!=null){
				   UserMaster userMaster = new UserMaster();
				   userMaster.setUserId(mqEvent.getCreatedBy());
				   statusNodeColorHistory.setUserMaster(userMaster);
			   }
			   
			   statusNodeColorHistory.setMqEvent(mqEvent);
			   
			   try {
				   ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());   
				   statusNodeColorHistoryDAO = (StatusNodeColorHistoryDAO)context0.getBean("statusNodeColorHistoryDAO");
				   statusNodeColorHistoryDAO.makePersistent(statusNodeColorHistory);
			} catch (Exception e) {
				e.printStackTrace();
			}
		   } catch (Exception e) {
			e.printStackTrace();
		}
		   return statusNodeColorHistory;
	}
	
	@Transactional(readOnly=false)
	public Map<Integer,String> saveMQEvents(List<TeacherDetail> teacherDetails,UserMaster userMaster,Map<Integer,String> mapExistLinkToKsn){
		logger.info("<<<<<<<<<<<<<<<< saveMQEvents >>>>>>>>>>>>>>>");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		try {
			   ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());   
			   jobForTeacherDAO = (JobForTeacherDAO)context0.getBean("jobForTeacherDAO");
			   secondaryStatusDAO = (SecondaryStatusDAO)context0.getBean("secondaryStatusDAO");
			   mqEventDAO = (MQEventDAO)context0.getBean("mqEventDAO");
			   
			   
			   List<JobForTeacher> jobForTeachers=new ArrayList<JobForTeacher>();
			   List<JobCategoryMaster> allJobcatParentIDList=new ArrayList<JobCategoryMaster>();
			   Map<Integer,SecondaryStatus> secStatusMap=new HashMap<Integer, SecondaryStatus>();
		       HeadQuarterMaster headQuarterMaster=new HeadQuarterMaster();
		       headQuarterMaster.setHeadQuarterId(1);
		       jobForTeachers=jobForTeacherDAO.findByTeacherListHQAndBranch(teacherDetails, headQuarterMaster, null);
		       System.out.println("teacherDetails::::::::::::::::::::::>"+teacherDetails.size());
		       System.out.println("jobForTeachers:::::::::::::::::::"+jobForTeachers.size());
		       if(jobForTeachers.size()>0){
			       for(JobForTeacher jobFort:jobForTeachers){
			    	   if(jobFort.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
			    		   allJobcatParentIDList.add(jobFort.getJobId().getJobCategoryMaster().getParentJobCategoryId());
			    	   }else{
			    		   allJobcatParentIDList.add(jobFort.getJobId().getJobCategoryMaster());
			    	   }
			       }
			       List<SecondaryStatus> secondaryStatusList = secondaryStatusDAO.findByNameAndJobCategoryparentIDList(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale),allJobcatParentIDList);
			       if(secondaryStatusList.size()>0){
				        for(SecondaryStatus secStatus:secondaryStatusList){
				        	secStatusMap.put(secStatus.getJobCategoryMaster().getJobCategoryId(), secStatus);
				        }
			       }
			       
			       String LinkToKsn = Utility.getLocaleValuePropByKey("lblLinkToKsn", locale);
				   List<MQEvent> mqEventList=mqEventDAO.findMQTeacherListByNode(teacherDetails,LinkToKsn);
				   Map<Integer,MQEvent> mqMap=new HashMap<Integer, MQEvent>();
			       for (MQEvent mqEvent : mqEventList) {
			    	   mqMap.put(mqEvent.getTeacherdetail().getTeacherId(),mqEvent);
			       }
			       
			       SessionFactory sessionFactory=jobForTeacherDAO.getSessionFactory();
				   StatelessSession statelesSsessionMass = sessionFactory.openStatelessSession();
		           Transaction transactionMass =statelesSsessionMass.beginTransaction();   
		           
			       for(JobForTeacher jobForTeacher:jobForTeachers){
			    	   SecondaryStatus secondaryStatus=null;
			    	   if(jobForTeacher.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
			    		   secondaryStatus=secStatusMap.get(jobForTeacher.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
			    	   }else{
			    		   secondaryStatus=secStatusMap.get(jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryId());
			    	   }
			    	   MQEvent mqEvent = new MQEvent();
			    	   if(mqMap.get(jobForTeacher.getTeacherId().getTeacherId())==null){
						   mqEvent.setTeacherdetail(jobForTeacher.getTeacherId());
						   if(secondaryStatus!=null && secondaryStatus.getStatusMaster()!=null){
							   mqEvent.setStatusMaster(secondaryStatus.getStatusMaster());
							   mqEvent.setEventType(secondaryStatus.getStatusMaster().getStatus());
						   }else if(secondaryStatus!=null){
							   mqEvent.setSecondaryStatus(secondaryStatus);
							   mqEvent.setEventType(secondaryStatus.getSecondaryStatusName());
						   }else{
							   mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
						   }
						   mqEvent.setStatus("R");
						   mqEvent.setAckStatus(null);
						   mqEvent.setCreatedBy(userMaster.getUserId());
						   mqEvent.setCreatedDateTime(new Date());
						   mqEvent.setIpAddress(request.getRemoteAddr());
						   statelesSsessionMass.insert(mqEvent);
						   mapExistLinkToKsn.put(jobForTeacher.getTeacherId().getTeacherId(),"Grey");
						   mqMap.put(jobForTeacher.getTeacherId().getTeacherId(), mqEvent);
			       		}
			    	   else{
			    		   if(secondaryStatus!=null && secondaryStatus.getStatusMaster()!=null){
							   mqEvent.setStatusMaster(secondaryStatus.getStatusMaster());
							   mqEvent.setEventType(secondaryStatus.getStatusMaster().getStatus());
						   }else if(secondaryStatus!=null){
							   mqEvent.setSecondaryStatus(secondaryStatus);
							   mqEvent.setEventType(secondaryStatus.getSecondaryStatusName());
						   }
			    		   mqEvent=mqMap.get(jobForTeacher.getTeacherId().getTeacherId());
			    		   mapExistLinkToKsn.put(jobForTeacher.getTeacherId().getTeacherId(),getNodeColorWithMqEvent(mqEvent, null));
		       				mqEvent.setStatus("R");
		       				mqEvent.setAckStatus(null);
						   statelesSsessionMass.update(mqEvent);
						   
						   mqMap.put(jobForTeacher.getTeacherId().getTeacherId(), mqEvent);
		       		}
					   //mqEventDAO.makePersistent(mqEvent);
			       }
			       transactionMass.commit();
		       }
		} catch (Exception e) {
			e.printStackTrace();
		}
	 return mapExistLinkToKsn;
	}
	
	public StatusNodeColorHistory getSaveStatusNodeColor(JobForTeacher jobForTeacher,MQEvent mqEvent,Map<String,MQEvent> mapNodeStatus,Map<String,Date> latestNodeColorDateMap){
		logger.info("<<<<<<<<<<<<<<<< saveStatusNodeColor >>>>>>>>>>>>>>>");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = null;
		if(context!=null)
			request = context.getHttpServletRequest();
		
		String color = "Grey";
		StatusNodeColorHistory statusNodeColorHistory = new StatusNodeColorHistory();
		try {
			String key = "";
			   if(jobForTeacher!=null){
				   
				   color = getStatusNodeColor(mqEvent.getEventType(),jobForTeacher.getTeacherId(),mapNodeStatus,locale);
				   if(jobForTeacher.getJobId().getHeadQuarterMaster()!=null){
					   statusNodeColorHistory.setHeadQuarterMaster(jobForTeacher.getJobId().getHeadQuarterMaster());
					   statusNodeColorHistory.setBranchMaster(jobForTeacher.getJobId().getBranchMaster());
				   }
				   else{
					   statusNodeColorHistory.setDistrictMaster(jobForTeacher.getJobId().getDistrictMaster());
				   }
				   
				   statusNodeColorHistory.setTeacherDetail(jobForTeacher.getTeacherId());
			   }
			   if(mqEvent!=null)
				   key = mqEvent.getTeacherdetail().getTeacherId()+"##"+mqEvent.getEventType();
			   
			   if(latestNodeColorDateMap.size()>0 && latestNodeColorDateMap.get(key)!=null){
				   statusNodeColorHistory.setCreatedDateTime(latestNodeColorDateMap.get(key));
			   }
			   else{
				   statusNodeColorHistory.setCreatedDateTime(new Date());
			   }
			   
			   if(mqEvent.getStatusMaster()!=null)
				   statusNodeColorHistory.setStatusId(mqEvent.getStatusMaster().getStatusId());
			   else if(mqEvent.getSecondaryStatus()!=null)
				   statusNodeColorHistory.setSecondaryStatusId(mqEvent.getSecondaryStatus().getSecondaryStatusId());
			  
			   logger.info(" prev color :  "+color);
			   statusNodeColorHistory.setPrevColor(color);
			   statusNodeColorHistory.setCurrentColor(color);
			   
			   if(request!=null)
				   statusNodeColorHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
			   
			   if(mqEvent.getCreatedBy()!=null){
				   UserMaster userMaster = new UserMaster();
				   userMaster.setUserId(mqEvent.getCreatedBy());
				   statusNodeColorHistory.setUserMaster(userMaster);
			   }
			   
			   statusNodeColorHistory.setMqEvent(mqEvent);
			   return statusNodeColorHistory;
			
		   } catch (Exception e) {
			e.printStackTrace();
		}
		   return statusNodeColorHistory;
	}
}