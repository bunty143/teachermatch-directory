package tm.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

public class ApplicantPoolAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void SchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;

	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(
			TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	/* @Author: Gagan */
	public String displayApplicantGrid(String jobId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		int jobOrderType = 0;
		int roleId = 0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else
		{
			UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
			jobOrderType = userMaster.getEntityType();
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}

		String roleAccess=null;
		try{
			roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,41,"applicantpool.do",0);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		StringBuffer apRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("applicantName") && !sortOrder.equals("status") && !sortOrder.equals("hiredBy")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("applicantName")){
					sortOrderNoField="applicantName";
				}
				if(sortOrder.equals("status")){
					sortOrderNoField="status";
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			System.out.println("sortOrderStrVal::::"+sortOrderStrVal);
			System.out.println("sortOrderTypeVal::::"+sortOrderTypeVal);
			int noOfRecordCheck = 0;
			//System.out.println(" sortOrderTypeVal  sdgffsdg "+sortOrderTypeVal+" sortOrderStrVal "+sortOrderStrVal);
			//String jobId = request.getParameter("jobId")		==	null?"124":request.getParameter("jobId").trim();
			JobOrder jobOrder									=	jobOrderDAO.findById(new Integer(jobId), false, false);
			List<JobForTeacher> applicantJobForTeacher 			=	jobForTeacherDAO.findSortedJFTApplicantbyJobOrder(sortOrderStrVal,jobOrder);
			System.out.println("\n =======ApplicantJobForTeacher "+applicantJobForTeacher.size());

			List<JobForTeacher> sortedlstJobForTeacher		=	new ArrayList<JobForTeacher>();

			SortedMap<String,JobForTeacher>	sortedMap = new TreeMap<String,JobForTeacher>();
			if(sortOrderNoField.equals("applicantName"))
			{
				sortOrderFieldName	=	"applicantName";
			}
			if(sortOrderNoField.equals("status"))
			{
				sortOrderFieldName	=	"status";
			}
			int mapFlag=2;
			for (JobForTeacher jobForTeacher : applicantJobForTeacher){
				String orderFieldName=jobForTeacher.getCreatedDateTime()+"";
				if(sortOrderFieldName.equals("applicantName")){
					orderFieldName=jobForTeacher.getTeacherId().getFirstName()+"||"+jobForTeacher.getTeacherId().getLastName()+"||"+jobForTeacher.getJobForTeacherId();
					sortedMap.put(orderFieldName+"||",jobForTeacher);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("status")){
					orderFieldName=jobForTeacher.getStatus().getStatus()+"||"+jobForTeacher.getJobForTeacherId();
					sortedMap.put(orderFieldName+"||",jobForTeacher);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstJobForTeacher.add((JobForTeacher) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstJobForTeacher.add((JobForTeacher) sortedMap.get(key));
				}
			}else{
				sortedlstJobForTeacher=applicantJobForTeacher;
			}

			totalRecord =sortedlstJobForTeacher.size();

			if(totalRecord<end)
				end=totalRecord;
			List<JobForTeacher> lstsortedJobForTeacher		=	sortedlstJobForTeacher.subList(start,end);
			
			apRecords.append("<table  id='applicantTable' width='100%' border='0' >");
			apRecords.append("<thead class='bg'>");
		
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblApplicantName", locale),sortOrderFieldName,"applicantName",sortOrderTypeVal,pgNo);
			apRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblAppliedOn", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			apRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblStatus", locale),sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			apRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			apRecords.append("<th width=''>"+Utility.getLocaleValuePropByKey("lblDetails", locale) +"</th>");		
		
			apRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("pdp2", locale)+"</th>");

			apRecords.append("</tr>");
			apRecords.append("</thead>");
			//================= Checking If Record Not Found ======================
			if(applicantJobForTeacher.size()==0)
				apRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNoApplicantFound", locale)+"</td></tr>" );
			System.out.println( Utility.getLocaleValuePropByKey("lblNoRecord", locale) +applicantJobForTeacher.size());
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";

			///////// vishwanath //////
			Map<Integer,Integer> baseTakenMap = new HashMap<Integer, Integer>();
			if(applicantJobForTeacher.size()>0)
			{
				List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
				for (JobForTeacher jbforteacher : lstsortedJobForTeacher) 
					teacherDetails.add(jbforteacher.getTeacherId());

				List<TeacherAssessmentStatus> teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetails);
				try{
					for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {
						boolean cgflag = teacherAssessmentStatus2.getCgUpdated()==null?false:teacherAssessmentStatus2.getCgUpdated();
						if(cgflag && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
							baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			/////// vishwanath //////
			Integer isBase = null;
			for (JobForTeacher jbforteacher : lstsortedJobForTeacher) 
			{
				noOfRecordCheck++;
				apRecords.append("<tr>");
				apRecords.append("<td>"+jbforteacher.getTeacherId().getFirstName()+" "+jbforteacher.getTeacherId().getLastName()+"</td>");
				apRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbforteacher.getCreatedDateTime())+"</td>");
				//apRecords.append("<td class='pagination-centered'><a data-original-title='Resume' rel='tooltip' href='javascript:void(0);' id='tpResume"+noOfRecordCheck+"' onclick='downloadResume("+jbforteacher.getTeacherId()+",'tpResume"+noOfRecordCheck+"');if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;'><img src='images/resumeicon.jpg' width='23' height='28' alt=''></a></td>");
				//apRecords.append("<td class='pagination-centered'><a href='#' ><img src='images/portfolioicon.png' width='36' height='27' alt='' href='javascript:void(0);' id='tpPortfolio"+noOfRecordCheck+"' onclick='downloadPortfolioReport('"+jbforteacher.getTeacherId()+"','tpPortfolio"+noOfRecordCheck+"');if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;'></a></td>");
				apRecords.append("<td>"+jbforteacher.getStatus().getStatus()+"</td>");
				apRecords.append("<td>");
				apRecords.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("lblResume", locale)+"' rel='tooltip' id='tpResume"+noOfRecordCheck+"' href='javascript:void(0)' onclick=\"downloadResume('"+jbforteacher.getTeacherId().getTeacherId()+"','tpResume"+noOfRecordCheck+"');"+windowFunc+"\"  ><img src='images/resumeicon.jpg' width='17px' alt=''></a>");
				apRecords.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("lblPortfolio1", locale)+"' rel='tooltip' id='tpPortfolio"+noOfRecordCheck+"' href='javascript:void(0)'  onclick=\"downloadPortfolioReport('"+jbforteacher.getTeacherId().getTeacherId()+"','tpPortfolio"+noOfRecordCheck+"');"+windowFunc+"  \" ><img src='images/portfolioicon.png' width='24px'  alt=''></a>");
				
				isBase = baseTakenMap.get(jbforteacher.getTeacherId().getTeacherId());
				//System.out.println(" ============ isBase =========="+isBase);
				if(isBase!=null && jbforteacher.getStatus().getStatusShortName().equalsIgnoreCase("hird"))
					apRecords.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headPdRep", locale)+"' rel='tooltip' id='tpPDReport"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"generatePDReport('"+jbforteacher.getTeacherId().getTeacherId()+"','tpPDReport"+noOfRecordCheck+"');\"><span class='icon-book icon-large iconcolor'></span></a>");
				else
				{
					if(isBase!=null)		
						apRecords.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("msgEPIcompleted1", locale)+"' rel='tooltip' id='tpPDReport"+noOfRecordCheck+"' ><span class='icon-book icon-large iconcolorhover'></span></a>");
					else
						apRecords.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("msgNoPDreport", locale)+"' rel='tooltip' id='tpPDReport"+noOfRecordCheck+"' ><span class='icon-book icon-large iconcolorhover'></span></a>");
				}
				apRecords.append("</td>");
				
				apRecords.append("<td>");
				
				if(roleAccess.indexOf("|4|")!=-1){
					apRecords.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("msgSendaMessage", locale)+"' rel='tooltip' id='tpMsg"+noOfRecordCheck+"' href='javascript:void(0);'  onclick=\"getMessageDiv('"+jbforteacher.getTeacherId().getTeacherId()+"','"+jbforteacher.getTeacherId().getEmailAddress()+"','"+jbforteacher.getJobId().getJobId()+"');\"  ><img src='images/message.png' width='24px'></a>");
					apRecords.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("msgVCNotes", locale)+"' rel='tooltip' id='tpNotes"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"getNotesDiv('"+jbforteacher.getTeacherId().getTeacherId()+"','"+jbforteacher.getJobId().getJobId()+"')\"><img src='images/notes.png' width='24px'></a>");
				}

				//<a href="candidatereport.do?jobId=1&JobOrderType=2"><img src="images/not.png"></a>
				apRecords.append("</td>");
				apRecords.append("</tr>");
			}
			apRecords.append("</table>");
			apRecords.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
			//System.out.println(apRecords.toString());
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return apRecords.toString();
	}

	/* @Author: Gagan 
	 * @Discription: It is used to displayHired Applicant Grid.
	 */
	public String displayHiredApplicantGrid(String jobId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer apRecords =	new StringBuffer();

		try{
			//System.out.println(" noOfRow "+noOfRow+" pageNo "+pageNo);
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecords	= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("applicantName") && !sortOrder.equals("hiredAt") && !sortOrder.equals("hiredBy")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("applicantName")){
					sortOrderNoField="applicantName";
				}
				if(sortOrder.equals("hiredAt")){
					sortOrderNoField="hiredAt";
				}
				if(sortOrder.equals("hiredBy")){
					sortOrderNoField="hiredBy";
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			//System.out.println("sortOrderStrVal::::"+sortOrderStrVal);
			//System.out.println("sortOrderTypeVal::::"+sortOrderTypeVal);
			//jobId = request.getParameter("jobId")		==	null?"0":request.getParameter("jobId").trim();
			System.out.println(" jobId "+jobId);
			JobOrder jobOrder									=	jobOrderDAO.findById(new Integer(jobId), false, false);

			StatusMaster statushird = WorkThreadServlet.statusMap.get("hird");
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			lstStatusMasters.add(statushird);
			//List<JobForTeacher> lstHiredApplicantFromJobForTeacher = jobForTeacherDAO.findJFTbyJobOrder(jobOrder,lstStatusMasters);
			List<JobForTeacher> lstHiredApplicantFromJobForTeacher = jobForTeacherDAO.findSortedJFTbyJobOrder(sortOrderStrVal,jobOrder,lstStatusMasters);

			List<JobForTeacher> sortedlstJobForTeacher		=	new ArrayList<JobForTeacher>();

			SortedMap<String,JobForTeacher>	sortedMap = new TreeMap<String,JobForTeacher>();
			if(sortOrderNoField.equals("applicantName"))
			{
				sortOrderFieldName	=	"applicantName";
			}
			if(sortOrderNoField.equals("hiredAt"))
			{
				sortOrderFieldName	=	"hiredAt";
			}
			if(sortOrderNoField.equals("hiredBy"))
			{
				sortOrderFieldName	=	"hiredBy";
			}
			int mapFlag=2;
			for (JobForTeacher jobForTeacher : lstHiredApplicantFromJobForTeacher){
				String orderFieldName=jobForTeacher.getCreatedDateTime()+"";
				if(sortOrderFieldName.equals("applicantName")){
					orderFieldName=jobForTeacher.getTeacherId().getFirstName()+"||"+jobForTeacher.getTeacherId().getLastName()+"||"+jobForTeacher.getJobForTeacherId();
					sortedMap.put(orderFieldName+"||",jobForTeacher);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("hiredAt")){
					if(jobForTeacher.getUpdatedBy().getEntityType().equals(2))
						orderFieldName=jobForTeacher.getUpdatedBy().getDistrictId().getDistrictName()+"||"+jobForTeacher.getJobForTeacherId();
					else
					{
						if(jobForTeacher.getUpdatedBy().getEntityType().equals(3))
							orderFieldName=jobForTeacher.getUpdatedBy().getSchoolId().getSchoolName()+"||"+jobForTeacher.getJobForTeacherId();
					}
					//orderFieldName=jobForTeacher.getCompetencyMaster().getCompetencyName()+"||"+jobForTeacher.getJobForTeacherId();
					sortedMap.put(orderFieldName+"||",jobForTeacher);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("hiredBy")){
					orderFieldName=jobForTeacher.getUpdatedBy().getFirstName()+"||"+jobForTeacher.getJobForTeacherId();
					sortedMap.put(orderFieldName+"||",jobForTeacher);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstJobForTeacher.add((JobForTeacher) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstJobForTeacher.add((JobForTeacher) sortedMap.get(key));
				}
			}else{
				sortedlstJobForTeacher=lstHiredApplicantFromJobForTeacher;
			}

			totalRecords =sortedlstJobForTeacher.size();

			if(totalRecords<end)
				end=totalRecords;
			List<JobForTeacher> lstsortedJobForTeacher		=	sortedlstJobForTeacher.subList(start,end);

			/*if(jobOrder.getCreatedForEntity().equals(3))
			{
				List<SchoolInJobOrder> lstSchoolInJobOrder			= schoolInJobOrderDAO.findJobOrder(jobOrder);
				//map.addAttribute("lstSchoolInJobOrder", lstSchoolInJobOrder.get(0));
			}*/

			apRecords.append("<table id='applicantHiredTable' width='100%'  border='0'>");
			apRecords.append("<thead class='bg'>");
			apRecords.append("<tr>");
			//apRecords.append("<th width='20%'>Applicant Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblApplicantName", locale),sortOrderFieldName,"applicantName",sortOrderTypeVal,pgNo);
			apRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			//apRecords.append("<th width='20%'>Applied on</th>");
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblAppliedOn", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			apRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			//apRecords.append("<th width='20%'>Hired on</th>");
			responseText=PaginationAndSorting.responseSortingLink( Utility.getLocaleValuePropByKey("msgHiredon", locale) ,sortOrderFieldName,"updatedDate",sortOrderTypeVal,pgNo);
			apRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			//apRecords.append("<th width='30%'>Hired At</th>");
			responseText=PaginationAndSorting.responseSortingLink( Utility.getLocaleValuePropByKey("msgHiredAt", locale) ,sortOrderFieldName,"hiredAt",sortOrderTypeVal,pgNo);
			apRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			//apRecords.append("<th width='35%'>Hired By</th>");
			responseText=PaginationAndSorting.responseSortingLink(  Utility.getLocaleValuePropByKey("msgHiredBy", locale) ,sortOrderFieldName,"hiredBy",sortOrderTypeVal,pgNo);
			apRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			apRecords.append("</tr>");
			apRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(lstsortedJobForTeacher.size()==0)
				apRecords.append("<tr><td colspan='5'>"+Utility.getLocaleValuePropByKey("msgNoApplicantHired", locale)+"</td></tr>" );
			System.out.println("lstHiredApplicantFromJobForTeacher "+lstsortedJobForTeacher.size());

			for (JobForTeacher hiredApplicant : lstsortedJobForTeacher) 
			{
				apRecords.append("<tr>" );
				apRecords.append("<td>"+hiredApplicant.getTeacherId().getFirstName()+" "+hiredApplicant.getTeacherId().getLastName()+"</td>");
				apRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(hiredApplicant.getCreatedDateTime())+"</td>");
				apRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(hiredApplicant.getUpdatedDate())+"</td>");
				///System.out.println(" Updated by  "+hiredApplicant.getUpdatedBy().getEntityType());
				if(hiredApplicant.getUpdatedBy().getEntityType().equals(2))
					apRecords.append("<td>"+hiredApplicant.getUpdatedBy().getDistrictId().getDistrictName()+"</td>");
				else
				{
					if(hiredApplicant.getUpdatedBy().getEntityType().equals(3))
						apRecords.append("<td>"+hiredApplicant.getUpdatedBy().getSchoolId().getSchoolName()+"</td>");
				}

				apRecords.append("<td>"+hiredApplicant.getUpdatedBy().getFirstName()+" "+hiredApplicant.getUpdatedBy().getLastName()+"</td>");
				apRecords.append("<td>");

			}
			apRecords.append("</table>");
			apRecords.append(PaginationAndSorting.getPaginationDoubleString(request,totalRecords,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return apRecords.toString();
	}

}
