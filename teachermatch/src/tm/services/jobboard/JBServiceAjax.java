package tm.services.jobboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.services.JobBoardAjax;
import tm.services.ManageJobOrdersAjax;
import tm.services.teacher.DWRAutoComplete;

public class JBServiceAjax 
{
	@Autowired
	private DWRAutoComplete dWRAutoComplete;
	
	@Autowired
	private ManageJobOrdersAjax manageJobOrdersAjax;
	
	@Autowired
	private JobBoardAjax jobBoardAjax;
	
	public String displayStateData(){
		return dWRAutoComplete.displayStateData();
	}
	
	public String selectedCityByStateId(Long stateId){
		return dWRAutoComplete.selectedCityByStateId(stateId);
	}
	
	public String getTickSlider(String Id,String name,String tickInterval,String max,String swidth,String svalue,String style)
	{
		String sReturnValue="<iframe id='"+Id+"'  src='slideract.do?name="+name+"&tickInterval="+tickInterval+"&max="+max+"&swidth="+swidth+"&svalue="+svalue+"' scrolling='no' frameBorder='0' style='"+style+"'></iframe>";
		return sReturnValue;
	}

	public List<DistrictMaster> getDistrictMasterList(String DistrictName)
	{
		return dWRAutoComplete.getDistrictMasterList(DistrictName);
	}
	public List<DistrictMaster> getDistrictMasterList(String DistrictName,boolean flag)
	{
		return dWRAutoComplete.getDistrictMasterList(DistrictName,flag);
	}
	public List<DistrictSchools> getSchoolMasterList(String schoolName, String districtId)
	{
		return dWRAutoComplete.getSchoolMasterList(schoolName, districtId);
	}
	public List<DistrictSchools> getSchoolMasterList(String schoolName, String districtId,boolean flag)
	{
		return dWRAutoComplete.getSchoolMasterList(schoolName, districtId,flag);
	}
	
	public List<CertificateTypeMaster> getAllCertificateTypeList(String certType)
	{
		return dWRAutoComplete.getAllCertificateTypeList(certType);
	}
	public List<CertificateTypeMaster> getAllCertificateTypeList(String certType,boolean flag)
	{
		return dWRAutoComplete.getAllCertificateTypeList(certType,flag);
	}
	
	public List<CertificateTypeMaster> getFilterCertificateTypeList(String certType,String stateId)
	{
		return dWRAutoComplete.getFilterCertificateTypeList(certType, stateId);
	}
	public String getZoneListAllForEJob(int districtId)
	{
		return manageJobOrdersAjax.getZoneListAllForEJob(districtId);
	}
	
	
	public String displayJobsByTM(String days,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String districtId,String schoolId1,String cityName,String stateId,String zipCode,String subjectId,String certificateTypeMaster)
	{
		return jobBoardAjax.displayJobsByTM(days, noOfRow, pageNo, sortOrder, sortOrderType, districtId, schoolId1, cityName, stateId, zipCode, subjectId, certificateTypeMaster);
	}
	
//shadab start
	
	
	public String searchByDocument(String searchTerm,int months,String districtName,String districtId,String stateName,String cityName,
			String schoolName,String certificateTypeMaster,String zipCode,int from,String subjectIdList,String noOfRow, String pageNo)
	{
		
		//return jobBoardAjax.searchByDocument(searchTerm.split(",")[0],searchTerm.split(",")[1],
			//	Integer.parseInt(searchTerm.split(",")[2]),searchTerm.split(",")[3]);
		if(certificateTypeMaster!=null && certificateTypeMaster.contains("["))
		{
			System.out.println(certificateTypeMaster.substring(0, certificateTypeMaster.indexOf("[")-1));
			System.out.println(certificateTypeMaster.substring(certificateTypeMaster.indexOf("[")+1, certificateTypeMaster.indexOf("]")));
		}
		
		//if(certificateTypeMaster!=null && certificateTypeMaster.contains("["))
			//certificateTypeMaster=certificateTypeMaster.substring(0, certificateTypeMaster.indexOf("[")-1);
		searchTerm=searchTerm.replaceAll("\"", "");
		return jobBoardAjax.searchByDocument(searchTerm.replaceAll(","," "),months,districtName,districtId,stateName,cityName,schoolName,certificateTypeMaster,zipCode,from,subjectIdList,noOfRow,pageNo);
		
		//return "";
	}
	//shadab end
}

