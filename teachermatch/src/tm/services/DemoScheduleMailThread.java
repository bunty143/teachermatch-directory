package tm.services;

import java.util.List;


public class DemoScheduleMailThread extends Thread{

	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) {
		this.emailerService = emailerService;
	}
	private List<String> lstBcc;
	
	private String filepath;
	
	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public List<String> getLstBcc() {
		return lstBcc;
	}

	public void setLstBcc(List<String> lstBcc) {
		this.lstBcc = lstBcc;
	}
	private String mailto;
	public void setMailto(String mailto) {
		this.mailto = mailto;
	}
	
	private String mailfrom;
	public void setMailfrom(String mailfrom) {
		this.mailfrom = mailfrom;
	}
	
	private String mailsubject;
	public void setMailsubject(String mailsubject) {
		this.mailsubject = mailsubject;
	}
	
	private String mailcontent;
	public void setMailcontent(String mailcontent) {
		this.mailcontent = mailcontent;
	}
	
	public DemoScheduleMailThread() {
		super();
	}
	
	public DemoScheduleMailThread(EmailerService emailerService,List<String> lstBcc,String mailto,String mailfrom,String mailsubject,String mailcontent) {
		this.emailerService = emailerService;
		this.lstBcc = lstBcc;
		this.mailto = mailto;
		this.mailfrom = mailfrom;
		this.mailsubject = mailsubject;
		this.mailcontent = mailcontent;
	}
	
	public void run()
	{
		
		try{
			if(filepath!=null){
				System.out.println("filepath:"+filepath);
				emailerService.sendMailWithAttachmentsWithBCC(mailto, mailsubject, "noreply@teachermatch.net", mailcontent,filepath,lstBcc);
			}else if(lstBcc!=null && lstBcc.size()>0){
				emailerService.sendMailWithAttachmentsWithBCC(mailto, mailsubject, "noreply@teachermatch.net", mailcontent,null,lstBcc);
			}else{
				emailerService.sendMailAsHTMLText(mailto, mailsubject,mailcontent);
			}
			System.out.println("Mail set successfully to "+mailto);
		}catch (Exception e) {
			emailerService.sendMailAsHTMLText(mailto, mailsubject,mailcontent);
			e.printStackTrace();
		}
	}
	
}
