package tm.services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.services.district.PrintOnConsole;
import tm.utility.Utility;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;

public class EmailerService
{

	@Autowired
	private MailSender mailSender;

	@Autowired
	private SimpleMailMessage simpleMailMessage;


	@Autowired
	private JavaMailSenderImpl javaMailSender;

	public void setMailSender(MailSender mailSender)
	{
		this.mailSender = mailSender;
	}

	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage)
	{
		this.simpleMailMessage = simpleMailMessage;
	}

	public void setJavaMailSender(JavaMailSenderImpl javaMailSender) {
		this.javaMailSender = javaMailSender;
	}


	static Properties  properties = new Properties();    		
	
	static{
		try {
			InputStream inputStream = new Utility().getClass().getClassLoader().getResourceAsStream("smtpmail.properties");
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendMail(String to, String subject, String text)
	{
		SimpleMailMessage msg = new SimpleMailMessage(this.simpleMailMessage);
		//msg.setFrom("info@naturesriches.in");
		msg.setTo(to);
		msg.setSubject(subject);
		msg.setText(text);        


		try
		{
			this.mailSender.send(msg);
		}
		catch(MailException ex)
		{

			ex.printStackTrace();
		}

	}
	public void sendMailAsHTMLText(String to,String subject, String text)
	{

		MimeMessage message = javaMailSender.createMimeMessage();

		try
		{
			
			String toCandidate="";
			String fromDA="";
			String fromEmailId="";
			if(to.contains("#"))
			{
				try
				{
			     toCandidate=to.substring(0,to.lastIndexOf("#"));
				 fromDA=to.substring(to.indexOf("#")).trim();
				 fromEmailId=fromDA.replace("#","").trim();
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				toCandidate=to;
			}
			
			
			String mgsmtpid = "";
			String mgsmtppost = "";
			String bccEmail = "";
			String defaultEmail = "";
			String defaultPass = "";
			String fromText = "";			
			String fromEmail= "";
			mgsmtpid=properties.getProperty("smtphost.id");
			mgsmtppost=properties.getProperty("smtphost.port");
			defaultEmail = properties.getProperty("smtphost.defaultusername");
			System.out.println();
			defaultPass = properties.getProperty("smtphost.defaultpassword");

			System.out.println("defaultPass of the new user =>  ..............."+defaultPass);


			fromText=properties.getProperty("smtphost.defaultfromtext");


			//bccEmail = properties.getProperty("smtphost.bccemail");
			if(fromEmailId!=null && !fromEmailId.equals(""))
			{
				fromEmail=fromEmailId;
			}
			else
			{
			fromEmail=properties.getProperty("smtphost.defaultfrom");
			}
			
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");
			String[] bcc = new String[arrBccEmail.length];
			
			for(int i=0; i<arrBccEmail.length;i++)
			{
				bcc[i]	=	arrBccEmail[i];
			}
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			
			javaMailSender.setHost(mgsmtpid);
			javaMailSender.setPort(Integer.parseInt(mgsmtppost));
			javaMailSender.setUsername(defaultEmail);
			javaMailSender.setPassword(defaultPass);


			javaMailSender.setDefaultEncoding("UTF-8");
			message = javaMailSender.createMimeMessage();

			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

			helperMsg.setSubject(subject);
			helperMsg.setTo(toCandidate);
			//helperMsg.setBcc(bccEmail);
			helperMsg.setBcc(bcc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail,fromText);


		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}



		try
		{
			javaMailSender.send(message);

		}
		catch(MailException ex)
		{

			ex.printStackTrace();
		}        

	}

	/*
	 * There are four flag to send mail using mailBy parameter
	 * admin=admin@teachermatch.com
	 * noreply=noreply@teachermatch.com
	 * feedback=feedback@teachermatch.com
	 * clientservices=clientservices@teachermatch.com
	 * 
	 */
	public void sendMailAsHTMLText(String to,String subject, String text , String mailBy)
	{    	


		MimeMessage message = javaMailSender.createMimeMessage();
		try
		{
			String mgsmtpid = "";
			String mgsmtppost = "";
			String userEmail = "";
			String userPass = "";
			String bccEmail = "";
			String fromText="";
			String fromEmail="";

			if(mailBy==null)
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.defaultfromtext");
				fromEmail=properties.getProperty("smtphost.defaultfrom");
			}
			else if(mailBy.trim().equalsIgnoreCase("admin"))
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.adminusername");
				fromEmail=fromText;
			}    		
			else if(mailBy.trim().equalsIgnoreCase("noreply"))
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.defaultfromtext");
				fromEmail=properties.getProperty("smtphost.noreplyusername");
			}
			else if(mailBy.trim().equalsIgnoreCase("feedback"))
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.usernamefeedback");
				fromEmail=fromText;
			}
			else if(mailBy.trim().equalsIgnoreCase("clientservices"))
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.usernameclientservices");
				fromEmail=fromText;
			}else if(mailBy.trim().equalsIgnoreCase("cms"))
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail =properties.getProperty("smtphost.cmsusername");
				userPass = properties.getProperty("smtphost.cmspassword");
				fromText=properties.getProperty("smtphost.cmsfromtext");
				fromEmail=properties.getProperty("smtphost.cmsfromemail");
			}
			else
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.defaultfromtext");
				fromEmail=properties.getProperty("smtphost.defaultfrom");
			}
			//bccEmail = properties.getProperty("smtphost.bccemail");
			
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");
			String[] bcc = new String[arrBccEmail.length];
			
			for(int i=0; i<arrBccEmail.length;i++)
			{
				bcc[i]	=	arrBccEmail[i];
			}
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			

			javaMailSender.setHost(mgsmtpid);
			javaMailSender.setPort(Integer.parseInt(mgsmtppost));
			javaMailSender.setUsername(userEmail);
			javaMailSender.setPassword(userPass);
			message = javaMailSender.createMimeMessage();

			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

			helperMsg.setSubject(subject);
			helperMsg.setTo(to);
			//helperMsg.setBcc(bccEmail);
			helperMsg.setBcc(bcc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail,fromText);


		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}



		try
		{
			javaMailSender.send(message);
		}
		catch(MailException ex)
		{

			ex.printStackTrace();
		}        


	}
	
	public void sendMailAsHTMLText(String fromText,String to,String subject, String text , String mailBy)
	{    	


		MimeMessage message = javaMailSender.createMimeMessage();
		try
		{
			String mgsmtpid = "";
			String mgsmtppost = "";
			String userEmail = "";
			String userPass = "";
			String bccEmail = "";
			String fromEmail="";

			if(mailBy==null)
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.defaultfromtext");
				fromEmail=properties.getProperty("smtphost.defaultfrom");
				
			}
			else if(mailBy.trim().equalsIgnoreCase("admin"))
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.adminusername");
				fromEmail=fromText;
			}    		
			else if(mailBy.trim().equalsIgnoreCase("noreply"))
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.defaultfromtext");
				fromEmail=properties.getProperty("smtphost.noreplyusername");
			}
			else if(mailBy.trim().equalsIgnoreCase("feedback"))
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.usernamefeedback");
				fromEmail=fromText;
			}
			else if(mailBy.trim().equalsIgnoreCase("clientservices"))
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.usernameclientservices");
				fromEmail=fromText;
			}else if(mailBy.trim().equalsIgnoreCase("cms"))
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail =properties.getProperty("smtphost.cmsusername");
				userPass = properties.getProperty("smtphost.cmspassword");
				fromText=properties.getProperty("smtphost.cmsfromtext");
				fromEmail=properties.getProperty("smtphost.cmsfromemail");
			}else if(mailBy.trim().equalsIgnoreCase("nocms"))
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail =properties.getProperty("smtphost.cmsusername");
				userPass = properties.getProperty("smtphost.cmspassword");
				fromEmail=properties.getProperty("smtphost.defaultfrom");
			}
			else
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.defaultfromtext");
				fromEmail=properties.getProperty("smtphost.defaultfrom");				
				
			}
			//bccEmail = properties.getProperty("smtphost.bccemail");
			
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");
			String[] bcc = new String[arrBccEmail.length];
			
			for(int i=0; i<arrBccEmail.length;i++)
			{
				bcc[i]	=	arrBccEmail[i];
			}
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			
			javaMailSender.setHost(mgsmtpid);
			javaMailSender.setPort(Integer.parseInt(mgsmtppost));
			javaMailSender.setUsername(userEmail);
			javaMailSender.setPassword(userPass);
			message = javaMailSender.createMimeMessage();

			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

			helperMsg.setSubject(subject);
			helperMsg.setTo(to);
			//helperMsg.setBcc(bccEmail);
			helperMsg.setBcc(bcc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail,fromText);


		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}



		try
		{
			javaMailSender.send(message);
		}
		catch(MailException ex)
		{

			ex.printStackTrace();
		}        


	}

	public void sendMailAsHTMLTextToClient(String fromText,String to,String subject, String text , String mailBy)
	{    	

		System.out.println("================= sendMailAsHTMLTextToClient ==============");
		MimeMessage message = javaMailSender.createMimeMessage();
		try
		{
			String mgsmtpid = "";
			String mgsmtppost = "";
			String userEmail = "";
			String userPass = "";
			String bccEmail = "";
			String bccClientEmail = "";
			String fromEmail="";			

			if(mailBy==null)
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.defaultfromtext");
				fromEmail=properties.getProperty("smtphost.defaultfrom");				
			}
			else if(mailBy.trim().equalsIgnoreCase("nocms"))
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail =properties.getProperty("smtphost.cmsusername");
				userPass = properties.getProperty("smtphost.cmspassword");
				fromEmail=properties.getProperty("smtphost.defaultfrom");
			}
			else
			{
				mgsmtpid=properties.getProperty("smtphost.id");
				mgsmtppost=properties.getProperty("smtphost.port");
				userEmail = properties.getProperty("smtphost.defaultusername");
				userPass = properties.getProperty("smtphost.defaultpassword");
				fromText=properties.getProperty("smtphost.defaultfromtext");
				fromEmail=properties.getProperty("smtphost.defaultfrom");
				
			}
			bccClientEmail = properties.getProperty("smtphost.bccClientEmail");
			/*bccEmail = properties.getProperty("smtphost.bccemail");
			bccClientEmail = properties.getProperty("smtphost.bccClientEmail");
			
			String[] bcc = {bccEmail,bccClientEmail};
			bcc[0]=properties.getProperty("smtphost.bccemail");
			bcc[1]= properties.getProperty("smtphost.bccClientEmail");*/
			
			
			
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");
			String[] bcc = new String[(arrBccEmail.length)+1];
			int i=0;
			for(i=0; i<arrBccEmail.length;i++)
			{
				bcc[i]	=	arrBccEmail[i];
			}
			bcc[i]	=	bccClientEmail;
			
			System.out.println("\n bccClientEmail : "+bccClientEmail+"\n Length :  "+arrBccEmail.length+" \n value of i : "+i+" \n  =========== Arr bcc  size : "+bcc.length);
			
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			/*for(i=0; i<bcc.length;i++)
			{
				System.out.println(i+" bcc  "+bcc[i]);
			}*/
			
			javaMailSender.setHost(mgsmtpid);
			javaMailSender.setPort(Integer.parseInt(mgsmtppost));
			javaMailSender.setUsername(userEmail);
			javaMailSender.setPassword(userPass);
			message = javaMailSender.createMimeMessage();
            MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);
            helperMsg.setSubject(subject);
			helperMsg.setTo(to);
			helperMsg.setBcc(bcc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail,fromText);


		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		try
		{
			javaMailSender.send(message);
		}
		catch(MailException ex)
		{

			ex.printStackTrace();
		}        


	}
	
	
	
	public void sendMailWithAttachments(String to,String subject,String from, String text,String filePath)
	{

		System.out.println("((((((((((((((()))))))))))))");
		//JavaMailSenderImpl sender = new JavaMailSenderImpl();
		//String from ="info@netsutra.com";
		MimeMessage message = javaMailSender.createMimeMessage();


		try
		{
			////////////////////////////////////////////////////////////////////////
			String bccEmail = "";
			String defaultEmail = "";
			String defaultPass = "";
			String fromText= "";
			String fromEmail="";			
			defaultEmail = properties.getProperty("smtphost.defaultusername");
			defaultPass = properties.getProperty("smtphost.defaultpassword");
			//bccEmail = properties.getProperty("smtphost.bccemail");
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");
			String[] bcc = new String[arrBccEmail.length];
			
			for(int i=0; i<arrBccEmail.length;i++)
			{
				bcc[i]	=	arrBccEmail[i];
			}
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/

			fromEmail=properties.getProperty("smtphost.defaultfrom");
			fromText=properties.getProperty("smtphost.defaultfromtext");
			javaMailSender.setUsername(defaultEmail);
			javaMailSender.setPassword(defaultPass);

			javaMailSender.setDefaultEncoding("UTF-8");
			//System.out.println("gggggggggencoding: "+javaMailSender.getDefaultEncoding());
			//message = javaMailSender.createMimeMessage();

			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

			helperMsg.setSubject(subject);
			helperMsg.setTo(to);
			//helperMsg.setBcc(bccEmail);
			helperMsg.setBcc(bcc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail,fromText);
			////////////////////////////////////////////////////////////////////////
			/*MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

	    	helperMsg.setSubject(subject);
	    	helperMsg.setTo(to);
	    	helperMsg.setText(text,true);
	    	helperMsg.setFrom(from);*/


			//helperMsg.addInline("identifier1234", res);
			//String filename=filePath.substring(filePath.lastIndexOf("/")+1,filePath.lastIndexOf(".")); 
			File f = new File(filePath);
			helperMsg.addAttachment(f.getName(), f);
			System.out.println("File Name :: "+f.getName()+" File Path :: "+f.getPath());

			/*FileSystemResource res1 = new FileSystemResource(new File("c:\\mvc.pdf"));
	    	helperMsg.addAttachment("identifier12345", res1);*/


			System.out.println("########"+helperMsg.toString());
			System.out.println("########mailServer:"+mailSender);
			System.out.println("########mailServer:"+mailSender.getClass());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}



		try
		{
			javaMailSender.send(message);
		}
		catch(MailException ex)
		{
			System.out.println("^^^^^^^^^^^^^^^^^^Main Exception^^^^^^^^^^^^^^^");
			ex.printStackTrace();
		}


	}
	public void sendMailsWithMultipleAttachments(List<String> tos,String subject,String from, String text,String filePath)
	{

		System.out.println("((((((((((((((()))))))))))))");
		//JavaMailSenderImpl sender = new JavaMailSenderImpl();
		//String from ="info@netsutra.com";
		MimeMessage message = javaMailSender.createMimeMessage();
		String tto="";
		try
		{
			////////////////////////////////////////////////////////////////////////
			String bccEmail = "";
			String defaultEmail = "";
			String defaultPass = "";
			String fromText= "";
			String fromEmail= "";
			defaultEmail = properties.getProperty("smtphost.defaultusername");
			defaultPass = properties.getProperty("smtphost.defaultpassword");
			//bccEmail = properties.getProperty("smtphost.bccemail");
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");
			String[] bcc = new String[arrBccEmail.length];
			
			for(int i=0; i<arrBccEmail.length;i++)
			{
				bcc[i]	=	arrBccEmail[i];
			}
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/

			fromEmail=properties.getProperty("smtphost.defaultfrom");
			fromText=properties.getProperty("smtphost.defaultfromtext");
			javaMailSender.setUsername(defaultEmail);
			javaMailSender.setPassword(defaultPass);

			javaMailSender.setDefaultEncoding("UTF-8");

			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

			helperMsg.setSubject(subject);
			/*helperMsg.setTo(tos.get(0));
			tos.remove(0);
			if(tos.size()>0)
			{
				String [] stockArr = tos.toArray(new String[tos.size()]);
				helperMsg.setCc(stockArr);
			}*/
			
			String [] stockArr = tos.toArray(new String[tos.size()]);
			helperMsg.setTo(stockArr);
			
			//helperMsg.setBcc(bccEmail);
			helperMsg.setBcc(bcc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail, fromText);
		
			//helperMsg.addInline("identifier1234", res);
			/*int i=1;
			for (String filePath : filePaths) {

				helperMsg.addAttachment("file"+i, new File(filePath));
				i++;
			}*/
/*			String filename=filePath.substring(filePath.lastIndexOf("/")+1,filePath.lastIndexOf(".")); 
			helperMsg.addAttachment(filename, new File(filePath));*/
			File f = new File(filePath);
			helperMsg.addAttachment(f.getName(), f);

			for(int i=0;i<stockArr.length;i++)
				tto+=stockArr[i]+",";
			
			System.out.println("######## mail sent successfully to "+tto);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}



		try
		{
			javaMailSender.send(message);
		}
		catch(MailException ex)
		{
			System.out.println("^^^^^^^^^^^^^^^^^^Main Exception^^^^^^^^^^^^^^^");
			ex.printStackTrace();
		}

	 

		//////////////////////////////////////

		/*String defaultEmail="vishwanath@netsutra.com";
		String bccEmail = "vishwanath@netsutra.com";
		//defaultPass="050706";
		javaMailSender.setHost("relay.netsutra.com");
		javaMailSender.setPort(25);

		String host="relay.netsutra.com";
		final String user="s.swain@netsutra.com";//change accordingly
		final String password="050706";//change accordingly

		//Get the session object
		Properties props = new Properties();
		props.put("mail.smtp.host",host);
		props.put("mail.smtp.auth", "true");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user,password);
			}
		});

		//Compose the message
		try {
			MimeMessage message = new MimeMessage(session);

			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

			helperMsg.setSubject(subject);
		
			String [] stockArr = tos.toArray(new String[tos.size()]);
			helperMsg.setTo(stockArr);

			helperMsg.setBcc(bccEmail);
			helperMsg.setText(text,true);
			helperMsg.setFrom(defaultEmail);

			String filename=filePath.substring(filePath.lastIndexOf("/")+1,filePath.lastIndexOf(".")); 
			helperMsg.addAttachment(filename, new File(filePath));
			//send the message
			Transport.send(message);
			System.out.println("######## mail sent successfully.");

			System.out.println("message sent successfully...");

		} catch (MessagingException e) {e.printStackTrace();}*/



	}
	public void sendMailsToMultipleContacts(List<String> tos,String subject,String from, String text)
	{

		System.out.println("((((((((((((((()))))))))))))");
		MimeMessage message = javaMailSender.createMimeMessage();
		String tto="";
		try
		{
			////////////////////////////////////////////////////////////////////////
			String bccEmail = "";
			String defaultEmail = "";
			String defaultPass = "";
			String fromEmail= "";
			String fromText= "";
			defaultEmail = properties.getProperty("smtphost.defaultusername");
			defaultPass = properties.getProperty("smtphost.defaultpassword");
			//bccEmail = properties.getProperty("smtphost.bccemail");
			
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");
			String[] bcc = new String[arrBccEmail.length];
			
			for(int i=0; i<arrBccEmail.length;i++)
			{
				bcc[i]	=	arrBccEmail[i];
			}
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			
			fromEmail=properties.getProperty("smtphost.defaultfrom");
			fromText=properties.getProperty("smtphost.defaultfromtext");
			javaMailSender.setUsername(defaultEmail);
			javaMailSender.setPassword(defaultPass);

			javaMailSender.setDefaultEncoding("UTF-8");
			//System.out.println("bccEmail: "+bccEmail);
			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

			helperMsg.setSubject(subject);
			
			String [] stockArr = tos.toArray(new String[tos.size()]);
			helperMsg.setTo(stockArr);
			//helperMsg.setBcc(bccEmail);
			helperMsg.setBcc(bcc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail,fromText);

			for(int i=0;i<stockArr.length;i++)
				tto+=stockArr[i]+",";
			
			System.out.println("######## mail sent successfully to "+tto);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		try
		{
			javaMailSender.send(message);
		}
		catch(MailException ex)
		{
			System.out.println("^^^^^^^^^^^^^^^^^^Main Exception^^^^^^^^^^^^^^^");
			ex.printStackTrace();
		}
	}
	
	
	
	public void sendMailWithAttachments(String to,String subject,String from, String text,String filePath,String fileName)
	{

		MimeMessage message = javaMailSender.createMimeMessage();
		try
		{
			String bccEmail = "";
			String defaultEmail = "";
			String defaultPass = "";
			String fromText= "";
			String fromEmail="";
			defaultEmail = properties.getProperty("smtphost.defaultusername");
			defaultPass = properties.getProperty("smtphost.defaultpassword");
			//bccEmail = properties.getProperty("smtphost.bccemail");
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");
			String[] bcc = new String[arrBccEmail.length];
			
			for(int i=0; i<arrBccEmail.length;i++)
			{
				bcc[i]	=	arrBccEmail[i];
			}
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/

			if(from!=null && !from.equals(""))
			{
				fromEmail=from;	
			}
			else
			{
				fromEmail=properties.getProperty("smtphost.defaultfrom");
			}
			
			fromText=properties.getProperty("smtphost.defaultfromtext");
			javaMailSender.setUsername(defaultEmail);
			javaMailSender.setPassword(defaultPass);

			javaMailSender.setDefaultEncoding("UTF-8");
			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

			helperMsg.setSubject(subject);
			helperMsg.setTo(to);
			//helperMsg.setBcc(bccEmail);
			helperMsg.setBcc(bcc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail,fromText);
			helperMsg.addAttachment(fileName, new File(filePath));

			System.out.println("Mail Send");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}



		try
		{
			javaMailSender.send(message);
		}
		catch(MailException ex)
		{
			System.out.println("^^^^^^^^^^^^^^^^^^Main Exception^^^^^^^^^^^^^^^");
			ex.printStackTrace();
		}
	}
	public void sendMailAsHTMLTextWithBCC(String to,String subject, String text,String bccEmail[],String offerTxt)
	{
		MimeMessage message = javaMailSender.createMimeMessage();

		try
		{
			String mgsmtpid = "";
			String mgsmtppost = "";
			String defaultEmail = "";
			String defaultPass = "";
			String fromText = "";
			String fromEmail = "";
			mgsmtpid=properties.getProperty("smtphost.id");
			mgsmtppost=properties.getProperty("smtphost.port");
			defaultEmail = properties.getProperty("smtphost.defaultusername");
			defaultPass = properties.getProperty("smtphost.defaultpassword");
			String offerBccEmail = properties.getProperty("smtphost.offerbccemail");
			fromEmail=properties.getProperty("smtphost.defaultfrom");
			fromText=properties.getProperty("smtphost.defaultfromtext");
			int lengthBcc=1;
			try{
				if(bccEmail!=null){
					lengthBcc=bccEmail.length;
				}
			}catch(Exception e){ e.printStackTrace();}
			
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");
			
			int lengthArr=0;
			if(arrBccEmail!=null){
				lengthArr=arrBccEmail.length;
			}
			int totLength=lengthBcc+lengthArr;
			
			String[] bcc = new String[totLength];
			try{
				for(int i=0; i<arrBccEmail.length;i++)
				{
					bcc[i]	=	arrBccEmail[i];
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(offerTxt!=null && bccEmail==null){
				try{
					bcc[lengthArr]=offerBccEmail;
				}catch(Exception e){e.printStackTrace();}
			}else{
				try{
					int ii=0;
					for(int j=lengthArr; j<totLength;j++){
						bcc[j]	=	bccEmail[ii];
						ii++;
					}
				}catch(Exception e){e.printStackTrace();}
			}
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			
			try{
				for(int i=0;i<bcc.length;i++){
					System.out.println("bcc::"+bcc[i]);
				}
			}catch(Exception e){}
			
			javaMailSender.setHost(mgsmtpid);
			javaMailSender.setPort(Integer.parseInt(mgsmtppost));
			javaMailSender.setUsername(defaultEmail);
			javaMailSender.setPassword(defaultPass);


			javaMailSender.setDefaultEncoding("UTF-8");
			message = javaMailSender.createMimeMessage();

			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);
			
			helperMsg.setSubject(subject);
			helperMsg.setTo(to);
			//helperMsg.setBcc(bccEmail);
			try{
				helperMsg.setBcc(bcc);
			}catch(Exception e){
				e.printStackTrace();
			}
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail,fromText);


		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}



		try
		{
			javaMailSender.send(message);

		}
		catch(MailException ex)
		{

			ex.printStackTrace();
		}        

	}
	public void sendMailWithAttachmentsWithBCC(String to,String subject,String from, String text,String filePath,List<String> lstBcc)
	{

		System.out.println("::::::::::::::sendMailWithAttachmentsWithBCC:::::::::::::");
		MimeMessage message = javaMailSender.createMimeMessage();
		try
		{
			String defaultEmail = "";
			String defaultPass = "";
			String fromText= "";
			String fromEmail="";
			defaultEmail = properties.getProperty("smtphost.defaultusername");
			defaultPass = properties.getProperty("smtphost.defaultpassword");
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");

			for(int i=0; i<arrBccEmail.length;i++)
			{
				if(arrBccEmail[i]!=null && !arrBccEmail[i].equals(""))
					lstBcc.add(arrBccEmail[i]);
			}
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			String[] bcc = new String[lstBcc.size()];
			try{
				int iBccCount=0;
				for(String tempBcc:lstBcc)
				{
					if(tempBcc!=null && !tempBcc.equals(""))
					{
						bcc[iBccCount]=tempBcc;
						
						System.out.println("tempBcc::::::"+tempBcc);
						iBccCount++;
					}
				}
				System.out.println("bcc:::::"+bcc.length);
			}catch(Exception e){
				e.printStackTrace();
			}
			fromText=properties.getProperty("smtphost.defaultfromtext");
			fromEmail=properties.getProperty("smtphost.defaultfrom");
			javaMailSender.setUsername(defaultEmail);
			javaMailSender.setPassword(defaultPass);

			javaMailSender.setDefaultEncoding("UTF-8");

			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

			helperMsg.setSubject(subject);
			helperMsg.setTo(to);
			helperMsg.setBcc(bcc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail,fromText);
			if(filePath!=null){
				File f = new File(filePath);
				helperMsg.addAttachment(f.getName(), f);
				System.out.println("File Name :: "+f.getName()+" File Path :: "+f.getPath());
			}
			System.out.println("########"+helperMsg.toString());
			System.out.println("########mailServer:"+mailSender);
			System.out.println("########mailServer:"+mailSender.getClass());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		try
		{
			javaMailSender.send(message);
		}
		catch(MailException ex)
		{
			ex.printStackTrace();
		}

	}
	public void sendMailAsHTMLByListToBcc(List<String> lstTo,List<String> lstBcc,String subject, String text)
	{

		MimeMessage message = javaMailSender.createMimeMessage();

		try
		{
			String mgsmtpid 	= "";
			String mgsmtppost 	= "";
			String defaultEmail = "";
			String defaultPass 	= "";
			String fromText 	= "";
			String fromEmail	= "";

			mgsmtpid		=	properties.getProperty("smtphost.id");
			mgsmtppost		=	properties.getProperty("smtphost.port");
			defaultEmail 	= 	properties.getProperty("smtphost.defaultusername");
			defaultPass 	= 	properties.getProperty("smtphost.defaultpassword");
			fromEmail		=	properties.getProperty("smtphost.defaultfrom");
			fromText		=	properties.getProperty("smtphost.defaultfromtext");

			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");

			for(int i=0; i<arrBccEmail.length;i++)
			{
				if(arrBccEmail[i]!=null && !arrBccEmail[i].equals(""))
					lstBcc.add(arrBccEmail[i]);
			}
			
			PrintOnConsole.debugPrintln("Email Service", "Adding To List in sendMailAsHTMLTextByListToBcc Method");
			String[] to = new String[lstTo.size()];
			int iToCount=0;
			for(String tempTo:lstTo)
			{
				if(tempTo!=null && !tempTo.equals(""))
				{
					to[iToCount]=tempTo;
					iToCount++;
					PrintOnConsole.debugPrintln("Email Service",tempTo);
				}
			}
			
			PrintOnConsole.debugPrintln("Email Service", "Adding Bcc List in sendMailAsHTMLTextByListToBcc Method");
			String[] bcc = new String[lstBcc.size()];
			int iBccCount=0;
			for(String tempBcc:lstBcc)
			{
				if(tempBcc!=null && !tempBcc.equals(""))
				{
					bcc[iBccCount]=tempBcc;
					iBccCount++;
					PrintOnConsole.debugPrintln("Email Service",tempBcc);
				}
			}
			
			javaMailSender.setHost(mgsmtpid);
			javaMailSender.setPort(Integer.parseInt(mgsmtppost));
			javaMailSender.setUsername(defaultEmail);
			javaMailSender.setPassword(defaultPass);

			javaMailSender.setDefaultEncoding("UTF-8");
			message = javaMailSender.createMimeMessage();

			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

			helperMsg.setSubject(subject);
			helperMsg.setTo(to);
			helperMsg.setBcc(bcc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail, fromText);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		try
		{
			javaMailSender.send(message);

		}
		catch(MailException ex)
		{
			ex.printStackTrace();
		}        

	}
	public void sendMailAsHTMLTextWithCC(String to,String subject, String text,String ccEmail[],String offerTxt)
	{
		MimeMessage message = javaMailSender.createMimeMessage();

		try
		{
			String mgsmtpid = "";
			String mgsmtppost = "";
			String defaultEmail = "";
			String defaultPass = "";
			String fromText = "";
			String fromEmail = "";
			mgsmtpid=properties.getProperty("smtphost.id");
			mgsmtppost=properties.getProperty("smtphost.port");
			defaultEmail = properties.getProperty("smtphost.defaultusername");
			defaultPass = properties.getProperty("smtphost.defaultpassword");
			String offerBccEmail = properties.getProperty("smtphost.offerbccemail");
			fromEmail=properties.getProperty("smtphost.defaultfrom");
			fromText=properties.getProperty("smtphost.defaultfromtext");
			int lengthcc=1;
			try{
				if(ccEmail!=null){
					lengthcc=ccEmail.length;
				}
			}catch(Exception e){ e.printStackTrace();}

			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			//String[] arrccEmail = properties.getProperty("smtphost.ccemail").split(",");
			String[] arrccEmail = ccEmail;
			
			int lengthArr=0;
			if(arrccEmail!=null){
				lengthArr=arrccEmail.length;
			}
			int totLength=lengthcc+lengthArr;
			
			String[] cc = new String[totLength];
			try{
				for(int i=0; i<arrccEmail.length;i++)
				{
					cc[i]	=	arrccEmail[i];
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(offerTxt!=null && arrccEmail==null){
				try{
					cc[lengthArr]=offerBccEmail;
				}catch(Exception e){e.printStackTrace();}
			}else{
				try{
					int ii=0;
					for(int j=lengthArr; j<totLength;j++){
						cc[j]	=	arrccEmail[ii];
						ii++;
					}
				}catch(Exception e){e.printStackTrace();}
			}
			/* ======== Gagan : Addinng Multiple Email in bcc array and sending email to all bcc emails ======*/
			
			try{
				for(int i=0;i<cc.length;i++){
					System.out.println("cc::"+cc[i]);
				}
			}catch(Exception e){}
			
			javaMailSender.setHost(mgsmtpid);
			javaMailSender.setPort(Integer.parseInt(mgsmtppost));
			javaMailSender.setUsername(defaultEmail);
			javaMailSender.setPassword(defaultPass);


			javaMailSender.setDefaultEncoding("UTF-8");
			message = javaMailSender.createMimeMessage();

			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);
			
			helperMsg.setSubject(subject);
			helperMsg.setTo(to);
			//helperMsg.setBcc(bccEmail);
			//helperMsg.setBcc(cc);
			helperMsg.setCc(cc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail, fromText);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}



		try
		{
			javaMailSender.send(message);

		}
		catch(MailException ex)
		{

			ex.printStackTrace();
		}        

	}
	
	public void sendMailAsHTMLByListToBccAndcc(List<String> lstTo,List<String> lstBcc,List<String> lstCc,String subject, String text)
	{

		MimeMessage message = javaMailSender.createMimeMessage();

		try
		{
			String mgsmtpid 	= 	"";
			String mgsmtppost 	= 	"";
			String defaultEmail = 	"";
			String defaultPass 	= 	"";
			String fromText 	= 	"";
			String fromEmail 	= 	"";

			mgsmtpid			=	properties.getProperty("smtphost.id");
			mgsmtppost			=	properties.getProperty("smtphost.port");
			defaultEmail 		= 	properties.getProperty("smtphost.defaultusername");
			defaultPass 		= 	properties.getProperty("smtphost.defaultpassword");
			fromEmail			=	properties.getProperty("smtphost.defaultfrom");
			fromText			=	properties.getProperty("smtphost.defaultfromtext");

			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");

			for(int i=0; i<arrBccEmail.length;i++)
			{
				if(arrBccEmail[i]!=null && !arrBccEmail[i].equals(""))
					lstBcc.add(arrBccEmail[i]);
			}

			PrintOnConsole.debugPrintln("Email Service", "Adding To List in sendMailAsHTMLTextByListToBcc Method");
			String[] to = new String[lstTo.size()];
			int iToCount=0;
			for(String tempTo:lstTo)
			{
				if(tempTo!=null && !tempTo.equals(""))
				{
					to[iToCount]=tempTo;
					iToCount++;
					PrintOnConsole.debugPrintln("Email Service",tempTo);
				}
			}
			
			PrintOnConsole.debugPrintln("Email Service", "Adding Bcc List in sendMailAsHTMLTextByListToBcc Method");
			String[] bcc = new String[lstBcc.size()];
			int iBccCount=0;
			for(String tempBcc:lstBcc)
			{
				if(tempBcc!=null && !tempBcc.equals(""))
				{
					bcc[iBccCount]=tempBcc;
					iBccCount++;
					PrintOnConsole.debugPrintln("BCC Email Service",tempBcc);
				}
			}
			
			/* Start CC Email Sent */
			String[] cc = new String[lstCc.size()];
			int iCcCount=0;
			for(String tempCc:lstCc)
			{
				if(tempCc!=null && !tempCc.equals(""))
				{
					cc[iCcCount]=tempCc;
					iCcCount++;
					PrintOnConsole.debugPrintln("CC Email Service",tempCc);
				}
			}		
			/*	End CC Email Sent */
			javaMailSender.setHost(mgsmtpid);
			javaMailSender.setPort(Integer.parseInt(mgsmtppost));
			javaMailSender.setUsername(defaultEmail);
			javaMailSender.setPassword(defaultPass);

			javaMailSender.setDefaultEncoding("UTF-8");
			message = javaMailSender.createMimeMessage();

			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

			helperMsg.setSubject(subject);
			helperMsg.setTo(to);
			helperMsg.setBcc(bcc);
			helperMsg.setCc(cc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail, fromText);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		try
		{
			javaMailSender.send(message);

		}
		catch(MailException ex)
		{
			ex.printStackTrace();
		}        
	}
	/*
	 * 	Dated		:	26-07-2014
	 * 	Created By	:	Hanzala Subhani
	 * 	Purpose		:	Send Email User and List Of CC
	 * 
	 */
	//public void sendMailAsHTMLByListToCc(String to,List<String> lstBcc,List<String> lstCc,String subject, String text)
	public void sendMailAsHTMLByListToCc(String to,List<String> lstBcc,List<String> lstCc,String subject, String text)
	 {
		System.out.println("::::::::::::::::::::::::::::::::::::::::::");
		MimeMessage message = javaMailSender.createMimeMessage();
		try
		{
			String mgsmtpid 	= 	"";
			String mgsmtppost 	= 	"";
			String defaultEmail = 	"";
			String defaultPass 	= 	"";
			String fromText 	= 	"";
			String fromEmail 	= 	"";
				
			mgsmtpid			=	properties.getProperty("smtphost.id");
			mgsmtppost			=	properties.getProperty("smtphost.port");
			defaultEmail 		= 	properties.getProperty("smtphost.defaultusername");
			defaultPass 		= 	properties.getProperty("smtphost.defaultpassword");
			fromEmail			=	properties.getProperty("smtphost.defaultfrom");
			fromText			=	properties.getProperty("smtphost.defaultfromtext");

			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");

			for(int i=0; i<arrBccEmail.length;i++)
			{
				if(arrBccEmail[i]!=null && !arrBccEmail[i].equals(""))
					lstBcc.add(arrBccEmail[i]);
			}
			
			PrintOnConsole.debugPrintln("Email Service", "Adding Bcc List in sendMailAsHTMLTextByListToBcc Method");
			String[] bcc = new String[lstBcc.size()];
			int iBccCount=0;
			for(String tempBcc:lstBcc)
			{
				if(tempBcc!=null && !tempBcc.equals(""))
				{
					bcc[iBccCount]=tempBcc;
					iBccCount++;
					PrintOnConsole.debugPrintln("BCC Email Service",tempBcc);
				}
			}
			
			/* Start CC Email Sent */
			String[] cc = new String[lstCc.size()];
			int iCcCount=0;
			for(String tempCc:lstCc)
			{
				if(tempCc!=null && !tempCc.equals(""))
				{
					cc[iCcCount]=tempCc;
					iCcCount++;
					PrintOnConsole.debugPrintln("CC Email Service",tempCc);
				}
			}		
			/*	End CC Email Sent */
			javaMailSender.setHost(mgsmtpid);
			javaMailSender.setPort(Integer.parseInt(mgsmtppost));
			javaMailSender.setUsername(defaultEmail);
			javaMailSender.setPassword(defaultPass);

			javaMailSender.setDefaultEncoding("UTF-8");
			message = javaMailSender.createMimeMessage();

			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);

			helperMsg.setSubject(subject);
			helperMsg.setTo(to);
			helperMsg.setBcc(bcc);
			helperMsg.setCc(cc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail, fromText);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		try
		{
			javaMailSender.send(message);

		}
		catch(MailException ex)
		{
			ex.printStackTrace();
		}        

	}
	public void sendMailAsHTMLTextCC(String[] to,String subject, String text,String ccEmail[])
	{
		MimeMessage message = javaMailSender.createMimeMessage();
		System.out.println("::::::::::::::sendMailAsHTMLTextCC new:::::::::::");
		try
		{
			String mgsmtpid = "";
			String mgsmtppost = "";
			String defaultEmail = "";
			String defaultPass = "";
			String fromText = "";
			String fromEmail = "";
			mgsmtpid=properties.getProperty("smtphost.id");
			mgsmtppost=properties.getProperty("smtphost.port");
			defaultEmail = properties.getProperty("smtphost.defaultusername");
			defaultPass = properties.getProperty("smtphost.defaultpassword");
			fromEmail=properties.getProperty("smtphost.defaultfrom");
			fromText=properties.getProperty("smtphost.defaultfromtext");
			
			String[] arrBccEmail = properties.getProperty("smtphost.bccemail").split(",");
			String[] bcc = new String[arrBccEmail.length];
			
			for(int i=0; i<arrBccEmail.length;i++){
				bcc[i]	=	arrBccEmail[i];
			}
			
			javaMailSender.setHost(mgsmtpid);
			javaMailSender.setPort(Integer.parseInt(mgsmtppost));
			javaMailSender.setUsername(defaultEmail);
			javaMailSender.setPassword(defaultPass);


			javaMailSender.setDefaultEncoding("UTF-8");
			message = javaMailSender.createMimeMessage();
			try{
				for(int i=0;i<ccEmail.length;i++){
					System.out.println("ccEmail::"+ccEmail[i]);
				}
			}catch(Exception e){}
			
			MimeMessageHelper helperMsg = new MimeMessageHelper(message, true);
			
			helperMsg.setSubject(subject);
			helperMsg.setTo(to);
			try{
				if(ccEmail.length>0){
					helperMsg.setCc(ccEmail);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			helperMsg.setBcc(bcc);
			helperMsg.setText(text,true);
			helperMsg.setFrom(fromEmail, fromText);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		try
		{
			javaMailSender.send(message);

		}
		catch(MailException ex)
		{

			ex.printStackTrace();
		}        

	}

}