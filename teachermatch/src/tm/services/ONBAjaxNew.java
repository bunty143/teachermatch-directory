package tm.services;

/**********************************
     ***********************
	*  @Author Ram Nath  *
   **********************
******************************/

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import tm.bean.EligibilityStatusMaster;

@Service
public interface ONBAjaxNew {
	public Map<String,String> getDistrictWiseColumn(Integer districtId);
	public JSONObject displayONBGrid(Map<String,String> allMap);
	public JSONObject getStatusJSON(Map<String,String> allMap);
	public boolean deleteStatusONB(Integer EVHId);
	public boolean saveNotesONB(Map<String,String> allMap);
	public boolean sendToPeopleSoftONB(Long[] selectedTSHId);
	public boolean sendToPeopleSoftONB(Long[] selectedTSHId,String errorMessage);
	public String generateOnboardingPDF(Map<String,String> allMap);
	public String generateOnboardingExcel(Map<String,String> allMap);
	public Long getJFTIdByJobIdAndTeacherId(Map<String,String> allMap);
	public List<String[]>  getAllStatusByPuttingStatus(Map<String,String> allMap);
	public String onBoardingSendMailToTeacher(Long teacherHistoryId);
	public String getQQOThumb();
	public List<EligibilityStatusMaster> getStatusByEidJSON(Integer eligibilityId);
}