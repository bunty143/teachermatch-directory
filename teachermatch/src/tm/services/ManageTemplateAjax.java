package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.DistrictTemplatesforMessages;
import tm.bean.EmailMessageTemplatesForFacilitators;
import tm.bean.EventDescriptionTemplates;
import tm.bean.EventEmailMessageTemplates;
import tm.bean.StatusWiseEmailSection;
import tm.bean.StatusWiseEmailSubSection;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictTemplatesforMessagesDAO;
import tm.dao.EmailMessageTemplatesForFacilitatorsDAO;
import tm.dao.EventDescriptionTemplatesDAO;
import tm.dao.EventEmailMessageTemplatesDAO;
import tm.dao.StatusWiseEmailSectionDAO;
import tm.dao.StatusWiseEmailSubSectionDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

public class ManageTemplateAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private DistrictTemplatesforMessagesDAO districtTemplatesforMessagesDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private EventDescriptionTemplatesDAO eventDescriptionTemplatesDAO;
	
	@Autowired
	private EmailMessageTemplatesForFacilitatorsDAO emailMessageTemplatesForFacilitatorsDAO;
	
	@Autowired
	private EventEmailMessageTemplatesDAO eventEmailMessageTemplatesDAO;
	
	@Autowired
	private StatusWiseEmailSectionDAO statusWiseEmailSectionDAO;
	
	@Autowired
	private StatusWiseEmailSubSectionDAO statusWiseEmailSubSectionDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	public int saveTemplate(Integer templateId,Integer districtId,Integer templateType,String templateName,String subject,String templateBody,String statusName)
	{
		System.out.println(" =========== saveTemplate ============= districtId "+districtId);
		WebContext context;
		String[] data= new String[2];
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") ==null) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		
		DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
		
		if(templateType==1)
		{
			DistrictTemplatesforMessages districtTemplatesforMessages = null;
			
			if(templateId==null)
			{
				districtTemplatesforMessages = new DistrictTemplatesforMessages();
			}
			else
			{
				districtTemplatesforMessages = districtTemplatesforMessagesDAO.findById(templateId, false, false);
			}
			
			districtTemplatesforMessages.setCreatedDateTime(new Date());
			districtTemplatesforMessages.setDistrictMaster(districtMaster);
			districtTemplatesforMessages.setRole(null);
			districtTemplatesforMessages.setStatus("A");
			districtTemplatesforMessages.setSubjectLine(subject);
			districtTemplatesforMessages.setTemplateBody(templateBody);
			districtTemplatesforMessages.setTemplateName(templateName);
			districtTemplatesforMessages.setUserMaster(userMaster);
			districtTemplatesforMessagesDAO.makePersistent(districtTemplatesforMessages);
			
			return 1;
		}
		else if(templateType==2)
		{
			EventDescriptionTemplates eventDescriptionTemplates = null;
			
			if(templateId==null)
			{
				eventDescriptionTemplates = new EventDescriptionTemplates();
			}
			else
			{
				eventDescriptionTemplates = eventDescriptionTemplatesDAO.findById(templateId, false, false);
			}
			
		//	DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			
			eventDescriptionTemplates.setCreatedDateTime(new Date());
			eventDescriptionTemplates.setDistrictMaster(districtMaster);
			//eventDescriptionTemplates.setRole(null);
			eventDescriptionTemplates.setStatus("A");
			//eventDescriptionTemplates.setSubjectLine(subject);
			eventDescriptionTemplates.setTemplateBody(templateBody);
			eventDescriptionTemplates.setTemplateName(templateName);
			eventDescriptionTemplates.setCreatedBY(userMaster);
			eventDescriptionTemplatesDAO.makePersistent(eventDescriptionTemplates);
			
			return 1;
		}
		else if(templateType==3)
		{
			System.out.println(" facilitator>>>>>>>>>>>>>>>>>>>>");
			EmailMessageTemplatesForFacilitators emailMessageTemplatesForFacilitators = null;
			
			if(templateId==null)
			{
				emailMessageTemplatesForFacilitators = new EmailMessageTemplatesForFacilitators();
			}
			else
			{
				emailMessageTemplatesForFacilitators = emailMessageTemplatesForFacilitatorsDAO.findById(templateId, false, false);
			}
			
		//	DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			
			emailMessageTemplatesForFacilitators.setCreatedDateTime(new Date());
			emailMessageTemplatesForFacilitators.setDistritMaster(districtMaster);
	//		emailMessageTemplatesForFacilitators.setRole(null);
			emailMessageTemplatesForFacilitators.setStatus("A");
			emailMessageTemplatesForFacilitators.setSubjectLine(subject);
			emailMessageTemplatesForFacilitators.setTemplateBody(templateBody);
			emailMessageTemplatesForFacilitators.setTemplateName(templateName);
			emailMessageTemplatesForFacilitators.setCreatedBY(userMaster);
			emailMessageTemplatesForFacilitatorsDAO.makePersistent(emailMessageTemplatesForFacilitators);
			
			return 1;
		}
		else if(templateType==4)
		{
			EventEmailMessageTemplates eventEmailMessageTemplates = null;
			
			if(templateId==null)
			{
				eventEmailMessageTemplates = new EventEmailMessageTemplates();
			}
			else
			{
				eventEmailMessageTemplates = eventEmailMessageTemplatesDAO.findById(templateId, false, false);
			}
			
		//	DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			
			eventEmailMessageTemplates.setCreatedDateTime(new Date());
			eventEmailMessageTemplates.setDistrictMaster(districtMaster);
		//	eventEmailMessageTemplates.setRole(null);
			eventEmailMessageTemplates.setStatus("A");
			eventEmailMessageTemplates.setSubjectLine(subject);
			eventEmailMessageTemplates.setTemplateBody(templateBody);
			eventEmailMessageTemplates.setTemplateName(templateName);
			eventEmailMessageTemplates.setCreatedBY(userMaster);
			eventEmailMessageTemplatesDAO.makePersistent(eventEmailMessageTemplates);
			
			return 1;
		}
		else if(templateType==5)
		{
			List<StatusWiseEmailSection> emailSections = new ArrayList<StatusWiseEmailSection>();
			StatusWiseEmailSection section = null;
			emailSections = statusWiseEmailSectionDAO.findByDistrictAndSectionName(districtMaster, statusName);
			
			if(emailSections.size()==0)
			{
				section = new StatusWiseEmailSection();
				
				section.setCreatedBy(userMaster.getUserId());
				section.setCreatedDateTime(new Date());
				section.setDistrictId(districtMaster.getDistrictId());
				section.setSectionName(statusName);
				section.setStatus("A");
				
				statusWiseEmailSectionDAO.makePersistent(section);
				
			}
			
			
			StatusWiseEmailSubSection statusWiseEmailSubSection = null;
			
			if(templateId==null)
			{
				statusWiseEmailSubSection = new StatusWiseEmailSubSection();
			}
			else
			{
				statusWiseEmailSubSection = statusWiseEmailSubSectionDAO.findById(templateId, false, false);
			}
			
		//	DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			
			
			//System.out.println(" emailSections.get(0) "+emailSections.get(0).getSectionName()+" templateName "+templateName+" userMaster.getEntityType() "+userMaster.getEntityType());
			try
			{
				statusWiseEmailSubSection.setCreatedDateTime(new Date());
				if(emailSections.size()>0)
					statusWiseEmailSubSection.setStatusWiseEmailSection(emailSections.get(0));
				else
					statusWiseEmailSubSection.setStatusWiseEmailSection(section);
			//	eventEmailMessageTemplates.setRole(null);
				statusWiseEmailSubSection.setStatus("A");
				statusWiseEmailSubSection.setSubSectionName(templateName);
				statusWiseEmailSubSection.setTemplateBody(templateBody);
			//	statusWiseEmailSubSection.set(tamplateName);
				statusWiseEmailSubSection.setCreatedBy(userMaster.getUserId());
				statusWiseEmailSubSectionDAO.makePersistent(statusWiseEmailSubSection);
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			
			return 1;
		}
		return 0;
	}
	
	

	public String displayTemplatesForDistrict(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer districtId,Integer templateType)
	{
		System.out.println("================ displayTemplates ==================");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
		//	boolean statusflag 	= false;
		//	boolean districtFlag 	= false;
			boolean allData	   	= false;
		//	boolean quesTextflag= false;

			
			DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			if(session.getAttribute("displayType").toString().equalsIgnoreCase("1") && districtMaster!=null){
				session.setAttribute("districtMasterId", districtMaster.getDistrictId());
				session.setAttribute("districtMasterName", districtMaster.getDistrictName());
			}
			else{
				session.setAttribute("districtMasterId", 0);
				session.setAttribute("districtMasterName", "");
			}
			List<DistrictTemplatesforMessages> districtTemplatesforMessagesList = new ArrayList<DistrictTemplatesforMessages>();
			List<DistrictTemplatesforMessages> filterData				=	new ArrayList<DistrictTemplatesforMessages>();
			List<DistrictTemplatesforMessages> finalDatalist			=	new ArrayList<DistrictTemplatesforMessages>();
			
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"templateName";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			
			
			districtTemplatesforMessagesList = districtTemplatesforMessagesDAO.findByDistrict(sortOrderStrVal, districtMaster);
			if(districtTemplatesforMessagesList.size()>0)
			{
				filterData.addAll(districtTemplatesforMessagesList);
				allData = true;
			}

				
			
			totalRecord = filterData.size();
			if(totalRecord<end)
				end=totalRecord;
			
			finalDatalist	=	filterData.subList(start,end);
			
			dmRecords.append("<table  id='templateTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			//dmRecords.append("<th width='55%'>Domain Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTemplateName", locale),sortOrderFieldName,"templateName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgSubjectLine", locale),sortOrderFieldName,"subjectLine",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
	//		responseText=PaginationAndSorting.responseSortingLink("Template&nbsp;Body",sortOrderFieldName,"templateBody",sortOrderTypeVal,pgNo);
	//		dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblcretedDate", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			dmRecords.append("<th width=''><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(finalDatalist.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNoTemplatefound", locale)+"</td></tr>" );
			System.out.println(Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+finalDatalist.size());
			
			int iCount =0;
			for (DistrictTemplatesforMessages i4qp: finalDatalist) 
			{
				
				
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+i4qp.getTemplateName()+"</td>");
				dmRecords.append("<td>"+i4qp.getSubjectLine()+"</td>");
			//	dmRecords.append("<td>"+tbody+"</td>");
			//	dmRecords.append("<td>"+i4qp.getTemplateBody()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(i4qp.getCreatedDateTime())+"</td>");
				dmRecords.append("<td><a href='javascript:void(0);' title='Edit' onclick='return showEditTemplate("+i4qp.getTemplateId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>");
				if(i4qp.getStatus().equalsIgnoreCase("A"))
				{
					dmRecords.append(" | <a href='javascript:void(0);' title='Activate' onclick=\"activateDeactivateTemplate("+i4qp.getTemplateId()+",'I')\"><i class='fa fa-check fa-lg'></i></a>");
				}else if(i4qp.getStatus().equalsIgnoreCase("I"))
				{
					dmRecords.append(" | <a href='javascript:void(0);' title='Deactivate' onclick=\"activateDeactivateTemplate("+i4qp.getTemplateId()+",'A')\"><i class='fa fa-times fa-lg'></i></a>");
				}
				
				dmRecords.append("</td>");
				dmRecords.append("</tr>");
				iCount++;
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjaxDSPQ(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	
	public String displayTemplatesForEventDescription(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer districtId,Integer templateType)
	{
		System.out.println("================ displayTemplates ==================");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			boolean allData	   	= false;
		
			DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			if(session.getAttribute("displayType").toString().equalsIgnoreCase("1")){
				session.setAttribute("districtMasterId", districtMaster.getDistrictId());
				session.setAttribute("districtMasterName", districtMaster.getDistrictName());
			}
			else{
				session.setAttribute("districtMasterId", 0);
				session.setAttribute("districtMasterName", "");
			}
			List<EventDescriptionTemplates> eventDescriptionTemplatesList = new ArrayList<EventDescriptionTemplates>();
			List<EventDescriptionTemplates> filterData				=	new ArrayList<EventDescriptionTemplates>();
			List<EventDescriptionTemplates> finalDatalist			=	new ArrayList<EventDescriptionTemplates>();
			
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"templateName";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			
			
		//	eventDescriptionTemplatesList = districtTemplatesforMessagesDAO.findByDistrict(sortOrderStrVal, districtMaster);
			
			eventDescriptionTemplatesList = eventDescriptionTemplatesDAO.findByDistrict(sortOrderStrVal, districtMaster);
			
			if(eventDescriptionTemplatesList.size()>0)
			{
				filterData.addAll(eventDescriptionTemplatesList);
				allData = true;
			}

				
			
			totalRecord = filterData.size();
			if(totalRecord<end)
				end=totalRecord;
			
			finalDatalist	=	filterData.subList(start,end);
			
			dmRecords.append("<table  id='templateTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			//dmRecords.append("<th width='55%'>Domain Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTemplateName", locale),sortOrderFieldName,"templateName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
	//		responseText=PaginationAndSorting.responseSortingLink("Subject&nbsp;Line",sortOrderFieldName,"templateName",sortOrderTypeVal,pgNo);
	//		dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
	//		responseText=PaginationAndSorting.responseSortingLink("Template&nbsp;Body",sortOrderFieldName,"templateBody",sortOrderTypeVal,pgNo);
	//		dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblcretedDate", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			dmRecords.append("<th width=''><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(finalDatalist.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNoTemplatefound", locale)+"</td></tr>" );
			System.out.println(Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+finalDatalist.size());
			
			int iCount =0;
			for (EventDescriptionTemplates i4qp: finalDatalist) 
			{
				
				
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+i4qp.getTemplateName()+"</td>");
			//	dmRecords.append("<td>"+i4qp.getTemplateName()+"</td>");
			//	dmRecords.append("<td>"+tbody+"</td>");
			//	dmRecords.append("<td>"+i4qp.getTemplateBody()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(i4qp.getCreatedDateTime())+"</td>");
				dmRecords.append("<td><a href='javascript:void(0);' title='Edit' onclick='return showEditTemplate("+i4qp.getTemplateId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>");
				if(i4qp.getStatus().equalsIgnoreCase("A"))
				{
					dmRecords.append(" | <a href='javascript:void(0);' title='Deactivate' onclick=\"activateDeactivateTemplate("+i4qp.getTemplateId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>");
				}else if(i4qp.getStatus().equalsIgnoreCase("I"))
				{
					dmRecords.append(" | <a href='javascript:void(0);' title='Activate' onclick=\"activateDeactivateTemplate("+i4qp.getTemplateId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>");
				}
				
				dmRecords.append("</td>");
				dmRecords.append("</tr>");
				iCount++;
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public String displayTemplatesForEventFacilitator(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer districtId,Integer templateType)
	{
		System.out.println("================ displayTemplates ==================");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			boolean allData	   	= false;
			
			DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			if(session.getAttribute("displayType").toString().equalsIgnoreCase("1")){
				session.setAttribute("districtMasterId", districtMaster.getDistrictId());
				session.setAttribute("districtMasterName", districtMaster.getDistrictName());
			}
			else{
				session.setAttribute("districtMasterId", 0);
				session.setAttribute("districtMasterName", "");
			}
			List<EmailMessageTemplatesForFacilitators> emailMessageTemplatesForFacilitatorsList = new ArrayList<EmailMessageTemplatesForFacilitators>();
			List<EmailMessageTemplatesForFacilitators> filterData				=	new ArrayList<EmailMessageTemplatesForFacilitators>();
			List<EmailMessageTemplatesForFacilitators> finalDatalist			=	new ArrayList<EmailMessageTemplatesForFacilitators>();
			
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"templateName";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			
			emailMessageTemplatesForFacilitatorsList = emailMessageTemplatesForFacilitatorsDAO.findByDistrict(sortOrderStrVal, districtMaster);
			if(emailMessageTemplatesForFacilitatorsList.size()>0)
			{
				filterData.addAll(emailMessageTemplatesForFacilitatorsList);
				allData = true;
			}

				
			
			totalRecord = filterData.size();
			if(totalRecord<end)
				end=totalRecord;
			
			finalDatalist	=	filterData.subList(start,end);
			
			dmRecords.append("<table  id='templateTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			//dmRecords.append("<th width='55%'>Domain Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTemplateName", locale),sortOrderFieldName,"templateName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Subject&nbsp;Line",sortOrderFieldName,"subjectLine",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
	//		responseText=PaginationAndSorting.responseSortingLink("Template&nbsp;Body",sortOrderFieldName,"templateBody",sortOrderTypeVal,pgNo);
	//		dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblcretedDate", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			dmRecords.append("<th width=''><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(finalDatalist.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNoTemplatefound", locale)+"</td></tr>" );
			System.out.println(Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+finalDatalist.size());
			
			int iCount =0;
			for (EmailMessageTemplatesForFacilitators i4qp: finalDatalist) 
			{
				
				
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+i4qp.getTemplateName()+"</td>");
				dmRecords.append("<td>"+i4qp.getSubjectLine()+"</td>");
			//	dmRecords.append("<td>"+tbody+"</td>");
			//	dmRecords.append("<td>"+i4qp.getTemplateBody()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(i4qp.getCreatedDateTime())+"</td>");
				dmRecords.append("<td><a href='javascript:void(0);' title='Edit' onclick='return showEditTemplate("+i4qp.getEmailMessageTemplatesForFacilitatorsId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>");
				if(i4qp.getStatus().equalsIgnoreCase("A"))
				{
					dmRecords.append(" | <a href='javascript:void(0);' title='Deactivate' onclick=\"activateDeactivateTemplate("+i4qp.getEmailMessageTemplatesForFacilitatorsId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>");
				}else if(i4qp.getStatus().equalsIgnoreCase("I"))
				{
					dmRecords.append(" | <a href='javascript:void(0);' title='Activate' onclick=\"activateDeactivateTemplate("+i4qp.getEmailMessageTemplatesForFacilitatorsId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>");
				}
				
				dmRecords.append("</td>");
				dmRecords.append("</tr>");
				iCount++;
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	
	public String displayTemplatesForEventParticipant(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer districtId,Integer templateType)
	{
		System.out.println("================ displayTemplates ==================");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			boolean allData	   	= false;
			
			DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			if(session.getAttribute("displayType").toString().equalsIgnoreCase("1")){
				session.setAttribute("districtMasterId", districtMaster.getDistrictId());
				session.setAttribute("districtMasterName", districtMaster.getDistrictName());
			}
			else{
				session.setAttribute("districtMasterId", 0);
				session.setAttribute("districtMasterName", "");
			}
			List<EventEmailMessageTemplates> eventEmailMessageTemplatesList = new ArrayList<EventEmailMessageTemplates>();
			List<EventEmailMessageTemplates> filterData				=	new ArrayList<EventEmailMessageTemplates>();
			List<EventEmailMessageTemplates> finalDatalist			=	new ArrayList<EventEmailMessageTemplates>();
			
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"templateName";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			
			
			eventEmailMessageTemplatesList = eventEmailMessageTemplatesDAO.findByDistrict(sortOrderStrVal, districtMaster);
			if(eventEmailMessageTemplatesList.size()>0)
			{
				filterData.addAll(eventEmailMessageTemplatesList);
				allData = true;
			}

				
			
			totalRecord = filterData.size();
			if(totalRecord<end)
				end=totalRecord;
			
			finalDatalist	=	filterData.subList(start,end);
			
			dmRecords.append("<table  id='templateTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			//dmRecords.append("<th width='55%'>Domain Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTemplateName", locale),sortOrderFieldName,"templateName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Subject&nbsp;Line",sortOrderFieldName,"subjectLine",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
	//		responseText=PaginationAndSorting.responseSortingLink("Template&nbsp;Body",sortOrderFieldName,"templateBody",sortOrderTypeVal,pgNo);
	//		dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblcretedDate", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			dmRecords.append("<th width=''><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(finalDatalist.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNoTemplatefound", locale)+"</td></tr>" );
			System.out.println(Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+finalDatalist.size());
			
			int iCount =0;
			for (EventEmailMessageTemplates i4qp: finalDatalist) 
			{
				
				
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+i4qp.getTemplateName()+"</td>");
				dmRecords.append("<td>"+i4qp.getSubjectLine()+"</td>");
			//	dmRecords.append("<td>"+tbody+"</td>");
			//	dmRecords.append("<td>"+i4qp.getTemplateBody()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(i4qp.getCreatedDateTime())+"</td>");
				dmRecords.append("<td><a href='javascript:void(0);' title='Edit' onclick='return showEditTemplate("+i4qp.getTemplateId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>");
				if(i4qp.getStatus().equalsIgnoreCase("A"))
				{
					dmRecords.append(" | <a href='javascript:void(0);' title='Deactivate' onclick=\"activateDeactivateTemplate("+i4qp.getTemplateId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>");
				}else if(i4qp.getStatus().equalsIgnoreCase("I"))
				{
					dmRecords.append(" | <a href='javascript:void(0);' title='Activate' onclick=\"activateDeactivateTemplate("+i4qp.getTemplateId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>");
				}
				
				dmRecords.append("</td>");
				dmRecords.append("</tr>");
				iCount++;
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public String displayTemplatesForEventStatus(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer districtId,String statusName)
	{
		System.out.println("================ displayTemplates ==================");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			boolean allData	   	= false;
			
			DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			if(session.getAttribute("displayType").toString().equalsIgnoreCase("1")){
				session.setAttribute("districtMasterId", districtMaster.getDistrictId());
				session.setAttribute("districtMasterName", districtMaster.getDistrictName());
			}
			else{
				session.setAttribute("districtMasterId", 0);
				session.setAttribute("districtMasterName", "");
			}
			List<StatusWiseEmailSection> emailSections = new ArrayList<StatusWiseEmailSection>();
			
			List<StatusWiseEmailSubSection> statusWiseEmailSubSections = new ArrayList<StatusWiseEmailSubSection>();
			List<StatusWiseEmailSubSection> filterData				=	new ArrayList<StatusWiseEmailSubSection>();
			List<StatusWiseEmailSubSection> finalDatalist			=	new ArrayList<StatusWiseEmailSubSection>();
			
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"subSectionName";

			System.out.println(" set sortOrderFieldName :: "+sortOrderFieldName);
			
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			
			System.out.println("sortOrderFieldName :: "+sortOrderFieldName+" sortOrder "+sortOrder);
			
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			System.out.println(" 01 sortOrderStrVal"+sortOrderStrVal);
			
			emailSections = statusWiseEmailSectionDAO.findByDistrictAndSectionName(districtMaster, statusName);
			
			////////// Add status, If not exist in template table ///////////////
			if(emailSections.size()==0)
			{
				saveSectionName(districtMaster,statusName);
			}
			
			
			/////////////////////////////////////////////////////////////////////
			if(emailSections.size()>0)
				statusWiseEmailSubSections = statusWiseEmailSubSectionDAO.findByEmailSection(sortOrderStrVal,emailSections.get(0));
			
			if(statusWiseEmailSubSections.size()>0)
			{
				filterData.addAll(statusWiseEmailSubSections);
				allData = true;
			}

				
			
			totalRecord = filterData.size();
			if(totalRecord<end)
				end=totalRecord;
			
			finalDatalist	=	filterData.subList(start,end);
			
			dmRecords.append("<table  id='templateTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			//dmRecords.append("<th width='55%'>Domain Name</th>");
			String responseText="";
	//		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTemplateName", locale),sortOrderFieldName,"subSectionName",sortOrderTypeVal,pgNo);
	//		dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTemplateName", locale),sortOrderFieldName,"subSectionName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
	//		responseText=PaginationAndSorting.responseSortingLink("Template&nbsp;Body",sortOrderFieldName,"templateBody",sortOrderTypeVal,pgNo);
	//		dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblcretedDate", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			dmRecords.append("<th width=''><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(finalDatalist.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNoTemplatefound", locale)+"</td></tr>" );
			System.out.println(Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+finalDatalist.size());
			
			int iCount =0;
			for (StatusWiseEmailSubSection i4qp: finalDatalist) 
			{
				
				
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+i4qp.getSubSectionName()+"</td>");
		//		dmRecords.append("<td>"+i4qp.getSubSectionName()+"</td>");
			//	dmRecords.append("<td>"+tbody+"</td>");
			//	dmRecords.append("<td>"+i4qp.getTemplateBody()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(i4qp.getCreatedDateTime())+"</td>");
				dmRecords.append("<td><a href='javascript:void(0);' title='Edit' onclick='return showEditTemplate("+i4qp.getEmailSubSectionId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>");
				if(i4qp.getStatus().equalsIgnoreCase("A"))
				{
					dmRecords.append(" | <a href='javascript:void(0);' title='Deactivate' onclick=\"activateDeactivateTemplate("+i4qp.getEmailSubSectionId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>");
				}else if(i4qp.getStatus().equalsIgnoreCase("I"))
				{
					dmRecords.append(" | <a href='javascript:void(0);' title='Activate' onclick=\"activateDeactivateTemplate("+i4qp.getEmailSubSectionId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>");
				}
				
				dmRecords.append("</td>");
				dmRecords.append("</tr>");
				iCount++;
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjaxDSPQ(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public DistrictTemplatesforMessages showEditTemplate(Integer templateId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		DistrictTemplatesforMessages districtTemplatesforMessages = null;
		
		try
		{
			districtTemplatesforMessages = districtTemplatesforMessagesDAO.findById(templateId, false, false);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return  districtTemplatesforMessages;
	}

	
	public EventDescriptionTemplates editTemplateForEventDiscription(Integer templateId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		EventDescriptionTemplates eventDescriptionTemplates = null;
		
		try
		{
			eventDescriptionTemplates = eventDescriptionTemplatesDAO.findById(templateId, false, false);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return  eventDescriptionTemplates;
	}
	
	public EmailMessageTemplatesForFacilitators editTemplateForEventFacilitator(Integer templateId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		EmailMessageTemplatesForFacilitators eveEmailMessageTemplatesForFacilitators = null;
		
		try
		{
			eveEmailMessageTemplatesForFacilitators = emailMessageTemplatesForFacilitatorsDAO.findById(templateId, false, false);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return  eveEmailMessageTemplatesForFacilitators;
	}
	
	public EventEmailMessageTemplates editTemplateForEventParticipant(Integer templateId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		EventEmailMessageTemplates eventEmailMessageTemplates = null;
		
		try
		{
			eventEmailMessageTemplates = eventEmailMessageTemplatesDAO.findById(templateId, false, false);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return  eventEmailMessageTemplates;
	}
	
	public StatusWiseEmailSubSection editTemplateForStatus(Integer templateId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		StatusWiseEmailSubSection statusWiseEmailSubSection = null;
		
		try
		{
			statusWiseEmailSubSection = statusWiseEmailSubSectionDAO.findById(templateId, false, false);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return  statusWiseEmailSubSection;
	}
	
	
	
	
	public void activateDeactivateTemplate(Integer templateId,String status,Integer templateType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		
		try
		{
			if(templateType==1)
			{
				DistrictTemplatesforMessages districtTemplatesforMessages = null;
				districtTemplatesforMessages = districtTemplatesforMessagesDAO.findById(templateId, false, false);
				districtTemplatesforMessages.setStatus(status);
				districtTemplatesforMessagesDAO.makePersistent(districtTemplatesforMessages);
			}
			else if(templateType==2)
			{
				EventDescriptionTemplates eventDescriptionTemplates = null;
				eventDescriptionTemplates = eventDescriptionTemplatesDAO.findById(templateId, false, false);
				eventDescriptionTemplates.setStatus(status);
				eventDescriptionTemplatesDAO.makePersistent(eventDescriptionTemplates);
			}
			else if(templateType==3)
			{
				EmailMessageTemplatesForFacilitators emailMessageTemplatesForFacilitators = null;
				emailMessageTemplatesForFacilitators = emailMessageTemplatesForFacilitatorsDAO.findById(templateId, false, false);
				emailMessageTemplatesForFacilitators.setStatus(status);
				emailMessageTemplatesForFacilitatorsDAO.makePersistent(emailMessageTemplatesForFacilitators);
			}
			else if(templateType==4)
			{
				EventEmailMessageTemplates eventEmailMessageTemplates = null;
				eventEmailMessageTemplates = eventEmailMessageTemplatesDAO.findById(templateId, false, false);
				eventEmailMessageTemplates.setStatus(status);
				eventEmailMessageTemplatesDAO.makePersistent(eventEmailMessageTemplates);
			}
			else if(templateType==5)
			{
				StatusWiseEmailSubSection statusWiseEmailSubSection = null;
				statusWiseEmailSubSection = statusWiseEmailSubSectionDAO.findById(templateId, false, false);
				statusWiseEmailSubSection.setStatus(status);
				statusWiseEmailSubSectionDAO.makePersistent(statusWiseEmailSubSection);
			}
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public String getStatusNameData(Integer districtId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		StringBuilder sb = new StringBuilder();
		
		try
		{
			DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			List<StatusMaster> statusMasters = WorkThreadServlet.statusMasters;
			List<SecondaryStatus> secondaryStatusList = secondaryStatusDAO.findSecondaryStatusByDistrictOnly(districtMaster);
			SortedSet<String> allStatusList = new TreeSet<String>();
			
			for(StatusMaster objStatusMaster:statusMasters)
			{
				if(!objStatusMaster.getStatus().equalsIgnoreCase("Timed Out") && !objStatusMaster.getStatus().equalsIgnoreCase("Available") && !objStatusMaster.getStatus().equalsIgnoreCase("Completed") && !objStatusMaster.getStatus().equalsIgnoreCase("Incomplete"))
				{
					allStatusList.add(objStatusMaster.getStatus());
				}
			}

			for(SecondaryStatus objSecondStatus:secondaryStatusList)
			{
				if(!objSecondStatus.getSecondaryStatusName().equalsIgnoreCase("Timed Out") && !objSecondStatus.getSecondaryStatusName().equalsIgnoreCase("Available") && !objSecondStatus.getSecondaryStatusName().equalsIgnoreCase("Completed") && !objSecondStatus.getSecondaryStatusName().equalsIgnoreCase("Incomplete"))
				{
					allStatusList.add(objSecondStatus.getSecondaryStatusName());
				}
			}
			
			if(allStatusList.size()>0)
			{
				sb.append("<select id='statusString' class='form-control top20'>");
				sb.append("<option id='slst' value='' selected='selected' >"+Utility.getLocaleValuePropByKey("sltStatis", locale)+"</option>");
				for(String obj:allStatusList)
				{
					sb.append("<option id='' value='"+obj+"' >"+obj+"</option>");
				}
				sb.append("</select>");
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		return sb.toString();
	}
	
	
	public String saveSectionName(DistrictMaster districtMaster,String sectionName)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			
		}
		
		try
		{
			if(sectionName=="")
			{
				StatusWiseEmailSection statusWiseEmailSection = new StatusWiseEmailSection();
				statusWiseEmailSection.setCreatedBy(userMaster.getUserId());
				statusWiseEmailSection.setDistrictId(districtMaster.getDistrictId());
				statusWiseEmailSection.setCreatedDateTime(new Date());
				statusWiseEmailSection.setSectionName(sectionName);
				statusWiseEmailSection.setStatus("A");
				statusWiseEmailSectionDAO.makePersistent(statusWiseEmailSection);
			}
			return "1";
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "2";
	}
	
}


