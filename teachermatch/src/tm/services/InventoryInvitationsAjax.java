package tm.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.assessment.InventoryInvitations;
import tm.bean.assessment.InventoryInvitationsTemp;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.TeacherDetailDAO;
import tm.dao.assessment.InventoryInvitationsDAO;
import tm.dao.assessment.InventoryInvitationsTempDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.AESEncryption;
import tm.utility.Utility;

/**
 * @author Amit Chaudhary
 * @date 08-June-2015
 */
public class InventoryInvitationsAjax
{
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setAssessmentDetailDAO(DistrictMasterDAO districtMasterDAO){
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO){
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired
	private InventoryInvitationsTempDAO inventoryInvitationsTempDAO;
	public void setInventoryInvitationsTempDAO(InventoryInvitationsTempDAO inventoryInvitationsTempDAO){
		this.inventoryInvitationsTempDAO = inventoryInvitationsTempDAO;
	}
	
	@Autowired
	private InventoryInvitationsDAO inventoryInvitationsDAO;
	public void setInventoryInvitationsDAO(InventoryInvitationsDAO inventoryInvitationsDAO){
		this.inventoryInvitationsDAO = inventoryInvitationsDAO;
	}
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO){
		this.userMasterDAO = userMasterDAO;
	}

	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService){
		this.emailerService = emailerService;
	}
	
	String locale = Utility.getValueOfPropByKey("locale");	 
	
	@SuppressWarnings("unchecked")
	private Vector vectorDataExcelXLSX = new Vector();

	@Transactional(readOnly=false)
	public int getDistrictWithAuthKey(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMaster")==null))
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		
		String authKey = null;
    	authKey = districtMasterDAO.findByCriteria(Restrictions.eq("districtId", districtId)).get(0).getAuthKey();
		if(authKey!=null)
			return 1;
		else
			return 0;
	}	
	
	@SuppressWarnings("unused")
	public String displayTempTeacherRecords(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords = new StringBuffer();
		try{
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			UserMaster userMaster=null;
			int roleId=0;
			
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			List<InventoryInvitationsTemp> inventoryInvitationsTempList = null;
			String sortOrderFieldName =	"inventoryInvitationsTempId";
			Order sortOrderStrVal = null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				 {
					 sortOrderFieldName		=	sortOrder;
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			
			Criterion criterion = Restrictions.eq("sessionId",session.getId());
			totalRecord = inventoryInvitationsTempDAO.getRowCountTempTeacher(criterion);
			inventoryInvitationsTempList = inventoryInvitationsTempDAO.findByTempTeacher(sortOrderStrVal,start,noOfRowInPage,criterion);
			
			dmRecords.append("<table  id='tempTeacherTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLink("User First Name",sortOrderFieldName,"candidateFirstName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("User Last Name",sortOrderFieldName,"candidateLastName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("User Email",sortOrderFieldName,"candidateEmail",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Errors",sortOrderFieldName,"errorText",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			dmRecords.append("</thead>");
			
			if(inventoryInvitationsTempList.size()==0)
				dmRecords.append("<tr><td colspan='6' align='center'>No Candidate found</td></tr>" );

			for (InventoryInvitationsTemp inventoryInvitationsTemp : inventoryInvitationsTempList){
				String fName=inventoryInvitationsTemp.getCandidateFirstName();
				String lName=inventoryInvitationsTemp.getCandidateLastName();
				String email=inventoryInvitationsTemp.getCandidateEmail();
				String rowCss="";
				
				if(!inventoryInvitationsTemp.getErrorText().equalsIgnoreCase("")){
					rowCss="style=\"background-color:#FF0000;\"";
				}
				
				dmRecords.append("<tr >" );
				dmRecords.append("<td "+rowCss+">"+fName+"</td>");
				dmRecords.append("<td "+rowCss+">"+lName+"</td>");
				dmRecords.append("<td "+rowCss+">"+email+"</td>");
				dmRecords.append("<td "+rowCss+">"+inventoryInvitationsTemp.getErrorText()+"</td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public TeacherDetail findByTeacherDetailObj(List<TeacherDetail> currentTeacherDetailList,List<TeacherDetail> teacherDetailList,int teacherId)
	{
		TeacherDetail TeacherDetailObj = null;
		boolean teacherFlag=false;
		try 
		{
			for(TeacherDetail teacherDetail : teacherDetailList){
				if(teacherDetail.getTeacherId()==teacherId){
					TeacherDetailObj=teacherDetail;
					teacherFlag=true;
				}
			}
			if(teacherFlag==false){
				for(TeacherDetail teacherDetail: currentTeacherDetailList){
					if(teacherDetail.getTeacherId()==teacherId)
						TeacherDetailObj=teacherDetail;
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(TeacherDetailObj==null)
			return null;
		else
			return TeacherDetailObj;	
	}

	@SuppressWarnings({ "unchecked", "finally" })
	public int saveTeacher()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		int returnVal=0;
        try{
        	String ipAddress = request.getRemoteAddr();
        	JSONObject jsonObject = null;
        	String authKey = null;
        	String orgType = null;
        	String assessmentType = null;
        	String assessmentName = "Smart Practices";
        	String grpName = null;
        	String encryptedGrpName = null;
        	Integer districtId = null;
        	List emails = new ArrayList();
        	
        	Criterion criterion1 = Restrictions.eq("sessionId",session.getId());
        	Criterion criterion2 = Restrictions.eq("errorText","");
        	List <InventoryInvitationsTemp> inventoryInvitationsTempList = inventoryInvitationsTempDAO.findByCriteria(criterion1,criterion2);
        	
        	InventoryInvitationsTemp inventoryInvitationsTempObj = inventoryInvitationsTempList.get(0);
        	
        	orgType = inventoryInvitationsTempObj.getOrgType();
        	assessmentType = inventoryInvitationsTempObj.getInventoryType();
        	grpName = inventoryInvitationsTempObj.getGroupName();
        	districtId = inventoryInvitationsTempObj.getDistrictId();
        	if(assessmentType.equals("4"))
        		assessmentName = "IPI";
        	
        	DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
        	authKey = districtMasterDAO.findByCriteria(Restrictions.eq("districtId", districtId)).get(0).getAuthKey();
        	encryptedGrpName = Utility.encodeInBase64(grpName);
        	
        	for(int i=0; i<inventoryInvitationsTempList.size(); i++) {
	           	emails.add(inventoryInvitationsTempList.get(i).getCandidateEmail());
			}
        	
        	List <TeacherDetail> teacherUploadTempList = teacherDetailDAO.findAllTeacherDetails(emails);
        	List teacherIdList = new ArrayList();
	        for(int i=0; i<teacherUploadTempList.size(); i++) {
	        	teacherIdList.add(teacherUploadTempList.get(i));
			}
	        
	        List<TeacherDetail> newTeacherDetailList = new ArrayList<TeacherDetail>();
	        List<TeacherDetail> existingTeacherDetailList = new ArrayList<TeacherDetail>();
	        
			SessionFactory sessionFactory1 = teacherDetailDAO.getSessionFactory();
			StatelessSession statelesSsession1 = sessionFactory1.openStatelessSession();
			Transaction txOpen1 = statelesSsession1.beginTransaction();
			 
			TeacherDetail teacherDetail;
			
			for(InventoryInvitationsTemp inventoryInvitationsTemp : inventoryInvitationsTempList){
				int teacherId = 0;
				if(emailTeacherExistForSave(newTeacherDetailList,teacherUploadTempList,inventoryInvitationsTemp.getCandidateEmail())!=0){
					teacherId = emailTeacherExistForSave(newTeacherDetailList,teacherUploadTempList,inventoryInvitationsTemp.getCandidateEmail());
				}
				System.out.println("teacherId ::: "+teacherId);
				String rPassword = Utility.randomString(8);
				
				if(inventoryInvitationsTemp.getExistTeacherId()==0 && teacherId==0){
					String password = MD5Encryption.toMD5(rPassword);
					System.out.println("Create New Teacher ::::::::::::::::::::::");
					int authorizationkey = (int) Math.round(Math.random() * 2000000);
					teacherDetail= new TeacherDetail();
					teacherDetail.setEmailAddress(inventoryInvitationsTemp.getCandidateEmail());
					teacherDetail.setFirstName(inventoryInvitationsTemp.getCandidateFirstName());
					teacherDetail.setLastName(inventoryInvitationsTemp.getCandidateLastName());
					teacherDetail.setPassword(password);
					teacherDetail.setUserType("R");
					teacherDetail.setExclusivePeriod(0);
					teacherDetail.setQuestCandidate(0);
					teacherDetail.setFbUser(false);
					teacherDetail.setAuthenticationCode(""+authorizationkey);
					teacherDetail.setVerificationCode(""+authorizationkey);
					teacherDetail.setVerificationStatus(1);				
					teacherDetail.setNoOfLogin(0);
					teacherDetail.setStatus("A");
					teacherDetail.setSendOpportunity(false);
					teacherDetail.setCreatedDateTime(new Date());
					teacherDetail.setIsPortfolioNeeded(true);
					teacherDetail.setForgetCounter(0);
					teacherDetail.setIsResearchTeacher(false);
					teacherDetail.setInternalTransferCandidate(false);
					statelesSsession1.insert(teacherDetail);
					newTeacherDetailList.add(teacherDetail);
					teacherDetail.setPassword(rPassword);
				}
				else{
					teacherDetail= findByTeacherDetailObj(newTeacherDetailList,teacherUploadTempList,teacherId);
					existingTeacherDetailList.add(teacherDetail);
				}
        	}
	       	txOpen1.commit();
	       	statelesSsession1.close();
	       	
	       	SessionFactory sessionFactory2 = inventoryInvitationsDAO.getSessionFactory();
			StatelessSession statelesSsession2 = sessionFactory2.openStatelessSession();
			Transaction txOpen2 = statelesSsession2.beginTransaction();
			InventoryInvitations inventoryInvitations = null;
			
	       	try{
	       		//welcome mail to new candidates
	       		for (TeacherDetail teacherDetail2 : newTeacherDetailList) {
			       	DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("admin@netsutra.com");
					dsmt.setMailto(teacherDetail2.getEmailAddress());
					dsmt.setMailsubject("Welcome to TeacherMatch, please confirm your account");
					dsmt.setMailcontent(MailText.getRegistrationMail(request, teacherDetail2));
					try {
						dsmt.start();	
					} catch (Exception e) {}
				}
	       		//invite mail to new candidates
	       		for (TeacherDetail teacherDetail2 : newTeacherDetailList) {
	       			jsonObject = new JSONObject();
	       			jsonObject.put("authKey", authKey);
	       			jsonObject.put("orgType", orgType);
	       			jsonObject.put("assessmentType", assessmentType);
	       			jsonObject.put("grpName", encryptedGrpName);
	       			jsonObject.put("candidateFirstName", teacherDetail2.getFirstName());
	       			jsonObject.put("candidateLastName", teacherDetail2.getLastName());
	       			jsonObject.put("candidateEmail", Utility.encodeInBase64(teacherDetail2.getEmailAddress()));
	       			String encryptText = AESEncryption.encrypt(jsonObject.toString());
	       			
			       	DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("admin@netsutra.com");
					dsmt.setMailto(teacherDetail2.getEmailAddress());
					dsmt.setMailsubject("You are invited to complete the "+assessmentName+" assessment");
					dsmt.setMailcontent(MailText.getInventoryInvitationMail(request, teacherDetail2, encryptText));
					try {
						dsmt.start();	
					} catch (Exception e) {}
					
					String invitationLink = Utility.getBaseURL(request)+"service/inventory.do?id="+encryptText;
					String invitationParams = jsonObject.toString();
					inventoryInvitations = new InventoryInvitations();
					inventoryInvitations.setOrgType(orgType);
					inventoryInvitations.setInventoryType(assessmentType);
					inventoryInvitations.setGroupName(grpName);
					inventoryInvitations.setDistrictMaster(districtMaster);
					inventoryInvitations.setCandidateFirstName(teacherDetail2.getFirstName());
					inventoryInvitations.setCandidateLastName(teacherDetail2.getLastName());
					inventoryInvitations.setCandidateEmail(teacherDetail2.getEmailAddress());
					inventoryInvitations.setInvitationLink(invitationLink);
					inventoryInvitations.setInvitationParams(invitationParams);
					inventoryInvitations.setUserMaster(userMaster);
					inventoryInvitations.setIpaddress(ipAddress);
					statelesSsession2.insert(inventoryInvitations);
				}
	       		//invite mail to existing candidates
	       		for (TeacherDetail teacherDetail2 : existingTeacherDetailList) {
	       			jsonObject = new JSONObject();
	       			jsonObject.put("authKey", authKey);
	       			jsonObject.put("orgType", orgType);
	       			jsonObject.put("assessmentType", assessmentType);
	       			jsonObject.put("grpName", encryptedGrpName);
	       			jsonObject.put("candidateFirstName", teacherDetail2.getFirstName());
	       			jsonObject.put("candidateLastName", teacherDetail2.getLastName());
	       			jsonObject.put("candidateEmail", Utility.encodeInBase64(teacherDetail2.getEmailAddress()));
	       			String encryptText = AESEncryption.encrypt(jsonObject.toString());
	       			
			       	DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("admin@netsutra.com");
					dsmt.setMailto(teacherDetail2.getEmailAddress());
					dsmt.setMailsubject("You are invited to complete the "+assessmentName+" assessment");
					dsmt.setMailcontent(MailText.getInventoryInvitationMail(request, teacherDetail2, encryptText));
					try {
						dsmt.start();	
					} catch (Exception e) {}
					
					String invitationLink = Utility.getBaseURL(request)+"service/inventory.do?id="+encryptText;
					String invitationParams = jsonObject.toString();
					inventoryInvitations = new InventoryInvitations();
					inventoryInvitations.setOrgType(orgType);
					inventoryInvitations.setInventoryType(assessmentType);
					inventoryInvitations.setGroupName(grpName);
					inventoryInvitations.setDistrictMaster(districtMaster);
					inventoryInvitations.setCandidateFirstName(teacherDetail2.getFirstName());
					inventoryInvitations.setCandidateLastName(teacherDetail2.getLastName());
					inventoryInvitations.setCandidateEmail(teacherDetail2.getEmailAddress());
					inventoryInvitations.setInvitationLink(invitationLink);
					inventoryInvitations.setInvitationParams(invitationParams);
					inventoryInvitations.setUserMaster(userMaster);
					inventoryInvitations.setIpaddress(ipAddress);
					statelesSsession2.insert(inventoryInvitations);
				}
	       		txOpen2.commit();
		       	statelesSsession2.close();
	       	}catch(Exception e){
	       		e.printStackTrace();
	       	}
	       	
	       	try{
	        	deleteXlsAndXlsxFile(request,session.getId());
	       		inventoryInvitationsTempDAO.deleteAllTeacherTemp(session.getId());
	       	}catch(Exception e){
	       		e.printStackTrace();
	       	}
		}catch (Exception e){
			System.out.println("Error >"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			return returnVal;
		}
	}
	
	public boolean emailExist(Map<String,String> emailMap,List<UserMaster> userMasterlst,String emailAddress,String sessionId){
		boolean userFlag=false,teacherFlag=false;
		for(UserMaster userMaster :userMasterlst){
			if(userMaster.getEmailAddress().equalsIgnoreCase(emailAddress)){
				userFlag=true;
			}
		}
		for(Map.Entry<String, String> entry : emailMap.entrySet()){
			String mEmail=entry.getValue();
			if(mEmail!=null)
				if(mEmail.equalsIgnoreCase(emailAddress))
					teacherFlag=true;
		}
		if(teacherFlag || userFlag)
		{
			return true;
		}else{
			return false;
		}
	}
	
	public int emailTeacherExistForSave(List<TeacherDetail> currentTeacherDetaillst,List<TeacherDetail> lstTeacherDetails,String emailAddress){
		int teacherId=0;
		for(TeacherDetail teacherDetail: lstTeacherDetails){
			if(teacherDetail.getEmailAddress().equalsIgnoreCase(emailAddress)){
				teacherId=teacherDetail.getTeacherId();
				return teacherId;
			}
			
		}
		if(teacherId==0)
		for(TeacherDetail teacherDetail: currentTeacherDetaillst){
			if(teacherDetail.getEmailAddress().equalsIgnoreCase(emailAddress)){
				teacherId=teacherDetail.getTeacherId();
				return teacherId;
			}
			
		}
		return teacherId;
	}
	
	public int emailTeacherExist(List<TeacherDetail> lstTeacherDetails,String emailAddress){
		int teacherId=0;
		for(TeacherDetail teacherDetail: lstTeacherDetails){
			if(teacherDetail.getEmailAddress().equalsIgnoreCase(emailAddress)){
				teacherId=teacherDetail.getTeacherId();
				return teacherId;
			}
			
		}
		return teacherId;
	}
	
	@SuppressWarnings("deprecation")
	public void deleteXlsAndXlsxFile(HttpServletRequest request,String sessionId){
		List<InventoryInvitationsTemp> inventoryInvitationsTempList = inventoryInvitationsTempDAO.findByAllTempTeacher();
		String root = request.getRealPath("/")+"/uploads/";
		for(InventoryInvitationsTemp inventoryInvitationsTemp : inventoryInvitationsTempList){
			String filePath="";
			File file=null;
			try{
				filePath=root+inventoryInvitationsTemp.getSessionId()+".xlsx";
				file = new File(filePath);
				file.delete();
			}catch(Exception e){}
			try{
				filePath=root+inventoryInvitationsTemp.getSessionId()+".xls";
				file = new File(filePath);
				file.delete();
			}catch(Exception e){}
		}
		
		String filePath="";
		File file=null;
		try{
			filePath=root+sessionId+".xlsx";
			file = new File(filePath);
			file.delete();
		}catch(Exception e){}
		try{
			filePath=root+sessionId+".xls";
			file = new File(filePath);
			file.delete();
		}catch(Exception e){}
	}
	
	public int deleteTempTeacher(String sessionId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			inventoryInvitationsTempDAO.deleteTeacherTemp(sessionId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public String saveTeacherTemp(String fileName,String sessionId,String orgType,String assessmentType,String groupName,Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		session.setAttribute("Did",districtId);
		String returnVal="";
        try{
        	 String root = request.getRealPath("/")+"/uploads/";
        	 String filePath=root+fileName;
        	 System.out.println("filePath :"+filePath);
        	 if(fileName.contains(".xlsx")){
        		 vectorDataExcelXLSX = readDataExcelXLSX(filePath);
        	 }else{
        		 vectorDataExcelXLSX = readDataExcelXLS(filePath);
        	 }
        	 System.out.println(":::::::: get value of XLSX and XLS :::::::::");
			 int user_first_name_count=11;
			 int user_last_name_count=11;
			 int user_email_count=11;
			 DistrictMaster districtMaster = new DistrictMaster();
			 districtMaster.setDistrictId(districtId);
			 
			 List<UserMaster> userMasterlst = userMasterDAO.findByAllUserMaster();
			 List emails = new ArrayList();
			 int user_email_count1=11;
			 Map<String,String> emailExistCheck = new HashMap<String, String>();
			 SessionFactory sessionFactory = inventoryInvitationsTempDAO.getSessionFactory();
			 StatelessSession sessionHiber = sessionFactory.openStatelessSession();
			
        	 Transaction txOpen =sessionHiber.beginTransaction();
			 for(int em=0; em<vectorDataExcelXLSX.size(); em++) {
	            Vector vectorCellEachRowDataGetEmail = (Vector) vectorDataExcelXLSX.get(em);
	            String email="";
	            for(int e=0; e<vectorCellEachRowDataGetEmail.size(); e++) {
	            	if(vectorCellEachRowDataGetEmail.get(e).toString().equalsIgnoreCase("user_email")){
	            		user_email_count1=e;
	            	}
	            	if(user_email_count1==e)
	            		email=vectorCellEachRowDataGetEmail.get(e).toString().trim();
	            }
	            if(em!=0){
	            	emails.add(email);
	            }
			 }
			 
			 List <TeacherDetail> teacherUploadTempList=teacherDetailDAO.findAllTeacherDetails(emails);
			 System.out.println("::::::::Start Inserting:::::::::");
			 InventoryInvitationsTemp inventoryInvitationsTemp = new InventoryInvitationsTemp();
			 for(int i=0; i<vectorDataExcelXLSX.size(); i++){
	             Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
				 String user_first_name="";
				 String user_last_name="";
				 String user_email="";
				 
	            for(int j=0; j<vectorCellEachRowData.size(); j++){
	            	try{
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("user_first_name")){
		            		user_first_name_count=j;
		            	}
		            	if(user_first_name_count==j)
		            		 user_first_name=vectorCellEachRowData.get(j).toString().trim();
		            		
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("user_last_name")){
		            		user_last_name_count=j;
		            	}
		            	if(user_last_name_count==j)
		            		user_last_name=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("user_email")){
		            		user_email_count=j;
		            	}
		            	if(user_email_count==j)
		            		user_email=vectorCellEachRowData.get(j).toString().trim();
	            	}catch(Exception e){}
	            	
	            }
	            if(i!=0){
	            	if(returnVal.equals("1") && (!user_first_name.equalsIgnoreCase("") || !user_last_name.equalsIgnoreCase("") || !user_email.equalsIgnoreCase(""))){
	            		inventoryInvitationsTemp = new InventoryInvitationsTemp();
	            	 	
		            	String errorText=""; boolean errorFlag=false;
	            	 	inventoryInvitationsTemp.setExistTeacherId(0);
	            	 	
	            	 	if(user_first_name.equalsIgnoreCase("")||user_first_name.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="User First Name is not available.";
	            	 		errorFlag=true;
	            	 	}
	            	 	if(user_last_name.equalsIgnoreCase("")||user_last_name.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br> ";	
	            	 		}
	            	 		errorText+="User Last Name is not available.";
	            	 		errorFlag=true;
	            	 	}
	            	 	if(user_email.equalsIgnoreCase("")||user_email.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="User Email is not available.";
	            	 	}else if(!Utility.validEmailForTeacher(user_email)){
            	 			if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="User Email is not valid.";
	            	 	}else if(emailExist(emailExistCheck,userMasterlst,user_email,session.getId())){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="User Email has already registered.";
	            	 	}else if(emailTeacherExist(teacherUploadTempList,user_email)!=0){
	            	 		inventoryInvitationsTemp.setExistTeacherId(emailTeacherExist(teacherUploadTempList,user_email));
	            	 	}
	            	 	emailExistCheck.put(i+"",user_email);
	            	 	inventoryInvitationsTemp.setCandidateFirstName(user_first_name);
	            	 	inventoryInvitationsTemp.setCandidateLastName(user_last_name);
	            	 	inventoryInvitationsTemp.setCandidateEmail(user_email);
	            	 	inventoryInvitationsTemp.setSessionId(sessionId);
	            	 	if(errorText!=null)
	            	 		inventoryInvitationsTemp.setErrorText(errorText);
	            	 	inventoryInvitationsTemp.setOrgType(orgType);
	            	 	inventoryInvitationsTemp.setInventoryType(assessmentType);
	            	 	inventoryInvitationsTemp.setGroupName(groupName);
	            	 	inventoryInvitationsTemp.setDistrictId(districtId);
	            	 	sessionHiber.insert(inventoryInvitationsTemp);
		             }
	            }else{
	            	 if(user_first_name_count!=11 && user_last_name_count!=11 && user_email_count!=11){
	            		 returnVal="1"; 
	            		 inventoryInvitationsTempDAO.deleteTeacherTemp(sessionId);
	            	 }else{
	            		 returnVal="";
	            		 boolean fieldVal=false;
		            	 if(user_first_name_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" user_first_name";
		            	 }
		            	 if(user_last_name_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" user_last_name";
		            	 }
		            	 if(user_email_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" user_email";
		            	 }
	            	 }
	            }
		     }
			 txOpen.commit();
     	 	 sessionHiber.close();
     	 	System.out.println("Data has been inserted as temp.");
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return returnVal;
	}
	
	@SuppressWarnings("unchecked")
	public static Vector readDataExcelXLS(String fileName) throws IOException {
		Vector vectorData = new Vector();
		try{
			InputStream ExcelFileToRead = new FileInputStream(fileName);
			HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
	 
			HSSFSheet sheet=wb.getSheetAt(0);
			HSSFRow row; 
			HSSFCell cell;
	 
			Iterator rows = sheet.rowIterator();
	        int user_first_name_count=11;
	        int user_last_name_count=11;
	        int user_email_count=11;
	        String indexCheck="";
			 
			while (rows.hasNext()){
				row=(HSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false;
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
				while (cells.hasNext())
				{
					cell=(HSSFCell) cells.next();
					cIndex=cell.getColumnIndex();
					
					if(cell.toString().equalsIgnoreCase("user_first_name")){
						user_first_name_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(user_first_name_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().equalsIgnoreCase("user_last_name")){
						user_last_name_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(user_last_name_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().equalsIgnoreCase("user_email")){
						user_email_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(user_email_count==cIndex){
						cellFlag=true;
					}
					if(cellFlag){
            			try{
            				mapCell.put(Integer.valueOf(cIndex),cell.getStringCellValue()+"");
            			}catch(Exception e){
            				mapCell.put(Integer.valueOf(cIndex),new Double(cell.getNumericCellValue()).longValue()+"");
            			}
					}
		        	cellFlag=false;
				}
			    vectorCellEachRowData=cellValuePopulate(mapCell);
				vectorData.addElement(vectorCellEachRowData);
			}
		}catch(Exception e){}
		return vectorData;
    }
	
	@SuppressWarnings("unchecked")
	public static Vector readDataExcelXLSX(String fileName) {
        Vector vectorData = new Vector();
        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            XSSFWorkbook xssfWorkBook = new XSSFWorkbook(fileInputStream);
            // Read data at sheet 0
            XSSFSheet xssfSheet = xssfWorkBook.getSheetAt(0);
            Iterator rowIteration = xssfSheet.rowIterator();
            int user_first_name_count=11;
            int user_last_name_count=11;
            int user_email_count=11;
            // Looping every row at sheet 0
            String indexCheck="";
            while (rowIteration.hasNext()) {
                XSSFRow xssfRow = (XSSFRow) rowIteration.next();
                Iterator cellIteration = xssfRow.cellIterator();
                Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false;
                // Looping every cell in each row at sheet 0
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
                while (cellIteration.hasNext()){
                    XSSFCell xssfCell = (XSSFCell) cellIteration.next();
                    cIndex=xssfCell.getColumnIndex();
                    
	            	if(xssfCell.toString().equalsIgnoreCase("user_first_name")){
	            		user_first_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(user_first_name_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("user_last_name")){
	            		user_last_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(user_last_name_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("user_email")){
	            		user_email_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(user_email_count==cIndex){
	            		cellFlag=true;
	            	}
	            	if(cellFlag){
	            		try{
	            			mapCell.put(Integer.valueOf(cIndex),xssfCell.getStringCellValue()+"");
	            		}catch(Exception e){
	            			mapCell.put(Integer.valueOf(cIndex),xssfCell.getRawValue()+"");
	            		}
	            	}
	            	cellFlag=false;
                }
                vectorCellEachRowData=cellValuePopulate(mapCell);
                vectorData.addElement(vectorCellEachRowData);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
         
        return vectorData;
    }
	
	@SuppressWarnings("unchecked")
	public static Vector cellValuePopulate(Map<Integer,String> mapCell){
		 Vector vectorCellEachRowData = new Vector();
		 Map<Integer,String> mapCellTemp = new TreeMap<Integer, String>();
		 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false;
		 for(Map.Entry<Integer, String> entry : mapCell.entrySet()){
			int key=entry.getKey();
			String cellValue=null;
			if(entry.getValue()!=null)
				cellValue=entry.getValue().trim();
			
			if(key==0){
				mapCellTemp.put(key, cellValue);
				flag0=true;
			}
			if(key==1){
				flag1=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==2){
				flag2=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==3){
				flag3=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==4){
				flag4=true;
				mapCellTemp.put(key, cellValue);
			}
		 }
		 if(flag0==false){
			 mapCellTemp.put(0, "");
		 }
		 if(flag1==false){
			 mapCellTemp.put(1, "");
		 }
		 if(flag2==false){
			 mapCellTemp.put(2, "");
		 }
		 if(flag3==false){
			 mapCellTemp.put(3, "");
		 }
		 if(flag4==false){
			 mapCellTemp.put(4, "");
		 }
		 for(Map.Entry<Integer, String> entry : mapCellTemp.entrySet()){
			 vectorCellEachRowData.addElement(entry.getValue());
		 }
		 return vectorCellEachRowData;
	}
}
