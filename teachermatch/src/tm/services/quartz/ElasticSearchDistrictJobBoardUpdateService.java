package tm.services.quartz;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import tm.controller.es.ElasticSearchController;
import tm.services.es.ElasticSearchService;

public class ElasticSearchDistrictJobBoardUpdateService implements Job
{
	public void execute(JobExecutionContext arg0) throws JobExecutionException
	{
		
		System.out.println("Calling DistrictJobOrderUpdateService execute "+new Date());
		System.out.println("******************************aaaaaaaaaaaaaaaaaaaaa******************************************");
		
		new ElasticSearchController().updateESForDistrictJobBoard();		
		System.out.println("******************************aaaaaaaaaaaaaaaaaaaaaaa******************************************");
		System.out.println("End ... Calling DistrictJobOrderUpdateService execute "+new Date());
		
	}
}
