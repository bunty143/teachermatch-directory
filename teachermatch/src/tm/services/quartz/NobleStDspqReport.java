package tm.services.quartz;

import java.util.Date;

import javax.servlet.ServletContext;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.dao.JobOrderDAO;
import tm.dao.master.DistrictSpecificPortfolioAnswersDAO;
import tm.services.NobleStDspqReportAjax;
import tm.services.NobleStJobsReportAjax;



public class NobleStDspqReport implements Job
{
	            ApplicationContext springContext=null;
	            ServletContext servletContext = null;
	            DistrictSpecificPortfolioAnswersDAO districtSpecificPortfolioAnswersDAO = null;
	            
                public void execute(JobExecutionContext arg0) throws JobExecutionException
                {
                	springContext = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
                	districtSpecificPortfolioAnswersDAO = (DistrictSpecificPortfolioAnswersDAO) springContext.getBean("districtSpecificPortfolioAnswersDAO");   
                	servletContext = ContextLoaderListener.getCurrentWebApplicationContext().getServletContext();
                	System.out.println("districtSpecificPortfolioAnswersDAO:********************** "+districtSpecificPortfolioAnswersDAO);
                	
                                System.out.println("districtSpecificPortfolioAnswersDAO execute "+new Date());
                                System.out.println("******************************aaaaaaaaaaaaaaaaaaaaa******************************************");
                             
                                 new NobleStDspqReportAjax().displayNobleDspqCSVRun(districtSpecificPortfolioAnswersDAO,servletContext);
                                              
                                System.out.println("******************************aaaaaaaaaaaaaaaaaaaaaaa******************************************");
                                System.out.println("End ... districtSpecificPortfolioAnswersDAO execute "+new Date());
                                
                }
}

