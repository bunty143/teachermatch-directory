package tm.services.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import tm.services.report.CGReportService;
import tm.utility.Utility;

public class CGDataUpdateService implements Job{
	@Autowired
	CGReportService cGReportService = null;
	public void setcGReportService(CGReportService cGReportService) {
		this.cGReportService = cGReportService;
	}
	
	public void execute(JobExecutionContext arg0) throws JobExecutionException{
		//Utility.sendRequestToURL(Utility.getValueOfPropByKey("basePath")+"/updatecgbyuser.do");
		Utility.sendRequestToURL(Utility.getValueOfPropByKey("contextBasePath")+"/updatecgbyuser.do");
		System.out.println("Hello cron job");
	}
}
