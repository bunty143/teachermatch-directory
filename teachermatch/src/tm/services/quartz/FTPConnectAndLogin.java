package tm.services.quartz;

import java.io.FileOutputStream; 
import java.io.IOException; 
import java.io.PrintWriter; 
import org.apache.commons.net.PrintCommandListener; 
import org.apache.commons.net.ftp.FTP; 
import org.apache.commons.net.ftp.FTPReply; 
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FTPConnectAndLogin   
{ 
		public static String  dropFileOnFTPServer(String localFile ) throws Exception 
			{ 
				System.setProperty("javax.net.debug", "ssl");

				String server = "ftp1.securevdr.com"; 
				String username = "teachermatchconfidential/plin@teachermatch.com"; 
				String password = "Te@cherMatch2006!"; 
				String remoteFile = "/Partner Data - Updated Permissions/IL-Noble/Platform Data Files/noblestapplication.csv"; 
				//String localFile = "D:/user/"+localFile1+""; 
				String protocol = "TLS"; 
				int port = 990; 
				//int timeoutInMillis = 3000; 
				boolean isImpicit = true;

				FTPSClient client = new FTPSClient(protocol, isImpicit);

				//client.setDataTimeout(timeoutInMillis); 
				client.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

				try 
					{ 
						int reply;

						client.connect(server, port);

						boolean sucess=client.login(username, password); 
						client.enterLocalPassiveMode();
						System.out.println("login success======="+sucess);
						client.setFileType(FTP.BINARY_FILE_TYPE);

						client.execPBSZ(0); 
						client.execPROT("P");

						System.out.println("Connected to " + server + ".");
						 InputStream inputStream = new FileInputStream(localFile);
						
						 boolean done = client.storeFile(remoteFile, inputStream);
				            System.out.println("completeeeeeeeeeeee============"+done);
				            inputStream.close();
				            if (done) {
				                System.out.println("The first file is uploaded successfully.");
				            }

						reply = client.getReplyCode();

							if (!FTPReply.isPositiveCompletion(reply)) 
								{ 
									client.disconnect(); 
									System.err.println("FTP server refused connection."); 
									System.exit(1); 
								}

							client.listFiles();

							boolean retrieved = client.retrieveFile(remoteFile, new FileOutputStream(localFile));

					} 
				catch (Exception e) 
				{

					if (client.isConnected()) 
					{ 
						try 
						{ 
							client.disconnect(); 
						} 
						catch (IOException ex) 
						{ 
							ex.printStackTrace(); 
						} 
					} 
						System.err.println("Could not connect to server."); 
						e.printStackTrace(); 
						return ""; 
				} 
				finally 
				{ 
					System.out.println("# client disconnected"); 
					client.disconnect(); 
				} 
				return ""; 
			} 
		
		
		/******method call for noble st jobs*********/
		public static String  dropFileOnFTPServerForNobleJobs(String localFile ) throws Exception 
		{ 
			System.setProperty("javax.net.debug", "ssl");

			String server = "ftp1.securevdr.com"; 
			String username = "teachermatchconfidential/plin@teachermatch.com"; 
			String password = "Te@cherMatch2006!"; 
			String remoteFile = "/Partner Data - Updated Permissions/IL-Noble/Platform Data Files/noblestjobs.csv"; 
			//String localFile = "D:/user/"+localFile1+""; 
			String protocol = "TLS"; 
			int port = 990; 
			//int timeoutInMillis = 3000; 
			boolean isImpicit = true;

			FTPSClient client = new FTPSClient(protocol, isImpicit);

			//client.setDataTimeout(timeoutInMillis); 
			client.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

			try 
				{ 
					int reply;

					client.connect(server, port);

					boolean sucess=client.login(username, password); 
					client.enterLocalPassiveMode();
					System.out.println("login success======="+sucess);
					client.setFileType(FTP.BINARY_FILE_TYPE);

					client.execPBSZ(0); 
					client.execPROT("P");

					System.out.println("Connected to " + server + ".");
					 InputStream inputStream = new FileInputStream(localFile);
					
					 boolean done = client.storeFile(remoteFile, inputStream);
			            System.out.println("completeeeeeeeeeeee============"+done);
			            inputStream.close();
			            if (done) {
			                System.out.println("The first file is uploaded successfully.");
			            }

					reply = client.getReplyCode();

						if (!FTPReply.isPositiveCompletion(reply)) 
							{ 
								client.disconnect(); 
								System.err.println("FTP server refused connection."); 
								System.exit(1); 
							}

						client.listFiles();

						boolean retrieved = client.retrieveFile(remoteFile, new FileOutputStream(localFile));

				} 
			catch (Exception e) 
			{

				if (client.isConnected()) 
				{ 
					try 
					{ 
						client.disconnect(); 
					} 
					catch (IOException ex) 
					{ 
						ex.printStackTrace(); 
					} 
				} 
					System.err.println("Could not connect to server."); 
					e.printStackTrace(); 
					return ""; 
			} 
			finally 
			{ 
				System.out.println("# client disconnected"); 
				client.disconnect(); 
			} 
			return ""; 
		} 
		/*******************************************/
		
		
		/******method call for noble st Candidate*********/
		public static String  dropFileOnFTPServerForNobleCandidate(String localFile ) throws Exception 
		{ 
			System.setProperty("javax.net.debug", "ssl");

			String server = "ftp1.securevdr.com"; 
			String username = "teachermatchconfidential/plin@teachermatch.com"; 
			String password = "Te@cherMatch2006!"; 
			String remoteFile = "/Partner Data - Updated Permissions/IL-Noble/Platform Data Files/noblestcandidate.csv"; 
			//String localFile = "D:/user/"+localFile1+""; 
			String protocol = "TLS"; 
			int port = 990; 
			//int timeoutInMillis = 3000; 
			boolean isImpicit = true;

			FTPSClient client = new FTPSClient(protocol, isImpicit);

			//client.setDataTimeout(timeoutInMillis); 
			client.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

			try 
				{ 
					int reply;

					client.connect(server, port);

					boolean sucess=client.login(username, password); 
					client.enterLocalPassiveMode();
					System.out.println("login success======="+sucess);
					client.setFileType(FTP.BINARY_FILE_TYPE);

					client.execPBSZ(0); 
					client.execPROT("P");

					System.out.println("Connected to " + server + ".");
					 InputStream inputStream = new FileInputStream(localFile);
					
					 boolean done = client.storeFile(remoteFile, inputStream);
			            System.out.println("completeeeeeeeeeeee============"+done);
			            inputStream.close();
			            if (done) {
			                System.out.println("The first file is uploaded successfully.");
			            }

					reply = client.getReplyCode();

						if (!FTPReply.isPositiveCompletion(reply)) 
							{ 
								client.disconnect();  
								System.err.println("FTP server refused connection."); 
								System.exit(1); 
							}

						client.listFiles();

						boolean retrieved = client.retrieveFile(remoteFile, new FileOutputStream(localFile));

				} 
			catch (Exception e) 
			{

				if (client.isConnected()) 
				{ 
					try 
					{ 
						client.disconnect(); 
					} 
					catch (IOException ex) 
					{ 
						ex.printStackTrace(); 
					} 
				} 
					System.err.println("Could not connect to server."); 
					e.printStackTrace(); 
					return ""; 
			} 
			finally 
			{ 
				System.out.println("# client disconnected"); 
				client.disconnect(); 
			} 
			return ""; 
		} 
		/*******************************************/
		/*********noble st DSPQ*******************/
		public static String  dropFileOnFTPServerForNobleDspq(String localFile ) throws Exception 
		{ 
			System.setProperty("javax.net.debug", "ssl");

			String server = "ftp1.securevdr.com"; 
			String username = "teachermatchconfidential/plin@teachermatch.com"; 
			String password = "Te@cherMatch2006!"; 
			String remoteFile = "/Partner Data - Updated Permissions/IL-Noble/Platform Data Files/noblestDSPQ.csv"; 
			//String localFile = "D:/user/"+localFile1+""; 
			String protocol = "TLS"; 
			int port = 990; 
			//int timeoutInMillis = 3000; 
			boolean isImpicit = true;

			FTPSClient client = new FTPSClient(protocol, isImpicit);

			//client.setDataTimeout(timeoutInMillis); 
			client.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

			try 
				{ 
					int reply;

					client.connect(server, port);

					boolean sucess=client.login(username, password); 
					client.enterLocalPassiveMode();
					System.out.println("login success======="+sucess);
					client.setFileType(FTP.BINARY_FILE_TYPE);

					client.execPBSZ(0); 
					client.execPROT("P");

					System.out.println("Connected to " + server + ".");
					 InputStream inputStream = new FileInputStream(localFile);
					
					 boolean done = client.storeFile(remoteFile, inputStream);
			            System.out.println("completeeeeeeeeeeee============"+done);
			            inputStream.close();
			            if (done) {
			                System.out.println("The first file is uploaded successfully.");
			            }

					reply = client.getReplyCode();

						if (!FTPReply.isPositiveCompletion(reply)) 
							{ 
								client.disconnect(); 
								System.err.println("FTP server refused connection."); 
								System.exit(1); 
							}

						client.listFiles();

						boolean retrieved = client.retrieveFile(remoteFile, new FileOutputStream(localFile));

				} 
			catch (Exception e) 
			{

				if (client.isConnected()) 
				{ 
					try 
					{ 
						client.disconnect(); 
					} 
					catch (IOException ex) 
					{ 
						ex.printStackTrace(); 
					} 
				} 
					System.err.println("Could not connect to server."); 
					e.printStackTrace(); 
					return ""; 
			} 
			finally 
			{ 
				System.out.println("# client disconnected"); 
				client.disconnect(); 
			} 
			return ""; 
		} 
		/***************************/
		public static String  dropFileOnFTPServerForNobleSchoolPostion(String localFile ) throws Exception 
		{ 
			System.setProperty("javax.net.debug", "ssl");

			String server = "ftp1.securevdr.com"; 
			String username = "teachermatchconfidential/plin@teachermatch.com"; 
			String password = "Te@cherMatch2006!"; 
			String remoteFile = "/Partner Data - Updated Permissions/IL-Noble/Platform Data Files/nobleschoolpostion.csv"; 
			//String localFile = "D:/user/"+localFile1+""; 
			String protocol = "TLS"; 
			int port = 990; 
			//int timeoutInMillis = 3000; 
			boolean isImpicit = true;

			FTPSClient client = new FTPSClient(protocol, isImpicit);

			//client.setDataTimeout(timeoutInMillis); 
			client.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

			try 
				{ 
					int reply;

					client.connect(server, port);

					boolean sucess=client.login(username, password); 
					client.enterLocalPassiveMode();
					System.out.println("login success======="+sucess);
					client.setFileType(FTP.BINARY_FILE_TYPE);

					client.execPBSZ(0); 
					client.execPROT("P");

					System.out.println("Connected to " + server + ".");
					 InputStream inputStream = new FileInputStream(localFile);
					
					 boolean done = client.storeFile(remoteFile, inputStream);
			            System.out.println("completeeeeeeeeeeee============"+done);
			            inputStream.close();
			            if (done) {
			                System.out.println("The first file is uploaded successfully.");
			            }

					reply = client.getReplyCode();

						if (!FTPReply.isPositiveCompletion(reply)) 
							{ 
								client.disconnect(); 
								System.err.println("FTP server refused connection."); 
								System.exit(1); 
							}

						client.listFiles();

						boolean retrieved = client.retrieveFile(remoteFile, new FileOutputStream(localFile));

				} 
			catch (Exception e) 
			{

				if (client.isConnected()) 
				{ 
					try 
					{ 
						client.disconnect(); 
					} 
					catch (IOException ex) 
					{ 
						ex.printStackTrace(); 
					} 
				} 
					System.err.println("Could not connect to server."); 
					e.printStackTrace(); 
					return ""; 
			} 
			finally 
			{ 
				System.out.println("# client disconnected"); 
				client.disconnect(); 
			} 
			return ""; 
		} 
		
		 public static boolean startFTP(String filePath,String fileToFTP){  
			  StandardFileSystemManager manager = new StandardFileSystemManager();
			 
			  try { 
			   String serverAddress = "sftp.teachermatch.org";  
			   String userId = "mqfteteachertmatch";  
			   String password = "Teacher%40mqfte%2320A5";   
			   String remoteDirectory ="outbox/" ; 
			   String localDirectory = filePath;
			   
			 
			   //check if the file exists
			   String filepath = localDirectory +"/"+  fileToFTP;
			   System.out.println("nameeeeee:::::::"+filepath);
			   File file = new File(filepath);
			   if (!file.exists())
			    throw new RuntimeException("Error. Local file not found");
			 
			   //Initializes the file manager
			   manager.init();
			    
			   //Setup our SFTP configuration
			   FileSystemOptions opts = new FileSystemOptions();
			   SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no");
			   SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, true);
			   SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 10000);
			    
			   //Create the SFTP URI using the host name, userid, password,  remote path and file name
			   String sftpUri = "sftp://" + userId + ":" + password +  "@" + serverAddress + "/" + 
			     remoteDirectory + fileToFTP;
			    
			   // Create local file object
			   FileObject localFile = manager.resolveFile(file.getAbsolutePath());
			 
			   // Create remote file object
			   FileObject remoteFile = manager.resolveFile(sftpUri, opts);
			 
			   // Copy local file to sftp server
			   remoteFile.copyFrom(localFile, Selectors.SELECT_SELF);
			   System.out.println("File upload successful");
			 
			  }
			  catch (Exception ex) {
			   ex.printStackTrace();
			   return false;
			  }
			  finally {
			   manager.close();
			  }
			 
			  return true;
			 }		
		
	}