package tm.services.quartz;


import java.util.Date;

import javax.servlet.ServletContext;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.services.AdminDashboardAjax;
import tm.services.CommonDashboardAjax;
import tm.utility.Utility;


public class JobReminderService implements Job{

	
	public void execute(JobExecutionContext arg0) throws JobExecutionException{
		
		System.out.println("============================================");
		System.out.println("JobReminderService Start: "+ new Date());
		
		ApplicationContext springContext = 
		    WebApplicationContextUtils.getWebApplicationContext(
		        ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		
		CommonDashboardAjax commonDashboardAjax = null;
		
		try {
		    
			ServletContext ctx = ContextLoaderListener.getCurrentWebApplicationContext().getServletContext();
			System.out.println("ctx: "+ctx);
			
			commonDashboardAjax = (CommonDashboardAjax) springContext.getBean("commonDashboardAjax");
			System.out.println("commonDashboardAjax: "+commonDashboardAjax);
			
     		commonDashboardAjax.startReminder();
			
			commonDashboardAjax.startReminderForHQ();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		//////////////////////////////////////
		System.out.println("============================================");
		System.out.println("JobReminderService  End: "+ new Date());
		System.out.println("============================================");
	}
	
}

