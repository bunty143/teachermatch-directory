package tm.services.quartz;

import java.util.Date;
import java.util.List;

import org.directwebremoting.export.Data;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.services.ManageJobOrdersAjax;
import tm.services.social.SocialService;
import tm.utility.Utility;


public class PostJobsOnSocialSitesService implements Job{

	
	public void execute(JobExecutionContext arg0) throws JobExecutionException{
		
		System.out.println("============================================");
		System.out.println("PostJobsOnSocialSitesService Start: "+ new Date());
		
		String basepath = Utility.getValueOfPropByKey("basePath");
		
		ApplicationContext springContext = 
		    WebApplicationContextUtils.getWebApplicationContext(
		        ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		SocialService socialService = null;
		ManageJobOrdersAjax manageJobOrdersAjax = null;
		JobOrderDAO jobOrderDAO = null;
		SchoolInJobOrderDAO schoolInJobOrderDAO = null;
		
		try {
			
			jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO");
			socialService = (SocialService) springContext.getBean("socialService");
			manageJobOrdersAjax = (ManageJobOrdersAjax) springContext.getBean("manageJobOrdersAjax");
			schoolInJobOrderDAO = (SchoolInJobOrderDAO) springContext.getBean("schoolInJobOrderDAO");
			
	/*		System.out.println("jobOrderDAO: "+jobOrderDAO);
			System.out.println("socialService: "+socialService);
			System.out.println("manageJobOrdersAjax: "+manageJobOrdersAjax);
			System.out.println("schoolInJobOrderDAO: "+schoolInJobOrderDAO);
			System.out.println("springContext: "+springContext);*/
			List<JobOrder> lstJobOrders = jobOrderDAO.getTodayAciveJob();
			System.out.println("kkkkkkkkkkkkk lstJobOrders: "+lstJobOrders);
			for(JobOrder jobOrder : lstJobOrders)
			{
				int JobOrderType = jobOrder.getCreatedForEntity();
				DistrictMaster districtMaster = jobOrder.getDistrictMaster();
				SchoolMaster schoolMaster = null;
				String JobPostingURL = Utility.getShortURL(basepath+"applyteacherjob.do?jobId="+jobOrder.getJobId());
				if(JobOrderType==3)
				{
					List<SchoolMaster>  lstSchoolId = schoolInJobOrderDAO.findSchoolIds(jobOrder);
					if(lstSchoolId.size()>0)
					{
						for(SchoolMaster schoolMaster2 : lstSchoolId)
						{
							manageJobOrdersAjax.postOnSocialNetwork(socialService, JobOrderType, districtMaster, schoolMaster2, basepath, jobOrder, JobPostingURL);
						}
					}
				}else if(JobOrderType==2)
					manageJobOrdersAjax.postOnSocialNetwork(socialService, JobOrderType, districtMaster, schoolMaster, basepath, jobOrder, JobPostingURL);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("============================================");
		System.out.println("PostJobsOnSocialSitesService End: "+ new Date());
		System.out.println("============================================");
	}
	
}
