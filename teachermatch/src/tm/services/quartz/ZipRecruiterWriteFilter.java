package tm.services.quartz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.dao.JobOrderDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.utility.Utility;

public class ZipRecruiterWriteFilter implements Filter {
      
	FilterConfig filterConfig = null;
	ServletContext servletContext= null;
	
	JobOrderDAO jobOrderDAO = null;
	SchoolMasterDAO schoolMasterDAO = null; 
	ApplicationContext springContext=null;
	//ServletContext servletContext = null;
	String fileName="feed.xml";

	@Override
	public void init(FilterConfig cinfig) throws ServletException {
		filterConfig = cinfig;
		System.out.println("ZipRecuriter Filter Initialized filterConfig");
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,	FilterChain filterChain) throws IOException, ServletException {

			System.out.println("Calling doFilter block");
			
			springContext = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
			jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO");
			
			//System.out.println("Joborder:*********** "+jobOrderDAO);
			
			getXMLFeedForZiprecruiterByPOST(jobOrderDAO);
			
			
			 servletContext = filterConfig.getServletContext();
			
			 String source = Utility.getValueOfPropByKey("rootPath")+"jobfeed/feed.xml";
			 System.out.println("source Path "+source);

			 String target = servletContext.getRealPath("/")+"/"+"/jobfeed/";
			 System.out.println("Real target Path "+target);
			 String pureString = getStoreFileString(source);
			 
			 File newTextFile = new File(source);
			 FileUtils.writeStringToFile(newTextFile, pureString);
			
			File sourceFile = new File(source);
			File targetDir = new File(target);
			
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());

			try {
				FileUtils.copyFile(sourceFile, targetFile);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			String path = Utility.getValueOfPropByKey("contextBasePath")+"/jobfeed/feed.xml";
			System.out.println("contextBasePath of URL: "+path);
			
			filterChain.doFilter(request, response);
		
	}

	@Override
	public void destroy() {
			System.out.println("ZipRecuriter Filter Destroyed");
	}
	
	
	public void getXMLFeedForZiprecruiterByPOST(JobOrderDAO jobOrderDAO){
		try{
			System.out.println("**************Enter in getXMLFeedForZiprecruiter*********************");
			FileOutputStream outStream = null;
			servletContext = ContextLoaderListener.getCurrentWebApplicationContext().getServletContext();
			
	   /***************************creating directory inside  ***rootPath*******************************************/		
			    String jobFeedDir = Utility.getValueOfPropByKey("rootPath"); 
				
				String sourceDirName = jobFeedDir+"//"+"jobfeed"; 
				String targetFileName = sourceDirName+"/"+fileName; ;
				
				File fileFeedDir = new File(sourceDirName);
				
				File sourceFile= new File(targetFileName);
				
				if(!fileFeedDir.exists())
				{
					fileFeedDir.mkdir();
					outStream = new FileOutputStream(sourceFile);
					System.out.println("directory created succesffully");
				}
				if(sourceFile.exists())
				{
					outStream = new FileOutputStream(targetFileName);
					System.out.println("sourceFile.exists()writing feed.xml : "+sourceFile.exists());
				}
				
		/********************************************************************/	
			
			List<DistrictMaster> lstDistrictMaster = null;
			List<JobOrder> lstJobOrder = null;
			
			String jobPostingDate = null;
			String country = "USA";				
			String url = "http://myedquest.org/jobboard.do";
			
			Timestamp currentTime = new Timestamp(new Date().getTime());
			jobPostingDate = currentTime.toString();
			
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = null;
			Document document = null;
			Element root = null;
			
			docBuilder = builderFactory.newDocumentBuilder();
			document = docBuilder.newDocument();
		
			root = document.createElement("source");
			document.appendChild(root);	
			
				if(lstDistrictMaster!=null){
				
				System.out.println("inside districtMaster: ");
				}else{
					    lstJobOrder = jobOrderDAO.findAllActivJobOrder();
						System.out.println("lstJobOrder::::::::::::::: : "+lstJobOrder.size());
						System.out.println("");
					}
			if(lstJobOrder!=null)	
			for(JobOrder jobOrder : lstJobOrder)
			{
				
				String jobTitle = "";
				Integer referenceNumber = null;
				String company = "";
				String city = "";
				String state = "";
				String postalCode = "";
				String description = "";
				String category = "";
				
				
				jobTitle = jobOrder.getJobTitle();
				if(jobTitle==null || jobTitle.equalsIgnoreCase(""))
				{
					jobTitle = "N/A";
				}
				
				referenceNumber = jobOrder.getJobId();
				String jobId = referenceNumber.toString();
				if(jobId==null ||jobId.equalsIgnoreCase(""))
				{
					jobId = "N/A";
				}
				
				if(jobOrder!=null && jobOrder.getDistrictMaster()!=null)
				{
					company = jobOrder.getDistrictMaster().getDistrictName();
					city = jobOrder.getDistrictMaster().getCityName();
					state = jobOrder.getDistrictMaster().getStateId().getStateShortName();
					postalCode = jobOrder.getDistrictMaster().getZipCode();
				}
				else if(jobOrder.getBranchMaster()!=null)
				{
					company = jobOrder.getBranchMaster().getBranchName();
					city = jobOrder.getBranchMaster().getCityName();
					state = jobOrder.getBranchMaster().getStateMaster().getStateShortName();
					postalCode = jobOrder.getBranchMaster().getZipCode();
				}
				else if(jobOrder.getHeadQuarterMaster()!=null)
				{
					company = jobOrder.getHeadQuarterMaster().getHeadQuarterName();
					city = jobOrder.getHeadQuarterMaster().getCityName();
					state = jobOrder.getHeadQuarterMaster().getStateId().getStateShortName();
					postalCode = jobOrder.getHeadQuarterMaster().getZipCode();
				}
					
				
				if(company==null || company.equalsIgnoreCase(""))
				{
					company = "N/A";
				}
				
				if(city==null || city.equalsIgnoreCase(""))
				{
					city = "N/A";
				}
				
				if(state==null ||state.equalsIgnoreCase(""))
				{
					state = "N/A";
				}
				
				if(postalCode==null || postalCode.equalsIgnoreCase(""))
				{
					postalCode = "N/A";
				}
				
				description = jobOrder.getJobDescription();
				if(description==null || description.equalsIgnoreCase(""))
				{	
					description = "N/A";
				}
				//System.out.println("jobTitle : "+jobTitle);
				//System.out.println("referenceNumber : "+jobId);
				//System.out.println("description : "+description);
				//System.out.println("category : "+category);
				//System.out.println("company : "+company);
				//System.out.println("city : "+city);
				//System.out.println("state : "+state);
				//System.out.println("postalCode : "+postalCode);
				
				Node jobNode = createJobNode(document,jobTitle,jobPostingDate,jobId,company,city,state,postalCode,country,description,category,url);
			    root.appendChild(jobNode);
			  }
		    try {
		        document.setXmlStandalone(true);
		        Source xmlSource = new DOMSource(document);
		      
		        Result result = new StreamResult(outStream);
		        
		        TransformerFactory transformerFactory = TransformerFactory.newInstance();
		        Transformer transformer = transformerFactory.newTransformer();
		        transformer.setOutputProperty("indent", "no");
		        transformer.transform(xmlSource, result);
		         
		      }
		    catch(TransformerFactoryConfigurationError factoryError) {
		          System.err.println("Error creating " + "TransformerFactory");
		          factoryError.printStackTrace();
		        }catch (TransformerException transformerError) {
		          System.err.println("Error transforming document");
		          transformerError.printStackTrace();
		        }
		}catch(Exception e){e.printStackTrace();}
	}
	
	public static String getStoreFileString(String str){
		try {
			File file = new File(str);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line);
				stringBuffer.append("\n");
			}
			fileReader.close();
			String originalString = stringBuffer.toString();
		
			 StringBuffer pureStr = new StringBuffer();
			 
			 Pattern p = Pattern.compile("&#16;");
			 Matcher m = p.matcher(originalString);
			 while (m.find()) {
			     m.appendReplacement(pureStr, " ");
			 }
			 m.appendTail(pureStr);
			
			
			 StringBuffer pureString = new StringBuffer();
			 Pattern p1 = Pattern.compile("&#19;");
			 Matcher m1 = p1.matcher(pureStr);
			 while (m1.find()) {
			     m1.appendReplacement(pureString, " ");
			 }
			 m1.appendTail(pureString);
			return pureString.toString();
		}catch (IOException e){
			e.printStackTrace();
			return null;
		}
	}
	 public static Node createJobNode(Document document,String title,String date,String jobId,String jobCompany,String jobCity,String jobState, String pstlCode,String jobCountry,String jobDescription,String jobCategory, String jobUrl) {
		 
		    Element jobTitle = document.createElement("title");
		    Element jobPostingDate = document.createElement("date");
		    
		    Element jobReferenceNumber = document.createElement("referencenumber");
		    Element company = document.createElement("company");
		    
		    Element city = document.createElement("city");
		    Element state = document.createElement("state");
		    
		    Element postalCode = document.createElement("postalcode");
		    Element country = document.createElement("country");
		    
		    Element description = document.createElement("description");
		    Element category = document.createElement("category");
		    Element url = document.createElement("url");
		    
		    jobTitle.appendChild(document.createCDATASection(title));
		    jobPostingDate.appendChild(document.createCDATASection(date));
		    
		    jobReferenceNumber.appendChild(document.createCDATASection(jobId));
		    company.appendChild(document.createCDATASection(jobCompany));
		    
		    city.appendChild(document.createCDATASection(jobCity));
		    state.appendChild(document.createCDATASection(jobState));
		    
		    postalCode.appendChild(document.createCDATASection(pstlCode));
		    country.appendChild(document.createCDATASection(jobCountry));
		    
		    description.appendChild(document.createCDATASection(jobDescription));
		    category.appendChild(document.createCDATASection(jobCategory));
		    url.appendChild(document.createCDATASection(jobUrl));
		    
		    Element job = document.createElement("job");

		    job.appendChild(jobTitle);
		    job.appendChild(jobPostingDate);
		    
		    job.appendChild(jobReferenceNumber);
		    job.appendChild(company);
		    
		    job.appendChild(city);
		    job.appendChild(state);
		    
		    
		    job.appendChild(postalCode);
		    job.appendChild(country);
		    
		    job.appendChild(description);
		    job.appendChild(category);
		    job.appendChild(url);
		    return job;
		  }
	
}
