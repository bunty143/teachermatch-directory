package tm.services.quartz;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.controller.textfile.HrmsPostionsTextController;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.hrmspostionstext.HrmsPositionsTextDetailDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.user.UserMasterDAO;

public class HrmsPostionsTextSchedular   implements Job
{

	ApplicationContext springContext=null;
	ServletContext servletContext = null;
	DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	SchoolMasterDAO schoolMasterDAO;
	DistrictMasterDAO districtMasterDAO;
	UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	JobCategoryMasterDAO jobCategoryMasterDAO;
	JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	SchoolInJobOrderDAO schoolInJobOrderDAO;
	JobOrderDAO jobOrderDAO;
	HrmsPositionsTextDetailDAO hrmsPositionsTextDetailDAO;
	UserMasterDAO userMasterDAO;
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException 
	{
		springContext = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		districtRequisitionNumbersDAO = (DistrictRequisitionNumbersDAO) springContext.getBean("districtRequisitionNumbersDAO");
		schoolMasterDAO = (SchoolMasterDAO) springContext.getBean("schoolMasterDAO");
		userMasterDAO = (UserMasterDAO) springContext.getBean("userMasterDAO");
		districtMasterDAO = (DistrictMasterDAO) springContext.getBean("districtMasterDAO");
		userUploadFolderAccessDAO = (UserUploadFolderAccessDAO) springContext.getBean("userUploadFolderAccessDAO");
		jobCategoryMasterDAO = (JobCategoryMasterDAO) springContext.getBean("jobCategoryMasterDAO");
		jobRequisitionNumbersDAO = (JobRequisitionNumbersDAO) springContext.getBean("jobRequisitionNumbersDAO");
		schoolInJobOrderDAO = (SchoolInJobOrderDAO) springContext.getBean("schoolInJobOrderDAO");
		jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO");
		hrmsPositionsTextDetailDAO = (HrmsPositionsTextDetailDAO) springContext.getBean("hrmsPositionsTextDetailDAO");
		
		HrmsPostionsTextController controller=new HrmsPostionsTextController();
		controller.setDAO(districtRequisitionNumbersDAO,
				schoolMasterDAO, 
				userMasterDAO, 
				districtMasterDAO, 
				userUploadFolderAccessDAO, 
				jobCategoryMasterDAO, 
				jobRequisitionNumbersDAO, 
				schoolInJobOrderDAO, 
				jobOrderDAO, 
				hrmsPositionsTextDetailDAO);
		controller.downloadingPositionFile(null);
		
	}

}
