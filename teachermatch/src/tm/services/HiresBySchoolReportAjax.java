package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.api.PaginationAndSortingAPI;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherProfileVisitHistory;
import tm.bean.cgreport.PercentileZscoreTscore;
import tm.bean.cgreport.TmpPercentileWiseZScore;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherProfileVisitHistoryDAO;
import tm.dao.cgreport.TmpPercentileWiseZScoreDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.services.report.CGReportService;
import tm.utility.DistrictNameCompratorASC;
import tm.utility.DistrictNameCompratorDESC;
import tm.utility.EndDateCompratorASC;
import tm.utility.EndDateCompratorDESC;
import tm.utility.JobTitleCompratorASC;
import tm.utility.JobTitleCompratorDESC;
import tm.utility.LastActivityDateCompratorASC;
import tm.utility.LastActivityDateCompratorDESC;
import tm.utility.PostingDateCompratorASC;
import tm.utility.PostingDateCompratorDESC;
import tm.utility.SchoolNameCompratorASC;
import tm.utility.SchoolNameComratorDESC;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
public class HiresBySchoolReportAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
			
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;

	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO;
		

	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	
	
	List<TmpPercentileWiseZScore> tmpPercentileWiseZScoreList = null;
	List<PercentileZscoreTscore> pztList = null;
	CGReportService cGReportService = new CGReportService();
	Map<Integer,String> allColor=new HashMap<Integer, String>();
	public String displayRecordsByEntityType(String districtName,String schoolName,int districtOrSchoolId,Integer schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String jobTitle,String expDate,String normScoreFilter,String posDate,String endPost)
	 {
			//System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId+":::::::::::jobTitle"+jobTitle+"::::::::::::::::::::Exp date:::::"+expDate);
		
		
		
		if(tmpPercentileWiseZScoreList==null)
		{
			tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreDAO.findByCriteria(Order.asc("zScore"));
			//System.out.println("tmpPercentileWiseZScoreList.size() == "+tmpPercentileWiseZScoreList.size());
		}
		if(pztList==null)
		{
			pztList = cGReportService.populateZScoreMap(tmpPercentileWiseZScoreList);
			//System.out.println("pztList.size() == "+pztList.size());
		}
		
		for(int i=1;i<=100;i++)
		{
			Integer normscore = Integer.valueOf((int) Math.round(i));						
			double percentile=cGReportService.getPercentile(pztList,normscore);			
			Object[] ob =cGReportService.getDeciles(percentile);
			allColor.put(i, ""+ob[0]);			
		}
		
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=1;
			try{
				boolean flagfordata=true;
				if(!districtName.trim().equals("") && districtOrSchoolId==0)
					flagfordata=false;
				else if(!schoolName.equals("") && schoolId==0)
					flagfordata=false;
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				//System.out.println("schoolMaster="+schoolMaster+"districtMaster="+districtMaster+"entityID="+entityID);
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position


				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="districtName";
				String sortOrderNoField="districtName";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("districtName"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("schoolName"))
					sortingcheck=2;
				else if(sortOrder.equalsIgnoreCase("jobTitle"))
					sortingcheck=3;
				else if(sortOrder.equalsIgnoreCase("posDate"))
					sortingcheck=4;
				else if(sortOrder.equalsIgnoreCase("expDate"))
					sortingcheck=5;
				else if(sortOrder.equalsIgnoreCase("noOfRecord"))
					sortingcheck=6;
				else if(sortOrder.equalsIgnoreCase("nepi"))
					sortingcheck=7;				
				else if(sortOrder.equalsIgnoreCase("avgEPI")){
					sortingcheck=8;
				}else
					sortingcheck=9;
								
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
								
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<JobOrder> lstJoOrder =new ArrayList<JobOrder>();
				 List<JobForTeacher> lstJobForTeacher1=new ArrayList<JobForTeacher>();
				 List<JobOrder> lstSchoolInJobOrder = new ArrayList<JobOrder>();
				 SchoolMaster schoolId1=null;
							
				if(flagfordata)
				if((entityID==2)||(entityID==3))
				{		
					DistrictMaster distId=districtMasterDAO.findById(districtOrSchoolId, false, false);
					lstJoOrder=jobOrderDAO.getByDistrictID(distId,jobTitle,expDate,posDate,endPost);
							    	 
				}
				else if(entityID==1) 
				{		if(districtOrSchoolId!=0)	
				      {
							DistrictMaster distId=districtMasterDAO.findById(districtOrSchoolId, false, false);
							lstJoOrder=jobOrderDAO.getByDistrictID(distId,jobTitle,expDate,posDate,endPost);
					  }
				}
							
				if(flagfordata)
				{
					if(schoolId!=null && schoolId!=0)
					{		
						schoolId1=schoolMasterDAO.findById(Long.valueOf(schoolId.longValue()), false, false);
						lstSchoolInJobOrder=schoolInJobOrderDAO.allJobOrderBySchool(schoolId1);
											
						if((jobTitle!=null && !jobTitle.equals("")) && (expDate!=null && !expDate.equals("")) )
						{
							lstJoOrder.retainAll(lstSchoolInJobOrder);
						}
						else if(jobTitle!=null && !jobTitle.equals(""))
						{
							lstJoOrder.retainAll(lstSchoolInJobOrder);
						}
						else if(expDate!=null && !expDate.equals(""))
						{						
							lstJoOrder.retainAll(lstSchoolInJobOrder);
						}						
						else
						{
							lstJoOrder.clear();
							lstJoOrder=lstSchoolInJobOrder;
						}
					}
				}				
				
				List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();			
				List<TeacherDetail> teachers1= new ArrayList<TeacherDetail>();
				List<TeacherNormScore> listTeacherNormScores =new ArrayList<TeacherNormScore>();
				Map<Integer,TeacherNormScore> mapofNormScoresByTeacher= new HashMap<Integer, TeacherNormScore>();
				 Map<String, JobForTeacher> mapjob=new HashMap<String, JobForTeacher>();
				 Map<String, Integer> count=new HashMap<String, Integer>();
				 Map<String, Integer> normScoreMap=new HashMap<String, Integer>(); 
				 Map<String, JobOrder> jobForTeacherMapforSchool=new HashMap<String, JobOrder>();	
				 Map<String, Integer> countForAvg=new HashMap<String, Integer>();
				
				 if(schoolId!=null && schoolId!=0)
				 if(lstJoOrder.size()>0)
				 {
					 for(JobOrder jobMap:lstJoOrder)
					 {	
					 jobForTeacherMapforSchool.put(schoolId1.getSchoolName()+"_"+jobMap.getJobId(), jobMap);					
					 }			 
				 }
				 				 
				if(lstJoOrder.size()>0)
				 {
					lstJobForTeacher=jobForTeacherDAO.findJobByJobCategoryAndDistrict(lstJoOrder);
				 }			
				
				if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
				{
				for(JobForTeacher jft : lstJobForTeacher){
							teachers1.add(jft.getTeacherId());
				}
				}
				
				if(teachers1!=null && teachers1.size()>0){
					listTeacherNormScores=teacherNormScoreDAO.findNormScoreByTeacherByList(teachers1);
					if(listTeacherNormScores!=null && listTeacherNormScores.size()>0){
						for (TeacherNormScore teacherNormScore : listTeacherNormScores) {
							mapofNormScoresByTeacher.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);							
							
						}
					}
				}				
						 
				 if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
				 {
					 for(JobForTeacher lstjobmap: lstJobForTeacher)
					 {
						 if(lstjobmap.getSchoolMaster()!=null)
                         {									
								 int total=1;
								 int normScore=0;
								 int totalAvg=1;
								 if(count.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())!=null)
								 {
									 total=count.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())+1;
									 count.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),total);
									 
									 if(mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId())!=null && normScoreMap.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())!=null)
									 {
										 if(countForAvg.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())!=null)
										 {
											 totalAvg=countForAvg.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())+1;
											 countForAvg.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),totalAvg); 
										 }
										
										 normScore=normScoreMap.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())+
									 			mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId()).getTeacherNormScore();
										 normScoreMap.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), normScore);
									 }									 
								 }
								 else
								 {
									 count.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),total);									 								 
									 if(mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId())!=null)
									 {	
										 countForAvg.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),totalAvg);
										 normScoreMap.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId()).getTeacherNormScore() );
									 }
									 else
										 normScoreMap.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), 0 );
									 
								 }		
								 if(schoolId!=null && schoolId!=0)
								 {
									 if(jobForTeacherMapforSchool.containsKey(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId()))
									 {
										 mapjob.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), lstjobmap); 
									 }
								 }
								 else	
								 {			lstjobmap.setSchoolName(lstjobmap.getSchoolMaster().getSchoolName());	
								            lstjobmap.setJobTitle(lstjobmap.getJobId().getJobTitle());	
								            lstjobmap.setPostDate(lstjobmap.getJobId().getJobStartDate());	
								            lstjobmap.setEndDate(lstjobmap.getJobId().getJobEndDate());							
									 mapjob.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), lstjobmap);
									 
								 }						
                         }
					 }
				 }
				 				 
				if(normScoreFilter==null || normScoreFilter.equals(""))
			    totalRecord=mapjob.size();
				List<JobForTeacher> finalJobOrder =new ArrayList<JobForTeacher>();
				List<JobForTeacher> finalJobOrdersort =new ArrayList<JobForTeacher>();
				List<JobForTeacher> finalJobOrdersortNorm =new ArrayList<JobForTeacher>();
				
			    
			   			 
			  List targetList = new ArrayList(mapjob.values());		
			  finalJobOrdersort=targetList;			  
			  int normtotal=0;
			  if(normScoreFilter!=null && !normScoreFilter.equals(""))
			  for(JobForTeacher finalJobOrderForGrid: finalJobOrdersort)
			  {
				  int norScore= normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())/count.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId());
				  if(normScoreFilter!=null && !normScoreFilter.equals(""))
					{								
						if(Integer.parseInt(normScoreFilter)==norScore)
						{
							finalJobOrdersortNorm.add(finalJobOrderForGrid);							
							normtotal++;
						}
						}
			  }			 
			  if(normScoreFilter!=null && !normScoreFilter.equals(""))
			  {
				  targetList.retainAll(finalJobOrdersortNorm);
				  totalRecord= normtotal;
			  }
			  
			  if(totalRecord<end)
					end=totalRecord;
			  
			  
				if(sortOrder.equals("schoolName")){ 
					
					  for(JobForTeacher jf:finalJobOrdersort) 
						 {
					    	 jf.setSchoolName(jf.getSchoolMaster().getSchoolName());
						 }
			 		
				     if(sortOrderType.equals("0"))
					  Collections.sort(targetList, new SchoolNameCompratorASC());
					 else 
					  Collections.sort(targetList, new SchoolNameComratorDESC());
			     }
			  
				if(sortOrder.equals("jobTitle")){ 
					
					  for(JobForTeacher jf:finalJobOrdersort) 
						 {
						  	 jf.setJobTitle(jf.getJobId().getJobTitle());
						 }
			 		
				     if(sortOrderType.equals("0"))
					  Collections.sort(targetList, new JobTitleCompratorASC());
					 else 
					  Collections.sort(targetList, new JobTitleCompratorDESC());
			     } 
				
				if(sortOrder.equals("posDate")){  
				     for(JobForTeacher jf:finalJobOrdersort) 
					 {
				    	 jf.setPostDate(jf.getJobId().getJobStartDate());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(targetList, new PostingDateCompratorASC());
					 else 
					  Collections.sort(targetList, new PostingDateCompratorDESC());
			     }
				
				if(sortOrder.equals("expDate")){  
				     for(JobForTeacher jf:finalJobOrdersort) 
					 {
				    	 jf.setEndDate(jf.getJobId().getJobEndDate());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(targetList, new EndDateCompratorASC());
					 else 
					  Collections.sort(targetList, new EndDateCompratorDESC());
			     }
				
				if(sortOrder.equals("noOfRecord"))
				{ 
					for(JobForTeacher jft : finalJobOrdersort)
					{
					if(count.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null)
							{								
								 jft.setNoOfRecord(count.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId()));
							}				
					}					 	
				 	    if(sortOrderType.equals("0"))
						  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecord);
						 else 
						  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecordDesc);
			     }
				
				if(sortOrder.equals("nepi"))
				{ 
					for(JobForTeacher jft : finalJobOrdersort)
					{
					if(countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null)
							{								
								 jft.setNoOfRecord(countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId()));
							}				
					}					 	
				 	    if(sortOrderType.equals("0"))
						  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecord);
						 else 
						  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecordDesc);
			     }
				
				if(sortOrder.equals("avgEPI"))
				{ 
					for(JobForTeacher jft : finalJobOrdersort)
					{
					if(normScoreMap.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null && countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null)
							{	
						try
						{
								 jft.setEpiNormScoreAvg(normScoreMap.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())/countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId()));
						}catch (Exception e) {
							
						}
							}
					else
					{
						jft.setEpiNormScoreAvg(0);
					}
					}					 	
				 	    if(sortOrderType.equals("0"))
						  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorEPINormScore);
						 else 
						  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorEPINormScoreDesc);
			     }
	
				  finalJobOrder=targetList.subList(start,end);	
				String responseText="";				
				tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDistrictName", locale),sortOrderNoField,"districtName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSchool", locale),sortOrderNoField,"schoolName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderNoField,"jobTitle",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblPostingDate", locale),sortOrderNoField,"posDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblExpirationDate", locale),sortOrderNoField,"expDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblNumberOfHires", locale),sortOrderNoField,"noOfRecord",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblN_EPI", locale),sortOrderNoField,"nepi",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblAvgEPINormScore", locale),sortOrderNoField,"avgEPI",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
							
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				if(finalJobOrder.size()==0){
					 tmRecords.append("<tr><td colspan='8' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
					}
							
			  if(finalJobOrder.size()>0){	
					 for(JobForTeacher finalJobOrderForGrid: finalJobOrder)
					  {	
						 
						 String ccsName="";
						 String colorName="";
						int norScore= 0;
						if(countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=null)
                    	{
							try
							{
							norScore= normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())/countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId());
							}catch (Exception e) {
								
							}
                    	}						
						if(allColor.containsKey(norScore))
						{							
							colorName=allColor.get(norScore).trim();							
						}											
							ccsName="nobground2";												 
						        tmRecords.append("<tr>");					  	                	       
	                            tmRecords.append("<td>"+finalJobOrderForGrid.getJobId().getDistrictMaster().getDistrictName()+"</td>");	                            	                         
		                        tmRecords.append("<td>"+finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"</td>");		                    
	                            tmRecords.append("<td>"+finalJobOrderForGrid.getJobId().getJobTitle()+"</td>");			                    
			                    tmRecords.append("<td>"+Utility.getUSformatDateTime(finalJobOrderForGrid.getJobId().getJobStartDate())+"</td>");			                    
			                    tmRecords.append("<td>"+Utility.getUSformatDateTime(finalJobOrderForGrid.getJobId().getJobEndDate())+"</td>");			                   
			                    tmRecords.append("<td>"+count.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())+"</td>");
			                    
			                    if(countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=null)
			                    {
			                    	tmRecords.append("<td>"+countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())+"</td>");
			                    }
			                    else
			                    {
			                    	tmRecords.append("<td>N/A</td>");
			                    }
			                    
			                    try
			                    {
			                    	
			                    	if(normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=0 && countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=null)
			                    	{
			                    		tmRecords.append("<td><span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>"+normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())/countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())+"</span></td>");	
			                    	}
			                    	else
			                    	{
			                    		tmRecords.append("<td>N/A</td>");
			                    	}
			                    
			                    }
			                    catch(Exception e){e.printStackTrace();}
							
					  }                          
	              }			
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			}catch(Exception e){
				e.printStackTrace();
			}			
		 return tmRecords.toString();
	  }
		
	public String displayRecordsByEntityTypeEXL(String districtName,String schoolName,int districtOrSchoolId,Integer schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String jobTitle,String expDate,String normScoreFilter,String posDate,String endPost)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		int sortingcheck=1;
		try{
			
			boolean flagfordata=true;
			if(!districtName.trim().equals("") && districtOrSchoolId==0)
				flagfordata=false;
			else if(!schoolName.equals("") && schoolId==0)
				flagfordata=false;
			
			
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
		}
		
		//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start =((pgNo-1)*noOfRowInPage);
		int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		
		 /** set default sorting fieldName **/
		String sortOrderFieldName="districtName";
		String sortOrderNoField="districtName";

		if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("districtName"))
			sortingcheck=1;
		else if(sortOrder.equalsIgnoreCase("schoolName"))
			sortingcheck=2;
		else if(sortOrder.equalsIgnoreCase("jobTitle"))
			sortingcheck=3;
		else if(sortOrder.equalsIgnoreCase("posDate"))
			sortingcheck=4;
		else if(sortOrder.equalsIgnoreCase("expDate"))
			sortingcheck=5;
		else if(sortOrder.equalsIgnoreCase("noOfRecord"))
			sortingcheck=6;
		else if(sortOrder.equalsIgnoreCase("nepi"))
			sortingcheck=7;
		else if(sortOrder.equalsIgnoreCase("avgEPI")){
			sortingcheck=8;
		}else
			sortingcheck=9;
			
			/**Start set dynamic sorting fieldName **/
			
		Order  sortOrderStrVal=null;

		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
		}

		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
		
		//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
			List<JobOrder> lstJoOrder =new ArrayList<JobOrder>();
			 List<JobForTeacher> lstJobForTeacher1=new ArrayList<JobForTeacher>();
			 List<JobOrder> lstSchoolInJobOrder = new ArrayList<JobOrder>();
			 SchoolMaster schoolId1=null;
						
			if(flagfordata)
			if((entityID==2)||(entityID==3))
			{		
				DistrictMaster distId=districtMasterDAO.findById(districtOrSchoolId, false, false);
				lstJoOrder=jobOrderDAO.getByDistrictID(distId,jobTitle,expDate,posDate,endPost);
				System.out.println("lstJoOrder::::::::::::::::::::"+lstJoOrder.size());
		    	 
			}
			else if(entityID==1) 
			{				
				DistrictMaster distId=districtMasterDAO.findById(districtOrSchoolId, false, false);
				lstJoOrder=jobOrderDAO.getByDistrictID(distId,jobTitle,expDate,posDate,endPost);
				totalRecord = lstJoOrder.size();
			}
						
			if(flagfordata)
			{
				if(schoolId!=null && schoolId!=0)
				{					
				    schoolId1=schoolMasterDAO.findById(Long.valueOf(schoolId.longValue()), false, false);
					lstSchoolInJobOrder=schoolInJobOrderDAO.allJobOrderBySchool(schoolId1);
					System.out.println("sizeee of lstSchoolInJobOrder:::"+lstSchoolInJobOrder.size());
					
					if((jobTitle!=null && !jobTitle.equals("")) && (expDate!=null && !expDate.equals("")) )
					{
						lstJoOrder.retainAll(lstSchoolInJobOrder);
					}
					else if(jobTitle!=null && !jobTitle.equals(""))
					{
						lstJoOrder.retainAll(lstSchoolInJobOrder);
					}
					else if(expDate!=null && !expDate.equals(""))
					{						
						lstJoOrder.retainAll(lstSchoolInJobOrder);
					}						
					else
					{
						lstJoOrder.clear();
						lstJoOrder=lstSchoolInJobOrder;
					}
				}
			}				
			
			List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();			
			List<TeacherDetail> teachers1= new ArrayList<TeacherDetail>();
			List<TeacherNormScore> listTeacherNormScores =new ArrayList<TeacherNormScore>();
			Map<Integer,TeacherNormScore> mapofNormScoresByTeacher= new HashMap<Integer, TeacherNormScore>();
			 Map<String, JobForTeacher> mapjob=new HashMap<String, JobForTeacher>();
			 Map<String, Integer> count=new HashMap<String, Integer>();
			 Map<String, Integer> normScoreMap=new HashMap<String, Integer>(); 
			 Map<String, JobOrder> jobForTeacherMapforSchool=new HashMap<String, JobOrder>();
			 Map<String, Integer> countForAvg=new HashMap<String, Integer>();
			 
			 			
			
			 if(schoolId!=null && schoolId!=0)
			 if(lstJoOrder.size()>0)
			 {
				 for(JobOrder jobMap:lstJoOrder)
				 {	
				 jobForTeacherMapforSchool.put(schoolId1.getSchoolName()+"_"+jobMap.getJobId(), jobMap);
				
				 }			 
			 }
			 				 
			if(lstJoOrder.size()>0)
			 {
				lstJobForTeacher=jobForTeacherDAO.findJobByJobCategoryAndDistrict(lstJoOrder);
				System.out.println("lstJobForTeacher::::"+lstJobForTeacher.size());
			 }			
			
			if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
			{
			for(JobForTeacher jft : lstJobForTeacher){
						teachers1.add(jft.getTeacherId());
			}
			}
			
			if(teachers1!=null && teachers1.size()>0){
				listTeacherNormScores=teacherNormScoreDAO.findNormScoreByTeacherByList(teachers1);
				if(listTeacherNormScores!=null && listTeacherNormScores.size()>0){
					for (TeacherNormScore teacherNormScore : listTeacherNormScores) {
						mapofNormScoresByTeacher.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);				
						
					}
				}
			}
			
					 
			 if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
			 {
				 for(JobForTeacher lstjobmap: lstJobForTeacher)
				 {
					 if(lstjobmap.getSchoolMaster()!=null)
                     {									
							 int total=1;
							 int normScore=0;
							 int totalAvg=1;
							 if(count.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())!=null)
							 {
								 total=count.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())+1;
								 count.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),total);
								 
								 if(mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId())!=null && normScoreMap.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())!=null)
								 {
									 if(countForAvg.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())!=null)
									 {
										 totalAvg=countForAvg.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())+1;
										 countForAvg.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),totalAvg); 
									 }
									 
									 normScore=normScoreMap.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())+
								 			mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId()).getTeacherNormScore();
									 normScoreMap.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), normScore);
								 }									 
							 }
							 else
							 {
								 count.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),total);
								 								 									 
								 if(mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId())!=null)
								 {
									 countForAvg.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),totalAvg);
									 normScoreMap.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId()).getTeacherNormScore() );
								 }
								 else
									 normScoreMap.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), 0 );
								 
							 }		
							 if(schoolId!=null && schoolId!=0)
							 {
								 if(jobForTeacherMapforSchool.containsKey(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId()))
								 {
									 mapjob.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), lstjobmap); 
								 }
							 }
							 else	
							 {
								
								 lstjobmap.setSchoolName(lstjobmap.getSchoolMaster().getSchoolName());
								 mapjob.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), lstjobmap);
								 
							 }						
                     }
				 }
			 }
			
			List<JobForTeacher> finalJobOrder =new ArrayList<JobForTeacher>();	
			List<JobForTeacher> finalJobOrdersort =new ArrayList<JobForTeacher>();	
			List<JobForTeacher> finalJobOrdersortNorm =new ArrayList<JobForTeacher>();
			
		   List targetList = new ArrayList(mapjob.values());
		   finalJobOrdersort=targetList;
		   
			  if(normScoreFilter!=null && !normScoreFilter.equals(""))
			  for(JobForTeacher finalJobOrderForGrid: finalJobOrdersort)
			  {
				  int norScore= normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())/count.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId());
				  if(normScoreFilter!=null && !normScoreFilter.equals(""))
					{								
						if(Integer.parseInt(normScoreFilter)==norScore)
						{
							finalJobOrdersortNorm.add(finalJobOrderForGrid);							
							
						}
						}
			  }			 
			  if(normScoreFilter!=null && !normScoreFilter.equals(""))
			  {
				  targetList.retainAll(finalJobOrdersortNorm);				  
			  }		   
		  
			  
			if(sortOrder.equals("schoolName")){ 
				
				  for(JobForTeacher jf:finalJobOrdersort) 
					 {
				    	 jf.setSchoolName(jf.getSchoolMaster().getSchoolName());
					 }
		 		
			     if(sortOrderType.equals("0"))
				  Collections.sort(targetList, new SchoolNameCompratorASC());
				 else 
				  Collections.sort(targetList, new SchoolNameComratorDESC());
		     }
		  
			if(sortOrder.equals("jobTitle")){ 
				
				  for(JobForTeacher jf:finalJobOrdersort) 
					 {
					  	 jf.setJobTitle(jf.getJobId().getJobTitle());
					 }
		 		
			     if(sortOrderType.equals("0"))
				  Collections.sort(targetList, new JobTitleCompratorASC());
				 else 
				  Collections.sort(targetList, new JobTitleCompratorDESC());
		     } 
			
			if(sortOrder.equals("posDate")){  
			     for(JobForTeacher jf:finalJobOrdersort) 
				 {
			    	 jf.setPostDate(jf.getJobId().getJobStartDate());
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(targetList, new PostingDateCompratorASC());
				 else 
				  Collections.sort(targetList, new PostingDateCompratorDESC());
		     }
			
			if(sortOrder.equals("expDate")){  
			     for(JobForTeacher jf:finalJobOrdersort) 
				 {
			    	 jf.setEndDate(jf.getJobId().getJobEndDate());
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(targetList, new EndDateCompratorASC());
				 else 
				  Collections.sort(targetList, new EndDateCompratorDESC());
		     }
			
			if(sortOrder.equals("noOfRecord"))
			{ 
				for(JobForTeacher jft : finalJobOrdersort)
				{
				if(count.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null)
						{								
							 jft.setNoOfRecord(count.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId()));
						}				
				}					 	
			 	    if(sortOrderType.equals("0"))
					  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecord);
					 else 
					  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecordDesc);
		     }
			
			if(sortOrder.equals("nepi"))
			{ 
				for(JobForTeacher jft : finalJobOrdersort)
				{
				if(countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null)
						{								
							 jft.setNoOfRecord(countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId()));
						}				
				}					 	
			 	    if(sortOrderType.equals("0"))
					  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecord);
					 else 
					  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecordDesc);
		     }
			
			
			if(sortOrder.equals("avgEPI"))
			{ 
				for(JobForTeacher jft : finalJobOrdersort)
				{
				if(normScoreMap.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null && countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null)
						{
					try
					{
					          jft.setEpiNormScoreAvg(normScoreMap.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())/countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId()));
					}catch (Exception e) {
						
					}
						}
				else
				{
					jft.setEpiNormScoreAvg(0);
				}
				}					 	
			 	    if(sortOrderType.equals("0"))
					  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorEPINormScore);
					 else 
					  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorEPINormScoreDesc);
		     }
		   	   
		     finalJobOrder=targetList;
		     
			 //  Excel   Exporting	
					String time = String.valueOf(System.currentTimeMillis()).substring(6);
					//String basePath = request.getRealPath("/")+"/candidate";
					String basePath = request.getSession().getServletContext().getRealPath ("/")+"/hiredbyschool";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="hiredbyschool"+time+".xls";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

					WorkbookSettings wbSettings = new WorkbookSettings();

					wbSettings.setLocale(new Locale("en", "EN"));

					WritableCellFormat timesBoldUnderline;
					WritableCellFormat header;
					WritableCellFormat headerBold;
					WritableCellFormat times;

					WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
					workbook.createSheet("hiredbyschool", 0);
					WritableSheet excelSheet = workbook.getSheet(0);

					WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
					// Define the cell format
					times = new WritableCellFormat(times10pt);
					// Lets automatically wrap the cells
					times.setWrap(true);

					// Create create a bold font with unterlines
					WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
					WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

					timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
					timesBoldUnderline.setAlignment(Alignment.CENTRE);
					// Lets automatically wrap the cells
					timesBoldUnderline.setWrap(true);

					header = new WritableCellFormat(times10ptBoldUnderline);
					headerBold = new WritableCellFormat(times10ptBoldUnderline);
					CellView cv = new CellView();
					cv.setFormat(times);
					cv.setFormat(timesBoldUnderline);
					cv.setAutosize(true);

					header.setBackground(Colour.GRAY_25);

					// Write a few headers
					excelSheet.mergeCells(0, 0, 7, 1);
					Label label;
					label = new Label(0, 0, Utility.getLocaleValuePropByKey("headMsgHiresBySchool", locale), timesBoldUnderline);
					excelSheet.addCell(label);
					excelSheet.mergeCells(0, 3, 7, 3);
					label = new Label(0, 3, "");
					excelSheet.addCell(label);
					excelSheet.getSettings().setDefaultColumnWidth(18);
					
					int k=4;
					int col=1;
					label = new Label(0, k, Utility.getLocaleValuePropByKey("lblDistrictName", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(1, k, Utility.getLocaleValuePropByKey("lblSchoolName", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJoTil", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblPostingDate", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblExpirationDate", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblNumberOfHires", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblN_EPI", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblAvgEPINormScore", locale),header); 
					excelSheet.addCell(label);
										
					k=k+1;
					if(finalJobOrder.size()==0)
					{	
						excelSheet.mergeCells(0, k, 7, k);
						label = new Label(0, k, Utility.getLocaleValuePropByKey("msgNorecordfound", locale)); 
						excelSheet.addCell(label);
					}
					
					
					HashMap<String, WritableCellFormat> mapColors= new HashMap<String, WritableCellFormat>();
					
					
					if (finalJobOrder.size()>0) {
						WritableCellFormat cellFormat = new WritableCellFormat();
						Color color = Color.decode("0xFF0000");
						int red = color.getRed();
						int blue = color.getBlue();
						int green = color.getGreen();
						workbook.setColourRGB(Colour.AQUA, red, green, blue);
						cellFormat.setBackground(Colour.AQUA);
						mapColors.put("FF0000", cellFormat);
						WritableCellFormat cellFormat1 = new WritableCellFormat();
						Color color1 = Color.decode("0xFF6666");
						workbook.setColourRGB(Colour.BLUE, color1.getRed(), color1
								.getGreen(), color1.getBlue());
						cellFormat1.setBackground(Colour.BLUE);
						mapColors.put("FF6666", cellFormat1);
						WritableCellFormat cellFormat2 = new WritableCellFormat();
						Color color2 = Color.decode("0xFFCCCC");
						workbook.setColourRGB(Colour.BROWN, color2.getRed(), color2
								.getGreen(), color2.getBlue());
						cellFormat2.setBackground(Colour.BROWN);
						mapColors.put("FFCCCC", cellFormat2);
						WritableCellFormat cellFormat3 = new WritableCellFormat();
						Color color3 = Color.decode("0xFF9933");
						workbook.setColourRGB(Colour.CORAL, color3.getRed(), color3
								.getGreen(), color3.getBlue());
						cellFormat3.setBackground(Colour.CORAL);
						mapColors.put("FF9933", cellFormat3);
						WritableCellFormat cellFormat4 = new WritableCellFormat();
						Color color4 = Color.decode("0xFFFFCC");
						workbook.setColourRGB(Colour.GREEN, color4.getRed(), color4
								.getGreen(), color4.getBlue());
						cellFormat4.setBackground(Colour.GREEN);
						mapColors.put("FFFFCC", cellFormat4);
						WritableCellFormat cellFormat5 = new WritableCellFormat();
						Color color5 = Color.decode("0xFFFF00");
						workbook.setColourRGB(Colour.INDIGO, color5.getRed(), color5
								.getGreen(), color5.getBlue());
						cellFormat5.setBackground(Colour.INDIGO);
						mapColors.put("FFFF00", cellFormat5);
						WritableCellFormat cellFormat6 = new WritableCellFormat();
						Color color6 = Color.decode("0x66CC66");
						workbook.setColourRGB(Colour.LAVENDER, color6.getRed(), color6
								.getGreen(), color6.getBlue());
						cellFormat6.setBackground(Colour.LAVENDER);
						mapColors.put("66CC66", cellFormat6);
						WritableCellFormat cellFormat7 = new WritableCellFormat();
						Color color7 = Color.decode("0x00FF00");
						workbook.setColourRGB(Colour.LIME, color7.getRed(), color7
								.getGreen(), color7.getBlue());
						cellFormat7.setBackground(Colour.LIME);
						mapColors.put("00FF00", cellFormat7);
						WritableCellFormat cellFormat8 = new WritableCellFormat();
						Color color8 = Color.decode("0x006633");
						workbook.setColourRGB(Colour.ORANGE, color8.getRed(), color8
								.getGreen(), color8.getBlue());
						cellFormat8.setBackground(Colour.ORANGE);
						mapColors.put("006633", cellFormat8);
					}
					
					
					
					
					
					
					
					  if(finalJobOrder.size()>0){	
							 for(JobForTeacher finalJobOrderForGrid: finalJobOrder)
							  {	
								 col=1;
				
						label = new Label(0, k, finalJobOrderForGrid.getJobId().getDistrictMaster().getDistrictName()); 
						excelSheet.addCell(label);
				
						label = new Label(1, k, finalJobOrderForGrid.getSchoolMaster().getSchoolName()); 
						excelSheet.addCell(label);			 
					
					label = new Label(++col, k, finalJobOrderForGrid.getJobId().getJobTitle()); 
					excelSheet.addCell(label);
					
					
					label = new Label(++col, k, Utility.getUSformatDateTime(finalJobOrderForGrid.getJobId().getJobStartDate())); 
					excelSheet.addCell(label);
					
					
					label = new Label(++col, k, Utility.getUSformatDateTime(finalJobOrderForGrid.getJobId().getJobEndDate())); 
					excelSheet.addCell(label);
					
					
					label = new Label(++col, k, ""+count.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())); 
					excelSheet.addCell(label);
					
					if(countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=null)
					{
						label = new Label(++col, k, ""+countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())); 
						excelSheet.addCell(label);	
					}
					else
					{
						label = new Label(++col, k, "N/A"); 
						excelSheet.addCell(label);
					}
					
					
					 String colorName="";
					 int norScore= 0;
						if(countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=null)
                    	{
							try
							{
							norScore= normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())/countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId());
							}catch (Exception e) {
								
							}
                    	}
						if(allColor.containsKey(norScore))
						{							
							colorName=allColor.get(norScore);
						}
					try
					{

                    	if(normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=0 && countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=null)
                    	{
                    		label = new Label(++col, k, ""+normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())/countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId()));
        					excelSheet.addCell(label);	
                    	}
                    	else
                    	{
                    		label = new Label(++col, k, "N/A");
        					excelSheet.addCell(label);
                    	}	
					}catch (Exception e) {
						e.printStackTrace();
					}				
													
				   k++; 
			 }
			}
	
			workbook.write();
			workbook.close();
		}catch(Exception e){}
		
	 return fileName;
  }
	
  
	
	public String displayRecordsByEntityTypePDF(String districtName,String schoolName,int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String jobTitle,String expDate,String normScoreFilter,String posDate,String endPost)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport( districtName, schoolName, districtOrSchoolId, schoolId, noOfRow, pageNo, sortOrder, sortOrderType,jobTitle,expDate,normScoreFilter,posDate,endPost,basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	public boolean generateCandidatePDReport(String districtName,String schoolName,int districtOrSchoolId,Integer schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String jobTitle,String expDate,String normScoreFilter,String posDate,String endPost,String path,String realPath)
	{
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			int sortingcheck=1;
			try{
				boolean flagfordata=true;
				if(!districtName.trim().equals("") && districtOrSchoolId==0)
					flagfordata=false;
				else if(!schoolName.equals("") && schoolId==0)
					flagfordata=false;
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				
				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				 /** set default sorting fieldName **/
				String sortOrderFieldName="districtName";
				String sortOrderNoField="districtName";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("districtName"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("schoolName"))
					sortingcheck=2;
				else if(sortOrder.equalsIgnoreCase("jobTitle"))
					sortingcheck=3;
				else if(sortOrder.equalsIgnoreCase("posDate"))
					sortingcheck=4;
				else if(sortOrder.equalsIgnoreCase("expDate"))
					sortingcheck=5;
				else if(sortOrder.equalsIgnoreCase("noOfRecord"))
					sortingcheck=6;
				else if(sortOrder.equalsIgnoreCase("nepi"))
					sortingcheck=7;
				else if(sortOrder.equalsIgnoreCase("avgEPI")){
					sortingcheck=8;
				}else
					sortingcheck=9;
					
					/**Start set dynamic sorting fieldName **/
					
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<JobOrder> lstJoOrder =new ArrayList<JobOrder>();
					 List<JobForTeacher> lstJobForTeacher1=new ArrayList<JobForTeacher>();
					 List<JobOrder> lstSchoolInJobOrder = new ArrayList<JobOrder>();
					 SchoolMaster schoolId1=null;
								
					if(flagfordata)
					if((entityID==2)||(entityID==3))
					{		
						DistrictMaster distId=districtMasterDAO.findById(districtOrSchoolId, false, false);
						lstJoOrder=jobOrderDAO.getByDistrictID(distId,jobTitle,expDate,posDate,endPost);
						System.out.println("lstJoOrder::::::::::::::::::::"+lstJoOrder.size());
				    	 
					}
					else if(entityID==1) 
					{				
						DistrictMaster distId=districtMasterDAO.findById(districtOrSchoolId, false, false);
						lstJoOrder=jobOrderDAO.getByDistrictID(distId,jobTitle,expDate,posDate,endPost);
						totalRecord = lstJoOrder.size();
					}
								
					if(flagfordata)
					{
						if(schoolId!=null && schoolId!=0)
						{
							System.out.println("school iddddd::::"+Long.valueOf(schoolId.longValue()));
							 schoolId1=schoolMasterDAO.findById(Long.valueOf(schoolId.longValue()), false, false);
							lstSchoolInJobOrder=schoolInJobOrderDAO.allJobOrderBySchool(schoolId1);
							System.out.println("sizeee of lstSchoolInJobOrder:::"+lstSchoolInJobOrder.size());
							
							if((jobTitle!=null && !jobTitle.equals("")) && (expDate!=null && !expDate.equals("")) )
							{
								lstJoOrder.retainAll(lstSchoolInJobOrder);
							}
							else if(jobTitle!=null && !jobTitle.equals(""))
							{
								lstJoOrder.retainAll(lstSchoolInJobOrder);
							}
							else if(expDate!=null && !expDate.equals(""))
							{						
								lstJoOrder.retainAll(lstSchoolInJobOrder);
							}						
							else
							{
								lstJoOrder.clear();
								lstJoOrder=lstSchoolInJobOrder;
							}
						}
					}				
					
					List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();			
					List<TeacherDetail> teachers1= new ArrayList<TeacherDetail>();
					List<TeacherNormScore> listTeacherNormScores =new ArrayList<TeacherNormScore>();
					Map<Integer,TeacherNormScore> mapofNormScoresByTeacher= new HashMap<Integer, TeacherNormScore>();
					 Map<String, JobForTeacher> mapjob=new HashMap<String, JobForTeacher>();
					 Map<String, Integer> count=new HashMap<String, Integer>();
					 Map<String, Integer> normScoreMap=new HashMap<String, Integer>(); 
					 Map<String, JobOrder> jobForTeacherMapforSchool=new HashMap<String, JobOrder>();
					 Map<String, Integer> countForAvg=new HashMap<String, Integer>();
					
					 if(schoolId!=null && schoolId!=0)
					 if(lstJoOrder.size()>0)
					 {
						 for(JobOrder jobMap:lstJoOrder)
						 {	
						 jobForTeacherMapforSchool.put(schoolId1.getSchoolName()+"_"+jobMap.getJobId(), jobMap);
						
						 }			 
					 }
					 				 
					if(lstJoOrder.size()>0)
					 {
						lstJobForTeacher=jobForTeacherDAO.findJobByJobCategoryAndDistrict(lstJoOrder);
						System.out.println("lstJobForTeacher::::"+lstJobForTeacher.size());
					 }			
					
					if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
					{
					for(JobForTeacher jft : lstJobForTeacher){
								teachers1.add(jft.getTeacherId());
					}
					}
					
					if(teachers1!=null && teachers1.size()>0){
						listTeacherNormScores=teacherNormScoreDAO.findNormScoreByTeacherByList(teachers1);
						if(listTeacherNormScores!=null && listTeacherNormScores.size()>0){
							for (TeacherNormScore teacherNormScore : listTeacherNormScores) {
								mapofNormScoresByTeacher.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);								
							}
						}
					}
					
							 
					 if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
					 {
						 for(JobForTeacher lstjobmap: lstJobForTeacher)
						 {
							 if(lstjobmap.getSchoolMaster()!=null)
	                         {									
									 int total=1;
									 int normScore=0;
									 int totalAvg=1;
									 if(count.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())!=null)
									 {
										 total=count.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())+1;
										 count.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),total);
										 
										 if(mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId())!=null && normScoreMap.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())!=null)
										 {
											 if(countForAvg.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())!=null)
											 {
												 totalAvg=countForAvg.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())+1;
												 countForAvg.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),totalAvg); 
											 }
											 normScore=normScoreMap.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())+
										 			mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId()).getTeacherNormScore();
											 normScoreMap.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), normScore);
										 }									 
									 }
									 else
									 {
										 count.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),total);
										 										 									 
										 if(mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId())!=null)
										 {
											 countForAvg.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),totalAvg);
											 normScoreMap.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId()).getTeacherNormScore() );
										 }
										 else
											 normScoreMap.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), 0 );
										 
									 }		
									 if(schoolId!=null && schoolId!=0)
									 {
										 if(jobForTeacherMapforSchool.containsKey(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId()))
										 {
											 mapjob.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), lstjobmap); 
										 }
									 }
									 else	
									 {
										
										 lstjobmap.setSchoolName(lstjobmap.getSchoolMaster().getSchoolName());
										 mapjob.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), lstjobmap);
										 
									 }						
	                         }
						 }
					 }
					 
					List<JobForTeacher> finalJobOrder =new ArrayList<JobForTeacher>();	
					List<JobForTeacher> finalJobOrdersort =new ArrayList<JobForTeacher>();	
					List<JobForTeacher> finalJobOrdersortNorm =new ArrayList<JobForTeacher>();
				   List targetList = new ArrayList(mapjob.values());
				   
				   finalJobOrdersort=targetList;
				   
				   if(normScoreFilter!=null && !normScoreFilter.equals(""))
						  for(JobForTeacher finalJobOrderForGrid: finalJobOrdersort)
						  {
							  int norScore= normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())/count.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId());
							  if(normScoreFilter!=null && !normScoreFilter.equals(""))
								{								
									if(Integer.parseInt(normScoreFilter)==norScore)
									{
										finalJobOrdersortNorm.add(finalJobOrderForGrid);										
									}
									}
						  }			 
						  if(normScoreFilter!=null && !normScoreFilter.equals(""))
						  {
							  targetList.retainAll(finalJobOrdersortNorm);							  
						  }

					  
					if(sortOrder.equals("schoolName")){ 
						
						  for(JobForTeacher jf:finalJobOrdersort) 
							 {
						    	 jf.setSchoolName(jf.getSchoolMaster().getSchoolName());
							 }
				 		
					     if(sortOrderType.equals("0"))
						  Collections.sort(targetList, new SchoolNameCompratorASC());
						 else 
						  Collections.sort(targetList, new SchoolNameComratorDESC());
				     }
				  
					if(sortOrder.equals("jobTitle")){ 
						
						  for(JobForTeacher jf:finalJobOrdersort) 
							 {
							  	 jf.setJobTitle(jf.getJobId().getJobTitle());
							 }
				 		
					     if(sortOrderType.equals("0"))
						  Collections.sort(targetList, new JobTitleCompratorASC());
						 else 
						  Collections.sort(targetList, new JobTitleCompratorDESC());
				     } 
					
					if(sortOrder.equals("posDate")){  
					     for(JobForTeacher jf:finalJobOrdersort) 
						 {
					    	 jf.setPostDate(jf.getJobId().getJobStartDate());
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(targetList, new PostingDateCompratorASC());
						 else 
						  Collections.sort(targetList, new PostingDateCompratorDESC());
				     }
					
					if(sortOrder.equals("expDate")){  
					     for(JobForTeacher jf:finalJobOrdersort) 
						 {
					    	 jf.setEndDate(jf.getJobId().getJobEndDate());
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(targetList, new EndDateCompratorASC());
						 else 
						  Collections.sort(targetList, new EndDateCompratorDESC());
				     }
					
					if(sortOrder.equals("noOfRecord"))
					{ 
						for(JobForTeacher jft : finalJobOrdersort)
						{
						if(count.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null)
								{								
									 jft.setNoOfRecord(count.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId()));
								}				
						}					 	
					 	    if(sortOrderType.equals("0"))
							  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecord);
							 else 
							  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecordDesc);
				     }
					
					if(sortOrder.equals("nepi"))
					{ 
						for(JobForTeacher jft : finalJobOrdersort)
						{
						if(countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null)
								{								
									 jft.setNoOfRecord(countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId()));
								}				
						}					 	
					 	    if(sortOrderType.equals("0"))
							  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecord);
							 else 
							  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecordDesc);
				     }
					
					
					if(sortOrder.equals("avgEPI"))
					{ 
						for(JobForTeacher jft : finalJobOrdersort)
						{
							if(normScoreMap.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null && countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null)
							{	
								try
								{
						          jft.setEpiNormScoreAvg(normScoreMap.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())/countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId()));
								}catch (Exception e) {
									
								}
							}
					else
					{
						jft.setEpiNormScoreAvg(0);
					}				
						}					 	
					 	    if(sortOrderType.equals("0"))
							  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorEPINormScore);
							 else 
							  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorEPINormScoreDesc);
				     }
				   
				     finalJobOrder=targetList;
				     
			 // System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
				 String fontPath = realPath;
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject(Utility.getLocaleValuePropByKey("headMsgHiresBySchool", locale));
						document.addCreationDate();
						document.addTitle(Utility.getLocaleValuePropByKey("headMsgHiresBySchool", locale));

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						

						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("headMsgHiresBySchool", locale),font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						
						
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						document.add(new Phrase("\n"));


						float[] tblwidth={.16f,.15f,.15f,.08f,.10f,.10f,.10f,.10f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[8];
						cell = new PdfPCell[8];
				// header
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblDistrictName", locale),font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblSchoolName", locale),font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJoTil", locale),font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[2]);

						para[3] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblPostingDate", locale),font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[3]);
						
						para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblExpirationDate", locale),font10_10);
						cell[4]= new PdfPCell(para[4]);
						cell[4].setBackgroundColor(bluecolor);
						cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[4]);
						
						para[5] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblNumberOfHires", locale),font10_10);
						cell[5]= new PdfPCell(para[5]);
						cell[5].setBackgroundColor(bluecolor);
						cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[5]);
						
						para[6] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblN_EPI", locale),font10_10);
						cell[6]= new PdfPCell(para[6]);
						cell[6].setBackgroundColor(bluecolor);
						cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[6]);
						
						para[7] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblAvgEPINormScore", locale),font10_10);
						cell[7]= new PdfPCell(para[7]);
						cell[7].setBackgroundColor(bluecolor);
						cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[7]);
																		
						document.add(mainTable);
						
						if(finalJobOrder.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgNorecordfound", locale),font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
				     
						
						  if(finalJobOrder.size()>0){	
								 for(JobForTeacher finalJobOrderForGrid: finalJobOrder)
								  {
							  int index=0;
							 									
							  mainTable = new PdfPTable(tblwidth);
							  mainTable.setWidthPercentage(100);
							  para = new Paragraph[8];
							  cell = new PdfPCell[8];
							  
                                 para[index] = new Paragraph(finalJobOrderForGrid.getJobId().getDistrictMaster().getDistrictName(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 
								 para[index] = new Paragraph( finalJobOrderForGrid.getSchoolMaster().getSchoolName(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 							
								 para[index] = new Paragraph(finalJobOrderForGrid.getJobId().getJobTitle(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								
								 para[index] = new Paragraph(Utility.getUSformatDateTime(finalJobOrderForGrid.getJobId().getJobStartDate()),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 
								 para[index] = new Paragraph(Utility.getUSformatDateTime(finalJobOrderForGrid.getJobId().getJobEndDate()),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								
								 para[index] = new Paragraph(""+count.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId()),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 if(countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=null)
									{
								 para[index] = new Paragraph(""+countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId()),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
									}
								 else
								 {
									 para[index] = new Paragraph("N/A");
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++;
									 
								 }
								 
								
								
								 String colorName="0";
								 int norScore= 0;
									if(countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=null)
			                    	{
										try
										{
										norScore= normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())/countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId());
										}catch (Exception e) {
											
										}
			                    	}
								if(allColor.containsKey(norScore))
								{							
									colorName=allColor.get(norScore);							
								}
								
								try
								{
									if(normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=0 && countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=null)
									{
										 para[index] = new Paragraph(""+normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())/countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId()),font8bold);
										 cell[index]= new PdfPCell(para[index]);
										 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
										 cell[index].setBackgroundColor(new Color(Integer.parseInt(colorName ,16)));
										 mainTable.addCell(cell[index]);
										 index++;
									}
									else
									{
										 para[index] = new Paragraph("N/A");
										 cell[index]= new PdfPCell(para[index]);
										 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
										 //cell[index].setBackgroundColor(new Color(Integer.parseInt(colorName ,16)));
										 mainTable.addCell(cell[index]);
										 index++;
									}
									
								}catch (Exception e) {
									e.printStackTrace();
								}
								 
								
								 								 
								 document.add(mainTable);
				   }
				}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	
	public String displayRecordsByEntityTypePrintPreview(String districtName,String schoolName,int districtOrSchoolId,Integer schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String jobTitle,String expDate,String normScoreFilter,String posDate,String endPost)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
			boolean flagfordata=true;
			if(!districtName.trim().equals("") && districtOrSchoolId==0)
				flagfordata=false;
			else if(!schoolName.equals("") && schoolId==0)
				flagfordata=false;
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="districtName";
				String sortOrderNoField="districtName";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("districtName"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("schoolName"))
					sortingcheck=2;
				else if(sortOrder.equalsIgnoreCase("jobTitle"))
					sortingcheck=3;
				else if(sortOrder.equalsIgnoreCase("posDate"))
					sortingcheck=4;
				else if(sortOrder.equalsIgnoreCase("expDate"))
					sortingcheck=5;
				else if(sortOrder.equalsIgnoreCase("noOfRecord"))
					sortingcheck=6;
				else if(sortOrder.equalsIgnoreCase("nepi"))
					sortingcheck=7;
				else if(sortOrder.equalsIgnoreCase("avgEPI")){
					sortingcheck=7;
				}else
					sortingcheck=8;
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<JobOrder> lstJoOrder =new ArrayList<JobOrder>();
				 List<JobForTeacher> lstJobForTeacher1=new ArrayList<JobForTeacher>();
				 List<JobOrder> lstSchoolInJobOrder = new ArrayList<JobOrder>();
				 SchoolMaster schoolId1=null;
							
				if(flagfordata)
				if((entityID==2)||(entityID==3))
				{		
					DistrictMaster distId=districtMasterDAO.findById(districtOrSchoolId, false, false);
					lstJoOrder=jobOrderDAO.getByDistrictID(distId,jobTitle,expDate,posDate,endPost);
					System.out.println("lstJoOrder::::::::::::::::::::"+lstJoOrder.size());
			    	 
				}
				else if(entityID==1) 
				{				
					DistrictMaster distId=districtMasterDAO.findById(districtOrSchoolId, false, false);
					lstJoOrder=jobOrderDAO.getByDistrictID(distId,jobTitle,expDate,posDate,endPost);
					totalRecord = lstJoOrder.size();
				}
							
				if(flagfordata)
				{
					if(schoolId!=null && schoolId!=0)
					{
						System.out.println("school iddddd::::"+Long.valueOf(schoolId.longValue()));
						 schoolId1=schoolMasterDAO.findById(Long.valueOf(schoolId.longValue()), false, false);
						lstSchoolInJobOrder=schoolInJobOrderDAO.allJobOrderBySchool(schoolId1);
						System.out.println("sizeee of lstSchoolInJobOrder:::"+lstSchoolInJobOrder.size());
						
						if((jobTitle!=null && !jobTitle.equals("")) && (expDate!=null && !expDate.equals("")) )
						{
							lstJoOrder.retainAll(lstSchoolInJobOrder);
						}
						else if(jobTitle!=null && !jobTitle.equals(""))
						{
							lstJoOrder.retainAll(lstSchoolInJobOrder);
						}
						else if(expDate!=null && !expDate.equals(""))
						{						
							lstJoOrder.retainAll(lstSchoolInJobOrder);
						}						
						else
						{
							lstJoOrder.clear();
							lstJoOrder=lstSchoolInJobOrder;
						}
					}
				}				
				
				List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();			
				List<TeacherDetail> teachers1= new ArrayList<TeacherDetail>();
				List<TeacherNormScore> listTeacherNormScores =new ArrayList<TeacherNormScore>();
				Map<Integer,TeacherNormScore> mapofNormScoresByTeacher= new HashMap<Integer, TeacherNormScore>();
				 Map<String, JobForTeacher> mapjob=new HashMap<String, JobForTeacher>();
				 Map<String, Integer> count=new HashMap<String, Integer>();
				 Map<String, Integer> normScoreMap=new HashMap<String, Integer>(); 
				 Map<String, JobOrder> jobForTeacherMapforSchool=new HashMap<String, JobOrder>();
				 Map<String, Integer> countForAvg=new HashMap<String, Integer>();
				
				 if(schoolId!=null && schoolId!=0)
				 if(lstJoOrder.size()>0)
				 {
					 for(JobOrder jobMap:lstJoOrder)
					 {	
					 jobForTeacherMapforSchool.put(schoolId1.getSchoolName()+"_"+jobMap.getJobId(), jobMap);
					
					 }			 
				 }
				 				 
				if(lstJoOrder.size()>0)
				 {
					lstJobForTeacher=jobForTeacherDAO.findJobByJobCategoryAndDistrict(lstJoOrder);
					System.out.println("lstJobForTeacher::::"+lstJobForTeacher.size());
				 }			
				
				if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
				{
				for(JobForTeacher jft : lstJobForTeacher){
							teachers1.add(jft.getTeacherId());
				}
				}
				
				if(teachers1!=null && teachers1.size()>0){
					listTeacherNormScores=teacherNormScoreDAO.findNormScoreByTeacherByList(teachers1);
					if(listTeacherNormScores!=null && listTeacherNormScores.size()>0){
						for (TeacherNormScore teacherNormScore : listTeacherNormScores) {
							mapofNormScoresByTeacher.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);							
						}
					}
				}
				
						 
				 if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
				 {
					 for(JobForTeacher lstjobmap: lstJobForTeacher)
					 {
						 if(lstjobmap.getSchoolMaster()!=null)
                         {									
								 int total=1;
								 int normScore=0;
								 int totalAvg=1;
								 if(count.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())!=null)
								 {
									 total=count.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())+1;
									 count.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),total);
									 
									 if(mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId())!=null && normScoreMap.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())!=null)
									 {
										 if(countForAvg.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())!=null)
										 {
											 totalAvg=countForAvg.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())+1;
											 countForAvg.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),totalAvg); 
										 }
										 normScore=normScoreMap.get(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId())+
									 			mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId()).getTeacherNormScore();
										 normScoreMap.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), normScore);
									 }									 
								 }
								 else
								 {
									 count.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),total);
									 									 									 
									 if(mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId())!=null)
									 {
										 countForAvg.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(),totalAvg);
										 normScoreMap.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), mapofNormScoresByTeacher.get(lstjobmap.getTeacherId().getTeacherId()).getTeacherNormScore() );
									 }
									 else
										 normScoreMap.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), 0 );
									 
								 }		
								 if(schoolId!=null && schoolId!=0)
								 {
									 if(jobForTeacherMapforSchool.containsKey(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId()))
									 {
										 mapjob.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), lstjobmap); 
									 }
								 }
								 else	
								 {
									
									 lstjobmap.setSchoolName(lstjobmap.getSchoolMaster().getSchoolName());
									 mapjob.put(lstjobmap.getSchoolMaster().getSchoolName()+"_"+lstjobmap.getJobId().getJobId(), lstjobmap);
									 
								 }						
                         }
					 }
				 }
				
			
			   List targetList = new ArrayList(mapjob.values());
			   List<JobForTeacher> finalJobOrder =new ArrayList<JobForTeacher>();
			   List<JobForTeacher> finalJobOrdersort =new ArrayList<JobForTeacher>();
			   List<JobForTeacher> finalJobOrdersortNorm =new ArrayList<JobForTeacher>();
			   
			   finalJobOrdersort=targetList;
			   
			   if(normScoreFilter!=null && !normScoreFilter.equals(""))
					  for(JobForTeacher finalJobOrderForGrid: finalJobOrdersort)
					  {
						  int norScore= normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())/count.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId());
						  if(normScoreFilter!=null && !normScoreFilter.equals(""))
							{								
								if(Integer.parseInt(normScoreFilter)==norScore)
								{
									finalJobOrdersortNorm.add(finalJobOrderForGrid);								
								}
								}
					  }			 
					  if(normScoreFilter!=null && !normScoreFilter.equals(""))
					  {
						  targetList.retainAll(finalJobOrdersortNorm);						 
					  }
			   
				  
				if(sortOrder.equals("schoolName")){ 
					
					  for(JobForTeacher jf:finalJobOrdersort) 
						 {
					    	 jf.setSchoolName(jf.getSchoolMaster().getSchoolName());
						 }
			 		
				     if(sortOrderType.equals("0"))
					  Collections.sort(targetList, new SchoolNameCompratorASC());
					 else 
					  Collections.sort(targetList, new SchoolNameComratorDESC());
			     }
			  
				if(sortOrder.equals("jobTitle")){ 
					
					  for(JobForTeacher jf:finalJobOrdersort) 
						 {
						  	 jf.setJobTitle(jf.getJobId().getJobTitle());
						 }
			 		
				     if(sortOrderType.equals("0"))
					  Collections.sort(targetList, new JobTitleCompratorASC());
					 else 
					  Collections.sort(targetList, new JobTitleCompratorDESC());
			     } 
				
				if(sortOrder.equals("posDate")){  
				     for(JobForTeacher jf:finalJobOrdersort) 
					 {
				    	 jf.setPostDate(jf.getJobId().getJobStartDate());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(targetList, new PostingDateCompratorASC());
					 else 
					  Collections.sort(targetList, new PostingDateCompratorDESC());
			     }
				
				if(sortOrder.equals("expDate")){  
				     for(JobForTeacher jf:finalJobOrdersort) 
					 {
				    	 jf.setEndDate(jf.getJobId().getJobEndDate());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(targetList, new EndDateCompratorASC());
					 else 
					  Collections.sort(targetList, new EndDateCompratorDESC());
			     }
				
				if(sortOrder.equals("noOfRecord"))
				{ 
					for(JobForTeacher jft : finalJobOrdersort)
					{
					if(count.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null)
							{								
								 jft.setNoOfRecord(count.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId()));
							}				
					}					 	
				 	    if(sortOrderType.equals("0"))
						  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecord);
						 else 
						  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecordDesc);
			     }
				
				if(sortOrder.equals("nepi"))
				{ 
					for(JobForTeacher jft : finalJobOrdersort)
					{
					if(countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null)
							{								
								 jft.setNoOfRecord(countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId()));
							}				
					}					 	
				 	    if(sortOrderType.equals("0"))
						  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecord);
						 else 
						  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorNoOfRecordDesc);
			     }
				
				if(sortOrder.equals("avgEPI"))
				{ 
					for(JobForTeacher jft : finalJobOrdersort)
					{
						if(normScoreMap.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null && countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())!=null)
						{	
							try
							{
					          jft.setEpiNormScoreAvg(normScoreMap.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId())/countForAvg.get(jft.getSchoolMaster().getSchoolName()+"_"+jft.getJobId().getJobId()));
							}catch (Exception e) {
								
							}
						}
							else
							{
								jft.setEpiNormScoreAvg(0);
							}				
					}					 	
				 	    if(sortOrderType.equals("0"))
						  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorEPINormScore);
						 else 
						  Collections.sort(targetList, JobForTeacher.jobForTeacherComparatorEPINormScoreDesc);
			     }
			        finalJobOrder=targetList;		

	        tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>"+Utility.getLocaleValuePropByKey("headMsgHiresBySchool", locale)+"</div><br/>");
			
			tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");
			
			
			tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");		
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblDistrictName", locale)+"</th>");			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th>");    
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoTil", locale)+"</th>");			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblPostingDate", locale)+"</th>");			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblExpirationDate", locale)+"</th>");			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblNumberOfHires", locale)+"</th>");	
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblN_EPI", locale)+"</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblAvgEPINormScore", locale)+"</th>");
				
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(finalJobOrder.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			}		
			
			if(finalJobOrder.size()>0){	
				 for(JobForTeacher finalJobOrderForGrid: finalJobOrder)
				  {	
					 String colorName="";
					 String ccsName="";
					 int norScore= 0;
						if(countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=null)
                 	{
							try
							{
							norScore= normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())/countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId());
							}catch (Exception e) {
								
							}
                 	}
						if(allColor.containsKey(norScore))
						{							
							colorName=allColor.get(norScore);							
						}											
							ccsName="nobground2";	
					 
					 
					 tmRecords.append("<tr>");				
					 tmRecords.append("<td style='font-size:12px;'>"+finalJobOrderForGrid.getJobId().getDistrictMaster().getDistrictName()+"</td>");					
						tmRecords.append("<td style='font-size:12px;'>"+finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"</td>");			 
						tmRecords.append("<td style='font-size:12px;'>"+finalJobOrderForGrid.getJobId().getJobTitle()+"</td>");						
			      		tmRecords.append("<td style='font-size:12px;'>"+ Utility.getUSformatDateTime(finalJobOrderForGrid.getJobId().getJobStartDate())+"</td>");			      		
						tmRecords.append("<td style='font-size:12px;'>"+Utility.getUSformatDateTime(finalJobOrderForGrid.getJobId().getJobEndDate())+"</td>");						
						tmRecords.append("<td style='font-size:12px;'>"+count.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())+"</td>");
						
						 if(countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=null)
							{
						tmRecords.append("<td style='font-size:12px;'>"+countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())+"</td>");
							}
						 else
						 {
							 tmRecords.append("<td style='font-size:12px;'>N/A</td>"); 
						 }
						 try
		                    {
		                    	if(normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=0 && countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())!=null)
		                    	{
		                    		tmRecords.append("<td><span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>"+normScoreMap.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())/countForAvg.get(finalJobOrderForGrid.getSchoolMaster().getSchoolName()+"_"+finalJobOrderForGrid.getJobId().getJobId())+"</span></td>");	
		                    	}
		                    	else
		                    	{
		                    		tmRecords.append("<td><span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>N/A</span></td>");
		                    	}
		                    
		                    }
		                    catch(Exception e){e.printStackTrace();}																	
					   tmRecords.append("</tr>");
			   }
			}
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
  }
	
}


