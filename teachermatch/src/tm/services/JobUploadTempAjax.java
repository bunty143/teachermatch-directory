package tm.services;

/* @Author: Sekhar 
 * @Discription: It is used to Teacher Upload
 */
import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.stream.events.StartDocument;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.python.antlr.ast.Str;
import org.python.core.exceptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.BatchJobOrder;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobUploadTemp;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherSecondaryStatus;
import tm.bean.TeacherUploadTemp;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictNotes;
import tm.bean.master.DistrictSchools;
import tm.bean.master.DomainMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.ObjectiveMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobUploadTempDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherUploadTempDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.ManageJobOrdersAjax.MailSend;
import tm.utility.Utility;

public class JobUploadTempAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO)
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	@Autowired
	private TeacherUploadTempDAO teacherUploadTempDAO;
	public void setTeacherUploadTempDAO(TeacherUploadTempDAO teacherUploadTempDAO)
	{
		this.teacherUploadTempDAO = teacherUploadTempDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) 
	{
		this.userMasterDAO = userMasterDAO;
	}
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private JobUploadTempDAO jobUploadTempDAO;
	public void setJobUploadTempDAO(JobUploadTempDAO jobUploadTempDAO) {
		this.jobUploadTempDAO = jobUploadTempDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	public void setBranchMasterDAO(BranchMasterDAO branchMasterDAO) {
		this.branchMasterDAO = branchMasterDAO;
	}
	private Vector vectorDataExcelXLSX = new Vector();
	
	public String displayTempJobRecords(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			System.out.println("calling displayTempJobRecords ");
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,48,"importjobdetails.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			/***** Sorting by  Stating ( Sekhar ) ******/
			List<JobUploadTemp> jobUploadTemplst	  =	null;
			
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"jobtempid";
	
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				 {
					 sortOrderFieldName		=	sortOrder;
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion = Restrictions.eq("sessionid",session.getId());
			totalRecord =jobUploadTempDAO.getRowCountTempJob(criterion);
			jobUploadTemplst = jobUploadTempDAO.findByTempJob(sortOrderStrVal,start,noOfRowInPage,criterion);
			
			
			/***** Sorting by Temp Job  End ******/
			dmRecords.append("<table  id='tempJobTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblcretedDate", locale),sortOrderFieldName,"created_date",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobId", locale),sortOrderFieldName,"job_id",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTy", locale),sortOrderFieldName,"job_type",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblNm", locale),sortOrderFieldName,"name",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgJobStartDate", locale),sortOrderFieldName,"job_start_date",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgJobEndDate", locale),sortOrderFieldName,"job_end_date",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgNoExpHires", locale),sortOrderFieldName,"no_exp_hires",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSchoolName", locale),sortOrderFieldName,"school_name",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoStatus", locale),sortOrderFieldName,"job_status",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgErrors3", locale),sortOrderFieldName,"errortext",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(jobUploadTemplst.size()==0)
				dmRecords.append("<tr><td colspan='6' align='center'>"+Utility.getLocaleValuePropByKey("msgNoJobFound", locale)+"</td></tr>" );
			System.out.println("No of Records "+jobUploadTemplst.size());

			for (JobUploadTemp jobUploadTemp : jobUploadTemplst){
				String rowCss="";
				
				if(!jobUploadTemp.getErrortext().equalsIgnoreCase("")){
					rowCss="style=\"background-color:#FF0000;\"";
				}
				
				dmRecords.append("<tr >" );
				dmRecords.append("<td "+rowCss+">"+jobUploadTemp.getCreated_date()+"</td>");
				dmRecords.append("<td "+rowCss+">"+jobUploadTemp.getJob_id()+"</td>");
				dmRecords.append("<td "+rowCss+">"+jobUploadTemp.getJob_type()+"</td>");
				dmRecords.append("<td "+rowCss+">"+jobUploadTemp.getName()+"</td>");
				dmRecords.append("<td "+rowCss+">"+jobUploadTemp.getJob_start_date()+"</td>");
				dmRecords.append("<td "+rowCss+">"+jobUploadTemp.getJob_end_date()+"</td>");
				dmRecords.append("<td "+rowCss+">"+jobUploadTemp.getNo_exp_hires()+"</td>");
				dmRecords.append("<td "+rowCss+">"+jobUploadTemp.getSchool_name()+"</td>");
				dmRecords.append("<td "+rowCss+">"+jobUploadTemp.getJob_status()+"</td>");
				dmRecords.append("<td "+rowCss+">"+jobUploadTemp.getErrortext()+"</td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	public JobOrder apiJobIdExist(Map<Integer,JobOrder> jobOrderList,String apiJobId,Integer districtId){
		for(int i=0;i<jobOrderList.size();i++){
			if(jobOrderList.get(i).getApiJobId()!=null){
				if(jobOrderList.get(i).getApiJobId().equalsIgnoreCase(apiJobId) && jobOrderList.get(i).getDistrictMaster()!=null && jobOrderList.get(i).getDistrictMaster().getDistrictId().equals(districtId)){
					return jobOrderList.get(i);
				}
			}
		}
		return null;
	}
	public SchoolMaster findBySchoolObj(List<SchoolMaster> schoolMasterList,Long schoolId)
	{
		SchoolMaster schMaster = null;
		try 
		{
			for(SchoolMaster schoolMaster : schoolMasterList){
				if(schoolMaster.getSchoolId().equals(schoolId))
					schMaster=schoolMaster;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(schMaster==null)
			return null;
		else
			return schMaster;	
	}
	public JobCategoryMaster getjobCategoryMasterObj(List<JobCategoryMaster> jobCategoryMasterList,int categoryId)
	{
		JobCategoryMaster jobCateMaster = null;
		try 
		{
			for(JobCategoryMaster jobCategoryMaster : jobCategoryMasterList){
				if(jobCategoryMaster.getJobCategoryId()==categoryId)
					jobCateMaster=jobCategoryMaster;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(jobCateMaster==null)
			return null;
		else
			return jobCateMaster;	
	}
	
	
	@Transactional(readOnly=false)
	public int saveJob()
	{
		
		// ========  For Session time Out Error =========
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		int entityID=0,createdBy=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			entityID=userMaster.getEntityType();
			createdBy=userMaster.getUserId();
		}
		
		System.out.println("::::::::saveJob:::::::::");
		int returnVal=0;
        try{
        	Criterion criterion1 = Restrictions.eq("sessionid",session.getId());
        	Criterion criterion2 = Restrictions.eq("errortext","");
        	List <JobUploadTemp> jobUploadTemplst=jobUploadTempDAO.findByCriteria(criterion1,criterion2);
        	DistrictMaster districtMaster =null;
        	if(jobUploadTemplst!=null){
        		if(jobUploadTemplst.size()>0)
        			districtMaster=districtMasterDAO.findById(jobUploadTemplst.get(0).getDistrictId(), false, false);	
        	}
        	List<SchoolMaster> schoolMasterList = schoolMasterDAO.findDistrictSchoolId(districtMaster);
        	List<JobCategoryMaster> categoryMasterList = jobCategoryMasterDAO.findAllJobCategoryNameByOrder();
        	Map<Integer,JobOrder> jobOrderList = jobOrderDAO.findByAllJobOrderWithInActive(); 
        	
        	 SessionFactory sessionFactory=jobOrderDAO.getSessionFactory();
			 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
        	 Transaction txOpen =statelesSsession.beginTransaction();
        	for(JobUploadTemp jobUploadTemp : jobUploadTemplst){
        		
        		int jobId=0;
        		JobOrder jobOrder=apiJobIdExist(jobOrderList,jobUploadTemp.getJob_id(),jobUploadTemplst.get(0).getDistrictId());
        		if(jobOrder==null){
        			jobOrder=new JobOrder();	
        		}else{
        			jobId=jobOrder.getJobId();
        		}
        		int JobOrderType=3,jobStartDate=0;
        		boolean jobSchoolFlag=false,jobAddFlag=true;
        		SchoolMaster schoolMaster=null;
        		if(jobUploadTemp.getJob_type().equalsIgnoreCase("DJO") || jobUploadTemp.getJob_type().equalsIgnoreCase("BJO")){
    				JobOrderType=2;
    			}
        		if(jobUploadTemp.getSchoolId()!=null){
        			jobSchoolFlag=true;
        			schoolMaster= findBySchoolObj(schoolMasterList,Long.parseLong(jobUploadTemp.getSchoolId()+""));
        		}
        		if(!jobUploadTemp.getErrortext().equals("")){
        			jobAddFlag=false;
        		}
        		boolean jobEndDateValid=true;
        		if((Utility.isDateValidOnly(jobUploadTemp.getJob_end_date(),1)==2)){
        			jobEndDateValid=false;
        		}
        		try{
        			String prevDate=getTempFormatDate(Utility.getPrevCurrentDate());
        			if(jobId!=0){
	        			if(jobEndDateValid){
	        					String startDate=getTempFormatDate(jobOrder.getJobStartDate());
		        				if(Utility.isDateValidGtLt(jobUploadTemp.getJob_end_date(),startDate, 1, 1)!=1){
			        				jobStartDate=2;
			        			}else{
			        				jobStartDate=3;
			        			}
	        			}else{
	        				String startDate=getTempFormatDate(jobOrder.getJobStartDate());
	        				if(Utility.isDateValidGtLt(prevDate,startDate, 1, 1)!=1){
		        				jobStartDate=1;
		        			}
	        			}
        			}else{
        				if(Utility.isDateValidGtLt(prevDate, jobUploadTemp.getJob_start_date(), 1, 1)!=1){
	        				jobStartDate=1;
	        			}
        			}
    			}catch(Exception e){}
        		
    			boolean closedFlag=false;
        		
        		if(jobId==0 && jobAddFlag){
        			System.out.println("New Records insert now.");
	    			jobOrder.setDistrictMaster(districtMaster);
	    			jobOrder.setJobTitle(jobUploadTemp.getName());
	    			jobOrder.setWritePrivilegeToSchool(false);
	    			try{
	    				jobOrder.setCreatedDateTime(Utility.getCurrentDateFormart(jobUploadTemp.getCreated_date().replaceAll("/","-")));
	    			}catch(Exception e){}
	    			jobOrder.setJobStartDate(Utility.getCurrentDateFormart(jobUploadTemp.getJob_start_date().replaceAll("/","-")));
	    			jobOrder.setJobEndDate(Utility.getCurrentDateFormart(jobUploadTemp.getJob_end_date().replaceAll("/","-")));
	    			
	    			
	    			if(jobUploadTemp.getRead_write_previlege().equalsIgnoreCase("N")){
	    				jobOrder.setWritePrivilegeToSchool(false);
	    			}else if(jobUploadTemp.getRead_write_previlege().equalsIgnoreCase("Y")){
	    					jobOrder.setWritePrivilegeToSchool(true);
	    			}
	    			
	    			if(JobOrderType==2 && jobSchoolFlag==false){
	    				jobOrder.setAllSchoolsInDistrict(2);
	    				jobOrder.setAllGrades(2);
	    				jobOrder.setPkOffered(2);
	    				jobOrder.setKgOffered(2);
	    				jobOrder.setG01Offered(2);
	    				jobOrder.setG02Offered(2);
	    				jobOrder.setG03Offered(2);
	    				jobOrder.setG04Offered(2);
	    				jobOrder.setG05Offered(2);
	    				jobOrder.setG06Offered(2);
	    				jobOrder.setG07Offered(2);
	    				jobOrder.setG08Offered(2);
	    				jobOrder.setG09Offered(2);
	    				jobOrder.setG10Offered(2);
	    				jobOrder.setG11Offered(2);
	    				jobOrder.setG12Offered(2);
	    				jobOrder.setSelectedSchoolsInDistrict(2);
	    				jobOrder.setNoSchoolAttach(1);
	    				jobOrder.setNoOfExpHires(Integer.parseInt(jobUploadTemp.getNo_exp_hires()));
	    			}else {
	    				jobOrder.setAllGrades(2);
	    				jobOrder.setAllSchoolsInDistrict(2);
	    				jobOrder.setAllGrades(2);
	    				jobOrder.setPkOffered(2);
	    				jobOrder.setKgOffered(2);
	    				jobOrder.setG01Offered(2);
	    				jobOrder.setG02Offered(2);
	    				jobOrder.setG03Offered(2);
	    				jobOrder.setG04Offered(2);
	    				jobOrder.setG05Offered(2);
	    				jobOrder.setG06Offered(2);
	    				jobOrder.setG07Offered(2);
	    				jobOrder.setG08Offered(2);
	    				jobOrder.setG09Offered(2);
	    				jobOrder.setG10Offered(2);
	    				jobOrder.setG11Offered(2);
	    				jobOrder.setG12Offered(2);
	    				jobOrder.setSelectedSchoolsInDistrict(1);
	    				jobOrder.setNoSchoolAttach(2);
	    			}
	    			jobOrder.setIsJobAssessment(false);
	    			jobOrder.setJobAssessmentStatus(0);
	    			jobOrder.setOfferVirtualVideoInterview(false);
					jobOrder.setAssessmentDocument(null);
					jobOrder.setAttachNewPillar(2);
					jobOrder.setAttachDefaultDistrictPillar(2);
					jobOrder.setAttachDefaultSchoolPillar(2);
					jobOrder.setSendAutoVVILink(false);
	    			jobOrder.setCreatedBy(createdBy);
	    			jobOrder.setCreatedByEntity(entityID);
	    			if(JobOrderType==2){
	    				if(jobUploadTemp.getJob_type().equalsIgnoreCase("BJO"))
	    				{
	    					jobOrder.setCreatedForEntity(5);
	    					jobOrder.setBranchMaster(jobUploadTemp.getBranchMaster());
	    					jobOrder.setHeadQuarterMaster(jobUploadTemp.getBranchMaster().getHeadQuarterMaster());
	    				}
	    					
	    				else
	    					jobOrder.setCreatedForEntity(2);
	    			}else{
	    				jobOrder.setCreatedForEntity(3);
	    			}
					JobCategoryMaster jobCategoryMaster = getjobCategoryMasterObj(categoryMasterList,8);
					jobOrder.setJobCategoryMaster(jobCategoryMaster);
				
					if(JobOrderType==2){
						String dExitURL=null;
						if(districtMaster.getExitURL()!=null)
							dExitURL=districtMaster.getExitURL().trim();
						
						String dExitMessage=null;
						if(districtMaster.getExitMessage()!=null)
							dExitMessage=districtMaster.getExitMessage().trim();
						
						int exitURLMessageVal=1;
						if(districtMaster.getFlagForMessage()!=0){
							if(districtMaster.getFlagForMessage()==1){
								exitURLMessageVal=2;
							}
						}
						if(exitURLMessageVal==2){
							if(dExitMessage!=null){
								if(!dExitMessage.equalsIgnoreCase("")){
									jobOrder.setExitMessage(dExitMessage);
								}else{
									jobOrder.setExitMessage("");
								}
							}else{
								jobOrder.setExitMessage("");
							}
							jobOrder.setExitURL("");
		    				jobOrder.setFlagForURL(2);
		    				jobOrder.setFlagForMessage(1);
						}else{
							if(dExitURL!=null){
								if(!dExitURL.equalsIgnoreCase(""))
									jobOrder.setExitURL(dExitURL);
								else
									jobOrder.setExitURL("");
							}else{
								jobOrder.setExitURL("");
							}
							jobOrder.setExitMessage("");
		    				jobOrder.setFlagForURL(1);
		    				jobOrder.setFlagForMessage(2);
						}
						if(districtMaster.getIsPortfolioNeeded())
							jobOrder.setIsPortfolioNeeded(true);
						else
							jobOrder.setIsPortfolioNeeded(false);
					}else{
						String sExitURL=null;
						if(schoolMaster!=null && schoolMaster.getExitURL()!=null)
							sExitURL=schoolMaster.getExitURL().trim();
					
						String sExitMessage=null;
						if(schoolMaster!=null && schoolMaster.getExitMessage()!=null)
							sExitMessage=schoolMaster.getExitMessage().trim();
						
						int exitURLMessageVal=1;
						if(schoolMaster!=null && schoolMaster.getFlagForMessage()!=0){
							if(schoolMaster.getFlagForMessage()==1){
								exitURLMessageVal=2;
							}
						}
						
						
						
						if(exitURLMessageVal==2){
							if(sExitMessage!=null){
								if(!sExitMessage.equalsIgnoreCase("")){
									jobOrder.setExitMessage(sExitMessage);
								}else{
									jobOrder.setExitMessage("");
								}
							}else{
								jobOrder.setExitMessage("");
							}
							jobOrder.setExitURL("");
		    				jobOrder.setFlagForURL(2);
		    				jobOrder.setFlagForMessage(1);
						}else{
							if(sExitURL!=null){
								if(!sExitURL.equalsIgnoreCase("")){
									jobOrder.setExitURL(sExitURL);
								}else{
									jobOrder.setExitURL("");
								}
							}else{
								jobOrder.setExitURL("");
							}
							jobOrder.setExitMessage("");
		    				jobOrder.setFlagForURL(1);
		    				jobOrder.setFlagForMessage(2);
						}
						if(schoolMaster!=null && schoolMaster.getIsPortfolioNeeded())
							jobOrder.setIsPortfolioNeeded(true);
						else
							jobOrder.setIsPortfolioNeeded(false);
					}
					jobOrder.setApiJobId(jobUploadTemp.getJob_id());
					jobOrder.setJobStatus("O");
					jobOrder.setStatus("A");
					
					jobOrder.setIsPoolJob(0);
					jobOrder.setJobType("F");
					
					boolean approvalBeforeGoLive=districtMaster.getApprovalBeforeGoLive()==null?false:districtMaster.getApprovalBeforeGoLive();			
					if(approvalBeforeGoLive)
					{
						jobOrder.setApprovalBeforeGoLive(0);
					}
					else
					{
						jobOrder.setApprovalBeforeGoLive(1);
					}
					jobOrder.setIsInviteOnly(false);
					jobOrder.setIsVacancyJob(false);
					
					if(jobUploadTemp.getJob_status()!=null)
					if(jobUploadTemp.getJob_status().equalsIgnoreCase("closed")){
						closedFlag=true;
						if(jobStartDate==1){
		        			jobOrder.setJobStartDate(Utility.getPrevCurrentDateWithFormart());
						}
		        		jobOrder.setJobEndDate(Utility.getPrevCurrentDateWithFormart());
						
            		}
					//jobOrderDAO.makePersistent(jobOrder);
					jobOrder.setNotificationToschool(true);
					statelesSsession.insert(jobOrder);
        		}else if(JobOrderType==2){
        			System.out.println("Update Records insert now..");
	        			try{
	        			String currentDate=getTempFormatDate(new Date());
	        			String endDate=getTempFormatDate(jobOrder.getJobEndDate());
	        			int closeDateChk=Utility.isDateValidGtLt(currentDate,endDate, 0,1);
	        			
	        			/*System.out.println("currentDate:::::"+currentDate);
	        			System.out.println("endDate:::::"+endDate);
	        			System.out.println(":::::::::::::"+closeDateChk);
	        			System.out.println(":::::::::::::"+jobStartDate);*/
	        			
	        			jobOrder.setSelectedSchoolsInDistrict(1);
	    				jobOrder.setNoSchoolAttach(2);
	    				if(closeDateChk!=0){
		    				if(jobUploadTemp.getJob_status()!=null)
		    				if(jobUploadTemp.getJob_status().equalsIgnoreCase("closed")){
		    					closedFlag=true;
		    					if(jobStartDate==1){
				        			jobOrder.setJobStartDate(Utility.getPrevCurrentDateWithFormart());
				        			jobOrder.setJobEndDate(Utility.getPrevCurrentDateWithFormart());
								}else if(jobStartDate==2){
									jobOrder.setJobStartDate(Utility.getCurrentDateFormart(jobUploadTemp.getJob_end_date().replaceAll("/","-")));
									jobOrder.setJobEndDate(Utility.getCurrentDateFormart(jobUploadTemp.getJob_end_date().replaceAll("/","-")));
								}else if(jobStartDate==3){
									jobOrder.setJobEndDate(Utility.getCurrentDateFormart(jobUploadTemp.getJob_end_date().replaceAll("/","-")));
								}else{
									jobOrder.setJobEndDate(Utility.getPrevCurrentDateWithFormart());
								}
		            		}
	    				}
	    				statelesSsession.update(jobOrder);
        			}catch(Exception e){
        				e.printStackTrace();
        			}
        		}else if(jobId!=0 && jobAddFlag){
        			try{
	        			String currentDate=getTempFormatDate(new Date());
	        			String endDate=getTempFormatDate(jobOrder.getJobEndDate());
	        			int closeDateChk=Utility.isDateValidGtLt(currentDate,endDate, 0,1);
	        			
	        		/*	System.out.println("currentDate:::::"+currentDate);
	        			System.out.println("endDate:::::"+endDate);
	        			System.out.println(":::::::::::::"+closeDateChk);*/
	        			
	        			if(closeDateChk!=0){
		        			if(jobUploadTemp.getJob_status()!=null)
		        			if(jobUploadTemp.getJob_status().equalsIgnoreCase("closed")){
			        			closedFlag=true;
			        			System.out.println("Update Only Close.");
			        			if(jobStartDate==1){
				        			jobOrder.setJobStartDate(Utility.getPrevCurrentDateWithFormart());
				        			jobOrder.setJobEndDate(Utility.getPrevCurrentDateWithFormart());
								}else if(jobStartDate==2){
									jobOrder.setJobStartDate(Utility.getCurrentDateFormart(jobUploadTemp.getJob_end_date().replaceAll("/","-")));
									jobOrder.setJobEndDate(Utility.getCurrentDateFormart(jobUploadTemp.getJob_end_date().replaceAll("/","-")));
								}else if(jobStartDate==3){
									jobOrder.setJobEndDate(Utility.getCurrentDateFormart(jobUploadTemp.getJob_end_date().replaceAll("/","-")));
								}else{
									jobOrder.setJobEndDate(Utility.getPrevCurrentDateWithFormart());
								}
								statelesSsession.update(jobOrder);
		        			}
	        			}
        			}catch(Exception e){
        				e.printStackTrace();
        			}
        		}
        		
				if(jobSchoolFlag  && jobAddFlag && ((closedFlag && jobId==0) || (closedFlag==false && jobAddFlag))){
					System.out.println("School in Job add");
					SchoolInJobOrder schoolInJobOrder= new SchoolInJobOrder();
					schoolInJobOrder.setJobId(jobOrder);
					schoolInJobOrder.setSchoolId(schoolMaster);
					schoolInJobOrder.setNoOfSchoolExpHires(Integer.parseInt(jobUploadTemp.getNo_exp_hires()));
					schoolInJobOrder.setCreatedDateTime(new Date());
					statelesSsession.insert(schoolInJobOrder);
					//schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
				}
        	}
        	txOpen.commit();
     	 	statelesSsession.close();
        	deleteXlsAndXlsxFile(request,session.getId());
        	jobUploadTempDAO.deleteAllJobTemp(session.getId());
		}catch (Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	public List<DistrictMaster> getFieldOfDistrictList(String DistrictName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		Criterion criterion1 = Restrictions.eq("status","A");
		try{
			
			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);
				
				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);
				
				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
				
			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25,criterion1);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return districtMasterList;
	}
	
	public void deleteXlsAndXlsxFile(HttpServletRequest request,String sessionId){
		List<TeacherUploadTemp> teacherUploadTemplst= teacherUploadTempDAO.findByAllTempTeacher();
		String root = request.getRealPath("/")+"/uploadjob/";
		for(TeacherUploadTemp teacherUploadTemp : teacherUploadTemplst){
			String filePath="";
			File file=null;
			try{
				filePath=root+teacherUploadTemp.getSessionid()+".xlsx";
				file = new File(filePath);
				file.delete();
			}catch(Exception e){}
			try{
				filePath=root+teacherUploadTemp.getSessionid()+".xls";
				file = new File(filePath);
				file.delete();
			}catch(Exception e){}
		}
		
		String filePath="";
		File file=null;
		try{
			filePath=root+sessionId+".xlsx";
			file = new File(filePath);
			file.delete();
		}catch(Exception e){}
		try{
			filePath=root+sessionId+".xls";
			file = new File(filePath);
			file.delete();
		}catch(Exception e){}
	}
	
	public int sameJobApiIdExistInJobOrder(Map<Integer,JobOrder> jobOrderlst,String jobApiId,int  districtId,SchoolMaster schoolMaster,String jobType) 
	{
		int createdForEntity=3;
		boolean apiIdFlag=false,createdForEntityFlag=false,districtIdFlag=false;
		boolean dJOFlag=false;
		if(jobType.equalsIgnoreCase("DJO") || jobType.equalsIgnoreCase("BJO")){
			createdForEntity=2;
		}
		boolean jobError=false;
		int jobApiFlag=0;
		for(int i=0;i<jobOrderlst.size();i++){
			//boolean apiIdFlag=false,createdForEntityFlag=false,districtIdFlag=false;
			apiIdFlag=false;
			createdForEntityFlag=false;
			districtIdFlag=false;
			if(jobOrderlst.get(i).getApiJobId()!=null){
				if(jobOrderlst.get(i).getApiJobId().equals(jobApiId)){
					apiIdFlag=true;
				}
			}
			if(jobOrderlst.get(i).getCreatedForEntity()!=null){
				if(jobOrderlst.get(i).getCreatedForEntity().equals(createdForEntity))
				createdForEntityFlag=true;
			}
			
			if(jobOrderlst.get(i).getDistrictMaster()!=null && jobOrderlst.get(i).getDistrictMaster().getDistrictId()==districtId){
				districtIdFlag=true;
			}
			if(apiIdFlag && createdForEntityFlag && districtIdFlag){
				if(createdForEntity==2){
					 dJOFlag=false;
					if(schoolMaster==null){
						return 1;
					}else{
						List<SchoolMaster> schoolMasters=jobOrderlst.get(i).getSchool();
						for(SchoolMaster schmaster: schoolMasters){
							if(schmaster.equals(schoolMaster)){
								return 1;
							}
						}
					}
					if(dJOFlag==false){
						return 1;
					}
				}else{
					if(jobOrderlst.get(i).getSchool().get(0).equals(schoolMaster)){
						return 1;
					}else{
						return 3;
					}
				}
			}else{
				
				if(apiIdFlag && districtIdFlag==false){
					jobError=true;
					jobApiFlag=0;
					return 0;
				}
				if(apiIdFlag && !createdForEntityFlag){
					jobApiFlag=4;
					return 4;
				}
				if(apiIdFlag==false){
					jobApiFlag=0;
				}
			}
		}
		return jobApiFlag;
	}
	public int jobApiIdExistInJobOrder(Map<Integer,JobOrder> jobOrderlst,String jobApiId,int districtId) 
	{
		int apiIdFlag=0;
		for(int i=0;i<jobOrderlst.size();i++){
			if(jobOrderlst.get(i).getApiJobId()!=null){
				if(jobOrderlst.get(i).getApiJobId().equals(jobApiId) && jobOrderlst.get(i).getDistrictMaster().getDistrictId()==districtId){
					apiIdFlag=1;
					return 1;
				}
				if(jobOrderlst.get(i).getApiJobId().equals(jobApiId) && jobOrderlst.get(i).getDistrictMaster().getDistrictId()!=districtId){
					apiIdFlag=2;
				}
				
			}
		}
		return apiIdFlag;
	}
	public boolean findNoOfExpHiresAndSchool(Map<Integer,SchoolInJobOrder> schoolInJobOrderList,JobOrder jobOrder,String no_exp_hires){
		boolean noOfExpHiresFlag=false;
		try{
			for(int i=0;i<schoolInJobOrderList.size();i++){
				if(schoolInJobOrderList.get(i).getJobId().equals(jobOrder) && schoolInJobOrderList.get(i).getNoOfSchoolExpHires()==Integer.parseInt(no_exp_hires)){
					noOfExpHiresFlag=true;
				}
			}
		}catch(Exception e){}
		return noOfExpHiresFlag;
	}
	public boolean sameRecordsExistInSInJobOrder(int schoolExistFlag,Map<Integer,SchoolInJobOrder> schoolInJobOrderList,Map<Integer,JobOrder> jobOrderlst,String created_date,String job_id,String job_type,String name,String job_start_date,String job_end_date,String no_exp_hires,String school_name,String read_write_previlege,Integer districtId) throws ParseException{
		boolean recordExist=false;
		boolean districtFlag=false;
		int createdForEntity=3;
		if(job_type.equalsIgnoreCase("DJO")){
			createdForEntity=2;
		}
		for(int i=0;i<jobOrderlst.size();i++){
			if(!job_id.equalsIgnoreCase("")){
				String apiJobId="", noOfExpHires="";
				boolean schoolFlag=false,noOfExpHiresFlag=false,createdDateFlag=false,jobStartDateFlag=false,jobEndDateFlag=false;
				if(jobOrderlst.get(i).getApiJobId()!=null){
					apiJobId=jobOrderlst.get(i).getApiJobId();
					if(apiJobId.equalsIgnoreCase(job_id)){
						try{
							if(!created_date.equalsIgnoreCase("") && jobOrderlst.get(i).getCreatedDateTime()!=null)
							if(Utility.compareToDateFormart(created_date,jobOrderlst.get(i).getCreatedDateTime()+"")){
								createdDateFlag=true;
							}
						}catch(Exception e){}
						try{
							if(Utility.compareToDateFormart(job_start_date,jobOrderlst.get(i).getJobStartDate()+"")){
								jobStartDateFlag=true;
							}
						}catch(Exception e){}
						try{
							if(Utility.compareToDateFormart(job_end_date,jobOrderlst.get(i).getJobEndDate()+"")){
								jobEndDateFlag=true;
							}
						}catch(Exception e){}
						if(createdForEntity==2){
							if(jobOrderlst.get(i).getNoOfExpHires()!=null){
								noOfExpHires=jobOrderlst.get(i).getNoOfExpHires()+"";
								if(noOfExpHires.equalsIgnoreCase(no_exp_hires)){
									noOfExpHiresFlag=true;
								}
							}
							if(school_name.equalsIgnoreCase("")){
								schoolFlag=true;
							}else{
								List<SchoolMaster> schoolMasters=jobOrderlst.get(i).getSchool();
								for(SchoolMaster schmaster: schoolMasters){
									if(schmaster.getSchoolName().equalsIgnoreCase(school_name)){
										schoolFlag=true;
										noOfExpHiresFlag=findNoOfExpHiresAndSchool(schoolInJobOrderList,jobOrderlst.get(i),no_exp_hires);
									}
								}
							}
						}else{
							noOfExpHiresFlag=findNoOfExpHiresAndSchool(schoolInJobOrderList,jobOrderlst.get(i),no_exp_hires);
							List<SchoolMaster> schoolMasters=jobOrderlst.get(i).getSchool();
							for(SchoolMaster schmaster: schoolMasters){
								if(schmaster.getSchoolName().equalsIgnoreCase(school_name)){
									schoolFlag=true;
								}
							}
						}
						
						if(jobOrderlst.get(i)!=null && jobOrderlst.get(i).getDistrictMaster()!=null && jobOrderlst.get(i).getDistrictMaster().getDistrictId().equals(districtId)){
							districtFlag = true;
						}
						if(schoolExistFlag==0){
							
							if(createdDateFlag && jobEndDateFlag && jobStartDateFlag && noOfExpHiresFlag && apiJobId.equalsIgnoreCase(job_id) && jobOrderlst.get(i).getCreatedForEntity()==createdForEntity && jobOrderlst.get(i).getJobTitle().equalsIgnoreCase(name) && districtFlag){
								recordExist=true;
							}
						}else{
							if(createdDateFlag && jobEndDateFlag && jobStartDateFlag && noOfExpHiresFlag && schoolFlag && apiJobId.equalsIgnoreCase(job_id) && jobOrderlst.get(i).getCreatedForEntity()==createdForEntity && jobOrderlst.get(i).getJobTitle().equalsIgnoreCase(name) && districtFlag){
								recordExist=true;
							}
						}
					}
				}
			}
		}
		return recordExist;
	}
	public boolean sameRecordsExistInSession(String jobType,Map<Integer,JobUploadTemp> jobUploadTempMap,String sessionId,String created_date,String job_id,String job_type,String name,String job_start_date,String job_end_date,String no_exp_hires,String school_name,String read_write_previlege){
		boolean recordExist=false;
		for(int i=0;i<jobUploadTempMap.size();i++){
			if(!job_id.equalsIgnoreCase("")){
				if(job_type.equalsIgnoreCase("DJO")){
					if(jobUploadTempMap.get(i).getSessionid().equalsIgnoreCase(sessionId) && jobUploadTempMap.get(i).getJob_id().equalsIgnoreCase(job_id) && jobUploadTempMap.get(i).getCreated_date().equalsIgnoreCase(created_date) && jobUploadTempMap.get(i).getJob_type().equalsIgnoreCase(job_type) && jobUploadTempMap.get(i).getName().equalsIgnoreCase(name) && jobUploadTempMap.get(i).getJob_start_date().equalsIgnoreCase(job_start_date) && jobUploadTempMap.get(i).getJob_end_date().equalsIgnoreCase(job_end_date) && jobUploadTempMap.get(i).getNo_exp_hires().equalsIgnoreCase(no_exp_hires) && jobUploadTempMap.get(i).getSchool_name().equalsIgnoreCase(school_name) && jobUploadTempMap.get(i).getRead_write_previlege().equalsIgnoreCase(read_write_previlege) ){
						recordExist=true;
					}
				}else{
					if(jobUploadTempMap.get(i).getSessionid().equalsIgnoreCase(sessionId) && jobUploadTempMap.get(i).getJob_id().equalsIgnoreCase(job_id) && jobUploadTempMap.get(i).getCreated_date().equalsIgnoreCase(created_date) && jobUploadTempMap.get(i).getJob_type().equalsIgnoreCase(job_type) && jobUploadTempMap.get(i).getName().equalsIgnoreCase(name) && jobUploadTempMap.get(i).getJob_start_date().equalsIgnoreCase(job_start_date) && jobUploadTempMap.get(i).getJob_end_date().equalsIgnoreCase(job_end_date) && jobUploadTempMap.get(i).getNo_exp_hires().equalsIgnoreCase(no_exp_hires)){
						recordExist=true;
					}
				}
			}
		}
		return recordExist;
	}
	public boolean notOlderDate(String createdDate){
		if(createdDate!=null){
			try{
				String date[]=createdDate.split("/");
				if(Integer.parseInt(date[2])>=1971){
					return false;
				}
			}catch(Exception e){}
		}
		
		return true;
	}
	public SchoolMaster findBySchoolNameWithDistrictId(List<SchoolMaster> schoolMasterList,String schoolName,DistrictMaster districtMaster)
	{
		SchoolMaster schMaster = null;
		try 
		{
			if(schoolMasterList!=null)
			for(SchoolMaster schoolMaster : schoolMasterList){
				if(schoolMaster.getSchoolName().equalsIgnoreCase(schoolName) && schoolMaster.getDistrictId().equals(districtMaster) && schoolMaster.getStatus().equalsIgnoreCase("A")){
					schMaster=schoolMaster;
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(schMaster==null)
			return null;
		else
			return schMaster;	
	}
	public String saveJobTemp(String fileName,String sessionId,int districtId,int branchId)
	{
		try
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			System.out.println("::::::::saveJobTemp:::::::::"+districtId);
			String returnVal="";
	        try{
	        	 String root = request.getRealPath("/")+"/uploadjob/";
	        	 String filePath=root+fileName;
	        	 System.out.println("filePath :"+filePath);
	        	 if(fileName.contains(".xlsx")){
	        		 vectorDataExcelXLSX = readDataExcelXLSX(filePath);
	        	 }else{
	        		 vectorDataExcelXLSX = readDataExcelXLS(filePath);
	        	 }
	        	int created_date_count=11;
	 	        int job_id_count=11;
	 	        int job_type_count=11;
	 	        int name_count=11;
	 	        int job_start_date_count=11;
	 	        int job_end_date_count=11;
	 	        int no_exp_hires_count=11;
	 	        int school_name_count=11;
	 	        int job_status_count=11;
	 	        int read_write_previlege_count=11;
	 	        DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
	 	        BranchMaster branchMaster=null;
	 	        if(branchId!=0){
	 	        	branchMaster=branchMasterDAO.findById(branchId, false, false);
	 	        }
	 	        String districtName="";
	 	        if(districtMaster!=null && districtMaster.getDistrictName()!=null)
	 	        	districtName=districtMaster.getDistrictName();
	 	        
	 	        SchoolMaster schoolMaster=null;
	 	        Map<Integer,JobOrder> jobOrderList = jobOrderDAO.findByAllJobOrderWithInActive();
	 	        Map<Integer,SchoolInJobOrder> schoolInJobOrderList = new HashMap<Integer, SchoolInJobOrder>();
	 	        // if(branchId==0)
	 	        	//schoolInJobOrderList= schoolInJobOrderDAO.findByAllSchoolInJobOrder(); 
	 	       List<SchoolMaster> schoolMasterList = null;
	 	      if(branchId==0)
	 	    	  schoolMasterList=schoolMasterDAO.findDistrictSchoolId(districtMaster);
	 	        Map<Integer,JobUploadTemp> jobUploadTempMap = new HashMap<Integer, JobUploadTemp>();
	 	        
	 	        SessionFactory sessionFactory=jobUploadTempDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	      	    Transaction txOpen =statelesSsession.beginTransaction();
	 	        int mapCount=0;
	 	        String job_id_store="";
				 for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
					
		            Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
		            String created_date="";
		  	        String job_id="";
		  	        String job_type="";
		  	        String name="";
		  	        String job_start_date="";
		  	        String job_end_date="";
		  	        String no_exp_hires="";
		  	        String school_name="";
		  	        String job_status="";
		  	        String read_write_previlege="";
					
		            for(int j=0; j<vectorCellEachRowData.size(); j++) {
		            	
		            	try{
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("created_date")){
			            		created_date_count=j;
			            	}
			            	if(created_date_count==j)
			            	{
			            		created_date=vectorCellEachRowData.get(j).toString().trim();
			            	}
			            		
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("job_id")){
			            		job_id_count=j;
			            	}
			            	if(job_id_count==j)
			            		job_id=vectorCellEachRowData.get(j).toString().trim();
		
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("job_type")){
			            		job_type_count=j;
			            	}
			            	if(job_type_count==j)
			            		job_type=vectorCellEachRowData.get(j).toString().trim();
			            		
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("name")){
			            		name_count=j;
			            	}
			            	if(name_count==j)
			            		name=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("job_start_date")){
			            		job_start_date_count=j;
			            	}
			            	if(job_start_date_count==j)
			            		job_start_date=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("job_end_date")){
			            		job_end_date_count=j;
			            	}
			            	if(job_end_date_count==j)
			            		job_end_date=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	if(job_end_date.trim().equals(""))
			            		job_end_date="12/12/2099";
			            	
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("no_exp_hires")){
			            		no_exp_hires_count=j;
			            	}
			            	if(no_exp_hires_count==j)
			            		no_exp_hires=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	if(no_exp_hires.trim().equals(""))
			            		no_exp_hires="1";
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("school_name")){
			            		school_name_count=j;
			            	}
			            	if(school_name_count==j)
			            		school_name=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("job_status")){
			            		job_status_count=j;
			            	}
			            	if(job_status_count==j)
			            		job_status=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("read_write_previlege")){
			            		read_write_previlege_count=j;
			            	}
			            	if(read_write_previlege_count==j)
			            		read_write_previlege=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	
		            	}catch(Exception e){e.printStackTrace();}
		            }
		            
		            if(i!=0){
		            	
			             if(returnVal.equals("1") && (!job_id.equalsIgnoreCase("") || !job_type.equalsIgnoreCase("")  || !name.equalsIgnoreCase("")  || !job_start_date.equalsIgnoreCase("")  || !job_end_date.equalsIgnoreCase("")  || !no_exp_hires.equalsIgnoreCase("") )){
		            	 	JobUploadTemp jobUploadTemp= new JobUploadTemp();
		            	 	 boolean errorFlag=false,jobStartDay=false,createdDate=false,jobFlag=false;
		            	 	 String currentDate=getTempFormatDate(new Date());
		            	 	 int sameJobApiIdExistInJobOrder=11;
		            	 	 int schoolFlag=0,jobType=0,jobStatusFlag=0;
		            	 	try{
		            	 		jobUploadTemp.setCreated_date(monthReplace(created_date));
		            	 	}catch(Exception e){}
		            	 	
		            	 	try{
		            	 		if(branchId!=0 && job_start_date.trim().equals(""))
			            	 	{
			            	 		job_start_date=Utility.getFormatDate(Utility.getCurrentDate());
			            	 	}
		            	 		jobUploadTemp.setJob_start_date(monthReplace(job_start_date));
		            	 	}catch(Exception e){}
		            	 	
		            	 	try{
		            	 		if(branchId!=0 && job_end_date.trim().equals(""))
			            	 	{
		            	 			job_end_date="12/12/2099";
			            	 	}
		            	 		jobUploadTemp.setJob_end_date(monthReplace(job_end_date));
		            	 	}catch(Exception e){}
		            	 	
		            	 	String errorText="";
		            	 	jobUploadTemp.setExist_jobId(0);
		            	 	String schoolName="";
		            	 	
		            	 	if(job_type.equalsIgnoreCase("")||job_type.equalsIgnoreCase(" ")){
		            	 		jobType=1;
		            	 	}else if(!job_type.equalsIgnoreCase("DJO") && !job_type.equalsIgnoreCase("SJO") 
		            	 			&& !job_type.equalsIgnoreCase("BJO") ){
		            	  		jobType=2;
		            	 	}
		            	 	
		            	 	if((school_name.equalsIgnoreCase("")||school_name.equalsIgnoreCase(" "))&& jobType==0){
		            			if(job_type.equalsIgnoreCase("SJO")){
			               	 		schoolFlag=1;
		            			}
		            	 	}else{
	            	 			 schoolMaster=findBySchoolNameWithDistrictId(schoolMasterList,school_name,districtMaster);
	            	 			 if(schoolMaster==null){
			              	 		schoolFlag=2;
	            	 			 }else{
	            	 				if(schoolMaster.getSchoolName()!=null)
	            	 					schoolName=schoolMaster.getSchoolName();
	            	 				jobUploadTemp.setSchoolId(Integer.parseInt(schoolMaster.getSchoolId()+""));
	            	 			 }
		            	 	}
		            	 	
		            	 	int jobStatus=0;
		            	 	if(!job_status.equalsIgnoreCase("closed") && !job_status.equals("")){
		            	 		jobStatusFlag=1;
		            	 		jobStatus=1;
		            	 	}else if(job_status.equalsIgnoreCase("closed")){
		            	 		jobStatusFlag=2;
		            	 	}
		            	 	int job_id_error=0;
		            	 	if(job_id.equalsIgnoreCase("")||job_id.equalsIgnoreCase(" ")){
		            	 		job_id_error=1;
		               	 	}else{
		            	 		if(jobType==0 && schoolFlag==0){
		            	 			sameJobApiIdExistInJobOrder=sameJobApiIdExistInJobOrder(jobOrderList,job_id,districtId,schoolMaster,job_type);
		            	 			System.out.println("sameJobApiIdExistInJobOrder:::::"+sameJobApiIdExistInJobOrder);
		            	 			if(sameJobApiIdExistInJobOrder==2){
		            	 				job_id_error=2;
		            	 			}else if(sameJobApiIdExistInJobOrder==3){
		            	 				job_id_error=3;
		            	 			}else if(sameJobApiIdExistInJobOrder==4){
		            	 				job_id_error=4;
		            	 			}
		            	 		}
		            	 		if(sameJobApiIdExistInJobOrder==1 && jobStatusFlag==2){
		            	 			jobStatusFlag=3;
		            	 		}
		            	 		
		            	 	}
		            	 	if(jobStatus==1 || (jobStatus==0 && jobStatusFlag==2 && sameJobApiIdExistInJobOrder!=0)){
	            	 			jobStatusFlag=3;
	            	 		}
		            	 	if(!created_date.equalsIgnoreCase("") && jobStatusFlag!=3){
		            	 		if(Utility.isDateValid(jobUploadTemp.getCreated_date(),1)==0){
		            	 			errorText+=Utility.getLocaleValuePropByKey("msgCreatedDate3", locale);
			            	 		errorFlag=true;
			            	 		createdDate=true;
		            	 		}else if(Utility.isDateValid(jobUploadTemp.getCreated_date(),1)==2){
		            	 			errorText+=Utility.getLocaleValuePropByKey("msgCreatedDateValid", locale);
			            	 		errorFlag=true;
			            	 		createdDate=true;
		            	 		}else if(notOlderDate(jobUploadTemp.getCreated_date())){
		            	 			errorText+=Utility.getLocaleValuePropByKey("msgCreatedDate1971", locale);
			            	 		errorFlag=true;
			            	 		createdDate=true;
		            	 		}
		            	 		currentDate=jobUploadTemp.getCreated_date();
		            	 	}
		            	 	if(job_id_error==1 && jobStatus==0){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobIdNotAvailable", locale);
		            	 		errorFlag=true;
		            	 		jobFlag=true;
			             	}else if(job_id_error==2  && jobStatus==0){
			             		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobBelongTo", locale)+" "+districtName+".";
		            	 		errorFlag=true;
		            	 		jobFlag=true;
			             	}else if(job_id_error==3  && jobStatus==0){
			             		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobBelongTo", locale)+" "+schoolName+".";
		            	 		errorFlag=true;
		            	 		jobFlag=true;
			             	}else if(job_id_error==4  && jobStatus==0){
			             		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobTypeWrong", locale);
		            	 		errorFlag=true;
		            	 		jobFlag=true;
			             	}
		            	 	
		            		if(jobType==1  && jobStatus==0){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobTypeWrong", locale);
		            	 		errorFlag=true;
		            	 	}else if(jobType==2  && jobStatus==0){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobTypeValid", locale);
		            	 		errorFlag=true;
		            	 	}
		            	 	
		            	 	if((name.equalsIgnoreCase("")||name.equalsIgnoreCase(" "))  && jobStatusFlag!=3){
		            	 		if(errorFlag){
		            	 			errorText+=" </br> ";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgNameAvailable", locale);
		            	 		errorFlag=true;
		            	 	}
		            	 	if(branchId!=0 && job_start_date.trim().equals(""))
		            	 	{
		            	 		job_start_date=Utility.getFormatDate(Utility.getCurrentDate());
		            	 	}
		            	 	if((job_start_date.equalsIgnoreCase("")||job_start_date.equalsIgnoreCase(" ")) && jobStatusFlag!=3){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobStartDateAvailable", locale);
		            	 		errorFlag=true;
		            	 		jobStartDay=true;
		            	 	}else if((Utility.isDateValidOnly(jobUploadTemp.getJob_start_date(),1)==2) && jobStatusFlag!=3){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
	            	 			errorText+=Utility.getLocaleValuePropByKey("msgJobStartDateValid", locale);
		            	 		errorFlag=true;
		            	 		jobStartDay=true;
		            	 	}else if((!createdDate && Utility.isDateValidGtLt(currentDate, jobUploadTemp.getJob_start_date(), 0, 1)!=1) && jobStatusFlag!=3){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobStartDate", locale);
		            	 		errorFlag=true;
		            	 	}
		            		if((job_end_date.equalsIgnoreCase("")||job_end_date.equalsIgnoreCase(" ")) && jobStatusFlag!=3){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobEndDateAvailable", locale);
		            	 		errorFlag=true;
		            	 	}else if((Utility.isDateValidOnly(jobUploadTemp.getJob_end_date(),1)==2) && jobStatusFlag!=3){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}	
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobEndDateValid", locale);
			            	 	errorFlag=true;
		            	 	}else if((jobStartDay==false && Utility.isDateValidGtLt(jobUploadTemp.getJob_start_date(), jobUploadTemp.getJob_end_date(), 0, 1)!=1) && jobStatusFlag!=3){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobStartDate3", locale);
		            	 		errorFlag=true;
		            	 	}
		            		if((branchId==0 && no_exp_hires.trim().equalsIgnoreCase("")) && jobStatusFlag!=3){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgNoExpHiresNotValid", locale);
		            	 		errorFlag=true;
		            	 	}else if(branchId==0 && jobStatusFlag!=3){
		            	 		try{
		            	 			Integer.parseInt(no_exp_hires);
		            	 		}catch(Exception e){
		            	 			if(errorFlag){
			            	 			errorText+=" </br>";	
			            	 		}
			            	 		errorText+=Utility.getLocaleValuePropByKey("msgNoExpHiresNotValid", locale);
			            	 		errorFlag=true;
		            	 		}
		            	 	}
		            		
		            		if(job_type.equalsIgnoreCase("DJO")){
		            			if(read_write_previlege.equalsIgnoreCase("Y")){
		            				if(school_name.equalsIgnoreCase("")){
		            					errorText+=Utility.getLocaleValuePropByKey("msgAttachedDistrict3", locale);
		    	            	 		errorFlag=true;
		            				}
		            			}
		            		}
		            		
		            		if(job_type.equalsIgnoreCase("SJO")){
		            			if(school_name.equalsIgnoreCase("")){
	            					errorText+=Utility.getLocaleValuePropByKey("msgSchoolNameNotAvailable", locale);
	    	            	 		errorFlag=true;
	            				}else if(!school_name.equalsIgnoreCase("") && read_write_previlege.equalsIgnoreCase("Y")){
	            					errorText+=Utility.getLocaleValuePropByKey("msgSchoolJobOrder", locale);
	    	            	 		errorFlag=true;
	            				}
		            		}
		            		try{
			            		if(school_name!=null && !school_name.equalsIgnoreCase("") && schoolMaster==null){
		            					errorText+=Utility.getLocaleValuePropByKey("msgSchoolNameNotValid", locale);
		    	            	 		errorFlag=true;
			            		}
		            		}catch(Exception e){
		            			e.printStackTrace();
		            		}
		            		
		            		/*if(schoolFlag==1){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+="School Name is not available.";
		            	 		errorFlag=true;
		            	 	}else if(schoolFlag==2){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+="School Name is not valid.";
		            	 		errorFlag=true;
		            	 	}*/
		            		
		            		
		            		if(jobStatus==1){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobStatusNotValid", locale);
		            	 		errorFlag=true;
		            	 	}
		            		
		            		
		            		try{
		            	 		jobUploadTemp.setJob_start_date(monthReplace(job_start_date));
		            	 	}catch(Exception e){}
		            	 	
		            	 	try{
		            	 		jobUploadTemp.setJob_end_date(monthReplace(job_end_date));
		            	 	}catch(Exception e){}
		            		
		            		if(!errorFlag){
		            			boolean existJobIdInSession=false;
		            			if(job_id_store.contains("||"+job_id+"||")){
		            				existJobIdInSession=true;
		            			}
		            			
		            			boolean sameRecordsExistInSession=sameRecordsExistInSession(job_type,jobUploadTempMap,session.getId(),currentDate,job_id,job_type,name,jobUploadTemp.getJob_start_date(),jobUploadTemp.getJob_end_date(),no_exp_hires,school_name,read_write_previlege);
		            			boolean sameRecordsExistNoSInSInJobOrder=false;
		            			boolean sameRecordsExistWithSInSInJobOrder=false;
		            			if(branchId==0)
		            			{
		            				sameRecordsExistNoSInSInJobOrder=sameRecordsExistInSInJobOrder(0,schoolInJobOrderList,jobOrderList,currentDate,job_id,job_type,name,jobUploadTemp.getJob_start_date(),jobUploadTemp.getJob_end_date(),no_exp_hires,school_name,read_write_previlege,districtId);
			            			sameRecordsExistWithSInSInJobOrder=sameRecordsExistInSInJobOrder(1,schoolInJobOrderList,jobOrderList,currentDate,job_id,job_type,name,jobUploadTemp.getJob_start_date(),jobUploadTemp.getJob_end_date(),no_exp_hires,school_name,read_write_previlege,districtId);
		            			}
		            				
		            			System.out.println("sameRecordsExistInSession:"+sameRecordsExistInSession);
		            			System.out.println("sameRecordsExistWithSInSInJobOrder:"+sameRecordsExistWithSInSInJobOrder);
			            			if((sameRecordsExistInSession && existJobIdInSession && jobStatusFlag!=3) || (sameRecordsExistWithSInSInJobOrder  && jobStatusFlag!=3)){
			            				if(errorFlag){
		    	            	 			errorText+=" </br>";	
		    	            	 		}
		    	            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobIdAlreadyExist", locale);
		    	            	 		errorFlag=true;
			            			}else if(sameJobApiIdExistInJobOrder==1 && sameRecordsExistNoSInSInJobOrder==false  && jobStatusFlag!=3){
			            				if(errorFlag){
		    	            	 			errorText+=" </br>";	
		    	            	 		}
		    	            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobIdAlreadyRegistered", locale);
		    	            	 		errorFlag=true;
			            			}else if(!sameRecordsExistInSession && existJobIdInSession && sameRecordsExistNoSInSInJobOrder==false  && jobStatusFlag!=3){
			            				if(errorFlag){
		    	            	 			errorText+=" </br>";	
		    	            	 		}
		    	            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobIdAlreadyExist", locale);
		    	            	 		errorFlag=true;
			            			}
		            		}
		            		job_id_store+="||"+job_id+"||";
		            		jobUploadTemp.setBranchMaster(branchMaster);
		            		jobUploadTemp.setDistrictId(districtId);
		            	 	jobUploadTemp.setJob_id(job_id);
		            	 	jobUploadTemp.setJob_type(job_type);
		            	 	jobUploadTemp.setName(name);
		            	 	jobUploadTemp.setJob_status(job_status);
		            	 	if((branchId!=0 && no_exp_hires.trim().equalsIgnoreCase(""))){
		            	 		no_exp_hires="0";
		            	 	}
		            	 	jobUploadTemp.setNo_exp_hires(no_exp_hires);
		            	 	jobUploadTemp.setSchool_name(school_name);
		            	 	jobUploadTemp.setRead_write_previlege(read_write_previlege);
		            	 	jobUploadTemp.setSessionid(sessionId);
		            	 	
		            	 	if(!errorText.equalsIgnoreCase("") && !errorText.equalsIgnoreCase(null)){
		            	 		jobUploadTemp.setErrortext(errorText);
		            	 	}else{
		            	 		jobUploadTemp.setErrortext("");
		            	 	}
		            	 	
		            	 	jobUploadTempMap.put(new Integer(""+mapCount),jobUploadTemp);
		            	 	mapCount++;
		            	 	//jobUploadTempDAO.makePersistent(jobUploadTemp);
		            	 	statelesSsession.insert(jobUploadTemp);
			             }
		            }else{
		            	if(branchId==0)
		            	{

			            	 if(job_id_count!=11 && job_type_count!=11 && name_count!=11 && job_start_date_count!=11 && job_end_date_count!=11 && no_exp_hires_count!=11){
			            		 returnVal="1"; 
			            		 jobUploadTempDAO.deleteJobTemp(sessionId);
			            	 }else{
			            		 returnVal="";
			            		 boolean fieldVal=false;
				            	 if(job_id_count==11){
				            		 returnVal+=" job_id";
				            		 fieldVal=true;
				            	 }
				            	 if(job_type_count==11){
				            		 if(fieldVal){
				            			 returnVal+=",";
				            		 }
				            		 fieldVal=true;
				            		 returnVal+=" job_type";
				            	 }
				            	 if(name_count==11){
				            		 if(fieldVal){
				            			 returnVal+=",";
				            		 }
				            		 fieldVal=true;
				            		 returnVal+=" name";
				            	 }
				            	 if(job_start_date_count==11){
				            		 if(fieldVal){
				            			 returnVal+=",";
				            		 }
				            		 fieldVal=true;
				            		 returnVal+=" job_start_date";
				            	 }
				            	 
				            	 if(job_end_date_count==11){
				            		 if(fieldVal){
				            			 returnVal+=",";
				            		 }
				            		 fieldVal=true;
				            		 returnVal+=" job_end_date";
				            	 }
				            	 
				            	 if(no_exp_hires_count==11){
				            		 if(fieldVal){
				            			 returnVal+=",";
				            		 }
				            		 fieldVal=true;
				            		 returnVal+=" no_exp_hires";
				            	 }
				            	 
			            	 }
			            
		            	}
		            	else
		            	{


			            	 if(job_id_count!=11 && job_type_count!=11 && name_count!=11 && job_start_date_count!=11 && job_end_date_count!=11 ){
			            		 returnVal="1"; 
			            		 jobUploadTempDAO.deleteJobTemp(sessionId);
			            	 }else{
			            		 returnVal="";
			            		 boolean fieldVal=false;
				            	 if(job_id_count==11){
				            		 returnVal+=" job_id";
				            		 fieldVal=true;
				            	 }
				            	 if(job_type_count==11){
				            		 if(fieldVal){
				            			 returnVal+=",";
				            		 }
				            		 fieldVal=true;
				            		 returnVal+=" job_type";
				            	 }
				            	 if(name_count==11){
				            		 if(fieldVal){
				            			 returnVal+=",";
				            		 }
				            		 fieldVal=true;
				            		 returnVal+=" name";
				            	 }
				            	 if(job_start_date_count==11){
				            		 if(fieldVal){
				            			 returnVal+=",";
				            		 }
				            		 fieldVal=true;
				            		 returnVal+=" job_start_date";
				            	 }
				            	 
				            	 if(job_end_date_count==11){
				            		 if(fieldVal){
				            			 returnVal+=",";
				            		 }
				            		 fieldVal=true;
				            		 returnVal+=" job_end_date";
				            	 }
				            	 
			            	 }
			            
		            	
		            	}
		            }
			     }
				 txOpen.commit();
	     	 	 statelesSsession.close();
			}catch (Exception e){
				e.printStackTrace();
			}
			
			return returnVal;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("------------------------------------------");
		return "";
		/* ========  For Session time Out Error =========*/
		
	}
	public static Vector readDataExcelXLS(String fileName) throws IOException {

		System.out.println(":::::::::::::::::readDataExcel XLS::::::::::::");
		Vector vectorData = new Vector();
		
		try{
			InputStream ExcelFileToRead = new FileInputStream(fileName);
			HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
	 
			HSSFSheet sheet=wb.getSheetAt(0);
			HSSFRow row; 
			HSSFCell cell;
	 
			Iterator rows = sheet.rowIterator();
			int created_date_count=11;
	        int job_id_count=11;
	        int job_type_count=11;
	        int name_count=11;
	        int job_start_date_count=11;
	        int job_end_date_count=11;
	        int no_exp_hires_count=11;
	        int school_name_count=11;
	        int job_status_count=11;
	        String indexCheck="";
			 
			while (rows.hasNext()){
				row=(HSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,dateFlag=false;
				String indexPrev="";
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
				while (cells.hasNext())
				{
					cell=(HSSFCell) cells.next();
					cIndex=cell.getColumnIndex();
					if(cell.toString().equalsIgnoreCase("created_date")){
						created_date_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(created_date_count==cIndex){
						cellFlag=true;
						dateFlag=true;
					}	
					  
					if(cell.toString().equalsIgnoreCase("job_id")){
						job_id_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(job_id_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().equalsIgnoreCase("job_type")){
						job_type_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(job_type_count==cIndex){
						cellFlag=true;
					}
					
					
					if(cell.toString().equalsIgnoreCase("name")){
						name_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(name_count==cIndex){
						cellFlag=true;
					}
					
					
					if(cell.toString().equalsIgnoreCase("job_start_date")){
						job_start_date_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(job_start_date_count==cIndex){
						cellFlag=true;
						dateFlag=true;
					}
					
					
					if(cell.toString().equalsIgnoreCase("job_end_date")){
						job_end_date_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(job_end_date_count==cIndex){
						cellFlag=true;
						dateFlag=true;
					}
					
					
					if(cell.toString().equalsIgnoreCase("no_exp_hires")){
						no_exp_hires_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(no_exp_hires_count==cIndex){
						cellFlag=true;
					}
					
					
					if(cell.toString().equalsIgnoreCase("school_name")){
						school_name_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(school_name_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().equalsIgnoreCase("job_status")){
						job_status_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(job_status_count==cIndex){
						cellFlag=true;
					}
					
					if(cellFlag){
						if(!dateFlag){
							try{
								mapCell.put(Integer.valueOf(cIndex),cell.getStringCellValue());
							}catch(Exception e){
								mapCell.put(Integer.valueOf(cIndex),new Double(cell.getNumericCellValue()).longValue()+"");
							}
						}else{
							mapCell.put(Integer.valueOf(cIndex),cell+"");
						}
					}
		        	cellFlag=false;
		        	dateFlag=false;
					
				}
			    vectorCellEachRowData=cellValuePopulate(mapCell);
				vectorData.addElement(vectorCellEachRowData);
			}
		}catch(Exception e){}
		return vectorData;
    }
	public static Vector readDataExcelXLSX(String fileName) {
		System.out.println(":::::::::::::::::readDataExcel XLSX ::::::::::::");
        Vector vectorData = new Vector();
        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            XSSFWorkbook xssfWorkBook = new XSSFWorkbook(fileInputStream);
            // Read data at sheet 0
            XSSFSheet xssfSheet = xssfWorkBook.getSheetAt(0);
            Iterator rowIteration = xssfSheet.rowIterator();
            int created_date_count=11;
            int job_id_count=11;
            int job_type_count=11;
            int name_count=11;
            int job_start_date_count=11;
            int job_end_date_count=11;
	        int no_exp_hires_count=11;
	        int school_name_count=11;
	        int job_status_count=11;
	        int read_write_previlege_count=11;
            // Looping every row at sheet 0
            String indexCheck="";
            while (rowIteration.hasNext()) {
                XSSFRow xssfRow = (XSSFRow) rowIteration.next();
                Iterator cellIteration = xssfRow.cellIterator();
                Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,dateFlag=false;
				String indexPrev="";
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
                // Looping every cell in each row at sheet 0
                while (cellIteration.hasNext()){
                    XSSFCell xssfCell = (XSSFCell) cellIteration.next();
                    cIndex=xssfCell.getColumnIndex();
                    
                    if(xssfCell.toString().equalsIgnoreCase("created_date")){
                    	created_date_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(created_date_count==cIndex){
	            		cellFlag=true;
	            		dateFlag=true;
	            	}	
	            	  
	            	if(xssfCell.toString().equalsIgnoreCase("job_id")){
	            		job_id_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(job_id_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("job_type")){
	            		job_type_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(job_type_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("name")){
	            		name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(name_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("job_start_date")){
	            		job_start_date_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(job_start_date_count==cIndex){
	            		cellFlag=true;
	            		dateFlag=true;
	            	}
	            	
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("job_end_date")){
	            		job_end_date_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(job_end_date_count==cIndex){
	            		cellFlag=true;
	            		dateFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("no_exp_hires")){
	            		no_exp_hires_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(no_exp_hires_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("school_name")){
	            		school_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(school_name_count==cIndex){
	            		cellFlag=true;
	            	}
	            	if(xssfCell.toString().equalsIgnoreCase("job_status")){
						job_status_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(job_status_count==cIndex){
						cellFlag=true;
					}
					
					if(xssfCell.toString().equalsIgnoreCase("read_write_previlege")){
						read_write_previlege_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(read_write_previlege_count==cIndex){
						cellFlag=true;
					}
					
	            	if(cellFlag){
	            		if(!dateFlag){
	            			try{
	            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getStringCellValue());
	            			}catch(Exception e){
	            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getRawValue()+"");
	            			}
	            		}else{
	            			mapCell.put(Integer.valueOf(cIndex),xssfCell+"");
	            		}
	            	}
	            	dateFlag=false;
	            	cellFlag=false;
                }
                vectorCellEachRowData=cellValuePopulate(mapCell);
                vectorData.addElement(vectorCellEachRowData);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
         
        return vectorData;
    }
	public static Vector cellValuePopulate(Map<Integer,String> mapCell){
		 Vector vectorCellEachRowData = new Vector();
		 Map<Integer,String> mapCellTemp = new TreeMap<Integer, String>();
		 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false;
		 for(Map.Entry<Integer, String> entry : mapCell.entrySet()){
			int key=entry.getKey();
			String cellValue=null;
			if(entry.getValue()!=null)
				cellValue=entry.getValue().trim();
			
			if(key==0){
				mapCellTemp.put(key, cellValue);
				flag0=true;
			}
			if(key==1){
				flag1=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==2){
				flag2=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==3){
				flag3=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==4){
				flag4=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==5){
				flag5=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==6){
				flag6=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==7){
				flag7=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==8){
				flag8=true;
				mapCellTemp.put(key, cellValue);
			}
		 }
		 if(flag0==false){
			 mapCellTemp.put(0, "");
		 }
		 if(flag1==false){
			 mapCellTemp.put(1, "");
		 }
		 if(flag2==false){
			 mapCellTemp.put(2, "");
		 }
		 if(flag3==false){
			 mapCellTemp.put(3, "");
		 }
		 if(flag4==false){
			 mapCellTemp.put(4, "");
		 }
		 if(flag5==false){
			 mapCellTemp.put(5, "");
		 }
		 if(flag6==false){
			 mapCellTemp.put(6, "");
		 }
		 if(flag7==false){
			 mapCellTemp.put(7, "");
		 }
		 if(flag8==false){
			 mapCellTemp.put(8, "");
		 }
		 for(Map.Entry<Integer, String> entry : mapCellTemp.entrySet()){
			 vectorCellEachRowData.addElement(entry.getValue());
		 }
		 return vectorCellEachRowData;
	}
	
	public static String monthReplace(String date){
		String currentDate="";
		try{
			
		   if(date.contains("Jan")){
			   currentDate=date.replaceAll("Jan","01");
		   }
		   if(date.contains("Feb")){
			   currentDate=date.replaceAll("Feb","02");
		   }
		   if(date.contains("Mar")){
			   currentDate=date.replaceAll("Mar","03");
		   }
		   if(date.contains("Apr")){
			   currentDate=date.replaceAll("Apr","04");
		   }
		   if(date.contains("May")){
			   currentDate=date.replaceAll("May","05");
		   }
		   if(date.contains("Jun")){
			   currentDate=date.replaceAll("Jun","06");
		   }
		   if(date.contains("Jul")){
			   currentDate=date.replaceAll("Jul","07");
		   }
		   if(date.contains("Aug")){
			   currentDate=date.replaceAll("Aug","08");
		   }
		   if(date.contains("Sep")){
			   currentDate=date.replaceAll("Sep","09");
		   }
		   if(date.contains("Oct")){
			   currentDate=date.replaceAll("Oct","10");
		   }
		   if(date.contains("Nov")){
			   currentDate=date.replaceAll("Nov","11");
		   }
		   if(date.contains("Dec")){
			   currentDate=date.replaceAll("Dec","12");
		   }
		   String uDate=currentDate.replaceAll("-","/");
		   if(Utility.isDateValid(uDate,0)==2){
			   currentDate=date; 
		   }else{
			   try{
				   String dateSplit[]=uDate.split("/");
				   currentDate=dateSplit[1]+"/"+dateSplit[0]+"/"+dateSplit[2];
			   }catch(Exception e){
				   currentDate=date; 
			   }
		   }
		}catch(Exception e){
			currentDate=date;
		}
		   return currentDate;
	 }
	public static String getTempFormatDate(Date dat)
	{
		try 
		{
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			return sdf.format(dat);
		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
			return "";
		}
	}
	
}

