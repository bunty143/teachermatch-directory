package tm.services.feedback;

import java.io.File;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.user.UserMaster;
import tm.dao.TeacherPersonalInfoDAO;
import tm.services.EmailerService;
import tm.services.FeedbackAndSupportService;
import tm.services.social.SocialService;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class FeedBackUploadServlet extends HttpServlet 
{

    String locale = Utility.getValueOfPropByKey("locale");
    
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println(">>>>>>> File Upload for feedback >>>>>>>>>>");

		HttpSession session = request.getSession(false);
		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");  

		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			//throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.hideFeedBack('3');");
			pw.print("</script>");
			return;
		}

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List uploadedItems = null;
		FileItem fileItem = null;
		String ext="",filePath="";
		String msgSubject="";
		String msg="";
		String reqType="";
		String feedbackmsg="";
		String visitedPageURL="";
		String visitedPageTitle = "";
		String f1="";
		String email="";
		String phone="";
		String starRating="";
		String username = "";
		String userrole = "";
		String domainQuest		=	"";
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		if(userMaster!=null)
		{
			email = userMaster.getEmailAddress();
			username = userMaster.getFirstName()+" "+userMaster.getLastName();
			userrole = userMaster.getRoleId().getRoleName();
			phone = userMaster.getMobileNumber()==null?"None":(userMaster.getMobileNumber().equals("")==true?"None":userMaster.getMobileNumber());
		}
		else
		{
			email = teacherDetail.getEmailAddress();
			username = teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
			userrole = "Teacher";
		}

		System.out.println("hhh: "+email);

		try{
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			String fileName="";

			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if(fileItem.getFieldName().equals("reqType")){
					reqType=fileItem.getString();
				}
				if(fileItem.getFieldName().equals("msgSubject")){
					msgSubject=fileItem.getString("UTF-8").trim();
				}
				if(fileItem.getFieldName().equals("msg")){
					msg=fileItem.getString("UTF-8").trim();
				}
				if(fileItem.getFieldName().equals("feedbackmsg")){
					feedbackmsg=fileItem.getString("UTF-8").trim();
				}
				if(fileItem.getFieldName().equals("starRating")){
					starRating=fileItem.getString();
				}
				if(fileItem.getFieldName().equals("visitedPageURL")){
					visitedPageURL=fileItem.getString();
				}
				if(fileItem.getFieldName().equals("f1")){
					f1=fileItem.getString();
				}
				if(fileItem.getFieldName().equals("visitedPageTitle")){
					visitedPageTitle=fileItem.getString();
				}
				if(fileItem.getFieldName().equals("domainQuest")){
					domainQuest=fileItem.getString("UTF-8").trim();
				}
			}
			//msgSubject = new String (msgSubject.getBytes ("iso-8859-1"), "UTF-8");
			//feedbackmsg = new String (feedbackmsg.getBytes ("iso-8859-1"), "UTF-8");
			//msg = new String (msg.getBytes ("iso-8859-1"), "UTF-8");
			/*	System.out.println("starRating: "+starRating);
			System.out.println("msgSubject::::::::: "+msgSubject);
			System.out.println("msg::::::::: "+msg);
			System.out.println("feedbackmsg::::::::: "+feedbackmsg);
			System.out.println("reqType::::::::: "+reqType);
			System.out.println("visitedPageURL::::::::: "+visitedPageURL);
			System.out.println("visitedPageTitle::::::::: "+visitedPageTitle);*/
			//System.out.println("f1::::::::: "+f1);

			if(reqType.equalsIgnoreCase("1") && !f1.equals(""))
			{
				filePath=Utility.getValueOfPropByKey("feedbackRootPath")+email+"/";
				System.out.println("filePath=======>>"+filePath);

				File f=new File(filePath);
				if(!f.exists())
					f.mkdirs();


				if (fileItem.isFormField() == false) 
				{
					if (fileItem.getSize() > 0)	
					{
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						//fileName="Inventory"+dateTime+ext;
						fileName="FeedBack"+ext;
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
						System.out.println("fileName="+fileName);
					}
				}
				fileItem=null;				
			}

			ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
			Object myDao = context.getBean("myMailSender");
			EmailerService emailerService = (EmailerService)myDao;
			FeedbackAndSupportService feedbackAndSupportService = (FeedbackAndSupportService)context.getBean("feedbackAndSupportService");

			if(userMaster==null)
			{
				TeacherPersonalInfoDAO teacherPersonalInfoDAO = (TeacherPersonalInfoDAO)context.getBean("teacherPersonalInfoDAO");
				TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);

				String teacherMobile = "0";
				String teacherPhone = "0";
				
				if(teacherPersonalInfo!=null)
				{
					teacherMobile = teacherPersonalInfo.getMobileNumber();
					teacherPhone = teacherPersonalInfo.getPhoneNumber();
				}
				if(teacherMobile.equals("0") && teacherPhone.equals("0"))
					phone="None";
				else if(!teacherMobile.equals("0"))
					phone=teacherMobile;
				else
					phone=teacherPhone;
			}
			String to="";
			if(reqType.equalsIgnoreCase("1"))
			{
				to=Utility.getValueOfPropByKey("supportEmailId");
				String supportmsg="<table style='font-family: Tahoma;font-size: 16px;'><tr><td>" +
				 Utility.getLocaleValuePropByKey("msgSuppMsgRecv", locale)+" "+username+", on "+Utility.getIndiaFormatDateAndTime(new Date())+"<br>"+
				 Utility.getLocaleValuePropByKey("msgUserEmail", locale)+": "+email+"<br>"+
				 Utility.getLocaleValuePropByKey("msgUserPhone", locale)+": "+phone+"<br>"+
				 Utility.getLocaleValuePropByKey("lblRole", locale)+": "+userrole+"<br>"+
				 Utility.getLocaleValuePropByKey("msgPageTitle", locale)+": "+visitedPageTitle+"<br>"+
				 Utility.getLocaleValuePropByKey("msgPageURL", locale)+": "+visitedPageURL+"<br>"+
				"</td></tr></table>";

				msg="<table><tr><td style='font-family: Tahoma;'>"+msg+"<hr>"+supportmsg+"</td></tr></table>";
			}
			else
			{
				//to="feedback@teachermatch.net";
				to=Utility.getValueOfPropByKey("feedbackEmailId");
				//msg=feedbackmsg;

				String ratingMsg = "";
				if(!starRating.equalsIgnoreCase("0"))
					ratingMsg="Star Rating: "+starRating+"<br>";
				else
					ratingMsg="Star Rating: None<br>";

				msg="<table style='font-family: Tahoma;font-size: 16px;'><tr><td>" +
				 Utility.getLocaleValuePropByKey("msgFeedbackRcvfrom", locale)+" "+username+", on "+Utility.getIndiaFormatDateAndTime(new Date())+"<br>"+
				 Utility.getLocaleValuePropByKey("msgUserEmail", locale)+": "+ email+"<br>"+
				 Utility.getLocaleValuePropByKey("msgUserPhone", locale)+": "+ phone+"<br>"+
				 Utility.getLocaleValuePropByKey("lblRole", locale)+": "+userrole+"<br>"+
				 Utility.getLocaleValuePropByKey("msgPageTitle", locale)+": "+visitedPageTitle+"<br>"+
				 Utility.getLocaleValuePropByKey("msgPageURL", locale)+": "+visitedPageURL+"<br>"+
				""+ratingMsg+
				"</td></tr></table>";

				msgSubject= Utility.getLocaleValuePropByKey("msgFeedbackfromUser", locale);

				//msg="<table><tr><td style='font-family: Tahoma;'>"+msg+"</td></tr></table>";
				msg="<table><tr><td style='font-family: Tahoma;'>"+feedbackmsg+"<hr>"+msg+"</td></tr></table>";
			}
			//to="vishwanath@netsura.com";
			//to="testmysite3@gmail.com";
			//to="vikas.sharma@netsura.com";
			//Data saving
			String questSb="";
			System.out.println("domainQuest "+domainQuest);
			if(domainQuest.contains("myedquest.org")){
				questSb= Utility.getLocaleValuePropByKey("msgQuestDA", locale)+" - ";
				System.out.println("questSb "+questSb);
			}
			feedbackAndSupportService.saveFeedbackOrSupport(reqType,userMaster, teacherDetail, visitedPageURL, filePath+fileName, questSb+msgSubject, msg, IPAddressUtility.getIpAddress(request),starRating,phone,visitedPageTitle);

			//System.out.println("to: "+to);

			if(!fileName.equals(""))
			{
				System.out.println("with attachment: "+fileName);
				//sendMailWithAttachments(String to,String subject,String from, String text,String filePath)
				emailerService.sendMailWithAttachments(to, questSb+msgSubject,email,Utility.getUTFToHTML(msg),filePath+fileName);
			}
			else
			{
				System.out.println("without attachment: "+fileName);
				//sendMailAsHTMLText(String to,String subject, String text)
				/*String ratingMsg = "";
				if(!starRating.equalsIgnoreCase("0"))
					ratingMsg="<br><br>User has rated it with "+starRating+" star(s).";*/

				emailerService.sendMailAsHTMLText(to, questSb+msgSubject,Utility.getUTFToHTML(msg));
			}



			//Utility.deleteAllFileFromDir(filePath);


			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.hideFeedBack('"+reqType+"');");
			pw.print("</script>");
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

}
