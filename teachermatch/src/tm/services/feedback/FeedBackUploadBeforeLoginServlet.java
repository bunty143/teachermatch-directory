package tm.services.feedback;	

import java.io.File;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.user.UserMaster;
import tm.dao.TeacherPersonalInfoDAO;
import tm.services.EmailerService;
import tm.services.FeedbackAndSupportService;
import tm.services.social.SocialService;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class FeedBackUploadBeforeLoginServlet extends HttpServlet 
{
    String locale = Utility.getValueOfPropByKey("locale");
    
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{

		HttpSession session = request.getSession(false);
		PrintWriter pw 		= response.getWriter();
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
				
		//String domain = request.getServerName().replaceAll(".*\\.(?=.*\\.)", "");
		//System.out.println("domain::: "+domain);
		String referrer = request.getHeader("referer");
		System.out.println("referrer:: "+referrer);
		boolean isTMorg = false;
		//System.out.println("contains: "+referrer.contains("https://www.teachermatch.org"));
		if(referrer!=null && referrer.contains("https://www.teachermatch.org"))
		{
			response.setHeader("Access-Control-Allow-Origin", "https://www.teachermatch.org");
			response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
			response.setHeader("Access-Control-Max-Age", "3600");
			response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
			isTMorg = true;
		}
		//isTMorg = true;
		
		FileItemFactory factory 	= 	new DiskFileItemFactory();
		ServletFileUpload upload 	= 	new ServletFileUpload(factory);
		List uploadedItems 			= 	null;
		FileItem fileItem 			= 	null;

		String ext				=	"";
		String filePath			=	"";
		String msgSubject		=	"";
		String msg				=	"";
		String reqType			=	"";
		String feedbackmsg		=	"";
		String visitedPageURL	=	"";
		String visitedPageTitle = 	"";
		String f1				=	"";
		String email			=	"";
		String phone			=	"";
		String starRating		=	"";
		String username 		= 	"";
		String userrole 		= 	"";
		String inputName		=	"";
		String inputEmail		=	"";
		String inputPhone		=	"";
		String inputDistrict	=	"";
		String formType			=	"";
		String inputFirstName	=	"";
		String inputLastName	=	"";
		String checkFeedback	=	"";
		String domain	 	    =	"";
		String domainQuest		=	"";
		String n1="";//n1 is for reqType in case of without login on feedback and support
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		Object myDao = context.getBean("myMailSender");
		EmailerService emailerService = (EmailerService)myDao;
		FeedbackAndSupportService feedbackAndSupportService = (FeedbackAndSupportService)context.getBean("feedbackAndSupportService");
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			System.out.println("FeedBack Withought Login");

		try{
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			String fileName="";
			
			while (i.hasNext()){

				fileItem = (FileItem) i.next();

				if(fileItem.getFieldName().equals("formType")){
					formType = fileItem.getString();
				}

				if(fileItem.getFieldName().equals("msgSubject")){
					msgSubject=fileItem.getString("UTF-8").trim();
				}

				if(fileItem.getFieldName().equals("msg")){
					msg=fileItem.getString("UTF-8").trim();
				}

				if(fileItem.getFieldName().equals("feedbackmsg")){
					feedbackmsg=fileItem.getString("UTF-8").trim();
				}

				if(fileItem.getFieldName().equals("starRating")){
					starRating=fileItem.getString();
				}

				if(fileItem.getFieldName().equals("visitedPageURL")){
					visitedPageURL=fileItem.getString();
				}

				if(fileItem.getFieldName().equals("f1")){
					f1=fileItem.getString();
				}

				if(fileItem.getFieldName().equals("visitedPageTitle")){
					visitedPageTitle=fileItem.getString();
				}

				if(fileItem.getFieldName().equals("inputFirstName")){
					inputFirstName=fileItem.getString("UTF-8").trim();
				}

				if(fileItem.getFieldName().equals("inputLastName")){
					inputLastName=fileItem.getString("UTF-8").trim();
				}

				if(fileItem.getFieldName().equals("inputEmail")){
					inputEmail=fileItem.getString("UTF-8").trim();
				}

				if(fileItem.getFieldName().equals("inputPhone")){
					inputPhone=fileItem.getString("UTF-8").trim();
				}

				if(fileItem.getFieldName().equals("inputDistrict")){
					inputDistrict=fileItem.getString("UTF-8").trim();
				}
				//domain
				if(fileItem.getFieldName().equals("domain")){
					domain=fileItem.getString("UTF-8").trim();
				}
				if(fileItem.getFieldName().equals("domainQuest")){
					domainQuest=fileItem.getString("UTF-8").trim();
				}
				if(fileItem.getFieldName().equals("rqtype")){
					n1=fileItem.getString("UTF-8").trim();//value get from jsp in case of without login.
					System.out.println("0000000000000    "+n1);
				}
				 
			}
			if(formType.equalsIgnoreCase("1") && !f1.equals("")) {
				filePath=Utility.getValueOfPropByKey("feedbackRootPath")+inputEmail+"/";
				File f=new File(filePath);

				if(!f.exists())
					f.mkdirs();

				if (fileItem.isFormField() == false) {
					if (fileItem.getSize() > 0) {
						File uploadedFile 		= 	null; 
						String myFullFileName 	= 	fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex 			= 	myFullFileName.lastIndexOf(slashType);
						myFileName 				= 	myFullFileName.substring(startIndex + 1, myFullFileName.length());
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						fileName="FeedBack"+Utility.getDateTime()+ext;
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
					}
				}
				fileItem=null;				
			}

			String to	=	"";
			username	=	inputLastName+" "+inputFirstName;
			email		=	inputEmail;
			phone		=	inputPhone;			
			
			if(!email.equals("")) {
				formType = "1";
				System.out.println("1111111111111111111111111111111111111111111111111111111111111");
				if(n1!=null &&(n1.equals("2b")||n1.equals("2a")))
			    {
			    
			    	to=Utility.getValueOfPropByKey("NCsupportEmailIdApplicants");
			    }
			    
			    if(n1!=null&&(n1.equals("3c")||n1.equals("3a")))
			    {
			    	
			    	to=Utility.getValueOfPropByKey("NCsupportEmailIdPartners");
			    }
				//to=Utility.getValueOfPropByKey("supportEmailId");
				//to="hanzala@netsutra.com";
				String supportmsg="<table style='font-family: Tahoma;font-size: 16px;'><tr><td>" +
				 Utility.getLocaleValuePropByKey("msgSuppMsgRecv", locale)+" "+username+", on "+Utility.getIndiaFormatDateAndTime(new Date())+"<br>"+
				 Utility.getLocaleValuePropByKey("msgUserEmail", locale)+": "+email+"<br>"+
				 Utility.getLocaleValuePropByKey("msgUserPhone", locale)+": "+phone+"<br>"+
				 Utility.getLocaleValuePropByKey("msgPageTitle", locale)+": "+visitedPageTitle+"<br>"+
				 Utility.getLocaleValuePropByKey("msgPageURL", locale)+": "+visitedPageURL+"<br>"+
				"</td></tr></table>";

				msg="<table><tr><td style='font-family: Tahoma;'>"+msg+"<hr>"+supportmsg+"</td></tr></table>";
			} else {
				formType = "2";
				System.out.println("1111111111111111111111111111111111111111111111111111111111111");
				if(n1!=null &&(n1.equals("2b")||n1.equals("2a")))
			    {
			    
			    	to=Utility.getValueOfPropByKey("NCsupportEmailIdApplicants");
			    }
			    
			    if(n1!=null&&(n1.equals("3c")||n1.equals("3a")))
			    {
			    	
			    	to=Utility.getValueOfPropByKey("NCsupportEmailIdPartners");
			    }
				//to=Utility.getValueOfPropByKey("supportEmailId");
				//to="hanzala@netsutra.com";
				String ratingMsg = "";
				if(!starRating.equalsIgnoreCase("0"))
					ratingMsg= Utility.getLocaleValuePropByKey("msgStarRating", locale)+": "+starRating+"<br>";
				else
					ratingMsg= Utility.getLocaleValuePropByKey("msgStarRating", locale)+": None<br>";

				msg="<table style='font-family: Tahoma;font-size: 16px;'><tr><td>" +
				 Utility.getLocaleValuePropByKey("msgFeedbackon", locale)+" "+Utility.getIndiaFormatDateAndTime(new Date())+"<br>"+
				 Utility.getLocaleValuePropByKey("msgPageTitle", locale)+": "+visitedPageTitle+"<br>"+
				 Utility.getLocaleValuePropByKey("msgPageURL", locale)+": "+visitedPageURL+"<br>"+
				""+ratingMsg+
				"</td></tr></table>";
				msg="<table><tr><td style='font-family: Tahoma;'>"+feedbackmsg+"<hr>"+msg+"</td></tr></table>";
			}			
			String questSb="";
			System.out.println("domainQuest "+domainQuest);
			if(domainQuest.contains("myedquest.org")){
				questSb= Utility.getLocaleValuePropByKey("msgQuestDA", locale)+" - ";
				System.out.println("questSb "+questSb);
			}
			feedbackAndSupportService.saveFeedbackOrSupportBeforeLogin(reqType,email,inputFirstName,inputLastName, visitedPageURL, filePath+fileName, questSb+msgSubject, msg, IPAddressUtility.getIpAddress(request),starRating,phone,visitedPageTitle);
			if(!fileName.equals(""))
			{
				System.out.println("with attachment without login: "+fileName);
				System.out.println("to===   "+to);
				
				emailerService.sendMailWithAttachments(to, questSb+msgSubject,email,Utility.getUTFToHTML(msg),filePath+fileName);
			} else {
				System.out.println("without attachment without login: "+fileName);
				System.out.println("to===   "+to);
				emailerService.sendMailAsHTMLText(to, questSb+msgSubject,Utility.getUTFToHTML(msg));
			}
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			if(isTMorg)
			{
				//pw.print("alert('Message sent successfully to TM Support.');");
				//pw.print("window.top.hideFeedBack('"+formType+"');");
				pw.print("window.parent.postMessage('"+formType+"', '*');");
			}
			else
				pw.print("window.top.hideFeedBack('"+formType+"');");
			
			
			pw.print("</script>");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		} else { System.out.println("After Login");
			UserMaster userMaster 		= 	(UserMaster)session.getAttribute("userMaster");
			TeacherDetail teacherDetail = 	(TeacherDetail)session.getAttribute("teacherDetail");
			if(userMaster!=null)
			{
				email 		= userMaster.getEmailAddress();
				username 	= userMaster.getFirstName()+" "+userMaster.getLastName();
				userrole 	= userMaster.getRoleId().getRoleName();
				phone 		= userMaster.getMobileNumber()==null?"None":(userMaster.getMobileNumber().equals("")==true?"None":userMaster.getMobileNumber());
			}
			else
			{
				email = teacherDetail.getEmailAddress();
				username = teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
				userrole = "Teacher";
			}

			try{
				uploadedItems = upload.parseRequest(request);
				upload.setSizeMax(10485760);
				Iterator i = uploadedItems.iterator();
				String fileName="";

				while (i.hasNext())	
				{
					fileItem = (FileItem) i.next();
					if(fileItem.getFieldName().equals("formType")){
						formType = fileItem.getString();
					}
					
					if(fileItem.getFieldName().equals("msgSubject")){
						msgSubject=fileItem.getString("UTF-8").trim();
					}
					if(fileItem.getFieldName().equals("msg")){
						msg=fileItem.getString("UTF-8").trim();
					}
					if(fileItem.getFieldName().equals("feedbackmsg")){
						feedbackmsg=fileItem.getString("UTF-8").trim();
					}
					if(fileItem.getFieldName().equals("starRating")){
						starRating=fileItem.getString();
					}
					if(fileItem.getFieldName().equals("visitedPageURL")){
						visitedPageURL=fileItem.getString();
					}
					if(fileItem.getFieldName().equals("f1")){
						f1=fileItem.getString();
					}
					//ashish
					if(fileItem.getFieldName().equals("reqType1")){
						reqType=fileItem.getString();
					}
					
					if(fileItem.getFieldName().equals("visitedPageTitle")){
						visitedPageTitle=fileItem.getString();
					}

					if(fileItem.getFieldName().equals("inputEmail")){
						inputEmail=fileItem.getString("UTF-8").trim();
					}

					if(fileItem.getFieldName().equals("checkFeedback")){
						checkFeedback=fileItem.getString("UTF-8").trim();
					}
					
				}
				if(formType.equalsIgnoreCase("1") && !f1.equals(""))
				{
					filePath=Utility.getValueOfPropByKey("feedbackRootPath")+email+"/";
					System.out.println("filePath=======>>"+filePath);
					File f=new File(filePath);
					if(!f.exists())
						f.mkdirs();

					if (fileItem.isFormField() == false) 
					{
						if (fileItem.getSize() > 0)	
						{
							File uploadedFile = null; 
							String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
							int startIndex = myFullFileName.lastIndexOf(slashType);
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
							ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
							fileName="FeedBack"+ext;
							uploadedFile = new File(filePath, fileName);
							fileItem.write(uploadedFile);
						}
					}
					fileItem=null;				
				}

				if(userMaster==null)
				{
					TeacherPersonalInfoDAO teacherPersonalInfoDAO = (TeacherPersonalInfoDAO)context.getBean("teacherPersonalInfoDAO");
					TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);

					String teacherMobile 	= "0";
					String teacherPhone 	= "0";
					
					if(teacherPersonalInfo!=null)
					{
						teacherMobile = teacherPersonalInfo.getMobileNumber();
						teacherPhone = teacherPersonalInfo.getPhoneNumber();
					}
					if(teacherMobile.equals("0") && teacherPhone.equals("0"))
						phone="None";
					else if(!teacherMobile.equals("0"))
						phone=teacherMobile;
					else
						phone=teacherPhone;
				}
				String to="";
				if(checkFeedback.equalsIgnoreCase("-1"))
				{	
					formType = "2";
				 
					if(Utility.isNC())
				    {
				    if(reqType!=null &&(reqType.equals("2b")||reqType.equals("2a")))
				    {
				    
				    	to=Utility.getValueOfPropByKey("NCsupportEmailIdApplicants");
				    }
				    
				    if(reqType!=null&&(reqType.equals("3c")||reqType.equals("3a")))
				    {
				    	
				    	to=Utility.getValueOfPropByKey("NCsupportEmailIdPartners");
				    }
				    }
				    else
				    {
				    	if(reqType!=null &&(reqType.equals("2b")||reqType.equals("2a")))
					    {
					    
					    	to=Utility.getValueOfPropByKey("NCsupportEmailIdApplicants");
					    }
					    
					    if(reqType!=null&&(reqType.equals("3c")||reqType.equals("3a")))
					    {
					    	
					    	to=Utility.getValueOfPropByKey("NCsupportEmailIdPartners");
					    }
				    	//to=Utility.getValueOfPropByKey("supportEmailId");
				    }
					
					//to="hanzala@netsutra.com";
					String ratingMsg = "";
					if(!starRating.equalsIgnoreCase("0"))
						ratingMsg= Utility.getLocaleValuePropByKey("msgStarRating", locale)+": "+starRating+"<br>";
					else
						ratingMsg= Utility.getLocaleValuePropByKey("msgStarRating", locale)+": None<br>";

					msg="<table style='font-family: Tahoma;font-size: 16px;'><tr><td>" +
					 Utility.getLocaleValuePropByKey("msgSuppMsgRecv", locale)+" "+username+", on "+Utility.getIndiaFormatDateAndTime(new Date())+"<br>"+
					 Utility.getLocaleValuePropByKey("msgUserEmail", locale)+": "+ email+"<br>"+
					 Utility.getLocaleValuePropByKey("msgUserPhone", locale)+": "+ phone+"<br>"+
					 Utility.getLocaleValuePropByKey("lblRole", locale)+": "+userrole+"<br>"+
					Utility.getLocaleValuePropByKey("msgPageTitle", locale)+": "+visitedPageTitle+"<br>"+
					Utility.getLocaleValuePropByKey("msgPageURL", locale)+": "+visitedPageURL+"<br>"+
					""+ratingMsg+
					"</td></tr></table>";
					msg="<table><tr><td style='font-family: Tahoma;'>"+feedbackmsg+"<hr>"+msg+"</td></tr></table>";

				} else {

					formType = "1";
				
				    if(Utility.isNC())
				    {
				    if(reqType!=null &&(reqType.equals("2b")||reqType.equals("2a")))
				    {
				    	
				    	to=Utility.getValueOfPropByKey("NCsupportEmailIdApplicants");
				    }
				    
				    if(reqType!=null&&(reqType.equals("3c")||reqType.equals("3a")))
				    {
				    	
				    	to=Utility.getValueOfPropByKey("NCsupportEmailIdPartners");
				    }
				    }
				    else
				    {
				    	if(reqType!=null &&(reqType.equals("2b")||reqType.equals("2a")))
					    {
				    	to=Utility.getValueOfPropByKey("NCsupportEmailIdApplicants");
					    }
				    	 if(reqType!=null&&(reqType.equals("3c")||reqType.equals("3a")))
						    {
						    	
						    	to=Utility.getValueOfPropByKey("NCsupportEmailIdPartners");
						    }
				    	
				    	
				    }
					System.out.println("to======="+to);
					//to="hanzala@netsutra.com";
					String supportmsg="<table style='font-family: Tahoma;font-size: 16px;'><tr><td>" +
					 Utility.getLocaleValuePropByKey("msgSuppMsgRecv", locale)+" "+username+", on "+Utility.getIndiaFormatDateAndTime(new Date())+"<br>"+
					 Utility.getLocaleValuePropByKey("msgUserEmail", locale)+": "+email+"<br>"+
					 Utility.getLocaleValuePropByKey("msgUserPhone", locale)+": "+phone+"<br>"+
					 Utility.getLocaleValuePropByKey("lblRole", locale)+": "+userrole+"<br>"+
					Utility.getLocaleValuePropByKey("msgPageTitle", locale)+": "+visitedPageTitle+"<br>"+
					 Utility.getLocaleValuePropByKey("msgPageURL", locale)+": "+visitedPageURL+"<br>"+
					"</td></tr></table>";
					msg="<table><tr><td style='font-family: Tahoma;'>"+msg+"<hr>"+supportmsg+"</td></tr></table>";
				}
				//Data saving
				System.out.println("to======="+to);
				feedbackAndSupportService.saveFeedbackOrSupportNew(formType,checkFeedback,userMaster, teacherDetail, visitedPageURL, filePath+fileName, msgSubject, msg, IPAddressUtility.getIpAddress(request),starRating,phone,visitedPageTitle);
				
				if(!fileName.equals(""))
				{
					System.out.println("with attachment: "+fileName);
					System.out.println("to======="+to);
					emailerService.sendMailWithAttachments(to, msgSubject,email,Utility.getUTFToHTML(msg),filePath+fileName);
				} else {
					System.out.println("without attachment: "+fileName);
					System.out.println("to======="+to);
					emailerService.sendMailAsHTMLText(to, msgSubject,Utility.getUTFToHTML(msg));
				}
				//System.out.println(msg);
				pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
				if(isTMorg)
				{
					//pw.print("alert('Message sent successfully to TM Support.');");
					//pw.print("window.top.hideFeedBack('"+formType+"');");
					pw.print("window.parent.postMessage('"+formType+"', '*');");
				}
				else
					pw.print("window.top.hideFeedBack('"+formType+"');");
				pw.print("</script>");
			} 
			catch (FileUploadException e) 
			{
				e.printStackTrace();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}

		
	}

}
