package tm.services;

/**********************************
 	***********************
   *  @Author Ram Nath  *
 **********************
********************************/

import java.util.List;
import java.util.Map;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.EligibilityStatusMaster;
public class ONBServiceAjax {
	
	@Autowired
	private ONBAjaxNew oNRAjaxNew;
	
	public Map<String,String>  getDistrictWiseColumn(Integer districtId){
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy------>> Service get getDistrictWiseColumn");
		return oNRAjaxNew.getDistrictWiseColumn(districtId);
	}
	
	public JSONObject displayONBGrid(Map<String,String> allMap){
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy------>> Service get displayONBGrid");
		return oNRAjaxNew.displayONBGrid(allMap);
	}
	public JSONObject getStatusJSON(Map<String,String> allMap){
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy------>> Service get StatusJSON");
		return oNRAjaxNew.getStatusJSON(allMap);
	}
	public boolean deleteStatusONB(Integer EVHId){
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy------>> Service get deleteStatusONB");
		return oNRAjaxNew.deleteStatusONB(EVHId);
	}
	public boolean saveNotesONB(Map<String,String> allMap){
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy------>> Service get saveNotesONB");
		return oNRAjaxNew.saveNotesONB(allMap);
	}
	
	public boolean sendToPeopleSoftONB(Long[] selectedTSHId){
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy------>> Service get sendToPeopleSoftONB");
		return oNRAjaxNew.sendToPeopleSoftONB(selectedTSHId);
	}
	public String generateOnboardingPDF(Map<String,String> allMap){
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy------>> Service get generateOnboardingPDF");
		return oNRAjaxNew.generateOnboardingPDF(allMap);
	}
	public String generateOnboardingExcel(Map<String,String> allMap){
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy------>> Service get generateOnboardingExcel");
		return oNRAjaxNew.generateOnboardingExcel(allMap);
	}
	public Long getJFTIdByJobIdAndTeacherId(Map<String,String> allMap){
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy------>> Service get getJFTIdByJobIdAndTeacherId");
		return oNRAjaxNew.getJFTIdByJobIdAndTeacherId(allMap);
	}
	public List<String[]> getAllStatusByPuttingStatus(Map<String,String> allMap){
		return oNRAjaxNew.getAllStatusByPuttingStatus(allMap);
	}
	public String onBoardingSendMailToTeacher(Long teacherHistoryId)
	{
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy------>> Service get onBoardingSendMailToTeacher");
		return oNRAjaxNew.onBoardingSendMailToTeacher(teacherHistoryId);
	}
	public String getQQOThumb(){
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy------>> Service get getQQOThumb");
		return oNRAjaxNew.getQQOThumb();
	}
	public List<EligibilityStatusMaster> getStatusByEidJSON(Integer eligibilityId){
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy------>> Service get getStatusByEidJSON");
		return oNRAjaxNew.getStatusByEidJSON(eligibilityId);
	}

}
