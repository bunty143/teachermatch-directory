package tm.services;
import java.util.Properties;
import javax.naming.*;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;


import tm.utility.Utility;
public class LDAPService {
	public boolean authenticate(String email,String password,String baseDN){
		System.out.println("::::::::::::username"+email+"\tpasword"+password);
		boolean auth=false;
		String ldapHost		       =  Utility.getValueOfPropByKey("ldapHost");
		String ldapAdminUser 	   =  Utility.getValueOfPropByKey("ldapAdminUser");
		String ldapAdminPassword   =  Utility.getValueOfPropByKey("ldapAdminPassword");
		String ldapPort 		   =  Utility.getValueOfPropByKey("ldapPort");
		String ldapUrl 			   =  "";
		if(ldapPort.equals("389")){
			ldapUrl = "ldap";
		}else if(ldapPort.equals("636")){
			ldapUrl = "ldaps";
		}
		 ldapUrl 			   =  ldapUrl+"://"+ldapHost+":"+ldapPort;
		 
		System.out.println(":::::::::::::::::::::::::::::::LDAP URI:::::::::::::"+ldapUrl);
		 
        Properties properties = new Properties();
        properties.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        properties.put(Context.REFERRAL, "follow");
        properties.put("com.sun.jndi.ldap.connect.timeout", "1000000");
        properties.put(Context.PROVIDER_URL, ldapUrl);
        properties.put(Context.SECURITY_PRINCIPAL,ldapAdminUser);
        properties.put(Context.SECURITY_CREDENTIALS,ldapAdminPassword); 
        try {
        	 javax.naming.directory.DirContext ctx = null;
  	         NamingEnumeration results = null;
        	 ctx = new InitialDirContext(properties);
        	 SearchControls controls = new SearchControls();
        	 String returnedAtts[]={"cn","givenName", "sAMAccountName","distinguishedName"};
        	 controls.setReturningAttributes(returnedAtts);
             controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
             String searchBase = "DC=adams12,DC=org";
             String searchFilter = "(userPrincipalName="+email+")";
             results = ctx.search(searchBase, searchFilter, controls);
             while (results.hasMore()) 	{
                 SearchResult searchResult = (SearchResult) results.next();
                 Attributes attributes = searchResult.getAttributes();
                 Attribute attr = attributes.get("givenName");
                 Attribute atr  = attributes.get("sAMAccountName");
                 String samlAcc = (String)atr.get();
                 String cn = (String) attr.get();
                 Properties env1 = new Properties();
                 System.out.println(" sAMAccountType = " + samlAcc);
                 System.out.println(" Person Common Name = " + cn);
                 System.out.println(" sAMAccountName = " + samlAcc);
                 System.out.println(" Person Common Name = " + cn);
                 Attribute dnAttr = attributes.get("distinguishedName");
                 String dn = (String) dnAttr.get();
                 System.out.println("DN:::::::::"+dn);
                 env1.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
                 env1.put(Context.PROVIDER_URL, ldapUrl);
                 env1.put(Context.SECURITY_PRINCIPAL, dn);
                 env1.put(Context.REFERRAL, "follow");
                 env1.put("com.sun.jndi.ldap.connect.pool", "true");
                 env1.put("com.sun.jndi.ldap.connect.timeout", "5000");
                 env1.put(Context.SECURITY_CREDENTIALS, password);
                 javax.naming.directory.DirContext ctx2 = new InitialDirContext(env1);   
                 System.out.println("Context2:::::::::::"+ctx2);
                 Attributes attrs2 = ctx2.getAttributes(dn);
                 Attribute attr2 = attributes.get("sAMAccountName");
                 System.out.println(":::::::::::::::GivenName:::::"+attr2.get());
                 if(ctx2!=null)
                 auth=true;
                 
                 ctx.close();
                 ctx2.close();
                 return auth;
             }
             
        } catch (Exception e) {
        	e.printStackTrace();
        }  
        return auth;
	}

}
