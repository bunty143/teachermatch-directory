package tm.services.teacher;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.InternalTransferCandidates;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.cgreport.TeacherStatusScores;
import tm.bean.cgreport.UserMailSend;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.teacher.StatusUploadTemp;
import tm.bean.user.UserMaster;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.cgreport.TeacherStatusScoresDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.teacher.StatusUploadTempDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CommonService;
import tm.services.EmailerService;
import tm.services.PaginationAndSorting;
import tm.services.report.CandidateGridService;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;


public class StatusUploadTempAjax 
{
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private TeacherStatusScoresDAO teacherStatusScoresDAO;
	
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@Autowired
    private JobForTeacherDAO jobForTeacherDAO;
    
    @Autowired
    private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
    
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private StatusUploadTempDAO statusUploadTempDAO;
	
	@Autowired
	private UserMasterDAO usermasterdao;
	public void setUsermasterdao(UserMasterDAO usermasterdao) {
		this.usermasterdao = usermasterdao;
	}
	@Autowired
	private  TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
		this.teacherDetailDAO = teacherDetailDAO;
	}
	@Autowired
	private  RoleMasterDAO roleMasterDAO;
	public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO) {
		this.roleMasterDAO = roleMasterDAO;
	}

	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}

	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	private Vector vectorDataExcelXLSX = new Vector();
	public String saveStatusTemp(String fileName,String sessionId,int branchId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		System.out.println("::::::::saveStatusTemp:::::::::");
		String returnVal="";
        try{
        	 String root = request.getRealPath("/")+"/uploads/";
        	 String filePath=root+fileName;
        	 System.out.println("filePath :"+filePath);
        	 if(fileName.contains(".xlsx")){
        		 vectorDataExcelXLSX = readDataExcelXLSX(filePath);
        	 }else{
        		 vectorDataExcelXLSX = readDataExcelXLS(filePath);
        	 }
        	 System.out.println("::::::::get value of XLSX and XLS:::::::::");
			 int talent_email_count=11;
			 int job_Id_count=11;
			 int job_Id_count1=11;
			 int node_name_count=11;
			 int status_count=11;
			 int user_email_count1=11;
			 int node_name_count1=11;
			 
			SessionFactory sessionFactory=usermasterdao.getSessionFactory();
			StatelessSession sessionHiber = sessionFactory.openStatelessSession();
			List<String> emails=new ArrayList<String>();
        	Transaction txOpen =sessionHiber.beginTransaction();
        	List<String> statusNameList=new ArrayList<String>();
         	List jobs = new ArrayList();
			for(int em=0; em<vectorDataExcelXLSX.size(); em++) {
	            Vector vectorCellEachRowDataGetEmail = (Vector) vectorDataExcelXLSX.get(em);
	            String email="",job_Id="",node_name="";
	            for(int e=0; e<vectorCellEachRowDataGetEmail.size(); e++) {
	            	if(vectorCellEachRowDataGetEmail.get(e).toString().equalsIgnoreCase("talent_email")){
	            		user_email_count1=e;
	            	}
	            	if(user_email_count1==e)
	            		email=vectorCellEachRowDataGetEmail.get(e).toString().trim();
	            	
	            	if(vectorCellEachRowDataGetEmail.get(e).toString().equalsIgnoreCase("node_name")){
	            		node_name_count1=e;
	            	}
	            	if(node_name_count1==e)
	            		node_name=vectorCellEachRowDataGetEmail.get(e).toString().trim();
	            	
	            	if(vectorCellEachRowDataGetEmail.get(e).toString().equalsIgnoreCase("job_Id")){
	            		job_Id_count1=e;
	            	}
	            	if(job_Id_count1==e)
	            		job_Id=vectorCellEachRowDataGetEmail.get(e).toString().trim();
	            	
	            }
	            if(em!=0){
	            	emails.add(email);
	            	statusNameList.add(node_name);
	            	jobs.add(job_Id);
	            }
			 }
			 List<TeacherDetail> teacherIdList =new ArrayList<TeacherDetail>();
			 List <TeacherDetail> teacherUploadTempList=teacherDetailDAO.findAllTeacherDetails(emails);
			 Map<String,TeacherDetail> teacherMap=new HashMap<String, TeacherDetail>();
			 for (TeacherDetail teacherDetail : teacherUploadTempList) {
				 teacherIdList.add(teacherDetail);
				 teacherMap.put(teacherDetail.getEmailAddress(),teacherDetail);
			}
			 System.out.println("teacherIdList:::::::::::"+teacherIdList.size());
	         List<JobForTeacher> jobForTeacherList=new ArrayList<JobForTeacher>();
	         if(teacherIdList.size()>0)
	        	 jobForTeacherList=jobForTeacherDAO.findJobByFilterTeacher(teacherIdList);
	         
	         System.out.println("jobForTeacherList::::::::::::"+jobForTeacherList.size());
	         
	         Map<String,JobForTeacher> jftMap=new HashMap<String, JobForTeacher>();
	         Map<String,JobForTeacher> jftBranchMap=new HashMap<String, JobForTeacher>();
	         for (JobForTeacher jobForTeacher : jobForTeacherList) {
	        	 String keyMap=jobForTeacher.getTeacherId().getEmailAddress()+"##"+jobForTeacher.getJobId().getApiJobId();
	        	 if(jobForTeacher.getJobId().getBranchMaster()!=null){
	        		 System.out.println("jobForTeacher.getJobId().getBranchMaster().getBranchId()::::::::::::::::::::::::"+jobForTeacher.getJobId().getBranchMaster().getBranchId());
	        		 System.out.println("branchId::::::::::::::::::::::::"+branchId);
	        		 if(jobForTeacher.getJobId().getBranchMaster().getBranchId()==branchId){
	        			 jftBranchMap.put(keyMap, jobForTeacher);
	        		 }
	        	 }
	        	 jftMap.put(keyMap,jobForTeacher);
			 }
	         ///////////////////////////////////////////////////////////////////////////////////////////
	         BranchMaster branchMaster=branchMasterDAO.findById(branchId, false, false);
        	 Map<Integer,JobOrder> jobOrderMap = jobOrderDAO.findByAllJobsWithBranch(jobs,branchMaster);
        	 System.out.println("jobOrderMap::::::::::"+jobOrderMap.size());
        	 List<JobCategoryMaster> jobCategoryMasters=new ArrayList<JobCategoryMaster>();
        	 Map<String,Integer> jobCategoryIds=new HashMap<String, Integer>();
        	 for(Map.Entry<Integer,JobOrder> entry : jobOrderMap.entrySet()){
        		 JobOrder jobOrder=entry.getValue();
        		 jobCategoryIds.put(jobOrder.getApiJobId(),jobOrder.getJobCategoryMaster().getJobCategoryId());
        		 if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null){
        			 jobCategoryMasters.add(jobOrder.getJobCategoryMaster().getParentJobCategoryId());
        			 jobCategoryIds.put(jobOrder.getApiJobId(),jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
        		 }else{
        			 jobCategoryIds.put(jobOrder.getApiJobId(),jobOrder.getJobCategoryMaster().getJobCategoryId());
        			 jobCategoryMasters.add(jobOrder.getJobCategoryMaster());
        		 }
    		 }
        	 System.out.println("jobCategoryMasters::::"+jobCategoryMasters.size());
        	 List<SecondaryStatus> secondaryStatusList= secondaryStatusDAO.findSecondaryStatusByNames(jobCategoryMasters,statusNameList);
        	 System.out.println("secondaryStatusList::::::::::"+secondaryStatusList.size());
        	 Map<String,SecondaryStatus> mapStatusNames=new HashMap<String, SecondaryStatus>();
        	 for (SecondaryStatus secondaryStatus : secondaryStatusList) {
    			 System.out.println("::::>>>"+secondaryStatus.getSecondaryStatusName()+"##"+secondaryStatus.getJobCategoryMaster().getJobCategoryId());
    			 mapStatusNames.put(secondaryStatus.getSecondaryStatusName()+"##"+secondaryStatus.getJobCategoryMaster().getJobCategoryId(),secondaryStatus);
			 }
        	 System.out.println("mapStatusNames:::::::::::::::::::::::::::::"+mapStatusNames.size());
	         ///////////////////////////////////////////////////////////////////////////////////////////
			 System.out.println("::::::::Start Inserting:::::::::");
			 StatusUploadTemp statusUploadTemp= new StatusUploadTemp();
			 for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
	             Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
	             String talent_email="";
				 String job_Id="";
				 String node_name="";
				 String status="";
				 String jobAppliedCheck="";
	            for(int j=0; j<vectorCellEachRowData.size(); j++) {
	            	
	            	try{
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("talent_email")){
		            		talent_email_count=j;
		            	}
		            	if(talent_email_count==j){
		            		talent_email=vectorCellEachRowData.get(j).toString().trim();
		            	}
		            	
	            		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("job_Id")){
	            			job_Id_count=j;
		            	}
		            	if(job_Id_count==j)
		            		job_Id=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("node_name")){
		            		node_name_count=j;
		            	}
		            	if(node_name_count==j)
		            		node_name=vectorCellEachRowData.get(j).toString().trim();
		            		
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("status")){
		            		status_count=j;
		            	}
		            	if(status_count==j)
		            		status=vectorCellEachRowData.get(j).toString().trim();
		            	
	            	}catch(Exception e){}
	            	
	            }
	            if(i!=0){
		             if(returnVal.equals("1") && (!talent_email.equalsIgnoreCase("") || !job_Id.equalsIgnoreCase("") || !node_name.equalsIgnoreCase("") || !status.equalsIgnoreCase(""))){
		            	statusUploadTemp= new StatusUploadTemp();
	            	 	String errorText=""; boolean errorFlag=false;
	            	 	
	            	 	jobAppliedCheck=talent_email+"##"+job_Id;
	            	 	
	            	 	if(talent_email.equalsIgnoreCase("")||talent_email.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="Talent Email is not available.";
	            	 		errorFlag=true;
	            	 	}else if(!Utility.validEmailForTeacher(talent_email)){
            	 			if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="Talent Email is not valid.";
	            	 		errorFlag=true;
	            	 	}else if(teacherMap.get(talent_email)==null){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="Talent Email does not exist.";
	            	 		errorFlag=true;
	            	 	}else if(jftMap.get(jobAppliedCheck)==null){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="Talent has not applied any job yet.";
	            	 		errorFlag=true;
	            	 	}else if(jftBranchMap.get(jobAppliedCheck)==null){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="Talent has not applied any job for this branch.";
	            	 		errorFlag=true;
	            	 	}
	            	 	if(job_Id.equalsIgnoreCase("")||job_Id.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="JobId is not available.";
	            	 		errorFlag=true;
	            	 	}
	            	 	int jobCategoryId=0;
	            	 	String statusKey=null;
	            	 	if(jobCategoryIds.get(job_Id)!=null){
	            	 		jobCategoryId=jobCategoryIds.get(job_Id);
	            	 		statusKey=node_name+"##"+jobCategoryId;
	     					System.out.println("statusKey:::::"+statusKey);
	            	 	}
	     				
	            	 	if(node_name.equalsIgnoreCase("")||node_name.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="Node Name is not available.";
	            	 		errorFlag=true;
	            	 	}else if(mapStatusNames.get(statusKey)==null){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="Node Name does not exist.";
	            	 		errorFlag=true;
	            	 	}else if(!node_name.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale)) && !node_name.equalsIgnoreCase("Prescreen")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="Node Name is not valid.";
	            	 		errorFlag=true;
	            	 	}
	            	 	
	            	 	if(status.equalsIgnoreCase("")||status.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br> ";	
	            	 		}
	            	 		errorText+="Status is not available.";
	            	 		errorFlag=true;
	            	 	}else if(!status.equalsIgnoreCase("waived")&& !status.equalsIgnoreCase("complete") && !status.equalsIgnoreCase("undo")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br> ";	
	            	 		}
	            	 		errorText+="Status is not valid.";
	            	 		errorFlag=true;
	            	 	}
	            	 	statusUploadTemp.setTalent_email(talent_email);
	            	 	statusUploadTemp.setJob_id(job_Id);
	            	 	statusUploadTemp.setNode_name(node_name);
	            	 	statusUploadTemp.setStatus(status);
	            	 	statusUploadTemp.setBranchId(branchId+"");
	            	 	statusUploadTemp.setSessionid(sessionId);
	            	 	
	            	 	if(errorText!=null)
	            	 		statusUploadTemp.setErrortext(errorText);
	            	 	sessionHiber.insert(statusUploadTemp);
		             }
	            }else{
	            	 if(talent_email_count!=11 && job_Id_count!=11 && node_name_count!=11 && status_count!=11){
	            		 returnVal="1"; 
	            		 statusUploadTempDAO.deleteStatusTemp(sessionId);
	            	 }else{
	            		 returnVal="";
	            		 boolean fieldVal=false;
		            	 if(talent_email_count==11){
		            		 returnVal+=" talent_email";
		            		 fieldVal=true;
		            	 }
		            	 if(job_Id_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" job_Id";
		            	 }
		            	 if(node_name_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" node_name";
		            	 }
		            	 if(status_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" status";
		            	 }
	            	 }
	            }
		     }
			 txOpen.commit();
     	 	 sessionHiber.close();
     	 	System.out.println("Data has been inserted successfully.");
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return returnVal;
	}
	public static Vector readDataExcelXLS(String fileName) throws IOException {

		System.out.println(":::::::::::::::::readDataExcel XLS::::::::::::");
		Vector vectorData = new Vector();
		
		try{
			InputStream ExcelFileToRead = new FileInputStream(fileName);
			HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
	 
			HSSFSheet sheet=wb.getSheetAt(0);
			HSSFRow row; 
			HSSFCell cell;
	 
			Iterator rows = sheet.rowIterator();
			int talent_email_count=11;
	        int job_Id_count=11;
	        int node_name_count=11;
	        int status_count=11;
	        String indexCheck="";
			 
			while (rows.hasNext()){
				row=(HSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,jobFlag=false;
				String indexPrev="";
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
				while (cells.hasNext())
				{
					cell=(HSSFCell) cells.next();
					cIndex=cell.getColumnIndex();
					if(cell.toString().equalsIgnoreCase("location")){
						talent_email_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(talent_email_count==cIndex){
						cellFlag=true;
					}	
					  
					if(cell.toString().equalsIgnoreCase("job_Id")){
						job_Id_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(job_Id_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().equalsIgnoreCase("node_name")){
						node_name_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(node_name_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().equalsIgnoreCase("status")){
						status_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(status_count==cIndex){
						cellFlag=true;
					}
					if(cellFlag){
						if(!jobFlag){
	            			try{
	            				mapCell.put(Integer.valueOf(cIndex),cell.getStringCellValue()+"");
	            			}catch(Exception e){
	            				mapCell.put(Integer.valueOf(cIndex),new Double(cell.getNumericCellValue()).longValue()+"");
	            			}
	            		}else{
	            			mapCell.put(Integer.valueOf(cIndex),cell+"");
	            		}
					}
		        	cellFlag=false;
					
				}
			    vectorCellEachRowData=cellValuePopulate(mapCell);
				vectorData.addElement(vectorCellEachRowData);
			}
		}catch(Exception e){}
		return vectorData;
    }
	public static Vector readDataExcelXLSX(String fileName) {
		System.out.println(":::::::::::::::::readDataExcel XLSX ::::::::::::");
        Vector vectorData = new Vector();
        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            XSSFWorkbook xssfWorkBook = new XSSFWorkbook(fileInputStream);
            // Read data at sheet 0
            XSSFSheet xssfSheet = xssfWorkBook.getSheetAt(0);
            Iterator rowIteration = xssfSheet.rowIterator();
            int talent_email_count=11;
            int job_Id_count=11;
            int node_name_count=11;
            int status_count=11;
            // Looping every row at sheet 0
            String indexCheck="";
            while (rowIteration.hasNext()) {
                XSSFRow xssfRow = (XSSFRow) rowIteration.next();
                Iterator cellIteration = xssfRow.cellIterator();
                Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,jobFlag=false;
				String indexPrev="";
                // Looping every cell in each row at sheet 0
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
                while (cellIteration.hasNext()){
                    XSSFCell xssfCell = (XSSFCell) cellIteration.next();
                    cIndex=xssfCell.getColumnIndex();
                    
                    if(xssfCell.toString().equalsIgnoreCase("talent_email")){
                    	talent_email_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(talent_email_count==cIndex){
	            		cellFlag=true;
	            	}	
	            	if(xssfCell.toString().equalsIgnoreCase("job_Id")){
	            		job_Id_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(job_Id_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("node_name")){
	            		node_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(node_name_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("status")){
	            		status_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(status_count==cIndex){
	            		cellFlag=true;
	            	}
	            	if(cellFlag){
	            		if(!jobFlag){
	            			try{
	            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getStringCellValue()+"");
	            			}catch(Exception e){
	            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getRawValue()+"");
	            			}
	            		}else{
	            			mapCell.put(Integer.valueOf(cIndex),xssfCell+"");
	            		}
	            	}
	            	cellFlag=false;
                }
                vectorCellEachRowData=cellValuePopulate(mapCell);
                vectorData.addElement(vectorCellEachRowData);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
         
        return vectorData;
    }
	public static Vector cellValuePopulate(Map<Integer,String> mapCell){
		 Vector vectorCellEachRowData = new Vector();
		 Map<Integer,String> mapCellTemp = new TreeMap<Integer, String>();
		 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false;
		 for(Map.Entry<Integer, String> entry : mapCell.entrySet()){
			int key=entry.getKey();
			String cellValue=null;
			if(entry.getValue()!=null)
				cellValue=entry.getValue().trim();
			
			if(key==0){
				mapCellTemp.put(key, cellValue);
				flag0=true;
			}
			if(key==1){
				flag1=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==2){
				flag2=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==3){
				flag3=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==4){
				flag4=true;
				mapCellTemp.put(key, cellValue);
			}
		 }
		 if(flag0==false){
			 mapCellTemp.put(0, "");
		 }
		 if(flag1==false){
			 mapCellTemp.put(1, "");
		 }
		 if(flag2==false){
			 mapCellTemp.put(2, "");
		 }
		 if(flag3==false){
			 mapCellTemp.put(3, "");
		 }
		 if(flag4==false){
			 mapCellTemp.put(4, "");
		 }
		 for(Map.Entry<Integer, String> entry : mapCellTemp.entrySet()){
			 vectorCellEachRowData.addElement(entry.getValue());
		 }
		 return vectorCellEachRowData;
	}
	public String displayTempStatusRecords(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			System.out.println("calling displayTempUserRecords ");
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			System.out.println("start"+start);
			System.out.println("end"+end);
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			/***** Sorting by  Stating ( Sekhar ) ******/
			List<StatusUploadTemp> statusUploadTempList	  =	new ArrayList<StatusUploadTemp>();
			
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"statustempId";
	
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				 {
					 sortOrderFieldName		=	sortOrder;
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion = Restrictions.eq("sessionid",session.getId());
			totalRecord = statusUploadTempDAO.getRowCountTempStatus(criterion);
			statusUploadTempList = statusUploadTempDAO.findByTempStatus(sortOrderStrVal,start,noOfRowInPage,criterion);
			
			
			/***** Sorting by Temp Teacher  End ******/
	
			dmRecords.append("<table  id='tempStatusTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLink("Talent Email",sortOrderFieldName,"talent_email",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Job Id",sortOrderFieldName,"job_id",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Node Name",sortOrderFieldName,"node_name",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Status",sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Errors",sortOrderFieldName,"errortext",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(statusUploadTempList.size()==0)
				dmRecords.append("<tr><td colspan='6' align='center'>No Status found</td></tr>" );
			System.out.println("No of Records "+statusUploadTempList.size());

			for (StatusUploadTemp statusUploadTemp : statusUploadTempList){
				String rowCss="";
				
				if(!statusUploadTemp.getErrortext().equalsIgnoreCase("")){
					rowCss="style=\"background-color:#FF0000;\"";
				}
				dmRecords.append("<tr >" );
				dmRecords.append("<td "+rowCss+">"+statusUploadTemp.getTalent_email()+"</td>");
				dmRecords.append("<td "+rowCss+">"+statusUploadTemp.getJob_id()+"</td>");
				dmRecords.append("<td "+rowCss+">"+statusUploadTemp.getNode_name()+"</td>");
				dmRecords.append("<td "+rowCss+">"+statusUploadTemp.getStatus()+"</td>");
				dmRecords.append("<td "+rowCss+">"+statusUploadTemp.getErrortext()+"</td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	public int deleteTempStatus(String sessionId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		System.out.println(":::::::::::::::::deleteTempStatus:::::::::::::::::"+sessionId);
		try{
			statusUploadTempDAO.deleteStatusTemp(sessionId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	public int saveStatus()
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession=null;
		HeadQuarterMaster headQuarterMaster=null;
		BranchMaster branchMaster=null;
		DistrictMaster districtMaster=null;
		
		
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }else{
	    	userSession=(UserMaster)session.getAttribute("userMaster");
		}
		System.out.println("::::::::saveStatus now:::::::::");
		int returnVal=0;
        try{
        	Criterion criterion1 = Restrictions.eq("sessionid",session.getId());
        	Criterion criterion2 = Restrictions.eq("errortext","");
        	List <StatusUploadTemp> statusUploadTemplst=statusUploadTempDAO.findByCriteria(criterion1,criterion2);
        	System.out.println("statusUploadTemplst:::::::"+statusUploadTemplst.size());
        	
        	if(statusUploadTemplst.size()==0)
        		return returnVal;
        	 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        	 List emails = new ArrayList();
        	 List jobs = new ArrayList();
        	 List<String> statusNameList=new ArrayList<String>();
        	 List<JobOrder> listJobOrders = new ArrayList<JobOrder>(); 
        	 Map<String,JobOrder> mapAllJobs = new HashMap<String, JobOrder>();
        	 int branchId=0;
        	 for(int e=0; e<statusUploadTemplst.size(); e++) {
        		 System.out.println("statusUploadTemplst.get(e).getJob_id()::::::::::::::"+statusUploadTemplst.get(e).getJob_id());
            		emails.add(statusUploadTemplst.get(e).getTalent_email());
            		jobs.add(statusUploadTemplst.get(e).getJob_id());
            		statusNameList.add(statusUploadTemplst.get(e).getNode_name());
            		if(e==0){
            			branchId=Integer.parseInt(statusUploadTemplst.get(e).getBranchId());
            		}
			 }
        	 branchMaster=branchMasterDAO.findById(branchId, false, false);
        	 System.out.println("jobs:::::::::"+jobs.size());
        	 
        	 List <TeacherDetail> teacherUploadTempList=teacherDetailDAO.findAllTeacherDetails(emails);
        	 List teacherIdList = new ArrayList();
        	 Map<String,TeacherDetail> teacherMap=new HashMap<String, TeacherDetail>();
	         for(int t=0; t<teacherUploadTempList.size(); t++) {
	        	 teacherIdList.add(teacherUploadTempList.get(t));
	        	 teacherMap.put(teacherUploadTempList.get(t).getEmailAddress(),teacherUploadTempList.get(t));
			 }
	        
	         List<JobForTeacher> jobForTeacherList=new ArrayList<JobForTeacher>();
	         if(teacherIdList.size()>0)
	        	 jobForTeacherList=jobForTeacherDAO.findJobByFilterTeacher(teacherIdList);
	         
	         Map<String,JobForTeacher> jftMap=new HashMap<String, JobForTeacher>();
	         for (JobForTeacher jobForTeacher : jobForTeacherList) {
	        	 if(jobForTeacher.getJobId().getBranchMaster()!=null){
	        		 if(jobForTeacher.getJobId().getBranchMaster().getBranchId()==branchId){
	        			 jftMap.put(jobForTeacher.getTeacherId().getTeacherId()+"##"+jobForTeacher.getJobId().getApiJobId(),jobForTeacher);
	        		 }
	        	 }
			 }
        	 Map<Integer,JobOrder> jobOrderMap = jobOrderDAO.findByAllJobsWithBranch(jobs,branchMaster);
        	 System.out.println("jobOrderMap::::::::::"+jobOrderMap.size());
        	 List<JobOrder> jobOrderList=new ArrayList<JobOrder>();
        	 List<JobCategoryMaster> jobCategoryMasters=new ArrayList<JobCategoryMaster>();
        	 for(Map.Entry<Integer,JobOrder> entry : jobOrderMap.entrySet()){
        		 jobOrderList.add(entry.getValue());
        		 if(entry.getValue().getJobCategoryMaster().getParentJobCategoryId()!=null){
        			 jobCategoryMasters.add(entry.getValue().getJobCategoryMaster().getParentJobCategoryId());
        		 }else{
        			 jobCategoryMasters.add(entry.getValue().getJobCategoryMaster());
        		 }
    		 }
        	 System.out.println("jobCategoryMasters::::"+jobCategoryMasters.size());
        	 System.out.println("jobOrderList::::"+jobOrderList.size());
        	 System.out.println("teacherUploadTempList::::"+teacherUploadTempList.size());
        	 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        	 
			List<TeacherStatusHistoryForJob> historyList =teacherStatusHistoryForJobDAO.findHistoryByTeachersAndJobs(teacherUploadTempList,jobOrderList);
			System.out.println("historyList:::::::::::::::"+historyList.size());
			
        	 
        	 ////////////////////////////////////////////////////////////////////////
        	 List<SecondaryStatus> secondaryStatusList= secondaryStatusDAO.findSecondaryStatusByNames(jobCategoryMasters,statusNameList);
        	 System.out.println("secondaryStatusList::::::::::"+secondaryStatusList.size());
        	 System.out.println("jobOrderList   :::::::::::"+jobOrderList);
        	 Map<String,SecondaryStatus> mapStatusNames=new HashMap<String, SecondaryStatus>();
        	 for (SecondaryStatus secondaryStatus : secondaryStatusList) {
    			 System.out.println("::::>>>"+secondaryStatus.getSecondaryStatusName()+"##"+secondaryStatus.getJobCategoryMaster().getJobCategoryId());
    			 mapStatusNames.put(secondaryStatus.getSecondaryStatusName()+"##"+secondaryStatus.getJobCategoryMaster().getJobCategoryId(),secondaryStatus);
			 }
        	/* List<SecondaryStatus> secondaryStatusDeafultList= secondaryStatusDAO.findSecondaryStatusByNamesWithDefault(statusNameList,branchMaster);
        	 System.out.println("secondaryStatusDeafultList::::::::::"+secondaryStatusDeafultList.size());
        	 for (SecondaryStatus secondaryStatus : secondaryStatusDeafultList) {
        		 System.out.println(":::>>:"+secondaryStatus.getSecondaryStatusName()+"##0");
        		 mapStatusNames.put(secondaryStatus.getSecondaryStatusName()+"##0",secondaryStatus);	 
			 }*/
        	 System.out.println("mapStatusNames:::::::::::::::::::::"+mapStatusNames.size());
        	 /**** Session dunamic ****/
        	 SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
			 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
        	 Transaction txOpen =statelesSsession.beginTransaction();
        
    	 	List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
			Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
			for (StatusMaster statusMaster : statusMasterList) {
				mapStatus.put(statusMaster.getStatusShortName(),statusMaster);
			}
			
			TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
			Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 
			List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobOrderList);
			List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
			Map<String,TeacherAssessmentStatus> mapOfTeacherAssessmentStatusByAssessmentDetail = new HashMap<String, TeacherAssessmentStatus>();
			Map<Integer,List<TeacherAssessmentStatus>> mapStatusForTeacher=new HashMap<Integer, List<TeacherAssessmentStatus>>();
			if(assessmentJobRelations1.size()>0){
				for(AssessmentJobRelation ajr: assessmentJobRelations1){
					mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
					adList.add(ajr.getAssessmentId());
				}
				if(adList.size()>0){
					List<TeacherAssessmentStatus> lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findJSITakenByAssessMentListAndTeacherLists(teacherUploadTempList, adList);
					if(lstTeacherAssessmentStatus!=null && lstTeacherAssessmentStatus.size()>0){
						for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus){
							int teacherId= tas.getTeacherDetail().getTeacherId();
							List<TeacherAssessmentStatus> lstAssStatus=mapStatusForTeacher.get(teacherId);
							if(lstAssStatus==null){
								List<TeacherAssessmentStatus> assStatusList=new ArrayList<TeacherAssessmentStatus>();
								assStatusList.add(tas);
								mapStatusForTeacher.put(teacherId, assStatusList);
							}else{
								lstAssStatus.add(tas);
								mapStatusForTeacher.put(teacherId, lstAssStatus);
							}
							mapOfTeacherAssessmentStatusByAssessmentDetail.put(tas.getTeacherDetail().getTeacherId()+"#"+tas.getAssessmentDetail().getAssessmentId(), tas);
						}
					}
				}
			}
			DistrictMaster districtMasterInternal=null;
			HashMap<Integer,InternalTransferCandidates> mapLstITRA = internalTransferCandidatesDAO.getDistrictForTeacherShowSatus(teacherUploadTempList);
			
			
			List<TeacherAssessmentStatus> teacherAssessmentStatusList=new ArrayList<TeacherAssessmentStatus>();
	         
	         if(teacherIdList.size()>0)
	        	 teacherAssessmentStatusList=teacherAssessmentStatusDAO.findByAllTeacherAssessmentStatus(teacherIdList);
	         
	         Map<Integer,TeacherAssessmentStatus> mapAssStatus=new HashMap<Integer, TeacherAssessmentStatus>();
	         
	         if(teacherAssessmentStatusList!=null)
	         for (TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList) {
	        	 mapAssStatus.put(teacherAssessmentStatus.getTeacherDetail().getTeacherId(),teacherAssessmentStatus);
			}
			
	         List<TeacherPortfolioStatus> teacherPortfolioStatusList= new ArrayList<TeacherPortfolioStatus>();
	         
	         if(teacherIdList.size()>0)
	        	 teacherPortfolioStatusList=teacherPortfolioStatusDAO.findAllTeacherPortfolioStatus(teacherIdList);
	         
	         Map<Integer,TeacherPortfolioStatus> mapPortStatus=new HashMap<Integer, TeacherPortfolioStatus>();
	         if(teacherPortfolioStatusList!=null)
	         for (TeacherPortfolioStatus teacherPortfolioStatus : teacherPortfolioStatusList) {
	        	 mapPortStatus.put(teacherPortfolioStatus.getTeacherId().getTeacherId(),teacherPortfolioStatus);
			 }
	     	Map<String,String> cmpMap = new HashMap<String, String>();
	     	Map<String,TeacherStatusHistoryForJob> historyObjMap = new HashMap<String, TeacherStatusHistoryForJob>();
			for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
				if(teacherStatusHistoryForJob.getSecondaryStatus()!=null)
				{		
					cmpMap.put("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId(),teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName());
					historyObjMap.put("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId(),teacherStatusHistoryForJob);
				}
				if(teacherStatusHistoryForJob.getStatusMaster()!=null && !teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equalsIgnoreCase("vcomp"))
				{
					cmpMap.put(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0",teacherStatusHistoryForJob.getStatusMaster().getStatus());
					historyObjMap.put(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0",teacherStatusHistoryForJob);
				}
			}		
    	 
		    CandidateGridService cgService=new CandidateGridService();
		    DashboardAjax dashboardAjax=new DashboardAjax();
        	List<UserMailSend> userMailSendList =new ArrayList<UserMailSend>();   
        	System.out.println("statusUploadTemplst:::::::::::::::::"+statusUploadTemplst.size());
        	List<String> emailList=new ArrayList<String>();
        	
        	System.out.println("teacherMap:::::::;;"+teacherMap.size());
        	
        	
        	for(StatusUploadTemp statusUploadTemp : statusUploadTemplst){
        		System.out.println("statusUploadTemp.getTalent_email():::>>>:::::::::"+statusUploadTemp.getTalent_email());
        		TeacherDetail teacherDetail=teacherMap.get(statusUploadTemp.getTalent_email());
        		//saveJobForTeacher(mapStatus,statelesSsession,jobOrderList,lstAssessmentJobRelation,mapAssStatus,jobForTeacherList,teacherPortfolioStatusList,statusMasterList,statusUploadTemp.getJob_id(),teacherDetail);
        		System.out.println("teacherDetails::::::::"+teacherDetail);
        		districtMasterInternal=null;
        		InternalTransferCandidates internalITRA = null;
				if(mapLstITRA.size()>0){
					internalITRA=mapLstITRA.get(teacherDetail.getTeacherId());
					if(internalITRA!=null)
					districtMasterInternal=internalITRA.getDistrictMaster();
				}
				TeacherPortfolioStatus tPortfolioStatus =null;
				if(teacherDetail!=null)
					tPortfolioStatus=mapPortStatus.get(teacherDetail.getTeacherId());
				
				lstJSI=mapStatusForTeacher.get(teacherDetail.getTeacherId());
				if(lstJSI!=null)
				for(TeacherAssessmentStatus ts : lstJSI){
					mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
				}
				String jftKey=teacherDetail.getTeacherId()+"##"+statusUploadTemp.getJob_id();
				JobForTeacher jobForTeacher=jftMap.get(jftKey);
				StatusMaster mainStatusMaster = mapStatus.get("icomp");
				mainStatusMaster=dashboardAjax.findByTeacherIdJobStausForLoop(internalITRA,jobForTeacher,mapAssess,teacherAssessmentStatusJSI,mapJSI,districtMasterInternal,tPortfolioStatus,mapStatus,mapAssStatus.get(teacherDetail.getTeacherId()));
 				
 				System.out.println("mainStatusMaster::::::::;>>>>"+mainStatusMaster.getStatus());
 				String statusKey="";
 				if(jobForTeacher.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
 					statusKey=statusUploadTemp.getNode_name()+"##"+jobForTeacher.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId();
 				}else{
 					statusKey=statusUploadTemp.getNode_name()+"##"+jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryId();
 				}
 				System.out.println("statusKey:::::"+statusKey);
 				SecondaryStatus secondaryStatus=mapStatusNames.get(statusKey);
 				if(secondaryStatus==null){
 					statusKey=statusUploadTemp.getNode_name()+"##0";
 					secondaryStatus=mapStatusNames.get(statusKey);
 				}
 				StatusMaster statusMaster=null;
 				if(secondaryStatus!=null && secondaryStatus.getStatusMaster()!=null){
 					statusMaster=secondaryStatus.getStatusMaster();
 				}
 				
 				//System.out.println("secondaryStatus::::::::::::"+secondaryStatus.getSecondaryStatusId());
 				
 				boolean selectedstatus=false;
 				boolean selectedSecondaryStatus=false;
 				try{
 					if(jobForTeacher.getStatus()!=null && statusMaster!=null){
 						selectedstatus=cgService.selectedStatusCheck(statusMaster,jobForTeacher.getStatus());
 					}
 				}catch(Exception e){
 					e.printStackTrace();
 				}
 				System.out.println("selectedstatus::::::::1:"+selectedstatus);
 			    try{
 					if(secondaryStatus!=null && jobForTeacher.getStatus()!=null &&  statusMaster!=null){
 						System.out.println("1:");
 						if(jobForTeacher.getSecondaryStatus()!=null){
 							System.out.println("2:");
 							selectedSecondaryStatus=cgService.selectedPriSecondaryStatusCheck(secondaryStatus,jobForTeacher.getSecondaryStatus());
 							if(selectedSecondaryStatus==false){
 								System.out.println("3:");
 								selectedstatus=false;
 							}
 						}
 					}else if(jobForTeacher.getSecondaryStatus()!=null && secondaryStatus!=null && statusMaster==null){
 						System.out.println("4:");
 						if(jobForTeacher.getStatus()!=null){
 							System.out.println("5:");
 							if(cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),secondaryStatus)){
 								System.out.println("6:");
 								selectedSecondaryStatus=cgService.selectedNotPriSecondaryStatusCheck(secondaryStatus,jobForTeacher.getSecondaryStatus());
 							}
 						}
 					}else if(jobForTeacher.getSecondaryStatus()==null && secondaryStatus!=null && statusMaster==null){
 						System.out.println("7:");
 						if(jobForTeacher.getStatus()!=null){
 							System.out.println("8:");
 							if(cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),secondaryStatus)){
 								System.out.println("9:");
 								selectedSecondaryStatus=true;
 							}
 						}
 					}
 				}catch(Exception e){
 					e.printStackTrace();
 				}
 				System.out.println("::::::selectedstatus::::::::;;>>>>>>>>>>>>>>>>>>>>>>>>>>>;>>"+selectedstatus);
 				System.out.println(":::::::;;selectedSecondaryStatus::::::::;;;>>"+selectedSecondaryStatus);
 				
 				String historyMKey="";
 				if(statusMaster!=null){
 					System.out.println("10:");
 					historyMKey=statusMaster.getStatusId()+"##0";
 				}else{
 					System.out.println("11:");
 					if(secondaryStatus!=null){
 						System.out.println("12:");
 						historyMKey="0##"+secondaryStatus.getSecondaryStatusId();
 					}
 				}
 				boolean historyExist=false;
 				if(cmpMap.get(historyMKey)!=null){
 					historyExist=true;
 				}
 				System.out.println("historyExist::::::::::::::::::::::"+historyExist);
 				boolean isUpdateStatus=cgService.isUpdateStatus(jobForTeacher,userSession,null);
 				System.out.println("isUpdateStatus:::::::::::::"+isUpdateStatus);
 				if(!statusUploadTemp.getStatus().equalsIgnoreCase("undo")){
	 				if(selectedstatus && statusMaster!=null && (statusMaster.getStatusShortName().equalsIgnoreCase("scomp") || statusMaster.getStatusShortName().equalsIgnoreCase("ecomp") || statusMaster.getStatusShortName().equalsIgnoreCase("vcomp"))){
	 					System.out.println("13:");
						jobForTeacher.setStatus(statusMaster);
						if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
							jobForTeacher.setStatusMaster(null);
						}else{
							jobForTeacher.setStatusMaster(statusMaster);
						}
						
						
						jobForTeacher.setUpdatedBy(userSession);
						jobForTeacher.setUpdatedByEntity(userSession.getEntityType());		
						jobForTeacher.setUpdatedDate(new Date());
						jobForTeacher.setLastActivity(statusMaster.getStatus());
						jobForTeacher.setLastActivityDate(new Date());
						jobForTeacher.setUserMaster(userSession);
						statelesSsession.update(jobForTeacher);
						System.out.println(":::::::::::::::Update JobForTeacher -:::::::::::");
						
						
						if(!historyExist){
							System.out.println("14:");
							TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
							tSHJ.setTeacherDetail(teacherDetail);
							tSHJ.setJobOrder(jobForTeacher.getJobId());
							tSHJ.setStatusMaster(statusMaster);
							if(statusUploadTemp.getStatus().equalsIgnoreCase("waived")){
								tSHJ.setStatus("W");
							}else{
								tSHJ.setStatus("S");
							}
							tSHJ.setCreatedDateTime(new Date());
							tSHJ.setUserMaster(userSession);
							statelesSsession.insert(tSHJ);
						}
					}else if(selectedstatus && statusMaster!=null && statusMaster.getStatusShortName().equalsIgnoreCase("hird")){
						System.out.println("15:");
						jobForTeacher.setStatus(statusMaster);
						if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
							jobForTeacher.setStatusMaster(null);
						}else{
							jobForTeacher.setStatusMaster(statusMaster);
						}
						jobForTeacher.setUpdatedBy(userSession);
						jobForTeacher.setUpdatedByEntity(userSession.getEntityType());		
						jobForTeacher.setUpdatedDate(new Date());
						jobForTeacher.setLastActivity(statusMaster.getStatus());
						jobForTeacher.setLastActivityDate(new Date());
						jobForTeacher.setUserMaster(userSession);
						statelesSsession.update(jobForTeacher);
						System.out.println(":::::::::::::::Update JobForTeacher -:::::::::::");
						if(!historyExist){
							System.out.println("16:");
							TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
							tSHJ.setTeacherDetail(teacherDetail);
							tSHJ.setJobOrder(jobForTeacher.getJobId());
							tSHJ.setStatusMaster(statusMaster);
							if(statusUploadTemp.getStatus().equalsIgnoreCase("waived")){
								tSHJ.setStatus("W");
							}else{
								tSHJ.setStatus("A");
							}
							tSHJ.setCreatedDateTime(new Date());
							tSHJ.setUserMaster(userSession);
							statelesSsession.insert(tSHJ);
						}
					}else if(selectedSecondaryStatus){
						System.out.println("17:");
						System.out.println(":::::::::::::::Secondary Staus Update JobForTeacher -:::::::::::");
						if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),secondaryStatus)){
							jobForTeacher.setStatusMaster(null);
						}else{
							jobForTeacher.setStatusMaster(statusMaster);
						}
						jobForTeacher.setSecondaryStatus(secondaryStatus);
						jobForTeacher.setUpdatedBy(userSession);
						jobForTeacher.setUpdatedByEntity(userSession.getEntityType());		
						jobForTeacher.setUpdatedDate(new Date());
						jobForTeacher.setLastActivity(secondaryStatus.getSecondaryStatusName());
						jobForTeacher.setLastActivityDate(new Date());
						jobForTeacher.setUserMaster(userSession);
						statelesSsession.update(jobForTeacher);
						if(!historyExist){
							System.out.println("18:");
							TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
							tSHJ.setTeacherDetail(teacherDetail);
							tSHJ.setJobOrder(jobForTeacher.getJobId());
							tSHJ.setSecondaryStatus(secondaryStatus);
							if(statusUploadTemp.getStatus().equalsIgnoreCase("waived")){
								tSHJ.setStatus("W");
							}else{
								tSHJ.setStatus("S");
							}
							tSHJ.setCreatedDateTime(new Date());
							tSHJ.setUserMaster(userSession);
							statelesSsession.insert(tSHJ);
						}
					}else {
						if(statusUploadTemp.getStatus().equalsIgnoreCase("complete")){
							try{
								if(historyObjMap.get(historyMKey)!=null){
									TeacherStatusHistoryForJob statusHistoryForWaived=historyObjMap.get(historyMKey);
									System.out.println(":::::::::::statusHistoryForWaived::::::::>:::::::");
									if(statusHistoryForWaived!=null && statusHistoryForWaived.getStatus().equalsIgnoreCase("W")){
										System.out.println(":::::::::::statusHistoryForWaived::::::::Updated Complete:::::::");
										statusHistoryForWaived.setStatus("S");
										statusHistoryForWaived.setUserMaster(userSession);
										statusHistoryForWaived.setUpdatedDateTime(new Date());
										statelesSsession.update(statusHistoryForWaived);
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
 				}else{
 					System.out.println(":::::::::::::::Undo status ::::::::::::::::::::::::::::::::");
 					try{
	 					 /*********Undo Status ************/
						StatusMaster masterForDPoint=null;
						try{
							if(statusMaster!=null && (statusMaster.getStatusShortName().equalsIgnoreCase("scomp") || statusMaster.getStatusShortName().equalsIgnoreCase("ecomp") || statusMaster.getStatusShortName().equalsIgnoreCase("vcomp"))){
								masterForDPoint=statusMaster;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						List<TeacherStatusScores> teacherStatusScoresForUndoList= teacherStatusScoresDAO.getFinalizeStatusScore(teacherDetail,jobForTeacher.getJobId(),masterForDPoint,secondaryStatus);
						System.out.println("teacherStatusScoresForUndoList:::::::::"+teacherStatusScoresForUndoList.size());
						try{
							if(teacherStatusScoresForUndoList!=null && teacherStatusScoresForUndoList.size()>0){
								for (TeacherStatusScores teacherStatusScores : teacherStatusScoresForUndoList) {
									teacherStatusScoresDAO.makeTransient(teacherStatusScores);
								}
								JobWiseConsolidatedTeacherScore jwScoreObj=jobWiseConsolidatedTeacherScoreDAO.getJWCTScore(teacherDetail, jobForTeacher.getJobId());
								if(jwScoreObj!=null){
									try{
										Double[] teacherStatusScoresAvg= teacherStatusScoresDAO.getAllFinalizeStatusScoreAvg(teacherDetail,jobForTeacher.getJobId());
										jwScoreObj.setJobWiseConsolidatedScore(teacherStatusScoresAvg[0]);
										jwScoreObj.setJobWiseMaxScore(teacherStatusScoresAvg[1]);
									}catch(Exception e){ e.printStackTrace();}
									jobWiseConsolidatedTeacherScoreDAO.updatePersistent(jwScoreObj);
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						try{
							List<TeacherStatusHistoryForJob>  statusHistoryObjList=teacherStatusHistoryForJobDAO.findByTeacherStatusForUndo(teacherDetail,jobForTeacher.getJobId(),masterForDPoint,secondaryStatus);
							System.out.println("statusHistoryObjList::::::::"+statusHistoryObjList.size());
							for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : statusHistoryObjList) {
								teacherStatusHistoryForJob.setStatus("I");
								teacherStatusHistoryForJob.setHiredByDate(null);
								teacherStatusHistoryForJob.setUpdatedDateTime(new Date());
								teacherStatusHistoryForJob.setUserMaster(userSession);
								statelesSsession.update(teacherStatusHistoryForJob);
							}
	
						}catch(Exception e){
							e.printStackTrace();
						}
						commonService.undoJobStatus(jobForTeacher);
 					}catch(Exception e){
 						e.printStackTrace();
 					}
 				}
 				System.out.println(":::::::::::::::Complete Here ::::::::::::::::::::::::::::::::");
 			}
	       	txOpen.commit();
	       	statelesSsession.close();
	       	try{
		       	//mailSendByThread(userMailSendList);
	        	deleteXlsAndXlsxFile(request,session.getId());
	        	statusUploadTempDAO.deleteAllStatusTemp(session.getId());
	       	}catch(Exception e){
	       		e.printStackTrace();
	       	}
		}catch (Exception e){
			System.out.println("Error >"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			return returnVal;
		}
	}
	public JobOrder saveJobForTeacher(Map<String, StatusMaster> mapStatus,StatelessSession statelesSsession,Map<Integer,JobOrder> jobOrderList,List<AssessmentJobRelation> assessmentJobRelationLst,Map<Integer,TeacherAssessmentStatus> mapAssStatus,List<JobForTeacher> jobForTeacherList,List<TeacherPortfolioStatus> teacherPortfolioStatusList,List <StatusMaster> statusMasterList,String jobId,TeacherDetail teacherDetail)
	{
		System.out.println("::::::::::::saveJobForTeacher::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
			String coverLetter ="",redirectedURL="";
			Integer districtid1=(Integer) session.getAttribute("Did");
			//******** Adding By Deepak   ****************************
			Integer  headQuarterId1=(Integer) session.getAttribute("Hid");
			Integer branchId1=(Integer) session.getAttribute("Bid");
			//********* End   ********************************
			
			//System.out.println("districtid1:::::::::::  "+districtid1+"       headQuarterId1::::::"+headQuarterId1+"  branchId1:::"+branchId1+"    jobId::::"+jobId);
			JobOrder jobOrder=null;			
			/*  jobOrder = findJobByApiJobId(jobOrderList,jobId,districtid1,headQuarterId1,branchId1);
			System.out.println(jobOrder.getJobId());
			//System.out.println("jobForTeacherList.size()::::"+jobForTeacherList.size()+"   ,teacherDetail    ::::"+teacherDetail.getTeacherId()+" ,jobOrder :::"+jobOrder.getApiJobId());
			JobForTeacher jobForTeacher =findJobByTeacherAndJob(jobForTeacherList,teacherDetail, jobOrder);
			if(jobForTeacher==null)
			{
				TeacherPortfolioStatus teacherPortfolioStatus = findPortfolioStatusByTeacher(teacherPortfolioStatusList,teacherDetail);
				jobForTeacher = new JobForTeacher();
				jobForTeacher.setTeacherId(teacherDetail);
				jobForTeacher.setJobId(jobOrder);
				jobForTeacher.setCoverLetter(coverLetter);
				jobForTeacher.setCreatedDateTime(new Date());
				
				if(createddDate!=null){
					jobForTeacher.setCreatedDateTime(createddDate);
				}
				StatusMaster statusMaster = null;
				statusMaster = mapStatus.get("icomp");
				AssessmentDetail assessmentDetail = null;
				TeacherAssessmentStatus teacherAssessmentStatus =mapAssStatus.get(jobForTeacher.getTeacherId());
				System.out.println(" job Category :: "+jobOrder.getJobCategoryMaster());
				if(!jobOrder.getJobCategoryMaster().getBaseStatus()){
					if(jobOrder.getIsJobAssessment()){
						List<AssessmentJobRelation> lstAssessmentJobRelation = findRelationByJobOrder(jobOrderList,assessmentJobRelationLst,jobOrder);
						if(lstAssessmentJobRelation.size()>0)
						{
							AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
							assessmentDetail = assessmentJobRelation.getAssessmentId();
							List<TeacherAssessmentStatus> lstTeacherAssessmentStatus =findTASByTeacherAndAssessment(teacherAssessmentStatusLst,teacherDetail, assessmentDetail);
							
							if(lstTeacherAssessmentStatus.size()>0)
							{						
								teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
								teacherAssessmentStatus.setTeacherAssessmentStatusId(null);
								teacherAssessmentStatus.setJobOrder(jobOrder);
								teacherAssessmentStatus.setCreatedDateTime(new Date());
								statelesSsession.insert(teacherAssessmentStatus);
								statusMaster = teacherAssessmentStatus.getStatusMaster();
							}
							else
							{
								statusMaster =mapStatus.get("icomp");
							}					
						}
						else
						{
							statusMaster = mapStatus.get("icomp");
						}
					}
					else{
						statusMaster =mapStatus.get("comp");
					}
				}
				else if(jobOrder.getIsJobAssessment()){
					List<AssessmentJobRelation> lstAssessmentJobRelation = findRelationByJobOrder(jobOrderList,assessmentJobRelationLst,jobOrder);;
					if(lstAssessmentJobRelation.size()>0)
					{
						AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
						assessmentDetail = assessmentJobRelation.getAssessmentId();
						List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = findTASByTeacherAndAssessment(teacherAssessmentStatusLst,teacherDetail, assessmentDetail);
						
						if(lstTeacherAssessmentStatus.size()>0)
						{						
							teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
							teacherAssessmentStatus.setTeacherAssessmentStatusId(null);
							teacherAssessmentStatus.setJobOrder(jobOrder);
							teacherAssessmentStatus.setCreatedDateTime(new Date());
							statelesSsession.insert(teacherAssessmentStatus);
							statusMaster = teacherAssessmentStatus.getStatusMaster();
						}
						else
						{
							statusMaster = mapStatus.get("icomp");
						}					
					}
					else
					{
						statusMaster = mapStatus.get("icomp");
					}
				}else{
					List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
					JobOrder jOrder = new JobOrder();
					jOrder.setJobId(0);
					teacherAssessmentStatusList =findAssessmentTaken(teacherAssessmentStatusLst,jobForTeacher.getTeacherId(),jOrder);
					if(teacherAssessmentStatusList.size()>0){
						teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
						statusMaster = teacherAssessmentStatus.getStatusMaster();
					}else{
						statusMaster = mapStatus.get("icomp");
					}
				}
				jobForTeacher.setIsAffilated(0);
				jobForTeacher.setRedirectedFromURL(redirectedURL);
				jobForTeacher.setStatus(statusMaster);
				jobForTeacher.setStatusMaster(statusMaster);
				statelesSsession.insert(jobForTeacher);
				//jobForTeacherDAO.makePersistent(jobForTeacher);				
			}*/
		return jobOrder;
	}
	public void mailSendByThread(List<UserMailSend> userMailSendList){
		MailSend mst =new MailSend(userMailSendList);
		Thread currentThread = new Thread(mst);
		currentThread.start(); 
	}
	public class MailSend  extends Thread{
		
		List<UserMailSend> userMailSendListTrdVal=new ArrayList<UserMailSend>();
		public MailSend(List<UserMailSend> userMailSendList){
			userMailSendListTrdVal=userMailSendList;
		 }
		 public void run() {
				try {
					Thread.sleep(1000);
					System.out.println("::Trd size::"+userMailSendListTrdVal.size());
					for(UserMailSend userMailSend :userMailSendListTrdVal){
						//emailerService.sendMailAsHTMLText(userMailSend.getEmail(), "I Have Added You As a User",MailText.createUserForUpload(userMailSend.getUserMasterForOther(),userMailSend.getUserMaster()));
					}
				}catch (NullPointerException en) {
					en.printStackTrace();
				}catch (InterruptedException e) {
					e.printStackTrace();
				}
		  }
	}
	public void deleteXlsAndXlsxFile(HttpServletRequest request,String sessionId){
		List<StatusUploadTemp> teacherUploadTemplst= statusUploadTempDAO.findByAllTempStatus();
		String root = request.getRealPath("/")+"/uploads/";
		for(StatusUploadTemp statusUploadTemp : teacherUploadTemplst){
			String filePath="";
			File file=null;
			try{
				filePath=root+statusUploadTemp.getSessionid()+".xlsx";
				file = new File(filePath);
				file.delete();
			}catch(Exception e){}
			try{
				filePath=root+statusUploadTemp.getSessionid()+".xls";
				file = new File(filePath);
				file.delete();
			}catch(Exception e){}
		}
		
		String filePath="";
		File file=null;
		try{
			filePath=root+sessionId+".xlsx";
			file = new File(filePath);
			file.delete();
		}catch(Exception e){}
		try{
			filePath=root+sessionId+".xls";
			file = new File(filePath);
			file.delete();
		}catch(Exception e){}
	}
}


