package tm.services.teacher;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.PortfolioRemindersDspq;
import tm.bean.PraxisMaster;
import tm.bean.TeacherAdditionalDocuments;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherExperience;
import tm.bean.TeacherGeneralKnowledgeExam;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherSecondaryStatus;
import tm.bean.TeacherSubjectAreaExam;
import tm.bean.TeacherVideoLink;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.CertificationStatusMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.bean.master.DistrictSpecificPortfolioOptions;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.SubjectAreaExamMaster;
import tm.bean.master.TFAAffiliateMaster;
import tm.bean.master.TFARegionMaster;
import tm.bean.master.UniversityMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.PortfolioRemindersDspqDAO;
import tm.dao.PraxisMasterDAO;
import tm.dao.TeacherAdditionalDocumentsDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherGeneralKnowledgeExamDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherSecondaryStatusDAO;
import tm.dao.TeacherSubjectAreaExamDAO;
import tm.dao.TeacherVideoLinksDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.CertificationStatusMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificPortfolioAnswersDAO;
import tm.dao.master.DistrictSpecificPortfolioOptionsDAO;
import tm.dao.master.DistrictSpecificPortfolioQuestionsDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.SubjectAreaExamMasterDAO;
import tm.dao.master.TFAAffiliateMasterDAO;
import tm.dao.master.TFARegionMasterDAO;
import tm.dao.master.UniversityMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.PaginationAndSorting;
import tm.utility.Utility;

public class PFCertifications 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	 String lblNm=Utility.getLocaleValuePropByKey("lblNm", locale);
	 String lblSt=Utility.getLocaleValuePropByKey("lblSt", locale);
	 String lblYearRece=Utility.getLocaleValuePropByKey("lblYearRece", locale);
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	 String lblSource=Utility.getLocaleValuePropByKey("lblSource", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String lbllDocName=Utility.getLocaleValuePropByKey("lbllDocName", locale);
	 String lblDoc=Utility.getLocaleValuePropByKey("lblDoc", locale);
	 String lnkViLnk=Utility.getLocaleValuePropByKey("lblDoc", locale);
	 String lblVid=Utility.getLocaleValuePropByKey("lblVid", locale);
	 String lblcretedDate=Utility.getLocaleValuePropByKey("lblcretedDate", locale);
	 String lblRefName=Utility.getLocaleValuePropByKey("lblRefName", locale);
	 String lblTitle=Utility.getLocaleValuePropByKey("lblTitle", locale);
	 String lblOrganizationEmp=Utility.getLocaleValuePropByKey("lblOrganizationEmp", locale);
	 String lblEmail=Utility.getLocaleValuePropByKey("lblEmail", locale);
	 String lblRecLetter=Utility.getLocaleValuePropByKey("lblRecLetter", locale);
	 String lblContactNo=Utility.getLocaleValuePropByKey("lblContactNo", locale);
	 String lblCanContact=Utility.getLocaleValuePropByKey("lblCanContact", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String lnkDlt=Utility.getLocaleValuePropByKey("lnkDlt", locale);
	 String lblActivate=Utility.getLocaleValuePropByKey("lblActivate", locale);
	 String lblDeactivate=Utility.getLocaleValuePropByKey("lblDeactivate", locale);
	 String msgNorecordfound=Utility.getLocaleValuePropByKey("msgNorecordfound", locale);
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lnkClickHereToViewDoc=Utility.getLocaleValuePropByKey("lnkClickHereToViewDoc", locale);
	 String lblSchoolName=Utility.getLocaleValuePropByKey("lblSchoolName", locale);
	 String lblOrga=Utility.getLocaleValuePropByKey("lblOrga", locale);
	 String lblCertificationDetail=Utility.getLocaleValuePropByKey("lblCertificationDetail", locale);
	 String msgReasonableAccommodationNotice1=Utility.getLocaleValuePropByKey("msgReasonableAccommodationNotice1", locale);
	 String msgReasonableAccommodationNotice2=Utility.getLocaleValuePropByKey("msgReasonableAccommodationNotice2", locale);
	 String msgReasonableAccommodationNotice3=Utility.getLocaleValuePropByKey("msgReasonableAccommodationNotice3", locale);
	 String msgReasonableAccommodationNotice4=Utility.getLocaleValuePropByKey("msgReasonableAccommodationNotice4", locale);
	 String lnkViewAnswer=Utility.getLocaleValuePropByKey("lnkViewAnswer", locale);
	 String lnkV=Utility.getLocaleValuePropByKey("lnkV", locale);
	 String msgViewScoreReport=Utility.getLocaleValuePropByKey("msgViewScoreReport", locale);
	 String toolViewRecommendationLetter=Utility.getLocaleValuePropByKey("toolViewRecommendationLetter", locale);
	 String toolViewScurec=Utility.getLocaleValuePropByKey("toolViewScurec", locale);
	 
	 
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private DistrictSpecificPortfolioQuestionsDAO districtSpecificPortfolioQuestionsDAO;
	
	@Autowired
	private DistrictSpecificPortfolioAnswersDAO districtSpecificPortfolioAnswersDAO; 
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private TeacherSubjectAreaExamDAO teacherSubjectAreaExamDAO;
	
	@Autowired
	private TeacherGeneralKnowledgeExamDAO teacherGeneralKnowledgeExamDAO;
	
	@Autowired
	private TeacherAdditionalDocumentsDAO teacherAdditionalDocumentsDAO;
	
	@Autowired
	private CertificationStatusMasterDAO certificationStatusMasterDAO;
	
	@Autowired
	public DistrictMasterDAO districtMasterDAO; 
	
	@Autowired
	public PortfolioRemindersDspqDAO portfolioRemindersDspqDAO; 
	public void setPortfolioRemindersDspqDAO(PortfolioRemindersDspqDAO portfolioRemindersDspqDAO) 
	{
		this.portfolioRemindersDspqDAO = portfolioRemindersDspqDAO;
	}
	
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	public void setTeacherCertificateDAO(TeacherCertificateDAO teacherCertificateDAO) 
	{
		this.teacherCertificateDAO = teacherCertificateDAO;
	}
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) 
	{
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}
	
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO) 
	{
		this.teacherExperienceDAO = teacherExperienceDAO;
	}
	
	@Autowired
	private PraxisMasterDAO praxisMasterDAO;
	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
		
	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	public void setTeacherElectronicReferencesDAO(
			TeacherElectronicReferencesDAO teacherElectronicReferencesDAO) {
		this.teacherElectronicReferencesDAO = teacherElectronicReferencesDAO;
	}
	
	@Autowired
	private TeacherVideoLinksDAO teacherVideoLinksDAO;
	public void setTeacherVideoLinksDAO(
			TeacherVideoLinksDAO teacherVideoLinksDAO) {
		this.teacherVideoLinksDAO = teacherVideoLinksDAO;
	}
	
	@Autowired
	private TFAAffiliateMasterDAO tfaAffiliateMasterDAO;
	public void setTfaAffiliateMasterDAO(TFAAffiliateMasterDAO tfaAffiliateMasterDAO) {
		this.tfaAffiliateMasterDAO = tfaAffiliateMasterDAO;
	}
	
	@Autowired
	private TFARegionMasterDAO tfaRegionMasterDAO;
	public void setTfaRegionMasterDAO(TFARegionMasterDAO tfaRegionMasterDAO) {
		this.tfaRegionMasterDAO = tfaRegionMasterDAO;
	}
	
	@Autowired
	private SubjectAreaExamMasterDAO subjectAreaExamMasterDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private DistrictSpecificPortfolioOptionsDAO districtSpecificPortfolioOptionsDAO;
	
	@Autowired
	private TeacherSecondaryStatusDAO teacherSecondaryStatusDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	@Autowired
	private UniversityMasterDAO universityMasterDAO;
	
	public String getPFCertificationsGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"certType";
			String sortOrderNoField		=	"certType";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("state")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("state")){
					 sortOrderNoField="state";
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			
			List<TeacherCertificate> listTeacherCertificates = null;
			listTeacherCertificates = teacherCertificateDAO.findSortedCertificateByTeacher(sortOrderStrVal,teacherDetail);
			
			List<TeacherCertificate> sortedlistTeacherCertificates		=	new ArrayList<TeacherCertificate>();
			
			SortedMap<String,TeacherCertificate>	sortedMap = new TreeMap<String,TeacherCertificate>();
			if(sortOrderNoField.equals("state"))
			{
				sortOrderFieldName	=	"state";
			}
			int mapFlag=2;
			for (TeacherCertificate trCertificate : listTeacherCertificates){
				String orderFieldName=trCertificate.getCertType();
				if(sortOrderFieldName.equals("state")){
					String stateName="";
					if(trCertificate.getStateMaster()==null){
						stateName = " ";
					}else{
						stateName = trCertificate.getStateMaster().getStateName();
					}
						
					orderFieldName=stateName+"||"+trCertificate.getCertId();
					sortedMap.put(orderFieldName+"||",trCertificate);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherCertificates=listTeacherCertificates;
			}
			
			totalRecord =sortedlistTeacherCertificates.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherCertificate> listsortedTeacherCertificates		=	sortedlistTeacherCertificates.subList(start,end);
			
			sb.append("<table border='0'  id='tblGridCertifications'  width='100%' class='table  table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");
			
			String responseText="";
			/*responseText=PaginationAndSorting.responseSortingMLink("State",sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink("Year received",sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
			sb.append("<th width='30%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink("Name",sortOrderFieldName,"certType",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink("Certification(s)/Licensure(s)  Letter",sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink("Certification(s)/Licensure(s)  Status",sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");*/
			
			responseText=PaginationAndSorting.responseSortingMLink(lblNm,sortOrderFieldName,"certType",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink(lblSt,sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink(lblYearRece,sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
			sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink(lblSource,sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top' id='certSRCHeader'>"+responseText+"</th>");
			
			sb.append("<th width='10%' valign='top'>");
			sb.append(lblAct);
			
			sb.append("</tr>");
			sb.append("</thead>");
			if(teacherCertificateDAO!=null)
			for(TeacherCertificate tCertificate:listsortedTeacherCertificates)
			{
				/*sb.append("<tr>");
				sb.append("<td>");
				sb.append(tCertificate.getStateMaster().getStateName());
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append(tCertificate.getYearReceived());
				sb.append("</td>");
				
				sb.append("<td>");
				if(tCertificate.getCertificateTypeMaster()!=null)
				sb.append(tCertificate.getCertificateTypeMaster().getCertType());
				sb.append("</td>");
				
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				sb.append("<td>");
				sb.append(tCertificate.getPathOfCertification()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view Certification Letter!' id='cert"+tCertificate.getCertId()+"' onclick=\"downloadCertification('"+tCertificate.getCertId()+"','cert"+tCertificate.getCertId()+"');"+windowFunc+"\">"+tCertificate.getPathOfCertification()+"</a>");
				sb.append("<script>$('#cert"+tCertificate.getCertId()+"').tooltip();</script>");
				sb.append("</td>");
				
				sb.append("<td>");
				if(tCertificate.getCertificationStatusMaster()!=null)
					sb.append(tCertificate.getCertificationStatusMaster().getCertificationStatusName());
				else
					sb.append("");
				sb.append("</td>");
				sb.append("<td>");
				sb.append("<a href='#' onclick=\"return showEditForm('"+tCertificate.getCertId()+"')\" >Edit</a>");
				sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return deleteRecord_Certificate('"+tCertificate.getCertId()+"')\">Delete</a>");
				sb.append("</td>");
				sb.append("</tr>");*/
				
				
				sb.append("<tr>");
				
				sb.append("<td>");
				if(tCertificate.getCertificateTypeMaster()!=null)
				sb.append(tCertificate.getCertificateTypeMaster().getCertType());
				sb.append("</td>");
				
				sb.append("<td>");
				if(tCertificate.getStateMaster()!=null)
				sb.append(tCertificate.getStateMaster().getStateName());
				sb.append("</td>");
				
				sb.append("<td>");
				String yearReceived = "";
				if(tCertificate.getYearReceived()!=null)
					yearReceived=tCertificate.getYearReceived().toString();
				sb.append(yearReceived);
				sb.append("</td>");
				
				sb.append("<td>");
				if(tCertificate.getCertificationStatusMaster()!=null)
					sb.append(tCertificate.getCertificationStatusMaster().getCertificationStatusName());
				else
					sb.append("");
				sb.append("</td>");
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				
				sb.append("<td class='certSRCRec'>");
				
				if(tCertificate.getPathOfCertification()!=null)
				{
					sb.append(tCertificate.getPathOfCertification()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+toolViewScurec+"' id='cert"+tCertificate.getCertId()+"' onclick=\"downloadCertification('"+tCertificate.getCertId()+"','cert"+tCertificate.getCertId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue' style='padding-left:20px;'></span></a><br>");
					sb.append("<script>$('#cert"+tCertificate.getCertId()+"').tooltip();</script>");
				}
				
				if(tCertificate.getCertUrl()!=null && !tCertificate.getCertUrl().equals(""))
				{
					if(tCertificate.getCertUrl().length()>5)
					{
						String url=tCertificate.getCertUrl().substring(0,5);
						url=url+"....";
						
						sb.append("<a href='"+tCertificate.getCertUrl()+"' rel='tooltip' data-original-title='"+tCertificate.getCertUrl()+"'  id='cert1"+tCertificate.getCertId()+"'  target='_blank'"+windowFunc+">"+url+"</a>");
						sb.append("<script>$('#cert1"+tCertificate.getCertId()+"').tooltip();</script>");
					}
				}
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append("<a href='#' onclick=\"return showEditForm('"+tCertificate.getCertId()+"')\" >"+lblEdit+"</a>");
				sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return deleteRecord_Certificate('"+tCertificate.getCertId()+"')\">"+lnkDlt+"</a>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			if(listTeacherCertificates==null || listTeacherCertificates.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
						
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	@Transactional
	public int saveOrUpdate(TeacherCertificate teacherCertificate)
	{
		System.out.println(teacherCertificate.getCertId()+"======================teacherCertificate============  "+teacherCertificate.getCertificationStatusMaster().getCertificationStatusId());
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		try 
		{	boolean duplicateCertificateFlag=false;
			boolean duplicateTeacherCertificateFlag=false;
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		 if(teacherCertificate!=null && teacherCertificate.getCertificationStatusMaster()!=null && !teacherCertificate.getCertificationStatusMaster().getCertificationStatusId().equals(5)){	
			if(teacherCertificate!=null && teacherCertificate.getCertType()!=null && teacherCertificate.getCertType().equalsIgnoreCase("Other"))
			{
				duplicateCertificateFlag=false;
			}
			else
			{
				if(teacherCertificate.getCertificateTypeMaster().getCertTypeId()==null){
					duplicateCertificateFlag=certificateTypeMasterDAO.findDuplicateCertificateType(null,teacherCertificate.getCertType(),teacherCertificate.getStateMaster(),0);
				}else{
					if(teacherCertificate.getCertificateTypeMaster()!=null)
						duplicateCertificateFlag=teacherCertificateDAO.findDuplicateCertificateTypeForTeacher(teacherCertificate.getTeacherDetail(),teacherCertificate.getCertificateTypeMaster(),teacherCertificate,1);
				}
			}
			
			
			if(duplicateCertificateFlag){
				return 1;
			}else
			{
				if(teacherCertificate.getCertificateTypeMaster().getCertTypeId()==null)
				{	
					CertificateTypeMaster ctmaster = new CertificateTypeMaster();
					ctmaster.setStateId(teacherCertificate.getStateMaster());
					ctmaster.setStatus("A");
					ctmaster.setCertType(teacherCertificate.getCertType());
					certificateTypeMasterDAO.makePersistent(ctmaster);
					teacherCertificate.setCertificateTypeMaster(ctmaster);
					
				}
			}
			
			if(teacherCertificate!=null && teacherCertificate.getCertType()!=null && teacherCertificate.getCertType().equalsIgnoreCase("Other"))
			{
				duplicateTeacherCertificateFlag=false;
			}
			else
			{
				if(teacherCertificate.getCertId()==null)
					duplicateTeacherCertificateFlag=teacherCertificateDAO.findDuplicateCertificateTypeForTeacher(teacherDetail, teacherCertificate.getCertificateTypeMaster(), teacherCertificate, 0);
				else
					duplicateTeacherCertificateFlag=teacherCertificateDAO.findDuplicateCertificateTypeForTeacher(teacherDetail, teacherCertificate.getCertificateTypeMaster(), teacherCertificate, 1);
			}
		 }
			
			teacherCertificate.setCreatedDateTime(new Date());
			teacherCertificate.setTeacherDetail(teacherDetail);
			if(duplicateTeacherCertificateFlag)
				return 1;
			else
				teacherCertificateDAO.makePersistent(teacherCertificate);
		}catch (Exception e) 
		{
			e.printStackTrace();
			return teacherCertificate.getCertId();
		}
		return 2;
	}
	@Transactional
	public Boolean findDuplicateCertificate(TeacherCertificate teacherCertificate)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		if(teacherCertificate!=null && teacherCertificate.getCertType()!=null && teacherCertificate.getCertType().equalsIgnoreCase("Other"))
		{
			return false;
		}
		else
		{
			try 
			{
				boolean duplicateCertificateFlag=false;
				if(teacherCertificate.getCertificateTypeMaster()==null){
					duplicateCertificateFlag=certificateTypeMasterDAO.findDuplicateCertificateType(null,teacherCertificate.getCertType(),teacherCertificate.getStateMaster(),0);
				}else{
					if(teacherCertificate.getCertificateTypeMaster()!=null)
					duplicateCertificateFlag=teacherCertificateDAO.findDuplicateCertificateTypeForTeacher(teacherCertificate.getTeacherDetail(),teacherCertificate.getCertificateTypeMaster(),teacherCertificate,1);
				}
				return duplicateCertificateFlag;
			}catch (Exception e) 
			{
				e.printStackTrace();
			}
			return false;
		}
	}
	
	public PraxisMaster getPraxis(StateMaster stateMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		List<PraxisMaster> praxisMasters = null;
		try 
		{
			if(stateMaster.getStateId()!=null){
				Criterion criterion = Restrictions.eq("stateMaster", stateMaster);
				praxisMasters = praxisMasterDAO.findByCriteria(criterion);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		if(praxisMasters!=null && praxisMasters.size()>0)
			return praxisMasters.get(0);
		else
			return null;
	}
	public TeacherCertificate showEditForm(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		TeacherCertificate teacherCertificate = null;
		try 
		{
			Integer certificateId = Integer.parseInt(id.trim());
			teacherCertificate = teacherCertificateDAO.findById(certificateId, false, false);
			if(teacherCertificate!=null &&  teacherCertificate.getPathOfCertification()!=null){
				session.setAttribute("previousCertificationFile", teacherCertificate.getPathOfCertification());	
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return teacherCertificate;
	}
	
	public boolean deleteRecord(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		Integer certificateId = Integer.parseInt(id.trim());
		TeacherCertificate teacherCertificate= teacherCertificateDAO.findById(certificateId, false, false);
		try{
			teacherCertificateDAO.makeTransient(teacherCertificate);
		}catch(Exception exception){
			exception.printStackTrace();
			return false;
		}
		return true;
	}
	
	public Boolean removeReferences(Integer referenceId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{	
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherElectronicReferences te = teacherElectronicReferencesDAO.findById(referenceId, false, false);
			if(te!=null && te.getPathOfReference()!=null){
				File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+te.getPathOfReference());
				if(file.exists()){
					if(file.delete()){
						//System.out.println(file.getName()+ " deleted");
					}else{
						//System.out.println("Delete operation is failed.");
						return false;
					}
					te.setPathOfReference(null);
					teacherElectronicReferencesDAO.makePersistent(te);
					return true;
				}
				
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	
	public Boolean removeCertification(Integer certId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherCertificate tc = teacherCertificateDAO.findById(certId, false, false);
			if(tc!=null && tc.getPathOfCertification()!=null){
				File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+tc.getPathOfCertification());
				if(file.exists()){
					if(file.delete()){
						//System.out.println(file.getName()+" deleted");
					}else{
						//System.out.println("Delete operation is failed.");
						return false;
					}
					tc.setPathOfCertification(null);
					teacherCertificateDAO.makePersistent(tc);
					return true;
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return false;
	}
	public String downloadReference(Integer referenceId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String path="";
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherElectronicReferences tele = teacherElectronicReferencesDAO.findById(referenceId, false, false);
			String fileName= tele.getPathOfReference();
	        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	        	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	        }	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	
	}
	public String downloadReferenceForCandidate(Integer referenceId,Integer teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String path="";
		try 
		{
			TeacherElectronicReferences tele = teacherElectronicReferencesDAO.findById(referenceId, false, false);
			String fileName= tele.getPathOfReference();
	        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherId+"/"+fileName;
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherId+"/";
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	        	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherId+"/"+sourceFile.getName();
	        }	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	
	}
	public String downloadCertification(Integer referenceId)
	{
		System.out.println(":::::::::::::::::downloadCertification: new:::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String path="";
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherCertificate tele = teacherCertificateDAO.findById(referenceId, false, false);
			String fileName= tele.getPathOfCertification();
	        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	        	path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	        	
	        }	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	
	}
	
	public boolean insertUpdateTeacherExp(TeacherExperience teacherExperience, Integer tfaAffiliateId,Integer corpsYear,Integer tfaRegionId,Integer canServeAsSubTeacher)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TFAAffiliateMaster tfaAffiliateMaster	=	null;
		TFARegionMaster tfaRegionMaster			=	null;
		try{
			TeacherExperience teacherExperienceCAgain= teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			if(canServeAsSubTeacher!=null){
				teacherExperience.setCanServeAsSubTeacher(canServeAsSubTeacher);
			}else{
				teacherExperience.setCanServeAsSubTeacher(2);
			}
			if(teacherExperienceCAgain!=null){
				teacherExperience.setExpId(teacherExperienceCAgain.getExpId());
				teacherExperience.setCreatedDateTime(teacherExperienceCAgain.getCreatedDateTime());
				teacherExperience.setResume(teacherExperienceCAgain.getResume());
				teacherExperience.setPotentialQuality(teacherExperienceCAgain.getPotentialQuality());
				teacherExperience.setKnowOtherLanguages(teacherExperienceCAgain.getKnowOtherLanguages());
			}else{
				teacherExperience.setCreatedDateTime(new Date());
				teacherExperience.setPotentialQuality(0);
				teacherExperience.setKnowOtherLanguages(false);
			}
			teacherExperience.setTeacherId(teacherDetail);
			if(teacherExperience.getNationalBoardCertYear() == null || teacherExperience.getNationalBoardCertYear().equals(""))
			{
				teacherExperience.setNationalBoardCert(false);
			}else{
				teacherExperience.setNationalBoardCert(true);
			}
			if(tfaAffiliateId!=null && tfaAffiliateId!=0 && tfaAffiliateId > 0){
				tfaAffiliateMaster	=	tfaAffiliateMasterDAO.findById(tfaAffiliateId, false, false);
				teacherExperience.setTfaAffiliateMaster(tfaAffiliateMaster);
				
				if(tfaAffiliateId!=3)
				{
					teacherExperience.setCorpsYear(corpsYear);
					tfaRegionMaster		=	tfaRegionMasterDAO.findById(tfaRegionId, false, false);
					teacherExperience.setTfaRegionMaster(tfaRegionMaster);
				}
			}
			else
			{
				teacherExperience.setTfaAffiliateMaster(null);
				teacherExperience.setCorpsYear(null);
				teacherExperience.setTfaRegionMaster(null);
			}
			teacherExperience.setIsNonTeacher(teacherExperience.getIsNonTeacher());
			teacherExperienceDAO.makePersistent(teacherExperience);
		} 
		catch (Exception e)	{
			e.printStackTrace();
		}
		boolean bTeacherPortfolioStatus=false;
		try{
			
			TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(teacherPortfolioStatus==null)
			{
				bTeacherPortfolioStatus=true;
				teacherPortfolioStatus = new TeacherPortfolioStatus();
				teacherPortfolioStatus.setIsCertificationsCompleted(true);
			}
			else
			{
				teacherPortfolioStatus.setIsCertificationsCompleted(true);
			}
			
			
			try{
				if(teacherPortfolioStatus.getIsPersonalInfoCompleted()==null)
				{
					teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsCertificationsCompleted()==null){
				teacherPortfolioStatus.setIsCertificationsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsCertificationsCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsExperiencesCompleted()==null){
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsAffidavitCompleted()==null){
				teacherPortfolioStatus.setIsAffidavitCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAffidavitCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsAcademicsCompleted()==null){
				teacherPortfolioStatus.setIsAcademicsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAcademicsCompleted(false);
				e.printStackTrace();
			}
			
			
			//teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(bTeacherPortfolioStatus && tpsTemp==null)
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			else
			{
				teacherPortfolioStatus.setId(tpsTemp.getId());
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return true;
	}
	
	//**************** Ramesh
	
	/**
	 * 
	 * @param teacherRole
	 * @return
	 * @author Ramesh
	 */
	
	public String saveOrUpdateElectronicReferences(TeacherElectronicReferences teacherElectronicReferences)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		
		List<TeacherElectronicReferences> references=null;
		try 
		{
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion_email = Restrictions.eq("email",teacherElectronicReferences.getEmail());
			
			if(teacherElectronicReferences.getElerefAutoId()==null)
			{
				teacherElectronicReferences.setStatus(1);
				teacherElectronicReferences.setCreatedDateTime(new Date());
				
				references = teacherElectronicReferencesDAO.findByCriteria(criterion,criterion_email);
				if(references!=null && references.size()>=1)
					return "isDuplicate";
			}
			else
			{
				TeacherElectronicReferences pojo=teacherElectronicReferencesDAO.findById(teacherElectronicReferences.getElerefAutoId(), false, false);
				teacherElectronicReferences.setStatus(pojo.getStatus());
				
				Criterion criterion_id = Restrictions.ne("elerefAutoId",teacherElectronicReferences.getElerefAutoId());
				references = teacherElectronicReferencesDAO.findByCriteria(criterion_id,criterion,criterion_email);
				if(references!=null && references.size()>=1)
					return "isDuplicate";
			}
				
			teacherElectronicReferences.setTeacherDetail(teacherDetail);
			teacherElectronicReferencesDAO.makePersistent(teacherElectronicReferences);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		
		return null;
	}
	public String findDuplicateEleReferences(TeacherElectronicReferences teacherElectronicReferences)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		
		List<TeacherElectronicReferences> references=null;
		try 
		{
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion_email = Restrictions.eq("email",teacherElectronicReferences.getEmail());
			
			if(teacherElectronicReferences.getElerefAutoId()==null)
			{
				teacherElectronicReferences.setStatus(1);
				teacherElectronicReferences.setCreatedDateTime(new Date());
				
				references = teacherElectronicReferencesDAO.findByCriteria(criterion,criterion_email);
				if(references!=null && references.size()>=1)
					return "isDuplicate";
			}
			else
			{
				TeacherElectronicReferences pojo=teacherElectronicReferencesDAO.findById(teacherElectronicReferences.getElerefAutoId(), false, false);
				teacherElectronicReferences.setStatus(pojo.getStatus());
				
				Criterion criterion_id = Restrictions.ne("elerefAutoId",teacherElectronicReferences.getElerefAutoId());
				references = teacherElectronicReferencesDAO.findByCriteria(criterion_id,criterion,criterion_email);
				if(references!=null && references.size()>=1)
					return "isDuplicate";
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * @param noOfRow
	 * @param pageNo
	 * @param sortOrder
	 * @param sortOrderType
	 * @return
	 * @author Ramesh
	 */
	public String getElectronicReferencesGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb = new StringBuffer();		
		try 
		{
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------

			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			List<TeacherElectronicReferences> listTeacherElectronicReferences= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"lastName";
			String sortOrderNoField		=	"lastName";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.isNull("districtMaster");
			listTeacherElectronicReferences = teacherElectronicReferencesDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2);		

			List<TeacherElectronicReferences> sortedTeacherRole		=	new ArrayList<TeacherElectronicReferences>();

			SortedMap<String,TeacherElectronicReferences>	sortedMap = new TreeMap<String,TeacherElectronicReferences>();
			int mapFlag=2;
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherRole.add((TeacherElectronicReferences) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherRole.add((TeacherElectronicReferences) sortedMap.get(key));
				}
			}else{
				sortedTeacherRole=listTeacherElectronicReferences;
			}

			totalRecord =listTeacherElectronicReferences.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherElectronicReferences> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);

			String responseText="";
			sb.append("<table border='0' id='eleReferencesGrid' width='100%' class='table table-striped'>");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingMLink(lblRefName,sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLink(lblTitle,sortOrderFieldName,"designation",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLink(lblOrganizationEmp,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink(lblEmail,sortOrderFieldName,"email",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink(lblRecLetter,sortOrderFieldName,"pathOfReference",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top' class='recomTit'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink(lblContactNo,sortOrderFieldName,"contactnumber",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink(lblCanContact,sortOrderFieldName,"rdcontacted",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			
			sb.append("<th width='20%' valign='top'>");
			sb.append(lblAct);
			sb.append("</th>");	

			sb.append("</tr>");
			sb.append("</thead>");
			if(listsortedTeacherRole!=null)
				
				
				for(TeacherElectronicReferences tRole:listsortedTeacherRole)
				{
					String Salutation="";
					if(tRole.getSalutation()==0)
						Salutation="";
					else if(tRole.getSalutation()==1)
						Salutation="Mrs.";
					else if(tRole.getSalutation()==2)
						Salutation="Mr.";
					else if(tRole.getSalutation()==3)
						Salutation="Miss";
					else if(tRole.getSalutation()==4)
						Salutation="Dr.";
					else if(tRole.getSalutation()==5)
						Salutation="Ms.";
							
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(Salutation+" "+tRole.getFirstName()+" "+tRole.getLastName());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(tRole.getDesignation());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(tRole.getOrganization());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(tRole.getEmail());
					sb.append("</td>");
					
					
					String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
					sb.append("<td class='recomTit'>");
					sb.append(tRole.getPathOfReference()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+toolViewRecommendationLetter+"'  id='trans"+tRole.getElerefAutoId()+"' onclick=\"downloadReference('"+tRole.getElerefAutoId()+"','trans"+tRole.getElerefAutoId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
					sb.append("<script>$('#trans"+tRole.getElerefAutoId()+"').tooltip();</script>");
					sb.append("</td>");
					
					 //TMHOTLINE-2628
					sb.append("<td>");
					sb.append(Utility.formatContactNo(tRole.getContactnumber()));					
					sb.append("</td>");
					
					sb.append("<td>");
					if(tRole.getRdcontacted())
						sb.append(Utility.getLocaleValuePropByKey("lblY", locale));
					else if(!tRole.getRdcontacted())
						sb.append(Utility.getLocaleValuePropByKey("lblN", locale));
					sb.append("</td>");
					
					
					sb.append("<td>");
					sb.append("<a href='#' onclick=\"return editFormElectronicReferences('"+tRole.getElerefAutoId()+"')\" >"+lblEdit+"</a>");
					
					if(tRole.getStatus()==0)
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return changeStatusElectronicReferences('"+tRole.getElerefAutoId()+"','1')\">"+lblActivate+"</a>");
					else if(tRole.getStatus()==1)
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return changeStatusElectronicReferences('"+tRole.getElerefAutoId()+"','0')\">"+lblDeactivate+"</a>");
					
					sb.append("</td>");
					sb.append("</tr>");
				}

			if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='8'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationDoubleString(request,totalRecord,noOfRow, pageNo));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @author Ramesh
	 */
	public Boolean deleteElectronicReferences(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		Integer elerefAutoId = Integer.parseInt(id.trim());
		TeacherElectronicReferences teacherElectronicReferences = teacherElectronicReferencesDAO.findById(elerefAutoId, false, false);
		teacherElectronicReferencesDAO.makeTransient(teacherElectronicReferences);
		return true;		
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @author Ramesh
	 */
	public Boolean changeStatusElectronicReferences(String id,Integer status)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		Integer elerefAutoId = Integer.parseInt(id.trim());
		TeacherElectronicReferences teacherElectronicReferences = teacherElectronicReferencesDAO.findById(elerefAutoId, false, false);
		teacherElectronicReferences.setStatus(status);
		teacherElectronicReferencesDAO.makePersistent(teacherElectronicReferences);
		return true;		
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @author Ramesh
	 */
	public TeacherElectronicReferences editElectronicReferences(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		TeacherElectronicReferences tele = null;
		try 
		{
			Integer elerefAutoId = Integer.parseInt(id.trim());
			tele = teacherElectronicReferencesDAO.findById(elerefAutoId, false, false);
			if(tele!=null && tele.getPathOfReference()!=null){
				session.setAttribute("previousReferenceFile", tele.getPathOfReference());
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tele;
	}
	
	
	/**
	 * 
	 * @param email
	 * @return
	 * @author Ramesh Kumar Bhartiya
	 * Date Oct 30, 2013
	 */
	public boolean checkEmailForEleRef(String email)
	{
		
		boolean isReturn=false;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		
		List<TeacherElectronicReferences> references=null;
		try 
		{
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion_email = Restrictions.eq("email",email);
			
			references = teacherElectronicReferencesDAO.findByCriteria(criterion,criterion_email);
			if(references!=null && references.size()>=1)
				isReturn=true;
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return isReturn;
	}
	
	
	
	public String getVideoLinksGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb = new StringBuffer();		
		try 
		{
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------
			System.out.println(start+" start "+end);
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			List<TeacherVideoLink> listTeacherVideoLink= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDate";
			String sortOrderNoField		=	"createdDate";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.isNull("districtMaster");
			listTeacherVideoLink = teacherVideoLinksDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2);		

			List<TeacherVideoLink> sortedTeacherRole		=	new ArrayList<TeacherVideoLink>();

			SortedMap<String,TeacherVideoLink>	sortedMap = new TreeMap<String,TeacherVideoLink>();
			int mapFlag=2;
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherRole.add((TeacherVideoLink) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherRole.add((TeacherVideoLink) sortedMap.get(key));
				}
			}else{
				sortedTeacherRole=listTeacherVideoLink;
			}

			totalRecord =listTeacherVideoLink.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherVideoLink> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);
			String video="";
			String responseText="";
			sb.append("<table border='0' id='videoLinktblGrid' class='table table-striped'>");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingMLink(lnkViLnk,sortOrderFieldName,"videourl",sortOrderTypeVal,pgNo);
			sb.append("<th width='50%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink(lblVid,sortOrderFieldName,"video",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLink(lblcretedDate,sortOrderFieldName,"createdDate",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			sb.append("<th  width='10%' valign='top'>");
			sb.append(lblAct);
			sb.append("</th>");	

			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherVideoLink!=null)
				
				for(TeacherVideoLink pojo:listsortedTeacherRole)
				{
					sb.append("<tr>");
					sb.append("<td width='50%'>");
					String httpStr="";
					String httpsStr="";
					
					if(pojo.getVideourl().length()>"http://".length()||pojo.getVideourl().length()>"https://".length()){
						httpStr=pojo.getVideourl().substring(0,"http://".length());
						httpsStr=pojo.getVideourl().substring(0,"https://".length());
					}
					if(httpStr.equals("http://"))
						sb.append("<a  href='"+pojo.getVideourl()+"' target='blank'>"+pojo.getVideourl()+"</a>");
					else if(httpsStr.equals("https://"))
						sb.append("<a  href='"+pojo.getVideourl()+"' target='blank'>"+pojo.getVideourl()+"</a>");
					else
						sb.append("<a  href='http://"+pojo.getVideourl()+"' target='blank'>"+pojo.getVideourl()+"</a>");
					sb.append("</td>");
					
					sb.append("<td>");
					
					if(pojo.getVideo()==null)					
						sb.append("");						
					else	
					{
						
					sb.append("<a onclick='return getVideoPlay("+pojo.getVideolinkAutoId()+")'  href='javascript:void(0)'>"+pojo.getVideo()+"</a>");
					}
					//"+filepath+"
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(pojo.getCreatedDate()));
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append("<a href='#' onclick=\"return editFormVideoLink('"+pojo.getVideolinkAutoId()+"')\" >"+lblEdit+"</a>");
					sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return delVideoLink('"+pojo.getVideolinkAutoId()+"')\" >"+lnkDlt+"</a>");
					sb.append("</td>");
					
					sb.append("</tr>");
				}

			if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='3'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationTrippleGrid(request,totalRecord,noOfRow, pageNo));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	
	
	public Boolean deleteVIdeoLink(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		Integer videolinkAutoId = Integer.parseInt(id.trim());
		TeacherVideoLink teacherVideoLink = teacherVideoLinksDAO.findById(videolinkAutoId, false, false);	
		if(teacherVideoLink!=null)
		if(teacherVideoLink.getVideourl()!=null || teacherVideoLink.getVideo()!=null){			
			String videoS=teacherVideoLink.getVideo();
			File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherVideoLink.getTeacherDetail().getTeacherId()+"/Video/"+teacherVideoLink.getVideo());
			//System.out.println("file "+file);
			if(videoS!=null && videoS!="")
			{
				if(file.exists()){					
					if(file.delete()){						
						teacherVideoLinksDAO.makeTransient(teacherVideoLink);
						//System.out.println(file.getName()+ " deleted");
					}else{						
						//System.out.println("Delete operation is failed.");
						return false;
					}			
					return true;
				}
			}			
			teacherVideoLinksDAO.makeTransient(teacherVideoLink);
		}		
		
		return true;		
	}
	
	
	
	public String[] saveOrUpdateVideoLinks(TeacherVideoLink teacherVideoLink)
	{
		String[] data= new String[2];
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		try
		{
			String previousVideo="";
			String videoId ="";
			String video="";
			String myFileName="";
			if(request.getSession().getAttribute("previousVideoFile")!=null){
				previousVideo= request.getSession().getAttribute("previousVideoFile").toString();
				System.out.println("previousVideo "+previousVideo);
				System.out.println("previousVideo "+teacherVideoLink.getVideo());
			}
			System.out.println("teacherVideoLink.getVideo() "+teacherVideoLink.getVideo());
			if(!previousVideo.equals(teacherVideoLink.getVideo()))
			{				
				video=teacherVideoLink.getVideo();						
				teacherVideoLink.setVideo(video);				
			}			
			if(teacherVideoLink.getVideolinkAutoId()==null && ( teacherVideoLink.getVideourl()==null || teacherVideoLink.getVideourl().trim().equalsIgnoreCase("")) && teacherVideoLink.getVideo()==null  )
				return null;		
			
			if(teacherVideoLink.getVideolinkAutoId()==null)
				teacherVideoLink.setCreatedDate(new Date());
			else
			{
				TeacherVideoLink temp=teacherVideoLinksDAO.findById(teacherVideoLink.getVideolinkAutoId(), false, false);
				teacherVideoLink.setCreatedDate(temp.getCreatedDate());
			}
			
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			teacherVideoLink.setTeacherDetail(teacherDetail);
			teacherVideoLinksDAO.makePersistent(teacherVideoLink);		
			data[1] = "1";
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return data;
		
	}
	
	
	public TeacherVideoLink editVideoLink(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		TeacherVideoLink teacherVideoLink = null;
		try 
		{
			Integer elerefAutoId = Integer.parseInt(id.trim());
			teacherVideoLink = teacherVideoLinksDAO.findById(elerefAutoId, false, false);
			session.removeAttribute("previousVideoFile");
			session.setAttribute("previousVideoFile", teacherVideoLink.getVideo());	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return teacherVideoLink;
	}
	
	public Boolean removeGeneralKnowledgeScoreReport(Integer referenceId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{	
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam = teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
			if(teacherGeneralKnowledgeExam!=null && teacherGeneralKnowledgeExam.getGeneralKnowledgeScoreReport()!=null){
				File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+teacherGeneralKnowledgeExam.getGeneralKnowledgeScoreReport());
				if(file.exists()){
					if(file.delete()){
						//System.out.println(file.getName()+ " deleted");
					}else{
						//System.out.println("Delete operation is failed.");
						return false;
					}
					teacherGeneralKnowledgeExam.setGeneralKnowledgeScoreReport(null);
					teacherGeneralKnowledgeExamDAO.updatePersistent(teacherGeneralKnowledgeExam);
					return true;
				}
				
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	public Boolean removeScoreReport(Integer referenceId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{	
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherSubjectAreaExam teacherSubjectAreaExam = teacherSubjectAreaExamDAO.findTeacherSubjectAreaExam(teacherDetail);
			if(teacherSubjectAreaExam!=null && teacherSubjectAreaExam.getScoreReport()!=null){
				File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+teacherSubjectAreaExam.getScoreReport());
				if(file.exists()){
					if(file.delete()){
						//System.out.println(file.getName()+ " deleted");
					}else{
						//System.out.println("Delete operation is failed.");
						return false;
					}
					teacherSubjectAreaExam.setScoreReport(null);
					teacherSubjectAreaExamDAO.updatePersistent(teacherSubjectAreaExam);
					return true;
				}
				
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	public TeacherGeneralKnowledgeExam getGKEValues()
	{
		System.out.println("Calling getGKEValues Ajax Method");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam=null;
		try{
			teacherGeneralKnowledgeExam=teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
		} 
		catch (Exception e)	{
			e.printStackTrace();
			return teacherGeneralKnowledgeExam;
		}	
		return teacherGeneralKnowledgeExam;
	}
	public TeacherSubjectAreaExam getSAEValues()
	{
		System.out.println("Calling getSAEValues Ajax Method");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TeacherSubjectAreaExam teacherSubjectAreaExam=null;
		try{
			 teacherSubjectAreaExam = teacherSubjectAreaExamDAO.findTeacherSubjectAreaExam(teacherDetail);
		} 
		catch (Exception e)	{
			e.printStackTrace();
			return teacherSubjectAreaExam;
		}	
		return teacherSubjectAreaExam;
	}
	public TeacherAdditionalDocuments getADValues()
	{
		System.out.println("Calling getADValues Ajax Method");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TeacherAdditionalDocuments teacherAdditionalDocuments=null;
		try{
			teacherAdditionalDocuments = teacherAdditionalDocumentsDAO.findTeacherAdditionalDocuments(teacherDetail);
		} 
		catch (Exception e)	{
			e.printStackTrace();
			return teacherAdditionalDocuments;
		}	
		return teacherAdditionalDocuments;
	}
	public String downloadGeneralKnowledge(Integer generalKnowledgeExamId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String path="";
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam = teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
			String fileName= teacherGeneralKnowledgeExam.getGeneralKnowledgeScoreReport();
	        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	        	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	        }	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	
	}
	public String downloadSubjectAreaExam(Integer subjectAreaExamId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String path="";
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			//TeacherSubjectAreaExam teacherSubjectAreaExam = teacherSubjectAreaExamDAO.findTeacherSubjectAreaExam(teacherDetail);
			
			TeacherSubjectAreaExam teacherSubjectAreaExam = teacherSubjectAreaExamDAO.findById(subjectAreaExamId, false, false);
			
			
			String fileName= teacherSubjectAreaExam.getScoreReport();
	        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	        	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	        }	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	
	}
	public String downloadUploadedDocument(Integer additionDocumentId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String path="";
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherAdditionalDocuments teacherAdditionalDocuments = teacherAdditionalDocumentsDAO.findById(additionDocumentId,false,false);
			String fileName= teacherAdditionalDocuments.getUploadedDocument();
	        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	        	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	        }	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	
	}
	public String downloadUploadedDocumentForCg(Integer additionDocumentId,Integer teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String path="";
		try 
		{
			
			TeacherAdditionalDocuments teacherAdditionalDocuments = teacherAdditionalDocumentsDAO.findById(additionDocumentId,false,false);
			String fileName= teacherAdditionalDocuments.getUploadedDocument();
	        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherId+"/"+fileName;	   
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherId+"/";	       
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	        	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherId+"/"+sourceFile.getName();
	        }	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	
	}
	public String getAdditionalDocumentsGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null)){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			List<TeacherAdditionalDocuments> lstTeacherAdditionalDocuments=teacherAdditionalDocumentsDAO.findSortedAdditionalDocumentsByTeacher(sortOrderStrVal,teacherDetail);
			
			totalRecord =lstTeacherAdditionalDocuments.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherAdditionalDocuments> teacherAdditionalDocumentsList		=	lstTeacherAdditionalDocuments.subList(start,end);
			
			
			sb.append("<table border='0' id='additionalDocumentsGrid'  width='100%' class='table table-striped mt30'  >");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLink(lbllDocName,sortOrderFieldName,"documentName",sortOrderTypeVal,pgNo);
			sb.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblDoc,sortOrderFieldName,"uploadedDocument",sortOrderTypeVal,pgNo);
			sb.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
	

			sb.append("<th width='20%' class='net-header-text'>");
			sb.append(lblAct);
			sb.append("</th>");			
			sb.append("</tr>");
			sb.append("</thead>");
			
			if(teacherAdditionalDocumentsList!=null && teacherAdditionalDocumentsList.size()>0)
			for(TeacherAdditionalDocuments objTAD:teacherAdditionalDocumentsList)
			{
				sb.append("<tr>");
				
				sb.append("<td>");
				sb.append(objTAD.getDocumentName());	
				sb.append("</td>");
				
				/*sb.append("<td>");
				sb.append(objTAD.getUploadedDocument());
				sb.append("</td>");*/
				
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				sb.append("<td style='text-align:left'>");								
				sb.append(objTAD.getUploadedDocument()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+lnkClickHereToViewDoc+"'  id='addDoc"+objTAD.getAdditionDocumentId()+"' onclick=\"downloadUploadedDocument('"+objTAD.getAdditionDocumentId()+"','addDoc"+objTAD.getAdditionDocumentId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
				sb.append("<script>$('#addDoc"+objTAD.getAdditionDocumentId()+"').tooltip();</script>");
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append("	<a href='#' onclick=\"return showEditTeacherAdditionalDocuments('"+objTAD.getAdditionDocumentId()+"')\" >"+lblEdit+"</a>");
				sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return delRow_Document('"+objTAD.getAdditionDocumentId()+"')\">"+lnkDlt+"</a>");
				sb.append("</td>");
				
				sb.append("</tr>");
			}
			
			if(teacherAdditionalDocumentsList==null || teacherAdditionalDocumentsList.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public TeacherAdditionalDocuments showEditTeacherAdditionalDocuments(String additionDocumentId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		TeacherAdditionalDocuments teacherAdditionalDocuments = teacherAdditionalDocumentsDAO.findById(Integer.parseInt(additionDocumentId), false, false);
		return teacherAdditionalDocuments;
	}
	public Boolean removeUploadedDocument(String additionDocumentId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{	
			System.out.println("additionDocumentId:::::::::"+additionDocumentId);
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherAdditionalDocuments	teacherAdditionalDocuments = teacherAdditionalDocumentsDAO.findById(Integer.parseInt(additionDocumentId), false,false);
			if(teacherAdditionalDocuments!=null && teacherAdditionalDocuments.getUploadedDocument()!=null){
				File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+teacherAdditionalDocuments.getUploadedDocument());
				if(file.exists()){
					if(file.delete()){
						//System.out.println(file.getName()+ " deleted");
					}else{
						//System.out.println("Delete operation is failed.");
						return false;
					}
					teacherAdditionalDocuments.setUploadedDocument(null);
					teacherAdditionalDocumentsDAO.makeTransient(teacherAdditionalDocuments);
					return true;
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public String saveOrUpdateAdditionalDocuments(String additionDocumentId, String documentName, String uploadedDocument,String uploadedDocumentHidden)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherAdditionalDocuments teacherAdditionalDocuments = null;
			System.out.println(uploadedDocument+":::::::::::additionDocumentId:::::::::"+additionDocumentId);
			if(additionDocumentId==null || additionDocumentId.equals(""))
			{
				teacherAdditionalDocuments = new TeacherAdditionalDocuments();				
			}
			else
			{
				teacherAdditionalDocuments = teacherAdditionalDocumentsDAO.findById(new Integer(additionDocumentId.trim()), false, false);
			}
			teacherAdditionalDocuments.setTeacherDetail(teacherDetail);
			teacherAdditionalDocuments.setDocumentName(documentName);
			teacherAdditionalDocuments.setCreatedDateTime(new Date());
			if(uploadedDocument!=null && !uploadedDocument.equals("")){
				if(!uploadedDocument.equals("") && additionDocumentId!=null && !additionDocumentId.equals("")){
					String filePath =Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+teacherAdditionalDocuments.getUploadedDocument();
					System.out.println("previous file path :: "+filePath);
					File file=new File(filePath);
					file.delete();
				}
				teacherAdditionalDocuments.setUploadedDocument(uploadedDocument);
			}else{
				teacherAdditionalDocuments.setUploadedDocument(teacherAdditionalDocuments.getUploadedDocument());
			}
			teacherAdditionalDocumentsDAO.makePersistent(teacherAdditionalDocuments);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "OK";
	}
	
	
	
	public String getSubjectAreasGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("subject")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 
				 if(sortOrder.equals("subject")){
					 sortOrderNoField="subject";
				 }
				 
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			
			
			
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			List<TeacherSubjectAreaExam> lstTeacherSubjectAreaExams=teacherSubjectAreaExamDAO.getTeacherSubjectAreaExamsByTeacher(sortOrderStrVal,teacherDetail);
			
			List<TeacherSubjectAreaExam> sortedlistTeacherSubjectAreaExams = new ArrayList<TeacherSubjectAreaExam>();
			
			SortedMap<String,TeacherSubjectAreaExam>	sortedMap = new TreeMap<String,TeacherSubjectAreaExam>();
			if(sortOrderNoField.equals("subject"))
			{
				sortOrderFieldName	=	"subject";
			}
			
			int mapFlag=2;
			for (TeacherSubjectAreaExam trSubArea : lstTeacherSubjectAreaExams){
				String orderFieldName=trSubArea.getTeacherSubjectAreaExamId()+"";
				if(sortOrderFieldName.equals("subject")){
					orderFieldName=trSubArea.getSubjectAreaExamMaster().getSubjectAreaExamName()+"||"+trSubArea.getTeacherSubjectAreaExamId();
					sortedMap.put(orderFieldName+"||",trSubArea);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			}
			
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherSubjectAreaExams.add((TeacherSubjectAreaExam) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherSubjectAreaExams.add((TeacherSubjectAreaExam) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherSubjectAreaExams=lstTeacherSubjectAreaExams;
			}
			
			totalRecord =sortedlistTeacherSubjectAreaExams.size();

			if(totalRecord<end)
				end=totalRecord;
			
			List<TeacherSubjectAreaExam> teacherSubjectAreaExamsList =	sortedlistTeacherSubjectAreaExams.subList(start,end);
			
			sb.append("<table border='0' id='subjectAreasGrid'  width='100%' class='table table-striped mt30'  >");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"examStatus",sortOrderTypeVal,pgNo);
			sb.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblExamDate", locale),sortOrderFieldName,"examDate",sortOrderTypeVal,pgNo);
			sb.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSub", locale),sortOrderFieldName,"subject",sortOrderTypeVal,pgNo);
			sb.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			//responseText=PaginationAndSorting.responseSortingLink("+Utility.getLocaleValuePropByKey("lblScoreReport", locale)+",sortOrderFieldName,"scoreReport",sortOrderTypeVal,pgNo);
			sb.append("<th width='40%' valign='top'>"+Utility.getLocaleValuePropByKey("lblScoreReport", locale)+"</th>");
			
			sb.append("<th width='20%' class='net-header-text'>");
			sb.append(lblAct);
			sb.append("</th>");			
			sb.append("</tr>");
			sb.append("</thead>");
			
			String sExamStatus="";
			if(teacherSubjectAreaExamsList!=null && teacherSubjectAreaExamsList.size()>0)
			for(TeacherSubjectAreaExam objTAD:teacherSubjectAreaExamsList)
			{
				if(objTAD.getExamStatus()!=null)
				{
					if(objTAD.getExamStatus().equalsIgnoreCase("P"))
						sExamStatus="Pass";
					else if(objTAD.getExamStatus().equalsIgnoreCase("F"))
						sExamStatus="Fail";
				}
				
				sb.append("<tr>");
				
				sb.append("<td>");
				sb.append(sExamStatus);	
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(objTAD.getExamDate()));
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append(objTAD.getSubjectAreaExamMaster().getSubjectAreaExamName());
				sb.append("</td>");
				
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				
				sb.append("<td>");
				
				if(objTAD.getScoreReport()!=null)
				{
					sb.append(objTAD.getScoreReport()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+msgViewScoreReport+"' id='cert"+objTAD.getTeacherSubjectAreaExamId()+"' onclick=\"downloadSubjectAreaExam('"+objTAD.getTeacherSubjectAreaExamId()+"','cert"+objTAD.getTeacherSubjectAreaExamId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue' style='padding-left:20px;'></span></a><br>");
					sb.append("<script>$('#cert"+objTAD.getTeacherSubjectAreaExamId()+"').tooltip();</script>");
				}
				
				sb.append("</td>");
				
				sb.append("<td>");
				
				sb.append("	<a href='#' onclick=\"return showEditSubject('"+objTAD.getTeacherSubjectAreaExamId()+"')\" >"+lblEdit+"</a>");
				sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return delRow_subjectArea('"+objTAD.getTeacherSubjectAreaExamId()+"')\">"+lnkDlt+"</a>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			if(teacherSubjectAreaExamsList==null || teacherSubjectAreaExamsList.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='5'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String deleteSubjectArea(int id)
	{
		String sReturnValue="1";
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{
			TeacherSubjectAreaExam teacherSubjectAreaExam=teacherSubjectAreaExamDAO.findById(id, false, false);
			if(teacherSubjectAreaExam!=null)
			{
				teacherSubjectAreaExamDAO.makeTransient(teacherSubjectAreaExam);
			}
			else
			{
				sReturnValue="0";
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			sReturnValue="0";
		}
		return sReturnValue;
	}
	
	
	
	public TeacherSubjectAreaExam showEditSubject(int id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		TeacherSubjectAreaExam teacherSubjectAreaExam=null;
		try 
		{
			teacherSubjectAreaExam=teacherSubjectAreaExamDAO.findById(id, false, false);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			return teacherSubjectAreaExam;
		}
	}
	
	public TeacherSubjectAreaExam InserOrUpdateSubject(String teacherSubjectAreaExamId,String examStatus,String examDate,String subjectIdforDSPQ,String scoreReport,String subjectExamNote)
	{
		System.out.println("Calling TeacherSubjectAreaExam "+examDate);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		
		TeacherSubjectAreaExam teacherSubjectAreaExam=null;
		try 
		{
			int iTeacherSubjectAreaExamId=0;
			if(teacherSubjectAreaExamId!=null && !teacherSubjectAreaExamId.equalsIgnoreCase("0"))
				try {
					iTeacherSubjectAreaExamId=Integer.parseInt(teacherSubjectAreaExamId);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			if(iTeacherSubjectAreaExamId > 0)
				teacherSubjectAreaExam=teacherSubjectAreaExamDAO.findById(iTeacherSubjectAreaExamId, false, false);
			else
				teacherSubjectAreaExam=new TeacherSubjectAreaExam();
			
			int iSubjectIdforDSPQ=Integer.parseInt(subjectIdforDSPQ);
			
			if(iSubjectIdforDSPQ > 0)
			{
				SubjectAreaExamMaster subjectAreaExamMaster=subjectAreaExamMasterDAO.findById(iSubjectIdforDSPQ, false, false);
				
				if(subjectAreaExamMaster!=null)
				{
					teacherSubjectAreaExam.setTeacherDetail(teacherDetail);
					teacherSubjectAreaExam.setExamStatus(examStatus);
					teacherSubjectAreaExam.setExamDate(Utility.getCurrentDateFormart(examDate));
					teacherSubjectAreaExam.setSubjectAreaExamMaster(subjectAreaExamMaster);
					if(scoreReport!=null && !scoreReport.equalsIgnoreCase(""))
						teacherSubjectAreaExam.setScoreReport(scoreReport);
					if(iTeacherSubjectAreaExamId == 0)
						teacherSubjectAreaExam.setCreatedDateTime(new Date());
					teacherSubjectAreaExam.setExamNote(subjectExamNote);
					teacherSubjectAreaExamDAO.makePersistent(teacherSubjectAreaExam);
				}
			}
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			return teacherSubjectAreaExam;
		}
	}

	
	public boolean findDuplicateCertificateDSPQ(int certId,long stateId,String sCertType,int iCertificateTypeMaster)
	{
		boolean duplicateCertificateFlag=false;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		
		if(sCertType!=null && sCertType.equalsIgnoreCase("Other"))
		{
			duplicateCertificateFlag=false;
		}
		else
		{
			StateMaster stateMaster=null;
			CertificateTypeMaster certificateTypeMaster=null;
			
			if(stateId > 0)
				stateMaster=stateMasterDAO.findById(stateId, false, false);
			
			if(iCertificateTypeMaster > 0)
				certificateTypeMaster=certificateTypeMasterDAO.findById(iCertificateTypeMaster, false, false);

			if(teacherDetail!=null && stateMaster!=null && !sCertType.equalsIgnoreCase("Other") && certificateTypeMaster!=null && certId==0)
			{
				duplicateCertificateFlag=teacherCertificateDAO.findDuplicateCertificateTypeForDSPQ(teacherDetail,certificateTypeMaster,certId,0);
			}
			else if(teacherDetail!=null && stateMaster!=null && !sCertType.equalsIgnoreCase("Other") && certificateTypeMaster!=null && certId > 0)
			{
				duplicateCertificateFlag=teacherCertificateDAO.findDuplicateCertificateTypeForDSPQ(teacherDetail, certificateTypeMaster, certId, 1);
			}
		}
		return duplicateCertificateFlag;
	}
	
	public String getDistrictSpecificPortfolioQuestions(Integer jobId)
	{

		System.out.println("*****:::::::::::::::||getDistrictSpecificPortfolioQuestions||:::::::::::::::::::;; ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		boolean isAlsoSA=false;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		StringBuffer sb =new StringBuffer();
		try{
			TeacherDetail teacherDetail = null;

			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
			{
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
				
				if(session!=null && session.getAttribute("isAlsoSA")!=null && session.getAttribute("isAlsoSA").equals(true)) 
				{
					isAlsoSA=true;
				}				
			}
			int jobCategoryId=0;
			if(jobId!=0)
			{
				JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				jobCategoryId=jobOrder.getJobCategoryMaster().getJobCategoryId();
				
				List<DistrictSpecificPortfolioAnswers>	lastList=null;
				boolean bPortfolioNewQuestion=false;
				int dspqType=0;	
				List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsList=new ArrayList<DistrictSpecificPortfolioQuestions>();
				
				boolean SelfServicePortfolioStatus=false;
				if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getSelfServicePortfolioStatus()!=null && jobOrder.getDistrictMaster().getSelfServicePortfolioStatus())
					SelfServicePortfolioStatus=jobOrder.getDistrictMaster().getSelfServicePortfolioStatus();
				
				if(!SelfServicePortfolioStatus)
				{
					if(jobOrder.getHeadQuarterMaster()!=null)
						lastList = districtSpecificPortfolioAnswersDAO.findSpecificPortfolioAnswers(teacherDetail,jobOrder,3);
					else
						lastList = districtSpecificPortfolioAnswersDAO.findSpecificPortfolioAnswers(teacherDetail,jobOrder,0);
					if(lastList!=null && lastList.size()>0)
					{
						for(DistrictSpecificPortfolioAnswers answers :lastList)
						{
							districtSpecificPortfolioQuestionsList.add(answers.getDistrictSpecificPortfolioQuestions());
						}
						bPortfolioNewQuestion=true;
					}
					else
					{
						bPortfolioNewQuestion=false;
						Map<Integer,List<DistrictSpecificPortfolioQuestions>> dSPQ=districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestion(jobOrder,isAlsoSA);
						try{
							for(int i=0;i<5;i++){
								if(dSPQ.get(i)!=null){
									districtSpecificPortfolioQuestionsList=dSPQ.get(i);
									dspqType=i;
									break;
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				
				
				System.out.println("districtSpecificPortfolioQuestionsList:::::::::::"+districtSpecificPortfolioQuestionsList.size());
				int totalQuestions = districtSpecificPortfolioQuestionsList.size();
				int cnt = 0;
				if(districtSpecificPortfolioQuestionsList.size()>0){
					System.out.println("dspqType:::::::::::"+dspqType);
					if(!bPortfolioNewQuestion)
						lastList = districtSpecificPortfolioAnswersDAO.findSpecificPortfolioAnswers(teacherDetail,jobOrder,dspqType);
					System.out.println("lastList::::::::"+lastList.size());
					Map<Integer, DistrictSpecificPortfolioAnswers> map = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
	
					for(DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : lastList)
						map.put(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId(), districtSpecificPortfolioAnswers);
	
					DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers = null;
					List<DistrictSpecificPortfolioOptions> questionOptionsList = null;
					String questionInstruction = null;
					String shortName = "";
				
					String selectedOptions = "",insertedRanks="";
						for (DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions : districtSpecificPortfolioQuestionsList) 
						{
							shortName = districtSpecificPortfolioQuestions.getQuestionTypeMaster().getQuestionTypeShortName();
							sb.append("<input type='hidden' name='dspqType' id='dspqType' value='"+dspqType+"'  />");
							sb.append("<input type='hidden' name='jobCategoryIdForDSPQ'   id='jobCategoryIdForDSPQ' value='"+jobCategoryId+"'  />");
							sb.append("<tr>");
							sb.append("<td width='90%'>");
							sb.append("<div><b>Question "+(++cnt)+" of "+totalQuestions+"</b>");
							if(districtSpecificPortfolioQuestions.getIsRequired()==1){
								sb.append("<span class=\"required\" >*</span>");
							}
							sb.append("</div>");
							sb.append("<input type='hidden' name='o_maxMarksS' value='"+(districtSpecificPortfolioQuestions.getMaxMarks()==null?0:districtSpecificPortfolioQuestions.getMaxMarks())+"'  />");
							//questionInstruction = districtSpecificPortfolioQuestions.getQuestionInstructions()==null?"":districtSpecificPortfolioQuestions.getQuestionInstructions();
							//if(!questionInstruction.equals(""))
								//sb.append(""+Utility.getUTFToHTML(questionInstruction)+"");
	
							sb.append("<div id='QS"+cnt+"question'>"+Utility.getUTFToHTML(districtSpecificPortfolioQuestions.getQuestion())+"</div>");
	
							questionOptionsList = districtSpecificPortfolioQuestions.getQuestionOptions();
							districtSpecificPortfolioAnswers = map.get(districtSpecificPortfolioQuestions.getQuestionId());
	
							String checked = "";
							Integer optId = 0;
							String insertedText = "";
							if(districtSpecificPortfolioAnswers!=null)
							{
								if(!shortName.equalsIgnoreCase("OSONP") && !shortName.equalsIgnoreCase("mloet") && !shortName.equalsIgnoreCase("DD") && !shortName.equalsIgnoreCase("drsls") && !shortName.equalsIgnoreCase("mlsel") && !shortName.equalsIgnoreCase("rt") && !shortName.equalsIgnoreCase("sscb") && shortName.equalsIgnoreCase(districtSpecificPortfolioAnswers.getQuestionType()))
								{
									if(districtSpecificPortfolioAnswers.getSelectedOptions()!="" && districtSpecificPortfolioAnswers.getSelectedOptions()!=null)
									{
										if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null)
											optId = Integer.parseInt(districtSpecificPortfolioAnswers.getSelectedOptions());
									}
	
									insertedText = districtSpecificPortfolioAnswers.getInsertedText();
								}
								if(shortName.equalsIgnoreCase("mloet") || shortName.equalsIgnoreCase("OSONP") || shortName.equalsIgnoreCase("sscb") || shortName.equalsIgnoreCase("sloet"))
								{
									insertedText = districtSpecificPortfolioAnswers.getInsertedText();
								}
							}
							
							if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel")|| shortName.equalsIgnoreCase("lkts"))
							{
								if(districtSpecificPortfolioQuestions.getQuestion().equals("Have you ever worked for Columbus City Schools?")){
									sb.append("<input type='hidden' name='columbus_setting' class='columbus_setting' value='"+insertedText+"' id='QS"+districtSpecificPortfolioQuestions.getQuestionId()+"columbus'/>");
								}
								sb.append("<table>");
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									String functionToCall="";
									int functionPerform=0;
										if(questionOptions!=null && questionOptions.getStatus().equalsIgnoreCase("A")){
											if(optId.equals(questionOptions.getOptionId()))
												checked = "checked";
											else
												checked = "";
											
											if(districtSpecificPortfolioQuestions.getQuestion().equals("Have you ever worked for Columbus City Schools?")){
												System.out.println("::::::::::::::Question #######::::::::"+districtSpecificPortfolioQuestions.getQuestion());
												if(questionOptions.getQuestionOption().equals("Yes"))
													functionPerform =1;
												functionToCall ="columbusCheck(this,"+districtSpecificPortfolioQuestions.getQuestionId()+","+teacherDetail.getTeacherId()+","+functionPerform+");";
												System.out.println(":::::::::::::This found now:::::::::::::"+functionToCall);
												if(districtSpecificPortfolioAnswers!=null)
												insertedText = districtSpecificPortfolioAnswers.getInsertedText();
												else{
													insertedText = "";
												}
											}
			
											sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" onchange='"+functionToCall+"'/></div>" +
													" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
													"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
											sb.append("</td></tr>");
									}
								}
								sb.append("</table>");
							}
							if(shortName.equalsIgnoreCase("rt")){	
									if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getInsertedRanks()!=null){
										insertedRanks = districtSpecificPortfolioAnswers.getInsertedRanks()==null?"":districtSpecificPortfolioAnswers.getInsertedRanks();
									}
									String[] ranks = insertedRanks.split("\\|");
									int rank=1;
									int count=0;
									String ans = "";
									sb.append("<table>");
									Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
									for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
										optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
									}
									for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
										if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
											try{ans =ranks[count];}catch(Exception e){}
											sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnkS"+rank+"' name='rankS' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRankForPortfolio(this);' value='"+ans+"'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
											sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"' /><input type='hidden' name='optS' value='"+teacherQuestionOption.getOptionId()+"' />");
											sb.append("<input type='hidden' name='o_rankS' value='"+(teacherQuestionOption.getRank()==null?0:teacherQuestionOption.getRank())+"' />");
											sb.append("<script type=\"text/javascript\" language=\"javascript\">");
											//sb.append("document.getElementById('rnkS1').focus();");
											sb.append("</script></td></tr>");
											rank++;
											count++;
										}
									}
									sb.append("</table >");
							}
							if(shortName.equalsIgnoreCase("mlsel")){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								String ansId="";
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											//System.out.println("ansId:::"+ansId);
											if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
												//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
												if(ansId!=null && !ansId.equals("")){
													ansId="checked";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									
									sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
									sb.append("</td></tr>");
									multiCount++;
								}
								}
								sb.append("</table >");
							}
													
							if(shortName.equalsIgnoreCase("mloet")){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								
								List<DistrictSpecificPortfolioOptions> tempDspqOptionsList = new ArrayList<DistrictSpecificPortfolioOptions>(questionOptionsList);
								List<DistrictSpecificPortfolioOptions> optionsWithOrderList = new ArrayList<DistrictSpecificPortfolioOptions>();
								
								for(DistrictSpecificPortfolioOptions districtSpecificPortfolioOptions : questionOptionsList){
									if(districtSpecificPortfolioOptions!=null && districtSpecificPortfolioOptions.getOptionOrder()!=null && districtSpecificPortfolioOptions.getStatus()!=null && districtSpecificPortfolioOptions.getStatus().equalsIgnoreCase("A"))
										optionsWithOrderList.add(districtSpecificPortfolioOptions);
								}
							//	System.out.println(" mloet tempDspqOptionsList.size() with questionOptionsList : "+tempDspqOptionsList.size());
								if(optionsWithOrderList!=null && optionsWithOrderList.size()>0){
									java.util.Collections.sort(optionsWithOrderList, new Comparator<DistrictSpecificPortfolioOptions>(){
										 public int compare(DistrictSpecificPortfolioOptions f1, DistrictSpecificPortfolioOptions f2){
											 if(f1!=null && f1.getOptionOrder()!=null && f2!=null && f2.getOptionOrder()!=null){
												 return f1.getOptionOrder().compareTo(f2.getOptionOrder());
											 }
											 else return 0;
										 }
									});
									tempDspqOptionsList = new ArrayList<DistrictSpecificPortfolioOptions>(optionsWithOrderList);
									//System.out.println(" mloet tempDspqOptionsList.size() with optionsWithOrderList : "+tempDspqOptionsList.size());
								}
								
								String ansId="";
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : tempDspqOptionsList) {
									if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
												if(ansId!=null && !ansId.equals("")){
													ansId="checked";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									
									sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
									sb.append("</td></tr>");
									multiCount++;
								}
								}
								sb.append("</table >");
								String questionInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								sb.append(questionInstructions+"</br><span style=''><textarea  name='QS"+cnt+"optmloet' id='QS"+cnt+"optmloet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('QS"+cnt+"optmloet').focus();");
								sb.append("</script>");
							}						   
							if(shortName.equalsIgnoreCase("sloet")){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								
								List<DistrictSpecificPortfolioOptions> tempDspqOptionsList = new ArrayList<DistrictSpecificPortfolioOptions>(questionOptionsList);
								List<DistrictSpecificPortfolioOptions> optionsWithOrderList = new ArrayList<DistrictSpecificPortfolioOptions>();
								
								for(DistrictSpecificPortfolioOptions districtSpecificPortfolioOptions : questionOptionsList){
									if(districtSpecificPortfolioOptions!=null && districtSpecificPortfolioOptions.getOptionOrder()!=null && districtSpecificPortfolioOptions.getStatus()!=null && districtSpecificPortfolioOptions.getStatus().equalsIgnoreCase("A"))
										optionsWithOrderList.add(districtSpecificPortfolioOptions);
								}
							//	System.out.println(" mloet tempDspqOptionsList.size() with questionOptionsList : "+tempDspqOptionsList.size());
								if(optionsWithOrderList!=null && optionsWithOrderList.size()>0){
									java.util.Collections.sort(optionsWithOrderList, new Comparator<DistrictSpecificPortfolioOptions>(){
										 public int compare(DistrictSpecificPortfolioOptions f1, DistrictSpecificPortfolioOptions f2){
											 if(f1!=null && f1.getOptionOrder()!=null && f2!=null && f2.getOptionOrder()!=null){
												 return f1.getOptionOrder().compareTo(f2.getOptionOrder());
											 }
											 else return 0;
										 }
									});
									tempDspqOptionsList = new ArrayList<DistrictSpecificPortfolioOptions>(optionsWithOrderList);
									//System.out.println(" mloet tempDspqOptionsList.size() with optionsWithOrderList : "+tempDspqOptionsList.size());
								}
								
								String ansId="";
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : tempDspqOptionsList) {
									if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
												if(ansId!=null && !ansId.equals("")){
													ansId="checked";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									
									sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 0px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
									sb.append("</td></tr>");
									multiCount++;
								}
								}
								sb.append("</table >");
								String questionInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								sb.append(questionInstructions+"</br><span style=''><textarea  name='QS"+cnt+"optmloet' id='QS"+cnt+"optmloet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('QS"+cnt+"optmloet').focus();");
								sb.append("</script>");
							}
							if(shortName.equalsIgnoreCase("it"))
							{
								sb.append("<table >");
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='optS' value='"+teacherQuestionOption.getOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherQuestionOption.getQuestionOption()+"'> ");
										sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
									}
								}
								sb.append("</table>");
							}
							if(shortName.equalsIgnoreCase("et"))
							{
								sb.append("<table>");
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									if(questionOptions!=null && questionOptions.getStatus().equalsIgnoreCase("A")){
										if(optId.equals(questionOptions.getOptionId()))
											checked = "checked";
										else
											checked = "";
		
										sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
												" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
												"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
										sb.append("</td></tr>");
								}
								}
								sb.append("</table>");
	
								String questionInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getQuestionInstructions()+"</br>";	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								sb.append(questionInstructions+"&nbsp;&nbsp;<span style=''><textarea  name='QS"+cnt+"optet' id='QS"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('QS"+cnt+"optet').focus();");
								sb.append("</script>");
							}
							if(shortName.equalsIgnoreCase("sl") || shortName.equalsIgnoreCase("dt"))
							{
								//Years of certified teaching experience
								if(districtSpecificPortfolioQuestions.getQuestion().contains("Years of certified teaching experience")){									
									String uniqueChkId ="QS"+cnt+"opt";
									String checkedCheckBox="";
									String fieldDisable="";
									if(districtSpecificPortfolioAnswers==null){
										checkedCheckBox="";
										fieldDisable="";
									}else{
										if(insertedText.equalsIgnoreCase("0.0")){
											checkedCheckBox="checked";
											fieldDisable="disabled";
										}
									}
									
									String questionCustomInstructions="";
									try{
										if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
											questionCustomInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getQuestionInstructions();	
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									sb.append("&nbsp;&nbsp;<div class='row'><div class='col-sm-4 col-md-4'><input type='text' name='QS"+cnt+"opt' "+fieldDisable+" onkeypress='return checkForDecimalTwo(event);' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' class='form-control'/></div>");
									sb.append("<div class='col-sm-4 col-md-4'><input type='checkbox' id='QS"+cnt+"exp' "+checkedCheckBox+" style='margin-top: 10px;'>"+questionCustomInstructions+"</div></div><br>");
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
										sb.append("$('#QS"+cnt+"exp').change(function(){ $(this).each(function(){if($(this).prop('checked')){$('#"+uniqueChkId+"').val('0.0');$('#"+uniqueChkId+"').prop('disabled',true);}else{$('#"+uniqueChkId+"').val('');$('#"+uniqueChkId+"').prop('disabled',false);}});});");
									sb.append("</script>");
								}else{
									sb.append("&nbsp;&nbsp;<input type='text' name='QS"+cnt+"opt' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' style='width:98%;margin:5px;'/><br/>");
								}								
							}else if(shortName.equalsIgnoreCase("ml"))
							{
								sb.append("&nbsp;&nbsp;<textarea name='QS"+cnt+"opt' id='QS"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");					
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('QS"+cnt+"opt').focus();");
								sb.append("</script>");
								if(districtSpecificPortfolioQuestions.getQuestionId().equals(8)){
									String dbScName= "";
									String dbScId="";
									try{
										if(districtSpecificPortfolioAnswers.getSchoolMaster()!=null && !districtSpecificPortfolioAnswers.getSchoolMaster().equals("")){
											dbScName=districtSpecificPortfolioAnswers.getSchoolMaster().getSchoolName();
											dbScId=Long.toString(districtSpecificPortfolioAnswers.getSchoolMaster().getSchoolId());
										}
									}catch (Exception e) {
										// TODO: handle exception
									}
									sb.append("<label>"+lblSchoolName+"</label><br><input type='text' value='"+dbScName+"' id='schoolName' maxlength='100' name='schoolName' class='form-control' placeholder='' onfocus=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onkeyup=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onblur=\"hideSchoolMasterDivQuestion(this,'QS"+cnt+"schoolId','divTxtShowData2');\"   style='width: 50%;'/>	");
									sb.append("<input type='hidden' id='schoolName' name='schoolName'/><input type='hidden' id='QS"+cnt+"schoolId' class='school"+cnt+"' name='schoolId' value='"+dbScId+"'/>");
									sb.append("<div id='divTxtShowData2'  onmouseover=\"mouseOverChk('divTxtShowData2','schoolName')\" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>");
								}
							}
							if(shortName.equalsIgnoreCase("OSONP"))
							{
								String optionClass="OSONP"+cnt;
								sb.append("<table>");
								Integer p1=0;
								
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								String ansId="";
								boolean openOther=false;
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									if(questionOptions!=null && questionOptions.getStatus().equalsIgnoreCase("A")){
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											if(ansId!=null && !ansId.equals("") && questionOptions.getOptionId()==Integer.parseInt(ansId)){
												if(ansId!=null && !ansId.equals("")){
													ansId="checked";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
																				
											if(questionOptions.getOpenText() && ansId.equalsIgnoreCase("checked")){
												sb.append("<script>onSelectOpenOptions('show','"+cnt+"');</script>");
											}
											
											String checkBox="";
											
											if(questionOptions.getOpenText()){
												checkBox="<input type='radio' name='"+optionClass+"' class='"+optionClass+"' value='"+questionOptions.getOptionId()+"' "+ansId+" onclick=\"onSelectOpenOptions('show','"+cnt+"');\" />";	
											}else{
												checkBox="<input type='radio' name='"+optionClass+"' class='"+optionClass+"' value='"+questionOptions.getOptionId()+"' "+ansId+" onclick=\"onSelectOpenOptions('hide','"+cnt+"');\"  />";
											}
											
											sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'>"+checkBox+"</div>" +
													" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
													"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
											sb.append("</td></tr>");
								}
							}
								sb.append("</table>");
								String questionCustomInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionCustomInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
									sb.append("<span class='hide textareaOp"+cnt+"' style='margin-left: 10px;'>"+questionCustomInstructions+"<textarea  name='QS"+cnt+"OSONP' id='QS"+cnt+"OSONP' class='form-control "+cnt+"OSONPtext' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea></span>");
								
							}
							
							if(shortName.equalsIgnoreCase("DD") || shortName.equalsIgnoreCase("drsls")){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								String questionInstructions="&nbsp;";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionInstructions=districtSpecificPortfolioQuestions.getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								sb.append(questionInstructions.trim());
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
								String ansId="";
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><select name='dropdown"+cnt+"' id='dropdown"+cnt+"' class='form-control'><option value=''>Select</option>");
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											//System.out.println("ansId:::"+ansId);
											if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
												//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
												if(ansId!=null && !ansId.equals("")){
													ansId="selected";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									sb.append("<option "+ansId+" value='"+teacherQuestionOption.getOptionId()+"'>"+teacherQuestionOption.getQuestionOption()+"</option>");
									//sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
									
									multiCount++;
								}
								}
								sb.append("</select></td></tr>");
								//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
								sb.append("</table><br>");
																
								
								
							}
							if(shortName.equalsIgnoreCase("sswc"))
							{
								sb.append("<table style='margin-bottom:10px'>");
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									if(questionOptions!=null && questionOptions.getStatus().equalsIgnoreCase("A")){
									if(optId.equals(questionOptions.getOptionId()))
										checked = "checked";
									else
										checked = "";
	
									sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
											" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
											"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
									sb.append("</td></tr>");
									}
								}
								sb.append("</table>");
	
								String questionInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								sb.append(questionInstructions+"<br>");
								
								if(districtSpecificPortfolioQuestions.getQuestion().equalsIgnoreCase("Have you previously worked for UCSN?")){
									
									System.out.println("insertedText.trim()   "+insertedText.trim());
									String[] arr = new String[2]; 
									if(insertedText.equalsIgnoreCase("##")){
										arr[0]="";
										arr[1]="";
									}else if(insertedText.contains("##")){
										arr = insertedText.split("##");
									}else{
										arr[0]=insertedText;
										arr[1]="";
									}
									
									sb.append("<div class='row'><div class='col-sm-3 col-md-3'><input type='text' name='QS"+cnt+"optet' id='QS"+cnt+"optet1' class='form-control' value='"+arr[0].trim()+"'></div>");
									sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet1', 'QS"+cnt+"optet1', '%m-%d-%Y');</script>");									
									sb.append("<div class='col-sm-3 col-md-3'><input type='text'  name='QS"+cnt+"optet' id='QS"+cnt+"optet2' class='form-control' value='"+arr[1].trim()+"'></div>");
									sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet2', 'QS"+cnt+"optet2', '%m-%d-%Y');</script>");									
								}else if(districtSpecificPortfolioQuestions.getQuestion().equalsIgnoreCase("Have you ever worked for Columbus City Schools?")){
									System.out.println("::::::::::::::::::::Found Columbus:::::::::::::::::::::");
									System.out.println("insertedText.trim()   "+insertedText.trim());
									String[] arr = new String[2]; 
									if(insertedText.equalsIgnoreCase("##")){
										arr[0]="";
										arr[1]="";
									}else if(insertedText.contains("##")){
										arr = insertedText.split("##");
									}else{
										arr[0]=insertedText;
										arr[1]="";
									}
									
									sb.append("<div class='row'><div class='col-sm-3 col-md-3'><input type='text' name='QS"+cnt+"optet' id='QS"+cnt+"optet1' class='form-control' value='"+arr[0].trim()+"'></div>");
									sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet1', 'QS"+cnt+"optet1', '%m-%d-%Y');</script>");									
									sb.append("<div class='col-sm-3 col-md-3'><input type='text'  name='QS"+cnt+"optet' id='QS"+cnt+"optet2' class='form-control' value='"+arr[1].trim()+"'></div>");
									sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet2', 'QS"+cnt+"optet2', '%m-%d-%Y');</script>");
									sb.append("<div class='col-sm-3 col-md-3'><input type='text'  name='QS"+cnt+"optet' id='QS"+cnt+"optet3' class='form-control' value='"+arr[1].trim()+"'></div>");
								}
								else{
									sb.append("<div class='row'><div class='col-sm-3 col-md-3'><input type='text' name='QS"+cnt+"optet' id='QS"+cnt+"optet' class='form-control' value='"+insertedText+"'></div>");
									sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet', 'QS"+cnt+"optet', '%m-%d-%Y');</script>");								
									
								}
																
								sb.append("</div>");
								sb.append("</br>");
							}
							
							if(shortName.equalsIgnoreCase("sscb")){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								String ansId="";
								int subsecCount=0; 
								
								String classSubSec="";
								List<DistrictSpecificPortfolioOptions> tempDspqOptionsList = new ArrayList<DistrictSpecificPortfolioOptions>(questionOptionsList);
								List<DistrictSpecificPortfolioOptions> optionsWithOrderList = new ArrayList<DistrictSpecificPortfolioOptions>();
								
								for(DistrictSpecificPortfolioOptions districtSpecificPortfolioOptions : questionOptionsList){
									if(districtSpecificPortfolioOptions!=null && districtSpecificPortfolioOptions.getOptionOrder()!=null && districtSpecificPortfolioOptions.getStatus()!=null && districtSpecificPortfolioOptions.getStatus().equalsIgnoreCase("A"))
										optionsWithOrderList.add(districtSpecificPortfolioOptions);
								}
								System.out.println(" sscb tempDspqOptionsList.size() with questionOptionsList : "+tempDspqOptionsList.size());
								for (DistrictSpecificPortfolioOptions teacherQuestionOptionHD : questionOptionsList) {
									if(teacherQuestionOptionHD.getSubSectionTitle()!=null && teacherQuestionOptionHD.getSubSectionTitle().equalsIgnoreCase("Y")){
										subsecCount++;
										classSubSec="sectionCnt"+subsecCount+"";
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;' class='secHeading' id='subSection"+cnt+"'>"+teacherQuestionOptionHD.getQuestionOption()+"");
										sb.append("</td></tr>");
									//}
									
										if(optionsWithOrderList!=null && optionsWithOrderList.size()>0){
											java.util.Collections.sort(optionsWithOrderList, new Comparator<DistrictSpecificPortfolioOptions>(){
												 public int compare(DistrictSpecificPortfolioOptions f1, DistrictSpecificPortfolioOptions f2){
													 if(f1!=null && f1.getOptionOrder()!=null && f2!=null && f2.getOptionOrder()!=null){
//														 System.out.println("::::::::::::::::::: f1 : "+f1.getOptionOrder()+" : f2 : "+f2.getOptionOrder()+": r : "+f1.getOptionOrder().compareTo(f2.getOptionOrder()));
														 return f1.getOptionOrder().compareTo(f2.getOptionOrder());
													 }
													 else return 0;
												 }
											});
											tempDspqOptionsList = new ArrayList<DistrictSpecificPortfolioOptions>(optionsWithOrderList);
											System.out.println(" sscb tempDspqOptionsList.size() with optionsWithOrderList : "+tempDspqOptionsList.size());
										}
										
									for (DistrictSpecificPortfolioOptions teacherQuestionOption : tempDspqOptionsList) {
											if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
											try{
												ansId="";
												for(int i=0;i<multiCounts.length;i++){
													try{ansId =multiCounts[i];}catch(Exception e){}
													//System.out.println("ansId:::"+ansId);
													if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
														//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
														if(ansId!=null && !ansId.equals("")){
															ansId="checked";
														}
														break;
													}
												}
											}catch(Exception e){
												e.printStackTrace();
											}
										
											if(teacherQuestionOption.getSubSectionTitle()!=null && teacherQuestionOption.getSubSectionTitle().equalsIgnoreCase(subsecCount+"")){
												/*subsecCount++;
												classSubSec="sectionCnt"+subsecCount+"";
												sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;' class='secHeading' id='subSection"+cnt+"'>"+teacherQuestionOption.getQuestionOption()+"");
												sb.append("</td></tr>");
											}else{*/
												sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='"+classSubSec+"' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'>&nbsp;&nbsp;&nbsp;"+teacherQuestionOption.getQuestionOption()+"<input type='hidden' id='QS"+teacherQuestionOption.getOptionId()+"validQuestion' value='"+teacherQuestionOption.getValidOption()+"'>");
												if(districtSpecificPortfolioQuestions.getDistrictMaster().getDistrictId().equals(7800040) && teacherQuestionOption.getQuestionOption().equalsIgnoreCase("College or University if selected, please choose the name of the school from the drop down"))
												{
													String dbUName= "";
													String dbUId="";
													try{
														if(districtSpecificPortfolioAnswers.getUniversityId()!=null && !districtSpecificPortfolioAnswers.getUniversityId().equals("")){
															dbUName=districtSpecificPortfolioAnswers.getUniversityId().getUniversityName();
															dbUId=districtSpecificPortfolioAnswers.getUniversityId().getUniversityId()+"";
														}
													}catch (Exception e) {
														// TODO: handle exception
													}											
													sb.append("<br><input type='text' value='"+dbUName+"' id='schoolName' maxlength='100' name='schoolName' class='form-control' placeholder='' onfocus=\"getUniversityAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onkeyup=\"getUniversityAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onblur=\"hideSchoolMasterDivQuestion(this,'QS"+cnt+"schoolId','divTxtShowData2');\" style='width:58%;display:inline;margin-left:5px;'/>");
													//sb.append("<label>School Name</label><br><input type='text' value='"+dbScName+"' id='schoolName' maxlength='100' name='schoolName' class='form-control' placeholder='' onfocus=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onkeyup=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onblur=\"hideSchoolMasterDivQuestion(this,'QS"+cnt+"schoolId','divTxtShowData2');\" style='width:58%;display:inline;'/>	");
													sb.append("<input type='hidden' id='schoolName' name='schoolName'/><input type='hidden' id='QS"+cnt+"schoolId' class='school"+cnt+"' name='schoolId' value='"+dbUId+"'/>");
													sb.append("<div id='divTxtShowData2'  onmouseover=\"mouseOverChk('divTxtShowData2','schoolName')\" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>");
													
												}
												if(districtSpecificPortfolioQuestions.getDistrictMaster().getDistrictId().equals(7800040) && teacherQuestionOption.getQuestionOption().contains("UNO Charter School Network Employee"))
												{
													sb.append("<input type='text'  name='QS"+cnt+"multiselectText' id='QS"+cnt+"multiselectText' class='form-control' style='width:35%;margin-left:5px;' value='"+insertedText+"'>");
												}
												//UNO Charter School Network Employee 
												sb.append("</td></tr>");
												multiCount++;										
											}		
										}
										
									}
								}
							}//main loop
								sb.append("</table >");
								
							}
							if(shortName.equalsIgnoreCase("UAT") || shortName.equalsIgnoreCase("UATEF"))
							{
								//sb.append("&nbsp;&nbsp;<input type='file' name='QS"+cnt+"opt' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' style='width:98%;margin:5px;'/><br/>");
								
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								String questionInstructions="&nbsp;";
								try{
									if(districtSpecificPortfolioQuestions.getQuestionInstructions()!=null){
										questionInstructions=districtSpecificPortfolioQuestions.getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								//sb.append(questionInstructions.trim());
								
								if(questionOptionsList!=null && questionOptionsList.size()>0){
										sb.append("<table>");
										Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
										for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
											optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
										}
										//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
										String ansId="";
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><select name='dropdown"+cnt+"' id='dropdown"+cnt+"' class='form-control'><option value=''>Select</option>");
										for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
											if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
											try{
												ansId="";
												for(int i=0;i<multiCounts.length;i++){
													try{ansId =multiCounts[i];}catch(Exception e){}
													//System.out.println("ansId:::"+ansId);
													if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
														//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
														if(ansId!=null && !ansId.equals("")){
															ansId="selected";
														}
														break;
													}
												}
											}catch(Exception e){
												e.printStackTrace();
											}
											sb.append("<option "+ansId+" value='"+teacherQuestionOption.getOptionId()+"'>"+teacherQuestionOption.getQuestionOption()+"</option>");
											//sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
											
											multiCount++;
											}
										}
										sb.append("</select></td></tr>");
										//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
										sb.append("</table>");
								}	
								sb.append("<div id='answerDiv'>");								
									sb.append("<iframe id='uploadFrameAnswerId' name='uploadFrameAnswer' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
									sb.append("<form id='frmAnswerUpload' enctype='multipart/form-data' method='post' target='uploadFrameAnswer' action='answerUploadServlet.do' class='form-inline'>");
										sb.append("<div class='row'>");
											sb.append("<div class='col-sm-12 col-md-12'>");
												sb.append("<div class='divErrorMsg' id='errAnswer' style='display: block;'></div>");
											sb.append("</div>");
											
											sb.append("<div class='col-sm-12 col-md-12'>");
												if(questionInstructions!="" && !questionInstructions.equals("&nbsp;"))
													sb.append("<span>"+questionInstructions.trim()+"</span><br/>");
												sb.append("<div class='col-sm-6 col-md-6 top5'>");
													sb.append("<input name='QS"+cnt+"File' id='QS"+cnt+"File' type='file'/>");
												sb.append("</div>");
												sb.append("<div class='col-sm-6 col-md-6 top5' id='divAnswerTxt'>");
													String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
													String fileName="";
													if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getFileName()!=null && !districtSpecificPortfolioAnswers.getFileName().equalsIgnoreCase(""))
													{
														fileName=districtSpecificPortfolioAnswers.getFileName();
														sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+lnkViewAnswer+"' id='hrefAnswer' onclick=\"downloadAnswer(0,'"+districtSpecificPortfolioAnswers.getAnswerId()+"');"+windowFunc+"\">"+lnkV+"</a>");
														sb.append("<script>$('#hrefAnswer').tooltip();</script>");
													}
												sb.append("</div>");

											sb.append("</div>");
											sb.append("<input type='hidden' name='answerFileName' id='answerFileName' value='"+fileName+"'>");
											sb.append("<input type='hidden' name='editCaseFile' id='editCaseFile' value='"+fileName+"'>");
											sb.append("<input type='hidden' name='answerFilePath' id='answerFilePath'>");
											sb.append("<input type='hidden' name='hdnQues' id='hdnQues' value='QS"+cnt+"File'>");										
										sb.append("</div>");
									sb.append("</form>");
								sb.append("</div>");
								if(districtSpecificPortfolioQuestions.getDistrictMaster().getDistrictId()==1302010 && districtSpecificPortfolioQuestions.getQuestion().equalsIgnoreCase("List sports clubs/activities you would be willing to coach/sponsor (attach coaching experience resume to profile)."))
								{
									sb.append("</br><span style=''><textarea  name='QS"+cnt+"Text' id='QS"+cnt+"Text' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
								}
							}
							
							sb.append("<input type='hidden' id='QS"+cnt+"isRequired' value='"+districtSpecificPortfolioQuestions.getIsRequired()+"'/>" );
							sb.append("<input type='hidden' id='QS"+cnt+"questionId' value='"+districtSpecificPortfolioQuestions.getQuestionId()+"'/>" );
							sb.append("<input type='hidden' id='QS"+cnt+"questionTypeId' value='"+districtSpecificPortfolioQuestions.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
							sb.append("<input type='hidden' id='QS"+cnt+"questionTypeShortName' value='"+shortName+"'/>");	
							sb.append("</td>");
							sb.append("</tr>");
							insertedText = "";
						}
						if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==7800047){
						sb.append("<b>"+msgReasonableAccommodationNotice1+"</b>" +
								"<p>"+msgReasonableAccommodationNotice2+"</p>" +
								"<p>"+msgReasonableAccommodationNotice3+"</p>" +
								"<p>"+msgReasonableAccommodationNotice4+"<p>");
						}
				    }
				
				if(jobOrder.getDistrictMaster()!=null)
					sb.append("@##@"+cnt+"@##@"+jobOrder.getDistrictMaster().getTextForDistrictSpecificQuestions());
				else
					sb.append("@##@"+cnt+"@##@"+"");
			}
			//////////////////////	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	
		
	}
	
	//@Transactional(readOnly=false)
	public String setDistrictQPortfoliouestions(DistrictSpecificPortfolioAnswers[] districtSpecificPortfolioAnswersList,int dspqType)
	{
		System.out.println("@@@@@@@@@@@@@:::::::::::::::    setDistrictQPortfoliouestions    :::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		TeacherDetail teacherDetail = null;

		if (session == null || session.getAttribute("teacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

		try {
				if(districtSpecificPortfolioAnswersList.length>0){
					int inValidCount = 0;
					JobOrder jobOrder = districtSpecificPortfolioAnswersList[0].getJobOrder();
					if(jobOrder!=null && jobOrder.getJobId()!=null)
					jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
		
					Map<Integer,Integer> map = new HashMap<Integer, Integer>();
					Map<Integer,DistrictSpecificPortfolioAnswers> mapDistrictSpecificPortfolioAnswers = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();			
					Map<String, String> questionOptions = new HashMap<String, String>();
					Map<String, TeacherSecondaryStatus> teacherTags = new HashMap<String, TeacherSecondaryStatus>();
					Map<Integer, DistrictSpecificPortfolioQuestions> questTags = new HashMap<Integer, DistrictSpecificPortfolioQuestions>();
					Map<Long, SchoolMaster> schoolMap = new HashMap<Long, SchoolMaster>();
					Map<Integer, UniversityMaster> universityMap = new HashMap<Integer, UniversityMaster>();
					UserMaster userMaster=null;
					try {
						Criterion criterionDis = Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
						
						List<SchoolMaster> schList = schoolMasterDAO.findDistrictSchoolId(jobOrder.getDistrictMaster());
						if(schList!=null && schList.size()>0){
							for (SchoolMaster schoolMaster : schList) {
								schoolMap.put(schoolMaster.getSchoolId(), schoolMaster);
							}
						}
						userMaster = userMasterDAO.findById(1, false,false);
						
						
						List<UniversityMaster> universityMasters = universityMasterDAO.findAll();						
						if(universityMasters!=null && universityMasters.size()>0){
							for (UniversityMaster universityMaster : universityMasters) {
								universityMap.put(universityMaster.getUniversityId(), universityMaster);
							}
							
						}
						
						List<DistrictSpecificPortfolioAnswers> dspqList= districtSpecificPortfolioAnswersDAO.findSpecificPortfolioAnswers(teacherDetail,jobOrder,dspqType);
						for (DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : dspqList) {
							map.put(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId(), districtSpecificPortfolioAnswers.getAnswerId());
							mapDistrictSpecificPortfolioAnswers.put(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId(), districtSpecificPortfolioAnswers);
						}
						
						List<SecondaryStatusMaster> tagIds= new ArrayList<SecondaryStatusMaster>();
						for (DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : dspqList) {
							map.put(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId(), districtSpecificPortfolioAnswers.getAnswerId());					
							
						}
							
						
						List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsLst = districtSpecificPortfolioQuestionsDAO.findByCriteria(criterionDis);
						
						for (DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions : districtSpecificPortfolioQuestionsLst) {
							questTags.put(districtSpecificPortfolioQuestions.getQuestionId(), districtSpecificPortfolioQuestions);					
							if(districtSpecificPortfolioQuestions.getProvideTag()){					
								tagIds.add(districtSpecificPortfolioQuestions.getSecondaryStatusMaster());
							}
						}
						
						Criterion criterion1 = Restrictions.in("districtSpecificPortfolioQuestions", districtSpecificPortfolioQuestionsLst);
						List<DistrictSpecificPortfolioOptions> listPortfolioOptions = districtSpecificPortfolioOptionsDAO.findByCriteria(criterion1);
						
						for (DistrictSpecificPortfolioOptions districtSpecificPortfolioOptions : listPortfolioOptions) {
							String questionOption = districtSpecificPortfolioOptions.getDistrictSpecificPortfolioQuestions().getQuestionId()+"##"+districtSpecificPortfolioOptions.getOptionId();
							questionOptions.put(questionOption, districtSpecificPortfolioOptions.getQuestionOption());
						}			
						
						Criterion criterion2 = Restrictions.in("secondaryStatusMaster", tagIds);
						Criterion criterion3 = Restrictions.eq("teacherDetail",teacherDetail);
						List<TeacherSecondaryStatus> tSecStatusList = new ArrayList<TeacherSecondaryStatus>();
						
						if(tagIds.size()>0){
							tSecStatusList = teacherSecondaryStatusDAO.findByCriteria(criterion3,criterion2);
						}
						if(tSecStatusList.size()>0)
						for (TeacherSecondaryStatus teacherSecondaryStatus : tSecStatusList) {
							teacherTags.put(teacherSecondaryStatus.getTeacherDetail().getTeacherId()+"##"+teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusId(), teacherSecondaryStatus);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
		
					List<Integer> answers = new ArrayList<Integer>();
					Integer ansId = null;
					for (DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : districtSpecificPortfolioAnswersList) {
						districtSpecificPortfolioAnswers.setTeacherDetail(teacherDetail);
						districtSpecificPortfolioAnswers.setDistrictMaster(jobOrder.getDistrictMaster());
						districtSpecificPortfolioAnswers.setCreatedDateTime(new Date());
		//universityMap
						
						if(districtSpecificPortfolioAnswers.getSchoolIdTemp()!=null && districtSpecificPortfolioAnswers.getQuestionType().equalsIgnoreCase("sscb")){						
							Integer uId =  (int) (long) districtSpecificPortfolioAnswers.getSchoolIdTemp();							
							if(districtSpecificPortfolioAnswers.getSchoolIdTemp()!=null && universityMap.containsKey(uId)){								
								districtSpecificPortfolioAnswers.setUniversityId(universityMap.get(uId));
							}
						}
						else if(districtSpecificPortfolioAnswers.getSchoolIdTemp()!=null && schoolMap.containsKey(districtSpecificPortfolioAnswers.getSchoolIdTemp())){
							districtSpecificPortfolioAnswers.setSchoolMaster(schoolMap.get(districtSpecificPortfolioAnswers.getSchoolIdTemp()));
						}
						
						ansId = map.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId());
						if(ansId!=null)
						{
							districtSpecificPortfolioAnswers.setAnswerId(ansId);
							
							if(mapDistrictSpecificPortfolioAnswers.containsKey(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId())){
								DistrictSpecificPortfolioAnswers tempDistrictSpecificPortfolioAnswers =mapDistrictSpecificPortfolioAnswers.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId());
								districtSpecificPortfolioAnswers.setSliderScore(tempDistrictSpecificPortfolioAnswers.getSliderScore());
								districtSpecificPortfolioAnswers.setScoreBy(tempDistrictSpecificPortfolioAnswers.getScoreBy());
								districtSpecificPortfolioAnswers.setScoreDateTime(tempDistrictSpecificPortfolioAnswers.getScoreDateTime());
							}
						}
						if(!districtSpecificPortfolioAnswers.getQuestionType().equalsIgnoreCase("ml"))
						{
							if(districtSpecificPortfolioAnswers.getIsValidAnswer()!=null && districtSpecificPortfolioAnswers.getIsValidAnswer()==false)
								inValidCount++;
						}
						System.out.println("dspqType::::::::::::::"+dspqType);
						if(dspqType==1){
							districtSpecificPortfolioAnswers.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
							districtSpecificPortfolioAnswers.setJobOrder(null);
						}else if(dspqType==0){
							districtSpecificPortfolioAnswers.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
							districtSpecificPortfolioAnswers.setJobOrder(jobOrder);
						}else{
							districtSpecificPortfolioAnswers.setJobOrder(null);
							districtSpecificPortfolioAnswers.setJobCategoryMaster(null);
						}
						
						districtSpecificPortfolioAnswers.setIsActive(true);
						districtSpecificPortfolioAnswersDAO.makePersistent(districtSpecificPortfolioAnswers);
						answers.add(districtSpecificPortfolioAnswers.getAnswerId());
						
						
						if(questTags.containsKey(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()) && questTags.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()).getProvideTag()){
							DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions = questTags.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId());
							String quesOpt = (districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()+"##"+districtSpecificPortfolioAnswers.getSelectedOptions()).replace("|", "");
							
							if(questionOptions.get(quesOpt).equalsIgnoreCase("Yes") && !teacherTags.containsKey(teacherDetail.getTeacherId()+"##"+districtSpecificPortfolioQuestions.getSecondaryStatusMaster().getSecStatusId())){
								TeacherSecondaryStatus teacherSecondaryStatus = new TeacherSecondaryStatus();					
								teacherSecondaryStatus.setTeacherDetail(teacherDetail);
								teacherSecondaryStatus.setJobOrder(null);
								teacherSecondaryStatus.setSecondaryStatusMaster(districtSpecificPortfolioQuestions.getSecondaryStatusMaster());
								teacherSecondaryStatus.setCreatedByUser(userMaster);
								teacherSecondaryStatus.setCreatedByEntity(userMaster.getEntityType());
								teacherSecondaryStatus.setDistrictMaster(jobOrder.getDistrictMaster());
								teacherSecondaryStatus.setCreatedDateTime(new Date());
								teacherSecondaryStatusDAO.makePersistent(teacherSecondaryStatus);					
							}
							if((questionOptions.get(quesOpt).equalsIgnoreCase("No") || questionOptions.get(quesOpt).equalsIgnoreCase("Prefer Not To Answer")) && teacherTags.containsKey(teacherDetail.getTeacherId()+"##"+districtSpecificPortfolioQuestions.getSecondaryStatusMaster().getSecStatusId())){
									TeacherSecondaryStatus teacherSecondaryStatus = teacherTags.get(teacherDetail.getTeacherId()+"##"+districtSpecificPortfolioQuestions.getSecondaryStatusMaster().getSecStatusId());				
									teacherSecondaryStatusDAO.makeTransient(teacherSecondaryStatus);					
							}
						}
					}
					return "1";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	@Transactional
	public int saveOrUpdateCertificationsDSPQNoble(int certId,int certificationStatusMaster,String certText)
	{	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		System.out.println("call nobler method");
		
		try {
			TeacherCertificate teacherCertificate =new TeacherCertificate();
			CertificationStatusMaster certificationStatusMasterObj = certificationStatusMasterDAO.findById(certificationStatusMaster, false, false);
			if(certId!=0)
				teacherCertificate = teacherCertificateDAO.findById(certId, false, false);
			
			teacherCertificate.setCertificationStatusMaster(certificationStatusMasterObj);	
			teacherCertificate.setCertText(certText);
			if(teacherCertificate.getCertId()==null){
				teacherCertificate.setCreatedDateTime(new Date());
				teacherCertificate.setTeacherDetail(teacherDetail);				
			}else{
				teacherCertificate.setCertId(teacherCertificate.getCertId());
			}
			
			teacherCertificateDAO.makePersistent(teacherCertificate);

			return teacherCertificate.getCertId();
		} catch (Exception e) {
			e.printStackTrace();
		//	return teacherCertificate.getCertId();
		}
		return 2;
	}
	
	@Transactional
	public String getPFCertificationsGridDspqNoble(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
				
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"certType";
			String sortOrderNoField		=	"certType";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("state")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("certificationStatusMaster")){
					 sortOrderNoField="certificationStatusMaster";
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			
			List<TeacherCertificate> listTeacherCertificates = null;
			listTeacherCertificates = teacherCertificateDAO.findSortedCertificateByTeacher(sortOrderStrVal,teacherDetail);
			
			List<TeacherCertificate> sortedlistTeacherCertificates		=	new ArrayList<TeacherCertificate>();
			
			SortedMap<String,TeacherCertificate>	sortedMap = new TreeMap<String,TeacherCertificate>();
			if(sortOrderNoField.equals("state"))
			{
				sortOrderFieldName	=	"state";
			}
			int mapFlag=2;
			for (TeacherCertificate trCertificate : listTeacherCertificates){
				String orderFieldName=trCertificate.getCertType();
				if(sortOrderFieldName.equals("state")){
					String stateName="";
					if(trCertificate.getStateMaster()==null){
						stateName = " ";
					}else{
						stateName = trCertificate.getStateMaster().getStateName();
					}
						
					orderFieldName=stateName+"||"+trCertificate.getCertId();
					sortedMap.put(orderFieldName+"||",trCertificate);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherCertificates=listTeacherCertificates;
			}
			
			totalRecord =sortedlistTeacherCertificates.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherCertificate> listsortedTeacherCertificates		=	sortedlistTeacherCertificates.subList(start,end);
			
			sb.append("<table border='0'  id='tblGridCertifications'  width='100%' class='table  table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");
			
			String responseText="";

			responseText=PaginationAndSorting.responseSortingMLink(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			sb.append("<th width='20%' valign='top'>"+lblCertificationDetail+"</th>");
			
			sb.append("<th width='10%' valign='top'>");
			sb.append(lblAct);
			
			
			sb.append("</tr>");
			sb.append("</thead>");
			if(teacherCertificateDAO!=null)
			for(TeacherCertificate tCertificate:listsortedTeacherCertificates)
			{
				sb.append("<tr>");
			
				sb.append("<td>");
				if(tCertificate.getCertificationStatusMaster()!=null)
					sb.append(tCertificate.getCertificationStatusMaster().getCertificationStatusName());
				else
					sb.append("");
				sb.append("</td>");
				String certHtml="";
				sb.append("<td>");
				if(tCertificate.getCertText()!=null && !tCertificate.getCertText().equals("")){
					sb.append("<a id='"+tCertificate.getCertId()+"' href='javascript:void(0)' onclick='return certTextDiv("+tCertificate.getCertId()+");'>");					
					certHtml = html2text(tCertificate.getCertText());
					if(certHtml.length()>49){
						sb.append(htmlwords(certHtml)+"...");
					}else{
						sb.append(certHtml);
					}
					sb.append("</a>");
					sb.append("<input type='hidden' value='"+tCertificate.getCertText()+"' id='cerTTextContent"+tCertificate.getCertId()+"'>" );
				}
				else
					sb.append("");
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append("<a href='#' onclick=\"return showEditForm('"+tCertificate.getCertId()+"')\" >"+lblEdit+"</a>");
				sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return deleteRecord_Certificate('"+tCertificate.getCertId()+"')\">"+lnkDlt+"</a>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			if(listTeacherCertificates==null || listTeacherCertificates.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
						
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	
	}
	
	public  String html2text(String html) {
	        
	        String text = html.replaceAll("\\<.*?\\>", "");
			return text;
		}
	public  String htmlwords(String html) {
		String htmlReturn = html.substring(0, Math.min(49, html.length()));
		return htmlReturn;
	//String truncated = explanation.subString(0, Math.min(49, explanation.length()));
	}
	
	public String getElectronicReferencesGridNoble(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb = new StringBuffer();		
		try 
		{
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------

			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			List<TeacherElectronicReferences> listTeacherElectronicReferences= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"lastName";
			String sortOrderNoField		=	"lastName";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.isNull("districtMaster");
			listTeacherElectronicReferences = teacherElectronicReferencesDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2);		

			List<TeacherElectronicReferences> sortedTeacherRole		=	new ArrayList<TeacherElectronicReferences>();

			SortedMap<String,TeacherElectronicReferences>	sortedMap = new TreeMap<String,TeacherElectronicReferences>();
			int mapFlag=2;
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherRole.add((TeacherElectronicReferences) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherRole.add((TeacherElectronicReferences) sortedMap.get(key));
				}
			}else{
				sortedTeacherRole=listTeacherElectronicReferences;
			}

			totalRecord =listTeacherElectronicReferences.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherElectronicReferences> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);

			String responseText="";
			sb.append("<table border='0' id='eleReferencesGrid' width='100%' class='table table-striped'>");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingMLink(lblRefName,sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLink(lblTitle,sortOrderFieldName,"designation",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLink(lblOrga,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink(lblEmail,sortOrderFieldName,"email",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			/*responseText=PaginationAndSorting.responseSortingMLink("Rec. Letter",sortOrderFieldName,"pathOfReference",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");*/
			
			responseText=PaginationAndSorting.responseSortingMLink(lblContactNo,sortOrderFieldName,"contactnumber",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			/*responseText=PaginationAndSorting.responseSortingMLink("Can<BR>Contact",sortOrderFieldName,"rdcontacted",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");*/
			
			
			sb.append("<th width='20%' valign='top'>");
			sb.append(lblAct);
			sb.append("</th>");	

			sb.append("</tr>");
			sb.append("</thead>");
			if(listsortedTeacherRole!=null)
				
				
				for(TeacherElectronicReferences tRole:listsortedTeacherRole)
				{
					String Salutation="";
					if(tRole.getSalutation()==0)
						Salutation="";
					else if(tRole.getSalutation()==1)
						Salutation="Mrs.";
					else if(tRole.getSalutation()==2)
						Salutation="Mr.";
					else if(tRole.getSalutation()==3)
						Salutation="Miss";
					else if(tRole.getSalutation()==4)
						Salutation="Dr.";
					else if(tRole.getSalutation()==5)
						Salutation="Ms.";
							
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(Salutation+" "+tRole.getFirstName()+" "+tRole.getLastName());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(tRole.getDesignation());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(tRole.getOrganization());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(tRole.getEmail());
					sb.append("</td>");
					
					
					/*String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
					sb.append("<td>");
					sb.append(tRole.getPathOfReference()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view recommendation letter !'  id='trans"+tRole.getElerefAutoId()+"' onclick=\"downloadReference('"+tRole.getElerefAutoId()+"','trans"+tRole.getElerefAutoId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
					sb.append("<script>$('#trans"+tRole.getElerefAutoId()+"').tooltip();</script>");
					sb.append("</td>");*/
					
					sb.append("<td>");
					sb.append(tRole.getContactnumber());
					sb.append("</td>");
					
					/*sb.append("<td>");
					if(tRole.getRdcontacted())
						sb.append("Yes");
					else if(!tRole.getRdcontacted())
						sb.append("No");
					sb.append("</td>");*/
					
					
					sb.append("<td>");
					sb.append("<a href='#' onclick=\"return editFormElectronicReferences('"+tRole.getElerefAutoId()+"')\" >"+lblEdit+"</a>");
					
					if(tRole.getStatus()==0)
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return changeStatusElectronicReferences('"+tRole.getElerefAutoId()+"','1')\">"+lblActivate+"</a>");
					else if(tRole.getStatus()==1)
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return changeStatusElectronicReferences('"+tRole.getElerefAutoId()+"','0')\">"+lblDeactivate+"</a>");
					
					sb.append("</td>");
					sb.append("</tr>");
				}

			if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='8'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationDoubleString(request,totalRecord,noOfRow, pageNo));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}
	public String saveUpdateElectronicReferences(TeacherElectronicReferences teacherElectronicReferences, Integer teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		TeacherDetail teacherDetail= null;
		teacherDetail  = teacherDetailDAO.findById(teacherId, false, false);
		List<TeacherElectronicReferences> references=null;
		try 
		{
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion_email = Restrictions.eq("email",teacherElectronicReferences.getEmail());
			
			if(teacherElectronicReferences.getElerefAutoId()==null)
			{
				teacherElectronicReferences.setStatus(1);
				teacherElectronicReferences.setCreatedDateTime(new Date());
				
				references = teacherElectronicReferencesDAO.findByCriteria(criterion,criterion_email);
				if(references!=null && references.size()>=1)
					return "isDuplicate";
			}
			else
			{
				TeacherElectronicReferences pojo=teacherElectronicReferencesDAO.findById(teacherElectronicReferences.getElerefAutoId(), false, false);
				teacherElectronicReferences.setStatus(pojo.getStatus());
				
				Criterion criterion_id = Restrictions.ne("elerefAutoId",teacherElectronicReferences.getElerefAutoId());
				references = teacherElectronicReferencesDAO.findByCriteria(criterion_id,criterion,criterion_email);
				if(references!=null && references.size()>=1)
					return "isDuplicate";
			}
			teacherElectronicReferences.setTeacherDetail(teacherDetail);
			teacherElectronicReferences.setDistrictMaster(userMaster.getDistrictId());
			teacherElectronicReferences.setUserMaster(userMaster);
			teacherElectronicReferencesDAO.makePersistent(teacherElectronicReferences);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
	public String findDuplicateReferences(TeacherElectronicReferences teacherElectronicReferences, Integer teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail	teacherDetail  = teacherDetailDAO.findById(teacherId, false, false);
		List<TeacherElectronicReferences> references=null;
		try 
		{
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion_email = Restrictions.eq("email",teacherElectronicReferences.getEmail());
			
			if(teacherElectronicReferences.getElerefAutoId()==null)
			{
				teacherElectronicReferences.setStatus(1);
				teacherElectronicReferences.setCreatedDateTime(new Date());
				
				references = teacherElectronicReferencesDAO.findByCriteria(criterion,criterion_email);
				if(references!=null && references.size()>=1)
					return "isDuplicate";
			}
			else
			{
				TeacherElectronicReferences pojo=teacherElectronicReferencesDAO.findById(teacherElectronicReferences.getElerefAutoId(), false, false);
				teacherElectronicReferences.setStatus(pojo.getStatus());
				
				Criterion criterion_id = Restrictions.ne("elerefAutoId",teacherElectronicReferences.getElerefAutoId());
				references = teacherElectronicReferencesDAO.findByCriteria(criterion_id,criterion,criterion_email);
				if(references!=null && references.size()>=1)
					return "isDuplicate";
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	public String saveOrUpdateVideoLink(TeacherVideoLink teacherVideoLink,Integer teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		DistrictMaster districtMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			teacherVideoLink.setUserMaster(userMaster);
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
				teacherVideoLink.setDistrictMaster(districtMaster);
			}
		}
		
		TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
		try
		{
			if(teacherVideoLink.getVideolinkAutoId()==null && ( teacherVideoLink.getVideourl()==null || teacherVideoLink.getVideourl().trim().equalsIgnoreCase("")) && teacherVideoLink.getVideo()==null )
				return null;
			
			String previousVideo="";			
			String video="";			
			if(request.getSession().getAttribute("previousVideoFile")!=null){
				previousVideo= request.getSession().getAttribute("previousVideoFile").toString();				
			}
			if(!previousVideo.equals(teacherVideoLink.getVideo()))
			{				
				video=teacherVideoLink.getVideo();						
				teacherVideoLink.setVideo(video);				
			}	
			if(teacherVideoLink.getVideolinkAutoId()==null)
				teacherVideoLink.setCreatedDate(new Date());
			else
			{
				TeacherVideoLink temp=teacherVideoLinksDAO.findById(teacherVideoLink.getVideolinkAutoId(), false, false);
				teacherVideoLink.setCreatedDate(temp.getCreatedDate());
			}
			
			teacherVideoLink.setTeacherDetail(teacherDetail);
			teacherVideoLinksDAO.makePersistent(teacherVideoLink);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
		
	}
	public Boolean removeReferencesForNoble(Integer referenceId,Integer teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{	
			TeacherDetail teacherDetail=null;
			
			//	System.out.println("################################### :: "+request.getParameter("teacherId"));
				teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			
			TeacherElectronicReferences te = teacherElectronicReferencesDAO.findById(referenceId, false, false);
			if(te!=null && te.getPathOfReference()!=null){
				File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+te.getPathOfReference());
				if(file.exists()){
					if(file.delete()){
						//System.out.println(file.getName()+ " deleted");
					}else{
						//System.out.println("Delete operation is failed.");
						return false;
					}
					te.setPathOfReference(null);
					teacherElectronicReferencesDAO.makePersistent(te);
					return true;
				}
				
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	public String getVideoPlayDiv(Integer videoLinkId)
	{
		WebContext context;
		String videoPath="";
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{			
			System.out.println("VideoID "+videoLinkId);
			TeacherVideoLink teacherVideoLink=null;		
			teacherVideoLink=teacherVideoLinksDAO.findById(videoLinkId, false, false);			
			
			if(teacherVideoLink.getVideo()!=null)
			{	
				TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");				
				String fileName= teacherVideoLink.getVideo().toString();
		        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/Video/"+fileName;
		        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/Video/";
		        File sourceFile = new File(source);
		        File targetDir = new File(target);
		        if(!targetDir.exists())
		        	targetDir.mkdirs();
		        
		        File targetFile = new File(targetDir+"/"+sourceFile.getName());
		        if(sourceFile.exists()){
		        	FileUtils.copyFile(sourceFile, targetFile);
		        	String	filepath = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/Video/"+sourceFile.getName();
					videoPath=filepath;		
					System.out.println("Video Path "+videoPath);
			}	
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return videoPath;
	}
	public String getVideoPlayDivForProfile(Integer videoLinkId,Integer teacherID)
	{
		WebContext context;
		String videoPath="";
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{			
			System.out.println("VideoID "+videoLinkId);
			TeacherVideoLink teacherVideoLink=null;		
			teacherVideoLink=teacherVideoLinksDAO.findById(videoLinkId, false, false);			
			
			if(teacherVideoLink.getVideo()!=null)
			{								
				String fileName= teacherVideoLink.getVideo().toString();
		        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherID+"/Video/"+fileName;
		        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherID+"/Video/";
		        File sourceFile = new File(source);
		        File targetDir = new File(target);
		        if(!targetDir.exists())
		        	targetDir.mkdirs();
		        
		        File targetFile = new File(targetDir+"/"+sourceFile.getName());
		        if(sourceFile.exists()){
		        	FileUtils.copyFile(sourceFile, targetFile);
		        	String	filepath = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherID+"/Video/"+sourceFile.getName();
					videoPath=filepath;		
					System.out.println("Video Path "+videoPath);
			}	
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return videoPath;
	}
	
	public String getDSPQForPortfolioReminder(Integer districtId)
	{

		System.out.println("*****:::::::::::::::||getDistrictSpecificPortfolioQuestions||:::::::::::::::::::;; ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		StringBuffer sb =new StringBuffer();
		try{
			TeacherDetail teacherDetail = null;

			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
			{
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");			
			}
			DistrictMaster districtMaster	=	null;
			int jobCategoryId=0;
			if(districtId!=0)
			{
				districtMaster			=	districtMasterDAO.findByDistrictId(districtId.toString());
				
				
				//Map<Integer,List<DistrictSpecificPortfolioQuestions>> dSPQ=districtSpecificPortfolioQuestionsDAO.getDSPQForPortfolioReminder(districtMaster);
				//List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsList=new ArrayList<DistrictSpecificPortfolioQuestions>();
				
				Map<Integer,List<PortfolioRemindersDspq>> dSPQ=portfolioRemindersDspqDAO.getDSPQByDistrict(districtMaster);
				List<PortfolioRemindersDspq> districtSpecificPortfolioQuestionsList=new ArrayList<PortfolioRemindersDspq>();
				
				
				
				int dspqType=0;
				try{
					for(int i=0;i<3;i++){
						if(dSPQ.get(i)!=null){
							districtSpecificPortfolioQuestionsList=dSPQ.get(i);
							dspqType=i;
							break;
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				System.out.println("districtSpecificPortfolioQuestionsList:::::::::::"+districtSpecificPortfolioQuestionsList.size());
				int totalQuestions = districtSpecificPortfolioQuestionsList.size();
				int cnt = 0;
				if(districtSpecificPortfolioQuestionsList.size()>0){
					System.out.println("dspqType:::::::::::"+dspqType);
					List<DistrictSpecificPortfolioAnswers>	lastList = districtSpecificPortfolioAnswersDAO.findDSPQAnswersForPortfolioReminder(teacherDetail,districtMaster,dspqType);
					System.out.println("lastList::::::::"+lastList.size());
					Map<Integer, DistrictSpecificPortfolioAnswers> map = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
	
					for(DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : lastList)
						map.put(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId(), districtSpecificPortfolioAnswers);
	
					DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers = null;
					List<DistrictSpecificPortfolioOptions> questionOptionsList = null;
					String questionInstruction = null;
					String shortName = "";
				
					String selectedOptions = "",insertedRanks="";
						for (PortfolioRemindersDspq districtSpecificPortfolioQuestions : districtSpecificPortfolioQuestionsList) 
						{
							shortName = districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionTypeMaster().getQuestionTypeShortName();
							sb.append("<input type='hidden' name='dspqType' id='dspqType' value='"+dspqType+"'  />");
							sb.append("<input type='hidden' name='jobCategoryIdForDSPQ'   id='jobCategoryIdForDSPQ' value='"+jobCategoryId+"'  />");
							sb.append("<tr>");
							sb.append("<td width='90%'>");
							sb.append("<div><b>Question "+(++cnt)+" of "+totalQuestions+"</b>");
							if(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getIsRequired()==1){
								sb.append("<span class=\"required\" >*</span>");
							}
							sb.append("</div>");
							sb.append("<input type='hidden' name='o_maxMarksS' value='"+(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getMaxMarks()==null?0:districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getMaxMarks())+"'  />");
							//questionInstruction = districtSpecificPortfolioQuestions.getQuestionInstructions()==null?"":districtSpecificPortfolioQuestions.getQuestionInstructions();
							//if(!questionInstruction.equals(""))
								//sb.append(""+Utility.getUTFToHTML(questionInstruction)+"");
	
							sb.append("<div id='QS"+cnt+"question'>"+Utility.getUTFToHTML(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestion())+"</div>");
	
							questionOptionsList = districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionOptions();
							districtSpecificPortfolioAnswers = map.get(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionId());
	
							String checked = "";
							Integer optId = 0;
							String insertedText = "";
							if(districtSpecificPortfolioAnswers!=null)
							{
								if(!shortName.equalsIgnoreCase("OSONP") && !shortName.equalsIgnoreCase("mloet") && !shortName.equalsIgnoreCase("DD") && !shortName.equalsIgnoreCase("mlsel") && !shortName.equalsIgnoreCase("rt") && !shortName.equalsIgnoreCase("sscb") && shortName.equalsIgnoreCase(districtSpecificPortfolioAnswers.getQuestionType()))
								{
									if(districtSpecificPortfolioAnswers.getSelectedOptions()!="" && districtSpecificPortfolioAnswers.getSelectedOptions()!=null)
										optId = Integer.parseInt(districtSpecificPortfolioAnswers.getSelectedOptions());
	
									insertedText = districtSpecificPortfolioAnswers.getInsertedText();
								}
								if(shortName.equalsIgnoreCase("mloet") || shortName.equalsIgnoreCase("OSONP") || shortName.equalsIgnoreCase("sscb"))
								{
									insertedText = districtSpecificPortfolioAnswers.getInsertedText();
								}
							}
							if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel")|| shortName.equalsIgnoreCase("lkts"))
							{
								sb.append("<table>");
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									if(questionOptions!=null && questionOptions.getStatus().equalsIgnoreCase("A")){
									if(optId.equals(questionOptions.getOptionId()))
										checked = "checked";
									else
										checked = "";
	
									sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
											" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
											"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
									sb.append("</td></tr>");
									}
								}
								sb.append("</table>");
							}
							if(shortName.equalsIgnoreCase("rt")){	
									if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getInsertedRanks()!=null){
										insertedRanks = districtSpecificPortfolioAnswers.getInsertedRanks()==null?"":districtSpecificPortfolioAnswers.getInsertedRanks();
									}
									String[] ranks = insertedRanks.split("\\|");
									int rank=1;
									int count=0;
									String ans = "";
									sb.append("<table>");
									Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
									for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
										optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
									}
									for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
										if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
										try{ans =ranks[count];}catch(Exception e){}
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnkS"+rank+"' name='rankS' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRankForPortfolio(this);' value='"+ans+"'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
										sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"' /><input type='hidden' name='optS' value='"+teacherQuestionOption.getOptionId()+"' />");
										sb.append("<input type='hidden' name='o_rankS' value='"+(teacherQuestionOption.getRank()==null?0:teacherQuestionOption.getRank())+"' />");
										sb.append("<script type=\"text/javascript\" language=\"javascript\">");
										//sb.append("document.getElementById('rnkS1').focus();");
										sb.append("</script></td></tr>");
										rank++;
										count++;
										}
									}
									sb.append("</table >");
							}
							if(shortName.equalsIgnoreCase("mlsel")){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								String ansId="";
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											//System.out.println("ansId:::"+ansId);
											if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
												//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
												if(ansId!=null && !ansId.equals("")){
													ansId="checked";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									
									sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
									sb.append("</td></tr>");
									multiCount++;
									}
								}
								sb.append("</table >");
							}
													
							if(shortName.equalsIgnoreCase("mloet")){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								String ansId="";
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
												if(ansId!=null && !ansId.equals("")){
													ansId="checked";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									
									sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
									sb.append("</td></tr>");
									multiCount++;
								}
								}
								sb.append("</table >");
								String questionInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions()!=null){
										questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								sb.append(questionInstructions+"</br><span style=''><textarea  name='QS"+cnt+"optmloet' id='QS"+cnt+"optmloet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('QS"+cnt+"optmloet').focus();");
								sb.append("</script>");
							}
							if(shortName.equalsIgnoreCase("it"))
							{
								sb.append("<table >");
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
									sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='optS' value='"+teacherQuestionOption.getOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherQuestionOption.getQuestionOption()+"'> ");
									sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
									}
								}
								sb.append("</table>");
							}
							if(shortName.equalsIgnoreCase("et"))
							{
								sb.append("<table>");
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									if(questionOptions!=null && questionOptions.getStatus().equalsIgnoreCase("A")){
									if(optId.equals(questionOptions.getOptionId()))
										checked = "checked";
									else
										checked = "";
	
									sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
											" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
											"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
									sb.append("</td></tr>");
								}
								}
								sb.append("</table>");
	
								String questionInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions()!=null){
										questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions()+"</br>";	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								sb.append(questionInstructions+"&nbsp;&nbsp;<span style=''><textarea  name='QS"+cnt+"optet' id='QS"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('QS"+cnt+"optet').focus();");
								sb.append("</script>");
							}
							if(shortName.equalsIgnoreCase("sl"))
							{
								//Years of certified teaching experience
								if(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestion().contains("Years of certified teaching experience")){									
									String uniqueChkId ="QS"+cnt+"opt";
									String checkedCheckBox="";
									String fieldDisable="";
									if(districtSpecificPortfolioAnswers==null){
										checkedCheckBox="";
										fieldDisable="";
									}else{
										if(insertedText.equalsIgnoreCase("0.0")){
											checkedCheckBox="checked";
											fieldDisable="disabled";
										}
									}
									
									String questionCustomInstructions="";
									try{
										if(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions()!=null){
											questionCustomInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions();	
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									sb.append("&nbsp;&nbsp;<div class='row'><div class='col-sm-4 col-md-4'><input type='text' name='QS"+cnt+"opt' "+fieldDisable+" onkeypress='return checkForDecimalTwo(event);' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' class='form-control'/></div>");
									sb.append("<div class='col-sm-4 col-md-4'><input type='checkbox' id='QS"+cnt+"exp' "+checkedCheckBox+" style='margin-top: 10px;'>"+questionCustomInstructions+"</div></div><br>");
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
										sb.append("$('#QS"+cnt+"exp').change(function(){ $(this).each(function(){if($(this).prop('checked')){$('#"+uniqueChkId+"').val('0.0');$('#"+uniqueChkId+"').prop('disabled',true);}else{$('#"+uniqueChkId+"').val('');$('#"+uniqueChkId+"').prop('disabled',false);}});});");
									sb.append("</script>");
								}else{
									sb.append("&nbsp;&nbsp;<input type='text' name='QS"+cnt+"opt' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' style='width:98%;margin:5px;'/><br/>");
								}								
							}else if(shortName.equalsIgnoreCase("ml"))
							{
								sb.append("&nbsp;&nbsp;<textarea name='QS"+cnt+"opt' id='QS"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");					
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('QS"+cnt+"opt').focus();");
								sb.append("</script>");
								if(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionId().equals(8)){
									String dbScName= "";
									String dbScId="";
									try{
										if(districtSpecificPortfolioAnswers.getSchoolMaster()!=null && !districtSpecificPortfolioAnswers.getSchoolMaster().equals("")){
											dbScName=districtSpecificPortfolioAnswers.getSchoolMaster().getSchoolName();
											dbScId=Long.toString(districtSpecificPortfolioAnswers.getSchoolMaster().getSchoolId());
										}
									}catch (Exception e) {
										// TODO: handle exception
									}
									sb.append("<label>"+lblSchoolName+"</label><br><input type='text' value='"+dbScName+"' id='schoolName' maxlength='100' name='schoolName' class='form-control' placeholder='' onfocus=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onkeyup=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onblur=\"hideSchoolMasterDivQuestion(this,'QS"+cnt+"schoolId','divTxtShowData2');\"   style='width: 50%;'/>	");
									sb.append("<input type='hidden' id='schoolName' name='schoolName'/><input type='hidden' id='QS"+cnt+"schoolId' class='school"+cnt+"' name='schoolId' value='"+dbScId+"'/>");
									sb.append("<div id='divTxtShowData2'  onmouseover=\"mouseOverChk('divTxtShowData2','schoolName')\" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>");
								}
							}
							if(shortName.equalsIgnoreCase("OSONP"))
							{
								String optionClass="OSONP"+cnt;
								sb.append("<table>");
								Integer p1=0;
								
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								String ansId="";
								boolean openOther=false;
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									if(questionOptions!=null && questionOptions.getStatus().equalsIgnoreCase("A")){
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											if(ansId!=null && !ansId.equals("") && questionOptions.getOptionId()==Integer.parseInt(ansId)){
												if(ansId!=null && !ansId.equals("")){
													ansId="checked";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
																				
											if(questionOptions.getOpenText() && ansId.equalsIgnoreCase("checked")){
												sb.append("<script>onSelectOpenOptions('show','"+cnt+"');</script>");
											}
											
											String checkBox="";
											
											if(questionOptions.getOpenText()){
												checkBox="<input type='radio' name='"+optionClass+"' class='"+optionClass+"' value='"+questionOptions.getOptionId()+"' "+ansId+" onclick=\"onSelectOpenOptions('show','"+cnt+"');\" />";	
											}else{
												checkBox="<input type='radio' name='"+optionClass+"' class='"+optionClass+"' value='"+questionOptions.getOptionId()+"' "+ansId+" onclick=\"onSelectOpenOptions('hide','"+cnt+"');\"  />";
											}
											
											sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'>"+checkBox+"</div>" +
													" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
													"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
											sb.append("</td></tr>");
								}
							}
								sb.append("</table>");
								String questionCustomInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions()!=null){
										questionCustomInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
									sb.append("<span class='hide textareaOp"+cnt+"' style='margin-left: 10px;'>"+questionCustomInstructions+"<textarea  name='QS"+cnt+"OSONP' id='QS"+cnt+"OSONP' class='form-control "+cnt+"OSONPtext' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea></span>");
								
							}
							
							if(shortName.equalsIgnoreCase("DD")){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								String questionInstructions="&nbsp;";
								try{
									if(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions()!=null){
										questionInstructions=districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								sb.append(questionInstructions.trim());
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
								String ansId="";
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><select name='dropdown"+cnt+"' id='dropdown"+cnt+"' class='form-control'><option value=''>Select</option>");
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											//System.out.println("ansId:::"+ansId);
											if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
												//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
												if(ansId!=null && !ansId.equals("")){
													ansId="selected";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									sb.append("<option "+ansId+" value='"+teacherQuestionOption.getOptionId()+"'>"+teacherQuestionOption.getQuestionOption()+"</option>");
									//sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
									
									multiCount++;
									}
								}
								sb.append("</select></td></tr>");
								//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
								sb.append("</table><br>");
																
								
								
							}
							if(shortName.equalsIgnoreCase("sswc"))
							{
								sb.append("<table style='margin-bottom:10px'>");
								for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
									if(questionOptions!=null && questionOptions.getStatus().equalsIgnoreCase("A")){
									if(optId.equals(questionOptions.getOptionId()))
										checked = "checked";
									else
										checked = "";
	
									sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
											" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
											"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
									sb.append("</td></tr>");
								}
							}
								sb.append("</table>");
	
								String questionInstructions="";
								try{
									if(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions()!=null){
										questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								sb.append(questionInstructions+"<br>");
								
								if(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestion().equalsIgnoreCase("Have you previously worked for UCSN?")){
									
									System.out.println("insertedText.trim()   "+insertedText.trim());
									String[] arr = new String[2]; 
									if(insertedText.equalsIgnoreCase("##")){
										arr[0]="";
										arr[1]="";
									}else if(insertedText.contains("##")){
										arr = insertedText.split("##");
									}else{
										arr[0]=insertedText;
										arr[1]="";
									}
									
									sb.append("<div class='row'><div class='col-sm-3 col-md-3'><input type='text' name='QS"+cnt+"optet' id='QS"+cnt+"optet1' class='form-control' value='"+arr[0].trim()+"'></div>");
									sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet1', 'QS"+cnt+"optet1', '%m-%d-%Y');</script>");									
									sb.append("<div class='col-sm-3 col-md-3'><input type='text'  name='QS"+cnt+"optet' id='QS"+cnt+"optet2' class='form-control' value='"+arr[1].trim()+"'></div>");
									sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet2', 'QS"+cnt+"optet2', '%m-%d-%Y');</script>");									
								}else{
									sb.append("<div class='row'><div class='col-sm-3 col-md-3'><input type='text' name='QS"+cnt+"optet' id='QS"+cnt+"optet' class='form-control' value='"+insertedText+"'></div>");
									sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet', 'QS"+cnt+"optet', '%m-%d-%Y');</script>");								
									
								}
																
								sb.append("</div>");
								sb.append("</br>");
							}
							
							if(shortName.equalsIgnoreCase("sscb")){	
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								String ansId="";
								int subsecCount=0; 
								
								String classSubSec="";
								for (DistrictSpecificPortfolioOptions teacherQuestionOptionHD : questionOptionsList) {
									if(teacherQuestionOptionHD.getSubSectionTitle()!=null && teacherQuestionOptionHD.getSubSectionTitle().equalsIgnoreCase("Y")){
										subsecCount++;
										classSubSec="sectionCnt"+subsecCount+"";
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;' class='secHeading' id='subSection"+cnt+"'>"+teacherQuestionOptionHD.getQuestionOption()+"");
										sb.append("</td></tr>");
									//}
									
									for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
											if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
											try{
												ansId="";
												for(int i=0;i<multiCounts.length;i++){
													try{ansId =multiCounts[i];}catch(Exception e){}
													//System.out.println("ansId:::"+ansId);
													if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
														//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
														if(ansId!=null && !ansId.equals("")){
															ansId="checked";
														}
														break;
													}
												}
											}catch(Exception e){
												e.printStackTrace();
											}
										
											if(teacherQuestionOption.getSubSectionTitle()!=null && teacherQuestionOption.getSubSectionTitle().equalsIgnoreCase(subsecCount+"")){
												/*subsecCount++;
												classSubSec="sectionCnt"+subsecCount+"";
												sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;' class='secHeading' id='subSection"+cnt+"'>"+teacherQuestionOption.getQuestionOption()+"");
												sb.append("</td></tr>");
											}else{*/
												sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='"+classSubSec+"' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'>&nbsp;&nbsp;&nbsp;"+teacherQuestionOption.getQuestionOption()+"<input type='hidden' id='QS"+teacherQuestionOption.getOptionId()+"validQuestion' value='"+teacherQuestionOption.getValidOption()+"'>");
												if(districtSpecificPortfolioQuestions.getDistrictMaster().getDistrictId().equals(7800040) && teacherQuestionOption.getQuestionOption().equalsIgnoreCase("College or University if selected, please choose the name of the school from the drop down"))
												{
													String dbUName= "";
													String dbUId="";
													try{
														if(districtSpecificPortfolioAnswers.getUniversityId()!=null && !districtSpecificPortfolioAnswers.getUniversityId().equals("")){
															dbUName=districtSpecificPortfolioAnswers.getUniversityId().getUniversityName();
															dbUId=districtSpecificPortfolioAnswers.getUniversityId().getUniversityId()+"";
														}
													}catch (Exception e) {
														// TODO: handle exception
													}											
													sb.append("<br><input type='text' value='"+dbUName+"' id='schoolName' maxlength='100' name='schoolName' class='form-control' placeholder='' onfocus=\"getUniversityAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onkeyup=\"getUniversityAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onblur=\"hideSchoolMasterDivQuestion(this,'QS"+cnt+"schoolId','divTxtShowData2');\" style='width:58%;display:inline;margin-left:5px;'/>");
													//sb.append("<label>School Name</label><br><input type='text' value='"+dbScName+"' id='schoolName' maxlength='100' name='schoolName' class='form-control' placeholder='' onfocus=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onkeyup=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onblur=\"hideSchoolMasterDivQuestion(this,'QS"+cnt+"schoolId','divTxtShowData2');\" style='width:58%;display:inline;'/>	");
													sb.append("<input type='hidden' id='schoolName' name='schoolName'/><input type='hidden' id='QS"+cnt+"schoolId' class='school"+cnt+"' name='schoolId' value='"+dbUId+"'/>");
													sb.append("<div id='divTxtShowData2'  onmouseover=\"mouseOverChk('divTxtShowData2','schoolName')\" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>");
													
												}
												if(districtSpecificPortfolioQuestions.getDistrictMaster().getDistrictId().equals(7800040) && teacherQuestionOption.getQuestionOption().contains("UNO Charter School Network Employee"))
												{
													sb.append("<input type='text'  name='QS"+cnt+"multiselectText' id='QS"+cnt+"multiselectText' class='form-control' style='width:35%;margin-left:5px;' value='"+insertedText+"'>");
												}
												//UNO Charter School Network Employee 
												sb.append("</td></tr>");
												multiCount++;										
											}		
										}
										
									}
								}
							}//main loop
								sb.append("</table >");
								
							}
							if(shortName.equalsIgnoreCase("UAT"))
							{
								//sb.append("&nbsp;&nbsp;<input type='file' name='QS"+cnt+"opt' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' style='width:98%;margin:5px;'/><br/>");
								
								if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getSelectedOptions()!=null){
									selectedOptions = districtSpecificPortfolioAnswers.getSelectedOptions()==null?"":districtSpecificPortfolioAnswers.getSelectedOptions();
								}
								System.out.println("selectedOptions::::>:"+selectedOptions);
								String[] multiCounts = selectedOptions.split("\\|");
								int multiCount=1;
								
								String questionInstructions="&nbsp;";
								try{
									if(districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions()!=null){
										questionInstructions=districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionInstructions();	
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								//sb.append(questionInstructions.trim());
								
								if(questionOptionsList!=null && questionOptionsList.size()>0){
										sb.append("<table>");
										Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
										for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
											optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
										}
										//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
										String ansId="";
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><select name='dropdown"+cnt+"' id='dropdown"+cnt+"' class='form-control'><option value=''>Select</option>");
										for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
											if(teacherQuestionOption!=null && teacherQuestionOption.getStatus().equalsIgnoreCase("A")){
											try{
												ansId="";
												for(int i=0;i<multiCounts.length;i++){
													try{ansId =multiCounts[i];}catch(Exception e){}
													//System.out.println("ansId:::"+ansId);
													if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
														//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
														if(ansId!=null && !ansId.equals("")){
															ansId="selected";
														}
														break;
													}
												}
											}catch(Exception e){
												e.printStackTrace();
											}
											sb.append("<option "+ansId+" value='"+teacherQuestionOption.getOptionId()+"'>"+teacherQuestionOption.getQuestionOption()+"</option>");
											//sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
											
											multiCount++;
											}
										}
										sb.append("</select></td></tr>");
										//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
										sb.append("</table>");
								}	
								sb.append("<div id='answerDiv'>");								
									sb.append("<iframe id='uploadFrameAnswerId' name='uploadFrameAnswer' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
									sb.append("<form id='frmAnswerUpload' enctype='multipart/form-data' method='post' target='uploadFrameAnswer' action='answerUploadServlet.do' class='form-inline'>");
										sb.append("<div class='row'>");
											sb.append("<div class='col-sm-12 col-md-12'>");
												sb.append("<div class='divErrorMsg' id='errAnswer' style='display: block;'></div>");
											sb.append("</div>");
											
											sb.append("<div class='col-sm-12 col-md-12'>");
												if(questionInstructions!="" && !questionInstructions.equals("&nbsp;"))
													sb.append("<span>"+questionInstructions.trim()+"</span><br/>");
												sb.append("<div class='col-sm-6 col-md-6 top5'>");
													sb.append("<input name='QS"+cnt+"File' id='QS"+cnt+"File' type='file'/>");
												sb.append("</div>");
												sb.append("<div class='col-sm-6 col-md-6 top5' id='divAnswerTxt'>");
													String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
													String fileName="";
													if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getFileName()!=null && !districtSpecificPortfolioAnswers.getFileName().equalsIgnoreCase(""))
													{
														fileName=districtSpecificPortfolioAnswers.getFileName();
														sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+lnkViewAnswer+"' id='hrefAnswer' onclick=\"downloadAnswer(0,'"+districtSpecificPortfolioAnswers.getAnswerId()+"');"+windowFunc+"\">"+lnkV+"</a>");
														sb.append("<script>$('#hrefAnswer').tooltip();</script>");
													}
												sb.append("</div>");

											sb.append("</div>");
											sb.append("<input type='hidden' name='answerFileName' id='answerFileName' value='"+fileName+"'>");
											sb.append("<input type='hidden' name='editCaseFile' id='editCaseFile' value='"+fileName+"'>");
											sb.append("<input type='hidden' name='answerFilePath' id='answerFilePath'>");
											sb.append("<input type='hidden' name='hdnQues' id='hdnQues' value='QS"+cnt+"File'>");										
										sb.append("</div>");
									sb.append("</form>");
								sb.append("</div>");
								if(districtSpecificPortfolioQuestions.getDistrictMaster().getDistrictId()==1302010 && districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestion().equalsIgnoreCase("List sports clubs/activities you would be willing to coach/sponsor (attach coaching experience resume to profile)."))
								{
									sb.append("</br><span style=''><textarea  name='QS"+cnt+"Text' id='QS"+cnt+"Text' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
								}
							}

							sb.append("<input type='hidden' id='QS"+cnt+"isRequired' value='"+districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getIsRequired()+"'/>" );
							sb.append("<input type='hidden' id='QS"+cnt+"questionId' value='"+districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionId()+"'/>" );
							sb.append("<input type='hidden' id='QS"+cnt+"questionTypeId' value='"+districtSpecificPortfolioQuestions.getDistrictSpecificPortfolioQuestions().getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
							sb.append("<input type='hidden' id='QS"+cnt+"questionTypeShortName' value='"+shortName+"'/>");	
							sb.append("</td>");
							sb.append("</tr>");
							insertedText = "";
						}
						if(districtMaster.getDistrictId()==7800047){
							sb.append("<b>"+msgReasonableAccommodationNotice1+"</b>" +
									"<p>"+msgReasonableAccommodationNotice2+"</p>" +
									"<p>"+msgReasonableAccommodationNotice3+"</p>" +
									"<p>"+msgReasonableAccommodationNotice4+"<p>");}
				    }
				sb.append("@##@"+cnt+"@##@"+districtMaster.getTextForDistrictSpecificQuestions());

				
			}
			//////////////////////	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	
		
	}

	public String setDistrictPortfolioAnswr(DistrictSpecificPortfolioAnswers[] districtSpecificPortfolioAnswersList,int dspqType,Integer districtId)
	{
		System.out.println(":::::::::::::::    setDistrictQPortfoliouestions    :::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		TeacherDetail teacherDetail = null;

		if (session == null || session.getAttribute("teacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		
		DistrictMaster districtMaster	=	null;
		if(districtId!=0)
		{
			districtMaster			=	districtMasterDAO.findByDistrictId(districtId.toString());
		}
		System.out.println(":: districtSpecificPortfolioAnswersList ::"+districtSpecificPortfolioAnswersList.length);
		try {
				if(districtSpecificPortfolioAnswersList.length>0){
					int inValidCount = 0;
					JobOrder jobOrder = districtSpecificPortfolioAnswersList[0].getJobOrder();
					jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
		
					Map<Integer,Integer> map = new HashMap<Integer, Integer>();
					Map<Integer,DistrictSpecificPortfolioAnswers> mapDistrictSpecificPortfolioAnswers = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();			
					Map<String, String> questionOptions = new HashMap<String, String>();
					Map<String, TeacherSecondaryStatus> teacherTags = new HashMap<String, TeacherSecondaryStatus>();
					Map<Integer, DistrictSpecificPortfolioQuestions> questTags = new HashMap<Integer, DistrictSpecificPortfolioQuestions>();
					Map<Long, SchoolMaster> schoolMap = new HashMap<Long, SchoolMaster>();
					Map<Integer, UniversityMaster> universityMap = new HashMap<Integer, UniversityMaster>();
					UserMaster userMaster=null;
					try {
						Criterion criterionDis = Restrictions.eq("districtMaster", districtMaster);
						
						List<SchoolMaster> schList = schoolMasterDAO.findDistrictSchoolId(districtMaster);
						if(schList!=null && schList.size()>0){
							for (SchoolMaster schoolMaster : schList) {
								schoolMap.put(schoolMaster.getSchoolId(), schoolMaster);
							}
						}
						userMaster = userMasterDAO.findById(1, false,false);
						
						
						List<UniversityMaster> universityMasters = universityMasterDAO.findAll();						
						if(universityMasters!=null && universityMasters.size()>0){
							for (UniversityMaster universityMaster : universityMasters) {
								universityMap.put(universityMaster.getUniversityId(), universityMaster);
							}
							
						}
						
						List<DistrictSpecificPortfolioAnswers> dspqList= districtSpecificPortfolioAnswersDAO.findDSPQAnswersForPortfolioReminder(teacherDetail,districtMaster,dspqType);
						for (DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : dspqList) {
							map.put(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId(), districtSpecificPortfolioAnswers.getAnswerId());
							mapDistrictSpecificPortfolioAnswers.put(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId(), districtSpecificPortfolioAnswers);
						}
						
						List<SecondaryStatusMaster> tagIds= new ArrayList<SecondaryStatusMaster>();
						for (DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : dspqList) {
							map.put(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId(), districtSpecificPortfolioAnswers.getAnswerId());					
							
						}
							
						
						List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsLst = districtSpecificPortfolioQuestionsDAO.findByCriteria(criterionDis);
						
						for (DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions : districtSpecificPortfolioQuestionsLst) {
							questTags.put(districtSpecificPortfolioQuestions.getQuestionId(), districtSpecificPortfolioQuestions);					
							if(districtSpecificPortfolioQuestions.getProvideTag()){					
								tagIds.add(districtSpecificPortfolioQuestions.getSecondaryStatusMaster());
							}
						}
						
						List<DistrictSpecificPortfolioOptions> listPortfolioOptions=new ArrayList<DistrictSpecificPortfolioOptions>();
						if(districtSpecificPortfolioQuestionsLst!=null && districtSpecificPortfolioQuestionsLst.size() >0)
						{
							Criterion criterion1 = Restrictions.in("districtSpecificPortfolioQuestions", districtSpecificPortfolioQuestionsLst);
							listPortfolioOptions = districtSpecificPortfolioOptionsDAO.findByCriteria(criterion1);
						}
						
						for (DistrictSpecificPortfolioOptions districtSpecificPortfolioOptions : listPortfolioOptions) {
							String questionOption = districtSpecificPortfolioOptions.getDistrictSpecificPortfolioQuestions().getQuestionId()+"##"+districtSpecificPortfolioOptions.getOptionId();
							questionOptions.put(questionOption, districtSpecificPortfolioOptions.getQuestionOption());
						}			
						
						Criterion criterion2 = Restrictions.in("secondaryStatusMaster", tagIds);
						Criterion criterion3 = Restrictions.eq("teacherDetail",teacherDetail);
						List<TeacherSecondaryStatus> tSecStatusList = new ArrayList<TeacherSecondaryStatus>();
						
						if(tagIds.size()>0){
							tSecStatusList = teacherSecondaryStatusDAO.findByCriteria(criterion3,criterion2);
						}
						if(tSecStatusList.size()>0)
						for (TeacherSecondaryStatus teacherSecondaryStatus : tSecStatusList) {
							teacherTags.put(teacherSecondaryStatus.getTeacherDetail().getTeacherId()+"##"+teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusId(), teacherSecondaryStatus);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
		
					List<Integer> answers = new ArrayList<Integer>();
					Integer ansId = null;
					
					/*
					 * 	Check DSPQ Question against district and Question text matching
					 *  
					 * */
						Map<String,List<DistrictSpecificPortfolioQuestions>> dsqpMap = new HashMap<String, List<DistrictSpecificPortfolioQuestions>>();
						List<DistrictSpecificPortfolioQuestions> dspqList = null;
						
						try{
							dspqList = districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestionBYDistrict(districtMaster);
							
							if(dspqList!=null && dspqList.size()>0){
								for(DistrictSpecificPortfolioQuestions data : dspqList){
									String question = data.getQuestion();
									List<DistrictSpecificPortfolioQuestions> treeStructureList=dsqpMap.get(question);
									if(dsqpMap.get(question)==null){
										List<DistrictSpecificPortfolioQuestions> treeStrNewList =new ArrayList<DistrictSpecificPortfolioQuestions>();
										treeStrNewList.add(data);
										dsqpMap.put(question, treeStrNewList);
									} else {
										treeStructureList.add(data);
										dsqpMap.put(question, treeStructureList);
									}
								}
							}
						} catch(Exception exception){
							exception.printStackTrace();
						}
						
						// Create Map For "DistrictSpecificPortfolioAnswers" against district and teacher
						Map<Integer,DistrictSpecificPortfolioAnswers> dsqpAnsMap = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
						List<DistrictSpecificPortfolioAnswers> dspqAnsList =    districtSpecificPortfolioAnswersDAO.getByDistrictAndTeacher(districtMaster,teacherDetail);
						if(dspqAnsList!=null && dspqAnsList.size()>0){
							for(DistrictSpecificPortfolioAnswers record1 : dspqAnsList){
								if(record1.getJobCategoryMaster()!=null && record1.getJobCategoryMaster().getJobCategoryId()!=null)
									dsqpAnsMap.put(record1.getJobCategoryMaster().getJobCategoryId(), record1);
							}
						}
						
					/*
					 * 	End Section
					 * */
					SessionFactory sessionFactoryDspq		=	districtSpecificPortfolioQuestionsDAO.getSessionFactory();
					StatelessSession statelesSsessionDspq 	= 	sessionFactoryDspq.openStatelessSession();
					Transaction txOpenDspq =statelesSsessionDspq.beginTransaction();
					
					for (DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : districtSpecificPortfolioAnswersList) {
						List<DistrictSpecificPortfolioQuestions> dspqQuestionList = dsqpMap.get(districtSpecificPortfolioAnswers.getQuestion().trim());
						System.out.println("::::::::::::::: dspqQuestionList ::::::::::::::"+dspqQuestionList.size());
						
						if(dspqQuestionList.size()>1){
							for(DistrictSpecificPortfolioQuestions record : dspqQuestionList){
								
								boolean chkDup=false;
								DistrictSpecificPortfolioAnswers dspqAns = dsqpAnsMap.get(record.getJobCategoryMaster().getJobCategoryId());
								//MessageToTeacher mSt = msgMap.get(teacherDetail.getTeacherId());
								
								if(dspqAns!=null)
									chkDup = true;
								
								districtSpecificPortfolioAnswers.setTeacherDetail(teacherDetail);
								districtSpecificPortfolioAnswers.setDistrictMaster(districtMaster);
								districtSpecificPortfolioAnswers.setCreatedDateTime(new Date());
							//universityMap
								
								if(districtSpecificPortfolioAnswers.getSchoolIdTemp()!=null && record.getQuestionTypeMaster().getQuestionTypeShortName().equalsIgnoreCase("sscb")){						
									Integer uId =  (int) (long) districtSpecificPortfolioAnswers.getSchoolIdTemp();							
									if(districtSpecificPortfolioAnswers.getSchoolIdTemp()!=null && universityMap.containsKey(uId)){								
										districtSpecificPortfolioAnswers.setUniversityId(universityMap.get(uId));
									}
								}
								else if(districtSpecificPortfolioAnswers.getSchoolIdTemp()!=null && schoolMap.containsKey(districtSpecificPortfolioAnswers.getSchoolIdTemp())){
									districtSpecificPortfolioAnswers.setSchoolMaster(schoolMap.get(districtSpecificPortfolioAnswers.getSchoolIdTemp()));
								}
								
								ansId = map.get(record.getQuestionId());
								if(ansId!=null)
								{
									districtSpecificPortfolioAnswers.setAnswerId(ansId);
									
									if(mapDistrictSpecificPortfolioAnswers.containsKey(record.getQuestionId())){
										DistrictSpecificPortfolioAnswers tempDistrictSpecificPortfolioAnswers =mapDistrictSpecificPortfolioAnswers.get(record.getQuestionId());
										districtSpecificPortfolioAnswers.setSliderScore(tempDistrictSpecificPortfolioAnswers.getSliderScore());
										districtSpecificPortfolioAnswers.setScoreBy(tempDistrictSpecificPortfolioAnswers.getScoreBy());
										districtSpecificPortfolioAnswers.setScoreDateTime(tempDistrictSpecificPortfolioAnswers.getScoreDateTime());
									}
								}
								if(!districtSpecificPortfolioAnswers.getQuestionType().equalsIgnoreCase("ml"))
								{
									if(districtSpecificPortfolioAnswers.getIsValidAnswer()!=null && districtSpecificPortfolioAnswers.getIsValidAnswer()==false)
										inValidCount++;
								}
								System.out.println("dspqType::::::::::::::"+dspqType);
								if(dspqType==1){
									districtSpecificPortfolioAnswers.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
									districtSpecificPortfolioAnswers.setJobOrder(null);
								}else if(dspqType==0){
									districtSpecificPortfolioAnswers.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
									districtSpecificPortfolioAnswers.setJobOrder(jobOrder);
								}else if(dspqType==2){
									districtSpecificPortfolioAnswers.setJobOrder(null);
									districtSpecificPortfolioAnswers.setJobCategoryMaster(null);
								}else if(dspqType==3)
								{
									districtSpecificPortfolioAnswers.setJobOrder(null);
									districtSpecificPortfolioAnswers.setJobCategoryMaster(null);
									districtSpecificPortfolioAnswers.setHeadQuarterId(jobOrder.getHeadQuarterMaster().getHeadQuarterId());
									districtSpecificPortfolioAnswers.setBranchId(null);
								}else if(dspqType==4)
								{
									districtSpecificPortfolioAnswers.setJobOrder(null);
									districtSpecificPortfolioAnswers.setJobCategoryMaster(null);
									
									districtSpecificPortfolioAnswers.setHeadQuarterId(jobOrder.getHeadQuarterMaster().getHeadQuarterId());
									districtSpecificPortfolioAnswers.setBranchId(jobOrder.getBranchMaster().getBranchId());
								}
								
								if(record.getJobCategoryMaster()!=null && record.getJobCategoryMaster().getJobCategoryId()!=null)
									districtSpecificPortfolioAnswers.setJobCategoryMaster(record.getJobCategoryMaster());
									
								districtSpecificPortfolioAnswers.setDistrictSpecificPortfolioQuestions(record); //.   setQuestion(question)  setJobCategoryMaster(record.getJobCategoryMaster());
								districtSpecificPortfolioAnswers.setQuestionTypeMaster(record.getQuestionTypeMaster());// (questionType)  setJobCategoryMaster(record.getJobCategoryMaster());
								//districtSpecificPortfolioAnswers.setQuestionType(questionType);
								
								districtSpecificPortfolioAnswers.setIsActive(true);
								
								//districtSpecificPortfolioAnswersDAO.makePersistent(districtSpecificPortfolioAnswers);
								
								if(chkDup==true){
									statelesSsessionDspq.update(dspqAns);
								} else {
									statelesSsessionDspq.insert(districtSpecificPortfolioAnswers);
								}
								
								//statelesSsessionDspq.insert(districtSpecificPortfolioAnswers);
								
								answers.add(districtSpecificPortfolioAnswers.getAnswerId());
							}
							txOpenDspq.commit();
							statelesSsessionDspq.close();
						} else {
							districtSpecificPortfolioAnswers.setTeacherDetail(teacherDetail);
							districtSpecificPortfolioAnswers.setDistrictMaster(districtMaster);
							districtSpecificPortfolioAnswers.setCreatedDateTime(new Date());
						//universityMap
							
							if(districtSpecificPortfolioAnswers.getSchoolIdTemp()!=null && districtSpecificPortfolioAnswers.getQuestionType().equalsIgnoreCase("sscb")){						
								Integer uId =  (int) (long) districtSpecificPortfolioAnswers.getSchoolIdTemp();							
								if(districtSpecificPortfolioAnswers.getSchoolIdTemp()!=null && universityMap.containsKey(uId)){								
									districtSpecificPortfolioAnswers.setUniversityId(universityMap.get(uId));
								}
							}
							else if(districtSpecificPortfolioAnswers.getSchoolIdTemp()!=null && schoolMap.containsKey(districtSpecificPortfolioAnswers.getSchoolIdTemp())){
								districtSpecificPortfolioAnswers.setSchoolMaster(schoolMap.get(districtSpecificPortfolioAnswers.getSchoolIdTemp()));
							}
							
							ansId = map.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId());
							if(ansId!=null)
							{
								districtSpecificPortfolioAnswers.setAnswerId(ansId);
								
								if(mapDistrictSpecificPortfolioAnswers.containsKey(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId())){
									DistrictSpecificPortfolioAnswers tempDistrictSpecificPortfolioAnswers =mapDistrictSpecificPortfolioAnswers.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId());
									districtSpecificPortfolioAnswers.setSliderScore(tempDistrictSpecificPortfolioAnswers.getSliderScore());
									districtSpecificPortfolioAnswers.setScoreBy(tempDistrictSpecificPortfolioAnswers.getScoreBy());
									districtSpecificPortfolioAnswers.setScoreDateTime(tempDistrictSpecificPortfolioAnswers.getScoreDateTime());
								}
							}
							if(!districtSpecificPortfolioAnswers.getQuestionType().equalsIgnoreCase("ml"))
							{
								if(districtSpecificPortfolioAnswers.getIsValidAnswer()!=null && districtSpecificPortfolioAnswers.getIsValidAnswer()==false)
									inValidCount++;
							}
							System.out.println("dspqType::::::::::::::"+dspqType);
							if(dspqType==1){
								districtSpecificPortfolioAnswers.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
								districtSpecificPortfolioAnswers.setJobOrder(null);
							}else if(dspqType==0){
								districtSpecificPortfolioAnswers.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
								districtSpecificPortfolioAnswers.setJobOrder(jobOrder);
							}else if(dspqType==2){
								districtSpecificPortfolioAnswers.setJobOrder(null);
								districtSpecificPortfolioAnswers.setJobCategoryMaster(null);
							}else if(dspqType==3)
							{
								districtSpecificPortfolioAnswers.setJobOrder(null);
								districtSpecificPortfolioAnswers.setJobCategoryMaster(null);
								districtSpecificPortfolioAnswers.setHeadQuarterId(jobOrder.getHeadQuarterMaster().getHeadQuarterId());
								districtSpecificPortfolioAnswers.setBranchId(null);
							}else if(dspqType==4)
							{
								districtSpecificPortfolioAnswers.setJobOrder(null);
								districtSpecificPortfolioAnswers.setJobCategoryMaster(null);
								
								districtSpecificPortfolioAnswers.setHeadQuarterId(jobOrder.getHeadQuarterMaster().getHeadQuarterId());
								districtSpecificPortfolioAnswers.setBranchId(jobOrder.getBranchMaster().getBranchId());
								
							}
							
							districtSpecificPortfolioAnswers.setIsActive(true);
							districtSpecificPortfolioAnswersDAO.makePersistent(districtSpecificPortfolioAnswers);
							answers.add(districtSpecificPortfolioAnswers.getAnswerId());
							
							
							if(questTags.containsKey(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()) && questTags.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()).getProvideTag()){
								DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions = questTags.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId());
								String quesOpt = (districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()+"##"+districtSpecificPortfolioAnswers.getSelectedOptions()).replace("|", "");
								
								if(questionOptions.get(quesOpt).equalsIgnoreCase("Yes") && !teacherTags.containsKey(teacherDetail.getTeacherId()+"##"+districtSpecificPortfolioQuestions.getSecondaryStatusMaster().getSecStatusId())){
									TeacherSecondaryStatus teacherSecondaryStatus = new TeacherSecondaryStatus();					
									teacherSecondaryStatus.setTeacherDetail(teacherDetail);
									teacherSecondaryStatus.setJobOrder(null);
									teacherSecondaryStatus.setSecondaryStatusMaster(districtSpecificPortfolioQuestions.getSecondaryStatusMaster());
									teacherSecondaryStatus.setCreatedByUser(userMaster);
									teacherSecondaryStatus.setCreatedByEntity(userMaster.getEntityType());
									teacherSecondaryStatus.setDistrictMaster(districtMaster);
									teacherSecondaryStatus.setCreatedDateTime(new Date());
									teacherSecondaryStatusDAO.makePersistent(teacherSecondaryStatus);					
								}
								if((questionOptions.get(quesOpt).equalsIgnoreCase("No") || questionOptions.get(quesOpt).equalsIgnoreCase("Prefer Not To Answer")) && teacherTags.containsKey(teacherDetail.getTeacherId()+"##"+districtSpecificPortfolioQuestions.getSecondaryStatusMaster().getSecStatusId())){
										TeacherSecondaryStatus teacherSecondaryStatus = teacherTags.get(teacherDetail.getTeacherId()+"##"+districtSpecificPortfolioQuestions.getSecondaryStatusMaster().getSecStatusId());				
										teacherSecondaryStatusDAO.makeTransient(teacherSecondaryStatus);					
								}
							}
						}
						
						if(questTags.containsKey(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()) && questTags.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()).getProvideTag()){
							DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions = questTags.get(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId());
							String quesOpt = (districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId()+"##"+districtSpecificPortfolioAnswers.getSelectedOptions()).replace("|", "");
							
							if(questionOptions.get(quesOpt).equalsIgnoreCase("Yes") && !teacherTags.containsKey(teacherDetail.getTeacherId()+"##"+districtSpecificPortfolioQuestions.getSecondaryStatusMaster().getSecStatusId())){
								TeacherSecondaryStatus teacherSecondaryStatus = new TeacherSecondaryStatus();					
								teacherSecondaryStatus.setTeacherDetail(teacherDetail);
								teacherSecondaryStatus.setJobOrder(null);
								teacherSecondaryStatus.setSecondaryStatusMaster(districtSpecificPortfolioQuestions.getSecondaryStatusMaster());
								teacherSecondaryStatus.setCreatedByUser(userMaster);
								teacherSecondaryStatus.setCreatedByEntity(userMaster.getEntityType());
								teacherSecondaryStatus.setDistrictMaster(districtMaster);
								teacherSecondaryStatus.setCreatedDateTime(new Date());
								teacherSecondaryStatusDAO.makePersistent(teacherSecondaryStatus);					
							}
							if((questionOptions.get(quesOpt).equalsIgnoreCase("No") || questionOptions.get(quesOpt).equalsIgnoreCase("Prefer Not To Answer")) && teacherTags.containsKey(teacherDetail.getTeacherId()+"##"+districtSpecificPortfolioQuestions.getSecondaryStatusMaster().getSecStatusId())){
									TeacherSecondaryStatus teacherSecondaryStatus = teacherTags.get(teacherDetail.getTeacherId()+"##"+districtSpecificPortfolioQuestions.getSecondaryStatusMaster().getSecStatusId());				
									teacherSecondaryStatusDAO.makeTransient(teacherSecondaryStatus);					
							}
						}
					}
					
					return "1";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	public DistrictSpecificPortfolioAnswers getAnswerData(int questionId,int teacherId){
		System.out.println("::::::::::::::QuestionID=="+questionId+",:::teacherId:="+teacherId);
		
		DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestion= districtSpecificPortfolioQuestionsDAO.findById(questionId, false, false);
		System.out.println("Question::"+districtSpecificPortfolioQuestion.getQuestion());
		TeacherDetail teacherDetail= teacherDetailDAO.findById(teacherId, false, false);
		System.out.println("Teacher Email::"+teacherDetail.getEmailAddress());
		Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
		Criterion criterion2 = Restrictions.eq("districtSpecificPortfolioQuestions",districtSpecificPortfolioQuestion);
		//List<DistrictSpecificPortfolioAnswers> list = districtSpecificPortfolioAnswersDAO.findByCriteria(Restrictions.eq("districtSpecificPortfolioQuestions", question),Restrictions.eq("teacherdetail", teacherdetail));
		List<DistrictSpecificPortfolioAnswers> list = districtSpecificPortfolioAnswersDAO.findByCriteria(criterion1,criterion2);
		for(DistrictSpecificPortfolioAnswers bean:list)
		System.out.println("::::::::::::bean.getInsertedText()"+bean.getInsertedText());
		if(list!=null)
		return list.get(0);
		else
			return null;
	}

}
