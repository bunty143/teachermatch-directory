package tm.services.teacher;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import tm.bean.TeacherDetail;
import tm.services.clamav.ClamAVUtil;
import tm.utility.Utility;

public class SelfServiceResumeUploadServlet extends HttpServlet 
{	
	private static final long serialVersionUID = -6732310484121501736L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		
		System.out.println("ExpResumeUploadServlet : doGET");
		PrintWriter pw = response.getWriter();
		FileItemFactory factory = new DiskFileItemFactory();
		
		String msg="";
		
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		upload.setSizeMax(10485760);
		
		HttpSession session = request.getSession();
		PrintWriter out = response.getWriter();
	    
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		//String currentFileName=request.getParameter("f");
		
		List uploadedItems = null;
		FileItem fileItem = null;
		String filePath =Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/";
		File f=new File(filePath);
		if(!f.exists())
			 f.mkdirs();
		String ext="";
		boolean bSuccess=true;
		String fullFileName="";
		try 
		{
			String path="";
			
			/*File fin = new File(filePath);
			for (File file : fin.listFiles()) 
			{
				if((!file.getName().equals("Thumbs.db") && (file.getName().equals(currentFileName)) ))
				{
					file.delete();
				}
			}  */ 
			
			uploadedItems = upload.parseRequest(request);
			Iterator i = uploadedItems.iterator();
			String fileName="";
			
			int spacePost	=	-1;
	
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if (fileItem.isFormField() == false) 
				{
					if (fileItem.getSize() > 0)	
					{
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("/") > 0) ? "/" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						if(myFullFileName.length()>125)
						{
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.lastIndexOf("."));
							ext=myFullFileName.substring(myFullFileName.lastIndexOf("."),myFullFileName.length());
							myFileName = myFullFileName.substring(startIndex + 1, 120);
							spacePost=myFileName.lastIndexOf(" ");
							if(spacePost!=-1)
							{
								myFileName = myFullFileName.substring(startIndex + 1, spacePost);
							}
							myFileName=myFileName.replaceAll("[^\\w\\s]", "");
							fullFileName=myFileName+""+ext;
						}
						else
						{
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.lastIndexOf("."));
							ext=myFullFileName.substring(myFullFileName.lastIndexOf("."),myFullFileName.length());
							myFileName=myFileName.replaceAll("[^\\w\\s]", "");
							fullFileName=myFileName+""+ext;
						}
						File fin = new File(filePath);
						for (File file : fin.listFiles()) 
						{
							if((!file.getName().equals("Thumbs.db") && (file.getName().equals(fullFileName)) ))
							{
								file.delete();
							}
						}  
						uploadedFile = new File(filePath, fullFileName);
						fileItem.write(uploadedFile);
						msg = ClamAVUtil.scanAndRemove(uploadedFile.toString());
					}
				}
				fileItem=null;				
			}
			
			response.setContentType("text/html");
			
			/*pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			if(msg.equals(""))
				pw.print("window.top.saveResume(\""+fullFileName+"\");");
			else
				pw.print("window.top.fileContainsVirusDiv('"+msg+"')");
			pw.print("</script>");*/
		}
		
		catch (FileUploadException e) 
		{
			bSuccess=false;
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			bSuccess=false;
			e.printStackTrace();
		}
		finally
		{
			if(bSuccess)
				pw.print(fullFileName);
			else
				pw.print("Error");
		}
	}
}
