package tm.services.teacher;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import tm.bean.TeacherDetail;
import tm.utility.Utility;
import tm.services.clamav.ClamAVUtil;

public class FileUploadServletForCertification extends HttpServlet {

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{	
		try{
		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");  

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List uploadedItems = null;
		FileItem fileItem = null;
		FileItem fileItemAttachement = null;
		
		String ext="",filePath="";
		String msg="";
		String file="";
		HttpSession session=request.getSession();
		
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		uploadedItems = upload.parseRequest(request);
		upload.setSizeMax(10485760);
		Iterator i = uploadedItems.iterator();
		String fileName="";
		String certDateTime=Utility.getDateTime();
		
		if(request.getSession().getAttribute("previousCertificationFile")!=null){
			String previousFileName=request.getSession().getAttribute("previousCertificationFile").toString();
			System.out.println("from certificate servlet >> "+previousFileName);
			if(previousFileName!=null && !previousFileName.equals("")){
				String pFileName=Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+previousFileName;
				File pFile=new File(pFileName);
				if(pFile.exists()){
					pFile.delete();
				}
				session.removeAttribute("previousCertificationFile");
			}
		}
		String sbtsource_cert=""; 
		while (i.hasNext())	
		{
			
			fileItem = (FileItem) i.next();
			
			if(fileItem.getFieldName().equals("sbtsource_cert")){
				sbtsource_cert=fileItem.getString();
			}
			
			if(fileItem.getFieldName().equals("pathOfCertificationFile")){
				fileItemAttachement=fileItem;
			}
		}
		
		fileItem=fileItemAttachement;
		
		if(teacherDetail!=null ){
			filePath=Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/";

			File f=new File(filePath);
			if(!f.exists())
				f.mkdirs();

			if (fileItem.isFormField() == false){
				if (fileItem.getSize() > 0){
					File uploadedFile = null; 
					String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
					int startIndex = myFullFileName.lastIndexOf(slashType);
					myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
					ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
					fileName="Certification_"+certDateTime+ext;
					uploadedFile = new File(filePath, fileName);
					fileItem.write(uploadedFile);
					msg = ClamAVUtil.scanAndRemove(uploadedFile.toString());
				}
			}
			fileItem=null;				
		}
		response.setContentType("text/html");  
		pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
		if(msg.equals(""))
			pw.print("window.top.saveCertification('"+fileName+"',"+sbtsource_cert+");");
		else
			pw.print("window.top.fileContainsVirusDiv('"+msg+"')");
		pw.print("</script>");
		
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

}
