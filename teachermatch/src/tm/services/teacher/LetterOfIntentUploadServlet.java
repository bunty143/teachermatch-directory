package tm.services.teacher;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import tm.bean.TeacherDetail;
import tm.services.clamav.ClamAVUtil;
import tm.utility.Utility;

public class LetterOfIntentUploadServlet extends HttpServlet 
{


	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{	
		try{
			System.out.println("::::::::::::::LetterOfIntentUploadServlet::::::::::2:");
			PrintWriter pw = response.getWriter();
			response.setContentType("text/html");  

			request.setCharacterEncoding("UTF-8");
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			String msg="";
			
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			List uploadedItems = null;
			FileItem fileItem = null;
			String ext="",filePath="";
			String file="";
			HttpSession session=request.getSession();
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			String fileName="";
			String acaDateTime=Utility.getDateTime();
			
			String letterOfIntent="";
			String jobId="";
			String schoolId="";
			
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				
				if(fileItem.getFieldName().equals("letterOfIntent")){
					letterOfIntent=fileItem.getString();
				}
				
				if(fileItem.getName()!=null && !fileItem.getName().equals("")){
					break;
				}
				if(fileItem.getFieldName().equals("schoolIdForletterOfIntent")){
					schoolId=fileItem.getString();
				}
				if(fileItem.getFieldName().equals("jobIdForletterOfIntent")){
					jobId=fileItem.getString();
				}
			}
			System.out.println("teacherDetail::::"+teacherDetail.getTeacherId());
			if(teacherDetail!=null ){
				filePath=Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+jobId+"/"+schoolId+"/";
				System.out.println("filePath:::::"+filePath);
				File f=new File(filePath);
				if(!f.exists())
					f.mkdirs();

				if (fileItem.isFormField() == false){
					if (fileItem.getSize() > 0){
						File uploadedFile = null; 
						
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						fileName=myFileName.substring(0,myFileName.lastIndexOf("."))+teacherDetail.getTeacherId()+acaDateTime+ext;
						
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
						msg = ClamAVUtil.scanAndRemove(uploadedFile.toString());
						
					}
				}
				fileItem=null;				
			}		
			System.out.println("fileName:::"+fileName);
			response.setContentType("text/html");  
			pw.print("<script type=\"text/javascript\" language=\"javascript\">");
			if(msg.equals(""))
			pw.print("window.top.saveSelectedSchoolsByServlet('"+fileName+"');");
			else
			pw.print("window.top.fileContainsVirusDiv('"+msg+"')");			
		    pw.print("</script>");	
			} 
			catch (FileUploadException e) 
			{
				e.printStackTrace();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
	}

}
