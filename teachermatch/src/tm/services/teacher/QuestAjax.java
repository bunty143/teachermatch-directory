package tm.services.teacher;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.net.ssl.HttpsURLConnection;
import javax.persistence.Basic;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.EmployeeMaster;
import tm.bean.JobApplicationHistory;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.Jobrequisitionnumberstemp;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAchievementScore;
import tm.bean.TeacherAffidavit;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherExperience;
import tm.bean.TeacherInvolvement;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherPreference;
import tm.bean.TeacherRole;
import tm.bean.TeacherSecondaryStatus;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.TeacherVideoLink;
import tm.bean.assessment.TeacherWiseAssessmentTime;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DegreeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.MasterSubjectMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.master.UniversityMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.controller.teacher.BasicController;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobApplicationHistoryDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAchievementScoreDAO;
import tm.dao.TeacherAffidavitDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherInvolvementDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.TeacherRoleDAO;
import tm.dao.TeacherSecondaryStatusDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.TeacherVideoLinksDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.assessment.TeacherWiseAssessmentTimeDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DegreeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.MasterSubjectMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.SecondaryStatusMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.TeacherAnswerDetailsForDistrictSpecificQuestionsDAO;
import tm.dao.master.UniversityMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.MasterPasswordDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.services.PaginationAndSorting;
import tm.services.district.PrintOnConsole;
import tm.services.report.CandidateGridService;
import tm.utility.ElasticSearchConfig;
import tm.utility.IPAddressUtility;
import tm.utility.TeacherDetailComparatorLastNameAsc;
import tm.utility.TeacherDetailComparatorLastNameDesc;
import tm.utility.Utility;

public class QuestAjax
{
	 String locale = Utility.getValueOfPropByKey("locale");
	 String msgLogingAuthenticationYouerAccout=Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout", locale);
	 String msgLogingAuthenticationYouerAccout2=Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout2", locale);
	 String msgLogingAuthenticationYouerAccout3=Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout3", locale);
	 String msgLogingAuthenticationYouerAccout4=Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout4", locale);
	 String msgIfMemberAlreadyRegPlzProvideOtherEmail=Utility.getLocaleValuePropByKey("msgIfMemberAlreadyRegPlzProvideOtherEmail", locale);
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lblNm=Utility.getLocaleValuePropByKey("lblNm", locale);
	 String lblNormScore=Utility.getLocaleValuePropByKey("lblNormScore", locale);
	 String btnSave=Utility.getLocaleValuePropByKey("btnSave", locale);
	 String btnClose=Utility.getLocaleValuePropByKey("btnClose", locale);
	 String toolNoEPI=Utility.getLocaleValuePropByKey("toolNoEPI", locale);
	 String sltEpiComp=Utility.getLocaleValuePropByKey("sltEpiComp", locale);
	 String toolResetEPI=Utility.getLocaleValuePropByKey("toolResetEPI", locale);
	 String toolAssDetails=Utility.getLocaleValuePropByKey("toolAssDetails", locale);
	 String toolNoAssDetail=Utility.getLocaleValuePropByKey("toolNoAssDetail", locale);
	 String lblEPIAccomodation=Utility.getLocaleValuePropByKey("lblEPIAccomodation", locale);
	 String optNA=Utility.getLocaleValuePropByKey("optNA", locale);
	 String msgNumOfJobAppiedAt=Utility.getLocaleValuePropByKey("msgNumOfJobAppiedAt", locale);
	 String msgNumOfJobAppiedAt2=Utility.getLocaleValuePropByKey("msgNumOfJobAppiedAt2", locale);
	 
	 private String lblCandidateFirstName = Utility.getLocaleValuePropByKey(
				"lblCandidateFirstName", locale);
		private String lblCandidateLastName = Utility.getLocaleValuePropByKey(
				"lblCandidateLastName", locale);
		private String lblCandidateEmailAddress = Utility.getLocaleValuePropByKey(
				"lblCandidateEmailAddress", locale);
		private String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord",
				locale);
		String lblDistrictName=Utility.getLocaleValuePropByKey("lblDistrictName",
				locale);
	 
	@Autowired 
	private MasterPasswordDAO masterPasswordDAO;
	@Autowired
	private TeacherCertificateDAO certificateDAO;
	@Autowired
	private TeacherPreferenceDAO preferenceDAO;
	@Autowired
	private TeacherAcademicsDAO academicsDAO;	
	@Autowired
	private TeacherVideoLinksDAO linksDAO;
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;	
	@Autowired
	private TeacherElectronicReferencesDAO electronicReferencesDAO;
	@Autowired
	private TeacherRoleDAO teacherRoleDAO;
	@Autowired
	private TeacherInvolvementDAO teacherInvolvementDAO;
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	@Autowired
	private UserMasterDAO userMasterDAO;
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@Autowired
	private BasicController basicController;
	@Autowired
	private EmailerService emailerService;
	@Autowired
	private RoleMasterDAO roleMasterDAO;
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;	
	@Autowired
	private TeacherAchievementScoreDAO teacherAchievementScoreDAO;
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	@Autowired
	private SecondaryStatusMasterDAO secondaryStatusMasterDAO;
	@Autowired
	private TeacherWiseAssessmentTimeDAO teacherWiseAssessmentTimeDAO;
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	public void setDistrictSpecificQuestionsDAO(
			DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO) {
		this.districtSpecificQuestionsDAO = districtSpecificQuestionsDAO;
	}
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}
	@Autowired
	private TeacherSecondaryStatusDAO teacherSecondaryStatusDAO;
	public void setTeacherSecondaryStatusDAO(TeacherSecondaryStatusDAO teacherSecondaryStatusDAO) {
		this.teacherSecondaryStatusDAO = teacherSecondaryStatusDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
	@Autowired
	private StateMasterDAO stateMasterDAO;
	@Autowired
	private TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	public void setTeacherAnswerDetailsForDistrictSpecificQuestionsDAO(
			TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO) {
		this.teacherAnswerDetailsForDistrictSpecificQuestionsDAO = teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	}
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	public void setJobCertificationDAO(JobCertificationDAO jobCertificationDAO) {
		this.jobCertificationDAO = jobCertificationDAO;
	}
	@Autowired
	private DegreeMasterDAO degreeMasterDAO;
	public void setDegreeMasterDAO(DegreeMasterDAO degreeMasterDAO) {
		this.degreeMasterDAO = degreeMasterDAO;
	}
	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;
	public void setTeacherPreferenceDAO(
			TeacherPreferenceDAO teacherPreferenceDAO) {
		this.teacherPreferenceDAO = teacherPreferenceDAO;
	}
	@Autowired
	private UniversityMasterDAO universityMasterDAO;
	public void setUniversityMasterDAO(UniversityMasterDAO universityMasterDAO) {
		this.universityMasterDAO = universityMasterDAO;
	}
	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	public void setTeacherAcademicsDAO(TeacherAcademicsDAO teacherAcademicsDAO) {
		this.teacherAcademicsDAO = teacherAcademicsDAO;
	}
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO; 
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	@Autowired
	private JobApplicationHistoryDAO jobApplicationHistoryDAO;	
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	@Autowired
	private MasterSubjectMasterDAO masterSubjectMasterDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	public void setMasterSubjectMasterDAO(
			MasterSubjectMasterDAO masterSubjectMasterDAO) {
		this.masterSubjectMasterDAO = masterSubjectMasterDAO;
	}
	public void setJobApplicationHistoryDAO(
			JobApplicationHistoryDAO jobApplicationHistoryDAO) {
		this.jobApplicationHistoryDAO = jobApplicationHistoryDAO;
	}

	public List<JobOrder> getFilterJobOrder(List<JobOrder> jobOrderSList, List<JobOrder> jobOrderCertList)
	{
		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		String jobIds="";

		try{
			for(JobOrder jobOrder: jobOrderSList){
				jobIds+="||"+jobOrder.getJobId()+"||";
			}
		}catch (Exception e) { }
		try{
			for(JobOrder jobOrder: jobOrderCertList){
				if(jobIds.contains("||"+jobOrder.getJobId()+"||")){
					jobOrderList.add(jobOrder);
				}
			}
		}catch (Exception e) {}
		return jobOrderList;
	}
	
	public int jobListByTeacher(int teacherId,SortedMap<Long,JobForTeacher> jftMap)
	{
		int jobList=0;
		try{
			Iterator iterator = jftMap.keySet().iterator();
			JobForTeacher jobForTeacher=null;
			while (iterator.hasNext()) {
				Object key = iterator.next();
				jobForTeacher=jftMap.get(key);
				int teacherKey=Integer.parseInt(jobForTeacher.getTeacherId().getTeacherId()+"");
				if(teacherKey==teacherId){
					jobList++;
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return jobList;
	}
	
	static Properties  properties = new Properties();  
	static{
		try {
			InputStream inputStream = new Utility().getClass().getClassLoader().getResourceAsStream("smtpmail.properties");
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public int powerTracker(int id)
	{	
		StringBuffer b=new StringBuffer();
		System.out.println("id    "+id);
		int marks=0;
		try
		{
		TeacherDetail teacherDetail=teacherDetailDAO.findById(id, false, false);
		if(teacherDetail!=null)
		{
			if(teacherDetail.getVerificationStatus()==1)
			{
				System.out.println("varification status==  "+teacherDetail.getVerificationStatus());
				marks=marks+5;
			}
			
		}
		
		TeacherPortfolioStatus teacherPortfolioStatus=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);				
		if(teacherPortfolioStatus!=null)
			if(teacherPortfolioStatus.getIsAffidavitCompleted())
			{
				System.out.println("IsAffidavitCompleted==   "+teacherPortfolioStatus.getIsAffidavitCompleted());
				marks=marks+5;
			}
	
		
		TeacherPreference teacherPreference=preferenceDAO.findPrefByTeacher(teacherDetail);
		if(teacherPreference!=null)
		{
			System.out.println("teacherPreference==   "+teacherPreference);
			marks=marks+5;
		}
		
		
		List<TeacherAcademics> teacherAcademics=academicsDAO.findAcadamicDetailByTeacher(teacherDetail);
		if(teacherAcademics!=null && teacherAcademics.size()!=0 )
		if( teacherAcademics!=null && teacherAcademics.size()!=0)
		{
			System.out.println("teacherAcademics==   "+teacherAcademics.size());
			marks=marks+5;
		}
		
		List<TeacherCertificate> teacherCertificates=certificateDAO.findCertificateByTeacher(teacherDetail);
		if(teacherCertificates!=null && teacherCertificates.size()!=0 )
		if(teacherCertificates!=null && teacherCertificates.size()!=0)
		{
			System.out.println("teacherCertificates  ==  "+teacherCertificates.size());
			if(teacherCertificates.size()==1)
			{
				marks=marks+5;
			}
			else if(teacherCertificates.size()>1)
			{
				marks=marks+10;
			}
			
		}		
		List<TeacherElectronicReferences> electronicReferences=electronicReferencesDAO.findActiveReferencesByTeacher(teacherDetail);
		if(teacherCertificates!=null && electronicReferences.size()!=0)
		{
			System.out.println("electronicReferences ==  "+electronicReferences.size());
			if(electronicReferences.size()==1)
			{
				marks=marks+5;
			}
			else if(electronicReferences.size()==2)
			{
				marks=marks+10;
			}
			else if(electronicReferences.size()>2)
			{
				marks=marks+15;
			}
		}	
		
		List<TeacherRole> teacherRoles=teacherRoleDAO.findTeacherRoleByTeacher(teacherDetail);
		if(teacherRoles!=null && teacherRoles.size()!=0)
		{
			System.out.println("teacherRoles  ==  "+teacherRoles.size());
			marks=marks+5;
		}
		
		List<TeacherInvolvement> teacherInvolvement=teacherInvolvementDAO.findTeacherInvolvementByTeacher(teacherDetail);
		if(teacherInvolvement!=null && teacherInvolvement.size()!=0)
		{
			System.out.println("teacherInvolvement ==  "+teacherInvolvement.size());
			marks=marks+5;
		}
		
		String teacherAssessmentStatus=teacherAssessmentStatusDAO.epiStatus(teacherDetail);		
		if(teacherAssessmentStatus.equalsIgnoreCase("comp"))
		{
			System.out.println("teacherAssessmentStatus ==  "+teacherAssessmentStatus);
			marks=marks+20;
		}
		
		List<JobForTeacher> jobForTeachers=jobForTeacherDAO.findJobByTeacherOneYear(teacherDetail);
		if(jobForTeachers!=null && jobForTeachers.size()!=0)
		{
			System.out.println("job for teacher==="+jobForTeachers.size());
			System.out.println(" List Job For Teacher : "+jobForTeachers.get(0).getCreatedDateTime());
			if(jobForTeachers.size()==1)
			{
				marks=marks+2;
			}
			else if(jobForTeachers.size()==2)
			{
				marks=marks+4;
			}
			else if(jobForTeachers.size()==3)
			{
				marks=marks+6;
			}
			else if(jobForTeachers.size()==4)
			{
				marks=marks+8;
			}
			else if(jobForTeachers.size()>4)
			{
				marks=marks+10;
			}
		}
		
		List<TeacherVideoLink> teacherVideoLink=linksDAO.findTeachByVideoLink(teacherDetail);
		if(teacherVideoLink!=null && teacherVideoLink.size()!=0)
		{
			System.out.println("vedio link ==  "+teacherVideoLink.size());
			marks=marks+10;
		}
		
		TeacherExperience teacherExperiences=teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
 		if(teacherExperiences!=null)
 		{
 			if(teacherExperiences.getResume()!=null)
 			{
 				System.out.println("Resume == "+teacherExperiences.getResume());
 				marks=marks+5;
 			}
 		}
 			
		}
		catch(Exception e)
		{e.printStackTrace();}
		
		System.out.println("Total Score=  "  +marks);		
		
		return marks;
	}
	public String teacherLogin(String emailAddress,String password)
	{
		String s="";		
		boolean teacherloggedIn=false;	
		try
		{
		password = MD5Encryption.toMD5(password);		
	    List<TeacherDetail>	listTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
	    if(listTeacherDetails!=null && listTeacherDetails.size() > 0)
		{
			PrintOnConsole.debugPrintln("Teacher Login","Teacher is in our TeacherDB, TeacherId: "+listTeacherDetails.get(0).getTeacherId());
			if(listTeacherDetails.get(0).getPassword().equalsIgnoreCase(password))
			{
				PrintOnConsole.debugPrintln("Teacher Login","password for Teacher is matched");
				if(listTeacherDetails.get(0).getVerificationStatus().equals(0))
				{ 
					s=msgLogingAuthenticationYouerAccout;					
				}
				else if(listTeacherDetails.get(0).getStatus().equalsIgnoreCase("I"))
				{
					s=msgLogingAuthenticationYouerAccout2;					
				}
			}
			else
			{
				if(masterPasswordDAO.isValidateUser("teacher", password))
				{
					PrintOnConsole.debugPrintln("Teacher Login","Master password for Teacher is matched");
					if(listTeacherDetails.get(0).getVerificationStatus().equals(0))
					{ 
						s=msgLogingAuthenticationYouerAccout;					
					}
					else if(listTeacherDetails.get(0).getStatus().equalsIgnoreCase("I"))
					{
						s=msgLogingAuthenticationYouerAccout2;					
					}
				}
				else
				{
					s=msgLogingAuthenticationYouerAccout3;					
				}				
			}
		}
	    else
	    {
	    	s=msgLogingAuthenticationYouerAccout3;
	    }    
	    
		}
		catch(Exception e)
		{e.printStackTrace();}		
		
		return s;
	}
	
	public String userLogin(String emailAddress,String password)
	{
		/*This variable(isType3User) are used to create json*/
		boolean isType3User=false;
				
		String s="";
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = null;
		session = request.getSession();	
		boolean teacherloggedIn=false;	
		try
		{
		password = MD5Encryption.toMD5(password);		
	    List<UserMaster> userDetails = userMasterDAO.findByEmail(emailAddress);
	    UserMaster userMaster = null;	 
	    if(userDetails.size()>0)
			userMaster=userDetails.get(0);
	    if(userDetails!=null && userDetails.size() > 0)
		{
			PrintOnConsole.debugPrintln("Teacher Login","Teacher is in our TeacherDB, TeacherId: "+userDetails.get(0).getUserId());
			if(userDetails.get(0).getPassword().equalsIgnoreCase(password))
			{
				PrintOnConsole.debugPrintln("user Login","password for Teacher is matched");				
				if(userMaster.getStatus().equalsIgnoreCase("I"))
				{
					s=msgLogingAuthenticationYouerAccout2;					
				}
				else
				{
					if(userMaster.getDistrictId().getStatus().equalsIgnoreCase("I"))
					{
						s=msgLogingAuthenticationYouerAccout4;	
					}
					else
					{
						session.setAttribute("userMaster", userMaster);
						isType3User= (userMaster.getEntityType()!=null && userMaster.getEntityType()==3);
					}					    				
				}
			}
			else
			{
				if(masterPasswordDAO.isValidateUser("user", password))
				{
					PrintOnConsole.debugPrintln("Teacher Login","Master password for Teacher is matched");
					if(userMaster.getStatus().equalsIgnoreCase("I"))
					{
						s=msgLogingAuthenticationYouerAccout2;					
					}	
					else
					{
						if(userMaster.getDistrictId().getStatus().equalsIgnoreCase("I"))
						{
							s=msgLogingAuthenticationYouerAccout4;
						}
						else
						{
							session.setAttribute("userMaster", userMaster);
							isType3User= (userMaster.getEntityType()!=null && userMaster.getEntityType()==3);
						}							
					}
				}
				else
				{
					s=msgLogingAuthenticationYouerAccout3;					
				}				
			}			
		}
	    else
	    {
	    	s=msgLogingAuthenticationYouerAccout3;
	    }
	    
	    //create a json, if user is type 3 and authorized
	    if(isType3User && userDetails.size()>1)
		{
	    	String scId = "";
	    	for(int i=0;i<userDetails.size();i++)
	    		if(i==0)
	    			scId = scId + "{\"schoolId\": \""+userDetails.get(i).getSchoolId().getSchoolId()+"\", \"schoolName\": \""+userDetails.get(i).getSchoolId().getSchoolName()+"\"}";
	    		else
	    			scId = scId + ",{\"schoolId\": \""+userDetails.get(i).getSchoolId().getSchoolId()+"\", \"schoolName\": \""+userDetails.get(i).getSchoolId().getSchoolName()+"\"}";
	    	
	    	s = "";
	    	s = s +"{\"isType3User\": \""+isType3User+"\"";
	    	s = s +" , \"schoolsDetails\": ["+scId+"]}";
	    	
	    	return s;
	    }
	    
	    }
		catch(Exception e)
		{e.printStackTrace();}
		
		return s;
	}
	public List<DistrictMaster> getFieldOfDistrictList(String DistrictName)
	{		
		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		try{

			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "I");
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);

				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);

				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));

			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion1);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return districtMasterList;
	}
	public boolean jobPost()
	{
		boolean result=false;	
		try
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);		
			if (session == null || session.getAttribute("teacherDetail") == null) 
			{
				result=false;
				return result;
			}
			else
			{
				return result=true;	
			}
	    
		}
		catch(Exception e)
		{e.printStackTrace();}		
		
		return result;
	}
	@Transactional(readOnly=false)
	public String questSignUp(String districtId,String fname,String lname,String emailAddress,String password,int sumOfCaptchaText,int other)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		String s="Server error";
		try
		{
		int authorizationkey=(int) Math.round(Math.random() * 2000000);
		System.out.println(districtId+"::"+fname+"::"+lname+"::"+fname+"::"+emailAddress+"::"+password+"::"+sumOfCaptchaText);
	
		TeacherDetail teacherDetail=new TeacherDetail();
		UserMaster userMaster=new UserMaster();
		HeadQuarterMaster headQuarterMaster=null;
		List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
		List<UserMaster> lstUserMaster = userMasterDAO.findByEmail(emailAddress);
		if((lstTeacherDetails!=null && lstTeacherDetails.size()>0) || (lstUserMaster!=null && lstUserMaster.size()>0)){
			s= msgIfMemberAlreadyRegPlzProvideOtherEmail;			
			return s;
		}
		if(other==1)
		{
			System.out.println("district signup with other");
			System.out.println("======Mail Content for Client Services======");	
			//System.out.println(MailText.getRegistrationNotificationMailForQuestUser(districtId,fname,lname,emailAddress, password,0));
			//emailerService.sendMailAsHTMLText(properties.getProperty("smtphost.clientservices"), "Non-Client District Sign-Up",MailText.getRegistrationNotificationMailForQuestUser(districtId,fname,lname,emailAddress, password,0));
			try {
				String content = "";
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom(properties.getProperty("smtphost.adminusername"));
				dsmt.setMailto(properties.getProperty("smtphost.clientservices"));
				dsmt.setMailsubject("Non-Client District Sign-Up");
				content=MailText.getRegistrationNotificationMailForQuestUser(districtId,fname,lname,emailAddress, password,0);					
				dsmt.setMailcontent(content);
				System.out.println("content  "+content);
				try {
					dsmt.start();	
				} catch (Exception e) {}

			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("======Mail Content for TeacherMatch Partners======");
			//System.out.println(MailText.getRegistrationNotificationMailForQuestUser(districtId,fname,lname,emailAddress, password,1));
		//	emailerService.sendMailAsHTMLText(properties.getProperty("smtphost.tmpartners"), "Non-Client District Sign-Up",MailText.getRegistrationNotificationMailForQuestUser(districtId,fname,lname,emailAddress, password,1));
			try {
				String content = "";
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom(properties.getProperty("smtphost.adminusername"));
				dsmt.setMailto(properties.getProperty("smtphost.tmpartners"));
				dsmt.setMailsubject("Non-Client District Sign-Up");
				content=MailText.getRegistrationNotificationMailForQuestUser(districtId,fname,lname,emailAddress, password,1);					
				dsmt.setMailcontent(content);
				System.out.println("content  "+content);
				try {
					dsmt.start();	
				} catch (Exception e) {}

			} catch (Exception e) {
				e.printStackTrace();
			}
			s="";
		}
		else
		{
			if(districtId.trim().length()==0)
			{
				System.out.println("teacher signup");
				teacherDetail.setFirstName(fname);
				teacherDetail.setLastName(lname);
				teacherDetail.setEmailAddress(emailAddress);
				teacherDetail.setUserType("N");
				teacherDetail.setPassword(MD5Encryption.toMD5(password));	
				teacherDetail.setFbUser(false);
				teacherDetail.setQuestCandidate(1);
				teacherDetail.setAuthenticationCode(""+authorizationkey);
				teacherDetail.setVerificationCode(""+authorizationkey);
				teacherDetail.setVerificationStatus(0);
				teacherDetail.setIsPortfolioNeeded(true);
				teacherDetail.setNoOfLogin(0);
				teacherDetail.setStatus("A");
				teacherDetail.setSendOpportunity(false);
				teacherDetail.setIsResearchTeacher(false);
				teacherDetail.setForgetCounter(0);
				teacherDetail.setCreatedDateTime(new Date());
				teacherDetail.setIpAddress(IPAddressUtility.getIpAddress(request));
				teacherDetail.setInternalTransferCandidate(false);
				teacherDetail.setQuestCandidate(1);
				teacherDetailDAO.makePersistent(teacherDetail);
				basicController.saveJobForTeacher(request, teacherDetail);				
				System.out.println("teacherDetail.getEmailAddress()==="+teacherDetail.getEmailAddress());
				//System.out.println(" Mail Content :: "+MailText.getRegistrationMailForQuest(request,teacherDetail));
				//emailerService.sendMailAsHTMLText("admin@teachermatch.com", "Welcome to TeacherMatch, please confirm your account",MailText.getRegistrationMailForQuest(request,teacherDetail));
				try {
					String content = "";
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom(properties.getProperty("smtphost.adminusername"));
					dsmt.setMailto(teacherDetail.getEmailAddress());
					dsmt.setMailsubject("Thank You For Signing Up for Quest!");
					content=MailText.getRegistrationMailForQuest(request,teacherDetail);					
					dsmt.setMailcontent(content);
					System.out.println("content  "+content);
					try {
						dsmt.start();	
					} catch (Exception e) {}

				} catch (Exception e) {
					e.printStackTrace();
				}
				s="";
			}
			else
			{  
				System.out.println("district signup");
				DistrictMaster districtMaster=districtMasterDAO.findByDistrictId(districtId);
				
				if(districtMaster!=null && districtMaster.getHeadQuarterMaster()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
				{
					headQuarterMaster=headQuarterMasterDAO.findById(2, false, false);
					if(headQuarterMaster!=null)
					userMaster.setHeadQuarterMaster(headQuarterMaster);
				}	
				
				districtMaster.setQuestDistrict(1);
				districtMaster.setAddress("");
				districtMaster.setStatus("A");
				districtMaster.setJobApplicationCriteriaForProspects(1);
				districtMaster.setDmName(""+fname+" "+lname);
				districtMasterDAO.makePersistent(districtMaster);				
				System.out.println("districtMaster  "+districtMaster);
				List<RoleMaster> roleMaster=roleMasterDAO.getRoleNameByEntityType(2);			
				userMaster.setEntityType(2);
				userMaster.setDistrictId(districtMaster);
				userMaster.setFirstName(fname);
				userMaster.setLastName(lname);				
				userMaster.setEmailAddress(emailAddress);			
				userMaster.setPassword(MD5Encryption.toMD5(password));	
				userMaster.setRoleId(roleMaster.get(0));
				userMaster.setAuthenticationCode(""+authorizationkey);
				userMaster.setVerificationCode(""+authorizationkey);
				userMaster.setStatus("I");
				userMaster.setForgetCounter(0);
				userMaster.setIsExternal(true);
				userMaster.setIsQuestCandidate(true);
				userMaster.setCreatedDateTime(new Date());							
				userMasterDAO.makePersistent(userMaster);				
				//emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), "Welcome to User, please confirm your account",MailText.getRegistrationMailForQuestUser(request,userMaster));	
				s="";
				try {
					String content = "";
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom(properties.getProperty("smtphost.adminusername"));
					dsmt.setMailto(properties.getProperty("smtphost.clientservices"));
					dsmt.setMailsubject("Non-Client District Sign-Up");
					content=MailText.getRegistrationForClientServices(request, userMaster);					
					dsmt.setMailcontent(content);
					System.out.println("content  "+content);
					try {
						dsmt.start();	
					} catch (Exception e) {}

				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					String content = "";
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom(properties.getProperty("smtphost.adminusername"));
					dsmt.setMailto(properties.getProperty("smtphost.tmpartners"));
					dsmt.setMailsubject("Non-Client District Sign-Up");
					content=MailText.getRegistrationForClientServices(request, userMaster);					
					dsmt.setMailcontent(content);
					System.out.println("content  "+content);
					try {
						dsmt.start();	
					} catch (Exception e) {}

				} catch (Exception e) {
					e.printStackTrace();
				}
				//getRegistrationMailForQuestUser
				try {
					String content = "";
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom(properties.getProperty("smtphost.tmpartners"));
					dsmt.setMailto(emailAddress);
					dsmt.setMailsubject("Thank You For Choosing Quest!");
					content=MailText.getRegistrationMailForNonClientDistrict(request, userMaster);					
					dsmt.setMailcontent(content);
					System.out.println("content  "+content);
					try {
						dsmt.start();	
					} catch (Exception e) {}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return s;
		}
		return s;
	}
	
	public String displayTeacherGrid(String firstName,String lastName, String emailAddress,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String jobAppliedFromDate,String jobAppliedToDate,int daysVal)
	{
		System.out.println("==================Quest Display Teacher Grid==========");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp); 
		}
		//-- get no of record in grid,
		//-- set start and end position
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start = ((pgNo-1)*noOfRowInPage);
		int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		boolean flagOhio8 = false;
		//------------------------------------
		StringBuffer tmRecords =	new StringBuffer();
		CandidateGridService cgService=new CandidateGridService();
		try{
			List<TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();

			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"lastName";
			String sortOrderNoField		=	"lastName";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			sortOrderStrVal=Order.asc("lastName");
			/**End ------------------------------------**/
			int roleId=0;
			int entityID=0;
			boolean isMiami = false;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			UserMaster userMaster=null;
			int entityTypeId=1;
			int utype = 1;
			boolean writePrivilegeToSchool =false;
			boolean isManage = true;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
					if(districtMaster.getDistrictId().equals(1200390))
						isMiami = true;
					writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
				}
				if(userMaster.getSchoolId()!=null){
					schoolMaster =userMaster.getSchoolId();
				}	
				entityID=userMaster.getEntityType();
				System.out.println("===========EntityId : "+entityID);
				utype = entityID;
			}
			
			if(isMiami && entityID==3)
				isManage=false;

			boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
			if(entityID==1)
			{
				achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
			}else
			{
				try{
					DistrictMaster districtMasterObj=districtMaster;
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
				}catch(Exception e){ e.printStackTrace();}
			}

			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,43,"teacherinfo.do",1);
			}catch(Exception e){
				e.printStackTrace();
			}
			Date fDate=null;
			Date tDate=null;
			Date appliedfDate=null;
			Date appliedtDate=null;
			boolean applieddateFlag=false;
			boolean dateFlag=false;
			try{
				// job Applied Filter
				if(jobAppliedFromDate!=null && !jobAppliedFromDate.equals("")){
					appliedfDate=Utility.getCurrentDateFormart(jobAppliedFromDate);
					applieddateFlag=true;
				}
				if(jobAppliedToDate!=null && !jobAppliedToDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(jobAppliedToDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					appliedtDate=cal2.getTime();
					applieddateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}			
			List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();
			boolean certTypeFlag=false;			
			System.out.println("jobOrderIds:: "+jobOrderIds.size());
			System.out.println("jobOrderIds:: "+jobOrderIds);
			System.out.println("certTypeFlag:: "+certTypeFlag);		
			List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
			List<TeacherDetail> NbyATeacherList = new ArrayList<TeacherDetail>();
			//List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean teacherFlag=false;			
			boolean nByAflag=false;		
			List<TeacherDetail> subjectTeacherList = new ArrayList<TeacherDetail>();
			boolean subjectFlag=false;
		
			if(subjectFlag && subjectTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(subjectTeacherList);
				}else{
					filterTeacherList.addAll(subjectTeacherList);
				}
			}
			System.out.println(" filterTeacherList :: "+filterTeacherList);
			if(subjectFlag){
				teacherFlag=true;
			}

		
		
		
		
			
			
			
			// Start /////////////////////////////// candidate list By certificate ///////////////////////////////////////
			StateMaster stateMasterForCandidate=null;
			List<CertificateTypeMaster> certTypeMasterListForCndt =new ArrayList<CertificateTypeMaster>();
			List<TeacherDetail> tListByCertificateTypeId = new ArrayList<TeacherDetail>();
			boolean tdataByCertTypeIdFlag = false;	
			
			/************* End **************/
			StatusMaster statusMaster = new StatusMaster();
			boolean status=false;			
			int internalCandVal=0;	
			SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
			SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
			List<JobForTeacher> lstJobForTeacher =	 new ArrayList<JobForTeacher>();
			List<JobOrder> lstJobOrderList =new ArrayList<JobOrder>();
			List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
			int noOfRecordCheck=0;
			
			SchoolMaster sm = schoolMaster;
			List<TeacherDetail> lstTeacherDetailAll= new ArrayList<TeacherDetail>();
			if(entityTypeId!=0){
				if(entityID==3){
					
					lstJobOrderSLList =schoolInJobOrderDAO.allJobOrderPostedBySchool(schoolMaster);
					entityTypeId=3;
				}				
				if(entityTypeId==3 && lstJobOrderSLList.size()>0 && certTypeFlag)
				{
					lstJobOrderList.addAll(getFilterJobOrder(lstJobOrderSLList,jobOrderIds));
				}
				else if(entityTypeId!=3 && lstJobOrderSLList.size()==0 && certTypeFlag)
				{
					lstJobOrderList.addAll(jobOrderIds);
					entityTypeId=3;
				}
				else
				{
					lstJobOrderList.addAll(lstJobOrderSLList);
				}
				if(entityID==2){
					entityTypeId=2;
				}
				
				String stq="";
				if(districtMaster!=null  && utype==3)
				{
					SecondaryStatus smaster = districtMaster.getSecondaryStatus();
					if(smaster!=null)
					{
						String secStatus = smaster.getSecondaryStatusName();
						if(secStatus!=null && !secStatus.equals(""))
						{
							//
							 List<SecondaryStatus> lstStatus = new ArrayList<SecondaryStatus>();
							 lstStatus = secondaryStatusDAO.findSecondaryStatusByName(districtMaster,secStatus);
							 System.out.println("lstStatus.size():::: "+lstStatus.size());
							 for (SecondaryStatus secondaryStatus : lstStatus) {
								 if(secondaryStatus.getJobCategoryMaster()!=null)
								 {
									 stq+=" IF(secondarys3_.jobCategoryId="+secondaryStatus.getJobCategoryMaster().getJobCategoryId()+", ( secondaryStatusId >="+secondaryStatus.getSecondaryStatusId()+" ),false) or";
								 }
							}
							 if(stq.length()>3)
							 {
								 stq = stq.substring(0, stq.length()-2);
							 }
						}
					}
				}
				
				boolean questFlag = false;
				List <DistrictMaster> districtMastersList=new ArrayList<DistrictMaster>();
				if(entityID==1 && entityTypeId==1 && dateFlag==false && applieddateFlag==false && status==false && internalCandVal==0){
					//>>>>>>>>>>>>>>>>>>>>> Job Applied change >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
					//System.out.println("lstJobOrderList:::"+lstJobOrderList.size());
					//lstTeacherDetail=jobForTeacherDAO.getTeacherDetailByDASA(sortOrderStrVal,start,noOfRowInPage,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq);
					lstTeacherDetailAll=jobForTeacherDAO.getQuestTeacherDetailRecords(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm);
					//>>>>>>>>>>>>>>>>>>>>> Job Applied change >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
					
					totalRecord=lstTeacherDetailAll.size();					
				}else{
					//shriram
					DistrictMaster dm=null;
					Criterion critdistrict=null;
					if(districtMaster!=null && entityID==2)
					{
						HeadQuarterMaster hq=districtMaster.getHeadQuarterMaster();
						if(hq!=null && hq.getHeadQuarterId()==3)
						{
							flagOhio8=true;
						Criterion critheadquarter=Restrictions.eq("headQuarterMaster", hq);
						dm= districtMasterDAO.findById(3904380, false, false);
						if(districtMaster.getDistrictId()!=3904380)
						{			critdistrict=  Restrictions.ne("districtId", dm.getDistrictId());
						districtMastersList=districtMasterDAO.findByCriteria(critheadquarter,critdistrict);
						}
						else
							districtMastersList=districtMasterDAO.findByCriteria(critheadquarter);
						System.out.println("Size of DistrictmasterList"+districtMastersList.size());
					}
					}
					//ended by shriram
					if(!(entityTypeId==3 && lstJobOrderList.size()==0))
					{
						//lstTeacherDetail=jobForTeacherDAO.getTeacherDetailByDASA(sortOrderStrVal,start,noOfRowInPage,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq);
						lstTeacherDetailAll=jobForTeacherDAO.getQuestTeacherDetailRecords(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList ,firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm);
					}
					totalRecord=lstTeacherDetailAll.size();
				}
				if(totalRecord<end)
					end=totalRecord;

				
				System.out.println("++++++++++++++++++++++++++++:: "+lstTeacherDetailAll.size());
				
				
				
				// sorting on normscore, ascore, lrscore
				if(!sortOrder.equals("tLastName") && !flagOhio8)
				{
					List<TeacherNormScore> teacherNormScoreList = teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetailAll);
					Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
					for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
						normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
					}
					Map<Integer,TeacherAchievementScore> mapAScore = null;

					if(entityID!=1 && achievementScore)
					{
						JobOrder jo=new JobOrder();
						jo.setDistrictMaster(districtMaster);
						List<TeacherAchievementScore> lstTeacherAchievementScore=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetailAll,jo);

						mapAScore = new HashMap<Integer, TeacherAchievementScore>();

						for(TeacherAchievementScore tAScore : lstTeacherAchievementScore){
							mapAScore.put(tAScore.getTeacherDetail().getTeacherId(),tAScore);
						}
					}
					TeacherAchievementScore tAScore=null;

					TeacherNormScore teacherNormScore = null;
					for(TeacherDetail teacherDetail : lstTeacherDetailAll)
					{
						teacherNormScore = normMap.get(teacherDetail.getTeacherId());
						if(teacherNormScore==null)
							teacherDetail.setNormScore(-0);
						else
							teacherDetail.setNormScore(teacherNormScore.getTeacherNormScore());

						if(entityID!=1 && achievementScore)
						{
							tAScore=mapAScore.get(teacherDetail.getTeacherId());
							if(tAScore!=null){
								if(tAScore.getAcademicAchievementScore()!=null)
									teacherDetail.setaScore(tAScore.getAcademicAchievementScore());

								if(tAScore.getLeadershipAchievementScore()!=null)
									teacherDetail.setlRScore(tAScore.getLeadershipAchievementScore());
							}else{
								teacherDetail.setaScore(-1);
								teacherDetail.setlRScore(-1);
							}
						}
					}

					if(sortOrderFieldName.equals("normScore") && sortOrderType.equals("0")){
						Collections.sort(lstTeacherDetailAll,TeacherDetail.teacherDetailComparatorNormScoreDesc );
					}else if(sortOrderFieldName.equals("normScore") && sortOrderType.equals("1")){
						Collections.sort(lstTeacherDetailAll,TeacherDetail.teacherDetailComparatorNormScore);
					}else if(sortOrderFieldName.equals("aScore") && sortOrderType.equals("1")){				
						Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorAScore );
					}else if(sortOrderFieldName.equals("aScore") && sortOrderType.equals("0")){				
						Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorAScoreDesc );				
					}else if(sortOrderFieldName.equals("lRScore") && sortOrderType.equals("1")){				
						Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorLRScore );
					}else if(sortOrderFieldName.equals("lRScore") && sortOrderType.equals("0")){				
						Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorLRScoreDesc );				
					}

					lstTeacherDetail=lstTeacherDetailAll.subList(start,end);
				}
				else
				{
					if(sortOrder.equals("tLastName")){
						for(TeacherDetail td:lstTeacherDetailAll)
						{
								td.settLastName(td.getLastName());
						}
						
						if(sortOrderType.equals("0"))
							Collections.sort(lstTeacherDetailAll,new TeacherDetailComparatorLastNameAsc());
						else
							Collections.sort(lstTeacherDetailAll,new TeacherDetailComparatorLastNameDesc());
					}
				
					lstTeacherDetail=lstTeacherDetailAll.subList(start,end);
				}



				List<Integer> lstStatusMasters = new ArrayList<Integer>();
				String[] statuss = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
				try{
					lstStatusMasters = statusMasterDAO.findStatusIdsByStatusByShortNames(statuss);
				}catch (Exception e) {
					e.printStackTrace();
				}
				if(totalRecord!=0){
					lstJobForTeacher=jobForTeacherDAO.getJobForTeacherByDSTeachersList(lstStatusMasters,lstTeacherDetail,lstJobOrderSLList,null,null,districtMaster,entityID);
					for(JobForTeacher jobForTeacher : lstJobForTeacher){
						jftMap.put(jobForTeacher.getJobForTeacherId(),jobForTeacher);
					}
					lstJobForTeacher=jobForTeacherDAO.getJobForTeacherByDSTeachersList(lstStatusMasters,lstTeacherDetail,lstJobOrderSLList,null,null,districtMaster,1);
					for(JobForTeacher jobForTeacher : lstJobForTeacher){
						jftMapForTm.put(jobForTeacher.getJobForTeacherId(),jobForTeacher);
					}
				}
			}
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			List<TeacherNormScore> teacherNormScoreList=teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetail);
			Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
			for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
				normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
			}

			Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();	
			Map<Integer,Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
			List<TeacherExperience> lstTeacherExperience=teacherExperienceDAO.findExperienceForTeachers(lstTeacherDetail);		

			// ************** Start ... IDENTIFY Internal Candidate *********************
			Map<Integer,Boolean> mapInternalCandidate = new HashMap<Integer, Boolean>();
			if(entityID!=1)
			{
				List<JobForTeacher> lstjobForTeachers=jobForTeacherDAO.findInternalCandidate(lstTeacherDetail);
				if(entityID==2)
				{
					for(JobForTeacher pojo:lstjobForTeachers){
						if(districtMaster.equals(pojo.getJobId().getDistrictMaster()))
						{
							mapInternalCandidate.put(pojo.getTeacherId().getTeacherId(), true);
						}
					}
				}
				else if(entityID==3)
				{
					Map<Integer,Boolean> mapJobOrder = new HashMap<Integer, Boolean>();
					List<JobOrder> lstJobOrders=new ArrayList<JobOrder>();
					for(JobForTeacher pojo:lstjobForTeachers){
						if(districtMaster.equals(pojo.getJobId().getDistrictMaster()))
							lstJobOrders.add(pojo.getJobId());
					}
					List<SchoolInJobOrder> lstSchoolInJobOrders= new ArrayList<SchoolInJobOrder>();
					if(schoolMaster!=null)
					 lstSchoolInJobOrders=schoolInJobOrderDAO.findInternalCandidate(lstJobOrders, schoolMaster);
					
					if(lstSchoolInJobOrders.size()>0){
						for(SchoolInJobOrder pojo:lstSchoolInJobOrders)
							mapJobOrder.put(pojo.getJobId().getJobId(), true);
					}

					for(JobForTeacher pojo:lstjobForTeachers){
						if(mapJobOrder.get(pojo.getJobId().getJobId())!=null && mapJobOrder.get(pojo.getJobId().getJobId()).equals(true)){
							mapInternalCandidate.put(pojo.getTeacherId().getTeacherId(), true);
						}
					}
				}
			}
			// ************** End ... IDENTIFY Internal Candidate *********************

			List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
			Integer isBase = null;
			try{
				if(lstTeacherDetail!=null && lstTeacherDetail.size()>0){
					teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(lstTeacherDetail);
					for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
						baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
						boolean cgflag = teacherAssessmentStatus2.getCgUpdated()==null?false:teacherAssessmentStatus2.getCgUpdated();
						if(cgflag && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
						{
							isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
						}
						else
						{
							if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
							{
								isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 2);
							}
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			    //alok kumar			
				Map<Integer,List<Integer>> jsiCount	=	new HashMap<Integer, List<Integer>>();
				Map<Integer,List<Integer>> epiCount	=	new HashMap<Integer, List<Integer>>();
				
				List<TeacherAssessmentStatus> teacherAssessmentJsi = null;
				List<TeacherAssessmentStatus> teacherAssessmentEpi = null;
				List<Integer> teacherId = new ArrayList();
				List<Integer> matchTeacherId = null;
				try
				{
					if(lstTeacherDetail!=null && lstTeacherDetail.size()>0)
					{

						for(TeacherDetail t:lstTeacherDetail)
						{
							teacherId.add(t.getTeacherId());
						}	
						teacherAssessmentJsi = teacherAssessmentStatusDAO.findAssessmentJsiByTeachers(lstTeacherDetail);
						teacherAssessmentEpi=teacherAssessmentStatusDAO.findAssessmentEpiByTeachers(lstTeacherDetail);						
						if(teacherAssessmentJsi.size()>0)
						{
							for(Integer id :teacherId)
							{
								matchTeacherId=new ArrayList();
								for(TeacherAssessmentStatus assessmentteacherId:teacherAssessmentJsi)
								{								  
								  if(id.equals(assessmentteacherId.getTeacherDetail().getTeacherId()))
								  {
									  matchTeacherId.add(assessmentteacherId.getTeacherDetail().getTeacherId());
								  }
								}
								if(matchTeacherId.size()>0)
								jsiCount.put(id, matchTeacherId);							
							}
						}
						if(teacherAssessmentEpi.size()>0)
						{
							for(Integer id :teacherId)
							{
								matchTeacherId=new ArrayList();
								for(TeacherAssessmentStatus assessmentteacherId:teacherAssessmentEpi)
								{
								  if(id.equals(assessmentteacherId.getTeacherDetail().getTeacherId()))
								  {
									  matchTeacherId.add(assessmentteacherId.getTeacherDetail().getTeacherId());
								  }
								}
								if(matchTeacherId.size()>0)
								epiCount.put(id, matchTeacherId);							
							}
						}
					}				
				}			
				catch(Exception e)
				{e.printStackTrace();}
				
			
			String tFname="",tLname="";
			tmRecords.append("<table id='teacherTable' width='100%' class='tablecgtbl'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");

			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblNm,sortOrderFieldName,"tLastName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			String schoolDistrict="District";
			if(entityID==3){
				schoolDistrict="School";
			}

			//tmRecords.append("<th width='60' valign='top'>Norm Score</th>");
			if(!flagOhio8){
			responseText=PaginationAndSorting.responseSortingLink(lblNormScore,sortOrderFieldName,"normScore",sortOrderTypeVal,pgNo);
			
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			}
			if(flagOhio8)
			tmRecords.append("<th width='100' valign='top'># of Jobs&nbsp;<a data-original-title='X/Y, X = msgNumOfJobAppiedAt "+schoolDistrict+" / Y = msgNumOfJobAppiedAt2"+schoolDistrict+"(s)' class='net-header-text ' rel='tooltip' id='jobAppliedTool' href='javascript:void(0);' ><span class='icon-question-sign '></span></a></th>");
			else
			tmRecords.append("<th width='50' valign='top'># of Jobs&nbsp;<a data-original-title='X/Y, X = msgNumOfJobAppiedAt "+schoolDistrict+" / Y = msgNumOfJobAppiedAt2"+schoolDistrict+"(s)' class='net-header-text ' rel='tooltip' id='jobAppliedTool' href='javascript:void(0);' ><span class='icon-question-sign '></span></a></th>");
			tmRecords.append("<script type='text/javascript'>$('#jobAppliedTool').tooltip();</script>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			List<TeacherPersonalInfo> lstTeacherPersonalInfo = new ArrayList<TeacherPersonalInfo>();
			List<TeacherAchievementScore> lstTeacherAchievementScore = new ArrayList<TeacherAchievementScore>();			
			List<DistrictSpecificQuestions> lstdistrictSpecificQuestions = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(userMaster.getDistrictId());
			boolean schoolCheckFlag=false;
			if(userMaster.getEntityType()==3)
			{
				try
				{
					schoolMaster = schoolMasterDAO.findById(userMaster.getSchoolId().getSchoolId(), false, false);
					DistrictSchools districtSchools = districtSchoolsDAO.findBySchoolMaster(schoolMaster);
					if(districtSchools.getDistrictMaster().getResetQualificationIssuesPrivilegeToSchool()==true)
					{
						schoolCheckFlag = true;
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			int counter=0;
			TeacherNormScore teacherNormScore = null;
			
			
			List<Integer>teacherIdLst=new ArrayList<Integer>();
			for (TeacherDetail teacherDetail : lstTeacherDetail) 
			{
				teacherIdLst.add(teacherDetail.getTeacherId());
			}
			Criterion criterionTeacherPersonalInfo=Restrictions.in("teacherId", teacherIdLst);
			List<TeacherPersonalInfo>teacherPersonalInfos=null;
			HashMap<Integer, TeacherPersonalInfo> map=new HashMap<Integer, TeacherPersonalInfo>();
			if(teacherIdLst!=null && teacherIdLst.size()>0){
				teacherPersonalInfos=teacherPersonalInfoDAO.findByCriteria(criterionTeacherPersonalInfo);
				for (TeacherPersonalInfo pojo : teacherPersonalInfos) 
				{
					map.put(pojo.getTeacherId(), pojo);
				}
			}	
			//alok
			Map<Integer, EmployeeMaster> empMap = new HashMap<Integer, EmployeeMaster>();
			Map<String,Boolean> mapAlum = new HashMap<String, Boolean>(); // For Alum
			if(entityID!=1)
			{
				String ssn="";				
				Map<Integer, TeacherPersonalInfo>tpInfomap=new  HashMap<Integer, TeacherPersonalInfo>();
				List<String> teacherFNameList = new ArrayList<String>();
				List<String> teacherLNameList = new ArrayList<String>();
				List<String> teacherSSNList = new ArrayList<String>();
				if(teacherPersonalInfos!=null && teacherPersonalInfos.size()>0){				
					for(TeacherPersonalInfo pojo :teacherPersonalInfos) {
						tpInfomap.put(pojo.getTeacherId(), pojo); 
						ssn = pojo.getSSN()==null?"":pojo.getSSN().trim();
						if(!ssn.equals(""))
						{
							teacherFNameList.add(pojo.getFirstName().trim());
							teacherLNameList.add(pojo.getLastName().trim());
							ssn = ssn.trim();
							try {
								ssn = Utility.decodeBase64(ssn);
							} catch (IOException e) {
								e.printStackTrace();
							}
							if(ssn.length()>4)
								ssn = ssn.substring(ssn.length() - 4, ssn.length());
							teacherSSNList.add(ssn.trim());
						}
					}
				}
				List<EmployeeMaster> employeeMasters =	employeeMasterDAO.checkEmployeeByFistName(teacherFNameList,teacherLNameList,teacherSSNList,districtMaster);
				mapAlum=cgService.getMapAlum(employeeMasters, districtMaster); // get Alum Map
				
				if(teacherPersonalInfos!=null && teacherPersonalInfos.size()>0 && employeeMasters.size()>0){				
					for(TeacherPersonalInfo pojo :teacherPersonalInfos) {
						ssn = pojo.getSSN()==null?"":pojo.getSSN().trim();
						if(!ssn.equals(""))
						{
							ssn = ssn.trim();
							try {
								ssn = Utility.decodeBase64(ssn);
							} catch (IOException e) {
								e.printStackTrace();
							}
							if(ssn.length()>4)
								ssn = ssn.substring(ssn.length() - 4, ssn.length());
							String empNo = pojo.getEmployeeNumber()==null?"":pojo.getEmployeeNumber().trim();
							
							for(EmployeeMaster emp :employeeMasters) {
								if(empNo.equals(""))// Not an employee
								{
									if(emp.getSSN().equalsIgnoreCase(ssn) && emp.getFirstName().equalsIgnoreCase(pojo.getFirstName()) && emp.getLastName().equalsIgnoreCase(pojo.getLastName()))
									{
										empMap.put(pojo.getTeacherId(), emp);
										break;
									}
								}
								else 
								{
									if(emp.getEmployeeCode().equalsIgnoreCase(empNo) && emp.getSSN().equalsIgnoreCase(ssn) && emp.getFirstName().equalsIgnoreCase(pojo.getFirstName()) && emp.getLastName().equalsIgnoreCase(pojo.getLastName()))
									{
										empMap.put(pojo.getTeacherId(), emp);
										break;
									}
								}
							}
						}
					}
				}
			}			
			//Gourav  districtMaster
			List<SecondaryStatusMaster> lstSecondaryStatusMaster = secondaryStatusMasterDAO.findTagswithDistrictInCandidatePool(districtMaster);
			Criterion district_cri = Restrictions.eq("districtMaster", districtMaster);
			Criterion school_cri = Restrictions.isNull("schoolMaster");
			Criterion job_cri = Restrictions.isNull("jobOrder");
			
			List<TeacherSecondaryStatus> lstMltTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByCriteria(district_cri,school_cri,job_cri);
			Map<String, TeacherSecondaryStatus> teachersTagsExistMap = new HashMap<String, TeacherSecondaryStatus>();
			if(lstMltTeacherSecondaryStatus.size()>0){
				for (TeacherSecondaryStatus teacherSecondaryStatus : lstMltTeacherSecondaryStatus) {
					String teacherTagKey = teacherSecondaryStatus.getTeacherDetail().getTeacherId()+"##"+teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusId();
					teachersTagsExistMap.put(teacherTagKey, teacherSecondaryStatus);
				}
			}
			
			if(lstTeacherDetail.size()==0)
				tmRecords.append("<tr><td colspan='9'>No Teacher Found</td></tr>" );
			else
			{
				if(entityID!=1)
				{
					JobOrder jobOrder = new JobOrder();
					jobOrder.setDistrictMaster(districtMaster);
					lstTeacherAchievementScore=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetail,jobOrder);
				}
				lstTeacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPInfoForTeacher(lstTeacherDetail);
			}
			Map<Integer,Boolean> qqTeachersMap = new HashMap<Integer, Boolean>(); 
			if(lstTeacherDetail.size()>0 && entityTypeId!=1 && lstdistrictSpecificQuestions.size()>0)
			{
				List<TeacherDetail> qqAttemptedTeachers =  teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findQQStatusByTeacher(lstTeacherDetail,districtMaster,true);
				List<TeacherDetail> invalidQQTeachers = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findQQStatusByTeacher(lstTeacherDetail,districtMaster,false);
				for (TeacherDetail teacherDetail : qqAttemptedTeachers) {
					qqTeachersMap.put(teacherDetail.getTeacherId(), true);
				}
				for (TeacherDetail teacherDetail : invalidQQTeachers) {
					qqTeachersMap.put(teacherDetail.getTeacherId(), false);
				}
			}

			// teacherwiseassessmenttime
			Map<Integer, TeacherWiseAssessmentTime> teacherWMap = new HashMap<Integer, TeacherWiseAssessmentTime>();
			if(lstTeacherDetail.size()>0){
				Calendar cal = Calendar.getInstance();

				cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
				Date sDate=cal.getTime();
				cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
				Date eDate=cal.getTime();

				Criterion teacherListCR = Restrictions.in("teacherDetail", lstTeacherDetail);
				Criterion assessmenType		= Restrictions.eq("assessmentType", 1);
				Criterion currentYear = Restrictions.between("createdDateTime", sDate, eDate);
				
				List<TeacherWiseAssessmentTime> teacherWiseAssessmentTime = teacherWiseAssessmentTimeDAO.findByCriteria(Order.desc("assessmentTimeId"),teacherListCR,assessmenType);
				
				if(teacherWiseAssessmentTime.size()>0){
					for (TeacherWiseAssessmentTime teacherWiseAssessmentTime2 : teacherWiseAssessmentTime) {
						if(!teacherWMap.containsKey(teacherWiseAssessmentTime2.getTeacherDetail().getTeacherId())){
							teacherWMap.put(teacherWiseAssessmentTime2.getTeacherDetail().getTeacherId(), teacherWiseAssessmentTime2);
						}
					}
				}
			}
			
			String sCheckBoxValue="";
			Map<Integer,Boolean> finalizedTMap = new HashMap<Integer, Boolean>();
			List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
			if(districtMaster!=null && lstTeacherDetail.size()>0)
			{
				lstTeacher = jobForTeacherDAO.getQQFinalizedTeacherList(lstTeacherDetail,districtMaster);
				for (TeacherDetail teacherDetail : lstTeacher) {
					finalizedTMap.put(teacherDetail.getTeacherId(), true);
				}
			}
			////////////////////////////////Add disable profile( Sekhar )///////////////////////////////
			Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
			System.out.println("=============================="+lstTeacherDetail.size());
			List<TeacherStatusHistoryForJob> historyForJobList = teacherStatusHistoryForJobDAO.findHireTeachersWithDistrictListAndTeacherList(lstTeacherDetail,districtMaster);
			List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherForOffers(lstTeacherDetail);
			List<String> posList=new ArrayList<String>();
			Map<String,String> reqMap=new HashMap<String, String>();
			for (JobForTeacher jobForTeacher : jftOffers){
				try{
					if(jobForTeacher.getRequisitionNumber()!=null){
						posList.add(jobForTeacher.getRequisitionNumber());
						reqMap.put(jobForTeacher.getJobId().getJobId()+"#"+jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getRequisitionNumber());
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(districtMaster,posList);
			Map<String,DistrictRequisitionNumbers> dReqNoMap = new HashMap<String,DistrictRequisitionNumbers>();
			try{
				for (DistrictRequisitionNumbers districtRequisitionNumbers2 : districtRequisitionNumbers) {
					dReqNoMap.put(districtRequisitionNumbers2.getRequisitionNumber(),districtRequisitionNumbers2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(historyForJobList.size()>0){
				mapForHiredTeachers=cgService.getHiredList(reqMap,dReqNoMap,lstTeacherDetail,historyForJobList,null);
			}

			
			/////////////////////////////////////End disable profile //////////////////////////////////////
			if(lstTeacherDetail!=null && lstTeacherDetail.size()>0)
			{
				System.out.println(" Teachers List Get ");
			for (TeacherDetail teacherDetail : lstTeacherDetail) 
			{
				teacherNormScore = normMap.get(teacherDetail.getTeacherId());
				int normScore = 0;
				String colorName = "";
				if(teacherNormScore!=null && !flagOhio8)
				{
					normScore = teacherNormScore.getTeacherNormScore();
					//percentile = teacherNormScore.getPercentile();
					colorName = teacherNormScore.getDecileColor();
				}
				//int normScore=getNormScore(teacherNormScoreList,teacherDetail);
				//double percentile=getPercentile(teacherNormScoreList,teacherDetail);
				//String colorName =cGReportService.getColorName(percentile);
				noOfRecordCheck++;
				tFname=teacherDetail.getFirstName();
				tLname=teacherDetail.getLastName();
				counter++;
				String gridColor="class='bggrid'";
				if(counter%2==0){
					gridColor="style='background-color:white;'";
				}

				tmRecords.append("<tr "+gridColor+">");
				//tmRecords.append("<TD width='240'><a class='profile"+noOfRecordCheck+"' data-placement='above' href='javascript:void(0);' id='pro"+teacherDetail.getTeacherId()+"'>"+tFname+" "+tLname+"</a></BR>");

				
					tmRecords.append("<TD width='240'>");
				
				// ************* Start ... Add for Internal Candidate ICON
				/*if(entityID!=1)
				{
					sCheckBoxValue="<input type=\"checkbox\"  name='chkMassTP'  id='chkMassTP"+teacherDetail.getTeacherId()+"' onclick=\"actionMenuTP();\"   value='"+teacherDetail.getTeacherId()+"'>&nbsp;";
				}
				else
					sCheckBoxValue="";*/
				// ************* End ... Add for Internal Candidate ICON
				
				
				boolean showProfile=true;
				
				try{
					if(entityID==3 && isMiami){
						if(mapForHiredTeachers.get(teacherDetail.getTeacherId())!=null && mapForHiredTeachers.get(teacherDetail.getTeacherId()).size()>0){
							showProfile=false;
						}
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
				String internalIcon = "";
				if(entityID!=1 && !flagOhio8)
				{
					if(mapInternalCandidate.get(teacherDetail.getTeacherId())!=null && mapInternalCandidate.get(teacherDetail.getTeacherId()))
						internalIcon="<a data-original-title='Internal Candidate' rel='tooltip' id='tpInternalCandidate"+noOfRecordCheck+"'  ><span class='fa-rocket icon-large internalCandidateIconColor'></span></a> ";
						//tmRecords.append("<a data-original-title='Internal Candidate' rel='tooltip' id='tpInternalCandidate"+noOfRecordCheck+"'  ><span class='fa-rocket icon-large internalCandidateIconColor'></span></a> ");
				}
				if(showProfile){
					//<a class='profile' href='javascript:void(0);' onclick=\"showProfileContentForTeacher(this,"+teacherDetail.getTeacherId()+","+noOfRecordCheck+",'Teacher Pool',event)\">
					tmRecords.append(sCheckBoxValue+internalIcon+""+tFname+" "+tLname+"</a>");
				}else{
					tmRecords.append(sCheckBoxValue+tFname+" "+tLname);
				}
				
				//gourav  tag loop start
				
				String cssheader="style='padding-left: 14px;padding-top: 15px; padding-bottom: 15px;'";
				String csscoloumns="style='padding-left: 14px;'";
				
				StringBuffer tagContent =	new StringBuffer();
				tagContent.append("<div>");
				int k3=0;
				tagContent.append("<table border=\"0\" class=\"tablecgtbl\">");	
				//shriram 11111
				if(!flagOhio8)
				{
				for(SecondaryStatusMaster secondaryStatusMaster : lstSecondaryStatusMaster){
					
					if(lstSecondaryStatusMaster.size()>15)
					{
						if (k3 % 3 == 0 && k3!= 0) {
							tagContent.append("</tr><tr>");
						}
						k3++;						
						//sb.append("<td width='20px' "+csscoloumns+" >");			
						/*if(teachersTagsExistMap.containsKey(teacherDetail.getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId())){
							tagContent.append("<td width=\"30px\" style=\"padding-left:14px;line-height:0px;\"><input type=\"checkbox\" checked=\"checked\"  name=\"tagsValue"+teacherDetail.getTeacherId()+"\" id=\"tagsValue\" value="+secondaryStatusMaster.getSecStatusId()+"></td>");
						}else{
							tagContent.append("<td width=\"30px\" style=\"padding-left:14px;line-height:0px;\"><input type=\"checkbox\" name=\"tagsValue"+teacherDetail.getTeacherId()+"\" id=\"tagsValue\" value="+secondaryStatusMaster.getSecStatusId()+"></td>");
						}*/
						//tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\" class=\"tdAct\">"+secondaryStatusMaster.getSecStatusName()+"</td>");
						
						if(secondaryStatusMaster.getPathOfTagsIcon()!=null){
							tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\"><img src=\""+showTagIconFile(secondaryStatusMaster)+"\" width=\"20px\" height=\"20px\">&nbsp;&nbsp;"+secondaryStatusMaster.getSecStatusName()+"</td>");
							
						}else{
							tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\" class=\"tdAct\">"+secondaryStatusMaster.getSecStatusName()+"</td>");
						}				
						
					}else{
					
						tagContent.append("<tr>");					
						if(teachersTagsExistMap.containsKey(teacherDetail.getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId())){
							tagContent.append("<td width=\"10%\" style=\"padding-left:14px;line-height:0px;\"><input type=\"checkbox\" checked=\"checked\"  name=\"tagsValue"+teacherDetail.getTeacherId()+"\" id=\"tagsValue\" value="+secondaryStatusMaster.getSecStatusId()+"></td>");
						}else{
							tagContent.append("<td width=\"10%\" style=\"padding-left:14px;line-height:0px;\"><input type=\"checkbox\" name=\"tagsValue"+teacherDetail.getTeacherId()+"\" id=\"tagsValue\" value="+secondaryStatusMaster.getSecStatusId()+"></td>");
						}
						//tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\" class=\"tdAct\">"+secondaryStatusMaster.getSecStatusName()+"</td>");
						
						if(secondaryStatusMaster.getPathOfTagsIcon()!=null){
							tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\"><img src=\""+showTagIconFile(secondaryStatusMaster)+"\" width=\"20px\" height=\"20px\">&nbsp;&nbsp;"+secondaryStatusMaster.getSecStatusName()+"</td>");
							
						}else{
							tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\" class=\"tdAct\">"+secondaryStatusMaster.getSecStatusName()+"</td>");
						}
						tagContent.append("</tr>");
					}
				}
				}
				
				tagContent.append("</table>");
				tagContent.append("</div>");
				tagContent.append("<div class=\"modal-footer\">");
				tagContent.append("<span><button  class=\"btn btn-large btn-primary\" onclick=\"saveTags("+teacherDetail.getTeacherId()+");\"  >"+btnSave+" <i class=\"icon\"></i></button>  <button id=\"closeTag"+noOfRecordCheck+"\" onclick=\"closeActAction("+noOfRecordCheck+");\" class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"  >"+btnClose+"</button></span>");						 
				tagContent.append("</div>");
				
				StringBuffer tagNames =	new StringBuffer();
				//SHRIRAM 2222
				for(SecondaryStatusMaster secondaryStatusMaster : lstSecondaryStatusMaster){
					if(teachersTagsExistMap.containsKey(teacherDetail.getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId())){						
						String tagName = teachersTagsExistMap.get(teacherDetail.getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId()).getSecondaryStatusMaster().getSecStatusName();						
						if(teachersTagsExistMap.get(teacherDetail.getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId()).getSecondaryStatusMaster().getPathOfTagsIcon()!=null){
							tagNames.append(" <img src='"+showTagIconFile(secondaryStatusMaster)+"' width=\"20px\" height=\"20px\">");
						}else{
							tagNames.append(" <span class=\"txtbgroundTagStatusCPool\" width=\"5%\">"+tagName+"</span>");
						}
					}
				}
				String tagString = tagContent.toString();
				if(roleId != 1){
					if(roleId != 3){
						
						
						
					tmRecords.append(" <a id='tagPreview"+noOfRecordCheck+"' class=\"allPopOver\" href='javascript:void(0);' rel='popover' data-content='"+tagContent.toString()+"'");
					//tmRecords.append(tagContent.toString());
					tmRecords.append(" data-html='true' onmouseover=\"closeProfileOpenTags("+noOfRecordCheck+");\">");
					tmRecords.append(tagNames.toString());
					tmRecords.append("</a>");
					
					tmRecords.append("<script type='text/javascript'>$('#tagPreview"+noOfRecordCheck+"').popover();</script>");
					}else{
						tmRecords.append(tagNames.toString());
					}
				}
				tmRecords.append("<br/>");
				//Add profile hover

				String profileNormScore="";
				if(normScore!=0){
					profileNormScore=normScore+"";
				}else{
					profileNormScore="N/A";
				}
				
			/* @Start
			 * @Ashish Kumar
			 * @Description :: Qualification Details Thumb 
			 * */
				
				//<a href='#' onclick=\"getMessageDiv('"+teacherDetail.getTeacherId()+"','"+teacherDetail.getEmailAddress()+"',0,0);\" >
				// NEED
				tmRecords.append(""+teacherDetail.getEmailAddress()+"</a>");
				
				TeacherPersonalInfo tempTeacher=map.get(teacherDetail.getTeacherId());
				if(tempTeacher!=null && tempTeacher.getIsVateran()!=null && tempTeacher.getIsVateran()==1 && !flagOhio8)
				{	
					tmRecords.append("<a data-original-title='Veteran Candidate' rel='tooltip' id='teacher_"+teacherDetail.getTeacherId()+"' href='javascript:void(0);'>&nbsp;&nbsp;<img height='20px' width='20' src=\"images/Veteran.jpg\"></a>");
					tmRecords.append("<script type='text/javascript'>$('#teacher_"+teacherDetail.getTeacherId()+"').tooltip();</script>");
				}
				
				boolean CheckForIssue = false; 
				
				if(userMaster.getEntityType()==2)
				{
					CheckForIssue = true;
				}
				
				if(schoolCheckFlag)
					CheckForIssue = true;
				
				if(CheckForIssue == true && !flagOhio8)
				{
					if(lstdistrictSpecificQuestions.size()>0)
					{
						Boolean qqTaken = qqTeachersMap.get(teacherDetail.getTeacherId());
						if(qqTaken!=null)
						{
							Boolean finalizeQQ = finalizedTMap.get(teacherDetail.getTeacherId());
							finalizeQQ = finalizeQQ==null?false:finalizeQQ;
							
							/*if(qqTaken || finalizeQQ)
								tmRecords.append(" &nbsp;<a data-original-title='Qualification Issues' rel='tooltip' id='tpInv"+noOfRecordCheck+"' href='javascript:void(0);'><span class='icon-thumbs-up icon-large thumbUpIconColor' onclick='showQualificationDivForTP("+teacherDetail.getTeacherId()+","+districtId+");'></span></a>");
							else 
								tmRecords.append(" &nbsp;<a data-original-title='Qualification Issues' rel='tooltip' id='tpInv"+noOfRecordCheck+"' href='javascript:void(0);'><span class='fa-thumbs-down icon-large iconcolorRed' onclick='showQualificationDivForTP("+teacherDetail.getTeacherId()+","+districtId+");'></span></a>");
						*/}
							
					}
				}
				//System.out.println("entityID:::::::::::::::: "+entityID);
				if(entityID!=1 && !flagOhio8)
				{

					EmployeeMaster employeeMaster = empMap.get(teacherDetail.getTeacherId());
					if(employeeMaster!=null)
					{ 
						System.out.println("value "+employeeMaster.getEmailAddress()+"ssn="+employeeMaster.getSSN()+"rrcode==="+employeeMaster.getRRCode()+"firsr name="+employeeMaster.getFirstName());
						if(entityID==2 && employeeMaster.getOCCode()!=null && employeeMaster.getOCCode().equalsIgnoreCase("y"))
						{
							tmRecords.append("<a data-original-title='OC' rel='tooltip' id='oc"+noOfRecordCheck+"' href='javascript:void(0);'>&nbsp;<img  src=\"images/O.png\"></div></a> ");
							tmRecords.append("<script type='text/javascript'>$('#oc"+noOfRecordCheck+"').tooltip();</script>");
						}
						
						if(entityID==2 && employeeMaster.getRRCode()!=null && employeeMaster.getRRCode().equalsIgnoreCase("y"))
						{					
							tmRecords.append("<a data-original-title='RR' rel='tooltip' id='tprr"+noOfRecordCheck+"' href='javascript:void(0);'>&nbsp;<img  src=\"images/R-22.png\"></a> ");
							tmRecords.append("<script type='text/javascript'>$('#tprr"+noOfRecordCheck+"').tooltip();</script>");
						}
					}
				}
				
				// Alum print
				try 
				{
					tmRecords.append(cgService.getAlum(tempTeacher, mapAlum, noOfRecordCheck));
				} catch (Exception e) {}
				
				tmRecords.append("</td>");				
				int jobList=jobListByTeacher(teacherDetail.getTeacherId(),jftMap);
				int jobListForTm=jobListByTeacher(teacherDetail.getTeacherId(),jftMapForTm);
				if(!flagOhio8)
				{
				if(normScore==0)
				{
					
						tmRecords.append("<td>"+optNA+"</td>");
										
				}
				else
				{
					String ccsName="";
					String normScoreLen=normScore+"";
					if(normScoreLen.length()==1){
						ccsName="nobground1";
					}else if(normScoreLen.length()==2){
						ccsName="nobground2";
					}else{
						ccsName="nobground3";
					}
					
						tmRecords.append("<td><span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>"+normScore+"</span></td>");
					
				
				}
				}
				////////////// A score ////////////////////
				if(entityID!=1 && achievementScore && !flagOhio8){
					String aScore =cgService.getAchievementMaxScore(lstTeacherAchievementScore,teacherDetail,"A");
					if(aScore!=null){
						tmRecords.append("<td id='teacherPoolaScore_"+teacherDetail.getTeacherId()+"'>"+aScore+"</td>"); //@AShish Added ascore ID
					}else{
						tmRecords.append("<td id='teacherPoolaScore_"+teacherDetail.getTeacherId()+"'>"+optNA+"</td>"); //@AShish Added ascore ID
					}
					String lScore =cgService.getAchievementMaxScore(lstTeacherAchievementScore,teacherDetail,"L");
					if(lScore!=null){
						tmRecords.append("<td id='teacherPoollrScore_"+teacherDetail.getTeacherId()+"'>"+lScore+"</td>"); //@AShish Added lrscore ID
					}else{
						tmRecords.append("<td id='teacherPoollrScore_"+teacherDetail.getTeacherId()+"'>"+optNA+"</td>"); //@AShish Added lrscore ID
					}
				}
				//////////////////////////// TFA //////////////////////////////
				if(tFA && !flagOhio8){
					String tFAVal =cgService.getTFA(lstTeacherExperience,teacherDetail);
					if(tFAVal!=null){
						tmRecords.append("<td>"+tFAVal+"</td>");
					}else{
						tmRecords.append("<td>"+optNA+"</td>");
					}
				}
				if(!flagOhio8)
				{
				tmRecords.append("<td>");
				if(jobList!=0){                                                                                                   //mukesh
					tmRecords.append("<a href='javascript:void(0);'  onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" >"+jobList+"</a>");
				}else{
					tmRecords.append("0");
				}
				}
				else
				{
					tmRecords.append("<td style='float: right;margin-right: 38px;'>");
					if(jobList!=0){                                                                                                   //mukesh
						tmRecords.append("<a href='javascript:void(0);'  onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" >"+jobList+"</a>");
					}else{
						tmRecords.append("0");
					}
				}
					
				tmRecords.append("/"+(jobListForTm-jobList)+"</td>");

				if(teachingOfYear && !flagOhio8)
				{
					tmRecords.append("<td>");
					if(cgService.getTEYear(lstTeacherExperience,teacherDetail)!=0){
						tmRecords.append(cgService.getTEYear(lstTeacherExperience,teacherDetail));
					}else{
						tmRecords.append("+optNA+");
					}
					tmRecords.append("</td>");
				}

				if(expectedSalary && !flagOhio8){
					tmRecords.append("<td>");
					if(cgService.getSalary(lstTeacherPersonalInfo,teacherDetail)!=-1){
						tmRecords.append("$"+cgService.getSalary(lstTeacherPersonalInfo,teacherDetail));
					}else{
						tmRecords.append("+optNA+");
					}
					tmRecords.append("</td>");
				}

				if(entityID==1){

					tmRecords.append("<td>");
					if(roleAccess.indexOf("|7|")!=-1 && userMaster.getEntityType()==1){
						if(teacherDetail.getStatus().equalsIgnoreCase("A"))
							tmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateTeacher("+teacherDetail.getTeacherId()+",'I')\">Deactivate");
						else
							tmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateTeacher("+teacherDetail.getTeacherId()+",'A')\">Activate");
					}else{
						if(teacherDetail.getStatus().equals("A"))
							tmRecords.append("Active");
						else
							tmRecords.append("Inactive");
					}
					tmRecords.append("</td>");
				}
				if(!flagOhio8){
				tmRecords.append("<td  style='vertical-align: top;padding:0px;'>");
				tmRecords.append("<table border=0 >");
				tmRecords.append("<tr "+gridColor+">");			
				String ccsName		=	"nobground1";
				String colorgray	=	"7F7F7F";	
				String colorblue	=	"007AB4";	
				if(entityID!=1 && isManage)
				{					
					
					tmRecords.append("<td  style='padding: 0px; padding-left:3px; margin: 0px; border: 0px;padding-top:10px;'>");					
				//	tmRecords.append("<a data-original-title='Apply/Add Candidate to Other Jobs' rel='tooltip' id='tpJobApplied"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"getJobDetailList('"+teacherDetail.getTeacherId()+"');getZoneListAll('"+districtId+"')\"><span class='fa-plus-square icon-large iconcolor'></span></a>");
					tmRecords.append("</td>");				
					
				}
				isBase = isbaseComplete.get(teacherDetail.getTeacherId());				
				if(entityID==1 && roleAccess.indexOf("|7|")!=-1 && userMaster.getEntityType()==1){
					
					try {
						String IconColor="";
						if(teacherWMap.containsKey(teacherDetail.getTeacherId())){
							IconColor=colorblue;
						}else{
							IconColor=colorgray;
						}
						tmRecords.append("<td  style='padding: 0px; padding-left:0px; margin: 0px; border: 0px;padding-top:5px;'>");
						tmRecords.append("<a data-original-title='"+lblEPIAccomodation+"' rel='tooltip' id='epiACC"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"openEpiTime('"+teacherDetail.getTeacherId()+"','"+tFname+" "+tLname+"','"+isBase+"')\" ><span class='icon-time icon-large' id='timeIcon"+teacherDetail.getTeacherId()+"' style='color: #"+IconColor+";'></span></a>");
						tmRecords.append("</td>");
					} catch (Exception e) {}
					try
					{
						tmRecords.append("<td  style='padding: 0px; padding-left:0px; margin: 0px; border: 0px;padding-top:5px;'>");
				        
						if(jsiCount.containsKey(teacherDetail.getTeacherId()) || epiCount.containsKey(teacherDetail.getTeacherId()))
				        {
				        	tmRecords.append("<a data-original-title='"+toolAssDetails+"' rel='tooltip' id='epiAndJsi"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"showEpiAndJsi("+teacherDetail.getTeacherId()+")\"><span class='fa-file-text icon-large' style='color: #"+colorblue+";'></span></a>");			        	
				    	}
				        else
				        {
				        	tmRecords.append("<a data-original-title='"+toolNoAssDetail+"' rel='tooltip' id='epiAndJsi"+noOfRecordCheck+"' href='javascript:void(0);'><span class='fa-file-text icon-large' style='color: #"+colorgray+";'></span></a>");
				        }
				        tmRecords.append("</td>");	
					}
					catch(Exception e)
					{e.printStackTrace();}
					
					tmRecords.append("<td style='padding: 0px; padding-left:0px; margin: 0px;padding-top:5px;'>");
					try
					{						
						if(isBase==2)
							tmRecords.append("<a data-original-title='"+toolResetEPI+"' rel='tooltip' id='tpReset"+noOfRecordCheck+"' href='javascript:void(0);' onclick=\"checkBaseViolatedForEPIReset('"+teacherDetail.getTeacherId()+"')\"><span class='icon-refresh  icon-large iconcolor'></span></a>");
						else if(isBase==1)
							tmRecords.append("<a data-original-title='"+sltEpiComp+"' rel='tooltip' id='tpReset"+noOfRecordCheck+"' ><span class='icon-refresh  icon-large iconcolorhover'></span></a>");
						else
							tmRecords.append("<a data-original-title='"+toolNoEPI+"' rel='tooltip' id='tpReset"+noOfRecordCheck+"'><span class='icon-refresh  icon-large iconcolorhover'></span></a>");
						
						tmRecords.append("</td>");
					
					}
					catch(NullPointerException e)
					{						
						tmRecords.append("<a data-original-title='"+toolNoEPI+"' rel='tooltip' id='tpReset"+noOfRecordCheck+"'><span class='icon-refresh  icon-large iconcolorhover'></span></a>");						
					}
					tmRecords.append("</td>");
					
					
				}
				
				tmRecords.append("<td width=5>&nbsp;");
				tmRecords.append("</td>");
				tmRecords.append("</tr>");
				
				tmRecords.append("</table>");
				tmRecords.append("</td>");
				}
				tmRecords.append("</tr>");
			}
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForJobboard(request,totalRecord,noOfRow, pageNo));
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
	
	
	public String showTagIconFile(SecondaryStatusMaster secondaryStatusMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String path="";
		String source="";
		String target="";
		try 
		{
			if(secondaryStatusMaster.getDistrictMaster()!=null){
				
				source = Utility.getValueOfPropByKey("districtRootPath")+secondaryStatusMaster.getDistrictMaster().getDistrictId()+"/tags/"+secondaryStatusMaster.getPathOfTagsIcon();
				target = context.getServletContext().getRealPath("/")+"/district/"+secondaryStatusMaster.getDistrictMaster().getDistrictId()+"/tags/";
			}else{
				source = Utility.getValueOfPropByKey("rootPath")+"tags/"+secondaryStatusMaster.getPathOfTagsIcon();
				target = context.getServletContext().getRealPath("/")+"/tags/";
			}
			
			
		    File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        FileUtils.copyFile(sourceFile, targetFile);
	        
	        String ext = null;
	        String s = sourceFile.getName();
	        int i = s.lastIndexOf('.');

	        if (i > 0 &&  i < s.length() - 1) {
	            ext = s.substring(i+1).toLowerCase();
	        }
	        
	        /*if(ext.equals("jpeg") || ext.equals("png") || ext.equals("gif") || ext.equals("jpg")){
	        	System.out.println("tag path >>>>>> "+targetDir+"/"+sourceFile.getName());
	        	ImageResize.resizeTag(targetDir+"/"+sourceFile.getName());
	        }*/
	        if(secondaryStatusMaster.getDistrictMaster()!=null){
	        		path = Utility.getValueOfPropByKey("contextBasePath")+"district/"+secondaryStatusMaster.getDistrictMaster().getDistrictId()+"/tags/"+secondaryStatusMaster.getPathOfTagsIcon();	
	        }else{
	        	path = Utility.getValueOfPropByKey("contextBasePath")+"/tags/"+secondaryStatusMaster.getPathOfTagsIcon();
	        }
	       	 
		}catch (Exception e){
			//e.printStackTrace();
		}		
		return path;
	}
	
	@Transactional(readOnly=false)
	public String questSignUpTemp(String districtId,String fname,String lname,String emailAddress,String password,int sumOfCaptchaText,int other)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession();
		String s="Server error";
		try
		{
		int authorizationkey=(int) Math.round(Math.random() * 2000000);
		System.out.println(districtId+"::"+fname+"::"+lname+"::"+fname+"::"+emailAddress+"::"+password+"::"+sumOfCaptchaText);
	
		TeacherDetail teacherDetail=new TeacherDetail();
		UserMaster userMaster=new UserMaster();
		List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
		List<UserMaster> lstUserMaster = userMasterDAO.findByEmail(emailAddress);
		if((lstTeacherDetails!=null && lstTeacherDetails.size()>0) || (lstUserMaster!=null && lstUserMaster.size()>0)){
			s= msgIfMemberAlreadyRegPlzProvideOtherEmail;			
			return s;
		}
		if(other==1)
		{
			System.out.println("district signup with other");
			//emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), "Welcome to User, please confirm your account",MailText.getRegistrationMailForQuestUser(request,userMaster));	
			//mail to admin   emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), "Welcome to User, please confirm your account",MailText.getRegistrationMailForQuestUser(request,userMaster));	
			s="";
		}
		else
		{
			String pass = "";
			if(password==null || password.isEmpty())
			{
				pass = Utility.randomString(6);
				//System.out.println(" Random Pass :: "+pass);
			}
			if(districtId.trim().length()==0)
			{
				System.out.println("teacher signup");
				teacherDetail.setFirstName(fname);
				teacherDetail.setLastName(lname);
				teacherDetail.setEmailAddress(emailAddress);
				teacherDetail.setUserType("N");
				teacherDetail.setPassword(MD5Encryption.toMD5(pass));	
				teacherDetail.setFbUser(false);
				teacherDetail.setQuestCandidate(1);
				teacherDetail.setAuthenticationCode(""+authorizationkey);
				teacherDetail.setVerificationCode(""+authorizationkey);
				teacherDetail.setVerificationStatus(0);
				teacherDetail.setIsPortfolioNeeded(true);
				teacherDetail.setNoOfLogin(0);
				teacherDetail.setStatus("A");
				teacherDetail.setSendOpportunity(false);
				teacherDetail.setIsResearchTeacher(false);
				teacherDetail.setForgetCounter(0);
				teacherDetail.setCreatedDateTime(new Date());
				teacherDetail.setIpAddress(IPAddressUtility.getIpAddress(request));
				teacherDetail.setInternalTransferCandidate(false);
				teacherDetail.setQuestCandidate(1);
				teacherDetailDAO.makePersistent(teacherDetail);
				System.out.println("===save");
				//basicController.saveJobForTeacher(request, teacherDetail);
				session.setAttribute("teacherDetail", teacherDetail);
				session.setAttribute("teacherName",	teacherDetail.getFirstName()+" "+teacherDetail.getLastName());
				session.setAttribute("lstMenuMaster", roleAccessPermissionDAO.getMenuListForTeacher(request));
				System.out.println("teacherDetail.getEmailAddress()==="+teacherDetail.getEmailAddress());
				System.out.println(" content :: "+MailText.getRegistrationMailForTempQuest(request,teacherDetail,pass));
				//emailerService.sendMailAsHTMLText("admin@teachermatch.com", "Welcome to TeacherMatch, please confirm your account",MailText.getRegistrationMailForQuest(request,teacherDetail));	
				s="";				
				try {
					String content = "";
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom(properties.getProperty("smtphost.tmpartners"));
					dsmt.setMailto(emailAddress);
					dsmt.setMailsubject("Thanks For Sign-Up With Quest");
					content=MailText.getRegistrationMailForTempQuest(request,teacherDetail,pass);					
					dsmt.setMailcontent(content);
					System.out.println("content  "+content);
					try {
						dsmt.start();	
					} catch (Exception e) {}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}			
		}
		}
		catch(Exception e)
		{
			//System.out.println(" ======error====");
			e.printStackTrace();			
			return s;
		}
		return s;
	}
	
	
	public String jobApplicationHistory(String jobId,String districtId,String redirectURL,String schoolId)
	{
		System.out.println("======== jobApplicationHistory ===========");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession();
		String s="Server error";
		try
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			DistrictMaster districtMaster = null;
			SchoolMaster schoolMaster = null;		
			JobApplicationHistory applicationHistory = new JobApplicationHistory();
			
			if(districtId!=null && !districtId.equals(""))
				districtMaster = districtMasterDAO.findByDistrictId(districtId);
			
			if(schoolId!=null && !schoolId.isEmpty())
			{
				schoolMaster = schoolMasterDAO.findById(new Long(schoolId), false, false);
			}			
			applicationHistory.setJobId(Integer.parseInt(jobId));	
			applicationHistory.setDistrictMaster(districtMaster);			
			if(teacherDetail!=null)
			{			
				applicationHistory.setTeacherDetail(teacherDetail);
			}			
			applicationHistory.setRedirectURL(redirectURL);
			applicationHistory.setAppliedDate(new Date());
			applicationHistory.setIpAddress(IPAddressUtility.getIpAddress(request));				
			jobApplicationHistoryDAO.makePersistent(applicationHistory);	
			System.out.println("===== Non-Client job applicant save=====");			
			s="";
		}
		catch(Exception e)
		{	
			e.printStackTrace();			
		}
		return s;
	}
	
	/**
	 * 
	 * author SONU
	 */
	public String displayPowerProfile()
	{
		System.out.println("displayPowerProfile..............");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}		
		StringBuffer ppRecords=new StringBuffer();		
		int marks=0;
		try
		{
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
			ppRecords.append("<table width='45%' ><tr><td></td><td class='head'>"+Utility.getLocaleValuePropByKey("lblMinimumPoints1", locale)+" </td><td class='head'>"+Utility.getLocaleValuePropByKey("MaximumPoints1", locale)+"</td> </tr>");
			ppRecords.append("<tr><td class='head' colspan='3'>"+Utility.getLocaleValuePropByKey("lblActiv", locale)+"</td></tr>");				
			
			if(teacherDetail !=null)
			{
				if(teacherDetail.getVerificationStatus()!=0)
				{
					System.out.println("varification status==  "+teacherDetail.getVerificationStatus());
					ppRecords.append("<tr><td>-"+Utility.getLocaleValuePropByKey("lblAuthenticatedRegistration1", locale)+"</td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/></td> </tr>");
				}
			}
			else
			{
				ppRecords.append("<tr><td>- "+Utility.getLocaleValuePropByKey("lblAuthenticatedRegistration1", locale)+"</td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/></td> </tr>");
			}
		TeacherPortfolioStatus teacherPortfolioStatus=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);				
		if(teacherPortfolioStatus!=null)
		{			
			if(teacherPortfolioStatus.getIsAffidavitCompleted())
			{
				System.out.println("IsAffidavitCompleted==   "+teacherPortfolioStatus.getIsAffidavitCompleted());
				ppRecords.append("<tr><td>- <a href='affidavit.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblSignedAffidavit1", locale)+"</a></td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/></td> </tr>");
				marks=marks+5;
			}
			else
			{				
				ppRecords.append("<tr><td>- <a href='affidavit.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblSignedAffidavit1", locale)+"</a></td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/></td> </tr>");
				
			}
		}
		else
		{			
			ppRecords.append("<tr><td>- <a href='affidavit.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblSignedAffidavit1", locale)+"</a></td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/></td> </tr>");
		}
		TeacherPreference teacherPreference=preferenceDAO.findPrefByTeacher(teacherDetail);
		if(teacherPreference!=null)
		{
			System.out.println("teacherPreference==   "+teacherPreference);
			ppRecords.append("<tr><td>- <a href='userpreference.do' target='_blank'>"+Utility.getLocaleValuePropByKey("Preferenceslbl", locale)+"<a/></td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/></td> </tr>");
			marks=marks+5;
		}
		else
		{
			ppRecords.append("<tr><td>- <a href='userpreference.do' target='_blank'>"+Utility.getLocaleValuePropByKey("Preferenceslbl", locale)+"</a></td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/></td> </tr>");
		}
		
		ppRecords.append("<tr><td class='head' colspan='3'>"+Utility.getLocaleValuePropByKey("lblCrede", locale)+"</td></tr>");
		
		List<TeacherAcademics> teacherAcademics=academicsDAO.findAcadamicDetailByTeacher(teacherDetail);
		
		if( teacherAcademics!=null && teacherAcademics.size()!=0)
		{			
			System.out.println("teacherAcademics==   "+teacherAcademics.size());
			ppRecords.append("<tr><td>- <a href='academics.do' target='_blank'>"+Utility.getLocaleValuePropByKey("headAcad", locale)+"</a></td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/></td> </tr>");
			marks=marks+5;
		}
		else
		{			
			ppRecords.append("<tr><td>- <a href='academics.do' target='_blank'>"+Utility.getLocaleValuePropByKey("headAcad", locale)+"</a></td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/></td> </tr>");
		}
		
		List<TeacherCertificate> teacherCertificates=certificateDAO.findCertificateByTeacher(teacherDetail);		
		
		
		
		if(locale.equals("en")){
		   if(teacherCertificates!=null && teacherCertificates.size()!=0)
		   {
			System.out.println("teacherCertificates  ==  "+teacherCertificates.size());
			if(teacherCertificates.size()==1)
			{				
				ppRecords.append("<tr><td>- <a href='certification.do?openDiv=Certifications' target='_blank'>"+Utility.getLocaleValuePropByKey("lblCertifications", locale)+"</a></td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/><img src='images/boxquestred.png'/></td> </tr>");
				
			}
			else if(teacherCertificates.size()>1)
			{				
				ppRecords.append("<tr><td>- <a href='certification.do?openDiv=Certifications' target='_blank'>"+Utility.getLocaleValuePropByKey("lblCertifications", locale)+"</a></td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/></td> </tr>");
				
			}			
		}	
		else
		{
			//System.out.println("certify333333333333");
			ppRecords.append("<tr><td>- <a href='certification.do?openDiv=Certifications' target='_blank'>"+Utility.getLocaleValuePropByKey("lblCertifications", locale)+"</a></td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/></td> </tr>");
		}
		
		}
		
		List<TeacherElectronicReferences> electronicReferences=electronicReferencesDAO.findActiveReferencesByTeacher(teacherDetail);
		if(teacherCertificates!=null && electronicReferences.size()!=0)
		{
			System.out.println("electronicReferences ==  "+electronicReferences.size());
			if(electronicReferences.size()==1)
			{
				ppRecords.append("<tr><td>- <a href='certification.do?openDiv=References' target='_blank'>"+Utility.getLocaleValuePropByKey("llbEndorsedReferences1", locale)+"</a></td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/></td> </tr>");
				
			}
			else if(electronicReferences.size()==2)
			{
				ppRecords.append("<tr><td>- <a href='certification.do?openDiv=References' target='_blank'>"+Utility.getLocaleValuePropByKey("llbEndorsedReferences1", locale)+"</a></td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/><img src='images/boxquestred.png'/></td> </tr>");
				
			}
			else if(electronicReferences.size()>2)
			{
				ppRecords.append("<tr><td>- <a href='certification.do?openDiv=References' target='_blank'>"+Utility.getLocaleValuePropByKey("llbEndorsedReferences1", locale)+"</a></td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/></td> </tr>");
				
			}
		}
		else
		{
			ppRecords.append("<tr><td>- <a href='certification.do?openDiv=References' target='_blank'>"+Utility.getLocaleValuePropByKey("llbEndorsedReferences1", locale)+"</a></td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/></td> </tr>");
		}
				
		TeacherExperience canServeAsSubTeacher=teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			
		if(canServeAsSubTeacher !=null)
		{
			if(canServeAsSubTeacher.getCanServeAsSubTeacher()!=2)
			{
				System.out.println("canServeAsSubTeacher  ==  "+canServeAsSubTeacher.getCanServeAsSubTeacher());
				ppRecords.append("<tr><td>- <a href='certification.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblOpenSubstitute1", locale)+"</a></td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/></td> </tr>");
				
			}
			else
			{
				ppRecords.append("<tr><td>- <a href='certification.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblOpenSubstitute1", locale)+"</a></td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/></td> </tr>");
			}
		}
		else
		{
			ppRecords.append("<tr><td>- <a href='certification.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblOpenSubstitute1", locale)+"</a></td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/></td> </tr>");
		}
		
		List<TeacherRole> teacherRoles=teacherRoleDAO.findTeacherRoleByTeacher(teacherDetail);
		if(teacherRoles!=null && teacherRoles.size()!=0)
		{
			System.out.println("teacherRoles  ==  "+teacherRoles.size());
			ppRecords.append("<tr><td>- <a href='experiences.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblEmplHis", locale)+"</a></td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/></td> </tr>");
			marks=marks+5;
		}
		else
		{
			ppRecords.append("<tr><td>- <a href='experiences.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblWorkHistory1", locale)+"</a></td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/></td> </tr>");
		}
		
		List<TeacherInvolvement> teacherInvolvement=teacherInvolvementDAO.findTeacherInvolvementByTeacher(teacherDetail);
		if(teacherInvolvement!=null && teacherInvolvement.size()!=0)
		{
			System.out.println("teacherInvolvement ==  "+teacherInvolvement.size());
			ppRecords.append("<tr><td>- <a href='experiences.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblVolunteerWork1", locale)+"</a></td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/></td> </tr>");
			marks=marks+5;
		}
		else
		{
			ppRecords.append("<tr><td>- <a href='experiences.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblVolunteerWork1", locale)+"</a></td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/></td> </tr>");
		}	
		
		//teacher EPI
		
		String teacherAssessmentStatus=teacherAssessmentStatusDAO.epiStatus(teacherDetail);		
		System.out.println("1"+teacherAssessmentStatus);
		if(teacherAssessmentStatus.equals("comp"))
		{			
			System.out.println("teacherAssessmentStatus ==  "+teacherAssessmentStatus);
			ppRecords.append("<tr><td class='head'>"+Utility.getLocaleValuePropByKey("lblTmEPI", locale)+"</td><td><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/></td> </tr>");
			
		}
		else
		{
			
			System.out.println("teacherAssessmentStatus ==  "+teacherAssessmentStatus);
			ppRecords.append("<tr><td class='head'><a href='javascript:void(0)' onclick='callepi();'>"+Utility.getLocaleValuePropByKey("lblTmEPI", locale)+"</a></td><td ><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/></td> </tr>");
		}
		
		
		//teacher EPI
		
		
	List<TeacherVideoLink> teacherVideoLink=linksDAO.findTeachByVideoLink(teacherDetail);
    if(!Utility.getValueOfPropByKey("basePath").equalsIgnoreCase("https://canada-en-test.teachermatch.org/")){
		if(teacherVideoLink!=null && teacherVideoLink.size()!=0){
			System.out.println("vedio link ==  "+teacherVideoLink.size());
			ppRecords.append("<tr><td class='head'><a href='certification.do?openDiv=Video' target='_blank'>"+Utility.getLocaleValuePropByKey("lblVideoProfile1", locale)+"</a></td><td ><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/></td> </tr>");
			marks=marks+10;
		}
		else{
		    ppRecords.append("<tr><td class='head'><a href='certification.do?openDiv=Video' target='_blank'>"+Utility.getLocaleValuePropByKey("lblVideoProfile1", locale)+"</a></td><td ><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/></td> </tr>");
		}		
	  }
		
		TeacherExperience teacherExperiences=teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
 		if(teacherExperiences!=null)
 		{
 			if(teacherExperiences.getResume()!=null)
 			{
 				System.out.println(teacherExperiences.getResume());
 				ppRecords.append("<tr><td class='head'><a href='experiences.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblResume", locale)+"</a></td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/></td> </tr>");
 				marks=marks+5;
 			}
 			else
 	 		{
 	 			ppRecords.append("<tr><td class='head'><a href='experiences.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblResume", locale)+"</a></td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/></td> </tr>");
 	 		}
 		}
 		else
 		{
 			ppRecords.append("<tr><td class='head'><a href='experiences.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblResume", locale)+"</a></td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/></td> </tr>");
 		}
		
 		List<JobForTeacher> jobForTeachers=jobForTeacherDAO.findJobByTeacherOneYear(teacherDetail);
		if(jobForTeachers!=null && jobForTeachers.size()!=0)
		{
			System.out.println("job for teacher==="+jobForTeachers.size());
			System.out.println(" List Job For Teacher : "+jobForTeachers.get(0).getCreatedDateTime());
			if(jobForTeachers.size()==1)
			{
				ppRecords.append("<tr><td><span class='head'><a href='jobsofinterest.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblJobsApplied", locale)+" </a></span>"+Utility.getLocaleValuePropByKey("lblAtleastone1", locale)+"</td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/><img src='images/boxquestred.png'/></td> </tr>");
				
			}
			else if(jobForTeachers.size()>=2)
			{
				ppRecords.append("<tr><td><span class='head'><a href='jobsofinterest.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblJobsApplied", locale)+" </a></span>"+Utility.getLocaleValuePropByKey("lblAtleastone1", locale)+"</td><td ><img src='images/boxquestgreen.png'/></td><td ><img src='images/boxquestgreen.png'/><img src='images/boxquestgreen.png'/></td> </tr>");
				
			}			
		}
		else
		{
			ppRecords.append("<tr><td><span class='head'><a href='jobsofinterest.do' target='_blank'>"+Utility.getLocaleValuePropByKey("lblJobsApplied", locale)+"</a> </span>(at least one)</td><td ><img src='images/boxquestred.png'/></td><td ><img src='images/boxquestred.png'/><img src='images/boxquestred.png'/></td> </tr>");
		}
		ppRecords.append("</table>");
		}
		catch(Exception e)
		{e.printStackTrace();}
		
		System.out.println("Total Score=  "  +marks);	
		
		return ppRecords.toString();
	}
	
	public String getSubjects(String districtId)
	{
		StringBuffer sb = new StringBuffer();
		System.out.println("======== get district wise subjects ===========");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
			
		DistrictMaster districtMaster = null;
		if(!districtId.equalsIgnoreCase("0"))
		{
			districtMaster = districtMasterDAO.findByDistrictId(districtId);
		}
		List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();
		System.out.println(" districtId :: "+districtId);
		if(districtMaster!=null)
		{
			System.out.println(" District Name :: "+districtMaster.getDistrictName()+"  &  District Id ::  " +districtMaster.getDistrictId());
			Criterion criterionD = Restrictions.eq("districtMaster", districtMaster);
			subjectMasters = subjectMasterDAO.findByCriteria(criterionD);
			if(subjectMasters!=null && subjectMasters.size()>0)
			{
				for(SubjectMaster subject : subjectMasters)
				{
					sb.append("<option value="+subject.getSubjectId()+">"+subject.getSubjectName()+"</option>");
				}
			}
		}
		else
		{
			//Criterion criterionD = Restrictions.eq("status", "A");
			List<MasterSubjectMaster> lstMasterSubjectMasters = masterSubjectMasterDAO.findAllActiveSuject();
			sb.append("<option value='0'>All</option>");
				if(lstMasterSubjectMasters!=null && lstMasterSubjectMasters.size()>0)
				{
					for(MasterSubjectMaster subject : lstMasterSubjectMasters)
					{
						sb.append("<option value="+subject.getMasterSubjectId()+">"+subject.getMasterSubjectName()+"</option>");
					}
				}
		}
		return sb.toString();
	}
	public String getSubjects(String districtId,boolean flag)
	{
		int i=0;
		StringBuffer sb = new StringBuffer();
		System.out.println("======== get district wise subjects ==========="+districtId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
			
		DistrictMaster districtMaster = null;
		if(!districtId.equalsIgnoreCase("0"))
		{
			try{
			districtMaster = districtMasterDAO.findByDistrictId(districtId);
			}catch(Exception e){e.printStackTrace();}
		}
		List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();
		System.out.println(" districtId :: "+districtId);
		if(districtMaster!=null)
		{
			System.out.println(" District Name :: "+districtMaster.getDistrictName()+"  &  District Id ::  " +districtMaster.getDistrictId());
			Criterion criterionD = Restrictions.eq("districtMaster", districtMaster);
			subjectMasters = subjectMasterDAO.findByCriteria(criterionD);
			if(subjectMasters!=null && subjectMasters.size()>0)
			{
				for(SubjectMaster subject : subjectMasters)
				{i++;
					sb.append("<option value="+subject.getSubjectId()+">"+subject.getSubjectName()+"</option>");
				}
			}
		}
		else
		{
			//shadab
			//change NC to platform
			System.out.println("before api call----"+i);
			if(ElasticSearchConfig.serverName!=null && ElasticSearchConfig.serverName.equalsIgnoreCase("platform"))
			{/*
				ClientConfig config = new DefaultClientConfig();
				URI uri=null;
			    Client client = Client.create(config);
			    try {
			    	//System.out.println("http://192.168.0.18:8080/teachermatch/services/callback/getDistrictMasterList/"+URLEncoder.encode(DistrictName,"UTF-8"));
			    	uri=new URI(ElasticSearchConfig.serviceURL+"/services/callback/getSubjectList/"+districtId);
			    	//System.out.println(ElasticSearchConfig.serviceURL+"/services/callback/getSubjectList/"+districtId);
			    	//uri=new URI("https://titan.teachermatch.org/services/callback/getSchoolMasterList/"+districtId+"/"+URLEncoder.encode(schoolName,"UTF-8"));
					//uri=new URI("http://192.168.0.18:8080/teachermatch/services/callback/getDistrictMasterList/"+DistrictName);
				} catch (URISyntaxException e) {
					
					e.printStackTrace();
				}
			    
			    WebResource service = client.resource(uri);
			    ClientResponse response = null;
			    response = service.type(MediaType.TEXT_HTML).get(ClientResponse.class);
			    String s=response.getEntity(String.class);
			    System.out.println(s);
			    try {
						sb.append(s);
					} catch (Exception e) {
					e.printStackTrace();
				}
			*/
				
				String sURLData="";
				try
				{
					URL oracle = new URL(ElasticSearchConfig.serviceURL+"/services/callback/getSubjectList/"+districtId);
					HttpsURLConnection yc = (HttpsURLConnection)oracle.openConnection();
					yc.setRequestProperty("User-Agent", "Mozilla/5.0");
					BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
				    String inputLine;
			        while ((inputLine = in.readLine()) != null) 
			        {
			           // System.out.println(inputLine);
			        	sb.append(inputLine);
			        }
			        in.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
			}
		}
		/*else
		{
			//Criterion criterionD = Restrictions.eq("status", "A");
			List<MasterSubjectMaster> lstMasterSubjectMasters = masterSubjectMasterDAO.findAllActiveSuject();
			sb.append("<option value='0'>All</option>");
				if(lstMasterSubjectMasters!=null && lstMasterSubjectMasters.size()>0)
				{i++;
					for(MasterSubjectMaster subject : lstMasterSubjectMasters)
					{
						sb.append("<option value="+subject.getMasterSubjectId()+">"+subject.getMasterSubjectName()+"</option>");
					}
				}
		}*/
		
		
		
		
		return sb.toString();
	}
	
	
	//Excel By shriram
	
	public String displayTeacherGridForExcel(String firstName,String lastName, String emailAddress,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String jobAppliedFromDate,String jobAppliedToDate,int daysVal)
	{

		String fileName = null;
		System.out.println("==================Quest Display Teacher Grid For Excel Method==========");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp); 
		}
		//-- get no of record in grid,
		//-- set start and end position
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start = ((pgNo-1)*noOfRowInPage);
		int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		boolean flagOhio8 = false;
		//------------------------------------
		StringBuffer tmRecords =	new StringBuffer();
		CandidateGridService cgService=new CandidateGridService();
		try{
			List<TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();

			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"lastName";
			String sortOrderNoField		=	"lastName";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			sortOrderStrVal=Order.asc("lastName");
			/**End ------------------------------------**/
			int roleId=0;
			int entityID=0;
			boolean isMiami = false;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			UserMaster userMaster=null;
			int entityTypeId=1;
			int utype = 1;
			boolean writePrivilegeToSchool =false;
			boolean isManage = true;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
					if(districtMaster.getDistrictId().equals(1200390))
						isMiami = true;
					writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
				}
				if(userMaster.getSchoolId()!=null){
					schoolMaster =userMaster.getSchoolId();
				}	
				entityID=userMaster.getEntityType();
				System.out.println("===========EntityId : "+entityID);
				utype = entityID;
			}
			
			if(isMiami && entityID==3)
				isManage=false;

			boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
			if(entityID==1)
			{
				achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
			}else
			{
				try{
					DistrictMaster districtMasterObj=districtMaster;
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
				}catch(Exception e){ e.printStackTrace();}
			}

			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,43,"teacherinfo.do",1);
			}catch(Exception e){
				e.printStackTrace();
			}
			Date fDate=null;
			Date tDate=null;
			Date appliedfDate=null;
			Date appliedtDate=null;
			boolean applieddateFlag=false;
			boolean dateFlag=false;
			try{
				// job Applied Filter
				if(jobAppliedFromDate!=null && !jobAppliedFromDate.equals("")){
					appliedfDate=Utility.getCurrentDateFormart(jobAppliedFromDate);
					applieddateFlag=true;
				}
				if(jobAppliedToDate!=null && !jobAppliedToDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(jobAppliedToDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					appliedtDate=cal2.getTime();
					applieddateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}			
			List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();
			boolean certTypeFlag=false;			
			System.out.println("jobOrderIds:: "+jobOrderIds.size());
			System.out.println("jobOrderIds:: "+jobOrderIds);
			System.out.println("certTypeFlag:: "+certTypeFlag);		
			List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
			List<TeacherDetail> NbyATeacherList = new ArrayList<TeacherDetail>();
			//List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean teacherFlag=false;			
			boolean nByAflag=false;		
			List<TeacherDetail> subjectTeacherList = new ArrayList<TeacherDetail>();
			boolean subjectFlag=false;
		
			if(subjectFlag && subjectTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(subjectTeacherList);
				}else{
					filterTeacherList.addAll(subjectTeacherList);
				}
			}
			System.out.println(" filterTeacherList :: "+filterTeacherList);
			if(subjectFlag){
				teacherFlag=true;
			}

			// Start /////////////////////////////// candidate list By certificate ///////////////////////////////////////
			StateMaster stateMasterForCandidate=null;
			List<CertificateTypeMaster> certTypeMasterListForCndt =new ArrayList<CertificateTypeMaster>();
			List<TeacherDetail> tListByCertificateTypeId = new ArrayList<TeacherDetail>();
			boolean tdataByCertTypeIdFlag = false;	
			
			/************* End **************/
			StatusMaster statusMaster = new StatusMaster();
			boolean status=false;			
			int internalCandVal=0;	
			SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
			SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
			List<JobForTeacher> lstJobForTeacher =	 new ArrayList<JobForTeacher>();
			List<JobOrder> lstJobOrderList =new ArrayList<JobOrder>();
			List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
			int noOfRecordCheck=0;
			
			SchoolMaster sm = schoolMaster;
			List<TeacherDetail> lstTeacherDetailAll= new ArrayList<TeacherDetail>();
			if(entityTypeId!=0){
				if(entityID==3){
					
					lstJobOrderSLList =schoolInJobOrderDAO.allJobOrderPostedBySchool(schoolMaster);
					entityTypeId=3;
				}				
				if(entityTypeId==3 && lstJobOrderSLList.size()>0 && certTypeFlag)
				{
					lstJobOrderList.addAll(getFilterJobOrder(lstJobOrderSLList,jobOrderIds));
				}
				else if(entityTypeId!=3 && lstJobOrderSLList.size()==0 && certTypeFlag)
				{
					lstJobOrderList.addAll(jobOrderIds);
					entityTypeId=3;
				}
				else
				{
					lstJobOrderList.addAll(lstJobOrderSLList);
				}
				if(entityID==2){
					entityTypeId=2;
				}
				
				String stq="";
				if(districtMaster!=null  && utype==3)
				{
					SecondaryStatus smaster = districtMaster.getSecondaryStatus();
					if(smaster!=null)
					{
						String secStatus = smaster.getSecondaryStatusName();
						if(secStatus!=null && !secStatus.equals(""))
						{
							//
							 List<SecondaryStatus> lstStatus = new ArrayList<SecondaryStatus>();
							 lstStatus = secondaryStatusDAO.findSecondaryStatusByName(districtMaster,secStatus);
							 System.out.println("lstStatus.size():::: "+lstStatus.size());
							 for (SecondaryStatus secondaryStatus : lstStatus) {
								 if(secondaryStatus.getJobCategoryMaster()!=null)
								 {
									 stq+=" IF(secondarys3_.jobCategoryId="+secondaryStatus.getJobCategoryMaster().getJobCategoryId()+", ( secondaryStatusId >="+secondaryStatus.getSecondaryStatusId()+" ),false) or";
								 }
							}
							 if(stq.length()>3)
							 {
								 stq = stq.substring(0, stq.length()-2);
							 }
						}
					}
				}
				
				boolean questFlag = false;
				List <DistrictMaster> districtMastersList=new ArrayList<DistrictMaster>();
				if(entityID==1 && entityTypeId==1 && dateFlag==false && applieddateFlag==false && status==false && internalCandVal==0){
					//>>>>>>>>>>>>>>>>>>>>> Job Applied change >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
					//System.out.println("lstJobOrderList:::"+lstJobOrderList.size());
					//lstTeacherDetail=jobForTeacherDAO.getTeacherDetailByDASA(sortOrderStrVal,start,noOfRowInPage,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq);
					lstTeacherDetailAll=jobForTeacherDAO.getQuestTeacherDetailRecords(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm);
					//>>>>>>>>>>>>>>>>>>>>> Job Applied change >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
					
					totalRecord=lstTeacherDetailAll.size();					
				}else{
					//shriram
					DistrictMaster dm=null;
					Criterion critdistrict=null;
					if(districtMaster!=null && entityID==2)
					{
						HeadQuarterMaster hq=districtMaster.getHeadQuarterMaster();
						if(hq!=null && hq.getHeadQuarterId()==3)
						{
							flagOhio8 = true;
						Criterion critheadquarter=Restrictions.eq("headQuarterMaster", hq);
						dm= districtMasterDAO.findById(3904380, false, false);
						if(districtMaster.getDistrictId()!=3904380)
						{			critdistrict=  Restrictions.ne("districtId", dm.getDistrictId());
						districtMastersList=districtMasterDAO.findByCriteria(critheadquarter,critdistrict);
						}
						else
							districtMastersList=districtMasterDAO.findByCriteria(critheadquarter);
						System.out.println("Size of DistrictmasterList"+districtMastersList.size());
					}
					}
					//ended by shriram
					if(!(entityTypeId==3 && lstJobOrderList.size()==0))
					{
						//lstTeacherDetail=jobForTeacherDAO.getTeacherDetailByDASA(sortOrderStrVal,start,noOfRowInPage,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq);
						lstTeacherDetailAll=jobForTeacherDAO.getQuestTeacherDetailRecords(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList ,firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm);
					}
					totalRecord=lstTeacherDetailAll.size();
				}
				if(totalRecord<end)
					end=totalRecord;

				
				System.out.println("++++++++++++++++++++++++++++Teacher Details Size:: "+lstTeacherDetailAll.size());
				
				
				
				// sorting on normscore, ascore, lrscore
				
					
				if(!sortOrder.equals("tLastName") && !flagOhio8)
				{
					List<TeacherNormScore> teacherNormScoreList = teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetailAll);
					Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
					for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
						normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
					}
					Map<Integer,TeacherAchievementScore> mapAScore = null;

					if(entityID!=1 && achievementScore)
					{
						JobOrder jo=new JobOrder();
						jo.setDistrictMaster(districtMaster);
						List<TeacherAchievementScore> lstTeacherAchievementScore=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetailAll,jo);

						mapAScore = new HashMap<Integer, TeacherAchievementScore>();

						for(TeacherAchievementScore tAScore : lstTeacherAchievementScore){
							mapAScore.put(tAScore.getTeacherDetail().getTeacherId(),tAScore);
						}
					}
					TeacherAchievementScore tAScore=null;

					TeacherNormScore teacherNormScore = null;
					for(TeacherDetail teacherDetail : lstTeacherDetailAll)
					{
						teacherNormScore = normMap.get(teacherDetail.getTeacherId());
						if(teacherNormScore==null)
							teacherDetail.setNormScore(-0);
						else
							teacherDetail.setNormScore(teacherNormScore.getTeacherNormScore());

						if(entityID!=1 && achievementScore)
						{
							tAScore=mapAScore.get(teacherDetail.getTeacherId());
							if(tAScore!=null){
								if(tAScore.getAcademicAchievementScore()!=null)
									teacherDetail.setaScore(tAScore.getAcademicAchievementScore());

								if(tAScore.getLeadershipAchievementScore()!=null)
									teacherDetail.setlRScore(tAScore.getLeadershipAchievementScore());
							}else{
								teacherDetail.setaScore(-1);
								teacherDetail.setlRScore(-1);
							}
						}
					}

					if(sortOrderFieldName.equals("normScore") && sortOrderType.equals("0")){
						Collections.sort(lstTeacherDetailAll,TeacherDetail.teacherDetailComparatorNormScoreDesc );
					}else if(sortOrderFieldName.equals("normScore") && sortOrderType.equals("1")){
						Collections.sort(lstTeacherDetailAll,TeacherDetail.teacherDetailComparatorNormScore);
					}else if(sortOrderFieldName.equals("aScore") && sortOrderType.equals("1")){				
						Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorAScore );
					}else if(sortOrderFieldName.equals("aScore") && sortOrderType.equals("0")){				
						Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorAScoreDesc );				
					}else if(sortOrderFieldName.equals("lRScore") && sortOrderType.equals("1")){				
						Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorLRScore );
					}else if(sortOrderFieldName.equals("lRScore") && sortOrderType.equals("0")){				
						Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorLRScoreDesc );				
					}

					lstTeacherDetail=lstTeacherDetailAll;
									}
				else
				{
					if(sortOrder.equals("tLastName")){
						for(TeacherDetail td:lstTeacherDetailAll)
						{
								td.settLastName(td.getLastName());
						}
						
						if(sortOrderType.equals("0"))
							Collections.sort(lstTeacherDetailAll,new TeacherDetailComparatorLastNameAsc());
						else
							Collections.sort(lstTeacherDetailAll,new TeacherDetailComparatorLastNameDesc());
					}
				
					lstTeacherDetail=lstTeacherDetailAll;
				}

			

				List<Integer> lstStatusMasters = new ArrayList<Integer>();
				String[] statuss = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
				try{
					lstStatusMasters = statusMasterDAO.findStatusIdsByStatusByShortNames(statuss);
				}catch (Exception e) {
					e.printStackTrace();
				}
				if(totalRecord!=0){
					lstJobForTeacher=jobForTeacherDAO.getJobForTeacherByDSTeachersList(lstStatusMasters,lstTeacherDetail,lstJobOrderSLList,null,null,districtMaster,entityID);
					for(JobForTeacher jobForTeacher : lstJobForTeacher){
						jftMap.put(jobForTeacher.getJobForTeacherId(),jobForTeacher);
					}
					lstJobForTeacher=jobForTeacherDAO.getJobForTeacherByDSTeachersList(lstStatusMasters,lstTeacherDetail,lstJobOrderSLList,null,null,districtMaster,1);
					for(JobForTeacher jobForTeacher : lstJobForTeacher){
						jftMapForTm.put(jobForTeacher.getJobForTeacherId(),jobForTeacher);
					}
				}
		}
			
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			List<TeacherNormScore> teacherNormScoreList=teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetail);
			Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
			for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
				normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
			}

			Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();	
			Map<Integer,Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
			List<TeacherExperience> lstTeacherExperience=teacherExperienceDAO.findExperienceForTeachers(lstTeacherDetail);		

			// ************** Start ... IDENTIFY Internal Candidate *********************
			Map<Integer,Boolean> mapInternalCandidate = new HashMap<Integer, Boolean>();
			if(entityID!=1)
			{
				List<JobForTeacher> lstjobForTeachers=jobForTeacherDAO.findInternalCandidate(lstTeacherDetail);
				if(entityID==2)
				{
					for(JobForTeacher pojo:lstjobForTeachers){
						if(districtMaster.equals(pojo.getJobId().getDistrictMaster()))
						{
							mapInternalCandidate.put(pojo.getTeacherId().getTeacherId(), true);
						}
					}
				}
				else if(entityID==3)
				{
					Map<Integer,Boolean> mapJobOrder = new HashMap<Integer, Boolean>();
					List<JobOrder> lstJobOrders=new ArrayList<JobOrder>();
					for(JobForTeacher pojo:lstjobForTeachers){
						if(districtMaster.equals(pojo.getJobId().getDistrictMaster()))
							lstJobOrders.add(pojo.getJobId());
					}
					List<SchoolInJobOrder> lstSchoolInJobOrders= new ArrayList<SchoolInJobOrder>();
					if(schoolMaster!=null)
					 lstSchoolInJobOrders=schoolInJobOrderDAO.findInternalCandidate(lstJobOrders, schoolMaster);
					
					if(lstSchoolInJobOrders.size()>0){
						for(SchoolInJobOrder pojo:lstSchoolInJobOrders)
							mapJobOrder.put(pojo.getJobId().getJobId(), true);
					}

					for(JobForTeacher pojo:lstjobForTeachers){
						if(mapJobOrder.get(pojo.getJobId().getJobId())!=null && mapJobOrder.get(pojo.getJobId().getJobId()).equals(true)){
							mapInternalCandidate.put(pojo.getTeacherId().getTeacherId(), true);
						}
					}
				}
			}
			// ************** End ... IDENTIFY Internal Candidate *********************

			List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
			Integer isBase = null;
			try{
				if(lstTeacherDetail!=null && lstTeacherDetail.size()>0){
					teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(lstTeacherDetail);
					for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
						baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
						boolean cgflag = teacherAssessmentStatus2.getCgUpdated()==null?false:teacherAssessmentStatus2.getCgUpdated();
						if(cgflag && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
						{
							isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
						}
						else
						{
							if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
							{
								isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 2);
							}
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			    //alok kumar			
				Map<Integer,List<Integer>> jsiCount	=	new HashMap<Integer, List<Integer>>();
				Map<Integer,List<Integer>> epiCount	=	new HashMap<Integer, List<Integer>>();
				
				List<TeacherAssessmentStatus> teacherAssessmentJsi = null;
				List<TeacherAssessmentStatus> teacherAssessmentEpi = null;
				List<Integer> teacherId = new ArrayList();
				List<Integer> matchTeacherId = null;
				try
				{
					if(lstTeacherDetail!=null && lstTeacherDetail.size()>0)
					{

						for(TeacherDetail t:lstTeacherDetail)
						{
							teacherId.add(t.getTeacherId());
						}	
						teacherAssessmentJsi = teacherAssessmentStatusDAO.findAssessmentJsiByTeachers(lstTeacherDetail);
						teacherAssessmentEpi=teacherAssessmentStatusDAO.findAssessmentEpiByTeachers(lstTeacherDetail);						
						if(teacherAssessmentJsi.size()>0)
						{
							for(Integer id :teacherId)
							{
								matchTeacherId=new ArrayList();
								for(TeacherAssessmentStatus assessmentteacherId:teacherAssessmentJsi)
								{								  
								  if(id.equals(assessmentteacherId.getTeacherDetail().getTeacherId()))
								  {
									  matchTeacherId.add(assessmentteacherId.getTeacherDetail().getTeacherId());
								  }
								}
								if(matchTeacherId.size()>0)
								jsiCount.put(id, matchTeacherId);							
							}
						}
						if(teacherAssessmentEpi.size()>0)
						{
							for(Integer id :teacherId)
							{
								matchTeacherId=new ArrayList();
								for(TeacherAssessmentStatus assessmentteacherId:teacherAssessmentEpi)
								{
								  if(id.equals(assessmentteacherId.getTeacherDetail().getTeacherId()))
								  {
									  matchTeacherId.add(assessmentteacherId.getTeacherDetail().getTeacherId());
								  }
								}
								if(matchTeacherId.size()>0)
								epiCount.put(id, matchTeacherId);							
							}
						}
					}				
				}			
				catch(Exception e)
				{e.printStackTrace();}
				
			
			String tFname="",tLname="";
			String responseText="";
			String schoolDistrict="District";
			if(entityID==3){
				schoolDistrict="School";
			}

			//tmRecords.append("<th width='60' valign='top'>Norm Score</th>");
			//shriram
			//shriram this is th means table header field (2) Norm Score
			List<TeacherPersonalInfo> lstTeacherPersonalInfo = new ArrayList<TeacherPersonalInfo>();
			List<TeacherAchievementScore> lstTeacherAchievementScore = new ArrayList<TeacherAchievementScore>();			
			List<DistrictSpecificQuestions> lstdistrictSpecificQuestions = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(userMaster.getDistrictId());
			boolean schoolCheckFlag=false;
			if(userMaster.getEntityType()==3)
			{
				try
				{
					schoolMaster = schoolMasterDAO.findById(userMaster.getSchoolId().getSchoolId(), false, false);
					DistrictSchools districtSchools = districtSchoolsDAO.findBySchoolMaster(schoolMaster);
					if(districtSchools.getDistrictMaster().getResetQualificationIssuesPrivilegeToSchool()==true)
					{
						schoolCheckFlag = true;
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			int counter=0;
			TeacherNormScore teacherNormScore = null;
			
			
			List<Integer>teacherIdLst=new ArrayList<Integer>();
			for (TeacherDetail teacherDetail : lstTeacherDetail) 
			{
				teacherIdLst.add(teacherDetail.getTeacherId());
			}
			Criterion criterionTeacherPersonalInfo=Restrictions.in("teacherId", teacherIdLst);
			List<TeacherPersonalInfo>teacherPersonalInfos=null;
			HashMap<Integer, TeacherPersonalInfo> map=new HashMap<Integer, TeacherPersonalInfo>();
			if(teacherIdLst!=null && teacherIdLst.size()>0){
				teacherPersonalInfos=teacherPersonalInfoDAO.findByCriteria(criterionTeacherPersonalInfo);
				for (TeacherPersonalInfo pojo : teacherPersonalInfos) 
				{
					map.put(pojo.getTeacherId(), pojo);
				}
			}	
			//alok
			Map<Integer, EmployeeMaster> empMap = new HashMap<Integer, EmployeeMaster>();
			Map<String,Boolean> mapAlum = new HashMap<String, Boolean>(); // For Alum
			if(entityID!=1)
			{
				String ssn="";				
				Map<Integer, TeacherPersonalInfo>tpInfomap=new  HashMap<Integer, TeacherPersonalInfo>();
				List<String> teacherFNameList = new ArrayList<String>();
				List<String> teacherLNameList = new ArrayList<String>();
				List<String> teacherSSNList = new ArrayList<String>();
				if(teacherPersonalInfos!=null && teacherPersonalInfos.size()>0){				
					for(TeacherPersonalInfo pojo :teacherPersonalInfos) {
						tpInfomap.put(pojo.getTeacherId(), pojo); 
						ssn = pojo.getSSN()==null?"":pojo.getSSN().trim();
						if(!ssn.equals(""))
						{
							teacherFNameList.add(pojo.getFirstName().trim());
							teacherLNameList.add(pojo.getLastName().trim());
							ssn = ssn.trim();
							try {
								ssn = Utility.decodeBase64(ssn);
							} catch (IOException e) {
								e.printStackTrace();
							}
							if(ssn.length()>4)
								ssn = ssn.substring(ssn.length() - 4, ssn.length());
							teacherSSNList.add(ssn.trim());
						}
					}
				}
				List<EmployeeMaster> employeeMasters =	employeeMasterDAO.checkEmployeeByFistName(teacherFNameList,teacherLNameList,teacherSSNList,districtMaster);
				mapAlum=cgService.getMapAlum(employeeMasters, districtMaster); // get Alum Map
				
				if(teacherPersonalInfos!=null && teacherPersonalInfos.size()>0 && employeeMasters.size()>0){				
					for(TeacherPersonalInfo pojo :teacherPersonalInfos) {
						ssn = pojo.getSSN()==null?"":pojo.getSSN().trim();
						if(!ssn.equals(""))
						{
							ssn = ssn.trim();
							try {
								ssn = Utility.decodeBase64(ssn);
							} catch (IOException e) {
								e.printStackTrace();
							}
							if(ssn.length()>4)
								ssn = ssn.substring(ssn.length() - 4, ssn.length());
							String empNo = pojo.getEmployeeNumber()==null?"":pojo.getEmployeeNumber().trim();
							
							for(EmployeeMaster emp :employeeMasters) {
								if(empNo.equals(""))// Not an employee
								{
									if(emp.getSSN().equalsIgnoreCase(ssn) && emp.getFirstName().equalsIgnoreCase(pojo.getFirstName()) && emp.getLastName().equalsIgnoreCase(pojo.getLastName()))
									{
										empMap.put(pojo.getTeacherId(), emp);
										break;
									}
								}
								else 
								{
									if(emp.getEmployeeCode().equalsIgnoreCase(empNo) && emp.getSSN().equalsIgnoreCase(ssn) && emp.getFirstName().equalsIgnoreCase(pojo.getFirstName()) && emp.getLastName().equalsIgnoreCase(pojo.getLastName()))
									{
										empMap.put(pojo.getTeacherId(), emp);
										break;
									}
								}
							}
						}
					}
				}
			}			
			//Gourav  districtMaster
			List<SecondaryStatusMaster> lstSecondaryStatusMaster = secondaryStatusMasterDAO.findTagswithDistrictInCandidatePool(districtMaster);
			Criterion district_cri = Restrictions.eq("districtMaster", districtMaster);
			Criterion school_cri = Restrictions.isNull("schoolMaster");
			Criterion job_cri = Restrictions.isNull("jobOrder");
			
			List<TeacherSecondaryStatus> lstMltTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByCriteria(district_cri,school_cri,job_cri);
			Map<String, TeacherSecondaryStatus> teachersTagsExistMap = new HashMap<String, TeacherSecondaryStatus>();
			if(lstMltTeacherSecondaryStatus.size()>0){
				for (TeacherSecondaryStatus teacherSecondaryStatus : lstMltTeacherSecondaryStatus) {
					String teacherTagKey = teacherSecondaryStatus.getTeacherDetail().getTeacherId()+"##"+teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusId();
					teachersTagsExistMap.put(teacherTagKey, teacherSecondaryStatus);
				}
			}
			
			if(lstTeacherDetail.size()==0){}
//				tmRecords.append("<tr><td colspan='9'>No Teacher Found</td></tr>" );//Shriram No Record Found
			
			else
			{
				if(entityID!=1)
				{
					JobOrder jobOrder = new JobOrder();
					jobOrder.setDistrictMaster(districtMaster);
					lstTeacherAchievementScore=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetail,jobOrder);
				}
				lstTeacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPInfoForTeacher(lstTeacherDetail);
			}
			Map<Integer,Boolean> qqTeachersMap = new HashMap<Integer, Boolean>(); 
			if(lstTeacherDetail.size()>0 && entityTypeId!=1 && lstdistrictSpecificQuestions.size()>0)
			{
				List<TeacherDetail> qqAttemptedTeachers =  teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findQQStatusByTeacher(lstTeacherDetail,districtMaster,true);
				List<TeacherDetail> invalidQQTeachers = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findQQStatusByTeacher(lstTeacherDetail,districtMaster,false);
				for (TeacherDetail teacherDetail : qqAttemptedTeachers) {
					qqTeachersMap.put(teacherDetail.getTeacherId(), true);
				}
				for (TeacherDetail teacherDetail : invalidQQTeachers) {
					qqTeachersMap.put(teacherDetail.getTeacherId(), false);
				}
			}

			// teacherwiseassessmenttime
			Map<Integer, TeacherWiseAssessmentTime> teacherWMap = new HashMap<Integer, TeacherWiseAssessmentTime>();
			if(lstTeacherDetail.size()>0){
				Calendar cal = Calendar.getInstance();

				cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
				Date sDate=cal.getTime();
				cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
				Date eDate=cal.getTime();

				Criterion teacherListCR = Restrictions.in("teacherDetail", lstTeacherDetail);
				Criterion assessmenType		= Restrictions.eq("assessmentType", 1);
				Criterion currentYear = Restrictions.between("createdDateTime", sDate, eDate);
				
				List<TeacherWiseAssessmentTime> teacherWiseAssessmentTime = teacherWiseAssessmentTimeDAO.findByCriteria(Order.desc("assessmentTimeId"),teacherListCR,assessmenType);
				
				if(teacherWiseAssessmentTime.size()>0){
					for (TeacherWiseAssessmentTime teacherWiseAssessmentTime2 : teacherWiseAssessmentTime) {
						if(!teacherWMap.containsKey(teacherWiseAssessmentTime2.getTeacherDetail().getTeacherId())){
							teacherWMap.put(teacherWiseAssessmentTime2.getTeacherDetail().getTeacherId(), teacherWiseAssessmentTime2);
						}
					}
				}
			}
			
			String sCheckBoxValue="";
			Map<Integer,Boolean> finalizedTMap = new HashMap<Integer, Boolean>();
			List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
			if(districtMaster!=null && lstTeacherDetail.size()>0)
			{
				lstTeacher = jobForTeacherDAO.getQQFinalizedTeacherList(lstTeacherDetail,districtMaster);
				for (TeacherDetail teacherDetail : lstTeacher) {
					finalizedTMap.put(teacherDetail.getTeacherId(), true);
				}
			}
			////////////////////////////////Add disable profile( Sekhar )///////////////////////////////
			Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
			System.out.println("=============================="+lstTeacherDetail.size());
			List<TeacherStatusHistoryForJob> historyForJobList = teacherStatusHistoryForJobDAO.findHireTeachersWithDistrictListAndTeacherList(lstTeacherDetail,districtMaster);
			List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherForOffers(lstTeacherDetail);
			List<String> posList=new ArrayList<String>();
			Map<String,String> reqMap=new HashMap<String, String>();
			for (JobForTeacher jobForTeacher : jftOffers){
				try{
					if(jobForTeacher.getRequisitionNumber()!=null){
						posList.add(jobForTeacher.getRequisitionNumber());
						reqMap.put(jobForTeacher.getJobId().getJobId()+"#"+jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getRequisitionNumber());
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(districtMaster,posList);
			Map<String,DistrictRequisitionNumbers> dReqNoMap = new HashMap<String,DistrictRequisitionNumbers>();
			try{
				for (DistrictRequisitionNumbers districtRequisitionNumbers2 : districtRequisitionNumbers) {
					dReqNoMap.put(districtRequisitionNumbers2.getRequisitionNumber(),districtRequisitionNumbers2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(historyForJobList.size()>0){
				mapForHiredTeachers=cgService.getHiredList(reqMap,dReqNoMap,lstTeacherDetail,historyForJobList,null);
			}
				String time = String.valueOf(System.currentTimeMillis()).substring(6);
				String basePath = request.getSession().getServletContext().getRealPath("/")+ "/candidatelistAppiledforjob";
				
				fileName = "CandidateList.xls";

				File file = new File(basePath);
				if (!file.exists())
					file.mkdirs();

				Utility.deleteAllFileFromDir(basePath);

				file = new File(basePath + "/" + fileName);

				WorkbookSettings wbSettings = new WorkbookSettings();

				wbSettings.setLocale(new Locale("en", "EN"));

				WritableCellFormat timesBoldUnderline;
				WritableCellFormat header;
				WritableCellFormat headerBold;
				WritableCellFormat times;

				WritableWorkbook workbook = Workbook.createWorkbook(file,wbSettings);
				workbook.createSheet("CandidateList", 0);
				WritableSheet excelSheet = workbook.getSheet(0);

				WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
				// Define the cell format
				times = new WritableCellFormat(times10pt);
				// Lets automatically wrap the cells
				times.setWrap(true);

				WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD, false);
				WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

				timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
				timesBoldUnderline.setAlignment(Alignment.CENTRE);
				// Lets automatically wrap the cells
				timesBoldUnderline.setWrap(true);

				header = new WritableCellFormat(times10ptBoldUnderline);
				headerBold = new WritableCellFormat(times10ptBoldUnderline);
				CellView cv = new CellView();
				cv.setFormat(times);
				cv.setFormat(timesBoldUnderline);
				cv.setAutosize(true);

				header.setBackground(Colour.GRAY_25);

				// Write a few headers
				excelSheet.mergeCells(0, 0, 2, 1);
				Label label;
				label = new Label(0, 0, "Ohio 8 Candidate List",header);
				excelSheet.addCell(label);
				excelSheet.mergeCells(0, 3, 2, 3);
				label = new Label(0, 3, "");
				excelSheet.addCell(label);
				excelSheet.getSettings().setDefaultColumnWidth(18);

				int k = 4;
				int col = 1;
				label = new Label(0, k, "Candidate First Name", header);
				excelSheet.addCell(label);
				label = new Label(1, k, "Candidate Last Name ", header);
				excelSheet.addCell(label);
				label = new Label(++col, k, "Candidate Email Address", header);
				excelSheet.addCell(label);
				if(!flagOhio8)
				{
				label = new Label(++col, k, "Norm Score", header);
				excelSheet.addCell(label);
				}
				label = new Label(++col, k, "# of Jobs", header);
				excelSheet.addCell(label);
				k = k+1;
						if(lstTeacherDetail!=null && lstTeacherDetail.size()>0)
							{
								System.out.println(" Teachers List Get ");//Shriram Teacher List Get
									for (TeacherDetail teacherDetail : lstTeacherDetail) 
												   {
												//////////////////////excel code//////////
											// Chhoda111	
												col = 1;
												///////////////////////////////////////////////
												teacherNormScore = normMap.get(teacherDetail.getTeacherId());
												int normScore = 0;
												String colorName = "";
												if(teacherNormScore!=null)
												{
													normScore = teacherNormScore.getTeacherNormScore();
												}
												noOfRecordCheck++;
												tFname=teacherDetail.getFirstName(); //shriram Teacher first name	
												tLname=teacherDetail.getLastName();//Shriram Teacher Last Name
												label = new Label(0, k, tFname);
												excelSheet.addCell(label);
												label = new Label(1, k, tLname);
												excelSheet.addCell(label);
												label = new Label(++col, k, teacherDetail.getEmailAddress());
												excelSheet.addCell(label);
												counter++;
												//gourav  tag loop start
												StringBuffer tagNames =	new StringBuffer();
												if(roleId != 1){}
												//Add profile hover
								
												String profileNormScore="";
												if(!flagOhio8)
												{
												if(normScore!=0){
													profileNormScore=normScore+"";
													label = new Label(++col, k, profileNormScore);
													excelSheet.addCell(label);
												}else{
													profileNormScore="N/A";
													label = new Label(++col, k, profileNormScore);
													excelSheet.addCell(label);
												}
												}
											/* @Start
											 * @Ashish Kumar
											 * @Description :: Qualification Details Thumb 
											 * */
												
												//<a href='#' onclick=\"getMessageDiv('"+teacherDetail.getTeacherId()+"','"+teacherDetail.getEmailAddress()+"',0,0);\" >
												TeacherPersonalInfo tempTeacher=map.get(teacherDetail.getTeacherId());
												
												
												boolean CheckForIssue = false; 
												
												if(userMaster.getEntityType()==2)
												{
													CheckForIssue = true;
												}
												
												if(schoolCheckFlag)
													CheckForIssue = true;
												
												if(CheckForIssue == true)
												{
													if(lstdistrictSpecificQuestions.size()>0)
													{
														Boolean qqTaken = qqTeachersMap.get(teacherDetail.getTeacherId());
														if(qqTaken!=null)
														{
															Boolean finalizeQQ = finalizedTMap.get(teacherDetail.getTeacherId());
															finalizeQQ = finalizeQQ==null?false:finalizeQQ;
															
														}
															
													}
												}
												
												int jobList=jobListByTeacher(teacherDetail.getTeacherId(),jftMap);
												int jobListForTm=jobListByTeacher(teacherDetail.getTeacherId(),jftMapForTm);
												//shriram JobList
												if(jobList!=0){   
													label = new Label(++col, k, jobList+"/"+(jobListForTm-jobList)+"");
													excelSheet.addCell(label);//mukesh
												}else{
													label = new Label(++col, k, "0"+"/"+(jobListForTm-jobList)+"");
													excelSheet.addCell(label);
												}
												k++;
											}
			}else
			{
				excelSheet.mergeCells(0, 5, 2, 1);
				label = new Label(0, 5, "No records found");
				excelSheet.addCell(label);
			}
			workbook.write();
			workbook.close();
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return fileName;
	}
	
	//PDF BY SHRIRAM
	
	

	public String displayOhioCandidateListPdf(String firstName,String lastName, String emailAddress,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String jobAppliedFromDate,String jobAppliedToDate,int daysVal) {
		System.out.println("in the pdf method");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session
						.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey(
					"msgYrSesstionExp", locale));
		}
		try {
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath + "fonts/tahoma.ttf",
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(
					fontPath + "fonts/tahoma.ttf", BaseFont.CP1252,
					BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
						String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")
					+ "/user/";
			
			System.out.println(basePath);
			String fileName = time + "Quest.pdf";

			File file = new File(basePath);
			if (!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			//System.out.println("user/" + userId + "/" + fileName);
			/*int teacherid =0;
			if(teacheridd=="")
				teacherid=0;
			else
				teacherid = Integer.parseInt(teacheridd);*/
		/*	generateCandidatePDReport(noOfRow, pageNo, sortOrder,sortOrderType, basePath + "/" + fileName, context.getServletContext().getRealPath("/"), districtID,
					jobcategoryID, jobstatus, jobTitle,"",fromdate, endDate);*/
			generateCandidateListPdf(basePath + "/" + fileName,context.getServletContext().getRealPath("/"),firstName,lastName,emailAddress,noOfRow,pageNo,sortOrder,sortOrderType,jobAppliedFromDate,jobAppliedToDate,daysVal);
			
			return "user/" + fileName;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "ABV";
	}
	
public Boolean generateCandidateListPdf(String path,String realPath,String firstName,String lastName, String emailAddress,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String jobAppliedFromDate,String jobAppliedToDate,int daysVal)
{
int cellSize = 0;
Document document = null;
FileOutputStream fos = null;
PdfWriter writer = null;
Paragraph footerpara = null;
HeaderFooter headerFooter = null;
Font font8 = null;
Font font8Green = null;
Font font8bold = null;
Font font9 = null;
Font font9bold = null;
Font font10 = null;
Font font10_10 = null;
Font font10bold = null;
Font font11 = null;
Font font11bold = null;
Font font20bold = null;
Font font11b = null;
Color bluecolor = null;
Font font8bold_new = null;

///////////////////////////////////////////////////////////////////////////////
String fileName = null;
System.out.println("==================Quest Display Teacher Grid For Excel Method==========");
/* ========  For Session time Out Error =========*/
WebContext context;
context = WebContextFactory.get();
HttpServletRequest request = context.getHttpServletRequest();
HttpSession session = request.getSession(false);
if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
{
	throw new IllegalStateException(msgYrSesstionExp); 
}
//-- get no of record in grid,
//-- set start and end position

int noOfRowInPage = Integer.parseInt(noOfRow);
int pgNo = Integer.parseInt(pageNo);
int start = ((pgNo-1)*noOfRowInPage);
int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
int totalRecord = 0;
boolean flagOhio8 = false;
//------------------------------------
StringBuffer tmRecords =	new StringBuffer();
CandidateGridService cgService=new CandidateGridService();
try{
	List<TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();

	/** set default sorting fieldName **/
	String sortOrderFieldName	=	"lastName";
	String sortOrderNoField		=	"lastName";

	/**Start set dynamic sorting fieldName **/
	Order  sortOrderStrVal=null;

	if(sortOrder!=null){
		if(!sortOrder.equals("") && !sortOrder.equals(null)){
			sortOrderFieldName=sortOrder;
			sortOrderNoField=sortOrder;
		}
	}

	String sortOrderTypeVal="0";
	if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
		if(sortOrderType.equals("0")){
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}else{
			sortOrderTypeVal="1";
			sortOrderStrVal=Order.desc(sortOrderFieldName);
		}
	}else{
		sortOrderTypeVal="0";
		sortOrderStrVal=Order.asc(sortOrderFieldName);
	}
	sortOrderStrVal=Order.asc("lastName");
	/**End ------------------------------------**/
	int roleId=0;
	int entityID=0;
	boolean isMiami = false;
	DistrictMaster districtMaster=null;
	SchoolMaster schoolMaster=null;
	UserMaster userMaster=null;
	int entityTypeId=1;
	int utype = 1;
	boolean writePrivilegeToSchool =false;
	boolean isManage = true;
	if (session == null || session.getAttribute("userMaster") == null) {
		//return "false";
	}else{
		userMaster=(UserMaster)session.getAttribute("userMaster");
		if(userMaster.getRoleId().getRoleId()!=null){
			roleId=userMaster.getRoleId().getRoleId();
		}
		if(userMaster.getDistrictId()!=null){
			districtMaster=userMaster.getDistrictId();
			if(districtMaster.getDistrictId().equals(1200390))
				isMiami = true;
			writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
		}
		if(userMaster.getSchoolId()!=null){
			schoolMaster =userMaster.getSchoolId();
		}	
		entityID=userMaster.getEntityType();
		System.out.println("===========EntityId : "+entityID);
		utype = entityID;
	}
	
	if(isMiami && entityID==3)
		isManage=false;

	boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
	if(entityID==1)
	{
		achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
	}else
	{
		try{
			DistrictMaster districtMasterObj=districtMaster;
			if(districtMasterObj.getDisplayAchievementScore()){
				achievementScore=true;
			}
			if(districtMasterObj.getDisplayTFA()){
				tFA=true;
			}
			if(districtMasterObj.getDisplayDemoClass()){
				demoClass=true;
			}
			if(districtMasterObj.getDisplayJSI()){
				JSI=true;
			}
			if(districtMasterObj.getDisplayYearsTeaching()){
				teachingOfYear=true;
			}
			if(districtMasterObj.getDisplayExpectedSalary()){
				expectedSalary=true;
			}
			if(districtMasterObj.getDisplayFitScore()){
				fitScore=true;
			}
		}catch(Exception e){ e.printStackTrace();}
	}

	String roleAccess=null;
	try{
		roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,43,"teacherinfo.do",1);
	}catch(Exception e){
		e.printStackTrace();
	}
	Date fDate=null;
	Date tDate=null;
	Date appliedfDate=null;
	Date appliedtDate=null;
	boolean applieddateFlag=false;
	boolean dateFlag=false;
	try{
		// job Applied Filter
		if(jobAppliedFromDate!=null && !jobAppliedFromDate.equals("")){
			appliedfDate=Utility.getCurrentDateFormart(jobAppliedFromDate);
			applieddateFlag=true;
		}
		if(jobAppliedToDate!=null && !jobAppliedToDate.equals("")){
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(Utility.getCurrentDateFormart(jobAppliedToDate));
			cal2.add(Calendar.HOUR,23);
			cal2.add(Calendar.MINUTE,59);
			cal2.add(Calendar.SECOND,59);
			appliedtDate=cal2.getTime();
			applieddateFlag=true;
		}
	}catch(Exception e){
		e.printStackTrace();
	}			
	List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();
	boolean certTypeFlag=false;			
	System.out.println("jobOrderIds:: "+jobOrderIds.size());
	System.out.println("jobOrderIds:: "+jobOrderIds);
	System.out.println("certTypeFlag:: "+certTypeFlag);		
	List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
	List<TeacherDetail> NbyATeacherList = new ArrayList<TeacherDetail>();
	//List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
	boolean teacherFlag=false;			
	boolean nByAflag=false;		
	List<TeacherDetail> subjectTeacherList = new ArrayList<TeacherDetail>();
	boolean subjectFlag=false;

	if(subjectFlag && subjectTeacherList.size()>0){
		if(teacherFlag){
			filterTeacherList.retainAll(subjectTeacherList);
		}else{
			filterTeacherList.addAll(subjectTeacherList);
		}
	}
	System.out.println(" filterTeacherList :: "+filterTeacherList);
	if(subjectFlag){
		teacherFlag=true;
	}





	
	
	
	// Start /////////////////////////////// candidate list By certificate ///////////////////////////////////////
	StateMaster stateMasterForCandidate=null;
	List<CertificateTypeMaster> certTypeMasterListForCndt =new ArrayList<CertificateTypeMaster>();
	List<TeacherDetail> tListByCertificateTypeId = new ArrayList<TeacherDetail>();
	boolean tdataByCertTypeIdFlag = false;	
	
	/************* End **************/
	StatusMaster statusMaster = new StatusMaster();
	boolean status=false;			
	int internalCandVal=0;	
	SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
	SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
	List<JobForTeacher> lstJobForTeacher =	 new ArrayList<JobForTeacher>();
	List<JobOrder> lstJobOrderList =new ArrayList<JobOrder>();
	List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
	int noOfRecordCheck=0;
	
	SchoolMaster sm = schoolMaster;
	List<TeacherDetail> lstTeacherDetailAll= new ArrayList<TeacherDetail>();
	if(entityTypeId!=0){
		if(entityID==3){
			
			lstJobOrderSLList =schoolInJobOrderDAO.allJobOrderPostedBySchool(schoolMaster);
			entityTypeId=3;
		}				
		if(entityTypeId==3 && lstJobOrderSLList.size()>0 && certTypeFlag)
		{
			lstJobOrderList.addAll(getFilterJobOrder(lstJobOrderSLList,jobOrderIds));
		}
		else if(entityTypeId!=3 && lstJobOrderSLList.size()==0 && certTypeFlag)
		{
			lstJobOrderList.addAll(jobOrderIds);
			entityTypeId=3;
		}
		else
		{
			lstJobOrderList.addAll(lstJobOrderSLList);
		}
		if(entityID==2){
			entityTypeId=2;
		}
		
		String stq="";
		if(districtMaster!=null  && utype==3)
		{
			SecondaryStatus smaster = districtMaster.getSecondaryStatus();
			if(smaster!=null)
			{
				String secStatus = smaster.getSecondaryStatusName();
				if(secStatus!=null && !secStatus.equals(""))
				{
					//
					 List<SecondaryStatus> lstStatus = new ArrayList<SecondaryStatus>();
					 lstStatus = secondaryStatusDAO.findSecondaryStatusByName(districtMaster,secStatus);
					 System.out.println("lstStatus.size():::: "+lstStatus.size());
					 for (SecondaryStatus secondaryStatus : lstStatus) {
						 if(secondaryStatus.getJobCategoryMaster()!=null)
						 {
							 stq+=" IF(secondarys3_.jobCategoryId="+secondaryStatus.getJobCategoryMaster().getJobCategoryId()+", ( secondaryStatusId >="+secondaryStatus.getSecondaryStatusId()+" ),false) or";
						 }
					}
					 if(stq.length()>3)
					 {
						 stq = stq.substring(0, stq.length()-2);
					 }
				}
			}
		}
		
		boolean questFlag = false;
		List <DistrictMaster> districtMastersList=new ArrayList<DistrictMaster>();
		if(entityID==1 && entityTypeId==1 && dateFlag==false && applieddateFlag==false && status==false && internalCandVal==0){
			//>>>>>>>>>>>>>>>>>>>>> Job Applied change >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			//System.out.println("lstJobOrderList:::"+lstJobOrderList.size());
			//lstTeacherDetail=jobForTeacherDAO.getTeacherDetailByDASA(sortOrderStrVal,start,noOfRowInPage,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq);
			lstTeacherDetailAll=jobForTeacherDAO.getQuestTeacherDetailRecords(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm);
			//>>>>>>>>>>>>>>>>>>>>> Job Applied change >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			totalRecord=lstTeacherDetailAll.size();					
		}else{
			//shriram
			DistrictMaster dm=null;
			Criterion critdistrict=null;
			if(districtMaster!=null && entityID==2)
			{
				HeadQuarterMaster hq=districtMaster.getHeadQuarterMaster();
				if(hq!=null && hq.getHeadQuarterId()==3)
				{
					flagOhio8 = true;
				Criterion critheadquarter=Restrictions.eq("headQuarterMaster", hq);
				dm= districtMasterDAO.findById(3904380, false, false);
				if(districtMaster.getDistrictId()!=3904380)
				{			critdistrict=  Restrictions.ne("districtId", dm.getDistrictId());
				districtMastersList=districtMasterDAO.findByCriteria(critheadquarter,critdistrict);
				}
				else
					districtMastersList=districtMasterDAO.findByCriteria(critheadquarter);
				System.out.println("Size of DistrictmasterList"+districtMastersList.size());
			}
			}
			//ended by shriram
			if(!(entityTypeId==3 && lstJobOrderList.size()==0))
			{
				//lstTeacherDetail=jobForTeacherDAO.getTeacherDetailByDASA(sortOrderStrVal,start,noOfRowInPage,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq);
				lstTeacherDetailAll=jobForTeacherDAO.getQuestTeacherDetailRecords(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList ,firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm);
			}
			totalRecord=lstTeacherDetailAll.size();
		}
		if(totalRecord<end)
			end=totalRecord;

		
		System.out.println("++++++++++++++++++++++++++++Teacher Details Size:: "+lstTeacherDetailAll.size());
		// sorting on normscore, ascore, lrscore
		if(!sortOrder.equals("tLastName") && !flagOhio8)
		{
			List<TeacherNormScore> teacherNormScoreList = teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetailAll);
			Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
			for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
				normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
			}
			Map<Integer,TeacherAchievementScore> mapAScore = null;

			if(entityID!=1 && achievementScore)
			{
				JobOrder jo=new JobOrder();
				jo.setDistrictMaster(districtMaster);
				List<TeacherAchievementScore> lstTeacherAchievementScore=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetailAll,jo);

				mapAScore = new HashMap<Integer, TeacherAchievementScore>();

				for(TeacherAchievementScore tAScore : lstTeacherAchievementScore){
					mapAScore.put(tAScore.getTeacherDetail().getTeacherId(),tAScore);
				}
			}
			TeacherAchievementScore tAScore=null;

			TeacherNormScore teacherNormScore = null;
			for(TeacherDetail teacherDetail : lstTeacherDetailAll)
			{
				teacherNormScore = normMap.get(teacherDetail.getTeacherId());
				if(teacherNormScore==null)
					teacherDetail.setNormScore(-0);
				else
					teacherDetail.setNormScore(teacherNormScore.getTeacherNormScore());

				if(entityID!=1 && achievementScore)
				{
					tAScore=mapAScore.get(teacherDetail.getTeacherId());
					if(tAScore!=null){
						if(tAScore.getAcademicAchievementScore()!=null)
							teacherDetail.setaScore(tAScore.getAcademicAchievementScore());

						if(tAScore.getLeadershipAchievementScore()!=null)
							teacherDetail.setlRScore(tAScore.getLeadershipAchievementScore());
					}else{
						teacherDetail.setaScore(-1);
						teacherDetail.setlRScore(-1);
					}
				}
			}

			if(sortOrderFieldName.equals("normScore") && sortOrderType.equals("0")){
				Collections.sort(lstTeacherDetailAll,TeacherDetail.teacherDetailComparatorNormScoreDesc );
			}else if(sortOrderFieldName.equals("normScore") && sortOrderType.equals("1")){
				Collections.sort(lstTeacherDetailAll,TeacherDetail.teacherDetailComparatorNormScore);
			}else if(sortOrderFieldName.equals("aScore") && sortOrderType.equals("1")){				
				Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorAScore );
			}else if(sortOrderFieldName.equals("aScore") && sortOrderType.equals("0")){				
				Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorAScoreDesc );				
			}else if(sortOrderFieldName.equals("lRScore") && sortOrderType.equals("1")){				
				Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorLRScore );
			}else if(sortOrderFieldName.equals("lRScore") && sortOrderType.equals("0")){				
				Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorLRScoreDesc );				
			}

			lstTeacherDetail=lstTeacherDetailAll;
		}
		else
		{
			if(sortOrder.equals("tLastName")){
				for(TeacherDetail td:lstTeacherDetailAll)
				{
						td.settLastName(td.getLastName());
				}
				
				if(sortOrderType.equals("0"))
					Collections.sort(lstTeacherDetailAll,new TeacherDetailComparatorLastNameAsc());
				else
					Collections.sort(lstTeacherDetailAll,new TeacherDetailComparatorLastNameDesc());
			}
		
			lstTeacherDetail=lstTeacherDetailAll;
		}



		List<Integer> lstStatusMasters = new ArrayList<Integer>();
		String[] statuss = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
		try{
			lstStatusMasters = statusMasterDAO.findStatusIdsByStatusByShortNames(statuss);
		}catch (Exception e) {
			e.printStackTrace();
		}
		if(totalRecord!=0){
			lstJobForTeacher=jobForTeacherDAO.getJobForTeacherByDSTeachersList(lstStatusMasters,lstTeacherDetail,lstJobOrderSLList,null,null,districtMaster,entityID);
			for(JobForTeacher jobForTeacher : lstJobForTeacher){
				jftMap.put(jobForTeacher.getJobForTeacherId(),jobForTeacher);
			}
			lstJobForTeacher=jobForTeacherDAO.getJobForTeacherByDSTeachersList(lstStatusMasters,lstTeacherDetail,lstJobOrderSLList,null,null,districtMaster,1);
			for(JobForTeacher jobForTeacher : lstJobForTeacher){
				jftMapForTm.put(jobForTeacher.getJobForTeacherId(),jobForTeacher);
			}
		}
	}
	String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
	List<TeacherNormScore> teacherNormScoreList=teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetail);
	Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
	for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
		normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
	}

	Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();	
	Map<Integer,Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
	List<TeacherExperience> lstTeacherExperience=teacherExperienceDAO.findExperienceForTeachers(lstTeacherDetail);		

	// ************** Start ... IDENTIFY Internal Candidate *********************
	Map<Integer,Boolean> mapInternalCandidate = new HashMap<Integer, Boolean>();
	if(entityID!=1)
	{
		List<JobForTeacher> lstjobForTeachers=jobForTeacherDAO.findInternalCandidate(lstTeacherDetail);
		if(entityID==2)
		{
			for(JobForTeacher pojo:lstjobForTeachers){
				if(districtMaster.equals(pojo.getJobId().getDistrictMaster()))
				{
					mapInternalCandidate.put(pojo.getTeacherId().getTeacherId(), true);
				}
			}
		}
		else if(entityID==3)
		{
			Map<Integer,Boolean> mapJobOrder = new HashMap<Integer, Boolean>();
			List<JobOrder> lstJobOrders=new ArrayList<JobOrder>();
			for(JobForTeacher pojo:lstjobForTeachers){
				if(districtMaster.equals(pojo.getJobId().getDistrictMaster()))
					lstJobOrders.add(pojo.getJobId());
			}
			List<SchoolInJobOrder> lstSchoolInJobOrders= new ArrayList<SchoolInJobOrder>();
			if(schoolMaster!=null)
			 lstSchoolInJobOrders=schoolInJobOrderDAO.findInternalCandidate(lstJobOrders, schoolMaster);
			
			if(lstSchoolInJobOrders.size()>0){
				for(SchoolInJobOrder pojo:lstSchoolInJobOrders)
					mapJobOrder.put(pojo.getJobId().getJobId(), true);
			}

			for(JobForTeacher pojo:lstjobForTeachers){
				if(mapJobOrder.get(pojo.getJobId().getJobId())!=null && mapJobOrder.get(pojo.getJobId().getJobId()).equals(true)){
					mapInternalCandidate.put(pojo.getTeacherId().getTeacherId(), true);
				}
			}
		}
	}
	// ************** End ... IDENTIFY Internal Candidate *********************

	List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
	Integer isBase = null;
	try{
		if(lstTeacherDetail!=null && lstTeacherDetail.size()>0){
			teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(lstTeacherDetail);
			for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
				baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
				boolean cgflag = teacherAssessmentStatus2.getCgUpdated()==null?false:teacherAssessmentStatus2.getCgUpdated();
				if(cgflag && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
				{
					isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
				}
				else
				{
					if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
					{
						isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 2);
					}
				}
			}
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	
	    //alok kumar			
		Map<Integer,List<Integer>> jsiCount	=	new HashMap<Integer, List<Integer>>();
		Map<Integer,List<Integer>> epiCount	=	new HashMap<Integer, List<Integer>>();
		
		List<TeacherAssessmentStatus> teacherAssessmentJsi = null;
		List<TeacherAssessmentStatus> teacherAssessmentEpi = null;
		List<Integer> teacherId = new ArrayList();
		List<Integer> matchTeacherId = null;
		try
		{
			if(lstTeacherDetail!=null && lstTeacherDetail.size()>0)
			{

				for(TeacherDetail t:lstTeacherDetail)
				{
					teacherId.add(t.getTeacherId());
				}	
				teacherAssessmentJsi = teacherAssessmentStatusDAO.findAssessmentJsiByTeachers(lstTeacherDetail);
				teacherAssessmentEpi=teacherAssessmentStatusDAO.findAssessmentEpiByTeachers(lstTeacherDetail);						
				if(teacherAssessmentJsi.size()>0)
				{
					for(Integer id :teacherId)
					{
						matchTeacherId=new ArrayList();
						for(TeacherAssessmentStatus assessmentteacherId:teacherAssessmentJsi)
						{								  
						  if(id.equals(assessmentteacherId.getTeacherDetail().getTeacherId()))
						  {
							  matchTeacherId.add(assessmentteacherId.getTeacherDetail().getTeacherId());
						  }
						}
						if(matchTeacherId.size()>0)
						jsiCount.put(id, matchTeacherId);							
					}
				}
				if(teacherAssessmentEpi.size()>0)
				{
					for(Integer id :teacherId)
					{
						matchTeacherId=new ArrayList();
						for(TeacherAssessmentStatus assessmentteacherId:teacherAssessmentEpi)
						{
						  if(id.equals(assessmentteacherId.getTeacherDetail().getTeacherId()))
						  {
							  matchTeacherId.add(assessmentteacherId.getTeacherDetail().getTeacherId());
						  }
						}
						if(matchTeacherId.size()>0)
						epiCount.put(id, matchTeacherId);							
					}
				}
			}				
		}			
		catch(Exception e)
		{e.printStackTrace();}
		
	
	String tFname="",tLname="";
	String responseText="";
	String schoolDistrict="District";
	if(entityID==3){
		schoolDistrict="School";
	}
	List<TeacherPersonalInfo> lstTeacherPersonalInfo = new ArrayList<TeacherPersonalInfo>();
	List<TeacherAchievementScore> lstTeacherAchievementScore = new ArrayList<TeacherAchievementScore>();			
	List<DistrictSpecificQuestions> lstdistrictSpecificQuestions = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(userMaster.getDistrictId());
	boolean schoolCheckFlag=false;
	if(userMaster.getEntityType()==3)
	{
		try
		{
			schoolMaster = schoolMasterDAO.findById(userMaster.getSchoolId().getSchoolId(), false, false);
			DistrictSchools districtSchools = districtSchoolsDAO.findBySchoolMaster(schoolMaster);
			if(districtSchools.getDistrictMaster().getResetQualificationIssuesPrivilegeToSchool()==true)
			{
				schoolCheckFlag = true;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	int counter=0;
	TeacherNormScore teacherNormScore = null;
	
	
	List<Integer>teacherIdLst=new ArrayList<Integer>();
	for (TeacherDetail teacherDetail : lstTeacherDetail) 
	{
		teacherIdLst.add(teacherDetail.getTeacherId());
	}
	Criterion criterionTeacherPersonalInfo=Restrictions.in("teacherId", teacherIdLst);
	List<TeacherPersonalInfo>teacherPersonalInfos=null;
	HashMap<Integer, TeacherPersonalInfo> map=new HashMap<Integer, TeacherPersonalInfo>();
	if(teacherIdLst!=null && teacherIdLst.size()>0){
		teacherPersonalInfos=teacherPersonalInfoDAO.findByCriteria(criterionTeacherPersonalInfo);
		for (TeacherPersonalInfo pojo : teacherPersonalInfos) 
		{
			map.put(pojo.getTeacherId(), pojo);
		}
	}	
	//alok
	Map<Integer, EmployeeMaster> empMap = new HashMap<Integer, EmployeeMaster>();
	Map<String,Boolean> mapAlum = new HashMap<String, Boolean>(); // For Alum
	if(entityID!=1)
	{
		String ssn="";				
		Map<Integer, TeacherPersonalInfo>tpInfomap=new  HashMap<Integer, TeacherPersonalInfo>();
		List<String> teacherFNameList = new ArrayList<String>();
		List<String> teacherLNameList = new ArrayList<String>();
		List<String> teacherSSNList = new ArrayList<String>();
		if(teacherPersonalInfos!=null && teacherPersonalInfos.size()>0){				
			for(TeacherPersonalInfo pojo :teacherPersonalInfos) {
				tpInfomap.put(pojo.getTeacherId(), pojo); 
				ssn = pojo.getSSN()==null?"":pojo.getSSN().trim();
				if(!ssn.equals(""))
				{
					teacherFNameList.add(pojo.getFirstName().trim());
					teacherLNameList.add(pojo.getLastName().trim());
					ssn = ssn.trim();
					try {
						ssn = Utility.decodeBase64(ssn);
					} catch (IOException e) {
						e.printStackTrace();
					}
					if(ssn.length()>4)
						ssn = ssn.substring(ssn.length() - 4, ssn.length());
					teacherSSNList.add(ssn.trim());
				}
			}
		}
		List<EmployeeMaster> employeeMasters =	employeeMasterDAO.checkEmployeeByFistName(teacherFNameList,teacherLNameList,teacherSSNList,districtMaster);
		mapAlum=cgService.getMapAlum(employeeMasters, districtMaster); // get Alum Map
		
		if(teacherPersonalInfos!=null && teacherPersonalInfos.size()>0 && employeeMasters.size()>0){				
			for(TeacherPersonalInfo pojo :teacherPersonalInfos) {
				ssn = pojo.getSSN()==null?"":pojo.getSSN().trim();
				if(!ssn.equals(""))
				{
					ssn = ssn.trim();
					try {
						ssn = Utility.decodeBase64(ssn);
					} catch (IOException e) {
						e.printStackTrace();
					}
					if(ssn.length()>4)
						ssn = ssn.substring(ssn.length() - 4, ssn.length());
					String empNo = pojo.getEmployeeNumber()==null?"":pojo.getEmployeeNumber().trim();
					
					for(EmployeeMaster emp :employeeMasters) {
						if(empNo.equals(""))// Not an employee
						{
							if(emp.getSSN().equalsIgnoreCase(ssn) && emp.getFirstName().equalsIgnoreCase(pojo.getFirstName()) && emp.getLastName().equalsIgnoreCase(pojo.getLastName()))
							{
								empMap.put(pojo.getTeacherId(), emp);
								break;
							}
						}
						else 
						{
							if(emp.getEmployeeCode().equalsIgnoreCase(empNo) && emp.getSSN().equalsIgnoreCase(ssn) && emp.getFirstName().equalsIgnoreCase(pojo.getFirstName()) && emp.getLastName().equalsIgnoreCase(pojo.getLastName()))
							{
								empMap.put(pojo.getTeacherId(), emp);
								break;
							}
						}
					}
				}
			}
		}
	}			
	//Gourav  districtMaster
	List<SecondaryStatusMaster> lstSecondaryStatusMaster = secondaryStatusMasterDAO.findTagswithDistrictInCandidatePool(districtMaster);
	Criterion district_cri = Restrictions.eq("districtMaster", districtMaster);
	Criterion school_cri = Restrictions.isNull("schoolMaster");
	Criterion job_cri = Restrictions.isNull("jobOrder");
	
	List<TeacherSecondaryStatus> lstMltTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByCriteria(district_cri,school_cri,job_cri);
	Map<String, TeacherSecondaryStatus> teachersTagsExistMap = new HashMap<String, TeacherSecondaryStatus>();
	if(lstMltTeacherSecondaryStatus.size()>0){
		for (TeacherSecondaryStatus teacherSecondaryStatus : lstMltTeacherSecondaryStatus) {
			String teacherTagKey = teacherSecondaryStatus.getTeacherDetail().getTeacherId()+"##"+teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusId();
			teachersTagsExistMap.put(teacherTagKey, teacherSecondaryStatus);
		}
	}
	
	if(lstTeacherDetail.size()==0){}
//		tmRecords.append("<tr><td colspan='9'>No Teacher Found</td></tr>" );//Shriram No Record Found
	
	else
	{
		if(entityID!=1)
		{
			JobOrder jobOrder = new JobOrder();
			jobOrder.setDistrictMaster(districtMaster);
			lstTeacherAchievementScore=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetail,jobOrder);
		}
		lstTeacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPInfoForTeacher(lstTeacherDetail);
	}
	Map<Integer,Boolean> qqTeachersMap = new HashMap<Integer, Boolean>(); 
	if(lstTeacherDetail.size()>0 && entityTypeId!=1 && lstdistrictSpecificQuestions.size()>0)
	{
		List<TeacherDetail> qqAttemptedTeachers =  teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findQQStatusByTeacher(lstTeacherDetail,districtMaster,true);
		List<TeacherDetail> invalidQQTeachers = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findQQStatusByTeacher(lstTeacherDetail,districtMaster,false);
		for (TeacherDetail teacherDetail : qqAttemptedTeachers) {
			qqTeachersMap.put(teacherDetail.getTeacherId(), true);
		}
		for (TeacherDetail teacherDetail : invalidQQTeachers) {
			qqTeachersMap.put(teacherDetail.getTeacherId(), false);
		}
	}

	// teacherwiseassessmenttime
	Map<Integer, TeacherWiseAssessmentTime> teacherWMap = new HashMap<Integer, TeacherWiseAssessmentTime>();
	if(lstTeacherDetail.size()>0){
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		Criterion teacherListCR = Restrictions.in("teacherDetail", lstTeacherDetail);
		Criterion assessmenType		= Restrictions.eq("assessmentType", 1);
		Criterion currentYear = Restrictions.between("createdDateTime", sDate, eDate);
		
		List<TeacherWiseAssessmentTime> teacherWiseAssessmentTime = teacherWiseAssessmentTimeDAO.findByCriteria(Order.desc("assessmentTimeId"),teacherListCR,assessmenType);
		
		if(teacherWiseAssessmentTime.size()>0){
			for (TeacherWiseAssessmentTime teacherWiseAssessmentTime2 : teacherWiseAssessmentTime) {
				if(!teacherWMap.containsKey(teacherWiseAssessmentTime2.getTeacherDetail().getTeacherId())){
					teacherWMap.put(teacherWiseAssessmentTime2.getTeacherDetail().getTeacherId(), teacherWiseAssessmentTime2);
				}
			}
		}
	}
	
	String sCheckBoxValue="";
	Map<Integer,Boolean> finalizedTMap = new HashMap<Integer, Boolean>();
	List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
	if(districtMaster!=null && lstTeacherDetail.size()>0)
	{
		lstTeacher = jobForTeacherDAO.getQQFinalizedTeacherList(lstTeacherDetail,districtMaster);
		for (TeacherDetail teacherDetail : lstTeacher) {
			finalizedTMap.put(teacherDetail.getTeacherId(), true);
		}
	}
	////////////////////////////////Add disable profile( Sekhar )///////////////////////////////
	Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
	System.out.println("=============================="+lstTeacherDetail.size());
	List<TeacherStatusHistoryForJob> historyForJobList = teacherStatusHistoryForJobDAO.findHireTeachersWithDistrictListAndTeacherList(lstTeacherDetail,districtMaster);
	List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherForOffers(lstTeacherDetail);
	List<String> posList=new ArrayList<String>();
	Map<String,String> reqMap=new HashMap<String, String>();
	for (JobForTeacher jobForTeacher : jftOffers){
		try{
			if(jobForTeacher.getRequisitionNumber()!=null){
				posList.add(jobForTeacher.getRequisitionNumber());
				reqMap.put(jobForTeacher.getJobId().getJobId()+"#"+jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getRequisitionNumber());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(districtMaster,posList);
	Map<String,DistrictRequisitionNumbers> dReqNoMap = new HashMap<String,DistrictRequisitionNumbers>();
	try{
		for (DistrictRequisitionNumbers districtRequisitionNumbers2 : districtRequisitionNumbers) {
			dReqNoMap.put(districtRequisitionNumbers2.getRequisitionNumber(),districtRequisitionNumbers2);
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	if(historyForJobList.size()>0){
		mapForHiredTeachers=cgService.getHiredList(reqMap,dReqNoMap,lstTeacherDetail,historyForJobList,null);
	}

	
	String fontPath = realPath;
	try {

		BaseFont.createFont(fontPath + "fonts/4637.ttf",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		BaseFont tahoma = BaseFont.createFont(fontPath
				+ "fonts/4637.ttf", BaseFont.CP1252,
				BaseFont.NOT_EMBEDDED);
		font8 = new Font(tahoma, 8);

		font8Green = new Font(tahoma, 8);
		font8Green.setColor(Color.BLUE);
		font8bold = new Font(tahoma, 8, Font.NORMAL);

		font9 = new Font(tahoma, 9);
		font9bold = new Font(tahoma, 9, Font.BOLD);
		font10 = new Font(tahoma, 10);

		font10_10 = new Font(tahoma, 10);
		font10_10.setColor(Color.white);
		font10bold = new Font(tahoma, 10, Font.BOLD);
		font11 = new Font(tahoma, 11);

		font11bold = new Font(tahoma, 11, Font.BOLD);
		bluecolor = new Color(0, 122, 180);

		// Color bluecolor = new Color(0,122,180);

		font20bold = new Font(tahoma, 20, Font.BOLD, bluecolor);
		// font20bold.setColor(Color.BLUE);
		font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);

	} catch (DocumentException e1) {
		e1.printStackTrace();
	} catch (IOException e1) {
		e1.printStackTrace();
	}

	document = new Document(PageSize.A4.rotate(), 10f, 10f, 10f, 10f);
	document.addAuthor("TeacherMatch");
	document.addCreator("TeacherMatch Inc.");
	document.addSubject("Ohio 8 Candidate List");
	document.addCreationDate();
	document.addTitle("Ohio 8 Candidate List");

	fos = new FileOutputStream(path);
	PdfWriter.getInstance(document, fos);

	document.open();

	PdfPTable mainTable = new PdfPTable(1);
	mainTable.setWidthPercentage(90);

	Paragraph[] para = null;
	PdfPCell[] cell = null;

	para = new Paragraph[3];
	cell = new PdfPCell[3];
	para[0] = new Paragraph(" ", font20bold);
	cell[0] = new PdfPCell(para[0]);
	cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
	cell[0].setBorder(0);
	mainTable.addCell(cell[0]);
	Image logo = Image.getInstance(fontPath
			+ "/images/Logo with Beta300.png");
	logo.scalePercent(75);

	// para[1] = new Paragraph(" ",font20bold);
	cell[1] = new PdfPCell(logo);
	cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
	cell[1].setBorder(0);
	mainTable.addCell(cell[1]);

	document.add(new Phrase("\n"));

	para[2] = new Paragraph("", font20bold);
	cell[2] = new PdfPCell(para[2]);
	cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
	cell[2].setBorder(0);
	mainTable.addCell(cell[2]);

	document.add(mainTable);

	document.add(new Phrase("\n"));

	float[] tblwidthz = { .15f };

	mainTable = new PdfPTable(tblwidthz);
	mainTable.setWidthPercentage(100);
	para = new Paragraph[1];
	cell = new PdfPCell[1];

	para[0] = new Paragraph("Ohio 8 Candidate List", font20bold);
	cell[0] = new PdfPCell(para[0]);
	cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
	cell[0].setBorder(0);
	mainTable.addCell(cell[0]);
	document.add(mainTable);

	document.add(new Phrase("\n"));

	float[] tblwidths = { .15f, .20f };

	mainTable = new PdfPTable(tblwidths);
	mainTable.setWidthPercentage(100);
	para = new Paragraph[2];
	cell = new PdfPCell[2];

	String name1 = userMaster.getFirstName() + " "
			+ userMaster.getLastName();

	para[0] = new Paragraph("Created By: " + name1, font10);
	cell[0] = new PdfPCell(para[0]);
	cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
	cell[0].setBorder(0);
	mainTable.addCell(cell[0]);

	para[1] = new Paragraph("" + "Created on: "
			+ Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),
			font10);
	cell[1] = new PdfPCell(para[1]);
	cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
	cell[1].setBorder(0);
	mainTable.addCell(cell[1]);

	document.add(mainTable);

	document.add(new Phrase("\n"));

	float[] tblwidth ={ .06f, .10f, .07f, .06f, .06f};
	float[] tblwidthOhio ={ .06f, .10f, .07f, .06f};
	/*if(!flagOhio8)
	{
		tblwieth= { .06f, .10f, .07f, .06f, .06f};
	}
	else
	{
		tblwidthOhio= { .06f, .10f, .07f, .06f};
	}*/
	
	if(!flagOhio8)
	mainTable = new PdfPTable(tblwidth);
	else
		mainTable = new PdfPTable(tblwidthOhio);
	mainTable.setWidthPercentage(100);
	para = new Paragraph[5];
	cell = new PdfPCell[5];
	// header

	// Shriram////
	para[0] = new Paragraph("" + lblCandidateFirstName, font10_10);
	cell[0] = new PdfPCell(para[0]);
	cell[0].setBackgroundColor(bluecolor);
	cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
	mainTable.addCell(cell[0]);
	
	para[1] = new Paragraph("" + lblCandidateLastName, font10_10);
	cell[1] = new PdfPCell(para[1]);
	cell[1].setBackgroundColor(bluecolor);
	cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
	mainTable.addCell(cell[1]);
	
	para[2] = new Paragraph("" + lblCandidateEmailAddress, font10_10);
	cell[2] = new PdfPCell(para[2]);
	cell[2].setBackgroundColor(bluecolor);
	cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
	mainTable.addCell(cell[2]);
 if(!flagOhio8)
 {
	para[3] = new Paragraph("" + "Norm Score", font10_10);
	cell[3] = new PdfPCell(para[3]);
	cell[3].setBackgroundColor(bluecolor);
	cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
	mainTable.addCell(cell[3]);

	para[4] = new Paragraph("" + "# of Jobs", font10_10);
	cell[4] = new PdfPCell(para[4]);
	cell[4].setBackgroundColor(bluecolor);
	cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
	mainTable.addCell(cell[4]);
 }
 else
 {
	 para[3] = new Paragraph("" + "# of Jobs", font10_10);
		cell[3] = new PdfPCell(para[3]);
		cell[3].setBackgroundColor(bluecolor);
		cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
		mainTable.addCell(cell[3]);
	 
 }
 

	document.add(mainTable);

	
	String sta = "";
	if(lstTeacherDetail!=null && lstTeacherDetail.size()>0)
	{
		System.out.println(" Teachers List Get ");//Shriram Teacher List Get
			for (TeacherDetail teacherDetail : lstTeacherDetail) 
						   {
				teacherNormScore = normMap.get(teacherDetail.getTeacherId());
				int normScore = 0;
				String colorName = "";
				if(teacherNormScore!=null)
				{
					normScore = teacherNormScore.getTeacherNormScore();
				}
				noOfRecordCheck++;
				tFname=teacherDetail.getFirstName(); //Teacher first name	
				tLname=teacherDetail.getLastName();//Teacher Last Name
			
				//gourav  tag loop start
				StringBuffer tagNames =	new StringBuffer();
				if(roleId != 1){}
				//Add profile hover

				String profileNormScore="";
				
				
			/* @Start
			 * @Ashish Kumar
			 * @Description :: Qualification Details Thumb 
			 * */
				TeacherPersonalInfo tempTeacher=map.get(teacherDetail.getTeacherId());
				
				
				boolean CheckForIssue = false; 
				
				if(userMaster.getEntityType()==2)
				{
					CheckForIssue = true;
				}
				
				if(schoolCheckFlag)
					CheckForIssue = true;
				
				if(CheckForIssue == true)
				{
					if(lstdistrictSpecificQuestions.size()>0)
					{
						Boolean qqTaken = qqTeachersMap.get(teacherDetail.getTeacherId());
						if(qqTaken!=null)
						{
							Boolean finalizeQQ = finalizedTMap.get(teacherDetail.getTeacherId());
							finalizeQQ = finalizeQQ==null?false:finalizeQQ;
							
						}
							
					}
				}
				
				int jobList=jobListByTeacher(teacherDetail.getTeacherId(),jftMap);
				int jobListForTm=jobListByTeacher(teacherDetail.getTeacherId(),jftMapForTm);
						String[] row = null;
						int index = 0;
						
						if(!flagOhio8)
						mainTable = new PdfPTable(tblwidth);
						else
							mainTable = new PdfPTable(tblwidthOhio);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[8];
						cell = new PdfPCell[8];
			
						String myVal1 = "";
						String myVal2 = "";
						
						counter++;
						
						para[index] = new Paragraph(tFname, font8bold);
						cell[index] = new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
			
					
						para[index] = new Paragraph(tLname, font8bold);
						cell[index] = new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
			
						
						para[index] = new Paragraph(teacherDetail.getEmailAddress(), font8bold);
						cell[index] = new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						if(!flagOhio8)
						{
						if(normScore!=0){
							profileNormScore=normScore+"";
							para[index] = new Paragraph(profileNormScore, font8bold);
							cell[index] = new PdfPCell(para[index]);
							cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							mainTable.addCell(cell[index]);
							index++;
						}else{
							profileNormScore="N/A";
							para[index] = new Paragraph(profileNormScore, font8bold);
							cell[index] = new PdfPCell(para[index]);
							cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							mainTable.addCell(cell[index]);
							index++;
						}
						}
					
						
						if(jobList!=0){   
							para[index] = new Paragraph(jobList+"/"+(jobListForTm-jobList)+"", font8bold);
							cell[index] = new PdfPCell(para[index]);
							cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							mainTable.addCell(cell[index]);
							index++;
						}else{
							para[index] = new Paragraph("0"+"/"+(jobListForTm-jobList)+"", font8bold);
							cell[index] = new PdfPCell(para[index]);
							cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							mainTable.addCell(cell[index]);
							index++;
						}
				
						document.add(mainTable);
					}
	}
			else
			{

				float[] tblwidth11 = { .10f };

				mainTable = new PdfPTable(tblwidth11);
				mainTable.setWidthPercentage(100);
				para = new Paragraph[1];
				cell = new PdfPCell[1];

				para[0] = new Paragraph(lblNoRecord, font8bold);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[0]);

				document.add(mainTable);
			
			}
	}
catch (Exception e) {
	e.printStackTrace();
} finally {
	if (document != null && document.isOpen())
		document.close();
	if (writer != null) {
		writer.flush();
		writer.close();
	}
}

return true;
}

////shriram PRINT PREVIEW 


public String displayOhioCandidateListPrintPreview(String firstName,String lastName, String emailAddress,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String jobAppliedFromDate,String jobAppliedToDate,int daysVal)
{
	System.out.println("jobAppliedFromDate\t"+jobAppliedFromDate);
	System.out.println("jobAppliedToDate\t"+jobAppliedToDate);
	System.out.println("daysVal\t"+daysVal);
	int cellSize = 0;
	Document document = null;
	FileOutputStream fos = null;
	PdfWriter writer = null;
	Paragraph footerpara = null;
	HeaderFooter headerFooter = null;
	Font font8 = null;
	Font font8Green = null;
	Font font8bold = null;
	Font font9 = null;
	Font font9bold = null;
	Font font10 = null;
	Font font10_10 = null;
	Font font10bold = null;
	Font font11 = null;
	Font font11bold = null;
	Font font20bold = null;
	Font font11b = null;
	Color bluecolor = null;
	Font font8bold_new = null;
	/*
	
	///////////////////////////////////////////////////////////////////////////////
	String fileName = null;
	/* ========  For Session time Out Error =========*/
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp); 
	}
	//-- get no of record in grid,
	//-- set start and end position

	int noOfRowInPage = Integer.parseInt(noOfRow);
	int pgNo = Integer.parseInt(pageNo);
	int start = ((pgNo-1)*noOfRowInPage);
	int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
	int totalRecord = 0;
	boolean flagOhio8 = false;
	//------------------------------------
	StringBuffer tmRecords =	new StringBuffer();
	CandidateGridService cgService=new CandidateGridService();
	try{
		List<TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();

		/** set default sorting fieldName **/
		String sortOrderFieldName	=	"lastName";
		String sortOrderNoField		=	"lastName";

		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal=null;

		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
		}

		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
		sortOrderStrVal=Order.asc("lastName");
		/**End ------------------------------------**/
		int roleId=0;
		int entityID=0;
		boolean isMiami = false;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityTypeId=1;
		int utype = 1;
		boolean writePrivilegeToSchool =false;
		boolean isManage = true;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
				if(districtMaster.getDistrictId().equals(1200390))
					isMiami = true;
				writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
			System.out.println("===========EntityId : "+entityID);
			utype = entityID;
		}
		
		if(isMiami && entityID==3)
			isManage=false;

		boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
		if(entityID==1)
		{
			achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
		}else
		{
			try{
				DistrictMaster districtMasterObj=districtMaster;
				if(districtMasterObj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(districtMasterObj.getDisplayTFA()){
					tFA=true;
				}
				if(districtMasterObj.getDisplayDemoClass()){
					demoClass=true;
				}
				if(districtMasterObj.getDisplayJSI()){
					JSI=true;
				}
				if(districtMasterObj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				if(districtMasterObj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				if(districtMasterObj.getDisplayFitScore()){
					fitScore=true;
				}
			}catch(Exception e){ e.printStackTrace();}
		}

		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,43,"teacherinfo.do",1);
		}catch(Exception e){
			e.printStackTrace();
		}
		Date fDate=null;
		Date tDate=null;
		Date appliedfDate=null;
		Date appliedtDate=null;
		boolean applieddateFlag=false;
		boolean dateFlag=false;
		try{
			// job Applied Filter
			if(jobAppliedFromDate!=null && !jobAppliedFromDate.equals("")){
				appliedfDate=Utility.getCurrentDateFormart(jobAppliedFromDate);
				applieddateFlag=true;
			}
			if(jobAppliedToDate!=null && !jobAppliedToDate.equals("")){
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(Utility.getCurrentDateFormart(jobAppliedToDate));
				cal2.add(Calendar.HOUR,23);
				cal2.add(Calendar.MINUTE,59);
				cal2.add(Calendar.SECOND,59);
				appliedtDate=cal2.getTime();
				applieddateFlag=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}			
		List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();
		boolean certTypeFlag=false;			
		System.out.println("jobOrderIds:: "+jobOrderIds.size());
		System.out.println("jobOrderIds:: "+jobOrderIds);
		System.out.println("certTypeFlag:: "+certTypeFlag);		
		List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
		List<TeacherDetail> NbyATeacherList = new ArrayList<TeacherDetail>();
		//List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
		boolean teacherFlag=false;			
		boolean nByAflag=false;		
		List<TeacherDetail> subjectTeacherList = new ArrayList<TeacherDetail>();
		boolean subjectFlag=false;

		if(subjectFlag && subjectTeacherList.size()>0){
			if(teacherFlag){
				filterTeacherList.retainAll(subjectTeacherList);
			}else{
				filterTeacherList.addAll(subjectTeacherList);
			}
		}
		System.out.println(" filterTeacherList :: "+filterTeacherList);
		if(subjectFlag){
			teacherFlag=true;
		}
		// Start /////////////////////////////// candidate list By certificate ///////////////////////////////////////
		StateMaster stateMasterForCandidate=null;
		List<CertificateTypeMaster> certTypeMasterListForCndt =new ArrayList<CertificateTypeMaster>();
		List<TeacherDetail> tListByCertificateTypeId = new ArrayList<TeacherDetail>();
		boolean tdataByCertTypeIdFlag = false;	
		
		/************* End **************/
		StatusMaster statusMaster = new StatusMaster();
		boolean status=false;			
		int internalCandVal=0;	
		SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
		SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
		List<JobForTeacher> lstJobForTeacher =	 new ArrayList<JobForTeacher>();
		List<JobOrder> lstJobOrderList =new ArrayList<JobOrder>();
		List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
		int noOfRecordCheck=0;
		
		SchoolMaster sm = schoolMaster;
		List<TeacherDetail> lstTeacherDetailAll= new ArrayList<TeacherDetail>();
		if(entityTypeId!=0){
			if(entityID==3){
				
				lstJobOrderSLList =schoolInJobOrderDAO.allJobOrderPostedBySchool(schoolMaster);
				entityTypeId=3;
			}				
			if(entityTypeId==3 && lstJobOrderSLList.size()>0 && certTypeFlag)
			{
				lstJobOrderList.addAll(getFilterJobOrder(lstJobOrderSLList,jobOrderIds));
			}
			else if(entityTypeId!=3 && lstJobOrderSLList.size()==0 && certTypeFlag)
			{
				lstJobOrderList.addAll(jobOrderIds);
				entityTypeId=3;
			}
			else
			{
				lstJobOrderList.addAll(lstJobOrderSLList);
			}
			if(entityID==2){
				entityTypeId=2;
			}
			
			String stq="";
			if(districtMaster!=null  && utype==3)
			{
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(smaster!=null)
				{
					String secStatus = smaster.getSecondaryStatusName();
					if(secStatus!=null && !secStatus.equals(""))
					{
						//
						 List<SecondaryStatus> lstStatus = new ArrayList<SecondaryStatus>();
						 lstStatus = secondaryStatusDAO.findSecondaryStatusByName(districtMaster,secStatus);
						 System.out.println("lstStatus.size():::: "+lstStatus.size());
						 for (SecondaryStatus secondaryStatus : lstStatus) {
							 if(secondaryStatus.getJobCategoryMaster()!=null)
							 {
								 stq+=" IF(secondarys3_.jobCategoryId="+secondaryStatus.getJobCategoryMaster().getJobCategoryId()+", ( secondaryStatusId >="+secondaryStatus.getSecondaryStatusId()+" ),false) or";
							 }
						}
						 if(stq.length()>3)
						 {
							 stq = stq.substring(0, stq.length()-2);
						 }
					}
				}
			}
			
			boolean questFlag = false;
			List <DistrictMaster> districtMastersList=new ArrayList<DistrictMaster>();
			if(entityID==1 && entityTypeId==1 && dateFlag==false && applieddateFlag==false && status==false && internalCandVal==0){
				//>>>>>>>>>>>>>>>>>>>>> Job Applied change >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//System.out.println("lstJobOrderList:::"+lstJobOrderList.size());
				//lstTeacherDetail=jobForTeacherDAO.getTeacherDetailByDASA(sortOrderStrVal,start,noOfRowInPage,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq);
				lstTeacherDetailAll=jobForTeacherDAO.getQuestTeacherDetailRecords(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm);
				//>>>>>>>>>>>>>>>>>>>>> Job Applied change >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				
				totalRecord=lstTeacherDetailAll.size();					
			}else{
				//shriram
				DistrictMaster dm=null;
				Criterion critdistrict=null;
				if(districtMaster!=null && entityID==2)
				{
					HeadQuarterMaster hq=districtMaster.getHeadQuarterMaster();
					if(hq!=null && hq.getHeadQuarterId()==3)
					{
						flagOhio8 = true;
					Criterion critheadquarter=Restrictions.eq("headQuarterMaster", hq);
					dm= districtMasterDAO.findById(3904380, false, false);
					if(districtMaster.getDistrictId()!=3904380)
					{			critdistrict=  Restrictions.ne("districtId", dm.getDistrictId());
					districtMastersList=districtMasterDAO.findByCriteria(critheadquarter,critdistrict);
					}
					else
						districtMastersList=districtMasterDAO.findByCriteria(critheadquarter);
					System.out.println("Size of DistrictmasterList"+districtMastersList.size());
					}
				}
				//ended by shriram
				if(!(entityTypeId==3 && lstJobOrderList.size()==0))
				{
					//lstTeacherDetail=jobForTeacherDAO.getTeacherDetailByDASA(sortOrderStrVal,start,noOfRowInPage,entityTypeId,lstJobOrderList,districtMaster, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq);
					lstTeacherDetailAll=jobForTeacherDAO.getQuestTeacherDetailRecords(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList ,firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm);
				}
				totalRecord=lstTeacherDetailAll.size();
			}
			if(totalRecord<end)
				end=totalRecord;

			
			System.out.println("++++++++++++++++++++++++++++Teacher Details Size:: "+lstTeacherDetailAll.size());
			
			
			
			// sorting on normscore, ascore, lrscore
			if(!sortOrder.equals("tLastName") && !flagOhio8)
			{
				List<TeacherNormScore> teacherNormScoreList = teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetailAll);
				Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
				for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
					normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
				}
				Map<Integer,TeacherAchievementScore> mapAScore = null;

				if(entityID!=1 && achievementScore)
				{
					JobOrder jo=new JobOrder();
					jo.setDistrictMaster(districtMaster);
					List<TeacherAchievementScore> lstTeacherAchievementScore=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetailAll,jo);

					mapAScore = new HashMap<Integer, TeacherAchievementScore>();

					for(TeacherAchievementScore tAScore : lstTeacherAchievementScore){
						mapAScore.put(tAScore.getTeacherDetail().getTeacherId(),tAScore);
					}
				}
				TeacherAchievementScore tAScore=null;

				TeacherNormScore teacherNormScore = null;
				for(TeacherDetail teacherDetail : lstTeacherDetailAll)
				{
					teacherNormScore = normMap.get(teacherDetail.getTeacherId());
					if(teacherNormScore==null)
						teacherDetail.setNormScore(-0);
					else
						teacherDetail.setNormScore(teacherNormScore.getTeacherNormScore());

					if(entityID!=1 && achievementScore)
					{
						tAScore=mapAScore.get(teacherDetail.getTeacherId());
						if(tAScore!=null){
							if(tAScore.getAcademicAchievementScore()!=null)
								teacherDetail.setaScore(tAScore.getAcademicAchievementScore());

							if(tAScore.getLeadershipAchievementScore()!=null)
								teacherDetail.setlRScore(tAScore.getLeadershipAchievementScore());
						}else{
							teacherDetail.setaScore(-1);
							teacherDetail.setlRScore(-1);
						}
					}
				}

				if(sortOrderFieldName.equals("normScore") && sortOrderType.equals("0")){
					Collections.sort(lstTeacherDetailAll,TeacherDetail.teacherDetailComparatorNormScoreDesc );
				}else if(sortOrderFieldName.equals("normScore") && sortOrderType.equals("1")){
					Collections.sort(lstTeacherDetailAll,TeacherDetail.teacherDetailComparatorNormScore);
				}else if(sortOrderFieldName.equals("aScore") && sortOrderType.equals("1")){				
					Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorAScore );
				}else if(sortOrderFieldName.equals("aScore") && sortOrderType.equals("0")){				
					Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorAScoreDesc );				
				}else if(sortOrderFieldName.equals("lRScore") && sortOrderType.equals("1")){				
					Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorLRScore );
				}else if(sortOrderFieldName.equals("lRScore") && sortOrderType.equals("0")){				
					Collections.sort(lstTeacherDetailAll,TeacherDetail.TeacherDetailComparatorLRScoreDesc );				
				}

				lstTeacherDetail=lstTeacherDetailAll;
			}
			else
			{
				if(sortOrder.equals("tLastName")){
					for(TeacherDetail td:lstTeacherDetailAll)
					{
							td.settLastName(td.getLastName());
					}
					
					if(sortOrderType.equals("0"))
						Collections.sort(lstTeacherDetailAll,new TeacherDetailComparatorLastNameAsc());
					else
						Collections.sort(lstTeacherDetailAll,new TeacherDetailComparatorLastNameDesc());
				}
			
				lstTeacherDetail=lstTeacherDetailAll;
			}



			List<Integer> lstStatusMasters = new ArrayList<Integer>();
			String[] statuss = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
			try{
				lstStatusMasters = statusMasterDAO.findStatusIdsByStatusByShortNames(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}
			if(totalRecord!=0){
				lstJobForTeacher=jobForTeacherDAO.getJobForTeacherByDSTeachersList(lstStatusMasters,lstTeacherDetail,lstJobOrderSLList,null,null,districtMaster,entityID);
				for(JobForTeacher jobForTeacher : lstJobForTeacher){
					jftMap.put(jobForTeacher.getJobForTeacherId(),jobForTeacher);
				}
				lstJobForTeacher=jobForTeacherDAO.getJobForTeacherByDSTeachersList(lstStatusMasters,lstTeacherDetail,lstJobOrderSLList,null,null,districtMaster,1);
				for(JobForTeacher jobForTeacher : lstJobForTeacher){
					jftMapForTm.put(jobForTeacher.getJobForTeacherId(),jobForTeacher);
				}
			}
		}
		String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
		List<TeacherNormScore> teacherNormScoreList=teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetail);
		Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
		for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
			normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
		}

		Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();	
		Map<Integer,Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
		List<TeacherExperience> lstTeacherExperience=teacherExperienceDAO.findExperienceForTeachers(lstTeacherDetail);		

		// ************** Start ... IDENTIFY Internal Candidate *********************
		Map<Integer,Boolean> mapInternalCandidate = new HashMap<Integer, Boolean>();
		if(entityID!=1)
		{
			List<JobForTeacher> lstjobForTeachers=jobForTeacherDAO.findInternalCandidate(lstTeacherDetail);
			if(entityID==2)
			{
				for(JobForTeacher pojo:lstjobForTeachers){
					if(districtMaster.equals(pojo.getJobId().getDistrictMaster()))
					{
						mapInternalCandidate.put(pojo.getTeacherId().getTeacherId(), true);
					}
				}
			}
			else if(entityID==3)
			{
				Map<Integer,Boolean> mapJobOrder = new HashMap<Integer, Boolean>();
				List<JobOrder> lstJobOrders=new ArrayList<JobOrder>();
				for(JobForTeacher pojo:lstjobForTeachers){
					if(districtMaster.equals(pojo.getJobId().getDistrictMaster()))
						lstJobOrders.add(pojo.getJobId());
				}
				List<SchoolInJobOrder> lstSchoolInJobOrders= new ArrayList<SchoolInJobOrder>();
				if(schoolMaster!=null)
				 lstSchoolInJobOrders=schoolInJobOrderDAO.findInternalCandidate(lstJobOrders, schoolMaster);
				
				if(lstSchoolInJobOrders.size()>0){
					for(SchoolInJobOrder pojo:lstSchoolInJobOrders)
						mapJobOrder.put(pojo.getJobId().getJobId(), true);
				}

				for(JobForTeacher pojo:lstjobForTeachers){
					if(mapJobOrder.get(pojo.getJobId().getJobId())!=null && mapJobOrder.get(pojo.getJobId().getJobId()).equals(true)){
						mapInternalCandidate.put(pojo.getTeacherId().getTeacherId(), true);
					}
				}
			}
		}
		// ************** End ... IDENTIFY Internal Candidate *********************

		List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
		Integer isBase = null;
		try{
			if(lstTeacherDetail!=null && lstTeacherDetail.size()>0){
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(lstTeacherDetail);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
					boolean cgflag = teacherAssessmentStatus2.getCgUpdated()==null?false:teacherAssessmentStatus2.getCgUpdated();
					if(cgflag && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
					{
						isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
					}
					else
					{
						if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
						{
							isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 2);
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		    //alok kumar			
			Map<Integer,List<Integer>> jsiCount	=	new HashMap<Integer, List<Integer>>();
			Map<Integer,List<Integer>> epiCount	=	new HashMap<Integer, List<Integer>>();
			
			List<TeacherAssessmentStatus> teacherAssessmentJsi = null;
			List<TeacherAssessmentStatus> teacherAssessmentEpi = null;
			List<Integer> teacherId = new ArrayList();
			List<Integer> matchTeacherId = null;
			try
			{
				if(lstTeacherDetail!=null && lstTeacherDetail.size()>0)
				{

					for(TeacherDetail t:lstTeacherDetail)
					{
						teacherId.add(t.getTeacherId());
					}	
					teacherAssessmentJsi = teacherAssessmentStatusDAO.findAssessmentJsiByTeachers(lstTeacherDetail);
					teacherAssessmentEpi=teacherAssessmentStatusDAO.findAssessmentEpiByTeachers(lstTeacherDetail);						
					if(teacherAssessmentJsi.size()>0)
					{
						for(Integer id :teacherId)
						{
							matchTeacherId=new ArrayList();
							for(TeacherAssessmentStatus assessmentteacherId:teacherAssessmentJsi)
							{								  
							  if(id.equals(assessmentteacherId.getTeacherDetail().getTeacherId()))
							  {
								  matchTeacherId.add(assessmentteacherId.getTeacherDetail().getTeacherId());
							  }
							}
							if(matchTeacherId.size()>0)
							jsiCount.put(id, matchTeacherId);							
						}
					}
					if(teacherAssessmentEpi.size()>0)
					{
						for(Integer id :teacherId)
						{
							matchTeacherId=new ArrayList();
							for(TeacherAssessmentStatus assessmentteacherId:teacherAssessmentEpi)
							{
							  if(id.equals(assessmentteacherId.getTeacherDetail().getTeacherId()))
							  {
								  matchTeacherId.add(assessmentteacherId.getTeacherDetail().getTeacherId());
							  }
							}
							if(matchTeacherId.size()>0)
							epiCount.put(id, matchTeacherId);							
						}
					}
				}				
			}			
			catch(Exception e)
			{e.printStackTrace();}
			
		
		String tFname="",tLname="";
		String responseText="";
		String schoolDistrict="District";
		if(entityID==3){
			schoolDistrict="School";
		}

		List<TeacherPersonalInfo> lstTeacherPersonalInfo = new ArrayList<TeacherPersonalInfo>();
		List<TeacherAchievementScore> lstTeacherAchievementScore = new ArrayList<TeacherAchievementScore>();			
		List<DistrictSpecificQuestions> lstdistrictSpecificQuestions = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(userMaster.getDistrictId());
		boolean schoolCheckFlag=false;
		if(userMaster.getEntityType()==3)
		{
			try
			{
				schoolMaster = schoolMasterDAO.findById(userMaster.getSchoolId().getSchoolId(), false, false);
				DistrictSchools districtSchools = districtSchoolsDAO.findBySchoolMaster(schoolMaster);
				if(districtSchools.getDistrictMaster().getResetQualificationIssuesPrivilegeToSchool()==true)
				{
					schoolCheckFlag = true;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		int counter=0;
		TeacherNormScore teacherNormScore = null;
		
		
		List<Integer>teacherIdLst=new ArrayList<Integer>();
		for (TeacherDetail teacherDetail : lstTeacherDetail) 
		{
			teacherIdLst.add(teacherDetail.getTeacherId());
		}
		Criterion criterionTeacherPersonalInfo=Restrictions.in("teacherId", teacherIdLst);
		List<TeacherPersonalInfo>teacherPersonalInfos=null;
		HashMap<Integer, TeacherPersonalInfo> map=new HashMap<Integer, TeacherPersonalInfo>();
		if(teacherIdLst!=null && teacherIdLst.size()>0){
			teacherPersonalInfos=teacherPersonalInfoDAO.findByCriteria(criterionTeacherPersonalInfo);
			for (TeacherPersonalInfo pojo : teacherPersonalInfos) 
			{
				map.put(pojo.getTeacherId(), pojo);
			}
		}	
		//alok
		Map<Integer, EmployeeMaster> empMap = new HashMap<Integer, EmployeeMaster>();
		Map<String,Boolean> mapAlum = new HashMap<String, Boolean>(); // For Alum
		if(entityID!=1)
		{
			String ssn="";				
			Map<Integer, TeacherPersonalInfo>tpInfomap=new  HashMap<Integer, TeacherPersonalInfo>();
			List<String> teacherFNameList = new ArrayList<String>();
			List<String> teacherLNameList = new ArrayList<String>();
			List<String> teacherSSNList = new ArrayList<String>();
			if(teacherPersonalInfos!=null && teacherPersonalInfos.size()>0){				
				for(TeacherPersonalInfo pojo :teacherPersonalInfos) {
					tpInfomap.put(pojo.getTeacherId(), pojo); 
					ssn = pojo.getSSN()==null?"":pojo.getSSN().trim();
					if(!ssn.equals(""))
					{
						teacherFNameList.add(pojo.getFirstName().trim());
						teacherLNameList.add(pojo.getLastName().trim());
						ssn = ssn.trim();
						try {
							ssn = Utility.decodeBase64(ssn);
						} catch (IOException e) {
							e.printStackTrace();
						}
						if(ssn.length()>4)
							ssn = ssn.substring(ssn.length() - 4, ssn.length());
						teacherSSNList.add(ssn.trim());
					}
				}
			}
			List<EmployeeMaster> employeeMasters =	employeeMasterDAO.checkEmployeeByFistName(teacherFNameList,teacherLNameList,teacherSSNList,districtMaster);
			mapAlum=cgService.getMapAlum(employeeMasters, districtMaster); // get Alum Map
			
			if(teacherPersonalInfos!=null && teacherPersonalInfos.size()>0 && employeeMasters.size()>0){				
				for(TeacherPersonalInfo pojo :teacherPersonalInfos) {
					ssn = pojo.getSSN()==null?"":pojo.getSSN().trim();
					if(!ssn.equals(""))
					{
						ssn = ssn.trim();
						try {
							ssn = Utility.decodeBase64(ssn);
						} catch (IOException e) {
							e.printStackTrace();
						}
						if(ssn.length()>4)
							ssn = ssn.substring(ssn.length() - 4, ssn.length());
						String empNo = pojo.getEmployeeNumber()==null?"":pojo.getEmployeeNumber().trim();
						
						for(EmployeeMaster emp :employeeMasters) {
							if(empNo.equals(""))// Not an employee
							{
								if(emp.getSSN().equalsIgnoreCase(ssn) && emp.getFirstName().equalsIgnoreCase(pojo.getFirstName()) && emp.getLastName().equalsIgnoreCase(pojo.getLastName()))
								{
									empMap.put(pojo.getTeacherId(), emp);
									break;
								}
							}
							else 
							{
								if(emp.getEmployeeCode().equalsIgnoreCase(empNo) && emp.getSSN().equalsIgnoreCase(ssn) && emp.getFirstName().equalsIgnoreCase(pojo.getFirstName()) && emp.getLastName().equalsIgnoreCase(pojo.getLastName()))
								{
									empMap.put(pojo.getTeacherId(), emp);
									break;
								}
							}
						}
					}
				}
			}
		}			
		//Gourav  districtMaster
		List<SecondaryStatusMaster> lstSecondaryStatusMaster = secondaryStatusMasterDAO.findTagswithDistrictInCandidatePool(districtMaster);
		Criterion district_cri = Restrictions.eq("districtMaster", districtMaster);
		Criterion school_cri = Restrictions.isNull("schoolMaster");
		Criterion job_cri = Restrictions.isNull("jobOrder");
		
		List<TeacherSecondaryStatus> lstMltTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByCriteria(district_cri,school_cri,job_cri);
		Map<String, TeacherSecondaryStatus> teachersTagsExistMap = new HashMap<String, TeacherSecondaryStatus>();
		if(lstMltTeacherSecondaryStatus.size()>0){
			for (TeacherSecondaryStatus teacherSecondaryStatus : lstMltTeacherSecondaryStatus) {
				String teacherTagKey = teacherSecondaryStatus.getTeacherDetail().getTeacherId()+"##"+teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusId();
				teachersTagsExistMap.put(teacherTagKey, teacherSecondaryStatus);
			}
		}
		
		if(lstTeacherDetail.size()==0){}
//			tmRecords.append("<tr><td colspan='9'>No Teacher Found</td></tr>" );//Shriram No Record Found
		
		else
		{
			if(entityID!=1)
			{
				JobOrder jobOrder = new JobOrder();
				jobOrder.setDistrictMaster(districtMaster);
				lstTeacherAchievementScore=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetail,jobOrder);
			}
			lstTeacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPInfoForTeacher(lstTeacherDetail);
		}
		Map<Integer,Boolean> qqTeachersMap = new HashMap<Integer, Boolean>(); 
		if(lstTeacherDetail.size()>0 && entityTypeId!=1 && lstdistrictSpecificQuestions.size()>0)
		{
			List<TeacherDetail> qqAttemptedTeachers =  teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findQQStatusByTeacher(lstTeacherDetail,districtMaster,true);
			List<TeacherDetail> invalidQQTeachers = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findQQStatusByTeacher(lstTeacherDetail,districtMaster,false);
			for (TeacherDetail teacherDetail : qqAttemptedTeachers) {
				qqTeachersMap.put(teacherDetail.getTeacherId(), true);
			}
			for (TeacherDetail teacherDetail : invalidQQTeachers) {
				qqTeachersMap.put(teacherDetail.getTeacherId(), false);
			}
		}

		// teacherwiseassessmenttime
		Map<Integer, TeacherWiseAssessmentTime> teacherWMap = new HashMap<Integer, TeacherWiseAssessmentTime>();
		if(lstTeacherDetail.size()>0){
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			Criterion teacherListCR = Restrictions.in("teacherDetail", lstTeacherDetail);
			Criterion assessmenType		= Restrictions.eq("assessmentType", 1);
			Criterion currentYear = Restrictions.between("createdDateTime", sDate, eDate);
			
			List<TeacherWiseAssessmentTime> teacherWiseAssessmentTime = teacherWiseAssessmentTimeDAO.findByCriteria(Order.desc("assessmentTimeId"),teacherListCR,assessmenType);
			
			if(teacherWiseAssessmentTime.size()>0){
				for (TeacherWiseAssessmentTime teacherWiseAssessmentTime2 : teacherWiseAssessmentTime) {
					if(!teacherWMap.containsKey(teacherWiseAssessmentTime2.getTeacherDetail().getTeacherId())){
						teacherWMap.put(teacherWiseAssessmentTime2.getTeacherDetail().getTeacherId(), teacherWiseAssessmentTime2);
					}
				}
			}
		}
		
		String sCheckBoxValue="";
		Map<Integer,Boolean> finalizedTMap = new HashMap<Integer, Boolean>();
		List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
		if(districtMaster!=null && lstTeacherDetail.size()>0)
		{
			lstTeacher = jobForTeacherDAO.getQQFinalizedTeacherList(lstTeacherDetail,districtMaster);
			for (TeacherDetail teacherDetail : lstTeacher) {
				finalizedTMap.put(teacherDetail.getTeacherId(), true);
			}
		}
		////////////////////////////////Add disable profile( Sekhar )///////////////////////////////
		Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
		System.out.println("=============================="+lstTeacherDetail.size());
		List<TeacherStatusHistoryForJob> historyForJobList = teacherStatusHistoryForJobDAO.findHireTeachersWithDistrictListAndTeacherList(lstTeacherDetail,districtMaster);
		List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherForOffers(lstTeacherDetail);
		List<String> posList=new ArrayList<String>();
		Map<String,String> reqMap=new HashMap<String, String>();
		for (JobForTeacher jobForTeacher : jftOffers){
			try{
				if(jobForTeacher.getRequisitionNumber()!=null){
					posList.add(jobForTeacher.getRequisitionNumber());
					reqMap.put(jobForTeacher.getJobId().getJobId()+"#"+jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getRequisitionNumber());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(districtMaster,posList);
		Map<String,DistrictRequisitionNumbers> dReqNoMap = new HashMap<String,DistrictRequisitionNumbers>();
		try{
			for (DistrictRequisitionNumbers districtRequisitionNumbers2 : districtRequisitionNumbers) {
				dReqNoMap.put(districtRequisitionNumbers2.getRequisitionNumber(),districtRequisitionNumbers2);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		if(historyForJobList.size()>0){
			mapForHiredTeachers=cgService.getHiredList(reqMap,dReqNoMap,lstTeacherDetail,historyForJobList,null);
		}
			tmRecords
					.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>Ohio 8 Candidate List</div><br/>");

			tmRecords.append("<div style='width:100%'>");
			tmRecords
					.append("<div style='width:50%; float:left; font-size: 15px; '> "
							+ "Created by: "
							+ userMaster.getFirstName()
							+ " "
							+ userMaster.getLastName() + "</div>");
			tmRecords
					.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>Date of Print: "
							+ Utility
									.convertDateAndTimeToUSformatOnlyDate(new Date())
							+ "</div>");
			tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");

			tmRecords
					.append("<table  id='tblGridOhioCandidateList' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");

			// shriram//Candidate First Name//
			tmRecords
					.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ lblCandidateFirstName + "</th>");
			// Candidate Last Name
			tmRecords
					.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ lblCandidateLastName + "</th>");
			// Candidate Email
			tmRecords
					.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ lblCandidateEmailAddress + "</th>");
// Norm Score
			if(!flagOhio8)
			{
			tmRecords
					.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ "Norm Score" + "</th>");
			}
			tmRecords
					.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ "# of Jobs" + "</th>");

			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");

		
			String sta = "";//SHRIRAM 444444444
			if(lstTeacherDetail!=null && lstTeacherDetail.size()>0)
			{
				System.out.println(" Teachers List Get ");//Shriram Teacher List Get
					for (TeacherDetail teacherDetail : lstTeacherDetail) 
					{
						teacherNormScore = normMap.get(teacherDetail.getTeacherId());
						int normScore = 0;
						String colorName = "";
						if(teacherNormScore!=null)
						{
							normScore = teacherNormScore.getTeacherNormScore();
						}
						noOfRecordCheck++;
						tFname=teacherDetail.getFirstName();
						tLname=teacherDetail.getLastName();
					
						//gourav  tag loop start
						StringBuffer tagNames =	new StringBuffer();
						if(roleId != 1){}
						//Add profile hover

						String profileNormScore="";
						
						
					/* @Start
					 * @Ashish Kumar
					 * @Description :: Qualification Details Thumb 
					 * */
						
						//<a href='#' onclick=\"getMessageDiv('"+teacherDetail.getTeacherId()+"','"+teacherDetail.getEmailAddress()+"',0,0);\" >
						TeacherPersonalInfo tempTeacher=map.get(teacherDetail.getTeacherId());
						
						
						boolean CheckForIssue = false; 
						
						if(userMaster.getEntityType()==2)
						{
							CheckForIssue = true;
						}
						
						if(schoolCheckFlag)
							CheckForIssue = true;
						
						if(CheckForIssue == true)
						{
							if(lstdistrictSpecificQuestions.size()>0)
							{
								Boolean qqTaken = qqTeachersMap.get(teacherDetail.getTeacherId());
								if(qqTaken!=null)
								{
									Boolean finalizeQQ = finalizedTMap.get(teacherDetail.getTeacherId());
									finalizeQQ = finalizeQQ==null?false:finalizeQQ;
																	
							}
						}
						
						int jobList=jobListByTeacher(teacherDetail.getTeacherId(),jftMap);
						int jobListForTm=jobListByTeacher(teacherDetail.getTeacherId(),jftMapForTm);

					tmRecords.append("<tr>");

					
					tmRecords.append("<td>" + tFname + "</td>");

				
					tmRecords.append("<td>" + tLname + "</td>");

					
					tmRecords.append("<td>" + teacherDetail.getEmailAddress() + "</td>");

					if(!flagOhio8)
					{
					if(normScore!=0){
						profileNormScore=normScore+"";
						tmRecords.append("<td>" + profileNormScore + "</td>");
					}else{
						profileNormScore="N/A";
						tmRecords.append("<td>" + profileNormScore + "</td>");
					}
					}
					
					
					if(jobList!=0){   
						tmRecords.append("<td>" + jobList+"/"+(jobListForTm-jobList)+"" + "</td>");
						
					}
					else{
						tmRecords.append("<td>" + "0"+"/"+(jobListForTm-jobList)+"" + "</td>");
					}
					}
					
					tmRecords.append("</tr>");
				}

			}else
			{
				tmRecords
				.append("<tr><td colspan='10' align='center' class='net-widget-content'>"
						+ Utility.getLocaleValuePropByKey(
								"lblNoRecord", locale) + "</td></tr>");
			}
			tmRecords.append("</tbody>");
			tmRecords.append("</table>");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return tmRecords.toString();
	}
	
	public int powerTracker_Op(int id)
	{	
		StringBuffer b=new StringBuffer();
		System.out.println("id    "+id);
		int marks=0;
		try
		{
		TeacherDetail teacherDetail=teacherDetailDAO.findById(id, false, false);
		if(teacherDetail!=null)
		{
			if(teacherDetail.getVerificationStatus()==1)
			{
				System.out.println("varification status==  "+teacherDetail.getVerificationStatus());
				marks=marks+5;
			}
			
		}
		
		TeacherPortfolioStatus teacherPortfolioStatus=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);				
		if(teacherPortfolioStatus!=null)
			if(teacherPortfolioStatus.getIsAffidavitCompleted())
			{
				System.out.println("IsAffidavitCompleted==   "+teacherPortfolioStatus.getIsAffidavitCompleted());
				marks=marks+5;
			}
	
		
		TeacherPreference teacherPreference=preferenceDAO.findPrefByTeacher(teacherDetail);
		if(teacherPreference!=null)
		{
			System.out.println("teacherPreference==   "+teacherPreference);
			marks=marks+5;
		}
		
		
		List<TeacherAcademics> teacherAcademics=academicsDAO.findAcadamicDetailByTeacher(teacherDetail);
		if(teacherAcademics!=null && teacherAcademics.size()!=0 )
		if( teacherAcademics!=null && teacherAcademics.size()!=0)
		{
			System.out.println("teacherAcademics==   "+teacherAcademics.size());
			marks=marks+5;
		}
		
		List<TeacherCertificate> teacherCertificates=certificateDAO.findCertificateByTeacher(teacherDetail);
		if(teacherCertificates!=null && teacherCertificates.size()!=0 )
		if(teacherCertificates!=null && teacherCertificates.size()!=0)
		{
			System.out.println("teacherCertificates  ==  "+teacherCertificates.size());
			if(teacherCertificates.size()==1)
			{
				marks=marks+5;
			}
			else if(teacherCertificates.size()>1)
			{
				marks=marks+10;
			}
			
		}		
		List<TeacherElectronicReferences> electronicReferences=electronicReferencesDAO.findActiveReferencesByTeacher(teacherDetail);
		if(teacherCertificates!=null && electronicReferences.size()!=0)
		{
			System.out.println("electronicReferences ==  "+electronicReferences.size());
			if(electronicReferences.size()==1)
			{
				marks=marks+5;
			}
			else if(electronicReferences.size()==2)
			{
				marks=marks+10;
			}
			else if(electronicReferences.size()>2)
			{
				marks=marks+15;
			}
		}	
		
		List<TeacherRole> teacherRoles=teacherRoleDAO.findTeacherRoleByTeacher(teacherDetail);
		if(teacherRoles!=null && teacherRoles.size()!=0)
		{
			System.out.println("teacherRoles  ==  "+teacherRoles.size());
			marks=marks+5;
		}
		
		List<TeacherInvolvement> teacherInvolvement=teacherInvolvementDAO.findTeacherInvolvementByTeacher(teacherDetail);
		if(teacherInvolvement!=null && teacherInvolvement.size()!=0)
		{
			System.out.println("teacherInvolvement ==  "+teacherInvolvement.size());
			marks=marks+5;
		}
		
		String teacherAssessmentStatus=teacherAssessmentStatusDAO.epiStatus(teacherDetail);		
		if(teacherAssessmentStatus.equalsIgnoreCase("comp"))
		{
			System.out.println("teacherAssessmentStatus ==  "+teacherAssessmentStatus);
			marks=marks+20;
		}
		
		//List<JobForTeacher> jobForTeachers=jobForTeacherDAO.findJobByTeacherOneYear(teacherDetail);
		
		 Integer jobForTeachers=jobForTeacherDAO.findJobByTeacherOneYear_Op(teacherDetail);
		if(jobForTeachers!=null && jobForTeachers > 0)
		{
			System.out.println("job for teacher==="+jobForTeachers );
		//	System.out.println(" List Job For Teacher : "+jobForTeachers.get(0).getCreatedDateTime());
			if(jobForTeachers ==1)
			{
				marks=marks+2;
			}
			else if(jobForTeachers ==2)
			{
				marks=marks+4;
			}
			else if(jobForTeachers ==3)
			{
				marks=marks+6;
			}
			else if(jobForTeachers ==4)
			{
				marks=marks+8;
			}
			else if(jobForTeachers >4)
			{
				marks=marks+10;
			}
			
			System.out.println("marks::----:::::"+marks +"----"+jobForTeachers);
		}
		
		List<TeacherVideoLink> teacherVideoLink=linksDAO.findTeachByVideoLink(teacherDetail);
		if(teacherVideoLink!=null && teacherVideoLink.size()!=0)
		{
			System.out.println("vedio link ==  "+teacherVideoLink.size());
			marks=marks+10;
		}
		
		TeacherExperience teacherExperiences=teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
 		if(teacherExperiences!=null)
 		{
 			if(teacherExperiences.getResume()!=null)
 			{
 				System.out.println("Resume == "+teacherExperiences.getResume());
 				marks=marks+5;
 			}
 		}
 			
		}
		catch(Exception e)
		{e.printStackTrace();}
		
		System.out.println("Total Score=  "  +marks);		
		
		return marks;
	}
	
	public String displayTeacherGrid_OP(String firstName,String lastName, String emailAddress,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String jobAppliedFromDate,String jobAppliedToDate,int daysVal)
	{
		//Quest Candidate Grid Display Method (questcandidatesnew.do)
		// ========  For Session time Out Error =========
	
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(msgYrSesstionExp); 
			}
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			boolean flagOhio8 = false;
			//------------------------------------
			StringBuffer tmRecords =	new StringBuffer();
			CandidateGridService cgService=new CandidateGridService();
			try{
				List<TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();

				/** set default sorting fieldName **/
				String sortOrderFieldName	=	"lastName";
				String sortOrderNoField		=	"lastName";

				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				//sortOrderStrVal=Order.asc("lastName");
				/**End ------------------------------------**/
				int roleId=0;
				int entityID=0;
				boolean isMiami = false;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				UserMaster userMaster=null;
				int entityTypeId=1;
				int utype = 1;
				boolean writePrivilegeToSchool =false;
				boolean isManage = true;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");
					if(userMaster.getRoleId().getRoleId()!=null){
						roleId=userMaster.getRoleId().getRoleId();
					}
					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
						if(districtMaster.getDistrictId().equals(1200390))
							isMiami = true;
						writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
					}
					if(userMaster.getSchoolId()!=null){
						schoolMaster =userMaster.getSchoolId();
					}	
					entityID=userMaster.getEntityType();
					System.out.println("===========EntityId : "+entityID);
					utype = entityID;
				}
				
				if(isMiami && entityID==3)
					isManage=false;

				boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
				if(entityID==1)
				{
					achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
				}else
				{
					try{
						DistrictMaster districtMasterObj=districtMaster;
						if(districtMasterObj.getDisplayAchievementScore()){
							achievementScore=true;
						}
						if(districtMasterObj.getDisplayTFA()){
							tFA=true;
						}
						if(districtMasterObj.getDisplayDemoClass()){
							demoClass=true;
						}
						if(districtMasterObj.getDisplayJSI()){
							JSI=true;
						}
						if(districtMasterObj.getDisplayYearsTeaching()){
							teachingOfYear=true;
						}
						if(districtMasterObj.getDisplayExpectedSalary()){
							expectedSalary=true;
						}
						if(districtMasterObj.getDisplayFitScore()){
							fitScore=true;
						}
					}catch(Exception e){ e.printStackTrace();}
				}

				String roleAccess=null;
				try{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,43,"teacherinfo.do",1);
				}catch(Exception e){
					e.printStackTrace();
				}
				Date fDate=null;
				Date tDate=null;
				Date appliedfDate=null;
				Date appliedtDate=null;
				boolean applieddateFlag=false;
				boolean dateFlag=false;
				try{
					// job Applied Filter
					if(jobAppliedFromDate!=null && !jobAppliedFromDate.equals("")){
						appliedfDate=Utility.getCurrentDateFormart(jobAppliedFromDate);
						applieddateFlag=true;
					}
					if(jobAppliedToDate!=null && !jobAppliedToDate.equals("")){
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(Utility.getCurrentDateFormart(jobAppliedToDate));
						cal2.add(Calendar.HOUR,23);
						cal2.add(Calendar.MINUTE,59);
						cal2.add(Calendar.SECOND,59);
						appliedtDate=cal2.getTime();
						applieddateFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}			
				List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();
				boolean certTypeFlag=false;			
				System.out.println("jobOrderIds:: "+jobOrderIds.size());
				System.out.println("jobOrderIds:: "+jobOrderIds);
				System.out.println("certTypeFlag:: "+certTypeFlag);		
				List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
				List<TeacherDetail> NbyATeacherList = new ArrayList<TeacherDetail>();
				//List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
				boolean teacherFlag=false;			
				boolean nByAflag=false;		
				List<TeacherDetail> subjectTeacherList = new ArrayList<TeacherDetail>();
				Map<Integer,Boolean> mapInternalCandidate = new HashMap<Integer, Boolean>();
				boolean subjectFlag=false;
			
				if(subjectFlag && subjectTeacherList.size()>0){
					if(teacherFlag){
						filterTeacherList.retainAll(subjectTeacherList);
					}else{
						filterTeacherList.addAll(subjectTeacherList);
					}
				}
				System.out.println(" filterTeacherList :: "+filterTeacherList);
				if(subjectFlag){
					teacherFlag=true;
				}

			
			
			
			
				
				
				
				// Start /////////////////////////////// candidate list By certificate ///////////////////////////////////////
				StateMaster stateMasterForCandidate=null;
				List<CertificateTypeMaster> certTypeMasterListForCndt =new ArrayList<CertificateTypeMaster>();
				List<TeacherDetail> tListByCertificateTypeId = new ArrayList<TeacherDetail>();
				boolean tdataByCertTypeIdFlag = false;	
				
				/************* End **************/
				StatusMaster statusMaster = new StatusMaster();
				boolean status=false;			
				int internalCandVal=0;	
				SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
				SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
				List<JobForTeacher> lstJobForTeacher =	 new ArrayList<JobForTeacher>();
				List<JobOrder> lstJobOrderList =new ArrayList<JobOrder>();
				List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
				int noOfRecordCheck=0;
				
				SchoolMaster sm = schoolMaster;
				List<TeacherDetail> lstTeacherDetailAll= new ArrayList<TeacherDetail>();
				if(entityTypeId!=0){
					if(entityID==3){
						
						lstJobOrderSLList =schoolInJobOrderDAO.allJobOrderPostedBySchool(schoolMaster);
						entityTypeId=3;
					}				
					if(entityTypeId==3 && lstJobOrderSLList.size()>0 && certTypeFlag)
					{
						lstJobOrderList.addAll(getFilterJobOrder(lstJobOrderSLList,jobOrderIds));
					}
					else if(entityTypeId!=3 && lstJobOrderSLList.size()==0 && certTypeFlag)
					{
						lstJobOrderList.addAll(jobOrderIds);
						entityTypeId=3;
					}
					else
					{
						lstJobOrderList.addAll(lstJobOrderSLList);
					}
					if(entityID==2){
						entityTypeId=2;
					}
					
					String stq="";
					if(districtMaster!=null  && utype==3)
					{
						SecondaryStatus smaster = districtMaster.getSecondaryStatus();
						if(smaster!=null)
						{
							String secStatus = smaster.getSecondaryStatusName();
							if(secStatus!=null && !secStatus.equals(""))
							{
								//
								 List<SecondaryStatus> lstStatus = new ArrayList<SecondaryStatus>();
								 lstStatus = secondaryStatusDAO.findSecondaryStatusByName(districtMaster,secStatus);
								 System.out.println("lstStatus.size():::: "+lstStatus.size());
								 for (SecondaryStatus secondaryStatus : lstStatus) {
									 if(secondaryStatus.getJobCategoryMaster()!=null)
									 {
										 stq+=" IF(secondarys3_.jobCategoryId="+secondaryStatus.getJobCategoryMaster().getJobCategoryId()+", ( secondaryStatusId >="+secondaryStatus.getSecondaryStatusId()+" ),false) or";
									 }
								}
								 if(stq.length()>3)
								 {
									 stq = stq.substring(0, stq.length()-2);
								 }
							}
						}
					}
					JSONArray[] gridJsonRecord = new JSONArray[2];
					boolean questFlag = false;
					List <DistrictMaster> districtMastersList=new ArrayList<DistrictMaster>();
					if(entityID==1 && entityTypeId==1 && dateFlag==false && applieddateFlag==false && status==false && internalCandVal==0){
						//>>>>>>>>>>>>>>>>>>>>> Job Applied change >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
						gridJsonRecord=jobForTeacherDAO.getQuestTeacherDetailRecords_OP(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm,start,end,noOfRow);
					}else{
						DistrictMaster dm=null;
						Criterion critdistrict=null;
						if(districtMaster!=null && entityID==2)
						{
							HeadQuarterMaster hq=districtMaster.getHeadQuarterMaster();
							if(hq!=null && hq.getHeadQuarterId()==3)
							{
								flagOhio8=true;
							Criterion critheadquarter=Restrictions.eq("headQuarterMaster", hq);
							dm= districtMasterDAO.findById(3904380, false, false);
							if(districtMaster.getDistrictId()!=3904380)
							{			critdistrict=  Restrictions.ne("districtId", dm.getDistrictId());
							districtMastersList=districtMasterDAO.findByCriteria(critheadquarter,critdistrict);
							}
							else
								districtMastersList=districtMasterDAO.findByCriteria(critheadquarter);
							System.out.println("Size of DistrictmasterList"+districtMastersList.size());
						}
						}
						if(!(entityTypeId==3 && lstJobOrderList.size()==0))
						{
							gridJsonRecord=jobForTeacherDAO.getQuestTeacherDetailRecords_OP(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm,start,end,noOfRow);
						}
					}
	
			
			
			
				//JSONArray[] gridJsonRecord = new JSONArray[2];
				//<------------Quest Candidate Grid Record-------------->
				//gridJsonRecord=jobForTeacherDAO.getQuestTeacherDetailRecords_OP(sortOrderStrVal, districtMaster, firstName, lastName, emailAddress, appliedfDate, appliedtDate, daysVal,start,end,noOfRow);
				
				//<------------Quest Candidate Grid Start-------------->
			tmRecords.append("<table id='teacherTable' width='100%' class='tablecgtbl table-striped'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");

			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblNm,sortOrderFieldName,"teacherLastName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			String schoolDistrict="District";
			if(entityID==3){
				schoolDistrict="School";
			}

			if(!flagOhio8){
			responseText=PaginationAndSorting.responseSortingLink(lblNormScore,sortOrderFieldName,"normScore",sortOrderTypeVal,pgNo);
			
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			}
			if(flagOhio8)
			tmRecords.append("<th width='100' valign='top'># of Jobs&nbsp;<a data-original-title='X/Y, X = msgNumOfJobAppiedAt "+schoolDistrict+" / Y = msgNumOfJobAppiedAt2"+schoolDistrict+"(s)' class='net-header-text ' rel='tooltip' id='jobAppliedTool' href='javascript:void(0);' ><span class='icon-question-sign '></span></a></th>");
			else
			tmRecords.append("<th width='50' valign='top'># of Jobs&nbsp;<a data-original-title='X/Y, X = msgNumOfJobAppiedAt "+schoolDistrict+" / Y = msgNumOfJobAppiedAt2"+schoolDistrict+"(s)' class='net-header-text ' rel='tooltip' id='jobAppliedTool' href='javascript:void(0);' ><span class='icon-question-sign '></span></a></th>");
			tmRecords.append("<script type='text/javascript'>$('#jobAppliedTool').tooltip();</script>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			
			int counter = 0;
			String internalIcon = "";
			String ccsName="";
			List<TeacherDetail> tempList = new ArrayList<TeacherDetail>();
			if(gridJsonRecord[0].size()>0){
				System.out.println("gridJsonRecord[0]    "+gridJsonRecord[0].size());
				for (int i = 0; i < gridJsonRecord[0].size(); i++) {
					
					JSONObject jsonrec = new JSONObject();
					jsonrec = gridJsonRecord[0].getJSONObject(i);
					TeacherDetail td = new TeacherDetail();
					td.setTeacherId(Integer.parseInt(jsonrec.get("teacherId").toString()));
					tempList.add(td);
				}
				
				
				
				
				List<JobForTeacher> lstjobForTeachers=jobForTeacherDAO.findInternalCandidate(tempList);
				if(entityID==2)
				{
					for(JobForTeacher pojo:lstjobForTeachers){
						if(districtMaster.equals(pojo.getJobId().getDistrictMaster()))
							mapInternalCandidate.put(pojo.getTeacherId().getTeacherId(), true);
					}
				}
				
			}
			if(gridJsonRecord[0].size()==0)
				 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			
			if(gridJsonRecord[0].size()>0){
				System.out.println("gridJsonRecord[0]    "+gridJsonRecord[0].size());
				for (int i = 0; i < gridJsonRecord[0].size(); i++) {
					
					JSONObject jsonrec = new JSONObject();
					jsonrec = gridJsonRecord[0].getJSONObject(i);
					String gridColor="class='bggrid'";
					counter++;
					if(counter%2==0)
						gridColor="style='background-color:white;'";
						
					tmRecords.append("<tr "+gridColor+">");
					tmRecords.append("<TD width='240'>");
					
					
					if(entityID!=1 && !flagOhio8)
					{
						if(mapInternalCandidate.get(Integer.parseInt((String)jsonrec.get("teacherId")))!=null && mapInternalCandidate.get(Integer.parseInt((String)jsonrec.get("teacherId")))){
							internalIcon="<a data-original-title='Internal Candidate' rel='tooltip' id='tpInternalCandidate"+counter+"'  ><span class='fa-rocket icon-large internalCandidateIconColor'></span></a> ";
							tmRecords.append(internalIcon+""+jsonrec.get("teacherFirstName")+" "+jsonrec.get("teacherLastName")+"<br>"+jsonrec.get("emailAddress")+"</a>");
					
						}else
							tmRecords.append(jsonrec.get("teacherFirstName")+" "+jsonrec.get("teacherLastName")+"<br>"+jsonrec.get("emailAddress"));
					}else{
						tmRecords.append(jsonrec.get("teacherFirstName")+" "+jsonrec.get("teacherLastName")+"<br>"+jsonrec.get("emailAddress"));
					}
					
					if(!flagOhio8)
					{
						if(!jsonrec.get("normScore").equals("N/A"))
						{
							if(jsonrec.get("normScore").toString().length()==1){
								ccsName="nobground1";
							}else if(jsonrec.get("normScore").toString().length()==2){
								ccsName="nobground2";
							}else{
								ccsName="nobground3";
							}
						}else
							ccsName="nobground3";
						
						if(!jsonrec.get("normScore").equals("N/A"))
							tmRecords.append("<td><span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+jsonrec.get("decileColor")+";'>"+jsonrec.get("normScore")+"</span></td>");
						else
							tmRecords.append("<td>"+jsonrec.get("normScore")+"</td>");
					}
					if(!flagOhio8)
					{
					tmRecords.append("<td>");
					if(!jsonrec.get("jobApply1").equals("0")){                                                                                                   //mukesh
						tmRecords.append("<a href='javascript:void(0);'  onclick=\"getJobList('"+jsonrec.get("teacherId")+"')\" >"+jsonrec.get("jobApply1")+"</a>");
					}else{
						tmRecords.append("0");
					}
					}
					else
					{
						tmRecords.append("<td style='float: right;margin-right: 38px;'>");
						if(!jsonrec.get("jobApply1").equals("0")){                                                                                                   //mukesh
							tmRecords.append("<a href='javascript:void(0);'  onclick=\"getJobList('"+jsonrec.get("teacherId")+"')\" >"+jsonrec.get("jobApply1")+"</a>");
						}else{
							tmRecords.append("0");
						}
					}
						
					tmRecords.append("/"+(Integer.parseInt((String) jsonrec.get("jobApply")) - Integer.parseInt((String) jsonrec.get("jobApply1")))+"</td>");
					tmRecords.append("</tr>");
				}
				tmRecords.append("</table>");
				//<------------Quest Candidate Grid Pagination Start-------------->
				//System.out.println(gridJsonRecord[1].getJSONObject(0).get("totalCount"));
				totalRecord = gridJsonRecord[1].getJSONObject(0).getInt("totalCount");
				if(totalRecord<end)
					end=totalRecord;
				tmRecords.append(PaginationAndSorting.getPaginationStringForJobboard(request,totalRecord,noOfRow, pageNo));
				//<------------Quest Candidate Grid Pagination End-------------->
			}
			//<------------Quest Candidate Grid End-------------->
		}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("tmRecords >>>>>>>>>> "+tmRecords.toString());
		return tmRecords.toString();
	}
	
	public String displayOhioCandidateListPdf_OP(String firstName,String lastName, String emailAddress,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String jobAppliedFromDate,String jobAppliedToDate,int daysVal) {
		//Quest Candidate PDF Generation Method (questcandidatesnew.do)
		/* ========  For Session time Out Error =========*/
		System.out.println("in the pdf method");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session
						.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey(
					"msgYrSesstionExp", locale));
		}
		try {
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath + "fonts/tahoma.ttf",
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(
					fontPath + "fonts/tahoma.ttf", BaseFont.CP1252,
					BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
						String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")
					+ "/user/";
			
			System.out.println(basePath);
			String fileName = time + "Quest.pdf";

			File file = new File(basePath);
			if (!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			//Calling PDF Generation Method
			generateCandidateListPdf_OP(basePath + "/" + fileName,context.getServletContext().getRealPath("/"),firstName,lastName,emailAddress,noOfRow,pageNo,sortOrder,sortOrderType,jobAppliedFromDate,jobAppliedToDate,daysVal);
			
			return "user/" + fileName;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "ABV";
	}
	
public Boolean generateCandidateListPdf_OP(String path,String realPath,String firstName,String lastName, String emailAddress,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String jobAppliedFromDate,String jobAppliedToDate,int daysVal)
{
	//PDF Generation Method For Quest Candidate
	int cellSize = 0;
	Document document = null;
	FileOutputStream fos = null;
	PdfWriter writer = null;
	Paragraph footerpara = null;
	HeaderFooter headerFooter = null;
	Font font8 = null;
	Font font8Green = null;
	Font font8bold = null;
	Font font9 = null;
	Font font9bold = null;
	Font font10 = null;
	Font font10_10 = null;
	Font font10bold = null;
	Font font11 = null;
	Font font11bold = null;
	Font font20bold = null;
	Font font11b = null;
	Color bluecolor = null;
	Font font8bold_new = null;

	///////////////////////////////////////////////////////////////////////////////
	String fileName = null;
	System.out.println("==================Quest Display Teacher Grid For Excel Method==========");
	/* ========  For Session time Out Error =========*/
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp); 
	}
	//-- get no of record in grid,
	//-- set start and end position

	int noOfRowInPage = Integer.parseInt(noOfRow);
	int pgNo = Integer.parseInt(pageNo);
	noOfRow = "0";
	int start =0;
	int end =0 ;
	int totalRecord = 0;
	boolean flagOhio8 = false;
	//------------------------------------
	StringBuffer tmRecords =	new StringBuffer();
	CandidateGridService cgService=new CandidateGridService();
	try{
		List<TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();

		/** set default sorting fieldName **/
		String sortOrderFieldName	=	"lastName";
		String sortOrderNoField		=	"lastName";

		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal=null;

		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
		}

		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
		//sortOrderStrVal=Order.asc("lastName");
		/**End ------------------------------------**/
		int roleId=0;
		int entityID=0;
		boolean isMiami = false;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityTypeId=1;
		int utype = 1;
		boolean writePrivilegeToSchool =false;
		boolean isManage = true;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
				if(districtMaster.getDistrictId().equals(1200390))
					isMiami = true;
				writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
			System.out.println("===========EntityId : "+entityID);
			utype = entityID;
		}
		
		if(isMiami && entityID==3)
			isManage=false;

		boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
		if(entityID==1)
		{
			achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
		}else
		{
			try{
				DistrictMaster districtMasterObj=districtMaster;
				if(districtMasterObj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(districtMasterObj.getDisplayTFA()){
					tFA=true;
				}
				if(districtMasterObj.getDisplayDemoClass()){
					demoClass=true;
				}
				if(districtMasterObj.getDisplayJSI()){
					JSI=true;
				}
				if(districtMasterObj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				if(districtMasterObj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				if(districtMasterObj.getDisplayFitScore()){
					fitScore=true;
				}
			}catch(Exception e){ e.printStackTrace();}
		}

		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,43,"teacherinfo.do",1);
		}catch(Exception e){
			e.printStackTrace();
		}
		Date fDate=null;
		Date tDate=null;
		Date appliedfDate=null;
		Date appliedtDate=null;
		boolean applieddateFlag=false;
		boolean dateFlag=false;
		try{
			// job Applied Filter
			if(jobAppliedFromDate!=null && !jobAppliedFromDate.equals("")){
				appliedfDate=Utility.getCurrentDateFormart(jobAppliedFromDate);
				applieddateFlag=true;
			}
			if(jobAppliedToDate!=null && !jobAppliedToDate.equals("")){
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(Utility.getCurrentDateFormart(jobAppliedToDate));
				cal2.add(Calendar.HOUR,23);
				cal2.add(Calendar.MINUTE,59);
				cal2.add(Calendar.SECOND,59);
				appliedtDate=cal2.getTime();
				applieddateFlag=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}			
		List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();
		boolean certTypeFlag=false;			
		System.out.println("jobOrderIds:: "+jobOrderIds.size());
		System.out.println("jobOrderIds:: "+jobOrderIds);
		System.out.println("certTypeFlag:: "+certTypeFlag);		
		List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
		List<TeacherDetail> NbyATeacherList = new ArrayList<TeacherDetail>();
		//List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
		boolean teacherFlag=false;			
		boolean nByAflag=false;		
		List<TeacherDetail> subjectTeacherList = new ArrayList<TeacherDetail>();
		boolean subjectFlag=false;

		if(subjectFlag && subjectTeacherList.size()>0){
			if(teacherFlag){
				filterTeacherList.retainAll(subjectTeacherList);
			}else{
				filterTeacherList.addAll(subjectTeacherList);
			}
		}
		System.out.println(" filterTeacherList :: "+filterTeacherList);
		if(subjectFlag){
			teacherFlag=true;
		}
		
		// Start /////////////////////////////// candidate list By certificate ///////////////////////////////////////
		StateMaster stateMasterForCandidate=null;
		List<CertificateTypeMaster> certTypeMasterListForCndt =new ArrayList<CertificateTypeMaster>();
		List<TeacherDetail> tListByCertificateTypeId = new ArrayList<TeacherDetail>();
		boolean tdataByCertTypeIdFlag = false;	
		
		/************* End **************/
		StatusMaster statusMaster = new StatusMaster();
		boolean status=false;			
		int internalCandVal=0;	
		SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
		SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
		List<JobForTeacher> lstJobForTeacher =	 new ArrayList<JobForTeacher>();
		List<JobOrder> lstJobOrderList =new ArrayList<JobOrder>();
		List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
		int noOfRecordCheck=0;
		
		SchoolMaster sm = schoolMaster;
		List<TeacherDetail> lstTeacherDetailAll= new ArrayList<TeacherDetail>();
		if(entityTypeId!=0){
			if(entityID==3){
				
				lstJobOrderSLList =schoolInJobOrderDAO.allJobOrderPostedBySchool(schoolMaster);
				entityTypeId=3;
			}				
			if(entityTypeId==3 && lstJobOrderSLList.size()>0 && certTypeFlag)
			{
				lstJobOrderList.addAll(getFilterJobOrder(lstJobOrderSLList,jobOrderIds));
			}
			else if(entityTypeId!=3 && lstJobOrderSLList.size()==0 && certTypeFlag)
			{
				lstJobOrderList.addAll(jobOrderIds);
				entityTypeId=3;
			}
			else
			{
				lstJobOrderList.addAll(lstJobOrderSLList);
			}
			if(entityID==2){
				entityTypeId=2;
			}
			
			String stq="";
			if(districtMaster!=null  && utype==3)
			{
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(smaster!=null)
				{
					String secStatus = smaster.getSecondaryStatusName();
					if(secStatus!=null && !secStatus.equals(""))
					{
						//
						 List<SecondaryStatus> lstStatus = new ArrayList<SecondaryStatus>();
						 lstStatus = secondaryStatusDAO.findSecondaryStatusByName(districtMaster,secStatus);
						 System.out.println("lstStatus.size():::: "+lstStatus.size());
						 for (SecondaryStatus secondaryStatus : lstStatus) {
							 if(secondaryStatus.getJobCategoryMaster()!=null)
							 {
								 stq+=" IF(secondarys3_.jobCategoryId="+secondaryStatus.getJobCategoryMaster().getJobCategoryId()+", ( secondaryStatusId >="+secondaryStatus.getSecondaryStatusId()+" ),false) or";
							 }
						}
						 if(stq.length()>3)
						 {
							 stq = stq.substring(0, stq.length()-2);
						 }
					}
				}
			}
			
			boolean questFlag = false;
			JSONArray[] gridJsonRecord = new JSONArray[2];
			List <DistrictMaster> districtMastersList=new ArrayList<DistrictMaster>();
			if(entityID==1 && entityTypeId==1 && dateFlag==false && applieddateFlag==false && status==false && internalCandVal==0){
				//<------------Quest Candidate Record For PDF-------------->
				gridJsonRecord=jobForTeacherDAO.getQuestTeacherDetailRecords_OP(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm,start,end,noOfRow);			
			}else{
				//shriram
				DistrictMaster dm=null;
				Criterion critdistrict=null;
				if(districtMaster!=null && entityID==2)
				{
					HeadQuarterMaster hq=districtMaster.getHeadQuarterMaster();
					if(hq!=null && hq.getHeadQuarterId()==3)
					{
						flagOhio8 = true;
					Criterion critheadquarter=Restrictions.eq("headQuarterMaster", hq);
					dm= districtMasterDAO.findById(3904380, false, false);
					if(districtMaster.getDistrictId()!=3904380)
					{			critdistrict=  Restrictions.ne("districtId", dm.getDistrictId());
					districtMastersList=districtMasterDAO.findByCriteria(critheadquarter,critdistrict);
					}
					else
						districtMastersList=districtMasterDAO.findByCriteria(critheadquarter);
					System.out.println("Size of DistrictmasterList"+districtMastersList.size());
				}
				}
				//ended by shriram
				if(!(entityTypeId==3 && lstJobOrderList.size()==0))
				{
					//<------------Quest Candidate Record For PDF-------------->
					gridJsonRecord=jobForTeacherDAO.getQuestTeacherDetailRecords_OP(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm,start,end,noOfRow);
				}
			}

	//<-----------PDF Generation Start------------->
	String fontPath = realPath;
	try {

		BaseFont.createFont(fontPath + "fonts/4637.ttf",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		BaseFont tahoma = BaseFont.createFont(fontPath
				+ "fonts/4637.ttf", BaseFont.CP1252,
				BaseFont.NOT_EMBEDDED);
		font8 = new Font(tahoma, 8);

		font8Green = new Font(tahoma, 8);
		font8Green.setColor(Color.BLUE);
		font8bold = new Font(tahoma, 8, Font.NORMAL);

		font9 = new Font(tahoma, 9);
		font9bold = new Font(tahoma, 9, Font.BOLD);
		font10 = new Font(tahoma, 10);

		font10_10 = new Font(tahoma, 10);
		font10_10.setColor(Color.white);
		font10bold = new Font(tahoma, 10, Font.BOLD);
		font11 = new Font(tahoma, 11);

		font11bold = new Font(tahoma, 11, Font.BOLD);
		bluecolor = new Color(0, 122, 180);

		// Color bluecolor = new Color(0,122,180);

		font20bold = new Font(tahoma, 20, Font.BOLD, bluecolor);
		// font20bold.setColor(Color.BLUE);
		font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);

	} catch (DocumentException e1) {
		e1.printStackTrace();
	} catch (IOException e1) {
		e1.printStackTrace();
	}

	document = new Document(PageSize.A4.rotate(), 10f, 10f, 10f, 10f);
	document.addAuthor("TeacherMatch");
	document.addCreator("TeacherMatch Inc.");
	document.addSubject("Ohio 8 Candidate List");
	document.addCreationDate();
	document.addTitle("Ohio 8 Candidate List");

	fos = new FileOutputStream(path);
	PdfWriter.getInstance(document, fos);

	document.open();

	PdfPTable mainTable = new PdfPTable(1);
	mainTable.setWidthPercentage(90);

	Paragraph[] para = null;
	PdfPCell[] cell = null;

	para = new Paragraph[3];
	cell = new PdfPCell[3];
	para[0] = new Paragraph(" ", font20bold);
	cell[0] = new PdfPCell(para[0]);
	cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
	cell[0].setBorder(0);
	mainTable.addCell(cell[0]);
	Image logo = Image.getInstance(fontPath
			+ "/images/Logo with Beta300.png");
	logo.scalePercent(75);

	// para[1] = new Paragraph(" ",font20bold);
	cell[1] = new PdfPCell(logo);
	cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
	cell[1].setBorder(0);
	mainTable.addCell(cell[1]);

	document.add(new Phrase("\n"));

	para[2] = new Paragraph("", font20bold);
	cell[2] = new PdfPCell(para[2]);
	cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
	cell[2].setBorder(0);
	mainTable.addCell(cell[2]);

	document.add(mainTable);

	document.add(new Phrase("\n"));

	float[] tblwidthz = { .15f };

	mainTable = new PdfPTable(tblwidthz);
	mainTable.setWidthPercentage(100);
	para = new Paragraph[1];
	cell = new PdfPCell[1];

	para[0] = new Paragraph("Ohio 8 Candidate List", font20bold);
	cell[0] = new PdfPCell(para[0]);
	cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
	cell[0].setBorder(0);
	mainTable.addCell(cell[0]);
	document.add(mainTable);

	document.add(new Phrase("\n"));

	float[] tblwidths = { .15f, .20f };

	mainTable = new PdfPTable(tblwidths);
	mainTable.setWidthPercentage(100);
	para = new Paragraph[2];
	cell = new PdfPCell[2];

	String name1 = userMaster.getFirstName() + " "
			+ userMaster.getLastName();

	para[0] = new Paragraph("Created By: " + name1, font10);
	cell[0] = new PdfPCell(para[0]);
	cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
	cell[0].setBorder(0);
	mainTable.addCell(cell[0]);

	para[1] = new Paragraph("" + "Created on: "
			+ Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),
			font10);
	cell[1] = new PdfPCell(para[1]);
	cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
	cell[1].setBorder(0);
	mainTable.addCell(cell[1]);

	document.add(mainTable);

	document.add(new Phrase("\n"));

	float[] tblwidth ={ .06f, .10f, .07f, .06f, .06f};
	float[] tblwidthOhio ={ .06f, .10f, .07f, .06f};
	/*if(!flagOhio8)
	{
		tblwieth= { .06f, .10f, .07f, .06f, .06f};
	}
	else
	{
		tblwidthOhio= { .06f, .10f, .07f, .06f};
	}*/
	
	if(!flagOhio8)
	mainTable = new PdfPTable(tblwidth);
	else
		mainTable = new PdfPTable(tblwidthOhio);
	mainTable.setWidthPercentage(100);
	para = new Paragraph[5];
	cell = new PdfPCell[5];
	// header

	// Shriram////
	para[0] = new Paragraph("" + lblCandidateFirstName, font10_10);
	cell[0] = new PdfPCell(para[0]);
	cell[0].setBackgroundColor(bluecolor);
	cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
	mainTable.addCell(cell[0]);
	
	para[1] = new Paragraph("" + lblCandidateLastName, font10_10);
	cell[1] = new PdfPCell(para[1]);
	cell[1].setBackgroundColor(bluecolor);
	cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
	mainTable.addCell(cell[1]);
	
	para[2] = new Paragraph("" + lblCandidateEmailAddress, font10_10);
	cell[2] = new PdfPCell(para[2]);
	cell[2].setBackgroundColor(bluecolor);
	cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
	mainTable.addCell(cell[2]);
 if(!flagOhio8)
 {
	para[3] = new Paragraph("" + "Norm Score", font10_10);
	cell[3] = new PdfPCell(para[3]);
	cell[3].setBackgroundColor(bluecolor);
	cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
	mainTable.addCell(cell[3]);

	para[4] = new Paragraph("" + "# of Jobs", font10_10);
	cell[4] = new PdfPCell(para[4]);
	cell[4].setBackgroundColor(bluecolor);
	cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
	mainTable.addCell(cell[4]);
 }
 else
 {
	 para[3] = new Paragraph("" + "# of Jobs", font10_10);
		cell[3] = new PdfPCell(para[3]);
		cell[3].setBackgroundColor(bluecolor);
		cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
		mainTable.addCell(cell[3]);
	 
 }
 

	document.add(mainTable);

	
	String sta = "";
	int counter = 0;
	String internalIcon = "";
	String ccsName="";
	String gridColor="class='bggrid'";
	
	if(gridJsonRecord[0].size()>0){
		System.out.println("gridJsonRecord[0]    "+gridJsonRecord[0].size());
		for (int i = 0; i < gridJsonRecord[0].size(); i++) {
			
			JSONObject jsonrec = new JSONObject();
			jsonrec = gridJsonRecord[0].getJSONObject(i);
			counter++;
			if(counter%2==0)
				gridColor="style='background-color:white;'";
			
				
						String[] row = null;
						int index = 0;
						
						if(!flagOhio8)
						mainTable = new PdfPTable(tblwidth);
						else
							mainTable = new PdfPTable(tblwidthOhio);
						
						mainTable.setWidthPercentage(100);
						para = new Paragraph[8];
						cell = new PdfPCell[8];
						
						para[index] = new Paragraph(jsonrec.get("teacherFirstName")+"", font8bold);
						cell[index] = new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
			
					
						para[index] = new Paragraph(jsonrec.get("teacherLastName")+"", font8bold);
						cell[index] = new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
			
						
						para[index] = new Paragraph(jsonrec.get("emailAddress")+"", font8bold);
						cell[index] = new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						if(!flagOhio8)
						{
							para[index] = new Paragraph(jsonrec.get("normScore")+"", font8bold);
							cell[index] = new PdfPCell(para[index]);
							cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							mainTable.addCell(cell[index]);
							index++;
						}
						
						if(!jsonrec.get("jobApply1").equals("0")){    
							para[index] = new Paragraph(jsonrec.get("jobApply1")+"/"+(Integer.parseInt((String) jsonrec.get("jobApply")) - Integer.parseInt((String) jsonrec.get("jobApply1")))+"", font8bold);
							cell[index] = new PdfPCell(para[index]);
							cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							mainTable.addCell(cell[index]);
							index++;
						}else{
							para[index] = new Paragraph("0"+"/"+(Integer.parseInt((String) jsonrec.get("jobApply")) - Integer.parseInt((String) jsonrec.get("jobApply1")))+"", font8bold);
							cell[index] = new PdfPCell(para[index]);
							cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							mainTable.addCell(cell[index]);
							index++;
						}
				
						document.add(mainTable);
					}
			}else{
				float[] tblwidth11 = { .10f };
				mainTable = new PdfPTable(tblwidth11);
				mainTable.setWidthPercentage(100);
				para = new Paragraph[1];
				cell = new PdfPCell[1];

				para[0] = new Paragraph(lblNoRecord, font8bold);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[0]);

				document.add(mainTable);
			}
		}
		}//<-----------PDF Generation End------------->
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (document != null && document.isOpen())
				document.close();
			if (writer != null) {
				writer.flush();
				writer.close();
			}
		}
		return true;
	}

public String displayTeacherGridForExcel_OP(String firstName,String lastName, String emailAddress,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String jobAppliedFromDate,String jobAppliedToDate,int daysVal)
{
	//Quest Candidate Excel Generation Method (questcandidatesnew.do)
	/* ========  For Session time Out Error =========*/
	
	String fileName = null;
	System.out.println("==================Quest Display Teacher Grid For Excel Method==========");
	/* ========  For Session time Out Error =========*/
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp); 
	}
	//-- get no of record in grid,
	//-- set start and end position
	
	int noOfRowInPage = Integer.parseInt(noOfRow);
	int pgNo = Integer.parseInt(pageNo);
	noOfRow = "0";
	int start =0;
	int end =0 ;
	int totalRecord = 0;
	boolean flagOhio8 = false;
	//------------------------------------
	StringBuffer tmRecords =	new StringBuffer();
	CandidateGridService cgService=new CandidateGridService();
	try{
		List<TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();

		/** set default sorting fieldName **/
		String sortOrderFieldName	=	"lastName";
		String sortOrderNoField		=	"lastName";

		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal=null;

		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
		}

		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
		//sortOrderStrVal=Order.asc("lastName");
		/**End ------------------------------------**/
		int roleId=0;
		int entityID=0;
		boolean isMiami = false;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityTypeId=1;
		int utype = 1;
		boolean writePrivilegeToSchool =false;
		boolean isManage = true;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
				if(districtMaster.getDistrictId().equals(1200390))
					isMiami = true;
				writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
			System.out.println("===========EntityId : "+entityID);
			utype = entityID;
		}
		
		if(isMiami && entityID==3)
			isManage=false;

		boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
		if(entityID==1)
		{
			achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
		}else
		{
			try{
				DistrictMaster districtMasterObj=districtMaster;
				if(districtMasterObj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(districtMasterObj.getDisplayTFA()){
					tFA=true;
				}
				if(districtMasterObj.getDisplayDemoClass()){
					demoClass=true;
				}
				if(districtMasterObj.getDisplayJSI()){
					JSI=true;
				}
				if(districtMasterObj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				if(districtMasterObj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				if(districtMasterObj.getDisplayFitScore()){
					fitScore=true;
				}
			}catch(Exception e){ e.printStackTrace();}
		}

		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,43,"teacherinfo.do",1);
		}catch(Exception e){
			e.printStackTrace();
		}
		Date fDate=null;
		Date tDate=null;
		Date appliedfDate=null;
		Date appliedtDate=null;
		boolean applieddateFlag=false;
		boolean dateFlag=false;
		try{
			// job Applied Filter
			if(jobAppliedFromDate!=null && !jobAppliedFromDate.equals("")){
				appliedfDate=Utility.getCurrentDateFormart(jobAppliedFromDate);
				applieddateFlag=true;
			}
			if(jobAppliedToDate!=null && !jobAppliedToDate.equals("")){
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(Utility.getCurrentDateFormart(jobAppliedToDate));
				cal2.add(Calendar.HOUR,23);
				cal2.add(Calendar.MINUTE,59);
				cal2.add(Calendar.SECOND,59);
				appliedtDate=cal2.getTime();
				applieddateFlag=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}			
		List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();
		boolean certTypeFlag=false;			
		System.out.println("jobOrderIds:: "+jobOrderIds.size());
		System.out.println("jobOrderIds:: "+jobOrderIds);
		System.out.println("certTypeFlag:: "+certTypeFlag);		
		List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
		List<TeacherDetail> NbyATeacherList = new ArrayList<TeacherDetail>();
		//List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
		boolean teacherFlag=false;			
		boolean nByAflag=false;		
		List<TeacherDetail> subjectTeacherList = new ArrayList<TeacherDetail>();
		boolean subjectFlag=false;
	
		if(subjectFlag && subjectTeacherList.size()>0){
			if(teacherFlag){
				filterTeacherList.retainAll(subjectTeacherList);
			}else{
				filterTeacherList.addAll(subjectTeacherList);
			}
		}
		System.out.println(" filterTeacherList :: "+filterTeacherList);
		if(subjectFlag){
			teacherFlag=true;
		}

		// Start /////////////////////////////// candidate list By certificate ///////////////////////////////////////
		StateMaster stateMasterForCandidate=null;
		List<CertificateTypeMaster> certTypeMasterListForCndt =new ArrayList<CertificateTypeMaster>();
		List<TeacherDetail> tListByCertificateTypeId = new ArrayList<TeacherDetail>();
		boolean tdataByCertTypeIdFlag = false;	
		
		/************* End **************/
		StatusMaster statusMaster = new StatusMaster();
		boolean status=false;			
		int internalCandVal=0;	
		SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
		SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
		List<JobForTeacher> lstJobForTeacher =	 new ArrayList<JobForTeacher>();
		List<JobOrder> lstJobOrderList =new ArrayList<JobOrder>();
		List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
		int noOfRecordCheck=0;
		
		SchoolMaster sm = schoolMaster;
		List<TeacherDetail> lstTeacherDetailAll= new ArrayList<TeacherDetail>();
		if(entityTypeId!=0){
			if(entityID==3){
				
				lstJobOrderSLList =schoolInJobOrderDAO.allJobOrderPostedBySchool(schoolMaster);
				entityTypeId=3;
			}				
			if(entityTypeId==3 && lstJobOrderSLList.size()>0 && certTypeFlag)
			{
				lstJobOrderList.addAll(getFilterJobOrder(lstJobOrderSLList,jobOrderIds));
			}
			else if(entityTypeId!=3 && lstJobOrderSLList.size()==0 && certTypeFlag)
			{
				lstJobOrderList.addAll(jobOrderIds);
				entityTypeId=3;
			}
			else
			{
				lstJobOrderList.addAll(lstJobOrderSLList);
			}
			if(entityID==2){
				entityTypeId=2;
			}
			
			String stq="";
			if(districtMaster!=null  && utype==3)
			{
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(smaster!=null)
				{
					String secStatus = smaster.getSecondaryStatusName();
					if(secStatus!=null && !secStatus.equals(""))
					{
						//
						 List<SecondaryStatus> lstStatus = new ArrayList<SecondaryStatus>();
						 lstStatus = secondaryStatusDAO.findSecondaryStatusByName(districtMaster,secStatus);
						 System.out.println("lstStatus.size():::: "+lstStatus.size());
						 for (SecondaryStatus secondaryStatus : lstStatus) {
							 if(secondaryStatus.getJobCategoryMaster()!=null)
							 {
								 stq+=" IF(secondarys3_.jobCategoryId="+secondaryStatus.getJobCategoryMaster().getJobCategoryId()+", ( secondaryStatusId >="+secondaryStatus.getSecondaryStatusId()+" ),false) or";
							 }
						}
						 if(stq.length()>3)
						 {
							 stq = stq.substring(0, stq.length()-2);
						 }
					}
				}
			}
			
			boolean questFlag = false;
			JSONArray[] gridJsonRecord = new JSONArray[2];
			List <DistrictMaster> districtMastersList=new ArrayList<DistrictMaster>();
			if(entityID==1 && entityTypeId==1 && dateFlag==false && applieddateFlag==false && status==false && internalCandVal==0){
				//<------------Quest Candidate Record For PDF-------------->
				gridJsonRecord=jobForTeacherDAO.getQuestTeacherDetailRecords_OP(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm,start,end,noOfRow);
				totalRecord=lstTeacherDetailAll.size();					
			}else{
				//shriram
				DistrictMaster dm=null;
				Criterion critdistrict=null;
				if(districtMaster!=null && entityID==2)
				{
					HeadQuarterMaster hq=districtMaster.getHeadQuarterMaster();
					if(hq!=null && hq.getHeadQuarterId()==3)
					{
						flagOhio8 = true;
					Criterion critheadquarter=Restrictions.eq("headQuarterMaster", hq);
					dm= districtMasterDAO.findById(3904380, false, false);
					if(districtMaster.getDistrictId()!=3904380)
					{			critdistrict=  Restrictions.ne("districtId", dm.getDistrictId());
					districtMastersList=districtMasterDAO.findByCriteria(critheadquarter,critdistrict);
					}
					else
						districtMastersList=districtMasterDAO.findByCriteria(critheadquarter);
					System.out.println("Size of DistrictmasterList"+districtMastersList.size());
				}
				}
				//ended by shriram
				if(!(entityTypeId==3 && lstJobOrderList.size()==0))
				{
					//<------------Quest Candidate Record For PDF-------------->
					gridJsonRecord=jobForTeacherDAO.getQuestTeacherDetailRecords_OP(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm,start,end,noOfRow);
				}
				totalRecord=lstTeacherDetailAll.size();
			}
			if(totalRecord<end)
				end=totalRecord;
			
			
			//<------------Quest Candidate Excel Generation Start-------------->
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = request.getSession().getServletContext().getRealPath("/")+ "/candidatelistAppiledforjob";
			
			fileName = "CandidateList.xls";

			File file = new File(basePath);
			if (!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);

			file = new File(basePath + "/" + fileName);

			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));

			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;

			WritableWorkbook workbook = Workbook.createWorkbook(file,wbSettings);
			workbook.createSheet("CandidateList", 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);

			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			header.setBackground(Colour.GRAY_25);

			// Write a few headers
			excelSheet.mergeCells(0, 0, 2, 1);
			Label label;
			label = new Label(0, 0, "Ohio 8 Candidate List",header);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 3, 2, 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			excelSheet.getSettings().setDefaultColumnWidth(18);

			int k = 4;
			int col = 1;
			label = new Label(0, k, "Candidate First Name", header);
			excelSheet.addCell(label);
			label = new Label(1, k, "Candidate Last Name ", header);
			excelSheet.addCell(label);
			label = new Label(++col, k, "Candidate Email Address", header);
			excelSheet.addCell(label);
			if(!flagOhio8)
			{
				label = new Label(++col, k, "Norm Score", header);
				excelSheet.addCell(label);
			}
			label = new Label(++col, k, "# of Jobs", header);
			excelSheet.addCell(label);
			k = k+1;
			
			if(gridJsonRecord[0].size()>0){
				System.out.println("gridJsonRecord[0]    "+gridJsonRecord[0].size());
				for (int i = 0; i < gridJsonRecord[0].size(); i++) {
					
					JSONObject jsonrec = new JSONObject();
					jsonrec = gridJsonRecord[0].getJSONObject(i);

					col = 1;

					label = new Label(0, k, jsonrec.get("teacherFirstName")+"");
					excelSheet.addCell(label);
					
					label = new Label(1, k, jsonrec.get("teacherLastName")+"");
					excelSheet.addCell(label);
					
					label = new Label(++col, k, jsonrec.get("emailAddress")+"");
					excelSheet.addCell(label);

					if(!flagOhio8)
					{
						label = new Label(++col, k, jsonrec.get("normScore")+"");
						excelSheet.addCell(label);
					}

					if(!jsonrec.get("jobApply1").equals("0")){    
						label = new Label(++col, k, jsonrec.get("jobApply1")+"/"+(Integer.parseInt((String) jsonrec.get("jobApply")) - Integer.parseInt((String) jsonrec.get("jobApply1")))+"");
						excelSheet.addCell(label);//mukesh
					}else{
						label = new Label(++col, k, "0"+"/"+(Integer.parseInt((String) jsonrec.get("jobApply")) - Integer.parseInt((String) jsonrec.get("jobApply1")))+"");
						excelSheet.addCell(label);
					}
					k++;
				}
			}else
			{
				excelSheet.mergeCells(0, 5, 2, 1);
				label = new Label(0, 5, "No records found");
				excelSheet.addCell(label);
			}
			workbook.write();
			workbook.close();
			//<------------Quest Candidate Excel Generation End-------------->
		}
	}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return fileName;
	}

public String displayOhioCandidateListPrintPreview_OP(String firstName,String lastName, String emailAddress,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String jobAppliedFromDate,String jobAppliedToDate,int daysVal)
{
	//Quest Candidate Print Preview Method (questcandidatesnew.do)
	/* ========  For Session time Out Error =========*/
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp); 
	}
	//-- get no of record in grid,
	//-- set start and end position

	int noOfRowInPage = Integer.parseInt(noOfRow);
	int pgNo = Integer.parseInt(pageNo);
	noOfRow = "0";
	int start =0;
	int end =0 ;
	int totalRecord = 0;
	boolean flagOhio8 = false;
	//------------------------------------
	StringBuffer tmRecords =	new StringBuffer();
	CandidateGridService cgService=new CandidateGridService();
	try{
		List<TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();

		/** set default sorting fieldName **/
		String sortOrderFieldName	=	"lastName";
		String sortOrderNoField		=	"lastName";

		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal=null;

		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
		}

		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
		//sortOrderStrVal=Order.asc("lastName");
		/**End ------------------------------------**/
		int roleId=0;
		int entityID=0;
		boolean isMiami = false;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityTypeId=1;
		int utype = 1;
		boolean writePrivilegeToSchool =false;
		boolean isManage = true;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
				if(districtMaster.getDistrictId().equals(1200390))
					isMiami = true;
				writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
			System.out.println("===========EntityId : "+entityID);
			utype = entityID;
		}
		
		if(isMiami && entityID==3)
			isManage=false;

		boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
		if(entityID==1)
		{
			achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
		}else
		{
			try{
				DistrictMaster districtMasterObj=districtMaster;
				if(districtMasterObj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(districtMasterObj.getDisplayTFA()){
					tFA=true;
				}
				if(districtMasterObj.getDisplayDemoClass()){
					demoClass=true;
				}
				if(districtMasterObj.getDisplayJSI()){
					JSI=true;
				}
				if(districtMasterObj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				if(districtMasterObj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				if(districtMasterObj.getDisplayFitScore()){
					fitScore=true;
				}
			}catch(Exception e){ e.printStackTrace();}
		}

		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,43,"teacherinfo.do",1);
		}catch(Exception e){
			e.printStackTrace();
		}
		Date fDate=null;
		Date tDate=null;
		Date appliedfDate=null;
		Date appliedtDate=null;
		boolean applieddateFlag=false;
		boolean dateFlag=false;
		try{
			// job Applied Filter
			if(jobAppliedFromDate!=null && !jobAppliedFromDate.equals("")){
				appliedfDate=Utility.getCurrentDateFormart(jobAppliedFromDate);
				applieddateFlag=true;
			}
			if(jobAppliedToDate!=null && !jobAppliedToDate.equals("")){
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(Utility.getCurrentDateFormart(jobAppliedToDate));
				cal2.add(Calendar.HOUR,23);
				cal2.add(Calendar.MINUTE,59);
				cal2.add(Calendar.SECOND,59);
				appliedtDate=cal2.getTime();
				applieddateFlag=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}			
		List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();
		boolean certTypeFlag=false;			
		System.out.println("jobOrderIds:: "+jobOrderIds.size());
		System.out.println("jobOrderIds:: "+jobOrderIds);
		System.out.println("certTypeFlag:: "+certTypeFlag);		
		List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
		List<TeacherDetail> NbyATeacherList = new ArrayList<TeacherDetail>();
		//List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
		boolean teacherFlag=false;			
		boolean nByAflag=false;		
		List<TeacherDetail> subjectTeacherList = new ArrayList<TeacherDetail>();
		boolean subjectFlag=false;

		if(subjectFlag && subjectTeacherList.size()>0){
			if(teacherFlag){
				filterTeacherList.retainAll(subjectTeacherList);
			}else{
				filterTeacherList.addAll(subjectTeacherList);
			}
		}
		System.out.println(" filterTeacherList :: "+filterTeacherList);
		if(subjectFlag){
			teacherFlag=true;
		}
		// Start /////////////////////////////// candidate list By certificate ///////////////////////////////////////
		StateMaster stateMasterForCandidate=null;
		List<CertificateTypeMaster> certTypeMasterListForCndt =new ArrayList<CertificateTypeMaster>();
		List<TeacherDetail> tListByCertificateTypeId = new ArrayList<TeacherDetail>();
		boolean tdataByCertTypeIdFlag = false;	
		
		/************* End **************/
		StatusMaster statusMaster = new StatusMaster();
		boolean status=false;			
		int internalCandVal=0;	
		SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
		SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
		List<JobForTeacher> lstJobForTeacher =	 new ArrayList<JobForTeacher>();
		List<JobOrder> lstJobOrderList =new ArrayList<JobOrder>();
		List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
		int noOfRecordCheck=0;
		
		SchoolMaster sm = schoolMaster;
		List<TeacherDetail> lstTeacherDetailAll= new ArrayList<TeacherDetail>();
		if(entityTypeId!=0){
			if(entityID==3){
				
				lstJobOrderSLList =schoolInJobOrderDAO.allJobOrderPostedBySchool(schoolMaster);
				entityTypeId=3;
			}				
			if(entityTypeId==3 && lstJobOrderSLList.size()>0 && certTypeFlag)
			{
				lstJobOrderList.addAll(getFilterJobOrder(lstJobOrderSLList,jobOrderIds));
			}
			else if(entityTypeId!=3 && lstJobOrderSLList.size()==0 && certTypeFlag)
			{
				lstJobOrderList.addAll(jobOrderIds);
				entityTypeId=3;
			}
			else
			{
				lstJobOrderList.addAll(lstJobOrderSLList);
			}
			if(entityID==2){
				entityTypeId=2;
			}
			
			String stq="";
			if(districtMaster!=null  && utype==3)
			{
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(smaster!=null)
				{
					String secStatus = smaster.getSecondaryStatusName();
					if(secStatus!=null && !secStatus.equals(""))
					{
						//
						 List<SecondaryStatus> lstStatus = new ArrayList<SecondaryStatus>();
						 lstStatus = secondaryStatusDAO.findSecondaryStatusByName(districtMaster,secStatus);
						 System.out.println("lstStatus.size():::: "+lstStatus.size());
						 for (SecondaryStatus secondaryStatus : lstStatus) {
							 if(secondaryStatus.getJobCategoryMaster()!=null)
							 {
								 stq+=" IF(secondarys3_.jobCategoryId="+secondaryStatus.getJobCategoryMaster().getJobCategoryId()+", ( secondaryStatusId >="+secondaryStatus.getSecondaryStatusId()+" ),false) or";
							 }
						}
						 if(stq.length()>3)
						 {
							 stq = stq.substring(0, stq.length()-2);
						 }
					}
				}
			}
			
			boolean questFlag = false;
			JSONArray[] gridJsonRecord = new JSONArray[2];
			List <DistrictMaster> districtMastersList=new ArrayList<DistrictMaster>();
			if(entityID==1 && entityTypeId==1 && dateFlag==false && applieddateFlag==false && status==false && internalCandVal==0){
				//<------------Quest Candidate Record For PDF-------------->
				gridJsonRecord=jobForTeacherDAO.getQuestTeacherDetailRecords_OP(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm,start,end,noOfRow);
			}else{
				//shriram
				DistrictMaster dm=null;
				Criterion critdistrict=null;
				if(districtMaster!=null && entityID==2)
				{
					HeadQuarterMaster hq=districtMaster.getHeadQuarterMaster();
					if(hq!=null && hq.getHeadQuarterId()==3)
					{
						flagOhio8 = true;
					Criterion critheadquarter=Restrictions.eq("headQuarterMaster", hq);
					dm= districtMasterDAO.findById(3904380, false, false);
					if(districtMaster.getDistrictId()!=3904380)
					{			critdistrict=  Restrictions.ne("districtId", dm.getDistrictId());
					districtMastersList=districtMasterDAO.findByCriteria(critheadquarter,critdistrict);
					}
					else
						districtMastersList=districtMasterDAO.findByCriteria(critheadquarter);
					System.out.println("Size of DistrictmasterList"+districtMastersList.size());
					}
				}
				//ended by shriram
				if(!(entityTypeId==3 && lstJobOrderList.size()==0))
				{
					//<------------Quest Candidate Record For PDF-------------->
					gridJsonRecord=jobForTeacherDAO.getQuestTeacherDetailRecords_OP(sortOrderStrVal,entityTypeId,lstJobOrderList,districtMaster,districtMastersList, firstName, lastName, emailAddress,dateFlag,fDate,tDate,teacherFlag,filterTeacherList,status,statusMaster,internalCandVal,applieddateFlag,appliedfDate,appliedtDate,daysVal,lstTeacherDetailAll,nByAflag,NbyATeacherList,utype,certTypeFlag,jobOrderIds,stq,sm,start,end,noOfRow);
				}
			}
			
			//<------------Quest Candidate Print Preview Start-------------->
			
			tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>Ohio 8 Candidate List</div><br/>");

			tmRecords.append("<div style='width:100%'>");
			tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "
							+ "Created by: "
							+ userMaster.getFirstName()
							+ " "
							+ userMaster.getLastName() + "</div>");
			tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>Date of Print: "
							+ Utility
									.convertDateAndTimeToUSformatOnlyDate(new Date())
							+ "</div>");
			tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");

			tmRecords.append("<table  id='tblGridOhioCandidateList' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");

			//Candidate First Name//
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ lblCandidateFirstName + "</th>");
			// Candidate Last Name
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ lblCandidateLastName + "</th>");
			// Candidate Email
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ lblCandidateEmailAddress + "</th>");
			// Norm Score
			if(!flagOhio8)
			{
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ "Norm Score" + "</th>");
			}
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"
							+ "# of Jobs" + "</th>");

			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");

		
			if(gridJsonRecord[0].size()>0){
				System.out.println("gridJsonRecord[0]    "+gridJsonRecord[0].size());
				for (int i = 0; i < gridJsonRecord[0].size(); i++) {
					JSONObject jsonrec = new JSONObject();
					jsonrec = gridJsonRecord[0].getJSONObject(i);
					
					tmRecords.append("<tr>");
					tmRecords.append("<td>" + jsonrec.get("teacherFirstName") + "</td>");
					tmRecords.append("<td>" + jsonrec.get("teacherLastName") + "</td>");
					tmRecords.append("<td>" + jsonrec.get("emailAddress") + "</td>");

					if(!flagOhio8)
						tmRecords.append("<td>" + jsonrec.get("normScore") + "</td>");
					
					if(!jsonrec.get("jobApply1").equals("0"))     
						tmRecords.append("<td>" + jsonrec.get("jobApply1")+"/"+(Integer.parseInt((String) jsonrec.get("jobApply")) - Integer.parseInt((String) jsonrec.get("jobApply1")))+ "</td>");
					else
						tmRecords.append("<td>" + "0"+"/"+(Integer.parseInt((String) jsonrec.get("jobApply")) - Integer.parseInt((String) jsonrec.get("jobApply1")))+ "</td>");
					tmRecords.append("</tr>");
				}
			}else
			{
				tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"
						+ Utility.getLocaleValuePropByKey(
								"lblNoRecord", locale) + "</td></tr>");
			}
			tmRecords.append("</tbody>");
			tmRecords.append("</table>");
			//<------------Quest Candidate Print Preview End-------------->
		}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return tmRecords.toString();
	}


}	