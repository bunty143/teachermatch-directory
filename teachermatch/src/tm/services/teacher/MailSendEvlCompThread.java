package tm.services.teacher;

import org.springframework.beans.factory.annotation.Autowired;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;

public class MailSendEvlCompThread extends Thread{
	@Autowired
	private OnlineActivityAjax onlineActivityAjax;
	

	private UserMaster schoolUser;	
	private JobOrder jobId;
	private TeacherDetail teacherId;
	private Integer districtId;
	private UserMaster userMaster;
	private String statusFlagAndStatusId;
	private boolean mailSend=true;
	private int temp;
	
	public boolean isMailSend() {
		return mailSend;
	}
	
	
		
	public void setStatusFlagAndStatusId(String statusFlagAndStatusId) {
		this.statusFlagAndStatusId = statusFlagAndStatusId;
	}



	public void setTemp(int temp) {
		this.temp = temp;
	}	

	public void setSchoolUser(UserMaster schoolUser) {
		this.schoolUser = schoolUser;
	}

	public void setOnlineActivityAjax(
			OnlineActivityAjax onlineActivityAjax) {
		this.onlineActivityAjax = onlineActivityAjax;
	}

	public void setJobId(JobOrder jobId) {
		this.jobId = jobId;
	}
	
	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
		
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public MailSendEvlCompThread() {
		super();
	}
	
	public void run()
	{
	 try{
		 onlineActivityAjax.sendEvlComplMail(teacherId,jobId,districtId,userMaster,temp,schoolUser,statusFlagAndStatusId);		
		}catch (Exception e) {
			e.printStackTrace();
			mailSend=false;
		}
	}
}
