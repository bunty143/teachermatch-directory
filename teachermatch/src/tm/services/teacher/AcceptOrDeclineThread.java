package tm.services.teacher;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
/*
*@Author: Ram Nath
*/
public class AcceptOrDeclineThread implements Runnable{
	
	@Autowired
	private AcceptOrDeclineAjax acceptOrDeclineAjax;
	//private StatelessSession statelessSession;	
	private Session statelessSession;
	private HttpServletRequest request;
	private JobOrder jobId;
	private TeacherDetail teacherId;
	private DistrictMaster districtId;
	private UserMaster userMaster;
	private SchoolMaster schoolId;
	private Integer callbytemplete;
	private String statusFlagAndStatusId;
	private String callingControllerText;
	private String subject;
	private String emailId;
	private String[] ccEmailId;
	private String flowOptionsName;

	public void setFlowOptionsName(String flowOptionsName) {
		this.flowOptionsName = flowOptionsName;
	}

	public void setAcceptOrDeclineAjax(AcceptOrDeclineAjax acceptOrDeclineAjax) {
		this.acceptOrDeclineAjax = acceptOrDeclineAjax;
	}

	

	/*public void setStatelessSession(StatelessSession statelessSession) {
		this.statelessSession = statelessSession;
	}*/
	
	public void setStatelessSession(Session statelessSession) {
		this.statelessSession = statelessSession;
	}



	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}



	public void setJobId(JobOrder jobId) {
		this.jobId = jobId;
	}



	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}



	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}



	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}



	public void setCallbytemplete(Integer callbytemplete) {
		this.callbytemplete = callbytemplete;
	}



	public void setStatusFlagAndStatusId(String statusFlagAndStatusId) {
		this.statusFlagAndStatusId = statusFlagAndStatusId;
	}



	public void setCallingControllerText(String callingControllerText) {
		this.callingControllerText = callingControllerText;
	}
	


	public void setSubject(String subject) {
		this.subject = subject;
	}
	

	public void setSchoolId(SchoolMaster schoolId) {
		this.schoolId = schoolId;
	}
	

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public void setCcEmailId(String[] ccEmailId) {
		this.ccEmailId = ccEmailId;
	}



	public void run()
	{
	 try{
		 if(callbytemplete!=null){
			 System.out.println("callingControllerText=="+callingControllerText);
			 Boolean flagNotification=callingControllerText.contains("_NF_");
			 System.out.println("flageeeeeeeeeeee============"+flagNotification);
			 if(flagNotification!=null && flagNotification){
				 acceptOrDeclineAjax.sendNotification(teacherId, jobId, districtId, userMaster, schoolId, callbytemplete, statusFlagAndStatusId, callingControllerText, subject, emailId, request, ccEmailId, statelessSession);
			 }else{
				 acceptOrDeclineAjax.sendAcceptOrDecline(teacherId,jobId,districtId,userMaster,schoolId,callbytemplete,statusFlagAndStatusId,callingControllerText,subject,emailId,flowOptionsName,request,ccEmailId,statelessSession);
			 }
		 }
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
