package tm.services.teacher;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.WordUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherSubjectAreaExam;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CertificateNameMaster;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.CityMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.DegreeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.FieldOfStudyMaster;
import tm.bean.master.GenderMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.RaceMaster;
import tm.bean.master.RegionMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.SubjectAreaExamMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.master.UniversityMaster;
import tm.bean.textfile.DegreeTypeMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherGeneralKnowledgeExamDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherSubjectAreaExamDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.CertificateNameMasterDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.DegreeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.FieldOfStudyMasterDAO;
import tm.dao.master.GenderMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.RegionMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.SubjectAreaExamMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.UniversityMasterDAO;
import tm.dao.textfile.DegreeTypeMasterDAO;
import tm.services.PaginationAndSorting;
import tm.utility.ElasticSearchConfig;
import tm.utility.Utility;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class DWRAutoComplete 
{
	
	  String locale = Utility.getValueOfPropByKey("locale");
	 String optAll=Utility.getLocaleValuePropByKey("optAll", locale);
	 String lblSchoolName=Utility.getLocaleValuePropByKey("lblSchoolName", locale);
	 String lblAdd=Utility.getLocaleValuePropByKey("lblAdd", locale);
	 String msgNorecordfound=Utility.getLocaleValuePropByKey("msgNorecordfound", locale);
	 String lblAllSubjects=Utility.getLocaleValuePropByKey("msgNorecordfound", locale);
	 String optStlSub=Utility.getLocaleValuePropByKey("optStlSub", locale);
	 String optSltDistForReleventSubjectList=Utility.getLocaleValuePropByKey("optSltDistForReleventSubjectList", locale);
	 String lblRac=Utility.getLocaleValuePropByKey("lblRac", locale);
	 String lblGend=Utility.getLocaleValuePropByKey("lblGend", locale);
	 
	 
	@Autowired
	private SubjectAreaExamMasterDAO subjectAreaExamMasterDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private TeacherSubjectAreaExamDAO teacherSubjectAreaExamDAO;
	
	@Autowired
	private TeacherGeneralKnowledgeExamDAO teacherGeneralKnowledgeExamDAO;
	
	@Autowired
	private DegreeMasterDAO degreeMasterDAO;
	public void setDegreeMasterDAO(DegreeMasterDAO degreeMasterDAO) 
	{
		this.degreeMasterDAO = degreeMasterDAO;
	}
	
	@Autowired
	private FieldOfStudyMasterDAO fieldOfStudyMasterDAO;
	public void setFieldOfStudyMasterDAO(FieldOfStudyMasterDAO fieldOfStudyMasterDAO) 
	{
		this.fieldOfStudyMasterDAO = fieldOfStudyMasterDAO;
	}
	
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	public void setSubjectMasterDAO(SubjectMasterDAO subjectMasterDAO) {
		this.subjectMasterDAO = subjectMasterDAO;
	}
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;

	@Autowired
	private UniversityMasterDAO universityMasterDAO;
	public void setUniversityMasterDAO(UniversityMasterDAO universityMasterDAO) 
	{
		this.universityMasterDAO = universityMasterDAO;
	}
	
	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
	public void setCertificateTypeMasterDAO(CertificateTypeMasterDAO certificateTypeMasterDAO) 
	{
		this.certificateTypeMasterDAO = certificateTypeMasterDAO;
	}
	
	@Autowired
	private CertificateNameMasterDAO certificateNameMasterDAO;
	public void setCertificateNameMasterDAO(CertificateNameMasterDAO certificateNameMasterDAO) 
	{
		this.certificateNameMasterDAO = certificateNameMasterDAO;
	}
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) 
	{
		this.stateMasterDAO = stateMasterDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
	{
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	@Autowired
	private RegionMasterDAO regionMasterDAO;
	public void setRegionMasterDAO(RegionMasterDAO regionMasterDAO) {
		this.regionMasterDAO = regionMasterDAO;
	}
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) {
		this.cityMasterDAO = cityMasterDAO;
	}
	
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	public void setHeadQuarterMasterDAO(HeadQuarterMasterDAO headQuarterMasterDAO) 
	{
		this.headQuarterMasterDAO = headQuarterMasterDAO;
	}
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	public void setBranchMasterDAO(BranchMasterDAO branchMasterDAO) 
	{
		this.branchMasterDAO = branchMasterDAO;
	}
	
	@Autowired
	private CountryMasterDAO countryMasterDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	
	@Autowired
	private DegreeTypeMasterDAO degreeTypeMasterDAO;
	
	@Autowired
	GenderMasterDAO genderMasterDao;
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	
	public void setDegreeTypeMasterDAO(DegreeTypeMasterDAO degreeTypeMasterDAO) 
	{
		this.degreeTypeMasterDAO = degreeTypeMasterDAO;
	}
	
	public List<DegreeMaster> getDegreeMasterList(String degreeName)
	{
		List<DegreeMaster> degreeMasterList = new ArrayList<DegreeMaster>();
		
		List<DegreeMaster> degreeMasterList1 = null;
		List<DegreeMaster> degreeMasterList2 = null;
		List<DegreeMaster> degreeMasterList3 = null;
		
		Criterion criterion = null;
		Criterion criterion2 = null;
		Criterion criterion3 = null;
		
		try 
		{
			criterion = Restrictions.ilike("degreeName",degreeName.trim()+"%" );
			degreeMasterList1 = degreeMasterDAO.findWithLimit(Order.asc("degreeName"), 0,5, criterion);
			degreeMasterList.addAll(degreeMasterList1);
			
			if(degreeMasterList.size()<15)
			{
				criterion2 = Restrictions.ilike("degreeName","% "+degreeName.trim()+"%" );			
				degreeMasterList2 = degreeMasterDAO.findWithLimit(Order.asc("degreeName"), 0,5, criterion2);
				degreeMasterList.addAll(degreeMasterList2);
			}
			
			if(degreeMasterList.size()<15)
			{
				criterion3 = Restrictions.ilike("degreeName","%"+degreeName.trim()+"%" );
				degreeMasterList3 = degreeMasterDAO.findWithLimit(Order.asc("degreeName"), 0,5, criterion3);
				degreeMasterList.addAll(degreeMasterList3);
			}
			
			Set<DegreeMaster> setDegreeMaster= new LinkedHashSet<DegreeMaster>(degreeMasterList);
			degreeMasterList = new ArrayList<DegreeMaster>(new LinkedHashSet<DegreeMaster>(setDegreeMaster));
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return degreeMasterList;
	}
	
	public List<RegionMaster> getRegionMasterList(String regionName)
	{
		List<RegionMaster> regionMasterList = new ArrayList<RegionMaster>();
		List<RegionMaster> regionMasterList1 = null;
		List<RegionMaster> regionMasterList2 = null;
		try 
		{
			Criterion criterion1 = Restrictions.ne("parentRegionId",0);
			Criterion criterion = Restrictions.like("regionName", regionName,MatchMode.START);
			regionMasterList1 = regionMasterDAO.findWithLimit(Order.asc("regionName"), 0, 10, criterion,criterion1);
						
			Criterion criterion2 = Restrictions.ilike("regionName","% "+regionName.trim()+"%" );
			regionMasterList2 = regionMasterDAO.findWithLimit(Order.asc("regionName"), 0, 10, criterion2,criterion1);
			
			regionMasterList.addAll(regionMasterList1);
			regionMasterList.addAll(regionMasterList2);
			
			
			Set<RegionMaster> setRegionMasters = new LinkedHashSet<RegionMaster>(regionMasterList);			
			regionMasterList = new ArrayList<RegionMaster>(new LinkedHashSet<RegionMaster>(setRegionMasters));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return regionMasterList;
	}
	
	public List<FieldOfStudyMaster> getFieldOfStudyList(String fieldOfStudy)
	{
		List<FieldOfStudyMaster> fieldOfStudyList = new ArrayList<FieldOfStudyMaster>();
		List<FieldOfStudyMaster> fieldOfStudyList1 = null;
		List<FieldOfStudyMaster> fieldOfStudyList2 = null;
		try 
		{
			Criterion criterion = Restrictions.like("fieldName", fieldOfStudy,MatchMode.START);
			fieldOfStudyList1 = fieldOfStudyMasterDAO.findWithLimit(Order.asc("fieldName"), 0, 10, criterion);
						
			Criterion criterion2 = Restrictions.ilike("fieldName","% "+fieldOfStudy.trim()+"%" );
			fieldOfStudyList2 = fieldOfStudyMasterDAO.findWithLimit(Order.asc("fieldName"), 0, 10, criterion2);
			
			fieldOfStudyList.addAll(fieldOfStudyList1);
			fieldOfStudyList.addAll(fieldOfStudyList2);
			
			
			Set<FieldOfStudyMaster> setFieldOfStudyMasters = new LinkedHashSet<FieldOfStudyMaster>(fieldOfStudyList);			
			fieldOfStudyList = new ArrayList<FieldOfStudyMaster>(new LinkedHashSet<FieldOfStudyMaster>(setFieldOfStudyMasters));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return fieldOfStudyList;
	}
	
	/* Gagan : Auto Complete  for Subject master */
	public List<SubjectMaster> getSubjectList(String subjectName,Integer districtId)
	{
		
		List<SubjectMaster> subjectList = new ArrayList<SubjectMaster>();
		List<SubjectMaster> subjectList1 = null;
		List<SubjectMaster> subjectList2 = null;
		try 
		{
			
			if(districtId!=null && !districtId.equals("")){
				
				DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
				Criterion criterion3 = Restrictions.eq("districtMaster", districtMaster);
				
				Criterion criterion = Restrictions.like("subjectName", subjectName,MatchMode.START);
				Criterion criterion1 = Restrictions.like("status", "A");
				subjectList1 = subjectMasterDAO.findWithLimit(Order.asc("subjectName"), 0, 10, criterion,criterion1,criterion3);
							
				Criterion criterion2 = Restrictions.ilike("subjectName","% "+subjectName.trim()+"%" );
				subjectList2 = subjectMasterDAO.findWithLimit(Order.asc("subjectName"), 0, 10, criterion1,criterion2,criterion3);
			
				
				subjectList.addAll(subjectList1);
				subjectList.addAll(subjectList2);
				
			}/*else{
				
				Criterion criterion = Restrictions.like("subjectName", subjectName,MatchMode.START);
				Criterion criterion1 = Restrictions.like("status", "A");
				subjectList1 = subjectMasterDAO.findWithLimit(Order.asc("subjectName"), 0, 10, criterion,criterion1);
							
				Criterion criterion2 = Restrictions.ilike("subjectName","% "+subjectName.trim()+"%" );
				subjectList2 = subjectMasterDAO.findWithLimit(Order.asc("subjectName"), 0, 10, criterion1,criterion2);
				
			}*/
			
			
			
			
			Set<SubjectMaster> setSubjectMasters = new LinkedHashSet<SubjectMaster>(subjectList);			
			subjectList = new ArrayList<SubjectMaster>(new LinkedHashSet<SubjectMaster>(setSubjectMasters));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return subjectList;
	}
	
	
	/* Gagan : Auto Complete  for Requisition Number */
	public List<DistrictRequisitionNumbers> getRequisationList(String requisitionNumber,int districtId)
	{
		//System.out.println(" requisitionNumber  ---- "+requisitionNumber);
		
		List<DistrictRequisitionNumbers> requisationList = new ArrayList<DistrictRequisitionNumbers>();
		List<DistrictRequisitionNumbers> requisationList1 = new ArrayList<DistrictRequisitionNumbers>();
		List<DistrictRequisitionNumbers> requisationList2 = new ArrayList<DistrictRequisitionNumbers>();
		DistrictMaster districtMaster = new DistrictMaster();
		try 
		{
			//System.out.println("\n districtId :"+districtId);
			districtMaster = districtMasterDAO.findById(districtId, false, false);
			Criterion criterion = Restrictions.like("requisitionNumber", ""+requisitionNumber,MatchMode.ANYWHERE);
			Criterion criterion1 = Restrictions.eq("isUsed", false);
			Criterion criterion3 = Restrictions.eq("districtMaster", districtMaster);
			requisationList1 = districtRequisitionNumbersDAO.findWithLimit(Order.asc("requisitionNumber"), 0, 10, criterion,criterion1,criterion3);
			requisationList.addAll(requisationList1);
			
			Set<DistrictRequisitionNumbers> setDistrictRequisitionNumbers = new LinkedHashSet<DistrictRequisitionNumbers>(requisationList);			
			requisationList = new ArrayList<DistrictRequisitionNumbers>(new LinkedHashSet<DistrictRequisitionNumbers>(setDistrictRequisitionNumbers));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return requisationList;
	}
	
	public List<UniversityMaster> getUniversityMasterList(String universityName)
	{
		List<UniversityMaster> universityList =  new ArrayList<UniversityMaster>();
		List<UniversityMaster> universityList1 = null;
		List<UniversityMaster> universityList2 = null;
		try 
		{
			Criterion criterion = Restrictions.like("universityName", universityName,MatchMode.START);
			universityList1 = universityMasterDAO.findWithLimit(Order.asc("universityName"), 0, 10, criterion);
			
			Criterion criterion2 = Restrictions.ilike("universityName","% "+universityName.trim()+"%" );
			universityList2 = universityMasterDAO.findWithLimit(Order.asc("universityName"), 0, 10, criterion2);
			
			
			universityList.addAll(universityList1);
			universityList.addAll(universityList2);
			
			Set<UniversityMaster> setUniversityMaster = new LinkedHashSet<UniversityMaster>(universityList);
			universityList = new ArrayList<UniversityMaster>(new LinkedHashSet<UniversityMaster>(setUniversityMaster));
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return universityList;
	}
	
	public List<CertificateTypeMaster> getCertificateTypeList(String certType, String state)
	{

		StateMaster stateMaster = null;
		Long stateId=0l;
		try
		{
			stateId = Long.parseLong(state);
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		stateMaster = stateMasterDAO.findById(stateId, false, false);
		
		
		
		List<CertificateTypeMaster> certList =  new ArrayList<CertificateTypeMaster>();
		List<CertificateTypeMaster> certList1 = null;
		List<CertificateTypeMaster> certList2 = null;
		try 
		{
			Criterion criterionMode1 = Restrictions.like("certType", certType,MatchMode.START);
			Criterion criterionMode2 = Restrictions.eq("stateId",stateMaster);
			Criterion criterionMode3 = Restrictions.eq("status","A");
			Criterion criterionMode = Restrictions.and(criterionMode1,criterionMode2);
			criterionMode = Restrictions.and(criterionMode,criterionMode3);
			certList1 = certificateTypeMasterDAO.findWithLimit(Order.asc("certType"), 0, 10, criterionMode);
			
			Criterion criterionLike1 = Restrictions.ilike("certType","% "+certType.trim()+"%");
			Criterion criterionLike2 = Restrictions.eq("stateId",stateMaster);
			Criterion criterionLike3 = Restrictions.eq("status","A");
			Criterion criterionLike = Restrictions.and(criterionLike1,criterionLike2);			
			criterionLike = Restrictions.and(criterionLike,criterionLike3);
			certList2 = certificateTypeMasterDAO.findWithLimit(Order.asc("certType"), 0, 10, criterionLike);
			
			certList.addAll(certList1);
			certList.addAll(certList2);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return certList;
	}
	
	public List<CertificateNameMaster> getCertificateNameList(String certName, String certTypeId)
	{

		Integer certficateTypeId=0;
		try
		{
			certficateTypeId = Integer.parseInt(certTypeId);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		System.out.println("certficateTypeId: "+certficateTypeId);
		CertificateTypeMaster certificateTypeMaster = new CertificateTypeMaster(); //certificateTypeMasterDAO.findById(certficateTypeId, false, false);
		certificateTypeMaster.setCertTypeId(certficateTypeId);
		List<CertificateNameMaster> certNameList =  new ArrayList<CertificateNameMaster>();
		List<CertificateNameMaster> certNameList1 = null;
		List<CertificateNameMaster> certNameList2 = null;
		try 
		{
			Criterion criterionMode1 = Restrictions.like("certName", certName,MatchMode.START);
			Criterion criterionMode2 = Restrictions.eq("certificateTypeMaster", certificateTypeMaster);
			Criterion criterionMode = Restrictions.and(criterionMode1,criterionMode2);
			
			certNameList1 = certificateNameMasterDAO.findWithLimit(Order.asc("certName"), 0, 10, criterionMode);
			
			Criterion criterionLike1 = Restrictions.ilike("certName","% "+certName.trim()+"%" );
			Criterion criterionLike2 = Restrictions.eq("certificateTypeMaster", certificateTypeMaster);
			Criterion criterionLike = Restrictions.and(criterionLike1,criterionLike2);
			
			
			certNameList2 = certificateNameMasterDAO.findWithLimit(Order.asc("certName"), 0, 10, criterionLike);
			
			certNameList.addAll(certNameList1);
			certNameList.addAll(certNameList2);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return certNameList;
	}
	
	//@vishwanath 
	public List<CertificateNameMaster> getAllCertificateNameList(String certName)
	{

		List<CertificateNameMaster> certNameList =  new ArrayList<CertificateNameMaster>();
		List<CertificateNameMaster> certNameList1 = null;
		List<CertificateNameMaster> certNameList2 = null;
		try 
		{
			Criterion criterionMode1 = Restrictions.like("certName", certName,MatchMode.START);
			certNameList1 = certificateNameMasterDAO.findWithLimit(Order.asc("certName"), 0, 10, criterionMode1);
			Criterion criterionLike1 = Restrictions.ilike("certName","% "+certName.trim()+"%" );
			certNameList2 = certificateNameMasterDAO.findWithLimit(Order.asc("certName"), 0, 10, criterionLike1);
			
			certNameList.addAll(certNameList1);
			certNameList.addAll(certNameList2);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return certNameList;
	}
	//@vishwanath 
	public List<CertificateTypeMaster> getAllCertificateTypeList(String certType)
	{

		
		List<CertificateTypeMaster> certList =  new ArrayList<CertificateTypeMaster>();
		List<CertificateTypeMaster> certList1 = null;
		List<CertificateTypeMaster> certList2 = null;
		try 
		{
			Criterion criterionMode1 = Restrictions.like("certType", certType,MatchMode.START);
			Criterion statusCriteria = Restrictions.eq("status", "A");
			certList1 = certificateTypeMasterDAO.findWithLimit(Order.asc("certType"), 0, 10, criterionMode1,statusCriteria);
			
			Criterion criterionLike1 = Restrictions.ilike("certType","% "+certType.trim()+"%" );
			certList2 = certificateTypeMasterDAO.findWithLimit(Order.asc("certType"), 0, 10, criterionLike1,statusCriteria);
			
			certList.addAll(certList1);
			certList.addAll(certList2);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return certList;
	}
	public List<CertificateTypeMaster> getAllCertificateTypeList(String certType,boolean flag)
	{

		
		List<CertificateTypeMaster> certList =  new ArrayList<CertificateTypeMaster>();
		List<CertificateTypeMaster> certList1 = null;
		List<CertificateTypeMaster> certList2 = null;
		try 
		{
			Criterion criterionMode1 = Restrictions.like("certType", certType,MatchMode.START);
			Criterion statusCriteria = Restrictions.eq("status", "A");
			certList1 = certificateTypeMasterDAO.findWithLimit(Order.asc("certType"), 0, 10, criterionMode1,statusCriteria);
			
			Criterion criterionLike1 = Restrictions.ilike("certType","% "+certType.trim()+"%" );
			certList2 = certificateTypeMasterDAO.findWithLimit(Order.asc("certType"), 0, 10, criterionLike1,statusCriteria);
			
			certList.addAll(certList1);
			certList.addAll(certList2);
			
			//change NC to platform
			System.out.println("before api call----"+certList.size());
			if(certList.size()<10 && ElasticSearchConfig.serverName!=null && ElasticSearchConfig.serverName.equalsIgnoreCase("platform"))
			{
				/*ClientConfig config = new DefaultClientConfig();
				URI uri=null;
			    Client client = Client.create(config);
			    try {
			    	//System.out.println("http://192.168.0.18:8080/teachermatch/services/callback/getDistrictMasterList/"+URLEncoder.encode(DistrictName,"UTF-8"));
					uri=new URI(ElasticSearchConfig.serviceURL+"/services/callback/getAllCertificateTypeList/"+URLEncoder.encode(certType,"UTF-8"));
					System.out.println(ElasticSearchConfig.serviceURL+"/services/callback/getAllCertificateTypeList/"+URLEncoder.encode(certType,"UTF-8"));
			    	//uri=new URI("https://titan.teachermatch.org/services/callback/getDistrictMasterList/"+URLEncoder.encode(DistrictName,"UTF-8"));
					//uri=new URI("http://192.168.0.18:8080/teachermatch/services/callback/getDistrictMasterList/"+DistrictName);
				} catch (URISyntaxException e) {
					
					e.printStackTrace();
				}*/
			    
			   
			    try 
			    {
			    	/*WebResource service = client.resource(uri);
				    ClientResponse response = null;
				    response = service.type(MediaType.TEXT_HTML).get(ClientResponse.class);
				    String s=response.getEntity(String.class);
				    System.out.println(s);
				    JSONObject jsonObject=new JSONObject();
				    jsonObject=jsonObject.fromObject(s);*/
			    	
			    	String sURLData="";
					JSONObject jsonObject=new JSONObject();
					URL oracle = new URL(ElasticSearchConfig.serviceURL+"/services/callback/getAllCertificateTypeList/"+URLEncoder.encode(certType,"UTF-8"));
					HttpsURLConnection yc = (HttpsURLConnection)oracle.openConnection();
					yc.setRequestProperty("User-Agent", "Mozilla/5.0");
					BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
				    String inputLine;
			        while ((inputLine = in.readLine()) != null) 
			        {
			           // System.out.println(inputLine);
			            sURLData=sURLData+inputLine;
			        }
			        in.close();
			        //System.out.println(sURLData);  
			        jsonObject=jsonObject.fromObject(sURLData);
					
					JSONArray certArray= (JSONArray) jsonObject.get("certList");
					 for(int i=0; i<certArray.size(); i++)
					 {
						CertificateTypeMaster certificateTypeMaster=new CertificateTypeMaster();
						StateMaster stateMaster=new StateMaster();
						jsonObject=(JSONObject)certArray.get(i);
						certificateTypeMaster.setCertTypeId(jsonObject.getInt("certTypeId"));
						certificateTypeMaster.setCertType(jsonObject.getString("certType"));
						stateMaster.setStateName(jsonObject.getString("stateName"));
						certificateTypeMaster.setStateId(stateMaster);
						certList.add(certificateTypeMaster);
						
						
					}
					


				} catch (Exception e) {
					e.printStackTrace();
				}
			} 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		System.out.println("after api call----"+certList.size());
		return certList;
	}
	
	public List<DistrictMaster> getDistrictMasterList(String DistrictName)
	{	
		
		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		try{
			
			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "A");
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				System.out.println(districtMasterDAO);
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);
				
				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);
				
				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
				
				
			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25,criterion1);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return districtMasterList;
	}
	
	public List<DistrictMaster> getDistrictMasterList(String DistrictName,boolean flag)
	{	
		
		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		try{
			
			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "A");
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				System.out.println(districtMasterDAO);
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);
				
				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);
				
				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
				//change NC to platform
				System.out.println("before api call----"+districtMasterList.size());
				if(districtMasterList.size()<10 && ElasticSearchConfig.serverName!=null && ElasticSearchConfig.serverName.equalsIgnoreCase("platform"))
				{
					/*ClientConfig config = new DefaultClientConfig();
					URI uri=null;
				    Client client = Client.create(config);
				    try {
				    	//System.out.println("http://192.168.0.18:8080/teachermatch/services/callback/getDistrictMasterList/"+URLEncoder.encode(DistrictName,"UTF-8"));
						//uri=new URI("http://192.168.0.18:8080/teachermatch/services/callback/getDistrictMasterList/"+URLEncoder.encode(DistrictName,"UTF-8"));
				    	//uri=new URI("https://titan.teachermatch.org/services/callback/getDistrictMasterList/"+URLEncoder.encode(DistrictName,"UTF-8"));
				    	//uri=new URI("https://cloud.teachermatch.org/services/callback/getDistrictMasterList/"+URLEncoder.encode(DistrictName,"UTF-8"));
				    	//uri=new URI("https://tmnc-a.teachermatch.org/services/callback/getDistrictMasterList/"+URLEncoder.encode(DistrictName,"UTF-8"));
				    	uri=new URI("https://nc.teachermatch.org/services/callback/getDistrictMasterList/"+URLEncoder.encode(DistrictName,"UTF-8"));
						//uri=new URI("http://192.168.0.18:8080/teachermatch/services/callback/getDistrictMasterList/"+DistrictName);
				    	//uri=new URI(ElasticSearchConfig.serviceURL+"/services/callback/getDistrictMasterList/"+URLEncoder.encode(DistrictName,"UTF-8"));
					} catch (URISyntaxException e) {
						
						e.printStackTrace();
					}*/
				    
					try 
					{
					  /*  WebResource service = client.resource(uri);
					    ClientResponse response = null;
					    response = service.type(MediaType.TEXT_HTML).get(ClientResponse.class);
						  String s=response.getEntity(String.class);
					    System.out.println(s);
					   jsonObject=jsonObject.fromObject(s);
				   */
						
						String sURLData="";
						JSONObject jsonObject=new JSONObject();
						URL oracle = new URL(ElasticSearchConfig.serviceURL+"/services/callback/getDistrictMasterList/"+URLEncoder.encode(DistrictName,"UTF-8"));
						//HttpsURLConnection yc = (HttpsURLConnection)oracle.openConnection();
						HttpURLConnection yc = (HttpURLConnection)oracle.openConnection();
						yc.setRequestProperty("User-Agent", "Mozilla/5.0");
						BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
					    String inputLine;
				        while ((inputLine = in.readLine()) != null) 
				        {
				           // System.out.println(inputLine);
				            sURLData=sURLData+inputLine;
				        }
				        in.close();
				        System.out.println(sURLData);  
				        jsonObject=jsonObject.fromObject(sURLData);
					    
				      
						
						JSONArray districtArray= (JSONArray) jsonObject.get("districtList");
						 for(int i=0; i<districtArray.size(); i++)
						 {
							 DistrictMaster districtMaster=new DistrictMaster();
							jsonObject=(JSONObject)districtArray.get(i);
							districtMaster.setDistrictId(jsonObject.getInt("districtId"));
							districtMaster.setDistrictName(jsonObject.getString("districtName"));
							//System.out.println(jsonObject.get("districtId"));
							//System.out.println(jsonObject.get("districtName"));
							districtMasterList.add(districtMaster);
							
							
						}
						


					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25,criterion1);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("------------------after api call----"+districtMasterList.size());
		return districtMasterList;
	}
	
	/* @Strat
	 * @Ashish
	 * @Description :: Get School List from DistrictSchools table 
	 * */
		public List<DistrictSchools> getSchoolMasterList(String schoolName, String districtId)
		{	
			//System.out.println(" inside DWRAutoComplete getSchoolMasterList :: -----> ");
	
			/* ========  For Session time Out Error =========*/
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", schoolName,MatchMode.START);
				
				// Get School List, this code using when districtId.equals("0") || districtId.equals("") in JobsBoard {Not Tested}
				if(districtId.equals("0") || districtId.equals("")){
					if(schoolName.trim()!=null && schoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
						Collections.sort(fieldOfSchoolList1,DistrictSchools.compSchoolMaster);
						Criterion criterion2 = Restrictions.ilike("schoolName","% "+schoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
						Collections.sort(fieldOfSchoolList2,DistrictSchools.compSchoolMaster);
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					}
					else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
						Collections.sort(fieldOfSchoolList1,DistrictSchools.compSchoolMaster);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
				// Get School List By District ID
				else{
					
					//System.out.println(" inside DWRAutoComplete getSchoolMasterList ELSE  -----::-----> ");
					
					DistrictMaster districtMaster = districtMasterDAO.findById(new Integer(districtId), false, false);
					Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
					
					if(schoolName.trim()!=null && schoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
						Criterion criterion3 = Restrictions.ilike("schoolName","% "+schoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
						
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						
					}else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
		}
		
		public List<DistrictSchools> getSchoolMasterList(String schoolName, String districtId,boolean flag)
		{	
			//System.out.println(" inside DWRAutoComplete getSchoolMasterList :: -----> ");
	
			/* ========  For Session time Out Error =========*/
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", schoolName,MatchMode.START);
				
				// Get School List, this code using when districtId.equals("0") || districtId.equals("") in JobsBoard {Not Tested}
				if(districtId.equals("0") || districtId.equals("")){
					if(schoolName.trim()!=null && schoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
						Collections.sort(fieldOfSchoolList1,DistrictSchools.compSchoolMaster);
						Criterion criterion2 = Restrictions.ilike("schoolName","% "+schoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
						Collections.sort(fieldOfSchoolList2,DistrictSchools.compSchoolMaster);
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					}
					else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
						Collections.sort(fieldOfSchoolList1,DistrictSchools.compSchoolMaster);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
				// Get School List By District ID
				else{
					
					//System.out.println(" inside DWRAutoComplete getSchoolMasterList ELSE  -----::-----> ");
					
					DistrictMaster districtMaster = districtMasterDAO.findById(new Integer(districtId), false, false);
					Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
					
					if(schoolName.trim()!=null && schoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
						Criterion criterion3 = Restrictions.ilike("schoolName","% "+schoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
						
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						
						//change NC to platform
						System.out.println("before api call----"+schoolMasterList.size());
						if(schoolMasterList.size()<10 && ElasticSearchConfig.serverName!=null && ElasticSearchConfig.serverName.equalsIgnoreCase("platform"))
						{
							/*ClientConfig config = new DefaultClientConfig();
							URI uri=null;
						    Client client = Client.create(config);
						    try {
						    	//System.out.println("http://192.168.0.18:8080/teachermatch/services/callback/getDistrictMasterList/"+URLEncoder.encode(DistrictName,"UTF-8"));
						    	uri=new URI(ElasticSearchConfig.serviceURL+"/services/callback/getSchoolMasterList/"+districtId+"/"+URLEncoder.encode(schoolName,"UTF-8"));
						    	System.out.println(ElasticSearchConfig.serviceURL+"/services/callback/getSchoolMasterList/"+districtId+"/"+URLEncoder.encode(schoolName,"UTF-8"));
						    	//uri=new URI("https://titan.teachermatch.org/services/callback/getSchoolMasterList/"+districtId+"/"+URLEncoder.encode(schoolName,"UTF-8"));
								//uri=new URI("http://192.168.0.18:8080/teachermatch/services/callback/getDistrictMasterList/"+DistrictName);
							} catch (URISyntaxException e) {
								
								e.printStackTrace();
							}*/
							try 
							{
							   /* WebResource service = client.resource(uri);
							    ClientResponse response = null;
							    response = service.type(MediaType.TEXT_HTML).get(ClientResponse.class);
							    String s=response.getEntity(String.class);
							    System.out.println(s);
							    JSONObject jsonObject=new JSONObject();
							    jsonObject=jsonObject.fromObject(s);*/
								
								String sURLData="";
								JSONObject jsonObject=new JSONObject();
								URL oracle = new URL(ElasticSearchConfig.serviceURL+"/services/callback/getSchoolMasterList/"+districtId+"/"+URLEncoder.encode(schoolName,"UTF-8"));
								HttpURLConnection yc = (HttpURLConnection)oracle.openConnection();
								yc.setRequestProperty("User-Agent", "Mozilla/5.0");
								BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
							    String inputLine;
						        while ((inputLine = in.readLine()) != null) 
						        {
						           // System.out.println(inputLine);
						            sURLData=sURLData+inputLine;
						        }
						        in.close();
						        //System.out.println(sURLData);  
						        jsonObject=jsonObject.fromObject(sURLData);
						  
								
								JSONArray districtArray= (JSONArray) jsonObject.get("schoolList");
								 for(int i=0; i<districtArray.size(); i++)
								 {
									DistrictSchools districtSchools=new DistrictSchools();
									SchoolMaster schoolMaster=new SchoolMaster();
									jsonObject=(JSONObject)districtArray.get(i);
									schoolMaster.setSchoolId(jsonObject.getLong("schoolId"));
									schoolMaster.setSchoolName(jsonObject.getString("schoolName"));
									districtSchools.setSchoolMaster(schoolMaster);
									schoolMasterList.add(districtSchools);
									
									
								}
								


							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						
					}else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
		}
	/* @End
	 * @Ashish
	 * @Description :: Get School List from DistrictSchools table 
	 * */
	
	
	
	//@Sekhar
	public List<CertificateTypeMaster> getFilterCertificateTypeList(String certType,String stateId)
	{
		List<CertificateTypeMaster> certList =  new ArrayList<CertificateTypeMaster>();
		List<CertificateTypeMaster> certList1 = null;
		List<CertificateTypeMaster> certList2 = null;
		try 
		{
			if(certType.contains("["))
			{
				StringTokenizer certType1 = new StringTokenizer(certType, "[");
				certType = (certType1.nextToken()).trim();
			}
				
			if(stateId.equals("0") || stateId.equals("")){
				Criterion criterionMode1 = Restrictions.like("certType", certType,MatchMode.START);
				Criterion criterianActive = Restrictions.eq("status","A");
				
				certList1 = certificateTypeMasterDAO.findWithLimit(Order.asc("certType"), 0, 10, criterionMode1,criterianActive);
				
				Criterion criterionLike1 = Restrictions.ilike("certType","% "+certType.trim()+"%" );
				certList2 = certificateTypeMasterDAO.findWithLimit(Order.asc("certType"), 0, 10, criterionLike1,criterianActive);
				
				certList.addAll(certList1);
				certList.addAll(certList2);
				Set<CertificateTypeMaster> setState = new LinkedHashSet<CertificateTypeMaster>(certList);
				certList = new ArrayList<CertificateTypeMaster>(new LinkedHashSet<CertificateTypeMaster>(setState));
			}else{
				StateMaster stateMaster = stateMasterDAO.findById(new Long(stateId), false, false);
				Criterion criterionState = Restrictions.eq("stateId",stateMaster);
				Criterion criterianActive = Restrictions.eq("status","A");
				
				Criterion criterionMode1 = Restrictions.like("certType", certType,MatchMode.START);
				certList1 = certificateTypeMasterDAO.findWithLimit(Order.asc("certType"), 0, 10, criterionMode1,criterionState,criterianActive);
				
				Criterion criterionLike1 = Restrictions.ilike("certType","% "+certType.trim()+"%" );
				certList2 = certificateTypeMasterDAO.findWithLimit(Order.asc("certType"), 0, 10, criterionLike1,criterionState,criterianActive);
				
				certList.addAll(certList1);
				certList.addAll(certList2);
				Set<CertificateTypeMaster> setState = new LinkedHashSet<CertificateTypeMaster>(certList);
				certList = new ArrayList<CertificateTypeMaster>(new LinkedHashSet<CertificateTypeMaster>(setState));
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return certList;
	}
	
	/* @Start
	 * @AShish
	 * @Description :: Find Selected Cities By StateId and All States
	 * */
	
		// Find cities by stateId
		public String selectedCityByStateId(Long stateId){
				List<CityMaster> lstCityMasters = null;
				StringBuffer apRecords =	new StringBuffer();
				
				try{
					StateMaster stateMaster = stateMasterDAO.findById(stateId, false, false);
					lstCityMasters = cityMasterDAO.findCityByState(stateMaster);
				
					if((int)(long)stateId ==0){
						apRecords.append("<select id='cityId' name='cityId' class='form-control' disabled='disabled'>");
						apRecords.append("<option value='0'>"+optAll+"</option>");
					}else{
						apRecords.append("<select id='cityId' name='cityId' class='form-control'>");
						apRecords.append("<option value='0'>"+optAll+"</option>");
						
						for(CityMaster ObjCitylist:lstCityMasters){
							apRecords.append("<option value='"+ObjCitylist.getCityName()+"'>"+WordUtils.capitalize(ObjCitylist.getCityName().toLowerCase())+"</option>");
						}
					}
					
					apRecords.append("</select>");
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
			return  apRecords.toString();
		}
		
		// Find All States
		public String displayStateData(){
			List<StateMaster> lststateMasters = null;
			StringBuffer apRecords =	new StringBuffer();
			
			try{
				
				lststateMasters = stateMasterDAO.findAll();			
					apRecords.append("<select id='stateId' name='stateId' class='form-control' onchange='selectCityByState()'>");
					apRecords.append("<option value='0'>"+optAll+"</option>");
					
					for(StateMaster ObjStateList:lststateMasters){
						if(ObjStateList.getStateName().length()>37){
							
							String stateName =   ObjStateList.getStateName().substring(0, 36);
							apRecords.append("<a id='iconpophover4' rel='tooltip'><option value='"+ObjStateList.getStateId()+"' title='"+ObjStateList.getStateName()+"'>"+stateName+"....</option></a>");
						
						}else{
							apRecords.append("<option value='"+ObjStateList.getStateId()+"'>"+ObjStateList.getStateName()+"</option>");
						}
					}
					apRecords.append("</select>");
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
		return  apRecords.toString();
	}
		
		public String displaySchoolList(String schoolIds,String noOfRow,String page,String sortOrder,String sortOrderType)
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			/*HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }*/
			
			StringBuffer dmRecords =	new StringBuffer();
		
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(page);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			List<Long> schoolListIds=new ArrayList<Long>();
			
			String[] sIds = schoolIds.split(",");
			if(sIds!=null && sIds.length>0)
			{
				for(String id:sIds)
				{
					schoolListIds.add(Long.parseLong(id));
				}
			}
			
			List<SchoolMaster> schoolMaster	  =	null;
			/** set default sorting fieldName **/
			String sortOrderFieldName="schoolName";
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterionDSchool= Restrictions.in("schoolId",schoolListIds);
				
			totalRecord = schoolMasterDAO.getRowCount(criterionDSchool);
			
			schoolMaster = schoolMasterDAO.findWithLimit(sortOrderStrVal,start,end,criterionDSchool);
			
			dmRecords.append("<table  class='' id='schoolListGrid' width='100%' border='0' >");
			dmRecords.append("<thead class='bg' style='color:#ffffff;text-align:left;'>");
			dmRecords.append("<tr>");
			String responseText="";
			
			//tmRecords.append("<th width='18%'>School Name</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblSchoolName,sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='45%' valign='top'>"+responseText+"</th>");
			
			//tmRecords.append("<th width='10%'>Final Decision Maker</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblAdd,sortOrderFieldName,"address",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'>"+responseText+"</th>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(schoolMaster.size()==0)
				dmRecords.append("<tr><td colspan='8' align='center'>"+msgNorecordfound+"</td></tr>" );
			System.out.println("No of Records "+schoolMaster.size());			
			
			if(schoolMaster.size()>0)
			{
				for(SchoolMaster lst:schoolMaster)
				{
					
					// Get School Address
					
					String schoolAddress="";
					String sAddress="";
					String sstate="";
					String scity = "";
					String szipcode="";
					
						if(lst.getAddress()!=null && !lst.getAddress().equals(""))
						{
							if(!lst.getCityName().equals(""))
							{
								schoolAddress = lst.getAddress()+", ";
							}
							else
							{
								schoolAddress = lst.getAddress();
							}
						}
						if(!lst.getCityName().equals(""))
						{
							if(lst.getStateMaster()!=null && !lst.getStateMaster().getStateName().equals(""))
							{
								scity = lst.getCityName()+", ";
							}else{
								scity = lst.getCityName();
							}
						}
						if(lst.getStateMaster()!=null && !lst.getStateMaster().getStateName().equals(""))
						{
							if(!lst.getZip().equals(""))
							{
								sstate = lst.getStateMaster().getStateName()+", ";
							}
							else
							{
								sstate = lst.getStateMaster().getStateName();
							}	
						}
						if(!lst.getZip().equals(""))
						{
								szipcode = lst.getZip().toString();
						}
						
						if(schoolAddress !="" || scity!="" ||sstate!="" || szipcode!=""){
							sAddress = schoolAddress+scity+sstate+szipcode;
						}else{
							sAddress="";
						}
					
					dmRecords.append("<tr>");
					dmRecords.append("<td>"+lst.getSchoolName()+"</td>");
					dmRecords.append("<td>"+sAddress+"</td>");
					dmRecords.append("</tr>");
				}
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationDoubleString(request,totalRecord,noOfRow,page));
			return dmRecords.toString();
		}  
		
		
		public String getSubjectByDistrict(Integer districtId)
		{
			System.out.println(" ============= inside getSubjectByDistrict ============ districtId :: ");
			
			List<SubjectMaster> lstSubjectMasters = new ArrayList<SubjectMaster>();
			StringBuffer apRecords =	new StringBuffer();
			
			try{
				
				if(districtId!=null && !districtId.equals(""))
				{
					DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
					lstSubjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(districtMaster);

					if(lstSubjectMasters.size()==0)
					{
						apRecords.append("<option value='0'>"+lblAllSubjects+"</option>>");
					}
					else if(lstSubjectMasters.size()>0)
					{
						apRecords.append("<option value='0'>"+optAll+"</option>>");
						for(SubjectMaster ObjSubjectMaster:lstSubjectMasters)
						{
							apRecords.append("<option value='"+ObjSubjectMaster.getSubjectId()+"'>"+ObjSubjectMaster.getSubjectName()+"</option>>");
						}
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return apRecords.toString();
		}
		
		
		public String getJobCategoryByDistrict(Integer districtId)
		{
			System.out.println(" ============= inside getJobCategoryByDistrict ============  ");
			
			List<JobCategoryMaster> lstJobCategoryMaster = new ArrayList<JobCategoryMaster>();
			StringBuffer apRecords =	new StringBuffer();
			
			try{
				
				if(districtId!=null && !districtId.equals(""))
				{
					DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
					lstJobCategoryMaster = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);

					System.out.println(" lstJobCategoryMaster "+lstJobCategoryMaster.size());
					
					if(lstJobCategoryMaster.size()==0)
					{
						apRecords.append("<option value='0'>"+optAll+"</option>>");
					}
					else if(lstJobCategoryMaster.size()>0)
					{
						apRecords.append("<option value='0'>"+optAll+"</option>>");
						for(JobCategoryMaster ObjJobCate:lstJobCategoryMaster)
						{
							apRecords.append("<option value='"+ObjJobCate.getJobCategoryId()+"'>"+ObjJobCate.getJobCategoryName()+"</option>>");
						}
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return apRecords.toString();
		}
		
		
		public String getSubjectByDistrictForAddJobOrder(Integer districtId)
		{
			System.out.println(" ============= inside getSubjectByDistrict ============ districtId :: "+districtId);
			
			List<SubjectMaster> lstSubjectMasters = new ArrayList<SubjectMaster>();
			StringBuffer apRecords =	new StringBuffer();
			
			try{
				
				if(districtId!=null && !districtId.equals(""))
				{
					DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
					lstSubjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(districtMaster);

					if(lstSubjectMasters.size()==0)
					{
						apRecords.append("<option value='0'>"+lblAllSubjects+"</option>>");
					}
					else if(lstSubjectMasters.size()>0)
					{
						apRecords.append("<option value='0'>"+optStlSub+"</option>>");
						for(SubjectMaster ObjSubjectMaster:lstSubjectMasters)
						{
							apRecords.append("<option value='"+ObjSubjectMaster.getSubjectId()+"'>"+ObjSubjectMaster.getSubjectName()+"</option>>");
						}
					}
				}
				else{
					apRecords.append("<option value='0'>"+optSltDistForReleventSubjectList+"</option>>");
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return apRecords.toString();
		}
		
	/* @End
	 * @AShish
	 * @Description :: Find Selected Cities By StateId and All States
	 * */
		
		public String getSubjectByDistrictForDSPQ(Integer teacherId,Integer districtId)
		{
			System.out.println(teacherId+" teacherId============= inside getSubjectByDistrictForDSPQ ============ districtId :: "+districtId);
			
			List<SubjectAreaExamMaster> lstSubjectMasters = new ArrayList<SubjectAreaExamMaster>();
			StringBuffer apRecords =	new StringBuffer();
			apRecords.append("<select class=\"form-control\" id=\"subjectIdforDSPQ\" name=\"subjectIdforDSPQ\">");
			try{
				
				if(districtId!=null && !districtId.equals("") && teacherId!=null && !teacherId.equals(""))
				{
					TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
					TeacherSubjectAreaExam teacherSubjectAreaExam = teacherSubjectAreaExamDAO.findTeacherSubjectAreaExam(teacherDetail);
					
					int subjectIdSelected=0;
					
					if(teacherSubjectAreaExam!=null){
						if(teacherSubjectAreaExam.getSubjectAreaExamMaster()!=null)
							subjectIdSelected=teacherSubjectAreaExam.getSubjectAreaExamMaster().getSubjectAreaExamId();
					}
					
					DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
					lstSubjectMasters = subjectAreaExamMasterDAO.findActiveSubjectExamByDistrict(districtMaster);

					if(lstSubjectMasters.size()==0)
					{
						apRecords.append("<option value='0'>"+lblAllSubjects+"</option>");
					}
					else if(lstSubjectMasters.size()>0)
					{
						apRecords.append("<option value='0'>"+optStlSub+"</option>");
						for(SubjectAreaExamMaster ObjSubjectMaster:lstSubjectMasters)
						{
							if(subjectIdSelected==ObjSubjectMaster.getSubjectAreaExamId()){
								apRecords.append("<option selected value=\""+ObjSubjectMaster.getSubjectAreaExamId()+"\">"+ObjSubjectMaster.getSubjectAreaExamName()+"</option>");
							}else{
								apRecords.append("<option value=\""+ObjSubjectMaster.getSubjectAreaExamId()+"\">"+ObjSubjectMaster.getSubjectAreaExamName()+"</option>");
							}
						}
					}
				}
				else{
					apRecords.append("<option value='0'>"+optSltDistForReleventSubjectList+"</option>");
				}
				apRecords.append("</select>");
			}catch(Exception e){
				e.printStackTrace();
			}
			//System.out.println("apRecords:::"+apRecords);
			return apRecords.toString();
		}
		
		
		public String displayUSAStateData(int iCountryId){
			List<StateMaster> lststateMasters = new ArrayList<StateMaster>();
			StringBuffer apRecords =	new StringBuffer();
			
			try{
				CountryMaster countryMaster=countryMasterDAO.findById(iCountryId, false, false);
				if(countryMaster!=null)
				{
					lststateMasters = stateMasterDAO.findActiveStateByCountryId(countryMaster);
					
					if(lststateMasters!=null && lststateMasters.size() > 0)
					{
						apRecords.append("<select id='stateId' name='stateId' class='form-control' onchange='selectCityByState()'>");
						apRecords.append("<option value='0'>"+optAll+"</option>");
						
						for(StateMaster ObjStateList:lststateMasters){
							if(ObjStateList.getStateName().length()>37){
								
								String stateName =   ObjStateList.getStateName().substring(0, 36);
								apRecords.append("<a id='iconpophover4' rel='tooltip'><option value='"+ObjStateList.getStateId()+"' title='"+ObjStateList.getStateName()+"'>"+stateName+"....</option></a>");
							
							}else{
								apRecords.append("<option value='"+ObjStateList.getStateId()+"'>"+ObjStateList.getStateName()+"</option>");
							}
						}
						apRecords.append("</select>");
					}
				}
				else
				{
					apRecords.append("<select id='stateId' name='stateId' class='span5' onchange='selectCityByState()'>");
					apRecords.append("<option value='0'>"+optAll+"</option>");
					apRecords.append("</select>");
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
		return  apRecords.toString();
	}
//sandeep start
		
		public List<DegreeTypeMaster> getDegreeTypeMasterList(String degreeName)
		{
			List<DegreeTypeMaster> degreeTypeMasterList = new ArrayList<DegreeTypeMaster>();
			
			List<DegreeTypeMaster> degreeTypeMasterList1 = null;
			List<DegreeTypeMaster> degreeTypeMasterList2 = null;
			List<DegreeTypeMaster> degreeTypeMasterList3 = null;
			
			Criterion criterion = null;
			Criterion criterion2 = null;
			Criterion criterion3 = null;
			
			try 
			{
				criterion = Restrictions.ilike("degreeName",degreeName.trim()+"%" );
				degreeTypeMasterList1 = degreeTypeMasterDAO.findWithLimit(Order.asc("degreeName"), 0,5, criterion);
				degreeTypeMasterList.addAll(degreeTypeMasterList1);
				
				if(degreeTypeMasterList.size()<15)
				{
					criterion2 = Restrictions.ilike("degreeName","% "+degreeName.trim()+"%" );			
					degreeTypeMasterList2 = degreeTypeMasterDAO.findWithLimit(Order.asc("degreeName"), 0,5, criterion2);
					degreeTypeMasterList.addAll(degreeTypeMasterList2);
				}
				
				if(degreeTypeMasterList.size()<15)
				{
					criterion3 = Restrictions.ilike("degreeName","%"+degreeName.trim()+"%" );
					degreeTypeMasterList3 = degreeTypeMasterDAO.findWithLimit(Order.asc("degreeName"), 0,5, criterion3);
					degreeTypeMasterList.addAll(degreeTypeMasterList3);
				}
				
				Set<DegreeTypeMaster> setDegreeTypeMaster= new LinkedHashSet<DegreeTypeMaster>(degreeTypeMasterList);
				degreeTypeMasterList = new ArrayList<DegreeTypeMaster>(new LinkedHashSet<DegreeTypeMaster>(setDegreeTypeMaster));
				
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			return degreeTypeMasterList;
		}
		
		
		
//sandeep end		
		
		
		public String getJobCategoryByHQAndBranch(Integer branchId,Integer headQuarterId)
		{
			System.out.println(" ============= inside getJobCategoryByHQAndBranch ============  ");
			
			List<JobCategoryMaster> lstJobCategoryMaster = new ArrayList<JobCategoryMaster>();
			StringBuffer apRecords =	new StringBuffer();
			System.out.println(" branchId ::"+branchId+" headQuarterId ::"+headQuarterId);
			try{
				if(headQuarterId!=null && !headQuarterId.equals("") && branchId!=null && !branchId.equals(""))
				{
					HeadQuarterMaster headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId,false,false);
					BranchMaster branchMaster = branchMasterDAO.findById(branchId,false,false);
					lstJobCategoryMaster = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(headQuarterMaster,branchMaster,null);
				}
				else if(headQuarterId!=null && !headQuarterId.equals("") && (branchId==null || branchId.equals("")))
				{
					HeadQuarterMaster headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId,false,false);	
					lstJobCategoryMaster = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(headQuarterMaster,null,null);
				}
				else if(branchId!=null && !branchId.equals("") && (headQuarterId==null || headQuarterId.equals("")))
				{
					BranchMaster branchMaster = branchMasterDAO.findById(branchId,false,false);
					lstJobCategoryMaster = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(null,branchMaster,null);
				}
				
				//if(headQuarterId!=null && !headQuarterId.equals(""))
				{

					System.out.println(" lstJobCategoryMaster "+lstJobCategoryMaster.size());
					
					if(lstJobCategoryMaster.size()==0)
					{
						apRecords.append("<option value='0'>All</option>>");
					}
					else if(lstJobCategoryMaster.size()>0)
					{
						apRecords.append("<option value='0'>All</option>>");
						for(JobCategoryMaster ObjJobCate:lstJobCategoryMaster)
						{
							apRecords.append("<option value='"+ObjJobCate.getJobCategoryId()+"'>"+ObjJobCate.getJobCategoryName()+"</option>>");
						}
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return apRecords.toString();
		}
		public String getJobSubCategoryByCategory(String jobCategoryIds)
		{
			StringBuffer sb	=	new StringBuffer();
			
			try
			{
				String[] jobcateIdsArr = jobCategoryIds.split(",");
				List<Integer> jcateIds = new ArrayList<Integer>();
				List<JobCategoryMaster> jobCategoryMasterlst = new ArrayList<JobCategoryMaster>();
				
				System.out.println(" jobcateIdsArr length :: "+jobcateIdsArr.length);
				
				for(int i=0; i<jobcateIdsArr.length;i++)
				{
					jcateIds.add(Integer.parseInt(jobcateIdsArr[i]));
				}
				
				if(jcateIds.size()>0)
					jobCategoryMasterlst = jobCategoryMasterDAO.findByCategoryID(jcateIds);
				System.out.println(",,,,,,,,,,,,,,,, jobcateList ,,,,,,,,,,,,,,,,:: "+jobCategoryMasterlst);
				System.out.println(" jcateIds :: "+jcateIds);
	
				//List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.getJobSubCategoryByJobCateId(jobcateList);
				System.out.println(";;;;;;;;;;jobCategoryMasterlst;;;;;;;;;;;;"+jobCategoryMasterlst.size());
				if(jobCategoryMasterlst.size()>0)
				{
					sb.append("<option value='0'>All</option>");
					if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
						for(JobCategoryMaster jb: jobCategoryMasterlst)
						{
							sb.append("<option value='"+jb.getJobCategoryId()+"'>"+jb.getJobCategoryName()+"</option>");
						}
					}
				}
				else
				{
					sb.append("<option value='0'>All</option>");
				}
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			return sb.toString();
		}
		public String getEeocDetail(String teacherId){
			System.out.println("========================DWR Ajax call for EEOC=========");
			String sGridtableDisplayCSS="padding:10px;";
			String sGridLabelCss="padding:0px;padding-left:12px;padding-top:10px;color:black;";
			StringBuffer eeocBuffer	=	new StringBuffer();
			try{
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(new Integer(teacherId), false, false);
			String race = getRace(teacherPersonalInfo);
			//sb.append("<tr><td class='net-header-text' style='"+sGridLabelCss+"' >");
			//sb.append(race);
			//sb.append("</td> </tr>");
			
			eeocBuffer.append("<tr class='profileContent'><td style='font-weight:bold;'>");
			eeocBuffer.append(race);
			eeocBuffer.append("</td> </tr>");
			String gender = getGender(teacherPersonalInfo);
			
		//	sb.append("<tr><td class='net-header-text' style='"+sGridLabelCss+"' >");
			//sb.append(gender);
		//sb.append("</td> </tr>");
			
			eeocBuffer.append("<tr class='profileContent'><td style='font-weight:bold;'>");
			eeocBuffer.append(gender);
			eeocBuffer.append("</td> </tr>");
			}catch(Exception e){
				System.out.println("EEOC "+e);
			}
			return eeocBuffer.toString();
			
		}
		public String getRace(TeacherPersonalInfo teacherPersonalInfo)
		{
			System.out.println("In getRace() method now.");
			List<RaceMaster> lstraceList = null;
			lstraceList = raceMasterDAO.findAllRaceByOrder();
			
			StringBuffer raceBuffer = new StringBuffer();
			//raceBuffer.append("<table style='width:85%;pointer-events: none;'> <tr> <td colspan='4'>");
			raceBuffer.append(lblRac);		
			//raceBuffer.append("</td> </tr>");
			
			int k=0;
			int l=0;
			List<String> raceList = new ArrayList<String>();
			if(teacherPersonalInfo!=null && teacherPersonalInfo.getRaceId()!=null)
				raceList = Arrays.asList(teacherPersonalInfo.getRaceId().split(","));
			
			for(RaceMaster raceMaster : lstraceList)
			{
				if(k==l && k!=0)
				{
					raceBuffer.append("</tr>");
				}
				
				if(k%4==0)
				{
					raceBuffer.append("<tr class='profileContent' style='font-weight: normal'>");
					l=k+4;
				}
				
				if(raceList.contains(""+raceMaster.getRaceId()))
					raceBuffer.append("<td><input type='checkbox' checked name='raceId' id='raceId' value='"+raceMaster.getRaceId()+"' disabled>");
				else
					raceBuffer.append("<td> <input type='checkbox' name='raceId' id='raceId' value='"+raceMaster.getRaceId()+"' disabled>");
				
				raceBuffer.append("<span class='eeoc-label'>"+raceMaster.getRaceName()+"</td>");
						
				k++;
			}
			
			k--;
	
			
			//raceBuffer.append("</table>");
			
			return raceBuffer.toString();
		}
		public String getGender(TeacherPersonalInfo teacherPersonalInfo){
			List<GenderMaster> lstraceList = null;
			lstraceList = genderMasterDao.findAllGenderByOrder();
			
			StringBuffer genderBuffer = new StringBuffer();
			//genderBuffer.append("<table style='width:85%;pointer-events: none;'> <tr> <td colspan='4'>");
			genderBuffer.append(lblGend);		
			//genderBuffer.append("</td> </tr>");
			int k=0;
			int l=0;
			
			System.out.println("Teacher Gender ID=="+teacherPersonalInfo.getGenderId().getGenderId());
			for(GenderMaster genderMaster : lstraceList)
			{
				if(k==l && k!=0)
				{
					genderBuffer.append("</tr>");
				}
				
				if(k%4==0)
				{
					genderBuffer.append("<tr class='profileContent' style='font-weight: normal'>");
					l=k+4;
				}
				if(genderMaster.getGenderId().equals(teacherPersonalInfo.getGenderId().getGenderId()))
					genderBuffer.append("<td> <input type='checkbox' checked name='raceId' id='raceId' value='"+genderMaster.getGenderId()+"' disabled>");
				else
					genderBuffer.append("<td> <input type='checkbox' name='raceId' id='raceId' value='"+genderMaster.getGenderId()+"' disabled>");
				
				genderBuffer.append("<span class='eeoc-label'>"+genderMaster.getGenderName()+"</span></td>");
						
				k++;
				
			}
		
			return genderBuffer.toString();
		}
		

}
