package tm.services.teacher;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import tm.bean.JobCertification;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.Utility;

public class MyDashboardAjax {
	
	
	
	 String locale = Utility.getValueOfPropByKey("locale");
	 String lblJoOfInterest=Utility.getLocaleValuePropByKey("lblJoOfInterest", locale);
	 String lblDistrictName=Utility.getLocaleValuePropByKey("lblDistrictName", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String headCom=Utility.getLocaleValuePropByKey("headCom", locale);
	 String lblApplyNow=Utility.getLocaleValuePropByKey("lblApplyNow", locale);
	 String lblSeeAll=Utility.getLocaleValuePropByKey("lblSeeAll", locale);
	 String lblPersonalPlng=Utility.getLocaleValuePropByKey("lblPersonalPlng", locale);
	 String lblNm=Utility.getLocaleValuePropByKey("lblNm", locale);
	 String lblCurrentSchoolLoca=Utility.getLocaleValuePropByKey("lblCurrentSchoolLoca", locale);
	 String lblManyLocations=Utility.getLocaleValuePropByKey("lblManyLocations", locale);
	 String lblhide=Utility.getLocaleValuePropByKey("lblhide", locale);
	 String btnShare=Utility.getLocaleValuePropByKey("btnShare", locale);
	 String lblYouhave=Utility.getLocaleValuePropByKey("lblYouhave", locale);
	 String lblNotAppliedand=Utility.getLocaleValuePropByKey("lblNotAppliedand", locale);
	 String optWithdrawn=Utility.getLocaleValuePropByKey("optWithdrawn", locale);
	 String lblManyPositions=Utility.getLocaleValuePropByKey("lblManyPositions", locale);
	 String lblPosition=Utility.getLocaleValuePropByKey("lblPosition", locale);
	 String lblCertifications=Utility.getLocaleValuePropByKey("lblCertifications", locale);
	 String lblManyCertifications=Utility.getLocaleValuePropByKey("lblManyCertifications", locale);
	 String lblTransferStatus=Utility.getLocaleValuePropByKey("lblTransferStatus", locale);
	 String lblPortfolio=Utility.getLocaleValuePropByKey("lblPortfolio", locale);
	 String btnUpdate=Utility.getLocaleValuePropByKey("btnUpdate", locale);
	 String lblCompleted=Utility.getLocaleValuePropByKey("lblCompleted", locale);
	 String optIncomlete=Utility.getLocaleValuePropByKey("optIncomlete", locale);
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	 
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(
			TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@InitBinder
	public void initBinder(HttpServletRequest request,DataBinder binder) 
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true,10));
	}
	
  public String showDashboardJbofIntresGridAvailable()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		//Integer districtId=5509600;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
		System.out.println("TTTTTTTTTTTTTTTTTTTTTTTT :: "+teacherDetail.getTeacherId());
		DistrictMaster districtMaster=null;
		
		districtMaster=(DistrictMaster) session.getAttribute("districtIdForImCandidate");
		
	  //  String teacherclassification=(String) session.getAttribute("classification");
		 //districtMaster= districtMasterDAO.findById(districtId, false, false);
		StringBuffer sb = new StringBuffer();
		try 
		{	
			List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
			List<CertificateTypeMaster> certObjList = new ArrayList<CertificateTypeMaster>();
			String crtIdList="";
			List<String> certList = new ArrayList<String>();
			
			List<TeacherCertificate> lstTeacherCertificates=teacherCertificateDAO.findCertificateAscByTeacher(teacherDetail);//teacherCertificateDAO.findCertificateByTeacher(teacherDetail);
			if(lstTeacherCertificates!=null){
				for(TeacherCertificate tc : lstTeacherCertificates){
					if(crtIdList.equalsIgnoreCase(""))
						crtIdList+=tc.getCertificateTypeMaster().getCertTypeId();
		             else
						crtIdList+=","+tc.getCertificateTypeMaster().getCertTypeId();
					certList.add(tc.getCertificateTypeMaster().getCertTypeId()+"");
					certList.add(crtIdList);
					certObjList.add(tc.getCertificateTypeMaster());
				}
			}
			List<JobOrder> jobOrderAllList =jobOrderDAO.findActiveJobOrderbyDistrict(districtMaster);
			List<JobOrder> listOfJobId = new ArrayList<JobOrder>();
			listOfJobId.addAll(jobOrderAllList);
			Map<JobOrder, String> jobCertMap = new HashMap<JobOrder, String>();
			if(listOfJobId!=null && listOfJobId.size()>0){
			List<JobCertification> findByJobIDS = jobCertificationDAO.findCertificationsByJobOrders(listOfJobId,certObjList);
				for (JobCertification jobCertification : findByJobIDS) {
					String certId = jobCertification.getCertificateTypeMaster().getCertTypeId()+"";
					jobOrderAllList.remove(jobCertification.getJobId());                                                                                      
					if(jobCertMap.containsKey(jobCertification.getJobId())){
						String existingCertId = jobCertMap.get(jobCertification.getJobId());
						jobCertMap.put(jobCertification.getJobId(), existingCertId+","+certId);
					}else{
						jobCertMap.put(jobCertification.getJobId(), certId);
					}
				}
			}
			List<JobOrder> addMatchJobs=new ArrayList<JobOrder>(); 
			for (String c1 : certList) {
				 for(java.util.Map.Entry<JobOrder, String> entry : jobCertMap.entrySet()){
					 /*if(entry.getKey().getClassification()!=null){
					//	 if(c1.equalsIgnoreCase(entry.getValue()) && entry.getKey().getClassification().equalsIgnoreCase(teacherclassification)){                                                                                                                                                                                                                                                                                         
								addMatchJobs.add(entry.getKey());
								jobOrderAllList.add(entry.getKey());
					//	 }
					 }else{
						 if(c1.equalsIgnoreCase(entry.getValue())){                                                                                                                                                                                                                                                                                         
								addMatchJobs.add(entry.getKey());
								jobOrderAllList.add(entry.getKey());
						 }
					 }*/
					 if(c1.equalsIgnoreCase(entry.getValue())){                                                                                                                                                                                                                                                                                         
							addMatchJobs.add(entry.getKey());
							jobOrderAllList.add(entry.getKey());
					 }
				  }
			 }
		                                                                
			lstJobOrder.addAll(jobOrderAllList);
			
			List<JobForTeacher> lstAllJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);
			List<JobForTeacher> lstwidrwJFT = new ArrayList<JobForTeacher>();
			for(JobForTeacher jft: lstAllJFT)
			{				
				JobOrder jobOrder = jft.getJobId();				
				if(lstJobOrder.contains(jobOrder))
				{
					lstJobOrder.remove(jobOrder);
				}
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("widrw"))
					lstwidrwJFT.add(jft);
			}


			sb.append("<table width='100%'  class='table  table-striped' style='border-top-color:#007ab4;border-top-radius:0px;'>");
			sb.append("<div class='bucketheader' style='height:28px;margin-left: -1.01px;'></div>");
			sb.append("<thead class='bg'>");
			sb.append("<tr style='background-color: #007ab4;'>");
			sb.append("<th width='40%' style='height:40px;'><img class='fl' src='images/jobs_of_interest_bucket.png' style='height:35px;width:35px;'> <div  class='subheading' style='color:white;font-size:13px;margin-left:45px;'>"+lblJoOfInterest+"</div></th>");
			sb.append("<th width='35%' style='vertical-align: middle;'>"+lblDistrictName+"</th>");
			sb.append("<th width='25%' style='vertical-align: middle;'>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");

			int rowCount=0;

			for(JobOrder jbOrder: lstJobOrder)
			{
				if(rowCount==4)
					break;
				rowCount++;
				sb.append("<tr>");
				//applymyjob.do
				if(jbOrder.getDistrictMaster().getJobApplicationCriteriaForProspects()==2)
				{
					sb.append("<td><a onclick=\"chkApplyJobNonClientDashBoard('"+jbOrder.getJobId()+"','"+jbOrder.getDistrictMaster().getJobApplicationCriteriaForProspects()+"','"+jbOrder.getExitURL()+"','"+jbOrder.getDistrictMaster().getDistrictId()+"','')\" href='javascript:void(0)' >"+jbOrder.getJobTitle()+"</a></td>");
				}
				else
				{
					sb.append("<td><a href='applyjob.do?jobId="+jbOrder.getJobId()+"'>"+jbOrder.getJobTitle()+"</a></td>");
				}
				sb.append("<td class='text-info' style='color:#007ab4;'>"+jbOrder.getDistrictMaster().getDistrictName()+"</td>");
				sb.append("<td>");
				if(jbOrder.getDistrictMaster().getJobApplicationCriteriaForProspects()==2)
				{
					sb.append("<a data-original-title='"+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' onclick=\"chkApplyJobNonClientDashBoard('"+jbOrder.getJobId()+"','"+jbOrder.getDistrictMaster().getJobApplicationCriteriaForProspects()+"','"+jbOrder.getExitURL()+"','"+jbOrder.getDistrictMaster().getDistrictId()+"','');\"   href='javascript:void(0)' ><img src='images/option01.png'  \">");					
				}
				else
				{
					//System.out.println(" Client District Job Id :: "+jbOrder.getJobId());
					sb.append("<a data-original-title='"+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jbOrder.getJobId()+"' ><img src='images/option01.png'style='width:21px; height:21px;'></a>");
				}
				sb.append("&nbsp;&nbsp;<a data-original-title='"+lblhide+"' rel='tooltip' id='tpHide"+rowCount+"' href='#' onclick=\"return jbHide('"+jbOrder.getJobId()+"')\"><img src='images/option03.png'style='width:21px; height:21px;'></a>");
				sb.append("&nbsp;&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+(rowCount+4)+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png'style='width:21px; height:21px;'></a>");

				sb.append("</td>");	   				
				sb.append("</tr>");
			}


			sb.append("<tr>");
			sb.append("<td colspan='3' >"+lblYouhave+" "
					+(lstJobOrder.size()==0?"0":"<a href='jobsofinterest.do?searchby=avlbl'>"+lstJobOrder.size()+"</a>")
					+" +lblNotAppliedand+ "+(lstwidrwJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=widrw'>"+lstwidrwJFT.size()+"</a>")+" +optWithdrawn+");
			sb.append("<div class='right'><a href='jobsofinterest.do'><strong>"+lblSeeAll+"</strong></a></div>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return sb.toString();
  }
	
  
  /* === ============== getMilestonesGrid  ==================*/
	@Transactional(readOnly=false)
	public String getMilestonesGrid()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String teacherFullName="";
		int completecounter=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb =new StringBuffer();
		TeacherExperience teacherExperience=new TeacherExperience();
		List<UserMaster> listUserMaster =new ArrayList<UserMaster>();
		List<RoleMaster> lisRoleMasters =new  ArrayList<RoleMaster>();
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");	
		teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
		  if(teacherExperience!=null){
			  if(teacherExperience.getResume()!=null && teacherExperience.getResume()!=""){
				  completecounter++;
			  }
			  if(teacherExperience.getIsDocumentOnFile()!=null){
				  completecounter++;
			  }
		  }
		
        String firstName=teacherDetail.getFirstName()==null?"":teacherDetail.getFirstName();
        String lastName=teacherDetail.getLastName()==null?"":teacherDetail.getLastName();
        teacherFullName=firstName +" "+lastName;
       
        //Current School Location
        StringBuffer locData= new StringBuffer();
        String currentLocation="";
        int locationCounter=1;
        String currentLocationSingle="";
        boolean locationflag=false;
        listUserMaster=userMasterDAO.findByEmail(teacherDetail.getEmailAddress().trim());
        System.out.println("listUserMaster :: "+listUserMaster.size());
        if(listUserMaster!=null && listUserMaster.size()>0){
          locData.append("<div style='width:200px;'>");
        	for (UserMaster user : listUserMaster) {
        		currentLocation="";
        		if(locationflag==false){
        			locationflag=true;
        			if(user.getSchoolId()!=null){
        				currentLocation=user.getSchoolId().getSchoolName();
        				currentLocationSingle=user.getSchoolId().getSchoolName();
        			}else{
        				currentLocation=user.getDistrictId().getDistrictName();
        				currentLocationSingle=user.getDistrictId().getDistrictName();
        			}
    			}else{
    				locationCounter++;
    				if(user.getSchoolId()!=null){
        				currentLocation=user.getSchoolId().getSchoolName();
        			}else{
        				currentLocation=user.getDistrictId().getDistrictName();
        			}
    			}
			locData.append(currentLocation+"</br>");
        		
			}
         locData.append("</div>");	
        }
   //Position 
        lisRoleMasters = userMasterDAO.findByRoleMaterByEmail(teacherDetail.getEmailAddress().trim());
        System.out.println("lisRoleMasters.size() :: "+lisRoleMasters.size());
        StringBuffer locPosData= new StringBuffer();
        int posCounter=1;
        String currentLocationPos="";
        String currentLocationSinglePos="";
        boolean locationPosflag=false;
        if(lisRoleMasters!=null && lisRoleMasters.size()>0){
        	locPosData.append("<div>");
        	for (RoleMaster role : lisRoleMasters) {
        		currentLocationPos="";
        		if(locationPosflag==false){
        			locationPosflag=true;
        				currentLocationPos=role.getRoleName();
        				currentLocationSinglePos=role.getRoleName();
    			}else{
    				    posCounter++;
        				currentLocationPos=role.getRoleName();
    			}
        	 locPosData.append(currentLocationPos+"</br>");
			}
        	locPosData.append("</div>");	
        }
        
        //for Certification
       
        StringBuffer certData= new StringBuffer();
        int cesCounter=1;
        String certificate="";
        String certificateSingle="";
        boolean certificateflag=false;
        
    	List<TeacherCertificate> lstTeacherCertificates=teacherCertificateDAO.findCertificateByTeacher(teacherDetail);
    	System.out.println("lstTeacherCertificates.size()  :: "+lstTeacherCertificates.size());
    	if(lstTeacherCertificates!=null && lstTeacherCertificates.size()>0){
    		completecounter++;
    		certData.append("<div>");
    		for(TeacherCertificate tc :lstTeacherCertificates){
    			certificate="";
    			if(certificateflag==false){
    				certificateflag=true;
    				certificate=tc.getCertificateTypeMaster().getCertType();
    				certificateSingle=tc.getCertificateTypeMaster().getCertType();
    			}else{
    				cesCounter++;
    				certificate=tc.getCertificateTypeMaster().getCertType();
    			}
    		 certData.append(certificate+"</br>");
    		}
    		certData.append("</div>");
    	}
    	System.out.println(certData.toString());
		try 
		{
			//PrintWriter pw = response.getWriter();			
			sb.append("<table width='100%' border='0' class='table table-striped' style='border-top-color:#007ab4;border-top-radius:0px;'>");
			sb.append("<div class='bucketheader' style='height:28px;margin-left: -1.01px;'></div>");
			sb.append("<thead class='bg'>");
			sb.append("<tr style='background-color: #007ab4;'>");
				sb.append("<th ><img class='fl' src='images/personal_planning_bucket.png' style='height:35px;width:35px;'> <div  class='subheading' style='color:white;font-size:13px;margin-left:45px;'>"+lblPersonalPlng+"</div></th>");
				sb.append("<th width='50%' style='vertical-align: middle;'></th>");
			sb.append("</tr>");
			sb.append("</thead>");

			//--Name row Start 
			
			    sb.append("<tr>");
				sb.append("<td>"+lblNm+"</td>");
				sb.append("<td>");
				sb.append("<strong class='text-success'>"+teacherFullName+"</strong>");
				sb.append("</td>");
			    sb.append("</tr>");
			
			//Current School Location 
			sb.append("<tr>");
				sb.append("<td>"+lblCurrentSchoolLoca+"</td>");
				if(currentLocationSingle=="" && currentLocationSingle.length()==0){
					sb.append("<td>");
					sb.append("</td>");
				}else{
					sb.append("<td>");
					   sb.append("<strong class='text-success'>");
					   if(locationCounter>1){
						   sb.append("<a data-html='true' id='iconpophoverBaseM1' href='javascript:void(0)' rel='tooltip' data-original-title=\""+locData.toString()+"\" data-placement='top'>"+lblManyLocations+"</a>");
					   }else{
						   sb.append(currentLocationSingle);
					   }
					sb.append("</strong></td>");
				}
		    sb.append("</tr>");
		    
		  //Position 
			sb.append("<tr>");
				sb.append("<td>"+lblPosition+"</td>");
				if(currentLocationSinglePos=="" && currentLocationSinglePos.length()==0){
					sb.append("<td>");
					sb.append("</td>");
				}else{
					sb.append("<td>");
					   sb.append("<strong class='text-success'>");
					   if(posCounter>1){
						   sb.append("<a data-html='true' id='iconpophoverBaseM2' href='javascript:void(0)' rel='tooltip' data-original-title=\""+locPosData.toString()+"\" data-placement='top'>"+lblManyPositions+"</a>");
					   }else{
						   sb.append(currentLocationSinglePos);
					   }
					sb.append("</strong></td>");
				}
		    sb.append("</tr>");
		    
		    //Certifications 
			sb.append("<tr>");
				sb.append("<td>"+lblCertifications+"</td>");
				if(certificateSingle=="" && certificateSingle.length()==0){
					sb.append("<td>");
					sb.append("</td>");
				}else{
					sb.append("<td>");
					   sb.append("<strong class='text-success'>");
					   if(cesCounter>1){
						   sb.append("<a data-html='true' id='iconpophoverBaseM3' href='javascript:void(0)' rel='tooltip' data-original-title=\""+certData.toString()+"\" data-placement='top'>"+lblManyCertifications+"</a>");
					   }else{
						   sb.append(certificateSingle);
					   }
					sb.append("</strong></td>");
				}
		    sb.append("</tr>");
		    
		    
		    //Transfer Status 
			sb.append("<tr>");
				sb.append("<td>"+lblTransferStatus+"</td>");
				sb.append("<td>");
				sb.append("<strong class='text-success'></strong>");
				sb.append("</td>");
		    sb.append("</tr>");
		    
		    //Portfolio (click to answer)
		    String portfolioTooltip="";
			sb.append("<tr>");
				sb.append("<td>"+lblPortfolio+"</td>");
				sb.append("<td>");
				if(completecounter==3)
				{
					portfolioTooltip=btnUpdate;
					sb.append("<strong class='text-success'>"+lblCompleted+"</strong>");
				}
				else
				{
					portfolioTooltip="Complete Now";
					sb.append("<strong class='text-error'>"+optIncomlete+"</strong>");
				}
					
				sb.append("&nbsp;&nbsp;<a data-original-title='"+portfolioTooltip+"' rel='tooltip' id='iconpophover2'href='myportfolio.do'>");
				sb.append("<img src='images/option08.png' style='width:21px; height:21px;'>");
				sb.append("</a>");	
					
				sb.append("</td>");
		    sb.append("</tr>");
		
			sb.append("</table>");
			 sb.append("<script> $('#iconpophoverBaseM1').tooltip();$('#iconpophoverBaseM2').tooltip();$('#iconpophoverBaseM3').tooltip();");		
			sb.append("</script>");

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
  
	public String addOrEditResume(String resume)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			System.out.println(" >>>>>>>>>>> teacherDetail.getTeacherId() :: "+teacherDetail.getTeacherId());
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);

			if(teacherExperience==null)
			{
				teacherExperience = new TeacherExperience();
				teacherExperience.setTeacherId(teacherDetail);
				teacherExperience.setCanServeAsSubTeacher(2);
				teacherExperience.setPotentialQuality(0);
				teacherExperience.setCreatedDateTime(new Date());
			}
			teacherExperience.setResume(resume);
			teacherExperienceDAO.makePersistent(teacherExperience);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public String saveDocumentonStatus(int  isdocumentonfile)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		boolean documentonflag=false;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);

			if(teacherExperience==null)
			{
				teacherExperience = new TeacherExperience();
				teacherExperience.setTeacherId(teacherDetail);
				teacherExperience.setCanServeAsSubTeacher(2);
				teacherExperience.setPotentialQuality(0);
				teacherExperience.setCreatedDateTime(new Date());
				
			}
			if(isdocumentonfile==1){
				documentonflag=true;
			}
			teacherExperience.setIsDocumentOnFile(documentonflag);
			teacherExperienceDAO.makePersistent(teacherExperience);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return documentonflag+"";
	}
	
	/*================  Report Grid New ==================*/
	public String getReportGrid()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb =new StringBuffer();
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

			sb.append("<table width='100%' border='0' class='table table-striped' style='border-top-color:#007ab4;border-radius:0px;'>");
			sb.append("<div class='bucketheader' style='height:28px;margin-left: -1.01px;'></div>");
			sb.append("<thead class='bg'>");
			  sb.append("<tr style='background-color: #007ab4;'>");		
				sb.append("<th width='50%;'><img class='fl' src='images/communications_bucket.png' style='height:35px;width:35px;'> <div  class='subheading' style='color:white;font-size:13px;margin-left:45px;'>"+headCom+"</div></th>");		
				sb.append("<th width='25%;' style='vertical-align: middle;' class='subheading'>"+lblStatus+"</th>");
				sb.append("<th width='25%;' style='vertical-align: middle;' class='subheading'>"+lblAct+"</th>");
			  sb.append("</tr>");
			sb.append("</thead>");
			
			sb.append("<tr>");
				sb.append("<td>&nbsp;</td>");
				sb.append("<td>&nbsp;</td>");
				sb.append("<td>&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
				sb.append("<td>&nbsp;</td>");
				sb.append("<td>&nbsp;</td>");
				sb.append("<td>&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
				sb.append("<td>&nbsp;</td>");
				sb.append("<td>&nbsp;</td>");
				sb.append("<td>&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
				sb.append("<td>&nbsp;</td>");
				sb.append("<td>&nbsp;</td>");
				sb.append("<td>&nbsp;</td>");
			sb.append("</tr>");
			sb.append("</table>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
}
