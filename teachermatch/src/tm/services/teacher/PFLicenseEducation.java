package tm.services.teacher;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.map.HashedMap;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.jfree.data.time.Month;
import org.springframework.beans.factory.annotation.Autowired;


import tm.bean.TeacherAcademics;
import tm.bean.TeacherDetail;
import tm.bean.TeacherHonor;
import tm.bean.TeacherPersonalInfo;
import tm.bean.master.FieldMaster;
import tm.bean.master.FieldOfStudyMaster;
import tm.bean.master.UniversityMaster;
import tm.bean.textfile.DegreeTypeMaster;
import tm.bean.textfile.LicensureEducation;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.master.FieldMasterDAO;
import tm.dao.master.FieldOfStudyMasterDAO;
import tm.dao.master.UniversityMasterDAO;
import tm.dao.textfile.DegreeTypeMasterDAO;
import tm.dao.textfile.LicensureEducationDAO;
import tm.utility.Utility;

public class PFLicenseEducation {

	
	@Autowired
	private LicensureEducationDAO licensureEducationDAO;
	
	@Autowired
	private DegreeTypeMasterDAO degreeTypeMasterDAO;
	
	@Autowired
	private UniversityMasterDAO universityMasterDAO;
	
	@Autowired
	private FieldOfStudyMasterDAO fieldOfStudyMasterDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	
	
	public void setUniversityMasterDAO(UniversityMasterDAO universityMasterDAO) {
		this.universityMasterDAO = universityMasterDAO;
	}

	public void setDegreeTypeMasterDAO(DegreeTypeMasterDAO degreeTypeMasterDAO) {
		this.degreeTypeMasterDAO = degreeTypeMasterDAO;
	}

	public void setLicensureEducationDAO(LicensureEducationDAO licensureEducationDAO) {
		this.licensureEducationDAO = licensureEducationDAO;
	}
	
	public void setFieldOfStudyMasterDAO(FieldOfStudyMasterDAO fieldOfStudyMasterDAO) {
		this.fieldOfStudyMasterDAO = fieldOfStudyMasterDAO;
	}

	public String getLicenseEducation(String ssn)
	{
		
		System.out.print("Get Load Education By SSN#########"+ssn);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		TeacherDetail teacherDetail = null;			
		TeacherPersonalInfo teacherPersonalInfo=null;
		if (session == null || session.getAttribute("teacherDetail") == null) {
			return "false";
		}else{
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		}

		if(teacherDetail!=null)
		{
			teacherPersonalInfo=teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		}
		
		if(teacherPersonalInfo!=null)
		{			
			try {				
				ssn=Utility.decodeBase64(teacherPersonalInfo.getSSN());
			} catch (IOException e) {				
				e.printStackTrace();
			}
		}

		StringBuffer sb = new StringBuffer();	
		
		
		List<LicensureEducation> licensureEducationList = new ArrayList<LicensureEducation>();
		
		try {
			
			Criterion criteria = Restrictions.eq("SSN", Utility.encodeInBase64( ssn));
			licensureEducationList = licensureEducationDAO.findByCriteria(criteria);
			
			

			sb.append("<table border='0' id='educationGrid' class='table table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");
			
			sb.append("<th width='25%' valign='top'>School</th>");
			sb.append("<th width='25%' valign='top'>Graduation Date</th>");
			sb.append("<th width='25%' valign='top'>Degree</th>");
			sb.append("<th width='25%' valign='top'>Major</th>");
			/*sb.append("<th width='7%' valign='top'></th>");*/

			
			sb.append("</tr>");
			sb.append("</thead>");

			if(licensureEducationList!=null)
				for(LicensureEducation licensureEducation:licensureEducationList)
				{
					
					List<DegreeTypeMaster> degreeTypeMasters = new ArrayList<DegreeTypeMaster>();
					List<UniversityMaster> universityMaster = new ArrayList<UniversityMaster>();
					List<FieldOfStudyMaster> fieldOfStudyMaster = new ArrayList<FieldOfStudyMaster>();
					sb.append("<tr>");
					sb.append("<td>"); 
					Criterion iheCriteria = Restrictions.eq("iheCode", licensureEducation.getIHECode());
					universityMaster = universityMasterDAO.findByCriteria(iheCriteria);
					if(universityMaster.size() != 0 && universityMaster != null)
					sb.append(universityMaster.get(0).getUniversityName());
					
				    sb.append("</td>");
				
					sb.append("<td>");
					String date= getIndiaFormatDateMonthFirst(licensureEducation.getGraduationDate());
					sb.append(date);
				    sb.append("</td>");					
					
				    sb.append("<td>"); 
				    Criterion degreeCriteria = Restrictions.eq("degreeTypeCode", licensureEducation.getDegreeType());
				    
				    System.out.println(" licensureEducation.getDegreeType() >>>> "+licensureEducation.getDegreeType());
				    
					degreeTypeMasters = degreeTypeMasterDAO.findByCriteria(degreeCriteria);
					if(degreeTypeMasters.size() != 0 && degreeTypeMasters != null)
					sb.append(degreeTypeMasters.get(0).getDegreeName());
				    
				    sb.append("</td>");				
		          
				    sb.append("<td>");
				    Criterion majorCriteria = Restrictions.eq("fieldCode", licensureEducation.getMajor());
				    fieldOfStudyMaster = fieldOfStudyMasterDAO.findByCriteria(majorCriteria);
					if(fieldOfStudyMaster.size() != 0 && fieldOfStudyMaster != null)
					sb.append(fieldOfStudyMaster.get(0).getFieldName());
				    //sb.append(licensureEducation.getMajor());
				    sb.append("</td>");
					/*if(licensureEducation.getEducationFlag() != null)
					    if(licensureEducation.getEducationFlag()){
							sb.append("<td>");
							System.out.print("gridid"+licensureEducation.getLicensureeducationId());
								sb.append("<a href='#' onclick=\"return showEditEducationLicense('"+licensureEducation.getLicensureeducationId()+"')\" >Edit</a>");
								//sb.append(" &nbsp;|&nbsp;<a href='#'  onclick=\"return deleteRecordHonors('"+licensureEducation.getLicensureeducationId()+"')\" >Delete</a>");
							sb.append("</td>");
					    }	*/
					sb.append("</tr>");
				}

			if(licensureEducationList==null || licensureEducationList.size()==0)
			{
				/*sb.append("<div class='row'>");
				sb.append("<div class='span4'>No Record Found.</div>");
				sb.append("</div>");*/
				sb.append("<tr>");
				sb.append("<td>No record found.");
				sb.append("<input type='hidden' id='ncEducationCheck' name='ncEducationCheck' value='1'>");
				sb.append("</td>");
				sb.append("</tr>");
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}		
	
	
	
	public String saveEducation(String Id , String ssn , String degreeTypeCode , String universityId , String major ,String graduationDate)
	{
		//System.out.print("licenseid-------"+Integer.parseInt(Id) );
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try 
		{
			    LicensureEducation  licensureEducation  =  new LicensureEducation();
			    
			    List<UniversityMaster> universityMaster = new ArrayList<UniversityMaster>();
			    UniversityMaster universityMaster2 = null;
			    SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
			    
			 //   Criterion majorCriteria = Restrictions.eq("fieldCode", licensureEducation.getMajor());
			    FieldOfStudyMaster fieldOfStudyMaster = fieldOfStudyMasterDAO.findById(Long.parseLong(major), false, false);
				
			    
			    
			    universityMaster2 = universityMasterDAO.findById(Integer.parseInt(universityId), false, false);
			    if(universityMaster2 == null)
			     return null;
			    else if(universityMaster2.getIheCode() == null)
			    	return null;
			    universityMaster.add(universityMaster2);
			    
			    
			    Integer licensureeducationId = 0;
			    if(!Id.isEmpty()){
			    	
			       licensureeducationId = Integer.parseInt(Id.trim());
			    }
			     licensureEducation = licensureEducationDAO.findById(licensureeducationId, false, false);
			
			    if(licensureEducation == null){
			    	
			    	licensureEducation = new LicensureEducation();
			    	
					licensureEducation.setSSN(Utility.encodeInBase64(ssn));
					
					if(degreeTypeCode != null)
					licensureEducation.setDegreeType(degreeTypeCode);
					
					licensureEducation.setGraduationDate(formatter.parse(graduationDate));
					if(fieldOfStudyMaster != null)
					licensureEducation.setMajor(fieldOfStudyMaster.getFieldCode());
					
					if(universityMaster.size() != 0 && universityMaster != null)
					licensureEducation.setIHECode(universityMaster.get(0).getIheCode());
					
					licensureEducation.setStatus("A");
					licensureEducation.setCreatedDate(new Date());
					licensureEducation.setCreatedBy(1);
					licensureEducation.setEducationFlag(true);
					licensureEducationDAO.makePersistent(licensureEducation);
			    }else{
			    	
			    	licensureEducation.setSSN(Utility.encodeInBase64(ssn));
			    	
			    	if(degreeTypeCode != null)
					licensureEducation.setDegreeType(degreeTypeCode);
					
			    	licensureEducation.setGraduationDate(formatter.parse(graduationDate));
			    	if(fieldOfStudyMaster != null)
					licensureEducation.setMajor(fieldOfStudyMaster.getFieldCode());
					
					if(universityMaster.size() != 0 && universityMaster != null)
					licensureEducation.setIHECode(universityMaster.get(0).getIheCode());
					
					licensureEducation.setStatus("A");
					licensureEducation.setCreatedDate(new Date());
					licensureEducation.setCreatedBy(1);
					licensureEducation.setEducationFlag(true);
					licensureEducationDAO.makePersistent(licensureEducation);
			    }
			

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;

	}
		
		
	
	public HashMap<String,String> showEditEducation(String id)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}

		LicensureEducation licensureEducation = null;
		try 
		{
			Integer licensureeducationId = Integer.parseInt(id.trim());
			licensureEducation = licensureEducationDAO.findById(licensureeducationId, false, false);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		//System.out.print(licensureEducation.getSSN()); 	
		HashMap<String,String> hm=new HashMap<String,String>();  
		List<DegreeTypeMaster> degreeTypeMasters = new ArrayList<DegreeTypeMaster>();
		List<UniversityMaster> universityMaster = new ArrayList<UniversityMaster>();
		List<FieldOfStudyMaster> fieldOfStudyMaster = new ArrayList<FieldOfStudyMaster>();
		Criterion degreeCriteria = Restrictions.eq("degreeTypeId", Integer.parseInt(licensureEducation.getDegreeType()));
		degreeTypeMasters = degreeTypeMasterDAO.findByCriteria(degreeCriteria);
		
		if(degreeTypeMasters.size() != 0 && degreeTypeMasters != null)
		hm.put("degreeName", degreeTypeMasters.get(0).getDegreeName());
		
		hm.put("degreeId", degreeTypeMasters.get(0).getDegreeTypeId()+"");
		
		Criterion iheCriteria = Restrictions.eq("iheCode", licensureEducation.getIHECode());
		universityMaster = universityMasterDAO.findByCriteria(iheCriteria);
		
		if(universityMaster.size() != 0 && universityMaster != null)
		hm.put("universityName", universityMaster.get(0).getUniversityName());
		hm.put("universityId", universityMaster.get(0).getUniversityId().toString());
		
		
		Criterion majorCriteria = Restrictions.eq("fieldCode", licensureEducation.getMajor());
	    fieldOfStudyMaster = fieldOfStudyMasterDAO.findByCriteria(majorCriteria);
		if(fieldOfStudyMaster.size() != 0 && fieldOfStudyMaster != null)
		hm.put("fieldId", fieldOfStudyMaster.get(0).getFieldId().toString());
		hm.put("fieldName", fieldOfStudyMaster.get(0).getFieldName());
		
		hm.put("GraduationDate", Utility.convertDateAndTimeToDatabaseformatOnlyDate(licensureEducation.getGraduationDate()));
		return hm;

	}
	
	
	public static String getIndiaFormatDateMonthFirst(Date dat)
	{
		try 
		{
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			return sdf.format(dat);
		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
			return "";
		}
	}
	
	
	
	
}
