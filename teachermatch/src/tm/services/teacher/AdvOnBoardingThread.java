package tm.services.teacher;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;

public class AdvOnBoardingThread extends Thread{
	@Autowired
	private OnlineActivityAjax onlineActivityAjax;
	
	private HttpServletRequest request;
	private JobOrder jobId;
	private TeacherDetail teacherId;
	private Integer districtId;
	private UserMaster userMaster;
	
	public void setJobId(JobOrder jobId) {
		this.jobId = jobId;
	}

	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}

	public OnlineActivityAjax getOnlineActivityAjax() {
		return onlineActivityAjax;
	}

	public void setOnlineActivityAjax(OnlineActivityAjax onlineActivityAjax) {
		this.onlineActivityAjax = onlineActivityAjax;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
		
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public AdvOnBoardingThread() {
		super();
	}
	
	public void run()
	{
	 try{
			onlineActivityAjax.sendAdvOnBoarding(teacherId,jobId,districtId,request,userMaster);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
