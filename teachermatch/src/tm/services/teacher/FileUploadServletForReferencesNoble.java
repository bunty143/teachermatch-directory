package tm.services.teacher;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import tm.bean.TeacherDetail;
import tm.utility.Utility;

public class FileUploadServletForReferencesNoble extends HttpServlet {

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		try{
			PrintWriter pw = response.getWriter();
			response.setContentType("text/html");  

			request.setCharacterEncoding("UTF-8");
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			List uploadedItems = null;
			FileItem fileItem = null;
			String ext="",filePath="";
			HttpSession session=request.getSession();
			Integer teacherId=0;
			TeacherDetail teacherDetail=null;
			
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			
			if(session.getAttribute("teacherDetail")!=null){
			 teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			}else{
				teacherDetail = new TeacherDetail();
			    Iterator JK = uploadedItems.iterator();
				 while (JK.hasNext())	
				 {
				  fileItem = (FileItem) JK.next();
					if(fileItem.getFieldName().equals("teacherId"))
					{
					 teacherId=Integer.parseInt(fileItem.getString());
         			 teacherDetail.setTeacherId(teacherId);
         			 break;
					}
			    }
			 }
			
			String fileName="";
			String refDateTime=Utility.getDateTime();
			if(request.getSession().getAttribute("previousReferenceFile")!=null){
				String previousFileName=request.getSession().getAttribute("previousReferenceFile").toString();
				System.out.println("from certificate servlet >> "+previousFileName);
				if(previousFileName!=null && !previousFileName.equals("")){
					String pFileName=Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+previousFileName;
					File pFile=new File(pFileName);
					if(pFile.exists()){
						pFile.delete();
					}
					session.removeAttribute("previousReferenceFile");
				}
			}
			String sbtsource_ref="";
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if(fileItem.getName()!=null && !fileItem.getName().equals("")){
				break;
				}
			}
			
			if(teacherDetail!=null ){
				filePath=Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/";

				File f=new File(filePath);
				if(!f.exists())
					f.mkdirs();

				if (fileItem.isFormField() == false){
					if (fileItem.getSize() > 0){
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						fileName="Reference_"+refDateTime+ext;
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
					}
				}
				fileItem=null;				
			}
			response.setContentType("text/html");  
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.saveNobleReference('"+fileName+"');");
			pw.print("</script>");
			} 
			catch (FileUploadException e) 
			{
				e.printStackTrace();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
	}

}
