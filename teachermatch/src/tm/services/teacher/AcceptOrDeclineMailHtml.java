package tm.services.teacher;

import java.text.SimpleDateFormat;
import tm.bean.hqbranchesmaster.BranchMaster;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.DistrictTemplatesforMessages;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictTemplatesforMessagesDAO;
import tm.services.district.GlobalServices;
import tm.utility.Utility;
import static tm.services.district.GlobalServices.*;
/*
*@Author: Ram Nath
*/
public class AcceptOrDeclineMailHtml{
	
	 static String locale = Utility.getValueOfPropByKey("locale");
	 static String lblhasaccepted=Utility.getLocaleValuePropByKey("lblhasaccepted", locale);
	 static String lblhasdeclined=Utility.getLocaleValuePropByKey("lblhasdeclined", locale);
	 static String lblhasdeclinedthe=Utility.getLocaleValuePropByKey("lblhasdeclinedthe",locale);
	 static String  lblSchoolName=Utility.getLocaleValuePropByKey("lblSchoolName", locale);
	 static String lblApplicantName=Utility.getLocaleValuePropByKey("lblApplicantName", locale);
	 static String lblPosition=Utility.getLocaleValuePropByKey("lblPosition", locale);
	 static String lblDear=Utility.getLocaleValuePropByKey("lblDear", locale);
	 
	 static String mailAcceotOrDeclineMailHtmal1=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal1", locale);
	 static String mailAcceotOrDeclineMailHtmal2=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal2", locale);
	 static String mailAcceotOrDeclineMailHtmal3=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal3", locale);
	 static String mailAcceotOrDeclineMailHtmal4=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal4", locale);
	 static String mailAcceotOrDeclineMailHtmal5=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal5", locale);
	 static String mailAcceotOrDeclineMailHtmal6=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal6", locale);
	 static String mailAcceotOrDeclineMailHtmal7=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal7", locale);
	 static String mailAcceotOrDeclineMailHtmal8=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal8", locale);
	 static String mailAcceotOrDeclineMailHtmal9=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal9", locale);
	 static String mailAcceotOrDeclineMailHtmal10=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal10", locale);
	 static String mailAcceotOrDeclineMailHtmal11=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal11", locale);
	 static String mailAcceotOrDeclineMailHtmal12=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal2", locale);
	 static String mailAcceotOrDeclineMailHtmal13=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal3", locale);
	 static String mailAcceotOrDeclineMailHtmal14=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal4", locale);
	 static String mailAcceotOrDeclineMailHtmal15=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal5", locale);
	 static String mailAcceotOrDeclineMailHtmal16=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal6", locale);
	 static String mailAcceotOrDeclineMailHtmal17=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal7", locale);
	 static String mailAcceotOrDeclineMailHtmal18=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal8", locale);
	 
	public synchronized static String getTemplete(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtId,UserMaster userMaster, SchoolMaster schoolId,Integer callbytemplete,String statusFlagAndStatusId,String callingControllerStatus,HttpServletRequest request,String acceptUrl,String declineUrl,String flowOptionNames){
		StringBuffer sb=new StringBuffer("");
		sb.append(getTMTableStart());
		if(callingControllerStatus.trim().equalsIgnoreCase("HR Review/Offer")){
			sb.append(getHrReviewOfferTemplate(teacherDetail,jobOrder,schoolId,callbytemplete,acceptUrl,declineUrl));
		}else if(callingControllerStatus.trim().equalsIgnoreCase("Evaluation Complete")){
			sb.append(getEvaluationCompleteTemplate(teacherDetail,jobOrder,schoolId,callbytemplete,acceptUrl,declineUrl));
		}else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1201470) && callingControllerStatus.trim().equalsIgnoreCase("Offer Made")){
			sb.append(getOscalaOfferMadeTemplate(teacherDetail,jobOrder,schoolId,callbytemplete,acceptUrl,declineUrl,userMaster));
		}else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(804800) && callingControllerStatus.trim().equalsIgnoreCase("Offer Made")){
			sb.append(getJeffcoOfferMadeTemplate(teacherDetail,jobOrder,schoolId,callbytemplete,acceptUrl,declineUrl,userMaster));
		}else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806900) && callingControllerStatus.trim().equalsIgnoreCase("Recommend for Hire")){
			sb.append(getAdamsRecommendForHireMailOffer(teacherDetail,jobOrder,schoolId,callbytemplete,acceptUrl,declineUrl,userMaster));
		}else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806810) && callingControllerStatus.trim().equalsIgnoreCase("Preliminary Offer")){
			sb.append(getSummitSchoolDostrictRecommendForHireMailOffer(teacherDetail,jobOrder,schoolId,callbytemplete,acceptUrl,declineUrl,userMaster));
		}else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806900) && callingControllerStatus.trim().equalsIgnoreCase("teacherAcceptedMail")){
			sb.append(getCustomMailFromDistrictTemplateForMessage(jobOrder,"Accepted Conditional Job Offer.DA & SA",teacherDetail,schoolId));
		}else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1201290) && callingControllerStatus.trim().equalsIgnoreCase("Offer Extended")){
			sb.append(getMartinMailOffer(teacherDetail,jobOrder,schoolId,callbytemplete,acceptUrl,declineUrl,userMaster));
		}else if(callingControllerStatus.trim().equalsIgnoreCase("teacherAcceptedMail")){
			sb.append(getTeacherAcceptMail(teacherDetail,jobOrder,schoolId,userMaster));
		}
		else if(callingControllerStatus.trim().equalsIgnoreCase("teacherDeclinedMail")){
			 if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806900)){
				 sb.append(getCustomMailFromDistrictTemplateForMessage(jobOrder,"Applicant NOT Accept Conditional Job Offer. DA. SA",teacherDetail,schoolId));
			 }else
				 sb.append(getTeacherDeclineMail(teacherDetail,jobOrder,schoolId,userMaster));
		}else if(callingControllerStatus.trim().equalsIgnoreCase("allSchoolUserNotificationMail")){
			sb.append(getAllSchoolUserNotificationMail(teacherDetail,jobOrder,schoolId,userMaster));
		}else if(callingControllerStatus.trim().equalsIgnoreCase("teacherAcceptedOfferMadeMail")){
			sb.append(getTeacherAcceptedOfferMadeMail(teacherDetail,jobOrder,schoolId,userMaster,"accepted"));
		}else if(callingControllerStatus.trim().equalsIgnoreCase("teacherDeclinedOfferMadeMail")){
			sb.append(getTeacherAcceptedOfferMadeMail(teacherDetail,jobOrder,schoolId,userMaster,"declined"));
		}else if(callingControllerStatus.trim().equalsIgnoreCase("teacherAcceptedOfferMadeJeffcoMail")){
			sb.append(getTeacherAcceptedOfferMadeJeffcoMail(teacherDetail,jobOrder,schoolId,userMaster,"accepted"));
		}else if(callingControllerStatus.trim().equalsIgnoreCase("teacherDeclinedOfferMadeJeffcoMail")){
			sb.append(getTeacherDeclinedOfferMadeJeffcoMail(teacherDetail,jobOrder,schoolId,userMaster,"declined"));
		}else if(callingControllerStatus.trim().equalsIgnoreCase("Restart Workflow")){
			sb.append(getFlowOptions(teacherDetail,jobOrder,flowOptionNames));
		}
		sb.append(getTMTableEnd());
		return sb.toString();
	}
	
	public synchronized static String getNotificationTemplete(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtId,UserMaster userMaster, SchoolMaster schoolId,Integer callbytemplete,String statusFlagAndStatusId,String callingControllerStatus,HttpServletRequest request){
		StringBuffer sb=new StringBuffer("");
		sb.append(getTMTableStart());
		if(callingControllerStatus.trim().equalsIgnoreCase("JEFFCO_NF_Offer Made") || callingControllerStatus.trim().equalsIgnoreCase("SUMMIT_NF_Preliminary Offer")){
			sb.append(getES_JEFFCO_NF_OfferMade(teacherDetail,jobOrder,schoolId,callbytemplete,userMaster,callingControllerStatus));
		}else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806900) &&callingControllerStatus.trim().equalsIgnoreCase("ADAMS_NF_HR Review Successful")){
			 sb.append(getCustomMailFromDistrictTemplateForMessage(jobOrder,"HR Applicant Review Successful.SA",teacherDetail,schoolId));	
		}else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806900) &&callingControllerStatus.trim().equalsIgnoreCase("ADAMS_NF_HR Approval")){
			 sb.append(getCustomMailFromDistrictTemplateForMessage(jobOrder,"HR Job Offer Approval.Applicant.SA.DA",teacherDetail,schoolId));	
		}
		sb.append(getTMTableEnd());
		return sb.toString();
	}
	
	public static String getTemplate(JobOrder jobOrder, SchoolMaster schoolId, TeacherDetail teacherDetail, String callingControllerStatus){
		StringBuffer sb=new StringBuffer("");
		sb.append(getTMTableStart());
		if(callingControllerStatus.trim().equalsIgnoreCase("INTENT TO HIRE")){
			sb.append(getIntentToHireMailContent(jobOrder,  schoolId,  teacherDetail));
		}
		sb.append(getTMTableEnd());
		return sb.toString();
	}
	
	private static String getTeacherAcceptMail(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,UserMaster userMaster){
		StringBuffer sb=new StringBuffer("");
		
		try{	
			String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
			if(schoolId!=null && schoolId.getSchoolName()!=null && !schoolId.getSchoolName().trim().equalsIgnoreCase(""))
				districtOrSchoolName=schoolId.getSchoolName();
			String acceptContent="";
			if(jobOrder.getDistrictMaster().getDistrictId().equals(806900))
			acceptContent="Dear Admin,</br>This email is to notify you that "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" ("+teacherDetail.getEmailAddress()+") "+lblhasaccepted+" "+"the position of "+jobOrder.getJobTitle()+" at location "+districtOrSchoolName+" on "+CURRENT_DATE+".</br></br>Best,</br>The Recruiting Department - Human Resources </br>Adams 12 Five Star Schools</br>720-972-4068";
			else
				acceptContent=teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" "+lblhasaccepted+" "+jobOrder.getJobTitle()+" at "+districtOrSchoolName+" on "+CURRENT_DATE+".";
			sb.append(getTrNormalHTML(acceptContent, 1));
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();		
	}
	private static String getTeacherAcceptedOfferMadeMail(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,UserMaster userMaster, String acceptOrDecline){
		StringBuffer sb=new StringBuffer("");
		SimpleDateFormat dateFormatYear = new SimpleDateFormat("yyyy");
		String urlCG="";
		try{
			urlCG=Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+jobOrder.getCreatedForEntity();
		}catch(Exception e){e.printStackTrace();}
		
		try{	
			String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
			if(schoolId!=null && schoolId.getSchoolName()!=null && !schoolId.getSchoolName().trim().equalsIgnoreCase(""))
				districtOrSchoolName=schoolId.getSchoolName();
			
			String sUserName="Dear Admin/Analyst,";//+userMaster.getFirstName()+" "+(userMaster.getLastName()!=null?userMaster.getLastName():"")+",";
			String acceptContent=teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" has "+acceptOrDecline+" "+jobOrder.getJobTitle()+" at "+districtOrSchoolName+".<br/> Please click <a href='"+urlCG+"'>here</a> to see the candidate grid.";
			String body1="Name of School:\t"+" "+districtOrSchoolName;
			String body11="Teaching Assignment Offered:\t"+" "+jobOrder.getJobTitle();
			String body111="School Year:\t"+" "+dateFormatYear.format(new Date());
			String body2="This offer is conditioned upon the following:";	
			String body22="<ul><li>Completion of the instructional employment application, receipt of official transcripts and references</br></li><li>Negative results on TB and drug test and clearance on fingerprint screening (FDLE and FBI Background Check)</li><li>Eligibility for a Florida Educator�s Certificate and proof of Highly Qualified status in accordance with the No Child Left Behind law</li><li>Final acceptance and approval by the Superintendent and the School Board of Osceola County, Florida, as required by law</li></ul>";
			String body3="Be advised that to qualify for an advanced degree salary supplement, Florida statute requires that the degree major, as posted on an official transcript, matches a subject listed on the Florida certificate. Please visit the Human Resources section of the District website for additional information concerning this law.";
			String body4="This conditional offer is being extended based on a salary at the bachelor�s level at zero years of experience. The appropriate documentation (experience verification and official transcripts) must be received and evaluated to determine if a higher salary level and supplement will be awarded.";
			String body5="The School District of Osceola County is not responsible for moving or relocation expenses incurred in establishing residency in the Central Florida area.";
			String body6="The candidate has 48 hours from the time this email was sent to accept or decline the conditional offer made by the School District of Osceola County, Florida. After that, this offer may be withdrawn by the District at any time.";
			String body7="This will help district with new hire on boarding.";
			
			sb.append(getTrNormalHTML(sUserName, 2));
			sb.append(getTrNormalHTML(acceptContent, 2));
			sb.append(getTrNormalHTML(body1, 2));
			sb.append(getTrNormalHTML(body11, 2));
			sb.append(getTrNormalHTML(body111, 2));
			sb.append(getTrNormalHTML(body2, 2));
			sb.append(getTrNormalHTML(body22, 2));
			sb.append(getTrNormalHTML(body3, 2));
			sb.append(getTrNormalHTML(body4, 2));
			sb.append(getTrNormalHTML(body5, 2));
			sb.append(getTrNormalHTML(body6, 2));
			sb.append(getTrNormalHTML(body7, 2));
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();		
	}
	
	private static String getTeacherAcceptedOfferMadeJeffcoMail(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,UserMaster userMaster, String acceptOrDecline){
		StringBuffer sb=new StringBuffer("");
				try{
					String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
					if(schoolId!=null && schoolId.getSchoolName()!=null && !schoolId.getSchoolName().trim().equalsIgnoreCase(""))
						districtOrSchoolName=schoolId.getSchoolName();
					String declineContent=teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" "+lblhasaccepted+" "+jobOrder.getJobTitle()+" position at "+districtOrSchoolName+" Job Order :"+jobOrder.getJobId();
					sb.append(getTrNormalHTML(declineContent, 1));
				}catch(Exception e){
					e.printStackTrace();
				}
				return sb.toString();
	}
	
	private static String getTeacherDeclinedOfferMadeJeffcoMail(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,UserMaster userMaster, String acceptOrDecline){
		StringBuffer sb=new StringBuffer("");
				try{
					String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
					if(schoolId!=null && schoolId.getSchoolName()!=null && !schoolId.getSchoolName().trim().equalsIgnoreCase(""))
						districtOrSchoolName=schoolId.getSchoolName();
					String declineContent=teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" "+lblhasdeclinedthe+" "+jobOrder.getJobTitle()+" position at "+districtOrSchoolName+" Job Order :"+jobOrder.getJobId();
					sb.append(getTrNormalHTML(declineContent, 1));
				}catch(Exception e){
					e.printStackTrace();
				}
				return sb.toString();
	}
	
	
	private static String getTeacherDeclineMail(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,UserMaster userMaster){
		StringBuffer sb=new StringBuffer("");
		
		try{
				
			String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
			if(schoolId!=null && schoolId.getSchoolName()!=null && !schoolId.getSchoolName().trim().equalsIgnoreCase(""))
				districtOrSchoolName=schoolId.getSchoolName();
			String declineContent=teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" "+lblhasdeclined+" "+jobOrder.getJobTitle()+" at "+districtOrSchoolName+" on "+CURRENT_DATE+".";
			sb.append(getTrNormalHTML(declineContent, 1));
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();		
	}
	
	private static String getAllSchoolUserNotificationMail(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,UserMaster userMaster){
		StringBuffer sb=new StringBuffer("");		
		try{			
			String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
			if(schoolId!=null && schoolId.getSchoolName()!=null && !schoolId.getSchoolName().trim().equalsIgnoreCase(""))
			districtOrSchoolName=schoolId.getSchoolName();					
			String schoolName=lblSchoolName+":"+districtOrSchoolName;
			String applicantName= lblApplicantName+": "+getTeacherOrUserName(teacherDetail);
			String position =lblPosition+": "+jobOrder.getJobTitle();
			
			sb.append(getTrNormalHTML(schoolName, 2));
			sb.append(getTrNormalHTML(applicantName, 2));
			sb.append(getTrNormalHTML(position, 2));
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();		
	}
	private static String getFlowOptions(TeacherDetail teacherDetail,JobOrder jobOrder,String flowOptionNames){
		StringBuffer sb=new StringBuffer("");		
		try{			
			String emailText1="Dear "+getTeacherOrUserName(teacherDetail);
			String emailText2="Your application is still incomplete. Please visit your TeacherMatch account by clicking the link below and complete the following items:";
			String emailText21=flowOptionNames;
			String emailText3="<a href='"+Utility.getValueOfPropByKey("basePath")+"/userdashboard.do"+"'>"+Utility.getValueOfPropByKey("basePath")+"/userdashboard.do</a>";
			String emailText4="If you have questions, please reach to your Kelly branch for additional support:";
			StringBuffer kesData=new StringBuffer();
			try{
				if(jobOrder!=null){
					BranchMaster branchMaster=jobOrder.getBranchMaster();
					if(branchMaster!=null){
						kesData.append("<br/>Branch Business Name: "+branchMaster.getBranchName());
						String branchAddress="";
						if(branchMaster.getAddress()!=null){
							branchAddress=branchMaster.getAddress();
						}
						kesData.append("<br/>Branch Address: "+branchAddress);
						
						/*String branchContact1="";
						if(branchMaster.getContactNumber1()!=null){
							branchContact1=branchMaster.getContactNumber1();
						}
						kesData.append("<br/>Branch Contact 1: "+branchContact1);
						String branchContact2="";
						if(branchMaster.getContactNumber2()!=null){
							branchContact2=branchMaster.getContactNumber2();
						}
						kesData.append("<br/>Branch Contact 2: "+branchContact2);*/
						kesData.append("<br/>Branch Email Address: "+branchMaster.getBranchEmailAddress());
						kesData.append("<br/>Branch Phone Number: "+branchMaster.getPhoneNumber());
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			String emailText6="Best,";
			String emailText7="Kelly Support Team";
			sb.append(getTrNormalHTML(emailText1, 2));
			sb.append(getTrNormalHTML(emailText2, 2));
			sb.append(getTrNormalHTML(emailText21, 2));
			sb.append(getTrNormalHTML(emailText3, 2));
			sb.append(getTrNormalHTML(emailText4, 2));
			sb.append(getTrNormalHTML(kesData.toString(), 2));
			sb.append(getTrNormalHTML(emailText6, 2));
			sb.append(getTrNormalHTML(emailText7, 2));
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("sb:::::::::::::::::::::::::::::::::::::::"+sb.toString());
		return sb.toString();		
	}
	
	private static String getEvaluationCompleteTemplate(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,Integer callbytemplete,String acceptUrl,String declineUrl){
		StringBuffer sb=new StringBuffer("");
		try{
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
		String mailDate=" "+dateFormat.format(new Date());	
		String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
		if(schoolId!=null && schoolId.getSchoolName()!=null && !schoolId.getSchoolName().trim().equalsIgnoreCase(""))
			districtOrSchoolName=schoolId.getSchoolName();
			String position = jobOrder.getJobTitle()+" at "+districtOrSchoolName;
			String tName=lblDear+getTeacherOrUserName(teacherDetail)+",";
			String body1=" "+mailAcceotOrDeclineMailHtmal1+"+position+"+mailAcceotOrDeclineMailHtmal2;
			String body2="<ul><li>"+mailAcceotOrDeclineMailHtmal3+"</br></li><li>mailAcceotOrDeclineMailHtmal4</li><li>"+mailAcceotOrDeclineMailHtmal5+"</li><li>"+mailAcceotOrDeclineMailHtmal5+"</li><li>"+mailAcceotOrDeclineMailHtmal6+"</li><li>"+mailAcceotOrDeclineMailHtmal7+"</li></ul>";						
			String body3="+mailAcceotOrDeclineMailHtmal8+";
			String link="<a href='"+acceptUrl+"'>"+mailAcceotOrDeclineMailHtmal9+"</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='"+declineUrl+"'>"+mailAcceotOrDeclineMailHtmal10+"</a>";
			String body4=""+mailAcceotOrDeclineMailHtmal11+"<br/><br/>"+mailAcceotOrDeclineMailHtmal12+"<br/>"+mailAcceotOrDeclineMailHtmal10+"";
			
			sb.append(getTrNormalHTML(tName, 2));
			sb.append(getTrNormalHTML(body1, 2));
			sb.append(getTrNormalHTML(body2, 2));
			sb.append(getTrNormalHTML(body3, 2));
			sb.append(getTrNormalHTML(link, 2));
			sb.append(getTrNormalHTML(body4, 2));
	}catch(Exception e){
		e.printStackTrace();
	}
		return sb.toString();
	}
	
	private static String getJeffcoOfferMadeTemplate(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,Integer callbytemplete,String acceptUrl,String declineUrl,UserMaster userMaster){
		StringBuffer sb=new StringBuffer("");
		try{
			String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
			if(schoolId!=null && schoolId.getSchoolName()!=null)
				districtOrSchoolName=schoolId.getSchoolName();
				
			String tName="Dear "+getTeacherOrUserName(teacherDetail)+",";
			String body1="We are pleased to offer you "+jobOrder.getJobId()+" "+jobOrder.getJobTitle()+" at "+(districtOrSchoolName)+" with Jeffco Public Schools.";
			String link="<a href='"+acceptUrl+"'>I accept</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='"+declineUrl+"'>I decline</a>";	
			String body2="Upon acceptance additional required steps in our hiring process will follow.";
			String signature="Sincerely,<br/>Jeffco Public Schools Employment Team";	
			
			sb.append(getTrNormalHTML(tName, 2));
			sb.append(getTrNormalHTML(body1, 2));
			sb.append(getTrNormalHTML(link, 2));
			sb.append(getTrNormalHTML(body2, 2));
			sb.append(getTrNormalHTML(signature, 2));
	}catch(Exception e){
		e.printStackTrace();
	}
		return sb.toString();
	}
	
	private static String getOscalaOfferMadeTemplate(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,Integer callbytemplete,String acceptUrl,String declineUrl,UserMaster userMaster){
		StringBuffer sb=new StringBuffer("");
		try{
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");
		SimpleDateFormat dateFormatYear = new SimpleDateFormat("yyyy");
		String mailDate=" "+dateFormat.format(new Date());	
		String senderName=userMaster.getFirstName()+" "+(userMaster.getLastName()!=null?userMaster.getLastName():"");
		String senderEmailId=userMaster.getEmailAddress()!=null?(userMaster.getEmailAddress().equalsIgnoreCase("")?"__________________________":userMaster.getEmailAddress()):"__________________________";
		
		String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
		if(schoolId!=null && schoolId.getSchoolName()!=null && !schoolId.getSchoolName().trim().equalsIgnoreCase(""))
			districtOrSchoolName=schoolId.getSchoolName();
			String position = jobOrder.getJobTitle()+" at "+districtOrSchoolName;
			String tName="Dear "+getTeacherOrUserName(teacherDetail)+",";
			String body1="We are excited to extend a Conditional Offer of Employment for you to join our Instructional Team in Osceola County, Florida!  Here are some details about the position:";
			String body11="Name of School:\t"+" "+districtOrSchoolName;
			String body111="Teaching Assignment Offered:\t"+" "+jobOrder.getJobTitle();
			String body1111="School Year:\t"+" "+dateFormatYear.format(new Date());
			String body2="This offer is conditioned upon the following:";			
			String body22="<ul><li>Completion of the instructional employment application, receipt of official transcripts and references</br></li><li>Negative results on TB and drug test and clearance on fingerprint screening (FDLE and FBI Background Check)</li><li>Eligibility for a Florida Educator�s Certificate and proof of Highly Qualified status in accordance with the No Child Left Behind law</li><li>Final acceptance and approval by the Superintendent and the School Board of Osceola County, Florida, as required by law</li></ul>";
			String body3="Be advised that to qualify for an advanced degree salary supplement, Florida statute requires that the degree major, as posted on an official transcript, matches a subject listed on the Florida certificate. Please visit the Human Resources section of the District website for additional information concerning this law.";
			String body33="This conditional offer is being extended based on a salary at the bachelor�s level at zero years of experience. The appropriate documentation (experience verification and official transcripts) must be received and evaluated to determine if a higher salary level and supplement will be awarded.";
			String body333="The School District of Osceola County is not responsible for moving or relocation expenses incurred in establishing residency in the Central Florida area.";
			String body3333="If being placed in an Early Hiring Pool, specific site assignments will be determined shortly and may require an additional interview/selection process by the site(s).";
			String body33333="I understand that I have 48 hours from the time this email was sent to me to accept or decline the conditional offer made by the School District of Osceola County, Florida.  After that, this offer may be withdrawn by the District at any time.";
			String body333333="We look forward to you becoming a part of our Instructional team and impacting the lives of the students of Osceola County.  Please contact me as quickly as possible if you have any additional questions about this offer.";
			String link="<a href='"+acceptUrl+"'>Accept the Offer</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='"+declineUrl+"'>Decline the Offer</a>";
			String body4="Regards,<br/><br/>"+senderName+"<br/><br/>"+(userMaster.getEntityType()==3?"Principal ":"")+districtOrSchoolName+"<br/><br/>"+getTMEmailIdHtml(senderEmailId,senderEmailId)+"<br/>";
			
			sb.append(getTrNormalHTML(tName, 2));
			sb.append(getTrNormalHTML(body1, 2));
			sb.append(getTrNormalHTML(body11, 2));
			sb.append(getTrNormalHTML(body111, 2));
			sb.append(getTrNormalHTML(body1111, 2));
			sb.append(getTrNormalHTML(body2, 2));
			sb.append(getTrNormalHTML(body22, 2));
			sb.append(getTrNormalHTML(body3, 2));
			sb.append(getTrNormalHTML(body33, 2));
			sb.append(getTrNormalHTML(body333, 2));
			sb.append(getTrNormalHTML(body3333, 2));
			sb.append(getTrNormalHTML(body33333, 2));
			sb.append(getTrNormalHTML(body333333, 2));
			sb.append(getTrNormalHTML(link, 3));
			sb.append(getTrNormalHTML(body4, 2));
	}catch(Exception e){
		e.printStackTrace();
	}
		return sb.toString();
	}
	
	
	private static String getHrReviewOfferTemplate(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,Integer callbytemplete,String acceptUrl,String declineUrl){
		StringBuffer sb=new StringBuffer("");
		try{
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
		String mailDate=" "+dateFormat.format(new Date());	
		String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
		if(schoolId!=null && schoolId.getSchoolName()!=null && schoolId.getSchoolName().trim().equalsIgnoreCase(""))
			districtOrSchoolName=schoolId.getSchoolName();
			String tName=lblDear+" "+getTeacherOrUserName(teacherDetail)+",";
			String body1=""+mailAcceotOrDeclineMailHtmal14+" "+jobOrder.getJobTitle()+" at "+districtOrSchoolName+".";
			String body2=mailAcceotOrDeclineMailHtmal15;			
			String link="<a href='"+acceptUrl+"'>"+mailAcceotOrDeclineMailHtmal9+"</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='"+declineUrl+"'>"+mailAcceotOrDeclineMailHtmal10+"</a>";
			String body3=mailAcceotOrDeclineMailHtmal16;
			String body4=""+mailAcceotOrDeclineMailHtmal11+",<br/>"+mailAcceotOrDeclineMailHtmal17+"<br/>"+mailAcceotOrDeclineMailHtmal18+"";
			
			sb.append(getTrNormalHTML(tName, 2));
			sb.append(getTrNormalHTML(body1, 2));
			sb.append(getTrNormalHTML(body2, 2));			
			sb.append(getTrNormalHTML(link, 2));
			sb.append(getTrNormalHTML(body3, 2));
			sb.append(getTrNormalHTML(body4, 2));
	}catch(Exception e){
		e.printStackTrace();
	}
		return sb.toString();
	}
	
	private static String getIntentToHireMailContent(JobOrder jobOrder, SchoolMaster schoolId, TeacherDetail teacherDetail){
		StringBuffer sb=new StringBuffer("");		
		try{			
			String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
			if(schoolId!=null && schoolId.getSchoolName()!=null && schoolId.getSchoolName().trim().equalsIgnoreCase(""))
			districtOrSchoolName=schoolId.getSchoolName();					
			String schoolName="School Name : "+districtOrSchoolName;
			String applicantName="Applicant Name : "+getTeacherOrUserName(teacherDetail);
			String position ="Position : "+jobOrder.getJobTitle();			
			sb.append(getTrNormalHTML(schoolName, 2));
			sb.append(getTrNormalHTML(applicantName, 2));
			sb.append(getTrNormalHTML(position, 2));
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();		
	}
	
	
	private static String getES_JEFFCO_NF_OfferMade(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,int callbytemplete, UserMaster userMaster, String callingControllerStatus){
		StringBuffer sb=new StringBuffer("");		
		try{			
			String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
			if(schoolId!=null && schoolId.getSchoolName()!=null && schoolId.getSchoolName().trim().equalsIgnoreCase(""))
			districtOrSchoolName=schoolId.getSchoolName();		
			String urlCG="";
			try{
				urlCG=Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+jobOrder.getCreatedForEntity();
			}catch(Exception e){e.printStackTrace();}
			
			/*String sUserName="Dear "+jobOrder.getEmploymentServicesTechnician().getPrimaryESTech()+"/"+jobOrder.getEmploymentServicesTechnician().getBackupESTech()+",";//+userMaster.getFirstName()+" "+(userMaster.getLastName()!=null?userMaster.getLastName():"")+",";
			String acceptContent=getTeacherOrUserName(teacherDetail)+" has accepted "+jobOrder.getJobTitle()+" at "+districtOrSchoolName+".<br/> Please click <a href='"+urlCG+"'>here</a> to see the candidate grid.";
			String body1="Name of School:\t"+" "+districtOrSchoolName;
			String body11="Teaching Assignment Offered:\t"+" "+jobOrder.getJobTitle();
			String body111="School Year:\t"+" "+new SimpleDateFormat("yyyy").format(new Date());
			String body2="This offer is conditioned upon the following:";	
			String body22="<ul><li>Completion of the instructional employment application, receipt of official transcripts and references</br></li><li>Negative results on TB and drug test and clearance on fingerprint screening (FDLE and FBI Background Check)</li><li>Eligibility for a Florida Educator�s Certificate and proof of Highly Qualified status in accordance with the No Child Left Behind law</li><li>Final acceptance and approval by the Superintendent and the School Board of "+jobOrder.getDistrictMaster().getDistrictName()+", as required by law</li></ul>";
			String body3="Be advised that to qualify for an advanced degree salary supplement, Florida statute requires that the degree major, as posted on an official transcript, matches a subject listed on the Florida certificate. Please visit the Human Resources section of the District website for additional information concerning this law.";
			String body4="This conditional offer is being extended based on a salary at the bachelor�s level at zero years of experience. The appropriate documentation (experience verification and official transcripts) must be received and evaluated to determine if a higher salary level and supplement will be awarded.";
			String body5="The School District of "+jobOrder.getDistrictMaster().getDistrictName()+" is not responsible for moving or relocation expenses incurred in establishing residency in the Central Florida area.";
			String body6="The candidate has 48 hours from the time this email was sent to accept or decline the conditional offer made by the School District of "+jobOrder.getDistrictMaster().getDistrictName()+", Florida. After that, this offer may be withdrawn by the District at any time.";
			String body7="This will help district with new hire on boarding.";
			
			sb.append(getTrNormalHTML(sUserName, 2));
			sb.append(getTrNormalHTML(acceptContent, 2));
			sb.append(getTrNormalHTML(body1, 2));
			sb.append(getTrNormalHTML(body11, 2));
			sb.append(getTrNormalHTML(body111, 2));
			sb.append(getTrNormalHTML(body2, 2));
			sb.append(getTrNormalHTML(body22, 2));
			sb.append(getTrNormalHTML(body3, 2));
			sb.append(getTrNormalHTML(body4, 2));
			sb.append(getTrNormalHTML(body5, 2));
			sb.append(getTrNormalHTML(body6, 2));
			sb.append(getTrNormalHTML(body7, 2));*/
			
			String mailBody1="Hello,"; 
			String mailBody2=getTeacherOrUserName(userMaster)+" recently changed the status of applicant "+getTeacherOrUserName(teacherDetail)+"  who had applied for "+jobOrder.getJobTitle()+" to "+callingControllerStatus.replace("JEFFCO_NF_", "")+".";
			String mailBody2_1="Position Number: "+(jobOrder.getRequisitionNumber()!=null?jobOrder.getRequisitionNumber():"");
			String mailBody2_2="Job Id: "+jobOrder.getJobId();
			String mailBody2_3="You can view the Candidate Grid for this job <a href='"+urlCG+"'>here</a>.";
			String mailBody3="If you have any questions or if you are having any technical issues, please email us at "+GlobalServices.getTMEmailIdHtml("clientservices@teachermatch.net", "clientservices@teachermatch.net")+" or call 1-888-312-7231.";
			String mailBody4="Sincerely<br/>"+districtOrSchoolName; 
			String mailBody5="you are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at "+GlobalServices.getTMEmailIdHtml("clientservices@teachermatch.net", "clientservices@teachermatch.net")+" or call 1-888-312-7231.";
			sb.append(getTrNormalHTML(mailBody1, 2));
			sb.append(getTrNormalHTML(mailBody2, 2));
			sb.append(getTrNormalHTML(mailBody2_1, 1));
			sb.append(getTrNormalHTML(mailBody2_2, 2));
			sb.append(getTrNormalHTML(mailBody2_3, 2));
			sb.append(getTrNormalHTML(mailBody3, 2));
			sb.append(getTrNormalHTML(mailBody4, 4));
			sb.append(getSmallTrNormalHTML(mailBody5, 2,"font-family: Century Gothic,Open Sans, sans-serif;font-size: 12px;"));
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	public synchronized static String getAutoInactiveNotificationMail(JobOrder jobOrder){
		StringBuffer sb=new StringBuffer();
		sb.append(getTMTableStart());
		String mailBody=jobOrder.getRequisitionNumber()+", "+jobOrder.getJobTitle()+" has been "+(jobOrder.getStatus().trim().equalsIgnoreCase("I")?"inactivated":"activated")+" because all hires for this position have been made. If you need to review this position or reactivate it, please go to your listing of jobs and select \"inactive\" in the job status search.";
		sb.append(getTrNormalHTML(mailBody, 2));
		sb.append(getTMTableEnd());
		return sb.toString();
	}
	
	public synchronized static String getSmallTrNormalHTML(String content, int howManyBr, String css){
		StringBuffer sb=new StringBuffer();
		sb.append("<tr><td style='"+css+"'>");
		sb.append(content);
		for(int i=0;i<howManyBr;i++){sb.append("<br/>");}
		sb.append("</td></tr>");
		return sb.toString();		
	}
	
	public synchronized static String sendNotificationTOAllSAsAndESTechMail(UserMaster userMaster, JobOrder jobOrder, SchoolMaster schoolMaster, TeacherDetail teacherDetail){
		StringBuffer sb=new StringBuffer();
		String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
		if(schoolMaster!=null)
			districtOrSchoolName=schoolMaster.getSchoolName();
		sb.append(getTMTableStart());
		System.out.println("schoolName========="+districtOrSchoolName);
		String mailContent1="Dear "+getTeacherOrUserName(userMaster)+",";
		String mailContent2=getTeacherOrUserName(teacherDetail)+" has been hired to "+jobOrder.getJobId()+" ("+jobOrder.getJobTitle()+") at "+districtOrSchoolName+".";
		String mailContent3="Name of School: "+districtOrSchoolName;
		String mailContent4="Teaching Assignment Offered: "+jobOrder.getJobId();
		                              
		sb.append(getTrNormalHTML(mailContent1, 2));
		sb.append(getTrNormalHTML(mailContent2, 2));
		sb.append(getTrNormalHTML(mailContent3, 2));
		sb.append(getTrNormalHTML(mailContent4, 2));
		sb.append(getTMTableEnd());
		return sb.toString();
	}
	
	public synchronized static String sendDenyNotificationTOJobCreaterMail(UserMaster userMaster, JobOrder jobOrder, String comments){
		StringBuffer sb=new StringBuffer();
		String districtName=jobOrder.getDistrictMaster().getDistrictName();
		sb.append(getTMTableStart());
		System.out.println("districtName========="+districtName);
		String mailContent1="This Job Order "+jobOrder.getJobId()+" has been denied by an approver for the following reason "+comments+"."; 
		//String mailContent1=jobOrder.getJobTitle()+", "+jobOrder.getJobId()+" was not approved by "+getTeacherOrUserName(userMaster)+" for the below reasons:";
		sb.append(getTrNormalHTML(mailContent1, 2));
		//sb.append(getTrNormalHTML(comments, 2));
		sb.append(getTMTableEnd());
		return sb.toString();
	}
	
	public synchronized static String sendAddUserByAPIMailNotification(UserMaster userMaster, DistrictMaster districtMaster,HttpServletRequest request){
		StringBuffer sb=new StringBuffer();
		String districtName=districtMaster.getDistrictName();
		sb.append(getTMTableStart());
		String body="Dear "+getTeacherOrUserName(userMaster)+": You have been added as a user on the TeacherMatch<sup>TM</sup> platform. Your Login Email is "+GlobalServices.getTMEmailIdHtml(userMaster.getEmailAddress(), userMaster.getEmailAddress())+". Your Authentication PIN is "+userMaster.getAuthenticationCode()+". In order to activate your account, please visit the link below to set your password:";  
		String body1verificationUrl		=	Utility.getBaseURL(request)+"resetpassword.do?key="+userMaster.getVerificationCode()+"&id="+userMaster.getUserId();
		String body2="Please contact your Admin if you have questions.";
		String body3="You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin."; 
		sb.append(getTrNormalHTML(body, 2));
		sb.append(getTrNormalHTML(getAnchorTagWithNewPage("",body1verificationUrl,body1verificationUrl), 2));
		sb.append(getTrNormalHTML(body2, 2));
		sb.append(getTrFooterHTML(body3, 2));
		sb.append(getTMTableEnd());
		return sb.toString();
	}
	
	private static String getAdamsRecommendForHireMailOffer(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,Integer callbytemplete,String acceptUrl,String declineUrl,UserMaster userMaster){
		StringBuffer sb=new StringBuffer("");
		try{
			String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
			if(schoolId!=null && schoolId.getSchoolName()!=null)
				districtOrSchoolName=schoolId.getSchoolName();
				
			String tName="Dear "+getTeacherOrUserName(teacherDetail)+",";
			String body1="Congratulations on being recommended for hire as a "+jobOrder.getJobId()+" "+jobOrder.getJobTitle()+" for "+(districtOrSchoolName)+" .";
			String body2="The school/department administrator has sent the new hire/transfer paperwork, to Human Resources, for approval.  As standard protocol, Human Resources must approve the paperwork and the applicant�s credentials before the recommendation for hire is complete.";
			String body3="<font color='red'><b><u>ATTENTION!  ACTION REQUIRED:</u></b>  Please CLICK ON THE LINK BELOW to login to TeacherMatch and accept this employment assignment with Adams 12 Five Star Schools.  The Human Resource Department will not begin processing the new hire/transfer paperwork until the offer is electronically accepted in TeacherMatch using the link below.  This offer is void if you do not reply within 48 hours.</font>";
			String link="<center><a href='"+acceptUrl+"'><font color='red'><b><i>Accept the Conditional Offer of Employment **</i></b></font></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='"+declineUrl+"'><font color='red'><b><i>Decline the Offer</i></b></font></a></center>";	
			String body4="After the electronic offer of employment is accepted and Human Resources approves the hire/transfer, you will receive an email detailing the next steps in the process.";
			String body5="If you have any questions, please contact Human Resources. ";
			String signature="Thank you,<br/>The Recruiting Department � Human Resources<br/>Adams 12 Five Star Schools<br/>720-972-4068";
			String body6="<i>** You will not be formally employed by the Adams 12 Five Star School District until you satisfy all the employment conditions to be eligible for hire by the District and the Board of Education accepts the recommendation for hire.  During this process, you may complete the necessary steps to begin performing work for the District.</i>";
			
			sb.append(getTrNormalHTML(tName, 2));
			sb.append(getTrNormalHTML(body1, 2));
			sb.append(getTrNormalHTML(body2, 2));
			sb.append(getTrNormalHTML(body3, 2));
			sb.append(getTrNormalHTML(link, 2));
			sb.append(getTrNormalHTML(body4, 2));
			sb.append(getTrNormalHTML(body5, 2));
			sb.append(getTrNormalHTML(signature, 2));
			sb.append(getTrFooterHTML(body6, 2));
	}catch(Exception e){
		e.printStackTrace();
	}
		return sb.toString();
	}
	
	private static String getSummitSchoolDostrictRecommendForHireMailOffer(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,Integer callbytemplete,String acceptUrl,String declineUrl,UserMaster userMaster){
		StringBuffer sb=new StringBuffer("");
		try{
			String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
			if(schoolId!=null && schoolId.getSchoolName()!=null)
				districtOrSchoolName=schoolId.getSchoolName();
				
			String tName="Dear "+getTeacherOrUserName(teacherDetail)+",";
			String body1="Congratulations! Upon completion of the selection steps that we conducted with you I would like to preliminarily offer to you, the position of "+jobOrder.getJobTitle()+" at "+(districtOrSchoolName) +"for the 2015-16 school year.  Welcome to Summit School District!";
			String body2="The Board of Education is scheduled to approve my recommendation to have you join our team for the 2015-16 school year at its next regularly scheduled meeting.  We are very pleased that you have accepted this preliminary offer of employment which is contingent upon the following items:";
			String body3="<ul style='list-style-type:disc'><li>Having your background check cleared by the Colorado Bureau of Investigation</li><li>Receipt of completed application materials and required paperwork</li><li>Reference checks</li><li>The Summit School District Office of Human Resources determining that you are �highly qualified� under Colorado�s definition of a highly qualified paraprofessional (pertains to paraprofessionals only)</li></ul>";

			String body2_2="The Board of Education is scheduled to approve my recommendation to have you join our team for the 2015-16 school year at its next regularly scheduled meeting.  We are very pleased that you have accepted this preliminary offer of employment that is contingent upon the following items:";
			String body3_3="<ul style='list-style-type:disc'><li>Having your background check cleared by the Colorado Bureau of Investigation, consistent with the information that you provided on your �Applicant�s Oath�</li><li>Receipt of completed application materials and required paperwork</li><li>Reference checks</li><li>The Summit School District�s Human Resource Office determining that you are �highly qualified� under Colorado�s definition of a highly qualified for the position that has been preliminarily offered</li><li>Verification and receipt of appropriate licensure issued by the Colorado Department of Education </li></ul>";
			String body5_5_0="Please visit the Summit School District website at <a href='http://www.summit.k12.co.us/'>www.summit.k12.co.us</a> and proceed as described below to register as a District user.  Upon registering, you may reserve your preferred date and time for the orientation outlined above.";
			String body7_7="<ul style='list-style-type:disc'><li>Click on the �register� tab (located near the top right side next to the �sign in� tab)</li><li>Register yourself as a District user</li><li>Click on the �Departments� tab</li><li>Click on �Human Resources�</li><li>Click on <a href='http://www.summit.k12.co.us/Page/4327'>�New Hire Information 2015�</a></li><li>Go to �New Licensed Staff� � under #2 -click <a href='http://goo.gl/forms/MVOuUZhqDo'>here to sign up for orientation.</a> </li><li>Fill out and submit your form.</li></ul>";
			String body7_70="The objectives of our employment orientation are to welcome you to Summit School District, meet state and federal hiring requirements and to provide you with information related to your employment with us.  Our orientations will be held at the Summit School District Central Office located at 150 School Road in Frisco, Colorado.";
			String body8_8="Please be prepared to provide the following documents/information during your employment orientation:";
			String body9_9="<ul style='list-style-type:disc'><li>Documentation to accompany your I-9 form.  The most common forms of documentation are driver�s license and social security card or a passport.</li><li>A voided check from any United States bank account for your payroll checks to be deposited.</li><li>Social security numbers and addresses of dependents/beneficiaries for the Public Employee�s Retirement Association (PERA) Form.</li><li>Transcripts indicating the date your degree was conferred from your college or university along with any other graduate level hours earned (unless you have uploaded it into your application).</li><li>A copy of your Colorado Teaching License or Authorization (unless you have uploaded it into your application).</li><li>A signed copy of this �Preliminary Offer of Employment� letter.</li></ul>";
			String body5_5="Please know that your attendance at employment orientation as a newly hired licensed staff member is mandatory and you are not eligible to begin work prior to attending employment orientation.  As mentioned above, beginning employment with Summit School District is contingent on meeting and completing all requirements and paperwork outlined above.  Additionally, should our Human Resources Office not be able to verify the above information or if the verified information does not meet Summit School District�s established criteria(s), you will be notified that Summit School District will not be formally offering you employment for the 2015-16 school year.";
			String body10_10="Information related to the annual salary that you will earn during the 2015-16 school year will be provided to you at orientation.  Our salary schedule can be found on the District website at <a href='http://www.summit.k12.co.us/'>www.summit.k12.co.us.</a> Click on the �Departments� tab; click on the �Human Resources� tab, click on <a href='http://www.summit.k12.co.us/Page/166'>Salary Schedules and Pay Information.</a>"+ 
								"Initial Placement on the Licensed Salary Schedule is based on the following:";		
			String body10_10_0="<ul style='list-style-type:disc'><li>Years of previous teaching experience (up to a maximum of 15 years) and complete graduate level coursework beyond being eligible for his/her teaching credential.</li><li>Letters across the top of the schedule represent completed graduate level course work beyond his/her teaching credential in bundles of 12 semester hours (column A equals 0 semester hours, column B equals 12 semester hours, etc.)</li><ul>";
			String body10_10_1="Your contract will be based on information that you have submitted in your application.  Please ensure that all transcripts (including undergraduate and graduate level coursework) and your current resume are uploaded in your application to ensure correct placement on the salary schedule.</br>"+
								"</br>On behalf of the Board of Education and our Superintendent, Dr. Heidi Pace I want to again welcome you to Summit School District.  We are pleased that you have accepted a position with us and we look forward to working together as we continue our goal of �Developing Caring Learners for the 21st Century�.</br>"+
								"</br>Please reply to this email if you accept this employment assignment with "+jobOrder.getDistrictMaster().getDistrictName()+". This offer is void if you do not reply within 2 business days. We look forward to welcoming you to "+jobOrder.getDistrictMaster().getDistrictName()+".</br>";

			String body4="Summit School District will conduct support staff employment orientations at the Summit School District Central Office in Frisco at 150 School Road on the following dates:<ul style='list-style-type:disc'><li>August 27, 2015 - May 3, 2016 � Thursday�s from 9:00 a.m. to 11:00 a.m.*</li></ul>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Or by appointment after September 24, 2015.  Please contact human resources at <a href='mailto:jbarnes@summit.k12.co.us'>jbarnes@summit.k12.co.us</a>.</br>";
			
			String body5="Please know that your attendance at employment orientation as a new support staff member is mandatory and you are not eligible to begin work prior to attending employment orientation.  The objectives of our employment orientation are to welcome you to Summit School District, meet state and federal hiring requirements and to provide you with information related to your employment with us. </br></br>To meet these requirements, please visit the Summit School District website at <a href='http://www.summit.k12.co.us/'>www.summit.k12.co.us</a> and proceed as follows to register as a District user.  Upon registering, you may reserve your preferred date and time for employment orientation. ";
			
			String body7="<ul style='list-style-type:disc'><li>Click on the �Register� tab (located near the top right side next to the �sign in� tab)</li><li>Register yourself as a District user</li><li>Click on the �Departments� tab</li><li>Click on �Human Resources�</li><li>Click on <a href='http://www.summit.k12.co.us/Page/4327'>�New Hire Information 2015�</a></li><li>Scroll down to the �New Support Staff� � under #2 - <a href='https://docs.google.com/forms/d/1IbkR4Ys0Eu0LaKZzQco1wbfgDC6aiML3gglZ8qlf1Hw/viewform?usp=send_form'>Click here</a> to sign up for an orientation.</li><li>Fill out and submit your form</li></ul>";
			String body8="Please be prepared to provide the following documents/information during the employment orientation:";
			String body9="<ul style='list-style-type:disc'><li>Documentation to verify identity and authorization to work in the United States for the Federal I-9 form.  (The most common forms of documentation are driver�s license and social security card, Passport, Permanent Resident Alien Card or Employment Authorization Documentation).</li><li>A voided check from any United States bank account for your payroll checks to be deposited.</li><li>Social security numbers and addresses of dependents/beneficiaries for the Public Employee�s Retirement Association (PERA) Form.</li><li>Transcripts indicating at least 48 semester hours of college level course work, an Associate�s Degree or documentation of passing a content test � if not already uploaded to your application (pertains to paraprofessionals only).</li><li>A signed copy of this �Preliminary Offer of Employment� letter.</li></ul>";
			String body10="Information related to the hourly rate of pay that you will earn during the 2015-16 school year will be provided to you at employment orientation.  If you would like information prior to employment orientation about your hourly rate, please reference our website at <a href='http://www.summit.k12.co.us/'>www.summit.k12.co.us</a>, click on the Departments tab, Human Resources, <a href='http://www.summit.k12.co.us/Page/166'>Salary Schedules and Pay Information</a> for our support staff salary schedule. </br>"+
							"</br>As mentioned above, beginning employment with Summit School District is contingent on completion of all requirements and paperwork outlined above.  Additionally, should our Human Resource Office not be able to verify the above information or if the verifications do not meet Summit School District established criteria(s), you will be notified that Summit School District will not be formally offering you employment for the 2015-16 school year.</br>"+
							"</br>On behalf of the Board of Education and our Superintendent, Dr. Heidi Pace I want to again welcome you to Summit School District.  We are pleased that you have accepted a position with us and we look forward to working together as we continue our goal of �Developing Caring Learners for the 21st Century�.</br>"+
							"</br>Please reply to this email if you accept this employment assignment with "+jobOrder.getDistrictMaster().getDistrictName()+". This offer is void if you do not reply within 2 business days. We look forward to welcoming you to "+jobOrder.getDistrictMaster().getDistrictName()+".</br>";
			
			String link="<center><a href='"+acceptUrl+"'><b><i>Accept the Offer</i></b></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='"+declineUrl+"'><b><i>Decline the Offer</i></b></a></center>";	
			String body11="Sincerely,</br></br>The Department of Human Resources</br>Summit Public Schools</br></br>";
			String body6="<i>Summit School District is an equal opportunity education institution and does not unlawfully discriminate on the basis of race, color, national origin, age, sex, sexual orientation or disability in admission or access to, or treatment, or employment in, its education programs or activities and provides equal access to the Boy Scouts and other designated youth groups. Inquiries concerning non-discrimination policies may be referred to the Summit School District Attn: Superintendent, P.O. Box 7, Frisco, CO 80443, (970) 368-1000.</i>";
			
				sb.append(getTrNormalHTML(tName, 1));
				sb.append(getTrNormalHTML(body1, 1));
			
			if(jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Administrator")||jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Teacher"))
			{
				sb.append(getTrNormalHTML(body2_2, 0));
				sb.append(getTrNormalHTML(body3_3, 1));
				sb.append(getTrNormalHTML(body5_5_0, 1));
				sb.append(getTrNormalHTML(body7_7, 1));
				sb.append(getTrNormalHTML(body7_70, 1));
				sb.append(getTrNormalHTML(body8_8, 1));
				sb.append(getTrNormalHTML(body9_9, 1));
				sb.append(getTrNormalHTML(body5_5, 1));
				sb.append(getTrNormalHTML(body10_10, 1));
				sb.append(getTrNormalHTML(body10_10_0, 1));
				sb.append(getTrNormalHTML(body10_10_1, 1));
			}else{
				sb.append(getTrNormalHTML(body2, 0));
				sb.append(getTrNormalHTML(body3, 1));
				sb.append(getTrNormalHTML(body4, 1));
				sb.append(getTrNormalHTML(body5, 1));
				sb.append(getTrNormalHTML(body7, 1));
				sb.append(getTrNormalHTML(body8, 1));
				sb.append(getTrNormalHTML(body9, 1));
				sb.append(getTrNormalHTML(body10, 1));
			}
				sb.append(getTrNormalHTML(link, 1));
				sb.append(getTrNormalHTML(body11, 1));
				sb.append(getTrFooterHTML(body6, 1));
	}catch(Exception e){
		e.printStackTrace();
	}
		return sb.toString();
	}
	// Send custom template when applicant clicks on the URL to DECLINE the recommendation for hire (Declined Node) 
	 private static synchronized DistrictTemplatesforMessages getDistrictTemplatesforMessagesForDeclined(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,DistrictTemplatesforMessages districtTemplatesforMessages){
			String  mailBody="";
			try{
//					"Applicant NOT Accept Conditional Job Offer. DA. SA"
					mailBody=districtTemplatesforMessages.getTemplateBody();
					mailBody=mailBody.replaceAll("&lt; Teacher First Name &gt;", teacherDetail.getFirstName());
					mailBody=mailBody.replaceAll("&lt; Teacher Last Name &gt;", teacherDetail.getLastName());
					mailBody=mailBody.replaceAll("&lt; Job Title &gt;", jobOrder.getJobTitle());
					if(schoolId!=null)
					mailBody=mailBody.replaceAll("&lt; District or School Location &gt;", schoolId.getSchoolName());
					if(schoolId==null)
						mailBody=mailBody.replaceAll("&lt; District or School Location &gt;", jobOrder.getDistrictMaster().getDistrictName());
					districtTemplatesforMessages.setTemplateBody(mailBody);
					
			}catch(Exception e){
				e.printStackTrace();
			}
			return districtTemplatesforMessages;		
		}
	 private static synchronized DistrictTemplatesforMessages getDistrictTemplatesforMessagesForAcceptance(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,DistrictTemplatesforMessages districtTemplatesforMessages){
			String  mailBody="";
			try{
//					"Accepted Conditional Job Offer. DA & SA"
					mailBody=districtTemplatesforMessages.getTemplateBody();
					mailBody=mailBody.replaceAll("&lt; Teacher First Name &gt;", teacherDetail.getFirstName());
					mailBody=mailBody.replaceAll("&lt; Teacher Last Name &gt;", teacherDetail.getLastName());
					mailBody=mailBody.replaceAll("&lt; Job Title &gt;", jobOrder.getJobTitle());
					if(schoolId!=null)
						mailBody=mailBody.replaceAll("&lt; District or School Name &gt;", schoolId.getSchoolName());
					if(schoolId==null)
						mailBody=mailBody.replaceAll("&lt; District or School Name &gt;", jobOrder.getDistrictMaster().getDistrictName());
					districtTemplatesforMessages.setTemplateBody(mailBody);
					
			}catch(Exception e){
				e.printStackTrace();
			}
			return districtTemplatesforMessages;		
		}
	 private static synchronized DistrictTemplatesforMessages getDistrictTemplatesforMessagesForHRReviewSuccessfull(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictTemplatesforMessages districtTemplatesforMessages){
		 String  mailBody="";
			try{
//					"HR Applicant Review Successful.SA"
					mailBody=districtTemplatesforMessages.getTemplateBody();
					mailBody=mailBody.replaceAll("&lt; Teacher First Name &gt;", teacherDetail.getFirstName());
					mailBody=mailBody.replaceAll("&lt; Teacher Last Name &gt;", teacherDetail.getLastName());
					districtTemplatesforMessages.setTemplateBody(mailBody);
					
			}catch(Exception e){
				e.printStackTrace();
			}
		 
		 return districtTemplatesforMessages;
	 }
	 private static synchronized DistrictTemplatesforMessages getDistrictTemplatesforMessagesForHRApproval(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictTemplatesforMessages districtTemplatesforMessages){
		 String  mailBody="";
			try{
//					"HR Job Offer Approval.Applicant.SA.DA"
					mailBody=districtTemplatesforMessages.getTemplateBody();
					mailBody=mailBody.replaceAll("&lt; Teacher First Name &gt;", teacherDetail.getFirstName());
					mailBody=mailBody.replaceAll("&lt; Teacher Last Name &gt;", teacherDetail.getLastName());
					mailBody=mailBody.replaceAll("&lt; Job Title &gt;", jobOrder.getJobTitle());
					mailBody=mailBody.replaceAll("&lt; District or School Name&gt;", jobOrder.getDistrictMaster().getDistrictName());
					mailBody=mailBody.replaceAll("&lt; District or School Name&gt;", jobOrder.getDistrictMaster().getDistrictName());
					districtTemplatesforMessages.setTemplateBody(mailBody);
					
			}catch(Exception e){
				e.printStackTrace();
			}
		 
		 return districtTemplatesforMessages;
	 }
	 private static synchronized String getCustomMailFromDistrictTemplateForMessage(JobOrder jobOrder,String templateName,TeacherDetail teacherDetail,SchoolMaster schoolId){
			DistrictMaster districtMaster=jobOrder.getDistrictMaster();
			DistrictTemplatesforMessages districtTemplatesforMessages=null;
			String mailBody="";
			try{
				ApplicationContext APPLICATIONCONTEXT = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
				DistrictTemplatesforMessagesDAO districtTemplatesforMessagesDAO = (DistrictTemplatesforMessagesDAO)APPLICATIONCONTEXT.getBean("districtTemplatesforMessagesDAO");
				List<DistrictTemplatesforMessages> districtTemplatesforMessagesList=districtTemplatesforMessagesDAO.findByDistrictAndTemplateName(districtMaster, templateName);
				if(districtTemplatesforMessagesList!=null && districtTemplatesforMessagesList.size()>0){
					 districtTemplatesforMessages=districtTemplatesforMessagesList.get(0);
				}
			if(districtTemplatesforMessages!=null && templateName!=null){
				if(templateName.equalsIgnoreCase("Applicant NOT Accept Conditional Job Offer. DA. SA"))
					districtTemplatesforMessages=getDistrictTemplatesforMessagesForDeclined(teacherDetail, jobOrder, schoolId, districtTemplatesforMessages);
				else if(templateName.equalsIgnoreCase("HR Applicant Review Successful.SA")){
					districtTemplatesforMessages=getDistrictTemplatesforMessagesForHRReviewSuccessfull(teacherDetail, jobOrder,districtTemplatesforMessages);
				}else if(templateName.equalsIgnoreCase("HR Job Offer Approval.Applicant.SA.DA")){
					districtTemplatesforMessages=getDistrictTemplatesforMessagesForHRApproval(teacherDetail, jobOrder,districtTemplatesforMessages);
				}else if(templateName.equalsIgnoreCase("Accepted Conditional Job Offer.DA & SA")){
					districtTemplatesforMessages=getDistrictTemplatesforMessagesForAcceptance(teacherDetail, jobOrder,schoolId,districtTemplatesforMessages);
				}
				mailBody=districtTemplatesforMessages.getTemplateBody();
			
			}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return 	mailBody;
	 }
	 
	 private static String getMartinMailOffer(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolId,Integer callbytemplete,String acceptUrl,String declineUrl,UserMaster userMaster){
			StringBuffer sb=new StringBuffer("");
			try{
				String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
				if(schoolId!=null && schoolId.getSchoolName()!=null)
					districtOrSchoolName=schoolId.getSchoolName();
					
				String tName="Dear "+getTeacherOrUserName(teacherDetail)+",";
				String body1="Congratulations! "+(districtOrSchoolName)+",Florida, hereby offers to conditionally employ you for the position of "+jobOrder.getJobTitle()+" at "+jobOrder.getDistrictMaster().getDisplayName()+","+jobOrder.getDistrictMaster().getAddress()+", "+jobOrder.getDistrictMaster().getCityName()+" "+jobOrder.getDistrictMaster().getZipCode()+"."+
								"This offer is conditional and based on the following requirements including/but not limited to:";
				String body2="<ul style='list-style-type:disc'><li>Certification/Eligibility Verification</li>"+
								"<li>Clearance of Fingerprints</li>"+
								"<li>Drug Testing Clearance</li>"+
								"<li>I-9/Employment Eligibility Verification</li>"+
								"<li>Acceptable Reference/Background Check</li>"+
								"<li>Transcript Verification</li></ul>";
				String link="<center><a href='"+acceptUrl+"'>Accept the Offer</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='"+declineUrl+"'>Decline the Offer</a></center>";	
				String body3="Please select from the options below to accept or decline this employment assignment with Martin County Public Schools. This offer is void if you do not reply within 2 business days. We look forward to welcoming you to Martin County Public Schools.";
				String body4="Sincerely,";
				String body5="The Office of Human Resources</br>Martin County Schools</br>(772) 219-1200";
				
				sb.append(getTrNormalHTML(tName, 2));
				sb.append(getTrNormalHTML(body1, 2));
				sb.append(getTrNormalHTML(body2, 1));
				sb.append(getTrNormalHTML(body3, 2));
				sb.append(getTrNormalHTML(link, 2));
				sb.append(getTrNormalHTML(body4, 1));
				sb.append(getTrFooterHTML(body5, 1));
		}catch(Exception e){
			e.printStackTrace();
		}
			return sb.toString();
		}
	 
}
