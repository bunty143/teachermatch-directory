package tm.services;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobActionFeed;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRemovedFromFeed;
import tm.bean.MessageToTeacher;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherRemovedFromFeed;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.WithdrawnReasonMaster;
import tm.bean.cgreport.PercentileZscoreTscore;
import tm.bean.cgreport.TmpPercentileWiseZScore;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.ExaminationCodeDetails;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobActionFeedDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRemovedFromFeedDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherActionFeedDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherRemovedFromFeedDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.WithdrawnReasonMasterDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.dao.cgreport.TmpPercentileWiseZScoreDAO;
import tm.dao.districtassessment.ExaminationCodeDetailsDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.service.profile.SelfServiceCandidateProfileService;
import tm.services.report.CGReportService;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

public class CommonDashboardAjax {

	String locale = Utility.getValueOfPropByKey("locale");


	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;
	
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
 
	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;

	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private WithdrawnReasonMasterDAO withdrawnReasonMasterDAO;

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;

	@Autowired
	private DistrictMasterDAO districtMasterDAO;

	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;

	@Autowired
	private DomainMasterDAO domainMasterDAO;

	@Autowired
	private TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO;

	@Autowired
	private CGReportService cGReportService;

	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;

	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;

	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;

	@Autowired
	private JobActionFeedDAO jobActionFeedDAO;

	@Autowired
	private TeacherActionFeedDAO teacherActionFeedDAO;

	@Autowired
	private TeacherRemovedFromFeedDAO teacherRemovedFromFeedDAO;

	@Autowired
	private JobRemovedFromFeedDAO jobRemovedFromFeedDAO;

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;

	@Autowired
	private EmailerService emailerService;

	@Autowired
	private ExaminationCodeDetailsDAO examinationCodeDetailsDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	
	@Autowired
	private SelfServiceCandidateProfileService selfServiceCandidateProfileService;

	private static final Logger logger = Logger.getLogger(CommonDashboardAjax.class.getName());
	List<TmpPercentileWiseZScore> tmpPercentileWiseZScoreList = null;
	List<PercentileZscoreTscore> pztList = null;

	public String getEndlessData(int from,int to,int jobFrom,int yJobFrom,DistrictMaster districtMaster,SchoolMaster schoolMaster,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		//logger.warn("This is debug message");
		int entityID=0;
		//DistrictMaster districtMaster=null;
		//SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		DistrictMaster districtMasterForSchool = null;

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			entityID=userMaster.getEntityType();
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}
			if(userMaster.getHeadQuarterMaster()!=null){
				headQuarterMaster=userMaster.getHeadQuarterMaster();
			}
			if(userMaster.getBranchMaster()!=null){
				branchMaster =userMaster.getBranchMaster();
			}
			if(entityID==2)
				schoolMaster=null;
			
			if(districtMaster!=null && entityID==1)
			{
				districtMaster = districtMasterDAO.findById(districtMaster.getDistrictId(), false, false);
			}
			
			if(districtMaster!=null && entityID==3)
			{
				districtMasterForSchool = districtMasterDAO.findById(districtMaster.getDistrictId(), false, false);
			}
			
			if(schoolMaster!=null && schoolMaster.getSchoolId()>0 && entityID==1)
			{
				schoolMaster = schoolMasterDAO.findById(schoolMaster.getSchoolId(), false, false);
			}
		}
		boolean isNormScore=true;
		if(districtMaster!=null)
		{
			if(districtMaster.getNoEPI()!=null && districtMaster.getNoEPI())
				isNormScore=false;
		}
		String showHide="";
		if(entityID==1)
			showHide="style='display:none;'";

		List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
		List<TeacherDetail> teacherDetails2 = new ArrayList<TeacherDetail>();

		int dScore = 0;
		List lstRawData = new ArrayList();
		StringBuilder sb = new StringBuilder();
		try {
			String realPath = Utility.getValueOfPropByKey("teacherRootPath");
			String webPath = Utility.getBaseURL(request);

			TeacherDetail teacherDetail	= null;
			JobOrder jobOrder1 	= null;
			//jobOrder jobOrder =null;
			List<JobOrder> jobOrderList = new ArrayList<JobOrder>();
			teacherDetails2= teacherRemovedFromFeedDAO.getRemovedTeachersByHBD(districtMaster, schoolMaster, headQuarterMaster, branchMaster);
			// For All Cases
			
			// Check For Show All Data findAllTeacherList()
			if(districtMasterForSchool!=null && districtMasterForSchool.getDisplayCandidateTOMosaic()!=null && districtMasterForSchool.getDisplayCandidateTOMosaic()==true){
				System.out.println(":::::::::::::::: TRUE ::::::::::::::::::::");
				lstRawData = teacherActionFeedDAO.findAllTeacherList(from,to,districtMaster, schoolMaster, 50, teacherDetails2,headQuarterMaster,branchMaster);
			} else {
				System.out.println("::::::::::::::: FALSE :::::::::::::::::::::");
				lstRawData = teacherActionFeedDAO.findTeacherListByNormScoreByHBD(from,to,districtMaster, schoolMaster, 50, teacherDetails2,headQuarterMaster,branchMaster);
			}
			System.out.println(":::::::: Total Record ::::::::::::"+lstRawData.size());
			int rows = lstRawData.size();
			for(Object oo: lstRawData){
				Object obj[] 	= (Object[])oo;
				teacherDetail 	= (TeacherDetail) obj[1];
				jobOrder1 		= (JobOrder) obj[6];
				teacherDetails.add(teacherDetail);
				jobOrderList.add(jobOrder1);
			}
			
			/*
			 * 	Hanzala
			 *  27-03-2015
			 */
			List<JobForTeacher>	jobForTeacherLists 		= 	new ArrayList<JobForTeacher>();
			Map<Integer, List<JobForTeacher>> jobMap 	= 	new HashMap<Integer, List<JobForTeacher>>();
			List<JobForTeacher> jobForTLiast =null;
			
			// Get Single Record From Map
			Map<Integer, JobForTeacher> jobSingleMap = new HashMap<Integer, JobForTeacher>();
			
			
			if(districtMaster!=null && userMaster!=null && userMaster.getEntityType().equals(3) && districtMaster.getDistrictId().equals(1200390)){
				try{
					if(jobOrderList.size()>0 && jobOrderList!=null)
						jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOB(jobOrderList);

					for(JobForTeacher jft : jobForTeacherLists)
					{
						if(jobMap.get(jft.getTeacherId().getTeacherId())== null)
						{
							jobForTLiast=new ArrayList<JobForTeacher>();
							jobForTLiast.add(jft);
							jobMap.put(jft.getTeacherId().getTeacherId(), jobForTLiast);
						} else {
							List<JobForTeacher> jf=jobMap.get(jft.getTeacherId().getTeacherId());
							jf.add(jft);
							jobMap.put(jft.getTeacherId().getTeacherId(), jf);
						}
						jobSingleMap.put(jft.getTeacherId().getTeacherId(), jft);
					}
				} catch(Exception exception){
					exception.printStackTrace();
				}
			}
			/*for (Map.Entry<Integer, List<JobForTeacher>> entry : jobMap.entrySet()){
				System.out.println(entry.getKey() +" ========== "+entry.getValue().size());
			}*/
			
			/* End Section */
			
			Map<Integer,Object[]> appliedmap = new HashMap<Integer, Object[]>(); 
			if(teacherDetails.size()>0)
			{
				List appliedList = jobForTeacherDAO.countApplidJobsHQLByHBD(headQuarterMaster,branchMaster,districtMaster, schoolMaster, teacherDetails);

				for(Object oo: appliedList){
					Object obj[] = (Object[])oo;
					appliedmap.put((Integer)obj[0], obj);
				}
				appliedList=null;
			}
			List<String> dataList = new ArrayList<String>();
			List<String> dataList1 = new ArrayList<String>();
			String teacherName = null;
			int i=0;
			String teacherType 	= null;
			String contacted 	= null;
			Integer jobStatus 	= null;
			
			// Find Status or Secondary Status Set By District
			String statusNameForSchoolSetup 		= "";
			String secondaryStstNameForSchoolSetup 	= "";

			if(headQuarterMaster!=null){
				if(headQuarterMaster != null && headQuarterMaster.getStatusMaster()!=null)
					statusNameForSchoolSetup			=	headQuarterMaster.getStatusMaster().getStatusShortName();
	
				if(headQuarterMaster!=null && headQuarterMaster.getSecondaryStatus()!=null)
					secondaryStstNameForSchoolSetup	=	headQuarterMaster.getSecondaryStatus().getSecondaryStatusName();
			}
			else if(districtMaster != null){
				if(districtMaster != null && districtMaster.getStatusMaster()!=null)
					statusNameForSchoolSetup			=	districtMaster.getStatusMaster().getStatusShortName();
	
				if(districtMaster!=null && districtMaster.getSecondaryStatus()!=null)
					secondaryStstNameForSchoolSetup	=	districtMaster.getSecondaryStatus().getSecondaryStatusName();
			}
			
			
			
		if(districtMaster.getDistrictId()!=1200390)
			for(Object oo: lstRawData){
				Integer hiredStatusCounter 		= 0;
				Integer rejectedStatusCounter 	= 0;
				String 	statusShortName			= "";
				Integer statusCount				= 0;
				Integer seStatusCount			= 0;		
				
				Object obj[] 	= (Object[])oo;
				teacherDetail 	= (TeacherDetail) obj[1];
				dScore 			= (Integer)obj[4];
				jobOrder1 		= (JobOrder) obj[6];
				
				List<JobForTeacher>  jobForTeacherList 	= 	jobMap.get(teacherDetail.getTeacherId());
				JobForTeacher  jobForTeacher 			= 	jobSingleMap.get(teacherDetail.getTeacherId());
				try{
					if(jobForTeacher !=null && jobForTeacher.getStatus()!=null)
						statusShortName   = 	jobForTeacher.getStatus().getStatusShortName();
					
					if(userMaster!=null && (userMaster.getEntityType().equals(3) || userMaster.getEntityType().equals(6)) && districtMaster.getDistrictId().equals(1200390)){
						if(statusShortName!=null){
							if(statusShortName.equals("hird")) {
								hiredStatusCounter = hiredStatusCounter+1;
							} else {
								if(jobForTeacherList!=null){
									for(JobForTeacher jftObj:jobForTeacherList){
										if(jftObj.getStatusMaster()!=null){
											if((statusShortName.equals("hide") || statusShortName.equals("widrw") || statusShortName.equals("rem")) && (jftObj.getStatusMaster().getStatusShortName().equals("rem") || jftObj.getStatusMaster().getStatusShortName().equals("hide") || jftObj.getStatusMaster().getStatusShortName().equals("widrw"))){
													rejectedStatusCounter = rejectedStatusCounter+1;
												} else {
													rejectedStatusCounter = 0;
												}
											}
											try{
												if((statusNameForSchoolSetup!="") || (secondaryStstNameForSchoolSetup !="")){
													if(statusNameForSchoolSetup!=null){
														if(jftObj.getStatusMaster()!=null){
															if(statusNameForSchoolSetup.equals(jftObj.getStatusMaster().getStatusShortName())){
																statusCount = 0;
															}else{
																statusCount = statusCount+1;
															}
														}
													} else if(secondaryStstNameForSchoolSetup !=null){
														if(secondaryStstNameForSchoolSetup.equals(jftObj.getSecondaryStatus().getSecondaryStatusName())){
															seStatusCount = 0;
														}else{
															seStatusCount = seStatusCount+1;
														}
													}
												}
											} catch(Exception exception){
												exception.printStackTrace();
											}
										}
									}
								}
						   }
					   } else {
						   if(jobForTeacherList!=null){
								for(JobForTeacher jftObj:jobForTeacherList){
									if(jftObj.getStatusMaster()!=null){
										try{
											if((statusNameForSchoolSetup!="") || (secondaryStstNameForSchoolSetup !="")){
												if(statusNameForSchoolSetup!=null){
													if(jftObj.getStatusMaster()!=null){
														if(statusNameForSchoolSetup.equals(jftObj.getStatusMaster().getStatusShortName())){
															statusCount = 0;
														}else{
															statusCount = statusCount+1;
														}
													}
												} else if(secondaryStstNameForSchoolSetup !=null){
													if(secondaryStstNameForSchoolSetup.equals(jftObj.getSecondaryStatus().getSecondaryStatusName())){
														seStatusCount = 0;
													}else{
														seStatusCount = seStatusCount+1;
													}
												}
											}
										} catch(Exception exception){
											exception.printStackTrace();
										}
									}
								}
							}
					   }
				} catch(Exception exception){
					exception.printStackTrace();
				}
				
				if((rejectedStatusCounter.equals(0) || hiredStatusCounter.equals(0)) && (statusCount.equals(0) && seStatusCount.equals(0))){
					i++;
					teacherName = teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
					if(obj[2]!=null && (Boolean)obj[2])
						teacherType=Utility.getLocaleValuePropByKey("lblInternal1", locale);
					else
						teacherType=Utility.getLocaleValuePropByKey("lblVeteran", locale);

					if(obj[5]==null)
						contacted = Utility.getLocaleValuePropByKey("btnNo", locale);
					else
						contacted=Utility.getLocaleValuePropByKey("btnYes", locale);

					sb.append("<div class='dcard table-bordered1' id='profilebox'>");
					sb.append("<table width='100%' cellspacing=0 cellpadding=0 class='tablecgtbl' border=0 style='width: 285px;border: solid red 0px;padding:0px;font-size:10px;' >");
					
					sb.append("<tr>");
					String ipath = "";
					/*if(teacherDetail.getIdentityVerificationPicture()!=null)
						ipath=CommonDashboardAjax.showPictureWeb(teacherDetail, realPath, webPath);
					if(ipath.equals(""))
						ipath="images/nophoto.png";*/

					ipath="images/nophoto.png";

					sb.append("<td width='20%' rowspan=3 style='padding:0px;'><img src='"+ipath+"' class='circle'/></td>");
					//Start new code
					//************************** Adding by deepak *****************************************
					Map<Integer,String>  portfolioIdMap=new HashMap<Integer, String>();
					List<TeacherDetail> lstTeacherDetail=new ArrayList<TeacherDetail>();
					lstTeacherDetail.add(teacherDetail);
					boolean portfolioFlag=selfServiceCandidateProfileService.checkPortfolioAvaliablity(lstTeacherDetail,userMaster,districtMaster,portfolioIdMap);
					//**************************************************************************************
					//String sServerDomainName=request.getServerName();
					boolean bSelfService=false;
					if(portfolioFlag && portfolioIdMap.get(teacherDetail.getTeacherId())!=null)
					    {
					     bSelfService=true;
					    }
						if(bSelfService){
						sb.append("<td width='74%' class='titlecolor' colspan=2 style='padding-bottom:2px;font-size:11px;'><span style='float:right;' class='divlable1'><!-- "+teacherType+" --></span><br><span class='tool' style=\"cursor: pointer;\"><b><a class='profile' data-placement='above' href='javascript:void(0);' onclick=\"showProfileContentNew(this,0,"+teacherDetail.getTeacherId()+",'0','0','','0',event,'"+teacherName.trim()+"','"+portfolioIdMap.get(teacherDetail.getTeacherId())+"','Mosaic')\"   data-original-title=' "+teacherName.trim()+" ' rel='tooltip'>"+teacherName.trim()+"</a></b></span></td>");
					}else{
						sb.append("<td width='74%' class='titlecolor' colspan=2 style='padding-bottom:2px;font-size:11px;'><span style='float:right;' class='divlable1'><!-- "+teacherType+" --></span><br><span class='tool' style=\"cursor: pointer;\"><b><a class='profile' data-placement='above' href='javascript:void(0);' onclick=\"showProfileContentForTeacher(this,"+teacherDetail.getTeacherId()+",0,'Mosaic',event)\">"+teacherName.trim()+"</a></b></span></td>");	
					}
					//sb.append("<td width='74%' class='titlecolor' colspan=2 style='padding-bottom:2px;font-size:11px;'><span style='float:right;' class='divlable1'><!-- "+teacherType+" --></span><br><span class='tool' style=\"cursor: pointer;\"><b><a class='profile' data-placement='above' href='javascript:void(0);' onclick=\"showProfileContentForTeacher(this,"+teacherDetail.getTeacherId()+",0,'Mosaic',event)\">"+teacherName.trim()+"</a></b></span></td>");				
					//End new ceode

					//sb.append("<td width='70%' class='titlecolor' colspan=2 style='padding-top:1px;font-size:11px;'><b class='profile' >"+teacherName.trim()+"</b></td>");

					sb.append("<td width='6%' style='text-align:right;'>");
					sb.append("<a style='padding-right: 3px;padding-top:0px;display:none;' id='lyrics'  onclick=\"divClose(this,"+teacherDetail.getTeacherId()+",0);\" href='javascript:void(0);' > <i class='icon-remove' "+showHide+" ></i> </a>");
					sb.append("</td>");
					sb.append("</tr>");
					if(isNormScore)
						sb.append("<tr><td colspan=3><span class='divlable1'>"+Utility.getLocaleValuePropByKey("lblEPINorm", locale)+":</span>&nbsp;<span class='divlableval1'>"+Math.round(dScore)+"</span>&nbsp;&nbsp;<span class='divlable1'>"+Utility.getLocaleValuePropByKey("lblContacted", locale)+":</span>&nbsp;<span class='divlableval1'>"+contacted+"</span></td></tr>");
					else
						sb.append("<tr><td colspan=3><span class='divlable1'>"+Utility.getLocaleValuePropByKey("lblContacted", locale)+":</span>&nbsp;<span class='divlableval1'>"+contacted+"</span></td></tr>");

					Object obj1[] =appliedmap.get(teacherDetail.getTeacherId());

					sb.append("<tr><td colspan=3><span class='divlable1'>"+Utility.getLocaleValuePropByKey("msgFirstAppliedon", locale)+":</span>&nbsp;<span class='divlableval1'>"+Utility.convertDateAndTimeToUSformatOnlyDate(((Date)obj1[1]))+"</span> &nbsp;<span class='divlable1'>");
					if(entityID==1)
						sb.append("Jobs:</span>&nbsp;<span class='divlableval1'>"+obj1[2]+"/0</span></td></tr>");
					else
						sb.append("Jobs:</span>&nbsp;<span class='divlableval1'>"+obj1[3]+"/"+(Integer.parseInt(String.valueOf(obj1[2]))-Integer.parseInt(String.valueOf(obj1[3])))+"</span></td></tr>");

					/*if(entityID==1)
						sb.append("Jobs:</span>&nbsp;<span class='divlableval1'><a href='javascript:void(0);' onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" >"+obj1[2]+"/0</a></span></td></tr>");
					else
						sb.append("Jobs:</span>&nbsp;<span class='divlableval1'><a href='javascript:void(0);' onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" >"+(Integer.parseInt(String.valueOf(obj1[2]))-Integer.parseInt(String.valueOf(obj1[3])))+"/"+obj1[3]+"</a></span></td></tr>");*/

					sb.append("</table>");
					sb.append("</div>");
					dataList.add(sb.toString());
					sb.setLength(0);
				} 
			}

			List<JobOrder> jobOrders = new ArrayList<JobOrder>();
			jobOrders = jobRemovedFromFeedDAO.getRemovedJobsByHBD(headQuarterMaster,branchMaster,districtMaster, schoolMaster);
			StringBuilder sb2 = new StringBuilder();
			String jobTitle = null;
			int j=0;
			String jobImg = "";
			//int type=Utility.getRandom(0,2);
			int job2show = 1;

			if(rows<to)
				job2show=(to-rows);

			List lstJobData = null;
			List lstJobData1 = null;
			List lstJobDataAll = new ArrayList();
			
			lstJobData = jobActionFeedDAO.findJobListByLimitByHBD(jobFrom, job2show,districtMaster,schoolMaster,0,jobOrders,headQuarterMaster,branchMaster);
			lstJobData1 = jobActionFeedDAO.findJobListByLimitByHBD(yJobFrom, job2show,districtMaster,schoolMaster,1,jobOrders,headQuarterMaster,branchMaster);

			JobOrder jobOrder = null;
			int jobListSize = lstJobData.size();
			jobListSize += lstJobData1.size();
			Map<Integer,String> map = new HashMap<Integer, String>();
			if(jobListSize>0)
			{
				lstJobDataAll.addAll(lstJobData);
				lstJobDataAll.addAll(lstJobData1);
				List<JobOrder> jobs = new ArrayList<JobOrder>();
				for(Object oo: lstJobDataAll){
					Object obj[] = (Object[])oo;
					jobs.add((JobOrder) obj[0]);
				}
				map = jobForTeacherDAO.countApplicantsByJobOrdersHQL(jobs,"6","8");
			}
			String appliedStr = null;
			if(districtMaster.getDistrictId()!=1200390)
			for(Object oo: lstJobData){
				Object obj[] = (Object[])oo;
				jobOrder = (JobOrder) obj[0];
				sb2.append("<div class='dcard table-bordered1' id='profilebox'>");
				sb2.append("<table width='100%' border=0 cellspacing=0 cellpadding=0 class='tablecgtbl' border=0 style='width: 285px;border: solid red 0px;font-size:10px;' >");
				sb2.append("<tr>");
				appliedStr = map.get(jobOrder.getJobId());
				jobImg="JobOrder-pink.png";

				sb2.append("<td width='20%' rowspan=4 style='padding:0px;'><img src='images/"+jobImg+"' class='circle'/></td>");
				jobTitle = jobOrder.getJobTitle().trim();
				if(jobTitle.length()<=30)
					sb2.append("<td width='70%' class='titlecolor' colspan=2 style='padding-bottom:2px;font-size:11px;'><span>&nbsp;</span><br><b><a target='blank' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+jobOrder.getCreatedForEntity()+"'>"+jobTitle.trim()+"</a></b></td>");
				else
					sb2.append("<td width='70%' class='titlecolor' colspan=2 style='padding-bottom:2px;font-size:11px;'><span>&nbsp;</span><span class='tool' style=\"cursor: pointer;\" title=\""+jobTitle+"\"><br><b><a target='blank' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+jobOrder.getCreatedForEntity()+"'>"+Utility.LimitCharacter(jobTitle, 30, "...")+"</a></b></span></td>");
				sb2.append("<td width='10%' style='text-align:right;'>");
				sb2.append("<a style='padding-right: 3px;display:none;' id='lyrics'  onclick=\"divClose(this,"+jobOrder.getJobId()+",1);\" href='javascript:void(0);' ><i class='icon-remove' "+showHide+"></i></a>");
				sb2.append("</td>");
				sb2.append("</tr>");
				sb2.append("<tr><td colspan=3><span class='divlable1'>"+Utility.getLocaleValuePropByKey("lblPostedon", locale)+":</span>&nbsp;<span class='divlableval1'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobStartDate())+"</span></td></tr>");
				//sb2.append("<tr><td colspan=3><span class='divlable1'>Expected Hires:</span>&nbsp;<span class='divlableval1'>"+obj[2]+"</span> &nbsp;<span class='divlable1'>"+Utility.getLocaleValuePropByKey("msgApplicantsHires", locale)+":</span>&nbsp;<span class='divlableval1'>"+obj[3]+"/"+obj[4]+"</span></td></tr>");
				if(appliedStr!=null)
					sb2.append("<tr><td colspan=3><span class='divlable1'>"+Utility.getLocaleValuePropByKey("lblExpectedHires", locale)+":</span>&nbsp;<span class='divlableval1'>"+obj[2]+"</span> &nbsp;<span class='divlable1'>"+Utility.getLocaleValuePropByKey("msgApplicantsHires", locale)+":</span>&nbsp;<span class='divlableval1'>"+appliedStr.split("##")[0]+"/"+appliedStr.split("##")[1]+"</span></td></tr>");
				else
					sb2.append("<tr><td colspan=3><span class='divlable1'>"+Utility.getLocaleValuePropByKey("lblExpectedHires", locale)+":</span>&nbsp;<span class='divlableval1'>"+obj[2]+"</span> &nbsp;<span class='divlable1'>"+Utility.getLocaleValuePropByKey("msgApplicantsHires", locale)+":</span>&nbsp;<span class='divlableval1'>0/0</span></td></tr>");
				sb2.append("</table>");
				sb2.append("</div>");
				dataList1.add(sb2.toString());
				sb2.setLength(0);
				j++;
			}
			StringBuilder sb3 = new StringBuilder();
			int k=0;
			if(districtMaster.getDistrictId()!=1200390)
			for(Object oo: lstJobData1){
				Object obj[] = (Object[])oo;
				jobOrder = (JobOrder) obj[0];
				sb3.append("<div class='dcard table-bordered1' id='profilebox'>");
				sb3.append("<table width='100%' border=0 cellspacing=0 cellpadding=0 class='tablecgtbl' border=0 style='width: 285px;border: solid red 0px;font-size:10px;' >");
				sb3.append("<tr>");
				appliedStr = map.get(jobOrder.getJobId());
				jobImg="JobOrder-yellow.png";

				sb3.append("<td width='20%' rowspan=4 style='padding:0px;'><img src='images/"+jobImg+"' class='circle'/></td>");
				jobTitle = jobOrder.getJobTitle().trim();
				if(jobTitle.length()<=30)
					sb3.append("<td width='70%' class='titlecolor' colspan=2 style='padding-bottom:2px;font-size:11px;'><span>&nbsp;</span><br><b><a target='blank' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+jobOrder.getCreatedForEntity()+"'>"+jobTitle.trim()+"</a></b></td>");
				else
					sb3.append("<td width='70%' class='titlecolor' colspan=2 style='padding-bottom:2px;font-size:11px;'><span>&nbsp;</span><span class='tool' style=\"cursor: pointer;\" title=\""+jobTitle+"\"><br><b><a target='blank' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+jobOrder.getCreatedForEntity()+"'>"+Utility.LimitCharacter(jobTitle, 30, "...")+"</a></b></span></td>");
				sb3.append("<td width='10%' style='text-align:right;'>");
				sb3.append("<a style='padding-right: 3px;display:none;' id='lyrics'  onclick=\"divClose(this,"+jobOrder.getJobId()+",1);\" href='javascript:void(0);' ><i class='icon-remove' "+showHide+"></i></a>");
				sb3.append("</td>");
				sb3.append("</tr>");
				sb3.append("<tr><td colspan=3><span class='divlable1'>"+Utility.getLocaleValuePropByKey("lblPostedon", locale)+":</span>&nbsp;<span class='divlableval1'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobStartDate())+"</span></td></tr>");
				//sb3.append("<tr><td colspan=3><span class='divlable1'>"+Utility.getLocaleValuePropByKey("lblExpectedHires", locale)+":</span>&nbsp;<span class='divlableval1'>"+obj[2]+" </span>&nbsp;<span class='divlable1'>"+Utility.getLocaleValuePropByKey("msgApplicantsHires", locale)+":</span>&nbsp;<span class='divlableval1'>"+obj[3]+"/"+obj[4]+"</span></td></tr>");
				if(appliedStr!=null)
					sb3.append("<tr><td colspan=3><span class='divlable1'>"+Utility.getLocaleValuePropByKey("lblExpectedHires", locale)+":</span>&nbsp;<span class='divlableval1'>"+obj[2]+" </span>&nbsp;<span class='divlable1'>"+Utility.getLocaleValuePropByKey("msgApplicantsHires", locale)+":</span>&nbsp;<span class='divlableval1'>"+appliedStr.split("##")[0]+"/"+appliedStr.split("##")[1]+"</span></td></tr>");
				else
					sb3.append("<tr><td colspan=3><span class='divlable1'>"+Utility.getLocaleValuePropByKey("lblExpectedHires", locale)+":</span>&nbsp;<span class='divlableval1'>"+obj[2]+" </span>&nbsp;<span class='divlable1'>"+Utility.getLocaleValuePropByKey("msgApplicantsHires", locale)+":</span>&nbsp;<span class='divlableval1'>0/0</span></td></tr>");
				sb3.append("</table>");
				sb3.append("</div>");

				dataList1.add(sb3.toString());
				sb3.setLength(0);
				k++;
			}
			if(jobListSize>0)
			{
				int teacherDataSize = dataList.size();
				if(teacherDataSize>0)
				{
					for (String datalst : dataList1) {
						int jr1=Utility.getRandom(0,teacherDataSize);
						dataList.add(jr1,datalst);
					}
				}else
				{
					dataList.addAll(dataList1);
				}

			}
			for (String string : dataList) {
				sb.append(string);
			}

			int nextRed=0;
			int nextYellow=0;

			nextRed=j+jobFrom;
			nextYellow=k+yJobFrom;

			String nodata = Utility.getLocaleValuePropByKey("msgEndfeeddata", locale);
			if(from==0 && jobFrom==0 && yJobFrom==0 && jobListSize==0 && rows==0)
				nodata =  Utility.getLocaleValuePropByKey("msgNodataavailable", locale);

			if(districtMaster.getDistrictId()==1200390){
				sb.append("<div class='dcard table-bordered1' id='profilebox' style='text-align:center;height:65px;'><div style='padding-top:20px;'><span class='titlecolor'>"+nodata+"</span></div></div>");
				sb.append("@###@"+(i+from)+"@###@"+(nextRed)+"@###@"+(nextYellow)+"@###@"+0);
			} else {
				if(jobListSize==0 && rows==0)
				{
					sb.append("<div class='dcard table-bordered1' id='profilebox' style='text-align:center;height:65px;'><div style='padding-top:20px;'><span class='titlecolor'>"+nodata+"</span></div></div>");
					sb.append("@###@"+(i+from)+"@###@"+(nextRed)+"@###@"+(nextYellow)+"@###@"+0);
				}
				else
					sb.append("@###@"+(i+from)+"@###@"+(nextRed)+"@###@"+(nextYellow)+"@###@"+1);
			}
			
			sb2=null;
			sb3=null;
			lstJobData=null;
			lstJobData1=null;
			lstRawData=null;
			dataList=null;
			appliedmap=null;
			map = null;
			lstJobDataAll = null;
			teacherDetails=null;
			teacherDetails2=null;
			jobOrder=null;
			teacherDetail=null;
			//startExamReminder();
			//startReminder();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public static String  showPictureWeb(TeacherDetail teacherDetail,String realPath,String webPath)
	{
		String path="";
		String source="";
		String target="";
		try 
		{

			String docFileName=teacherDetail.getIdentityVerificationPicture();
			source = Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+docFileName;

			target = realPath+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			path = webPath+"/teacher/"+teacherDetail.getTeacherId()+"/"+docFileName;

		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
		}

		return path;

	}

	public String  showCandidateDetails(Integer teacherId,int normScore)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuilder sb = new StringBuilder();

		String realPath = Utility.getValueOfPropByKey("teacherRootPath");
		String webPath = Utility.getBaseURL(request);
		sb.append("<div>");

		String imgPath="";
		TeacherDetail teacherDetail = null;
		UserMaster userMaster = null;
		try{
			//teacherDetail = new TeacherDetail();
			teacherDetail = teacherDetailDAO.findById(teacherId,false,false);
			userMaster=(UserMaster)session.getAttribute("userMaster");
			/*if(teacherDetail.getIdentityVerificationPicture()!=null){
				imgPath=CommonDashboardAjax.showPictureWeb(teacherDetail, realPath, webPath);
			}*/
		}catch(Exception e){
			e.printStackTrace();
		}
		List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
		teacherDetails.add(teacherDetail);

		String showImg="";
		/*if(!imgPath.equals("")){
			showImg="<img src=\""+imgPath+"\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;'>";
		}else{
			showImg="<img src=\"images/nophoto.png\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;' >";
		}*/

		showImg="<img src=\"images/nophoto.png\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;' >";

		List<JobForTeacher>  jobForTeachers = jobForTeacherDAO.findJobByTeacher(teacherDetail);
		JobForTeacher jobForTeacher = null;
		String firstAppliedOn="";
		String lastAppliedOn="";
		Date lastActivityDate=null;
		String lastContactedOn="None";
		if(jobForTeachers.size()>0)
		{
			jobForTeacher = jobForTeachers.get(0);
			firstAppliedOn = Utility.convertDateAndTimeToUSformatOnlyDate(jobForTeacher.getCreatedDateTime());
			jobForTeacher = jobForTeachers.get(jobForTeachers.size()-1);
			lastAppliedOn = Utility.convertDateAndTimeToUSformatOnlyDate(jobForTeacher.getCreatedDateTime());

			Collections.reverse(jobForTeachers);
			for (JobForTeacher jobForTeacher2 : jobForTeachers) {
				lastActivityDate = jobForTeacher2.getLastActivityDate();
				if(lastActivityDate!=null)
				{
					//lastContactedOn = Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")";
					lastContactedOn ="<a href='javascript:void(0);'  onclick=\"getCommunicationsDiv(1,0,'"+teacherDetail.getTeacherId()+"',0);\" ><span class='divlablelinkval'>"+Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")</span></a>"; 
					break;
				}
			}
		} 

		sb.append("<table width=400 border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
		sb.append("<tr><td width=42 rowspan=5 style='vertical-algin:top;text-align:left;padding:0px;padding-left:5px;padding-right:5px;padding-top:5px;'>"+showImg+"</td><td  width=60  class='divlable' style='padding:0px;padding-top:5px;'>&nbsp;Name:</td><td class='divlableval' width=240 style='padding:0px;padding-top:5px;'>&nbsp;"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"</td></tr>");
		sb.append("<tr><td class='divlable' style='padding:0px;'>&nbsp;EPI Norm:</td><td  class='divlableval' style='padding:0px;'>&nbsp;"+normScore+"</td></tr>");
		sb.append("<tr><td class='divlable' style='padding:0px;'>&nbsp;First Job Applied On:</td><td  class='divlableval' style='padding:0px;'>&nbsp;"+firstAppliedOn+"</td></tr>");
		sb.append("<tr><td class='divlable' style='padding:0px;'>&nbsp;Last Job Applied On:</td><td  class='divlableval' style='padding:0px;'>&nbsp;"+lastAppliedOn+"</td></tr>");
		sb.append("<tr><td  class='divlable' style='padding:0px;'>&nbsp;Last Contacted On:</td><td  class='divlableval' style='padding:0px;'>");
		sb.append("&nbsp;"+lastContactedOn);
		sb.append("</td></tr>");
		//sb.append("<tr><td colspan=2 style='padding:0px;'>&nbsp;</td></tr>");
		sb.append("<tr><td colspan=3 style='padding:0px;'><table>");
		sb.append("<tr><th width=180 class='divlable' style='padding:0px;padding-left:5px;padding-top:10px;'>Education</th><th width=170 class='divlable' style='padding:0px;padding-left:5px;padding-top:10px;'>Major</th><th width=20 class='divlable' style='padding:0px;padding-left:2px;padding-top:10px;'>CGPA</th><th class='divlable' style='padding:0px;padding-left:7px;padding-top:10px;' width=30 >"+Utility.getLocaleValuePropByKey("optYear", locale)+"</th></tr>");
		//sb.append("<tr><td colspan=4  class='divlable' style='padding:0px;padding-left:5px;padding-top:10px;'>Education:</td></tr>");
		List<TeacherAcademics>  lstTeacherAcademics = teacherAcademicsDAO.findAcademicsByTeacher(teacherDetails);

		for(TeacherAcademics teacherAcademics :lstTeacherAcademics){
			String degree="";
			String universityName="";
			String fieldName="";
			if(teacherAcademics.getDegreeId()!=null){
				degree=teacherAcademics.getDegreeId().getDegreeShortName();
			}
			if(teacherAcademics.getUniversityId()!=null){
				universityName=teacherAcademics.getUniversityId().getUniversityName();
			}
			if(teacherAcademics.getFieldId().getFieldName()!=null){
				fieldName=teacherAcademics.getFieldId().getFieldName();	
			}
			sb.append("<tr><td width=180  class='divlableval' style='padding:0px;padding-left:5px;'>"+degree+", "+universityName+" </td><td width=170  class='divlableval' style='padding:0px;padding-left:4px;'>"+fieldName+"</td><td width=20  class='divlableval' style='padding:0px;padding-left:4px;'>"+(teacherAcademics.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(teacherAcademics.getGpaCumulative()))+"</td><td width=30  class='divlableval' style='padding:0px;padding-left:8px;'>"+teacherAcademics.getLeftInYear()+"</td></tr>");
		}
		if(lstTeacherAcademics.size()==0){
			sb.append("<tr><td width=400 colspan=4   class='divlableval' style='padding:0px;padding-left:5px;'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td>"); 
		}

		sb.append("</table></td></tr>");
		sb.append("<tr><td colspan=3><table border=0>");
		sb.append("<tr><td  class='divlable' style='padding:0px;padding-left:3px;padding-top:10px;'>"+Utility.getLocaleValuePropByKey("lblCertifications", locale)+":</td></tr>");
		List<TeacherCertificate> lstTeacherCertificate = teacherCertificateDAO.findCertificateByTeacher(teacherDetails);
		for(TeacherCertificate teacherCertificate :lstTeacherCertificate){
			String cityType="",stateName="";
			if(teacherCertificate.getCertificateTypeMaster()!=null){
				cityType=teacherCertificate.getCertificateTypeMaster().getCertType();
			}
			if(teacherCertificate.getStateMaster()!=null){
				stateName=teacherCertificate.getStateMaster().getStateName();
			}

			sb.append("<tr><td  class='divlableval' style='padding:0px;padding-left:3px;'>"+cityType+", "+stateName+" </td></tr>");
		}
		if(lstTeacherCertificate.size()==0){
			sb.append("<tr><td class='divlableval' style='padding:0px;padding-left:3px;'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td>"); 
		}
		sb.append("</table></td></tr>");

		sb.append("<tr><td colspan=3 width='100%' style='padding-top:5px;'><table border=0 width='100%'><tr bgcolor='#F2FAEF'><td style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(1,0,'"+teacherDetail.getTeacherId()+"',0);\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+Utility.getLocaleValuePropByKey("headCom", locale)+"</a></td>");

		if(userMaster.getEntityType()!=1){
			sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' onclick=\"saveToFolderJFTNULL(0,'"+teacherDetail.getTeacherId()+"',0)\"><span class='icon-save icon-large iconcolor'></span>&nbsp;"+Utility.getLocaleValuePropByKey("btnSave", locale)+"</a></td>" +
					"<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);'  onclick=\"displayUsergrid('"+teacherDetail.getTeacherId()+"',1)\"><span class='icon-share icon-large iconcolor'></span>&nbsp;"+Utility.getLocaleValuePropByKey("btnShare", locale)+"</a></td>");
		}else{
			sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-save icon-large iconcolorhover'></span>&nbsp;"+Utility.getLocaleValuePropByKey("btnSave", locale)+"</td>" +
			"<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-share icon-large iconcolorhover'></span>&nbsp;"+Utility.getLocaleValuePropByKey("btnShare", locale)+"</td>");
		}
		sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+Utility.getLocaleValuePropByKey("headTag", locale)+"</td>");

		sb.append("</tr>");
		sb.append("</table></td></tr></table>");
		sb.append("</div>");
		return sb.toString();

	}


	public String  UpdateRecords()
	{

		UpdateMosaicThread updateMosaicThread = new UpdateMosaicThread();
		updateMosaicThread.setcGReportService(cGReportService);
		updateMosaicThread.setDomainMasterDAO(domainMasterDAO);
		updateMosaicThread.setJobActionFeedDAO(jobActionFeedDAO);
		updateMosaicThread.setTeacherActionFeedDAO(teacherActionFeedDAO);
		updateMosaicThread.setTeacherNormScoreDAO(teacherNormScoreDAO);
		updateMosaicThread.setPztList(pztList);
		updateMosaicThread.setRawDataForDomainDAO(rawDataForDomainDAO);
		updateMosaicThread.setTmpPercentileWiseZScoreDAO(tmpPercentileWiseZScoreDAO);
		updateMosaicThread.setTmpPercentileWiseZScoreList(tmpPercentileWiseZScoreList);
		updateMosaicThread.setJobForTeacherDAO(jobForTeacherDAO);
		updateMosaicThread.setJobOrderDAO(jobOrderDAO);
		updateMosaicThread.setStatusMasterDAO(statusMasterDAO);

		updateMosaicThread.start();

		return "Done";

	}

	public String removeFromFeed(Integer removedEntiyId,int flg)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			int entityID = 0;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			UserMaster userMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}
				if(userMaster.getSchoolId()!=null){
					schoolMaster =userMaster.getSchoolId();
				}	
				entityID=userMaster.getEntityType();
			}

			if(flg==0)
			{
				TeacherRemovedFromFeed teacherRemovedFromFeed = new TeacherRemovedFromFeed();
				TeacherDetail teacherDetail = new TeacherDetail();
				teacherDetail.setTeacherId(removedEntiyId);
				teacherRemovedFromFeed.setTeacherDetail(teacherDetail);
				teacherRemovedFromFeed.setDeletedDateTime(new Date());
				teacherRemovedFromFeed.setDistrictMaster(districtMaster);
				teacherRemovedFromFeed.setSchoolMaster(schoolMaster);
				teacherRemovedFromFeed.setUserMaster(userMaster);
				teacherRemovedFromFeed.setUserEntityTypeId(entityID);
				teacherRemovedFromFeedDAO.makePersistent(teacherRemovedFromFeed);

			}else
			{
				JobRemovedFromFeed jobRemovedFromFeed = new JobRemovedFromFeed();
				JobOrder jobOrder = new JobOrder();
				jobOrder.setJobId(removedEntiyId);
				jobRemovedFromFeed.setJobOrder(jobOrder);
				jobRemovedFromFeed.setDeletedDateTime(new Date());
				jobRemovedFromFeed.setDistrictMaster(districtMaster);
				jobRemovedFromFeed.setSchoolMaster(schoolMaster);
				jobRemovedFromFeed.setUserMaster(userMaster);
				jobRemovedFromFeed.setUserEntityTypeId(entityID);
				jobRemovedFromFeedDAO.makePersistent(jobRemovedFromFeed);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public JSONObject getCandidateStats(DistrictMaster districtMaster,SchoolMaster schoolMaster,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		int entityID=0;

		String locale = Utility.getValueOfPropByKey("locale");
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		JSONObject jsonOutput = new JSONObject();
		UserMaster userMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			entityID=userMaster.getEntityType();
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}
			if(userMaster.getHeadQuarterMaster()!=null){
				headQuarterMaster=userMaster.getHeadQuarterMaster();
			}
			if(userMaster.getBranchMaster()!=null){
				branchMaster =userMaster.getBranchMaster();
			}
			if(entityID==2)
				schoolMaster = null;
			if(districtMaster!=null && entityID==1)
			{
				districtMaster = districtMasterDAO.findById(districtMaster.getDistrictId(), false, false);
			}
			if(schoolMaster!=null && schoolMaster.getSchoolId()>0 && entityID==1)
			{
				schoolMaster = schoolMasterDAO.findById(schoolMaster.getSchoolId(), false, false);
			}	

			JSONArray categories = new JSONArray();
			JSONArray dataArray = new JSONArray();
			JSONArray dataArray1 = new JSONArray();
			if(userMaster.getEntityType()!=6){

				List teachers = teacherPersonalInfoDAO.getHiredCandidatesIntHQL(districtMaster, schoolMaster);
				List data1 = new ArrayList();
				if(teachers.size()>0)
					data1 = teacherPersonalInfoDAO.countHiredCandidatesIntHQL(teachers);
	
				for(Object oo: data1){
					Object obj[] = (Object[])oo;
					categories.add(obj[2].toString());
					dataArray1.add(obj[1]);
					dataArray.add(Integer.parseInt(String.valueOf(obj[0]))-Integer.parseInt(String.valueOf(obj[1])));
				}
				if(data1.size()==0)
				{
	
					categories.add("AL");
					categories.add("ID");
					categories.add("DC");
					categories.add("IL");
					categories.add("IN");
					categories.add("MI");
					categories.add("MO");
					categories.add("NH");
					categories.add("NY");
					categories.add("VA");
	
	
					dataArray.add(10);
					dataArray.add(21);
					dataArray.add(25);
					dataArray.add(31);
					dataArray.add(37);
					dataArray.add(41);
					dataArray.add(43);
					dataArray.add(52);
					dataArray.add(59);
					dataArray.add(67);
	
					dataArray1.add(5);
					dataArray1.add(10);
					dataArray1.add(12);
					dataArray1.add(13);
					dataArray1.add(15);
					dataArray1.add(18);
					dataArray1.add(21);
					dataArray1.add(29);
					dataArray1.add(36);
					dataArray1.add(41);
				}
	
				JSONObject chart = new JSONObject();
				chart.put("type", "bar");
				chart.put("renderTo", "container4");
				chart.put("events", new JSONObject());
				jsonOutput.put("chart", chart);
				JSONObject exporting = new JSONObject();
				exporting.put("enabled", false);
				jsonOutput.put("exporting", exporting);
	
				JSONObject title = new JSONObject();
				
				String basepath = Utility.getValueOfPropByKey("basePath");
				if(basepath.equalsIgnoreCase("https://canada-en-test.teachermatch.org/")){
					title.put("text","Hired Candidate Distribution By Province");
				}else{
					title.put("text", Utility.getLocaleValuePropByKey("lblCandidateStats", locale));
				}
				title.put("align","left");
				JSONObject style = new JSONObject();
				style.put("color", "#007AB4");
				style.put("fontSize", "10px");
				style.put("fontfamily", "Century Gothic");
				title.put("style", style);
				jsonOutput.put("title", title);
	
				JSONObject xAxis = new JSONObject();
	
				xAxis.put("categories", categories);
	
				JSONObject labels = new JSONObject();
				labels.put("style", style);
				xAxis.put("labels", labels);
				jsonOutput.put("xAxis", xAxis);
				JSONObject yAxis = new JSONObject();
				yAxis.put("min", 0);
				title.put("text", "");
				yAxis.put("title", title);
				yAxis.put("labels", labels);
				yAxis.put("gridLineWidth", 0);
				jsonOutput.put("yAxis", yAxis);
	
				JSONObject legend = new JSONObject();
				legend.put("backgroundColor", "#FFFFFF");
				legend.put("itemStyle", style);
				//legend.put("enabled", false);
				legend.put("reversed", true);
				jsonOutput.put("legend", legend);
				JSONObject plotOptions = new JSONObject();
				JSONObject series = new JSONObject();
				series.put("stacking", "normal");
				plotOptions.put("series", series);
				jsonOutput.put("plotOptions", plotOptions);
				JSONArray seriesArray = new JSONArray();
	
	
				JSONObject seriesData = new JSONObject();
				seriesData.put("name", Utility.getLocaleValuePropByKey("lblOtherCandidates", locale));
				seriesData.put("color", "#A52A2A");
	
				seriesData.put("data", dataArray);
				seriesArray.add(seriesData);
	
				seriesData.put("name", Utility.getLocaleValuePropByKey("lblTopCandidates", locale));
				seriesData.put("color", "#8bbc21");
	
				seriesData.put("data", dataArray1);
				seriesArray.add(seriesData);
	
				jsonOutput.put("series", seriesArray);
	
				JSONObject credits = new JSONObject();
				credits.put("enabled", false);
				jsonOutput.put("credits", credits);
				/*
				 * tooltip: {
		                valueSuffix: ' candidates',
		                style: tmstyle
		            }
				 */
				JSONObject tooltip = new JSONObject();
				tooltip.put("style", style);
				jsonOutput.put("tooltip", tooltip);
	
				data1 = null;
				teachers = null;
			}
			else{
				categories.add("AL");
				categories.add("ID");
				categories.add("DC");
				categories.add("IL");
				categories.add("IN");
				categories.add("MI");
				categories.add("MO");
				categories.add("NH");
				categories.add("NY");
				categories.add("VA");


				dataArray.add(10);
				dataArray.add(21);
				dataArray.add(25);
				dataArray.add(31);
				dataArray.add(37);
				dataArray.add(41);
				dataArray.add(43);
				dataArray.add(52);
				dataArray.add(59);
				dataArray.add(67);

				dataArray1.add(5);
				dataArray1.add(10);
				dataArray1.add(12);
				dataArray1.add(13);
				dataArray1.add(15);
				dataArray1.add(18);
				dataArray1.add(21);
				dataArray1.add(29);
				dataArray1.add(36);
				dataArray1.add(41);
				
				JSONObject chart = new JSONObject();
				chart.put("type", "bar");
				chart.put("renderTo", "container4");
				chart.put("events", new JSONObject());
				jsonOutput.put("chart", chart);
				JSONObject exporting = new JSONObject();
				exporting.put("enabled", false);
				jsonOutput.put("exporting", exporting);
	
				JSONObject title = new JSONObject();
				String basepath = Utility.getValueOfPropByKey("basePath");
				if(basepath.equalsIgnoreCase("https://canada-en-test.teachermatch.org/")){
					title.put("text","Hired Candidate Distribution By Province");
				}else{
					title.put("text", Utility.getLocaleValuePropByKey("lblCandidateStats", locale));
				}
				title.put("align","left");
				JSONObject style = new JSONObject();
				style.put("color", "#007AB4");
				style.put("fontSize", "10px");
				style.put("fontfamily", "Century Gothic");
				title.put("style", style);
				jsonOutput.put("title", title);
	
				JSONObject xAxis = new JSONObject();
	
				xAxis.put("categories", categories);
	
				JSONObject labels = new JSONObject();
				labels.put("style", style);
				xAxis.put("labels", labels);
				jsonOutput.put("xAxis", xAxis);
				JSONObject yAxis = new JSONObject();
				yAxis.put("min", 0);
				title.put("text", "");
				yAxis.put("title", title);
				yAxis.put("labels", labels);
				yAxis.put("gridLineWidth", 0);
				jsonOutput.put("yAxis", yAxis);
	
				JSONObject legend = new JSONObject();
				legend.put("backgroundColor", "#FFFFFF");
				legend.put("itemStyle", style);
				//legend.put("enabled", false);
				legend.put("reversed", true);
				jsonOutput.put("legend", legend);
				JSONObject plotOptions = new JSONObject();
				JSONObject series = new JSONObject();
				series.put("stacking", "normal");
				plotOptions.put("series", series);
				jsonOutput.put("plotOptions", plotOptions);
				JSONArray seriesArray = new JSONArray();
	
	
				JSONObject seriesData = new JSONObject();
				seriesData.put("name", Utility.getLocaleValuePropByKey("lblOtherCandidates", locale));
				seriesData.put("color", "#A52A2A");
	
				seriesData.put("data", dataArray);
				seriesArray.add(seriesData);
	
				seriesData.put("name", Utility.getLocaleValuePropByKey("lblTopCandidates", locale));
				seriesData.put("color", "#8bbc21");
	
				seriesData.put("data", dataArray1);
				seriesArray.add(seriesData);
	
				jsonOutput.put("series", seriesArray);
	
				JSONObject credits = new JSONObject();
				credits.put("enabled", false);
				jsonOutput.put("credits", credits);
				/*
				 * tooltip: {
		                valueSuffix: ' candidates',
		                style: tmstyle
		            }
				 */
				JSONObject tooltip = new JSONObject();
				tooltip.put("style", style);
				jsonOutput.put("tooltip", tooltip);
			}
		}
		return jsonOutput;
	}

	public JSONArray getJobOrderStatus(DistrictMaster districtMaster,SchoolMaster schoolMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		int entityID=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String lblCriticialJobName =Utility.getLocaleValuePropByKey("tbCritical", locale);//Red
		String lblAttentionJobName = "Attention";//Yellow

		UserMaster userMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			entityID=userMaster.getEntityType();
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
				if(districtMaster.getLblCriticialJobName()!=null)
					lblCriticialJobName=districtMaster.getLblCriticialJobName();
				if(districtMaster.getLblAttentionJobName()!=null)
					lblAttentionJobName=districtMaster.getLblAttentionJobName();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
				if(schoolMaster.getLblCriticialJobName()!=null)
					lblCriticialJobName=schoolMaster.getLblCriticialJobName();
				if(schoolMaster.getLblAttentionJobName()!=null)
					lblAttentionJobName=schoolMaster.getLblAttentionJobName();
			}
			if(entityID==2)
				schoolMaster = null;
			if(districtMaster!=null && entityID==1)
			{
				districtMaster = districtMasterDAO.findById(districtMaster.getDistrictId(), false, false);
				if(districtMaster.getLblCriticialJobName()!=null)
					lblCriticialJobName=districtMaster.getLblCriticialJobName();
				if(districtMaster.getLblAttentionJobName()!=null)
					lblAttentionJobName=districtMaster.getLblAttentionJobName();
			}
			if(schoolMaster!=null && schoolMaster.getSchoolId()>0 && entityID==1)
			{
				schoolMaster = schoolMasterDAO.findById(schoolMaster.getSchoolId(), false, false);
				if(schoolMaster.getLblCriticialJobName()!=null)
					lblCriticialJobName=schoolMaster.getLblCriticialJobName();
				if(schoolMaster.getLblAttentionJobName()!=null)
					lblAttentionJobName=schoolMaster.getLblAttentionJobName();
			}	
		}
		int grandTotal=0;
		int grandTotalYou=0;
		int red=0;
		int redYou=0;
		int yellow=0;
		int yellowYou=0;
		Set<Integer> redSet = new HashSet<Integer>();
		Set<Integer> yellowSet = new HashSet<Integer>();
		Set<Integer> greenSet = new HashSet<Integer>();
		// All jobs
		List li  = jobActionFeedDAO.countJobList(districtMaster, schoolMaster,true);
		for(Object oo: li){
			Object obj[] = (Object[])oo;
			int total = Integer.parseInt(String.valueOf(obj[0]));
			grandTotal+=total;
			int redjob=Integer.parseInt(String.valueOf(obj[1]));
			if(redjob!=0 && obj[3]!=null)
				redSet.add(Integer.parseInt(String.valueOf(obj[3])));

			red+=redjob;
			int yellowjob=Integer.parseInt(String.valueOf(obj[2]));
			if(yellowjob!=0 && obj[3]!=null)
				yellowSet.add(Integer.parseInt(String.valueOf(obj[3])));

			yellow+=yellowjob;

			if(total!=0 && obj[3]!=null)
				greenSet.add(Integer.parseInt(String.valueOf(obj[3])));
		}
		List li1  = jobActionFeedDAO.countJobList(districtMaster, schoolMaster,false);
		for(Object oo: li1){
			Object obj[] = (Object[])oo;
			grandTotalYou+=Integer.parseInt(String.valueOf(obj[0]));
			redYou+=Integer.parseInt(String.valueOf(obj[1]));
			yellowYou+=Integer.parseInt(String.valueOf(obj[2]));
		}

		JSONArray categories = new JSONArray();
		int total = grandTotal-(red+yellow);
		int totalYou = grandTotalYou-(redYou+yellowYou);

		/////////////// New Changes on demand //////////////////////
		if(redSet.size()>0)
			red = red/redSet.size();
		if(yellowSet.size()>0)
			yellow = yellow/yellowSet.size();
		if(greenSet.size()>0)
			total = total/greenSet.size();

		grandTotal = red+yellow+total;
		////////////////////////////////////////


		float toatalFloat = ((float)total/(float)grandTotal)*100;
		float redFloat = ((float)red/(float)grandTotal)*100;
		float yellowFloat = ((float)yellow/(float)grandTotal)*100;
		float toatalYouFloat = ((float)totalYou/(float)grandTotalYou)*100;
		float redYouFloat = ((float)redYou/(float)grandTotalYou)*100;
		float yellowYouFloat = ((float)yellowYou/(float)grandTotalYou)*100;
		if(toatalFloat==0 && redFloat==0 && yellowFloat==0 && toatalYouFloat==0 && redYouFloat==0 && yellowYouFloat==0)
		{
			categories.add(20);
			categories.add(25);
			categories.add(55);

			categories.add(27);
			categories.add(43);
			categories.add(30);

		}else
		{
			categories.add(Math.round(redFloat));
			categories.add(Math.round(yellowFloat));
			categories.add(Math.round(toatalFloat));

			categories.add(Math.round(redYouFloat));
			categories.add(Math.round(yellowYouFloat));
			categories.add(Math.round(toatalYouFloat));
		}

		categories.add(lblCriticialJobName);
		categories.add(lblAttentionJobName);
		return categories;
	}

	public String getHiringVelocity(int entityType)
	{
		return null;
	}
	public JSONArray getPoolQuality(DistrictMaster districtMaster,SchoolMaster schoolMaster,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		int entityID=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			entityID=userMaster.getEntityType();
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}
			if(entityID==2)
				schoolMaster = null;

			if(districtMaster!=null && entityID==1)
			{
				districtMaster = districtMasterDAO.findById(districtMaster.getDistrictId(), false, false);
			}

			if(schoolMaster!=null && schoolMaster.getSchoolId()>0 && entityID==1)
			{
				schoolMaster = schoolMasterDAO.findById(schoolMaster.getSchoolId(), false, false);
			}	
		}
		String locale = Utility.getValueOfPropByKey("locale");
		String decile9=Utility.getValueOfPropByKey("decile9");
		String decile8=Utility.getValueOfPropByKey("decile8");
		String decile7=Utility.getValueOfPropByKey("decile7");
		String decile6=Utility.getValueOfPropByKey("decile6");
		String decile5=Utility.getValueOfPropByKey("decile5");
		String decile4=Utility.getValueOfPropByKey("decile4");
		String decile3=Utility.getValueOfPropByKey("decile3");
		String decile2=Utility.getValueOfPropByKey("decile2");
		String decile1=Utility.getValueOfPropByKey("decile1");


		JSONArray deciles = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		JSONArray dataArray = new JSONArray();
		Map<String,Integer> totalMap = new HashMap<String, Integer>();
		Map<String,Integer> hiredMap = new HashMap<String, Integer>();
		if(userMaster.getEntityType()!=6){
			List lstTotalvk = teacherNormScoreDAO.getCandidatesPoolHQL(districtMaster, schoolMaster, null);
			List lstTotal = new ArrayList();
			if(lstTotalvk.size()>0)
				lstTotal = teacherNormScoreDAO.countCandidatesPoolHQL(lstTotalvk);
	
			Integer totalCount=0;
			Integer totalHiredCount=0;
	
			for(Object oo: lstTotal){
				Object obj[] = (Object[])oo;
				totalMap.put(""+obj[1], Integer.parseInt(String.valueOf(obj[0])));
				totalCount=totalCount+Integer.parseInt(String.valueOf(obj[0]));
			}
	
			StatusMaster statusMaster = new StatusMaster();
			statusMaster.setStatusId(6);
	
			List lstTotalvkhrd = teacherNormScoreDAO.getCandidatesPoolHQL(districtMaster, schoolMaster, statusMaster);
	
			List lstHired = new ArrayList();
	
			if(lstTotalvkhrd.size()>0)
				lstHired = teacherNormScoreDAO.countCandidatesPoolHQL(lstTotalvkhrd);
			for(Object oo: lstHired){
				Object obj[] = (Object[])oo;
				hiredMap.put(""+obj[1], Integer.parseInt(String.valueOf(obj[0])));
				totalHiredCount+=Integer.parseInt(String.valueOf(obj[0]));
				//System.out.println( "111"+Integer.parseInt(String.valueOf(obj[0])));
			}
			// checking hired & unhired		
			boolean flag=false;
			if(lstTotal.size()==0 && lstHired.size()==0)
				flag=true;
	
			if(flag)
			{	dataArray.add(16);
			dataArray.add(13);
			}else
			{	
				float total=0;
				float hire=0;
				if(totalMap.get(decile9)!=null){
					total=((float)totalMap.get(decile9)/(float)totalCount)*100;
				}
				if(hiredMap.get(decile9)!=null){
					hire=((float)hiredMap.get(decile9)/(float)totalHiredCount)*100;
				}
				//System.out.println("D 9 ::"+total);
				System.out.println("D 9 ::"+((double) Math.round(total * 100) / 100));
				dataArray.add(totalMap.get(decile9)==null?0:((double) Math.round(total * 100) / 100));
				dataArray.add(hiredMap.get(decile9)==null?0:((double) Math.round(hire * 100) / 100));
			}
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile9", locale));
			jsonObject.put("color", "#"+decile9);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
			if(flag)
			{	dataArray.add(6);
			dataArray.add(11);
			}else
			{	
				float total=0;
				float hire=0;
				if(totalMap.get(decile8)!=null){
					total=((float)totalMap.get(decile8)/(float)totalCount)*100;
	
				}
				if(hiredMap.get(decile8)!=null){
					hire=((float)hiredMap.get(decile8)/(float)totalHiredCount)*100;
				}
				//System.out.println("D 8 ::"+total);
				System.out.println("D 8 ::"+((double) Math.round(total * 100) / 100));
				dataArray.add(totalMap.get(decile8)==null?0:((double) Math.round(total * 100) / 100));
				dataArray.add(hiredMap.get(decile8)==null?0:((double) Math.round(hire * 100) / 100));
			}
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile8", locale));
			jsonObject.put("color", "#"+decile8);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
			if(flag)
			{	dataArray.add(4);
			dataArray.add(0);
			}else
			{	
				float total=0;
				float hire=0;
				if(totalMap.get(decile7)!=null){
					total=((float)totalMap.get(decile7)/(float)totalCount)*100;
				}
				if(hiredMap.get(decile7)!=null){
					hire=((float)hiredMap.get(decile7)/(float)totalHiredCount)*100;
				}
				//System.out.println("D 7 ::"+total);
				System.out.println("D 7 ::"+((double) Math.round(total * 100) / 100));
				dataArray.add(totalMap.get(decile7)==null?0:((double) Math.round(total * 100) / 100));
				dataArray.add(hiredMap.get(decile7)==null?0:((double) Math.round(hire * 100) / 100));
			}
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile7", locale));
			jsonObject.put("color", "#"+decile7);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
			if(flag)
			{	dataArray.add(16);
			dataArray.add(15);
			}else
			{	
				float total=0;
				float hire=0;
				if(totalMap.get(decile6)!=null){
					total=((float)totalMap.get(decile6)/(float)totalCount)*100;
				}
				if(hiredMap.get(decile6)!=null){
					hire=((float)hiredMap.get(decile6)/(float)totalHiredCount)*100;
				}
				//System.out.println("D 6 ::"+total);
				System.out.println("D 6 ::"+((double) Math.round(total * 100) / 100));
				dataArray.add(totalMap.get(decile6)==null?0:((double) Math.round(total * 100) / 100));
				dataArray.add(hiredMap.get(decile6)==null?0:((double) Math.round(hire * 100) / 100));
			}
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile6", locale));
			jsonObject.put("color", "#"+decile6);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
			if(flag)
			{	dataArray.add(3);
			dataArray.add(7);
			}else
			{	
				float total=0;
				float hire=0;
				if(totalMap.get(decile5)!=null){
					total=((float)totalMap.get(decile5)/(float)totalCount)*100;
				}
				if(hiredMap.get(decile5)!=null){
					hire=((float)hiredMap.get(decile5)/(float)totalHiredCount)*100;
				}
	
				System.out.println("D 5 ::"+((double) Math.round(total * 100) / 100));
				dataArray.add(totalMap.get(decile5)==null?0:((double) Math.round(total * 100) / 100));
				dataArray.add(hiredMap.get(decile5)==null?0:((double) Math.round(hire * 100) / 100));
			}
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile5", locale));
			jsonObject.put("color", "#"+decile5);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
			if(flag)
			{	dataArray.add(17);
			dataArray.add(12);
			}else
			{	
	
				float total=0;
				float hire=0;
				if(totalMap.get(decile4)!=null){
					total=((float)totalMap.get(decile4)/(float)totalCount)*100;
				}
				if(hiredMap.get(decile4)!=null){
					hire=((float)hiredMap.get(decile4)/(float)totalHiredCount)*100;
				}
	
				System.out.println("D 4 ::"+((double) Math.round(total * 100) / 100));
				dataArray.add(totalMap.get(decile4)==null?0:((double) Math.round(total * 100) / 100));
				dataArray.add(hiredMap.get(decile4)==null?0:((double) Math.round(hire * 100) / 100));
			}
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile4", locale));
			jsonObject.put("color", "#"+decile4);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
			if(flag)
			{	dataArray.add(8);
			dataArray.add(9);
			}else
			{	
				float total=0;
				float hire=0;
				if(totalMap.get(decile3)!=null){
					total=((float)totalMap.get(decile3)/(float)totalCount)*100;
				}
				if(hiredMap.get(decile3)!=null){
					hire=((float)hiredMap.get(decile3)/(float)totalHiredCount)*100;
				}
	
				System.out.println("D 3 ::"+((double) Math.round(total * 100) / 100));
				dataArray.add(totalMap.get(decile3)==null?0:((double) Math.round(total * 100) / 100));
				dataArray.add(hiredMap.get(decile3)==null?0:((double) Math.round(hire * 100) / 100));
			}
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile3", locale));
			jsonObject.put("color", "#"+decile3);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
			if(flag)
			{	dataArray.add(12);
			dataArray.add(11);
			}else
			{	
	
				float total=0;
				float hire=0;
				if(totalMap.get(decile2)!=null){
					total=((float)totalMap.get(decile2)/(float)totalCount)*100;
				}
				if(hiredMap.get(decile2)!=null){
					hire=((float)hiredMap.get(decile2)/(float)totalHiredCount)*100;
				}
	
				System.out.println("D 2 ::"+((double) Math.round(total * 100) / 100));
				dataArray.add(totalMap.get(decile2)==null?0:((double) Math.round(total * 100) / 100));
				dataArray.add(hiredMap.get(decile2)==null?0:((double) Math.round(hire * 100) / 100));
			}
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile2", locale));
			jsonObject.put("color", "#"+decile2);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
			if(flag)
			{	
				dataArray.add(18);
				dataArray.add(22);
			}else
			{	
				float total=0;
				float hire=0;
				if(totalMap.get(decile1)!=null){
					total=((float)totalMap.get(decile1)/(float)totalCount)*100;
				}
				if(hiredMap.get(decile1)!=null){
					hire=((float)hiredMap.get(decile1)/(float)totalHiredCount)*100;
				}
				System.out.println("decile 1 :: "+((double) Math.round(total * 100) / 100));
				dataArray.add(totalMap.get(decile1)==null?0:((double) Math.round(total * 100) / 100));
				dataArray.add(hiredMap.get(decile1)==null?0:((double) Math.round(hire * 100) / 100));
			}
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile1", locale));
			jsonObject.put("color", "#"+decile1);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			totalMap = null;
			hiredMap = null;
			lstTotalvkhrd = null;
			lstTotal = null;
			lstTotalvkhrd = null;
			lstHired = null;
		}
		else{
			dataArray.add(16);
			dataArray.add(13);
	
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile9", locale));
			jsonObject.put("color", "#"+decile9);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
	
			dataArray.add(6);
			dataArray.add(11);
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile8", locale));
			jsonObject.put("color", "#"+decile8);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
	
	
			dataArray.add(4);
			dataArray.add(0);
	
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile7", locale));
			jsonObject.put("color", "#"+decile7);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
	
			dataArray.add(16);
			dataArray.add(15);
	
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile6", locale));
			jsonObject.put("color", "#"+decile6);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
	
			dataArray.add(3);
			dataArray.add(7);
	
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile5", locale));
			jsonObject.put("color", "#"+decile5);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
	
	
			dataArray.add(17);
			dataArray.add(12);
	
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile4", locale));
			jsonObject.put("color", "#"+decile4);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
	
			dataArray.add(8);
			dataArray.add(9);
	
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile3", locale));
			jsonObject.put("color", "#"+decile3);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
	
			dataArray.add(12);
			dataArray.add(11);
	
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile2", locale));
			jsonObject.put("color", "#"+decile2);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
	
			dataArray.clear();
	
			dataArray.add(18);
			dataArray.add(22);
	
			jsonObject.put("name", Utility.getLocaleValuePropByKey("lblDecile1", locale));
			jsonObject.put("color", "#"+decile1);
			jsonObject.put("data", dataArray);
			deciles.add(jsonObject);
		}

		return deciles;
	}

	public JSONObject getJobOrderStatusBySchools(DistrictMaster districtMaster,SchoolMaster schoolMaster, Integer schoolId,String stateId,String certType, boolean checkFlag, boolean chkbyopening,int numOpeningSelectVal, int numberofopening)
	{
		System.out.println("chkbyopening :: "+chkbyopening+" numOpeningSelectVal :: "+numOpeningSelectVal  + " numberofopening :: "+numberofopening);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		int entityID=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String lblCriticialJobName = Utility.getLocaleValuePropByKey("tbCritical", locale);//Red
		String lblAttentionJobName = "Attention";//Yellow

		UserMaster userMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			entityID=userMaster.getEntityType();
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
				if(districtMaster.getLblCriticialJobName()!=null)
					lblCriticialJobName=districtMaster.getLblCriticialJobName();
				if(districtMaster.getLblAttentionJobName()!=null)
					lblAttentionJobName=districtMaster.getLblAttentionJobName();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
				if(schoolMaster.getLblCriticialJobName()!=null)
					lblCriticialJobName=schoolMaster.getLblCriticialJobName();
				if(schoolMaster.getLblAttentionJobName()!=null)
					lblAttentionJobName=schoolMaster.getLblAttentionJobName();
			}
			if(entityID==2)
				schoolMaster = null;
			if(districtMaster!=null && entityID==1)
			{
				districtMaster = districtMasterDAO.findById(districtMaster.getDistrictId(), false, false);
				if(districtMaster.getLblCriticialJobName()!=null)
					lblCriticialJobName=districtMaster.getLblCriticialJobName();
				if(districtMaster.getLblAttentionJobName()!=null)
					lblAttentionJobName=districtMaster.getLblAttentionJobName();
			}
			if(schoolMaster!=null && schoolMaster.getSchoolId()>0 && entityID==1)
			{
				schoolMaster = schoolMasterDAO.findById(schoolMaster.getSchoolId(), false, false);
				if(schoolMaster.getLblCriticialJobName()!=null)
					lblCriticialJobName=schoolMaster.getLblCriticialJobName();
				if(schoolMaster.getLblAttentionJobName()!=null)
					lblAttentionJobName=schoolMaster.getLblAttentionJobName();
			}	


			if(schoolId!=null){
				schoolMaster = schoolMasterDAO.findById(new Long(schoolId), false, false);
			}
		}
		int grandTotal=0;
		int grandTotalYou=0;
		int red=0;
		int redYou=0;
		int yellow=0;
		int yellowYou=0;
		Set<Integer> redSet = new HashSet<Integer>();
		Set<Integer> yellowSet = new HashSet<Integer>();
		Set<Integer> greenSet = new HashSet<Integer>();


		//mukesh
		//// Licensure Name/Licensure State filter

		System.out.println("stateId :: "+stateId +" certType :: "+certType);
		List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
		StateMaster stateMaster=null;
		List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
		List<Integer> jobOrderIds =new ArrayList<Integer>();

		if(!certType.equals("0") && !certType.equals("")||!stateId.equals("0")){
			if(!stateId.equals("0")){
				stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
			}
			certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
			System.out.println("certificateTypeMasterList:: "+certificateTypeMasterList.size());
			if(certificateTypeMasterList.size()>0)
				certJobOrderList=jobCertificationDAO.findCertificationByJobWithState(certificateTypeMasterList);
			System.out.println("certJobOrderList:: "+certJobOrderList.size());
			if(certJobOrderList.size()>0){
				for(JobOrder job: certJobOrderList){
					jobOrderIds.add(job.getJobId());
				}
			}

		}


		String jobIdStr = ""; 
		for(JobOrder jft:certJobOrderList) 
		{
			if(jobIdStr.equals("")){
				jobIdStr=""+jft.getJobId();
			}else{
				jobIdStr=jobIdStr+","+jft.getJobId();
			}

		}

		// check by number of opening

		String jobIdS = "";
		List<JobActionFeed> listJobActionFeeds1 =null;
		Map<Integer, Integer> mapjobopening =  new HashMap<Integer, Integer>();
		if(chkbyopening && numOpeningSelectVal!=0)
		{ 
			listJobActionFeeds1 = jobActionFeedDAO.findbyDistrictAndOrSchool(districtMaster, schoolMaster); 
			for(JobActionFeed jaf : listJobActionFeeds1)
			{
				if(mapjobopening.get(jaf.getJobOrder().getJobId())!=null)
				{
					mapjobopening.put(jaf.getJobOrder().getJobId(), jaf.getExpectedHires()+mapjobopening.get(jaf.getJobOrder().getJobId()));
				}else
				{
					mapjobopening.put(jaf.getJobOrder().getJobId(), jaf.getExpectedHires());
				}
			}

			for (Map.Entry<Integer, Integer> entry : mapjobopening.entrySet())
			{
				if(numOpeningSelectVal==1)
				{ 
					if(entry.getValue()==numberofopening)
					{
						if(jobIdS.equals("")){
							jobIdS=""+entry.getKey();
						}else{
							jobIdS=jobIdS+","+entry.getKey();
						}
					}

				}	
				else if(numOpeningSelectVal==2)
				{ 
					if(entry.getValue()<numberofopening)
					{
						if(jobIdS.equals("")){
							jobIdS=""+entry.getKey();
						}else{
							jobIdS=jobIdS+","+entry.getKey();
						}
					}
				}
				else if(numOpeningSelectVal==3)
				{ 
					if(entry.getValue()<=numberofopening)
					{
						if(jobIdS.equals("")){
							jobIdS=""+entry.getKey();
						}else{
							jobIdS=jobIdS+","+entry.getKey();
						}
					}

				}
				else if(numOpeningSelectVal==4)
				{ 
					if(entry.getValue()>numberofopening)
					{
						if(jobIdS.equals("")){
							jobIdS=""+entry.getKey();
						}else{
							jobIdS=jobIdS+","+entry.getKey();
						}
					}

				}
				else if(numOpeningSelectVal==5)
				{ 
					if(entry.getValue()>=numberofopening)
					{
						if(jobIdS.equals("")){
							jobIdS=""+entry.getKey();
						}else{
							jobIdS=jobIdS+","+entry.getKey();
						}
					}

				}


			}
			System.out.println("=================================== "+jobIdS); 	
		}

		//

		List li  = new ArrayList();
		List li1  = new ArrayList();
		// All jobs
		//if(checkFlag==true && certJobOrderList.size()>0)
		li  = jobActionFeedDAO.countJobListByFilter(districtMaster, schoolMaster,true,jobIdStr,checkFlag,chkbyopening,jobIdS,numOpeningSelectVal);
		if(li.size()>0)
			for(Object oo: li){
				Object obj[] = (Object[])oo;
				int total = Integer.parseInt(String.valueOf(obj[0]));
				grandTotal+=total;
				int redjob=Integer.parseInt(String.valueOf(obj[1]));
				if(redjob!=0 && obj[3]!=null)
					redSet.add(Integer.parseInt(String.valueOf(obj[3])));

				red+=redjob;
				int yellowjob=Integer.parseInt(String.valueOf(obj[2]));
				if(yellowjob!=0 && obj[3]!=null)
					yellowSet.add(Integer.parseInt(String.valueOf(obj[3])));

				yellow+=yellowjob;

				if(total!=0 && obj[3]!=null)
					greenSet.add(Integer.parseInt(String.valueOf(obj[3])));
			}
		//if(checkFlag==true && certJobOrderList.size()>0)
		li1  = jobActionFeedDAO.countJobListByFilter(districtMaster, schoolMaster,false,jobIdStr,checkFlag,chkbyopening,jobIdS, numOpeningSelectVal);
		if(li1.size()>0)
			for(Object oo: li1){
				Object obj[] = (Object[])oo;
				grandTotalYou+=Integer.parseInt(String.valueOf(obj[0]));
				redYou+=Integer.parseInt(String.valueOf(obj[1]));
				yellowYou+=Integer.parseInt(String.valueOf(obj[2]));
			}

		/*JSONArray data = new JSONArray();*/
		JSONObject jsonObject = new JSONObject();
		JSONArray categories = new JSONArray();
		int total = grandTotal-(red+yellow);
		int totalYou = grandTotalYou-(redYou+yellowYou);

		/////////////// New Changes on demand //////////////////////
		if(redSet.size()>0)
			red = red/redSet.size();
		if(yellowSet.size()>0)
			yellow = yellow/yellowSet.size();
		if(greenSet.size()>0)
			total = total/greenSet.size();

		grandTotal = red+yellow+total;
		////////////////////////////////////////


		float toatalFloat = ((float)total/(float)grandTotal)*100;
		float redFloat = ((float)red/(float)grandTotal)*100;
		float yellowFloat = ((float)yellow/(float)grandTotal)*100;
		float toatalYouFloat = ((float)totalYou/(float)grandTotalYou)*100;
		float redYouFloat = ((float)redYou/(float)grandTotalYou)*100;
		float yellowYouFloat = ((float)yellowYou/(float)grandTotalYou)*100; 
		//System.out.println("AA :: "+toatalFloat+" 2 "+ redFloat +" 3 " +yellowFloat +" 4 "+yellowFloat+" 5 "+toatalYouFloat+" 6 "+redYouFloat+" 7 "+yellowYouFloat);

		//System.out.println(" @@@@@ toatalFloat :: "+toatalFloat);
		if(Float.isNaN(toatalFloat))
		{
			System.out.println("(((((((((((((((((((((((((");
		}
		if(Float.isNaN(toatalFloat) && Float.isNaN(redFloat) && Float.isNaN(yellowFloat) && Float.isNaN(toatalYouFloat)&& Float.isNaN(redYouFloat) && Float.isNaN(yellowYouFloat))
		{
			categories.add(20);
			categories.add(25);
			categories.add(55);
			jsonObject.put("overall", categories);


			categories.clear();

			categories.add(27);
			categories.add(43);
			categories.add(30);
			jsonObject.put("within", categories);

		}else
		{

			categories.add(Math.round(redYouFloat));
			categories.add(Math.round(yellowYouFloat));
			categories.add(Math.round(toatalYouFloat));
			jsonObject.put("overAll", categories);

			categories.clear();

			categories.add(Math.round(redFloat));
			categories.add(Math.round(yellowFloat));
			categories.add(Math.round(toatalFloat));
			jsonObject.put("within", categories);


		}

		System.out.println(lblCriticialJobName);
		System.out.println(lblAttentionJobName);
		System.out.println(" "+jsonObject.toString());

		return jsonObject;
	}

	public String startReminder()
	{
		System.out.println("=======================================startReminder ");
		List<DistrictMaster> lstDistrictMaster = null;
		lstDistrictMaster = districtMasterDAO.findDistrictsReminderRequired(true);
		System.out.println("lstDistrictMaster.size()::: "+lstDistrictMaster.size());
		List<JobOrder> jobOrders = jobOrderDAO.getActiveJobs(lstDistrictMaster);
		System.out.println("jobOrders.size()::: "+jobOrders.size());
		//Map<Integer,Integer[]> map = new HashMap<Integer, Integer[]>();
		/*for (JobOrder jobOrder : jobOrders) {
			Integer[] da = new Integer[2];
			DistrictMaster districtMaster = jobOrder.getDistrictMaster();
			da[0]=districtMaster.getReminderFrequencyInDays();
			da[1]=districtMaster.getNoOfReminder();
			map.put(jobOrder.getJobId(), da);
		}*/
		List<JobForTeacher> lstJobForTeacherActionNeededList = new ArrayList<JobForTeacher>();
		Date startDate = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		List<Long> jbIdListNew = new ArrayList<Long>();
		List<Long> jbIdListExisting = new ArrayList<Long>();
		Map<JobForTeacher,StatusMaster> actionMap = new HashMap<JobForTeacher, StatusMaster>();
		if(jobOrders.size()>0)
		{
			List<JobForTeacher> lstJobForTeacher = jobForTeacherDAO.findByIncompJobs(jobOrders);
			System.out.println("lstJobForTeacher.size()::: "+lstJobForTeacher.size());
			
			List<TeacherDetail> teacherDetailList=new ArrayList<TeacherDetail>();
			Map<Integer,TeacherPortfolioStatus> maptPortfolioStatus = new HashMap<Integer, TeacherPortfolioStatus>();
			
			try{
				if(lstJobForTeacher!=null && lstJobForTeacher.size()>0){
					for (JobForTeacher jobForTeacher : lstJobForTeacher) {
						teacherDetailList.add(jobForTeacher.getTeacherId());
					}
				}
				maptPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacherlist(teacherDetailList);
				
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			Date endDate = new Date();
			for (JobForTeacher jobForTeacher : lstJobForTeacher) {
				
				TeacherPortfolioStatus tPortfolioStatus = new TeacherPortfolioStatus();
				try{
					if(jobForTeacher.getTeacherId().getTeacherId()!=null)
						tPortfolioStatus = maptPortfolioStatus.get(jobForTeacher.getTeacherId().getTeacherId());
				}catch(Exception exception){
					exception.printStackTrace();
				}
				
				boolean piStatus = false;
				boolean acStatus = false;
				boolean crStatus = false;
				boolean exStatus = false;
				boolean afStatus = false;

				if(tPortfolioStatus!=null){
					if(tPortfolioStatus.getIsPersonalInfoCompleted()==false || tPortfolioStatus.getIsPersonalInfoCompleted()==null)
						piStatus = true;
					
					if(tPortfolioStatus.getIsAcademicsCompleted()==false || tPortfolioStatus.getIsAcademicsCompleted()==null)
						acStatus = true;
					
					if(tPortfolioStatus.getIsCertificationsCompleted()==false || tPortfolioStatus.getIsCertificationsCompleted()==null)
						crStatus = true;
					
					if(tPortfolioStatus.getIsExperiencesCompleted()==false || tPortfolioStatus.getIsExperiencesCompleted()==null)
						exStatus = true;
					
					if(tPortfolioStatus.getIsAffidavitCompleted()==false || tPortfolioStatus.getIsAffidavitCompleted()==null)
						afStatus = true;
					
				} else {
					piStatus = true;
					acStatus = true;
					crStatus = true;
					exStatus = true;
					afStatus = true;
				}
				System.out.println(":::::::::::::::::::::::::::::::::::::::::::::"+jobForTeacher.getTeacherId());
				//System.out.println(tPortfolioStatus+"||"+jobForTeacher.getTeacherId().getTeacherId()+"||"+piStatus+"||"+acStatus+"||"+crStatus+"||"+exStatus+"||"+afStatus);
				
				DistrictMaster districtMaster = jobForTeacher.getJobId().getDistrictMaster();
				int reminderFrequencyInDays = districtMaster.getReminderFrequencyInDays()==null?0:districtMaster.getReminderFrequencyInDays();
				int noOfReminder = districtMaster.getNoOfReminder()==null?0:districtMaster.getNoOfReminder();
				int reminderOfFirstFrequencyInDays = districtMaster.getReminderOfFirstFrequencyInDays()==null?0:districtMaster.getReminderOfFirstFrequencyInDays();
				int noOfReminderSent = jobForTeacher.getNoOfReminderSent()==null?0:jobForTeacher.getNoOfReminderSent();

				if(noOfReminderSent!=0)
					reminderFrequencyInDays = reminderOfFirstFrequencyInDays;
				
				System.out.println(":::::::::::::::::::::::::::::::::::::::::::::");
				//System.out.println("reminderFrequencyInDays:: "+reminderFrequencyInDays);

				if(piStatus == true || acStatus == true || crStatus == true || exStatus == true || afStatus == true)
				{
					//System.out.println("::::::::::::: Reminder Sent :::::::::::::");
					if(noOfReminderSent<noOfReminder+1)
					{
						//System.out.println("reminderFrequencyInDays:: "+reminderFrequencyInDays);
						//System.out.println("noOfReminder:: "+noOfReminder);
						if(jobForTeacher.getReminderSentDate()==null)
						{
							startDate = jobForTeacher.getCreatedDateTime();
							try {startDate = dateFormat.parse(dateFormat.format(startDate));
							} catch (ParseException e) {}
						}
						else
							startDate = jobForTeacher.getReminderSentDate();

						int difInDays = (int) ((endDate.getTime() - startDate.getTime())/(1000*60*60*24));
						System.out.println("difInDays::: "+difInDays);
						if(noOfReminderSent>=noOfReminder && difInDays==reminderFrequencyInDays)
						{
							StatusMaster statusMaster = districtMaster.getStatusIdReminderExpireAction();
							if(statusMaster!=null)
							{
								actionMap.put(jobForTeacher, statusMaster);
							}
							if(districtMaster.getStatusIdReminderExpireAction()!=null || districtMaster.getSecondaryStatusIdReminderExpireAction()!=null)
							{
								lstJobForTeacherActionNeededList.add(jobForTeacher);
							}
						}else if(difInDays==reminderFrequencyInDays || (noOfReminderSent==0 && difInDays>=reminderFrequencyInDays))
						{
							//System.out.println("jobForTeacher: "+jobForTeacher.getTeacherId().getEmailAddress());
							if(noOfReminderSent==0)
								jbIdListNew.add(jobForTeacher.getJobForTeacherId());
							else
								jbIdListExisting.add(jobForTeacher.getJobForTeacherId());

							try {
								JobOrder jobOrder = jobForTeacher.getJobId();
								TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
								if(districtMaster!=null && districtMaster.getDistrictId()!=null && districtMaster.getDistrictId()==614730)	//Fullerton:-614730 
									emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), Utility.getLocaleValuePropByKey("msgIncompletejob", locale)+" "+jobOrder.getJobTitle()+" "+Utility.getLocaleValuePropByKey("msgwithFullertonSchoolDistrict", locale) ,MailText.sendReminderMailTextForFullerton(jobOrder,teacherDetail));
								else
									emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), Utility.getLocaleValuePropByKey("msgUpdateapplicationob", locale)+" "+jobOrder.getJobTitle()+" "+Utility.getLocaleValuePropByKey("lblwith", locale) +" "+districtMaster.getDistrictName(),MailText.sendReminderMailText(jobOrder,teacherDetail));
								System.out.println("Reminder Mail set to "+teacherDetail.getEmailAddress());
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

					}
				}
			}
			if(jbIdListNew.size()>0)
			{
				jobForTeacherDAO.addReminderCount(jbIdListNew);
			}
			if(jbIdListExisting.size()>0)
			{
				jobForTeacherDAO.updateReminderCount(jbIdListExisting);
			}
			System.out.println("New : "+jbIdListNew.size());
			System.out.println("Existing : "+jbIdListExisting.size());
			System.out.println("After All reminder : "+actionMap.size());
			if(actionMap.size()>0)
			{
				Map<JobForTeacher, StatusMaster> jftSm=new HashMap<JobForTeacher, StatusMaster>();
				SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				Transaction txOpen =statelesSsession.beginTransaction();

				for (Map.Entry<JobForTeacher, StatusMaster> entry : actionMap.entrySet()) {
					System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
					JobForTeacher jft = entry.getKey();
					StatusMaster sm = entry.getValue();
					jftSm.put(jft, sm);
					System.out.println("---------------------------------- set value in the  Map<JobForTeacher, StatusMaster> jftSm  Map-----------------------------------------------------------------");
					jft.setStatus(sm);
					jft.setStatusMaster(sm);
					statelesSsession.update(jft);
				}
				txOpen.commit();
				statelesSsession.close();
				System.out.println("---------------------------------- size of the  Map<JobForTeacher, StatusMaster> jftSm  Map------- and call -setVluInteacherStatusHistoryForJob(jftSm) mehtod---------------------------------------------------------  "+jftSm.size());
				setVluInteacherStatusHistoryForJob(jftSm);
				actionMap = null;
				jbIdListExisting = null;
				jbIdListNew = null;
				lstJobForTeacher = null;
			}
		}
		return "Done";

	}

	public String startExamReminder()
	{
		System.out.println("=======================================startExamReminder ");
		List<ExaminationCodeDetails> lstExaminationCodeDetails = examinationCodeDetailsDAO.getExamCodesByStatus();
		Date endDate = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		try {
			endDate = dateFormat.parse(dateFormat.format(new Date()));
		} catch (ParseException e) {
			endDate = new Date();
		}
		boolean mailFlag = false;
		for (ExaminationCodeDetails examinationCodeDetails : lstExaminationCodeDetails) {
			//System.out.println("OOOOOOOOOOOOOOOOOOOOOOOO "+examinationCodeDetails.getInviteStatus());
			Date createDateTime = examinationCodeDetails.getCreatedDateTime();
			try {
				createDateTime = dateFormat.parse(dateFormat.format(createDateTime));
			} catch (ParseException e) {
				createDateTime = examinationCodeDetails.getCreatedDateTime();
			}

			DistrictAssessmentDetail districtAssessmentDetail = examinationCodeDetails.getDistrictAssessmentDetail();
			JobOrder jobOrder = examinationCodeDetails.getJobOrder();
			int difInDays = (int) ((endDate.getTime() - createDateTime.getTime())/(1000*60*60*24));
			System.out.println("difInDays::: "+difInDays);
			System.out.println("endDate:::: "+endDate);
			System.out.println("createDateTime:::: "+createDateTime);
			String baseURL=Utility.getValueOfPropByKey("basePath");
			if(difInDays==1)
			{
				mailFlag = true;
			}else if(difInDays>=5)
			{
				mailFlag = true;
			}else
				mailFlag = false;

			if(mailFlag)
			{
				String emailAddress = examinationCodeDetails.getTeacherDetail().getEmailAddress();
				System.out.println(" teacher emailAddress :: "+emailAddress);
				String linkIds = emailAddress+"###"+districtAssessmentDetail.getDistrictAssessmentId()+"###"+jobOrder.getJobId();
				String forMated = Utility.encodeInBase64(linkIds);
				forMated = baseURL+"examslotselection.do?id="+forMated;
				System.out.println("formatted url ::  "+forMated);
				String assessmentLink="<a href='"+forMated+"'> "+Utility.getLocaleValuePropByKey("msgclickforexamslot", locale)+" </a>";

				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom(Utility.getValueOfSmtpPropByKey("smtphost.adminusername"));
				dsmt.setMailto(emailAddress);
				dsmt.setMailsubject(Utility.getLocaleValuePropByKey("msgPoliteReminderinvite", locale));
				String content = MailText.mailTextForExamSlotSelection(examinationCodeDetails,assessmentLink);
				dsmt.setMailcontent(content);
				//System.out.println("content  "+content);
			}
		}
		return "Done";

	}

	public String startReminderForHQ()
	{
		System.out.println("=======================================startReminder for Head Quarter =============== ");
		List<HeadQuarterMaster> lstHeadQuarterMaster = null;
		lstHeadQuarterMaster = headQuarterMasterDAO.findHeadquartersReminderRequired(true);
		List<JobOrder> jobOrders = jobOrderDAO.getActiveHQJobs(lstHeadQuarterMaster);

		List<JobForTeacher> lstJobForTeacherActionNeededList = new ArrayList<JobForTeacher>(); 
		Date startDate = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		List<Long> jbIdListNew = new ArrayList<Long>();
		List<Long> jbIdListExisting = new ArrayList<Long>();
		Map<JobForTeacher,StatusMaster> actionMap = new HashMap<JobForTeacher, StatusMaster>();
		if(jobOrders.size()>0)
		{
			List<JobForTeacher> lstJobForTeacher = jobForTeacherDAO.findByIncompJobs(jobOrders);
			System.out.println("lstJobForTeacher.size()::: "+lstJobForTeacher.size());
			Date endDate = new Date();
			for (JobForTeacher jobForTeacher : lstJobForTeacher) {

				HeadQuarterMaster headQuarterMaster = jobForTeacher.getJobId().getHeadQuarterMaster();
				int reminderFrequencyInDays = headQuarterMaster.getReminderFrequencyInDays()==null?0:headQuarterMaster.getReminderFrequencyInDays();
				int noOfReminder = headQuarterMaster.getNoOfReminder()==null?0:headQuarterMaster.getNoOfReminder();
				int reminderOfFirstFrequencyInDays = headQuarterMaster.getReminderOfFirstFrequencyInDays()==null?0:headQuarterMaster.getReminderOfFirstFrequencyInDays();
				int noOfReminderSent = jobForTeacher.getNoOfReminderSent()==null?0:jobForTeacher.getNoOfReminderSent();

				if(noOfReminderSent!=0)
					reminderFrequencyInDays = reminderOfFirstFrequencyInDays;

				System.out.println("reminderFrequencyInDays:: "+reminderFrequencyInDays);

				if(noOfReminderSent<noOfReminder+1)
				{
					if(jobForTeacher.getReminderSentDate()==null)
					{
						startDate = jobForTeacher.getCreatedDateTime();
						try {startDate = dateFormat.parse(dateFormat.format(startDate));
						} catch (ParseException e) {}
					}
					else
						startDate = jobForTeacher.getReminderSentDate();

					int difInDays = (int) ((endDate.getTime() - startDate.getTime())/(1000*60*60*24));
					System.out.println("difInDays::: "+difInDays);
					if(noOfReminderSent>=noOfReminder && difInDays==reminderFrequencyInDays)
					{
						StatusMaster statusMaster = headQuarterMaster.getStatusIdReminderExpireAction();
						if(statusMaster!=null)
						{
							actionMap.put(jobForTeacher, statusMaster);
						}
					}else if(difInDays==reminderFrequencyInDays || (noOfReminderSent==0 && difInDays>=reminderFrequencyInDays))
					{
						 System.out.println("jobForTeacher: "+jobForTeacher.getTeacherId().getEmailAddress());
						if(noOfReminderSent==0)
							jbIdListNew.add(jobForTeacher.getJobForTeacherId());
						else
							jbIdListExisting.add(jobForTeacher.getJobForTeacherId());

						try {
							JobOrder jobOrder = jobForTeacher.getJobId();
							TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
							if(headQuarterMaster!=null)	
								emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Update your Job application for job "+jobOrder.getJobTitle()+" with "+headQuarterMaster.getHeadQuarterName(),MailText.sendReminderMailTextForHQ(jobOrder,teacherDetail));
							System.out.println("Reminder Mail set to "+teacherDetail.getEmailAddress());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				}
			}
			if(jbIdListNew.size()>0)
			{
				jobForTeacherDAO.addReminderCount(jbIdListNew);
			}
			if(jbIdListExisting.size()>0)
			{
				jobForTeacherDAO.updateReminderCount(jbIdListExisting);
			}
			System.out.println("New : "+jbIdListNew.size());
			System.out.println("Existing : "+jbIdListExisting.size());
			System.out.println("After All reminder : "+actionMap.size());
			if(actionMap.size()>0)
			{
				Map<JobForTeacher, StatusMaster> jftSm=new HashMap<JobForTeacher, StatusMaster>();
				SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				Transaction txOpen =statelesSsession.beginTransaction();

				for (Map.Entry<JobForTeacher, StatusMaster> entry : actionMap.entrySet()) {
					System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
					JobForTeacher jft = entry.getKey();
					StatusMaster sm = entry.getValue();
					jftSm.put(jft, sm);
					System.out.println("---------------------------------- set value in the  Map<JobForTeacher, StatusMaster> jftSm  Map-----------------------------------------------------------------");
					jft.setStatus(sm);
					jft.setStatusMaster(sm);
					statelesSsession.update(jft);
				}
				txOpen.commit();
				statelesSsession.close();
				System.out.println("---------------------------------- size of the  Map<JobForTeacher, StatusMaster> jftSm  Map------- and call -setVluInteacherStatusHistoryForJob(jftSm) mehtod---------------------------------------------------------  "+jftSm.size());
				setVluInteacherStatusHistoryForJob(jftSm);
				actionMap = null;
				jbIdListExisting = null;
				jbIdListNew = null;
				lstJobForTeacher = null;
			}
		}
		return "Done";
	}
	
	
	public String startPortfolioReminder(){
		
		System.out.println("::::::::::::: Start Portfolio Reminder :::::::::::::");
		List<DistrictMaster> lstDistrictMaster = null;
		lstDistrictMaster = districtMasterDAO.findDistrictsReminderForPortfolio(true);
		System.out.println("lstDistrictMaster.size()::: "+lstDistrictMaster.size());
		List<JobOrder> jobOrders = jobOrderDAO.getActiveJobs(lstDistrictMaster);
		System.out.println("jobOrders.size()::: "+jobOrders.size());
		Integer noOfReminderPortfolio = 0;
		
		Date startDate = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		List<Long> jbIdListNew = new ArrayList<Long>();
		List<Long> jbIdListExisting = new ArrayList<Long>();
		Map<JobForTeacher,StatusMaster> actionMap = new HashMap<JobForTeacher, StatusMaster>();
		
		if(lstDistrictMaster!=null)
			noOfReminderPortfolio = lstDistrictMaster.get(0).getNoOfReminderPortfolio();
		
		List<MessageToTeacher> messageToTeacherList = null;
		List<TeacherDetail> teacherDetailList = null;
		StatusMaster statusMaster = WorkThreadServlet.statusIdMap.get(6);
		try{
			if(noOfReminderPortfolio!=0)
				teacherDetailList = messageToTeacherDAO.getPortfolioRecord(noOfReminderPortfolio);
			
			if(teacherDetailList.size()>0)
				
			{
				List<JobForTeacher> lstJobForTeacher = jobForTeacherDAO.getAllUnhiredJob(teacherDetailList,statusMaster);
				System.out.println("lstJobForTeacher.size()::: "+lstJobForTeacher.size());
				Date endDate = new Date();
				for (JobForTeacher jobForTeacher : lstJobForTeacher) {

					DistrictMaster districtMaster = jobForTeacher.getJobId().getDistrictMaster();
					int reminderFrequencyInDays = districtMaster.getReminderFrequencyInDaysPortfolio()==null?0:districtMaster.getReminderFrequencyInDaysPortfolio();
					int noOfReminder = districtMaster.getNoOfReminder()==null?0:districtMaster.getNoOfReminder();
					int reminderOfFirstFrequencyInDays = districtMaster.getReminderOfFirstFrequencyInDaysPortfolio()==null?0:districtMaster.getReminderOfFirstFrequencyInDaysPortfolio();
					int noOfReminderSent = jobForTeacher.getNoOfReminderSent()==null?0:jobForTeacher.getNoOfReminderSent();

					if(noOfReminderSent!=0)
						reminderFrequencyInDays = reminderOfFirstFrequencyInDays;

					System.out.println("reminderFrequencyInDays:: "+reminderFrequencyInDays);

					if(noOfReminderSent<noOfReminder+1)
					{
						if(jobForTeacher.getReminderSentDate()==null)
						{
							startDate = jobForTeacher.getCreatedDateTime();
							try {startDate = dateFormat.parse(dateFormat.format(startDate));
							} catch (ParseException e) {}
						}
						else
							startDate = jobForTeacher.getReminderSentDate();

						int difInDays = (int) ((endDate.getTime() - startDate.getTime())/(1000*60*60*24));
						System.out.println("difInDays::: "+difInDays);
						if(noOfReminderSent>=noOfReminder && difInDays==reminderFrequencyInDays)
						{
							Integer statusId = districtMaster.getStatusIdReminderExpireActionPortfolio();
							StatusMaster statusMaster1 = WorkThreadServlet.statusIdMap.get(statusId);
							if(statusMaster!=null)
							{
								actionMap.put(jobForTeacher, statusMaster1);
							}
						}else if(difInDays==reminderFrequencyInDays || (noOfReminderSent==0 && difInDays>=reminderFrequencyInDays))
						{
							if(noOfReminderSent==0)
								jbIdListNew.add(jobForTeacher.getJobForTeacherId());
							else
								jbIdListExisting.add(jobForTeacher.getJobForTeacherId());

							try {
								JobOrder jobOrder = jobForTeacher.getJobId();
								TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
								if(districtMaster!=null && districtMaster.getDistrictId()!=null && districtMaster.getDistrictId()==614730)	//Fullerton:-614730 
									emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), Utility.getLocaleValuePropByKey("msgIncompletejob", locale)+" "+jobOrder.getJobTitle()+" "+Utility.getLocaleValuePropByKey("msgwithFullertonSchoolDistrict", locale) ,MailText.sendReminderMailTextForFullerton(jobOrder,teacherDetail));
								else
									emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), Utility.getLocaleValuePropByKey("msgUpdateapplicationob", locale)+" "+jobOrder.getJobTitle()+" "+Utility.getLocaleValuePropByKey("lblwith", locale) +" "+districtMaster.getDistrictName(),MailText.sendReminderMailText(jobOrder,teacherDetail));
								System.out.println("Reminder Mail set to "+teacherDetail.getEmailAddress());
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

					}

				}
				if(jbIdListNew.size()>0)
				{
					jobForTeacherDAO.addReminderCount(jbIdListNew);
				}
				if(jbIdListExisting.size()>0)
				{
					jobForTeacherDAO.updateReminderCount(jbIdListExisting);
				}
				System.out.println("New : "+jbIdListNew.size());
				System.out.println("Existing : "+jbIdListExisting.size());
				System.out.println("After All reminder : "+actionMap.size());
				if(actionMap.size()>0)
				{
					SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
					StatelessSession statelesSsession = sessionFactory.openStatelessSession();
					Transaction txOpen =statelesSsession.beginTransaction();

					for (Map.Entry<JobForTeacher, StatusMaster> entry : actionMap.entrySet()) {
						System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
						JobForTeacher jft = entry.getKey();
						StatusMaster sm = entry.getValue();
						jft.setStatus(sm);
						jft.setStatusMaster(sm);
						// statelesSsession.update(jft);
					}
					txOpen.commit();
					statelesSsession.close();
					actionMap = null;
					jbIdListExisting = null;
					jbIdListNew = null;
					lstJobForTeacher = null;
				}
			}
			
		} catch(Exception exception){
			exception.printStackTrace();
		}
		
		return null;
	}
	
	//====================================
	public void setVluInteacherStatusHistoryForJob(Map<JobForTeacher, StatusMaster> jftSm){
		System.out.println("===========================nnnnnnn========================== in the setVluInteacherStatusHistoryForJob method============================================================================================");
		SessionFactory sessionFactory=teacherStatusHistoryForJobDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
		Transaction txOpen =statelesSsession.beginTransaction();
		List<Integer> lsttchdetailId=new ArrayList<Integer>();
		List<Integer> lstjobOrderId=new ArrayList<Integer>();
		List<Integer> lstStatusId=new ArrayList<Integer>();
		List<TeacherStatusHistoryForJob>  lstteacherStatusHistoryForJob=null;
		WithdrawnReasonMaster withdrawnReasonMaster=null;
		List<JobForTeacher> jft=new ArrayList<JobForTeacher>();
		
		System.out.println("=======================nnnnnnnnn===============  size of   Map<JobForTeacher, StatusMaster> jftSm  ===============================================================================  "+jftSm.size());
			if(jftSm.size()>0){
				for (Map.Entry<JobForTeacher, StatusMaster> entry : jftSm.entrySet()) {
					System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
					lsttchdetailId.add(entry.getKey().getTeacherId().getTeacherId());
					lstjobOrderId.add(entry.getKey().getJobId().getJobId());   
					lstStatusId.add(entry.getValue().getStatusId()) ;
				}
				lstteacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.fatchDataTeacherStatusHistoryForJob(lsttchdetailId,lstjobOrderId,lstStatusId,"A");
				withdrawnReasonMaster=withdrawnReasonMasterDAO.findbydistrictIdIsNullInactiveRecord();
			}
			System.out.println("===================nnnnnnnn===================  size of   lstteacherStatusHistoryForJob  ===============================================================================  "+lstteacherStatusHistoryForJob.size());
			try
			{
				if(jftSm.size()>0){
					txOpen =statelesSsession.beginTransaction();
					for (Map.Entry<JobForTeacher, StatusMaster> entry : jftSm.entrySet()) {
						JobForTeacher jft1=entry.getKey();
						StatusMaster statusMasterObj=entry.getValue();
						if(lstteacherStatusHistoryForJob!=null && lstteacherStatusHistoryForJob.size()>0){
							System.out.println("if(lstteacherStatusHistoryForJob!=null && lstteacherStatusHistoryForJob.size()>0){:::::::::::::::::   if :::::::::  ");
							TeacherStatusHistoryForJob tshisForJob=null;
							int count=0;
							for(TeacherStatusHistoryForJob tSHJ1:lstteacherStatusHistoryForJob){
								boolean a = tSHJ1.getTeacherDetail().getTeacherId().toString().trim().equalsIgnoreCase(
								          jft1.getTeacherId().getTeacherId().toString().trim());
								boolean b = tSHJ1.getJobOrder().getJobId().toString().trim().equalsIgnoreCase(
								          jft1.getJobId().getJobId().toString().trim());
								boolean c =tSHJ1.getStatusMaster().getStatusId().toString().trim().equalsIgnoreCase(
								          statusMasterObj.getStatusId().toString().trim());

								if( a && b && c){
									System.out.println("======================nnnnnnnnn==========================for(TeacherStatusHistoryForJob tSHJ1:lstteacherStatusHistoryForJob){ ::::::::::::::::::: if :::::::::::::::::::::::::::");
									++count;
									tshisForJob=tSHJ1;
									break;
								}
							}
							if(count!=0 && tshisForJob!=null){
								UserMaster userMaster = new UserMaster();
								userMaster.setUserId(1);
								tshisForJob.setStatus("A");
								tshisForJob.setStatusMaster(statusMasterObj);
								tshisForJob.setUpdatedDateTime(new Date());
								tshisForJob.setUserMaster(userMaster);
								tshisForJob.setActiontakenByCandidate(true);
								tshisForJob.setJobOrder(jft1.getJobId());
								tshisForJob.setTeacherDetail(jft1.getTeacherId());
								if(statusMasterObj!=null && statusMasterObj.getStatusShortName().equals("rem"))
									tshisForJob.setWithdrawnReasonMaster(null);
								else
									tshisForJob.setWithdrawnReasonMaster(withdrawnReasonMaster);
								statelesSsession.update(tshisForJob);
								System.out.println("======================nnnnnnnnn==========================if(count!=0 && tshisForJob!=null){ ::::::::::::::::::: if :::::::::::::::::::::::::::");
							}else{
								TeacherStatusHistoryForJob tSHJ=new TeacherStatusHistoryForJob();
								UserMaster userMaster = new UserMaster();
								userMaster.setUserId(1);
								tSHJ.setStatus("A");
								tSHJ.setStatusMaster(statusMasterObj);
								tSHJ.setUpdatedDateTime(new Date());
								tSHJ.setUserMaster(userMaster);
								tSHJ.setActiontakenByCandidate(true);
								tSHJ.setJobOrder(jft1.getJobId());
								tSHJ.setTeacherDetail(jft1.getTeacherId());
								if(statusMasterObj!=null && statusMasterObj.getStatusShortName().equals("rem"))
									tSHJ.setWithdrawnReasonMaster(null);
								else
									tSHJ.setWithdrawnReasonMaster(withdrawnReasonMaster);
								statelesSsession.insert(tSHJ);
								System.out.println("====================nnnnnnnnn=============================if(count!=0 && tshisForJob!=null){ ::::::::::::::::::: else  :::::::::::::::::::::::::::");
							}
						}else{
							System.out.println("===================nnnnnnnnnn=========================if(lstteacherStatusHistoryForJob!=null && lstteacherStatusHistoryForJob.size()>0){:::::::::::::::::   else :::::::::  ");
							TeacherStatusHistoryForJob tSHJ=new TeacherStatusHistoryForJob();
							UserMaster userMaster = new UserMaster();
							userMaster.setUserId(1);
							tSHJ.setStatus("A");
							tSHJ.setStatusMaster(statusMasterObj);
							tSHJ.setUpdatedDateTime(new Date());
							tSHJ.setUserMaster(userMaster);
							tSHJ.setActiontakenByCandidate(true);
							tSHJ.setJobOrder(jft1.getJobId());
							tSHJ.setTeacherDetail(jft1.getTeacherId());
							if(statusMasterObj!=null && statusMasterObj.getStatusShortName().equals("rem"))
								tSHJ.setWithdrawnReasonMaster(null);
							else
								tSHJ.setWithdrawnReasonMaster(withdrawnReasonMaster);
							statelesSsession.insert(tSHJ);
							System.out.println("=========================================:::nnnnnnnn::::::::::::::::::::::::::::::::::::::::::::;end set value:   2  222222   :::::::::::::::::::::::::::");
						}
					}
					txOpen.commit();
					System.out.println("==============================================================::::::::::::::::::::::nnnnnnnnnnnnnn:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::commit the query::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
}

