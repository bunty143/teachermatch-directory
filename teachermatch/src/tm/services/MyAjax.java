package tm.services;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import tm.bean.master.CityMaster;
import tm.bean.master.StateMaster;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.StateMasterDAO;

public class MyAjax 
{
	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) 
	{
		this.cityMasterDAO = cityMasterDAO;
	}
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) 
	{
		this.stateMasterDAO = stateMasterDAO;
	}

	
	
	public List<CityMaster> getCityListByState(String stateId)
	{		
		StateMaster stateMaster = null;
		List<CityMaster> listCityMasters = null;
		try 
		{
			System.out.println("public List<CityMaster> getCityListByState(String stateId)"+stateId);
			stateMaster = stateMasterDAO.findById(Long.parseLong(stateId), false, false);
			listCityMasters = cityMasterDAO.findCityByState(stateMaster);
			System.out.println(listCityMasters);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return listCityMasters;
	}
	
	public StateMaster getStateByZipcode(String stateId)
	{
		System.out.println("public List<CityMaster> getCityByZipcode(String stateId)");
		return null;
	}



	
	

}


