package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.PanelAttendeesDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.DistrictNameCompratorASC;
import tm.utility.DistrictNameCompratorDESC;
import tm.utility.JobTitleCompratorASC;
import tm.utility.JobTitleCompratorDESC;
import tm.utility.LastNameCompratorASC;
import tm.utility.LastNameCompratorDESC;
import tm.utility.StafferNameCompratorASC;
import tm.utility.StafferNameCompratorDESC;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;




public class OfferReadyByStafferAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private PanelAttendeesDAO panelAttendeesDAO;
	
	@Autowired
	private PanelScheduleDAO panelScheduleDAO;
	
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	
	public String displayRecordsByEntityType(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,int status,String startDate,String endDate,String stafferFirstName,String  stafferLastName)
		{
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			boolean sortingcheck=false;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position

					int noOfRowInPage = Integer.parseInt(noOfRow);
					int pgNo = Integer.parseInt(pageNo);
					int start =((pgNo-1)*noOfRowInPage);
					int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
					int totalRecord = 0;
					//------------------------------------
				 /** set default sorting fieldName **/
					String sortOrderFieldName="distName";
					String sortOrderNoField="distName";

					if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("stafferName") || sortOrder.equalsIgnoreCase("distName")|| sortOrder.equalsIgnoreCase("schoolname") || sortOrder.equalsIgnoreCase("jobTitle") || sortOrder.equalsIgnoreCase("lastName") )
						sortingcheck=true;
					
					/**Start set dynamic sorting fieldName **/
					
					Order  sortOrderStrVal=null;

					if(sortOrder!=null){
						if(!sortOrder.equals("") && !sortOrder.equals(null)){
							sortOrderFieldName=sortOrder;
							sortOrderNoField=sortOrder;
						}
					}

					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("0")){
							sortOrderStrVal=Order.asc(sortOrderFieldName);
						}else{
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
						}
					}else{
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<JobForTeacher>	jobForTeacherLists    = new ArrayList<JobForTeacher>();
					boolean checkflag = false;
					
					List<JobOrder> lstJobOrders =null;
					System.out.println(":::::::::::::::: status ::::::::::::::::::::::"+status);
					if(entityID==2){
						//jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatus(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck);
						jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatusNew(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck,status,startDate,endDate);
						if(jobForTeacherLists.size()>0)
						checkflag = true;
					 }
					else if(entityID==1) 
					{
						if(districtOrSchoolId!=0)
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					       
						 //jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatus(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck);
						 jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatusNew(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck,status,startDate,endDate);
						 if(jobForTeacherLists.size()>0)
					        	checkflag = true;
						
					}
				
		      Map<String,List<PanelAttendees>> mapPanelAttendee = new HashMap<String,List<PanelAttendees>>();
		      List<TeacherDetail> lstTeacherDetailsOR=new ArrayList<TeacherDetail>();
			  if(jobForTeacherLists!=null && jobForTeacherLists.size()>0){
				List<JobOrder> lstjobOrdersOR = new ArrayList<JobOrder>(); 
				for(JobForTeacher jft : jobForTeacherLists){
					lstjobOrdersOR.add(jft.getJobId());
					lstTeacherDetailsOR.add(jft.getTeacherId());
				}
				List<PanelSchedule> lstPanelSchedules =panelScheduleDAO.findPanelScheduleListByTeachersAndJobs(lstTeacherDetailsOR,lstjobOrdersOR,districtMaster);
				
				System.out.println("lstPanelSchedules::::::"+lstPanelSchedules.size());
				List<PanelAttendees> lstPanelAttendees =new ArrayList<PanelAttendees>();
				if(lstPanelSchedules.size()>0){
					lstPanelAttendees =panelAttendeesDAO.getPanelAttendeeList(lstPanelSchedules);
				}
					
				if(lstPanelSchedules.size()>0)
					for(PanelSchedule ps : lstPanelSchedules)
					{
						List<PanelAttendees> lstPanelAttendee = new ArrayList<PanelAttendees>();
						for(PanelAttendees pa : lstPanelAttendees)
						{
							 if(pa.getPanelSchedule().getPanelId().equals(ps.getPanelId()))
							 {
								 lstPanelAttendee.add(pa);
							 }
						}
						 mapPanelAttendee.put(ps.getJobOrder().getJobId()+"#"+ps.getTeacherDetail().getTeacherId(), lstPanelAttendee);
					}
					
			}	
		 System.out.println("Final ************** jft ::::::::  "+jobForTeacherLists.size());	
		 
		/*List<JobForTeacher> filterPanelList=new ArrayList<JobForTeacher>();
		filterPanelList.addAll(jobForTeacherLists);
		if(filterPanelList.size()>0)
		 for (JobForTeacher jft : filterPanelList) {
			 String key=jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId();
			 if(mapPanelAttendee.get(key)==null){
				 jobForTeacherLists.remove(jft);
			 }
		}*/
		System.out.println("Final *********2***** jft ::::::::  "+jobForTeacherLists.size());	
		 
					 
		 if(schoolId!=0)
		 {
			List<JobForTeacher> filterSchoolJFT =new ArrayList<JobForTeacher>();
			 for(JobForTeacher jft : jobForTeacherLists)
			 {
				 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
				 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
					 for(PanelAttendees pa: lstPanelAttendee)
					 {
							 if(pa.getPanelInviteeId().getEntityType()==3)
							 {
								 if(pa.getPanelInviteeId().getSchoolId().getSchoolId()==schoolId){
									 filterSchoolJFT.add(jft);
								 }
							 }
					 }
				 }
			 } 
		   jobForTeacherLists.clear();	 
		   jobForTeacherLists.addAll(filterSchoolJFT);
		}
		 
		 
		 if(stafferFirstName.trim().length()>0){

				List<JobForTeacher> filterSchoolJFT =new ArrayList<JobForTeacher>();
				 for(JobForTeacher jft : jobForTeacherLists)
				 {
					 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
					 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
						 for(PanelAttendees pa: lstPanelAttendee)
						 {
								
							 if(pa.getPanelInviteeId().getEntityType()==2)
							 {
								 if(stafferFirstName.trim().length()>0){
									 String firstName=pa.getPanelInviteeId().getFirstName().trim().toUpperCase();
									 if(firstName.matches("(.*)"+stafferFirstName.trim().toUpperCase()+"(.*)")){
										 filterSchoolJFT.add(jft);
									 }
								 }

							 }
						 }
					 }
				 } 
			   jobForTeacherLists.clear();	 
			   jobForTeacherLists.addAll(filterSchoolJFT);
		   }
		 
		 if(stafferLastName.trim().length()>0){

				List<JobForTeacher> filterSchoolJFT =new ArrayList<JobForTeacher>();
				 for(JobForTeacher jft : jobForTeacherLists)
				 {
					 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
					 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
						 for(PanelAttendees pa: lstPanelAttendee)
						 {
							 if(pa.getPanelInviteeId().getEntityType()==2)
							 {
								 if(stafferLastName.trim().length()>0){
									 String lastName=pa.getPanelInviteeId().getLastName().trim().toUpperCase();
									 if(lastName.matches("(.*)"+stafferLastName.trim().toUpperCase()+"(.*)")){
										 filterSchoolJFT.add(jft);
									 }
								 }
							 }
						 } 
					 }
				 } 
			   jobForTeacherLists.clear();	 
			   jobForTeacherLists.addAll(filterSchoolJFT);
		   }
		 
		 List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		 
	    // sorting logic start 
	     
	     if(sortOrder.equals("")){
	    	 for(JobForTeacher jft:jobForTeacherLists){
	    		 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
	    		 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
		    		 for(PanelAttendees pa: lstPanelAttendee){
						 UserMaster userObj=pa.getPanelInviteeId();
						  if(userObj.getEntityType()==3){
							 jft.setDistName(pa.getPanelInviteeId().getSchoolId().getSchoolName());
						 }
					 }
	    		 }else{
	    			 jft.setDistName("");
	    		 }
	    	 } 
			 Collections.sort(jobForTeacherLists, new DistrictNameCompratorASC());
	     }
	     
	//job Title sortning
	     if(sortOrder.equals("stafferName")){  
		     for(JobForTeacher jft:jobForTeacherLists) 
			 {
		    	 
				 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
				 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
					 for(PanelAttendees pa: lstPanelAttendee){
							jft.setStafferName(pa.getPanelInviteeId().getLastName());
					 } 
				 }
			 }
		     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, new StafferNameCompratorASC());
				 else 
				  Collections.sort(jobForTeacherLists, new StafferNameCompratorDESC());
	     
	     }
	     
	     
		 if(sortOrder.equals("distName")){  
		     for(JobForTeacher jft:jobForTeacherLists) 
			 {
	    		 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
				 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
					 for(PanelAttendees pa: lstPanelAttendee){
						 UserMaster userObj=pa.getPanelInviteeId();
						  if(userObj.getEntityType()==3){
							 jft.setDistName(pa.getPanelInviteeId().getSchoolId().getSchoolName());
						 }
					 }
				 }
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherLists, new DistrictNameCompratorASC());
			 else 
			  Collections.sort(jobForTeacherLists, new DistrictNameCompratorDESC());
	     } 
		 
		 if(sortOrder.equals("lastName")){    
		     for(JobForTeacher jf:jobForTeacherLists) 
			 {
		    	 jf.setLastName(jf.getTeacherId().getLastName());
				
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherLists, new LastNameCompratorASC());
			 else 
			  Collections.sort(jobForTeacherLists, new LastNameCompratorDESC());
	     }  
	     if(sortOrder.equals("jobTitle")){    
		     for(JobForTeacher jf:jobForTeacherLists) 
			 {
		    	 jf.setJobTitle(jf.getJobId().getJobTitle());
			 }
		     if(sortOrderType.equals("0"))
			  Collections.sort(jobForTeacherLists, new JobTitleCompratorASC());
			 else 
			  Collections.sort(jobForTeacherLists, new JobTitleCompratorDESC());
	     } 

			   System.out.println("xxxxxxxxxxxxxjobForTeacherLists :: "+jobForTeacherLists.size());	
			
			   totalRecord=jobForTeacherLists.size();
				if(totalRecord<end)
					end=totalRecord;
			   lstJobForTeacher	=	jobForTeacherLists.subList(start,end);
			   
			   /*	
			    *  Create Map For OfferReady Date
			    * 
			    * */
			   List<SecondaryStatus> secondaryStatusList = null;
			   List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobLsit = null;
			   Map<String,TeacherStatusHistoryForJob> maptHist = new HashMap<String, TeacherStatusHistoryForJob>();
			   System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			   try{
				   secondaryStatusList =  secondaryStatusDAO.getOfferReadyByDistrict(districtMaster);
				   if(secondaryStatusList!=null)
					   teacherStatusHistoryForJobLsit = teacherStatusHistoryForJobDAO.findByTeacherSecondaryStatus(lstTeacherDetailsOR,secondaryStatusList);
				   
				   if(teacherStatusHistoryForJobLsit!=null && teacherStatusHistoryForJobLsit.size()>0){
					   for(TeacherStatusHistoryForJob jfth : teacherStatusHistoryForJobLsit){
						   String key = jfth.getTeacherDetail().getTeacherId()+"#"+jfth.getJobOrder().getJobId();
						   maptHist.put(key, jfth);
					   }
				   }
			   } catch(Exception exception){
				   exception.printStackTrace();
			   }
			   System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			   /*
			    * 	END OfferReady
			    * */
				
				String responseText="";
				
				tmRecords.append("<table  id='tblGridOfferReady' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgStafferName", locale),sortOrderNoField,"stafferName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("tableheaderSchool", locale),sortOrderNoField,"distName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgPositionNumber", locale),sortOrderNoField,"requisitionNumber",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblApplicantName", locale),sortOrderNoField,"lastName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderNoField,"jobTitle",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblStatus", locale),sortOrderFieldName,"offerReady",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+" </th>");
				
				//responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblStatus", locale),sortOrderFieldName,"offerReady",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>Offer Ready Date</th>");
				
			
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				tmRecords.append("<tbody>");
				
				if(lstJobForTeacher.size()==0){
				 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
				}
			
				if(lstJobForTeacher.size()>0){
				 for(JobForTeacher jft:lstJobForTeacher) 
				 {
					 tmRecords.append("<tr>");
					 
					 /*	
					  * 	Get OfferReady Date
					  * */
					 String key = jft.getTeacherId().getTeacherId()+"#"+jft.getJobId().getJobId();
					 TeacherStatusHistoryForJob dObj=maptHist.get(key);
					 
					 //System.out.println(jft.getTeacherId().getTeacherId()+"::::"+jft.getJobId().getJobId()+"::::"+dObj.getCreatedDateTime());
					 
					 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
					 
					 String schoolName="";
					 String stafferName="";
					 
				//	 System.out.println("jobId :: "+ jft.getJobId().getJobId()+"\tteacherId :: "+jft.getTeacherId().getTeacherId());
					 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
						 for(PanelAttendees pa: lstPanelAttendee){
							 UserMaster userObj=pa.getPanelInviteeId();
							 if(userObj.getEntityType()==2){
								 stafferName=pa.getPanelInviteeId().getFirstName()+" "+pa.getPanelInviteeId().getLastName();
							 }else  if(userObj.getEntityType()==3){
								 schoolName=pa.getPanelInviteeId().getSchoolId().getSchoolName();
							 }
						 }
					 }
					 
					tmRecords.append("<td>"+stafferName+"</td>");
					 
					tmRecords.append("<td>"+schoolName+"</td>");
					tmRecords.append("<td>"+jft.getRequisitionNumber()+"</td>");
					 
					 tmRecords.append("<td>"+jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")"+"</td>");
					 tmRecords.append("<td>"+jft.getJobId().getJobTitle()+"</td>");
					 
					if(jft.getOfferReady())
					  tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblCompleted", locale)+"</td>");
					 else
				     tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblPending", locale)+"</td>");
					 
					if(jft.getOfferMadeDate()!=null)
						tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getOfferMadeDate())+"</td>");
					else
						tmRecords.append("<td> </td>");
					
				   tmRecords.append("</tr>");
				 }
				}
			tmRecords.append("</tbody>");
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));

			}catch(Exception e){
				e.printStackTrace();
			}
			
		 return tmRecords.toString();
	  }
		
	public String displayRecordsByEntityTypeEXL(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,int status,String startDate,String endDate,String stafferFirstName,String  stafferLastName)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		boolean sortingcheck=false;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="distName";
				String sortOrderNoField="distName";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("stafferName") || sortOrder.equalsIgnoreCase("distName")|| sortOrder.equalsIgnoreCase("schoolname") || sortOrder.equalsIgnoreCase("jobTitle") || sortOrder.equalsIgnoreCase("lastName") )
					sortingcheck=true;
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<JobForTeacher>	jobForTeacherLists    = new ArrayList<JobForTeacher>();
				boolean checkflag = false;
				
				List<JobOrder> lstJobOrders =null;
				if(entityID==2){
					//jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatus(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck);
					jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatusNew(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck,status,startDate,endDate);
					if(jobForTeacherLists.size()>0)
					checkflag = true;
				 }
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0)
					 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
				       
					 //jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatus(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck);
					 jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatusNew(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck,status,startDate,endDate);
					 if(jobForTeacherLists.size()>0)
				        	checkflag = true;
					
				}
			
	      Map<String,List<PanelAttendees>> mapPanelAttendee = new HashMap<String,List<PanelAttendees>>();
	      List<TeacherDetail> lstTeacherDetailsOR=new ArrayList<TeacherDetail>();
		  if(jobForTeacherLists!=null && jobForTeacherLists.size()>0){
			List<JobOrder> lstjobOrdersOR = new ArrayList<JobOrder>(); 
			for(JobForTeacher jft : jobForTeacherLists){
				lstjobOrdersOR.add(jft.getJobId());
				lstTeacherDetailsOR.add(jft.getTeacherId());
			}
			List<PanelSchedule> lstPanelSchedules =panelScheduleDAO.findPanelScheduleListByTeachersAndJobs(lstTeacherDetailsOR,lstjobOrdersOR,districtMaster);
			List<PanelAttendees> lstPanelAttendees =panelAttendeesDAO.getPanelAttendeeList(lstPanelSchedules);
				
			//PanelSchedule ps : lstPanelSchedules
			System.out.println("lstPanelSchedules .size()   :: "+lstPanelSchedules.size());
				for(PanelSchedule ps : lstPanelSchedules)
				{
					List<PanelAttendees> lstPanelAttendee = new ArrayList<PanelAttendees>();
					for(PanelAttendees pa : lstPanelAttendees)
					{
						 if(pa.getPanelSchedule().getPanelId().equals(ps.getPanelId()))
						 {
							 lstPanelAttendee.add(pa);
						 }
					}
					 mapPanelAttendee.put(ps.getJobOrder().getJobId()+"#"+ps.getTeacherDetail().getTeacherId(), lstPanelAttendee);
				}
				
		}	
	 System.out.println("Final ************** jft ::::::::  "+jobForTeacherLists.size());	
	
	 	/*List<JobForTeacher> filterPanelList=new ArrayList<JobForTeacher>();
		filterPanelList.addAll(jobForTeacherLists);
		if(filterPanelList.size()>0)
		 for (JobForTeacher jft : filterPanelList) {
			 String key=jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId();
			 if(mapPanelAttendee.get(key)==null){
				 jobForTeacherLists.remove(jft);
			 }
		}*/
		System.out.println("Final *********2***** jft ::::::::  "+jobForTeacherLists.size());	
	 
	 
	 if(schoolId!=0)
	 {
		List<JobForTeacher> filterSchoolJFT =new ArrayList<JobForTeacher>();
		 for(JobForTeacher jft : jobForTeacherLists)
		 {
			 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
			 
			 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
				 for(PanelAttendees pa: lstPanelAttendee)
				 {
						if(pa.getPanelInviteeId().getEntityType()==3)
						 {
							 if(pa.getPanelInviteeId().getSchoolId().getSchoolId()==schoolId){
								 filterSchoolJFT.add(jft);
							 }
						 }
				 }
			 }
		 } 
	   jobForTeacherLists.clear();	 
	   jobForTeacherLists.addAll(filterSchoolJFT);
		}		 
		
	 if(stafferFirstName.trim().length()>0){

			List<JobForTeacher> filterSchoolJFT =new ArrayList<JobForTeacher>();
			 for(JobForTeacher jft : jobForTeacherLists)
			 {
				 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
				 
				 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
					 for(PanelAttendees pa: lstPanelAttendee)
					 {
							
						 if(pa.getPanelInviteeId().getEntityType()==2)
						 {
							 if(stafferFirstName.trim().length()>0){
								 String firstName=pa.getPanelInviteeId().getFirstName().trim().toUpperCase();
								 if(firstName.matches("(.*)"+stafferFirstName.trim().toUpperCase()+"(.*)")){
									 filterSchoolJFT.add(jft);
								 }
							 }

						 }
					 }
				 }
			 } 
		   jobForTeacherLists.clear();	 
		   jobForTeacherLists.addAll(filterSchoolJFT);
	   }
	 
	 if(stafferLastName.trim().length()>0){

			List<JobForTeacher> filterSchoolJFT =new ArrayList<JobForTeacher>();
			 for(JobForTeacher jft : jobForTeacherLists)
			 {
				 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
				 
				 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
					 for(PanelAttendees pa: lstPanelAttendee)
					 {
						 if(pa.getPanelInviteeId().getEntityType()==2)
						 {
							 if(stafferLastName.trim().length()>0){
								 String lastName=pa.getPanelInviteeId().getLastName().trim().toUpperCase();
								 if(lastName.matches("(.*)"+stafferLastName.trim().toUpperCase()+"(.*)")){
									 filterSchoolJFT.add(jft);
								 }
							 }
						 }
					 }
				 }
			 } 
		   jobForTeacherLists.clear();	 
		   jobForTeacherLists.addAll(filterSchoolJFT);
	   }
	 
			    // sorting logic start 
			     
			     if(sortOrder.equals("")){
			    	 for(JobForTeacher jft:jobForTeacherLists){
			    		 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
						 
			    		 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
				    		 for(PanelAttendees pa: lstPanelAttendee){
								 UserMaster userObj=pa.getPanelInviteeId();
								  if(userObj.getEntityType()==3){
									 jft.setDistName(pa.getPanelInviteeId().getSchoolId().getSchoolName());
								 }
							 }
			    		 }
			    	 } 
				    	 
					
					  Collections.sort(jobForTeacherLists, new DistrictNameCompratorASC());
			     }
			     
			//job Title sortning
			     if(sortOrder.equals("stafferName")){  
				     for(JobForTeacher jft:jobForTeacherLists) 
					 {
				    	 
						 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
						 
						 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
							 for(PanelAttendees pa: lstPanelAttendee){
									jft.setStafferName(pa.getPanelInviteeId().getLastName());
							 }
						 }
					 }
				     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new StafferNameCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new StafferNameCompratorDESC());
			     
			     }
			     
			     
				 if(sortOrder.equals("distName")){  
				     for(JobForTeacher jft:jobForTeacherLists) 
					 {
			    		 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
						 
			    		 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
				    		 for(PanelAttendees pa: lstPanelAttendee){
								 UserMaster userObj=pa.getPanelInviteeId();
								  if(userObj.getEntityType()==3){
									 jft.setDistName(pa.getPanelInviteeId().getSchoolId().getSchoolName());
								 }
							 }
			    		 }
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new DistrictNameCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new DistrictNameCompratorDESC());
			     } 
				 
				 if(sortOrder.equals("lastName")){    
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setLastName(jf.getTeacherId().getLastName());
						
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new LastNameCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new LastNameCompratorDESC());
			     }  
			     if(sortOrder.equals("jobTitle")){    
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setJobTitle(jf.getJobId().getJobTitle());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new JobTitleCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new JobTitleCompratorDESC());
			     } 
		
			     
			       /*	
				    *  Create Map For OfferReady Date
				    * 
				    * */
				   List<SecondaryStatus> secondaryStatusList = null;
				   List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobLsit = null;
				   Map<String,TeacherStatusHistoryForJob> maptHist = new HashMap<String, TeacherStatusHistoryForJob>();
				   System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::");
				   try{
					   secondaryStatusList =  secondaryStatusDAO.getOfferReadyByDistrict(districtMaster);
					   if(secondaryStatusList!=null)
						   teacherStatusHistoryForJobLsit = teacherStatusHistoryForJobDAO.findByTeacherSecondaryStatus(lstTeacherDetailsOR,secondaryStatusList);
					   
					   if(teacherStatusHistoryForJobLsit!=null && teacherStatusHistoryForJobLsit.size()>0){
						   for(TeacherStatusHistoryForJob jfth : teacherStatusHistoryForJobLsit){
							   String key = jfth.getTeacherDetail().getTeacherId()+"#"+jfth.getJobOrder().getJobId();
							   maptHist.put(key, jfth);
						   }
					   }
				   } catch(Exception exception){
					   exception.printStackTrace();
				   }
				   System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::");
				   /*
				    * 	END OfferReady
				    * */
			     
			   //  Excel   Exporting	
					
					String time = String.valueOf(System.currentTimeMillis()).substring(6);
					//String basePath = request.getRealPath("/")+"/candidate";
					String basePath = request.getSession().getServletContext().getRealPath ("/")+"/offerreadybystaffer";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="offerreadybystaffer"+time+".xls";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

					WorkbookSettings wbSettings = new WorkbookSettings();

					wbSettings.setLocale(new Locale("en", "EN"));

					WritableCellFormat timesBoldUnderline;
					WritableCellFormat header;
					WritableCellFormat headerBold;
					WritableCellFormat times;

					WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
					workbook.createSheet("offerreadybystaffer", 0);
					WritableSheet excelSheet = workbook.getSheet(0);

					WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
					// Define the cell format
					times = new WritableCellFormat(times10pt);
					// Lets automatically wrap the cells
					times.setWrap(true);

					// Create create a bold font with unterlines
					WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
					WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

					timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
					timesBoldUnderline.setAlignment(Alignment.CENTRE);
					// Lets automatically wrap the cells
					timesBoldUnderline.setWrap(true);

					header = new WritableCellFormat(times10ptBoldUnderline);
					headerBold = new WritableCellFormat(times10ptBoldUnderline);
					CellView cv = new CellView();
					cv.setFormat(times);
					cv.setFormat(timesBoldUnderline);
					cv.setAutosize(true);

					header.setBackground(Colour.GRAY_25);

					// Write a few headers
					excelSheet.mergeCells(0, 0, 5, 1);
					Label label;
					label = new Label(0, 0, Utility.getLocaleValuePropByKey("headOfferReByStaffer", locale), timesBoldUnderline);
					excelSheet.addCell(label);
					excelSheet.mergeCells(0, 3, 5, 3);
					label = new Label(0, 3, "");
					excelSheet.addCell(label);
					excelSheet.getSettings().setDefaultColumnWidth(18);
					
					int k=4;
					int col=1;
					label = new Label(0, k, Utility.getLocaleValuePropByKey("msgStafferName", locale),header); 
					excelSheet.addCell(label);
					label = new Label(1, k, Utility.getLocaleValuePropByKey("lblSchoolName", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgPositionNumber", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblApplicantName", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJoTil", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblStatus", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblOfferReadyDate", locale),header); 
					excelSheet.addCell(label);
					
					k=k+1;
					if(jobForTeacherLists.size()==0)
					{	
						excelSheet.mergeCells(0, k, 8, k);
						label = new Label(0, k, Utility.getLocaleValuePropByKey("msgNorecordfound", locale)); 
						excelSheet.addCell(label);
					}
					
			if(jobForTeacherLists.size()>0){
			 for(JobForTeacher jft:jobForTeacherLists) 
			 {
				 
				 /*	
				  * 	Get OfferReady Date
				  * */
				 String key = jft.getTeacherId().getTeacherId()+"#"+jft.getJobId().getJobId();
				 TeacherStatusHistoryForJob dObj=maptHist.get(key);
				 
				 col=1;
				 
				 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
				 
				 String schoolName="";
				 String stafferName="";
				 
				 for(PanelAttendees pa: lstPanelAttendee){
					 UserMaster userObj=pa.getPanelInviteeId();
					 if(userObj.getEntityType()==2){
						 stafferName=pa.getPanelInviteeId().getFirstName()+" "+pa.getPanelInviteeId().getLastName();
					 }else  if(userObj.getEntityType()==3){
						 schoolName=pa.getPanelInviteeId().getSchoolId().getSchoolName();
					 }
				 }
				 
				 label = new Label(0, k, stafferName); 
					excelSheet.addCell(label);
				 
					
				label = new Label(1, k,schoolName); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, jft.getRequisitionNumber()); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")"); 
				excelSheet.addCell(label);
				 
				label = new Label(++col, k, jft.getJobId().getJobTitle()); 
				excelSheet.addCell(label);
				 
				 
				if(jft.getOfferReady())
				{
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblCompleted", locale)); 
					excelSheet.addCell(label);
				}
				 else{
					 label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblPending", locale)); 
						excelSheet.addCell(label);
				 }
				if(jft.getOfferMadeDate()!=null)
					label = new Label(++col, k, Utility.convertDateAndTimeToUSformatOnlyDate(jft.getOfferMadeDate()));
				else
					label = new Label(++col, k, "");

					excelSheet.addCell(label);

				k++;
			 }
			}
	
			workbook.write();
			workbook.close();
		}catch(Exception e){}
		
	 return fileName;
  }
	
  
	
	public String displayRecordsByEntityTypePDF(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,int status,String startDate,String endDate,String stafferFirstName,String  stafferLastName)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport( districtOrSchoolId, schoolId, noOfRow, pageNo, sortOrder, sortOrderType,basePath+"/"+fileName,context.getServletContext().getRealPath("/"),status,startDate,endDate,stafferFirstName,stafferLastName);
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	public boolean generateCandidatePDReport(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String path,String realPath,int status,String startDate,String endDate,String stafferFirstName,String stafferLastName){
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			String fileName = null;
			boolean sortingcheck=false;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position

					int noOfRowInPage = Integer.parseInt(noOfRow);
					int pgNo = Integer.parseInt(pageNo);
					int start =((pgNo-1)*noOfRowInPage);
					int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
					int totalRecord = 0;
					//------------------------------------
				 /** set default sorting fieldName **/
					String sortOrderFieldName="distName";
					String sortOrderNoField="distName";

					if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("stafferName") || sortOrder.equalsIgnoreCase("distName")|| sortOrder.equalsIgnoreCase("schoolname") || sortOrder.equalsIgnoreCase("jobTitle") || sortOrder.equalsIgnoreCase("lastName") )
						sortingcheck=true;
					
					/**Start set dynamic sorting fieldName **/
					
					Order  sortOrderStrVal=null;

					if(sortOrder!=null){
						if(!sortOrder.equals("") && !sortOrder.equals(null)){
							sortOrderFieldName=sortOrder;
							sortOrderNoField=sortOrder;
						}
					}

					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("0")){
							sortOrderStrVal=Order.asc(sortOrderFieldName);
						}else{
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
						}
					}else{
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<JobForTeacher>	jobForTeacherLists    = new ArrayList<JobForTeacher>();
					boolean checkflag = false;
					
					List<JobOrder> lstJobOrders =null;
					if(entityID==2){
						//jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatus(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck);
						jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatusNew(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck,status,startDate,endDate);
						if(jobForTeacherLists.size()>0)
						checkflag = true;
					 }
					else if(entityID==1) 
					{
						if(districtOrSchoolId!=0)
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					       
						 //jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatus(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck);
						 jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatusNew(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck,status,startDate,endDate);
						 if(jobForTeacherLists.size()>0)
					        	checkflag = true;
						
					}
				
		      Map<String,List<PanelAttendees>> mapPanelAttendee = new HashMap<String,List<PanelAttendees>>();
		      List<TeacherDetail> lstTeacherDetailsOR=new ArrayList<TeacherDetail>();
			  if(jobForTeacherLists!=null && jobForTeacherLists.size()>0){
				
				List<JobOrder> lstjobOrdersOR = new ArrayList<JobOrder>(); 
				for(JobForTeacher jft : jobForTeacherLists){
					lstjobOrdersOR.add(jft.getJobId());
					lstTeacherDetailsOR.add(jft.getTeacherId());
				}
				List<PanelSchedule> lstPanelSchedules =panelScheduleDAO.findPanelScheduleListByTeachersAndJobs(lstTeacherDetailsOR,lstjobOrdersOR,districtMaster);
				List<PanelAttendees> lstPanelAttendees =panelAttendeesDAO.getPanelAttendeeList(lstPanelSchedules);
					
				//PanelSchedule ps : lstPanelSchedules
				System.out.println("lstPanelSchedules .size()   :: "+lstPanelSchedules.size());
					for(PanelSchedule ps : lstPanelSchedules)
					{
						List<PanelAttendees> lstPanelAttendee = new ArrayList<PanelAttendees>();
						for(PanelAttendees pa : lstPanelAttendees)
						{
							 if(pa.getPanelSchedule().getPanelId().equals(ps.getPanelId()))
							 {
								 lstPanelAttendee.add(pa);
							 }
						}
						 mapPanelAttendee.put(ps.getJobOrder().getJobId()+"#"+ps.getTeacherDetail().getTeacherId(), lstPanelAttendee);
					}
					
			}	
		 System.out.println("Final ************** jft ::::::::  "+jobForTeacherLists.size());	
		 
		 	/*List<JobForTeacher> filterPanelList=new ArrayList<JobForTeacher>();
			filterPanelList.addAll(jobForTeacherLists);
			if(filterPanelList.size()>0)
			 for (JobForTeacher jft : filterPanelList) {
				 String key=jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId();
				 if(mapPanelAttendee.get(key)==null){
					 jobForTeacherLists.remove(jft);
				 }
			}*/
			System.out.println("Final *********2***** jft ::::::::  "+jobForTeacherLists.size());	
					 
		 if(schoolId!=0)
		 {
			List<JobForTeacher> filterSchoolJFT =new ArrayList<JobForTeacher>();
			 for(JobForTeacher jft : jobForTeacherLists)
			 {
				 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
				 
				 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
					 for(PanelAttendees pa: lstPanelAttendee)
					 {
							if(pa.getPanelInviteeId().getEntityType()==3)
							 {
								 if(pa.getPanelInviteeId().getSchoolId().getSchoolId()==schoolId){
									 filterSchoolJFT.add(jft);
								 }
							 }
					 }
				 }
			 } 
		   jobForTeacherLists.clear();	 
		   jobForTeacherLists.addAll(filterSchoolJFT);
			}	
		 
		 if(stafferFirstName.trim().length()>0){

				List<JobForTeacher> filterSchoolJFT =new ArrayList<JobForTeacher>();
				 for(JobForTeacher jft : jobForTeacherLists)
				 {
					 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
					 
					 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
						 for(PanelAttendees pa: lstPanelAttendee)
						 {
								
							 if(pa.getPanelInviteeId().getEntityType()==2)
							 {
								 if(stafferFirstName.trim().length()>0){
									 String firstName=pa.getPanelInviteeId().getFirstName().trim().toUpperCase();
									 if(firstName.matches("(.*)"+stafferFirstName.trim().toUpperCase()+"(.*)")){
										 filterSchoolJFT.add(jft);
									 }
								 }

							 }
						 }
					 }
				 } 
			   jobForTeacherLists.clear();	 
			   jobForTeacherLists.addAll(filterSchoolJFT);
		   }
		 
		 if(stafferLastName.trim().length()>0){

				List<JobForTeacher> filterSchoolJFT =new ArrayList<JobForTeacher>();
				 for(JobForTeacher jft : jobForTeacherLists)
				 {
					 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
					 
					 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
						 for(PanelAttendees pa: lstPanelAttendee)
						 {
							 if(pa.getPanelInviteeId().getEntityType()==2)
							 {
								 if(stafferLastName.trim().length()>0){
									 String lastName=pa.getPanelInviteeId().getLastName().trim().toUpperCase();
									 if(lastName.matches("(.*)"+stafferLastName.trim().toUpperCase()+"(.*)")){
										 filterSchoolJFT.add(jft);
									 }
								 }
							 }
						 }
					 }
				 } 
			   jobForTeacherLists.clear();	 
			   jobForTeacherLists.addAll(filterSchoolJFT);
		   }
		 
		 
				    // sorting logic start 
				     
				     if(sortOrder.equals("")){
				    	 for(JobForTeacher jft:jobForTeacherLists){
				    		 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
			    		 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
				    		 for(PanelAttendees pa: lstPanelAttendee){
								 UserMaster userObj=pa.getPanelInviteeId();
								  if(userObj.getEntityType()==3){
									 jft.setDistName(pa.getPanelInviteeId().getSchoolId().getSchoolName());
								 }
							  }
				    		 }
				    		 
				    	 } 
					    	 
						
						  Collections.sort(jobForTeacherLists, new DistrictNameCompratorASC());
				     }
				     
				//job Title sortning
				     if(sortOrder.equals("stafferName")){  
					     for(JobForTeacher jft:jobForTeacherLists) 
						 {
					    	 
							 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
							 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
								 for(PanelAttendees pa: lstPanelAttendee){
										jft.setStafferName(pa.getPanelInviteeId().getLastName());
								 }
							 }
						 }
					     if(sortOrderType.equals("0"))
							  Collections.sort(jobForTeacherLists, new StafferNameCompratorASC());
							 else 
							  Collections.sort(jobForTeacherLists, new StafferNameCompratorDESC());
				     
				     }
				     
				     
					 if(sortOrder.equals("distName")){  
					     for(JobForTeacher jft:jobForTeacherLists) 
						 {
				    		 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
				    		 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
				    		 for(PanelAttendees pa: lstPanelAttendee){
								 UserMaster userObj=pa.getPanelInviteeId();
								  if(userObj.getEntityType()==3){
									 jft.setDistName(pa.getPanelInviteeId().getSchoolId().getSchoolName());
								 }
							 }
				    		}
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new DistrictNameCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new DistrictNameCompratorDESC());
				     } 
					 
					 if(sortOrder.equals("lastName")){    
					     for(JobForTeacher jf:jobForTeacherLists) 
						 {
					    	 jf.setLastName(jf.getTeacherId().getLastName());
							
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new LastNameCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new LastNameCompratorDESC());
				     }  
				     if(sortOrder.equals("jobTitle")){    
					     for(JobForTeacher jf:jobForTeacherLists) 
						 {
					    	 jf.setJobTitle(jf.getJobId().getJobTitle());
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new JobTitleCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new JobTitleCompratorDESC());
				     } 
				     String fontPath = realPath;
				     
				     /*	
					    *  Create Map For OfferReady Date
					    * 
					    * */
					   List<SecondaryStatus> secondaryStatusList = null;
					   List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobLsit = null;
					   Map<String,TeacherStatusHistoryForJob> maptHist = new HashMap<String, TeacherStatusHistoryForJob>();
					   System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::");
					   try{
						   secondaryStatusList =  secondaryStatusDAO.getOfferReadyByDistrict(districtMaster);
						   if(secondaryStatusList!=null)
							   teacherStatusHistoryForJobLsit = teacherStatusHistoryForJobDAO.findByTeacherSecondaryStatus(lstTeacherDetailsOR,secondaryStatusList);
						   
						   if(teacherStatusHistoryForJobLsit!=null && teacherStatusHistoryForJobLsit.size()>0){
							   for(TeacherStatusHistoryForJob jfth : teacherStatusHistoryForJobLsit){
								   String key = jfth.getTeacherDetail().getTeacherId()+"#"+jfth.getJobOrder().getJobId();
								   maptHist.put(key, jfth);
							   }
						   }
					   } catch(Exception exception){
						   exception.printStackTrace();
					   }
					   System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::");
					   /*
					    * 	END OfferReady
					    * */
				     
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject(Utility.getLocaleValuePropByKey("headOfferReByStaffer", locale));
						document.addCreationDate();
						document.addTitle(Utility.getLocaleValuePropByKey("headOfferReByStaffer", locale));

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						

						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("headOfferReByStaffer", locale),font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						
						
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						
						
						document.add(new Phrase("\n"));


						float[] tblwidth={.12f,.20f,.08f,.18f,.21f,.08f,.12f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[7];
						cell = new PdfPCell[7];
				// header
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgStafferName", locale),font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[0].setBorder(1);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblSchoolName", locale),font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[1].setBorder(1);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgPositionNumber", locale),font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
					//	cell[2].setBorder(1);
						mainTable.addCell(cell[2]);

						para[3] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblApplicantName", locale),font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[3].setBorder(1);
						mainTable.addCell(cell[3]);
						
						para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJoTil", locale),font10_10);
						cell[4]= new PdfPCell(para[4]);
						cell[4].setBackgroundColor(bluecolor);
						cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
					//	cell[4].setBorder(1);
						mainTable.addCell(cell[4]);
						
						para[5] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblStatus", locale),font10_10);
						cell[5]= new PdfPCell(para[5]);
						cell[5].setBackgroundColor(bluecolor);
						cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[5].setBorder(1);
						mainTable.addCell(cell[5]);
						
						para[6] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblOfferReadyDate", locale),font10_10);
						cell[6]= new PdfPCell(para[6]);
						cell[6].setBackgroundColor(bluecolor);
						cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[5].setBorder(1);
						mainTable.addCell(cell[6]);
						
						document.add(mainTable);
						
						if(jobForTeacherLists.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgNorecordfound", locale),font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 //cell[index].setBorder(1);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
				     
						if(jobForTeacherLists.size()>0){
							 for(JobForTeacher jft:jobForTeacherLists) 
							 {
								
								 String key = jft.getTeacherId().getTeacherId()+"#"+jft.getJobId().getJobId();
								 TeacherStatusHistoryForJob dObj=maptHist.get(key);
								 
								 int index=0;
								 float[] tblwidth1={.12f,.20f,.08f,.18f,.21f,.08f,.12f};
									
								mainTable = new PdfPTable(tblwidth1);
								mainTable.setWidthPercentage(100);
								 para = new Paragraph[7];
								 cell = new PdfPCell[7];
								
								 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
								 
								 String schoolName="";
								 String stafferName="";
								 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0)
								 for(PanelAttendees pa: lstPanelAttendee){
									 UserMaster userObj=pa.getPanelInviteeId();
									 if(userObj.getEntityType()==2){
										 stafferName=pa.getPanelInviteeId().getFirstName()+" "+pa.getPanelInviteeId().getLastName();
									 }else  if(userObj.getEntityType()==3){
										 schoolName=pa.getPanelInviteeId().getSchoolId().getSchoolName();
									 }
								 }
								 
								 para[index] = new Paragraph(""+stafferName,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 //cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 para[index] = new Paragraph(""+schoolName,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 //cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 para[index] = new Paragraph(""+jft.getRequisitionNumber(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 //cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 para[index] = new Paragraph(""+jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 //cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
								
								 para[index] = new Paragraph(""+jft.getJobId().getJobTitle(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 //cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								if(jft.getOfferReady())
								{
									 para[index] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCompleted", locale),font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 //cell[index].setBorder(1);
									 mainTable.addCell(cell[index]);
									 index++;
								}
								 else{
									 para[index] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblPending", locale),font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 //cell[index].setBorder(1);
									 mainTable.addCell(cell[index]);
									 index++;
								 }
								
								if(jft.getOfferMadeDate()!=null)
									para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getOfferMadeDate()),font8bold);
								else
									para[index] = new Paragraph("",font8bold);

								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 //cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
								
								document.add(mainTable);
							 }
						}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	
	public String displayRecordsByEntityTypePrintPreview(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,int status,String startDate,String endDate,String stafferFirstName,String stafferLastName)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		boolean sortingcheck=false;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="distName";
				String sortOrderNoField="distName";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("stafferName") || sortOrder.equalsIgnoreCase("distName")|| sortOrder.equalsIgnoreCase("schoolname") || sortOrder.equalsIgnoreCase("jobTitle") || sortOrder.equalsIgnoreCase("lastName") )
					sortingcheck=true;
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<JobForTeacher>	jobForTeacherLists    = new ArrayList<JobForTeacher>();
				boolean checkflag = false;
				
				List<JobOrder> lstJobOrders =null;
				if(entityID==2){
					jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatus(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck);
					if(jobForTeacherLists.size()>0)
					checkflag = true;
				 }
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0)
					 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
				       
					 jobForTeacherLists  =  jobForTeacherDAO.findJFTforOfferReadystatus(districtMaster,sortOrderStrVal,lstJobOrders,sortingcheck);
					 if(jobForTeacherLists.size()>0)
				        	checkflag = true;
					
				}
			
	      Map<String,List<PanelAttendees>> mapPanelAttendee = new HashMap<String,List<PanelAttendees>>();
	      List<TeacherDetail> lstTeacherDetailsOR=new ArrayList<TeacherDetail>();
		  if(jobForTeacherLists!=null && jobForTeacherLists.size()>0){
			
			List<JobOrder> lstjobOrdersOR = new ArrayList<JobOrder>(); 
			for(JobForTeacher jft : jobForTeacherLists){
				lstjobOrdersOR.add(jft.getJobId());
				lstTeacherDetailsOR.add(jft.getTeacherId());
			}
			List<PanelSchedule> lstPanelSchedules =panelScheduleDAO.findPanelScheduleListByTeachersAndJobs(lstTeacherDetailsOR,lstjobOrdersOR,districtMaster);
			List<PanelAttendees> lstPanelAttendees =panelAttendeesDAO.getPanelAttendeeList(lstPanelSchedules);
				
			//PanelSchedule ps : lstPanelSchedules
			System.out.println("lstPanelSchedules .size()   :: "+lstPanelSchedules.size());
				for(PanelSchedule ps : lstPanelSchedules)
				{
					List<PanelAttendees> lstPanelAttendee = new ArrayList<PanelAttendees>();
					for(PanelAttendees pa : lstPanelAttendees)
					{
						 if(pa.getPanelSchedule().getPanelId().equals(ps.getPanelId()))
						 {
							 lstPanelAttendee.add(pa);
						 }
					}
					 mapPanelAttendee.put(ps.getJobOrder().getJobId()+"#"+ps.getTeacherDetail().getTeacherId(), lstPanelAttendee);
				}
				
		}	
	 System.out.println("Final ************** jft ::::::::  "+jobForTeacherLists.size());	
	 
	 	/*List<JobForTeacher> filterPanelList=new ArrayList<JobForTeacher>();
		filterPanelList.addAll(jobForTeacherLists);
		if(filterPanelList.size()>0)
		 for (JobForTeacher jft : filterPanelList) {
			 String key=jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId();
			 if(mapPanelAttendee.get(key)==null){
				 jobForTeacherLists.remove(jft);
			 }
		}*/
		System.out.println("Final *********2***** jft ::::::::  "+jobForTeacherLists.size());	
				 
	 if(schoolId!=0)
	 {
		List<JobForTeacher> filterSchoolJFT =new ArrayList<JobForTeacher>();
		 for(JobForTeacher jft : jobForTeacherLists)
		 {
			 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
			 
			 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
			 for(PanelAttendees pa: lstPanelAttendee)
			 {
					if(pa.getPanelInviteeId().getEntityType()==3)
					 {
						 if(pa.getPanelInviteeId().getSchoolId().getSchoolId()==schoolId){
							 filterSchoolJFT.add(jft);
						 }
					 }
			  }
			 }
		 } 
	   jobForTeacherLists.clear();	 
	   jobForTeacherLists.addAll(filterSchoolJFT);
		}		
	 
	 
	 if(stafferFirstName.trim().length()>0){

			List<JobForTeacher> filterSchoolJFT =new ArrayList<JobForTeacher>();
			 for(JobForTeacher jft : jobForTeacherLists)
			 {
				 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
				 
				 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
				 for(PanelAttendees pa: lstPanelAttendee)
				 {
						
					 if(pa.getPanelInviteeId().getEntityType()==2)
					 {
						 if(stafferFirstName.trim().length()>0){
							 String firstName=pa.getPanelInviteeId().getFirstName().trim().toUpperCase();
							 if(firstName.matches("(.*)"+stafferFirstName.trim().toUpperCase()+"(.*)")){
								 filterSchoolJFT.add(jft);
							 }
						 }

					 }
				 }
				 }
			 } 
		   jobForTeacherLists.clear();	 
		   jobForTeacherLists.addAll(filterSchoolJFT);
	   }
	 
	 if(stafferLastName.trim().length()>0){

			List<JobForTeacher> filterSchoolJFT =new ArrayList<JobForTeacher>();
			 for(JobForTeacher jft : jobForTeacherLists)
			 {
				 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
				 
				 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
				 for(PanelAttendees pa: lstPanelAttendee)
				 {
					 if(pa.getPanelInviteeId().getEntityType()==2)
					 {
						 if(stafferLastName.trim().length()>0){
							 String lastName=pa.getPanelInviteeId().getLastName().trim().toUpperCase();
							 if(lastName.matches("(.*)"+stafferLastName.trim().toUpperCase()+"(.*)")){
								 filterSchoolJFT.add(jft);
							 }
						 }
					 }
				 }
				 }
			 } 
		   jobForTeacherLists.clear();	 
		   jobForTeacherLists.addAll(filterSchoolJFT);
	   }
	 
	 
			    // sorting logic start 
			     if(sortOrder.equals("")){
			    	 for(JobForTeacher jft:jobForTeacherLists){
			    		 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
			    		 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
				    		 for(PanelAttendees pa: lstPanelAttendee){
								 UserMaster userObj=pa.getPanelInviteeId();
								  if(userObj.getEntityType()==3){
									 jft.setDistName(pa.getPanelInviteeId().getSchoolId().getSchoolName());
								 }
							 }
			    	   }
			    	 } 
				    	 
					
					  Collections.sort(jobForTeacherLists, new DistrictNameCompratorASC());
			     }
			     
			//job Title sortning
			     if(sortOrder.equals("stafferName")){  
				     for(JobForTeacher jft:jobForTeacherLists) 
					 {
				    	 
						 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
						 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
							 for(PanelAttendees pa: lstPanelAttendee){
									jft.setStafferName(pa.getPanelInviteeId().getLastName());
							 }
						 }
					 }
				     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new StafferNameCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new StafferNameCompratorDESC());
			     
			     }
			     
			     
				 if(sortOrder.equals("distName")){  
				     for(JobForTeacher jft:jobForTeacherLists) 
					 {
			    		 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
			    		 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0){
				    		 for(PanelAttendees pa: lstPanelAttendee){
								 UserMaster userObj=pa.getPanelInviteeId();
								  if(userObj.getEntityType()==3){
									 jft.setDistName(pa.getPanelInviteeId().getSchoolId().getSchoolName());
								 }
							 }
			    		 }
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new DistrictNameCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new DistrictNameCompratorDESC());
			     } 
				 
				 if(sortOrder.equals("lastName")){    
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setLastName(jf.getTeacherId().getLastName());
						
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new LastNameCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new LastNameCompratorDESC());
			     }  
			     if(sortOrder.equals("jobTitle")){    
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setJobTitle(jf.getJobId().getJobTitle());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new JobTitleCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new JobTitleCompratorDESC());
			     } 
		
		   System.out.println("xxxxxxxxxxxxxjobForTeacherLists :: "+jobForTeacherLists.size());	
		   /*	
		    *  Create Map For OfferReady Date
		    * 
		    * */
		   List<SecondaryStatus> secondaryStatusList = null;
		   List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobLsit = null;
		   Map<String,TeacherStatusHistoryForJob> maptHist = new HashMap<String, TeacherStatusHistoryForJob>();
		   System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		   try{
			   secondaryStatusList =  secondaryStatusDAO.getOfferReadyByDistrict(districtMaster);
			   if(secondaryStatusList!=null)
				   teacherStatusHistoryForJobLsit = teacherStatusHistoryForJobDAO.findByTeacherSecondaryStatus(lstTeacherDetailsOR,secondaryStatusList);
			   
			   if(teacherStatusHistoryForJobLsit!=null && teacherStatusHistoryForJobLsit.size()>0){
				   for(TeacherStatusHistoryForJob jfth : teacherStatusHistoryForJobLsit){
					   String key = jfth.getTeacherDetail().getTeacherId()+"#"+jfth.getJobOrder().getJobId();
					   maptHist.put(key, jfth);
				   }
			   }
		   } catch(Exception exception){
			   exception.printStackTrace();
		   }
		   System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		   /*
		    * 	END OfferReady
		    * */
			tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>"+Utility.getLocaleValuePropByKey("headOfferReByStaffer", locale)+"</div><br/>");
			
			tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");
			
			tmRecords.append("<table  id='tblGridOfferReady' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			tmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("msgStafferName", locale)+"</th>");
    
			tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("msgPositionNumber", locale)+"</th>");
			
			tmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblApplicantName", locale)+"</th>");
			
			tmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoTil", locale)+"</th>");
			
			
			tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblOfferReadyDate", locale)+"</th>");
		
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(jobForTeacherLists.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			}
		
			if(jobForTeacherLists.size()>0){
			 for(JobForTeacher jft:jobForTeacherLists) 
			 {
				 
				 /*	
				  * 	Get OfferReady Date
				  * */
				 String key = jft.getTeacherId().getTeacherId()+"#"+jft.getJobId().getJobId();
				 TeacherStatusHistoryForJob dObj=maptHist.get(key);
				 
				 tmRecords.append("<tr>");	
				 
				 List<PanelAttendees> lstPanelAttendee =mapPanelAttendee.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId());
				 
				 String schoolName="";
				 String stafferName="";
				 
				 if(lstPanelAttendee!=null && lstPanelAttendee.size()>0)
				 for(PanelAttendees pa: lstPanelAttendee){
					 UserMaster userObj=pa.getPanelInviteeId();
					 if(userObj.getEntityType()==2){
						 stafferName=pa.getPanelInviteeId().getFirstName()+" "+pa.getPanelInviteeId().getLastName();
					 }else  if(userObj.getEntityType()==3){
						 schoolName=pa.getPanelInviteeId().getSchoolId().getSchoolName();
					 }
				 }
				 
				tmRecords.append("<td style='font-size:12px;'>"+stafferName+"</td>");
				 
				tmRecords.append("<td style='font-size:12px;'>"+schoolName+"</td>");
				tmRecords.append("<td style='font-size:12px;'>"+jft.getRequisitionNumber()+"</td>");
				 
				 
				 tmRecords.append("<td style='font-size:12px;'>"+jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")"+"</td>");
				 tmRecords.append("<td style='font-size:12px;'>"+jft.getJobId().getJobTitle()+"</td>");
				 
				if(jft.getOfferReady())
				  tmRecords.append("<td style='font-size:12px;'>"+Utility.getLocaleValuePropByKey("lblCompleted", locale)+"</td>");
				 else
			     tmRecords.append("<td style='font-size:12px;'>"+Utility.getLocaleValuePropByKey("lblPending", locale)+"</td>");
				 
				if(jft.getOfferMadeDate()!=null)
					tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getOfferMadeDate())+"</td>");
				else
					tmRecords.append("<td> </td>");
				
			   tmRecords.append("</tr>");
			 }
			}
	

		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){}
		
	 return tmRecords.toString();
  }
	
}


