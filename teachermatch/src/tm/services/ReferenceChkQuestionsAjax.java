package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificRefChkOptions;
import tm.bean.master.DistrictSpecificRefChkQuestions;
import tm.bean.master.ReferenceQuestionSetQuestions;
import tm.bean.master.ReferenceQuestionSets;
import tm.bean.user.UserMaster;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.DistrictSpecificRefChkOptionsDAO;
import tm.dao.master.DistrictSpecificRefChkQuestionsDAO;
import tm.dao.master.OptionsForDistrictSpecificQuestionsDAO;
import tm.dao.master.ReferenceQuestionSetQuestionsDAO;
import tm.dao.master.ReferenceQuestionSetsDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.Utility;

public class ReferenceChkQuestionsAjax {

	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	@Autowired
	DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	
	@Autowired
	OptionsForDistrictSpecificQuestionsDAO optionsForDistrictSpecificQuestionsDAO;
	
	@Autowired
	DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private DistrictSpecificRefChkQuestionsDAO districtSpecificRefChkQuestionsDAO;
	
	@Autowired
	private DistrictSpecificRefChkOptionsDAO districtSpecificRefChkOptionsDAO;
	
	@Autowired
	private ReferenceQuestionSetsDAO referenceQuestionSetsDAO;
	
	@Autowired
	private ReferenceQuestionSetQuestionsDAO referenceQuestionSetQuestionsDAO;
	
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	
	public String getReferenceInstruction(Integer headQuarterId , Integer districtId)  // @ Anurag
	{
		
		System.out.println(":::::::::::::::::::::::::::::: getReferenceInstruction :::::::::::::::::::::::::");
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }

		StringBuffer sb =new StringBuffer();
		try{
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,22,"assessmentquestions.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			HeadQuarterMaster headQuarterMaster = null;
			DistrictMaster districtMaster=null;
			if(districtId!=null)
			districtMaster=districtMasterDAO.findById(districtId, false, false);
			else if(headQuarterId!=null && headQuarterId!=0)
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
			
			if(districtMaster!=null && districtMaster.getTextForReferenceInstruction()!=null){
				sb.append("<table border='0' width='100%'>");
				sb.append("<tr><td align='right' valign='top' style='padding-right:20px;'>");
				
				sb.append("<span><a href='javascript:void(0)' onclick=\"editReferenceInstruction()\">"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a></span>&nbsp;|&nbsp;");
				sb.append("<span><a href='javascript:void(0)' onclick=\"deleteInstruction()\">"+Utility.getLocaleValuePropByKey("lnkDlt", locale)+"</a></span>");
			
				sb.append("</td></tr>");
				sb.append("<tr><td style='padding-left:5px;'>"+districtMaster.getTextForReferenceInstruction()+"</td>");
				sb.append("</tr>");
				sb.append("</table>");
			}
			else if(headQuarterMaster!=null && headQuarterMaster.getTextForReferenceInstruction()!=null){
				
				sb.append("<table border='0' width='100%'>");
				sb.append("<tr><td align='right' valign='top' style='padding-right:20px;'>");
				
				sb.append("<span><a href='javascript:void(0)' onclick=\"editReferenceInstruction()\">"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a></span>&nbsp;|&nbsp;");
				sb.append("<span><a href='javascript:void(0)' onclick=\"deleteInstruction()\">"+Utility.getLocaleValuePropByKey("lnkDlt", locale)+"</a></span>");
			
				sb.append("</td></tr>");
				sb.append("<tr><td style='padding-left:5px;'>"+headQuarterMaster.getTextForReferenceInstruction()+"</td>");
				sb.append("</tr>");
				sb.append("</table>");
				
			}
	}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String getDistrictQuestions(Integer districtId,Integer headQuarterId)       // change @ Anurag
	{
		
		System.out.println(":::::::::::: getDistrictQuestions :::::::::::::"+districtId);
		System.out.println("************ HeadQuarterId *********"+headQuarterId);
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		StringBuffer sb =new StringBuffer();
		try{
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,22,"assessmentquestions.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<DistrictSpecificRefChkQuestions> districtSpecificQuestionList =null;
			DistrictMaster districtMaster=null;
			if(districtId!=null)
			districtMaster = districtMasterDAO.findById(districtId, false, false);
			
			if(districtMaster!=null){
			Criterion criterion = Restrictions.eq("districtMaster", districtMaster);
			districtSpecificQuestionList = districtSpecificRefChkQuestionsDAO.findByCriteria(criterion);
			}
			System.out.println("before...........");
			
			if(headQuarterId != null){
				System.out.println("************ HeadQuarterId  2323232 *********"+headQuarterId);
				HeadQuarterMaster headQuarterMaster= headQuarterMasterDAO.findById(headQuarterId, false, false);
				Criterion criterion = Restrictions.eq("headQuarterMaster", headQuarterMaster);
				districtSpecificQuestionList = districtSpecificRefChkQuestionsDAO.findByCriteria(criterion);
			}
			
			
			int i=0;
			List<DistrictSpecificRefChkOptions> questionOptionsList = null;
			String questionInstruction = null;
			String shortName = "";
			
			sb.append("");
			
			for (DistrictSpecificRefChkQuestions districtSpecificQuestion : districtSpecificQuestionList) 
			{
				shortName = districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeShortName();
				
				sb.append("<tr>");
				sb.append("<td width='88%'>");
				
				sb.append("Question "+(++i)+": "+districtSpecificQuestion.getQuestion()+"");

				questionOptionsList = districtSpecificQuestion.getQuestionOptions();
				if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel"))
				{
					sb.append("<table >");
					for (DistrictSpecificRefChkOptions questionOptions : questionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt"+i+"' /></td><td style='border:0px;background-color: transparent;padding: 2px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
				}
				
				if(shortName.equalsIgnoreCase("sloet"))
				{
					sb.append("<table >");
					for (DistrictSpecificRefChkOptions questionOptions : questionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt"+i+"' /></td><td style='border:0px;background-color: transparent;padding: 2px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
					if(districtSpecificQuestion.getQuestionExplanation()!=null)
					sb.append("<br/>"+districtSpecificQuestion.getQuestionExplanation()+"");
					sb.append("<br/><br/><B>"+Utility.getLocaleValuePropByKey("msgAdditionalComments", locale)+": </B><textarea name='opt"+i+"' class='form-control' style='width:98%;margin:5px;' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\"/></textarea><br/>");
				}
				
				if(shortName.equalsIgnoreCase("et"))
				{
					sb.append("<table>");
					for (DistrictSpecificRefChkOptions questionOptions : questionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt"+i+"' /></td><td style='border:0px;background-color: transparent;padding: 2px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
					
					if(districtSpecificQuestion.getQuestionExplanation()!=null)
					sb.append("<br/>"+districtSpecificQuestion.getQuestionExplanation()+"");
					sb.append("<br/><br/><textarea name='opt"+i+"' class='form-control' style='width:98%;margin:5px;' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\"/></textarea><br/>");
				}
				else if(shortName.equalsIgnoreCase("ml"))
				{
					sb.append("&nbsp;&nbsp;<BR><textarea name='opt"+i+"' class='form-control' style='width:98%;margin:5px;' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\"/></textarea><br/>");
				}
				else if(shortName.equalsIgnoreCase("SLD"))
				{
					sb.append("&nbsp;&nbsp;<BR><B>Additional comments:</B><textarea name='opt"+i+"' class='form-control' style='width:98%;margin:5px;' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\"/></textarea><br/>");
				}
				
				questionInstruction = districtSpecificQuestion.getQuestionInstructions()==null?"":districtSpecificQuestion.getQuestionInstructions();
				if(!questionInstruction.equals(""))
					sb.append("<br/>"+Utility.getLocaleValuePropByKey("lblInst", locale)+": </br>"+questionInstruction);
				else
					sb.append("<br/>");
				sb.append("</td>");
				sb.append("<td width='12%'>");
				boolean pipeFlag=false;
			
			 
				sb.append("<a href='referencecheckspecificquestions.do?districtId="+(districtSpecificQuestion.getDistrictMaster()!=null?districtSpecificQuestion.getDistrictMaster().getDistrictId():"")+"&headQuarterId="+(districtSpecificQuestion.getHeadQuarterMaster()!=null?districtSpecificQuestion.getHeadQuarterMaster().getHeadQuarterId():"")+"&questionId="+districtSpecificQuestion.getQuestionId()+"' >"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>");
				sb.append("&nbsp;|&nbsp;");
				if(districtSpecificQuestion.getStatus().equalsIgnoreCase("A"))
					sb.append("<span id='Q"+districtSpecificQuestion.getQuestionId()+"'><a href='javascript:void(0)' onclick=\"activateDeactivateDistrictQuestion("+districtSpecificQuestion.getQuestionId()+",'I')\">"+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+"</a></span>");
				else
					sb.append("<span id='Q"+districtSpecificQuestion.getQuestionId()+"'><a href='javascript:void(0)' onclick=\"activateDeactivateDistrictQuestion("+districtSpecificQuestion.getQuestionId()+",'A')\">"+Utility.getLocaleValuePropByKey("lblActivate", locale)+"</a></span>");
		
				sb.append("</td>");

			}
			if(districtSpecificQuestionList.size()==0)
				sb.append("<tr id='tr1' ><td id='tr1' colspan=2>"+Utility.getLocaleValuePropByKey("lblNoQuestionfound1", locale)+"</td></tr>" );
			
	}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	/* @Author: Hanzala Subhani
	 * @Discription: It is used to save District Question.
	 */
	//@Transactional(readOnly=false)
public DistrictSpecificRefChkQuestions saveDistrictQuestion(DistrictSpecificRefChkQuestions districtSpecificQuestion,DistrictMaster districtMaster,HeadQuarterMaster headQuarterMaster, DistrictSpecificRefChkOptions[] optionsForDistrictSpecificQuestions)
	{
	
	System.out.println("::::::::::::::::::::::::saveDistrictQuestion ::::::::::");
	
	if(districtMaster!=null && districtMaster.getDistrictId()==0)
		districtMaster=null;
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	
	UserMaster user = null;
	if (session == null || session.getAttribute("userMaster") == null) {
		//return "false";
	}else
		user=(UserMaster)session.getAttribute("userMaster");
	
	if (districtSpecificQuestion.getQuestionId()==null)
	{
		districtSpecificQuestion.setCreatedDateTime(new Date());			
	}

	try{
		districtSpecificQuestion.setDistrictMaster(districtMaster);
		districtSpecificQuestion.setHeadQuarterMaster(headQuarterMaster);
		districtSpecificQuestion.setUserMaster(user);
		districtSpecificQuestion.setStatus("A");
		DistrictSpecificRefChkQuestions districtSpecificQuestionNew = null;
		List<DistrictSpecificRefChkOptions> qopt = null;
		if(districtSpecificQuestion.getQuestionId()!=null)
		{
			districtSpecificQuestionNew=districtSpecificRefChkQuestionsDAO.findById(districtSpecificQuestion.getQuestionId(), false, false);
			qopt = districtSpecificQuestionNew.getQuestionOptions();
			districtSpecificQuestion.setStatus(districtSpecificQuestionNew.getStatus());
			districtSpecificQuestion.setCreatedDateTime(districtSpecificQuestionNew.getCreatedDateTime());
			//districtSpecificQuestionsDAO.clear();
		}
		districtSpecificRefChkQuestionsDAO.makePersistent(districtSpecificQuestion);

		Map<Integer,DistrictSpecificRefChkOptions> map = new HashMap<Integer,DistrictSpecificRefChkOptions>();
		if(qopt!=null)
			for (DistrictSpecificRefChkOptions questionOptions2 : qopt) {
				map.put(questionOptions2.getOptionId(), questionOptions2);
			}

		for (DistrictSpecificRefChkOptions optionsForDistrictSpecificQuestions2 : optionsForDistrictSpecificQuestions) {
			optionsForDistrictSpecificQuestions2.setDistrictSpecificRefChkQuestions(districtSpecificQuestion);
			optionsForDistrictSpecificQuestions2.setCreatedDateTime(new Date());
			optionsForDistrictSpecificQuestions2.setUserMaster(user);
			optionsForDistrictSpecificQuestions2.setStatus("A");
			districtSpecificRefChkOptionsDAO.makePersistent(optionsForDistrictSpecificQuestions2);

			if(qopt!=null)
				map.remove(optionsForDistrictSpecificQuestions2.getOptionId());
			optionsForDistrictSpecificQuestions2=null;
		}
		if(qopt!=null)
			for(Integer aKey : map.keySet()) {
				districtSpecificRefChkOptionsDAO.makeTransient(map.get(aKey));
			}

	}catch (Exception e) {
		e.printStackTrace();
	}
  
	
	System.out.println("::::::::::::::::::::::::saveDistrictQuestion ::::::::END::");
	
	return districtSpecificQuestion;

}
	
	/* @Author: Hanzala Subhani
	 * @Discription: It is used to change a particular district question status.
	 */
	@Transactional(readOnly=false)
	public DistrictSpecificRefChkQuestions activateDeactivateDistrictQuestion(int questionId,String status)
	{	System.out.println(":::::: activateDeactivateDistrictQuestion ::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }

		try{
			DistrictSpecificRefChkQuestions districtSpecificQuestion = districtSpecificRefChkQuestionsDAO.findById(questionId, false, false);

			districtSpecificQuestion.setStatus(status);
			districtSpecificRefChkQuestionsDAO.makePersistent(districtSpecificQuestion);
			return districtSpecificQuestion;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/* @Author: Hanzala Subhani
	 * @Discription: It is used to get a particular District Question.
	 */
	@Transactional(readOnly=false)
	public DistrictSpecificRefChkQuestions getDistrictQuestionById(DistrictSpecificRefChkQuestions districtSpecificQuestion)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		System.out.println(" districtSpecificQuestion "+districtSpecificQuestion.getQuestionId());
		try{
			districtSpecificQuestion = districtSpecificRefChkQuestionsDAO.findById(districtSpecificQuestion.getQuestionId(), false, false);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println(":::::::::::"+districtSpecificQuestion);
		return districtSpecificQuestion;
	}
	
	
public DistrictSpecificRefChkQuestions saveDistrictQuestionFromQueationSet(DistrictSpecificRefChkQuestions districtSpecificQuestion,DistrictMaster districtMaster,HeadQuarterMaster headQuarterMaster, DistrictSpecificRefChkOptions[] optionsForDistrictSpecificQuestions,String quesSetId)
	{
	System.out.println("::::::::::::::::::::::::saveDistrictQuestionFromQueationSet ::::::::::"+districtMaster.getDistrictId());
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	
	UserMaster user = null;
	if (session == null || session.getAttribute("userMaster") == null) {
		//return "false";
	}else
		user=(UserMaster)session.getAttribute("userMaster");
	
	if (districtSpecificQuestion.getQuestionId()==null)
	{
		districtSpecificQuestion.setCreatedDateTime(new Date());			
	}

	try{
		if(districtMaster.getDistrictId()==0)
			districtMaster = null;
		
		districtSpecificQuestion.setDistrictMaster(districtMaster);
		districtSpecificQuestion.setHeadQuarterMaster(headQuarterMaster);
		districtSpecificQuestion.setUserMaster(user);
		districtSpecificQuestion.setStatus("A");
		DistrictSpecificRefChkQuestions districtSpecificQuestionNew = null;
		List<DistrictSpecificRefChkOptions> qopt = null;
		if(districtSpecificQuestion.getQuestionId()!=null)
		{
			districtSpecificQuestionNew=districtSpecificRefChkQuestionsDAO.findById(districtSpecificQuestion.getQuestionId(), false, false);
			qopt = districtSpecificQuestionNew.getQuestionOptions();
			districtSpecificQuestion.setStatus(districtSpecificQuestionNew.getStatus());
			districtSpecificQuestion.setCreatedDateTime(districtSpecificQuestionNew.getCreatedDateTime());
			//districtSpecificQuestionsDAO.clear();
		}
		districtSpecificRefChkQuestionsDAO.makePersistent(districtSpecificQuestion);

		Map<Integer,DistrictSpecificRefChkOptions> map = new HashMap<Integer,DistrictSpecificRefChkOptions>();
		if(qopt!=null)
			for (DistrictSpecificRefChkOptions questionOptions2 : qopt) {
				map.put(questionOptions2.getOptionId(), questionOptions2);
			}

		for (DistrictSpecificRefChkOptions optionsForDistrictSpecificQuestions2 : optionsForDistrictSpecificQuestions) {
			optionsForDistrictSpecificQuestions2.setDistrictSpecificRefChkQuestions(districtSpecificQuestion);
			optionsForDistrictSpecificQuestions2.setCreatedDateTime(new Date());
			optionsForDistrictSpecificQuestions2.setUserMaster(user);
			optionsForDistrictSpecificQuestions2.setStatus("A");
			districtSpecificRefChkOptionsDAO.makePersistent(optionsForDistrictSpecificQuestions2);

			if(qopt!=null)
				map.remove(optionsForDistrictSpecificQuestions2.getOptionId());
			optionsForDistrictSpecificQuestions2=null;
		}
		if(qopt!=null)
			for(Integer aKey : map.keySet()) {
				districtSpecificRefChkOptionsDAO.makeTransient(map.get(aKey));
			}
		
		// Set Question Set
		
		ReferenceQuestionSets referenceQuestionSets = new ReferenceQuestionSets();
		ReferenceQuestionSetQuestions referenceQuestionSetQuestions = new ReferenceQuestionSetQuestions();
		
		referenceQuestionSets = referenceQuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
		
		int quesSequence =0;
		
		try
		{
			List<ReferenceQuestionSetQuestions> existQuesList = new ArrayList<ReferenceQuestionSetQuestions>();
			existQuesList = referenceQuestionSetQuestionsDAO.findByQuesSetId(referenceQuestionSets,districtMaster,headQuarterMaster);
			if(existQuesList.size()==0)
			{
				quesSequence=1;
			}
			else
			{
				if(existQuesList.size()>0)
				{
					quesSequence = existQuesList.get(existQuesList.size()-1).getQuestionSequence()+1; 
				}
			}

			referenceQuestionSetQuestions.setQuestionSequence(quesSequence);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		referenceQuestionSetQuestions.setDistrictSpecificRefChkQuestions(districtSpecificQuestion);
		referenceQuestionSetQuestions.setQuestionSets(referenceQuestionSets);
		
		referenceQuestionSetQuestionsDAO.makePersistent(referenceQuestionSetQuestions);
		

	}catch (Exception e) {
		e.printStackTrace();
	}
	return districtSpecificQuestion;
 }

public void saveReferenceInstruction(String Qins,Integer headQuarterId ,Integer districtId)
{
	
	try{
		HeadQuarterMaster headQuarterMaster = null;
		DistrictMaster districtMaster= null;
		if(districtId!=null && districtId!=0)
	        districtMaster=districtMasterDAO.findById(districtId, false, false);
		else if(headQuarterId!=null && headQuarterId!=0)
			headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
			
	if(districtMaster!=null){
		districtMaster.setTextForReferenceInstruction(Qins);
		districtMasterDAO.makePersistent(districtMaster);
	}
	else if(headQuarterMaster!=null)
	{
		headQuarterMaster.setTextForReferenceInstruction(Qins);
		headQuarterMasterDAO.makePersistent(headQuarterMaster);
	}
	
	}catch (Exception e) {
		e.printStackTrace();
	}
}

public String loadReferenceInstruction(Integer headQuarterId ,Integer districtId)
{
	try{
		HeadQuarterMaster headQuarterMaster =null;
	DistrictMaster districtMaster= null;
	if(districtId!=null && districtId!=0)
	districtMaster		=		districtMasterDAO.findById(districtId, false, false);
	else if(headQuarterId!=null && headQuarterId!=0)
		headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
	
	if(districtMaster!=null){
		return districtMaster.getTextForReferenceInstruction();
	}else {if(headQuarterMaster!=null)
				return headQuarterMaster.getTextForReferenceInstruction();
		else
		return null;
	}
	}catch (Exception e) {
		e.printStackTrace();
		return null;
	}
}

public boolean deleteReferenceInstruction(Integer districtId)
{
	try{
		DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
		if(districtMaster!=null){
			districtMaster.setTextForReferenceInstruction(null);
			districtMasterDAO.makePersistent(districtMaster);
			return true;
		}else{
			return false;
		}
	}catch(Exception e){
		e.printStackTrace();
		return false;
	}
}
	
}
