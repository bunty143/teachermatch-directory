package tm.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.user.UserMaster;
import tm.dao.master.CompetencyMasterDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.Utility;

public class CompetencyAjax
{
	 String locale = Utility.getValueOfPropByKey("locale");
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lblDomName=Utility.getLocaleValuePropByKey("lblDomName", locale);
	 String lblCompNm=Utility.getLocaleValuePropByKey("lblDomName", locale);
     String lblRk=Utility.getLocaleValuePropByKey("lblRk", locale);
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String lblNoCompetencyfound=Utility.getLocaleValuePropByKey("lblNoCompetencyfound", locale);
	 String optAct=Utility.getLocaleValuePropByKey("optAct", locale);
	 String optInActiv=Utility.getLocaleValuePropByKey("optInActiv", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String lnkV=Utility.getLocaleValuePropByKey("lnkV", locale);
	 String lblDeactivate=Utility.getLocaleValuePropByKey("lblDeactivate", locale);
	
	 
	 
	
	@Autowired
	private CompetencyMasterDAO competencyMasterDAO;
	
	public void setCompetencyMasterDAO(CompetencyMasterDAO competencyMasterDAO) 
	{
		this.competencyMasterDAO = competencyMasterDAO;
	}
	
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) {
		this.domainMasterDAO = domainMasterDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	/* @Author: Gagan 
	 * @Discription: It is used to display Competencies Records.
	 */	
	public String displayCompetencyRecords(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"competencyName";
			String sortOrderNoField		=	"competencyName";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("domainName")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("domainName")){
					 sortOrderNoField="domainName";
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,18,"competency.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			/***** Sorting by Domain Name then Sorting by Competency Name Stating ( Sekhar ) ******/
			Map<String,CompetencyMaster> map = new LinkedHashMap<String,CompetencyMaster>();
			
			List<CompetencyMaster> lstcompetencyMaster	  	=	null;
			
			List<CompetencyMaster> reslstcompetencyMaster	=	new ArrayList<CompetencyMaster>();
			//lstcompetencyMaster = competencyMasterDAO.findByCriteria();
			totaRecord 			= competencyMasterDAO.getRowCountWithSort(sortOrderStrVal);
			lstcompetencyMaster = competencyMasterDAO.findWithLimit(sortOrderStrVal,0,100);
			
			List<CompetencyMaster> lstCompetencyMaster		=	new ArrayList<CompetencyMaster>();
			
			SortedMap<String,CompetencyMaster>	sortedMap = new TreeMap<String,CompetencyMaster>();
			if(sortOrderNoField.equals("domainName")){
				sortOrderFieldName="domainName";
			}
			int mapFlag=2;
			for (CompetencyMaster competencyMaster : lstcompetencyMaster){
				String orderFieldName=competencyMaster.getCompetencyName();
				if(sortOrderFieldName.equals("domainName")){
					orderFieldName=competencyMaster.getDomainMaster().getDomainName()+"||"+competencyMaster.getCompetencyId();
					sortedMap.put(orderFieldName+"||",competencyMaster);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					lstCompetencyMaster.add((CompetencyMaster) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					lstCompetencyMaster.add((CompetencyMaster) sortedMap.get(key));
				}
			}else{
				lstCompetencyMaster=lstcompetencyMaster;
			}
			
			
			
			
			
			/***** Sorting by Domain Name then Sorting by Competency Name End ******/
			dmRecords.append("<table  id='competencyTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			//dmRecords.append("<th width='35%'>Domain Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblDomName,sortOrderFieldName,"domainName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='35%' valign='top'>"+responseText+"</th>");
			
			//dmRecords.append("<th width='35%'>Competency Name</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblCompNm,sortOrderFieldName,"competencyName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='35%' valign='top'>"+responseText+"</th>");
			
			//dmRecords.append("<th width='7%'>Rank</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblRk,sortOrderFieldName,"rank",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='35%' valign='top'>"+responseText+"</th>");
			
			//dmRecords.append("<th width='8%'>Status</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='35%' valign='top'>"+responseText+"</th>");
			
			dmRecords.append("<th width='15%'>"+lblAct+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(lstCompetencyMaster.size()==0)
				dmRecords.append("<tr><td colspan='6' align='center'>"+lblNoCompetencyfound+"</td></tr>" );
			System.out.println(Utility.getLocaleValuePropByKey("msgNoOfRecords", locale)+lstCompetencyMaster.size());

			/*for (CompetencyMaster competencyMasterDetail : reslstcompetencyMaster) */
			for (CompetencyMaster competencyMasterDetail : lstCompetencyMaster) 
			{
				if(competencyMasterDetail.getDomainMaster().getStatus().equalsIgnoreCase("A"))
				{
					Object rank	=	competencyMasterDetail.getRank()==null?"":competencyMasterDetail.getRank();
					dmRecords.append("<tr>" );
					dmRecords.append("<td>"+competencyMasterDetail.getDomainMaster().getDomainName()+"</td>");
					dmRecords.append("<td>"+competencyMasterDetail.getCompetencyName()+"</td>");
					dmRecords.append("<td>"+rank+"</td>");
					dmRecords.append("<td>");
					if(competencyMasterDetail.getStatus().equalsIgnoreCase("A"))
						dmRecords.append(optAct);
					else
						dmRecords.append(optInActiv);
					dmRecords.append("</td>");
					dmRecords.append("<td>");
					
					boolean pipeFlag=false;
					if(roleAccess.indexOf("|2|")!=-1){
						dmRecords.append("<a href='javascript:void(0);' onclick='return editCompetency("+competencyMasterDetail.getCompetencyId()+")'>"+lblEdit+"</a>");
						pipeFlag=true;
					}else if(roleAccess.indexOf("|4|")!=-1){
						dmRecords.append("<a href='javascript:void(0);' onclick='return editCompetency("+competencyMasterDetail.getCompetencyId()+")'>"+lnkV+"</a>");
						pipeFlag=true;
					}
					if(roleAccess.indexOf("|7|")!=-1){
						if(pipeFlag)dmRecords.append(" | ");
						if(competencyMasterDetail.getStatus().equalsIgnoreCase("A"))
							dmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateCompetency("+competencyMasterDetail.getCompetencyId()+",'I')\">"+lblDeactivate+"");
						else
							dmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateCompetency("+competencyMasterDetail.getCompetencyId()+",'A')\">"+optAct+"");
					}else{
						dmRecords.append("&nbsp;");
					}
					dmRecords.append("</td>");
					dmRecords.append("</tr>");
				}
			}
			dmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	/* =================      saveCompetency        =========================*/
	public int saveCompetency(Integer domainId,Integer competencyId,String competencyName,Short rank,String competencyStatus, String competencyShortName, String competencyUId )
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		CompetencyMaster competencyMaster=new CompetencyMaster();
		try
		{
			System.out.println("\n domainId "+domainId+" competencyId "+competencyId+" competencyName "+competencyName+" rank "+rank+" competencyStatus "+competencyStatus+"  competencyShortName "+competencyShortName+" \n");
			DomainMaster domainMaster = domainMasterDAO.findById(domainId, false, false);
			
			if(competencyId	!=	null)
			{
				competencyMaster= competencyMasterDAO.findById(competencyId, false, false);
				
			}else
				competencyMaster.setDisplayInPDReport(false);
			/*========= It will set domain Id ==========*/
			competencyMaster.setDomainMaster(domainMaster);
			if(competencyUId!=null && !competencyUId.equals("") && competencyUId.trim().length()==1)
				competencyUId = "0"+competencyUId;
			competencyMaster.setCompetencyUId(competencyUId);
			competencyMaster.setCompetencyName(competencyName);
			competencyMaster.setRank(rank);
			competencyMaster.setCompetencyShortName(competencyShortName);
			competencyMaster.setStatus(competencyStatus);
			
			
			/*======= Check For Unique Competency ==========*/
			List<CompetencyMaster> dupcompetencyMaster	=	competencyMasterDAO.checkDuplicateCompetencyName(domainMaster,competencyName,competencyId);
			int size 									=	dupcompetencyMaster.size();
			if(size>0)
				return 3;
			
			List<CompetencyMaster> dupRankMaster		=	competencyMasterDAO.checkDuplicateRank(domainMaster,rank,competencyId);
			int sizeRankList 							=	dupRankMaster.size();
			if(sizeRankList>0)
				return 4;
			
			List<CompetencyMaster> competencyMasterList = null;
			if(domainMaster!=null && competencyId==null)
				competencyMasterList = competencyMasterDAO.findCompetenciesByDomainAndUId(domainMaster, competencyUId);
			if(competencyMasterList!=null && competencyMasterList.size()>0)
				return 5;//if duplicate competencyUId
			
			competencyMasterDAO.makePersistent(competencyMaster);
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}

		return 1;
	}
	/* ===============      Activate Deactivate Competency        =========================*/
	@Transactional(readOnly=false)
	public boolean activateDeactivateCompetency(int domainId,String status)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		try{
			CompetencyMaster competencyMaster	=	competencyMasterDAO.findById(domainId, false, false);
			competencyMaster.setStatus(status);
			competencyMasterDAO.makePersistent(competencyMaster);
			
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/* ===============    Edit Competency Functionality        =========================*/
	@Transactional(readOnly=false)
	public CompetencyMaster getCompetencyById(int competencyId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		CompetencyMaster competencyMaster =null;
		try
		{
			competencyMaster	=	competencyMasterDAO.findById(competencyId, false, false);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		return competencyMaster;
	}
}
