package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.lang.ArrayUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAchievementScore;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobCertificationDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAchievementScoreDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.AppsDateCompratorASC;
import tm.utility.AppsDateCompratorDESC;
import tm.utility.AvlCandidateCompratorASC;
import tm.utility.AvlCandidateCompratorDESC;
import tm.utility.DistrictNameCompratorASC;
import tm.utility.DistrictNameCompratorDESC;
import tm.utility.EndDateCompratorASC;
import tm.utility.EndDateCompratorDESC;
import tm.utility.HiredDateCompratorASC;
import tm.utility.HiredDateCompratorDESC;
import tm.utility.InternalCandidateCompratorASC;
import tm.utility.InternalCandidateCompratorDESC;
import tm.utility.JobTitleCompratorASC;
import tm.utility.JobTitleCompratorDESC;
import tm.utility.LastActivityDateCompratorASC;
import tm.utility.LastActivityDateCompratorDESC;
import tm.utility.LastNameCompratorASC;
import tm.utility.LastNameCompratorDESC;
import tm.utility.PostingDateCompratorASC;
import tm.utility.PostingDateCompratorDESC;
import tm.utility.TotalCandidateCompratorASC;
import tm.utility.TotalCandidateCompratorDESC;
import tm.utility.Utility;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class CandidatejobstatusAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
	
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	
	@Autowired
	private TeacherDetailDAO 	teacherDetailDAO;
	
	@Autowired 
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO 	jobWiseConsolidatedTeacherScoreDAO;
	
	@Autowired
	private TeacherAchievementScoreDAO 	teacherAchievementScoreDAO;
	
	@Autowired
	private TeacherNormScoreDAO 	teacherNormScoreDAO;
	
	@Autowired
	private SchoolInJobOrderDAO 	schoolInJobOrderDAO;
	
    @Autowired
	private SchoolMasterDAO 	schoolMasterDAO;
	
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	
	 public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	
	 //mukesh1
		public String displayRecordsByEntityType(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
				schoolId,String subjectId,String certifications,String jobOrderId,String status,String noOfRow,
				String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo, String firstName,String lastName,
				String emailAddress, String stateId,String certType,String jobCategoryIds,String statusId,
				String sfromDate, String stoDate, String endfromDate, String endtoDate, String appsfromDate, String appstoDate, 
				boolean pdateFlag,String normScoreSelectVal,String normScoreVal, int intenalchk, String hiredfromDate, String hiredtoDate,String jobStatus,int hiddenjob)
		
		 {
			System.out.println(statusId+" :: statusId @@@@@@@@@@....... "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				SubjectMaster subjectMaster	=	null;
				int roleId=0;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
					if(userMaster.getRoleId().getRoleId()!=null){
						roleId=userMaster.getRoleId().getRoleId();
					}
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position

					int noOfRowInPage = Integer.parseInt(noOfRow);
					int pgNo = Integer.parseInt(pageNo);
					int start =((pgNo-1)*noOfRowInPage);
					int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
					int totalRecord = 0;
					//------------------------------------
				 /** set default sorting fieldName **/
					String sortOrderFieldName="jobTitle";
					String sortOrderNoField="jobTitle";
					
					boolean deafultFlag=false;
					
					/**Start set dynamic sorting fieldName **/
					Order  sortOrderStrVal=null;

					if(sortOrder!=null){
						if(!sortOrder.equals("") && !sortOrder.equals(null)){
							sortOrderFieldName=sortOrder;
							sortOrderNoField=sortOrder;
						}
					}

					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("0")){
							sortOrderStrVal=Order.asc(sortOrderFieldName);
						}else{
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
						}
					}else{
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					
					List<JobForTeacher>	jobForTeacherLists    = new ArrayList<JobForTeacher>();
					List<JobForTeacher>	listByFirstName       = new ArrayList<JobForTeacher>();
					List<JobForTeacher>	jobJobId              = new ArrayList<JobForTeacher>();
					List<JobForTeacher> listByJobCategory     = new ArrayList<JobForTeacher>();
					
					boolean checkflag = false;
					
					if(entityID==2){
						if(schoolId==0){
							jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatusAll(districtMaster);
							if(jobForTeacherLists.size()>0)
							checkflag = true;
							 //pdateFlag=true;
						}else{
							List<JobOrder> lstJobOrders =null;
							SchoolMaster  sclMaster=null;
							sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
							lstJobOrders = schoolInJobOrderDAO.allJobOrderBySchool(sclMaster);
							if(lstJobOrders.size()>0){
								jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOBStatus(lstJobOrders);
								checkflag = true;
							}
						}
						
					}
					else if(entityID==1) 
					{
						if(districtOrSchoolId!=0){
							if(schoolId==0){
								 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
						       
								 jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatusAll(districtMaster);
						          if(jobForTeacherLists.size()>0)
						        	checkflag = true;
							}
							else{
								List<JobOrder> lstJobOrders =null;
								SchoolMaster  sclMaster=null;
								sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
								lstJobOrders = schoolInJobOrderDAO.allJobOrderBySchool(sclMaster);
								if(lstJobOrders.size()>0){
									jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOBStatus(lstJobOrders);
									checkflag = true;
								}
							}
						}
						else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						}
					}else if(entityID==3){
						if(schoolId==0){
							 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					        jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatus(districtMaster);
					        if(jobForTeacherLists.size()>0)
					        	checkflag = true;
						}else{
							List<JobOrder> lstJobOrders =null;
							SchoolMaster  sclMaster=null;
							sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
							lstJobOrders = schoolInJobOrderDAO.allJobOrderBySchool(sclMaster);
							if(lstJobOrders.size()>0){
								jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOBStatus(lstJobOrders);
								checkflag = true;
							}
						}
					}
				
		//filter start
	    //Jobstatus filter *******************************************************************************************************
			if (jobStatus.equals("A") || jobStatus.equals("I")) {
				List<Integer> list = new ArrayList<Integer>();
				for (JobForTeacher jobForTeacher : jobForTeacherLists) {
					list.add(jobForTeacher.getJobId().getJobId());
					// System.out.println("??????????????????       "+jobForTeacher.getJobId().getJobId());
				}
				List<JobOrder> jobstatuslist = new ArrayList<JobOrder>();
				//List<JobForTeacher> jobstatuslist = new ArrayList<JobForTeacher>();
				boolean jobstatusflag = false;
				if (jobStatus.equals("A")) {
					districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
					Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
					Criterion criterion = Restrictions.eq("status", jobStatus);
					Criterion criterion2 = Restrictions.in("jobId", list);
					jobstatuslist = jobOrderDAO.findByCriteria(criterion1,criterion, criterion2);
					//jobstatuslist=jobForTeacherDAO.findByCriteria(criterion1,criterion, criterion2);
					System.out.println("jobstatuslist---------------"+ jobstatuslist.size() + "           "	+ list.size());
					if (jobstatuslist.size() > 0) {
						jobstatusflag = true;
					}
				}

				if (jobStatus.equals("I")) {
					districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
					Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
					Criterion criterion = Restrictions.eq("status", jobStatus);
					Criterion criterion2 = Restrictions.in("jobId", list);
					jobstatuslist = jobOrderDAO.findByCriteria(criterion1,criterion, criterion2);
					System.out.println("jobstatuslist---------------"+ jobstatuslist.size() + "           "	+ list.size());
					if (jobstatuslist.size() > 0) {
						jobstatusflag = true;
					}
				}

				List<JobForTeacher> jobForTeacher1 = new ArrayList<JobForTeacher>();
				if (jobstatusflag) {
					for (JobOrder jobOrder : jobstatuslist) {
						for (JobForTeacher jobForTeacherLists1 : jobForTeacherLists) {
							if (jobOrder.getJobId().equals(
									jobForTeacherLists1.getJobId().getJobId())) {
								jobForTeacher1.add(jobForTeacherLists1);
								//break;
							}
						}
					}
					jobForTeacherLists = jobForTeacher1;
				}
			}
			
			
			//Hidden Job filter start**************************************************************************************************
			Boolean hdjob=false;
			if(hiddenjob==1)
			{
				hdjob=true;
			}else if(hiddenjob==0)
			{
				hdjob=false;
			}
			//System.out.println("hiddenjob------------"+hdjob);
				List<Integer> hiddenJobTempList = new ArrayList<Integer>();
				for (JobForTeacher jobForTeacher : jobForTeacherLists) {
					hiddenJobTempList.add(jobForTeacher.getJobId().getJobId());
					// System.out.println("jobForTeacherLists  JobID   "+jobForTeacher.getJobId().getJobId());
				}
				List<JobOrder> hiddenJobList = new ArrayList<JobOrder>();
				boolean hiddenJobflag = false;
				if (hiddenjob!=2) {
					districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
					Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
					System.out.println("Hiddeen Jobs ::::::::"+hdjob);
					Criterion criterion = Restrictions.eq("hiddenJob",hdjob);
					Criterion criterion2 = Restrictions.in("jobId",	hiddenJobTempList);
					hiddenJobList = jobOrderDAO.findByCriteria(criterion,criterion1);
					System.out.println("hiddenJobTempList---------------"+ hiddenJobTempList.size() + "hiddenJobList.size()"	+ hiddenJobList.size());
					if (hiddenJobList.size() > 0) {
						hiddenJobflag = true;
					}else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					}
					}
				List<JobForTeacher> jobForTeacherHiddenJob = new ArrayList<JobForTeacher>();
				if (hiddenJobflag) {
					for (JobOrder jobOrder : hiddenJobList) {
						for (JobForTeacher jobForTeacherLists1 : jobForTeacherLists) {
							if (jobOrder.getJobId().equals(	jobForTeacherLists1.getJobId().getJobId())) {
								jobForTeacherHiddenJob.add(jobForTeacherLists1);
								//break;
							}
						}
					}
					jobForTeacherLists = jobForTeacherHiddenJob;
				}
		// Hidden Filter End			
					//******* first Name , Last Name and Email filter 
					List<TeacherDetail> basicSearchList = new ArrayList<TeacherDetail>();
				
					List<Criterion> basicSearch = new ArrayList<Criterion>();
					boolean basicFlag = false;
					
					if(firstName!=null && !firstName.equals(""))
					{
						Criterion criterion = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
						basicSearch.add(criterion);
						basicFlag = true;
					}
						
					if(lastName!=null && !lastName.equals(""))
					{
						Criterion criterion = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
						basicSearch.add(criterion);
						basicFlag = true;
					}
					
					if(emailAddress!=null && !emailAddress.equals(""))
					{						
						Criterion criterion = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
						basicSearch.add(criterion);
						basicFlag = true;
					}
					if(basicFlag)
					{
						basicSearchList  = teacherDetailDAO.getTeachersByNameAndEmail(basicSearch);
						if(basicSearchList!=null && basicSearchList.size()>0)
						{
							listByFirstName = jobForTeacherDAO.findTeacherByTeacherDeatailAndJobStatus(basicSearchList);
							 if(listByFirstName!=null && listByFirstName.size()>0){
								 if(checkflag){									 
				   						jobForTeacherLists.retainAll(listByFirstName);				   						
				   					}else{				   				
				   						jobForTeacherLists.addAll(listByFirstName);
				   						checkflag=true;
				   					}
							 }
							 else{								
									jobForTeacherLists = new ArrayList<JobForTeacher>();
								 }
						}
						else{							
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						 }
					}
					
		   		 //****** job ID filter
					//JobOrder joborder =null;
					if(!jobOrderId.equals(""))
					{
						List<Integer> jobIDS= new ArrayList<Integer>();
						String strjobIds[]=jobOrderId.split(",");
						
						if(strjobIds.length>0)
					     for(String str : strjobIds){
					    	 if(!str.equals(""))
					    	 jobIDS.add(Integer.parseInt(str));
					     }
							jobJobId = jobForTeacherDAO.findbyJobOrderAndStatus(jobIDS);
						
							if (jobJobId.size()>0){ 
								if(checkflag)
								{ 
									jobForTeacherLists.retainAll(jobJobId);
								}
								else{
									jobForTeacherLists.addAll(jobJobId);
									checkflag=true;
								}
								
							}else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							 }
					  }
					
				// Licensure Name/Licensure State filter
					List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
					List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();
                    List<JobForTeacher> listBycertification = new ArrayList<JobForTeacher>();
					StateMaster stateMaster=null;
					List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
					
					
					if(!certType.equals("0") && !certType.equals("")||!stateId.equals("0")){
						if(!stateId.equals("0")){
							stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
						}
						certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
						//System.out.println("certificateTypeMasterList:: "+certificateTypeMasterList.size());
						if(certificateTypeMasterList.size()>0)
							certJobOrderList=jobCertificationDAO.findCertificationByJobWithState(certificateTypeMasterList);
						System.out.println("certJobOrderList:: "+certJobOrderList.size());
						if(certJobOrderList.size()>0){
							for(JobOrder job: certJobOrderList){
								jobOrderIds.add(job);
							}
						}
						if(jobOrderIds.size()>0){
							listBycertification = jobForTeacherDAO.getJobForTeacherByJOBStatus(jobOrderIds);
							if(listBycertification.size()>0){
								if(checkflag){
			   						jobForTeacherLists.retainAll(listBycertification);
			   					}else{
			   						jobForTeacherLists.addAll(listBycertification);
			   						checkflag=true;
			   					}
							}else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							}
						}else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						}
					
					}
				
				 //filter job category
					if(!jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
					{
						List<Integer> jobcatIds = new ArrayList<Integer>();
						List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
						String jobCategoryIdStr[] =jobCategoryIds.split(",");
					  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
					  {		  
						for(String str : jobCategoryIdStr){
					    	 if(!str.equals(""))
					    		 jobcatIds.add(Integer.parseInt(str));
						 } 
				    	 if(jobcatIds.size()>0){
				    		 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
				    	 }
				    	 if(listjJobOrders.size()>0){
				    		 listByJobCategory = jobForTeacherDAO.getJobForTeacherByJOBStatus(listjJobOrders);
				    		 if(listByJobCategory.size()>0){
									if(checkflag){
				   						jobForTeacherLists.retainAll(listByJobCategory);
				   					}else{
				   						jobForTeacherLists.addAll(listByJobCategory);
				   						checkflag=true;
				   					}
								}else{
									jobForTeacherLists = new ArrayList<JobForTeacher>();
								}
				         }
				    	 else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							 }
					  }	 
					}
					// filter of job status 
					
					List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
					List<JobForTeacher>  listByJobStatus = new ArrayList<JobForTeacher>();
					
					 if(!statusId.equals("") && !statusId.equals("0")){
						 System.out.println(" Filter StatusId Start ::  "+statusId);
						 String statusNames[] = statusId.split("#@");
						
					      if(!ArrayUtils.contains(statusNames,"0"))
					      {	 
							 DistrictMaster districtMaster2 = districtMasterDAO.findById(districtOrSchoolId, false, false);
							 lstSecondaryStatus = secondaryStatusDAO.findByMultipleStatusName(districtMaster2, statusNames);
					
							  List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
							  Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
							  for (StatusMaster statusMaster1 : statusMasterList) {
								  mapStatus.put(statusMaster1.getStatusShortName(),statusMaster1);
							  }
							  StatusMaster stMaster =null;
							  List <StatusMaster> statusMaster1st=new ArrayList<StatusMaster>();
							  
							  if(ArrayUtils.contains(statusNames,"Available")){
									  stMaster = mapStatus.get("comp");
									  statusMaster1st.add(stMaster);
							  }
							  if(ArrayUtils.contains(statusNames,"Rejected")){
								    stMaster = mapStatus.get("rem");
								    statusMaster1st.add(stMaster);
						     }
							  if(ArrayUtils.contains(statusNames,"Timed Out")){
								    stMaster = mapStatus.get("vlt");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Withdrew")){
								    stMaster = mapStatus.get("widrw");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Incomplete")){
								    stMaster = mapStatus.get("icomp");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Hired")){
								    stMaster = mapStatus.get("hird");
								    statusMaster1st.add(stMaster);
						     }   
							 
							 for(SecondaryStatus ss: lstSecondaryStatus)
							  {
								 if(ss.getStatusMaster()!=null)
									 statusMaster1st.add(ss.getStatusMaster()); 
							  }
						    /* if(statusMaster1st.size()==0){ 
									  listByJobStatus = jobForTeacherDAO.findBySecondStatus(lstSecondaryStatus);
							  }else{*/
						     listByJobStatus = jobForTeacherDAO.findAvailableCandidtes(districtMaster2,statusMaster1st,lstSecondaryStatus);
							 // }
							  if(listByJobStatus.size()>0){
									if(checkflag){
				   						jobForTeacherLists.retainAll(listByJobStatus);
				   					}else{
				   						jobForTeacherLists.addAll(listByJobStatus);
				   						checkflag=true;
				   					}
								}else{
									jobForTeacherLists = new ArrayList<JobForTeacher>();
								}
				         }
							  
					 }
				  // Advanced filters + Date Filter Start  Norm Score +A
					
					 List<JobForTeacher> lstDateJFT =new ArrayList<JobForTeacher>();
					 if(pdateFlag){
						 List<TeacherDetail> normTeacherList = new ArrayList<TeacherDetail>();
						 if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") && !normScoreSelectVal.equals("6"))
						 {
							normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore(normScoreVal,normScoreSelectVal);
						 }
						 
						if(!normScoreSelectVal.equals("") && normScoreSelectVal.equals("6"))
						{
							 normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore("0","5");
						}
						 lstDateJFT = jobForTeacherDAO.findJobForTeacherbyInputDateFilterwithDistrict(districtMaster,sfromDate,stoDate,endfromDate ,endtoDate,appsfromDate,appstoDate,normTeacherList ,normScoreSelectVal,intenalchk);
						 if(lstDateJFT!=null && lstDateJFT.size()>0)
						 {
							 if(checkflag){
			   						jobForTeacherLists.retainAll(lstDateJFT);
			   					}else{
			   						jobForTeacherLists.addAll(lstDateJFT);
			   						checkflag=true;
			   					}
						 }else{
							 jobForTeacherLists = new ArrayList<JobForTeacher>();
						 }
					 }
					
					//filter of Hiring Date
					 //List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJobs = new ArrayList<TeacherStatusHistoryForJob>();
					 List<JobForTeacher> lstTeacherStatusHistoryForJobs = new ArrayList<JobForTeacher>();
					 if(!hiredfromDate.equals("") || !hiredtoDate.equals("")){
						 System.out.println("filter of Hiring Date ");
						 List<JobForTeacher> filterHCForTeachers =new ArrayList<JobForTeacher>();
						 List<TeacherDetail> lstHCDetails =  new ArrayList<TeacherDetail>();
						 Map<String,JobForTeacher> mapteacherStatusHistory = new HashMap<String, JobForTeacher>();
 						 //lstTeacherStatusHistoryForJobs = teacherStatusHistoryForJobDAO.findByHIredDAte(districtMaster, hiredfromDate, hiredtoDate);
						 lstTeacherStatusHistoryForJobs = jobForTeacherDAO.findByHIredDAte(districtMaster, hiredfromDate, hiredtoDate);
						 System.out.println("lstTeacherStatusHistoryForJobs :: "+lstTeacherStatusHistoryForJobs.size());
						
 						 if(lstTeacherStatusHistoryForJobs!=null && lstTeacherStatusHistoryForJobs.size()>0){
							 for(JobForTeacher hc : lstTeacherStatusHistoryForJobs){
								 lstHCDetails.add(hc.getTeacherId());
								 mapteacherStatusHistory.put(hc.getJobId().getJobId()+"#"+hc.getTeacherId().getTeacherId(), hc);
							 }
						       List<JobForTeacher> lstHCForTeachers = jobForTeacherDAO.findTeacherByTeacherDeatailAndJobStatus(lstHCDetails);
						       for(JobForTeacher jft: lstHCForTeachers){
						    	   if(mapteacherStatusHistory.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId())!=null){
						    		   filterHCForTeachers.add(jft);
						    	   }
						       }
						       
						       if(checkflag){
		   						    jobForTeacherLists.retainAll(filterHCForTeachers);
				   					}
						       else{
			   						jobForTeacherLists.addAll(filterHCForTeachers);
			   						checkflag=true;
				   				}
						 } 
						 else{
							 jobForTeacherLists = new ArrayList<JobForTeacher>();
						 }
					 }
					 
					 
					 System.out.println("Final ************** jft ::::::::  "+jobForTeacherLists.size());	
					
					
				     ArrayList<Integer> teacherIds = new ArrayList<Integer>();
				     ArrayList<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
				
					 List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
					 List<TeacherNormScore> listTeacherNormScores =null;
					 Map<Integer,TeacherNormScore> mapNormScores = new HashMap<Integer, TeacherNormScore>();
					 Map<String,TeacherAchievementScore> mapTeacherAchievementScore = new HashMap<String, TeacherAchievementScore>();
				     Map<String,JobWiseConsolidatedTeacherScore> mapJWCTeacherScore = new HashMap<String, JobWiseConsolidatedTeacherScore>();
					
				  // map for candidtates count on a job   
				     Map<Integer,List<JobForTeacher>> maptotalCandidates      = new HashMap<Integer, List<JobForTeacher>>();
				     Map<Integer,Integer> maptotalAvalCandidates  = new HashMap<Integer, Integer>();
				     List<DistrictMaster> districtMasterList = new ArrayList<DistrictMaster>();
				     List<JobOrder> lstHUJobOrders = new ArrayList<JobOrder>();
				     List<TeacherDetail> lstHUTeacherDetails = new ArrayList<TeacherDetail>();
				     //Map<String,TeacherStatusHistoryForJob> mapHiredDate =new HashMap<String, TeacherStatusHistoryForJob>(); 
				     Map<String,JobForTeacher> mapHiredDate =new HashMap<String, JobForTeacher>();
				     List<JobCategoryMaster> categoryMasters= new ArrayList<JobCategoryMaster>();
				     
				     List<JobOrder> listJoboOrders = new ArrayList<JobOrder>();
				     String jobIdStr = ""; 
				     if(jobForTeacherLists.size()>0)
				     for(JobForTeacher jft:jobForTeacherLists) 
					 {
				    	 listTeacherDetails.add(jft.getTeacherId());
						 teacherIds.add(jft.getTeacherId().getTeacherId());
						 listJoboOrders.add(jft.getJobId());
						 categoryMasters.add(jft.getJobId().getJobCategoryMaster());
				    	 lstHUJobOrders.add(jft.getJobId());
				    	 lstHUTeacherDetails.add(jft.getTeacherId());
				    	 districtMasterList.add(jft.getJobId().getDistrictMaster());
				    	 if(jobIdStr.equals("")){
				    		 jobIdStr=""+jft.getJobId().getJobId();
				    	 }else{
				    		 jobIdStr=jobIdStr+","+jft.getJobId().getJobId();
				    	 }
						//////////////////////////////////////////////////
				    	 int jobId=jft.getJobId().getJobId();
				    	 List<JobForTeacher> tAlist = maptotalCandidates.get(jobId);
							if(tAlist==null){
								List<JobForTeacher> jobs = new ArrayList<JobForTeacher>();
								jobs.add(jft);
								maptotalCandidates.put(jobId, jobs);
							}else{
								tAlist.add(jft);
								maptotalCandidates.put(jobId, tAlist);
							}
				    	 ////////////////////////////////////////////////
					
					
					 }
				     
				 	for (Map.Entry<Integer,List<JobForTeacher>> entry : maptotalCandidates.entrySet()) {
							 List<JobForTeacher> jfList=entry.getValue();
					 int count=0;
					 for (JobForTeacher jobForTeacher : jfList) {
						 if(!jobForTeacher.getStatus().getStatusShortName().equals("icomp") && !jobForTeacher.getStatus().getStatusShortName().equals("vlt") && !jobForTeacher.getStatus().getStatusShortName().equals("widrw") && !jobForTeacher.getStatus().getStatusShortName().equals("hide")){
							 count++;
						 }
					}
					 maptotalAvalCandidates.put(entry.getKey(),count); 
				   }
				    
				  //*********hired calculation**********************
				 	 List<JobForTeacher>  lstStatusHistoryForJobs = new  ArrayList<JobForTeacher>();
				     if(jobForTeacherLists!=null && jobForTeacherLists.size()>0){
				    	 lstStatusHistoryForJobs = jobForTeacherDAO.findByTeacherIdAndJObIds(districtMaster,lstHUTeacherDetails,lstHUJobOrders);
				         if(lstStatusHistoryForJobs!=null && lstStatusHistoryForJobs.size()>0){
				        	 for(JobForTeacher ht : lstStatusHistoryForJobs){
				        		 mapHiredDate.put(ht.getJobId().getJobId()+"#"+ht.getTeacherId().getTeacherId(),ht);
				        	 }
				         }
				     }
				     
				     
				    
				    // sorting logic start 
				     
				     if(sortOrder.equals("")){
				    	 for(JobForTeacher jf:jobForTeacherLists) 
					    	 jf.setJobTitle(jf.getJobId().getJobTitle());
						
						  Collections.sort(jobForTeacherLists, new JobTitleCompratorASC());
				     }
				     
				     
				     if(sortOrder.equals("lastName")){    
					     for(JobForTeacher jf:jobForTeacherLists) 
						 {
					    	 jf.setLastName(jf.getTeacherId().getLastName());
							
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new LastNameCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new LastNameCompratorDESC());
				     }  
				     if(sortOrder.equals("jobTitle")){    
					     for(JobForTeacher jf:jobForTeacherLists) 
						 {
					    	 jf.setJobTitle(jf.getJobId().getJobTitle());
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new JobTitleCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new JobTitleCompratorDESC());
				     } 
				    
				     if(sortOrder.equals("totalCandidate")){    
					     for(JobForTeacher jf:jobForTeacherLists) 
						 {
					    	 jf.setTotalCandidate(maptotalCandidates.get(jf.getJobId().getJobId()).size());
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new TotalCandidateCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new TotalCandidateCompratorDESC());
				     } 
				 
				     if(sortOrder.equals("avlCandidate")){    
					     for(JobForTeacher jf:jobForTeacherLists) 
						 {
					    	 jf.setAvlCandidate(maptotalAvalCandidates.get(jf.getJobId().getJobId()));
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new AvlCandidateCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new AvlCandidateCompratorDESC());
				     } 
				 
				    
				         
				    Map<String,SecondaryStatus> mapSecStatuss = new HashMap<String, SecondaryStatus>();
				    //List<SecondaryStatus> lstSecondaryStatuss =	secondaryStatusDAO.findSecondaryStatusWithDistrictList(districtMasterList);
				   
						List<SecondaryStatus> lstSecondaryStatuss =	secondaryStatusDAO.findSecondaryStatusByJobCategoryLists(districtMasterList,categoryMasters);
				    if(lstSecondaryStatuss!=null && lstSecondaryStatuss.size()>0)
						for(SecondaryStatus sec : lstSecondaryStatuss ){
							if(sec.getStatusMaster()!=null){								
								if(sec.getJobCategoryMaster()!=null)
								mapSecStatuss.put(sec.getStatusMaster().getStatusId()+"##0"+"#"+sec.getJobCategoryMaster().getJobCategoryId(),sec);
							}else{
								if(sec.getJobCategoryMaster()!=null)
								mapSecStatuss.put("##0"+sec.getSecondaryStatusId()+"#"+sec.getJobCategoryMaster().getJobCategoryId(),sec);
							}
						}				   
			//Candidate Status Sorting 	
				if(sortOrder.equals("candidateStatus"))    
				 for(JobForTeacher jft: jobForTeacherLists){
					 StatusMaster statusObj =jft.getStatusMaster();
						
						String mapKey="";
						try {
							if(jft.getStatusMaster()!=null){
								mapKey=jft.getStatusMaster().getStatusId()+"##0"+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
								if(mapSecStatuss.get(mapKey)!=null){
									statusObj=mapSecStatuss.get(mapKey).getStatusMaster();
								}
							}else{
								mapKey="##0"+jft.getSecondaryStatus().getSecondaryStatusId()+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
							}
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						
						try {
							if(statusObj!=null)
							{
								if(statusObj.getStatusShortName().equalsIgnoreCase("comp")){
									jft.setAssessmentStatus("Available"); 
								}else if(statusObj.getStatusShortName().equalsIgnoreCase("rem")){
									jft.setAssessmentStatus(statusObj.getStatus());
								}else if(statusObj.getStatusShortName().equalsIgnoreCase("icomp")){
									jft.setAssessmentStatus(statusObj.getStatus());
								}else if(statusObj.getStatusShortName().equalsIgnoreCase("vlt")){
									jft.setAssessmentStatus("Time Out"); 
								}else if(statusObj.getStatusShortName().equalsIgnoreCase("scomp")){
									jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
								}else if(statusObj.getStatusShortName().equalsIgnoreCase("ecomp")){
									jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
								}else if(statusObj.getStatusShortName().equalsIgnoreCase("vcomp")){
									jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
								}else if(statusObj.getStatusShortName().equalsIgnoreCase("dcln"))
									jft.setAssessmentStatus("Declined"); 
								else if(statusObj.getStatusShortName().equalsIgnoreCase("widrw"))
									jft.setAssessmentStatus("Withdrew"); 
								else if(statusObj.getStatusShortName().equalsIgnoreCase("hird"))
									jft.setAssessmentStatus("Hired"); 
							}else{
								if(jft.getSecondaryStatus()!=null)
								{
									if(mapSecStatuss.get(mapKey)!=null)
									jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
									else
									jft.setAssessmentStatus(jft.getSecondaryStatus().getSecondaryStatusName()); 	
								}
									 
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				 }
			    if(sortOrder.equals("candidateStatus") && sortOrderType.equals("0")){ 
				 Collections.sort(jobForTeacherLists,JobForTeacher.jobForTeacherComparatorAssessmentStatus ); 
				 }else if(sortOrder.equals("candidateStatus") && sortOrderType.equals("1")){ 
				 Collections.sort(jobForTeacherLists,JobForTeacher.jobForTeacherComparatorAssessmentStatusDesc);
				 }
	    
				     
				if(jobForTeacherLists.size()>0)
				{	
					/*for Teacher norm score*/
					if(listTeacherDetails.size()>0){
					 listTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(listTeacherDetails);
					}
					
					for(TeacherNormScore tns:listTeacherNormScores)
				 	 {
					  mapNormScores.put(tns.getTeacherDetail().getTeacherId(),tns);
				 	 }
					
					// Sorting By normScore 
					//Sorting By A Score
				 	if(sortOrder.equals("normScore")){
							for(JobForTeacher jft : jobForTeacherLists){
								if(mapNormScores.get(jft.getTeacherId().getTeacherId()) != null){
									  jft.setNormScore(Double.valueOf(mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+""));
								}else{
									jft.setNormScore(-0.0);
								}
							}
				 	
				 	 if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorNormScore);
						 else 
						  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorNormScoreDesc);
				 	
				 	}
						
					//for A Score && L/R Schore
				    List<TeacherAchievementScore> listTeacherAchievementScores = teacherAchievementScoreDAO.getAchievementScoreListByTDList(listTeacherDetails);
				 	for(TeacherAchievementScore tas:listTeacherAchievementScores)
				 	 {
				 		mapTeacherAchievementScore.put(tas.getTeacherDetail().getTeacherId().toString(),tas);
				 	 }
					 
				 	//Sorting By A Score
				 	if(sortOrder.equals("aScore")){
							for(JobForTeacher jft : jobForTeacherLists){
								if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()) != null){
									if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore().equals("") && mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()!=null)
									{
									  jft.setaScore(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore());
									}else{
										jft.setaScore(-1);
									}
								}else{
									jft.setaScore(-1);
								}
							}
				 	
				 	 if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorAScore);
						 else 
						  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorAScoreDesc);
				 	
				 	}
				 	
				 	//Sorting By L/R Score
				 	  if(sortOrder.equals("lRScore")){
							for(JobForTeacher jft : jobForTeacherLists){
								if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()) != null){
									if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore().equals("") &&  mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()!=null )
									{ 
									  jft.setlRScore(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore());
									}else{
										jft.setlRScore(-1);
									}
								}else{
									jft.setlRScore(-1);
								}
							}
				 	
				 	    if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorLRScore);
						 else 
						  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorLRScoreDesc);
				 	
				 	}
				 	
					/* for fit score */
					List<JobWiseConsolidatedTeacherScore> listJobWiseCTScore = jobWiseConsolidatedTeacherScoreDAO.findbyTeacherDetails(listTeacherDetails);
				 	for(JobWiseConsolidatedTeacherScore jwct:listJobWiseCTScore)
				 	 {
				 		mapJWCTeacherScore.put(jwct.getTeacherDetail().getTeacherId().toString(),jwct);
				 	 }
					
				 	//Sorting By Fit Score fitScore
				 	 if(sortOrder.equals("fitScore")){
							for(JobForTeacher jft : jobForTeacherLists){
								if(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()) != null){
									if(jft.getTeacherId().getTeacherId().toString().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()) && jft.getJobId().getJobId().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobOrder().getJobId()))
									{ 
									  jft.setFitScore(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseConsolidatedScore());
									}else{
										jft.setFitScore(-1.0);
									}
								}else{
									jft.setFitScore(-1.0);
								}
							}
				 	
				 	    if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorFitScore);
						 else 
						  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorFitScoreDesc);
				 	
				 	}
				 	 if(sortOrder.equals("distName")){  
					     for(JobForTeacher jf:jobForTeacherLists) 
						 {
					    	 jf.setDistName(jf.getJobId().getDistrictMaster().getDistrictName());
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new DistrictNameCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new DistrictNameCompratorDESC());
				     } 
				 	 
				 	if(sortOrder.equals("postDate")){  
					     for(JobForTeacher jf:jobForTeacherLists) 
						 {
					    	 jf.setPostDate(jf.getJobId().getJobStartDate());
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new PostingDateCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new PostingDateCompratorDESC());
				     }
				 	
				 	if(sortOrder.equals("endDate")){  
					     for(JobForTeacher jf:jobForTeacherLists) 
						 {
					    	 jf.setEndDate(jf.getJobId().getJobEndDate());
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new EndDateCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new EndDateCompratorDESC());
				     } 
				 	 
				 	if(sortOrder.equals("appsDate")){  
					     for(JobForTeacher jf:jobForTeacherLists) 
						 {
					    	 jf.setAppsDate(jf.getCreatedDateTime());
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new AppsDateCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new AppsDateCompratorDESC());
				     } 
				 	if(sortOrder.equals("isAffilated"))
				 	{  
				 		for(JobForTeacher jf:jobForTeacherLists) 
						 {
				 			if(jf.getIsAffilated()==null || jf.getIsAffilated()==0)
				 			 jf.setIsAffilated(0);
				 			else
				 			 jf.setIsAffilated(1);
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new InternalCandidateCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new InternalCandidateCompratorDESC());
					    
					    
				     }

				 	if(sortOrder.equals("hiredDate")) {
				 		System.out.println("AAAHHHHHHHHHHHHHHHHHHHHHHHHH");
				 		for(JobForTeacher jf:jobForTeacherLists) {
				 			if(mapHiredDate.get(jf.getJobId().getJobId()+"#"+jf.getTeacherId().getTeacherId())!=null){
				 				System.out.println("1111111111111111111111111111111111");
				 				 // Changes for Hired Date
				 				//  jf.setHiredDate(mapHiredDate.get(jf.getJobId().getJobId()+"#"+jf.getTeacherId().getTeacherId()).getHiredByDate());
				 				jf.setHiredDate(mapHiredDate.get(jf.getJobId().getJobId()+"#"+jf.getTeacherId().getTeacherId()).getCreatedDateTime());
				 			} else {
				 				System.out.println("22222222222222222222222222222222222");
				 				jf.setHiredDate(new Date(0));
				 				//  System.out.println("dddd :: "+new Date(0000-00-00));
				 			}
						 }
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new HiredDateCompratorASC());
						 else
						  Collections.sort(jobForTeacherLists, new HiredDateCompratorDESC());

				 	}
				 	
				 	if(sortOrder.equals("activityDate")){ 
				 		
					     if(sortOrderType.equals("0"))
						  Collections.sort(jobForTeacherLists, new LastActivityDateCompratorASC());
						 else 
						  Collections.sort(jobForTeacherLists, new LastActivityDateCompratorDESC());
				     } 
				 	
				}	
			  System.out.println("xxxxxxxxxxxxxjobForTeacherLists :: "+jobForTeacherLists.size());	
			  System.out.println(" start  ::::::::::::::   "+start);
			  totalRecord=jobForTeacherLists.size();
				if(totalRecord<end)
					end=totalRecord;
			  lstJobForTeacher	=	jobForTeacherLists.subList(start,end);
			
				String responseText="";
				
				tmRecords.append("<table  id='tblGridEEC' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingLink("Candidate Name",sortOrderNoField,"lastName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink("District Name",sortOrderNoField,"distName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Job Title",sortOrderNoField,"jobTitle",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
								
				responseText=PaginationAndSorting.responseSortingLink("Posting  Date",sortOrderNoField,"postDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Expiration  Date ",sortOrderNoField,"endDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Application Date",sortOrderNoField,"appsDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP("EPI/Norm Score",sortOrderFieldName,"normScore",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Application Status",sortOrderFieldName,"candidateStatus",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+" </th>");
				
				
				responseText=PaginationAndSorting.responseSortingLink("Hired Date",sortOrderFieldName,"hiredDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+" </th>");
				
				//responseText=PaginationAndSorting.responseSortingLink("Hired By",sortOrderFieldName,"hiredBy",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>Hired By</th>");
				
				
				
				responseText=PaginationAndSorting.responseSortingLink("Total Candidates",sortOrderNoField,"totalCandidate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Available Candidates",sortOrderNoField,"avlCandidate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
				responseText=PaginationAndSorting.responseSortingLink("Internal",sortOrderFieldName,"isAffilated",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink("A Score",sortOrderFieldName,"aScore",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP("L/R Score",sortOrderFieldName,"lRScore",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP("Fit Score",sortOrderFieldName,"fitScore",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP("Activity Date",sortOrderFieldName,"activityDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				

				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				tmRecords.append("<tbody>");
				
				if(lstJobForTeacher.size()==0){
				 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
				}
							
				 List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJobsActivity = new ArrayList<TeacherStatusHistoryForJob>();
				 Map<String,TeacherStatusHistoryForJob> mapteacherStatusHistoryACtivity = new HashMap<String, TeacherStatusHistoryForJob>();
						
				 if(lstJobForTeacher.size()>0)
				 {
					 List<TeacherDetail> tIds = new ArrayList<TeacherDetail>();
					 List<JobOrder> jIds = new ArrayList<JobOrder>();
					 List<StatusMaster> statusIds = new ArrayList<StatusMaster>();
					 
					 for(JobForTeacher jft:lstJobForTeacher){
						 tIds.add(jft.getTeacherId());
						 jIds.add(jft.getJobId());
						 statusIds.add(jft.getStatus());
					 }
					 					 
					 lstTeacherStatusHistoryForJobsActivity = teacherStatusHistoryForJobDAO.FindStatusForLastActivity(tIds.get(0), jIds.get(0), statusIds.get(0));
					 System.out.println("*****************************************lstTeacherStatusHistoryForJobsActivity======"+lstTeacherStatusHistoryForJobsActivity.size());
					 
					 if(lstTeacherStatusHistoryForJobsActivity!=null && lstTeacherStatusHistoryForJobsActivity.size()>0)
					 {
						 for(TeacherStatusHistoryForJob lastActivitySatatus : lstTeacherStatusHistoryForJobsActivity)
						 {							
							 if(lastActivitySatatus.getTeacherDetail().getTeacherId()!=null && lastActivitySatatus.getJobOrder().getJobId()!=null && lastActivitySatatus.getStatusMaster().getStatusId()!=null)
							 mapteacherStatusHistoryACtivity.put(lastActivitySatatus.getTeacherDetail().getTeacherId()+"#"+lastActivitySatatus.getJobOrder().getJobId()+"#"+lastActivitySatatus.getStatusMaster().getStatusId(), lastActivitySatatus);
						 }					 
					 }				 
				 }
				 
				
				if(lstJobForTeacher.size()>0){
			     for(JobForTeacher jft:lstJobForTeacher) 
				 {				 
				    tmRecords.append("<tr>");	
					
					tmRecords.append("<td>"+jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")"+"</td>");

					tmRecords.append("<td>"+jft.getJobId().getDistrictMaster().getDistrictName()+"</td>");
					
					tmRecords.append("<td>"+jft.getJobId().getJobTitle()+"</td>");
					
					tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobStartDate())+",12:01 AM</td>");
					
					if(Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobEndDate()).equals("Dec 25, 2099"))
						tmRecords.append("<td>Until filled</td>");
					else
						tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobEndDate())+",11:59 PM</td>");
										
					tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getCreatedDateTime())+"</td>");
					
					
					StatusMaster statusObj =jft.getStatusMaster();
					
					String mapKey="";
					try {
						if(jft.getStatusMaster()!=null){
							mapKey=jft.getStatusMaster().getStatusId()+"##0"+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
							if(mapSecStatuss.get(mapKey)!=null){
								statusObj=mapSecStatuss.get(mapKey).getStatusMaster();
							}
						}else{
							mapKey="##0"+jft.getSecondaryStatus().getSecondaryStatusId()+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					String sDidplayStatusName="";
					try {
						if(statusObj!=null)
						{
					
							if(statusObj.getStatusShortName().equalsIgnoreCase("comp")){
								sDidplayStatusName="Available"; 
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("rem")){
								sDidplayStatusName=statusObj.getStatus();
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("icomp")){
								sDidplayStatusName=statusObj.getStatus();
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("vlt")){
								sDidplayStatusName="Time Out"; 
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("scomp")){
								sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName();
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("ecomp")){
								sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName();
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("vcomp")){
								sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName();
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("dcln"))
								sDidplayStatusName="Declined"; 
							else if(statusObj.getStatusShortName().equalsIgnoreCase("widrw"))
								sDidplayStatusName="Withdrew"; 
							else if(statusObj.getStatusShortName().equalsIgnoreCase("hird"))
								sDidplayStatusName="Hired"; 
						}else{
							if(jft.getSecondaryStatus()!=null)
							{
								
								if(mapSecStatuss.get(mapKey)!=null)
									sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName(); 
								else
									sDidplayStatusName=jft.getSecondaryStatus().getSecondaryStatusName(); 
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					
					//for Norm Score	
					if(mapNormScores.get(jft.getTeacherId().getTeacherId())!=null){
						
						String ccsName="";
						String normScoreLen=mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+"";
						if(normScoreLen.length()==1){
							ccsName="nobground1";
						}else if(normScoreLen.length()==2){
							ccsName="nobground2";
						}else{
							ccsName="nobground3";
						}
						String colorName=mapNormScores.get(jft.getTeacherId().getTeacherId()).getDecileColor();
						
						tmRecords.append("<td><span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>"+mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+"</span></td>");
					 // tmRecords.append("<td>"+mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+"</td>");
					}else{
					  tmRecords.append("<td>N/A</td>");
					}
					
					
					tmRecords.append("<td>"+sDidplayStatusName+"</td>");
					
					// Hired Date
					
					if(mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId())!=null){
					  tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId()).getHiredByDate())+"</td>");
					}else{
						tmRecords.append("<td>-</td>");
					}
					
					if(mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId())!=null){
						if(jft.getSchoolMaster()!=null){
							tmRecords.append("<td>"+jft.getSchoolMaster().getSchoolName()+"</td>");
						}else{
							tmRecords.append("<td>"+mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId()).getUserMaster().getDistrictId().getDistrictName()+"</td>");
						}
					}else{
						tmRecords.append("<td>-</td>");
					}
					
					
					// GOD
					//for total and available candidate
					if(maptotalCandidates.get(jft.getJobId().getJobId())!=null){
					tmRecords.append("<td>"+maptotalCandidates.get(jft.getJobId().getJobId()).size()+" </td>");
					}else{
						tmRecords.append("<td>0</td>");
					}
					if(maptotalAvalCandidates.get(jft.getJobId().getJobId())!=null){
					tmRecords.append("<td>"+maptotalAvalCandidates.get(jft.getJobId().getJobId())+" </td>");
					}else{
						tmRecords.append("<td >0</td>");
					}
					
					//Are you district Employee?(Internal)
					if(jft.getIsAffilated()!=null && jft.getIsAffilated()==1)
					tmRecords.append("<td >"+"Yes"+"</td>");
					else
					tmRecords.append("<td >"+"No"+"</td>");
					
					
					// GOO	
				 // for A Score
				   if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
					{
						if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()))
						{
							if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore().equals("") && mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()!=null)
						    tmRecords.append("<td>"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()+"/"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementMaxScore()+"</td>");
							else
							tmRecords.append("<td >N/A</td>");
						}
					}
					else
					{
						tmRecords.append("<td>N/A</td>");
					}
				
				   //for L/R Score 
				   if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
					{
						if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()))
						{
							if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore().equals("") &&  mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()!=null )
						    tmRecords.append("<td >"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()+"/"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementMaxScore()+"</td>");
							else
							tmRecords.append("<td >N/A</td>");
						}else
						{
							tmRecords.append("<td>N/A</td>");
						}
					}
					else
					{
						tmRecords.append("<td>N/A</td>");
					}
				
					   
				  // for Fit Score 	
				 if(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
					{
					  if(jft.getTeacherId().getTeacherId().toString().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()) && jft.getJobId().getJobId().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobOrder().getJobId()))
						{
						    tmRecords.append("<td >"+mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseConsolidatedScore()+"/"+mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseMaxScore()+"</td>");
						}
					     else{	
							 tmRecords.append("<td>N/A</td>");
							 }
					}
					else
					{
						tmRecords.append("<td >N/A</td>");
					}
				 
				 if(jft.getStatus().getStatusShortName().equals("widrw") && jft.getWithdrwanDateTime()!=null)
			     {
			      tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getWithdrwanDateTime())+"</td>"); 
			     }else  if(jft.getLastActivity()!=null)
			     {			 					 
					 tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getLastActivityDate())+"</td>");
					}
				 else if(mapteacherStatusHistoryACtivity.get(jft.getTeacherId().getTeacherId()+"#"+jft.getJobId().getJobId()+"#"+jft.getStatus().getStatusId())!=null)
				 {
					 tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate( mapteacherStatusHistoryACtivity.get(jft.getTeacherId().getTeacherId()+"#"+jft.getJobId().getJobId()+"#"+jft.getStatus().getStatusId()).getCreatedDateTime())+"</td>");
				 }
				 else
					 {
						 tmRecords.append("<td>N/A</td>"); 
					 }
				 				 
				  tmRecords.append("</tr>");
				 }
				}
		

			tmRecords.append("</tbody>");
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));

			}catch(Exception e){
				
				e.printStackTrace();
			}
			
		 return tmRecords.toString();
	  }
		
	 
		 
	public String displayRecordsByEntityTypeEXL(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
			schoolId,String subjectId,String certifications,String jobOrderId,String status,String noOfRow,
			String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo, String firstName,String lastName,
			String emailAddress, String stateId,String certType,String jobCategoryIds,String statusId,
			String sfromDate, String stoDate, String endfromDate, String endtoDate, String appsfromDate, String appstoDate, 
			boolean pdateFlag,String normScoreSelectVal,String normScoreVal, int intenalchk, String hiredfromDate, String hiredtoDate,String jobStatus,int hiddenjob)
	 {System.out.println(sortOrderType+" ::@@@@@@@@@@............ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		//System.out.println("job status-------------------"+jobStatus);
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName=null;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			SubjectMaster subjectMaster	=	null;
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="jobTitle";
				String sortOrderNoField="jobTitle";
				
				boolean deafultFlag=false;
				
				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				
				List<JobForTeacher>	jobForTeacherLists    = new ArrayList<JobForTeacher>();
				List<JobForTeacher>	listByFirstName       = new ArrayList<JobForTeacher>();
				List<JobForTeacher>	jobJobId              = new ArrayList<JobForTeacher>();
				List<JobForTeacher> listByJobCategory     = new ArrayList<JobForTeacher>();
				
				boolean checkflag = false;
				
				if(entityID==2){
					if(schoolId==0){
						jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatusAll(districtMaster);
						if(jobForTeacherLists.size()>0)
						checkflag = true;
					}else{
						List<JobOrder> lstJobOrders =null;
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.allJobOrderBySchool(sclMaster);
						if(lstJobOrders.size()>0){
							jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOBStatus(lstJobOrders);
							checkflag = true;
						}
					}
					
				}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0){
						if(schoolId==0){
							 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					        jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatusAll(districtMaster);
					        if(jobForTeacherLists.size()>0)
					        	checkflag = true;
						}
						else{
							List<JobOrder> lstJobOrders =null;
							SchoolMaster  sclMaster=null;
							sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
							lstJobOrders = schoolInJobOrderDAO.allJobOrderBySchool(sclMaster);
							if(lstJobOrders.size()>0){
								jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOBStatus(lstJobOrders);
								checkflag = true;
							}
						}
					}
					else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					}
				}else if(entityID==3){
					if(schoolId==0){
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
				        jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatus(districtMaster);
				        if(jobForTeacherLists.size()>0)
				        	checkflag = true;
					}else{
						List<JobOrder> lstJobOrders =null;
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.allJobOrderBySchool(sclMaster);
						if(lstJobOrders.size()>0){
							jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOBStatus(lstJobOrders);
							checkflag = true;
						}
					}
				}
			
	//filter start
				//Jobstatus filter *******************************************************************************************************
					if (jobStatus.equals("A") || jobStatus.equals("I")) {
						List<Integer> list = new ArrayList<Integer>();
						for (JobForTeacher jobForTeacher : jobForTeacherLists) {
							list.add(jobForTeacher.getJobId().getJobId());
							// System.out.println("??????????????????       "+jobForTeacher.getJobId().getJobId());
						}
						List<JobOrder> jobstatuslist = new ArrayList<JobOrder>();
						boolean jobstatusflag = false;
						if (jobStatus.equals("A")) {
							districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
							Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
							Criterion criterion = Restrictions.eq("status", jobStatus);
							Criterion criterion2 = Restrictions.in("jobId", list);
							jobstatuslist = jobOrderDAO.findByCriteria(criterion,criterion1, criterion2);
							System.out.println("jobstatuslist---------------"+ jobstatuslist.size() + "           "	+ list.size());
							if (jobstatuslist.size() > 0) {
								jobstatusflag = true;
							}
						}

						if (jobStatus.equals("I")) {
							districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
							Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
							Criterion criterion = Restrictions.eq("status", jobStatus);
							Criterion criterion2 = Restrictions.in("jobId", list);
							jobstatuslist = jobOrderDAO.findByCriteria(criterion,criterion1, criterion2);
							System.out.println("jobstatuslist---------------"+ jobstatuslist.size() + "           "	+ list.size());
							if (jobstatuslist.size() > 0) {
								jobstatusflag = true;
							}
						}

						List<JobForTeacher> jobForTeacher1 = new ArrayList<JobForTeacher>();
						if (jobstatusflag) {
							for (JobOrder jobOrder : jobstatuslist) {
								for (JobForTeacher jobForTeacherLists1 : jobForTeacherLists) {
									if (jobOrder.getJobId().equals(
											jobForTeacherLists1.getJobId().getJobId())) {
										jobForTeacher1.add(jobForTeacherLists1);
										//break;
									}
								}
							}
							jobForTeacherLists = jobForTeacher1;
						}
					}
					
					//Hidden Job filter start**************************************************************************************************
					Boolean hdjob=false;
					if(hiddenjob==1)
					{
						hdjob=true;
					}else if(hiddenjob==0)
					{
						hdjob=false;
					}
					//System.out.println("hiddenjob------------"+hdjob);
						List<Integer> hiddenJobTempList = new ArrayList<Integer>();
						for (JobForTeacher jobForTeacher : jobForTeacherLists) {
							hiddenJobTempList.add(jobForTeacher.getJobId().getJobId());
							// System.out.println("jobForTeacherLists  JobID   "+jobForTeacher.getJobId().getJobId());
						}
						List<JobOrder> hiddenJobList = new ArrayList<JobOrder>();
						boolean hiddenJobflag = false;
						if (hiddenjob!=2) {
							districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
							Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
							Criterion criterion = Restrictions.eq("hiddenJob",hdjob);
							Criterion criterion2 = Restrictions.in("jobId",	hiddenJobTempList);
							hiddenJobList = jobOrderDAO.findByCriteria(criterion,criterion1, criterion2);
							//System.out.println("hiddenJobTempList---------------"+ hiddenJobTempList.size() + "           "	+ hiddenJobList.size());
							if (hiddenJobList.size() > 0) {
								hiddenJobflag = true;
							}else
							{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							}
							}
						List<JobForTeacher> jobForTeacherHiddenJob = new ArrayList<JobForTeacher>();
						if (hiddenJobflag) {
							for (JobOrder jobOrder : hiddenJobList) {
								for (JobForTeacher jobForTeacherLists1 : jobForTeacherLists) {
									if (jobOrder.getJobId().equals(	jobForTeacherLists1.getJobId().getJobId())) {
										jobForTeacherHiddenJob.add(jobForTeacherLists1);
										//break;
									}
								}
							}
							jobForTeacherLists = jobForTeacherHiddenJob;
						}
				// Hidden Filter End			
				//******* first Name , Last Name and Email filter 
				
				List<TeacherDetail> basicSearchList = new ArrayList<TeacherDetail>();
			
				List<Criterion> basicSearch = new ArrayList<Criterion>();
				boolean basicFlag = false;
				
				if(firstName!=null && !firstName.equals(""))
				{
					Criterion criterion = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
					
				if(lastName!=null && !lastName.equals(""))
				{
					Criterion criterion = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
				
				if(emailAddress!=null && !emailAddress.equals(""))
				{
					Criterion criterion = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
				if(basicFlag)
				{
					basicSearchList  = teacherDetailDAO.getTeachersByNameAndEmail(basicSearch);
					if(basicSearchList!=null && basicSearchList.size()>0)
					{
						listByFirstName = jobForTeacherDAO.findTeacherByTeacherDeatailAndJobStatus(basicSearchList);
						 if(listByFirstName!=null && listByFirstName.size()>0){
							 if(checkflag){
			   						jobForTeacherLists.retainAll(listByFirstName);
			   					}else{
			   						jobForTeacherLists.addAll(listByFirstName);
			   						checkflag=true;
			   					}
						 }
						 else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							 }
					}
					else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					 }
				}
				
	   		 //****** job ID filter
				//JobOrder joborder =null;
				if(!jobOrderId.equals(""))
				{
					List<Integer> jobIDS= new ArrayList<Integer>();
					String strjobIds[]=jobOrderId.split(",");
					
					if(strjobIds.length>0)
				     for(String str : strjobIds){
				    	 if(!str.equals(""))
				    	 jobIDS.add(Integer.parseInt(str));
				     }
						jobJobId = jobForTeacherDAO.findbyJobOrderAndStatus(jobIDS);
					
						if (jobJobId.size()>0){ 
							if(checkflag)
							{ 
								jobForTeacherLists.retainAll(jobJobId);
							}
							else{
								jobForTeacherLists.addAll(jobJobId);
								checkflag=true;
							}
							
						}else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						 }
				  }
				
			// Licensure Name/Licensure State filter
				List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
				List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();
                List<JobForTeacher> listBycertification = new ArrayList<JobForTeacher>();
				StateMaster stateMaster=null;
				List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
				
				
				if(!certType.equals("0") && !certType.equals("")||!stateId.equals("0")){
					if(!stateId.equals("0")){
						stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
					}
					certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
					//System.out.println("certificateTypeMasterList:: "+certificateTypeMasterList.size());
					if(certificateTypeMasterList.size()>0)
						certJobOrderList=jobCertificationDAO.findCertificationByJobWithState(certificateTypeMasterList);
					System.out.println("certJobOrderList:: "+certJobOrderList.size());
					if(certJobOrderList.size()>0){
						for(JobOrder job: certJobOrderList){
							jobOrderIds.add(job);
						}
					}
					if(jobOrderIds.size()>0){
						listBycertification = jobForTeacherDAO.getJobForTeacherByJOBStatus(jobOrderIds);
						if(listBycertification.size()>0){
							if(checkflag){
		   						jobForTeacherLists.retainAll(listBycertification);
		   					}else{
		   						jobForTeacherLists.addAll(listBycertification);
		   						checkflag=true;
		   					}
						}else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						}
					}else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					}
				
				}
			
			 //filter job category
				if(!jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
				{
					List<Integer> jobcatIds = new ArrayList<Integer>();
					List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
					String jobCategoryIdStr[] =jobCategoryIds.split(",");
					if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
					{
					 for(String str : jobCategoryIdStr){
				    	 if(!str.equals(""))
				    		 jobcatIds.add(Integer.parseInt(str));
					 } 
			    	 if(jobcatIds.size()>0){
			    		 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
			    	 }
			    	 if(listjJobOrders.size()>0){
			    		 listByJobCategory = jobForTeacherDAO.getJobForTeacherByJOBStatus(listjJobOrders);
			    		 if(listByJobCategory.size()>0){
								if(checkflag){
			   						jobForTeacherLists.retainAll(listByJobCategory);
			   					}else{
			   						jobForTeacherLists.addAll(listByJobCategory);
			   						checkflag=true;
			   					}
							}else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							}
			         }
			    	 else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						 }
					}
				}
				// filter of job status 
				
				List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
				List<JobForTeacher>  listByJobStatus = new ArrayList<JobForTeacher>();
				
				 if(!statusId.equals("") && !statusId.equals("0")){
					 System.out.println(" Filter StatusId Start ::  "+statusId);
					 String statusNames[] = statusId.split("#@");
					
				      if(!ArrayUtils.contains(statusNames,"0"))
				      {	 
						 DistrictMaster districtMaster2 = districtMasterDAO.findById(districtOrSchoolId, false, false);
						 lstSecondaryStatus = secondaryStatusDAO.findByMultipleStatusName(districtMaster2, statusNames);
				
						  List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
						  Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
						  for (StatusMaster statusMaster1 : statusMasterList) {
							  mapStatus.put(statusMaster1.getStatusShortName(),statusMaster1);
						  }
						  StatusMaster stMaster =null;
						  List <StatusMaster> statusMaster1st=new ArrayList<StatusMaster>();
						  
						  if(ArrayUtils.contains(statusNames,"Available")){
								  stMaster = mapStatus.get("comp");
								  statusMaster1st.add(stMaster);
						  }
						  if(ArrayUtils.contains(statusNames,"Rejected")){
							    stMaster = mapStatus.get("rem");
							    statusMaster1st.add(stMaster);
					     }
						  if(ArrayUtils.contains(statusNames,"Timed Out")){
							    stMaster = mapStatus.get("vlt");
							    statusMaster1st.add(stMaster);
					     }
						 if(ArrayUtils.contains(statusNames,"Withdrew")){
							    stMaster = mapStatus.get("widrw");
							    statusMaster1st.add(stMaster);
					     }
						 if(ArrayUtils.contains(statusNames,"Incomplete")){
							    stMaster = mapStatus.get("icomp");
							    statusMaster1st.add(stMaster);
					     }
						 if(ArrayUtils.contains(statusNames,"Hired")){
							    stMaster = mapStatus.get("hird");
							    statusMaster1st.add(stMaster);
					     }   
						 
						 for(SecondaryStatus ss: lstSecondaryStatus)
						  {
							 if(ss.getStatusMaster()!=null)
								 statusMaster1st.add(ss.getStatusMaster()); 
						  }
					    /* if(statusMaster1st.size()==0){ 
								  listByJobStatus = jobForTeacherDAO.findBySecondStatus(lstSecondaryStatus);
						  }else{*/
					     listByJobStatus = jobForTeacherDAO.findAvailableCandidtes(districtMaster2,statusMaster1st,lstSecondaryStatus);
						 // }
						  if(listByJobStatus.size()>0){
								if(checkflag){
			   						jobForTeacherLists.retainAll(listByJobStatus);
			   					}else{
			   						jobForTeacherLists.addAll(listByJobStatus);
			   						checkflag=true;
			   					}
							}else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							}
			         }
						  
				 }
			  // Advanced filters + Date Filter Start  Norm Score +A
				
				 List<JobForTeacher> lstDateJFT =new ArrayList<JobForTeacher>();
				 if(pdateFlag){
					 List<TeacherDetail> normTeacherList = new ArrayList<TeacherDetail>();
					 if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") && !normScoreSelectVal.equals("6"))
					 {
						normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore(normScoreVal,normScoreSelectVal);
					 }
					 
					if(!normScoreSelectVal.equals("") && normScoreSelectVal.equals("6"))
					{
						 normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore("0","5");
					}
					 lstDateJFT = jobForTeacherDAO.findJobForTeacherbyInputDateFilterwithDistrict(districtMaster,sfromDate,stoDate,endfromDate,endtoDate,appsfromDate,appstoDate,normTeacherList ,normScoreSelectVal,intenalchk);
					 if(lstDateJFT!=null && lstDateJFT.size()>0)
					 {
						 if(checkflag){
		   						jobForTeacherLists.retainAll(lstDateJFT);
		   					}else{
		   						jobForTeacherLists.addAll(lstDateJFT);
		   						checkflag=true;
		   					}
					 }else{
						 jobForTeacherLists = new ArrayList<JobForTeacher>();
					 }
				 }
				
				//filter of Hiring Date
				 List<JobForTeacher> lstTeacherStatusHistoryForJobs = new ArrayList<JobForTeacher>();
				 if(!hiredfromDate.equals("") || !hiredtoDate.equals("")){
					 System.out.println("filter of Hiring Date ");
					 List<JobForTeacher> filterHCForTeachers =new ArrayList<JobForTeacher>();
					 List<TeacherDetail> lstHCDetails =  new ArrayList<TeacherDetail>();
					 Map<String,JobForTeacher> mapteacherStatusHistory = new HashMap<String, JobForTeacher>();
						 lstTeacherStatusHistoryForJobs = jobForTeacherDAO.findByHIredDAte(districtMaster, hiredfromDate, hiredtoDate);
					System.out.println("lstTeacherStatusHistoryForJobs :: "+lstTeacherStatusHistoryForJobs.size());
					
						 if(lstTeacherStatusHistoryForJobs!=null && lstTeacherStatusHistoryForJobs.size()>0){
						 for(JobForTeacher hc : lstTeacherStatusHistoryForJobs){
							 lstHCDetails.add(hc.getTeacherId());
							 mapteacherStatusHistory.put(hc.getJobId().getJobId()+"#"+hc.getTeacherId().getTeacherId(), hc);
						 }
					       List<JobForTeacher> lstHCForTeachers = jobForTeacherDAO.findTeacherByTeacherDeatailAndJobStatus(lstHCDetails);
					       for(JobForTeacher jft: lstHCForTeachers){
					    	   if(mapteacherStatusHistory.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId())!=null){
					    		   filterHCForTeachers.add(jft);
					    	   }
					       }
					       
					       if(checkflag){
	   						    jobForTeacherLists.retainAll(filterHCForTeachers);
			   					}
					       else{
		   						jobForTeacherLists.addAll(filterHCForTeachers);
		   						checkflag=true;
			   				}
					 } 
					 else{
						 jobForTeacherLists = new ArrayList<JobForTeacher>();
					 }
				 }
				 
				 
				 System.out.println("Final ************** jft ::::::::  "+jobForTeacherLists.size());	
				
				
			     ArrayList<Integer> teacherIds = new ArrayList<Integer>();
			     ArrayList<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
			
				 List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
				 List<TeacherNormScore> listTeacherNormScores =null;
				 Map<Integer,TeacherNormScore> mapNormScores = new HashMap<Integer, TeacherNormScore>();
				 Map<String,TeacherAchievementScore> mapTeacherAchievementScore = new HashMap<String, TeacherAchievementScore>();
			     Map<String,JobWiseConsolidatedTeacherScore> mapJWCTeacherScore = new HashMap<String, JobWiseConsolidatedTeacherScore>();
				
			  // map for candidtates count on a job   
			     Map<Integer,List<JobForTeacher>> maptotalCandidates      = new HashMap<Integer, List<JobForTeacher>>();
			     Map<Integer,Integer> maptotalAvalCandidates  = new HashMap<Integer, Integer>();
			     List<DistrictMaster> districtMasterList = new ArrayList<DistrictMaster>();
			     List<JobOrder> lstHUJobOrders = new ArrayList<JobOrder>();
			     List<TeacherDetail> lstHUTeacherDetails = new ArrayList<TeacherDetail>();
			     Map<String,JobForTeacher> mapHiredDate =new HashMap<String, JobForTeacher>(); 
			   
			     List<JobCategoryMaster> categoryMasters= new ArrayList<JobCategoryMaster>();
			     
			     List<JobOrder> listJoboOrders = new ArrayList<JobOrder>();
			      
					
			     
			     String jobIdStr = ""; 
			     if(jobForTeacherLists.size()>0)
			     for(JobForTeacher jft:jobForTeacherLists) 
				 {
			    	 listTeacherDetails.add(jft.getTeacherId());
					 teacherIds.add(jft.getTeacherId().getTeacherId());
					 listJoboOrders.add(jft.getJobId());
					 categoryMasters.add(jft.getJobId().getJobCategoryMaster());
			    	 lstHUJobOrders.add(jft.getJobId());
			    	 lstHUTeacherDetails.add(jft.getTeacherId());
			    	 districtMasterList.add(jft.getJobId().getDistrictMaster());
			    	 if(jobIdStr.equals("")){
			    		 jobIdStr=""+jft.getJobId().getJobId();
			    	 }else{
			    		 jobIdStr=jobIdStr+","+jft.getJobId().getJobId();
			    	 }
					//////////////////////////////////////////////////
			    	 int jobId=jft.getJobId().getJobId();
			    	 List<JobForTeacher> tAlist = maptotalCandidates.get(jobId);
						if(tAlist==null){
							List<JobForTeacher> jobs = new ArrayList<JobForTeacher>();
							jobs.add(jft);
							maptotalCandidates.put(jobId, jobs);
						}else{
							tAlist.add(jft);
							maptotalCandidates.put(jobId, tAlist);
						}
			    	 ////////////////////////////////////////////////
						
						
				 }
			     
			 	for (Map.Entry<Integer,List<JobForTeacher>> entry : maptotalCandidates.entrySet()) {
						 List<JobForTeacher> jfList=entry.getValue();
						 int count=0;
						 for (JobForTeacher jobForTeacher : jfList) {
							 if(!jobForTeacher.getStatus().getStatusShortName().equals("icomp") && !jobForTeacher.getStatus().getStatusShortName().equals("vlt") && !jobForTeacher.getStatus().getStatusShortName().equals("widrw") && !jobForTeacher.getStatus().getStatusShortName().equals("hide")){
								 count++;
							 }
						}
						// System.out.println("count::::::"+count);
						 maptotalAvalCandidates.put(entry.getKey(),count); 
			   }
			    
			  //*********hired calculation**********************
			     List<JobForTeacher>  lstStatusHistoryForJobs = new  ArrayList<JobForTeacher>();
			     if(jobForTeacherLists!=null && jobForTeacherLists.size()>0){
			    	 lstStatusHistoryForJobs = jobForTeacherDAO.findByTeacherIdAndJObIds(districtMaster,lstHUTeacherDetails,lstHUJobOrders);
			         if(lstStatusHistoryForJobs!=null && lstStatusHistoryForJobs.size()>0){
			        	 for(JobForTeacher ht : lstStatusHistoryForJobs){
			        		 mapHiredDate.put(ht.getJobId().getJobId()+"#"+ht.getTeacherId().getTeacherId(),ht);
			        	 }
			         }
			     }
			     
			     
			    
			    // sorting logic start 
			     
			     if(sortOrder.equals("")){
			    	 for(JobForTeacher jf:jobForTeacherLists) 
				    	 jf.setJobTitle(jf.getJobId().getJobTitle());
					
					  Collections.sort(jobForTeacherLists, new JobTitleCompratorASC());
			     }
			     
			     
			     if(sortOrder.equals("lastName")){    
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setLastName(jf.getTeacherId().getLastName());
						
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new LastNameCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new LastNameCompratorDESC());
			     }  
			     if(sortOrder.equals("jobTitle")){    
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setJobTitle(jf.getJobId().getJobTitle());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new JobTitleCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new JobTitleCompratorDESC());
			     } 
			    
			     if(sortOrder.equals("totalCandidate")){    
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setTotalCandidate(maptotalCandidates.get(jf.getJobId().getJobId()).size());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new TotalCandidateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new TotalCandidateCompratorDESC());
			     } 
			 
			     if(sortOrder.equals("avlCandidate")){    
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setAvlCandidate(maptotalAvalCandidates.get(jf.getJobId().getJobId()));
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new AvlCandidateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new AvlCandidateCompratorDESC());
			     } 
			 
			    
			         
			    Map<String,SecondaryStatus> mapSecStatuss = new HashMap<String, SecondaryStatus>();
			    //List<SecondaryStatus> lstSecondaryStatuss =	secondaryStatusDAO.findSecondaryStatusWithDistrictList(districtMasterList);
			   
					List<SecondaryStatus> lstSecondaryStatuss =	secondaryStatusDAO.findSecondaryStatusByJobCategoryLists(districtMasterList,categoryMasters);
			    if(lstSecondaryStatuss!=null && lstSecondaryStatuss.size()>0)
					for(SecondaryStatus sec : lstSecondaryStatuss ){
						if(sec.getStatusMaster()!=null){
							mapSecStatuss.put(sec.getStatusMaster().getStatusId()+"##0"+"#"+sec.getJobCategoryMaster().getJobCategoryId(),sec);
						}else{
							mapSecStatuss.put("##0"+sec.getSecondaryStatusId()+"#"+sec.getJobCategoryMaster().getJobCategoryId(),sec);
						}
					}
			   	  

			   
		//Candidate Status Sorting 	
			if(sortOrder.equals("candidateStatus"))    
			 for(JobForTeacher jft: jobForTeacherLists){
				 StatusMaster statusObj =jft.getStatusMaster();
					
					String mapKey="";
					try {
						if(jft.getStatusMaster()!=null){
							mapKey=jft.getStatusMaster().getStatusId()+"##0"+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
							if(mapSecStatuss.get(mapKey)!=null){
								statusObj=mapSecStatuss.get(mapKey).getStatusMaster();
							}
						}else{
							mapKey="##0"+jft.getSecondaryStatus().getSecondaryStatusId()+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					
					try {
						if(statusObj!=null)
						{
							if(statusObj.getStatusShortName().equalsIgnoreCase("comp")){
								jft.setAssessmentStatus("Available"); 
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("rem")){
								jft.setAssessmentStatus(statusObj.getStatus());
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("icomp")){
								jft.setAssessmentStatus(statusObj.getStatus());
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("vlt")){
								jft.setAssessmentStatus("Time Out"); 
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("scomp")){
								jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("ecomp")){
								jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("vcomp")){
								jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("dcln"))
								jft.setAssessmentStatus("Declined"); 
							else if(statusObj.getStatusShortName().equalsIgnoreCase("widrw"))
								jft.setAssessmentStatus("Withdrew"); 
							else if(statusObj.getStatusShortName().equalsIgnoreCase("hird"))
								jft.setAssessmentStatus("Hired"); 
						}else{
							if(jft.getSecondaryStatus()!=null)
							{
								if(mapSecStatuss.get(mapKey)!=null)
								jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
								else
								jft.setAssessmentStatus(jft.getSecondaryStatus().getSecondaryStatusName());
							}
								 
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			 }
		    if(sortOrder.equals("candidateStatus") && sortOrderType.equals("0")){ 
			 Collections.sort(jobForTeacherLists,JobForTeacher.jobForTeacherComparatorAssessmentStatus ); 
			 }else if(sortOrder.equals("candidateStatus") && sortOrderType.equals("1")){ 
			 Collections.sort(jobForTeacherLists,JobForTeacher.jobForTeacherComparatorAssessmentStatusDesc);
			 }
    
			     
			if(jobForTeacherLists.size()>0)
			{	
				/*for Teacher norm score*/
				if(listTeacherDetails.size()>0){
				 listTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(listTeacherDetails);
				}
				
				for(TeacherNormScore tns:listTeacherNormScores)
			 	 {
				  mapNormScores.put(tns.getTeacherDetail().getTeacherId(),tns);
			 	 }
				
				// Sorting By normScore 
				//Sorting By A Score
			 	if(sortOrder.equals("normScore")){
						for(JobForTeacher jft : jobForTeacherLists){
							if(mapNormScores.get(jft.getTeacherId().getTeacherId()) != null){
								  jft.setNormScore(Double.valueOf(mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+""));
							}else{
								jft.setNormScore(-0.0);
							}
						}
			 	
			 	 if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorNormScore);
					 else 
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorNormScoreDesc);
			 	
			 	}
					
				//for A Score && L/R Schore
			    List<TeacherAchievementScore> listTeacherAchievementScores = teacherAchievementScoreDAO.getAchievementScoreListByTDList(listTeacherDetails);
			 	for(TeacherAchievementScore tas:listTeacherAchievementScores)
			 	 {
			 		mapTeacherAchievementScore.put(tas.getTeacherDetail().getTeacherId().toString(),tas);
			 	 }
				 
			 	//Sorting By A Score
			 	if(sortOrder.equals("aScore")){
						for(JobForTeacher jft : jobForTeacherLists){
							if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()) != null){
								if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore().equals("") && mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()!=null)
								{
								  jft.setaScore(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore());
								}else{
									jft.setaScore(-1);
								}
							}else{
								jft.setaScore(-1);
							}
						}
			 	
			 	 if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorAScore);
					 else 
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorAScoreDesc);
			 	
			 	}
			 	
			 	//Sorting By L/R Score
			 	  if(sortOrder.equals("lRScore")){
						for(JobForTeacher jft : jobForTeacherLists){
							if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()) != null){
								if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore().equals("") &&  mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()!=null )
								{ 
								  jft.setlRScore(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore());
								}else{
									jft.setlRScore(-1);
								}
							}else{
								jft.setlRScore(-1);
							}
						}
			 	
			 	    if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorLRScore);
					 else 
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorLRScoreDesc);
			 	
			 	}
			 	
				/* for fit score */
				List<JobWiseConsolidatedTeacherScore> listJobWiseCTScore = jobWiseConsolidatedTeacherScoreDAO.findbyTeacherDetails(listTeacherDetails);
			 	for(JobWiseConsolidatedTeacherScore jwct:listJobWiseCTScore)
			 	 {
			 		mapJWCTeacherScore.put(jwct.getTeacherDetail().getTeacherId().toString(),jwct);
			 	 }
				
			 	//Sorting By Fit Score fitScore
			 	 if(sortOrder.equals("fitScore")){
						for(JobForTeacher jft : jobForTeacherLists){
							if(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()) != null){
								if(jft.getTeacherId().getTeacherId().toString().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()) && jft.getJobId().getJobId().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobOrder().getJobId()))
								{ 
								  jft.setFitScore(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseConsolidatedScore());
								}else{
									jft.setFitScore(-1.0);
								}
							}else{
								jft.setFitScore(-1.0);
							}
						}
			 	
			 	    if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorFitScore);
					 else 
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorFitScoreDesc);
			 	
			 	}
			 	 if(sortOrder.equals("distName")){  
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setDistName(jf.getJobId().getDistrictMaster().getDistrictName());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new DistrictNameCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new DistrictNameCompratorDESC());
			     } 
			 	 
			 	if(sortOrder.equals("postDate")){  
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setPostDate(jf.getJobId().getJobStartDate());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new PostingDateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new PostingDateCompratorDESC());
			     }
			 	
			 	if(sortOrder.equals("endDate")){  
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setEndDate(jf.getJobId().getJobEndDate());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new EndDateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new EndDateCompratorDESC());
			     } 
			 	 
			 	if(sortOrder.equals("appsDate")){  
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setAppsDate(jf.getCreatedDateTime());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new AppsDateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new AppsDateCompratorDESC());
			     } 
			 	if(sortOrder.equals("isAffilated"))
			 	{  
			 		for(JobForTeacher jf:jobForTeacherLists) 
					 {
			 			if(jf.getIsAffilated()==null || jf.getIsAffilated()==0)
			 			 jf.setIsAffilated(0);
			 			else
			 			 jf.setIsAffilated(1);
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new InternalCandidateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new InternalCandidateCompratorDESC());
				    
				    
			     } 
			 	if(sortOrder.equals("hiredDate"))
			 	{  
			 		for(JobForTeacher jf:jobForTeacherLists) 
					 {
			 			if(mapHiredDate.get(jf.getJobId().getJobId()+"#"+jf.getTeacherId().getTeacherId())!=null){
			 				jf.setHiredDate(mapHiredDate.get(jf.getJobId().getJobId()+"#"+jf.getTeacherId().getTeacherId()).getCreatedDateTime());
			 			}
			 			else{
			 				jf.setHiredDate(new Date(0));
			 			}
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new HiredDateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new HiredDateCompratorDESC());
				    
				    
			     } 
			 	
			 	if(sortOrder.equals("activityDate")){ 
			 		
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new LastActivityDateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new LastActivityDateCompratorDESC());
			     } 
			}	
		  System.out.println("xxxxxxxxxxxxxjobForTeacherLists :: "+jobForTeacherLists.size());	

//  Excel   Exporting	
			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			//String basePath = request.getRealPath("/")+"/candidate";
			String basePath = request.getSession().getServletContext().getRealPath ("/")+"/candidatestatusreport";
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
			fileName ="candidatestatusreport"+time+".xls";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);

			file = new File(basePath+"/"+fileName);
			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));
			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;
			WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
			workbook.createSheet(" candidatestatusreport", 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);
			// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);
			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			header.setBackground(Colour.GRAY_25);
			// Write a few headers
			excelSheet.mergeCells(0, 0, 16, 1);
			Label label;
			label = new Label(0, 0, " Candidate Job Status Report ", timesBoldUnderline);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 3,16, 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			excelSheet.getSettings().setDefaultColumnWidth(18);
			int k=4;
			int col=1;
			label = new Label(0, k, "Candidate Name",header); 
			excelSheet.addCell(label);
			label = new Label(1, k, "District Name",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k, "Job Title",header); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "Posting Date",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k, "Expiration Date",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k, "Application Date",header); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "EPI/Norm Score",header); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "Application Status",header); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "Hired Date",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k, "Hired By",header); 
			excelSheet.addCell(label);
			
			
			
			label = new Label(++col, k, "Total Candidates",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k, "Available Candidates",header); 
			
			excelSheet.addCell(label);
			label = new Label(++col, k, "Internal Candidate",header); 
			
			excelSheet.addCell(label);
			label = new Label(++col, k, "A Score",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k, "L/R Score",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k, "Fit Score",header); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "Activity Date",header); 
			excelSheet.addCell(label);
				
			 
			
			k=k+1;
			if(jobForTeacherLists.size()==0)
			{	
				excelSheet.mergeCells(0, k, 8, k);
				label = new Label(0, k, "No Records Founds"); 
				excelSheet.addCell(label);
			}
			HashMap<String, WritableCellFormat> mapColors= new HashMap<String, WritableCellFormat>();
			
			
			if (jobForTeacherLists.size()>0) {
				WritableCellFormat cellFormat = new WritableCellFormat();
				Color color = Color.decode("0xFF0000");
				int red = color.getRed();
				int blue = color.getBlue();
				int green = color.getGreen();
				workbook.setColourRGB(Colour.AQUA, red, green, blue);
				cellFormat.setBackground(Colour.AQUA);
				mapColors.put("FF0000", cellFormat);
				WritableCellFormat cellFormat1 = new WritableCellFormat();
				Color color1 = Color.decode("0xFF6666");
				workbook.setColourRGB(Colour.BLUE, color1.getRed(), color1
						.getGreen(), color1.getBlue());
				cellFormat1.setBackground(Colour.BLUE);
				mapColors.put("FF6666", cellFormat1);
				WritableCellFormat cellFormat2 = new WritableCellFormat();
				Color color2 = Color.decode("0xFFCCCC");
				workbook.setColourRGB(Colour.BROWN, color2.getRed(), color2
						.getGreen(), color2.getBlue());
				cellFormat2.setBackground(Colour.BROWN);
				mapColors.put("FFCCCC", cellFormat2);
				WritableCellFormat cellFormat3 = new WritableCellFormat();
				Color color3 = Color.decode("0xFF9933");
				workbook.setColourRGB(Colour.CORAL, color3.getRed(), color3
						.getGreen(), color3.getBlue());
				cellFormat3.setBackground(Colour.CORAL);
				mapColors.put("FF9933", cellFormat3);
				WritableCellFormat cellFormat4 = new WritableCellFormat();
				Color color4 = Color.decode("0xFFFFCC");
				workbook.setColourRGB(Colour.GREEN, color4.getRed(), color4
						.getGreen(), color4.getBlue());
				cellFormat4.setBackground(Colour.GREEN);
				mapColors.put("FFFFCC", cellFormat4);
				WritableCellFormat cellFormat5 = new WritableCellFormat();
				Color color5 = Color.decode("0xFFFF00");
				workbook.setColourRGB(Colour.INDIGO, color5.getRed(), color5
						.getGreen(), color5.getBlue());
				cellFormat5.setBackground(Colour.INDIGO);
				mapColors.put("FFFF00", cellFormat5);
				WritableCellFormat cellFormat6 = new WritableCellFormat();
				Color color6 = Color.decode("0x66CC66");
				workbook.setColourRGB(Colour.LAVENDER, color6.getRed(), color6
						.getGreen(), color6.getBlue());
				cellFormat6.setBackground(Colour.LAVENDER);
				mapColors.put("66CC66", cellFormat6);
				WritableCellFormat cellFormat7 = new WritableCellFormat();
				Color color7 = Color.decode("0x00FF00");
				workbook.setColourRGB(Colour.LIME, color7.getRed(), color7
						.getGreen(), color7.getBlue());
				cellFormat7.setBackground(Colour.LIME);
				mapColors.put("00FF00", cellFormat7);
				WritableCellFormat cellFormat8 = new WritableCellFormat();
				Color color8 = Color.decode("0x006633");
				workbook.setColourRGB(Colour.ORANGE, color8.getRed(), color8
						.getGreen(), color8.getBlue());
				cellFormat8.setBackground(Colour.ORANGE);
				mapColors.put("006633", cellFormat8);
			}
			
			 List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJobsActivity = new ArrayList<TeacherStatusHistoryForJob>();
			 Map<String,TeacherStatusHistoryForJob> mapteacherStatusHistoryACtivity = new HashMap<String, TeacherStatusHistoryForJob>();
					
			 if(jobForTeacherLists.size()>0)
			 {
				 List<TeacherDetail> tIds = new ArrayList<TeacherDetail>();
				 List<JobOrder> jIds = new ArrayList<JobOrder>();
				 List<StatusMaster> statusIds = new ArrayList<StatusMaster>();
				 
				 for(JobForTeacher jft:jobForTeacherLists){
					 tIds.add(jft.getTeacherId());
					 jIds.add(jft.getJobId());
					 statusIds.add(jft.getStatus());
				 }
				 					 
				 lstTeacherStatusHistoryForJobsActivity = teacherStatusHistoryForJobDAO.FindStatusForLastActivity(tIds.get(0), jIds.get(0), statusIds.get(0));
				 System.out.println("*****************************************lstTeacherStatusHistoryForJobsActivity======"+lstTeacherStatusHistoryForJobsActivity.size());
				 
				 if(lstTeacherStatusHistoryForJobsActivity!=null && lstTeacherStatusHistoryForJobsActivity.size()>0)
				 {
					 for(TeacherStatusHistoryForJob lastActivitySatatus : lstTeacherStatusHistoryForJobsActivity)
					 {							
						 if(lastActivitySatatus.getTeacherDetail().getTeacherId()!=null && lastActivitySatatus.getJobOrder().getJobId()!=null && lastActivitySatatus.getStatusMaster().getStatusId()!=null)
						 mapteacherStatusHistoryACtivity.put(lastActivitySatatus.getTeacherDetail().getTeacherId()+"#"+lastActivitySatatus.getJobOrder().getJobId()+"#"+lastActivitySatatus.getStatusMaster().getStatusId(), lastActivitySatatus);
					 }					 
				 }				 
			 }			
			
			if(jobForTeacherLists.size()>0){
			 for(JobForTeacher jft:jobForTeacherLists) 
			 {
				 col=1;
				 String name=jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"("+jft.getTeacherId().getEmailAddress()+")";
				 label = new Label(0, k, name); 
				 excelSheet.addCell(label);
				 
				 label = new Label(1, k, jft.getJobId().getDistrictMaster().getDistrictName()); 
				 excelSheet.addCell(label);
				 
				 label = new Label(++col, k, jft.getJobId().getJobTitle()); 
				 excelSheet.addCell(label);
				 
				 label = new Label(++col, k, Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobStartDate())+",12:01 AM"); 
				 excelSheet.addCell(label);
				 if(Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobEndDate()).equals("Dec 25, 2099"))
					 label = new Label(++col, k, "Until filled");
				 else
				 label = new Label(++col, k, Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobEndDate())+",11:59 PM"); 
				 excelSheet.addCell(label);
				 label = new Label(++col, k, Utility.convertDateAndTimeToUSformatOnlyDate(jft.getCreatedDateTime())); 
				 excelSheet.addCell(label);
				 
					//for Norm Score	
					if(mapNormScores.get(jft.getTeacherId().getTeacherId())!=null){
						
						String colorName=mapNormScores.get(jft.getTeacherId().getTeacherId()).getDecileColor();
						System.out.println("colorName:::::::"+colorName);
						System.out.println("mapColors.get(colorName)::::"+mapColors.get(colorName));
						label = new Label(++col, k, String.valueOf(mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()),mapColors.get(colorName)); 
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, "N/A"); 
						excelSheet.addCell(label);
					}
				 
				
				 StatusMaster statusObj =jft.getStatusMaster();
					
					String mapKey="";
					try {
						if(jft.getStatusMaster()!=null){
							mapKey=jft.getStatusMaster().getStatusId()+"##0"+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
							if(mapSecStatuss.get(mapKey)!=null){
								statusObj=mapSecStatuss.get(mapKey).getStatusMaster();
							}
						}else{
							mapKey="##0"+jft.getSecondaryStatus().getSecondaryStatusId()+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					String sDidplayStatusName="";
					
					
					try {
						if(statusObj!=null)
						{
							if(statusObj.getStatusShortName().equalsIgnoreCase("comp")){
								sDidplayStatusName="Available"; 
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("rem")){
								sDidplayStatusName=statusObj.getStatus();
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("icomp")){
								sDidplayStatusName=statusObj.getStatus();
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("vlt")){
								sDidplayStatusName="Time Out"; 
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("scomp")){
								sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName();
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("ecomp")){
								sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName();
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("vcomp")){
								sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName();
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("dcln"))
								sDidplayStatusName="Declined"; 
							else if(statusObj.getStatusShortName().equalsIgnoreCase("widrw"))
								sDidplayStatusName="Withdrew"; 
							else if(statusObj.getStatusShortName().equalsIgnoreCase("hird"))
								sDidplayStatusName="Hired"; 
						}else{
							if(jft.getSecondaryStatus()!=null)
								if(mapSecStatuss.get(mapKey)!=null){
									sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName(); 
								}
								else 
								sDidplayStatusName=jft.getSecondaryStatus().getSecondaryStatusName();
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					label = new Label(++col, k, sDidplayStatusName); 
					excelSheet.addCell(label);
					
					if(mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId())!=null){
						label = new Label(++col, k, Utility.convertDateAndTimeToUSformatOnlyDate(mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId()).getCreatedDateTime())); 
						excelSheet.addCell(label);
						}else{
							label = new Label(++col, k, "-"); 
							excelSheet.addCell(label);
						}
					if(mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId())!=null){
							if(jft.getSchoolMaster()!=null){
							   label = new Label(++col, k, jft.getSchoolMaster().getSchoolName()); 
							excelSheet.addCell(label);
						}else {
							label = new Label(++col, k, mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId()).getUserMaster().getDistrictId().getDistrictName()); 
							excelSheet.addCell(label);
						}
					}else{
						label = new Label(++col, k, "-"); 
						excelSheet.addCell(label);
					}
					
					
					
				
				 
				//for total and available candidate
				if(maptotalCandidates.get(jft.getJobId().getJobId())!=null){
					label = new Label(++col, k, String.valueOf(maptotalCandidates.get(jft.getJobId().getJobId()).size())); 
					excelSheet.addCell(label);
				}else{
					label = new Label(++col, k, "0"); 
					excelSheet.addCell(label);
				}
				
				if(maptotalAvalCandidates.get(jft.getJobId().getJobId())!=null){
					label = new Label(++col, k, String.valueOf(maptotalAvalCandidates.get(jft.getJobId().getJobId()))); 
					excelSheet.addCell(label);
				}else{
					label = new Label(++col, k, "0"); 
					excelSheet.addCell(label);
				}
				
				
				
				//Are you district Employee?(Internal)
				if(jft.getIsAffilated()!=null && jft.getIsAffilated()==1){
					label = new Label(++col, k, "Yes"); 
					excelSheet.addCell(label);
				
				}
				else{
					label = new Label(++col, k, "No"); 
					excelSheet.addCell(label);
				}
				
				
				
			 // for A Score
			   if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
					if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()))
					{
						if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore().equals("") && mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()!=null)
					    {
							label = new Label(++col, k, mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()+"/"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementMaxScore()); 
							excelSheet.addCell(label);
					    }else{
					    	label = new Label(++col, k, "N/A"); 
							excelSheet.addCell(label);
					    }
					}
				}
				else
				{
					label = new Label(++col, k, "N/A"); 
					excelSheet.addCell(label);
				}
			
			   //for L/R Score 
			   if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
					if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()))
					{
						if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore().equals("") &&  mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()!=null )
						{
							label = new Label(++col, k, mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()+"/"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementMaxScore()); 
							excelSheet.addCell(label);
						}else
						{
							label = new Label(++col, k, "N/A"); 
							excelSheet.addCell(label);
						}
					}else
					{
						label = new Label(++col, k, "N/A"); 
						excelSheet.addCell(label);
					}
				}
				else
				{
					label = new Label(++col, k, "N/A"); 
					excelSheet.addCell(label);
				}
			
				   
			  // for Fit Score 	
			 if(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
				  if(jft.getTeacherId().getTeacherId().toString().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()) && jft.getJobId().getJobId().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobOrder().getJobId()))
					{
					  label = new Label(++col, k, mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseConsolidatedScore()+"/"+mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseMaxScore()); 
						excelSheet.addCell(label);
					}
				     else{	
				    	 label = new Label(++col, k, "N/A"); 
						 excelSheet.addCell(label);
						 }
				}
				else
				{
					label = new Label(++col, k, "N/A"); 
					excelSheet.addCell(label);
				}
			 if(jft.getLastActivityDate()!=null)
				{
				 label = new Label(++col, k, ""+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getLastActivityDate())); 
				 excelSheet.addCell(label);
				}
			 else if(mapteacherStatusHistoryACtivity.get(jft.getTeacherId().getTeacherId()+"#"+jft.getJobId().getJobId()+"#"+jft.getStatus().getStatusId())!=null)
			 {
				 label = new Label(++col, k, ""+Utility.convertDateAndTimeToUSformatOnlyDate( mapteacherStatusHistoryACtivity.get(jft.getTeacherId().getTeacherId()+"#"+jft.getJobId().getJobId()+"#"+jft.getStatus().getStatusId()).getCreatedDateTime())); 
				 excelSheet.addCell(label);
							
			 } 
				 else
				 {
					 label = new Label(++col, k, "N/A"); 
					 excelSheet.addCell(label);
				 }	    
			 
			 
			  ++k;
			 }
			}
	

		workbook.write();
		workbook.close();

		}catch(Exception e){}
		
	 return fileName;
 }
	

	public String getCertificationList(int districtId)
	{
		StringBuffer getListCert=new StringBuffer(); 
		try
		{
			DistrictMaster districtMaster=null;
			districtMaster=districtMasterDAO.findById(districtId,false, false);
				 if(districtMaster!=null)
				 {
					 List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
					 getListCert.append("<option value='0'>All </option>");	
					 for(JobCategoryMaster jobCat:jobCategoryMasterlst)
						{
							getListCert.append("<option value='"+jobCat.getJobCategoryId()+"'>"+jobCat.getJobCategoryName()+"</option>");
						}
				 }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return getListCert.toString();
	}
	
	

	public String getsecStatusList(int districtId)
	{
		StringBuffer getListSS=new StringBuffer(); 
		try
		{
			DistrictMaster districtMaster=null;
			districtMaster=districtMasterDAO.findById(districtId,false, false);
			
			List<SecondaryStatus> lstSecondaryStatus = secondaryStatusDAO.findSecondryStatusByDistrict(districtMaster);
			Set<String> setStatus =new TreeSet<String>();
			if(lstSecondaryStatus.size()>0){
				getListSS.append("<option value='0'>All </option>");
				for(SecondaryStatus ss: lstSecondaryStatus)
				{
					setStatus.add(ss.getSecondaryStatusName());
				}
				
				for(String str: setStatus)
				{
					getListSS.append("<option value='"+str+"'>"+str+"</option>");
				
				}
				getListSS.append("<option value='Hired'>Hired</option>");
				getListSS.append("<option value='Incomplete'>Incomplete</option>");
				getListSS.append("<option value='Rejected'>Rejected</option>");
				getListSS.append("<option value='Timed Out'>Timed Out</option>");
				getListSS.append("<option value='Withdrew'>Withdrew</option>");
				
			}
				
		}catch (Exception e) {
			e.printStackTrace();
		}
		return getListSS.toString();
	}
public String downloadCandidateJobStatusReportPDF(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
		schoolId,String subjectId,String certifications,String jobOrderId,String status,String noOfRow,
		String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo, String firstName,String lastName,
		String emailAddress, String stateId,String certType,String jobCategoryIds,String statusId,
		String sfromDate, String stoDate, String endfromDate, String endtoDate, String appsfromDate, String appstoDate, 
		boolean pdateFlag,String normScoreSelectVal,String normScoreVal, int intenalchk, String hiredfromDate, String hiredtoDate,String jobStatus,int hiddenjob
          ){
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}


	try
	{	
		String fontPath = request.getRealPath("/");
		BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			userMaster = (UserMaster)session.getAttribute("userMaster");

		int userId = userMaster.getUserId();

		
		String time = String.valueOf(System.currentTimeMillis()).substring(6);
		String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
		String fileName =time+"Report.pdf";

		File file = new File(basePath);
		if(!file.exists())
			file.mkdirs();

		Utility.deleteAllFileFromDir(basePath);
		System.out.println("user/"+userId+"/"+fileName);

		generateCandidatePDReport(resultFlag,JobOrderType,districtOrSchoolId,
				schoolId, subjectId, certifications, jobOrderId, status, noOfRow,
				 pageNo, sortOrder, sortOrderType,  disJobReqNo,  firstName, lastName,
				 emailAddress,  stateId, certType, jobCategoryIds, statusId,sfromDate,stoDate,endfromDate,endtoDate,appsfromDate,appstoDate, 
					pdateFlag,normScoreSelectVal,normScoreVal, intenalchk,hiredfromDate, hiredtoDate,basePath+"/"+fileName,context.getServletContext().getRealPath("/"),jobStatus,hiddenjob);
		return "user/"+userId+"/"+fileName;
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return "ABV";
}


public boolean generateCandidatePDReport(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
		schoolId,String subjectId,String certifications,String jobOrderId,String status,String noOfRow,
		String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo, String firstName,String lastName,
		String emailAddress, String stateId,String certType,String jobCategoryIds,String statusId,
		String sfromDate, String stoDate, String endfromDate, String endtoDate, String appsfromDate, String appstoDate, 
		boolean pdateFlag,String normScoreSelectVal,String normScoreVal, int intenalchk,String hiredfromDate, String hiredtoDate,
		String path,String realPath,String jobStatus,int hiddenjob){
	int cellSize = 0;
	Document document=null;
	FileOutputStream fos = null;
	PdfWriter writer = null;
	Paragraph footerpara = null;
	HeaderFooter headerFooter = null;
	
		
		Font font8 = null;
		Font font8Green = null;
		Font font8bold = null;
		Font font9 = null;
		Font font9bold = null;
		Font font10 = null;
		Font font10_10 = null;
		Font font10bold = null;
		Font font11 = null;
		Font font11bold = null;
		Font font20bold = null;
		Font font11b   =null;
		Color bluecolor =null;
		Font font8bold_new = null;
		
		System.out.println(sortOrderType+" ::@@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			SubjectMaster subjectMaster	=	null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="jobTitle";
				String sortOrderNoField="jobTitle";
				
				boolean deafultFlag=false;
				
				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				
				List<JobForTeacher>	jobForTeacherLists    = new ArrayList<JobForTeacher>();
				List<JobForTeacher>	listByFirstName       = new ArrayList<JobForTeacher>();
				List<JobForTeacher>	jobJobId              = new ArrayList<JobForTeacher>();
				List<JobForTeacher> listByJobCategory     = new ArrayList<JobForTeacher>();
				
				boolean checkflag = false;
				
				if(entityID==2){
					if(schoolId==0){
						jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatusAll(districtMaster);
						if(jobForTeacherLists.size()>0)
						checkflag = true;
					}else{
						List<JobOrder> lstJobOrders =null;
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.allJobOrderBySchool(sclMaster);
						if(lstJobOrders.size()>0){
							jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOBStatus(lstJobOrders);
							checkflag = true;
						}
					}
					
				}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0){
						if(schoolId==0){
							 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					        jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatusAll(districtMaster);
					        if(jobForTeacherLists.size()>0)
					        	checkflag = true;
						}
						else{
							List<JobOrder> lstJobOrders =null;
							SchoolMaster  sclMaster=null;
							sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
							lstJobOrders = schoolInJobOrderDAO.allJobOrderBySchool(sclMaster);
							if(lstJobOrders.size()>0){
								jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOBStatus(lstJobOrders);
								checkflag = true;
							}
						}
					}
					else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					}
				}else if(entityID==3){
					if(schoolId==0){
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
				        jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatusAll(districtMaster);
				        if(jobForTeacherLists.size()>0)
				        	checkflag = true;
					}else{
						List<JobOrder> lstJobOrders =null;
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.allJobOrderBySchool(sclMaster);
						if(lstJobOrders.size()>0){
							jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOBStatus(lstJobOrders);
							checkflag = true;
						}
					}
				}
			
	//filter start
				//Jobstatus filter *******************************************************************************************************
					if (jobStatus.equals("A") || jobStatus.equals("I")) {
						List<Integer> list = new ArrayList<Integer>();
						for (JobForTeacher jobForTeacher : jobForTeacherLists) {
							list.add(jobForTeacher.getJobId().getJobId());
							// System.out.println("??????????????????       "+jobForTeacher.getJobId().getJobId());
						}
						List<JobOrder> jobstatuslist = new ArrayList<JobOrder>();
						boolean jobstatusflag = false;
						if (jobStatus.equals("A")) {
							districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
							Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
							Criterion criterion = Restrictions.eq("status", jobStatus);
							Criterion criterion2 = Restrictions.in("jobId", list);
							jobstatuslist = jobOrderDAO.findByCriteria(criterion,criterion1, criterion2);
							System.out.println("jobstatuslist---------------"+ jobstatuslist.size() + "           "	+ list.size());
							if (jobstatuslist.size() > 0) {
								jobstatusflag = true;
							}
						}

						if (jobStatus.equals("I")) {
							districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
							Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
							Criterion criterion = Restrictions.eq("status", jobStatus);
							Criterion criterion2 = Restrictions.in("jobId", list);
							jobstatuslist = jobOrderDAO.findByCriteria(criterion,criterion1, criterion2);
							System.out.println("jobstatuslist---------------"+ jobstatuslist.size() + "           "	+ list.size());
							if (jobstatuslist.size() > 0) {
								jobstatusflag = true;
							}
						}

						List<JobForTeacher> jobForTeacher1 = new ArrayList<JobForTeacher>();
						if (jobstatusflag) {
							for (JobOrder jobOrder : jobstatuslist) {
								for (JobForTeacher jobForTeacherLists1 : jobForTeacherLists) {
									if (jobOrder.getJobId().equals(
											jobForTeacherLists1.getJobId().getJobId())) {
										jobForTeacher1.add(jobForTeacherLists1);
										//break;
									}
								}
							}
							jobForTeacherLists = jobForTeacher1;
						}
					}
					
					//Hidden Job filter start**************************************************************************************************
					Boolean hdjob=false;
					if(hiddenjob==1)
					{
						hdjob=true;
					}else if(hiddenjob==0)
					{
						hdjob=false;
					}
					//System.out.println("hiddenjob------------"+hdjob);
						List<Integer> hiddenJobTempList = new ArrayList<Integer>();
						for (JobForTeacher jobForTeacher : jobForTeacherLists) {
							hiddenJobTempList.add(jobForTeacher.getJobId().getJobId());
							// System.out.println("jobForTeacherLists  JobID   "+jobForTeacher.getJobId().getJobId());
						}
						List<JobOrder> hiddenJobList = new ArrayList<JobOrder>();
						boolean hiddenJobflag = false;
						if (hiddenjob!=2) {
							districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
							Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
							Criterion criterion = Restrictions.eq("hiddenJob",hdjob);
							Criterion criterion2 = Restrictions.in("jobId",	hiddenJobTempList);
							hiddenJobList = jobOrderDAO.findByCriteria(criterion,criterion1, criterion2);
							//System.out.println("hiddenJobTempList---------------"+ hiddenJobTempList.size() + "           "	+ hiddenJobList.size());
							if (hiddenJobList.size() > 0) {
								hiddenJobflag = true;
							}else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							}
						}
						List<JobForTeacher> jobForTeacherHiddenJob = new ArrayList<JobForTeacher>();
						if (hiddenJobflag) {
							for (JobOrder jobOrder : hiddenJobList) {
								for (JobForTeacher jobForTeacherLists1 : jobForTeacherLists) {
									if (jobOrder.getJobId().equals(	jobForTeacherLists1.getJobId().getJobId())) {
										jobForTeacherHiddenJob.add(jobForTeacherLists1);
										//break;
									}
								}
							}
							jobForTeacherLists = jobForTeacherHiddenJob;
						}
				// Hidden Filter End			
				//******* first Name , Last Name and Email filter 
				
				List<TeacherDetail> basicSearchList = new ArrayList<TeacherDetail>();
			
				List<Criterion> basicSearch = new ArrayList<Criterion>();
				boolean basicFlag = false;
				
				if(firstName!=null && !firstName.equals(""))
				{
					Criterion criterion = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
					
				if(lastName!=null && !lastName.equals(""))
				{
					Criterion criterion = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
				
				if(emailAddress!=null && !emailAddress.equals(""))
				{
					Criterion criterion = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
				if(basicFlag)
				{
					basicSearchList  = teacherDetailDAO.getTeachersByNameAndEmail(basicSearch);
					if(basicSearchList!=null && basicSearchList.size()>0)
					{
						listByFirstName = jobForTeacherDAO.findTeacherByTeacherDeatailAndJobStatus(basicSearchList);
						 if(listByFirstName!=null && listByFirstName.size()>0){
							 if(checkflag){
			   						jobForTeacherLists.retainAll(listByFirstName);
			   					}else{
			   						jobForTeacherLists.addAll(listByFirstName);
			   						checkflag=true;
			   					}
						 }
						 else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							 }
					}
					else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					 }
				}
				
	   		 //****** job ID filter
				//JobOrder joborder =null;
				if(!jobOrderId.equals(""))
				{
					List<Integer> jobIDS= new ArrayList<Integer>();
					String strjobIds[]=jobOrderId.split(",");
					
					if(strjobIds.length>0)
				     for(String str : strjobIds){
				    	 if(!str.equals(""))
				    	 jobIDS.add(Integer.parseInt(str));
				     }
						jobJobId = jobForTeacherDAO.findbyJobOrderAndStatus(jobIDS);
					
						if (jobJobId.size()>0){ 
							if(checkflag)
							{ 
								jobForTeacherLists.retainAll(jobJobId);
							}
							else{
								jobForTeacherLists.addAll(jobJobId);
								checkflag=true;
							}
							
						}else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						 }
				  }
				
			// Licensure Name/Licensure State filter
				List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
				List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();
                List<JobForTeacher> listBycertification = new ArrayList<JobForTeacher>();
				StateMaster stateMaster=null;
				List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
				
				
				if(!certType.equals("0") && !certType.equals("")||!stateId.equals("0")){
					if(!stateId.equals("0")){
						stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
					}
					certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
					//System.out.println("certificateTypeMasterList:: "+certificateTypeMasterList.size());
					if(certificateTypeMasterList.size()>0)
						certJobOrderList=jobCertificationDAO.findCertificationByJobWithState(certificateTypeMasterList);
					System.out.println("certJobOrderList:: "+certJobOrderList.size());
					if(certJobOrderList.size()>0){
						for(JobOrder job: certJobOrderList){
							jobOrderIds.add(job);
						}
					}
					if(jobOrderIds.size()>0){
						listBycertification = jobForTeacherDAO.getJobForTeacherByJOBStatus(jobOrderIds);
						if(listBycertification.size()>0){
							if(checkflag){
		   						jobForTeacherLists.retainAll(listBycertification);
		   					}else{
		   						jobForTeacherLists.addAll(listBycertification);
		   						checkflag=true;
		   					}
						}else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						}
					}else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					}
				
				}
			
			 //filter job category
				if(!jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
				{
					List<Integer> jobcatIds = new ArrayList<Integer>();
					List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
					String jobCategoryIdStr[] =jobCategoryIds.split(",");
					
					if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
					{
					 for(String str : jobCategoryIdStr){
				    	 if(!str.equals(""))
				    		 jobcatIds.add(Integer.parseInt(str));
					 } 
			    	 if(jobcatIds.size()>0){
			    		 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
			    	 }
			    	 if(listjJobOrders.size()>0){
			    		 listByJobCategory = jobForTeacherDAO.getJobForTeacherByJOBStatus(listjJobOrders);
			    		 if(listByJobCategory.size()>0){
								if(checkflag){
			   						jobForTeacherLists.retainAll(listByJobCategory);
			   					}else{
			   						jobForTeacherLists.addAll(listByJobCategory);
			   						checkflag=true;
			   					}
							}else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							}
			         }
			    	 else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						 }
					}
				}
				// filter of job status 
				
				List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
				List<JobForTeacher>  listByJobStatus = new ArrayList<JobForTeacher>();
				
				 if(!statusId.equals("") && !statusId.equals("0")){
					 System.out.println(" Filter StatusId Start ::  "+statusId);
					 String statusNames[] = statusId.split("#@");
					
				      if(!ArrayUtils.contains(statusNames,"0"))
				      {	 
						 DistrictMaster districtMaster2 = districtMasterDAO.findById(districtOrSchoolId, false, false);
						 lstSecondaryStatus = secondaryStatusDAO.findByMultipleStatusName(districtMaster2, statusNames);
				
						  List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
						  Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
						  for (StatusMaster statusMaster1 : statusMasterList) {
							  mapStatus.put(statusMaster1.getStatusShortName(),statusMaster1);
						  }
						  StatusMaster stMaster =null;
						  List <StatusMaster> statusMaster1st=new ArrayList<StatusMaster>();
						  
						  if(ArrayUtils.contains(statusNames,"Available")){
								  stMaster = mapStatus.get("comp");
								  statusMaster1st.add(stMaster);
						  }
						  if(ArrayUtils.contains(statusNames,"Rejected")){
							    stMaster = mapStatus.get("rem");
							    statusMaster1st.add(stMaster);
					     }
						  if(ArrayUtils.contains(statusNames,"Timed Out")){
							    stMaster = mapStatus.get("vlt");
							    statusMaster1st.add(stMaster);
					     }
						 if(ArrayUtils.contains(statusNames,"Withdrew")){
							    stMaster = mapStatus.get("widrw");
							    statusMaster1st.add(stMaster);
					     }
						 if(ArrayUtils.contains(statusNames,"Incomplete")){
							    stMaster = mapStatus.get("icomp");
							    statusMaster1st.add(stMaster);
					     }
						 if(ArrayUtils.contains(statusNames,"Hired")){
							    stMaster = mapStatus.get("hird");
							    statusMaster1st.add(stMaster);
					     }   
						 
						 for(SecondaryStatus ss: lstSecondaryStatus)
						  {
							 if(ss.getStatusMaster()!=null)
								 statusMaster1st.add(ss.getStatusMaster()); 
						  }
					    /* if(statusMaster1st.size()==0){ 
								  listByJobStatus = jobForTeacherDAO.findBySecondStatus(lstSecondaryStatus);
						  }else{*/
					     listByJobStatus = jobForTeacherDAO.findAvailableCandidtes(districtMaster2,statusMaster1st,lstSecondaryStatus);
						 // }
						  if(listByJobStatus.size()>0){
								if(checkflag){
			   						jobForTeacherLists.retainAll(listByJobStatus);
			   					}else{
			   						jobForTeacherLists.addAll(listByJobStatus);
			   						checkflag=true;
			   					}
							}else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							}
			         }
						  
				 }
			  // Advanced filters + Date Filter Start  Norm Score +A
				
				 List<JobForTeacher> lstDateJFT =new ArrayList<JobForTeacher>();
				 if(pdateFlag){
					 List<TeacherDetail> normTeacherList = new ArrayList<TeacherDetail>();
					 if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") && !normScoreSelectVal.equals("6"))
					 {
						normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore(normScoreVal,normScoreSelectVal);
					 }
					 
					if(!normScoreSelectVal.equals("") && normScoreSelectVal.equals("6"))
					{
						 normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore("0","5");
					}
					 lstDateJFT = jobForTeacherDAO.findJobForTeacherbyInputDateFilterwithDistrict(districtMaster,sfromDate,stoDate,endfromDate,endtoDate,appsfromDate,appstoDate,normTeacherList ,normScoreSelectVal,intenalchk);
					 if(lstDateJFT!=null && lstDateJFT.size()>0)
					 {
						 if(checkflag){
		   						jobForTeacherLists.retainAll(lstDateJFT);
		   					}else{
		   						jobForTeacherLists.addAll(lstDateJFT);
		   						checkflag=true;
		   					}
					 }else{
						 jobForTeacherLists = new ArrayList<JobForTeacher>();
					 }
				 }
				
				//filter of Hiring Date
				 List<JobForTeacher> lstTeacherStatusHistoryForJobs = new ArrayList<JobForTeacher>();
				 if(!hiredfromDate.equals("") || !hiredtoDate.equals("")){
					 System.out.println("filter of Hiring Date ");
					 List<JobForTeacher> filterHCForTeachers =new ArrayList<JobForTeacher>();
					 List<TeacherDetail> lstHCDetails =  new ArrayList<TeacherDetail>();
					 Map<String,JobForTeacher> mapteacherStatusHistory = new HashMap<String, JobForTeacher>();
						 lstTeacherStatusHistoryForJobs = jobForTeacherDAO.findByHIredDAte(districtMaster, hiredfromDate, hiredtoDate);
					System.out.println("lstTeacherStatusHistoryForJobs :: "+lstTeacherStatusHistoryForJobs.size());
					
						 if(lstTeacherStatusHistoryForJobs!=null && lstTeacherStatusHistoryForJobs.size()>0){
						 for(JobForTeacher hc : lstTeacherStatusHistoryForJobs){
							 lstHCDetails.add(hc.getTeacherId());
							 mapteacherStatusHistory.put(hc.getJobId().getJobId()+"#"+hc.getTeacherId().getTeacherId(), hc);
						 }
					       List<JobForTeacher> lstHCForTeachers = jobForTeacherDAO.findTeacherByTeacherDeatailAndJobStatus(lstHCDetails);
					       for(JobForTeacher jft: lstHCForTeachers){
					    	   if(mapteacherStatusHistory.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId())!=null){
					    		   filterHCForTeachers.add(jft);
					    	   }
					       }
					       
					       if(checkflag){
	   						    jobForTeacherLists.retainAll(filterHCForTeachers);
			   					}
					       else{
		   						jobForTeacherLists.addAll(filterHCForTeachers);
		   						checkflag=true;
			   				}
					 } 
					 else{
						 jobForTeacherLists = new ArrayList<JobForTeacher>();
					 }
				 }
				 
				 
				 System.out.println("Final ************** jft ::::::::  "+jobForTeacherLists.size());	
				
				
			     ArrayList<Integer> teacherIds = new ArrayList<Integer>();
			     ArrayList<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
			
				 List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
				 List<TeacherNormScore> listTeacherNormScores =null;
				 Map<Integer,TeacherNormScore> mapNormScores = new HashMap<Integer, TeacherNormScore>();
				 Map<String,TeacherAchievementScore> mapTeacherAchievementScore = new HashMap<String, TeacherAchievementScore>();
			     Map<String,JobWiseConsolidatedTeacherScore> mapJWCTeacherScore = new HashMap<String, JobWiseConsolidatedTeacherScore>();
				
			  // map for candidtates count on a job   
			     Map<Integer,List<JobForTeacher>> maptotalCandidates      = new HashMap<Integer, List<JobForTeacher>>();
			     Map<Integer,Integer> maptotalAvalCandidates  = new HashMap<Integer, Integer>();
			     List<DistrictMaster> districtMasterList = new ArrayList<DistrictMaster>();
			     List<JobOrder> lstHUJobOrders = new ArrayList<JobOrder>();
			     List<TeacherDetail> lstHUTeacherDetails = new ArrayList<TeacherDetail>();
			     Map<String,JobForTeacher> mapHiredDate =new HashMap<String, JobForTeacher>(); 
			   
			     List<JobCategoryMaster> categoryMasters= new ArrayList<JobCategoryMaster>();
			     
			     List<JobOrder> listJoboOrders = new ArrayList<JobOrder>();
			      
					
			     
			     String jobIdStr = ""; 
			     if(jobForTeacherLists.size()>0)
			     for(JobForTeacher jft:jobForTeacherLists) 
				 {
			    	 listTeacherDetails.add(jft.getTeacherId());
					 teacherIds.add(jft.getTeacherId().getTeacherId());
					 listJoboOrders.add(jft.getJobId());
					 categoryMasters.add(jft.getJobId().getJobCategoryMaster());
			    	 lstHUJobOrders.add(jft.getJobId());
			    	 lstHUTeacherDetails.add(jft.getTeacherId());
			    	 districtMasterList.add(jft.getJobId().getDistrictMaster());
			    	 if(jobIdStr.equals("")){
			    		 jobIdStr=""+jft.getJobId().getJobId();
			    	 }else{
			    		 jobIdStr=jobIdStr+","+jft.getJobId().getJobId();
			    	 }
					//////////////////////////////////////////////////
			    	 int jobId=jft.getJobId().getJobId();
			    	 List<JobForTeacher> tAlist = maptotalCandidates.get(jobId);
						if(tAlist==null){
							List<JobForTeacher> jobs = new ArrayList<JobForTeacher>();
							jobs.add(jft);
							maptotalCandidates.put(jobId, jobs);
						}else{
							tAlist.add(jft);
							maptotalCandidates.put(jobId, tAlist);
						}
			    	 ////////////////////////////////////////////////
						
						
				 }
			     
			 	for (Map.Entry<Integer,List<JobForTeacher>> entry : maptotalCandidates.entrySet()) {
						 List<JobForTeacher> jfList=entry.getValue();
						 int count=0;
						 for (JobForTeacher jobForTeacher : jfList) {
							 if(!jobForTeacher.getStatus().getStatusShortName().equals("icomp") && !jobForTeacher.getStatus().getStatusShortName().equals("vlt") && !jobForTeacher.getStatus().getStatusShortName().equals("widrw") && !jobForTeacher.getStatus().getStatusShortName().equals("hide")){
								 count++;
							 }
						}
						// System.out.println("count::::::"+count);
						 maptotalAvalCandidates.put(entry.getKey(),count); 
			   }
			    
			  //*********hired calculation**********************
			     List<JobForTeacher>  lstStatusHistoryForJobs = new  ArrayList<JobForTeacher>();
			     if(jobForTeacherLists!=null && jobForTeacherLists.size()>0){
			    	 lstStatusHistoryForJobs = jobForTeacherDAO.findByTeacherIdAndJObIds(districtMaster,lstHUTeacherDetails,lstHUJobOrders);
			         if(lstStatusHistoryForJobs!=null && lstStatusHistoryForJobs.size()>0){
			        	 for(JobForTeacher ht : lstStatusHistoryForJobs){
			        		 mapHiredDate.put(ht.getJobId().getJobId()+"#"+ht.getTeacherId().getTeacherId(),ht);
			        	 }
			         }
			     }
			     
			    
			    
			    // sorting logic start 
			     
			     if(sortOrder.equals("")){
			    	 for(JobForTeacher jf:jobForTeacherLists) 
				    	 jf.setJobTitle(jf.getJobId().getJobTitle());
					
					  Collections.sort(jobForTeacherLists, new JobTitleCompratorASC());
			     }
			     
			     
			     if(sortOrder.equals("lastName")){    
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setLastName(jf.getTeacherId().getLastName());
						
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new LastNameCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new LastNameCompratorDESC());
			     }  
			     if(sortOrder.equals("jobTitle")){    
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setJobTitle(jf.getJobId().getJobTitle());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new JobTitleCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new JobTitleCompratorDESC());
			     } 
			    
			     if(sortOrder.equals("totalCandidate")){    
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setTotalCandidate(maptotalCandidates.get(jf.getJobId().getJobId()).size());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new TotalCandidateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new TotalCandidateCompratorDESC());
			     } 
			 
			     if(sortOrder.equals("avlCandidate")){    
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setAvlCandidate(maptotalAvalCandidates.get(jf.getJobId().getJobId()));
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new AvlCandidateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new AvlCandidateCompratorDESC());
			     } 
			 
			    
			         
			    Map<String,SecondaryStatus> mapSecStatuss = new HashMap<String, SecondaryStatus>();
			    //List<SecondaryStatus> lstSecondaryStatuss =	secondaryStatusDAO.findSecondaryStatusWithDistrictList(districtMasterList);
			   
					List<SecondaryStatus> lstSecondaryStatuss =	secondaryStatusDAO.findSecondaryStatusByJobCategoryLists(districtMasterList,categoryMasters);
			    if(lstSecondaryStatuss!=null && lstSecondaryStatuss.size()>0)
					for(SecondaryStatus sec : lstSecondaryStatuss ){
						if(sec.getStatusMaster()!=null){
							mapSecStatuss.put(sec.getStatusMaster().getStatusId()+"##0"+"#"+sec.getJobCategoryMaster().getJobCategoryId(),sec);
						}else{
							mapSecStatuss.put("##0"+sec.getSecondaryStatusId()+"#"+sec.getJobCategoryMaster().getJobCategoryId(),sec);
						}
					}
			   	  

			   
		//Candidate Status Sorting 	
			if(sortOrder.equals("candidateStatus"))    
			 for(JobForTeacher jft: jobForTeacherLists){
				 StatusMaster statusObj =jft.getStatusMaster();
					
					String mapKey="";
					try {
						if(jft.getStatusMaster()!=null){
							mapKey=jft.getStatusMaster().getStatusId()+"##0"+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
							if(mapSecStatuss.get(mapKey)!=null){
								statusObj=mapSecStatuss.get(mapKey).getStatusMaster();
							}
						}else{
							mapKey="##0"+jft.getSecondaryStatus().getSecondaryStatusId()+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					
					try {
						if(statusObj!=null)
						{
							if(statusObj.getStatusShortName().equalsIgnoreCase("comp")){
								jft.setAssessmentStatus("Available"); 
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("rem")){
								jft.setAssessmentStatus(statusObj.getStatus());
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("icomp")){
								jft.setAssessmentStatus(statusObj.getStatus());
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("vlt")){
								jft.setAssessmentStatus("Time Out"); 
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("scomp")){
								jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("ecomp")){
								jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("vcomp")){
								jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
							}else if(statusObj.getStatusShortName().equalsIgnoreCase("dcln"))
								jft.setAssessmentStatus("Declined"); 
							else if(statusObj.getStatusShortName().equalsIgnoreCase("widrw"))
								jft.setAssessmentStatus("Withdrew"); 
							else if(statusObj.getStatusShortName().equalsIgnoreCase("hird"))
								jft.setAssessmentStatus("Hired"); 
						}else{
							if(jft.getSecondaryStatus()!=null)
							{
								   if(mapSecStatuss.get(mapKey)!=null)
									jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
									else
									jft.setAssessmentStatus(jft.getSecondaryStatus().getSecondaryStatusName());
							}
								 
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			 }
		    if(sortOrder.equals("candidateStatus") && sortOrderType.equals("0")){ 
			 Collections.sort(jobForTeacherLists,JobForTeacher.jobForTeacherComparatorAssessmentStatus ); 
			 }else if(sortOrder.equals("candidateStatus") && sortOrderType.equals("1")){ 
			 Collections.sort(jobForTeacherLists,JobForTeacher.jobForTeacherComparatorAssessmentStatusDesc);
			 }
    
			     
			if(jobForTeacherLists.size()>0)
			{	
				/*for Teacher norm score*/
				if(listTeacherDetails.size()>0){
				 listTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(listTeacherDetails);
				}
				
				for(TeacherNormScore tns:listTeacherNormScores)
			 	 {
				  mapNormScores.put(tns.getTeacherDetail().getTeacherId(),tns);
			 	 }
				
				// Sorting By normScore 
				//Sorting By A Score
			 	if(sortOrder.equals("normScore")){
						for(JobForTeacher jft : jobForTeacherLists){
							if(mapNormScores.get(jft.getTeacherId().getTeacherId()) != null){
								  jft.setNormScore(Double.valueOf(mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+""));
							}else{
								jft.setNormScore(-0.0);
							}
						}
			 	
			 	 if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorNormScore);
					 else 
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorNormScoreDesc);
			 	
			 	}
					
				//for A Score && L/R Schore
			    List<TeacherAchievementScore> listTeacherAchievementScores = teacherAchievementScoreDAO.getAchievementScoreListByTDList(listTeacherDetails);
			 	for(TeacherAchievementScore tas:listTeacherAchievementScores)
			 	 {
			 		mapTeacherAchievementScore.put(tas.getTeacherDetail().getTeacherId().toString(),tas);
			 	 }
				 
			 	//Sorting By A Score
			 	if(sortOrder.equals("aScore")){
						for(JobForTeacher jft : jobForTeacherLists){
							if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()) != null){
								if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore().equals("") && mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()!=null)
								{
								  jft.setaScore(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore());
								}else{
									jft.setaScore(-1);
								}
							}else{
								jft.setaScore(-1);
							}
						}
			 	
			 	 if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorAScore);
					 else 
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorAScoreDesc);
			 	
			 	}
			 	
			 	//Sorting By L/R Score
			 	  if(sortOrder.equals("lRScore")){
						for(JobForTeacher jft : jobForTeacherLists){
							if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()) != null){
								if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore().equals("") &&  mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()!=null )
								{ 
								  jft.setlRScore(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore());
								}else{
									jft.setlRScore(-1);
								}
							}else{
								jft.setlRScore(-1);
							}
						}
			 	
			 	    if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorLRScore);
					 else 
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorLRScoreDesc);
			 	
			 	}
			 	
				/* for fit score */
				List<JobWiseConsolidatedTeacherScore> listJobWiseCTScore = jobWiseConsolidatedTeacherScoreDAO.findbyTeacherDetails(listTeacherDetails);
			 	for(JobWiseConsolidatedTeacherScore jwct:listJobWiseCTScore)
			 	 {
			 		mapJWCTeacherScore.put(jwct.getTeacherDetail().getTeacherId().toString(),jwct);
			 	 }
				
			 	//Sorting By Fit Score fitScore
			 	 if(sortOrder.equals("fitScore")){
						for(JobForTeacher jft : jobForTeacherLists){
							if(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()) != null){
								if(jft.getTeacherId().getTeacherId().toString().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()) && jft.getJobId().getJobId().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobOrder().getJobId()))
								{ 
								  jft.setFitScore(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseConsolidatedScore());
								}else{
									jft.setFitScore(-1.0);
								}
							}else{
								jft.setFitScore(-1.0);
							}
						}
			 	
			 	    if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorFitScore);
					 else 
					  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorFitScoreDesc);
			 	
			 	}
			 	 if(sortOrder.equals("distName")){  
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setDistName(jf.getJobId().getDistrictMaster().getDistrictName());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new DistrictNameCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new DistrictNameCompratorDESC());
			     } 
			 	 
			 	if(sortOrder.equals("postDate")){  
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setPostDate(jf.getJobId().getJobStartDate());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new PostingDateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new PostingDateCompratorDESC());
			     }
			 	
			 	if(sortOrder.equals("endDate")){  
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setEndDate(jf.getJobId().getJobEndDate());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new EndDateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new EndDateCompratorDESC());
			     } 
			 	 
			 	if(sortOrder.equals("appsDate")){  
				     for(JobForTeacher jf:jobForTeacherLists) 
					 {
				    	 jf.setAppsDate(jf.getCreatedDateTime());
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new AppsDateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new AppsDateCompratorDESC());
			     } 
			 	if(sortOrder.equals("isAffilated"))
			 	{  
			 		for(JobForTeacher jf:jobForTeacherLists) 
					 {
			 			if(jf.getIsAffilated()==null || jf.getIsAffilated()==0)
			 			 jf.setIsAffilated(0);
			 			else
			 			 jf.setIsAffilated(1);
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new InternalCandidateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new InternalCandidateCompratorDESC());
				    
				    
			     } 
			 	if(sortOrder.equals("hiredDate"))
			 	{  
			 		for(JobForTeacher jf:jobForTeacherLists) 
					 {
			 			if(mapHiredDate.get(jf.getJobId().getJobId()+"#"+jf.getTeacherId().getTeacherId())!=null){
			 				jf.setHiredDate(mapHiredDate.get(jf.getJobId().getJobId()+"#"+jf.getTeacherId().getTeacherId()).getCreatedDateTime());
			 			}
			 			else{
			 				jf.setHiredDate(new Date(0));
			 			}
					 }
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new HiredDateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new HiredDateCompratorDESC());
				    
				    
			     } 
			 	
			 	if(sortOrder.equals("activityDate")){ 
			 		
				     if(sortOrderType.equals("0"))
					  Collections.sort(jobForTeacherLists, new LastActivityDateCompratorASC());
					 else 
					  Collections.sort(jobForTeacherLists, new LastActivityDateCompratorDESC());
			     } 
			 	
			}	
		  System.out.println("xxxxxxxxxxxxxjobForTeacherLists :: "+jobForTeacherLists.size());	
	//#########################################################################################################
		  String fontPath = realPath;
			 	try {
					
					BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
					BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
					font8 = new Font(tahoma, 8);

					font8Green = new Font(tahoma, 8);
					font8Green.setColor(Color.BLUE);
					font8bold = new Font(tahoma, 8, Font.NORMAL);
					
					font8bold_new = new Font(tahoma, 7, Font.NORMAL);
					

					font9 = new Font(tahoma, 9);
					font9bold = new Font(tahoma, 9, Font.BOLD);
					font10 = new Font(tahoma, 10);
					
					font10_10 = new Font(tahoma, 8);
					font10_10.setColor(Color.white);
					//font10.setColor(Color.white);
					font10bold = new Font(tahoma, 10, Font.BOLD);
					//font10bold.setColor(Color.white);
					font11 = new Font(tahoma, 11);
					font11bold = new Font(tahoma, 11,Font.BOLD);
					
					
					 bluecolor =  new Color(0,122,180); 
					
					font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
					//font20bold.setColor(Color.BLUE);
					font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


				} 
				catch (DocumentException e1) 
				{
					e1.printStackTrace();
				} 
				catch (IOException e1) 
				{
					e1.printStackTrace();
				}
				
				document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
				document.addAuthor("TeacherMatch");
				document.addCreator("TeacherMatch Inc.");
				document.addSubject("Candidate Job Status Report ");
				document.addCreationDate();
				document.addTitle("Candidate Job Status Report ");

				fos = new FileOutputStream(path);
				PdfWriter.getInstance(document, fos);
				
				document.open();
				
				PdfPTable mainTable = new PdfPTable(1);
				mainTable.setWidthPercentage(100);

				Paragraph [] para = null;
				PdfPCell [] cell = null;


				para = new Paragraph[3];
				cell = new PdfPCell[3];
				
				//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
				Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
				logo.scalePercent(75);
				
				document.add(new Phrase("\n"));
				//para[0] = new Paragraph(" ",font20bold);
				cell[0]= new PdfPCell(logo);
				cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[0].setBorder(0);
				mainTable.addCell(cell[0]);
				
				

				//para[1] = new Paragraph(" ",font20bold);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				mainTable.addCell(cell[1]);

				document.add(new Phrase("\n"));
				/*document.add(new Phrase("\n"));
				document.add(new Phrase("\n"));*/
				
				

				document.add(mainTable);
				
				document.add(new Phrase("\n"));
				//document.add(new Phrase("\n"));
				

				float[] tblwidthz={.15f};
				
				mainTable = new PdfPTable(tblwidthz);
				mainTable.setWidthPercentage(100);
				para = new Paragraph[1];
				cell = new PdfPCell[1];

				
				para[0] = new Paragraph("Candidate Job Status Report ",font20bold);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[0].setBorder(0);
				mainTable.addCell(cell[0]);
				document.add(mainTable);

				document.add(new Phrase("\n"));
				
				
		        float[] tblwidths={.15f,.20f};
				
				mainTable = new PdfPTable(tblwidths);
				mainTable.setWidthPercentage(100);
				para = new Paragraph[2];
				cell = new PdfPCell[2];

				String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
				
				para[0] = new Paragraph("Created By: "+name1,font10);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].setBorder(0);
				mainTable.addCell(cell[0]);
				
				para[1] = new Paragraph(""+"Created on: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell[1].setBorder(0);
				mainTable.addCell(cell[1]);
				
				document.add(mainTable);
							
				//document.add(new Phrase("\n"));
				/*document.add(new Phrase("\n"));
				document.add(new Phrase("\n")); */	
				document.add(Chunk.NEWLINE);
				
				float[] tblwidth={1.0f,.60f,1.0f,.30f,.30f,.30f,.39f,.30f,.60f,.22f,.42f,.42f,.28f,.22f,.22f,.30f,.50f};
				
				mainTable = new PdfPTable(tblwidth);
				mainTable.setWidthPercentage(100);
				para = new Paragraph[17];
				cell = new PdfPCell[17];
		// header
				para[0] = new Paragraph("Candidate Name",font10_10);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setBackgroundColor(bluecolor);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				//cell[0].setBorder(1);
				mainTable.addCell(cell[0]);
				
				para[1] = new Paragraph(""+"District Name",font10_10);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setBackgroundColor(bluecolor);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				//cell[1].setBorder(1);
				mainTable.addCell(cell[1]);
				
				para[2] = new Paragraph(""+"Job Title",font10_10);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setBackgroundColor(bluecolor);
				cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
				//cell[1].setBorder(1);
				mainTable.addCell(cell[2]);
				
				para[3] = new Paragraph(""+"Posting Date",font10_10);
				cell[3]= new PdfPCell(para[3]);
				cell[3].setBackgroundColor(bluecolor);
				cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
				//cell[1].setBorder(1);
				mainTable.addCell(cell[3]);
				
				para[4] = new Paragraph(""+"Expiration Date",font10_10);
				cell[4]= new PdfPCell(para[4]);
				cell[4].setBackgroundColor(bluecolor);
				cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
				//cell[1].setBorder(1);
				mainTable.addCell(cell[4]);
				
				para[5] = new Paragraph(""+"Application Date",font10_10);
				cell[5]= new PdfPCell(para[5]);
				cell[5].setBackgroundColor(bluecolor);
				cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
				//cell[1].setBorder(1);
				mainTable.addCell(cell[5]);
				
				para[6] = new Paragraph(""+"Norm Score",font10_10);
				cell[6]= new PdfPCell(para[6]);
				cell[6].setBackgroundColor(bluecolor);
				cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
				//cell[1].setBorder(1);
				mainTable.addCell(cell[6]);
				
				para[7] = new Paragraph(""+"Application Status",font10_10);
				cell[7]= new PdfPCell(para[7]);
				cell[7].setBackgroundColor(bluecolor);
				cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
			//	cell[4].setBorder(1);
				mainTable.addCell(cell[7]);
				
				para[8] = new Paragraph(""+"Hired Date",font10_10);
				cell[8]= new PdfPCell(para[8]);
				cell[8].setBackgroundColor(bluecolor);
				cell[8].setHorizontalAlignment(Element.ALIGN_LEFT);
			//	cell[4].setBorder(1);
				mainTable.addCell(cell[8]);
				
				para[9] = new Paragraph(""+"Hired By",font10_10);
				cell[9]= new PdfPCell(para[9]);
				cell[9].setBackgroundColor(bluecolor);
				cell[9].setHorizontalAlignment(Element.ALIGN_LEFT);
			//	cell[4].setBorder(1);
				mainTable.addCell(cell[9]);
				
				
				
				para[10] = new Paragraph(""+"Total Candidates",font10_10);
				cell[10]= new PdfPCell(para[10]);
				cell[10].setBackgroundColor(bluecolor);
				cell[10].setHorizontalAlignment(Element.ALIGN_LEFT);
			//	cell[2].setBorder(1);
				mainTable.addCell(cell[10]);

				para[11] = new Paragraph(""+"Available Candidates",font10_10);
				cell[11]= new PdfPCell(para[11]);
				cell[11].setBackgroundColor(bluecolor);
				cell[11].setHorizontalAlignment(Element.ALIGN_LEFT);
				//cell[3].setBorder(1);
				mainTable.addCell(cell[11]);
				
				
				para[12] = new Paragraph(""+"Internal ",font10_10);
				cell[12]= new PdfPCell(para[12]);
				cell[12].setBackgroundColor(bluecolor);
				cell[12].setHorizontalAlignment(Element.ALIGN_LEFT);
				//cell[5].setBorder(1);
				mainTable.addCell(cell[12]);
		
				
				para[13] = new Paragraph(""+"A Score",font10_10);
				cell[13]= new PdfPCell(para[13]);
				cell[13].setBackgroundColor(bluecolor);
				cell[13].setHorizontalAlignment(Element.ALIGN_LEFT);
			//	cell[2].setBorder(1);
				mainTable.addCell(cell[13]);

				para[14] = new Paragraph(""+"L/R Score",font10_10);
				cell[14]= new PdfPCell(para[14]);
				cell[14].setBackgroundColor(bluecolor);
				cell[14].setHorizontalAlignment(Element.ALIGN_LEFT);
				//cell[3].setBorder(1);
				mainTable.addCell(cell[14]);
				
				para[15] = new Paragraph(""+"Fit Score",font10_10);
				cell[15]= new PdfPCell(para[15]);
				cell[15].setBackgroundColor(bluecolor);
				cell[15].setHorizontalAlignment(Element.ALIGN_LEFT);
			//	cell[4].setBorder(1);
				mainTable.addCell(cell[15]);
				
				para[16] = new Paragraph(""+"Activity Date",font10_10);
				cell[16]= new PdfPCell(para[16]);
				cell[16].setBackgroundColor(bluecolor);
				cell[16].setHorizontalAlignment(Element.ALIGN_LEFT);
			//	cell[4].setBorder(1);
				mainTable.addCell(cell[16]);
				
				document.add(mainTable);
			
				if(jobForTeacherLists.size()==0){
				    float[] tblwidth11={.10f};
					
					 mainTable = new PdfPTable(tblwidth11);
					 mainTable.setWidthPercentage(100);
					 para = new Paragraph[1];
					 cell = new PdfPCell[1];
				
					 para[0] = new Paragraph("No Record Found.",font8bold);
					 cell[0]= new PdfPCell(para[0]);
					 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
					 //cell[index].setBorder(1);
					 mainTable.addCell(cell[0]);
				
				document.add(mainTable);
			}
				 List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJobsActivity = new ArrayList<TeacherStatusHistoryForJob>();
				 Map<String,TeacherStatusHistoryForJob> mapteacherStatusHistoryACtivity = new HashMap<String, TeacherStatusHistoryForJob>();
						
				 if(jobForTeacherLists.size()>0)
				 {
					 List<TeacherDetail> tIds = new ArrayList<TeacherDetail>();
					 List<JobOrder> jIds = new ArrayList<JobOrder>();
					 List<StatusMaster> statusIds = new ArrayList<StatusMaster>();
					 
					 for(JobForTeacher jft:jobForTeacherLists){
						 tIds.add(jft.getTeacherId());
						 jIds.add(jft.getJobId());
						 statusIds.add(jft.getStatus());
					 }
					 					 
					 lstTeacherStatusHistoryForJobsActivity = teacherStatusHistoryForJobDAO.FindStatusForLastActivity(tIds.get(0), jIds.get(0), statusIds.get(0));
					 System.out.println("*****************************************lstTeacherStatusHistoryForJobsActivity======"+lstTeacherStatusHistoryForJobsActivity.size());
					 
					 if(lstTeacherStatusHistoryForJobsActivity!=null && lstTeacherStatusHistoryForJobsActivity.size()>0)
					 {
						 for(TeacherStatusHistoryForJob lastActivitySatatus : lstTeacherStatusHistoryForJobsActivity)
						 {							
							 if(lastActivitySatatus.getTeacherDetail().getTeacherId()!=null && lastActivitySatatus.getJobOrder().getJobId()!=null && lastActivitySatatus.getStatusMaster().getStatusId()!=null)
							 mapteacherStatusHistoryACtivity.put(lastActivitySatatus.getTeacherDetail().getTeacherId()+"#"+lastActivitySatatus.getJobOrder().getJobId()+"#"+lastActivitySatatus.getStatusMaster().getStatusId(), lastActivitySatatus);
						 }					 
					 }				 
				 }

				
				
			 	 
		if(jobForTeacherLists.size()>0){
		 for(JobForTeacher jft : jobForTeacherLists){
			 int index=0;
			 float[] tblwidth1={1.0f,.60f,1.0f,.30f,.30f,.30f,.39f,.30f,.60f,.22f,.42f,.42f,.28f,.22f,.22f,.30f,.50f};
				
			mainTable = new PdfPTable(tblwidth1);
			mainTable.setWidthPercentage(100);
			para = new Paragraph[17];
			cell = new PdfPCell[17];
			
			 //Candidate Name
			 String name=jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")";
			 
			 para[index] = new Paragraph(name,font8bold_new);
			 cell[index]= new PdfPCell(para[index]);
			 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
			 mainTable.addCell(cell[index]);
			 index++;
	
			//District Name
			 para[index] = new Paragraph(""+jft.getJobId().getDistrictMaster().getDistrictName(),font8bold_new);
			 cell[index]= new PdfPCell(para[index]);
			 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
			 mainTable.addCell(cell[index]);
			 index++;
			 
			 //job Title
			 para[index] = new Paragraph(""+jft.getJobId().getJobTitle(),font8bold_new);
			 cell[index]= new PdfPCell(para[index]);
			 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
			 mainTable.addCell(cell[index]);
			 index++;
			 
			 //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			 para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobStartDate())+",12:01 AM",font8bold_new);
			 cell[index]= new PdfPCell(para[index]);
			 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
			 mainTable.addCell(cell[index]);
			 index++;
			 
			 if(Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobEndDate()).equals("Dec 25, 2099"))
				 para[index] = new Paragraph("Until filled",font8bold_new);
			 else
				 para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobEndDate())+",11:59 PM",font8bold_new);
			 cell[index]= new PdfPCell(para[index]);
			 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
			 mainTable.addCell(cell[index]);
			 index++;
			 
			 para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getCreatedDateTime()),font8bold_new);
			 cell[index]= new PdfPCell(para[index]);
			 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
			 mainTable.addCell(cell[index]);
			 index++;
			//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			 
			 
			 //for Norm Score	
				if(mapNormScores.get(jft.getTeacherId().getTeacherId())!=null){
					 para[index] = new Paragraph(""+mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore(),font8bold_new);
					 cell[index]= new PdfPCell(para[index]);
					 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell[index].setBackgroundColor(new Color(Integer.parseInt(mapNormScores.get(jft.getTeacherId().getTeacherId()).getDecileColor() ,16)));
					 
					 mainTable.addCell(cell[index]);
					 index++;
				}else{
					 para[index] = new Paragraph(""+"N/A",font8bold_new);
					 cell[index]= new PdfPCell(para[index]);
					 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
					 mainTable.addCell(cell[index]);
					 index++;
				}
		 
			 
			// job Satatus 
				
				StatusMaster statusObj =jft.getStatusMaster();
				
				String mapKey="";
				try {
					if(jft.getStatusMaster()!=null){
						mapKey=jft.getStatusMaster().getStatusId()+"##0"+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
						if(mapSecStatuss.get(mapKey)!=null){
							statusObj=mapSecStatuss.get(mapKey).getStatusMaster();
						}
					}else{
						mapKey="##0"+jft.getSecondaryStatus().getSecondaryStatusId()+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				String sDidplayStatusName="";
				
				
				try {
					if(statusObj!=null)
					{
						if(statusObj.getStatusShortName().equalsIgnoreCase("comp")){
							sDidplayStatusName="Available"; 
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("rem")){
							sDidplayStatusName=statusObj.getStatus();
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("icomp")){
							sDidplayStatusName=statusObj.getStatus();
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("vlt")){
							sDidplayStatusName="Time Out"; 
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("scomp")){
							sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName();
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("ecomp")){
							sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName();
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("vcomp")){
							sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName();
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("dcln"))
							sDidplayStatusName="Declined"; 
						else if(statusObj.getStatusShortName().equalsIgnoreCase("widrw"))
							sDidplayStatusName="Withdrew"; 
						else if(statusObj.getStatusShortName().equalsIgnoreCase("hird"))
							sDidplayStatusName="Hired"; 
					}else{
						if(jft.getSecondaryStatus()!=null)
							if(mapSecStatuss.get(mapKey)!=null)
							sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName(); 
							else
							sDidplayStatusName=jft.getSecondaryStatus().getSecondaryStatusName();  
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 para[index] = new Paragraph(""+sDidplayStatusName,font8bold_new);
				 cell[index]= new PdfPCell(para[index]);
				 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
				 mainTable.addCell(cell[index]);
				 index++;


				 if(mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId())!=null){
					 para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId()).getCreatedDateTime()),font8bold_new);
					 cell[index]= new PdfPCell(para[index]);
					 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
					 mainTable.addCell(cell[index]);
					 index++;
					}else{
						 para[index] = new Paragraph(""+"-",font8bold_new);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
					}
					if(mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId())!=null){
						if(jft.getSchoolMaster()!=null){
							 para[index] = new Paragraph(""+jft.getSchoolMaster().getSchoolName(),font8bold_new);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
						}else{
							para[index] = new Paragraph(""+mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId()).getUserMaster().getDistrictId().getDistrictName(),font8bold_new);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
						}
					}else{
						 para[index] = new Paragraph(""+"-",font8bold_new);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
					}
				 
				 
				 
				
			 // Total And Available candidates
			 
			 if(maptotalCandidates.get(jft.getJobId().getJobId())!=null){
				 para[index] = new Paragraph(""+maptotalCandidates.get(jft.getJobId().getJobId()).size(),font8bold_new);
				 cell[index]= new PdfPCell(para[index]);
				 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
				 mainTable.addCell(cell[index]);
				 index++;
				}else{
					 para[index] = new Paragraph("" +"0",font8bold_new);
					 cell[index]= new PdfPCell(para[index]);
					 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
					 mainTable.addCell(cell[index]);
				}
				if(maptotalAvalCandidates.get(jft.getJobId().getJobId())!=null){
					 para[index] = new Paragraph(""+maptotalAvalCandidates.get(jft.getJobId().getJobId()),font8bold_new);
					 cell[index]= new PdfPCell(para[index]);
					 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
					 mainTable.addCell(cell[index]);
					 index++;
				}else{
					para[index] = new Paragraph(""+"0",font8bold_new);
					 cell[index]= new PdfPCell(para[index]);
					 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
					 mainTable.addCell(cell[index]);
					 index++;
				}
		 
		
				//Are you district Employee?(Internal)
				if(jft.getIsAffilated()!=null && jft.getIsAffilated()==1){
					 para[index] = new Paragraph(""+"Yes",font8bold_new);
					 cell[index]= new PdfPCell(para[index]);
					 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
					 mainTable.addCell(cell[index]);
					 index++;
				}
				else{
					 para[index] = new Paragraph(""+"No",font8bold_new);
					 cell[index]= new PdfPCell(para[index]);
					 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
					 mainTable.addCell(cell[index]);
					 index++;
				}
				
				
				
				 // for A Score
				   if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
					{
						if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()))
						{
							if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore().equals("") && mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()!=null){
								 para[index] = new Paragraph(""+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()+"/"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementMaxScore(),font8bold_new);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							}else{
								 para[index] = new Paragraph(""+"N/A",font8bold_new);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							}
						}
					}
					else
					{
						para[index] = new Paragraph(""+"N/A",font8bold_new);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
					}
				
				   //for L/R Score 
				   if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
					{
						if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()))
						{
							if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore().equals("") &&  mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()!=null ){
								 para[index] = new Paragraph(""+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()+"/"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementMaxScore(),font8bold_new);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							}else{
								para[index] = new Paragraph(""+"N/A",font8bold_new);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							}
						}else
						{
							para[index] = new Paragraph(""+"N/A",font8bold_new);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
						}
					}
					else
					{
						para[index] = new Paragraph(""+"N/A",font8bold_new);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
					}
				
					   
				  // for Fit Score 	
				 if(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
					{
					  if(jft.getTeacherId().getTeacherId().toString().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()) && jft.getJobId().getJobId().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobOrder().getJobId()))
						{
						  para[index] = new Paragraph(""+mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseConsolidatedScore()+"/"+mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseMaxScore(),font8bold_new);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
						}
					     else{	
					    	 para[index] = new Paragraph(""+"N/A",font8bold_new);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
							 }
					}
					else
					{
						para[index] = new Paragraph(""+"N/A",font8bold_new);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
					}
				 
				 if(jft.getLastActivityDate()!=null)
					{
						 para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getLastActivityDate()),font8bold_new);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
					}
				 else if(mapteacherStatusHistoryACtivity.get(jft.getTeacherId().getTeacherId()+"#"+jft.getJobId().getJobId()+"#"+jft.getStatus().getStatusId())!=null)
				 {
					 para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate( mapteacherStatusHistoryACtivity.get(jft.getTeacherId().getTeacherId()+"#"+jft.getJobId().getJobId()+"#"+jft.getStatus().getStatusId()).getCreatedDateTime()),font8bold_new);
					 cell[index]= new PdfPCell(para[index]);
					 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
					 mainTable.addCell(cell[index]);
					 index++;
					
				 }				 
				 else
					{
						 para[index] = new Paragraph(""+"N/A",font8bold_new);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
					}
				 
				document.add(mainTable);
					 
		 }
			
		}
	
	}catch(Exception e){e.printStackTrace();}
	finally
	{
		if(document != null && document.isOpen())
			document.close();
		if(writer!=null){
			writer.flush();
			writer.close();
		}
	}
	return true;

}
public String candidateJobStatusReportPrintPreview(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
		schoolId,String subjectId,String certifications,String jobOrderId,String status,String noOfRow,
		String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo, String firstName,String lastName,
		String emailAddress, String stateId,String certType,String jobCategoryIds,String statusId,
		String sfromDate, String stoDate, String endfromDate, String endtoDate, String appsfromDate, String appstoDate, 
		boolean pdateFlag,String normScoreSelectVal,String normScoreVal, int intenalchk,String hiredfromDate, String hiredtoDate,String jobStatus,int hiddenjob)
{
	
	System.out.println(sortOrderType+" ::@@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	StringBuffer tmRecords =	new StringBuffer();
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		SubjectMaster subjectMaster	=	null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		
		//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
		//-- get no of record in grid,
		//-- set start and end position

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------
		 /** set default sorting fieldName **/
			String sortOrderFieldName="jobTitle";
			String sortOrderNoField="jobTitle";
			
			boolean deafultFlag=false;
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
		
		//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
			
			List<JobForTeacher>	jobForTeacherLists    = new ArrayList<JobForTeacher>();
			List<JobForTeacher>	listByFirstName       = new ArrayList<JobForTeacher>();
			List<JobForTeacher>	jobJobId              = new ArrayList<JobForTeacher>();
			List<JobForTeacher> listByJobCategory     = new ArrayList<JobForTeacher>();
			
			boolean checkflag = false;
			
			if(entityID==2){
				if(schoolId==0){
					jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatusAll(districtMaster);
					if(jobForTeacherLists.size()>0)
					checkflag = true;
				}else{
					List<JobOrder> lstJobOrders =null;
					SchoolMaster  sclMaster=null;
					sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
					lstJobOrders = schoolInJobOrderDAO.allJobOrderBySchool(sclMaster);
					if(lstJobOrders.size()>0){
						jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOBStatus(lstJobOrders);
						checkflag = true;
					}
				}
				
			}
			else if(entityID==1) 
			{
				if(districtOrSchoolId!=0){
					if(schoolId==0){
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
				        jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatusAll(districtMaster);
				        if(jobForTeacherLists.size()>0)
				        	checkflag = true;
					}
					else{
						List<JobOrder> lstJobOrders =null;
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.allJobOrderBySchool(sclMaster);
						if(lstJobOrders.size()>0){
							jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOBStatus(lstJobOrders);
							checkflag = true;
						}
					}
				}
				else{
					jobForTeacherLists = new ArrayList<JobForTeacher>();
				}
			}else if(entityID==3){
				if(schoolId==0){
					 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
			        jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatus(districtMaster);
			        if(jobForTeacherLists.size()>0)
			        	checkflag = true;
				}else{
					List<JobOrder> lstJobOrders =null;
					SchoolMaster  sclMaster=null;
					sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
					lstJobOrders = schoolInJobOrderDAO.allJobOrderBySchool(sclMaster);
					if(lstJobOrders.size()>0){
						jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOBStatus(lstJobOrders);
						checkflag = true;
					}
				}
			}
		
//filter start
			//Jobstatus filter *******************************************************************************************************
				if (jobStatus.equals("A") || jobStatus.equals("I")) {
					List<Integer> list = new ArrayList<Integer>();
					for (JobForTeacher jobForTeacher : jobForTeacherLists) {
						list.add(jobForTeacher.getJobId().getJobId());
						// System.out.println("??????????????????       "+jobForTeacher.getJobId().getJobId());
					}
					List<JobOrder> jobstatuslist = new ArrayList<JobOrder>();
					boolean jobstatusflag = false;
					if (jobStatus.equals("A")) {
						districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
						Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
						Criterion criterion = Restrictions.eq("status", jobStatus);
						Criterion criterion2 = Restrictions.in("jobId", list);
						jobstatuslist = jobOrderDAO.findByCriteria(criterion,criterion1, criterion2);
						System.out.println("jobstatuslist---------------"+ jobstatuslist.size() + "           "	+ list.size());
						if (jobstatuslist.size() > 0) {
							jobstatusflag = true;
						}
					}

					if (jobStatus.equals("I")) {
						districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
						Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
						Criterion criterion = Restrictions.eq("status", jobStatus);
						Criterion criterion2 = Restrictions.in("jobId", list);
						jobstatuslist = jobOrderDAO.findByCriteria(criterion,criterion1, criterion2);
						System.out.println("jobstatuslist---------------"+ jobstatuslist.size() + "           "	+ list.size());
						if (jobstatuslist.size() > 0) {
							jobstatusflag = true;
						}
					}

					List<JobForTeacher> jobForTeacher1 = new ArrayList<JobForTeacher>();
					if (jobstatusflag) {
						for (JobOrder jobOrder : jobstatuslist) {
							for (JobForTeacher jobForTeacherLists1 : jobForTeacherLists) {
								if (jobOrder.getJobId().equals(
										jobForTeacherLists1.getJobId().getJobId())) {
									jobForTeacher1.add(jobForTeacherLists1);
									//break;
								}
							}
						}
						jobForTeacherLists = jobForTeacher1;
					}
				}
				
				//Hidden Job filter start**************************************************************************************************
				Boolean hdjob=false;
				if(hiddenjob==1)
				{
					hdjob=true;
				}else if(hiddenjob==0)
				{
					hdjob=false;
				}
				//System.out.println("hiddenjob------------"+hdjob);
					List<Integer> hiddenJobTempList = new ArrayList<Integer>();
					for (JobForTeacher jobForTeacher : jobForTeacherLists) {
						hiddenJobTempList.add(jobForTeacher.getJobId().getJobId());
						// System.out.println("jobForTeacherLists  JobID   "+jobForTeacher.getJobId().getJobId());
					}
					List<JobOrder> hiddenJobList = new ArrayList<JobOrder>();
					boolean hiddenJobflag = false;
					if (hiddenjob!=2) {
						districtMaster = districtMasterDAO.findById(districtOrSchoolId, false, false);
						Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
						Criterion criterion = Restrictions.eq("hiddenJob",hdjob);
						Criterion criterion2 = Restrictions.in("jobId",	hiddenJobTempList);
						hiddenJobList = jobOrderDAO.findByCriteria(criterion,criterion1, criterion2);
						//System.out.println("hiddenJobTempList---------------"+ hiddenJobTempList.size() + "           "	+ hiddenJobList.size());
						if (hiddenJobList.size() > 0) {
							hiddenJobflag = true;
						}else
						{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						}
						}
					List<JobForTeacher> jobForTeacherHiddenJob = new ArrayList<JobForTeacher>();
					if (hiddenJobflag) {
						for (JobOrder jobOrder : hiddenJobList) {
							for (JobForTeacher jobForTeacherLists1 : jobForTeacherLists) {
								if (jobOrder.getJobId().equals(	jobForTeacherLists1.getJobId().getJobId())) {
									jobForTeacherHiddenJob.add(jobForTeacherLists1);
									//break;
								}
							}
						}
						jobForTeacherLists = jobForTeacherHiddenJob;
					}
			// Hidden Filter End			
			//******* first Name , Last Name and Email filter 
			
			List<TeacherDetail> basicSearchList = new ArrayList<TeacherDetail>();
		
			List<Criterion> basicSearch = new ArrayList<Criterion>();
			boolean basicFlag = false;
			
			if(firstName!=null && !firstName.equals(""))
			{
				Criterion criterion = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
				basicSearch.add(criterion);
				basicFlag = true;
			}
				
			if(lastName!=null && !lastName.equals(""))
			{
				Criterion criterion = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
				basicSearch.add(criterion);
				basicFlag = true;
			}
			
			if(emailAddress!=null && !emailAddress.equals(""))
			{
				Criterion criterion = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
				basicSearch.add(criterion);
				basicFlag = true;
			}
			if(basicFlag)
			{
				basicSearchList  = teacherDetailDAO.getTeachersByNameAndEmail(basicSearch);
				if(basicSearchList!=null && basicSearchList.size()>0)
				{
					listByFirstName = jobForTeacherDAO.findTeacherByTeacherDeatailAndJobStatus(basicSearchList);
					 if(listByFirstName!=null && listByFirstName.size()>0){
						 if(checkflag){
		   						jobForTeacherLists.retainAll(listByFirstName);
		   					}else{
		   						jobForTeacherLists.addAll(listByFirstName);
		   						checkflag=true;
		   					}
					 }
					 else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						 }
				}
				else{
					jobForTeacherLists = new ArrayList<JobForTeacher>();
				 }
			}
			
   		 //****** job ID filter
			//JobOrder joborder =null;
			if(!jobOrderId.equals(""))
			{
				List<Integer> jobIDS= new ArrayList<Integer>();
				String strjobIds[]=jobOrderId.split(",");
				
				if(strjobIds.length>0)
			     for(String str : strjobIds){
			    	 if(!str.equals(""))
			    	 jobIDS.add(Integer.parseInt(str));
			     }
					jobJobId = jobForTeacherDAO.findbyJobOrderAndStatus(jobIDS);
				
					if (jobJobId.size()>0){ 
						if(checkflag)
						{ 
							jobForTeacherLists.retainAll(jobJobId);
						}
						else{
							jobForTeacherLists.addAll(jobJobId);
							checkflag=true;
						}
						
					}else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					 }
			  }
			
		// Licensure Name/Licensure State filter
			List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
			List<JobOrder> jobOrderIds=new ArrayList<JobOrder>();
            List<JobForTeacher> listBycertification = new ArrayList<JobForTeacher>();
			StateMaster stateMaster=null;
			List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
			
			
			if(!certType.equals("0") && !certType.equals("")||!stateId.equals("0")){
				if(!stateId.equals("0")){
					stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
				}
				certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
				//System.out.println("certificateTypeMasterList:: "+certificateTypeMasterList.size());
				if(certificateTypeMasterList.size()>0)
					certJobOrderList=jobCertificationDAO.findCertificationByJobWithState(certificateTypeMasterList);
				System.out.println("certJobOrderList:: "+certJobOrderList.size());
				if(certJobOrderList.size()>0){
					for(JobOrder job: certJobOrderList){
						jobOrderIds.add(job);
					}
				}
				if(jobOrderIds.size()>0){
					listBycertification = jobForTeacherDAO.getJobForTeacherByJOBStatus(jobOrderIds);
					if(listBycertification.size()>0){
						if(checkflag){
	   						jobForTeacherLists.retainAll(listBycertification);
	   					}else{
	   						jobForTeacherLists.addAll(listBycertification);
	   						checkflag=true;
	   					}
					}else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					}
				}else{
					jobForTeacherLists = new ArrayList<JobForTeacher>();
				}
			
			}
		
		 //filter job category
			if(!jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
			{
				List<Integer> jobcatIds = new ArrayList<Integer>();
				List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
				String jobCategoryIdStr[] =jobCategoryIds.split(",");
				if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
				{
				 for(String str : jobCategoryIdStr){
			    	 if(!str.equals(""))
			    		 jobcatIds.add(Integer.parseInt(str));
				 } 
		    	 if(jobcatIds.size()>0){
		    		 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
		    	 }
		    	 if(listjJobOrders.size()>0){
		    		 listByJobCategory = jobForTeacherDAO.getJobForTeacherByJOBStatus(listjJobOrders);
		    		 if(listByJobCategory.size()>0){
							if(checkflag){
		   						jobForTeacherLists.retainAll(listByJobCategory);
		   					}else{
		   						jobForTeacherLists.addAll(listByJobCategory);
		   						checkflag=true;
		   					}
						}else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						}
		         }
		    	 else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					 }
				}
			}
			// filter of job status 
			
			List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
			List<JobForTeacher>  listByJobStatus = new ArrayList<JobForTeacher>();
			
			 if(!statusId.equals("") && !statusId.equals("0")){
				 System.out.println(" Filter StatusId Start ::  "+statusId);
				 String statusNames[] = statusId.split("#@");
				
			      if(!ArrayUtils.contains(statusNames,"0"))
			      {	 
					 DistrictMaster districtMaster2 = districtMasterDAO.findById(districtOrSchoolId, false, false);
					 lstSecondaryStatus = secondaryStatusDAO.findByMultipleStatusName(districtMaster2, statusNames);
			
					  List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
					  Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
					  for (StatusMaster statusMaster1 : statusMasterList) {
						  mapStatus.put(statusMaster1.getStatusShortName(),statusMaster1);
					  }
					  StatusMaster stMaster =null;
					  List <StatusMaster> statusMaster1st=new ArrayList<StatusMaster>();
					  
					  if(ArrayUtils.contains(statusNames,"Available")){
							  stMaster = mapStatus.get("comp");
							  statusMaster1st.add(stMaster);
					  }
					  if(ArrayUtils.contains(statusNames,"Rejected")){
						    stMaster = mapStatus.get("rem");
						    statusMaster1st.add(stMaster);
				     }
					  if(ArrayUtils.contains(statusNames,"Timed Out")){
						    stMaster = mapStatus.get("vlt");
						    statusMaster1st.add(stMaster);
				     }
					 if(ArrayUtils.contains(statusNames,"Withdrew")){
						    stMaster = mapStatus.get("widrw");
						    statusMaster1st.add(stMaster);
				     }
					 if(ArrayUtils.contains(statusNames,"Incomplete")){
						    stMaster = mapStatus.get("icomp");
						    statusMaster1st.add(stMaster);
				     }
					 if(ArrayUtils.contains(statusNames,"Hired")){
						    stMaster = mapStatus.get("hird");
						    statusMaster1st.add(stMaster);
				     }   
					 
					 for(SecondaryStatus ss: lstSecondaryStatus)
					  {
						 if(ss.getStatusMaster()!=null)
							 statusMaster1st.add(ss.getStatusMaster()); 
					  }
				    /* if(statusMaster1st.size()==0){ 
							  listByJobStatus = jobForTeacherDAO.findBySecondStatus(lstSecondaryStatus);
					  }else{*/
				     listByJobStatus = jobForTeacherDAO.findAvailableCandidtes(districtMaster2,statusMaster1st,lstSecondaryStatus);
					 // }
					  if(listByJobStatus.size()>0){
							if(checkflag){
		   						jobForTeacherLists.retainAll(listByJobStatus);
		   					}else{
		   						jobForTeacherLists.addAll(listByJobStatus);
		   						checkflag=true;
		   					}
						}else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						}
		         }
					  
			 }
		  // Advanced filters + Date Filter Start  Norm Score +A
			
			 List<JobForTeacher> lstDateJFT =new ArrayList<JobForTeacher>();
			 if(pdateFlag){
				 List<TeacherDetail> normTeacherList = new ArrayList<TeacherDetail>();
				 if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") && !normScoreSelectVal.equals("6"))
				 {
					normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore(normScoreVal,normScoreSelectVal);
				 }
				 
				if(!normScoreSelectVal.equals("") && normScoreSelectVal.equals("6"))
				{
					 normTeacherList=teacherNormScoreDAO.findTeacherListByNormScore("0","5");
				}
				 lstDateJFT = jobForTeacherDAO.findJobForTeacherbyInputDateFilterwithDistrict(districtMaster,sfromDate,stoDate,endfromDate,endtoDate,appsfromDate,appstoDate,normTeacherList ,normScoreSelectVal,intenalchk);
				 if(lstDateJFT!=null && lstDateJFT.size()>0)
				 {
					 if(checkflag){
	   						jobForTeacherLists.retainAll(lstDateJFT);
	   					}else{
	   						jobForTeacherLists.addAll(lstDateJFT);
	   						checkflag=true;
	   					}
				 }else{
					 jobForTeacherLists = new ArrayList<JobForTeacher>();
				 }
			 }
			
			//filter of Hiring Date
			 List<JobForTeacher> lstTeacherStatusHistoryForJobs = new ArrayList<JobForTeacher>();
			 if(!hiredfromDate.equals("") || !hiredtoDate.equals("")){
				 System.out.println("filter of Hiring Date ");
				 List<JobForTeacher> filterHCForTeachers =new ArrayList<JobForTeacher>();
				 List<TeacherDetail> lstHCDetails =  new ArrayList<TeacherDetail>();
				 Map<String,JobForTeacher> mapteacherStatusHistory = new HashMap<String, JobForTeacher>();
					 lstTeacherStatusHistoryForJobs = jobForTeacherDAO.findByHIredDAte(districtMaster, hiredfromDate, hiredtoDate);
				System.out.println("lstTeacherStatusHistoryForJobs :: "+lstTeacherStatusHistoryForJobs.size());
				
					 if(lstTeacherStatusHistoryForJobs!=null && lstTeacherStatusHistoryForJobs.size()>0){
					 for(JobForTeacher hc : lstTeacherStatusHistoryForJobs){
						 lstHCDetails.add(hc.getTeacherId());
						 mapteacherStatusHistory.put(hc.getJobId().getJobId()+"#"+hc.getTeacherId().getTeacherId(), hc);
					 }
				       List<JobForTeacher> lstHCForTeachers = jobForTeacherDAO.findTeacherByTeacherDeatailAndJobStatus(lstHCDetails);
				       for(JobForTeacher jft: lstHCForTeachers){
				    	   if(mapteacherStatusHistory.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId())!=null){
				    		   filterHCForTeachers.add(jft);
				    	   }
				       }
				       
				       if(checkflag){
   						    jobForTeacherLists.retainAll(filterHCForTeachers);
		   					}
				       else{
	   						jobForTeacherLists.addAll(filterHCForTeachers);
	   						checkflag=true;
		   				}
				 } 
				 else{
					 jobForTeacherLists = new ArrayList<JobForTeacher>();
				 }
			 }
			 
			 
			 System.out.println("Final ************** jft ::::::::  "+jobForTeacherLists.size());	
			
			
		     ArrayList<Integer> teacherIds = new ArrayList<Integer>();
		     ArrayList<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
		
			 List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
			 List<TeacherNormScore> listTeacherNormScores =null;
			 Map<Integer,TeacherNormScore> mapNormScores = new HashMap<Integer, TeacherNormScore>();
			 Map<String,TeacherAchievementScore> mapTeacherAchievementScore = new HashMap<String, TeacherAchievementScore>();
		     Map<String,JobWiseConsolidatedTeacherScore> mapJWCTeacherScore = new HashMap<String, JobWiseConsolidatedTeacherScore>();
			
		  // map for candidtates count on a job   
		     Map<Integer,List<JobForTeacher>> maptotalCandidates      = new HashMap<Integer, List<JobForTeacher>>();
		     Map<Integer,Integer> maptotalAvalCandidates  = new HashMap<Integer, Integer>();
		     List<DistrictMaster> districtMasterList = new ArrayList<DistrictMaster>();
		     List<JobOrder> lstHUJobOrders = new ArrayList<JobOrder>();
		     List<TeacherDetail> lstHUTeacherDetails = new ArrayList<TeacherDetail>();
		     Map<String,JobForTeacher> mapHiredDate =new HashMap<String, JobForTeacher>(); 
		   
		     List<JobCategoryMaster> categoryMasters= new ArrayList<JobCategoryMaster>();
		     
		     List<JobOrder> listJoboOrders = new ArrayList<JobOrder>();
		      
				
		     
		     String jobIdStr = ""; 
		     if(jobForTeacherLists.size()>0)
		     for(JobForTeacher jft:jobForTeacherLists) 
			 {
		    	 listTeacherDetails.add(jft.getTeacherId());
				 teacherIds.add(jft.getTeacherId().getTeacherId());
				 listJoboOrders.add(jft.getJobId());
				 categoryMasters.add(jft.getJobId().getJobCategoryMaster());
		    	 lstHUJobOrders.add(jft.getJobId());
		    	 lstHUTeacherDetails.add(jft.getTeacherId());
		    	 districtMasterList.add(jft.getJobId().getDistrictMaster());
		    	 if(jobIdStr.equals("")){
		    		 jobIdStr=""+jft.getJobId().getJobId();
		    	 }else{
		    		 jobIdStr=jobIdStr+","+jft.getJobId().getJobId();
		    	 }
				//////////////////////////////////////////////////
		    	 int jobId=jft.getJobId().getJobId();
		    	 List<JobForTeacher> tAlist = maptotalCandidates.get(jobId);
					if(tAlist==null){
						List<JobForTeacher> jobs = new ArrayList<JobForTeacher>();
						jobs.add(jft);
						maptotalCandidates.put(jobId, jobs);
					}else{
						tAlist.add(jft);
						maptotalCandidates.put(jobId, tAlist);
					}
		    	 ////////////////////////////////////////////////
					
					
			 }
		     
		 	for (Map.Entry<Integer,List<JobForTeacher>> entry : maptotalCandidates.entrySet()) {
					 List<JobForTeacher> jfList=entry.getValue();
					 int count=0;
					 for (JobForTeacher jobForTeacher : jfList) {
						 if(!jobForTeacher.getStatus().getStatusShortName().equals("icomp") && !jobForTeacher.getStatus().getStatusShortName().equals("vlt") && !jobForTeacher.getStatus().getStatusShortName().equals("widrw") && !jobForTeacher.getStatus().getStatusShortName().equals("hide")){
							 count++;
						 }
					}
					// System.out.println("count::::::"+count);
					 maptotalAvalCandidates.put(entry.getKey(),count); 
		   }
		    
		  //*********hired calculation**********************
		     List<JobForTeacher>  lstStatusHistoryForJobs = new  ArrayList<JobForTeacher>();
		     if(jobForTeacherLists!=null && jobForTeacherLists.size()>0){
		    	 lstStatusHistoryForJobs = jobForTeacherDAO.findByTeacherIdAndJObIds(districtMaster,lstHUTeacherDetails,lstHUJobOrders);
		         if(lstStatusHistoryForJobs!=null && lstStatusHistoryForJobs.size()>0){
		        	 for(JobForTeacher ht : lstStatusHistoryForJobs){
		        		 mapHiredDate.put(ht.getJobId().getJobId()+"#"+ht.getTeacherId().getTeacherId(),ht);
		        	 }
		         }
		     }
		     
		     
		    
		    // sorting logic start 
		     
		     if(sortOrder.equals("")){
		    	 for(JobForTeacher jf:jobForTeacherLists) 
			    	 jf.setJobTitle(jf.getJobId().getJobTitle());
				
				  Collections.sort(jobForTeacherLists, new JobTitleCompratorASC());
		     }
		     
		     
		     if(sortOrder.equals("lastName")){    
			     for(JobForTeacher jf:jobForTeacherLists) 
				 {
			    	 jf.setLastName(jf.getTeacherId().getLastName());
					
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, new LastNameCompratorASC());
				 else 
				  Collections.sort(jobForTeacherLists, new LastNameCompratorDESC());
		     }  
		     if(sortOrder.equals("jobTitle")){    
			     for(JobForTeacher jf:jobForTeacherLists) 
				 {
			    	 jf.setJobTitle(jf.getJobId().getJobTitle());
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, new JobTitleCompratorASC());
				 else 
				  Collections.sort(jobForTeacherLists, new JobTitleCompratorDESC());
		     } 
		    
		     if(sortOrder.equals("totalCandidate")){    
			     for(JobForTeacher jf:jobForTeacherLists) 
				 {
			    	 jf.setTotalCandidate(maptotalCandidates.get(jf.getJobId().getJobId()).size());
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, new TotalCandidateCompratorASC());
				 else 
				  Collections.sort(jobForTeacherLists, new TotalCandidateCompratorDESC());
		     } 
		 
		     if(sortOrder.equals("avlCandidate")){    
			     for(JobForTeacher jf:jobForTeacherLists) 
				 {
			    	 jf.setAvlCandidate(maptotalAvalCandidates.get(jf.getJobId().getJobId()));
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, new AvlCandidateCompratorASC());
				 else 
				  Collections.sort(jobForTeacherLists, new AvlCandidateCompratorDESC());
		     } 
		 
		    
		         
		    Map<String,SecondaryStatus> mapSecStatuss = new HashMap<String, SecondaryStatus>();
		    //List<SecondaryStatus> lstSecondaryStatuss =	secondaryStatusDAO.findSecondaryStatusWithDistrictList(districtMasterList);
		   
				List<SecondaryStatus> lstSecondaryStatuss =	secondaryStatusDAO.findSecondaryStatusByJobCategoryLists(districtMasterList,categoryMasters);
		    if(lstSecondaryStatuss!=null && lstSecondaryStatuss.size()>0)
				for(SecondaryStatus sec : lstSecondaryStatuss ){
					if(sec.getStatusMaster()!=null){
						mapSecStatuss.put(sec.getStatusMaster().getStatusId()+"##0"+"#"+sec.getJobCategoryMaster().getJobCategoryId(),sec);
					}else{
						mapSecStatuss.put("##0"+sec.getSecondaryStatusId()+"#"+sec.getJobCategoryMaster().getJobCategoryId(),sec);
					}
				}
		   	  

		   
	//Candidate Status Sorting 	
		if(sortOrder.equals("candidateStatus"))    
		 for(JobForTeacher jft: jobForTeacherLists){
			 StatusMaster statusObj =jft.getStatusMaster();
				
				String mapKey="";
				try {
					if(jft.getStatusMaster()!=null){
						mapKey=jft.getStatusMaster().getStatusId()+"##0"+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
						if(mapSecStatuss.get(mapKey)!=null){
							statusObj=mapSecStatuss.get(mapKey).getStatusMaster();
						}
					}else{
						mapKey="##0"+jft.getSecondaryStatus().getSecondaryStatusId()+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				try {
					if(statusObj!=null)
					{
						if(statusObj.getStatusShortName().equalsIgnoreCase("comp")){
							jft.setAssessmentStatus("Available"); 
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("rem")){
							jft.setAssessmentStatus(statusObj.getStatus());
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("icomp")){
							jft.setAssessmentStatus(statusObj.getStatus());
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("vlt")){
							jft.setAssessmentStatus("Time Out"); 
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("scomp")){
							jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("ecomp")){
							jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("vcomp")){
							jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
						}else if(statusObj.getStatusShortName().equalsIgnoreCase("dcln"))
							jft.setAssessmentStatus("Declined"); 
						else if(statusObj.getStatusShortName().equalsIgnoreCase("widrw"))
							jft.setAssessmentStatus("Withdrew"); 
						else if(statusObj.getStatusShortName().equalsIgnoreCase("hird"))
							jft.setAssessmentStatus("Hired"); 
					}else{
						if(jft.getSecondaryStatus()!=null)
						{
							if(mapSecStatuss.get(mapKey)!=null)
							jft.setAssessmentStatus(mapSecStatuss.get(mapKey).getSecondaryStatusName());
							else
							jft.setAssessmentStatus(jft.getSecondaryStatus().getSecondaryStatusName());
						}
							 
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 }
	    if(sortOrder.equals("candidateStatus") && sortOrderType.equals("0")){ 
		 Collections.sort(jobForTeacherLists,JobForTeacher.jobForTeacherComparatorAssessmentStatus ); 
		 }else if(sortOrder.equals("candidateStatus") && sortOrderType.equals("1")){ 
		 Collections.sort(jobForTeacherLists,JobForTeacher.jobForTeacherComparatorAssessmentStatusDesc);
		 }

		     
		if(jobForTeacherLists.size()>0)
		{	
			/*for Teacher norm score*/
			if(listTeacherDetails.size()>0){
			 listTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(listTeacherDetails);
			}
			
			for(TeacherNormScore tns:listTeacherNormScores)
		 	 {
			  mapNormScores.put(tns.getTeacherDetail().getTeacherId(),tns);
		 	 }
			
			// Sorting By normScore 
			//Sorting By A Score
		 	if(sortOrder.equals("normScore")){
					for(JobForTeacher jft : jobForTeacherLists){
						if(mapNormScores.get(jft.getTeacherId().getTeacherId()) != null){
							  jft.setNormScore(Double.valueOf(mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+""));
						}else{
							jft.setNormScore(-0.0);
						}
					}
		 	
		 	 if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorNormScore);
				 else 
				  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorNormScoreDesc);
		 	
		 	}
				
			//for A Score && L/R Schore
		    List<TeacherAchievementScore> listTeacherAchievementScores = teacherAchievementScoreDAO.getAchievementScoreListByTDList(listTeacherDetails);
		 	for(TeacherAchievementScore tas:listTeacherAchievementScores)
		 	 {
		 		mapTeacherAchievementScore.put(tas.getTeacherDetail().getTeacherId().toString(),tas);
		 	 }
			 
		 	//Sorting By A Score
		 	if(sortOrder.equals("aScore")){
					for(JobForTeacher jft : jobForTeacherLists){
						if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()) != null){
							if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore().equals("") && mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()!=null)
							{
							  jft.setaScore(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore());
							}else{
								jft.setaScore(-1);
							}
						}else{
							jft.setaScore(-1);
						}
					}
		 	
		 	 if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorAScore);
				 else 
				  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorAScoreDesc);
		 	
		 	}
		 	
		 	//Sorting By L/R Score
		 	  if(sortOrder.equals("lRScore")){
					for(JobForTeacher jft : jobForTeacherLists){
						if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()) != null){
							if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore().equals("") &&  mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()!=null )
							{ 
							  jft.setlRScore(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore());
							}else{
								jft.setlRScore(-1);
							}
						}else{
							jft.setlRScore(-1);
						}
					}
		 	
		 	    if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorLRScore);
				 else 
				  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorLRScoreDesc);
		 	
		 	}
		 	
			/* for fit score */
			List<JobWiseConsolidatedTeacherScore> listJobWiseCTScore = jobWiseConsolidatedTeacherScoreDAO.findbyTeacherDetails(listTeacherDetails);
		 	for(JobWiseConsolidatedTeacherScore jwct:listJobWiseCTScore)
		 	 {
		 		mapJWCTeacherScore.put(jwct.getTeacherDetail().getTeacherId().toString(),jwct);
		 	 }
			
		 	//Sorting By Fit Score fitScore
		 	 if(sortOrder.equals("fitScore")){
					for(JobForTeacher jft : jobForTeacherLists){
						if(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()) != null){
							if(jft.getTeacherId().getTeacherId().toString().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()) && jft.getJobId().getJobId().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobOrder().getJobId()))
							{ 
							  jft.setFitScore(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseConsolidatedScore());
							}else{
								jft.setFitScore(-1.0);
							}
						}else{
							jft.setFitScore(-1.0);
						}
					}
		 	
		 	    if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorFitScore);
				 else 
				  Collections.sort(jobForTeacherLists, JobForTeacher.jobForTeacherComparatorFitScoreDesc);
		 	
		 	}
		 	 if(sortOrder.equals("distName")){  
			     for(JobForTeacher jf:jobForTeacherLists) 
				 {
			    	 jf.setDistName(jf.getJobId().getDistrictMaster().getDistrictName());
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, new DistrictNameCompratorASC());
				 else 
				  Collections.sort(jobForTeacherLists, new DistrictNameCompratorDESC());
		     } 
		 	 
		 	if(sortOrder.equals("postDate")){  
			     for(JobForTeacher jf:jobForTeacherLists) 
				 {
			    	 jf.setPostDate(jf.getJobId().getJobStartDate());
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, new PostingDateCompratorASC());
				 else 
				  Collections.sort(jobForTeacherLists, new PostingDateCompratorDESC());
		     }
		 	
		 	if(sortOrder.equals("endDate")){  
			     for(JobForTeacher jf:jobForTeacherLists) 
				 {
			    	 jf.setEndDate(jf.getJobId().getJobEndDate());
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, new EndDateCompratorASC());
				 else 
				  Collections.sort(jobForTeacherLists, new EndDateCompratorDESC());
		     } 
		 	 
		 	if(sortOrder.equals("appsDate")){  
			     for(JobForTeacher jf:jobForTeacherLists) 
				 {
			    	 jf.setAppsDate(jf.getCreatedDateTime());
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, new AppsDateCompratorASC());
				 else 
				  Collections.sort(jobForTeacherLists, new AppsDateCompratorDESC());
		     } 
		 	if(sortOrder.equals("isAffilated"))
		 	{  
		 		for(JobForTeacher jf:jobForTeacherLists) 
				 {
		 			if(jf.getIsAffilated()==null || jf.getIsAffilated()==0)
		 			 jf.setIsAffilated(0);
		 			else
		 			 jf.setIsAffilated(1);
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, new InternalCandidateCompratorASC());
				 else 
				  Collections.sort(jobForTeacherLists, new InternalCandidateCompratorDESC());
			    
			    
		     } 
		 	if(sortOrder.equals("hiredDate"))
		 	{  
		 		for(JobForTeacher jf:jobForTeacherLists) 
				 {
		 			if(mapHiredDate.get(jf.getJobId().getJobId()+"#"+jf.getTeacherId().getTeacherId())!=null){
		 				jf.setHiredDate(mapHiredDate.get(jf.getJobId().getJobId()+"#"+jf.getTeacherId().getTeacherId()).getCreatedDateTime());
		 			}
		 			else{
		 				jf.setHiredDate(new Date(0));
		 			}
				 }
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, new HiredDateCompratorASC());
				 else 
				  Collections.sort(jobForTeacherLists, new HiredDateCompratorDESC());
			    
			    
		     } 
		 	
		 	if(sortOrder.equals("activityDate")){ 
		 		
			     if(sortOrderType.equals("0"))
				  Collections.sort(jobForTeacherLists, new LastActivityDateCompratorASC());
				 else 
				  Collections.sort(jobForTeacherLists, new LastActivityDateCompratorDESC());
		     } 
		 	
		}	
	  System.out.println("xxxxxxxxxxxxxjobForTeacherLists :: "+jobForTeacherLists.size());	
	
        tmRecords.append("<div style='text-align: center; font-size: 25px; text-decoration: underline; font-weight:bold;'>Candidate Job Status Report</div><br/>");
		
		tmRecords.append("<div style='width:100%'>");
			tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+"Created by: "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
			tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>Date of Print: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
			tmRecords.append("<br/><br/>");
		tmRecords.append("</div>");
		tmRecords.append("<table  id='tblGridEECPrint' width='100%' border='0'>");
		tmRecords.append("<thead class='bg' style='display: table-header-group; '>");
		tmRecords.append("<tr>");
		
		tmRecords.append("<th  style='text-align:left; font-size:12px;' valign='top'>Candidate Name</th>");

		tmRecords.append("<th  style='text-align:left; font-size:12px;' valign='top'>District Name</th>");
		
		tmRecords.append("<th  style='text-align:left ;font-size:12px;' valign='top'>Job Title</th>");
		
		tmRecords.append("<th  style='text-align:left; font-size:12px;' valign='top'>Posting Date</th>");
		
		tmRecords.append("<th  style='text-align:left;font-size:12px;' valign='top'>Expiration Date</th>");
		
		tmRecords.append("<th  style='text-align:left;font-size:12px;' valign='top'>Application Date</th>");
		
		tmRecords.append("<th  style='text-align:left;font-size:12px;' valign='top'>EPI/Norm Score</th>");
		
		tmRecords.append("<th  style='text-align:left;font-size:12px;' valign='top'>Application Status</th>");
		
		tmRecords.append("<th  style='text-align:left;font-size:12px;' valign='top'>Hired Date</th>");
		
		tmRecords.append("<th  style='text-align:left;font-size:12px;' valign='top'>Hired By</th>");
		
		tmRecords.append("<th  style='text-align:left;font-size:12px;' valign='top'>Total Candidates</th>");
		
		tmRecords.append("<th  style='text-align:left; font-size:12px;' valign='top'>Available Candidates</th>");

		tmRecords.append("<th   style='text-align:left;font-size:12px;'valign='top'>Internal Candidate</th>");
		
	  
		
		tmRecords.append("<th   style='text-align:left ;font-size:12px;' valign='top'>A Score</th>");
		
		tmRecords.append("<th  style='text-align:left ;font-size:12px;'  valign='top'>L/R Score</th>");

		tmRecords.append("<th   style='text-align:left; font-size:12px;' valign='top'>Fit Score</th>");
		tmRecords.append("<th   style='text-align:left; font-size:12px;' valign='top'>Activity Date</th>");



		tmRecords.append("</tr>");
		tmRecords.append("</thead>");
		tmRecords.append("<tbody>");
		
		if(jobForTeacherLists==null || jobForTeacherLists.size()==0){
     		tmRecords.append("<tr><td style='font-size:12px;' >"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
		}
	
		List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJobsActivity = new ArrayList<TeacherStatusHistoryForJob>();
		 Map<String,TeacherStatusHistoryForJob> mapteacherStatusHistoryACtivity = new HashMap<String, TeacherStatusHistoryForJob>();
				
		 if(jobForTeacherLists.size()>0)
		 {
			 List<TeacherDetail> tIds = new ArrayList<TeacherDetail>();
			 List<JobOrder> jIds = new ArrayList<JobOrder>();
			 List<StatusMaster> statusIds = new ArrayList<StatusMaster>();
			 
			 for(JobForTeacher jft:jobForTeacherLists){
				 tIds.add(jft.getTeacherId());
				 jIds.add(jft.getJobId());
				 statusIds.add(jft.getStatus());
			 }
			 					 
			 lstTeacherStatusHistoryForJobsActivity = teacherStatusHistoryForJobDAO.FindStatusForLastActivity(tIds.get(0), jIds.get(0), statusIds.get(0));
			 System.out.println("*****************************************lstTeacherStatusHistoryForJobsActivity======"+lstTeacherStatusHistoryForJobsActivity.size());
			 
			 if(lstTeacherStatusHistoryForJobsActivity!=null && lstTeacherStatusHistoryForJobsActivity.size()>0)
			 {
				 for(TeacherStatusHistoryForJob lastActivitySatatus : lstTeacherStatusHistoryForJobsActivity)
				 {							
					 if(lastActivitySatatus.getTeacherDetail().getTeacherId()!=null && lastActivitySatatus.getJobOrder().getJobId()!=null && lastActivitySatatus.getStatusMaster().getStatusId()!=null)
					 mapteacherStatusHistoryACtivity.put(lastActivitySatatus.getTeacherDetail().getTeacherId()+"#"+lastActivitySatatus.getJobOrder().getJobId()+"#"+lastActivitySatatus.getStatusMaster().getStatusId(), lastActivitySatatus);
				 }					 
			 }				 
		 }
		
	
	if(jobForTeacherLists.size()>0){
		for(JobForTeacher jft  : jobForTeacherLists){
			
		 tmRecords.append("<tr>");
		 tmRecords.append("<td  style='font-size:11px;'>");
		 	tmRecords.append("<div>");
		 	tmRecords.append("<span>");		
		 	tmRecords.append("<span style='font-size:11px;'>");
		 	tmRecords.append(jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName());
		 	tmRecords.append("</br>");
		 	tmRecords.append(jft.getTeacherId().getEmailAddress());		 	
		 	tmRecords.append("</span>");
		 	tmRecords.append("</span>");
		 	tmRecords.append("</div>");
		 tmRecords.append("</td>");
		 
		 tmRecords.append("<td style='font-size:11px;'>"+jft.getJobId().getDistrictMaster().getDistrictName()+"</td>");
		 
		 tmRecords.append("<td style='font-size:11px;'>"+jft.getJobId().getJobTitle()+"</td>");	
		 
		 tmRecords.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobStartDate())+",12:01 AM</td>");
		
		 if(Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobEndDate()).equals("Dec 25, 2099"))
			 tmRecords.append("<td style='font-size:11px;'>Until filled</td>");
		 else
			 tmRecords.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobEndDate())+",11:59 PM"+"</td>");
			
		 tmRecords.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getCreatedDateTime())+"</td>");
		 
		 
		//for Norm Score	
			if(mapNormScores.get(jft.getTeacherId().getTeacherId())!=null){
				String ccsName="";
				String normScoreLen=mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+"";
				if(normScoreLen.length()==1){
					ccsName="nobground1";
				}else if(normScoreLen.length()==2){
					ccsName="nobground2";
				}else{
					ccsName="nobground3";
				}
				String colorName=mapNormScores.get(jft.getTeacherId().getTeacherId()).getDecileColor();
				
				tmRecords.append("<td style='font-size:11px;'><span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>"+mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+"</span></td>");
			}else{
			  tmRecords.append("<td style='font-size:11px;'>N/A</td>");
			}
		 
		 
		 
		 
		 StatusMaster statusObj =jft.getStatusMaster();			
			String mapKey="";
			try {
				if(jft.getStatusMaster()!=null){
					mapKey=jft.getStatusMaster().getStatusId()+"##0"+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
					if(mapSecStatuss.get(mapKey)!=null){
						statusObj=mapSecStatuss.get(mapKey).getStatusMaster();
					}
				}else{
					mapKey="##0"+jft.getSecondaryStatus().getSecondaryStatusId()+"#"+jft.getJobId().getJobCategoryMaster().getJobCategoryId();
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			String sDidplayStatusName="";
			
			
			try {
				if(statusObj!=null)
				{
					if(statusObj.getStatusShortName().equalsIgnoreCase("comp")){
						sDidplayStatusName="Available"; 
					}else if(statusObj.getStatusShortName().equalsIgnoreCase("rem")){
						sDidplayStatusName=statusObj.getStatus();
					}else if(statusObj.getStatusShortName().equalsIgnoreCase("icomp")){
						sDidplayStatusName=statusObj.getStatus();
					}else if(statusObj.getStatusShortName().equalsIgnoreCase("vlt")){
						sDidplayStatusName="Time Out"; 
					}else if(statusObj.getStatusShortName().equalsIgnoreCase("scomp")){
						sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName();
					}else if(statusObj.getStatusShortName().equalsIgnoreCase("ecomp")){
						sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName();
					}else if(statusObj.getStatusShortName().equalsIgnoreCase("vcomp")){
						sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName();
					}else if(statusObj.getStatusShortName().equalsIgnoreCase("dcln"))
						sDidplayStatusName="Declined"; 
					else if(statusObj.getStatusShortName().equalsIgnoreCase("widrw"))
						sDidplayStatusName="Withdrew"; 
					else if(statusObj.getStatusShortName().equalsIgnoreCase("hird"))
						sDidplayStatusName="Hired"; 
				}else{
					if(jft.getSecondaryStatus()!=null)
						if(mapSecStatuss.get(mapKey)!=null)
							sDidplayStatusName=mapSecStatuss.get(mapKey).getSecondaryStatusName(); 
						else
							sDidplayStatusName=jft.getSecondaryStatus().getSecondaryStatusName();  
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			tmRecords.append("<td style='font-size:11px;'>"+sDidplayStatusName+"</td>");
		 
			if(mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId())!=null){
				  tmRecords.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId()).getCreatedDateTime())+"</td>");
				}else{
					tmRecords.append("<td style='font-size:11px;'>-</td>");
				}
				if(mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId())!=null){
					if(jft.getSchoolMaster()!=null){
						tmRecords.append("<td style='font-size:11px;'>"+jft.getSchoolMaster().getSchoolName()+"</td>");
					}else{
						tmRecords.append("<td style='font-size:11px;'>"+mapHiredDate.get(jft.getJobId().getJobId()+"#"+jft.getTeacherId().getTeacherId()).getUserMaster().getDistrictId().getDistrictName()+"</td>");
					}
				}else{
					tmRecords.append("<td style='font-size:11px;'>-</td>");
				}
			
			
			
		//for total and available candidate
			if(maptotalCandidates.get(jft.getJobId().getJobId())!=null){
			tmRecords.append("<td style='text-align: left; font-size:11px;'>"+maptotalCandidates.get(jft.getJobId().getJobId()).size()+" </td>");
			}else{
				tmRecords.append("<td style='font-size:11px; text-align: left;'>0</td>");
			}
			if(maptotalAvalCandidates.get(jft.getJobId().getJobId())!=null){
			tmRecords.append("<td style='font-size:11px' text-align: left;'>"+maptotalAvalCandidates.get(jft.getJobId().getJobId())+" </td>");
			}else{
				tmRecords.append("<td  style='font-size:11px; text-align: left;'>0</td>");
			}
			
			
			
			//Are you district Employee?(Internal)
			if(jft.getIsAffilated()!=null && jft.getIsAffilated()==1)
			tmRecords.append("<td style='font-size:11px;'>"+"Yes"+"</td>");
			else
			tmRecords.append("<td style='font-size:11px;'>"+"No"+"</td>");
			
			// GOO	
		 // for A Score
		   if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()))
				{
					if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore().equals("") && mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()!=null)
				    tmRecords.append("<td style='font-size:11px;'>"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()+"/"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementMaxScore()+"</td>");
					else
					tmRecords.append("<td style='font-size:11px;'>N/A</td>");
				}
			}
			else
			{
				tmRecords.append("<td style='font-size:11px;'>N/A</td>");
			}
		
		   //for L/R Score 
		   if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()))
				{
					if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore().equals("") &&  mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()!=null )
				    tmRecords.append("<td style='font-size:11px;'>"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()+"/"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementMaxScore()+"</td>");
					else
					tmRecords.append("<td style='font-size:11px;'>N/A</td>");
				}else
				{
					tmRecords.append("<td style='font-size:11px;'> N/A</td>");
				}
			}
			else
			{
				tmRecords.append("<td style='font-size:11px;'>N/A</td>");
			}
		
			   
		  // for Fit Score 	
		 if(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
			  if(jft.getTeacherId().getTeacherId().toString().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()) && jft.getJobId().getJobId().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobOrder().getJobId()))
				{
				    tmRecords.append("<td style='font-size:11px;'>"+mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseConsolidatedScore()+"/"+mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseMaxScore()+"</td>");
				}
			     else{	
					 tmRecords.append("<td style='font-size:11px;'>N/A</td>");
					 }
			}
			else
			{
				tmRecords.append("<td style='font-size:11px;'>N/A</td>");
			}
		 
		 if(jft.getLastActivityDate()!=null)
			{
		      tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getLastActivityDate())+"</td>");
			}
		 else if(mapteacherStatusHistoryACtivity.get(jft.getTeacherId().getTeacherId()+"#"+jft.getJobId().getJobId()+"#"+jft.getStatus().getStatusId())!=null)
		 {
			 tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate( mapteacherStatusHistoryACtivity.get(jft.getTeacherId().getTeacherId()+"#"+jft.getJobId().getJobId()+"#"+jft.getStatus().getStatusId()).getCreatedDateTime())+"</td>");
		 }	 
			 else
			 {
				 tmRecords.append("<td>N/A</td>"); 
			 }
			
		
		}
		tmRecords.append("</tr>");
	}
	    
	
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");
	
	}catch (Exception e) {
		// TODO: handle exception
	}
	
	
	
	return tmRecords.toString();
}


}


