package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentQuestions;
import tm.bean.assessment.AssessmentSections;
import tm.bean.assessment.QuestionOptions;
import tm.bean.assessment.QuestionsPool;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.DistrictSpecificRefChkOptions;
import tm.bean.master.DistrictSpecificRefChkQuestions;
import tm.bean.master.DomainMaster;
import tm.bean.master.ObjectiveMaster;
import tm.bean.master.OptionsForDistrictSpecificQuestions;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.QqQuestionsetQuestions;
import tm.bean.master.ReferenceQuestionSetQuestions;
import tm.bean.master.ReferenceQuestionSets;
import tm.bean.user.UserMaster;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.OptionsForDistrictSpecificQuestionsDAO;
import tm.dao.master.QqQuestionSetsDAO;
import tm.dao.master.QqQuestionsetQuestionsDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.Utility;

public class DistrictQuestionsAjax {
	
    String locale = Utility.getValueOfPropByKey("locale");

	@Autowired
	RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	@Autowired
	DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	
	@Autowired
	OptionsForDistrictSpecificQuestionsDAO optionsForDistrictSpecificQuestionsDAO;
	
	@Autowired
	DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	QqQuestionSetsDAO qqQuestionSetsDAO;
	
	@Autowired
	QqQuestionsetQuestionsDAO qqQuestionsetQuestionsDAO;
	
	@Autowired
	HeadQuarterMasterDAO headQuarterMasterDAO;
	
	
	
	public String getQuestionInstruction(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }

		StringBuffer sb =new StringBuffer();
		try{
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,22,"assessmentquestions.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			if(districtMaster!=null && districtMaster.getTextForDistrictSpecificQuestions()!=null){
				sb.append("<table border='0' width='100%'>");
				sb.append("<tr><td align='right' valign='top' style='padding-right:20px;'>");
				
				sb.append("<span><a href='javascript:void(0)' title='Edit' onclick=\"editQuestionInstruction()\"><i class='fa fa-pencil-square-o fa-lg'></i></a></span>&nbsp;|&nbsp;");
				sb.append("<span><a href='javascript:void(0)' title='Delete' onclick=\"deleteInstruction()\"><i class='fa fa-trash-o' fa-lg></i></a></span>");
			
				sb.append("</td></tr>");
				sb.append("<tr><td style='padding-left:5px;'>"+districtMaster.getTextForDistrictSpecificQuestions()+"</td>");
				sb.append("</tr>");
				sb.append("</table>");
			}
	}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	
	public String getDistrictQuestions(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		StringBuffer sb =new StringBuffer();
		try{
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,22,"assessmentquestions.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<DistrictSpecificQuestions> districtSpecificQuestionList =null;
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			if(session.getAttribute("displayType").toString().equalsIgnoreCase("1")){
				session.setAttribute("districtMasterId", districtMaster.getDistrictId());
				session.setAttribute("districtMasterName", districtMaster.getDistrictName());
			}
			else{
				session.setAttribute("districtMasterId", 0);
				session.setAttribute("districtMasterName", "");
			}
			Criterion criterion = Restrictions.eq("districtMaster", districtMaster);
			districtSpecificQuestionList = districtSpecificQuestionsDAO.findByCriteria(criterion);
			
			int i=0;
			List<OptionsForDistrictSpecificQuestions> questionOptionsList = null;
			String questionInstruction = null;
			String shortName = "";
			
			sb.append("");
			
			for (DistrictSpecificQuestions districtSpecificQuestion : districtSpecificQuestionList) 
			{
				shortName = districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeShortName();
				
				sb.append("<tr>");
				sb.append("<td width='88%'>");
				
				sb.append("Question "+(++i)+": "+districtSpecificQuestion.getQuestion()+"<br/><br/>");

				questionOptionsList = districtSpecificQuestion.getQuestionOptions();
				if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel"))
				{
					sb.append("<table >");
					for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt"+i+"' /></td><td style='border:0px;background-color: transparent;padding: 2px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
				}
				if(shortName.equalsIgnoreCase("et"))
				{
					sb.append("<table>");
					for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt"+i+"' /></td><td style='border:0px;background-color: transparent;padding: 2px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
					
					if(districtSpecificQuestion.getQuestionExplanation()!=null)
					sb.append("<br/>"+districtSpecificQuestion.getQuestionExplanation()+"");
					sb.append("<br/><br/><textarea name='opt"+i+"' class='form-control' style='width:98%;margin:5px;' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\"/></textarea><br/>");
					//sb.append(Utility.genrateHTMLspace(148)+"Max Length: 5000 Characters<br/>");
				}
				else if(shortName.equalsIgnoreCase("ml"))
				{
					sb.append("&nbsp;&nbsp;<textarea name='opt"+i+"' class='form-control' style='width:98%;margin:5px;' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\"/></textarea><br/>");
					//sb.append(Utility.genrateHTMLspace(148)+"Max Length: 5000 Characters<br/>");
				}				
				questionInstruction = districtSpecificQuestion.getQuestionInstructions()==null?"":districtSpecificQuestion.getQuestionInstructions();
				if(!questionInstruction.equals(""))
					sb.append("<br/>"+Utility.getLocaleValuePropByKey("lblInst", locale)+": </br>"+questionInstruction);
				else
					sb.append("<br/>");
				sb.append("</td>");
				sb.append("<td width='12%'>");
				boolean pipeFlag=false;
			
				sb.append("<a title='Edit' href='districtspecificquestions.do?districtId="+districtSpecificQuestion.getDistrictMaster().getDistrictId()+"&questionId="+districtSpecificQuestion.getQuestionId()+"' ><i class='fa fa-pencil-square-o fa-lg'></i></a>");
				sb.append("&nbsp;|&nbsp;");
				if(districtSpecificQuestion.getStatus().equalsIgnoreCase("A"))
					sb.append("<span id='Q"+districtSpecificQuestion.getQuestionId()+"'><a href='javascript:void(0)' title='Deactivate' onclick=\"activateDeactivateDistrictQuestion("+districtSpecificQuestion.getQuestionId()+",'I')\"><i class='fa fa-times fa-lg'></i></a></span>");
				else
					sb.append("<span id='Q"+districtSpecificQuestion.getQuestionId()+"'><a href='javascript:void(0)' title='Activate' onclick=\"activateDeactivateDistrictQuestion("+districtSpecificQuestion.getQuestionId()+",'A')\"><i class='fa fa-check fa-lg'></i></a></span>");
		
				sb.append("</td>");

			}
			if(districtSpecificQuestionList.size()==0)
				sb.append("<tr id='tr1' ><td id='tr1' colspan=2>"+Utility.getLocaleValuePropByKey("lblNoQuestionfound1", locale)+"</td></tr>" );
			
	}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to save District Question.
	 */
	//@Transactional(readOnly=false)
	public DistrictSpecificQuestions saveDistrictQuestion(DistrictSpecificQuestions districtSpecificQuestion,DistrictMaster districtMaster,OptionsForDistrictSpecificQuestions[] optionsForDistrictSpecificQuestions)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		UserMaster user = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		if (districtSpecificQuestion.getQuestionId()==null)
		{
			districtSpecificQuestion.setCreatedDateTime(new Date());			
		}

		try{
			districtSpecificQuestion.setDistrictMaster(districtMaster);
			districtSpecificQuestion.setUserMaster(user);
			districtSpecificQuestion.setStatus("A");
			DistrictSpecificQuestions districtSpecificQuestionNew = null;
			List<OptionsForDistrictSpecificQuestions> qopt = null;
			if(districtSpecificQuestion.getQuestionId()!=null)
			{
				districtSpecificQuestionNew=districtSpecificQuestionsDAO.findById(districtSpecificQuestion.getQuestionId(), false, false);
				qopt = districtSpecificQuestionNew.getQuestionOptions();
				districtSpecificQuestion.setStatus(districtSpecificQuestionNew.getStatus());
				districtSpecificQuestion.setCreatedDateTime(districtSpecificQuestionNew.getCreatedDateTime());
				//districtSpecificQuestionsDAO.clear();
			}
			districtSpecificQuestionsDAO.makePersistent(districtSpecificQuestion);

			Map<Integer,OptionsForDistrictSpecificQuestions> map = new HashMap<Integer,OptionsForDistrictSpecificQuestions>();
			if(qopt!=null)
				for (OptionsForDistrictSpecificQuestions questionOptions2 : qopt) {
					map.put(questionOptions2.getOptionId(), questionOptions2);
				}

			for (OptionsForDistrictSpecificQuestions optionsForDistrictSpecificQuestions2 : optionsForDistrictSpecificQuestions) {
				optionsForDistrictSpecificQuestions2.setDistrictSpecificQuestions(districtSpecificQuestion);
				optionsForDistrictSpecificQuestions2.setCreatedDateTime(new Date());
				optionsForDistrictSpecificQuestions2.setUserMaster(user);
				optionsForDistrictSpecificQuestions2.setStatus("A");
				optionsForDistrictSpecificQuestionsDAO.makePersistent(optionsForDistrictSpecificQuestions2);

				if(qopt!=null)
					map.remove(optionsForDistrictSpecificQuestions2.getOptionId());
				optionsForDistrictSpecificQuestions2=null;
			}
			if(qopt!=null)
				for(Integer aKey : map.keySet()) {
					optionsForDistrictSpecificQuestionsDAO.makeTransient(map.get(aKey));
				}

		}catch (Exception e) {
			e.printStackTrace();
		}

		return districtSpecificQuestion;
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to change a particular district question status.
	 */
	@Transactional(readOnly=false)
	public DistrictSpecificQuestions activateDeactivateDistrictQuestion(int questionId,String status)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }

		try{
			DistrictSpecificQuestions districtSpecificQuestion = districtSpecificQuestionsDAO.findById(questionId, false, false);

			districtSpecificQuestion.setStatus(status);
			districtSpecificQuestionsDAO.makePersistent(districtSpecificQuestion);
			return districtSpecificQuestion;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get a particular District Question.
	 */
	@Transactional(readOnly=false)
	public DistrictSpecificQuestions getDistrictQuestionById(DistrictSpecificQuestions districtSpecificQuestion)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		System.out.println(" districtSpecificQuestion "+districtSpecificQuestion.getQuestionId());
		try{
			districtSpecificQuestion = districtSpecificQuestionsDAO.findById(districtSpecificQuestion.getQuestionId(), false, false);
			
		}catch (Exception e) {
			e.printStackTrace();
		}

		return districtSpecificQuestion;
	}
	
	@Transactional
	public DistrictSpecificQuestions saveDistrictQuestionFromQueationSet(DistrictSpecificQuestions districtSpecificQuestion,HeadQuarterMaster hqMaster,DistrictMaster dMaster,OptionsForDistrictSpecificQuestions[] optionsForDistrictSpecificQuestions,String quesSetId){
		

		System.out.println("::::::::::::::::::::::::saveDistrictQuestion ::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		UserMaster user = null;
		HeadQuarterMaster headQuarterMaster =null;
		DistrictMaster districtMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		try{
			
			if(hqMaster!=null && hqMaster.getHeadQuarterId()!=null){
				headQuarterMaster = headQuarterMasterDAO.findById(hqMaster.getHeadQuarterId(), false, false);
			}
			
			if(dMaster!=null && dMaster.getDistrictId()!=null)
				districtMaster = districtMasterDAO.findById(dMaster.getDistrictId(), false, false);
			
			if (districtSpecificQuestion.getQuestionId()==null)
			{
				districtSpecificQuestion.setCreatedDateTime(new Date());			
			}
			
			if(headQuarterMaster==null){
				districtSpecificQuestion.setDistrictMaster(districtMaster);
			}
			
			districtSpecificQuestion.setHeadQuarterMaster(headQuarterMaster);
			districtSpecificQuestion.setUserMaster(user);
			districtSpecificQuestion.setStatus("A");
			//DistrictSpecificQuestions districtSpecificQuestionNew = null;
			List<OptionsForDistrictSpecificQuestions> qopt = null;
			districtSpecificQuestionsDAO.makePersistent(districtSpecificQuestion);
			if(districtSpecificQuestion.getQuestionId()!=null)
			{
				//districtSpecificQuestionNew=districtSpecificQuestionsDAO.findById(districtSpecificQuestion.getQuestionId(), false, false);
				qopt = districtSpecificQuestion.getQuestionOptions();
				districtSpecificQuestion.setStatus(districtSpecificQuestion.getStatus());
				districtSpecificQuestion.setCreatedDateTime(districtSpecificQuestion.getCreatedDateTime());
				//districtSpecificQuestionsDAO.clear();
			}

			Map<Integer,OptionsForDistrictSpecificQuestions> map = new HashMap<Integer,OptionsForDistrictSpecificQuestions>();
			if(qopt!=null)
				for (OptionsForDistrictSpecificQuestions questionOptions2 : qopt) {
					map.put(questionOptions2.getOptionId(), questionOptions2);
				}

			for (OptionsForDistrictSpecificQuestions optionsForDistrictSpecificQuestions2 : optionsForDistrictSpecificQuestions) {
				optionsForDistrictSpecificQuestions2.setDistrictSpecificQuestions(districtSpecificQuestion);
				optionsForDistrictSpecificQuestions2.setCreatedDateTime(new Date());
				optionsForDistrictSpecificQuestions2.setUserMaster(user);
				optionsForDistrictSpecificQuestions2.setStatus("A");
				optionsForDistrictSpecificQuestionsDAO.makePersistent(optionsForDistrictSpecificQuestions2);

				if(qopt!=null)
					map.remove(optionsForDistrictSpecificQuestions2.getOptionId());
				optionsForDistrictSpecificQuestions2=null;
			}
			if(qopt!=null)
				for(Integer aKey : map.keySet()) {
					optionsForDistrictSpecificQuestionsDAO.makeTransient(map.get(aKey));
				}
			
			// Set Question Set
			
			QqQuestionSets qqQuestionSets = new QqQuestionSets();
			QqQuestionsetQuestions qqQuestionsetQuestions = new QqQuestionsetQuestions();
			
			qqQuestionSets = qqQuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
		//	System.out.println("qqQuestionSets ::>> "+qqQuestionSets.getQQQuestionSetID());
			
			int quesSequence =0;
			try
			{
				List<QqQuestionsetQuestions> existQuesList = new ArrayList<QqQuestionsetQuestions>();
				
				
				if(districtMaster!=null && headQuarterMaster==null)
					existQuesList = qqQuestionsetQuestionsDAO.findByQuesSetId(qqQuestionSets,districtMaster);
				else if(headQuarterMaster!=null)
					existQuesList = qqQuestionsetQuestionsDAO.findByQuesSetByHeadQuater(qqQuestionSets,headQuarterMaster);
				
				
				
				if(existQuesList==null || existQuesList.size()==0)
				{
					quesSequence=1;
				}
				else
				{
					if(existQuesList.size()>0)
					{
						quesSequence = existQuesList.get(existQuesList.size()-1).getQuestionSequence()+1; 
					}
				}

				qqQuestionsetQuestions.setQuestionSequence(quesSequence);
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			qqQuestionsetQuestions.setDistrictSpecificQuestions(districtSpecificQuestion);
			qqQuestionsetQuestions.setQuestionSets(qqQuestionSets);
			qqQuestionsetQuestionsDAO.makePersistent(qqQuestionsetQuestions);

		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtSpecificQuestion;
	}
	
}
