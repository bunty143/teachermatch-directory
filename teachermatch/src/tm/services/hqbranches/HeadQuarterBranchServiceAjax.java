package tm.services.hqbranches;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.cgreport.JobCategoryWiseStatusPrivilege;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusSpecificQuestions;
import tm.bean.user.UserMaster;
import tm.dao.cgreport.JobCategoryWiseStatusPrivilegeDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusSpecificQuestionsDAO;

public class HeadQuarterBranchServiceAjax {
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private  HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private StatusSpecificQuestionsDAO statusSpecificQuestionsDAO;
	
	public SecondaryStatus createStatusFolder(int parentId,Integer headQuarterId,Integer branchId,String districtId,Integer jobCategory)
	{
		System.out.println(":::createStatusFolder::new::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		DistrictMaster districtMaster			=	null;
		SecondaryStatus secondaryStatus 			= 	new SecondaryStatus();
		JobCategoryMaster jobCategoryMaster=null;
		BranchMaster branchMaster=null;
		HeadQuarterMaster headQuarterMaster=null;
		
		if(districtId!=null && !districtId.equals(""))
			districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId), false, false);

		if(jobCategory!=null && jobCategory>0)
			jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategory, false, false);

		if(branchId!=null && branchId>0){
			branchMaster=branchMasterDAO.findById(branchId, false, false);
		}
		if(headQuarterId!=null && headQuarterId>0){
			headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
		}
		try 
		{
			SecondaryStatus secondaryStatusObj= secondaryStatusDAO.findById(parentId, false, false);
			String secondaryStatusName=secondaryStatusDAO.findSecondaryStatusExistForHeadBranch(headQuarterMaster,branchMaster,districtMaster,"New Status");

			if(secondaryStatusObj.getStatusNodeMaster()!=null){
				secondaryStatus.setUsermaster(userSession);
				if(districtMaster!=null){
					secondaryStatus.setDistrictMaster(districtMaster);
				}
				if(branchMaster!=null){
					secondaryStatus.setBranchMaster(branchMaster);
				}
				if(headQuarterMaster!=null){
					secondaryStatus.setHeadQuarterMaster(headQuarterMaster);
				}
				if(userSession.getEntityType()==3 && userSession.getSchoolId()!=null)
					secondaryStatus.setSchoolMaster(userSession.getSchoolId());

				secondaryStatus.setSecondaryStatusName(secondaryStatusName);
				secondaryStatus.setOrderNumber(11);
				secondaryStatus.setStatus("A");
				secondaryStatus.setSecondaryStatus(secondaryStatusObj);
				secondaryStatus.setCreatedDateTime(new Date());

				if(jobCategoryMaster!=null)
					secondaryStatus.setJobCategoryMaster(jobCategoryMaster);

				secondaryStatusDAO.makePersistent(secondaryStatus);

				// Start ... Add data for child relationship
				Map<SecondaryStatus, JobCategoryMaster> mapjc=new HashMap<SecondaryStatus, JobCategoryMaster>();
				boolean categoryFlag=false;
				if(userSession.getEntityType()==5){
					if((jobCategory==null || jobCategory==0 ) && branchMaster==null && districtMaster==null){
						categoryFlag=true;
					}
					else if((jobCategory==null || jobCategory==0 ) && branchMaster!=null){
						categoryFlag=true;
					}
				}else{
					if(jobCategory==null || jobCategory==0){
						categoryFlag=true;
					}
				}
				System.out.println("categoryFlag::::::::::::::::::::"+categoryFlag);
				if(categoryFlag){
					System.out.println("Inside Check:::::::::::::::::::::::::::::");
					mapjc=secondaryStatusDAO.findSSByDistrictPlusByMapHeadBranch(headQuarterMaster,branchMaster,districtMaster,secondaryStatusObj);
					SessionFactory sessionFactory=secondaryStatusDAO.getSessionFactory();
					StatelessSession statelesSsession = sessionFactory.openStatelessSession();
					Transaction txOpen =statelesSsession.beginTransaction();
					if(mapjc.size()>0){
						Iterator iter = mapjc.entrySet().iterator();
						while (iter.hasNext()) {
							Map.Entry mEntry = (Map.Entry) iter.next();

							SecondaryStatus ssp=null;
							JobCategoryMaster jobCategoryMaster2=null;
							if(mEntry.getKey()!=null)
								ssp=(SecondaryStatus)mEntry.getKey();

							if(mEntry.getValue()!=null)
								jobCategoryMaster2=(JobCategoryMaster)mEntry.getValue();

							SecondaryStatus secondaryStatus3=new SecondaryStatus();
							if(districtMaster!=null){
								secondaryStatus3.setDistrictMaster(districtMaster);
							}
							if(branchMaster!=null){
								secondaryStatus3.setBranchMaster(branchMaster);
							}
							if(headQuarterMaster!=null){
								secondaryStatus3.setHeadQuarterMaster(headQuarterMaster);
							}
							if(userSession.getEntityType()==3 && userSession.getSchoolId()!=null)
								secondaryStatus.setSchoolMaster(userSession.getSchoolId());
							secondaryStatus3.setJobCategoryMaster(jobCategoryMaster2);
							secondaryStatus3.setSecondaryStatusName(secondaryStatus.getSecondaryStatusName());
							secondaryStatus3.setOrderNumber(secondaryStatus.getOrderNumber());
							secondaryStatus3.setStatus(secondaryStatus.getStatus());
							secondaryStatus3.setSecondaryStatus(ssp);
							secondaryStatus3.setSecondaryStatus_copy(secondaryStatus);
							secondaryStatus3.setUsermaster(userSession);
							secondaryStatus3.setCreatedDateTime(secondaryStatus.getCreatedDateTime());
							statelesSsession.insert(secondaryStatus3);
						}
					}
					txOpen.commit();
					statelesSsession.close();
				}
				// End ... Add data for child relationship

			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return secondaryStatus;
	}
	@Transactional(readOnly=false)
	public int renameStatusFolder(int folderId,String folderName )
	{
		//System.out.println("folderId>>>>::::::::"+folderId+" == folderName =="+folderName);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		SecondaryStatus secondaryStatus = new SecondaryStatus();
		List<SecondaryStatus> secondaryStatusList= new ArrayList<SecondaryStatus>();
		try{
			secondaryStatus=secondaryStatusDAO.findById(folderId, false, false);
			if(secondaryStatus.getStatusNodeMaster()!=null || (secondaryStatus.getStatusMaster()!=null && secondaryStatus.getStatusMaster().getBanchmarkStatus()!=1)){
				return 0;
			}else{
				List<JobCategoryMaster> jobCategoryMasterList=new ArrayList<JobCategoryMaster>();
				Map<Integer,JobCategoryMaster> jCM= new HashMap<Integer, JobCategoryMaster>();

				List<JobCategoryWiseStatusPrivilege> jobCWSP=new ArrayList<JobCategoryWiseStatusPrivilege>();
				List<StatusSpecificQuestions> lstSSQuestions = new ArrayList<StatusSpecificQuestions>();

				if(secondaryStatus.getJobCategoryMaster()!=null){
					jobCWSP=jobCategoryWiseStatusPrivilegeDAO.getMaxJSIValueListBYHBD(secondaryStatus);
					lstSSQuestions=statusSpecificQuestionsDAO.findQuestionsByHBD(secondaryStatus);
					if(jobCWSP.size()>0 && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("jsi")){
						return 3;
					}else if(lstSSQuestions.size()>0 && !secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("jsi")){
						return 5;
					}
					jobCategoryMasterList=jobCategoryMasterDAO.findAssessmentDocumentByDistrictJobCategoryHeadBranch(secondaryStatus);
				}else{
					jobCategoryMasterList=jobCategoryMasterDAO.findAssessmentDocumentByDistrictJobCategoryHeadBranch(secondaryStatus);
				}
				if(jobCategoryMasterList.size()>0){
					for(JobCategoryMaster jc: jobCategoryMasterList){
						jCM.put(jc.getJobCategoryId(),jc);
					}
				}
				if(!folderName.equalsIgnoreCase("jsi") || (folderName.equalsIgnoreCase("jsi") && jobCategoryMasterList.size()>0)){
					secondaryStatusList=secondaryStatusDAO.getSecondaryStatusForRenameByHBD(secondaryStatus,folderName);
					System.out.println("secondaryStatusList Rename:::"+secondaryStatusList.size());
					if(secondaryStatusList.size()>0){
						return 1;
					}else{
						String strOldStatusName=secondaryStatus.getSecondaryStatusName();
						secondaryStatus.setSecondaryStatusName(folderName);
						secondaryStatusDAO.makePersistent(secondaryStatus);
						secondaryStatusDAO.updateChildNodes(jCM,secondaryStatus, folderName,strOldStatusName);
						System.out.println(" Rename Check::::::::::::");
						return 2;
					}
				}else{
					return 4;
				}
			}

		}catch (Exception e) {
			e.printStackTrace();
		}		
		return 2;
	}
	@Transactional(readOnly=false)
	public SecondaryStatus cutPasteFolder(int parentId,int dictkey,Integer headQuarterId,Integer branchId,String districtId,Integer jobCategory)
	{
		//System.out.println("cutPasteFolder   parentId::::::::"+parentId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}

		UserMaster userMaster	=	(UserMaster) session.getAttribute("userMaster");

		DistrictMaster districtMaster=null;
		if(userMaster.getEntityType()!=1)
			districtMaster=userMaster.getDistrictId();
		else if(districtId!=null && !districtId.equals(""))
			districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId), false, false);

		SecondaryStatus secondaryStatus = secondaryStatusDAO.findById(dictkey, false, false);
		String sourceParetnName=secondaryStatus.getSecondaryStatus().getSecondaryStatusName();
		try 
		{
			SecondaryStatus secondaryStatusObj= secondaryStatusDAO.findById(parentId, false, false);
			secondaryStatus.setSecondaryStatus(secondaryStatusObj);
			secondaryStatus.setCreatedDateTime(new Date());
			secondaryStatusDAO.makePersistent(secondaryStatus);

			// Start ... Add data for child relationship
			if(jobCategory==null || jobCategory==0)
				secondaryStatusDAO.cutAndPasteForJobCategory(secondaryStatusObj, secondaryStatus, jobCategory,districtMaster,sourceParetnName);
			// End ... Add data for child relationship

		}catch (Exception e) {
			e.printStackTrace();
		}

		return secondaryStatus;
	}
	public SecondaryStatus copyPasteFolder(int parentId, String title,Integer headQuarterId,Integer branchId,String districtId,Integer jobCategory)
	{
		//System.out.println("parentId::::::::"+parentId+" Title  "+title);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		UserMaster userMaster	=	(UserMaster) session.getAttribute("userMaster");
		SecondaryStatus secondaryStatus 			= 	new SecondaryStatus();


		DistrictMaster districtMaster=null;
		JobCategoryMaster jobCategoryMaster=null;

		if(userMaster.getEntityType()!=1)
			districtMaster=userMaster.getDistrictId();
		else if(districtId!=null && !districtId.equals(""))
			districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId), false, false);

		if(jobCategory!=null && jobCategory>0)
			jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategory, false, false);

		try 
		{

			secondaryStatus.setUsermaster(userMaster);
			secondaryStatus.setDistrictMaster(userMaster.getDistrictId());
			if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null)
			{
				secondaryStatus.setSchoolMaster(userMaster.getSchoolId());
			}
			SecondaryStatus secondaryStatusObj= secondaryStatusDAO.findById(parentId, false, false);
			String secondaryStatusName=secondaryStatusDAO.findSecondaryStatusExist(districtMaster,"Copy of "+title);

			secondaryStatus.setSecondaryStatusName(secondaryStatusName);
			secondaryStatus.setOrderNumber(11);
			secondaryStatus.setStatus("A");
			secondaryStatus.setSecondaryStatus(secondaryStatusObj);
			secondaryStatus.setCreatedDateTime(new Date());

			if(districtMaster!=null)
				secondaryStatus.setDistrictMaster(districtMaster);
			if(jobCategoryMaster!=null)
				secondaryStatus.setJobCategoryMaster(jobCategoryMaster);

			secondaryStatusDAO.makePersistent(secondaryStatus);

			// Start ... Add data for child relationship
			Map<SecondaryStatus, JobCategoryMaster> mapjc=new HashMap<SecondaryStatus, JobCategoryMaster>();
			if(jobCategory==null || jobCategory==0)
			{
				mapjc=secondaryStatusDAO.findSSByDistrictPlusByMap(districtMaster,secondaryStatusObj);
				/**** Session dynamic ****/
				SessionFactory sessionFactory=secondaryStatusDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				Transaction txOpen =statelesSsession.beginTransaction();
				if(mapjc.size()>0)
				{
					Iterator iter = mapjc.entrySet().iterator();
					while (iter.hasNext()) {


						Map.Entry mEntry = (Map.Entry) iter.next();

						SecondaryStatus ssp=null;
						JobCategoryMaster jobCategoryMaster2=null;
						if(mEntry.getKey()!=null)
							ssp=(SecondaryStatus)mEntry.getKey();

						if(mEntry.getValue()!=null)
							jobCategoryMaster2=(JobCategoryMaster)mEntry.getValue();

						SecondaryStatus secondaryStatus3=new SecondaryStatus();

						secondaryStatus3.setDistrictMaster(districtMaster);
						secondaryStatus3.setJobCategoryMaster(jobCategoryMaster2);
						secondaryStatus3.setSecondaryStatusName(secondaryStatusName);
						secondaryStatus3.setOrderNumber(11);
						secondaryStatus3.setStatus("A");
						//secondaryStatus3.setSecondaryStatus(secondaryStatusObj);
						secondaryStatus3.setCreatedDateTime(new Date());
						secondaryStatus3.setSecondaryStatus(ssp);
						secondaryStatus3.setSecondaryStatus_copy(secondaryStatus);
						secondaryStatus3.setUsermaster(userMaster);
						if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null)
							secondaryStatus3.setSchoolMaster(userMaster.getSchoolId());

						statelesSsession.insert(secondaryStatus3);
					}
				}
				txOpen.commit();
				statelesSsession.close();
			}
			// End ... Add data for child relationship

		}catch (Exception e) {
			e.printStackTrace();
		}		
		return secondaryStatus;
	}
	@Transactional(readOnly=false)
	public int deleteStatusFolder(int folderId)
	{
		//System.out.println("delete Folder Method: folderId::::::::"+folderId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		SecondaryStatus secondaryStatus = new SecondaryStatus();
		try 
		{
			secondaryStatus	= secondaryStatusDAO.findById(folderId, false, false);
			secondaryStatus.setStatus("I");
			try{
				secondaryStatusDAO.updatePersistent(secondaryStatus);
				secondaryStatusDAO.deleteChildNodes(secondaryStatus);
				return 1;
			}
			catch (Exception e)
			{
				return 2;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return 1;
	}

}

