package tm.services.hqbranches;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.HqBranchesDistricts;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.hqbranchesmaster.HqBranchesDistrictsDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.utility.Utility;

public class AutoSearchFilterAjax {
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;	
	public void setBranchMasterDAO(BranchMasterDAO branchMasterDAO) {
		this.branchMasterDAO = branchMasterDAO;
	}
	
	@Autowired
	private HqBranchesDistrictsDAO hqBranchesDistrictsDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
		
	public List<DistrictMaster> getFieldOfDistrictListByBranchAndHead(String DistrictName,int branchId,int headQuarterId)
	{
		/* ========  For Session time Out Error =========*/
		System.out.println("getFieldOfDistrictListByBranchAndHead ::: headQuarterId " +headQuarterId);
		/*WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}*/

		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
		Criterion criterion1 = Restrictions.eq("status", "A");
		try{
			
			if(branchId!=0)
			{
				System.out.println("branchId " +branchId);
				if(DistrictName.trim()!=null && DistrictName.trim().length()>0)
				{
					List<HqBranchesDistricts> hqBranchesDistrictsList = new ArrayList<HqBranchesDistricts>();
					BranchMaster branchMaster=branchMasterDAO.findById(branchId, false, false);
					Criterion criterion3=Restrictions.eq("branchMaster", branchMaster);
					hqBranchesDistrictsList=hqBranchesDistrictsDAO.findByCriteria(criterion3);
					if(hqBranchesDistrictsList!=null && hqBranchesDistrictsList.size()>0)
					{
						String dist = null;
						if(hqBranchesDistrictsList!=null && hqBranchesDistrictsList.size()>0)
						{
							if(hqBranchesDistrictsList.get(0)!=null){
								dist = hqBranchesDistrictsList.get(0).getDistrictId();
							}
						}
						String[] distIds = null;
						if(dist!=null && dist.length()>0)
							distIds = dist.split(",");
							
						List<Integer> districtArray =new ArrayList<Integer>();
						for(int i=0;i<distIds.length;i++)
						{
							districtArray.add(Integer.parseInt(distIds[i]));
						}
						Criterion criterion2 = Restrictions.in("districtId", districtArray);				
						List<DistrictMaster> districtMasters = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1,criterion2);
						
						Criterion criterion4 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
						fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion4,criterion1,criterion2);
						
						districtMasterList.addAll(districtMasters);
						districtMasterList.addAll(fieldOfDistrictList2);
						Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
						districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
					}
				}
			}
			else if(headQuarterId!=0 && headQuarterId!=2)
			{
				System.out.println("*** headQuarterId " +headQuarterId);
				if(DistrictName.trim()!=null && DistrictName.trim().length()>0)
				{
					List<HqBranchesDistricts> hqBranchesDistrictsList = new ArrayList<HqBranchesDistricts>();
					HeadQuarterMaster headQuarterMaster=null;
					headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
					Criterion criterion3=Restrictions.eq("headQuarterMaster", headQuarterMaster);
					hqBranchesDistrictsList=hqBranchesDistrictsDAO.findByCriteria(criterion3);
					if(hqBranchesDistrictsList!=null && hqBranchesDistrictsList.size()>0)
					{						
						String[] distIds = null;
						List<Integer> districtArray =new ArrayList<Integer>();
						if(hqBranchesDistrictsList!=null && hqBranchesDistrictsList.size()>0)
						{
							for (HqBranchesDistricts hqBranchesDistricts : hqBranchesDistrictsList) {
								if(hqBranchesDistricts.getDistrictId()!=null)
								{
									distIds = hqBranchesDistricts.getDistrictId().split(",");
									for(int i=0;i<distIds.length;i++)
									{
										districtArray.add(Integer.parseInt(distIds[i]));
									}
								}
							}							
						}											
						
						System.out.println(districtArray.size());
						Criterion criterion2 = Restrictions.in("districtId", districtArray);				
						List<DistrictMaster> districtMasters = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1,criterion2);
						
						Criterion criterion4 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
						fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion4,criterion1,criterion2);
						
						districtMasterList.addAll(districtMasters);
						districtMasterList.addAll(fieldOfDistrictList2);
						Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
						districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
					}
				}
			}
			else
			{
				
				if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
					fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);

					Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
					fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);

					districtMasterList.addAll(fieldOfDistrictList1);
					districtMasterList.addAll(fieldOfDistrictList2);
					Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
					districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));

				}else{
					fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion1);
					districtMasterList.addAll(fieldOfDistrictList1);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return districtMasterList;
	}
	
	public boolean getFieldOfDistrictListByBranch(int branchId)
	{
		/* ========  For Session time Out Error =========*/
		System.out.println(":::::::::::::::::::::::::getFieldOfDistrictListByBranch:::::::::::::::::::::::::");
		Criterion criterion1 = Restrictions.eq("status", "A");
		boolean branchFlag=false;
		try{
			if(branchId!=0){
				System.out.println("branchId " +branchId);
					List<HqBranchesDistricts> hqBranchesDistrictsList = new ArrayList<HqBranchesDistricts>();
					BranchMaster branchMaster=branchMasterDAO.findById(branchId, false, false);
					Criterion criterion3=Restrictions.eq("branchMaster", branchMaster);
					hqBranchesDistrictsList=hqBranchesDistrictsDAO.findByCriteria(criterion3);
					if(hqBranchesDistrictsList!=null && hqBranchesDistrictsList.size()>0){
						String dist = null;
						if(hqBranchesDistrictsList!=null && hqBranchesDistrictsList.size()>0){
							if(hqBranchesDistrictsList.get(0)!=null){
								dist = hqBranchesDistrictsList.get(0).getDistrictId();
							}
						}
						String[] distIds = null;
						if(dist!=null && dist.length()>0)
							distIds = dist.split(",");
							
						List<Integer> districtArray =new ArrayList<Integer>();
						if(distIds!=null){
							for(int i=0;i<distIds.length;i++)
							{
								districtArray.add(Integer.parseInt(distIds[i]));
							}
							Criterion criterion2 = Restrictions.in("districtId", districtArray);				
							List<DistrictMaster> districtMasters = districtMasterDAO.findByCriteria(criterion1,criterion2);
							if(districtMasters!=null && districtMasters.size()>0){
								branchFlag=true;
							}
						}
					}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return branchFlag;
	}
	@Transactional(readOnly=true)
	public String displayJobCategoryByDistrictBranchAndHead(Integer headQuarterId,Integer branchId,Integer districtId,String txtjobCategory)
	{
		System.out.println("::::::::::::::::::::::displayJobCategoryByDistrictBranchAndHead:::::::::::::::");
		StringBuffer buffer=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		BranchMaster branchMaster=null;
		HeadQuarterMaster headQuarterMaster=null;
		DistrictMaster districtMaster=null;
		Integer jobCategory=0;
		if(txtjobCategory!=null && !txtjobCategory.equals(""))
			jobCategory=Integer.parseInt(txtjobCategory);

		if(branchId!=null && branchId>0){
			branchMaster=branchMasterDAO.findById(branchId, false, false);
		}
		if(headQuarterId!=null && headQuarterId>0){
			headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
		}
		if(districtId!=null && districtId>0){
			districtMaster=districtMasterDAO.findById(districtId, false, false);
		}
		List<JobCategoryMaster> lstjobCategoryMasters=jobCategoryMasterDAO.findAllJobCategoryNameByDistrictBranchAndHead(headQuarterMaster,branchMaster,districtMaster);
		if(lstjobCategoryMasters.size()>0){
			buffer.append("<select class='form-control' name='jobCategory' id='jobCategory' onchange='resetJobCategory();'>");
			buffer.append("<option selected='selected' value=''>All the categories</option>");
			for(JobCategoryMaster pojo:lstjobCategoryMasters)
				if(pojo!=null)
				{
					if(pojo.getJobCategoryId().equals(jobCategory))
						buffer.append("<option value='"+pojo.getJobCategoryId()+"' selected='selected'>"+pojo.getJobCategoryName()+"</option>");
					else
						buffer.append("<option value='"+pojo.getJobCategoryId()+"'>"+pojo.getJobCategoryName()+"</option>");
				}
			buffer.append("</select>");
		}else{
			buffer.append("<select class='form-control' name='jobCategory' id='jobCategory' disabled='disabled' onchange='resetJobCategory();'>");
			buffer.append("<option selected='selected' value=''>All the categories</option>");
			buffer.append("</select>");
		}
		return buffer.toString();
	}
	
	
	public List<BranchMaster> getBranchListByHQ(String hqId,String branchName)
	{
		System.out.println("branchName "+branchName+" hqId=="+hqId);
		
		List<BranchMaster> branchMasterList =  new ArrayList<BranchMaster>();
		if(hqId!=null && !hqId.equals("") && !hqId.equals("0"))
		{
			System.out.println("hqId "+hqId);
			int iHqId=Utility.getIntValue(hqId);
			HeadQuarterMaster headQuarterMaster=headQuarterMasterDAO.findById(iHqId, false, false);
			Criterion criterionHQ = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			
			List<BranchMaster> fieldOfBranchList1 = null;
			List<BranchMaster> fieldOfBranchList2 = null;
			try{
				
				Criterion criterion = Restrictions.like("branchName", branchName.trim(),MatchMode.START);
				if(branchName.trim()!=null && branchName.trim().length()>0){
					fieldOfBranchList1 = branchMasterDAO.findWithLimit(Order.asc("branchName"), 0, 25, criterion,criterionHQ);
					
					Criterion criterion2 = Restrictions.ilike("branchName","% "+branchName.trim()+"%" );
					fieldOfBranchList2 = branchMasterDAO.findWithLimit(Order.asc("branchName"), 0, 25, criterion2,criterionHQ);
					
					branchMasterList.addAll(fieldOfBranchList1);
					branchMasterList.addAll(fieldOfBranchList2);
					Set<BranchMaster> setBranch = new LinkedHashSet<BranchMaster>(branchMasterList);
					branchMasterList = new ArrayList<BranchMaster>(new LinkedHashSet<BranchMaster>(setBranch));
					
				}else{
					fieldOfBranchList1 = branchMasterDAO.findWithLimit(Order.asc("branchName"), 0, 25);
					branchMasterList.addAll(fieldOfBranchList1);
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			System.out.println(" branchMasterList :: "+branchMasterList.size());
		}
		return branchMasterList;
	}
	
	public List<HeadQuarterMaster> getFieldOfHeadQuarterList(String headQuarterName)
	{
		/* ========  For Session time Out Error =getFieldOfBranchList========*/
		System.out.println("enter for Head quarter-----------------------------------------");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		List<HeadQuarterMaster> headQuarterMasterList =  new ArrayList<HeadQuarterMaster>();
		List<HeadQuarterMaster> fieldOfHQList1 = null;
		List<HeadQuarterMaster> fieldOfHQList2 = null;
		try{

			Criterion criterion = Restrictions.like("headQuarterName", headQuarterName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "A");
			if(headQuarterName.trim()!=null && headQuarterName.trim().length()>0){
				fieldOfHQList1 = headQuarterMasterDAO.findWithLimit(Order.asc("headQuarterName"), 0, 25, criterion,criterion1);

				Criterion criterion2 = Restrictions.ilike("headQuarterName","% "+headQuarterName.trim()+"%" );
				fieldOfHQList2 = headQuarterMasterDAO.findWithLimit(Order.asc("headQuarterName"), 0, 25, criterion2,criterion1);

				headQuarterMasterList.addAll(fieldOfHQList1);
				headQuarterMasterList.addAll(fieldOfHQList2);
				Set<HeadQuarterMaster> setDistrict = new LinkedHashSet<HeadQuarterMaster>(headQuarterMasterList);
				headQuarterMasterList = new ArrayList<HeadQuarterMaster>(new LinkedHashSet<HeadQuarterMaster>(setDistrict));
			}else{
				fieldOfHQList1 = headQuarterMasterDAO.findWithLimit(Order.asc("headQuarterName"), 0, 25,criterion1);
				headQuarterMasterList.addAll(fieldOfHQList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("headQuarterMasterList=========="+headQuarterMasterList.size());
		return headQuarterMasterList;
	}
	
	@Transactional(readOnly=true)
	public String displayJobCategoryByHeadQuarter(Integer headQuarterId,Integer branchId,Integer districtId,String txtjobCategory)
	{
		System.out.println("::::::::::::::::::::::displayJobCategoryByHead:::::::::::::::");
		StringBuffer buffer=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		BranchMaster branchMaster=null;
		HeadQuarterMaster headQuarterMaster=null;
		DistrictMaster districtMaster=null;
		Integer jobCategory=0;
		if(txtjobCategory!=null && !txtjobCategory.equals(""))
			jobCategory=Integer.parseInt(txtjobCategory);

		if(branchId!=null && branchId>0){
			branchMaster=branchMasterDAO.findById(branchId, false, false);
		}
		if(headQuarterId!=null && headQuarterId>0){
			headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
		}
		if(districtId!=null && districtId>0){
			districtMaster=districtMasterDAO.findById(districtId, false, false);
		}
		List<JobCategoryMaster> lstjobCategoryMasters=jobCategoryMasterDAO.findAllJobCategoryNameByDistrictBranchAndHead(headQuarterMaster,branchMaster,districtMaster);
		if(lstjobCategoryMasters.size()>0){
			buffer.append("<select class='form-control' name='jobCategory' id='jobCategory'>");
			buffer.append("<option selected='selected' value=''>All the categories</option>");
			for(JobCategoryMaster pojo:lstjobCategoryMasters)
				if(pojo!=null)
				{
					if(pojo.getJobCategoryId().equals(jobCategory))
						buffer.append("<option value='"+pojo.getJobCategoryId()+"' selected='selected'>"+pojo.getJobCategoryName()+"</option>");
					else
						buffer.append("<option value='"+pojo.getJobCategoryId()+"'>"+pojo.getJobCategoryName()+"</option>");
				}
			buffer.append("</select>");
		}else{
			buffer.append("<select class='form-control' name='jobCategory' id='jobCategory' disabled='disabled'>");
			buffer.append("<option selected='selected' value=''>All the categories</option>");
			buffer.append("</select>");
		}
		return buffer.toString();
	}
	
	public List<BranchMaster> getActiveBranchListByHQ(String hqId,String branchName)
	{
		System.out.println("branchName "+branchName+" hqId=="+hqId);
		
		List<BranchMaster> branchMasterList =  new ArrayList<BranchMaster>();
		if(hqId!=null && !hqId.equals("") && !hqId.equals("0"))
		{
			System.out.println("hqId "+hqId);
			int iHqId=Utility.getIntValue(hqId);
			HeadQuarterMaster headQuarterMaster=headQuarterMasterDAO.findById(iHqId, false, false);
			Criterion criterionHQ = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterionStatus = Restrictions.eq("status","A");
			
			List<BranchMaster> fieldOfBranchList1 = null;
			List<BranchMaster> fieldOfBranchList2 = null;
			try{
				
				Criterion criterion = Restrictions.like("branchName", branchName.trim(),MatchMode.START);
				if(branchName.trim()!=null && branchName.trim().length()>0){
					fieldOfBranchList1 = branchMasterDAO.findWithLimit(Order.asc("branchName"), 0, 25, criterion,criterionHQ,criterionStatus);
					
					Criterion criterion2 = Restrictions.ilike("branchName","% "+branchName.trim()+"%" );
					fieldOfBranchList2 = branchMasterDAO.findWithLimit(Order.asc("branchName"), 0, 25, criterion2,criterionHQ,criterionStatus);
					
					branchMasterList.addAll(fieldOfBranchList1);
					branchMasterList.addAll(fieldOfBranchList2);
					Set<BranchMaster> setBranch = new LinkedHashSet<BranchMaster>(branchMasterList);
					branchMasterList = new ArrayList<BranchMaster>(new LinkedHashSet<BranchMaster>(setBranch));
					
				}else{
					fieldOfBranchList1 = branchMasterDAO.findWithLimit(Order.asc("branchName"), 0, 25,criterionStatus);
					branchMasterList.addAll(fieldOfBranchList1);
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			System.out.println(" active branchMasterList :: "+branchMasterList.size());
		}
		return branchMasterList;
	}
}

