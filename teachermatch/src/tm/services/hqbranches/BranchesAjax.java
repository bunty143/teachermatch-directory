package tm.services.hqbranches;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.HqBranchesDistricts;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.hqbranchesmaster.HqBranchesDistrictsDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.services.PaginationAndSorting;
import tm.utility.ImageResize;
import tm.utility.Utility;

public class BranchesAjax {
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;	
	public void setBranchMasterDAO(BranchMasterDAO branchMasterDAO) {
		this.branchMasterDAO = branchMasterDAO;
	}
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) 
	{
		this.districtKeyContactDAO = districtKeyContactDAO;
	}
	
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
 	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) 
 	{
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}
 	
 	@Autowired
	private UserMasterDAO usermasterdao;
	public void setUsermasterdao(UserMasterDAO usermasterdao) 
	{
		this.usermasterdao = usermasterdao;
	}
	
	@Autowired
	private  TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) 
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private  RoleMasterDAO roleMasterDAO;
	public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO) 
	{
		this.roleMasterDAO = roleMasterDAO;
	}
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	
	@Autowired
	private HqBranchesDistrictsDAO hqBranchesDistrictsDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	public String showFile(String branch,int branchId,String docFileName)
	{
		System.out.println(" Inside Branch Ajax showfile() :: ");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		String path="";
		String source="";
		String target="";
		try 
		{
			if(branch.equalsIgnoreCase("branch")){
				source = Utility.getValueOfPropByKey("branchRootPath")+branchId+"/"+docFileName;
				target = context.getServletContext().getRealPath("/")+"/"+"/branch/"+branchId+"/";
			}else{
				source = Utility.getValueOfPropByKey("branchRootPath")+branchId+"/"+docFileName;
				target = context.getServletContext().getRealPath("/")+"/"+"/branch/"+branchId+"/";
			}
		    File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        System.out.println(" Inside Branch Ajax showfile() :: sourceFile.getName() ::"+sourceFile.getName());
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        FileUtils.copyFile(sourceFile, targetFile);
	        
	        String ext = null;
	       
	        String s = sourceFile.getName();
	        int i = s.lastIndexOf('.');

	        if (i > 0 &&  i < s.length() - 1) {
	            ext = s.substring(i+1).toLowerCase();
	            System.out.println(" ext of file :: "+ext);
	        }
	        
	        if(ext.equals("jpeg") || ext.equals("png") || ext.equals("gif") || ext.equals("jpg")){
	        	ImageResize.resizeImage(targetDir+"/"+sourceFile.getName());
	        }
        
	        if(branch.equalsIgnoreCase("branch")){
	        	  path = Utility.getValueOfPropByKey("contextBasePath")+"/branch/"+branchId+"/"+docFileName; 	
	        	  System.out.println(" branch path :: "+path);
	        	  
	        }else{
	        	  path = Utility.getValueOfPropByKey("contextBasePath")+"/branch/"+branchId+"/"+docFileName;
	        }
		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
		}
		
		return path;
	}
	
	public String getBranchesByHeadQuarter(int HeadQuarterId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		
		
		return "";
	}

	
	public List<BranchMaster> getFieldOfBranchList(String branchName,int hqId)
	{
		/* ========  For Session time Out Error =========*/
		//WebContext context;
		//context = WebContextFactory.get();
		//HttpServletRequest request = context.getHttpServletRequest();
		//HttpSession session = request.getSession(false);
		/*if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }*/
		List<BranchMaster> branchMasterList =  new ArrayList<BranchMaster>();
		List<BranchMaster> fieldOfBranchList1 = null;
		List<BranchMaster> fieldOfBranchList2 = null;
		try{
			if(hqId!=0)
			{
				HeadQuarterMaster headQuarterMaster = headQuarterMasterDAO.findById(hqId, false, false);
				Criterion criterionH = Restrictions.eq("headQuarterId", headQuarterMaster);
				Criterion criterion = Restrictions.like("branchName", branchName.trim(),MatchMode.START);
				if(branchName.trim()!=null && branchName.trim().length()>0){
					fieldOfBranchList1 = branchMasterDAO.findWithLimit(Order.asc("branchName"), 0, 25, criterion,criterionH);

					Criterion criterion2 = Restrictions.ilike("branchName","% "+branchName.trim()+"%" );
					fieldOfBranchList2 = branchMasterDAO.findWithLimit(Order.asc("branchName"), 0, 25, criterion2,criterionH);

					branchMasterList.addAll(fieldOfBranchList1);
					branchMasterList.addAll(fieldOfBranchList2);
					Set<BranchMaster> setBranch = new LinkedHashSet<BranchMaster>(branchMasterList);
					branchMasterList = new ArrayList<BranchMaster>(new LinkedHashSet<BranchMaster>(setBranch));

				}else{
					fieldOfBranchList1 = branchMasterDAO.findWithLimit(Order.asc("branchName"), 0, 25);
					branchMasterList.addAll(fieldOfBranchList1);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println(" branchMasterList :: "+branchMasterList.size());
		return branchMasterList;
	}
	
	
	public int saveKeyContact(int updateUser, Integer keyContactId,Integer headQuarterId,Integer branchId,Integer keyContactTypeId,String keyContactFirstName,String keyContactLastName,String keyContactEmailAddress,String keyContactPhoneNumber,String keyContactTitle,String keyContactIdVal)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		DistrictKeyContact districtKeyContact	=	new DistrictKeyContact();
		int emailCheck=0;
		int keyContactTypeIdcheck=0;
		int flagVal=0;
		if(keyContactId	!=	null){	
			flagVal=1;
		}
		HeadQuarterMaster headQuarterMaster = null;
		BranchMaster branchMaster = null;
		Boolean branchFlag = false;
		System.out.println(" keyContactTypeId :: "+keyContactTypeId);
		if(branchId!=0 && headQuarterId==0)
		{
			branchFlag = true;
			branchMaster=branchMasterDAO.findById(branchId, false, false);
			emailCheck=districtKeyContactDAO.checkEmailBranchMaster(flagVal,keyContactId,keyContactEmailAddress,branchMaster);
		}
		else if(branchId!=0 && headQuarterId!=0)
		{
			branchFlag = true;
			headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
			branchMaster=branchMasterDAO.findById(branchId, false, false);			
			emailCheck=districtKeyContactDAO.checkEmailBranchMaster(flagVal,keyContactId,keyContactEmailAddress,branchMaster);
		}
		else
		{
			headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);			
			emailCheck=districtKeyContactDAO.checkEmailHeadQuarterMaster(flagVal,keyContactId,keyContactEmailAddress,headQuarterMaster);
		}
		
		try
		{
			ContactTypeMaster contactType  = contactTypeMasterDAO.findById(keyContactTypeId, false, false);
			if(branchFlag)
				keyContactTypeIdcheck=districtKeyContactDAO.checkkeyContactTypeBranchMaster(contactType,branchMaster);
			else
				keyContactTypeIdcheck=districtKeyContactDAO.checkkeyContactTypeHeadQuarterMaster(contactType,headQuarterMaster);	
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return 2;
		}
		if(emailCheck>=1 && keyContactTypeIdcheck>=1  && keyContactIdVal==null){
			return 4;
		}		
		String prevEmail="";
		int userId=0;
		try
		{
			if(keyContactId	!=	null){	
				prevEmail=districtKeyContactDAO.findEmail(keyContactId);
				userId = usermasterdao.getUserId(prevEmail);
				districtKeyContact.setKeyContactId(keyContactId);
			}			
			districtKeyContact.setHeadQuarterMaster(headQuarterMaster);
			districtKeyContact.setBranchMaster(branchMaster);			
			ContactTypeMaster contactTypeMaster		=	contactTypeMasterDAO.findById(keyContactTypeId, false, false);		
			districtKeyContact.setKeyContactTypeId(contactTypeMaster);
			districtKeyContact.setKeyContactFirstName(keyContactFirstName);
			districtKeyContact.setKeyContactLastName(keyContactLastName);
			districtKeyContact.setKeyContactEmailAddress(keyContactEmailAddress);
			districtKeyContact.setKeyContactPhoneNumber(keyContactPhoneNumber);
			districtKeyContact.setKeyContactTitle(keyContactTitle);
			
			/*======= Check For Unique User Email address ==========*/
			List<UserMaster> dupUserEmail =	usermasterdao.checkDuplicateUserEmail(userId,keyContactEmailAddress);
			int size =	dupUserEmail.size();
			
			List<TeacherDetail> dupteacherEmail	= teacherDetailDAO.findByEmail(keyContactEmailAddress);
			int sizeTeacherList = dupteacherEmail.size();		
			if(sizeTeacherList>0)
				return 3;
		
			UserMaster master=null;
			int insertusermaster=0;
			if((size==0 && sizeTeacherList==0 && updateUser==2) || (userId==0 && size==0 && sizeTeacherList==0 && updateUser==1)){
				int verificationCode=(int) Math.round(Math.random() * 2000000);
				UserMaster userSession = (UserMaster) session.getAttribute("userMaster");
				UserMaster userMaster =	new UserMaster();
				if(branchMaster!=null)
				{
					userMaster.setEntityType(6);  // branch	
				}
				else
				{
					userMaster.setEntityType(5);  // headQuarter
				}
				
				Utility utility	=	new Utility();
				String authenticationCode	=	utility.getUniqueCodebyId((1));
				userMaster.setPassword(MD5Encryption.toMD5("teachermatch"));
				userMaster.setAuthenticationCode(authenticationCode);			
				
				userMaster.setTitle(keyContactTitle);
				userMaster.setBranchMaster(branchMaster);
				userMaster.setHeadQuarterMaster(headQuarterMaster);
				userMaster.setFirstName(keyContactFirstName);
				userMaster.setLastName(keyContactLastName);
				userMaster.setPhoneNumber(keyContactPhoneNumber);
				userMaster.setEmailAddress(keyContactEmailAddress);
				RoleMaster roleMaster = roleMasterDAO.findById(2, false, false);			
				userMaster.setRoleId(roleMaster);
				userMaster.setStatus("A");
				userMaster.setVerificationCode(""+verificationCode);
				userMaster.setForgetCounter(0);
				userMaster.setIsQuestCandidate(false);
				userMaster.setCreatedDateTime(new Date());			   
				usermasterdao.makePersistent(userMaster);
				master=userMaster;
				insertusermaster=1;
				emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), "I Have Added You As a User",MailText.createUserPwdMailToOtUser(request,userMaster));
			
			}else if((size!=0 || sizeTeacherList!=0)&& updateUser==1){	
				return 3;
			}else if(size==0 && sizeTeacherList==0 && updateUser==1){	
			   DistrictKeyContact districtKeyContact2 = districtKeyContactDAO.findById(keyContactId, false, false);			
			   master=new UserMaster(); 
			   master.setUserId(districtKeyContact2.getUserMaster().getUserId());
			   districtKeyContact.setUserMaster(master);
			    districtKeyContactDAO.makePersistent(districtKeyContact);
				if(updateUser==1){
					UserMaster userMaster =	usermasterdao.findById(userId, false, false);
					userMaster.setTitle(keyContactTitle);
					userMaster.setFirstName(keyContactFirstName);
					userMaster.setLastName(keyContactLastName);
					userMaster.setPhoneNumber(keyContactPhoneNumber);
					userMaster.setEmailAddress(keyContactEmailAddress);
					userMaster.setForgetCounter(0);
					
					if((userMaster.getBranchMaster()!=null && userMaster.getBranchMaster().equals(branchMaster)) || userMaster.getHeadQuarterMaster().equals(headQuarterMaster))	
					{
						usermasterdao.makePersistent(userMaster);						
					}			
						
				}
			}
			if(updateUser==2){				
				if(insertusermaster==0)
				{	
					master=new UserMaster();
					int userid=usermasterdao.getUserId(keyContactEmailAddress);				
					master.setUserId(userid);					
				}
				districtKeyContact.setUserMaster(master);
				districtKeyContactDAO.makePersistent(districtKeyContact);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}
        
		return 1;
	}
	
	public String displayKeyContactGrid(int branchId,int headQuarterId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,134,"editbranch.do",100);
			}catch(Exception e){
				e.printStackTrace();
			}	
			BranchMaster branchMaster = null;
			HeadQuarterMaster headQuarterMaster = null;
			Criterion criterion1 =null;
			if(branchId!=0 && headQuarterId!=0)
			{
				branchMaster = branchMasterDAO.findById(branchId, false, false);
				criterion1 = Restrictions.eq("branchMaster",branchMaster);
			}
			else if(headQuarterId!=0)
			{
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
				criterion1 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			}
			
			List<DistrictKeyContact> districtKeyContact	  	=	new ArrayList<DistrictKeyContact>();
		    try {
				districtKeyContact = districtKeyContactDAO.findByCriteria(Order.asc("keyContactFirstName"),criterion1);
			} catch (Exception e) {
				//e.printStackTrace();
			}
		
			//dmRecords.append("<div id='keyContactTable' class='span12' ><br/>");
			dmRecords.append("<table border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			dmRecords.append("<th width='20%'>Contact Type</th>");
			dmRecords.append("<th width='20%'>Contact Name</th>");
			dmRecords.append("<th width='15%'>Email</th>");
			dmRecords.append("<th width='15%'>Phone</th>");
			dmRecords.append("<th width='15%'>Title</th>");
			dmRecords.append("<th width='15%'>Actions</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(districtKeyContact.size()==0)
			{
				dmRecords.append("<tr><td colspan='6'>No Contact found</td></tr>" );
			}
			else
			{
				for (DistrictKeyContact districtKeyContactDetail : districtKeyContact) 
				{
					dmRecords.append("<tr>" );
					dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactTypeId().getContactType()+"</td>");
					dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactFirstName()+" "+districtKeyContactDetail.getKeyContactLastName()+"</td>");
					dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactEmailAddress()+"</td>");
					dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactPhoneNumber()+"</td>");
					dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactTitle()+"</td>");
					dmRecords.append("<td>");
					if(roleAccess.indexOf("|2|")!=-1){
						dmRecords.append("<a href='javascript:void(0);' onclick='return beforeEditKeyContact("+districtKeyContactDetail.getKeyContactId()+")'>Edit</a>");
					}else{
						dmRecords.append("&nbsp;");
					}
					if(roleAccess.indexOf("|3|")!=-1){
						dmRecords.append("| <a href='javascript:void(0);' onclick='return deleteKeyContact("+districtKeyContactDetail.getKeyContactId()+")'>Delete</a>");
					}
					dmRecords.append("</td>");
				}				
			}
			dmRecords.append("</table>");
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	@Transactional(readOnly=false)
	public boolean deleteKeyContact(Integer keyContactId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		try{
			DistrictKeyContact districtKeyContact	=	null;
			districtKeyContact		=	districtKeyContactDAO.findById(keyContactId, false, false);			
			districtKeyContactDAO.makeTransient(districtKeyContact);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/*======== Branch's User Functionality ============*/
	/*======== It will add branch's user (Administrator and Analyst) in usermaster Table and check duplicate user email address in teacherdetail and usermaster table ============*/
	public int saveBranchAdministratorOrAnalyst(int headQuarterId,int branchId,String emailAddress,String firstName,String lastName,int salutation,int entitytype,String title,String phoneNumber,String mobileNumber,String authCode,int roleId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer tmRecords =	new StringBuffer();
		try{
			int userId							 =	0;
			int entityType 						 =	2;
			BranchMaster branchMaster			 =	null;
			HeadQuarterMaster headQuarterMaster	 =	null;
			/*======= Check For Unique User Email address ==========*/
			List<UserMaster> dupUserEmail				=	usermasterdao.checkDuplicateUserEmail(userId,emailAddress);
			int size 									=	dupUserEmail.size();
			if(size>0)
				return 3;	/*=== it return 3 if find duplicate email address from usermaster Table=======*/
			
			List<TeacherDetail> dupteacherEmail			=	teacherDetailDAO.findByEmail(emailAddress);
			int sizeTeacherList 						=	dupteacherEmail.size();
			if(sizeTeacherList>0)
				return 4;	/*=== it return 4 if find duplicate email address from teacherdetail Table=======*/
			
			int verificationCode=(int) Math.round(Math.random() * 2000000);
			
			UserMaster userSession									=	(UserMaster) session.getAttribute("userMaster");
			UserMaster userMaster									=	new UserMaster();
			
			
			Utility utility											=	new Utility();
			String authenticationCode								=	utility.getUniqueCodebyId((userId+1));
			if(userId!=0)
			{	
				userMaster.setUserId(userId);
				UserMaster userPwd		=	usermasterdao.findById(userId, false, false);
				userMaster.setPassword(userPwd.getPassword());
				userMaster.setAuthenticationCode(userPwd.getAuthenticationCode());
			}
			else
			{
				userMaster.setPassword(MD5Encryption.toMD5("teachermatch"));
				userMaster.setAuthenticationCode(authenticationCode);
		    }
			
			if(branchId!=0 && headQuarterId!=0)
			{
				headQuarterMaster       =   headQuarterMasterDAO.findById(headQuarterId, false, false);
				branchMaster			=	branchMasterDAO.findById(branchId, false, false);
				userMaster.setEntityType(6);
			}
			else
			{
				headQuarterMaster       =   headQuarterMasterDAO.findById(headQuarterId, false, false);
				userMaster.setEntityType(5);
			}			
			
			userMaster.setTitle(title);
			userMaster.setHeadQuarterMaster(headQuarterMaster);
			userMaster.setBranchMaster(branchMaster);
			userMaster.setFirstName(firstName);
			userMaster.setLastName(lastName);
			userMaster.setSalutation(salutation);
			userMaster.setMobileNumber(mobileNumber);
			userMaster.setPhoneNumber(phoneNumber);
			userMaster.setEmailAddress(emailAddress);
			RoleMaster roleMaster = roleMasterDAO.findById(roleId, false, false);			
			userMaster.setRoleId(roleMaster);
			userMaster.setStatus("A");
			userMaster.setVerificationCode(""+verificationCode);
			userMaster.setForgetCounter(0);
			userMaster.setIsQuestCandidate(false);
			userMaster.setCreatedDateTime(new Date());
		
			usermasterdao.makePersistent(userMaster);
			emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), "I Have Added You As a User",MailText.createUserPwdMailToOtUser(request,userMaster));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return 2;
		}
		return 1;
	}
	
	
	public String displayBranchAdministratorGrid(int headQuarterId,int branchId,int roleId)
	{
		/** ========  For Session time Out Error ========= **/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			
			int roleIdForAccess		=	0;
			int tooltipCounter		=	0;
			String actDeactivateUser=	null;
			if(roleId==11)
			{
				actDeactivateUser	=	"actDeactivateUserAdministrator";
			}
			if(roleId==13)
			{
				actDeactivateUser	=	"actDeactivateUserAnalyst";
			}
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleIdForAccess=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleIdForAccess,134,"editbranch.do",100);
				System.out.println(" roleAccess :: "+roleAccess);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			HeadQuarterMaster headQuarterMaster 	=	null;
			BranchMaster branchMaster		=	null;
			if(branchId!=0 && headQuarterId!=0)
			{
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
				branchMaster = branchMasterDAO.findById(branchId, false, false);				
			}
			else
			{
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
			}
			
			RoleMaster roleMaster									=	null;
			roleMaster												=	roleMasterDAO.findById(roleId, false, false);
			
			Criterion criterion =null;			
			if(branchMaster!=null)
			{
				criterion =	Restrictions.eq("branchMaster",branchMaster);
			}
			else
			{
				criterion =	Restrictions.eq("headQuarterMaster",headQuarterMaster);
			}
			Criterion criterion1		=	Restrictions.eq("roleId",roleMaster);
			List<UserMaster> userMaster	=	usermasterdao.findByCriteria(criterion,criterion1);
			if(userMaster.size()<1)
			{
				if(roleId==11)
				{
					dmRecords.append("<div class='col-sm-3 col-md-3'>No Administrator Found</div>");
				}
				if(roleId==13)
				{
					dmRecords.append("<div class='col-sm-3 col-md-3'>No Analyst Found</div>");
				}
			}
		
			for (UserMaster userMasterDetail : userMaster) 
			{
				if(userMasterDetail.getStatus().equalsIgnoreCase("A"))
				{
					tooltipCounter++;
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+userMasterDetail.getFirstName()+" "+userMasterDetail.getLastName());
					if(roleAccess.indexOf("|3|")!=-1 && roleIdForAccess!=11){	
						dmRecords.append("  <a data-original-title='Deactivate' rel='tooltip' id='"+actDeactivateUser+""+tooltipCounter+"' href='javascript:void(0);' onclick=\"return activateDeactivateDistrictAdministratorOrAnalyst("+userMasterDetail.getRoleId().getRoleId()+","+userMasterDetail.getUserId()+",'I');\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a>");
					}
					dmRecords.append("</div>");
				}
				else
				{
					tooltipCounter++;
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+userMasterDetail.getFirstName()+" "+userMasterDetail.getLastName());
					if(roleAccess.indexOf("|3|")!=-1  && roleIdForAccess!=11){	
						dmRecords.append("  <a data-original-title='Activate' rel='tooltip' id='"+actDeactivateUser+""+tooltipCounter+"' href='javascript:void(0);' onclick=\"return activateDeactivateDistrictAdministratorOrAnalyst("+userMasterDetail.getRoleId().getRoleId()+","+userMasterDetail.getUserId()+",'A');\"><img width='15' height='15' class='can' src='images/option02.png' alt=''></a>");
					}
					dmRecords.append("</div>");
				}
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	

	@Transactional(readOnly=false)
	public boolean deleteHqBranchesDistrict(Integer hqBranchDistrictId,String districtId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		String districtIds = "";
		String str = "";
		try
		{
			HqBranchesDistricts hqBranchesDistricts = null;
			if(hqBranchDistrictId!=0)
			{
				hqBranchesDistricts = hqBranchesDistrictsDAO.findById(hqBranchDistrictId, false, false);
				if(hqBranchesDistricts!=null && hqBranchesDistricts.getDistrictId()!=null)
				{
					districtIds = hqBranchesDistricts.getDistrictId();
					if(districtIds.contains(","))
					{
						String[] newDistrictIds = districtIds.split(",");
						StringBuffer sb = new StringBuffer();
						for(String s : newDistrictIds)
						{
							if(!s.equalsIgnoreCase(districtId))
							{
								sb.append(str).append(s);
								str = ",";
							}
						}
						hqBranchesDistricts.setDistrictId(sb.toString());
					}
					else if(districtIds.contains(districtId))
					{
						hqBranchesDistricts.setDistrictId(null);
					}
					hqBranchesDistrictsDAO.updatePersistent(hqBranchesDistricts);
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Transactional(readOnly=false)
	public String displayHqBranchesDistrictGrid(int branchId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,134,"editbranch.do",100);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<HqBranchesDistricts> hqBranchesDistricts	=	null;
			BranchMaster branchMaster = null;
			if(branchId!=0)			
			{
				branchMaster = branchMasterDAO.findById(branchId, false, false);
				Criterion criterion = Restrictions.eq("branchMaster", branchMaster);
				hqBranchesDistricts = hqBranchesDistrictsDAO.findByCriteria(criterion);
			}
			
			List<HqBranchesDistricts> sortedlst		=	new ArrayList<HqBranchesDistricts>();
			
			SortedMap<String,HqBranchesDistricts>	sortedMap = new TreeMap<String,HqBranchesDistricts>();
			
			int mapFlag=2;		
			
			totalRecord =sortedlst.size();

			if(totalRecord<end)
				end=totalRecord;
			List<HqBranchesDistricts> lstsortedlst		=	sortedlst.subList(start,end);
			
			dmRecords.append("<table id='districtSchoolsTable' border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			//responseText=PaginationAndSorting.responseSortingLink("Branch Name",sortOrderFieldName,"branchName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='40%' valign='top'>District Name</th>");
			
			//responseText=PaginationAndSorting.responseSortingLink("Added On",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='25%' valign='top'>Added On</th>");
			
			//responseText=PaginationAndSorting.responseSortingLink("Added By",sortOrderFieldName,"addedBy",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='20%' valign='top'>Added By</th>");
			
			dmRecords.append("<th width='15%'>Actions</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			List<Integer> distIntIds = new ArrayList<Integer>();
			if(hqBranchesDistricts==null || hqBranchesDistricts.size()==0 || hqBranchesDistricts.get(0).getDistrictId()==null || hqBranchesDistricts.get(0).getDistrictId().equals(""))
			{
				dmRecords.append("<tr><td colspan='6'>No District found</td></tr>" );
			}
			else if(hqBranchesDistricts.get(0).getDistrictId()!=null && hqBranchesDistricts.get(0).getDistrictId().length()>0)
			{
				String dIds = hqBranchesDistricts.get(0).getDistrictId();
				List<DistrictMaster> districtMasters = null;
				DistrictMaster districtMaster = null;
				if(dIds!=null && dIds.length()>0 && dIds.contains(","))
				{
					String[] districtIds = dIds.split(",");
					for(String s : districtIds)
					{
						distIntIds.add(Integer.parseInt(s));
					}
					try {
						Criterion criterion = Restrictions.in("districtId", distIntIds);					
						districtMasters = districtMasterDAO.findByCriteria(Order.asc("districtName"),criterion);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else
				{
					try {
						Criterion criterion = Restrictions.eq("districtId", Integer.parseInt(dIds));					
						districtMasters = districtMasterDAO.findByCriteria(Order.asc("districtName"),criterion);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				if(hqBranchesDistricts!=null && hqBranchesDistricts.size()>0)
				{
					for (DistrictMaster dMaster : districtMasters) 
					{
						dmRecords.append("<tr>" );
						dmRecords.append("<td>"+dMaster.getDistrictName()+"</td>");
						dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(hqBranchesDistricts.get(0).getCreatedDateTime())+"</td>");
						dmRecords.append("<td>"+hqBranchesDistricts.get(0).getUserMaster().getFirstName()+" "+hqBranchesDistricts.get(0).getUserMaster().getLastName()+"</td>");
						dmRecords.append("<td>");
						if(roleAccess.indexOf("|3|")!=-1){
							dmRecords.append("<a href='javascript:void(0);' onclick=\"return deleteHqBranchDistricts("+hqBranchesDistricts.get(0).getHqBranchDistrictId()+",'"+dMaster.getDistrictId()+"')\">Remove</a>");
						}else{
							dmRecords.append("&nbsp;");
						}
						dmRecords.append("</td>");
						dmRecords.append("</tr>");
					}
				}
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForDistriceAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	@Transactional(readOnly=false)
	public int getBranchJob(Integer branchId)
	{
		System.out.println("getBranchJob ::: branchId == "+branchId);
		Integer jobId = null;
		try{
			BranchMaster branchMaster = branchMasterDAO.findById(branchId, false, false);
			List<JobOrder> jobOrderList1 = jobOrderDAO.getActiveBranchJobs(branchMaster,true);
			//List<JobOrder> jobOrderList2 = jobOrderDAO.getAllBranchesJobs();
			if(jobOrderList1!=null && jobOrderList1.size()>0)
				jobId = jobOrderList1.get(0).getJobId();
			//else if(jobOrderList2!=null && jobOrderList2.size()>0)
			//	jobId = jobOrderList2.get(0).getJobId();
			if(jobId!=null)
				return jobId;
			else if(jobId==null)
				return 0;
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	public String getBranchDetails(JobOrder jobOrder){
		String returnVal="";
		StringBuffer kesData=new StringBuffer();
		try{
			if(jobOrder!=null){
				BranchMaster branchMaster=jobOrder.getBranchMaster();
				if(branchMaster!=null){
					kesData.append("<br/>Branch Number : "+branchMaster.getBranchCode());
					kesData.append("<br/>Branch Display Name : "+branchMaster.getBranchName());
					/*String branchContact1="";
					if(branchMaster.getContactNumber1()!=null){
						branchContact1=branchMaster.getContactNumber1();
					}
					kesData.append("<br/>Branch Contact 1 : "+branchContact1);
					String branchContact2="";
					if(branchMaster.getContactNumber2()!=null){
						branchContact2=branchMaster.getContactNumber2();
					}
					kesData.append("<br/>Branch Contact 2 : "+branchContact2);*/
					kesData.append("<br/>Branch Phone Number : "+branchMaster.getPhoneNumber());
					kesData.append("<br/>Email Address : "+branchMaster.getBranchEmailAddress());
					returnVal=kesData.toString();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return returnVal;
	}
}
