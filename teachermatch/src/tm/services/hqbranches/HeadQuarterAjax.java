package tm.services.hqbranches;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.UserLoginHistory;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.HqBranchesDistricts;
import tm.bean.hqbranchesmaster.TempDistricts;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.TimeZoneMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.assessment.AssessmentSectionDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.hqbranchesmaster.HqBranchesDistrictsDAO;
import tm.dao.hqbranchesmaster.TempDistrictsDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.logs.CreateLog;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.services.PaginationAndSorting;
import tm.services.es.ElasticSearchService;
import tm.utility.DistrictNameCompratorASC;
import tm.utility.DistrictNameCompratorDESC;
import tm.utility.ElasticSearchConfig;
import tm.utility.IPAddressUtility;
import tm.utility.ImageResize;
import tm.utility.Utility;

public class HeadQuarterAjax {
	String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;	
	public void setBranchMasterDAO(BranchMasterDAO branchMasterDAO) {
		this.branchMasterDAO = branchMasterDAO;
	}
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
		
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) 
	{
		this.districtKeyContactDAO = districtKeyContactDAO;
	}
	
	@Autowired
	private UserMasterDAO usermasterdao;
	public void setUsermasterdao(UserMasterDAO usermasterdao) 
	{
		this.usermasterdao = usermasterdao;
	}
	
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
 	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) 
 	{
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}
 	
 	@Autowired
	private  TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) 
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired
	private  RoleMasterDAO roleMasterDAO;
	public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO) 
	{
		this.roleMasterDAO = roleMasterDAO;
	}
	
	@Autowired
	private HqBranchesDistrictsDAO hqBranchesDistrictsDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private TempDistrictsDAO tempDistrictsDAO;

	@Autowired
	private JobOrderDAO jobOrderDAO;
	
 	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

 	@Autowired
 	private SchoolInJobOrderDAO schoolInJobOrderDAO;
 	
 	@Autowired
 	private JobCategoryMasterDAO jobCategoryMasterDAO;

 	@Autowired
 	private AssessmentDetailDAO assessmentDetailDAO;
 	
 	@Autowired
 	private AssessmentJobRelationDAO assessmentJobRelationDAO;
 	
 	@Autowired
	private AssessmentSectionDAO assessmentSectionDAO;
 	
 	@Autowired
 	private StateMasterDAO stateMasterDAO;
 	
 	@Autowired
 	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
 	
 	@Autowired
 	private JobForTeacherDAO jobForTeacherDAO;
 	
 	@Autowired
 	private StatusMasterDAO statusMasterDAO;
 	
 	@Autowired
 	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
 	
 	@Autowired
 	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
 	
 	@Autowired
 	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
 	
 	
 	@Autowired
 	private CreateLog createLog;
 	/*@Autowired
 	private JobOrderLogDAO jobOrderLogDAO;
 	
 	
	public void setJobOrderLogDAO(JobOrderLogDAO jobOrderLogDAO) {
		this.jobOrderLogDAO = jobOrderLogDAO;
	}*/



	public String SearchHeadQuarterRecords(int entityID,String headQuarterName,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		System.out.println(" headQuarterName ::: "+headQuarterName);
		HeadQuarterMaster headDao = null;
		StringBuffer tmRecords =	new StringBuffer();
		try{
			
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord 		=	0;
			//------------------------------------

			
			UserMaster userMaster = null;
			int headQuarterId =0;
			int entityIDLogin=0;
			int roleId=0;

			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getHeadQuarterMaster()!=null){
					
					headQuarterId =userMaster.getHeadQuarterMaster().getHeadQuarterId();
				}
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				entityIDLogin=userMaster.getEntityType();
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,98,"manageheadquarter.do",100);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(headQuarterId!=0 && entityIDLogin==5){
				entityID=entityIDLogin;
			}
		
			List<HeadQuarterMaster> headQuarterMasters	  =	new ArrayList<HeadQuarterMaster>();
			
			/** set default sorting fieldName **/
			String sortOrderFieldName="headQuarterName";
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				sortOrderFieldName=sortOrder;
			}
			String sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
			if(sortOrderType!=null)
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("1")){
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}

			/**End ------------------------------------**/
			
			if(entityID==1 || headQuarterId==0){/*
					if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
						Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
						totaRecord			= districtMasterDAO.getRowCountWithSort(sortOrderStrVal,criterion);
						branchMaster  	= districtMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion);
					}else{
						totaRecord			= districtMasterDAO.getRowCountWithSort(sortOrderStrVal);
						districtMaster  	= districtMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage);
					}
			*/}else{
				
				System.out.println(" headQuarterId :: "+headQuarterId);
				 headDao = headQuarterMasterDAO.findById(headQuarterId, false, false);
				 Criterion criterion= Restrictions.eq("headQuarterId", headQuarterId);
				 totaRecord		= headQuarterMasterDAO.getRowCountWithSort(sortOrderStrVal,criterion);
				 headQuarterMasters  	= headQuarterMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion);
			}
				
			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("HeadQuarter Name",sortOrderFieldName,"headQuarterName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='250' valign='top'>"+responseText+"</th>");
		
			responseText=PaginationAndSorting.responseSortingLink("State",sortOrderFieldName,"stateId",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			//responseText=PaginationAndSorting.responseSortingLink("Final Decision Maker",sortOrderFieldName,"dmName",sortOrderTypeVal,pgNo);
			//tmRecords.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("# of Branches",sortOrderFieldName,"totalNoOfBranches",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='20' valign='top'> "+responseText+"</th>");
			
			//responseText=PaginationAndSorting.responseSortingLink("# of Students",sortOrderFieldName,"totalNoOfStudents",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='20' valign='top'> City Name</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Created On",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='12%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Status",sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			tmRecords.append(" <th width='50' valign='top'>"+responseText+"</th>");
			tmRecords.append(" <th  width='15%' valign='top'>Actions</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");

			if(headQuarterMasters.size()==0)
				tmRecords.append("<tr><td colspan='8' align='center'>No HeadQuarter found</td></tr>" );
			
			System.out.println("No of Records "+headQuarterMasters.size());

			for (HeadQuarterMaster headQuarterMaster : headQuarterMasters) 
			{
				tmRecords.append("<tr>");
				tmRecords.append("<td  >"+headQuarterMaster.getHeadQuarterName()+"</td>");
				tmRecords.append("<td>"+headQuarterMaster.getStateId().getStateName()+"</td>");
				//tmRecords.append("<td>Ankit Sharma</td>");
				tmRecords.append("<td><span style='margin-left:35px;'>"+headQuarterMaster.getTotalNoOfBranches()+"</span></td>");
				tmRecords.append("<td>"+headQuarterMaster.getCityName()+"</td>");
				//tmRecords.append("<td>N/A</td>");
				tmRecords.append("<td nowrap>"+Utility.convertDateAndTimeToUSformatOnlyDate(headQuarterMaster.getCreatedDateTime())+"</td>");
				tmRecords.append("<td>");
				if(headQuarterMaster.getStatus().equalsIgnoreCase("A"))
					tmRecords.append("Active  ");
				else
					tmRecords.append("Inactive");
				tmRecords.append("</td>");
				tmRecords.append("<td nowrap>");
				
					boolean pipeFlag=false;
					if(roleAccess.indexOf("|2|")!=-1){
					
						tmRecords.append("<a href='editheadquarter.do?hqId="+headQuarterMaster.getHeadQuarterId()+"'>Edit</a>");
						pipeFlag=true;
					}else if(roleAccess.indexOf("|4|")!=-1){
						tmRecords.append("<a href='editheadquarter.do?hqId="+headQuarterMaster.getHeadQuarterId()+"'>View</a>");
						pipeFlag=true;
					} 
					if(roleAccess.indexOf("|7|")!=-1 && (entityIDLogin==1)){
						if(pipeFlag)tmRecords.append(" | ");
						if(headQuarterMaster.getStatus().equalsIgnoreCase("A"))
							tmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateHeadQuarter("+headQuarterMaster.getHeadQuarterId()+",'I')\">Deactivate");
						else
							tmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateHeadQuarter("+headQuarterMaster.getHeadQuarterId()+",'A')\">Activate");
					}else{
						tmRecords.append("&nbsp;");
					}
				tmRecords.append("</td>");
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForManageJobOrdersAjax(request,totaRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
	
	
	
	public String SearchBranchRecords(int entityID,String DistrictName,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String branchId,String headQuarterHiddenId,String status_filter)
	{
		System.out.println("::::::::::::::::SearchBranchRecords::::::::::::::");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		HeadQuarterMaster headQuarterMaster = null;
		StringBuffer tmRecords =	new StringBuffer();
		int tempBranchId=0;
		List<BranchMaster> branchMasterList=new ArrayList<BranchMaster>();
		try{
			//shadab
			if(branchId!=null && !branchId.trim().equals(""))
				tempBranchId=Integer.parseInt(branchId);
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord 		=	0;
			//------------------------------------
			BranchMaster branchMaster=null;
			UserMaster userMaster = null;
			int headQuarterId =0;
			int entityIDLogin=0;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()==null){					
					headQuarterId =userMaster.getHeadQuarterMaster().getHeadQuarterId();
				}
 				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				entityIDLogin=userMaster.getEntityType();
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,99,"managebranches.do",100);
			}catch(Exception e){
				e.printStackTrace();
			}
			if(headQuarterId!=0 && entityIDLogin==5){
				entityID=entityIDLogin;
			}
			/** set default sorting fieldName **/
			String sortOrderFieldName="branchName";
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				sortOrderFieldName=sortOrder;
			}
			String sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
			if(sortOrderType!=null)
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("1")){
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}
			/**End ------------------------------------**/
			if(entityIDLogin==5)
			{
				if(headQuarterHiddenId!=null && !headQuarterHiddenId.trim().equals(""))
				{
				//	headQuarterId=Integer.parseInt(headQuarterHiddenId);
				}
			}
			if(headQuarterId!=0){
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
			}
			List<BranchMaster> totalBranches=null;
			if(entityID!=5){		
				tempBranchId=userMaster.getBranchMaster().getBranchId();
				headQuarterId=userMaster.getHeadQuarterMaster().getHeadQuarterId();	
				headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
				totalBranches = branchMasterDAO.getBranchesByHeadQuarterForBA(headQuarterMaster,tempBranchId);	
				branchMasterList = branchMasterDAO.findBranchesWithHeadQuarterForBA(headQuarterMaster,tempBranchId, sortOrderStrVal, start, noOfRowInPage);
			}else{
			 totalBranches = branchMasterDAO.getBranchesByHeadQuarter(headQuarterMaster,status_filter);
			 branchMasterList = branchMasterDAO.findBranchesWithHeadQuarter(headQuarterMaster,tempBranchId, sortOrderStrVal, start, noOfRowInPage,status_filter);
			}
			if(branchId!=null && !branchId.trim().equals(""))
				totaRecord = branchMasterList.size();
			else
				totaRecord = totalBranches.size();
				System.out.println(" total branch Record :: "+totaRecord);
				
			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("Branch Name",sortOrderFieldName,"branchName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='250' valign='top'>"+responseText+"</th>");
		
			responseText=PaginationAndSorting.responseSortingLink("State",sortOrderFieldName,"stateId",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Final Decision Maker",sortOrderFieldName,"dmName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("# of Districts",sortOrderFieldName,"totalNoOfDistricts",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='20' valign='top'> "+responseText+"</th>");
			
			//responseText=PaginationAndSorting.responseSortingLink("# of Students",sortOrderFieldName,"totalNoOfStudents",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='20' valign='top'> City Name</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("# of Schools",sortOrderFieldName,"totalNoOfSchools",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  width='20' valign='top'> "+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Created On",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='12%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Status",sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			tmRecords.append(" <th width='50' valign='top'>"+responseText+"</th>");
			tmRecords.append(" <th  width='15%' valign='top'>Actions</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");

			if(branchMasterList==null || branchMasterList.size()==0)
			{
				tmRecords.append("<tr><td colspan='8' align='center'>No Branch found</td></tr>" );
			}
			else
			{
				for (BranchMaster branch : branchMasterList) 
				{
					tmRecords.append("<tr>");
					tmRecords.append("<td  >"+branch.getBranchName()+"</td>");
					tmRecords.append("<td>"+branch.getStateMaster().getStateName()+"</td>");
					tmRecords.append("<td>"+branch.getHeadQuarterMaster().getHeadQuarterName()+"</td>");
					tmRecords.append("<td>"+branch.getTotalNoOfDistricts()+"</td>");
					tmRecords.append("<td>"+branch.getCityName()+"</td>");
					tmRecords.append("<td>N/A</td>");
					tmRecords.append("<td nowrap>"+Utility.convertDateAndTimeToUSformatOnlyDate(branch.getCreatedDateTime())+"</td>");
					tmRecords.append("<td>");
					if(branch.getStatus().equalsIgnoreCase("A"))
						tmRecords.append("Active  ");
					else
						tmRecords.append("Inactive");
					tmRecords.append("</td>");
					tmRecords.append("<td nowrap>");

					boolean pipeFlag=false;
					if(roleAccess.indexOf("|2|")!=-1){

						tmRecords.append("<a href='editbranch.do?bnchId="+branch.getBranchId()+"'>Edit</a>");
						pipeFlag=true;
					}else if(roleAccess.indexOf("|4|")!=-1){
						tmRecords.append("<a href='editbranch.do?bnchId="+branch.getBranchId()+"'>View</a>");
						pipeFlag=true;
					} 
					if(roleAccess.indexOf("|7|")!=-1 && (entityIDLogin==1 || entityIDLogin==5)){
						if(pipeFlag)tmRecords.append(" | ");
						if(branch.getStatus().equalsIgnoreCase("A"))
							tmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateBranch("+branch.getBranchId()+",'I')\">Deactivate");
						else
							tmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateBranch("+branch.getBranchId()+",'A')\">Activate");
					}else{
						tmRecords.append("&nbsp;");
					}
					tmRecords.append("</td>");
				}
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totaRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
	
	public boolean activateDeactivateBranch(int branchId,String status)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }else{
	    	userMaster=(UserMaster)session.getAttribute("userMaster");
	    }
		try{
			BranchMaster branchMaster = branchMasterDAO.findById(branchId, false, false);
			branchMaster.setStatus(status);
			branchMasterDAO.updatePersistent(branchMaster);
			//--------------------------
			
			
			String messageSubject="";
			String messageSend="";
			SimpleDateFormat dformat=new SimpleDateFormat("HH:MM:SS");
			Date d=new Date();
		
			String branchName=branchMaster.getBranchName();		
			
			if(status.equals("I"))
			{
				messageSubject="Branch Deactivated";
				messageSend="Dear TMAdmin,<br><br>" +branchName+
						" was deactivated by " + userMaster.getFirstName()+" "+userMaster.getLastName()
						+" at "+dformat.format(d)+". Please verify with "+userMaster.getFirstName()+" "+userMaster.getLastName()+
						"("+userMaster.getEmailAddress()+") "+
						"that this is an intended action.Thank you.";
			}
			if(status.equals("A"))
				{
				messageSubject="Branch Activated";
				messageSend="Dear TMAdmin,<br><br>" +branchName+
						" was activated by " + userMaster.getFirstName()+" "+userMaster.getLastName()
						+" at "+dformat.format(d)+". Please verify with "+userMaster.getFirstName()+" "+userMaster.getLastName()+
						"("+userMaster.getEmailAddress()+") "+
						"that this is an intended action.Thank you.";
				}
			System.out.println(messageSend);
				String content="";
				
				Properties  properties = new Properties();  
				try {
					InputStream inputStream = new Utility().getClass().getClassLoader().getResourceAsStream("smtpmail.properties");
					properties.load(inputStream);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			try {				
				
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom(properties.getProperty("smtphost.noreplyusername"));
				dsmt.setMailto(properties.getProperty("smtphost.tmsysadmin"));
				dsmt.setMailsubject(messageSubject);
				content=MailText.messageForDefaultFont(messageSend, userMaster);					
				dsmt.setMailcontent(content);
				
				System.out.println("content     "+content);
				try {
					dsmt.start();	
				} catch (Exception e) {}
			} catch (Exception e) {
				e.printStackTrace();
			}
			//--------------------------
			try{
				UserLoginHistory userLoginHistory = new UserLoginHistory();
				userLoginHistory.setUserMaster(userMaster);
				userLoginHistory.setSessionId(session.getId());
				userLoginHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
				userLoginHistory.setLogTime(new Date());
				if(branchMaster!=null)
					userLoginHistory.setBranchMaster(branchMaster);
				if(status.equalsIgnoreCase("A")){
					userLoginHistory.setLogType("Activate Branch");
				}else{
					userLoginHistory.setLogType("Deactivate Branch");
				}
				userLoginHistoryDAO.makePersistent(userLoginHistory);
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}catch (Exception e) 
		{
			e.printStackTrace();
			return true;
		}
		System.out.println("complete");
		return true;
	}
	
	public String showFile(String headQuarter,int headQuarterId,String docFileName)
	{
		System.out.println(" Inside HeadQuarter Ajax showfile() :: ");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		String path="";
		String source="";
		String target="";
		try 
		{
			if(headQuarter.equalsIgnoreCase("headQuarter")){
				source = Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterId+"/"+docFileName;
				target = context.getServletContext().getRealPath("/")+"/"+"/headquarter/"+headQuarterId+"/";
			}else{
				source = Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterId+"/"+docFileName;
				target = context.getServletContext().getRealPath("/")+"/"+"/headquarter/"+headQuarterId+"/";
			}
		    File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        System.out.println(" Inside HeadQuarter Ajax showfile() :: sourceFile.getName() ::"+sourceFile.getName());
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        FileUtils.copyFile(sourceFile, targetFile);
	        
	        String ext = null;
	       
	        String s = sourceFile.getName();
	        int i = s.lastIndexOf('.');

	        if (i > 0 &&  i < s.length() - 1) {
	            ext = s.substring(i+1).toLowerCase();
	            System.out.println(" ext of file :: "+ext);
	        }
	        
	        if(ext.equals("jpeg") || ext.equals("png") || ext.equals("gif") || ext.equals("jpg")){
	        	ImageResize.resizeImage(targetDir+"/"+sourceFile.getName());
	        }
        
	        if(headQuarter.equalsIgnoreCase("headQuarter")){
	        	  path = Utility.getValueOfPropByKey("contextBasePath")+"/headquarter/"+headQuarterId+"/"+docFileName; 	
	        	  System.out.println(" headquarter path :: "+path);
	        	  
	        }else{
	        	  path = Utility.getValueOfPropByKey("contextBasePath")+"/headquarter/"+headQuarterId+"/"+docFileName;
	        }
		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
		}
		
		return path;
	}
	
	
	@Transactional(readOnly=false)
	public int saveKeyContact(int updateUser, Integer keyContactId,Integer headQuarterId,Integer keyContactTypeId,String keyContactFirstName,String keyContactLastName,String keyContactEmailAddress,String keyContactPhoneNumber,String keyContactTitle,String keyContactIdVal)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		DistrictKeyContact districtKeyContact	=	new DistrictKeyContact();
		int emailCheck=0;
		int keyContactTypeIdcheck=0;
		int flagVal=0;
		if(keyContactId	!=	null){	
			flagVal=1;
		}
		HeadQuarterMaster headQuarterMaster = null;
		BranchMaster branchMaster = null;
		Boolean branchFlag = false;
		System.out.println(" keyContactTypeId :: "+keyContactTypeId);
		if(headQuarterId!=0)
		{
			headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);			
			emailCheck=districtKeyContactDAO.checkEmailHeadQuarterMaster(flagVal,keyContactId,keyContactEmailAddress,headQuarterMaster);
		}
		
		try
		{
			ContactTypeMaster contactType  = contactTypeMasterDAO.findById(keyContactTypeId, false, false);
			if(branchFlag)
				keyContactTypeIdcheck=districtKeyContactDAO.checkkeyContactTypeBranchMaster(contactType,branchMaster);
			else
				keyContactTypeIdcheck=districtKeyContactDAO.checkkeyContactTypeHeadQuarterMaster(contactType,headQuarterMaster);	
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return 2;
		}
		if(emailCheck>=1 && keyContactTypeIdcheck>=1  && keyContactIdVal==null){
			return 4;
		}		
		String prevEmail="";
		int userId=0;
		try
		{
			if(keyContactId	!=	null){	
				prevEmail=districtKeyContactDAO.findEmail(keyContactId);
				userId = usermasterdao.getUserId(prevEmail);
				districtKeyContact.setKeyContactId(keyContactId);
			}			
			districtKeyContact.setHeadQuarterMaster(headQuarterMaster);
			//districtKeyContact.setBranchMaster(branchMaster);			
			ContactTypeMaster contactTypeMaster		=	contactTypeMasterDAO.findById(keyContactTypeId, false, false);		
			districtKeyContact.setKeyContactTypeId(contactTypeMaster);
			districtKeyContact.setKeyContactFirstName(keyContactFirstName);
			districtKeyContact.setKeyContactLastName(keyContactLastName);
			districtKeyContact.setKeyContactEmailAddress(keyContactEmailAddress);
			districtKeyContact.setKeyContactPhoneNumber(keyContactPhoneNumber);
			districtKeyContact.setKeyContactTitle(keyContactTitle);
			
			/*======= Check For Unique User Email address ==========*/
			List<UserMaster> dupUserEmail =	usermasterdao.checkDuplicateUserEmail(userId,keyContactEmailAddress);
			int size =	dupUserEmail.size();
			
			List<TeacherDetail> dupteacherEmail	= teacherDetailDAO.findByEmail(keyContactEmailAddress);
			int sizeTeacherList = dupteacherEmail.size();		
			if(sizeTeacherList>0)
				return 3;
		
			UserMaster master=null;
			int insertusermaster=0;
			if((size==0 && sizeTeacherList==0 && updateUser==2) || (userId==0 && size==0 && sizeTeacherList==0 && updateUser==1)){
				int verificationCode=(int) Math.round(Math.random() * 2000000);
				UserMaster userSession = (UserMaster) session.getAttribute("userMaster");
				UserMaster userMaster =	new UserMaster();
				if(branchMaster!=null)
				{
					userMaster.setEntityType(6);  // branch	
				}
				else
				{
					userMaster.setEntityType(5);  // headQuarter
				}
				
				Utility utility	=	new Utility();
				String authenticationCode	=	utility.getUniqueCodebyId((1));
				userMaster.setPassword(MD5Encryption.toMD5("teachermatch"));
				userMaster.setAuthenticationCode(authenticationCode);			
				
				userMaster.setTitle(keyContactTitle);
				//userMaster.setBranchMaster(branchMaster);
				userMaster.setHeadQuarterMaster(headQuarterMaster);
				userMaster.setFirstName(keyContactFirstName);
				userMaster.setLastName(keyContactLastName);
				userMaster.setPhoneNumber(keyContactPhoneNumber);
				userMaster.setEmailAddress(keyContactEmailAddress);
				RoleMaster roleMaster = roleMasterDAO.findById(2, false, false);			
				userMaster.setRoleId(roleMaster);
				userMaster.setStatus("A");
				userMaster.setVerificationCode(""+verificationCode);
				userMaster.setForgetCounter(0);
				userMaster.setIsQuestCandidate(false);
				userMaster.setCreatedDateTime(new Date());			   
				usermasterdao.makePersistent(userMaster);
				master=userMaster;
				insertusermaster=1;
				emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), "I Have Added You As a User",MailText.createUserPwdMailToOtUser(request,userMaster));
			
			}else if((size!=0 || sizeTeacherList!=0)&& updateUser==1){	
				return 3;
			}else if(size==0 && sizeTeacherList==0 && updateUser==1){	
			   DistrictKeyContact districtKeyContact2 = districtKeyContactDAO.findById(keyContactId, false, false);			
			   master=new UserMaster(); 
			   master.setUserId(districtKeyContact2.getUserMaster().getUserId());
			   districtKeyContact.setUserMaster(master);
			    districtKeyContactDAO.makePersistent(districtKeyContact);
				if(updateUser==1){
					UserMaster userMaster =	usermasterdao.findById(userId, false, false);
					userMaster.setTitle(keyContactTitle);
					userMaster.setFirstName(keyContactFirstName);
					userMaster.setLastName(keyContactLastName);
					userMaster.setPhoneNumber(keyContactPhoneNumber);
					userMaster.setEmailAddress(keyContactEmailAddress);
					userMaster.setForgetCounter(0);
					
					if((userMaster.getBranchMaster()!=null && userMaster.getBranchMaster().equals(branchMaster)) || userMaster.getHeadQuarterMaster().equals(headQuarterMaster))	
					{
						usermasterdao.makePersistent(userMaster);						
					}			
						
				}
			}
			if(updateUser==2){				
				if(insertusermaster==0)
				{	
					master=new UserMaster();
					int userid=usermasterdao.getUserId(keyContactEmailAddress);				
					master.setUserId(userid);					
				}
				districtKeyContact.setUserMaster(master);
				districtKeyContactDAO.makePersistent(districtKeyContact);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}
        
		return 1;
	}
	
	@Transactional(readOnly=false)
	public String displayKeyContactGrid(int headQuarterId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,133,"editheadquarter.do",100);
			}catch(Exception e){
				e.printStackTrace();
			}	
			BranchMaster branchMaster = null;
			HeadQuarterMaster headQuarterMaster = null;
			Criterion criterion1 =null;
			Criterion criterion2 = null;
			if(headQuarterId!=0)
			{
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
				criterion1 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
				criterion2 = Restrictions.isNull("branchMaster");
			}
			
			List<DistrictKeyContact> districtKeyContact	  	=	null;
		    try {
				districtKeyContact = districtKeyContactDAO.findByCriteria(Order.asc("keyContactFirstName"),criterion1,criterion2);
			} catch (Exception e) {
				//e.printStackTrace();
			}
		
			//dmRecords.append("<div id='keyContactTable' class='span12' ><br/>");
			dmRecords.append("<table border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			dmRecords.append("<th width='20%'>Contact Type</th>");
			dmRecords.append("<th width='20%'>Contact Name</th>");
			dmRecords.append("<th width='15%'>Email</th>");
			dmRecords.append("<th width='15%'>Phone</th>");
			dmRecords.append("<th width='15%'>Title</th>");
			dmRecords.append("<th width='15%'>Actions</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(districtKeyContact==null || districtKeyContact.size()==0)
			{
				dmRecords.append("<tr><td colspan='6'>No Contact found</td></tr>" );
			}
			else
			{
				for (DistrictKeyContact districtKeyContactDetail : districtKeyContact) 
				{
					dmRecords.append("<tr>" );
					dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactTypeId().getContactType()+"</td>");
					dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactFirstName()+" "+districtKeyContactDetail.getKeyContactLastName()+"</td>");
					dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactEmailAddress()+"</td>");
					dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactPhoneNumber()+"</td>");
					dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactTitle()+"</td>");
					dmRecords.append("<td>");
					if(roleAccess.indexOf("|2|")!=-1){
						dmRecords.append("<a href='javascript:void(0);' onclick='return beforeEditKeyContact("+districtKeyContactDetail.getKeyContactId()+")'>Edit</a>");
					}else{
						dmRecords.append("&nbsp;");
					}
					if(roleAccess.indexOf("|3|")!=-1){
						dmRecords.append("| <a href='javascript:void(0);' onclick='return deleteKeyContact("+districtKeyContactDetail.getKeyContactId()+")'>Delete</a>");
					}
					dmRecords.append("</td>");
					dmRecords.append("</tr>" );
				}
				
			}
			dmRecords.append("</table>");
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	@Transactional(readOnly=false)
	public boolean deleteKeyContact(Integer keyContactId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		try{
			DistrictKeyContact districtKeyContact	=	null;
			districtKeyContact		=	districtKeyContactDAO.findById(keyContactId, false, false);			
			districtKeyContactDAO.makeTransient(districtKeyContact);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/*======== HeadQuarter's User Functionality ============*/
	/*======== It will add headQuarter's user (Administrator and Analyst) in usermaster Table and check duplicate user email address in teacherdetail and usermaster table ============*/
	@Transactional(readOnly=false)
	public int saveHeadQuarterAdministratorOrAnalyst(int headQuarterId,String emailAddress,String firstName,String lastName,int salutation,int entitytype,String title,String phoneNumber,String mobileNumber,String authCode,int roleId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer tmRecords =	new StringBuffer();
		try{
			int userId							 =	0;
			//int entityType 						 =	2;
			BranchMaster branchMaster			 =	null;
			HeadQuarterMaster headQuarterMaster	 =	null;
			/*======= Check For Unique User Email address ==========*/
			List<UserMaster> dupUserEmail				=	usermasterdao.checkDuplicateUserEmail(userId,emailAddress);
			int size 									=	dupUserEmail.size();
			if(size>0)
				return 3;	/*=== it return 3 if find duplicate email address from usermaster Table=======*/
			
			List<TeacherDetail> dupteacherEmail			=	teacherDetailDAO.findByEmail(emailAddress);
			int sizeTeacherList 						=	dupteacherEmail.size();
			if(sizeTeacherList>0)
				return 4;	/*=== it return 4 if find duplicate email address from teacherdetail Table=======*/
			
			int verificationCode=(int) Math.round(Math.random() * 2000000);
			
			UserMaster userSession			=	(UserMaster) session.getAttribute("userMaster");
			UserMaster userMaster			=	new UserMaster();
			
			
			Utility utility				=	new Utility();
			String authenticationCode	=	utility.getUniqueCodebyId((userId+1));
			if(userId!=0)
			{	
				userMaster.setUserId(userId);
				UserMaster userPwd		=	usermasterdao.findById(userId, false, false);
				userMaster.setPassword(userPwd.getPassword());
				userMaster.setAuthenticationCode(userPwd.getAuthenticationCode());
			}
			else
			{
				userMaster.setPassword(MD5Encryption.toMD5("teachermatch"));
				userMaster.setAuthenticationCode(authenticationCode);
		    }
			
			if(headQuarterId!=0)			
			{
				headQuarterMaster       =   headQuarterMasterDAO.findById(headQuarterId, false, false);
				userMaster.setEntityType(5);
			}			
			
			userMaster.setTitle(title);
			userMaster.setHeadQuarterMaster(headQuarterMaster);
			userMaster.setBranchMaster(branchMaster);
			userMaster.setFirstName(firstName);
			userMaster.setLastName(lastName);
			userMaster.setSalutation(salutation);
			userMaster.setMobileNumber(mobileNumber);
			userMaster.setPhoneNumber(phoneNumber);
			userMaster.setEmailAddress(emailAddress);
			RoleMaster roleMaster = roleMasterDAO.findById(roleId, false, false);			
			userMaster.setRoleId(roleMaster);
			userMaster.setStatus("A");
			userMaster.setVerificationCode(""+verificationCode);
			userMaster.setForgetCounter(0);
			userMaster.setIsQuestCandidate(false);
			userMaster.setCreatedDateTime(new Date());
		
			usermasterdao.makePersistent(userMaster);
			emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), "I Have Added You As a User",MailText.createUserPwdMailToOtUser(request,userMaster));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return 2;
		}
		return 1;
	}
	
	@Transactional(readOnly=false)
	public String displayHeadQuarterAdministratorGrid(int headQuarterId,int roleId)
	{
		/** ========  For Session time Out Error ========= **/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			
			int roleIdForAccess		=	0;
			int tooltipCounter		=	0;
			String actDeactivateUser=	null;
			if(roleId==10 || roleId==11)
			{
				actDeactivateUser	=	"actDeactivateUserAdministrator";
			}
			if(roleId==12 || roleId == 13)
			{
				actDeactivateUser	=	"actDeactivateUserAnalyst";
			}
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleIdForAccess=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleIdForAccess,133,"editheadquarter.do",100);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			HeadQuarterMaster headQuarterMaster 	=	null;
			BranchMaster branchMaster		=	null;
			if(headQuarterId!=0)			
			{
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
			}
			
			RoleMaster roleMaster		=	null;
			roleMaster					=	roleMasterDAO.findById(roleId, false, false);
			
			Criterion criterion =null;			
			if(branchMaster!=null)
			{
				criterion =	Restrictions.eq("branchMaster",branchMaster);
			}
			else
			{
				criterion =	Restrictions.eq("headQuarterMaster",headQuarterMaster);
			}
			Criterion criterion1		=	Restrictions.eq("roleId",roleMaster);
			Criterion criterion2		=	Restrictions.isNull("branchMaster");
			List<UserMaster> userMaster	=	usermasterdao.findByCriteria(criterion,criterion1);
			if(userMaster.size()==0)
			{
				if(roleId==10 || roleId==11)
				{
					dmRecords.append("<div class='col-sm-3 col-md-3'>No Administrator Found</div>");
				}
				if(roleId==12 || roleId==13)
				{
					dmRecords.append("<div class='col-sm-3 col-md-3'>No Analyst Found</div>");
				}
			}
		
			for (UserMaster userMasterDetail : userMaster) 
			{
				if(userMasterDetail.getStatus().equalsIgnoreCase("A"))
				{
					tooltipCounter++;
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+userMasterDetail.getFirstName()+" "+userMasterDetail.getLastName());
					if(roleAccess.indexOf("|3|")!=-1){	
						dmRecords.append("  <a data-original-title='Deactivate' rel='tooltip' id='"+actDeactivateUser+""+tooltipCounter+"' href='javascript:void(0);' onclick=\"return activateDeactivateDistrictAdministratorOrAnalyst("+userMasterDetail.getRoleId().getRoleId()+","+userMasterDetail.getUserId()+",'I');\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a>");
					}
					dmRecords.append("</div>");
				}
				else
				{
					tooltipCounter++;
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+userMasterDetail.getFirstName()+" "+userMasterDetail.getLastName());
					if(roleAccess.indexOf("|3|")!=-1){	
						dmRecords.append("  <a data-original-title='Activate' rel='tooltip' id='"+actDeactivateUser+""+tooltipCounter+"' href='javascript:void(0);' onclick=\"return activateDeactivateDistrictAdministratorOrAnalyst("+userMasterDetail.getRoleId().getRoleId()+","+userMasterDetail.getUserId()+",'A');\"><img width='15' height='15' class='can' src='images/option02.png' alt=''></a>");
					}
					dmRecords.append("</div>");
				}
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	@Transactional(readOnly=false)
	public String displayHqBranchesDistrictGrid(int headQuarterId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,133,"editheadquarter.do",100);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<HqBranchesDistricts> hqBranchesDistricts	=	null;
			HeadQuarterMaster headQuarterMaster = null;
			SortedMap map = new TreeMap();
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("branchName")&& !sortOrder.equals("addedBy")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("branchName"))
				 {
					 sortOrderNoField="branchName";
				 }
				 if(sortOrder.equals("addedBy"))
				 {
					 sortOrderNoField="addedBy";
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			if(headQuarterId!=0)			
			{
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
				Criterion criterion = Restrictions.eq("headQuarterMaster", headQuarterMaster);
				hqBranchesDistricts = hqBranchesDistrictsDAO.findByCriteria(criterion);
			}
			//DistrictMaster districtMaster =	districtMasterDAO.findById(districtId, false, false);
			
			//Criterion criterionDSchool = Restrictions.eq("districtMaster",districtMaster);
			//districtSchools 						=	districtSchoolsDAO.findByCriteria(sortOrderStrVal,criterionDSchool);
			
			
			List<HqBranchesDistricts> sortedlst		=	new ArrayList<HqBranchesDistricts>();
			
			SortedMap<String,HqBranchesDistricts>	sortedMap = new TreeMap<String,HqBranchesDistricts>();
			if(sortOrderNoField.equals("branchName"))
			{
				sortOrderFieldName	=	"branchName";
			}
			if(sortOrderNoField.equals("addedBy"))
			{
				sortOrderFieldName	=	"addedBy";
			}
			int mapFlag=2;
			for (HqBranchesDistricts hqBD : hqBranchesDistricts){
				String orderFieldName=""+hqBD.getCreatedDateTime();
				if(sortOrderFieldName.equals("branchName")){
					orderFieldName=hqBD.getBranchMaster().getBranchName()+"||"+hqBD.getHqBranchDistrictId();
					sortedMap.put(orderFieldName+"||",hqBD);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("addedBy")){
					orderFieldName=hqBD.getUserMaster().getFirstName()+" "+hqBD.getUserMaster().getLastName()+"||"+hqBD.getHqBranchDistrictId();
					sortedMap.put(orderFieldName+"||",hqBD);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlst.add((HqBranchesDistricts) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlst.add((HqBranchesDistricts) sortedMap.get(key));
				}
			}else{
				sortedlst=hqBranchesDistricts;
			}
			
			totalRecord =sortedlst.size();

			if(totalRecord<end)
				end=totalRecord;
			List<HqBranchesDistricts> lstsortedlst		=	sortedlst.subList(start,end);
			
			dmRecords.append("<table id='districtSchoolsTable' border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("Branch Name",sortOrderFieldName,"branchName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Added On",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='25%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Added By",sortOrderFieldName,"addedBy",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			dmRecords.append("<th width='15%'>Actions</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(hqBranchesDistricts==null)
			{
				dmRecords.append("<tr><td colspan='6'>No Branch found</td></tr>" );
			}
			else
			{
				for (HqBranchesDistricts hqBDDetail : lstsortedlst) 
				{
					if(hqBDDetail.getBranchMaster()!=null)
					{
						dmRecords.append("<tr>" );
						dmRecords.append("<td>"+hqBDDetail.getBranchMaster().getBranchName()+"</td>");					
						dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(hqBDDetail.getCreatedDateTime())+"</td>");
						dmRecords.append("<td>"+hqBDDetail.getUserMaster().getFirstName()+" "+hqBDDetail.getUserMaster().getLastName()+"</td>");
						dmRecords.append("<td>");
						if(roleAccess.indexOf("|3|")!=-1){
							dmRecords.append("<a href='javascript:void(0);' onclick='return deleteHqBranchesDistricts("+hqBDDetail.getHqBranchDistrictId()+")'>Remove</a>");
						}else{
							dmRecords.append("&nbsp;");
						}
						dmRecords.append("</td>");
						dmRecords.append("</tr>");
					}
				}
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForDistriceAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	
	@Transactional(readOnly=false)
	public int saveHeadQuarterBranchesDistricts(Integer headQuarterId,Integer branchId,String districtId,Integer schoolId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		System.out.println(" districtId ::: "+districtId);
		HeadQuarterMaster headQuarterMaster = null;
		BranchMaster branchMaster	=	null;
		UserMaster userMaster		=	null;
		HqBranchesDistricts hqBranchesDistricts = null;
		Criterion criterion = null;
		Criterion criterion1 = null;
		List<HqBranchesDistricts> hqBranchesDistrictsList = null;
		Boolean districtExists = false;
		Boolean saved = false;
		String districtIds = "";
		System.out.println(" districtId ::::: "+districtId);
		try
		{
			if(branchId!=0)
			{
				branchMaster		=	branchMasterDAO.findById(branchId, false, false);
				criterion			=	Restrictions.eq("branchMaster",branchMaster);
			}
			if(headQuarterId!=0)
			{
				headQuarterMaster	=	headQuarterMasterDAO.findById(headQuarterId, false, false);
				criterion1			=	Restrictions.eq("headQuarterMaster",headQuarterMaster);
			}
			userMaster				=	usermasterdao.findById(userSession.getUserId(), false, false);
			
			if(criterion!=null && criterion1!=null)
			{
				hqBranchesDistrictsList		=	hqBranchesDistrictsDAO.findByCriteria(criterion,criterion1);
			}
			else
			{
				hqBranchesDistrictsList		=	hqBranchesDistrictsDAO.findByCriteria(criterion1);
			}
			
			int size 	=	hqBranchesDistrictsList.size();
			if(size>0)
			{
				if(districtId!=null && districtId.length()>0 &&  hqBranchesDistrictsList.size()>0)
				{
					if(hqBranchesDistrictsList.get(0).getDistrictId()!=null && hqBranchesDistrictsList.get(0).getDistrictId().contains(districtId))
						districtExists = true; // district id exists
					else
					{
						if(hqBranchesDistrictsList.get(0).getDistrictId()!=null)
						{
							districtIds = hqBranchesDistrictsList.get(0).getDistrictId()+","+districtId;
						}
						else
						{
							if(districtId!=null && districtId.length()>0)
								districtIds = districtId;
						}
						hqBranchesDistricts = hqBranchesDistrictsList.get(0);
						hqBranchesDistricts.setHeadQuarterMaster(headQuarterMaster);
						hqBranchesDistricts.setBranchMaster(branchMaster);
						if(districtIds!="0"){
							hqBranchesDistricts.setDistrictId(districtIds);
						}
						else{
							hqBranchesDistricts.setDistrictId(null);
						}
						hqBranchesDistricts.setUserMaster(userMaster);
						hqBranchesDistricts.setCreatedDateTime(new Date());
						hqBranchesDistrictsDAO.updatePersistent(hqBranchesDistricts);
						saved = true;
					}
				}
			}
			if((size>0 && !districtExists && !saved)  || districtExists)
				return 3;	/*=== it return 3 if find dup hq Branch District from  Table=======*/
			
			if(size==0)
			{
				hqBranchesDistricts = new HqBranchesDistricts();
				hqBranchesDistricts.setHeadQuarterMaster(headQuarterMaster);
				hqBranchesDistricts.setBranchMaster(branchMaster);
				if(districtId!="0"){
					hqBranchesDistricts.setDistrictId(districtId);
				}
				hqBranchesDistricts.setUserMaster(userMaster);
				hqBranchesDistricts.setCreatedDateTime(new Date());
				hqBranchesDistrictsDAO.makePersistent(hqBranchesDistricts);
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}

		return 1;
	}
	
	
	@Transactional(readOnly=false)
	public boolean deleteHqBranchesDistrict(Integer hqBranchDistrictId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		try
		{
			HqBranchesDistricts hqBranchesDistricts = null;
			if(hqBranchDistrictId!=0)
			{
				hqBranchesDistricts = hqBranchesDistrictsDAO.findById(hqBranchDistrictId, false, false);
				if(hqBranchesDistricts!=null)
				{
					hqBranchesDistrictsDAO.makeTransient(hqBranchesDistricts);
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Transactional(readOnly=false)
	public boolean activateDeactivateHeadQuarter(int headQuarterId,String status)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }else{
	    	userMaster=(UserMaster)session.getAttribute("userMaster");
	    }
		try{
			HeadQuarterMaster headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
			headQuarterMaster.setStatus(status);
			headQuarterMasterDAO.updatePersistent(headQuarterMaster);
			//--------------------------
			
			
			String messageSubject="";
			String messageSend="";
			SimpleDateFormat dformat=new SimpleDateFormat("HH:MM:SS");
			Date d=new Date();
		
			String hqName=headQuarterMaster.getHeadQuarterName();		
			
			if(status.equals("I"))
			{
				messageSubject="HeadQuarter Deactivated";
				messageSend="Dear TMAdmin,<br><br>" +hqName+
						" was deactivated by " + userMaster.getFirstName()+" "+userMaster.getLastName()
						+" at "+dformat.format(d)+". Please verify with "+userMaster.getFirstName()+" "+userMaster.getLastName()+
						"("+userMaster.getEmailAddress()+") "+
						"that this is an intended action.Thank you.";
			}
			if(status.equals("A"))
				{
				messageSubject="HeadQuarter Activated";
				messageSend="Dear TMAdmin,<br><br>" +hqName+
						" was activated by " + userMaster.getFirstName()+" "+userMaster.getLastName()
						+" at "+dformat.format(d)+". Please verify with "+userMaster.getFirstName()+" "+userMaster.getLastName()+
						"("+userMaster.getEmailAddress()+") "+
						"that this is an intended action.Thank you.";
				}
			System.out.println(messageSend);
				String content="";
				
				Properties  properties = new Properties();  
				try {
					InputStream inputStream = new Utility().getClass().getClassLoader().getResourceAsStream("smtpmail.properties");
					properties.load(inputStream);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			try {				
				
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom(properties.getProperty("smtphost.noreplyusername"));
				dsmt.setMailto(properties.getProperty("smtphost.tmsysadmin"));
				dsmt.setMailsubject(messageSubject);
				content=MailText.messageForDefaultFont(messageSend, userMaster);					
				dsmt.setMailcontent(content);
				
				System.out.println("content     "+content);
				try {
					dsmt.start();	
				} catch (Exception e) {}
			} catch (Exception e) {
				e.printStackTrace();
			}
			//--------------------------
			try{
				UserLoginHistory userLoginHistory = new UserLoginHistory();
				userLoginHistory.setUserMaster(userMaster);
				userLoginHistory.setSessionId(session.getId());
				userLoginHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
				userLoginHistory.setLogTime(new Date());
				if(headQuarterMaster!=null)
					userLoginHistory.setHeadQuarterMaster(headQuarterMaster);
				if(status.equalsIgnoreCase("A")){
					userLoginHistory.setLogType("Activate HeadQuarter");
				}else{
					userLoginHistory.setLogType("Deactivate HeadQuarter");
				}
				userLoginHistoryDAO.makePersistent(userLoginHistory);
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}catch (Exception e) 
		{
			e.printStackTrace();
			return true;
		}
		System.out.println("complete");
		return true;
	}
	
	@Transactional(readOnly=false)
	public String displayHqDistrictGrid(int headQuarterId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,134,"editbranch.do",100);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<HqBranchesDistricts> hqBranchesDistricts	=	null;
			HeadQuarterMaster headQuarterMaster = null;
			if(headQuarterId!=0)			
			{
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
				Criterion criterion = Restrictions.eq("headQuarterMaster", headQuarterMaster);
				Criterion criterion2 = Restrictions.isNull("branchMaster");
				hqBranchesDistricts = hqBranchesDistrictsDAO.findByCriteria(criterion,criterion2);
			}
			
			List<HqBranchesDistricts> sortedlst		=	new ArrayList<HqBranchesDistricts>();
			
			SortedMap<String,HqBranchesDistricts>	sortedMap = new TreeMap<String,HqBranchesDistricts>();
			
			int mapFlag=2;		
			
			totalRecord =sortedlst.size();

			if(totalRecord<end)
				end=totalRecord;
			List<HqBranchesDistricts> lstsortedlst		=	sortedlst.subList(start,end);
			
			dmRecords.append("<table id='districtTable' border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			//responseText=PaginationAndSorting.responseSortingLink("Branch Name",sortOrderFieldName,"branchName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='40%' valign='top'>District Name</th>");
			
			//responseText=PaginationAndSorting.responseSortingLink("Added On",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='25%' valign='top'>Added On</th>");
			
			//responseText=PaginationAndSorting.responseSortingLink("Added By",sortOrderFieldName,"addedBy",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='20%' valign='top'>Added By</th>");
			
			dmRecords.append("<th width='15%'>Actions</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			List<Integer> distIntIds = new ArrayList<Integer>();
			if(hqBranchesDistricts==null || hqBranchesDistricts.size()==0 || hqBranchesDistricts.get(0).getDistrictId()==null)
			{
				dmRecords.append("<tr><td colspan='6'>No District found</td></tr>" );
			}
			else if(hqBranchesDistricts.get(0).getDistrictId()!=null)
			{
				String dIds = hqBranchesDistricts.get(0).getDistrictId();
				List<DistrictMaster> districtMasters = null;
				DistrictMaster districtMaster = null;
				if(dIds.contains(","))
				{
					String[] districtIds = dIds.split(",");
					for(String s : districtIds)
					{
						distIntIds.add(Integer.parseInt(s));
					}
					try {
						Criterion criterion = Restrictions.in("districtId", distIntIds);					
						districtMasters = districtMasterDAO.findByCriteria(Order.asc("districtName"),criterion);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else
				{
					try {
						Criterion criterion = Restrictions.eq("districtId", Integer.parseInt(dIds));					
						districtMasters = districtMasterDAO.findByCriteria(Order.asc("districtName"),criterion);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				if(hqBranchesDistricts!=null && hqBranchesDistricts.size()>0)
				{
					System.out.println(" districtMasters :: "+districtMasters.size());
					for (DistrictMaster dMaster : districtMasters) 
					{
						dmRecords.append("<tr>" );
						dmRecords.append("<td>"+dMaster.getDistrictName()+"</td>");
						dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(hqBranchesDistricts.get(0).getCreatedDateTime())+"</td>");
						dmRecords.append("<td>"+hqBranchesDistricts.get(0).getUserMaster().getFirstName()+" "+hqBranchesDistricts.get(0).getUserMaster().getLastName()+"</td>");
						dmRecords.append("<td>");
						if(roleAccess.indexOf("|3|")!=-1){
							dmRecords.append("<a href='javascript:void(0);' onclick=\"return deleteHqDistricts("+hqBranchesDistricts.get(0).getHqBranchDistrictId()+",'"+dMaster.getDistrictId()+"')\">Remove</a>");
						}else{
							dmRecords.append("&nbsp;");
						}
						dmRecords.append("</td>");
						dmRecords.append("</tr>");
					}
				}
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForDistriceAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	@Transactional(readOnly=false)
	public String saveDistrict(int headQuarterId,int districtId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		UserMaster userMaster = null;
		if(session.getAttribute("userMaster")!=null)
		{
			userMaster = (UserMaster)session.getAttribute("userMaster");
		}
		HeadQuarterMaster headQuarterMaster = null;
		BranchMaster branchMaster = null;
		DistrictMaster districtMaster = null;
		String result = "";
		if(headQuarterId!=0)
		{
			headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
		}
		if(districtId!=0)
		{
			districtMaster = districtMasterDAO.findById(districtId, false, false);
		}
		String temp = session.getId();
		List<TempDistricts> tempDistrictList = null;
		tempDistrictList = tempDistrictsDAO.getTempDistrictsBySessionIdAndDistrictId(temp, districtMaster);
		if(tempDistrictList!=null && tempDistrictList.size()>0)
		{
			result = "3";
		}
		else
		{
			TempDistricts tempDistricts = new TempDistricts();
			tempDistricts.setBranchMaster(branchMaster);
			tempDistricts.setHeadQuarterMaster(headQuarterMaster);
			tempDistricts.setDistrictMaster(districtMaster);
			tempDistricts.setCreatedDateTime(new Date());
			tempDistricts.setUserMaster(userMaster);
			tempDistricts.setTempId(temp);
			try {
				tempDistrictsDAO.makePersistent(tempDistricts);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	
	public String showTempDistricts(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		
		StringBuffer sb = new StringBuffer();
		
		int noOfRowInPage	=	Integer.parseInt(noOfRow);
		int pgNo			= 	Integer.parseInt(pageNo);
		int start 			= 	((pgNo-1)*noOfRowInPage);
		int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord 	=	0;
		
		String temp = session.getId();
		List<TempDistricts> tempDistricts = null;
		tempDistricts = tempDistrictsDAO.getTempDistrictsBySessionId(temp);
		sb.append("<table id='tempDistrictTable' border='0' class='table table-bordered table-striped' >");
		sb.append("<thead class='bg'>");
		sb.append("<tr>");
		String responseText="";
		//responseText=PaginationAndSorting.responseSortingLink("District Name",sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
		sb.append("<th width='40%' valign='top'>District Name</th>");
		
		//responseText=PaginationAndSorting.responseSortingLink("Added On",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
		//sb.append("<th width='25%' valign='top'>Added On</th>");
		
		//responseText=PaginationAndSorting.responseSortingLink("Added By",sortOrderFieldName,"addedBy",sortOrderTypeVal,pgNo);
		sb.append("<th width='20%' valign='top'>Added By</th>");
		
		sb.append("<th width='15%'>Actions</th>");
		sb.append("</tr>");
		sb.append("</thead>");
		if(tempDistricts==null || tempDistricts.size()==0)
		{
			sb.append("<tr><td colspan='6'>No District found</td></tr>" );
		}			
		else
		{
			for(TempDistricts jbtemp : tempDistricts)
			{
				sb.append("<tr>" );
				sb.append("<td>"+jbtemp.getDistrictMaster().getDistrictName()+"</td>");
				//sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbtemp.getCreatedDateTime())+"</td>");
				sb.append("<td>"+jbtemp.getUserMaster().getFirstName()+" "+jbtemp.getUserMaster().getLastName()+"</td>");
				sb.append("<td>");
				sb.append("<a href='javascript:void(0);' onclick=\"return removeTempDistricts("+jbtemp.getDistrictMaster().getDistrictId()+")\">Remove</a>");
				sb.append("</td>");
				sb.append("</tr>");
			}
		}
		sb.append("</table>");
		return sb.toString();
	}
	
	@Transactional(readOnly=false)
	public String deleteTemp()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		
		String temp = session.getId();
		
		System.out.println(" session id :: "+temp);
		
		List<TempDistricts> tempDistricts = null;
		tempDistricts = tempDistrictsDAO.getTempDistrictsBySessionId(temp);
		if(tempDistricts!=null && tempDistricts.size()>0)
		{
			for(TempDistricts jbtemp:tempDistricts)
			{
				tempDistrictsDAO.makeTransient(jbtemp);
			}
		}
		
		return "";
	}
	
	@Transactional(readOnly=false)
	public String deleteTempDistrict(int districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		
		String temp = session.getId();
		DistrictMaster districtMaster = null;
		if(districtId!=0)
		{
			districtMaster = districtMasterDAO.findById(districtId, false, false);
		}
		List<TempDistricts> tempDistricts = null;
		
		tempDistricts = tempDistrictsDAO.getTempDistrictsBySessionIdAndDistrictId(temp,districtMaster);
		
		if(tempDistricts!=null && tempDistricts.size()>0)
		{
			for(TempDistricts jbtemp:tempDistricts)
			{
				tempDistrictsDAO.makeTransient(jbtemp);
			}
		}
		
		return "";
	}
	
	
	@Transactional(readOnly=false)
	public String saveHeadQuarterBranch(int headQuarterId,String branchCode,String branchEmail,String bbName,String bdName,String branchAdress1,String branchAdress2,String stateId,
			String cityName,String zipCode,String phoneNumber,int timeZoneId,String contactNumber1,String contactNumber2)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try
		{
			List<BranchMaster> checkBranchDuplicate = null;
			
			if(branchCode!=null && branchCode.length()>0 && bbName!=null && bbName.length()>0)
			checkBranchDuplicate = branchMasterDAO.checkDuplicateBranchByName(branchCode+": "+bbName);
			
			
			if(checkBranchDuplicate==null || checkBranchDuplicate.size()==0)
			{
				BranchMaster branchMaster=new BranchMaster();

				HeadQuarterMaster headQuarterMaster= headQuarterMasterDAO.findById(headQuarterId, false, false);
				branchMaster.setHeadQuarterMaster(headQuarterMaster);

				branchMaster.setBranchCode(branchCode);
				branchMaster.setBranchEmailAddress(branchEmail);
				branchMaster.setBranchBusinessName(bbName);
				branchMaster.setBranchDisplayName(bdName);
				branchMaster.setAddress(branchAdress1+" "+branchAdress2);
				StateMaster stateMaster = stateMasterDAO.findById(Long.parseLong(stateId), false, false);
				branchMaster.setStateMaster(stateMaster);
				branchMaster.setCityName(cityName);
				branchMaster.setZipCode(zipCode);
				branchMaster.setPhoneNumber(phoneNumber);

				TimeZoneMaster timeZonMaster=new TimeZoneMaster();
				timeZonMaster.setTimeZoneId(timeZoneId);
				branchMaster.setTimeZoneMaster(timeZonMaster);

				branchMaster.setContactNumber1(contactNumber1);
				branchMaster.setContactNumber2(contactNumber2);
				branchMaster.setBranchName(branchMaster.getBranchCode()+": "+branchMaster.getBranchBusinessName());
				branchMaster.setAnnualSubsciptionAmount(new Double(0));
				branchMaster.setAreAllDistrictsInContract(false);
				branchMaster.setOfferQualificationItems(false);
				branchMaster.setOfferDistrictSpecificItems(false);;
				branchMaster.setDisplayFitScore(false);
				branchMaster.setDisplayExpectedSalary(false);
				branchMaster.setDisplayYearsTeaching(false);
				branchMaster.setDisplayJSI(false);
				branchMaster.setDisplayDemoClass(false);
				branchMaster.setDisplayTFA(false);
				branchMaster.setDisplayTMDefaultJobCategory(false);
				branchMaster.setWritePrivilegeToDistrict(false);
				branchMaster.setDisplayAchievementScore(false);
				branchMaster.setAutoNotifyCandidateOnAttachingWithJob(true);
				branchMaster.setPostingOnHQWall(2);
				branchMaster.setPostingOnTMWall(2);
				branchMaster.setCommunicationsAccess(2);
				branchMaster.setCanTMApproach(0);
				branchMaster.setStatusPrivilegeForBranches(false);
				branchMaster.setHiringAuthority("");
				branchMaster.sethQApproval(true);
				branchMaster.setIsPortfolioNeeded(true);
				branchMaster.setIsWeeklyCgReport(0);
				branchMaster.setTotalNoOfDistricts(0);
				branchMaster.setCreatedDateTime(new Date());
				branchMaster.setSendAutoVVILink(false);
				branchMaster.setOfferVirtualVideoInterview(false);
				branchMaster.setOfferPortfolioNeeded(false);
				branchMaster.setOfferJSI(false);
				branchMaster.setOfferEPI(false);
				branchMaster.setStatus("A");
				
				
				String temp = session.getId();
				DistrictMaster districtMaster = null;
				List<TempDistricts> tempDistricts = null;

				tempDistricts = tempDistrictsDAO.getTempDistrictsBySessionId(temp);

				StringBuffer buffer=new StringBuffer();
				String str = "";
				for(TempDistricts tempdis:tempDistricts)
				{
					buffer.append(str).append(tempdis.getDistrictMaster().getDistrictId());
					str = ",";

				}
				
				if(tempDistricts.size()>0)
					branchMaster.setSelectedDistrictUnderContract(1);
				else
					branchMaster.setNoDistrictUnderContract(1);
				
				HqBranchesDistricts hqBranchesDistricts=new HqBranchesDistricts();
				hqBranchesDistricts.setBranchMaster(branchMaster);
				
				if(buffer.toString().length()>0)
					hqBranchesDistricts.setDistrictId(buffer.toString());
				
				hqBranchesDistricts.setHeadQuarterMaster(headQuarterMaster);
				hqBranchesDistricts.setCreatedDateTime(new Date());

				UserMaster userMaster = null;
				userMaster=(UserMaster)session.getAttribute("userMaster");
				hqBranchesDistricts.setUserMaster(userMaster);

				try {
					branchMasterDAO.makePersistent(branchMaster);
				} catch (Exception e) {
					e.printStackTrace();
				}

				/*String temp = session.getId();
				DistrictMaster districtMaster = null;
				List<TempDistricts> tempDistricts = null;

				tempDistricts = tempDistrictsDAO.getTempDistrictsBySessionId(temp);

				StringBuffer buffer=new StringBuffer();
				for(TempDistricts tempdis:tempDistricts)
				{
					buffer.append(tempdis.getDistrictMaster().getDistrictId()+",");

				}
				if(tempDistricts.size()>0)
					buffer.replace(buffer.lastIndexOf(","), buffer.lastIndexOf(",")+1, "");
				HqBranchesDistricts hqBranchesDistricts=new HqBranchesDistricts();
				hqBranchesDistricts.setBranchMaster(branchMaster);
				hqBranchesDistricts.setDistrictId(buffer.toString());
				hqBranchesDistricts.setHeadQuarterMaster(headQuarterMaster);
				hqBranchesDistricts.setCreatedDateTime(new Date());

				UserMaster userMaster = null;
				userMaster=(UserMaster)session.getAttribute("userMaster");
				hqBranchesDistricts.setUserMaster(userMaster);*/

				hqBranchesDistrictsDAO.makePersistent(hqBranchesDistricts);
				headQuarterMaster=headQuarterMasterDAO.findById(headQuarterMaster.getHeadQuarterId(), false, false);
				headQuarterMaster.setIsBranchExist(true);
				headQuarterMasterDAO.makePersistent(headQuarterMaster);
			}
			else
			{
				return "duplicate";
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "managebranches";
	}

	@Transactional
	public String saveHeadQuarterJoborder(int jobId,int headQuarterId,int branchId,int districtId,String jobTitle,String jobStartDate,String jobEndDate,String jDescription,String jQualification,int jobCategoryId,
    		boolean isExpHireNotEqualToReqNo,boolean branchAdministrator,String noOfExpHires,String attachDistrict,int tmInventory,String hprocess,String jobDescriptionFileName
    		,boolean isInviteOnly,String hiddenJob,int isJobAssessment,int offerJSI,int attachJobAssessment,String assessmentDocument,int jsiReq)
	{
		System.out.println("saveHeadQuarterJoborder is called");
		
		String result = "";
		JobOrder jobOrder=null;
		
		boolean isNew = false;		// for log maintain
		JobOrder jobOrderForLog = new JobOrder();
		UserMaster userMaster = null;
		try 
		{			
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			int entityID=0;
			int createdBy=0;
			
			Boolean jobAssessment = false;
			Integer attachDefault = 0;
			Integer attachDefaultHeadQuarter = 0;
			Integer attachNewPillar = 0;
			int roleId =0;
			
		
			
			if(jobId==0){
				jobOrder=new JobOrder();
				isNew = true;		// for log maintain
			}else{
				System.out.println(" jobId :: "+jobId);
				jobOrder = jobOrderDAO.findById(jobId, false, false);
				attachNewPillar = jobOrder.getAttachNewPillar()==null?0:jobOrder.getAttachNewPillar();
				// for log maintainance
				//isNew = true;		// for log maintain
				BeanUtils.copyProperties(jobOrder, jobOrderForLog);
			}
			
			HeadQuarterMaster headQuarterMaster= headQuarterMasterDAO.findById(headQuarterId, false, false);
			jobOrder.setHeadQuarterMaster(headQuarterMaster);
			
			BranchMaster branchMaster = branchMasterDAO.findById(branchId, false, false);
			jobOrder.setBranchMaster(branchMaster);
			
			jobOrder.setJobTitle(jobTitle);
			if(jobStartDate!=null)
				jobOrder.setJobStartDate(Utility.getCurrentDateFormart(jobStartDate));
			if(jobEndDate!=null)
				jobOrder.setJobEndDate(Utility.getCurrentDateFormart(jobEndDate));
			jobOrder.setJobDescription(jDescription);
			jobOrder.setJobQualification(jQualification);
			JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();
			jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
			//jobCategoryMaster.setJobCategoryId(jobCategoryId);
			jobOrder.setJobCategoryMaster(jobCategoryMaster);
			System.out.println(jobOrder.getJobCategoryMaster().getJobCategoryName());
			jobOrder.setIsExpHireNotEqualToReqNo(false);
			
			if(noOfExpHires!=null && !noOfExpHires.trim().equals(""))
				jobOrder.setNoOfExpHires(Integer.parseInt(noOfExpHires));
			
			if(jobDescriptionFileName!=null && jobDescriptionFileName.length()>0)
			jobOrder.setDistrictAttachment(jobDescriptionFileName);			
			
			if(jobCategoryId!=0){
				jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				if(jobCategoryMaster!=null && jobCategoryMaster.getDistrictMaster()!=null)
				jobOrder.setDistrictMaster(jobCategoryMaster.getDistrictMaster());
			}
			
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			jobOrder.setDistrictMaster(districtMaster);
			
				
			userMaster=(UserMaster)session.getAttribute("userMaster");
			roleId = userMaster.getRoleId().getRoleId();
			entityID=userMaster.getEntityType();
			createdBy=userMaster.getUserId();
			jobOrder.setCreatedByEntity(entityID);
			jobOrder.setCreatedBy(createdBy);
			jobOrder.setWritePrivilegeToBranch(branchAdministrator);
			if(jobId==0)
			jobOrder.setStatus("A");
			jobOrder.setIsPoolJob(0);
			jobOrder.setSendAutoVVILink(false);
			jobOrder.setOfferVirtualVideoInterview(false);
			jobOrder.setJobType("F");
			jobOrder.setJobStatus("O");
			jobOrder.setCreatedByEntity(5);
			jobOrder.setCreatedForEntity(5);
			jobOrder.setCreatedDateTime(new Date());
			jobOrder.setApprovalBeforeGoLive(1);
			jobOrder.setIsVacancyJob(false);
			jobOrder.setGeoZoneMaster(null);
			jobOrder.setIsPortfolioNeeded(false);
			if(jobCategoryMaster.getJobInviteOnly()!=null && jobCategoryMaster.getJobInviteOnly()){
				jobOrder.setIsInviteOnly(true);
			}
			else{
				jobOrder.setIsInviteOnly(false);
			}
			
			if(jobOrder.getJobCategoryMaster().getHiddenJob()!=null && jobOrder.getJobCategoryMaster().getHiddenJob()){
				jobOrder.setHiddenJob(true);
			}
			else
			{
				jobOrder.setHiddenJob(false);
			}
			
			if(jobId!=0){
				jobAssessment=jobOrder.getIsJobAssessment();
				attachDefault=jobOrder.getAttachNewPillar();
				attachDefaultHeadQuarter=jobOrder.getAttachDefaultHeadQuarterPillar();
			}
			
			System.out.println("isJobAssessment :: "+isJobAssessment);
			
			/**
			 *  configure JSI with job Category flag(offerJsi)
			 *  Start
			 */			
				if(jobCategoryMaster!=null && jobCategoryMaster.getOfferJSI()==1){
					isJobAssessment=1;
					attachJobAssessment=1;
				}
				else
				{
					isJobAssessment=0;
					attachJobAssessment=0;
				}
			/**
			 *  configure JSI with job Category flag(offerJsi)
			 *  End
			 */
			
			if(isJobAssessment==1){
				jobOrder.setIsJobAssessment(true);
				jobOrder.setJobAssessmentStatus(1); // set 1 if jobCategory(offerJSI) sets 1 to make it mandatory
				
			}else{
				jobOrder.setIsJobAssessment(false);
				jobOrder.setJobAssessmentStatus(offerJSI);
			}
			if(isJobAssessment==1 && attachJobAssessment==2) // attach new JSI
			{ 
				if(!assessmentDocument.equals(""))
				jobOrder.setAssessmentDocument(assessmentDocument);
				jobOrder.setAttachNewPillar(1);
				jobOrder.setAttachDefaultHeadQuarterPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultJobSpecificPillar(2);
			}else if(isJobAssessment==1 && attachJobAssessment==1) // attach headQuarter Default JSI
			{ 
				jobOrder.setAssessmentDocument(null);
				jobOrder.setAttachDefaultHeadQuarterPillar(1);
				jobOrder.setAttachNewPillar(2);				
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultJobSpecificPillar(2);
			}else if(isJobAssessment==1 && attachJobAssessment==3) 
			{ 
				jobOrder.setAssessmentDocument(null);
				jobOrder.setAttachNewPillar(2);
				jobOrder.setAttachDefaultHeadQuarterPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultJobSpecificPillar(2);
			}else if(isJobAssessment==1 && attachJobAssessment==4)  // attach jobCategory JSI
			{
				jobOrder.setAssessmentDocument(null);
				jobOrder.setAttachNewPillar(2);
				jobOrder.setAttachDefaultHeadQuarterPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultJobSpecificPillar(1);
			}else if(isJobAssessment==2) // attach no JSI
			{ 
				jobOrder.setAttachNewPillar(2);
				jobOrder.setAttachDefaultHeadQuarterPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultJobSpecificPillar(2);
			}
			else
			{
				jobOrder.setAttachNewPillar(2);
				jobOrder.setAttachDefaultHeadQuarterPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultJobSpecificPillar(2);
			}
			
			//if(tmInventory==1)
			//{
				/*if(headQuarterMaster.getExitURL()!=null){
					
				}*/
				jobOrder.setFlagForMessage(1);
				jobOrder.setFlagForURL(2);
				if(hprocess!=null && hprocess.length()>0){
					jobOrder.setExitMessage(hprocess);
				}
				else if(branchMaster.getCompletionMessage()!=null && branchMaster.getCompletionMessage().length()>0){
					jobOrder.setExitMessage(branchMaster.getCompletionMessage());
				}
				else if(headQuarterMaster.getCompletionMessage()!=null && headQuarterMaster.getCompletionMessage().length()>0){
					jobOrder.setExitMessage(headQuarterMaster.getCompletionMessage());
				}
				
				jobOrder.setExitURL(null);
			//}
			/*else
			{
				jobOrder.setFlagForMessage(2);
				jobOrder.setFlagForURL(1);
				jobOrder.setExitURL("http://www.kellyservices.com/Global/Home/");
			}*/
			
			jobOrderDAO.makePersistent(jobOrder);
			
	
			if(jobOrder!=null){
				jobOrder.setApiJobId(jobOrder.getJobId()+"");			   
				jobOrderDAO.makePersistent(jobOrder);
			}
			ArrayList<JobWisePanelStatus> listJobWise = new ArrayList<JobWisePanelStatus>();
			// store log record ( it should be after last joborder persist
			if(isNew) {jobId=jobOrder.getJobId(); jobOrderForLog=jobOrder;}
			createLog.logForJobOrder(request, jobOrderForLog, isNew , jobId, userMaster,listJobWise);
				
			
			
			
			/**
			 *  comment code  (no need of JSI for kelly)
			 *  Start
			 */
			/*
			HeadQuarterMaster hqMaster = null;
			if(jobOrder.getHeadQuarterMaster()!=null)
				hqMaster = jobOrder.getHeadQuarterMaster();
			
			if(!(isJobAssessment==1 && attachJobAssessment==2)){
				AssessmentJobRelation  assessmentJobRelation=null;				
				assessmentJobRelation=assessmentJobRelationDAO.findRelationExistByJobOrder(jobOrder);
				if(attachJobAssessment!=2 && isJobAssessment==1){
					
					AssessmentDetail assessmentDetail=null;
					
					if(attachJobAssessment==1){
						if(roleId==10)
							assessmentDetail=assessmentDetailDAO.checkIsDefaultForHeadQuarter(jobOrder.getHeadQuarterMaster(), null,5,null);
						else if(roleId==11)
							assessmentDetail=assessmentDetailDAO.checkIsDefaultForHeadQuarter(jobOrder.getHeadQuarterMaster(), null,5,null);
					}else if(attachJobAssessment==4){
						if(roleId==10)
							assessmentDetail=assessmentDetailDAO.checkIsDefaultForHeadQuarter(jobOrder.getHeadQuarterMaster(), null,5,null);
						else if(roleId==11)
							assessmentDetail=assessmentDetailDAO.checkIsDefaultForHeadQuarter(jobOrder.getHeadQuarterMaster(), null,5,null);
					}
	
					if(assessmentDetail==null){
						// No default JSI
						// Create new JSI assessment
						AssessmentDetail assessmentDetailNew = new AssessmentDetail();
						System.out.println(" hqName :: "+hqMaster.getHeadQuarterName());
						if(roleId==10){
							assessmentDetailNew.setHeadQuarterMaster(jobOrder.getHeadQuarterMaster());
							assessmentDetailNew.setAssessmentName(jobOrder.getHeadQuarterMaster().getHeadQuarterName()+" default JSI");
						assessmentDetailNew.setJobOrderType(5);
						}
						else if(roleId==11){
							assessmentDetailNew.setBranchMaster(jobOrder.getBranchMaster());
							assessmentDetailNew.setAssessmentName(jobOrder.getBranchMaster().getBranchName()+" default JSI");
						assessmentDetailNew.setJobOrderType(5);
						}
							
						assessmentDetailNew.setAssessmentDescription("");
						assessmentDetailNew.setAssessmentType(2);
						assessmentDetailNew.setIsDefault(true);
						assessmentDetailNew.setStatus("A");
						assessmentDetailNew.setIsResearchEPI(false);
						assessmentDetailNew.setCreatedDateTime(new Date());
						assessmentDetailNew.setUserMaster(userMaster);
						

						if(attachJobAssessment==4)
						{
							System.out.println(" hqName :: "+hqMaster.getHeadQuarterName());
							if(roleId==10){
								assessmentDetailNew.setHeadQuarterMaster(jobOrder.getHeadQuarterMaster());
								assessmentDetailNew.setAssessmentName(jobOrder.getHeadQuarterMaster().getHeadQuarterName()+" default JSI for "+jobOrder.getJobCategoryMaster().getJobCategoryName());
								assessmentDetailNew.setJobOrderType(5);
							}
							else if(roleId==11){
								assessmentDetailNew.setBranchMaster(jobOrder.getBranchMaster());
								assessmentDetailNew.setAssessmentName(jobOrder.getBranchMaster().getBranchName()+" default JSI for "+jobOrder.getJobCategoryMaster().getJobCategoryName());
								assessmentDetailNew.setJobOrderType(5);
							assessmentDetailNew.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
							}
						}

						String ipAddress = IPAddressUtility.getIpAddress(request);
						assessmentDetailNew.setIpaddress(ipAddress);
						assessmentDetailNew.setStatus("A");
						assessmentDetailNew.setUserMaster(userMaster);
						assessmentDetailNew.setCreatedDateTime(new Date());
						try{
							List<AssessmentDetail> assessmentDetails = assessmentDetailDAO.checkAssessmentByName(assessmentDetailNew.getAssessmentName());
							if(assessmentDetails.size()>0)
								assessmentDetailNew.setAssessmentName(assessmentDetailNew.getAssessmentName()+" "+Utility.now());
							assessmentDetailDAO.makePersistent(assessmentDetailNew);
						}catch (Exception e) {
							if(e.getCause() instanceof com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException)
							{
								e.printStackTrace();
							}
						}

						// attaching assessment

						if(assessmentJobRelation==null){
							assessmentJobRelation= new AssessmentJobRelation();
							assessmentJobRelation.setJobId(jobOrder);
							assessmentJobRelation.setCreatedBy(userMaster);
							assessmentJobRelation.setAssessmentId(assessmentDetailNew);
							assessmentJobRelation.setStatus("A");
							assessmentJobRelation.setIpaddress(ipAddress);
							assessmentJobRelationDAO.makePersistent(assessmentJobRelation);
						}else{
							assessmentJobRelation.setJobId(jobOrder);
							assessmentJobRelation.setCreatedBy(userMaster);
							assessmentJobRelation.setAssessmentId(assessmentDetailNew);
							assessmentJobRelation.setStatus("A");
							assessmentJobRelation.setIpaddress(ipAddress);
							assessmentJobRelationDAO.updatePersistent(assessmentJobRelation);
						}
					}else
					{	
						if(assessmentJobRelation==null){
							assessmentJobRelation= new AssessmentJobRelation();
							String ipAddress = IPAddressUtility.getIpAddress(request);
							assessmentJobRelation.setJobId(jobOrder);
							assessmentJobRelation.setCreatedBy(userMaster);
							assessmentJobRelation.setAssessmentId(assessmentDetail);
							assessmentJobRelation.setStatus("A");
							assessmentJobRelation.setIpaddress(ipAddress);
							System.out.println("<<<>>>>>>>>>>  "+ipAddress);
							assessmentJobRelationDAO.makePersistent(assessmentJobRelation);
						}else{
							
							System.out.println("attachNewPillar : "+attachNewPillar);
							AssessmentJobRelation  assessmentJobRelationOld=null;
							AssessmentDetail assessmentDetailOld = null;
							if(attachNewPillar==1)
							{	
								assessmentJobRelationOld=assessmentJobRelationDAO.findRelationExistByJobOrder(jobOrder);
								assessmentDetailOld = assessmentJobRelationOld.getAssessmentId();
							}
							
							String ipAddress = IPAddressUtility.getIpAddress(request);
							assessmentJobRelation.setJobId(jobOrder);
							assessmentJobRelation.setCreatedBy(userMaster);
							assessmentJobRelation.setAssessmentId(assessmentDetail);
							assessmentJobRelation.setStatus("A");
							assessmentJobRelation.setIpaddress(ipAddress);
							assessmentJobRelationDAO.updatePersistent(assessmentJobRelation);
							
							if(attachNewPillar==1)
							{	
								
								System.out.println("**************** "+assessmentDetailOld.getAssessmentName());
								List<AssessmentSections> assessmentSections = assessmentSectionDAO.getSectionsByAssessmentId(assessmentDetailOld);
								if(assessmentSections.size()==0)
								{
									assessmentDetailDAO.makeTransient(assessmentDetailOld);
								}
								
							}
						}
					}
					
				}else{
					if(assessmentJobRelation!=null && roleId!=11){
						try{
							if(jobId!=0)
							{
								AssessmentDetail assessmentDetail = assessmentJobRelation.getAssessmentId();
								assessmentJobRelationDAO.makeTransient(assessmentJobRelation);
								System.out.println("attachNewPillar : "+attachNewPillar);
								if(attachNewPillar==1)
								{
									List<AssessmentSections> assessmentSections = assessmentSectionDAO.getSectionsByAssessmentId(assessmentDetail);
									if(assessmentSections.size()==0)
									{
										assessmentDetailDAO.makeTransient(assessmentDetail);
									}
								}
							}
						}catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

			}else
			{
				///////////////////////////////////////
				// let me add my jsi

				AssessmentJobRelation  assessmentJobRelation=null;
				assessmentJobRelation=assessmentJobRelationDAO.findRelationExistByJobOrder(jobOrder);
				System.out.println("=================================================================");
				System.out.println("assessmentJobRelation: "+assessmentJobRelation);
				AssessmentDetail assessmentDetail = null;
				if(assessmentJobRelation!=null)
				{
					assessmentDetail = assessmentJobRelation.getAssessmentId();
					System.out.println("assessmentDetail: "+assessmentDetail.getAssessmentName());
					if(assessmentDetail.getIsDefault())
						assessmentDetail = null;
				}
				System.out.println("assessmentJobRelation: "+assessmentJobRelation+" , assessmentDetail: "+assessmentDetail);
				if(assessmentDetail==null){

					// Create new JSI assessment
					AssessmentDetail assessmentDetailNew = new AssessmentDetail();
					System.out.println(" hqName :: "+hqMaster.getHeadQuarterName());
					
					if(roleId==10){
						assessmentDetailNew.setHeadQuarterMaster(jobOrder.getHeadQuarterMaster());
						assessmentDetailNew.setAssessmentName(jobOrder.getHeadQuarterMaster().getHeadQuarterName()+" JSI for "+jobOrder.getJobTitle()+" (JobId: "+jobOrder.getJobId()+")");
						assessmentDetailNew.setJobOrderType(5);
					}
					else if(roleId==11){
						assessmentDetailNew.setBranchMaster(jobOrder.getBranchMaster());
						assessmentDetailNew.setAssessmentName(jobOrder.getBranchMaster().getBranchName()+" JSI for "+jobOrder.getJobTitle()+" (JobId: "+jobOrder.getJobId()+")");
						assessmentDetailNew.setJobOrderType(5);
					}
					
					assessmentDetailNew.setAssessmentDescription("");
					assessmentDetailNew.setAssessmentType(2);
					assessmentDetailNew.setIsDefault(false);
					assessmentDetailNew.setIsResearchEPI(false);					
					assessmentDetailNew.setStatus("A");
					assessmentDetailNew.setIsResearchEPI(false);
					assessmentDetailNew.setCreatedDateTime(new Date());
					assessmentDetailNew.setUserMaster(userMaster);
					
					System.out.println("jobOrder.getJobCategoryMaster()::::::::::: "+jobOrder.getJobCategoryMaster());
					if(jobOrder.getJobCategoryMaster()!=null)
						assessmentDetailNew.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
					
					String ipAddress = IPAddressUtility.getIpAddress(request);
					assessmentDetailNew.setIpaddress(ipAddress);
					assessmentDetailNew.setStatus("A");
					assessmentDetailNew.setUserMaster(userMaster);
					assessmentDetailNew.setCreatedDateTime(new Date());
					
					//if(roleId!=11)
					assessmentDetailDAO.makePersistent(assessmentDetailNew);

					// attaching 
					if(assessmentJobRelation==null){
						assessmentJobRelation= new AssessmentJobRelation();
						assessmentJobRelation.setJobId(jobOrder);
						assessmentJobRelation.setCreatedBy(userMaster);
						assessmentJobRelation.setAssessmentId(assessmentDetailNew);
						assessmentJobRelation.setStatus("A");
						assessmentJobRelation.setIpaddress(ipAddress);
						
						//if(roleId!=11)
						assessmentJobRelationDAO.makePersistent(assessmentJobRelation);
					}else{
						assessmentJobRelation.setJobId(jobOrder);
						assessmentJobRelation.setCreatedBy(userMaster);
						assessmentJobRelation.setAssessmentId(assessmentDetailNew);
						assessmentJobRelation.setStatus("A");
						assessmentJobRelation.setIpaddress(ipAddress);
						
						//if(roleId!=11)
						assessmentJobRelationDAO.updatePersistent(assessmentJobRelation);
					}

				}else
				{	
					if(assessmentJobRelation==null){
						assessmentJobRelation= new AssessmentJobRelation();
						String ipAddress = IPAddressUtility.getIpAddress(request);
						assessmentJobRelation.setJobId(jobOrder);
						assessmentJobRelation.setCreatedBy(userMaster);
						assessmentJobRelation.setAssessmentId(assessmentDetail);
						assessmentJobRelation.setStatus("A");
						assessmentJobRelation.setIpaddress(ipAddress);
						assessmentJobRelationDAO.makePersistent(assessmentJobRelation);
					}else{
						String ipAddress = IPAddressUtility.getIpAddress(request);
						assessmentJobRelation.setJobId(jobOrder);
						assessmentJobRelation.setCreatedBy(userMaster);
						assessmentJobRelation.setAssessmentId(assessmentDetail);
						assessmentJobRelation.setStatus("A");
						assessmentJobRelation.setIpaddress(ipAddress);
						
						//if(roleId!=11)
						assessmentJobRelationDAO.updatePersistent(assessmentJobRelation);
					}
				}

			}
			
			*/
			/////////////////////////////////////////////////
			
			/**
			 * comment code
			 *  End
			 */
			
			
			
			
			
			/*if(attachDistrict.equals("1"))
			{
				String temp = session.getId();
				List<TempDistricts> tempDistricts = null;
				
				tempDistricts = tempDistrictsDAO.getTempDistrictsBySessionId(temp);
				
				//SessionFactory sessionFactory = schoolInJobOrderDAO.getSessionFactory();
				//StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				//Transaction txOpen =statelesSsession.beginTransaction();
				
				SchoolInJobOrder schoolInJobOrder = null;
				
				if(tempDistricts!=null && tempDistricts.size()>0)
				{
					for(TempDistricts tempdis:tempDistricts)
					{
						if(jobOrder!=null)
							System.out.println(jobOrder.getJobId());
						else
							System.out.println("Ha ha null");
						schoolInJobOrder = new  SchoolInJobOrder();
						schoolInJobOrder.setDistrictMaster(tempdis.getDistrictMaster());
						schoolInJobOrder.setJobId(jobOrder);
						schoolInJobOrder.setNoOfSchoolExpHires(Integer.parseInt(noOfExpHires));
						schoolInJobOrder.setCreatedDateTime(new Date());
						
						schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
					//	txOpen = statelesSsession.beginTransaction();
						//statelesSsession.insert(schoolInJobOrder);
						//txOpen.commit();						
					}
				}
				//statelesSsession.close();
			}*/
			
			try
			{
				
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				
				ElasticSearchService elasticSearch=new ElasticSearchService();
				System.out.println();
				if(jobOrder.getStatus().equalsIgnoreCase("A")  && jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive()==1 
						&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
						&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
						&&(jobOrder.getIsPoolJob()!=null && jobOrder.getIsPoolJob()!=1 ) && (jobOrder.getIsVacancyJob()!=null && jobOrder.getIsVacancyJob()!=true) && (jobOrder.getIsInviteOnly()!=null && !jobOrder.getIsInviteOnly())) 
				{
					elasticSearch.updateESDistrictJobBoardByJobId(jobOrder);
				}
				else
				{
					elasticSearch.searchDataAndDelete(jobOrder.getJobId(), ElasticSearchConfig.indexForhqDistrictJobBoard);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			// System.out.println("jjjobbbbboder=="+jobOrder.getJobId());
			
			
			if(jobId != 0 && !isNew)
			{
				result = "update-"+jobOrder.getJobId();
			}
			else
			{
				if(jobOrder!=null){
					result = "success-"+jobOrder.getJobId();
				}
				else
				{
					result = "success-";
				}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	public String displayHQBRJobs(int headQuarterId, int branchId,String jobOrderId,String status,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		
		System.out.println(":::::::::::::displayHQBRJobs:::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		StringBuffer sb = new StringBuffer();
		BranchMaster branchMaster=null;
		try{
			
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord 		=	0;
			//------------------------------------
			
			int entityIDLogin=0;
			int roleId=0;
			int entityID = 0;
			
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getHeadQuarterMaster()!=null){					
				headQuarterId =userMaster.getHeadQuarterMaster().getHeadQuarterId();
			}
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getBranchMaster()!=null){
				branchMaster=userMaster.getBranchMaster();
			}
			entityIDLogin=userMaster.getEntityType();
			
			String roleAccess=null;
			try{
				if(roleId==10 || roleId==12)
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,110,"managehqbrjoborders.do",1);
				else if(roleId==11 || roleId==13)
					roleAccess=roleAccessPermissionDAO.getMenuOptionListByMenuId(roleId,144,"managehqbrjoborders.do",1,151);
				System.out.println(" roleAccess :: "+roleAccess);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(headQuarterId!=0 && entityIDLogin==5){
				entityID=entityIDLogin;
			}
		
			List<HeadQuarterMaster> headQuarterMasters	  =	null;
			List<JobOrder> jobOrdersList = null;
			
			String sortOrderFieldName="jobId";
			
			Order  sortOrderStrVal=null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				sortOrderFieldName=sortOrder;
			}
			String sortOrderTypeVal="1";
			sortOrderStrVal=Order.desc(sortOrderFieldName);
			if(sortOrderType!=null)
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("1")){
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
					if(sortOrderType.equals("0")){
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}
				}

			/**End ------------------------------------**/
			
			HeadQuarterMaster headQuarterMaster = null;
			if(entityID==1 || headQuarterId==0){
				
			}else{
				Criterion criterionH = null;
				Criterion criterionB = null;
				Criterion criterion = null;
				if(headQuarterId!=0){
					headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
					criterionH = Restrictions.eq("headQuarterMaster",headQuarterMaster);
				}
				
				Date dateWithoutTime = Utility.getDateWithoutTime();
				Criterion criterion1 = null;
				if(status.equals("All"))
					criterion1 =Restrictions.isNotNull("status");
				else					
					criterion1 =Restrictions.eq("status",status);
				Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
				Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
				if(branchMaster!=null){
					criterionB=Restrictions.eq("branchMaster",branchMaster);
					System.out.println("branchMaster:::"+branchMaster.getBranchId());
				}else if(branchId!=0){
					branchMaster = branchMasterDAO.findById(branchId, false, false);
					criterionB=Restrictions.eq("branchMaster",branchMaster);
					System.out.println("branchMaster:::"+branchMaster.getBranchId());
				}
				System.out.println("jobOrderId::>:"+jobOrderId);
				int jobId=0;
				if(jobOrderId!=null && !jobOrderId.equals("") && !jobOrderId.equals("0")){
					try{
						jobId=Integer.parseInt(jobOrderId);
					}catch(Exception e){}
				}
				if(jobId!=0){
					criterion=Restrictions.eq("jobId",jobId);
				}
				if(criterionH!=null && criterionB==null){
					if(criterion!=null){
						//jobOrdersList = jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionH,criterion,criterion1,criterion2,criterion3);
						//totaRecord	= jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionH,criterion,criterion1,criterion2,criterion3);
						jobOrdersList = jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionH,criterion,criterion1);
						totaRecord	= jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionH,criterion,criterion1);
					}else{
						//jobOrdersList = jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionH,criterion1,criterion2,criterion3);
						//totaRecord	= jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionH,criterion1,criterion2,criterion3);
						jobOrdersList = jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionH,criterion1);
						totaRecord	= jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionH,criterion1);

					}
				}
				else if(criterionH!=null && criterionB!=null){
					if(criterion!=null){
					//jobOrdersList = jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionH,criterionB,criterion,criterion1,criterion2,criterion3);
					//totaRecord	= jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionH,criterionB,criterion,criterion1,criterion2,criterion3);
						jobOrdersList = jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionH,criterionB,criterion,criterion1);
						totaRecord	= jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionH,criterionB,criterion,criterion1);
					}
					else{
						//jobOrdersList = jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionH,criterionB,criterion1,criterion2,criterion3);
						//totaRecord	= jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionH,criterionB,criterion1,criterion2,criterion3);
						jobOrdersList = jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionH,criterionB,criterion1);
						totaRecord	= jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionH,criterionB,criterion1);


					}
				}
				
				//System.out.println(" jobOrdersList for hq :: "+jobOrdersList);
			}
			
			
			 Map<Integer,String> map = new HashMap<Integer, String>();
				if(jobOrdersList.size()>0)
				 map = jobForTeacherDAO.countApplicantsByJobOrdersAllll(jobOrdersList);
				
			sb.append("<table  id='tblJobsGrid' width='100%' border='0'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("Job Id",sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
			sb.append("<th width='250' valign='top'>"+responseText+"</th>");
		
			responseText=PaginationAndSorting.responseSortingLink("Job Title",sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			//responseText=PaginationAndSorting.responseSortingLink("District Name",sortOrderFieldName,"districtId",sortOrderTypeVal,pgNo);
			//sb.append("<th width='14%' valign='top'>"+responseText+"</th>");
			sb.append("<th width='14%' valign='top'>Branch Name</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Status",sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			sb.append("<th width='20' valign='top'> "+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Posted Until",sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
			sb.append("<th width='12%' valign='top'>"+responseText+"</th>");
			
			//sb.append("<th  valign='top'>Applicant(s) <a href='#' id='iconpophover1' class='net-header-text ' rel='tooltip' data-original-title='X/Y, X = Total number of applicants applied for this job, Y = Total number of Available Candidates  (Available Candidates = Total Candidates applied for this job � Incomplete Candidates � Timed Out Candidates � Withdrawn Candidates)'><span class='icon-question-sign'></span></a></th>");
			sb.append("<th  valign='top'>Applicant(s) <a href='#' id='iconpophover1' class='net-header-text ' rel='tooltip' data-original-title='X = Total number of applicants applied for this job'><span class='icon-question-sign'></span></a></th>");
			sb.append("<script>$('#iconpophover1').tooltip();</script>");

			sb.append("<th  valign='top'>Hire(s)</th>");
			
			sb.append("<th  valign='top'>Candidate Grid</th>");
			
			boolean cgForKelly=false;
			try{
				if(Utility.getLocaleValuePropByKey("kellyupdatecodelive", locale)!=null && Utility.getLocaleValuePropByKey("kellyupdatecodelive", locale).equalsIgnoreCase("yes")){ 
					cgForKelly=true;
					System.out.println(":::::::Inside:::::::::::;");
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			
			sb.append(" <th  width='15%' valign='top'>Actions</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			int noOfRecordCheck =0;
			String filePath = "";
			String fileName = "";
			Integer counter = 1;
			int count=0;
			if(jobOrdersList==null || (jobOrdersList!=null &&jobOrdersList.size()==0))
				sb.append("<tr><td colspan='8' align='center'>No Job found</td></tr>" );
			else
			for (JobOrder  jobOrder : jobOrdersList) 
			{
				int jobForTeacherSize=0;
				int jftAvailableCandidates=0;
				int jobForHiresSize=0;
				count++;
				noOfRecordCheck++;  
				sb.append("<tr>");
				sb.append("<td  >"+jobOrder.getJobId()+"</td>");
				sb.append("<td>"+jobOrder.getJobTitle()+"</td>");
				if(jobOrder.getBranchMaster()!=null)
					sb.append("<td>"+jobOrder.getBranchMaster().getBranchName()+"</td>");
				else
					sb.append("<td></td>");
				if(jobOrder.getStatus().equals("A"))
					sb.append("<td>Active</td>");
				else
					sb.append("<td>Inactive</td>");
				
				if(Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobEndDate()).equals("Dec 25, 2099"))
					sb.append("<td>Until filled</td>");
				else
				sb.append("<td nowrap>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobEndDate())+"</td>");
				
				String appliedStr = null;
				appliedStr = map.get(jobOrder.getJobId());
				 if(appliedStr!=null){
					 //sb.append("<td>"+appliedStr.split("##")[0]+"/"+appliedStr.split("##")[1]+"</td>");
					 sb.append("<td>"+appliedStr.split("##")[0]+"</td>");
					 sb.append("<td >"+appliedStr.split("##")[2]+"</td>");
				 }else
				 {
					//sb.append("<td>0/0</td>");
					 sb.append("<td>0</td>");
					sb.append("<td>0</td>");
				 }
				 if(roleAccess.indexOf("|2|")!=-1){
					 if(cgForKelly){
						 sb.append("<td style='text-align:center'>&nbsp;<a data-original-title='CG View' rel='tooltip' id='cgn"+noOfRecordCheck+"' target=\"_blank\" href='candidategridkelly.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+5+"'><span class='icon-table icon-large'></span></a></td>");
					 }else{
						 sb.append("<td style='text-align:center'>&nbsp;<a data-original-title='CG View' rel='tooltip' id='cgn"+noOfRecordCheck+"' target=\"_blank\" href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+5+"'><span class='icon-table icon-large'></span></a></td>");
					 }
				 }else{
					 sb.append("<td></td>");
				 }
				sb.append("<td nowrap>");
				
					boolean pipeFlag=false;
					if(roleAccess.indexOf("|2|")!=-1){
					
						sb.append("<a title=Edit href='hqbrjoborder.do?jobId="+jobOrder.getJobId()+"'><i class='fa fa-pencil-square-o fa-lg' /></a>");
						sb.append(" | ");
						sb.append("<a href='javascript:void(0);' onclick='return showEditHQJobHistory("+jobOrder.getJobId()+")'><i class='fa fa-history fa-lg'/></a>");
						pipeFlag=true;
					}else if(roleAccess.indexOf("|4|")!=-1){
						sb.append("<a href='hqbrjoborder.do?jobId="+jobOrder.getJobId()+"'>View</a>");
						pipeFlag=true;
					} 
					if(roleAccess.indexOf("|7|")!=-1){
						if(pipeFlag)sb.append(" | ");
						if(jobOrder.getStatus().equalsIgnoreCase("A"))
							sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateJob("+jobOrder.getJobId()+",'I')\">Deactivate");
						else
							sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateJob("+jobOrder.getJobId()+",'A')\">Activate");
						sb.append(" | ");
						sb.append("<a href='hqbrjoborder.do?jobId="+jobOrder.getJobId()+"&clone=yes'>Clone");
					}else{
						sb.append("&nbsp;");
					}
				
					if(jobOrder.getIsInviteOnly() !=null && jobOrder.getIsInviteOnly() && (roleAccess.indexOf("|2|")!=-1)){
						sb.append("&nbsp;|<a  href='importcandidatedetails.do?jobId="+jobOrder.getJobId()+"' rel='tooltip' id='exelexport"+count+"' data-original-title='Upload Candidates against this job'><i class='icon-upload' style='font-size: 1.5em;'></i></a>");
						sb.append("<script>$('#exelexport"+count+"').tooltip();</script>");
					}
				sb.append("</td>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationStringForManageJobOrdersAjax(request,totaRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	
	
	public String downloadAttachment(String filePath,String fileName)
	{	
		System.out.println("filePath::::::::"+filePath);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		String path="";
		try 
		{
			String source = Utility.getValueOfPropByKey("headQuarterRootPath")+filePath+"/"+fileName;
			String target = context.getServletContext().getRealPath("/")+filePath+"/";
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+filePath+"/"+fileName;
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			path="";
		}
	
		return path;
	 }
	
	@Transactional(readOnly=false)
	public String updateStatus(int jobId,String status)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		String result = "";
		JobOrder jobOrder = null;
		if(jobId!=0)
		{
			jobOrder = jobOrderDAO.findById(jobId, false, false);			
			jobOrder.setStatus(status);
			jobOrderDAO.updatePersistent(jobOrder);
			result = "update";
			try
			{
				
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				
				ElasticSearchService elasticSearch=new ElasticSearchService();
				System.out.println();
				if(jobOrder.getStatus().equalsIgnoreCase("A") && (jobOrder.getIsInviteOnly()==null ||  !jobOrder.getIsInviteOnly()) && jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive()==1 
						&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
						&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
						&&(jobOrder.getIsPoolJob()!=null && jobOrder.getIsPoolJob()!=1 ) && (jobOrder.getIsVacancyJob()!=null && jobOrder.getIsVacancyJob()!=true))
				{
					elasticSearch.updateESDistrictJobBoardByJobId(jobOrder);
				}
				else
				{
					elasticSearch.searchDataAndDelete(jobOrder.getJobId(), ElasticSearchConfig.indexForhqDistrictJobBoard);
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	
	public String getDistrictDiv(int jobId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer tmRecord = new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		JobOrder jobOrder = null;
		if(jobId!=0)
		{
			jobOrder = jobOrderDAO.findById(jobId, false, false);			
			List<SchoolInJobOrder> listSchoolInJobOrders =schoolInJobOrderDAO.findJobOrder(jobOrder);
			
			tmRecord.append("<table id='savedDistrictTable' border='0' class='table table-bordered table-striped' >");
			tmRecord.append("<thead class='bg'>");
			tmRecord.append("<tr>");
			String responseText="";
			tmRecord.append("<th width='40%' valign='top'>District Name</th>");
			
			tmRecord.append("<th width='25%' valign='top'>Added On</th>");
			
			//tmRecord.append("<th width='15%'>Actions</th>");
			tmRecord.append("</tr>");
			tmRecord.append("</thead>");
			if(listSchoolInJobOrders==null || listSchoolInJobOrders.size()==0)
			{
				tmRecord.append("<tr><td colspan='6'>No District found</td></tr>" );
			}			
			else
			{
				for(SchoolInJobOrder jbtemp : listSchoolInJobOrders)
				{
					tmRecord.append("<tr>" );
					tmRecord.append("<td>"+jbtemp.getDistrictMaster().getDistrictName()+"</td>");
					tmRecord.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbtemp.getCreatedDateTime())+"</td>");
					//tmRecord.append("<td>");
					//tmRecord.append("<a href='javascript:void(0);' onclick=\"return removeTempDistricts("+jbtemp.getDistrictMaster().getDistrictId()+")\">Remove</a>");
					//tmRecord.append("</td>");
					tmRecord.append("</tr>");
				}
			}
			tmRecord.append("</table>");
		}
		return tmRecord.toString();
	}
	public JobCategoryMaster checkForHiddenJob(int jobcatId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		JobCategoryMaster jobCategoryMaster =new JobCategoryMaster();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try {
			if(jobcatId!=0){
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobcatId, false,false);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return jobCategoryMaster;
	}
	public String populateJobCategoryByBranch(int branchId,int headQuarterId)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer tmRecord = new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		StringBuffer sb = new StringBuffer();
		List<JobCategoryMaster> jobCategoryMasterList = null;
		if(branchId!=0)
		{
			BranchMaster branchMaster = branchMasterDAO.findById(branchId, false, false);
			HeadQuarterMaster headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
			jobCategoryMasterList = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(headQuarterMaster, branchMaster, null);
			if(jobCategoryMasterList!=null && jobCategoryMasterList.size()>0)
			{
				System.out.println(" jobCategoryMasterList by branch :: "+jobCategoryMasterList.size());
				sb.append("<option value='-1'>Select Job Category</option>");
				for(JobCategoryMaster jobCat : jobCategoryMasterList)
				{
					sb.append("<option value='"+jobCat.getJobCategoryId()+"'>"+jobCat.getJobCategoryName()+"</option>");
				}				
			}
		}
		return sb.toString();
	}
	
	public String checkForJobCatJSI(int jobcatId)
	{
		StringBuffer sb = new StringBuffer();
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		JobCategoryMaster jobCategoryMaster =new JobCategoryMaster();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try {
			if(jobcatId!=0){
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobcatId, false,false);
				
				if(jobCategoryMaster.getOfferJSI()!=null && (jobCategoryMaster.getOfferJSI()==1 || jobCategoryMaster.getOfferJSI()==2))
				{
					sb.append("found");
				}				
			}
		} catch (Exception e) {
		}
		
		return sb.toString();
	}
	
	@Transactional(readOnly=true)
	public String displayJobCategoryByDistrictBranchAndHead(Integer headQuarterId,Integer branchId,Integer districtId,String txtjobCategory)
	{
		System.out.println("::::::::::::::::::::::displayJobCategoryByDistrictBranchAndHead:::::::::::::::");
		StringBuffer buffer=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		BranchMaster branchMaster=null;
		HeadQuarterMaster headQuarterMaster=null;
		DistrictMaster districtMaster=null;
		Integer jobCategory=0;
		if(txtjobCategory!=null && !txtjobCategory.equals(""))
			jobCategory=Integer.parseInt(txtjobCategory);

		if(branchId!=null && branchId>0){
			branchMaster=branchMasterDAO.findById(branchId, false, false);
		}
		if(headQuarterId!=null && headQuarterId>0){
			headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
		}
		if(districtId!=null && districtId>0){
			districtMaster=districtMasterDAO.findById(districtId, false, false);
		}
		List<JobCategoryMaster> lstjobCategoryMasters=jobCategoryMasterDAO.findAllJobCategoryNameByDistrictBranchAndHead(headQuarterMaster,branchMaster,districtMaster);
		if(lstjobCategoryMasters.size()>0){
			
			System.out.println(" jobCategoryMasterList by branch :: "+lstjobCategoryMasters.size());
			buffer.append("<option value='-1'>Select Job Category</option>");
			for(JobCategoryMaster jobCat : lstjobCategoryMasters)
			{
				buffer.append("<option value='"+jobCat.getJobCategoryId()+"'>"+jobCat.getJobCategoryName()+"</option>");
			}	
			
			/*buffer.append("<select class='form-control' name='jobCategory' id='jobCategory' onchange='resetJobCategory();'>");
			buffer.append("<option selected='selected' value=''>All the categories</option>");
			for(JobCategoryMaster pojo:lstjobCategoryMasters)
				if(pojo!=null)
				{
					if(pojo.getJobCategoryId().equals(jobCategory))
						buffer.append("<option value='"+pojo.getJobCategoryId()+"' selected='selected'>"+pojo.getJobCategoryName()+"</option>");
					else
						buffer.append("<option value='"+pojo.getJobCategoryId()+"'>"+pojo.getJobCategoryName()+"</option>");
				}
			buffer.append("</select>");*/
		}/*else{
			buffer.append("<select class='form-control' name='jobCategory' id='jobCategory' disabled='disabled' onchange='resetJobCategory();'>");
			buffer.append("<option selected='selected' value=''>All the categories</option>");
			buffer.append("</select>");
		}*/
		return buffer.toString();
	}
	
	public HashMap kesdashboardResult(String resultType,Integer hqId,Integer branchId)
	{

		System.out.println(" first Day of week  :::: "+firstDayOfWeek(new Date()));
		System.out.println(" first Day of Month :::: "+firstDayOfMonth(new Date()));
		System.out.println("branchId " +branchId );
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession sess = request.getSession(false);
		if(sess == null ||(sess.getAttribute("teacherDetail")==null && sess.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		UserMaster userMaster = null;
		int listSize=0;
		int activeBranches =0;
		int totalApplicants=0;
		HashMap<String, String> resultmap= new HashMap<String, String>();
		List<JobOrder> activeJobs = new ArrayList<JobOrder>();
		List<Integer>  applicants = new ArrayList<Integer> ();
		List<TeacherStatusHistoryForJob> hires = new ArrayList<TeacherStatusHistoryForJob>();
		HeadQuarterMaster headQuarterMaster = null;
		BranchMaster branchMaster = null;
		
		if(sess.getAttribute("userMaster")!=null)
			userMaster = (UserMaster) sess.getAttribute("userMaster");
		
		if(hqId!=null && hqId!=0){
			headQuarterMaster = headQuarterMasterDAO.findById(hqId, false, false);
		}
		else if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()==null){
			headQuarterMaster = userMaster.getHeadQuarterMaster();
		}
		if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()!=null){
			branchMaster = userMaster.getBranchMaster();
			headQuarterMaster = userMaster.getHeadQuarterMaster();
		}
		if(branchId!=0)
		{
			try{
				branchMaster = branchMasterDAO.findById(branchId, false, false);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		Session session=jobOrderDAO.getSessionFactory().openSession();
		try{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			Criteria cr = session.createCriteria(JobOrder.class);
			cr.setProjection(Projections.projectionList()
					.add(Projections.property("jobId"),"jobId")	
				      .add(Projections.property("headQuarterMaster"), "headQuarterMaster")
					.add(Projections.property("branchMaster"), "branchMaster"))
					.add(Restrictions.eq("headQuarterMaster.headQuarterId", headQuarterMaster.getHeadQuarterId()))
					.add(Restrictions.eq("status", "A"))
					.setResultTransformer(Transformers.aliasToBean(JobOrder.class));
			if(branchMaster!=null){
				cr.add(Restrictions.eq("branchMaster.branchId", branchMaster.getBranchId()));
			}
			cr.add(Restrictions.le("jobStartDate",dateWithoutTime));
			cr.add(Restrictions.ge("jobEndDate",dateWithoutTime));
			activeJobs = cr.list();



		}catch (Exception e) {
			e.printStackTrace();
		}
		// get all applicants for these jobs

		try{
			if(activeJobs!=null && activeJobs.size()>0){
				Criteria cr1 = session.createCriteria(JobForTeacher.class);
				cr1.setProjection(Projections.rowCount())
				.add(Restrictions.in("jobId", activeJobs))  ; 
				applicants = cr1.list();
			}

		}catch (Exception e) {
			e.printStackTrace();
		}


		try{	
			if(activeJobs!=null && activeJobs.size()>0){
				StatusMaster statusMaster = new StatusMaster();
				statusMaster.setStatusId(6);
				Criterion statusId = Restrictions.eq("statusMaster", statusMaster);
				Criterion jobIds = Restrictions.in("jobOrder", activeJobs);
				Criterion start =null;
				Criterion end =	null;

				if(resultType.equalsIgnoreCase("week")){
					start = Restrictions.le("hiredByDate", new Date());
					end = Restrictions.ge("hiredByDate",firstDayOfWeek(new Date()));
				}
				if(resultType.equalsIgnoreCase("month")){

					start = Restrictions.le("hiredByDate", new Date());
					end = Restrictions.ge("hiredByDate",firstDayOfMonth(new Date()));

				}
				hires = teacherStatusHistoryForJobDAO.findByCriteria(start,end,statusId,jobIds);
			}
			Map<String,Integer> jobcat = new HashMap<String,Integer>();

			for(TeacherStatusHistoryForJob tsh : hires ){

				System.out.println("  ajob category  ::::::::::   "+tsh.getJobOrder().getJobCategoryMaster().getJobCategoryName());

				if(jobcat.containsKey(tsh.getJobOrder().getJobCategoryMaster().getJobCategoryName())){
					jobcat.put(tsh.getJobOrder().getJobCategoryMaster().getJobCategoryName() , jobcat.get(tsh.getJobOrder().getJobCategoryMaster().getJobCategoryName() ) + 1);
				}
				else{
					jobcat.put(tsh.getJobOrder().getJobCategoryMaster().getJobCategoryName() , 1);
				} 
			}

			System.out.println(jobcat);
			jobcat = sortByValues(jobcat);
			System.out.println(jobcat);


			int counter=1;
			resultmap.put("top1CatName", "");
			resultmap.put("top2CatName", "");
			resultmap.put("top3CatName", "");
			resultmap.put("top1CatVal", "0");
			resultmap.put("top2CatVal", "0");
			resultmap.put("top3CatVal", "0");


			for (Map.Entry<String, Integer> entry : jobcat.entrySet())
			{
				System.out.println("====="+entry.getKey() +"---------"+entry.getValue());
				if(counter<=3){

					if(counter==1){
						resultmap.put("top1CatName", ""+entry.getKey());
						resultmap.put("top1CatVal", ""+entry.getValue());
					}

					if(counter==2){
						resultmap.put("top2CatName", ""+entry.getKey());
						resultmap.put("top2CatVal", ""+entry.getValue());
					}

					if(counter==3){
						resultmap.put("top3CatName", ""+entry.getKey());
						resultmap.put("top3CatVal", ""+entry.getValue());
					}

					counter++;


				}
				else break;
			}



			//	 		


		}catch (Exception e) {
			e.printStackTrace();
		}

		// get active branches
		Map<Integer,BranchMaster> branchMap = new HashMap<Integer, BranchMaster>();
		for(JobOrder job:activeJobs){
			//if(null!=job.getBranchMaster())  activeBranches++;
			if(job.getBranchMaster()!=null){
				branchMap.put(job.getBranchMaster().getBranchId(), job.getBranchMaster());
			}
		}

		activeBranches = branchMap.size();
		listSize=activeJobs.size();


		//System.out.println("applicants  ::::: "+applicants.get(0));
		if(activeJobs!=null && activeJobs.size()>0){
		resultmap.put("activeJobs", ""+activeJobs.size());
		resultmap.put("activeBranches", ""+activeBranches);
		resultmap.put("totalApplicants",applicants.size()>0? ""+applicants.get(0):""+0);
		resultmap.put("hires", ""+hires.size());
		}
		else{
			resultmap.put("activeJobs", "0");
			resultmap.put("activeBranches", "0");
			resultmap.put("totalApplicants","0");
			resultmap.put("hires", "0");
		}


		System.out.println(resultmap);

		if(session!=null)session.clear();
		if(session!=null)session.close();



		return resultmap;   
	}
	
	private static Date firstDayOfMonth(Date date) {
		   Calendar calendar = Calendar.getInstance();
		   calendar.setTime(date);
		   calendar.set(Calendar.DATE, 1);
		   calendar.set(Calendar.HOUR_OF_DAY,0);
		   calendar.set(Calendar.MINUTE,0);
		   calendar.set(Calendar.SECOND,0);
		   return calendar.getTime();
		}
	
	
	
	private static Date firstDayOfWeek(Date date) {
		   Calendar calendar = Calendar.getInstance();
		   calendar.setTime(date);
		   calendar.set(Calendar.DAY_OF_WEEK, 2);
		   
		   calendar.set(Calendar.HOUR_OF_DAY,0);
		   calendar.set(Calendar.MINUTE,0);
		   calendar.set(Calendar.SECOND,0);
		   
		   
		   return calendar.getTime();
		}
	
	
	
	public static <K extends Comparable,V extends Comparable> Map<K,V> sortByValues(Map<K,V> map){
        List<Map.Entry<K,V>> entries = new LinkedList<Map.Entry<K,V>>(map.entrySet());
      
        Collections.sort(entries, new Comparator<Map.Entry<K,V>>() {

        	@Override
			public int compare(Entry<K, V> o1, Entry<K, V> o2) {
				return 0;
			}
        });
      
        //LinkedHashMap will keep the keys in the order they are inserted
        //which is currently sorted on natural ordering
        Map<K,V> sortedMap = new LinkedHashMap<K,V>();
        ListIterator li = entries.listIterator(entries.size());
        while(li.hasPrevious()){
        	Map.Entry<K,V> entry = (Entry<K, V>) li.previous();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
      
        return sortedMap;
    }
	/*public void saveJobOrderLog(int jobId)
	{
		try
		{
			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			SessionFactory sessionFactory = jobOrderLogDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			Transaction txOpen = statelesSsession.beginTransaction();
			JobOrderLog jobOrderLog=new JobOrderLog();
			
			BeanUtils.copyProperties(jobOrderLog, jobOrder);
			jobOrderLog.setJobOrder(jobOrder);
			System.out.println(jobOrder.getJobId());
			System.out.println(jobOrder.getJobTitle());
			//jobOrderLog.setCreatedByEntity(userMaster.getEntityType());
			
			try {
				txOpen.begin();
				statelesSsession.insert(jobOrderLog);
				txOpen.commit();
				statelesSsession.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}*/
	
	
	public String ExportKellyBranchToExcel(String hqId,String branchId,String status_filter,int entityID){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		HeadQuarterMaster headQuarterMaster = null;
		StringBuffer tmRecords =	new StringBuffer();
		int tempBranchId=0;
		List<BranchMaster> branchMasterList=new ArrayList<BranchMaster>();
		if(branchId!=null && !branchId.trim().equals(""))
			tempBranchId=Integer.parseInt(branchId);	
		int totaRecord 		=	0;
		String fileName     = null;
		//------------------------------------
		BranchMaster branchMaster=null;
		UserMaster userMaster = null;
		int headQuarterId =0;
		int entityIDLogin=0;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()==null){				
				headQuarterId =userMaster.getHeadQuarterMaster().getHeadQuarterId();
			}if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		entityIDLogin=userMaster.getEntityType();
			}if(headQuarterId!=0 && entityIDLogin==5){
			entityID=entityIDLogin;
		}	 
		
		try{
		if(headQuarterId!=0){
			headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
		}
		List<BranchMaster> totalBranches = branchMasterDAO.getBranchesByHeadQuarter(headQuarterMaster,status_filter);
		branchMasterList = branchMasterDAO.findBranchesWithHeadQuarterForExcel(headQuarterMaster,tempBranchId,status_filter);
		if(branchId!=null && !branchId.trim().equals(""))
			totaRecord = branchMasterList.size();
		else
			totaRecord = totalBranches.size();
			System.out.println(" total branch Record in export to excel are:::::::: "+totaRecord);
			
			 //  Excel   Exporting				
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			//String basePath = request.getRealPath("/")+"/candidate";
			String basePath = request.getSession().getServletContext().getRealPath ("/")+"/manageBranches";
			fileName ="managebranches"+time+".xls";
			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();
			Utility.deleteAllFileFromDir(basePath);
			file = new File(basePath+"/"+fileName);
			WorkbookSettings wbSettings = new WorkbookSettings();
			wbSettings.setLocale(new Locale("en", "EN"));
			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;

			WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
			workbook.createSheet("profilevisitreport", 0);
			WritableSheet excelSheet = workbook.getSheet(0);
			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);// Define the cell format
			times = new WritableCellFormat(times10pt);
			times.setWrap(true);
			// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);
			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);
			header.setBackground(Colour.GRAY_25);

			// Write headers
			excelSheet.mergeCells(0, 0, 7, 1);
			Label label;
			label = new Label(0, 0, "Manage Branch Area", timesBoldUnderline);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 3, 7, 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			excelSheet.getSettings().setDefaultColumnWidth(18);
			//header's names
			int k=4;
			int col=1;
			label = new Label(0, k, Utility.getLocaleValuePropByKey("lblBranchName", locale),header); 
			excelSheet.addCell(label);
			
			label = new Label(1, k, Utility.getLocaleValuePropByKey("PortfolioField_16_State", locale),header); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblFinalDecisionMaker", locale),header); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "# of Districts",header); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgCityName", locale),header); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "# of Schools",header); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblCreatedOn", locale),header); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, "Status",header); 
			excelSheet.addCell(label);
			
			
			k=k+1;
			if(branchMasterList.size()==0)
			{	
				excelSheet.mergeCells(0, k, 9, k);
				label = new Label(0, k, Utility.getLocaleValuePropByKey("msgNorecordfound", locale)); 
				excelSheet.addCell(label);
			}
			if(branchMasterList.size()>0){
				int kk=0;
				for (BranchMaster bm:branchMasterList) 					            
				{				
			    	col=0;		
			    	// branch name
				    label = new Label(0, k,bm.getBranchName().toString()); 
					excelSheet.addCell(label);
					//state
				    String  myVal1 = (bm.getStateMaster().getStateName()==null)? " " : bm.getStateMaster().getStateName().toString();
					label = new Label(++col, k, myVal1); 
					excelSheet.addCell(label);
					// final dicision maker
					String  myVal2 = ((bm.getHeadQuarterMaster().getHeadQuarterName()==null)||(bm.getHeadQuarterMaster().getHeadQuarterName().toString().length()==0))? " " : bm.getHeadQuarterMaster().getHeadQuarterName().toString();
					label = new Label(++col, k, myVal2); 
					excelSheet.addCell(label);
					//total no. of district
					String  myVal3 = ((bm.getTotalNoOfDistricts()==null)||(bm.getTotalNoOfDistricts().toString().length()==0))? " " : bm.getTotalNoOfDistricts().toString();
					label = new Label(++col, k, myVal3); 
					excelSheet.addCell(label);
					// city name
					String  myVal4 = ((bm.getCityName()==null)||(bm.getCityName().toString().length()==0))? "0" : bm.getCityName().toString();
					label = new Label(++col, k, myVal4); 
					excelSheet.addCell(label);
					//no of school					
					String  myVal5 = "N/A";
					label = new Label(++col, k, myVal5); 
					excelSheet.addCell(label);
					//created on
					String  myVal7 = ((Utility.convertDateAndTimeToUSformatOnlyDate(bm.getCreatedDateTime())==null)||(Utility.convertDateAndTimeToUSformatOnlyDate(bm.getCreatedDateTime()).toString().length()==0))? "0" : Utility.convertDateAndTimeToUSformatOnlyDate(bm.getCreatedDateTime()).toString();
					label = new Label(++col, k, myVal7); 
					excelSheet.addCell(label);
					//status
					String status="";
					if(bm.getStatus().equalsIgnoreCase("A")){
						status="Active";
					}else{
						status="Inactive";
					}
					String  myVal8 = ((status==null)||(status.toString().length()==0))? "0" : status.toString();
					label = new Label(++col, k, myVal8); 
					excelSheet.addCell(label);
				   k++; 			 
				}
			}
			workbook.write();
			workbook.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}	
	
	//export to pdf	
	public String ExportKellyBranchesToPDF(String hqId,String branchId,String status_filter,int entityID){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		HeadQuarterMaster headQuarterMaster = null;
		StringBuffer tmRecords =	new StringBuffer();
		int tempBranchId=0;
		List<BranchMaster> branchMasterList=new ArrayList<BranchMaster>();	
		if(branchId!=null && !branchId.trim().equals(""))
			tempBranchId=Integer.parseInt(branchId);		
		int totaRecord 		=	0;		
		BranchMaster branchMaster=null;
		UserMaster userMaster = null;
		int headQuarterId =0;
		int entityIDLogin=0;
		int roleId=0;
		String fileName="";
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()==null){				
				headQuarterId =userMaster.getHeadQuarterMaster().getHeadQuarterId();
			}			
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			entityIDLogin=userMaster.getEntityType();
		}				
		if(headQuarterId!=0 && entityIDLogin==5){
			entityID=entityIDLogin;
		}
		try{
		if(headQuarterId!=0){
			headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
		}
		List<BranchMaster> totalBranches = branchMasterDAO.getBranchesByHeadQuarter(headQuarterMaster,status_filter);
		branchMasterList = branchMasterDAO.findBranchesWithHeadQuarterForExcel(headQuarterMaster,tempBranchId,status_filter);
		if(branchId!=null && !branchId.trim().equals(""))
			totaRecord = branchMasterList.size();
		else
			totaRecord = totalBranches.size();
			System.out.println(" total branch Record in export to pdf are:::::::: "+totaRecord);
			
			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/manageBranches/";
			fileName =time+"BranchReport.pdf";
			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();
			Utility.deleteAllFileFromDir(basePath);
			generateCandidatePDReport(hqId, branchId,basePath+"/"+fileName,context.getServletContext().getRealPath("/"), branchMasterList);
		}
		catch (Exception e) {
		e.printStackTrace();
		}	
		return "manageBranches/"+fileName;
	}
	
	public boolean generateCandidatePDReport(String hqId,String branchId,String path,String realPath, List<BranchMaster> branchMasterList){
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;	
		Font font8 = null;
		Font font8Green = null;
		Font font8bold = null;
		Font font9 = null;
		Font font9bold = null;
		Font font10 = null;
		Font font10_10 = null;
		Font font10bold = null;
		Font font11 = null;
		Font font11bold = null;
		Font font20bold = null;
		Font font11b   =null;
		Color bluecolor =null;
		Font font8bold_new = null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		int sortingcheck=1;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");
					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();			
					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}
					entityID=userMaster.getEntityType();
				}
				//for pdf
				 String fontPath = realPath;
				     try {							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);

							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);
						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
					document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
					document.addAuthor("TeacherMatch");
					document.addCreator("TeacherMatch Inc.");
					document.addSubject("Manage Branch Area");
					document.addCreationDate();
					document.addTitle("Manage Branch Area");

					fos = new FileOutputStream(path);
					PdfWriter.getInstance(document, fos);
					document.open();
					PdfPTable mainTable = new PdfPTable(1);
					mainTable.setWidthPercentage(90);
					Paragraph [] para = null;
					PdfPCell [] cell = null;

					para = new Paragraph[3];
					cell = new PdfPCell[3];
					para[0] = new Paragraph(" ",font20bold);
					cell[0]= new PdfPCell(para[0]);
					cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell[0].setBorder(0);
					mainTable.addCell(cell[0]);
						
					Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
					logo.scalePercent(75);

					cell[1]= new PdfPCell(logo);
					cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[1].setBorder(0);
					mainTable.addCell(cell[1]);
					document.add(new Phrase("\n"));
					
					para[2] = new Paragraph("",font20bold);
					cell[2]= new PdfPCell(para[2]);
					cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[2].setBorder(0);
					mainTable.addCell(cell[2]);
					document.add(mainTable);
					document.add(new Phrase("\n"));
						
					float[] tblwidthz={.15f};
					mainTable = new PdfPTable(tblwidthz);
					mainTable.setWidthPercentage(100);
					para = new Paragraph[1];
					cell = new PdfPCell[1];
					
					para[0] = new Paragraph("Manage Branch Area",font20bold);
					cell[0]= new PdfPCell(para[0]);
					cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[0].setBorder(0);
					mainTable.addCell(cell[0]);
					document.add(mainTable);
					document.add(new Phrase("\n"));
					
			        float[] tblwidths={.15f,.20f};
					mainTable = new PdfPTable(tblwidths);
					mainTable.setWidthPercentage(100);
					para = new Paragraph[2];
					cell = new PdfPCell[2];

					String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
					para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
					cell[0]= new PdfPCell(para[0]);
					cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
					cell[0].setBorder(0);
					mainTable.addCell(cell[0]);
					
					para[1] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
					cell[1]= new PdfPCell(para[1]);
					cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell[1].setBorder(0);
					mainTable.addCell(cell[1]);						
					document.add(mainTable);						
					document.add(new Phrase("\n"));
					float[] tblwidth={.22f,.15f,.18f,.08f,.12f,.15f,.09f,.06f};						
					mainTable = new PdfPTable(tblwidth);
					mainTable.setWidthPercentage(100);
					para = new Paragraph[8];
					cell = new PdfPCell[8];
						
				// header
					para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblBranchName", locale),font10_10);
					cell[0]= new PdfPCell(para[0]);
					cell[0].setBackgroundColor(bluecolor);
					cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[0]);
					
					para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("PortfolioField_16_State", locale),font10_10);
					cell[1]= new PdfPCell(para[1]);
					cell[1].setBackgroundColor(bluecolor);
					cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[1]);
					
					para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblFinalDecisionMaker", locale),font10_10);
					cell[2]= new PdfPCell(para[2]);
					cell[2].setBackgroundColor(bluecolor);
					cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[2]);
					
					para[3] = new Paragraph("# of Districts",font10_10);
					cell[3]= new PdfPCell(para[3]);
					cell[3].setBackgroundColor(bluecolor);
					cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[3]);

					para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgCityName", locale),font10_10);
					cell[4]= new PdfPCell(para[4]);
					cell[4].setBackgroundColor(bluecolor);
					cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[4]);
					
					para[5] = new Paragraph("# of Schools",font10_10);
					cell[5]= new PdfPCell(para[5]);
					cell[5].setBackgroundColor(bluecolor);
					cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[5]);
					
					para[6] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale),font10_10);
					cell[6]= new PdfPCell(para[6]);
					cell[6].setBackgroundColor(bluecolor);
					cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[6]);
					
					para[7] = new Paragraph("Status",font10_10);
					cell[7]= new PdfPCell(para[7]);
					cell[7].setBackgroundColor(bluecolor);
					cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[7]);						
					document.add(mainTable);
						
					if(branchMasterList.size()==0){
					     float[] tblwidth11={.10f};								
						 mainTable = new PdfPTable(tblwidth11);
						 mainTable.setWidthPercentage(100);
						 para = new Paragraph[1];
						 cell = new PdfPCell[1];							
						 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgNorecordfound", locale),font8bold);
						 cell[0]= new PdfPCell(para[0]);
						 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[0]);							
					     document.add(mainTable);
					}
					if(branchMasterList.size()>0){
						 for(BranchMaster bm :branchMasterList) 
						 {
							 int index=0;
							 mainTable = new PdfPTable(tblwidth);
							 mainTable.setWidthPercentage(100);
							 para = new Paragraph[8];
							 cell = new PdfPCell[8];
							 // Branch NAme
							 para[index] = new Paragraph(""+bm.getBranchName(),font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
							 // state
							 String  myVal1 = (bm.getStateMaster().getStateName()==null)? " " : bm.getStateMaster().getStateName().toString();
							 para[index] = new Paragraph(myVal1,font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
					    	 // final dicision maker
							 String  myVal2 = ((bm.getHeadQuarterMaster().getHeadQuarterName()==null)||(bm.getHeadQuarterMaster().getHeadQuarterName().toString().length()==0))? " " : bm.getHeadQuarterMaster().getHeadQuarterName().toString();
							 para[index] = new Paragraph(myVal2,font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
							 //total no. of district
							 String  myVal3 = ((bm.getTotalNoOfDistricts()==null)||(bm.getTotalNoOfDistricts().toString().length()==0))? " " : bm.getTotalNoOfDistricts().toString();
							 para[index] = new Paragraph(myVal3,font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
							 // city name
							 String  myVal4 = ((bm.getCityName()==null)||(bm.getCityName().toString().length()==0))? "0" : bm.getCityName().toString();
							 para[index] = new Paragraph(myVal4,font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
							 //no of school					
							 String  myVal5 = "N/A";
							 para[index] = new Paragraph(myVal5,font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;									
							 //created on
							 String  myVal7 = ((Utility.convertDateAndTimeToUSformatOnlyDate(bm.getCreatedDateTime())==null)||(Utility.convertDateAndTimeToUSformatOnlyDate(bm.getCreatedDateTime()).toString().length()==0))? "0" : Utility.convertDateAndTimeToUSformatOnlyDate(bm.getCreatedDateTime()).toString();
							 para[index] = new Paragraph(myVal7,font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;									
							 //status
							 String status="";
							 if(bm.getStatus().equalsIgnoreCase("A"))
								status="Active";
							 else
								status="Inactive";								 								
							 String  myVal8 = ((status==null)||(status.toString().length()==0))? "0" : status.toString();
							 para[index] = new Paragraph(myVal8,font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;								
					         document.add(mainTable);
				   }
				}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}			
			return true;
	}
	
	//for print
	public String KellyManageBranchesPrintPre(String hqId,String branchId,String status_filter,int entityID,int flag)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		HeadQuarterMaster headQuarterMaster = null;
		StringBuffer tmRecords =	new StringBuffer();
		int tempBranchId=0;
		List<BranchMaster> branchMasterList=new ArrayList<BranchMaster>();
		if(branchId!=null && !branchId.trim().equals(""))
			tempBranchId=Integer.parseInt(branchId);	
		int totaRecord 		=	0;
		String fileName     = null;
		//------------------------------------
		BranchMaster branchMaster=null;
		UserMaster userMaster = null;
		int headQuarterId =0;
		int entityIDLogin=0;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()==null){				
				headQuarterId =userMaster.getHeadQuarterMaster().getHeadQuarterId();
			}if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		entityIDLogin=userMaster.getEntityType();
			}if(headQuarterId!=0 && entityIDLogin==5){
			entityID=entityIDLogin;
		}	 
		
		try{
		if(headQuarterId!=0){
			headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
		}
		List<BranchMaster> totalBranches = branchMasterDAO.getBranchesByHeadQuarter(headQuarterMaster,status_filter);
		branchMasterList = branchMasterDAO.findBranchesWithHeadQuarterForExcel(headQuarterMaster,tempBranchId,status_filter);
		if(branchId!=null && !branchId.trim().equals(""))
			totaRecord = branchMasterList.size();
		else
			totaRecord = totalBranches.size();
			System.out.println(" total branch Record in export to printing  are:::::::: "+totaRecord);
			
			
	        tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>Manage Branch Area</div><br/>");
			tmRecords.append("<div style='width:100%'>");
			tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+ Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
			tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
			tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");		
			if(flag==0)
				tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
			else
				tmRecords.append("<table  id='tblGridHired' width='100%' border='1'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblBranchName", locale)+"</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("PortfolioField_16_State", locale)+"</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblFinalDecisionMaker", locale)+"</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'># of Districts</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgCityName", locale)+"</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'># of Schools</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+"</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Status</th>");			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(branchMasterList.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			}
		String sta="";
			if(branchMasterList.size()>0){
				 for (BranchMaster bm:branchMasterList) 					            
					{
					 String myVal1="";
                     String myVal2="";
					 tmRecords.append("<tr>");
					 String val1=bm.getBranchName().toString()==null?"N/A":bm.getBranchName().toString();
					 String val2=(bm.getStateMaster().getStateName()==null)? " " : bm.getStateMaster().getStateName().toString();
					 String val3=((bm.getHeadQuarterMaster().getHeadQuarterName()==null)||(bm.getHeadQuarterMaster().getHeadQuarterName().toString().length()==0))? " " : bm.getHeadQuarterMaster().getHeadQuarterName().toString();
					 String val4=((bm.getTotalNoOfDistricts()==null)||(bm.getTotalNoOfDistricts().toString().length()==0))? " " : bm.getTotalNoOfDistricts().toString();
					 String val5=((bm.getCityName()==null)||(bm.getCityName().toString().length()==0))? "0" : bm.getCityName().toString();
					 String val6= "N/A";
					 String val7=((Utility.convertDateAndTimeToUSformatOnlyDate(bm.getCreatedDateTime())==null)||(Utility.convertDateAndTimeToUSformatOnlyDate(bm.getCreatedDateTime()).toString().length()==0))? "0" : Utility.convertDateAndTimeToUSformatOnlyDate(bm.getCreatedDateTime()).toString();
					 String status="";
						if(bm.getStatus().equalsIgnoreCase("A")){
							status="Active";
						}else{
							status="Inactive";
						}
						 String val8=((status==null)||(status.toString().length()==0))? "0" : status.toString();
					tmRecords.append("<td style='font-size:12px;'>"+val1+"</td>");						
					tmRecords.append("<td style='font-size:12px;'>"+val2+"</td>");
					tmRecords.append("<td style='font-size:12px;'>"+val3+"</td>");
					tmRecords.append("<td style='font-size:12px;'>"+val4+"</td>");
			      	tmRecords.append("<td style='font-size:12px;'>"+ val5+"</td>");	
					tmRecords.append("<td style='font-size:12px;'>"+val6+"</td>");
					tmRecords.append("<td style='font-size:12px;'>"+val7+"</td>");
					tmRecords.append("<td style='font-size:12px;'>"+val8+"</td>");
					tmRecords.append("</tr>");
			   }
			}
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
	 return tmRecords.toString();
	}
}