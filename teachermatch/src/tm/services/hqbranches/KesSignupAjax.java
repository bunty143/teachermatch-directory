package tm.services.hqbranches;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.ExclusivePeriodMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.services.PaginationAndSorting;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class KesSignupAjax {

	
	@Autowired
	TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	UserMasterDAO userMasterDAO;
	
	@Autowired
	ExclusivePeriodMasterDAO exclusivePeriodMasterDAO;
	
	@Autowired
	JobOrderDAO jobOrderDAO;
	
	@Autowired
	TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	BranchMasterDAO branchMasterDAO;
	
	@Autowired
	StatusMasterDAO statusMasterDAO;
	
	@Autowired 
	EmailerService emailerService;
	
	@Autowired
	RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	
	
	@Autowired
	MessageToTeacherDAO messageToTeacherDAO;
	
	@Autowired
	HeadQuarterMasterDAO headQuarterMasterDAO;
	
	
	String locale = Utility.getValueOfPropByKey("locale");	 
	public String doSignup(String fname,String lname, String email, String password,Integer branchId, Integer jobId ,String refType) {
		
		String result = "native";
		String mailTextInfo = "";
		Integer teacherId=0;
		System.out.println(" Basic /signup.do Post");
		try 
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(email);
			//List<UserMaster> lstUserMaster = userMasterDAO.findByEmail(email);
			JobOrder jobOrder = null;
			if(refType==null || refType.length()==0)
				refType = "N";
			
			if(lstTeacherDetails !=null && lstTeacherDetails.size()==0) {
			TeacherDetail teacherDetail = new TeacherDetail();

			int authorizationkey=(int) Math.round(Math.random() * 2000000);
			HttpSession session = request.getSession();
			BranchMaster branchMaster = branchMasterDAO.findById(branchId, false, false);
			teacherDetail.setFirstName(fname);
			teacherDetail.setLastName(lname);
			teacherDetail.setEmailAddress(email);
			teacherDetail.setUserType(refType);
			teacherDetail.setPassword(MD5Encryption.toMD5(password));	
			teacherDetail.setFbUser(false);
			teacherDetail.setQuestCandidate(0);
			teacherDetail.setAuthenticationCode(""+authorizationkey);
			teacherDetail.setVerificationCode(""+authorizationkey);
			teacherDetail.setVerificationStatus(0);
			teacherDetail.setIsPortfolioNeeded(true);
			teacherDetail.setNoOfLogin(0);
			teacherDetail.setStatus("A");
			teacherDetail.setSendOpportunity(false);
			teacherDetail.setIsResearchTeacher(false);
			teacherDetail.setForgetCounter(0);
			teacherDetail.setCreatedDateTime(new Date());
			teacherDetail.setIpAddress(IPAddressUtility.getIpAddress(request));
			teacherDetail.setInternalTransferCandidate(false);
			teacherDetail.setSmartPracticesCandidate(null);
			teacherDetail.setAssociatedBranchId(branchMaster);
		
			teacherDetailDAO.makePersistent(teacherDetail);
			teacherId=teacherDetail.getTeacherId();
			
			
			System.out.println("jobId :: "+jobId);
			if(jobId!=null && jobId!=0 && jobId>0){
				jobOrder = jobOrderDAO.findById(jobId, false, false); 
				JobForTeacher jobForTeacher = jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
				StatusMaster statusMaster = null;
				if(jobForTeacher==null)
				{
					jobForTeacher = new JobForTeacher();
					jobForTeacher.setTeacherId(teacherDetail);
					jobForTeacher.setJobId(jobOrder);
					try{
						int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
						statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
					}catch(Exception ex){
						statusMaster = WorkThreadServlet.statusMap.get("icomp");
					}
					if(statusMaster!=null){
						jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
					}
					jobForTeacher.setStatus(statusMaster);
					jobForTeacher.setStatusMaster(statusMaster);
					jobForTeacher.setCreatedDateTime(new Date());
					jobForTeacherDAO.makePersistent(jobForTeacher);	
				}
			}
			
			try {	
				Properties  properties = new Properties();  
				InputStream inputStream = new Utility().getClass().getClassLoader().getResourceAsStream("smtpmail.properties");
				properties.load(inputStream);
				
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom(properties.getProperty("smtphost.noreplyusername"));
				dsmt.setMailto(teacherDetail.getEmailAddress());
				dsmt.setMailsubject("Welcome to "+branchMaster.getHeadQuarterMaster().getHeadQuarterName()+Utility.getLocaleValuePropByKey("msgAuthMailSubject", locale));
				String content=MailText.getRegistrationMailForKellyUrlJobSpecific(request,teacherDetail,jobOrder,refType);					
				dsmt.setMailcontent(content);

				System.out.println("content     "+content);
				
				mailTextInfo = "You were sent an authentication email to the following email address:("+teacherDetail.getEmailAddress()+"). Please click the link provided in the email to complete the signup process and log in.";
				try {
					dsmt.start();	
				} catch (Exception e) {}
				
				/* Insert mail in log By Sekhar  */
				try{
						List<UserMaster> userMasters= userMasterDAO.findByEmail("hq@teachermatch.com");
						try{
							if(userMasters==null || userMasters.size()==0){
								userMasters= userMasterDAO.getAllHeadquarterAdmins();
							}
						}catch(Exception e){}
						MessageToTeacher  messageToTeacher= new MessageToTeacher();
						messageToTeacher.setTeacherId(teacherDetail);
						if(jobOrder!=null){
							messageToTeacher.setJobId(jobOrder);
						}
						messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
						messageToTeacher.setMessageSubject("Welcome to "+branchMaster.getHeadQuarterMaster().getHeadQuarterName()+Utility.getLocaleValuePropByKey("msgAuthMailSubject", locale));
						messageToTeacher.setMessageSend(content);
						messageToTeacher.setSenderId(userMasters.get(0));
						messageToTeacher.setSenderEmailAddress(userMasters.get(0).getEmailAddress());
						messageToTeacher.setEntityType(userMasters.get(0).getEntityType());
						messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
						messageToTeacher.setImportType("A");
						messageToTeacherDAO.makePersistent(messageToTeacher);
				}catch(Exception e){
					e.printStackTrace();
				}
				/* End mail in log by Sekhar */
			} catch (Exception e) {
				e.printStackTrace();
			}
				result="success";	
			}	
			else{
				result="duplicate";
			}
		}
		 
		catch (Exception e) 
		{
			result="failure";
			e.printStackTrace();
		}

		return result+"#"+refType+"#"+mailTextInfo+"#"+teacherId;
	}
        public String searchBranchLookup(String stateId,String keyWord,String noOfRow, String pageNo,String sortOrder,String sortOrderType) {
        	
        	StringBuffer sb =	new StringBuffer();
			String responseText="";
		try 
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			
			
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
	   		int pgNo 			= 	Integer.parseInt(pageNo);
	   		String sortOrderFieldName	=	"branchName";
	   		Order  sortOrderStrVal		=	null;		
	   		if(!sortOrder.equals("") && !sortOrder.equals(null))
	   		{
	   			sortOrderFieldName		=	sortOrder;
	   		}
	   		
	   		String sortOrderTypeVal="0";
	   		if(!sortOrderType.equals("") && !sortOrderType.equals(null))
	   		{
	   			if(sortOrderType.equals("0"))
	   			{
	   				sortOrderStrVal		=	Order.asc(sortOrderFieldName);
	   			}
	   			else
	   			{
	   				sortOrderTypeVal	=	"1";
	   				sortOrderStrVal		=	Order.desc(sortOrderFieldName);
	   			}
	   		}
	   		else
	   		{
	   			sortOrderTypeVal		=	"0";
	   			sortOrderStrVal			=	Order.asc(sortOrderFieldName);
	   		}
			
			List<BranchMaster> branchMasterList = new ArrayList<BranchMaster>();
			boolean branchFlag=false;
			int count=0;
			
			List<BranchMaster> stateBranchList = new ArrayList<BranchMaster>();
			boolean stateFlag=false;		
			if(stateId!=null && !stateId.equals("") && !stateId.equals("0"))
			{
				StateMaster stateMaster1 = stateMasterDAO.findById(Long.valueOf(stateId), false, false);
				stateBranchList = branchMasterDAO.getBranchByState(stateMaster1);
				stateFlag=true;
				System.out.println("stateId=="+stateId);
			}else{
				count++;
			}
			if(stateFlag)
			{
				if(branchFlag)
				branchMasterList.retainAll(stateBranchList);
				else
				branchMasterList.addAll(stateBranchList);
			}
			if(stateFlag)
			branchFlag=true;
							
			List<BranchMaster> keyWordBranchList = new ArrayList<BranchMaster>();
			boolean KeyWordFlag=false;
			if(keyWord!=null && !keyWord.equals(""))
			{
				keyWordBranchList = branchMasterDAO.getBranchByKeyWord(keyWord);
				KeyWordFlag=true;
				System.out.println("keyWord=="+keyWord);
			}else{
				count++;
			}
			if(KeyWordFlag)
			{
				if(branchFlag)
				branchMasterList.retainAll(keyWordBranchList);
				else
				branchMasterList.addAll(keyWordBranchList);
			}
			if(KeyWordFlag)
			branchFlag=true;
			
			HeadQuarterMaster hq=headQuarterMasterDAO.findById(1, false, false);
			System.out.println("Count=="+count);
			
			List<BranchMaster> branchMasterList1 = new ArrayList<BranchMaster>();
			if(branchMasterList.size()>0){
				branchMasterList1=branchMasterDAO.getBranchBySorting(branchMasterList, sortOrderStrVal);
				
			}else if(count==2){
				branchMasterList1=branchMasterDAO.getAllBranches(hq, sortOrderStrVal);				
			}
			System.out.println("branchMasterList1=="+branchMasterList1.size());
				
				    sb.append("<table id='branchLookupTable' width='100%'>");
	   				sb.append("<thead class='bg'>");
	   				sb.append("<tr>");



					responseText=PaginationAndSorting.responseSortingLink("Branch Name",sortOrderFieldName,"branchName",sortOrderTypeVal,pgNo);
	   				sb.append("<th valign='top'>"+responseText+"</th>");
	   				
	   				responseText="City Name";
	   				sb.append("<th valign='top'>"+responseText+"</th>");
	   				
	   				responseText=PaginationAndSorting.responseSortingLink("StateName",sortOrderFieldName,"branchName",sortOrderTypeVal,pgNo);
	   				sb.append("<th valign='top'>"+responseText+"</th>");

					responseText="Action";
	   				sb.append("<th valign='top'>"+responseText+"</th>");

					sb.append("</tr>");
	   			    sb.append("</thead>");
	   			        
	   		 if(branchMasterList1.size()>0){
	   			 
//	   			 Set<BranchMaster> setbranchMasterList = new HashSet<BranchMaster>(branchMasterList);
//				 List<BranchMaster> branchMasterList1 = new ArrayList<BranchMaster>(setbranchMasterList);
//				 branchMasterList1=branchMasterDAO.getBranchBySorting(branchMasterList1, sortOrderStrVal);
	   			 
	   			 for(BranchMaster branch : branchMasterList1)
	   			 {
	   			
	   			    sb.append("<tr>");
	   			    sb.append("<td>");
	   			    sb.append(branch.getBranchName());
	   			    sb.append("</td>");
	   			    
	   			    
	   			    sb.append("<td>");
	   			    if(branch.getCityName()!="" || branch.getCityName()!=null)
	   			    sb.append(branch.getCityName());
	   			    else
	   			     sb.append("N/A");
	   			    	
	   			    sb.append("</td>");
	   			    
	   			    
	   			    sb.append("<td>");
	   			    if(branch.getStateMaster()!=null)
	   			    sb.append(branch.getStateMaster().getStateName());
	   			    else
	   			    sb.append("N/A");
	   			   
	   			    sb.append("</td>");
	   			    
	   			   
	   			    sb.append("<td>");
	   			    sb.append("<a href='javascript:void(0)' onclick=\"selectBranchLookup('"+branch.getBranchId()+"','"+branch.getBranchName()+"');\">Select</a>");
	   			    sb.append("</td>");
	   			 }
				
			}else{
				
				    sb.append("<tr>");
			    	sb.append("<td>");
			    	sb.append("No Record Found");
			    	sb.append("<td>");
			    	sb.append("</tr>");
				
			}
			sb.append("</table>");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
        }

}
