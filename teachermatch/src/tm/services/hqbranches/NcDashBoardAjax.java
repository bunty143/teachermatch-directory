package tm.services.hqbranches;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobOrder;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.utility.Utility;

public class NcDashBoardAjax {
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;

	@Autowired
	private JobOrderDAO jobOrderDAO;
 	
 	@Autowired
 	private JobForTeacherDAO jobForTeacherDAO;
 	
 	@Autowired
 	private SchoolMasterDAO schoolMasterDAO;
 	
 	@Autowired
 	private UserLoginHistoryDAO userLoginHistoryDAO;
 	
 	

 	public String getTotalJobsAndCandidateDetail(Integer headQuartetId)
 	{
 		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
 		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String result="";
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try{
			
			UserMaster userMaster = null;
			HeadQuarterMaster headQuarterMaster=null;
			DistrictMaster districtMaster=null;
			boolean isHeadQuarter=false;
			int totalActiveJob=0;
			int noofHire=0;
			
			List<DistrictMaster> listDistrictMasters=new  ArrayList<DistrictMaster>(); 
			List<JobOrder> lstJobOrders=new ArrayList<JobOrder>();
			
			userMaster=(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getEntityType()!=null && userMaster.getEntityType().equals(5)){
				isHeadQuarter=true;
			}
			
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			
			if(isHeadQuarter){
				headQuarterMaster=userMaster.getHeadQuarterMaster();
				listDistrictMasters=districtMasterDAO.getActiveDistrictListByHeadQuater(headQuarterMaster);
			}else{
				listDistrictMasters.add(districtMaster);
			}
			lstJobOrders=jobOrderDAO.findJobOrderbyDistrictList(listDistrictMasters);
			
			//Jobs Posted
			if(lstJobOrders!=null && lstJobOrders.size()>0){
				totalActiveJob=lstJobOrders.size();
				
				result=totalActiveJob+"#";
				
				//Positions Available	
				for(JobOrder job : lstJobOrders){
					System.out.println(job.getJobId());
					if(job.getNoOfHires()!=null){
						noofHire+=job.getNoOfHires();
					}
				}
				result+=noofHire+"#";
				
				//Applications
				int applications=jobForTeacherDAO.totalAppliedApplicantsByJobOrders(lstJobOrders,null);
				
				result+=applications+"#";
				
				int uniqueApplications=jobForTeacherDAO.totalUniqueAppliedApplicantsByJobOrders(lstJobOrders);
				result+=uniqueApplications;
				
			}else{
				result="0#0#0#0";
			}
			
		}catch (Exception e) {
			result="0#0#0#0";
			e.printStackTrace();
		}
 		
 		return result;
 	}

	public Object[] positionFilledAndHiredChart(Integer headQuartetId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		Map<Integer,Integer> mapOfDistrictWiseHired=null;
		
		Object objData[]=new Object[5];
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			UserMaster userMaster = null;
			HeadQuarterMaster headQuarterMaster=null;
			DistrictMaster districtMaster=null;
			
			int noofHire=0;			
			boolean isHeadQuarter=false;
			List<DistrictMaster> listDistrictMasters=new  ArrayList<DistrictMaster>(); 
			List<JobOrder> lstJobOrders=new ArrayList<JobOrder>();

			userMaster=(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getEntityType()!=null && userMaster.getEntityType().equals(5)){
				isHeadQuarter=true;
			}
			
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			
			if(isHeadQuarter){
				headQuarterMaster=userMaster.getHeadQuarterMaster();
				listDistrictMasters=districtMasterDAO.getActiveDistrictListByHeadQuater(headQuarterMaster);
			}else{
				listDistrictMasters.add(districtMaster);
			}
			 System.out.println(listDistrictMasters.size());
			lstJobOrders=jobOrderDAO.findJobOrderbyDistrictList(listDistrictMasters);
			System.out.println(lstJobOrders.size());
			
			Map<Integer,Integer> districtWiseOpening =new HashMap<Integer, Integer>();
			
			//Positions Available	
			for(JobOrder job : lstJobOrders)
			{
				if(job.getNoOfHires()!=null){
					noofHire+=job.getNoOfHires();	//counting total
				}
			  //Calculation of district wise opening	
				if(job.getDistrictMaster()!=null)
				{
					int distHire=0;
					if(districtWiseOpening.get(job.getDistrictMaster().getDistrictId())!=null)
					{
						distHire=districtWiseOpening.get(job.getDistrictMaster().getDistrictId());
						if(job.getNoOfHires()!=null){
							distHire+=job.getNoOfHires();
						}
						districtWiseOpening.put(job.getDistrictMaster().getDistrictId(), distHire);
					}else{
						if(job.getNoOfHires()!=null){
							distHire+=job.getNoOfHires();
						}
						districtWiseOpening.put(job.getDistrictMaster().getDistrictId(), distHire);
					}	
				}
			}
		
			mapOfDistrictWiseHired=jobForTeacherDAO.countApplicantsByJobOrdersMukesh(lstJobOrders);
		
			int underFilledLEADistrict=0;
			for (Entry<Integer, Integer> entry : districtWiseOpening.entrySet()) 
			{
				if(mapOfDistrictWiseHired.get(entry.getKey())!=null)
				{
					float totalHireByDistrict= mapOfDistrictWiseHired.get(entry.getKey());
					float totalHireByDistrictPer=((totalHireByDistrict*100)/districtWiseOpening.get(entry.getKey()));
					if(totalHireByDistrictPer<=20){
						underFilledLEADistrict++;
					}
				}else{
					underFilledLEADistrict++;
				}
				
					
			}
			
			
			int hiredList=jobForTeacherDAO.totalAppliedApplicantsByJobOrders(lstJobOrders,"hird");
			
			try{
				//System.out.println(("MMMMMMMMMMMMMMMMMMMMM :: "+(hiredList*100)/noofHire));
				float value=Math.round(((float)hiredList*100)/(float)noofHire);
				objData[0]=value;
				objData[1]=(noofHire-hiredList);
			}catch (Exception e) {
				objData[0]=0;
				e.printStackTrace();
			}
		
		 //Under-filled LEAs
			
			//objData[2]=underFilledLEADistrict;
			objData[2]=listDistrictMasters.size()-districtWiseOpening.size()+underFilledLEADistrict;
			objData[3]=hiredList;
			objData[4]=noofHire;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return objData;
	}
	
	
	public Object[] positionCompetitiveChart(Integer headQuartetId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		Object objData[]=new Object[5];
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			UserMaster userMaster = null;
			HeadQuarterMaster headQuarterMaster=null;
			DistrictMaster districtMaster=null;
			Map<Integer,Integer> districtWiseOpening =new HashMap<Integer, Integer>();
			
			boolean isHeadQuarter=false;
			List<DistrictMaster> listDistrictMasters=new  ArrayList<DistrictMaster>(); 
			List<JobOrder> lstJobOrders=new ArrayList<JobOrder>();
			
			userMaster=(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getEntityType()!=null && userMaster.getEntityType().equals(5)){
				isHeadQuarter=true;
			}
			
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			
			if(isHeadQuarter){
				headQuarterMaster=userMaster.getHeadQuarterMaster();
				listDistrictMasters=districtMasterDAO.getActiveDistrictListByHeadQuater(headQuarterMaster);
			}else{
				listDistrictMasters.add(districtMaster);
			}
			
			lstJobOrders=jobOrderDAO.findJobOrderbyDistrictList(listDistrictMasters);
			
			Map<Integer,String> mapofTotalAppliedCan	=	jobForTeacherDAO.countApplicantsByJobOrders(lstJobOrders);
			
			float position_Competative=0;
			try {
				position_Competative=((mapofTotalAppliedCan.size()*100)/lstJobOrders.size());	
				objData[3]=mapofTotalAppliedCan.size();
				objData[4]=lstJobOrders.size();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//Position Competative	
			objData[0]=position_Competative;
			
			// UNCOMPETATIVE
			objData[1]=(lstJobOrders.size()-mapofTotalAppliedCan.size());
			
			// Under-competitive LEAs
			int under_Competitive_LEA=0;
			if(isHeadQuarter)
			{
				
				//Positions Available	
				for(JobOrder job : lstJobOrders)
				{
				  //Calculation of district wise opening	
					if(job.getDistrictMaster()!=null)
					{
						int distHire=0;
						if(districtWiseOpening.get(job.getDistrictMaster().getDistrictId())!=null)
						{
							distHire=districtWiseOpening.get(job.getDistrictMaster().getDistrictId());
							if(job.getNoOfHires()!=null){
								distHire+=job.getNoOfHires();
							}
							districtWiseOpening.put(job.getDistrictMaster().getDistrictId(), distHire);
						}else{
							if(job.getNoOfHires()!=null){
								distHire+=job.getNoOfHires();
							}
							districtWiseOpening.put(job.getDistrictMaster().getDistrictId(), distHire);
						}	
					}
				}
			
				Map<Integer,Integer> mapOfDistrictWiseHired=jobForTeacherDAO.countApplicantsByJobOrdersMukesh(lstJobOrders);
			
				int underFilledLEADistrict=0;
				for (Entry<Integer, Integer> entry : districtWiseOpening.entrySet()) 
				{
					if(mapOfDistrictWiseHired.get(entry.getKey())!=null){
						float totalHireByDistrict= mapOfDistrictWiseHired.get(entry.getKey());
						float totalHireByDistrictPer=((totalHireByDistrict*100)/districtWiseOpening.get(entry.getKey()));
						if(totalHireByDistrictPer<=20){
							underFilledLEADistrict++;
						}
					}
					else{
						underFilledLEADistrict++;
					}
				}
				under_Competitive_LEA=underFilledLEADistrict;
			}
			
			//objData[2]=under_Competitive_LEA;
			objData[2]=listDistrictMasters.size()-districtWiseOpening.size()+under_Competitive_LEA;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return objData;
	}
	
	
	public Object[] leasActiveChart(Integer headQuartetId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		Object objData[]=new Object[5];
		float lEAs_Active=0;
		int inactive_LEAs=0;
		int under_Active_LEAs=0;
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			UserMaster userMaster = null;
			HeadQuarterMaster headQuarterMaster=null;
			DistrictMaster districtMaster=null;
			
			boolean isHeadQuarter=false;
			boolean isDistrict=false;
			List<DistrictMaster> listDistrictMasters=new  ArrayList<DistrictMaster>(); 
			List<SchoolMaster> listSchoolMasters= new ArrayList<SchoolMaster>();
			
			userMaster=(UserMaster) session.getAttribute("userMaster");
			
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getHeadQuarterMaster()!=null){
				headQuarterMaster=userMaster.getHeadQuarterMaster();	
			}
			
			if(userMaster.getEntityType()!=null && userMaster.getEntityType().equals(5)){
				isHeadQuarter=true;
			}else if(userMaster.getEntityType()!=null && userMaster.getEntityType().equals(2)){
				isDistrict=true;
			}
			
			
			
			if(isHeadQuarter)
			{
			   listDistrictMasters  =  districtMasterDAO.getActiveDistrictListByHeadQuater(headQuarterMaster);//total active district
			
			   Map<Integer,Integer> noODistrictLogin=userLoginHistoryDAO.countUserLoginByDistrict(false, listDistrictMasters, listSchoolMasters);//total login
			
			  for(Entry<Integer, Integer> entry :noODistrictLogin.entrySet()){
				System.out.println("distritId :: "+entry.getKey() +"\t"+entry.getValue());
			  }
			  
			  inactive_LEAs=listDistrictMasters.size()-noODistrictLogin.size();
			  
			  Map<Integer,Integer> noODistrictLoginMoreThan10=userLoginHistoryDAO.countUserLoginByDistrict(true, listDistrictMasters, listSchoolMasters);//having login between0 to 10
			  //under_Active_LEAs=listDistrictMasters.size()-noODistrictLoginMoreThan10.size();
			  //shadab
			  under_Active_LEAs=noODistrictLoginMoreThan10.size();
			  
		      //For LEA Active
			
			  Map<Integer,Integer> noODistrictLoginAll=userLoginHistoryDAO.countUserLoginByDistrictLEA(false, listDistrictMasters, listSchoolMasters);
			  Map<Integer,Integer> noODistrictLoginWeek=userLoginHistoryDAO.countUserLoginByDistrictLEA(true, listDistrictMasters, listSchoolMasters);//having login >=10
			  
			  //shadab
			  //if(noODistrictLoginAll!=null && noODistrictLoginAll.size()>0)
			  if(listDistrictMasters!=null && listDistrictMasters.size()>0)
			      lEAs_Active=(noODistrictLoginWeek.size()*100)/listDistrictMasters.size();
			  objData[3]=noODistrictLoginWeek.size();
			  //shadab
			  //objData[4]=noODistrictLoginAll.size();
			  objData[4]=listDistrictMasters.size();
			  
			}else if(isDistrict)
			{
				System.out.println("districtMaster :: "+districtMaster.getDistrictId());
				listSchoolMasters    =  schoolMasterDAO.findByCriteria(Restrictions.eq("districtId", districtMaster));
				System.out.println("listSchoolMasters "+listSchoolMasters.size());
				
				Map<Integer,Integer> noODistrictLogin=userLoginHistoryDAO.countUserLoginBySchool(false, listDistrictMasters, listSchoolMasters);
				Map<Integer,Integer> noODistrictLoginMoreThan10=userLoginHistoryDAO.countUserLoginBySchool(true, listDistrictMasters, listSchoolMasters);//10<loginn>0 

				inactive_LEAs=listSchoolMasters.size()-noODistrictLogin.size();
				//under_Active_LEAs=listSchoolMasters.size()-noODistrictLoginMoreThan10.size();
				under_Active_LEAs=noODistrictLoginMoreThan10.size();
				
				//For LEA Active
				
				Map<Integer,Integer> noODistrictLoginAll=userLoginHistoryDAO.countUserLoginBySchoolLEA(false, listDistrictMasters, listSchoolMasters);
				Map<Integer,Integer> noODistrictLoginWeek=userLoginHistoryDAO.countUserLoginBySchoolLEA(true, listDistrictMasters, listSchoolMasters);
				//shadab 
				//if(noODistrictLoginAll!=null && noODistrictLoginAll.size()>0)
				if(listSchoolMasters!=null && listSchoolMasters.size()>0)
					lEAs_Active=(noODistrictLoginWeek.size()*100)/listSchoolMasters.size();
				System.out.println("lEAs_ActivelEAs_Activev :: "+lEAs_Active);
				objData[3]=noODistrictLoginWeek.size();
				//shadab
				//objData[4]=noODistrictLoginAll.size();
				objData[4]=listSchoolMasters.size();
			}
			
			//LEAs active %
			objData[0]=lEAs_Active;
			
			//Inactive LEAs
			objData[1]=inactive_LEAs;
			
			//Under-active LEAs
			objData[2]=under_Active_LEAs;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return objData;
	}
	
	//List UnderFilled Distrct
	public Object[] listUnFilledDistrict(Integer headQuartetId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		Object objData[]=new Object[3];
		StringBuffer tmRecord= new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			UserMaster userMaster = null;
			HeadQuarterMaster headQuarterMaster=null;
			DistrictMaster districtMaster=null;
			
		//	int noofHire=0;			
			boolean isHeadQuarter=false;
			boolean isDistrict=false;
			List<DistrictMaster> listDistrictMasters=new  ArrayList<DistrictMaster>(); 
			List<JobOrder> lstJobOrders=new ArrayList<JobOrder>();

			userMaster=(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getEntityType()!=null && userMaster.getEntityType().equals(5)){
				isHeadQuarter=true;
			}if(userMaster.getEntityType()!=null && userMaster.getEntityType().equals(2)){
				isDistrict=true;
				districtMaster=userMaster.getDistrictId();
			}
			
			if(isHeadQuarter){
				headQuarterMaster=userMaster.getHeadQuarterMaster();
				listDistrictMasters=districtMasterDAO.getActiveDistrictListByHeadQuater(headQuarterMaster);
			}else{
				listDistrictMasters.add(districtMaster);
			}
			 
			lstJobOrders=jobOrderDAO.findJobOrderbyDistrictList(listDistrictMasters);
			
			Map<Integer,Integer> districtWiseOpening =new HashMap<Integer, Integer>();
			Map<Integer,DistrictMaster> mapOfDistrict=new HashMap<Integer, DistrictMaster>();
			Map<Integer,List<JobOrder>> districtWiseJobs=new HashMap<Integer, List<JobOrder>>();
			Map<Integer,Integer> districtWiseCompCount=new HashMap<Integer, Integer>();
			if(isHeadQuarter){
				for(JobOrder job : lstJobOrders)
				{
					/*if(job.getNoOfHires()!=null){
						noofHire+=job.getNoOfHires();
					}*/
				  //Calculation of district wise opening	
					if(job.getDistrictMaster()!=null)
					{
						int distHire=0;
						if(districtWiseOpening.get(job.getDistrictMaster().getDistrictId())!=null)
						{
							distHire=districtWiseOpening.get(job.getDistrictMaster().getDistrictId());
							if(job.getNoOfHires()!=null){
								distHire+=job.getNoOfHires();
							}
							districtWiseOpening.put(job.getDistrictMaster().getDistrictId(), distHire);
						}else{
							if(job.getNoOfHires()!=null){
								distHire+=job.getNoOfHires();
							}
							districtWiseOpening.put(job.getDistrictMaster().getDistrictId(), distHire);
						}	
						//to getting district map
						mapOfDistrict.put(job.getDistrictMaster().getDistrictId(), job.getDistrictMaster());
						
						//District Wise Job Calculation
						List<JobOrder> tempJobs=null;
						if(districtWiseJobs.get(job.getDistrictMaster().getDistrictId())!=null){
							tempJobs=districtWiseJobs.get(job.getDistrictMaster().getDistrictId());
						}else{
							tempJobs=new ArrayList<JobOrder>();
						}
						tempJobs.add(job);
						districtWiseJobs.put(job.getDistrictMaster().getDistrictId(), tempJobs);
					}
				}

				Map<Integer,String> jobWiseCompetitiveMap=jobForTeacherDAO.countApplicantsByJobOrders(lstJobOrders);
				
				for(Entry<Integer,String> entry :jobWiseCompetitiveMap.entrySet())
				{
					int key=Utility.getIntValue(entry.getValue().split("##")[0]);
					if(districtWiseCompCount.get(key)!=null){
						districtWiseCompCount.put(key, (districtWiseCompCount.get(key))+1);
					}else{
						districtWiseCompCount.put(key, 1);
					}
				}
			}
		
			Map<Integer,Integer> mapOfDistrictWiseHired=jobForTeacherDAO.countApplicantsByJobOrdersMukesh(lstJobOrders);
		
			int underFilledLEADistrict=0;
			
			//mukesh
			//distId,totalHireByDistrictPer
			Map<Integer,Float> uncompDistRateMap=new HashMap<Integer, Float>();
			for (Entry<Integer, Integer> entry : districtWiseOpening.entrySet()) 
			{
				if(mapOfDistrictWiseHired.get(entry.getKey())!=null)
				{
					float totalHireByDistrict= mapOfDistrictWiseHired.get(entry.getKey());
					//shadab entry.getKey() should be entry.getValue()
					//float totalHireByDistrictPer=(((float)totalHireByDistrict*100)/(float)entry.getKey());
					float totalHireByDistrictPer=(((float)totalHireByDistrict*100)/(float)entry.getValue());
					//float totalHireByDistrictPer=Math.round((((float)totalHireByDistrict*100)/(float)entry.getKey()));//((totalHireByDistrict*100)/entry.getKey());
					if(totalHireByDistrictPer<=20){
						DecimalFormat dfm=new DecimalFormat("##.##");
						underFilledLEADistrict++;
						System.out.println(mapOfDistrict.get(entry.getKey()).getDistrictName()+"\t"+Float.valueOf(dfm.format(totalHireByDistrictPer))+"\t"+totalHireByDistrictPer);
						//uncompDistRateMap.put(entry.getKey(),totalHireByDistrictPer);
						uncompDistRateMap.put(entry.getKey(),Float.valueOf(dfm.format(totalHireByDistrictPer)));
						
					}
				}else{
					uncompDistRateMap.put(entry.getKey(), (float)0);
				}
			}
			//No. of Login...
			//<districtId,No of Login> 
			Map<Integer,Integer> loginDistrictMap= new HashMap<Integer, Integer>();
			if(listDistrictMasters!=null && listDistrictMasters.size()>0){
				loginDistrictMap=userLoginHistoryDAO.countUserLoginByDistrictLEA(false,listDistrictMasters,null);
			}
			for (Entry<Integer, Float> entry : uncompDistRateMap.entrySet()) 
			{
				DistrictMaster districtsMaster=null;
				
				//System.out.println(entry.getKey() +"\t"+mapOfDistrict.get(entry.getKey()).getDistrictName());
			
				districtsMaster=mapOfDistrict.get(entry.getKey());
				
				 tmRecord.append("<div class='col-md-4 col-sm-4'>");
					tmRecord.append("<div class='dcard2 table-bordered1' id='profilebox'>");
					tmRecord.append("<table width='100%' style='width: 285px;border: solid red 0px;font-size:10px;' >");
						tmRecord.append("<tr>");
					
						  tmRecord.append("<td width='100%' class='titlecolor' style='font-size:11px;padding-left: 10px;'><span>&nbsp;</span><b>"+districtsMaster.getDistrictName()+"</b></td>");
							 tmRecord.append("<td width='10%' style='text-align:right;padding-right: 5px;'>");
							  tmRecord.append("<a style='padding-right: 3px;' id='lyrics'  onclick=\"divClose(this,"+1+",1);\" href='javascript:void(0);' ><i class='icon-remove'></i></a>");
							 tmRecord.append("</td>");	
						  
						tmRecord.append("</tr>");
						
						//Fill Rate:
						tmRecord.append("<tr>" +
											"<td colspan=3 style='padding-left: 10px;'><span class='divlable1'>Fill Rate:</span>&nbsp;<span class='divlableval1'>"+entry.getValue()+"%</span>" +
											"</td>" +
										"</tr>");
						
						//competitive rate calculation
						float competitiveRate=0;
						int totalJob=0;
						int totalComJob=0;
						if(districtWiseJobs.get(entry.getKey())!=null){
							totalJob=districtWiseJobs.get(entry.getKey()).size();
						}
						if(districtWiseCompCount.get(entry.getKey())!=null){
							totalComJob=districtWiseCompCount.get(entry.getKey());
						}
						if (totalJob==0 || totalComJob==0) {
							competitiveRate=0;
						}else if(totalJob!=0 && totalComJob!=0){
							competitiveRate=(totalComJob*100)/totalJob;
						}
						tmRecord.append("<tr>" +
											"<td colspan=3 style='padding-left: 10px;'><span class='divlable1'>Competitive Rate:</span>&nbsp;<span class='divlableval1'>"+competitiveRate+"%</span>" +
											"</td>" +
										"</tr>");
						
						//Active Session/Logins
						int noOfLogin=0;
						if(loginDistrictMap.get(entry.getKey())!=null){
							noOfLogin=loginDistrictMap.get(entry.getKey());
						}
						tmRecord.append("<tr>" +
								"<td colspan=3 style='padding-left: 10px;'><span class='divlable1'>Logins:</span>&nbsp;<span class='divlableval1'>"+noOfLogin+"</span>" +
								"</td>" +
							"</tr>");
						
					tmRecord.append("</table>");
					tmRecord.append("</div>");
				tmRecord.append("</div>");	
				
			}
			
			objData[0]=tmRecord.toString();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return objData;
	}
	
}