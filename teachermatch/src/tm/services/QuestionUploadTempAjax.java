package tm.services;

/* @Author: Hanzala 
 * @Discription: It is used to Question Upload
 */
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobOrder;
import tm.bean.QuestionUploadTemp;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.assessment.AssessmentQuestions;
import tm.bean.assessment.AssessmentSections;
import tm.bean.assessment.QuestionOptions;
import tm.bean.assessment.QuestionsPool;
import tm.bean.assessment.StageOneStatus;
import tm.bean.assessment.StageThreeStatus;
import tm.bean.assessment.StageTwoStatus;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.ObjectiveMaster;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.QuestionUploadTempDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.assessment.QuestionsPoolDAO;
import tm.dao.assessment.StageOneStatusDAO;
import tm.dao.assessment.StageThreeStatusDAO;
import tm.dao.assessment.StageTwoStatusDAO;
import tm.dao.master.CompetencyMasterDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.ObjectiveMasterDAO;
import tm.dao.master.QuestionTypeMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class QuestionUploadTempAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private QuestionUploadTempDAO questionUploadTempDAO;
	@Autowired
	private QuestionsPoolDAO questionsPoolDAO;
	@Autowired
	private QuestionTypeMasterDAO questionTypeMasterDAO;
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	@Autowired
	private CompetencyMasterDAO competencyMasterDAO;
	@Autowired
	private ObjectiveMasterDAO objectiveMasterDAO;
	@Autowired
	private StageOneStatusDAO stageOneStatusDAO;
	@Autowired
	private StageTwoStatusDAO stageTwoStatusDAO;
	@Autowired
	private StageThreeStatusDAO stageThreeStatusDAO;
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO)
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) 
	{
		this.userMasterDAO = userMasterDAO;
	}
	private Vector vectorDataExcelXLSX = new Vector();
	
	
	public String displayTempQuestionRecords(String noOfRow, String pageNo,String sortOrder,String sortOrderType) {
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,47,"importcandidatedetails.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			/***** Sorting by  Stating ( Sekhar ) ******/
			List<QuestionUploadTemp> questionUploadTemplst  =	null;
			
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"questiontempid";
	
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				 {
					 sortOrderFieldName		=	sortOrder;
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion 	= 	Restrictions.eq("sessionid",session.getId());
			totalRecord 			=	questionUploadTempDAO.getRowCountTempTeacher(criterion);
			questionUploadTemplst 	= 	questionUploadTempDAO.findByTempTeacher(sortOrderStrVal,start,noOfRowInPage,criterion);
			System.out.println(session.getId());
			/***** Sorting by Temp Teacher  End ******/
	
			dmRecords.append("<table  id='tempQuestionTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgErrors3", locale),sortOrderFieldName,"errortext",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDomName", locale),sortOrderFieldName,"domain_name",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCompNm", locale),sortOrderFieldName,"competency_name",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblObjectiveName1", locale),sortOrderFieldName,"objective_name",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSt1", locale),sortOrderFieldName,"stage1",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSt2", locale),sortOrderFieldName,"stage2",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSt3", locale),sortOrderFieldName,"stage3",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblQues", locale),sortOrderFieldName,"question",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("isExperimental", locale),sortOrderFieldName,"isExperimental",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblQuestionWeightage1", locale),sortOrderFieldName,"question_weightage",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblMMarks", locale),sortOrderFieldName,"max_marks",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblInst", locale),sortOrderFieldName,"instructions",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("headQuesType", locale),sortOrderFieldName,"question_type_id",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption1", locale),sortOrderFieldName,"option1",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption1Score", locale),sortOrderFieldName,"option1_score",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption1Rank", locale),sortOrderFieldName,"option1_rank",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption2", locale),sortOrderFieldName,"option2",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption2Score", locale),sortOrderFieldName,"option2_score",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption2Rank", locale),sortOrderFieldName,"option2_rank",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption3", locale),sortOrderFieldName,"option3",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption3Score", locale),sortOrderFieldName,"option3_score",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption3Rank", locale),sortOrderFieldName,"option3_rank",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption4", locale),sortOrderFieldName,"option4",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption4Score", locale),sortOrderFieldName,"option4_score",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption4Rank", locale),sortOrderFieldName,"option4_rank",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption5", locale),sortOrderFieldName,"option5",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption5Score", locale),sortOrderFieldName,"option5_score",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption5Rank", locale),sortOrderFieldName,"option5_rank",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption6", locale),sortOrderFieldName,"option6",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption6Score", locale),sortOrderFieldName,"option6_score",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgOption5Rank", locale),sortOrderFieldName,"option6_rank",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblItemCode", locale),sortOrderFieldName,"itemCode",sortOrderTypeVal,pgNo);
			dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(questionUploadTemplst.size()==0)
				dmRecords.append("<tr><td colspan='6' align='center'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			String assessmentId = "", sectionId = "", stage1 = "", stage2="", stage3="";
			String stage1Name = "", stage2Name="", stage3Name="",question_type_name="";
			for (QuestionUploadTemp questionUploadTemp : questionUploadTemplst){
				String domain_name			=	questionUploadTemp.getDomain_name();
				String competency_name		=	questionUploadTemp.getCompetency_name();
				String objective_name		=	questionUploadTemp.getObjective_name();
				String question				=	questionUploadTemp.getQuestion();
				String isExperimental		=	questionUploadTemp.getIsExperimental();
				String question_weightage	=	questionUploadTemp.getQuestion_weightage();
				String max_marks			=	questionUploadTemp.getMax_marks();
				String instructions			=	questionUploadTemp.getInstructions();
				String question_type		=	questionUploadTemp.getQuestion_type();
				String option1				=	questionUploadTemp.getOption1();
				String score1				=	questionUploadTemp.getScore1();
				String rank1				=	questionUploadTemp.getRank1();
				String option2				=	questionUploadTemp.getOption2();
				String score2				=	questionUploadTemp.getScore2();
				String rank2				=	questionUploadTemp.getRank2();
				String option3				=	questionUploadTemp.getOption3();
				String score3				=	questionUploadTemp.getScore3();
				String rank3				=	questionUploadTemp.getRank3();
				String option4				=	questionUploadTemp.getOption4();
				String score4				=	questionUploadTemp.getScore4();
				String rank4				=	questionUploadTemp.getRank4();
				String option5				=	questionUploadTemp.getOption5();
				String score5				=	questionUploadTemp.getScore5();
				String rank5				=	questionUploadTemp.getRank5();
				String option6				=	questionUploadTemp.getOption6();
				String score6				=	questionUploadTemp.getScore6();
				String rank6				=	questionUploadTemp.getRank6();
				String itemCode				=	questionUploadTemp.getItemCode();
				String errortext			=	questionUploadTemp.getErrortext();
				
				assessmentId			=	questionUploadTemp.getAssessmentId();
				sectionId				=	questionUploadTemp.getSectionId();
				
				stage1	= questionUploadTemp.getStage1();
				stage2	= questionUploadTemp.getStage2();
				stage3	= questionUploadTemp.getStage3();
				
				stage1Name	=	questionUploadTemp.getStage1_name();
				stage2Name	=	questionUploadTemp.getStage2_name();
				stage3Name	=	questionUploadTemp.getStage3_name();
				question_type_name = questionUploadTemp.getQuestion_type_name();
				
				String rowCss="";

				if(!questionUploadTemp.getErrortext().equalsIgnoreCase("")){
					rowCss="style=\"background-color:#FF0000;\"";
				}

				dmRecords.append("<tr>" );
				dmRecords.append("<td "+rowCss+">"+errortext+"</td>");
				dmRecords.append("<td "+rowCss+">"+domain_name+"</td>");
				dmRecords.append("<td "+rowCss+">"+competency_name+"</td>");
				dmRecords.append("<td "+rowCss+">"+objective_name+"</td>");
				dmRecords.append("<td "+rowCss+">"+stage1Name+"</td>");
				dmRecords.append("<td "+rowCss+">"+stage2Name+"</td>");
				dmRecords.append("<td "+rowCss+">"+stage3Name+"</td>");
				dmRecords.append("<td "+rowCss+">"+question+"</td>");
				dmRecords.append("<td "+rowCss+">"+isExperimental+"</td>");
				dmRecords.append("<td "+rowCss+">"+question_weightage+"</td>");
				dmRecords.append("<td "+rowCss+">"+max_marks+"</td>");
				dmRecords.append("<td "+rowCss+">"+instructions+"</td>");
				dmRecords.append("<td "+rowCss+">"+question_type_name+"</td>");
				dmRecords.append("<td "+rowCss+">"+option1+"</td>");
				dmRecords.append("<td "+rowCss+">"+score1+"</td>");
				dmRecords.append("<td "+rowCss+">"+rank1+"</td>");
				dmRecords.append("<td "+rowCss+">"+option2+"</td>");
				dmRecords.append("<td "+rowCss+">"+score2+"</td>");
				dmRecords.append("<td "+rowCss+">"+rank2+"</td>");
				dmRecords.append("<td "+rowCss+">"+option3+"</td>");
				dmRecords.append("<td "+rowCss+">"+score3+"</td>");
				dmRecords.append("<td "+rowCss+">"+rank3+"</td>");
				dmRecords.append("<td "+rowCss+">"+option4+"</td>");
				dmRecords.append("<td "+rowCss+">"+score4+"</td>");
				dmRecords.append("<td "+rowCss+">"+rank4+"</td>");
				dmRecords.append("<td "+rowCss+">"+option5+"</td>");
				dmRecords.append("<td "+rowCss+">"+score5+"</td>");
				dmRecords.append("<td "+rowCss+">"+rank5+"</td>");
				dmRecords.append("<td "+rowCss+">"+option6+"</td>");
				dmRecords.append("<td "+rowCss+">"+score6+"</td>");
				dmRecords.append("<td "+rowCss+">"+rank6+"</td>");
				dmRecords.append("<td "+rowCss+">"+itemCode+"</td>");
				dmRecords.append("</tr>");
				
			}
			dmRecords.append("<input type='hidden' name='assessmentId' id='assessmentId' value='"+ assessmentId + "'>");
			dmRecords.append("<input type='hidden' name='sectionId' id='sectionId' value='"+ sectionId + "'>");
			dmRecords.append("<input type='hidden' name='numRecords' id='numRecordsToUpload' value='"+ questionUploadTemplst.size() + "'>");
			dmRecords.append("</table>");
			int tableWidth	=	3940;
			dmRecords.append("<table width="+tableWidth+">");
			dmRecords.append("<tr>");
			dmRecords.append("<td colspan=3>");
			dmRecords.append(PaginationAndSorting.getPaginationStringCommon(request,totalRecord,noOfRow, pageNo));
			//dmRecords.append(PaginationAndSorting.getPaginationDoubleStringPnr(request,totalRecord,noOfRow, pageNo));
			dmRecords.append("</td></tr>");
			dmRecords.append("</table>");
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	public int saveQuestion()
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		
		int returnVal=0;
        try{
        	Criterion criterion1 = Restrictions.eq("sessionid",session.getId());
        	Criterion criterion2 = Restrictions.eq("errortext","");
        	List <QuestionUploadTemp> questionUploadTemplst = questionUploadTempDAO.findByCriteria(criterion1,criterion2);
        	List<String> itemCodeList = new ArrayList<String>();
        	Map<String, QuestionsPool> questionsPoolMap = new HashMap<String, QuestionsPool>();
        	for(QuestionUploadTemp questionUploadTemp : questionUploadTemplst){
        		itemCodeList.add(questionUploadTemp.getItemCode());
        	}
        	List<QuestionsPool> questionsPoolList = questionsPoolDAO.getQuestionByItemCodes(itemCodeList);
        	for(QuestionsPool questionsPool : questionsPoolList){
        		questionsPoolMap.put(questionsPool.getItemCode(), questionsPool);
        	}
	    	List emails = new ArrayList();
	    	List jobs = new ArrayList();
	    	/**** Session dunamic ****/
	    	SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	    	Transaction txOpen =statelesSsession.beginTransaction();
        	 
        	QuestionsPool questionspool;
        	AssessmentQuestions assessmentquestions;
        	QuestionOptions questionoptions;
        	
        	 boolean isPortfolioNeeded=true,oneTime=false;
        	 int i=0;
        	 String questionUId			=	"";
			 String domain_name			=	"";
			 String competency_name		=	"";
			 String objective_name		=	"";
			 String question			=	"";
			 String isExperimental		=	"";
			 String question_weightage	=	"";
			 String max_marks			=	"";
			 String instructions		=	"";
			 String question_type		=	"";
			 String option1				=	"";
			 String score1				=	"";
			 String rank1				=	"";
			 String option2				=	"";
			 String score2				=	"";
			 String rank2				=	"";
			 String option3				=	"";
			 String score3				=	"";
			 String rank3				=	"";
			 String option4				=	"";
			 String score4				=	"";
			 String rank4				=	"";
			 String option5				=	"";
			 String score5				=	"";
			 String rank5				=	"";
			 String option6				=	"";
			 String score6				=	"";
			 String rank6				=	"";
			 String itemCode		    =	"";
			 String assessmentId		=	"";
			 String sectionId			=	"";
			 String option,score,rank;
			 int k = 0;
			 String ipaddress			=	"";
			 
			 String stage1Id = "",stage2Id = "",stage3Id = "";
			 String domainUId = "", competencyUId = "", objectiveUId = "";
			 ipaddress	=	IPAddressUtility.getIpAddress(request);
			 int questionId	=	questionsPoolDAO.getLastQuestionsPoolId();
			 
			 /*	Map for Domain Master*/
        	 Map<Integer,DomainMaster> domainMasterMap = new HashMap<Integer, DomainMaster>();
        	 List<DomainMaster> domainMasterList  = new ArrayList<DomainMaster>();
        	 domainMasterList = domainMasterDAO.getActiveDomains();

        	 for(DomainMaster domainMaster : domainMasterList){
        		 domainMasterMap.put(domainMaster.getDomainId(), domainMaster);
        	 }

        	 /*	Map for Competency Master Master*/
        	 Map<Integer,CompetencyMaster> competencyMasterMap = new HashMap<Integer, CompetencyMaster>();
        	 List<CompetencyMaster> competencyMasterList  = new ArrayList<CompetencyMaster>();
        	 competencyMasterList = competencyMasterDAO.getActiveCompetencies();

        	 for(CompetencyMaster competencyMaster : competencyMasterList){
        		 competencyMasterMap.put(competencyMaster.getCompetencyId(), competencyMaster);
        	 }

        	 /*	Map for Objective Master*/
        	 Map<Integer,ObjectiveMaster> objectiveMasterMap = new HashMap<Integer, ObjectiveMaster>();
        	 List<ObjectiveMaster> objectiveMasterList  = new ArrayList<ObjectiveMaster>();
        	 objectiveMasterList = objectiveMasterDAO.getActiveObjectives();

        	 for(ObjectiveMaster objectiveMaster : objectiveMasterList){
        		 objectiveMasterMap.put(objectiveMaster.getObjectiveId(), objectiveMaster);
        	 }

        	for(QuestionUploadTemp questionUploadTemp : questionUploadTemplst){
        		int questionTypeCount = 0;
        		
        		domain_name			=	questionUploadTemp.getDomain_id() == null ? "" : questionUploadTemp.getDomain_id();
        		competency_name		=	questionUploadTemp.getCompetency_id() == null ? "" : questionUploadTemp.getCompetency_id();
        		objective_name		=	questionUploadTemp.getObjective_id() == null ? "" : questionUploadTemp.getObjective_id();
        		question			=	questionUploadTemp.getQuestion() == null ? "" : questionUploadTemp.getQuestion();
        		isExperimental		=	questionUploadTemp.getIsExperimental() == null ? "" : questionUploadTemp.getIsExperimental();
        		question_weightage	=	questionUploadTemp.getQuestion_weightage() == null ? "" : questionUploadTemp.getQuestion_weightage();
        		max_marks			=	questionUploadTemp.getMax_marks() == null ? "" : questionUploadTemp.getMax_marks();
        		instructions		=	questionUploadTemp.getInstructions() == null ? "" : questionUploadTemp.getInstructions();
        		question_type 		=	questionUploadTemp.getQuestion_type() == null ? "" : questionUploadTemp.getQuestion_type();
        		assessmentId		=	questionUploadTemp.getAssessmentId() == null ? "" : questionUploadTemp.getAssessmentId();
        		sectionId			=	questionUploadTemp.getSectionId() == null ? "" : questionUploadTemp.getSectionId();
        		stage1Id			=	questionUploadTemp.getStage1() == null ? "" : questionUploadTemp.getStage1();
        		stage2Id			=	questionUploadTemp.getStage2() == null ? "" : questionUploadTemp.getStage2();
        		stage3Id			=	questionUploadTemp.getStage3() == null ? "" : questionUploadTemp.getStage3();
        		
        		
        		/*	Calculate questionUId dynamically  */
        		if(domainMasterMap.get(Integer.parseInt(domain_name)) != null){
        			domainUId = domainMasterMap.get(Integer.parseInt(domain_name)).getDomainUId().toString();
        		}
        		
        		if(competencyMasterMap.get(Integer.parseInt(competency_name)) != null){
        			competencyUId = competencyMasterMap.get(Integer.parseInt(competency_name)).getCompetencyUId().toLowerCase();
        		}
        		
        		if(objectiveMasterMap.get(Integer.parseInt(objective_name)) != null){
        			objectiveUId = objectiveMasterMap.get(Integer.parseInt(objective_name)).getObjectiveUId().toString();
        		}
        		
        		/*	End Calculation */

        		/* Calculation of Max Marks */
        		int scoreLargeVal 	= 0;
        		double maxMarksVal 	= 0;
        		double maxMarksValRt= 0;

        		for(k=1; k<=6; k++){
					String optionValue1 = "option"+k;
					String rankValue1 	= "rank"+k;
					String scoreValue1 	= "score"+k;
					if(k==1){
						optionValue1=	questionUploadTemp.getOption1() == null ? "" : questionUploadTemp.getOption1();
						scoreValue1	=	questionUploadTemp.getScore1() == null ? "" : questionUploadTemp.getScore1();
					}
					
					if(k==2){
						optionValue1=	questionUploadTemp.getOption2() == null ? "" : questionUploadTemp.getOption2();
						scoreValue1	=	questionUploadTemp.getScore2() == null ? "" : questionUploadTemp.getScore2();
					}
	        		
					if(k==3){
						optionValue1=	questionUploadTemp.getOption3() == null ? "" : questionUploadTemp.getOption3();
						scoreValue1	=	questionUploadTemp.getScore3() == null ? "" : questionUploadTemp.getScore3();
					}
	        		if(k==4){
	        			optionValue1=	questionUploadTemp.getOption4() == null ? "" : questionUploadTemp.getOption4();
	        			scoreValue1	=	questionUploadTemp.getScore4() == null ? "" : questionUploadTemp.getScore4();
	        		}
	        		if(k==5){
	        			optionValue1=	questionUploadTemp.getOption5() == null ? "" : questionUploadTemp.getOption5();
	        			scoreValue1	=	questionUploadTemp.getScore5() == null ? "" : questionUploadTemp.getScore5();
	        		}
	        		if(k==6){
	        			optionValue1=	questionUploadTemp.getOption6() == null ? "" : questionUploadTemp.getOption6();
	        			scoreValue1	=	questionUploadTemp.getScore6() == null ? "" : questionUploadTemp.getScore6();
	        		}

	        		if(optionValue1!=null && !optionValue1.equalsIgnoreCase("") && !optionValue1.equalsIgnoreCase("")){
	        			if(question_type.equalsIgnoreCase("2")){
	        				if(scoreValue1!=null && !scoreValue1.equalsIgnoreCase("")){
				        		if(scoreLargeVal < Integer.parseInt(scoreValue1)){
				        			scoreLargeVal = Integer.parseInt(scoreValue1);
				        		}
			        		}
	        			} else {
	        				if(scoreValue1!=null && !scoreValue1.equalsIgnoreCase("")){
	        					maxMarksValRt = maxMarksValRt+Integer.parseInt(scoreValue1);
				        		if(scoreLargeVal < Integer.parseInt(scoreValue1)){
				        			scoreLargeVal = Integer.parseInt(scoreValue1);
				        		}
			        		}
	        			}
	        		}
				}
        		if(question_weightage!=null && !question_weightage.equalsIgnoreCase("") && !question_weightage.equalsIgnoreCase(" ")){
        			
        			if(question_type.equalsIgnoreCase("3")){
        				maxMarksVal = maxMarksValRt*Integer.parseInt(question_weightage);
            		} else {
            			maxMarksVal = scoreLargeVal*Integer.parseInt(question_weightage);
            		}
        		} else {
        			question_weightage = "0";
        		}
        		if(questionsPoolMap.size()>0 && questionsPoolMap.get(questionUploadTemp.getItemCode())!=null){
        			questionspool = questionsPoolMap.get(questionUploadTemp.getItemCode());
        		}
        		else{
					questionspool = new QuestionsPool();
					questionId = questionId+1;
					questionUId = domainUId+"."+competencyUId+"."+objectiveUId+"."+Utility.addZeroBeforeNo(questionId,5);
					questionspool.setQuestionUId(questionUId);
					DomainMaster domainMaster = new DomainMaster();
					domainMaster.setDomainId(Integer.parseInt(domain_name));
					questionspool.setDomainMaster(domainMaster);
					
					CompetencyMaster competencyMaster = new CompetencyMaster();
					competencyMaster.setCompetencyId(Integer.parseInt(competency_name));
					questionspool.setCompetencyMaster(competencyMaster);
					
					ObjectiveMaster objectiveMaster = new ObjectiveMaster();
					objectiveMaster.setObjectiveId(Integer.parseInt(objective_name));
					questionspool.setObjectiveMaster(objectiveMaster);
					
					StageOneStatus stageOneStatus = new StageOneStatus();
					stageOneStatus.setStage1Id(Integer.parseInt(stage1Id));
					questionspool.setStageOneStatus(stageOneStatus);
					
					StageTwoStatus stageTwoStatus = new StageTwoStatus();
					stageTwoStatus.setStage2Id(Integer.parseInt(stage2Id));
					questionspool.setStageTwoStatus(stageTwoStatus);
					
					StageThreeStatus stageThreeStatus = new StageThreeStatus();
					stageThreeStatus.setStage3Id(Integer.parseInt(stage3Id));
					questionspool.setStageThreeStatus(stageThreeStatus);
					
					questionspool.setQuestion(question);
					questionspool.setQuestionWeightage(Double.parseDouble(question_weightage));
					questionspool.setMaxMarks(maxMarksVal);
					questionspool.setQuestionInstruction(instructions);
					QuestionTypeMaster questionTypeMaster = new QuestionTypeMaster();
					questionTypeMaster.setQuestionTypeId(Integer.parseInt(question_type));
					questionspool.setQuestionTypeMaster(questionTypeMaster);
					questionspool.setUserMaster(userMaster);
					questionspool.setQuestionTaken(0);
					questionspool.setStatus("A");
					questionspool.setItemCode(questionUploadTemp.getItemCode());
					questionspool.setIpaddress(ipaddress);
					questionspool.setCreatedDateTime(new Date());
					
					statelesSsession.insert(questionspool);
        		}
				try{
					questionId	=	questionspool.getQuestionId();
				} catch(Exception er){

				}

				/* Insert Record Into assessmentquestions Table */
				assessmentquestions = new AssessmentQuestions();
				if(isExperimental!=null && isExperimental!="" && isExperimental.equalsIgnoreCase("yes")){
					assessmentquestions.setIsExperimental(true);
				}else{
					assessmentquestions.setIsExperimental(false);
				}
				assessmentquestions.setQuestionWeightage(Double.parseDouble(question_weightage));
				assessmentquestions.setQuestionsPool(questionspool);
				AssessmentDetail assessmentDetail = new AssessmentDetail();
				assessmentDetail.setAssessmentId(Integer.parseInt(assessmentId));
				assessmentquestions.setAssessmentDetail(assessmentDetail);
				AssessmentSections assessmentSections = new AssessmentSections(); 
				assessmentSections.setSectionId(Integer.parseInt(sectionId));
				assessmentquestions.setAssessmentSections(assessmentSections);
				assessmentquestions.setIsMandatory(false);
				assessmentquestions.setQuestionPosition(0);
				assessmentquestions.setUserMaster(userMaster);
				assessmentquestions.setStatus("A");
				assessmentquestions.setCreatedDateTime(new Date());
				statelesSsession.insert(assessmentquestions);

				if(questionsPoolMap.get(questionUploadTemp.getItemCode())==null){
					/* Insert Into Option Table */
					questionoptions = new QuestionOptions();
					Integer questionOptionTag = 1;
					
					for(k=1; k<=6; k++){
						String optionValue 	= "option"+k;
						String rankValue 	= "rank"+k;
						String scoreValue 	= "score"+k;
						if(k==1){
							optionValue	=	(questionUploadTemp.getOption1() == null || questionUploadTemp.getOption1().trim().equals(""))? "" : questionUploadTemp.getOption1();
							scoreValue	=	(questionUploadTemp.getScore1() == null  || questionUploadTemp.getScore1().trim().equals("")) ? "0" : questionUploadTemp.getScore1();
							rankValue	=	(questionUploadTemp.getRank1()  == null  || questionUploadTemp.getRank1().trim().equals("")) ? "0" : questionUploadTemp.getRank1();
						}
						if(k==2){
							optionValue	=	(questionUploadTemp.getOption2() == null || questionUploadTemp.getOption2().trim().equals(""))? "" : questionUploadTemp.getOption2();
							scoreValue	=	(questionUploadTemp.getScore2() == null  || questionUploadTemp.getScore2().trim().equals("")) ? "0" : questionUploadTemp.getScore2();
							rankValue	=	(questionUploadTemp.getRank2()  == null  || questionUploadTemp.getRank2().trim().equals("")) ? "0" : questionUploadTemp.getRank2();
						}
						if(k==3){
							optionValue	=	(questionUploadTemp.getOption3() == null || questionUploadTemp.getOption3().trim().equals(""))? "" : questionUploadTemp.getOption3();
							scoreValue	=	(questionUploadTemp.getScore3() == null  || questionUploadTemp.getScore3().trim().equals("")) ? "0" : questionUploadTemp.getScore3();
							rankValue	=	(questionUploadTemp.getRank3()  == null  || questionUploadTemp.getRank3().trim().equals("")) ? "0" : questionUploadTemp.getRank3();
						}
		        		if(k==4){
							optionValue	=	(questionUploadTemp.getOption4() == null || questionUploadTemp.getOption4().trim().equals(""))? "" : questionUploadTemp.getOption4();
							scoreValue	=	(questionUploadTemp.getScore4() == null  || questionUploadTemp.getScore4().trim().equals("")) ? "0" : questionUploadTemp.getScore4();
							rankValue	=	(questionUploadTemp.getRank4()  == null  || questionUploadTemp.getRank4().trim().equals("")) ? "0" : questionUploadTemp.getRank4();
		        		}
		        		if(k==5){
							optionValue	=	(questionUploadTemp.getOption5() == null || questionUploadTemp.getOption5().trim().equals(""))? "" : questionUploadTemp.getOption5();
							scoreValue	=	(questionUploadTemp.getScore5() == null  || questionUploadTemp.getScore5().trim().equals("")) ? "0" : questionUploadTemp.getScore5();
							rankValue	=	(questionUploadTemp.getRank5()  == null  || questionUploadTemp.getRank5().trim().equals("")) ? "0" : questionUploadTemp.getRank5();
		        		}
		        		if(k==6){
							optionValue	=	(questionUploadTemp.getOption6() == null || questionUploadTemp.getOption6().trim().equals(""))? "" : questionUploadTemp.getOption6();
							scoreValue	=	(questionUploadTemp.getScore6() == null  || questionUploadTemp.getScore6().trim().equals("")) ? "0" : questionUploadTemp.getScore6();
							rankValue	=	(questionUploadTemp.getRank6()  == null  || questionUploadTemp.getRank6().trim().equals("")) ? "0" : questionUploadTemp.getRank6();
		        		}

						if(optionValue!=null && !optionValue.trim().equalsIgnoreCase("") && !optionValue.trim().equalsIgnoreCase("")){
							//System.out.println(optionValue+" || "+scoreValue+" || "+rankValue);
							System.out.println("questionOptionTag : "+questionOptionTag);
							questionTypeCount++;
							if(question_type.equalsIgnoreCase("2")){
								if(questionTypeCount<=2){
									try{
										questionoptions.setQuestionsPool(questionspool);
										questionoptions.setQuestionOption(optionValue);
										questionoptions.setRank(Integer.parseInt(rankValue));
										questionoptions.setScore(Double.parseDouble(scoreValue));
										questionoptions.setUserMaster(userMaster);
										questionoptions.setStatus("A");
										questionoptions.setCreatedDateTime(new Date());
										questionoptions.setIpaddress(ipaddress);
										questionoptions.setQuestionOptionTag(questionOptionTag);
										statelesSsession.insert(questionoptions);
										questionOptionTag++;
									} catch (Exception ee){

									}
								}

							} else {
								try{
									questionoptions.setQuestionsPool(questionspool);
									questionoptions.setQuestionOption(optionValue);
									questionoptions.setRank(Integer.parseInt(rankValue));
									questionoptions.setScore(Double.parseDouble(scoreValue));
									questionoptions.setUserMaster(userMaster);
									questionoptions.setStatus("A");
									questionoptions.setCreatedDateTime(new Date());
									questionoptions.setIpaddress(ipaddress);
									questionoptions.setQuestionOptionTag(questionOptionTag);
									statelesSsession.insert(questionoptions);
									questionOptionTag++;
								} catch (Exception ee){

								}
							}
						}
					}
				}
        	}
	       	txOpen.commit();
	       	statelesSsession.close();
	       	/*try{
		       	mailSendByThread(teacherMailSendList);
	        	deleteXlsAndXlsxFile(request,session.getId());
	       		teacherUploadTempDAO.deleteAllTeacherTemp(session.getId());
	       	}catch(Exception e){
	       		e.printStackTrace();
	       	}*/
		}catch (Exception e){
			System.out.println("Error >"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			return returnVal;
		}
	}
	public int deleteTempQuestion(String sessionId) {
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		System.out.println(":::::::::::::::::deleteTempTeacher:::::::::::::::::"+sessionId);
		try{
			//teacherUploadTempDAO.deleteTeacherTemp(sessionId);
			questionUploadTempDAO.deleteQuestionTemp(sessionId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
		
	public String saveQuestionTemp(String fileName,String sessionId,String assesmentAndSection)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		System.out.println(":::::::: Save Question Data :::::::::");
		String returnVal="";
        try{
        	 String root = request.getRealPath("/")+"/uploads/";
        	 String filePath=root+fileName;
        	 if(fileName.contains(".xlsx")){
        		 vectorDataExcelXLSX = readDataExcelXLSX(filePath);
        	 }else{
        		 vectorDataExcelXLSX = readDataExcelXLS(filePath);
        	 }

        	 int domain_name_count			=	11;
			 int competency_name_count		=	11;
			 int objective_name_count		=	11;
			 int question_count				=	11;
			 int isExperimental_count		=	11;
			 int question_weightage_count	=	11;
			 int max_marks_count			=	11;
			 int instructions_count			=	11;
			 int question_type_count		=	11;
			 int option1_count				=	11;
			 int score1_count				=	11;
			 int rank1_count				=	11;
			 int option2_count				=	11;
			 int score2_count				=	11;
			 int rank2_count				=	11;
			 int option3_count				=	11;
			 int score3_count				=	11;
			 int rank3_count				=	11;
			 int option4_count				=	11;
			 int score4_count				=	11;
			 int rank4_count				=	11;
			 int option5_count				=	11;
			 int score5_count				=	11;
			 int rank5_count				=	11;
			 int option6_count				=	11;
			 int score6_count				=	11;
			 int rank6_count				=	11;
			 int itemCode_count				=	11;
			 int k							=	0;
			 
			 int stage1_count	=	11;
			 int stage2_count	=	11;
			 int stage3_count	=	11;
			 
			String[] parts 			= 	assesmentAndSection.split("####");
			String assessmentId 	= 	parts[0];
			String sectionId		= 	parts[1];
			 
			 Map<Integer,JobOrder> jobOrderList = 	jobOrderDAO.findByAllJobOrder(); 
			 List<UserMaster> userMasterlst 	= 	userMasterDAO.findByAllUserMaster();
			 List emails 						= 	new ArrayList();
			 Map<String,String> emailExistCheck = 	new HashMap<String, String>();
			 SessionFactory sessionFactory		=	questionUploadTempDAO.getSessionFactory();
			 StatelessSession sessionHiber 		= 	sessionFactory.openStatelessSession();
        	 Transaction txOpen 				=	sessionHiber.beginTransaction();
        	 
        	 /*Map For Checking QuestionUID*/
        	 Map<String,QuestionsPool> questionMap = new HashMap<String, QuestionsPool>();
        	 List<QuestionsPool>  questionsPoolList= new ArrayList<QuestionsPool>();
        	 questionsPoolList	= questionsPoolDAO.findAll();
        	 for (QuestionsPool questionsPool : questionsPoolList) {
        		 questionMap.put(questionsPool.getQuestionUId(),questionsPool);
     		 }
        	 
        	 /* Map For Check Question Type */
        	 Map<String,QuestionTypeMaster> questionTypeMap = new HashMap<String, QuestionTypeMaster>();
        	 List<QuestionTypeMaster>  questionTypeMasterList = new ArrayList<QuestionTypeMaster>();
        	 questionTypeMasterList	=	questionTypeMasterDAO.getActiveQuestionTypes();

        	 for (QuestionTypeMaster questionTypeMaster : questionTypeMasterList) {
        		 questionTypeMap.put(questionTypeMaster.getQuestionType(),questionTypeMaster);
     		 }
        	 
        	 /*	Map for Domain Master*/
        	 Map<String,DomainMaster> domainMasterMap = new HashMap<String, DomainMaster>();
        	 List<DomainMaster> domainMasterList  = new ArrayList<DomainMaster>();
        	 domainMasterList = domainMasterDAO.getActiveDomains();

        	 for(DomainMaster domainMaster : domainMasterList){
        		 domainMasterMap.put(domainMaster.getDomainName(), domainMaster);
        	 }

        	 /*	Map for Competency Master Master*/
        	 Map<String,CompetencyMaster> competencyMasterMap = new HashMap<String, CompetencyMaster>();
        	 List<CompetencyMaster> competencyMasterList  = new ArrayList<CompetencyMaster>();
        	 competencyMasterList = competencyMasterDAO.getActiveCompetencies();

        	 for(CompetencyMaster competencyMaster : competencyMasterList){
        		 competencyMasterMap.put(competencyMaster.getCompetencyName(), competencyMaster);
        	 }

        	 /*	Map for Objective Master*/
        	 Map<String,ObjectiveMaster> objectiveMasterMap = new HashMap<String, ObjectiveMaster>();
        	 List<ObjectiveMaster> objectiveMasterList  = new ArrayList<ObjectiveMaster>();
        	 objectiveMasterList = objectiveMasterDAO.getActiveObjectives();

        	 for(ObjectiveMaster objectiveMaster : objectiveMasterList){
        		 objectiveMasterMap.put(objectiveMaster.getObjectiveName(), objectiveMaster);
        	 }
        	 
        	 /*	Map for Stage 1*/
        	 Map<String,StageOneStatus> stageOneStatusMap = new HashMap<String, StageOneStatus>();
        	 List<StageOneStatus> stageOneStatusList  = new ArrayList<StageOneStatus>();
        	 stageOneStatusList = stageOneStatusDAO.getActiveStageOne();

        	 for(StageOneStatus stageOneStatus : stageOneStatusList){
        		 stageOneStatusMap.put(stageOneStatus.getName(), stageOneStatus);
        	 }
        	 
        	 /*	Map for Stage 2*/
        	 Map<String,StageTwoStatus> stageTwoStatusMap = new HashMap<String, StageTwoStatus>();
        	 List<StageTwoStatus> stageTwoStatusList  = new ArrayList<StageTwoStatus>();
        	 stageTwoStatusList = stageTwoStatusDAO.getActiveStageTwo();

        	 for(StageTwoStatus stageTwoStatus : stageTwoStatusList){
        		 stageTwoStatusMap.put(stageTwoStatus.getName(), stageTwoStatus);
        	 }
        	 
        	 /*	Map for Stage 2*/
        	 Map<String,StageThreeStatus> stageThreeStatusMap = new HashMap<String, StageThreeStatus>();
        	 List<StageThreeStatus> stageThreeStatusList = new ArrayList<StageThreeStatus>();
        	 stageThreeStatusList = stageThreeStatusDAO.getActiveStageThree();
        	 
        	 for(StageThreeStatus stageThreeStatus : stageThreeStatusList){
        		 stageThreeStatusMap.put(stageThreeStatus.getName(), stageThreeStatus);
        	 }

        	 QuestionUploadTemp questionUploadTemp= new QuestionUploadTemp();
			 for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
	             Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
	             String domain_name			=	"";
				 String competency_name		=	"";
				 String objective_name		=	"";
				 String question			=	"";
				 String isExperimental		=	"";
				 String question_weightage	=	"";
				 String max_marks			=	"";
				 String instructions		=	"";
				 String question_type		=	"";
				 String option1				=	"";
				 String score1				=	"";
				 String rank1				=	"";
				 String option2				=	"";
				 String score2				=	"";
				 String rank2				=	"";
				 String option3				=	"";
				 String score3				=	"";
				 String rank3				=	"";
				 String option4				=	"";
				 String score4				=	"";
				 String rank4				=	"";
				 String option5				=	"";
				 String score5				=	"";
				 String rank5				=	"";
				 String option6				=	"";
				 String score6				=	"";
				 String rank6				=	"";
				 String itemCode			=	"";
				 String questionTypeShortName = "";
				 String	question_type_Id	=	"";
				 String option				=	"";
				 int optionCount			=	0;
				 int scoreCount				=	0;
				 int rankCount				=	0;
				 int rankTypeCount			=	0;
				 String stage1				=	"";
				 String stage2				=	"";
				 String stage3				=	"";

				 String domainName			=	"";
				 String competencyName		=	"";
				 String objectiveName		=	"";
				 String stageOneName		=	"";
				 String stageTwoName		=	"";
				 String stageThreeName		=	"";
				 
				 String domainNameSet		=	"";
				 String competencyNameSet	=	"";
				 String objectiveNameSet	=	"";
				 String stageOneNameSet		=	"";
				 String stageTwoNameSet		=	"";
				 String stageThreeNameSet	=	"";
				 String question_type_name	=	"";

	            for(int j=0; j<vectorCellEachRowData.size(); j++) {
	            	
	            	//System.out.println(":::::::::::"+vectorCellEachRowData.get(j).toString());
	            	try{
		            	
	            		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("domain_name")){
		            		domain_name_count=j;
		            	}
		            	if(domain_name_count==j)
		            		domain_name=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("competency_name")){
		            		competency_name_count=j;
		            	}
		            	if(competency_name_count==j)
		            		competency_name=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("objective_name")){
		            		objective_name_count=j;
		            	}
		            	if(objective_name_count==j)
		            		objective_name=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	/*	Stage 1,2,3 */
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("stage1")){
		            		stage1_count=j;
		            	}
		            	if(stage1_count==j){
		            		stage1=vectorCellEachRowData.get(j).toString().trim();
		            	}
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("stage2")){
		            		stage2_count=j;
		            	}
		            	if(stage2_count==j)
		            		stage2=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("stage3")){
		            		stage3_count=j;
		            	}
		            	if(stage3_count==j)
		            		stage3=vectorCellEachRowData.get(j).toString().trim();
		            	/*	End Of Stage */
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("question")){
		            		question_count=j;
		            	}
		            	if(question_count==j)
		            		question=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("isExperimental")){
		            		isExperimental_count=j;
		            	}
		            	if(isExperimental_count==j)
		            		isExperimental=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("question_weightage")){
		            		question_weightage_count=j;
		            	}
		            	if(question_weightage_count==j)
		            		question_weightage=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("max_marks")){
		            		max_marks_count=j;
		            	}
		            	if(max_marks_count==j)
		            		max_marks=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("instructions")){
		            		instructions_count=j;
		            	}
		            	if(instructions_count==j)
		            		instructions=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("question_type")){
		            		question_type_count=j;
		            	}
		            	if(question_type_count==j)
		            		question_type=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("option1")){
		            		option1_count=j;
		            	}
		            	if(option1_count==j)
		            		option1=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("score1")){
		            		score1_count=j;
		            	}
		            	if(score1_count==j)
		            		score1=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("rank1")){
		            		rank1_count=j;
		            	}
		            	if(rank1_count==j)
		            		rank1=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("option2")){
		            		option2_count=j;
		            	}
		            	if(option2_count==j)
		            		option2=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("score2")){
		            		score2_count=j;
		            	}
		            	if(score2_count==j)
		            		score2=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("rank2")){
		            		rank2_count=j;
		            	}
		            	if(rank2_count==j)
		            		rank2=vectorCellEachRowData.get(j).toString().trim();

		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("option3")){
		            		option3_count=j;
		            	}
		            	if(option3_count==j)
		            		option3=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("score3")){
		            		score3_count=j;
		            	}
		            	if(score3_count==j)
		            		score3=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("rank3")){
		            		rank3_count=j;
		            	}
		            	if(rank3_count==j)
		            		rank3=vectorCellEachRowData.get(j).toString().trim();

		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("option4")){
		            		option4_count=j;
		            	}
		            	if(option4_count==j)
		            		option4=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("score4")){
		            		score4_count=j;
		            	}
		            	if(score4_count==j)
		            		score4=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("rank4")){
		            		rank4_count=j;
		            	}
		            	if(rank4_count==j)
		            		rank4=vectorCellEachRowData.get(j).toString().trim();

		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("option5")){
		            		option5_count=j;
		            	}
		            	if(option5_count==j)
		            		option5=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("score5")){
		            		score5_count=j;
		            	}
		            	if(score5_count==j)
		            		score5=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("rank5")){
		            		rank5_count=j;
		            	}
		            	if(rank5_count==j)
		            		rank5=vectorCellEachRowData.get(j).toString().trim();

		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("option6")){
		            		option6_count=j;
		            	}
		            	if(option6_count==j)
		            		option6=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("score6")){
		            		score6_count=j;
		            	}
		            	if(score6_count==j)
		            		score6=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("rank6")){
		            		rank6_count=j;
		            	}
		            	if(rank6_count==j)
		            		rank6=vectorCellEachRowData.get(j).toString().trim();

		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("itemCode")){
		            		itemCode_count=j;
		            	}
		            	if(itemCode_count==j)
		            		itemCode=vectorCellEachRowData.get(j).toString().trim();

	            	}catch(Exception e){}

	            }
	            
	            try{
	            	 domainNameSet		=	domain_name;
					 competencyNameSet	=	competency_name;
					 objectiveNameSet	=	objective_name;
					 stageOneNameSet	=	stage1;
					 stageTwoNameSet	=	stage2;
					 stageThreeNameSet	=	stage3;
	            	
	            	if(questionTypeMap.get(question_type) != null){
	            		question_type_name =   question_type;
		            	question_type_Id  =	 questionTypeMap.get(question_type).getQuestionTypeId().toString();
		            }
	            	if(domainMasterMap.get(domain_name) != null){
	            		domainName = domain_name;
	            		domain_name  = domainMasterMap.get(domain_name).getDomainId().toString();
	            	}
	            	
	            	if(competencyMasterMap.get(competency_name) != null){
	            		competencyName=competency_name;
	            		competency_name = competencyMasterMap.get(competency_name).getCompetencyId().toString();
	            	}
	            	
	            	if(objectiveMasterMap.get(objective_name) != null){
	            		objectiveName = objective_name;
	            		objective_name  = objectiveMasterMap.get(objective_name).getObjectiveId().toString();
	            	}
	            	
	            	if(stageOneStatusMap.get(stage1) != null){
	            		stageOneName = stage1;
	            		stage1 =  stageOneStatusMap.get(stage1).getStage1Id().toString();
	            	}
	            	
	            	if(stageTwoStatusMap.get(stage2) != null){
	            		stageTwoName = stage2;
	            		stage2 =  stageTwoStatusMap.get(stage2).getStage2Id().toString();
	            	}
	            	
	            	if(stageThreeStatusMap.get(stage3) != null){
	            		stageThreeName = stage3;
	            		stage3 =  stageThreeStatusMap.get(stage3).getStage3Id().toString();
	            	}
	            	
	            } catch(Exception er){
	
	            }
	            
	            if(i!=0){
	            	if(returnVal.equals("1")) {
	            		String errorText=""; boolean errorFlag=false;
		            		questionUploadTemp = new QuestionUploadTemp();
		            		
		            		if(domain_name.equalsIgnoreCase("")||domain_name.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgDomainNameNotAvailable", locale);
		            	 		errorFlag=true;
		            	 	} else if(domainName.equalsIgnoreCase("") || domainName.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgDomainNameWrong", locale);
		            	 		errorFlag=true;
		            	 	}
		            		
		            		if(competency_name.equalsIgnoreCase("")||competency_name.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgCompetencyName", locale);
		            	 		errorFlag=true;
		            	 	} else if(competencyName.equalsIgnoreCase("") || competencyName.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgCompetencyNameWrong", locale);
		            	 		errorFlag=true;
		            	 	} else {
		            	 		try{
		            	 			boolean cmDomainId=false;
			            	 		CompetencyMaster cmObject = competencyMasterMap.get(competencyName);
			            	 		if(cmObject!=null){
			            	 			if(cmObject.getDomainMaster().getDomainId()==Integer.parseInt(domain_name)){
			            	 				cmDomainId=true;
			            	 			}
			            	 		}
			    	            	if(!cmDomainId){
			    	            		if(errorFlag){
				            	 			errorText+=" </br>";	
				            	 		}
				            	 		errorText+=Utility.getLocaleValuePropByKey("msgCompetencyNameMatch", locale);
				            	 		errorFlag=true;
			    	            	} else {
			    	            		
			    	            		/* Check For Objective ID */
			    	            		boolean cmCompId=false;
			    	            		ObjectiveMaster objObject = objectiveMasterMap.get(objectiveName);
			    	            		if(objObject!=null){
			    	            			if(cmObject.getCompetencyId().equals(objObject.getCompetencyMaster().getCompetencyId())){
			    	            				cmCompId = true;
			    	            			}
			    	            		}
			    	            		if(!cmCompId){
				    	            		if(errorFlag){
					            	 			errorText+=" </br>";	
					            	 		}
					            	 		errorText+=Utility.getLocaleValuePropByKey("msgObjectiveNameMatch", locale);
					            	 		errorFlag=true;
				    	            	}
			    	            	}
		            	 			
		            	 		} catch(Exception ee){
		            	 			
		            	 		}
		            	 		
		            	 	}

		            		if(objective_name.equalsIgnoreCase("")||objective_name.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgObjectiveNameAvailable", locale);
		            	 		errorFlag=true;
		            	 	} else if(objectiveName.equalsIgnoreCase("") || objectiveName.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgObjectiveNameWrong", locale);
		            	 		errorFlag=true;
		            	 	}
		            		
		            		if(stage1.equalsIgnoreCase("")||stage1.equalsIgnoreCase(" ")){
		            			if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgStage1Available", locale);
		            	 		errorFlag=true;
		            			
		            		} else if(stageOneName.equalsIgnoreCase("") || stageOneName.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgStage1Wrong", locale);
		            	 		errorFlag=true;
		            	 	}
		            		
		            		if(stage2.equalsIgnoreCase("")||stage2.equalsIgnoreCase(" ")){
		            			if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgStage2Available", locale);
		            	 		errorFlag=true;
		            			
		            		} else if(stageTwoName.equalsIgnoreCase("") || stageTwoName.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgStage2Wrong", locale);
		            	 		errorFlag=true;
		            	 	}
		            		
		            		if(stage3.equalsIgnoreCase("")||stage3.equalsIgnoreCase(" ")){
		            			if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgStage3Available", locale);
		            	 		errorFlag=true;
		            			
		            		} else if(stageThreeName.equalsIgnoreCase("") || stageThreeName.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgStage3Wrong", locale);
		            	 		errorFlag=true;
		            	 	}
		            		
		            		
		            		if(question.equalsIgnoreCase("")||question.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgQuestionNotAvailable", locale);
		            	 		errorFlag=true;
		            	 	}
		            		
		            		if(isExperimental.equalsIgnoreCase("")||isExperimental.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgIsExperimentalNotAvailable", locale);
		            	 		errorFlag=true;
		            	 	} else if(isExperimental.equalsIgnoreCase("yes") || isExperimental.equalsIgnoreCase("no")){
		            	 		
		            	 	} else {
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgIsExperimental", locale);
		            	 		errorFlag=true;
		            	 	}
		            		
		            		//int foo = Integer.parseInt("question_weightage");
		            		//String foo = "question_weightage";
		            		int op1 = 0;
		            		int num = 0;
		            		
		            		try{
		            			num = Integer.parseInt(question_weightage);
		            			op1 = 1;
		            		} catch(NumberFormatException nfe){
		            			op1 = 0;
		            		}
		            		if(question_weightage.equalsIgnoreCase("")||question_weightage.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgQuestionWeightage", locale);
		            	 		errorFlag=true;
		            	 	} else if(op1 == 0){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgQuestionWeightage", locale);
		            	 		errorFlag=true;
		            	 	}
		            		
		            		if(!isExperimental.equalsIgnoreCase("") && !isExperimental.equalsIgnoreCase(" ") && isExperimental.equalsIgnoreCase("yes") && !question_weightage.equalsIgnoreCase("") && !question_weightage.equalsIgnoreCase(" ") && num>0){
		            			if(errorFlag){
		            	 			errorText+=" </br>";
		            	 		}
		            			errorText+=Utility.getLocaleValuePropByKey("msgQIsExperimental", locale);
		            	 		errorFlag=true;
		            		}
		            		
		            		/* Option Count */
		            		if(!option1.equalsIgnoreCase("") && !option1.equalsIgnoreCase(" ")){
		            			optionCount	= optionCount+1;
		            	 	}
		            		if(!option2.equalsIgnoreCase("") && !option2.equalsIgnoreCase(" ")){
		            			optionCount	= optionCount+1;
		            	 	}
		            		if(!option3.equalsIgnoreCase("") && !option3.equalsIgnoreCase(" ")){
		            			optionCount	= optionCount+1;
		            	 	}
		            		if(!option4.equalsIgnoreCase("") && !option4.equalsIgnoreCase(" ")){
		            			optionCount	= optionCount+1;
		            	 	}
		            		if(!option5.equalsIgnoreCase("") && !option5.equalsIgnoreCase(" ")){
		            			optionCount	= optionCount+1;
		            	 	}
		            		if(!option6.equalsIgnoreCase("") && !option6.equalsIgnoreCase(" ")){
		            			optionCount	= optionCount+1;
		            	 	}

		            		/* Rank Count */
		            		if(!rank1.equalsIgnoreCase("") && !rank1.equalsIgnoreCase(" ")){
		            			rankCount	= rankCount+1;
		            	 	}
		            		if(!rank2.equalsIgnoreCase("") && !rank2.equalsIgnoreCase(" ")){
		            			rankCount	= rankCount+1;
		            	 	}
		            		if(!rank3.equalsIgnoreCase("") && !rank3.equalsIgnoreCase(" ")){
		            			rankCount	= rankCount+1;
		            	 	}
		            		if(!rank4.equalsIgnoreCase("") && !rank4.equalsIgnoreCase(" ")){
		            			rankCount	= rankCount+1;
		            	 	}
		            		if(!rank5.equalsIgnoreCase("") && !rank5.equalsIgnoreCase(" ")){
		            			rankCount	= rankCount+1;
		            	 	}
		            		if(!rank6.equalsIgnoreCase("") && !rank6.equalsIgnoreCase(" ")){
		            			rankCount	= rankCount+1;
		            	 	}

		            		if(question_type.equalsIgnoreCase("")|| question_type.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgQuestionTypeNotAvailable", locale);
		            	 		errorFlag=true;
		            	 	} else if(question_type_name.equalsIgnoreCase("")|| question_type_name.equalsIgnoreCase(" ")){
		            	 		if(errorFlag){
		            	 			errorText+=" </br>";	
		            	 		}
		            	 		errorText+=Utility.getLocaleValuePropByKey("msgQuestionTypeWrong", locale);
		            	 		errorFlag=true;
		            	 	} else if(question_type.equalsIgnoreCase("Single Selection")) {
		            	 		if(optionCount < 2){
		            	 			if(errorFlag){
			            	 			errorText+=" </br>";	
			            	 		}
			            	 		errorText+=Utility.getLocaleValuePropByKey("msgAtlease2Option", locale);
			            	 		errorFlag=true;
		            	 		}
		            	 	} else if(question_type.equalsIgnoreCase("Likert Scale")) {
		            	 		if(optionCount < 2){
		            	 			if(errorFlag){
			            	 			errorText+=" </br>";	
			            	 		}
			            	 		errorText+=Utility.getLocaleValuePropByKey("msgAtlease2Option", locale);
			            	 		errorFlag=true;
		            	 		}
		            	 	} else if(question_type.equalsIgnoreCase("Single Selection")) {
		            	 		if(optionCount < 2){
		            	 			if(errorFlag){
			            	 			errorText+=" </br>";	
			            	 		}
			            	 		errorText+=Utility.getLocaleValuePropByKey("msgAtlease2Option", locale);
			            	 		errorFlag=true;
		            	 		}
		            	 	} else if(question_type.equalsIgnoreCase("True / False")) {
		            	 		if(optionCount < 2){
		            	 			if(errorFlag){
			            	 			errorText+=" </br>";	
			            	 		}
			            	 		errorText+=Utility.getLocaleValuePropByKey("msgAtlease2Option", locale);
			            	 		errorFlag=true;
		            	 		}
		            	 	} else if(question_type.equalsIgnoreCase("Rank Type")) {
								if((option1.equalsIgnoreCase("") || option1.equalsIgnoreCase(" ")) || (rank1.equalsIgnoreCase("") || rank1.equalsIgnoreCase(" "))){
									rankTypeCount	= rankTypeCount+1;
								}
								if((option2.equalsIgnoreCase("") || option2.equalsIgnoreCase(" ")) || (rank2.equalsIgnoreCase("") || rank2.equalsIgnoreCase(" "))){
									rankTypeCount	= rankTypeCount+1;
								}
								if((option3.equalsIgnoreCase("") || option3.equalsIgnoreCase(" ")) || (rank3.equalsIgnoreCase("") || rank3.equalsIgnoreCase(" "))){
									rankTypeCount	= rankTypeCount+1;
								}
								if((option4.equalsIgnoreCase("") || option4.equalsIgnoreCase(" ")) || (rank4.equalsIgnoreCase("") || rank4.equalsIgnoreCase(" "))){
									rankTypeCount	= rankTypeCount+1;
								}
								if((option5.equalsIgnoreCase("") || option5.equalsIgnoreCase(" ")) || (rank5.equalsIgnoreCase("") || rank5.equalsIgnoreCase(" "))){
									rankTypeCount	= rankTypeCount+1;
								}
								if((option6.equalsIgnoreCase("") || option6.equalsIgnoreCase(" ")) || (rank6.equalsIgnoreCase("") || rank6.equalsIgnoreCase(" "))){
									rankTypeCount	= rankTypeCount+1;
								}

						        if(rankTypeCount > 4){
						        	if(errorFlag){
				        	 			errorText+=" </br>";	
				        	 		}
				        	 		errorText+=Utility.getLocaleValuePropByKey("msgCorrespondingrank2", locale);
				        	 		errorFlag=true;
						        }
		            	 		
		            	 	}
			            	questionUploadTemp.setDomain_id(domain_name);
			            	questionUploadTemp.setCompetency_id(competency_name);
			            	questionUploadTemp.setObjective_id(objective_name);
			            	questionUploadTemp.setStage1(stage1);
			            	questionUploadTemp.setStage2(stage2);
			            	questionUploadTemp.setStage3(stage3);
			            	questionUploadTemp.setDomain_name(domainNameSet);
			            	questionUploadTemp.setCompetency_name(competencyNameSet);
			            	questionUploadTemp.setObjective_name(objectiveNameSet);
			            	questionUploadTemp.setStage1_name(stageOneNameSet);
			            	questionUploadTemp.setStage2_name(stageTwoNameSet);
			            	questionUploadTemp.setStage3_name(stageThreeNameSet);
			            	questionUploadTemp.setQuestion(question);
			            	questionUploadTemp.setIsExperimental(isExperimental);
			            	questionUploadTemp.setQuestion_weightage(question_weightage);
			            	questionUploadTemp.setMax_marks(max_marks);
			            	questionUploadTemp.setInstructions(instructions);
			            	questionUploadTemp.setQuestion_type(question_type_Id);
			            	questionUploadTemp.setQuestion_type_name(question_type_name);
			            	questionUploadTemp.setAssessmentId(assessmentId);
			            	questionUploadTemp.setSectionId(sectionId);
			            	questionUploadTemp.setOption1(option1);
			            	questionUploadTemp.setScore1(score1);
			            	questionUploadTemp.setRank1(rank1);
			            	questionUploadTemp.setOption2(option2);
			            	questionUploadTemp.setScore2(score2);
			            	questionUploadTemp.setRank2(rank2);
			            	questionUploadTemp.setOption3(option3);
			            	questionUploadTemp.setScore3(score3);
			            	questionUploadTemp.setRank3(rank3);
			            	questionUploadTemp.setOption4(option4);
			            	questionUploadTemp.setScore4(score4);
			            	questionUploadTemp.setRank4(rank4);
			            	questionUploadTemp.setOption5(option5);
			            	questionUploadTemp.setScore5(score5);
			            	questionUploadTemp.setRank5(rank5);
			            	questionUploadTemp.setOption6(option6);
			            	questionUploadTemp.setScore6(score6);
			            	questionUploadTemp.setRank6(rank6);
			            	questionUploadTemp.setItemCode(itemCode);
			            	questionUploadTemp.setSessionid(sessionId);
			            	if(errorText!=null)
			            		questionUploadTemp.setErrortext(errorText);

		            	 	sessionHiber.insert(questionUploadTemp);
	            	}
	            }else{
	            	if(domain_name_count!=11){
	            		 returnVal="1"; 
	            		 questionUploadTempDAO.deleteQuestionTemp(sessionId);
	            	 }
	            }
		     }
			 txOpen.commit();
     	 	 sessionHiber.close();
     	 	System.out.println(Utility.getLocaleValuePropByKey("msgInsertedSuccessfully", locale));
		}catch (Exception e){
			e.printStackTrace();
		}

		return returnVal;
	}
	public static Vector readDataExcelXLS(String fileName) throws IOException {

		System.out.println(":::::::::::::::::readDataExcel XLS::::::::::::");
		Vector vectorData = new Vector();
		
		try{
			InputStream ExcelFileToRead = new FileInputStream(fileName);
			HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
	 
			HSSFSheet sheet=wb.getSheetAt(0);
			HSSFRow row; 
			HSSFCell cell;
	 
			 Iterator rows = sheet.rowIterator();
       	 	 int domain_name_count			=	11;
			 int competency_name_count		=	11;
			 int objective_name_count		=	11;
			 int question_count				=	11;
			 int isExperimental_count		=	11;
			 int question_weightage_count	=	11;
			 int max_marks_count			=	11;
			 int instructions_count			=	11;
			 int question_type_count		=	11;
			 int option1_count				=	11;
			 int score1_count				=	11;
			 int rank1_count				=	11;
			 int option2_count				=	11;
			 int score2_count				=	11;
			 int rank2_count				=	11;
			 int option3_count				=	11;
			 int score3_count				=	11;
			 int rank3_count				=	11;
			 int option4_count				=	11;
			 int score4_count				=	11;
			 int rank4_count				=	11;
			 int option5_count				=	11;
			 int score5_count				=	11;
			 int rank5_count				=	11;
			 int option6_count				=	11;
			 int score6_count				=	11;
			 int rank6_count				=	11;
			 int itemCode_count				=	11;
			 int errortext_count			=	11;
			 
			 int stage1_count				=	11;
			 int stage2_count				=	11;
			 int stage3_count				=	11;
			 
	         String indexCheck="";

			while (rows.hasNext()){
				row=(HSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,dateFlag=false;
				String indexPrev="";
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
				while (cells.hasNext())
				{
					cell=(HSSFCell) cells.next();
					cIndex=cell.getColumnIndex();
					
					if(cell.toString().equalsIgnoreCase("domain_name")){
                    	domain_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(domain_name_count==cIndex){
	            		cellFlag=true;
	            		dateFlag=true;
	            	}

	            	if(cell.toString().equalsIgnoreCase("competency_name")){
	            		competency_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(competency_name_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(cell.toString().equalsIgnoreCase("objective_name")){
	            		objective_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(objective_name_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	/* Stage 1,2,3 */
	            	if(cell.toString().equalsIgnoreCase("stage1")){
	            		stage1_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(stage1_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("stage2")){
	            		stage2_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(stage2_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("stage3")){
	            		stage3_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(stage3_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(cell.toString().equalsIgnoreCase("question")){
	            		question_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(question_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(cell.toString().equalsIgnoreCase("isExperimental")){
	            		isExperimental_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(isExperimental_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("question_weightage")){
	            		question_weightage_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(question_weightage_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(cell.toString().equalsIgnoreCase("max_marks")){
	            		max_marks_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(max_marks_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(cell.toString().equalsIgnoreCase("instructions")){
	            		instructions_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(instructions_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(cell.toString().equalsIgnoreCase("question_type")){
	            		question_type_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(question_type_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(cell.toString().equalsIgnoreCase("option1")){
	            		option1_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(option1_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(cell.toString().equalsIgnoreCase("score1")){
	            		score1_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(score1_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(cell.toString().equalsIgnoreCase("rank1")){
	            		rank1_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(rank1_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(cell.toString().equalsIgnoreCase("option2")){
	            		option2_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(option2_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("score2")){
	            		score2_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(score2_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("rank2")){
	            		rank2_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(rank2_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("option3")){
	            		option3_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(option3_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("score3")){
	            		score3_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(score3_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("rank3")){
	            		rank3_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(rank3_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("option4")){
	            		option4_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(option4_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("score4")){
	            		score4_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(score4_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("rank4")){
	            		rank4_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(rank4_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("option5")){
	            		option5_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(option5_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("score5")){
	            		score5_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(score5_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(cell.toString().equalsIgnoreCase("rank5")){
	            		rank5_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(rank5_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("option6")){
	            		option6_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(option6_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("score6")){
	            		score6_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(score6_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("rank6")){
	            		rank6_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(rank6_count==cIndex){
	            		cellFlag=true;
	            	}
	            	if(cell.toString().equalsIgnoreCase("itemCode")){
	            		itemCode_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(itemCode_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(cell.toString().equalsIgnoreCase("errortext")){
	            		errortext_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(errortext_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
					if(cellFlag){
						if(!dateFlag){
	            			try{
	            				mapCell.put(Integer.valueOf(cIndex),cell.getStringCellValue()+"");
	            			}catch(Exception e){
	            				mapCell.put(Integer.valueOf(cIndex),new Double(cell.getNumericCellValue()).longValue()+"");
	            			}
	            		}else{
	            			mapCell.put(Integer.valueOf(cIndex),cell+"");
	            		}
					}
					dateFlag=false;
		        	cellFlag=false;
					
				}
			    vectorCellEachRowData=cellValuePopulate(mapCell);
				vectorData.addElement(vectorCellEachRowData);
			}
		}catch(Exception e){}
		return vectorData;
    }
	public static Vector readDataExcelXLSX(String fileName) {
		System.out.println("::::::::::::::::: readDataExcel XLSX ::::::::::::");
        Vector vectorData = new Vector();
        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            XSSFWorkbook xssfWorkBook = new XSSFWorkbook(fileInputStream);
            // Read data at sheet 0
            XSSFSheet xssfSheet = xssfWorkBook.getSheetAt(0);
            Iterator rowIteration = xssfSheet.rowIterator();
       	 	 int domain_name_count			=	11;
			 int competency_name_count		=	11;
			 int objective_name_count		=	11;
			 int question_count				=	11;
			 int isExperimental_count		=	11;
			 int question_weightage_count	=	11;
			 int max_marks_count			=	11;
			 int instructions_count			=	11;
			 int question_type_count		=	11;
			 int option1_count				=	11;
			 int score1_count				=	11;
			 int rank1_count				=	11;
			 int option2_count				=	11;
			 int score2_count				=	11;
			 int rank2_count				=	11;
			 int option3_count				=	11;
			 int score3_count				=	11;
			 int rank3_count				=	11;
			 int option4_count				=	11;
			 int score4_count				=	11;
			 int rank4_count				=	11;
			 int option5_count				=	11;
			 int score5_count				=	11;
			 int rank5_count				=	11;
			 int option6_count				=	11;
			 int score6_count				=	11;
			 int rank6_count				=	11;
			 int itemCode_count				=	11;
			 int errortext_count			=	11;
			 int stage1_count	=	11;
			 int stage2_count	=	11;
			 int stage3_count	=	11;
            // Looping every row at sheet 0
            String indexCheck="";
            while (rowIteration.hasNext()) {
                XSSFRow xssfRow = (XSSFRow) rowIteration.next();
                Iterator cellIteration = xssfRow.cellIterator();
                Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,dateFlag=false;
				String indexPrev="";
                // Looping every cell in each row at sheet 0
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
                while (cellIteration.hasNext()){
                    XSSFCell xssfCell = (XSSFCell) cellIteration.next();
                    cIndex=xssfCell.getColumnIndex();
                   // System.out.println("::::::::::::"+xssfCell.toString());
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("domain_name")){
                    	domain_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(domain_name_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("competency_name")){
	            		competency_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(competency_name_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("objective_name")){
	            		objective_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(objective_name_count==cIndex){
	            		cellFlag=true;
	            	}

	            	/* Stage 1,2,3 */
	            	if(xssfCell.toString().equalsIgnoreCase("stage1")){
	            		stage1_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(stage1_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("stage2")){
	            		stage2_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(stage2_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("stage3")){
	            		stage3_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(stage3_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("question")){
	            		question_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(question_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("isExperimental")){
	            		isExperimental_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(isExperimental_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("question_weightage")){
	            		question_weightage_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(question_weightage_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("max_marks")){
	            		max_marks_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(max_marks_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("instructions")){
	            		instructions_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(instructions_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("question_type")){
	            		question_type_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(question_type_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("option1")){
	            		option1_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(option1_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("score1")){
	            		score1_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(score1_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("rank1")){
	            		rank1_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(rank1_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("option2")){
	            		option2_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(option2_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("score2")){
	            		score2_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(score2_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("rank2")){
	            		rank2_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(rank2_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("option3")){
	            		option3_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(option3_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("score3")){
	            		score3_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(score3_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("rank3")){
	            		rank3_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(rank3_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("option4")){
	            		option4_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(option4_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("score4")){
	            		score4_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(score4_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("rank4")){
	            		rank4_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(rank4_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("option5")){
	            		option5_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(option5_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("score5")){
	            		score5_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(score5_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("rank5")){
	            		rank5_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(rank5_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("option6")){
	            		option6_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(option6_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("score6")){
	            		score6_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(score6_count==cIndex){
	            		cellFlag=true;
	            	}

	            	if(xssfCell.toString().equalsIgnoreCase("rank6")){
	            		rank6_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(rank6_count==cIndex){
	            		cellFlag=true;
	            	}
	            	if(xssfCell.toString().equalsIgnoreCase("itemCode")){
	            		itemCode_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(itemCode_count==cIndex){
	            		cellFlag=true;
	            	}


	            	if(xssfCell.toString().equalsIgnoreCase("errortext")){
	            		errortext_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(errortext_count==cIndex){
	            		cellFlag=true;
	            	}
	            	//System.out.println(cellFlag+" ||| "+cIndex);
	            	if(cellFlag){
	            		if(!dateFlag){
	            			try{
	            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getStringCellValue()+"");
	            			}catch(Exception e){
	            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getRawValue()+"");
	            			}
	            		}else{
	            			mapCell.put(Integer.valueOf(cIndex),xssfCell+"");
	            		}
	            	}
	            	dateFlag=false;
	            	cellFlag=false;
                }
              //  System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::"+mapCell.size());
                vectorCellEachRowData=cellValuePopulate(mapCell);
                vectorData.addElement(vectorCellEachRowData);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
       // System.out.println("vectorData>>>>>>>>"+vectorData.size());
        return vectorData;
    }
	public static Vector cellValuePopulate(Map<Integer,String> mapCell){
		 Vector vectorCellEachRowData = new Vector();
		 Map<Integer,String> mapCellTemp = new TreeMap<Integer, String>();
		 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false,flag9=false;
		 boolean flag10=false,flag11=false,flag12=false,flag13=false,flag14=false,flag15=false,flag16=false,flag17=false,flag18=false,flag19=false;
		 boolean flag20=false,flag21=false,flag22=false,flag23=false,flag24=false,flag25=false,flag26=false,flag27=false,flag28=false,flag29=false;
		 boolean flag30=false;
		 
		 for(Map.Entry<Integer, String> entry : mapCell.entrySet()){
			int key=entry.getKey();
			String cellValue=null;
			if(entry.getValue()!=null)
				cellValue=entry.getValue().trim();
			
			if(key==0){
				mapCellTemp.put(key, cellValue);
				flag0=true;
			}
			if(key==1){
				flag1=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==2){
				flag2=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==3){
				flag3=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==4){
				flag4=true;
				mapCellTemp.put(key, cellValue);
			}
			
			if(key==5){
				flag5=true;
				mapCellTemp.put(key, cellValue);
			}
			
			if(key==6){
				flag6=true;
				mapCellTemp.put(key, cellValue);
			}
			
			if(key==7){
				flag7=true;
				mapCellTemp.put(key, cellValue);
			}
			
			if(key==8){
				flag8=true;
				mapCellTemp.put(key, cellValue);
			}
			
			if(key==9){
				flag9=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==10){
				flag10=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==11){
				flag11=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==12){
				flag12=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==13){
				flag13=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==14){
				flag14=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==15){
				flag15=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==16){
				flag16=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==17){
				flag17=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==18){
				flag18=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==19){
				flag19=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==20){
				flag20=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==21){
				flag21=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==22){
				flag22=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==23){
				flag23=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==24){
				flag24=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==25){
				flag25=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==26){
				flag26=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==27){
				flag27=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==28){
				flag28=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==29){
				flag29=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==30){
				flag30=true;
				mapCellTemp.put(key, cellValue);
			}
		 }
		 if(flag0==false){
			 mapCellTemp.put(0, "");
		 }
		 if(flag1==false){
			 mapCellTemp.put(1, "");
		 }
		 if(flag2==false){
			 mapCellTemp.put(2, "");
		 }
		 if(flag3==false){
			 mapCellTemp.put(3, "");
		 }
		 if(flag4==false){
			 mapCellTemp.put(4, "");
		 }
		 if(flag5==false){
			 mapCellTemp.put(5, "");
		 }
		 if(flag6==false){
			 mapCellTemp.put(6, "");
		 }
		 if(flag7==false){
			 mapCellTemp.put(7, "");
		 }
		 if(flag8==false){
			 mapCellTemp.put(8, "");
		 }
		 if(flag9==false){
			 mapCellTemp.put(9, "");
		 }
		 if(flag10==false){
			 mapCellTemp.put(10, "");
		 }
		 if(flag11==false){
			 mapCellTemp.put(11, "");
		 }
		 if(flag12==false){
			 mapCellTemp.put(12, "");
		 }
		 if(flag13==false){
			 mapCellTemp.put(13, "");
		 }
		 if(flag14==false){
			 mapCellTemp.put(14, "");
		 }
		 if(flag15==false){
			 mapCellTemp.put(15, "");
		 }
		 if(flag16==false){
			 mapCellTemp.put(16, "");
		 }
		 if(flag16==false){
			 mapCellTemp.put(16, "");
		 }
		 if(flag17==false){
			 mapCellTemp.put(17, "");
		 }
		 if(flag18==false){
			 mapCellTemp.put(18, "");
		 }
		 if(flag19==false){
			 mapCellTemp.put(19, "");
		 }
		 if(flag20==false){
			 mapCellTemp.put(20, "");
		 }
		 if(flag21==false){
			 mapCellTemp.put(21, "");
		 }
		 if(flag22==false){
			 mapCellTemp.put(22, "");
		 }
		 if(flag23==false){
			 mapCellTemp.put(23, "");
		 }
		 if(flag24==false){
			 mapCellTemp.put(24, "");
		 }
		 if(flag25==false){
			 mapCellTemp.put(25, "");
		 }
		 if(flag26==false){
			 mapCellTemp.put(26, "");
		 }
		 if(flag27==false){
			 mapCellTemp.put(27, "");
		 }
		 if(flag28==false){
			 mapCellTemp.put(28, "");
		 }
		 if(flag29==false){
			 mapCellTemp.put(29, "");
		 }
		 if(flag30==false){
			 mapCellTemp.put(30, "");
		 }
		 for(Map.Entry<Integer, String> entry : mapCellTemp.entrySet()){
			 vectorCellEachRowData.addElement(entry.getValue());
		 }
		 return vectorCellEachRowData;
	}

}

