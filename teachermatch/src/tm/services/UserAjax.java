package tm.services;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.HqBranchesDistricts;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.TeacherDetailDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.hqbranchesmaster.HqBranchesDistrictsDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;


public class UserAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private UserMasterDAO usermasterdao;
	public void setUsermasterdao(UserMasterDAO usermasterdao) {
		this.usermasterdao = usermasterdao;
	}
	@Autowired
	private  TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
		this.teacherDetailDAO = teacherDetailDAO;
	}
	@Autowired
	private  RoleMasterDAO roleMasterDAO;
	public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO) {
		this.roleMasterDAO = roleMasterDAO;
	}

	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}

	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private HqBranchesDistrictsDAO hqBranchesDistrictsDAO;
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) {
		this.districtKeyContactDAO = districtKeyContactDAO;
	}
	
	public String displayRecordsByEntityType(boolean resultFlag,int entityID,int searchTextId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String firstName,String lastName, String emailAddress,String headQuarterId, String branchId,String districtId,String schoolId)
	{
		
		/* ========  For Session time Out Error =========*/

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			//String locale = Utility.getValueOfPropByKey("locale");
            String lblLastName = Utility.getLocaleValuePropByKey("lblLastName", locale);
            String lblFirstName = Utility.getLocaleValuePropByKey("lblFirstName", locale);
            String lblTitle = Utility.getLocaleValuePropByKey("lblTitle", locale);
            String lblRole = Utility.getLocaleValuePropByKey("lblRole", locale);
            String lblEmail = Utility.getLocaleValuePropByKey("lblEmail", locale);
            String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
            String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
            String lblDeactivate = Utility.getLocaleValuePropByKey("lblDeactivate", locale);
            String lblActivate = Utility.getLocaleValuePropByKey("lblActivate", locale);
            String lblView = Utility.getLocaleValuePropByKey("lblView", locale);
            String lblEdit = Utility.getLocaleValuePropByKey("lblEdit", locale);
            String lblAuthorised = Utility.getLocaleValuePropByKey("lblAuthorised", locale);
            String lblUnauthorised = Utility.getLocaleValuePropByKey("lblUnauthorised", locale);
            String lblDistrictName  = Utility.getLocaleValuePropByKey("lblDistrictName", locale);
            String lblSchoolName  = Utility.getLocaleValuePropByKey("lblSchoolName ", locale);
            String msgActiveUser  = Utility.getLocaleValuePropByKey("msgActiveUser ", locale);
            String msgInactiveUser  = Utility.getLocaleValuePropByKey("msgInactiveUser ", locale);
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord = 0;
			//------------------------------------
			UserMaster userSession	=null;
			int roleId=0, entityType=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userSession		=	(UserMaster) session.getAttribute("userMaster");
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
					entityType=userSession.getEntityType();
				}
			}
			String roleAccess=null;
			try{
				if(entityType==5 || entityType==6)
					roleAccess=roleAccessPermissionDAO.getMenuOptionListByMenuId(roleId,96,"manageuser.do",1,100);
				else
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,1,"manageuser.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<UserMaster> userMaster	  =	null;

			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"firstName";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}

			if(firstName==null)
				firstName="";

			if(lastName==null)
				lastName="";

			if(emailAddress==null)
				emailAddress="";


			ArrayList<Criterion> lstCriterion=new ArrayList<Criterion>();
			lstCriterion.add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE));
			lstCriterion.add( Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE));
			lstCriterion.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
			//lstCriterion.add(Restrictions.eq("entityType", entityID));
			if(branchId!=null && !branchId.trim().equalsIgnoreCase("")){
				lstCriterion.add(Restrictions.eq("branchMaster",branchMasterDAO.findById(Integer.parseInt(branchId), false, false)));
			}
			if(headQuarterId!=null && !headQuarterId.trim().equalsIgnoreCase("")){
				lstCriterion.add(Restrictions.eq("headQuarterMaster",headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false)));
			}
			
			/**End ------------------------------------**/
			Criterion criterion1 = Restrictions.eq("entityType", entityID);
			/*Criterion firstname  = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
			Criterion lastname  = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
			Criterion emailaddress = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);*/
			if(userSession.getEntityType()==1 || userSession.getEntityType()==2 || userSession.getEntityType()==3){
				if(entityID==0 && userSession.getEntityType()==2){
					DistrictMaster districtMaster =districtMasterDAO.findById(userSession.getDistrictId().getDistrictId(), false, false);
					//Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
					lstCriterion.add(Restrictions.eq("districtId",districtMaster));
					Criterion[] cri=new Criterion[lstCriterion.size()];
					int i=0;
					for(Criterion cr:lstCriterion){
						System.out.println(":::::::::::->>>"+cr);
						cri[i]=cr;
						++i;
					}
					totaRecord = usermasterdao.getRowCountUserByRole(cri);
					userMaster = usermasterdao.findByUserByRole(sortOrderStrVal,start,noOfRowInPage,cri);
					System.out.println(" First totaRecord "+totaRecord);
				}else if(entityID==0){
					Criterion[] cri=new Criterion[lstCriterion.size()];
					int i=0;
					for(Criterion cr:lstCriterion){
						System.out.println(":::::::::::->>>"+cr);
						cri[i]=cr;
						++i;
					}
					totaRecord = usermasterdao.getRowCountUserByRole(cri);
					userMaster = usermasterdao.findByUserByRole(sortOrderStrVal,start,noOfRowInPage,cri);
					System.out.println(" Second :::: totaRecord "+totaRecord);
				}else if(entityID==2 && searchTextId!=0 && userSession.getEntityType()!=3){
					
					DistrictMaster districtMaster =districtMasterDAO.findById(searchTextId, false, false);
					
					//Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
					lstCriterion.add(Restrictions.eq("districtId",districtMaster));
					
					System.out.println( " >>>>>>> userSession.getEntityType() "+userSession.getEntityType());
					
					lstCriterion.add(criterion1);
					
					Criterion[] cri=new Criterion[lstCriterion.size()];
					int i=0;
					for(Criterion cr:lstCriterion){
						System.out.println(":::::::::::->>>"+cr);
						cri[i]=cr;
						++i;
					}
					totaRecord = usermasterdao.getRowCountUserByRole(cri);
					userMaster = usermasterdao.findByUserByRole(sortOrderStrVal,start,noOfRowInPage,cri);
					System.out.println(" Third >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>totaRecord "+totaRecord);
	
				}else if(entityID==3 && searchTextId!=0 && userSession.getEntityType()==2){
					DistrictMaster districtMaster =districtMasterDAO.findById(userSession.getDistrictId().getDistrictId(), false, false);
					Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
					SchoolMaster schoolMaster =schoolMasterDAO.findById((long)searchTextId, false, false);
					Criterion criterion3 = Restrictions.eq("schoolId",schoolMaster);
					
					lstCriterion.add(Restrictions.eq("districtId",districtMaster));
					lstCriterion.add(Restrictions.eq("schoolId",schoolMaster));
					lstCriterion.add(criterion1);
					Criterion[] cri=new Criterion[lstCriterion.size()];
					int i=0;
					for(Criterion cr:lstCriterion){
						System.out.println(":::::::::::->>>"+cr);
						cri[i]=cr;
						++i;
					}
					
					totaRecord = usermasterdao.getRowCountUserByRole(cri);
					userMaster = usermasterdao.findByUserByRole(sortOrderStrVal,start,noOfRowInPage,cri);
					System.out.println(" Fourth totaRecord "+totaRecord);
	
				}else if(entityID==3 && searchTextId!=0){
					SchoolMaster schoolMaster =schoolMasterDAO.findById((long)searchTextId, false, false);
					//Criterion criterion3 = Restrictions.eq("schoolId",schoolMaster);
					
					lstCriterion.add(Restrictions.eq("schoolId",schoolMaster));
					lstCriterion.add(criterion1);
					Criterion[] cri=new Criterion[lstCriterion.size()];
					int i=0;
					for(Criterion cr:lstCriterion){
						System.out.println(":::::::::::->>>"+cr);
						cri[i]=cr;
						++i;
					}
					
					totaRecord = usermasterdao.getRowCountUserByRole(cri);
					userMaster = usermasterdao.findByUserByRole(sortOrderStrVal,start,noOfRowInPage,cri);
					System.out.println(" Fifth totaRecord "+totaRecord);
				}else if(userSession.getEntityType()==1){
					lstCriterion.add(criterion1);
					
					System.out.println(">>>>>>>>>>>>> lstCriterion.size() >>>>>>>>>> "+lstCriterion.size());
					
					Criterion[] cri=new Criterion[lstCriterion.size()];
					int i=0;
					for(Criterion cr:lstCriterion){
						System.out.println(":::::::::::->>>"+cr);
						cri[i]=cr;
						++i;
					}
					totaRecord = usermasterdao.getRowCountUserByRole(cri);
					userMaster = usermasterdao.findByUserByRole(sortOrderStrVal,start,noOfRowInPage,cri);
					System.out.println(" Six totaRecord:::: "+totaRecord);
				}else if(userSession.getEntityType()==2 && entityID==3){
					List schoolIds = new ArrayList();
					if(userSession.getDistrictId()!=null){
						schoolIds =	districtSchoolsDAO.findSchoolObjAllList(userSession.getDistrictId());
						if(schoolIds.size()>0){
							//Criterion criterionSchoolIds = Restrictions.in("schoolId",schoolIds);
							
							lstCriterion.add(criterion1);
							lstCriterion.add(Restrictions.in("schoolId",schoolIds));
							Criterion[] cri=new Criterion[lstCriterion.size()];
							int i=0;
							for(Criterion cr:lstCriterion){
								System.out.println(":::::::::::->>>"+cr);
								cri[i]=cr;
								++i;
							}
							totaRecord = usermasterdao.getRowCountUserByRole(cri);
							userMaster = usermasterdao.findByUserByRole(sortOrderStrVal,start,noOfRowInPage,cri);
							System.out.println(" Seven totaRecord "+totaRecord);
						}else{
							userMaster=  new ArrayList<UserMaster>();
							System.out.println(" Seven else condition ");
						}
					}
				}else if(userSession.getEntityType()==2 && entityID==2){
					DistrictMaster districtMaster =districtMasterDAO.findById(userSession.getDistrictId().getDistrictId(), false, false);
					Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
					
					lstCriterion.add(criterion1);
					lstCriterion.add(Restrictions.eq("districtId",districtMaster));
					Criterion[] cri=new Criterion[lstCriterion.size()];
					int i=0;
					for(Criterion cr:lstCriterion){
						System.out.println(":::::::::::->>>"+cr);
						cri[i]=cr;
						++i;
					}
					totaRecord = usermasterdao.getRowCountUserByRole(cri);
					userMaster = usermasterdao.findByUserByRole(sortOrderStrVal,start,noOfRowInPage,cri);
					System.out.println(" Eight totaRecord "+totaRecord);
				}
			}else if(userSession.getEntityType()==5 || userSession.getEntityType()==6)
			{
				//UserMaster				
				System.out.println("enty============="+5555555);	
				lstCriterion=new ArrayList<Criterion>();
				//if(userSession.getEntityType()!=6)
				lstCriterion.add(Restrictions.eq("headQuarterMaster",userSession.getHeadQuarterMaster()));
				
				lstCriterion.add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE));
				lstCriterion.add( Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE));
				lstCriterion.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
				if(districtId!=null && !districtId.trim().equalsIgnoreCase("")){
					lstCriterion.add(Restrictions.eq("districtId",districtMasterDAO.findById(Integer.parseInt(districtId), false, false)));
				}
				if(branchId!=null && !branchId.trim().equalsIgnoreCase("")){
					lstCriterion.add(Restrictions.eq("branchMaster",branchMasterDAO.findById(Integer.parseInt(branchId), false, false)));
				}
				System.out.println("searchTextId==:::::::::::::::::::::::::::======"+searchTextId);			
					/*if(entityID== 2 && schoolId!=null && !schoolId.trim().equalsIgnoreCase("")){
						lstCriterion.add(Restrictions.eq("schoolId",schoolMasterDAO.findById(Long.parseLong(schoolId), false, false)));
					}*/				
				if(entityID==3 && searchTextId!=0){
					lstCriterion.add(Restrictions.eq("schoolId",schoolMasterDAO.findById((long)searchTextId, false, false)));
				}	
				if(userSession.getEntityType()==5){
					lstCriterion.add(Restrictions.eq("headQuarterMaster", userSession.getHeadQuarterMaster()));
				}
				if(userSession.getEntityType()==6){
					lstCriterion.add(Restrictions.eq("headQuarterMaster", userSession.getHeadQuarterMaster()));
					lstCriterion.add(Restrictions.eq("branchMaster", userSession.getBranchMaster()));
				}
				
				Criterion[] cri=new Criterion[lstCriterion.size()];
				int i=0;
				for(Criterion cr:lstCriterion){
					System.out.println(":::::::::::->>>"+cr);
					cri[i]=cr;
					++i;
				}
				//Criterion criterion = Restrictions.eq("headQuarterMaster",userSession.getHeadQuarterMaster());
				totaRecord = usermasterdao.getRowCountUserByRole(cri);
				userMaster = usermasterdao.findByUserByRole(sortOrderStrVal,start,noOfRowInPage,cri);
				System.out.println(" totaRecord for headQuarter :: "+totaRecord);
			}

			if(resultFlag==false){
				totaRecord=0;
			}
			DistrictKeyContact userKeyContact=null;
			try{
				List<DistrictKeyContact>	districtKeyContactList1=districtKeyContactDAO.findByContactType(userSession, "Manage Users");
				if(districtKeyContactList1!=null && districtKeyContactList1.size()>0){
					userKeyContact=districtKeyContactList1.get(0);
				
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");

			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblLastName,sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='18%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblFirstName,sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='18%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblTitle,sortOrderFieldName,"title",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='18%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblRole,sortOrderFieldName,"roleName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='22%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblEmail,sortOrderFieldName,"emailAddress",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='22%' valign='top'>"+responseText+"</th>");
			tmRecords.append("<th width='14%'valign='top'>"+lblActions+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(resultFlag){
				if(userMaster==null || userMaster.size()==0)
				{
					tmRecords.append("<tr><td colspan='6'>"+lblNoRecord+"</td></tr>" );
				}
				else
				{
					boolean showEditModeUser=true;
					if(userSession.getEntityType()==2){
						if(userKeyContact==null){
							showEditModeUser=false;
						}
						if(userSession.getRoleId().getRoleId()==7){
							showEditModeUser=true;
						}
					} else if(userSession.getEntityType()==3){
						if(userSession.getDistrictId().getSaAddUser()==null || !userSession.getDistrictId().getSaAddUser()){
							showEditModeUser=false;
						}
					}
				for (UserMaster userMasterDetail : userMaster) 
				{
					String title	=	userMasterDetail.getTitle()==null?"":userMasterDetail.getTitle();
					String location = "";
					try
					{
						if(userMasterDetail.getEntityType()==2)
							location = "<table><tr><td>"+lblDistrictName+": "+userMasterDetail.getDistrictId().getDistrictName()+"</td></tr></table>";
						if(userMasterDetail.getEntityType()==3)
						{
							location = "<table>" +
							"<tr><td>"+lblDistrictName+": "+userMasterDetail.getDistrictId().getDistrictName()+"</td></tr>" +
							"<tr><td>"+lblSchoolName+": "+userMasterDetail.getSchoolId().getSchoolName()+"</td></tr>" +
							"</table>" ;
						}
					}catch(Exception e)
					{
						e.printStackTrace();
					}


					tmRecords.append("<tr>" );
					tmRecords.append("<td>"+userMasterDetail.getLastName()+"</td>");
					tmRecords.append("<td>"+userMasterDetail.getFirstName()+"</td>");
					tmRecords.append("<td>"+title+"</td>");
					location= location.replace("'", "&#39;");
					String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
					tmRecords.append("<td><a data-html='true' href='javascript:void(0)' rel='tooltip' data-original-title='"+location+"' id='usr"+userMasterDetail.getUserId()+"' "+windowFunc+"\">"+userMasterDetail.getRoleId().getRoleName()+"</a></td>");
					tmRecords.append("<script>$('#usr"+userMasterDetail.getUserId()+"').tooltip();</script>");
					tmRecords.append("<td>"+userMasterDetail.getEmailAddress()+"");
					tmRecords.append("<td>");
					boolean pipeFlag=false;
					if(roleAccess.indexOf("|2|")!=-1){
						if(showEditModeUser){
							tmRecords.append("<a href='#' title='"+lblEdit+"' onclick=\"return beforeEditUser('"+userMasterDetail.getUserId()+"')\"><i class='fa fa-pencil-square-o fa-lg'></i></a>");
							pipeFlag=true;
						} else{
							tmRecords.append("<a href='addedituser.do?&userId="+userMasterDetail.getUserId()+"'>"+lblView+"</a>");
							pipeFlag=true;
						}
					}else if(roleAccess.indexOf("|4|")!=-1){
						tmRecords.append("<a href='addedituser.do?&userId="+userMasterDetail.getUserId()+"'>"+lblView+"</a>");
						pipeFlag=true;
					}
					if(roleAccess.indexOf("|7|")!=-1){
						if(showEditModeUser){
						if(pipeFlag)tmRecords.append(" | ");
						/*	modified */
						
						if(userMasterDetail.getStatus().equalsIgnoreCase("A") && (userMasterDetail.getIsQuestCandidate()==null || !userMasterDetail.getIsQuestCandidate()) )
							tmRecords.append("<a href='javascript:void(0);' title='"+msgActiveUser+"' onclick=\"return activateDeactivateUser("+entityID+","+userMasterDetail.getUserId()+",'I')\"><i class='fa fa-times fa-lg' style='color:red;'></i></a>");							
						else if(userMasterDetail.getStatus().equalsIgnoreCase("I") && (userMasterDetail.getIsQuestCandidate()==null || !userMasterDetail.getIsQuestCandidate()))
							tmRecords.append("<a href='javascript:void(0);' title='"+msgInactiveUser+"' onclick=\"return activateDeactivateUser("+entityID+","+userMasterDetail.getUserId()+",'A')\"><i class='fa fa-check fa-lg' style='color:green;'></i></a>");						
						
						/*
						 * added by ankit to add Authorised / Unauthorised Link for Non-Client District
						 */
						/*   start   */
						if((userMasterDetail.getIsQuestCandidate()!=null || Boolean.valueOf(userMasterDetail.getIsQuestCandidate())))
						{						
							if(userMasterDetail.getStatus().equalsIgnoreCase("A"))
							{
								if(userMasterDetail.getIsQuestCandidate()!=null && userMasterDetail.getIsQuestCandidate()==true)
								{
									tmRecords.append("<a href='javascript:void(0);' title='"+msgActiveUser+"' onclick=\"return activateDeactivateQuestUser("+entityID+","+userMasterDetail.getUserId()+",'I')\"><i class='fa fa-times fa-lg' style='color:red;'></i></a>");
									tmRecords.append(" | ");
									tmRecords.append("<a href='javascript:void(0);' title='"+msgInactiveUser+"' onclick=\"return beforeAuthorisedUser("+entityID+","+userMasterDetail.getUserId()+",'I','"+userMasterDetail.getEmailAddress()+"')\"><i class='fa fa-check fa-lg' style='color:green;'></i></a>");									
								
								}									
								}						
							else if(userMasterDetail.getStatus().equalsIgnoreCase("I"))
							{
								if(userMasterDetail.getIsQuestCandidate()!=null && userMasterDetail.getIsQuestCandidate()==true)
								{
									tmRecords.append("<a href='javascript:void(0);' title='"+msgActiveUser+"' onclick=\"return activateDeactivateQuestUser("+entityID+","+userMasterDetail.getUserId()+",'A')\"><i class='fa fa-times fa-lg' style='color:red;'></a>");
									tmRecords.append(" | ");
									tmRecords.append("<a href='javascript:void(0);' title='"+msgInactiveUser+"' onclick=\"return beforeAuthorisedUser("+entityID+","+userMasterDetail.getUserId()+",'A','"+userMasterDetail.getEmailAddress()+"')\"><i class='fa fa-check fa-lg' style='color:green;'></a>");						
								
								}
								
								}
						}
						
						}
						/*   end   */
						
					}else{
						tmRecords.append("&nbsp;");
					}
					tmRecords.append("</td>");
					tmRecords.append("</tr>");
				}
				}
			}else{
				tmRecords.append("<tr><td colspan='6'>"+lblNoRecord+"</td></tr>" );
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjaxForDSPQ(request,totaRecord,noOfRow, pageNo));
		}catch (Exception e) {
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
	public String showFile(int userId,String docFileName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String path="";
		String source="";
		String target="";
		try 
		{
			source = Utility.getValueOfPropByKey("userRootPath")+userId+"/"+docFileName;
			target = context.getServletContext().getRealPath("/")+"/"+"/user/"+userId+"/";
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+"/user/"+userId+"/"+docFileName;

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return path;
	}
	/*======== It check duplicate user email address in teacherdetail and usermaster table ============*/
	public int checkDuplicateUserRecords(int userId,String emailAddress,String schoolId,int entityType ,String districtId,String headQuarterId,String branchId)
	{
		System.out.println("userId:"+userId);
		System.out.println("emailAddress:"+emailAddress);
		System.out.println("schoolId:"+schoolId);
		System.out.println("districtId:"+districtId);
		System.out.println("headQuarterId:"+headQuarterId);
		System.out.println("branchId:"+branchId);
		
		/* ========  For Session time Out Error =========*/
	    WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession=null;
		
		
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		try
		{
	//**********Adding By Deepak for Head Quarter and branch ****************************	
				if((entityType==5 || entityType == 6))
				{
					Criterion criterion2 = Restrictions.eq("headQuarterMaster.headQuarterId",Integer.parseInt(headQuarterId));
					Criterion criterion = Restrictions.eq("emailAddress",emailAddress);
					List< UserMaster> duplicateBranchEmail=null;
					if(entityType==5)
					{
						System.out.println("With two criteria .......");
						duplicateBranchEmail= usermasterdao.findByCriteria(criterion,criterion2);
					}
					else
					{
						List<UserMaster>    Emailforbranch			=usermasterdao.checkUserEmail(emailAddress);

						if(Emailforbranch!=null && Emailforbranch.size()>0)
						{
							if(Emailforbranch.get(0).getHeadQuarterMaster()==null || Emailforbranch.get(0).getBranchMaster()==null)
							{
								return 3;
							}
							else if(Emailforbranch.get(0).getHeadQuarterMaster().getHeadQuarterId()== Integer.parseInt(headQuarterId))
							{
								System.out.println("with three criteria.........");
								Criterion criterion1 = Restrictions.eq("branchMaster.branchId",Integer.parseInt(branchId));
								List< UserMaster> duplicateBranchEmail1= usermasterdao.findByCriteria(criterion,criterion1,criterion2);
								if(duplicateBranchEmail1!=null && duplicateBranchEmail1.size()>0){
									if(userId!=0)
										return 1;
									else
										return 12;
								}
									

							}
							else
							{
								return 11;
							}

						}
						/*else
						{
							return 10;
						}*/
					}

					System.out.println("branch ID  =========<>========== :::"+districtId+"  ,headQuarter ID  =========<>========== :::"+headQuarterId);
					if(duplicateBranchEmail!=null && duplicateBranchEmail.size()>0)
					{
						System.out.println("Duplicate Email  ....  "+duplicateBranchEmail.size());
						if(userId!=0)
							return 1;
						else
							return 3;
					}
					else
					{
						List<UserMaster>    checkExistsEmail = usermasterdao.checkUserEmail(emailAddress);
						if(checkExistsEmail!=null && checkExistsEmail.size()>0 && ((checkExistsEmail.get(0).getHeadQuarterMaster()==null && checkExistsEmail.get(0).getBranchMaster()==null) || (checkExistsEmail.get(0).getEntityType()!=6)))
							return 3;
						else
							return 1;
					}


			}else{
				
			if(userId!=0 && (userSession.getEntityType()==5 || userSession.getEntityType()==6))
			{		
				System.out.println("userId for update :::::::::======="+userId);			
					return 1;
								
			}else if(entityType==2)
			{
				/*if(userSession.getEntityType()==5 || userSession.getEntityType()==6){
					districtId = districtId;
					schoolId=schoolId;
				}else*/{
				districtId = schoolId;
				schoolId="";
				}
			}
			if((districtId!=null&&!districtId.equals(""))&& entityType==3 && userId==0)                                        
			{

				List<TeacherDetail> dupteacherEmail	=	teacherDetailDAO.findByEmail(emailAddress);
				if(dupteacherEmail!=null && dupteacherEmail.size()>0)
				{
					return 4;
				}
				
				System.out.println("districtId => "+districtId);
				 
				 Integer districtIdInintger = Integer.parseInt(districtId);
		         Long schoolIdLong =Long.parseLong(schoolId);
				
				List< UserMaster> userMasterListDistId = usermasterdao.findByCriteria(Restrictions.and(Restrictions.eq("districtId.districtId",districtIdInintger), Restrictions.eq("emailAddress", emailAddress)));
                List< UserMaster> userMasterListSchId = usermasterdao.findByCriteria(Restrictions.and(Restrictions.eq("schoolId.schoolId", schoolIdLong), Restrictions.eq("emailAddress", emailAddress)));
                List< UserMaster> emailaddress = usermasterdao.findByCriteria(Restrictions.eq("emailAddress", emailAddress));
			    List<UserMaster> dupUserEmail =	usermasterdao.checkDuplicateUserEmail(userId,emailAddress);
			    
			    if((userMasterListDistId!=null && userMasterListDistId.size()>0)&&(userMasterListSchId==null || userMasterListSchId.size()<=0) &&(emailaddress.size()>0))
			    {
			    	return 1;
			    }
			    
			    if((userMasterListDistId==null || userMasterListDistId.size()<=0)&&(userMasterListSchId==null || userMasterListSchId.size()<=0) &&(emailaddress.size()>0))
			    {
			    	return 3;
			    }
			    if((userMasterListDistId==null || userMasterListDistId.size()<=0)&&(userMasterListSchId==null || userMasterListSchId.size()<=0) &&(emailaddress.size()<=0)&&(dupUserEmail.size()<0))
			    {
			    	return 1;
			    }
			    if((userMasterListDistId!=null && userMasterListDistId.size()>0)&&(userMasterListSchId!=null && userMasterListSchId.size()>0) &&(emailaddress.size()>0))
			    {
			    	return 3;
			    }
			  }
		  else{
			       /*======= Check For Unique User Email address ==========*/
			  if(userId!=0)
			  {
                 List< UserMaster> userID = usermasterdao.findByCriteria(Restrictions.eq("userId",userId));
			     
                 Integer entitytype = userID.get(0).getEntityType();
                
                 Integer dID = null;
                 
                 if(entitytype>1)
                 {
                   DistrictMaster  Did =userID.get(0).getDistrictId();
                   dID = Did.getDistrictId();
                 }          	
                            	
			   if(userID.size()>0&&dID!=null)
			   {
					// List< UserMaster> userMasterListDistId = usermasterdao.findByCriteria(Restrictions.and(Restrictions.eq("districtId.districtId",dd), Restrictions.eq("emailAddress", emailAddress)));
				    Criterion criterion1 = Restrictions.not(Restrictions.eq("districtId.districtId",dID));
					Criterion criterion2 = Restrictions.eq("emailAddress",emailAddress);
					
					List< UserMaster> userMasterListDistId = usermasterdao.findByCriteria(criterion1,criterion2);
					
				    if(userMasterListDistId.size()!=0)
				    {
						
					   return 3; 
				    
				    }else{
				    	
					  return 1;
				    
				    }
			  }
		}
			
  /* === it return 3 if find duplicate email address from usermaster Table=======*/
	 				
 			   List<TeacherDetail> dupteacherEmail			=	teacherDetailDAO.findByEmail(emailAddress);
 			   int sizeTeacherList 						    =	dupteacherEmail.size();
 			   if(sizeTeacherList>0)
 				 return 4;
	 			   
              List<UserMaster> dupUserEmail =	usermasterdao.checkDuplicateUserEmail(userId,emailAddress);
              int size 	=	dupUserEmail.size();
 			  if(entityType==1)
 			  {
 				  if(size>0)
 				  {
 					  return 3;
 				  }
 			   }
	          
              if(size>0)
               {
                     List<UserMaster> districtidAndEmailAdderss=usermasterdao.checkDuplicateEmailAddersAndEntityType3(emailAddress, districtId);
                     if(districtidAndEmailAdderss.size()>0||districtidAndEmailAdderss!=null )
                    	   {
                        	List<UserMaster> districtidAndEmailAdderssEntityType2=usermasterdao.checkDuplicateEmailAddersAndEntityType2(emailAddress, districtId, entityType);
                    	   if(districtidAndEmailAdderssEntityType2.size()>0)
                    	   {
                    		   return 4;
                    	   }else{
                    		   return 1;
                    	   }
                       }else{
                    	   return 4;
                       }
               }else{
            	   return 1;
               }
		    }
		}
	}
	//deepak			
		catch (Exception e)
		{
			e.printStackTrace();
			return 2;
		}
		
		return 1;
		
	}
	/*======== It return RoleName from rolemaster corresponding Entity Type ============*/
	public String getRoleNameByEntityType(int entity_type,int roleId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<RoleMaster> roleMaster  =	null;
		StringBuffer roleNameOptions =	new StringBuffer();
		try{
			roleMaster	=	roleMasterDAO.getRoleNameByEntityType(entity_type);
			roleNameOptions.append("<select class='form-control' name='AEU_RoleName' id='AEU_RoleName'>" );
			if(roleId==0)
			{
				roleNameOptions.append("<option value='0' selected='selected'>Select Role</option>");
			}
			for (RoleMaster roleMasterDetail : roleMaster) 
			{
				if((roleId!=0)&&(roleMasterDetail.getRoleId()==roleId))
				{
					roleNameOptions.append("<option id='"+roleMasterDetail.getRoleId()+"' value='"+roleMasterDetail.getRoleId()+"' selected='selected' >"+roleMasterDetail.getRoleName()+"</option>");
				}
				else
				{
					roleNameOptions.append("<option id='"+roleMasterDetail.getRoleId()+"' value='"+roleMasterDetail.getRoleId()+"'>"+roleMasterDetail.getRoleName()+"</option>");
				}
			}
			roleNameOptions.append("</select>");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return roleNameOptions.toString();
	}

	/* ===============      Activate Deactivate User        =========================*/
	@Transactional(readOnly=false)
	public boolean activateDeactivateUser(int userId,String status)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		UserMaster userSession=null;
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{
			userSession=(UserMaster)session.getAttribute("userMaster");
			UserMaster userMaster	=	usermasterdao.findById(userId, false, false);
			userMaster.setStatus(status);
			usermasterdao.makePersistent(userMaster);			
			String messageSubject="";			
			String username=userMaster.getFirstName()+" "+userMaster.getLastName();		
			Criterion crtentitytype=Restrictions.eq("entityType",1);
			RoleMaster rolm=roleMasterDAO.findById(1,false,false);
			Criterion crtrollid=Restrictions.eq("roleId",rolm);
			List<UserMaster> um = usermasterdao.findByCriteria(crtentitytype,crtrollid);					
			Date d=new Date();
			String messageSend="";			
		//	SimpleDateFormat dformat=new SimpleDateFormat("HH:MM:SS");
			if(status.equals("I")){
				messageSubject=Utility.getLocaleValuePropByKey("msgDistrictSchoolDeactivated", locale);
				messageSend=Utility.getLocaleValuePropByKey("msgDearTMAdmin", locale)+",<br><br>" +username+
				Utility.getLocaleValuePropByKey("msgWasDeactivatedby", locale) + userSession.getFirstName()+" "+userSession.getLastName()
				+Utility.getLocaleValuePropByKey("msgAt3", locale)+Utility.convertDateAndTimeFormatForEpiAndJsi(d)+". "+Utility.getLocaleValuePropByKey("msgPleaseVerifyWith", locale)+userMaster.getFirstName()+" "+userMaster.getLastName()+
						"("+userMaster.getEmailAddress()+")"+
						Utility.getLocaleValuePropByKey("msgIntendedAction", locale);

			}
			if(status.equals("A"))
			{
				messageSubject=Utility.getLocaleValuePropByKey("msgDistrictSchoolActivated", locale);
				messageSend=Utility.getLocaleValuePropByKey("msgDearTMAdmin", locale)+",<br><br>" +username+
				Utility.getLocaleValuePropByKey("msgWasActivatedBy", locale) + userSession.getFirstName()+" "+userSession.getLastName()
				+Utility.getLocaleValuePropByKey("msgAt3", locale)+Utility.convertDateAndTimeFormatForEpiAndJsi(d)+". "+Utility.getLocaleValuePropByKey("msgPleaseVerifyWith", locale)+" "+userMaster.getFirstName()+" "+userMaster.getLastName()+
						"("+userMaster.getEmailAddress()+")"+
						Utility.getLocaleValuePropByKey("msgIntendedAction", locale);
			}

			System.out.println(messageSend);
				String content="";
				try {

					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("admin@netsutra.com");
					dsmt.setMailto("tm-system-notifications@teachermatch.org");
					dsmt.setMailsubject(messageSubject);
					content=MailText.messageForDefaultFont(messageSend, userMaster);					
					dsmt.setMailcontent(content);

					System.out.println("content     "+content);
					try {
						dsmt.start();	
					} catch (Exception e) {}

				} catch (Exception e) {
					e.printStackTrace();
				}
			try{
				String logType="";
				if(status.equalsIgnoreCase("A")){
					logType="Activate user";
				}else{
					logType="Deactivate user";
				}
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return true;
		}
		return true;
	}


	/* ===============      Activate Deactivate Quest User (District)       =========================*/
	@Transactional(readOnly=false)
	public boolean activateDeactivateQuestUser(int userId,String status)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		UserMaster userSession=null;
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{
			userSession=(UserMaster)session.getAttribute("userMaster");
			UserMaster userMaster	=	usermasterdao.findById(userId, false, false);
			userMaster.setStatus(status);
			usermasterdao.makePersistent(userMaster);	
			String messageSubject="";			
			String username=userMaster.getFirstName()+" "+userMaster.getLastName();		
			/*Criterion crtentitytype=Restrictions.eq("entityType",2);
			RoleMaster rolm = roleMasterDAO.findById(2,false,false);
			Criterion crtrollid=Restrictions.eq("roleId",rolm);
			Criterion district = Restrictions.eq("districtId",userMaster.getDistrictId());*/
			Date d=new Date();
			String messageSend="";			
			//SimpleDateFormat dformat=new SimpleDateFormat("HH:MM:SS");
			
			if(status.equals("I")){
				messageSubject=Utility.getLocaleValuePropByKey("msgQuestAcntDeactivated", locale);
				messageSend=Utility.getLocaleValuePropByKey("msgDearTMAdmin", locale)+",<br><br>" +username+
				Utility.getLocaleValuePropByKey("msgWasDeactivatedby", locale) + userSession.getFirstName()+" "+userSession.getLastName()
				+Utility.getLocaleValuePropByKey("msgAt3", locale)+Utility.convertDateAndTimeFormatForEpiAndJsi(d)+". "+Utility.getLocaleValuePropByKey("msgPleaseVerify", locale)+" " +
				Utility.getLocaleValuePropByKey("msgIntendedAction", locale);
			}
			if(status.equals("A"))
			{
				messageSubject=Utility.getLocaleValuePropByKey("msgQuestAcctActivated", locale);
				messageSend=Utility.getLocaleValuePropByKey("lblDear", locale) + userMaster.getFirstName()+" "+userMaster.getLastName()+":<br><br>" +				
				""+Utility.getLocaleValuePropByKey("msgWelcomeTeacherMatchQuest", locale)+"<br><br>" +
				""+Utility.getLocaleValuePropByKey("msgloginEmailAddress", locale)+" " +userMaster.getEmailAddress()+"<br><br>" +
				""+Utility.getLocaleValuePropByKey("msgPasswordChoseDuringSignup", locale)+"<br/><br/> "
				+" "+Utility.getLocaleValuePropByKey("msgHappyPosting", locale)+"<br>"+Utility.getLocaleValuePropByKey("msgPartnerSuccessTeam", locale);
				
			}
			System.out.println(messageSend);
			String content="";
				try {
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("admin@netsutra.com");
					dsmt.setMailto(userMaster.getEmailAddress());
					dsmt.setMailsubject(messageSubject);
					content=MailText.messageForDefaultFontForQuestUser(messageSend, userMaster);					
					dsmt.setMailcontent(content);
					System.out.println("content     "+content);
					try {
						dsmt.start();	
					} catch (Exception e) {}

				} catch (Exception e) {
					e.printStackTrace();
				}
			try{
				String logType="";
				if(status.equalsIgnoreCase("A")){
					logType="Activate user";
				}else{
					logType="Deactivate user";
				}
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return true;
		}
		return true;
	}


	/* ===============      Authorised Unauthorised Quest User  (District)     =========================*/
	@Transactional(readOnly=false)
	public boolean authorisedUnauthorisedUser(int userId,String status)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		UserMaster userSession=null;
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{
			userSession = (UserMaster)session.getAttribute("userMaster");
			UserMaster userMaster	=	usermasterdao.findById(userId, false, false);
			String emailAddress = userMaster.getEmailAddress();
			DistrictMaster districtMaster = userMaster.getDistrictId();
			Criterion criterion = Restrictions.eq("districtId",districtMaster);
			List<UserMaster> users = usermasterdao.findByCriteria(criterion);			
			System.out.println("=====users get===="+users.size());
			if(users!=null && users.size()>0)
			{
				/*for(UserMaster u : users)
				 {
					u.setStatus(status);
					usermasterdao.updatePersistent(u);	
				 }*/
				try {
					usermasterdao.actDeactUsers(users,status);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			String messageSubject="";			
			boolean authorised = false;
			if(status.equals("A"))
			{
				authorised = true;
				messageSubject=Utility.getLocaleValuePropByKey("msgWelcomeToTeacherMatch", locale);			
			}					
			if(authorised == true)
			{			
				Properties  properties = new Properties();  
				try {
					InputStream inputStream = new Utility().getClass().getClassLoader().getResourceAsStream("smtpmail.properties");
					properties.load(inputStream);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				String content="";
				try {
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);					
					dsmt.setMailfrom(properties.getProperty("smtphost.clientservices"));
					dsmt.setMailto(emailAddress);
					dsmt.setMailsubject(messageSubject);
					content=MailText.getRegistrationMailForQuestUser(request, userMaster);					
					dsmt.setMailcontent(content);
					System.out.println("content  "+content);
					try {
						dsmt.start();	
					} catch (Exception e) {}

				} catch (Exception e) {
					e.printStackTrace();
				}			
			}
			try{
				String logType="";
				if(status.equalsIgnoreCase("A")){
					logType="Activate user";
				}else{
					logType="Deactivate user";
				}
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return true;
		}
		return true;
	}

	/*======== It will send new password email to user ============*/
	public String sendNewPwdToUser(int userId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer sb =	new StringBuffer();
		try{
			int verificationCode=(int) Math.round(Math.random() * 2000000);

			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			UserMaster userMaster		=	usermasterdao.findById(userId, false, false);

			if(userId!=0)
			{	
				userMaster.setUserId(userId);
				userMaster.setVerificationCode(""+verificationCode);
			}
			usermasterdao.makePersistent(userMaster);
			emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), Utility.getLocaleValuePropByKey("msgRequestResettingPassword", locale),MailText.newPwdMailToUser(request,userMaster));
			sb.append("&#149; "+Utility.getLocaleValuePropByKey("msgNewPasswordSentSuccEmail", locale)+" <br>");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	public List<DistrictMaster> getFieldOfDistrictList(String DistrictName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		try{

			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "A");
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);

				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);

				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25,criterion1);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return districtMasterList;
	}
	public List<SchoolMaster> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<SchoolMaster> schoolMasterList =  new ArrayList<SchoolMaster>();
		List<SchoolMaster> fieldOfSchoolList1 = null;
		List<SchoolMaster> fieldOfSchoolList2 = null;
		System.out.println(" districtIdForSchool  :: "+districtIdForSchool);
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
			if(districtIdForSchool==0){
				System.out.println("TMAdmin Search");
				System.out.println(" districtIdForSchool :: "+districtIdForSchool);
				List schoolIds = new ArrayList();
				schoolIds =	districtSchoolsDAO.findSchoolIdAllList();
				Criterion criterionSchoolIds = Restrictions.in("schoolId",schoolIds);
				if(schoolIds.size()>0){
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterionSchoolIds);

						Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion2,criterionSchoolIds);

						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<SchoolMaster> setSchool = new LinkedHashSet<SchoolMaster>(schoolMasterList);
						schoolMasterList = new ArrayList<SchoolMaster>(new LinkedHashSet<SchoolMaster>(setSchool));
					}else{
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterionSchoolIds);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
					//System.out.println(" List Size ::: "+schoolMasterList.size());
				}
			}else{
				System.out.println("DistrictAdmin Search");
				DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
				Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
				List schoolIds = new ArrayList();
				schoolIds =	districtSchoolsDAO.findSchoolIdList(districtMaster);
				Criterion criterionSchoolIds = Restrictions.in("schoolId",schoolIds);
				if(schoolIds.size()>0){
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2,criterionSchoolIds);
						Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2,criterionSchoolIds);

						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<SchoolMaster> setSchool = new LinkedHashSet<SchoolMaster>(schoolMasterList);
						schoolMasterList = new ArrayList<SchoolMaster>(new LinkedHashSet<SchoolMaster>(setSchool));
					}else{
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2,criterionSchoolIds);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
					//System.out.println(" List Size ::: "+schoolMasterList.size());
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return schoolMasterList;
	}
	public DistrictMaster getDistrictId(int schooId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		DistrictMaster districtMaster =null;

		List<SchoolMaster> fieldOfSchoolList = null;
		long schoolId = (long) (schooId);
		try{
			Criterion criterion = Restrictions.eq("schoolId",schoolId);
			fieldOfSchoolList = schoolMasterDAO.findByCriteria(criterion);
			districtMaster =districtMasterDAO.findById(fieldOfSchoolList.get(0).getDistrictId().getDistrictId(), false, false);

		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtMaster;
	}
	
	public List<DistrictMaster> getFieldOfDistrictListByBranchOrHQId(String DistrictName,String branchId,String headQuarterId)
	{
		
		/* ========  For Session time Out Error =========*/
		boolean flagfornulldistrict=false;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		List<Integer> districtIdList=new ArrayList<Integer>();
		Criterion branch_criterion=null;
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		System.out.println("entityType=="+userSession.getEntityType()+"   he=="+userSession.getHeadQuarterMaster());		
		
		if(userSession.getEntityType()==1 && (branchId==null || branchId.equals("")) && (headQuarterId==null || headQuarterId.equals("")))
		{
			//For NC Only
			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "A");
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				if(branch_criterion!=null)
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1,branch_criterion);
				else
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);

				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				if(branch_criterion!=null)
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1,branch_criterion);
				else
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);

				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
			}
		} 
		else
		{
			if(userSession.getHeadQuarterMaster()!=null && userSession.getHeadQuarterMaster().getIsBranchExist()){
				districtIdList.clear();
			if(branchId!=null && !branchId.trim().equalsIgnoreCase("")){
				BranchMaster  bm=branchMasterDAO.findById(Integer.parseInt(branchId), false, false);
				List<HqBranchesDistricts> lstBDRelation=hqBranchesDistrictsDAO.findByCriteria(Restrictions.eq("branchMaster", bm),Restrictions.eq("headQuarterMaster", bm.getHeadQuarterMaster()));
				
				
				if(lstBDRelation!=null && lstBDRelation.size()>0){
					HqBranchesDistricts hbdistrict=	lstBDRelation.get(0);
				if(hbdistrict.getDistrictId()!=null && !hbdistrict.getDistrictId().trim().equalsIgnoreCase("")){
					String[] districtId=hbdistrict.getDistrictId().split(",");				
					for(String dId:districtId){
						if(dId!=null && !dId.trim().equalsIgnoreCase(""))
						districtIdList.add(Integer.parseInt(dId));
					}				
				}			
			}
			}
			flagfornulldistrict=true;
			}else if((userSession.getHeadQuarterMaster()!=null && !userSession.getHeadQuarterMaster().getIsBranchExist()) || (userSession.getEntityType()==1 && headQuarterId!=null && !headQuarterId.trim().equalsIgnoreCase("")))
			{
				System.out.println("1111111111111111111"+headQuarterId);
				districtIdList.clear();
				HeadQuarterMaster hqm=null; 
				List<HqBranchesDistricts> lstBDRelation=null;
				
				if(userSession.getEntityType()==1 && headQuarterId!=null && !headQuarterId.equals("")){
					System.out.println("headQuarterId==1=="+headQuarterId);
					
					if(headQuarterId!=null && !headQuarterId.equals(""))
						hqm=headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
					
						if(hqm!=null){
							if(hqm.getIsBranchExist() && branchId!=null && !branchId.trim().equalsIgnoreCase(""))
								lstBDRelation=hqBranchesDistrictsDAO.findByCriteria(Restrictions.eq("headQuarterMaster", hqm),Restrictions.eq("branchMaster", branchMasterDAO.findById(Integer.parseInt(branchId), false, false)));
							else
								lstBDRelation=hqBranchesDistrictsDAO.findByCriteria(Restrictions.eq("headQuarterMaster", hqm));
						}
					}else{					
						hqm=userSession.getHeadQuarterMaster();
						System.out.println("headQuarterId==2=="+hqm.getHeadQuarterId());
						lstBDRelation=hqBranchesDistrictsDAO.findByCriteria(Restrictions.eq("headQuarterMaster", hqm));
					}
				
				if(lstBDRelation!=null && lstBDRelation.size()>0){
					for(HqBranchesDistricts hbd:lstBDRelation){
					String[] districtId=hbd.getDistrictId().split(",");				
					for(String dId:districtId){
						if(dId!=null && !dId.trim().equalsIgnoreCase(""))
						districtIdList.add(Integer.parseInt(dId));
					}				
					}				
				}
				flagfornulldistrict=true;
			}
			if(flagfornulldistrict){
				if(districtIdList.size()>0){
					System.out.println("districtIdList=========="+districtIdList);
					branch_criterion=Restrictions.in("districtId", districtIdList);
					flagfornulldistrict=false;
			}else{
					flagfornulldistrict=true;
				}
			}
			
			
			try{
				Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
				Criterion criterion1 = Restrictions.eq("status", "A");
				if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
					if(branch_criterion!=null)
					fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1,branch_criterion);
					else
					fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);

					Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
					if(branch_criterion!=null)
					fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1,branch_criterion);
					else
					fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);

					districtMasterList.addAll(fieldOfDistrictList1);
					districtMasterList.addAll(fieldOfDistrictList2);
					Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
					districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
				}else{
					if(branch_criterion!=null)
					fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25,criterion1,branch_criterion);
					else
					fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25,criterion1);
					districtMasterList.addAll(fieldOfDistrictList1);
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			System.out.println("flagfornulldistrict=====gg========"+flagfornulldistrict+" size===="+districtMasterList.size());
			if(flagfornulldistrict){
				districtMasterList.clear();	
			}
		}
		
		return districtMasterList;
	}
	
}


