package tm.services;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

/** Inner class to add a header and a footer. */
public class HeaderAndFooter extends PdfPageEventHelper {
	/** Alternating phrase for the header. */
	Phrase[] header = new Phrase[2];
	/** Current page number (will be reset for every chapter). */
	int pagenumber;
	String cdtName = null;
	Font footerFont = null;
	public HeaderAndFooter(String candidateName,Font f) {
		cdtName = candidateName;
		footerFont = f;
	}
	/**
	 * Initialize one of the headers.
	 */
	public void onOpenDocument(PdfWriter writer, Document document) {
		header[0] = new Phrase(" ",footerFont);
	}

	/**
	 * Initialize one of the headers, based on the chapter title;
	 * reset the page number.
	 */
	public void onChapter(PdfWriter writer, Document document,
			float paragraphPosition, Paragraph title) {
		header[1] = new Phrase(title.getContent(),footerFont);
		pagenumber = 1;
	}

	/**
	 * Increase the page number.
	 */
	public void onStartPage(PdfWriter writer, Document document) {
		pagenumber++;
	}

	/**
	 * Adds the header and the footer.
	 */
	public void onEndPage(PdfWriter writer, Document document) {
		Rectangle rect = writer.getBoxSize("art");
		switch(writer.getPageNumber() % 2) {
		case 0:
			ColumnText.showTextAligned(writer.getDirectContent(),
					Element.ALIGN_RIGHT, header[0],
					rect.getRight(), rect.getTop(), 0);
			break;
		case 1:
			ColumnText.showTextAligned(writer.getDirectContent(),
					Element.ALIGN_LEFT, header[1],
					rect.getLeft(), rect.getTop(), 0);
			break;
		}
		if(pagenumber!=1)
		{
			ColumnText.showTextAligned(writer.getDirectContent(),
					Element.ALIGN_LEFT, new Phrase(cdtName,footerFont),
					10, rect.getBottom() - 48, 0);
			ColumnText.showTextAligned(writer.getDirectContent(),
					Element.ALIGN_RIGHT, new Phrase(String.format("Page %d", pagenumber),footerFont),
					(rect.getLeft() + rect.getRight())-10, rect.getBottom() - 48, 0);
		}
	}
}
