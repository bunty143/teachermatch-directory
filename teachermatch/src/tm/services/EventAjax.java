package tm.services;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.util.UidGenerator;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.CGStatusEventTemp;
import tm.bean.CmsEventChannelMaster;
import tm.bean.CmsEventFormatMaster;
import tm.bean.CmsEventScheduleMaster;
import tm.bean.CmsEventTypeMaster;
import tm.bean.EmailMessageTemplatesForFacilitators;
import tm.bean.EventDescriptionTemplates;
import tm.bean.EventDetails;
import tm.bean.EventEmailMessageTemplates;
import tm.bean.EventFacilitatorsList;
import tm.bean.EventParticipantsList;
import tm.bean.EventSchedule;
import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4InterviewInvites;
import tm.bean.i4.I4QuestionPool;
import tm.bean.i4.I4QuestionSets;
import tm.bean.i4.VVIResponse;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.CGStatusEventTempDAO;
import tm.dao.CandidateEventDetailsDAO;
import tm.dao.CmsEventChannelMasterDAO;
import tm.dao.CmsEventFormatMasterDAO;
import tm.dao.CmsEventScheduleMasterDAO;
import tm.dao.CmsEventTypeMasterDao;
import tm.dao.DistrictTemplatesforMessagesDAO;
import tm.dao.EmailMessageTemplatesForFacilitatorsDAO;
import tm.dao.EventDescriptionTemplatesDAO;
import tm.dao.EventDetailsDAO;
import tm.dao.EventEmailMessageTemplatesDAO;
import tm.dao.EventFacilitatorsListDAO;
import tm.dao.EventParticipantsListDAO;
import tm.dao.EventScheduleDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.i4.I4QuestionPoolDAO;
import tm.dao.i4.I4QuestionSetsDAO;
import tm.dao.i4.VVIResponseDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.DistrictOrSchoolNameCompratorASC;
import tm.utility.DistrictOrSchoolNameCompratorDESC;
import tm.utility.EventEndDateCompratorASC;
import tm.utility.EventEndDateCompratorDESC;
import tm.utility.EventStartDateCompratorASC;
import tm.utility.EventStartDateCompratorDESC;
import tm.utility.EventTotalCandidateCompratorASC;
import tm.utility.EventTotalCandidateCompratorDESC;
import tm.utility.Utility;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class EventAjax {
	
	 String locale = Utility.getValueOfPropByKey("locale");
	 String lblEventName=Utility.getLocaleValuePropByKey("lblEventName", locale);
	 
	 String lblSchoolName=Utility.getLocaleValuePropByKey("lblSchoolName", locale);
	 String lblDistrictName=Utility.getLocaleValuePropByKey("lblDistrictName", locale);
	 
	 String lblEventType=Utility.getLocaleValuePropByKey("lblEventType", locale);
	 String lblSchedule=Utility.getLocaleValuePropByKey("lblSchedule", locale);
	 String lblFacilitators=Utility.getLocaleValuePropByKey("lblFacilitators", locale);
	 String lblParticipants=Utility.getLocaleValuePropByKey("lblParticipants", locale);
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String lblNoEventfound=Utility.getLocaleValuePropByKey("lblNoEventfound", locale);
	 String optCmplt=Utility.getLocaleValuePropByKey("optCmplt", locale);
	 String optIncomlete=Utility.getLocaleValuePropByKey("optIncomlete", locale);
	 String lblVid=Utility.getLocaleValuePropByKey("lblVid", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String btnP=Utility.getLocaleValuePropByKey("btnP", locale);
	 String btnClr=Utility.getLocaleValuePropByKey("btnClr", locale);
	 String headPrintPreview=Utility.getLocaleValuePropByKey("headPrintPreview", locale);
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lblAllEventType=Utility.getLocaleValuePropByKey("lblAllEventType", locale);
	 String lblSelectFormat=Utility.getLocaleValuePropByKey("lblSelectFormat", locale);
	 String lblSelectChannel=Utility.getLocaleValuePropByKey("lblSelectChannel", locale);
	 String lblSelectType=Utility.getLocaleValuePropByKey("lblSelectType", locale);
	 String sltTemplate=Utility.getLocaleValuePropByKey("sltTemplate", locale);
	 String msgToRegisterForThisEvent=Utility.getLocaleValuePropByKey("msgToRegisterForThisEvent", locale);
	 String msgToRegisterForThisEventl2=Utility.getLocaleValuePropByKey("msgToRegisterForThisEventl2", locale);
	 String lblSelectFacilitator=Utility.getLocaleValuePropByKey("lblSelectFacilitator", locale);
	 
	 
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private CGStatusEventTempDAO cGStatusEventTempDAO;
	
	@Autowired
	private CandidateEventDetailsDAO candidateEventDetailsDAO;
	
@Autowired
private DistrictMasterDAO districtMasterDAO;

@Autowired
EmailMessageTemplatesForFacilitatorsDAO emailMessageTemplatesForFacilitatorsDAO;

public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
{
this.districtMasterDAO = districtMasterDAO;
}
@Autowired
private CmsEventTypeMasterDao cmsEventTypeMasterDao;
public void setCmsEventTypeMasterDao(
			CmsEventTypeMasterDao cmsEventTypeMasterDao) {
		this.cmsEventTypeMasterDao = cmsEventTypeMasterDao;
	}
@Autowired
private CmsEventFormatMasterDAO cmsEventFormatMasterDAO;
public void setCmsEventFormatMasterDAO(
		CmsEventFormatMasterDAO cmsEventFormatMasterDAO) {
	this.cmsEventFormatMasterDAO = cmsEventFormatMasterDAO;
}
@Autowired
private CmsEventChannelMasterDAO cmsEventChannelMasterDAO; 
public void setCmsEventChannelMasterDAO(
		CmsEventChannelMasterDAO cmsEventChannelMasterDAO) {
	this.cmsEventChannelMasterDAO = cmsEventChannelMasterDAO;
}

@Autowired
public CmsEventScheduleMasterDAO cmsEventScheduleMasterDAO;
public void setCmsEventScheduleMasterDAO(
		CmsEventScheduleMasterDAO cmsEventScheduleMasterDAO) {
	this.cmsEventScheduleMasterDAO = cmsEventScheduleMasterDAO;
}
@Autowired
public EventDetailsDAO eventDetailsDAO; 
public void setEventDetailsDAO(EventDetailsDAO eventDetailsDAO) {
	this.eventDetailsDAO = eventDetailsDAO;
}
@Autowired
public EventScheduleDAO eventScheduleDAO;
public void setEventScheduleDAO(EventScheduleDAO eventScheduleDAO) {
	this.eventScheduleDAO = eventScheduleDAO;
}
@Autowired
public DistrictTemplatesforMessagesDAO districtTemplatesforMessagesDAO;
public void setDistrictTemplatesforMessagesDAO(
		DistrictTemplatesforMessagesDAO districtTemplatesforMessagesDAO) {
	this.districtTemplatesforMessagesDAO = districtTemplatesforMessagesDAO;
}
@Autowired
private EmailerService emailerService;
public void setEmailerService(EmailerService emailerService) 
{
	this.emailerService = emailerService;
}
@Autowired
private EventDescriptionTemplatesDAO eventDescriptionTemplatesDAO;
public void setEventDescriptionTemplatesDAO(
		EventDescriptionTemplatesDAO eventDescriptionTemplatesDAO) {
	this.eventDescriptionTemplatesDAO = eventDescriptionTemplatesDAO;
}
@Autowired
private  EventEmailMessageTemplatesDAO eventEmailMessageTemplatesDAO;
public void setEventEmailMessageTemplatesDAO(
		EventEmailMessageTemplatesDAO eventEmailMessageTemplatesDAO) {
	this.eventEmailMessageTemplatesDAO = eventEmailMessageTemplatesDAO;
}
@Autowired
private  EventFacilitatorsListDAO eventFacilitatorsListDAO;
public void setEventFacilitatorsListDAO(
		EventFacilitatorsListDAO eventFacilitatorsListDAO) {
	this.eventFacilitatorsListDAO = eventFacilitatorsListDAO;
}
@Autowired
private  EventParticipantsListDAO eventParticipantsListDAO;
public void setEventParticipantsListDAO(
		EventParticipantsListDAO eventParticipantsListDAO) {
	this.eventParticipantsListDAO = eventParticipantsListDAO;
}
@Autowired
private  DistrictSchoolsDAO districtSchoolsDAO;

@Autowired
private I4QuestionSetsDAO i4QuestionSetsDAO;

@Autowired
private VVIResponseDAO vviResponseDAO;

@Autowired
private I4QuestionPoolDAO i4QuestionPoolDAO;


public String displayEvents(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer districtId, String eventName,Integer eventTypeId,int headQuarterId,int branchId,int schoolId)	
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	StringBuffer dmRecords =	new StringBuffer();

	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		HeadQuarterMaster headQuarterMaster=null;
		BranchMaster branchMaster=null;
		if (session == null || session.getAttribute("userMaster") ==null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			
			if(schoolId!=0){
				schoolMaster=new SchoolMaster();
				schoolMaster.setSchoolId(Long.parseLong(schoolId+""));
			}

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getHeadQuarterMaster()!=null){
            	headQuarterMaster=userMaster.getHeadQuarterMaster();
            }
            if(userMaster.getBranchMaster()!=null){
            	branchMaster=userMaster.getBranchMaster();
            	headQuarterMaster=branchMaster.getHeadQuarterMaster();
            }
          //Only For Teachermatch Admin
			/*if(headQuarterId!=0){
				headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId,false,false);
			}*/
			if(branchId!=0){
				branchMaster=branchMasterDAO.findById(branchId,false,false);
			}
			
		}
		int sortingcheck=0;
		int noOfRowInPage	=	Integer.parseInt(noOfRow);
		int pgNo			= 	Integer.parseInt(pageNo);
		int start 			= 	((pgNo-1)*noOfRowInPage);
		int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord 	=	0;
		//------------------------------------
	
		String sortOrderFieldName	=	"createdDateTime";
		Order  sortOrderStrVal		=	null;
		
		if(sortOrder.equalsIgnoreCase("eventTypeId"))
			sortingcheck=1;
		
		
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
			}
		}

		String sortOrderTypeVal="0";
		
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderTypeVal="1";
			sortOrderStrVal=Order.desc(sortOrderFieldName);
		}

		if(sortOrderType.equals("")){
			sortOrderStrVal=Order.desc(sortOrderFieldName);
		}
		
		List<EventDetails> eventdetails	 =	new ArrayList<EventDetails>();
		CmsEventTypeMaster cmsEventTypeMaster =null;
		
		if(eventTypeId!=null && eventTypeId!=0)
			cmsEventTypeMaster=cmsEventTypeMasterDao.findById(eventTypeId, false,false);
		
		if(entityID==1){
		
			if(districtId!=null && districtId!=0)
				districtMaster=districtMasterDAO.findById(districtId, false,false);
			eventdetails=eventDetailsDAO.getEventDetailsByFilter(entityID,sortingcheck,sortOrderStrVal,0,0,districtMaster,eventName,cmsEventTypeMaster,null,null,null);
		}
		else if(entityID==2 || entityID==3)
		{
			
			List<UserMaster> listUserMasters=new  ArrayList<UserMaster>();
			
			if(schoolMaster!=null ){
				Criterion criterionSchool=Restrictions.eq("schoolId", schoolMaster);
				Criterion criterionEntityTYpe=Restrictions.eq("entityType",new Integer(3));
				listUserMasters=userMasterDAO.findByCriteria(criterionSchool,criterionEntityTYpe);
			}
			eventdetails=eventDetailsDAO.getEventDetailsByFilter(entityID,sortingcheck,sortOrderStrVal,0,0,districtMaster,eventName,cmsEventTypeMaster,null,null,listUserMasters);
		}
		else if(entityID==5 || entityID==6){
			eventdetails=eventDetailsDAO.getEventDetailsByFilter(entityID,sortingcheck,sortOrderStrVal,0,0,districtMaster,eventName,cmsEventTypeMaster,headQuarterMaster,branchMaster,null);
		}	
		
		
		Map<Integer, EventParticipantsList> eventDetailspar = new HashMap<Integer, EventParticipantsList>();
		Map<Integer, EventFacilitatorsList> eventDetailsfac = new HashMap<Integer, EventFacilitatorsList>();
		Map<Integer, EventSchedule> eventDetailssch = new HashMap<Integer, EventSchedule>();
	
		Map<Integer,Integer> mapCountEventIdBySlotSelction = eventDetailsDAO.countEventIdSlotSelection();
		
		Map<Integer,Integer> mapEventIdCandidateEventDetails = eventDetailsDAO.countEventIdCandidateEventDetails();
		
		Map<Integer,Date> mapReturnMaxEventStartDate = eventDetailsDAO.returnMaxEventEndDate();
		Map<Integer,Date> mapReturnMinEventStartDate = eventDetailsDAO.returnMinEventStartDate();
		
		
		if(eventdetails.size()>0){
			
			Criterion eventCri = Restrictions.in("eventDetails", eventdetails);
		    List<EventSchedule> eventSchList=eventScheduleDAO.findByCriteria(eventCri);
			List<EventFacilitatorsList> eventFacilitatorsList=eventFacilitatorsListDAO.findByCriteria(eventCri);
			List<EventParticipantsList> eventParticipantsList=eventParticipantsListDAO.findByCriteria(eventCri);
			
			for(EventSchedule eventschlist:eventSchList)
			{
				eventDetailssch.put(eventschlist.getEventDetails().getEventId(), eventschlist);	
			}	
			for(EventFacilitatorsList eventfachlist:eventFacilitatorsList)
			{
				eventDetailsfac.put(eventfachlist.getEventDetails().getEventId(), eventfachlist);	
			}	
			
			if(eventParticipantsList!=null && eventParticipantsList.size()>0){
				for(EventParticipantsList eventparlist:eventParticipantsList)
				{
					eventDetailspar.put(eventparlist.getEventDetails().getEventId(), eventparlist);	
				}	
			}
		}
		
		//Dhananjay Verma
		if(eventdetails!=null && eventdetails.size()>0 && sortOrder.equalsIgnoreCase("subjectforParticipants")){//subjectforParticipants noOfCandidate
			
			for(EventDetails eventdtllist :eventdetails){
				
				int total_scheduled_candidates=0;
				
				if(!mapCountEventIdBySlotSelction.isEmpty() || !mapEventIdCandidateEventDetails.isEmpty())
				{
					if(mapCountEventIdBySlotSelction.get(eventdtllist.getEventId())!=null)
					{
						total_scheduled_candidates=mapCountEventIdBySlotSelction.get(eventdtllist.getEventId());
						
					}else if(mapEventIdCandidateEventDetails.get(eventdtllist.getEventId())!=null)
					{
						
						total_scheduled_candidates=mapEventIdCandidateEventDetails.get(eventdtllist.getEventId());
					}
					
					else{
						total_scheduled_candidates=0;
					}
				}
				eventdtllist.setNoOfCandidate(total_scheduled_candidates);
			}	
			if(sortOrderType.equals("0"))
				 Collections.sort(eventdetails, new EventTotalCandidateCompratorASC());
			 else 
				 Collections.sort(eventdetails, new EventTotalCandidateCompratorDESC());
		}
		
		
	


//Dhananjay Verma Sorting by District Or School Name.
if(eventdetails!=null && eventdetails.size()>0 && sortOrder.equalsIgnoreCase("subjectforFacilator")){//subjectforParticipants District Or School Name
			
				
	for (EventDetails eventdtllist :eventdetails) {
	   
		if(entityID!=5 && entityID!=6){
				String districtOrSchoolName="";
				if(eventdtllist.getCreatedBY().getEntityType()==1){
					
					districtOrSchoolName=eventdtllist.getDistrictMaster().getDistrictName();
				}
				else {
					districtOrSchoolName=eventdtllist.getCreatedBY().getSchoolId()==null ? eventdtllist.getCreatedBY().getDistrictId().getDistrictName(): eventdtllist.getCreatedBY().getSchoolId().getSchoolName();
				}
			
				eventdtllist.setDistrictOrSchoolName(districtOrSchoolName);
		}
	}
			
			if(sortOrderType.equals("0"))
				 Collections.sort(eventdetails, new DistrictOrSchoolNameCompratorASC());
			 else 
				 Collections.sort(eventdetails, new DistrictOrSchoolNameCompratorDESC());
 }

//Dhananjay Verma Sorting by Date
if(eventdetails!=null && eventdetails.size()>0 && sortOrder.equalsIgnoreCase("msgtoparticipants")){//subjectforParticipants eventStartDate
			
				
				if(!mapReturnMaxEventStartDate.isEmpty())
				{
					for(EventDetails eventdtllist :eventdetails){
						
						Date maxEventDate = null;
						

						if(mapReturnMaxEventStartDate.get(eventdtllist.getEventId())!=null && mapReturnMinEventStartDate.get(eventdtllist.getEventId())!=null )
						{
							maxEventDate=mapReturnMaxEventStartDate.get(eventdtllist.getEventId());
							
						}
						
						else{
							maxEventDate=null;
							
						}
						eventdtllist.setEventStartDate(maxEventDate);
					}
				}
			
			
			if(sortOrderType.equals("0"))
				 Collections.sort(eventdetails, new EventStartDateCompratorASC());
			 else 
				 Collections.sort(eventdetails, new EventStartDateCompratorDESC());
 }

if(eventdetails!=null && eventdetails.size()>0 && sortOrder.equalsIgnoreCase("msgtofacilitators")){//subjectforParticipants eventStartDate
	
	
	if(!mapReturnMinEventStartDate.isEmpty())
	{
		for(EventDetails eventdtllist :eventdetails){
			
			Date minEventDate = null;
			

			if(mapReturnMinEventStartDate.get(eventdtllist.getEventId())!=null && mapReturnMinEventStartDate.get(eventdtllist.getEventId())!=null )
			{
				minEventDate=mapReturnMinEventStartDate.get(eventdtllist.getEventId());
				
			}
			
			else{
				minEventDate=null;
				
			}
			eventdtllist.setEventEndDate(minEventDate);
		}
	}


if(sortOrderType.equals("0"))
	 Collections.sort(eventdetails, new EventEndDateCompratorASC());
 else 
	 Collections.sort(eventdetails, new EventEndDateCompratorDESC());
}
		
		
		totalRecord =eventdetails.size();		
		if(totalRecord<end)
		end=totalRecord;
        List<EventDetails> lstsortedeventDetails =	eventdetails.subList(start,end);
		
        dmRecords.append("<table id='eventTable' border='0' class='table table-bordered table-striped' >");
		dmRecords.append("<thead class='bg'>");
		dmRecords.append("<tr>");
		String responseText="";
		
		responseText=PaginationAndSorting.responseSortingLink(lblEventName,sortOrderFieldName,"eventName",sortOrderTypeVal,pgNo);
		
		dmRecords.append("<th width='40%' valign='top'>"+responseText+"</th>");
		
		if(entityID!=5 && entityID!=6){
			
			responseText=PaginationAndSorting.responseSortingLink("District Or School Name",sortOrderFieldName,"subjectforFacilator",sortOrderTypeVal,pgNo);
			
			dmRecords.append("<th width='40%' valign='top'>"+responseText+"</th>");
		}
		
		responseText=PaginationAndSorting.responseSortingLink("# Candidate",sortOrderFieldName,"subjectforParticipants",sortOrderTypeVal,pgNo);
		
		dmRecords.append("<th width='40%' valign='top'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(lblEventType,sortOrderFieldName,"eventTypeId",sortOrderTypeVal,pgNo);
		dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
		
		
		responseText=PaginationAndSorting.responseSortingLink("Event Date Start",sortOrderFieldName,"msgtofacilitators",sortOrderTypeVal,pgNo);
		dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
		
		
		responseText=PaginationAndSorting.responseSortingLink("Event Date End",sortOrderFieldName,"msgtoparticipants",sortOrderTypeVal,pgNo);
		dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
	

		dmRecords.append("<th  valign='top'>"+lblSchedule+"</th>");
		dmRecords.append("<th  valign='top'>"+lblFacilitators+"</th>");
		dmRecords.append("<th  valign='top'>"+lblParticipants+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"eventURL",sortOrderTypeVal,pgNo);
		dmRecords.append("<th  valign='top'>"+responseText+"</th>");
		
		dmRecords.append("<th >"+lblAct+"</th>");
		dmRecords.append("</tr>");
		dmRecords.append("</thead>");
		/*================= Checking If Record Not Found ======================*/
		if(eventdetails.size()==0)
			dmRecords.append("<tr><td colspan='6'>"+lblNoEventfound+"</td></tr>" );
		
		// Get Event Participants List In Virtual Video Interview Case
		Map<Integer,EventParticipantsList> vviCompletedList = new HashMap<Integer,EventParticipantsList>();
		
		List<EventParticipantsList> participantsList = new ArrayList<EventParticipantsList>();
		
		if(lstsortedeventDetails!=null && lstsortedeventDetails.size()>0)
		   participantsList = eventParticipantsListDAO.findByEventList(lstsortedeventDetails);
		
		
		
		for(EventParticipantsList participantsObj:participantsList)
		{
			vviCompletedList.put(participantsObj.getEventDetails().getEventId(), participantsObj);
		}
		
		for (EventDetails eventdtllist :lstsortedeventDetails) {
		    dmRecords.append("<tr>");

			dmRecords.append("<td>"+eventdtllist.getEventName()+"</td>");
			
			if(entityID!=5 && entityID!=6){
					String districtOrSchoolName="";
					if(eventdtllist.getCreatedBY().getEntityType()==1){
						
						districtOrSchoolName=eventdtllist.getDistrictMaster().getDistrictName();
					}
					else {
						districtOrSchoolName=eventdtllist.getCreatedBY().getSchoolId()==null ? eventdtllist.getCreatedBY().getDistrictId().getDistrictName(): eventdtllist.getCreatedBY().getSchoolId().getSchoolName();
					}
				dmRecords.append("<td>"+districtOrSchoolName+"</td>");
				
				
			}
		 
			int total_scheduled_candidates=0;
			
			
			if(!mapCountEventIdBySlotSelction.isEmpty() || !mapEventIdCandidateEventDetails.isEmpty())
			{
				
				
				if(mapCountEventIdBySlotSelction.get(eventdtllist.getEventId())!=null)
				{
					total_scheduled_candidates=mapCountEventIdBySlotSelction.get(eventdtllist.getEventId());
					
				}else if(mapEventIdCandidateEventDetails.get(eventdtllist.getEventId())!=null)
				{
					
					total_scheduled_candidates=mapEventIdCandidateEventDetails.get(eventdtllist.getEventId());
				}
				
				else{
					total_scheduled_candidates=0;
				}
			}
			
			
			Date maxEventDate=null;
			Date minEventDate=null;
			
			if(!mapReturnMaxEventStartDate.isEmpty() && !mapReturnMinEventStartDate.isEmpty())
			{
				
				if(mapReturnMaxEventStartDate.get(eventdtllist.getEventId())!=null && mapReturnMinEventStartDate.get(eventdtllist.getEventId())!=null )
				{
					maxEventDate=mapReturnMaxEventStartDate.get(eventdtllist.getEventId());
					
					minEventDate=mapReturnMinEventStartDate.get(eventdtllist.getEventId());
				}
				
				else{
					maxEventDate=null;
					minEventDate=null;
				}
			}
						
			dmRecords.append("<td>"+total_scheduled_candidates+"</td>");
			
																															
			dmRecords.append("<td>"+eventdtllist.getEventTypeId().getEventTypeName()+"</td>");
			
			if(minEventDate == null && maxEventDate == null)
			{
				dmRecords.append("<td>N/A</td>");
				dmRecords.append("<td>N/A</td>");
			}
			else
			{
				dmRecords.append("<td>"+maxEventDate+"</td>");
				dmRecords.append("<td>"+minEventDate+"</td>");
			}
			
			
			
			boolean statusFlagssch=false;
			boolean statusFlagPart=false;
			boolean statusFlagFac=false;
			if(eventDetailssch.containsKey(eventdtllist.getEventId())){
				dmRecords.append("<td style='text-align:center'><a href='eventschedule.do?eventId="+eventdtllist.getEventId()+"'><span class='icon-calendar icon-large'></span></a></td>");
				statusFlagssch=true;
			}else{
				dmRecords.append("<td style='text-align:center'><a href='eventschedule.do?eventId="+eventdtllist.getEventId()+"'><span class='icon-calendar icon-large iconcolorhover'></span></a></td>");
			}
			
			if(eventdtllist.getEventTypeId().getEventTypeId()!=1){
			if(eventDetailsfac.containsKey(eventdtllist.getEventId())){
				dmRecords.append("<td style='text-align:center'><a href='managefacilitators.do?eventId="+eventdtllist.getEventId()+"'><span class='fa-pencil-square-o icon-large'></span></a></td>");
				statusFlagFac=true;
					}else{
				dmRecords.append("<td style='text-align:center'><a href='managefacilitators.do?eventId="+eventdtllist.getEventId()+"'><span class='fa-pencil-square-o icon-large iconcolorhover'></span></a></td>");
					
			}}else
			{
				dmRecords.append("<td></td>");	
			}	
			
			
			if(eventDetailspar.containsKey(eventdtllist.getEventId())){
				dmRecords.append("<td style='text-align:center'><a href='manageparticipant.do?eventId="+eventdtllist.getEventId()+"'><span class='fa-pencil-square-o icon-large'></span></a></td>");
				statusFlagPart=true;
					}else{
				dmRecords.append("<td style='text-align:center'><a href='manageparticipant.do?eventId="+eventdtllist.getEventId()+"'><span class='fa-pencil-square-o icon-large iconcolorhover'></span></a></td>");	
			}
			
			if(eventdtllist.getEventTypeId().getEventTypeId().equals(1))
			{
				if(statusFlagssch && statusFlagPart){
					dmRecords.append("<td>"+optCmplt+"</td>");
				}else{
					dmRecords.append("<td>"+optIncomlete+"</td>");
				}
			}else{
				if(statusFlagssch && statusFlagPart && statusFlagFac){
					dmRecords.append("<td>"+optCmplt+"</td>");
				}else{
					dmRecords.append("<td>"+optIncomlete+"</td>");
				}
			}
			
			
			boolean vviChk = false;
			
			if(eventdtllist.getEventTypeId().getEventTypeId().equals(1)){
				if(vviCompletedList.get(eventdtllist.getEventId())!=null){	if(vviCompletedList.get(eventdtllist.getEventId()).getI4InterviewInvites()!=null && !vviCompletedList.get(eventdtllist.getEventId()).getI4InterviewInvites().equals("")){
						if(vviCompletedList.get(eventdtllist.getEventId()).getI4InterviewInvites()!=null && !vviCompletedList.get(eventdtllist.getEventId()).getI4InterviewInvites().equals("")){
							List<VVIResponse> vviResponseList = vviResponseDAO.findByI4InviteId(vviCompletedList.get(eventdtllist.getEventId()).getI4InterviewInvites().getI4inviteid());
							if(vviResponseList!=null && vviResponseList.size()>0){
								if(vviCompletedList.get(eventdtllist.getEventId()).getI4InterviewInvites().getStatus().equals(3))
									vviChk = true; 
							}
						}
					}
				}
			}
			
			if(vviChk)
				dmRecords.append("<td><a href='javascript:void(0);' onclick='return showVirtualVideoInterview("+vviCompletedList.get(eventdtllist.getEventId()).getEventParticipantId()+")'>"+lblVid+"</a>");
			else
				dmRecords.append("<td><a data-toggle='tooltip' title='Edit' href='javascript:void(0);' onclick='return showEditEvent("+eventdtllist.getEventId()+")'><i class='fa fa-pencil-square-o icon-small'></i></a>");
			
			if(eventdtllist.getEventTypeId().getEventTypeId()==3 || eventdtllist.getEventTypeId().getEventTypeId()==4)
			 dmRecords.append("| <a data-toggle='tooltip' title='"+headPrintPreview+"' id='printId' href='javascript:void(0);' onclick='return printPreEventDetails("+eventdtllist.getEventId()+")'>print</a>");
				
				if(eventdtllist.getHeadQuarterMaster()==null)
				  dmRecords.append("| <a  data-toggle='tooltip' title='Cancel Event' href='javascript:void(0);' onclick='return showCancelEventPopup("+eventdtllist.getEventId()+")'><i class='fa fa-times icon-small'></i></a>");
			
			dmRecords.append("</td>");
			}
		dmRecords.append("</table>");
		dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));

	} catch (Exception e) {
		e.printStackTrace();
	}
	return dmRecords.toString();


}


public String displayRecordsByEntityTypeEXL(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer districtId, String eventName,Integer eventTypeId,int headQuarterId,int branchId,int schoolId)
{
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	String fileName = null;
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		HeadQuarterMaster headQuarterMaster=null;
		BranchMaster branchMaster=null;
		if (session == null || session.getAttribute("userMaster") ==null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			
			if(schoolId!=0){
				schoolMaster=new SchoolMaster();
				schoolMaster.setSchoolId(Long.parseLong(schoolId+""));
			}

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getHeadQuarterMaster()!=null){
            	headQuarterMaster=userMaster.getHeadQuarterMaster();
            }
            if(userMaster.getBranchMaster()!=null){
            	branchMaster=userMaster.getBranchMaster();
            	headQuarterMaster=branchMaster.getHeadQuarterMaster();
            }
         
			if(branchId!=0){
				branchMaster=branchMasterDAO.findById(branchId,false,false);
			}
			
		}
		int sortingcheck=0;
		String sortOrderFieldName	=	"createdDateTime";
		Order  sortOrderStrVal		=	null;
		
		if(sortOrder.equalsIgnoreCase("eventTypeId"))
			sortingcheck=1;
		
		
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
			}
		}

		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderStrVal=Order.desc(sortOrderFieldName);
		}

		if(sortOrderType.equals("")){
			sortOrderStrVal=Order.desc(sortOrderFieldName);
		}
		
		List<EventDetails> eventdetails	 =	new ArrayList<EventDetails>();
		
		List<EventDetails> finalJobOrder =new ArrayList<EventDetails>();	
		
		CmsEventTypeMaster cmsEventTypeMaster =null;
		
		if(eventTypeId!=null && eventTypeId!=0)
			cmsEventTypeMaster=cmsEventTypeMasterDao.findById(eventTypeId, false,false);
		
		if(entityID==1){
		
			if(districtId!=null && districtId!=0)
				districtMaster=districtMasterDAO.findById(districtId, false,false);
			eventdetails=eventDetailsDAO.getEventDetailsByFilter(entityID,sortingcheck,sortOrderStrVal,0,0,districtMaster,eventName,cmsEventTypeMaster,null,null,null);
		}
		else if(entityID==2 || entityID==3)
		{
			
			List<UserMaster> listUserMasters=new  ArrayList<UserMaster>();
			
			if(schoolMaster!=null ){
				Criterion criterionSchool=Restrictions.eq("schoolId", schoolMaster);
				Criterion criterionEntityTYpe=Restrictions.eq("entityType",new Integer(3));
				listUserMasters=userMasterDAO.findByCriteria(criterionSchool,criterionEntityTYpe);
			}
			eventdetails=eventDetailsDAO.getEventDetailsByFilter(entityID,sortingcheck,sortOrderStrVal,0,0,districtMaster,eventName,cmsEventTypeMaster,null,null,listUserMasters);
		}
		else if(entityID==5 || entityID==6){
			eventdetails=eventDetailsDAO.getEventDetailsByFilter(entityID,sortingcheck,sortOrderStrVal,0,0,districtMaster,eventName,cmsEventTypeMaster,headQuarterMaster,branchMaster,null);
		}	
		
		
		Map<Integer, EventParticipantsList> eventDetailspar = new HashMap<Integer, EventParticipantsList>();
		Map<Integer, EventFacilitatorsList> eventDetailsfac = new HashMap<Integer, EventFacilitatorsList>();
		Map<Integer, EventSchedule> eventDetailssch = new HashMap<Integer, EventSchedule>();
	
		Map<Integer,Integer> mapCountEventIdBySlotSelction = eventDetailsDAO.countEventIdSlotSelection();
		
		Map<Integer,Integer> mapEventIdCandidateEventDetails = eventDetailsDAO.countEventIdCandidateEventDetails();
		
		Map<Integer,Date> mapReturnMaxEventStartDate = eventDetailsDAO.returnMaxEventEndDate();
		Map<Integer,Date> mapReturnMinEventStartDate = eventDetailsDAO.returnMinEventStartDate();
		
		
		if(eventdetails.size()>0){
			
			Criterion eventCri = Restrictions.in("eventDetails", eventdetails);
		    List<EventSchedule> eventSchList=eventScheduleDAO.findByCriteria(eventCri);
			List<EventFacilitatorsList> eventFacilitatorsList=eventFacilitatorsListDAO.findByCriteria(eventCri);
			List<EventParticipantsList> eventParticipantsList=eventParticipantsListDAO.findByCriteria(eventCri);
			
			for(EventSchedule eventschlist:eventSchList)
			{
				eventDetailssch.put(eventschlist.getEventDetails().getEventId(), eventschlist);	
			}	
			for(EventFacilitatorsList eventfachlist:eventFacilitatorsList)
			{
				eventDetailsfac.put(eventfachlist.getEventDetails().getEventId(), eventfachlist);	
			}	
			
			if(eventParticipantsList!=null && eventParticipantsList.size()>0){
				for(EventParticipantsList eventparlist:eventParticipantsList)
				{
					eventDetailspar.put(eventparlist.getEventDetails().getEventId(), eventparlist);	
				}	
			}
		}
	
	
	     	finalJobOrder=eventdetails;
	     
		 		//  Excel   Exporting	
				String time = String.valueOf(System.currentTimeMillis()).substring(6);
				
				String basePath = request.getSession().getServletContext().getRealPath ("/")+"/manageevents";
				
				fileName ="manageevents"+time+".xls";

				
				File file = new File(basePath);
				if(!file.exists())
					file.mkdirs();

				Utility.deleteAllFileFromDir(basePath);

				file = new File(basePath+"/"+fileName);

				WorkbookSettings wbSettings = new WorkbookSettings();

				wbSettings.setLocale(new Locale("en", "EN"));

				WritableCellFormat timesBoldUnderline;
				WritableCellFormat header;
				WritableCellFormat headerBold;
				WritableCellFormat times;

				WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
				workbook.createSheet("manageevents", 0);
				WritableSheet excelSheet = workbook.getSheet(0);

				WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
				// Define the cell format
				times = new WritableCellFormat(times10pt);
				// Lets automatically wrap the cells
				times.setWrap(true);

				// Create create a bold font with unterlines
				WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
				WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

				timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
				timesBoldUnderline.setAlignment(Alignment.CENTRE);
				// Lets automatically wrap the cells
				timesBoldUnderline.setWrap(true);

				header = new WritableCellFormat(times10ptBoldUnderline);
				headerBold = new WritableCellFormat(times10ptBoldUnderline);
				CellView cv = new CellView();
				cv.setFormat(times);
				cv.setFormat(timesBoldUnderline);
				cv.setAutosize(true);

				header.setBackground(Colour.GRAY_25);

				// Write a few headers
				excelSheet.mergeCells(0, 0, 6, 1);
				Label label;
				label = new Label(0, 0, Utility.getLocaleValuePropByKey("headMsgManageEvents", locale), timesBoldUnderline);
				excelSheet.addCell(label);
				excelSheet.mergeCells(0, 3, 6, 3);
				label = new Label(0, 3, "");
				excelSheet.addCell(label);
				excelSheet.getSettings().setDefaultColumnWidth(18);
				
				int k=4;
				int col=1;
				label = new Label(0, k, lblEventName,header); 
				excelSheet.addCell(label);
				
				label = new Label(1, k, lblDistrictName,header); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, "# Candidate",header); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, lblEventType,header); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, "Event Date Start",header); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, "Event Date End",header); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, lblStatus,header); 
				excelSheet.addCell(label);
									
				k=k+1;
				if(finalJobOrder.size()==0)
				{	
					excelSheet.mergeCells(0, k, 6, k);
					label = new Label(0, k, lblNoEventfound); 
					excelSheet.addCell(label);
				}
				
				
				HashMap<String, WritableCellFormat> mapColors= new HashMap<String, WritableCellFormat>();
				
				
				if (finalJobOrder.size()>0) {
					WritableCellFormat cellFormat = new WritableCellFormat();
					Color color = Color.decode("0xFF0000");
					
					int red = color.getRed();
					int blue = color.getBlue();
					int green = color.getGreen();
					
					workbook.setColourRGB(Colour.AQUA, red, green, blue);
					cellFormat.setBackground(Colour.AQUA);
					
					mapColors.put("FF0000", cellFormat);
					
					WritableCellFormat cellFormat1 = new WritableCellFormat();
					Color color1 = Color.decode("0xFF6666");
					
					workbook.setColourRGB(Colour.BLUE, color1.getRed(), color1.getGreen(), color1.getBlue());
					cellFormat1.setBackground(Colour.BLUE);
					
					mapColors.put("FF6666", cellFormat1);
					WritableCellFormat cellFormat2 = new WritableCellFormat();
					
					Color color2 = Color.decode("0xFFCCCC");
					
					workbook.setColourRGB(Colour.BROWN, color2.getRed(), color2.getGreen(), color2.getBlue());
					cellFormat2.setBackground(Colour.BROWN);
					
					mapColors.put("FFCCCC", cellFormat2);
					WritableCellFormat cellFormat3 = new WritableCellFormat();
					
					Color color3 = Color.decode("0xFF9933");
					workbook.setColourRGB(Colour.CORAL, color3.getRed(), color3.getGreen(), color3.getBlue());
					
					cellFormat3.setBackground(Colour.CORAL);
					mapColors.put("FF9933", cellFormat3);
					
					WritableCellFormat cellFormat4 = new WritableCellFormat();
					Color color4 = Color.decode("0xFFFFCC");
					
					workbook.setColourRGB(Colour.GREEN, color4.getRed(), color4.getGreen(), color4.getBlue());
					cellFormat4.setBackground(Colour.GREEN);
					
					mapColors.put("FFFFCC", cellFormat4);
					
					WritableCellFormat cellFormat5 = new WritableCellFormat();
					
					Color color5 = Color.decode("0xFFFF00");
					
					workbook.setColourRGB(Colour.INDIGO, color5.getRed(), color5.getGreen(), color5.getBlue());
					cellFormat5.setBackground(Colour.INDIGO);
					
					mapColors.put("FFFF00", cellFormat5);
					WritableCellFormat cellFormat6 = new WritableCellFormat();
					
					Color color6 = Color.decode("0x66CC66");
					workbook.setColourRGB(Colour.LAVENDER, color6.getRed(), color6.getGreen(), color6.getBlue());
					
					cellFormat6.setBackground(Colour.LAVENDER);
					mapColors.put("66CC66", cellFormat6);
					
				}
				
				
				  if(finalJobOrder.size()>0){	
						 for(EventDetails eventdeatilsexl: finalJobOrder)
						  {	
							 col=1;
			
					label = new Label(0, k, eventdeatilsexl.getEventName()); 
					excelSheet.addCell(label);
			
					
					if(entityID!=5 && entityID!=6){
						String districtOrSchoolName="";
						if(eventdeatilsexl.getCreatedBY().getEntityType()==1){
							
							districtOrSchoolName=eventdeatilsexl.getDistrictMaster().getDistrictName();
						}
						else {
							districtOrSchoolName=eventdeatilsexl.getCreatedBY().getSchoolId()==null ? eventdeatilsexl.getCreatedBY().getDistrictId().getDistrictName(): eventdeatilsexl.getCreatedBY().getSchoolId().getSchoolName();
						}
						
						label = new Label(1, k, districtOrSchoolName); 
						excelSheet.addCell(label);		

					}
					
					
					int total_scheduled_candidates=0;
					Date maxEventDate=null;
					Date minEventDate=null;
					
					if(!mapCountEventIdBySlotSelction.isEmpty() || !mapEventIdCandidateEventDetails.isEmpty())
					{
						
						
						if(mapCountEventIdBySlotSelction.get(eventdeatilsexl.getEventId())!=null)
						{
							total_scheduled_candidates=mapCountEventIdBySlotSelction.get(eventdeatilsexl.getEventId());
							
						}else if(mapEventIdCandidateEventDetails.get(eventdeatilsexl.getEventId())!=null)
						{
							
							total_scheduled_candidates=mapEventIdCandidateEventDetails.get(eventdeatilsexl.getEventId());
						}
						
						else{
							total_scheduled_candidates=0;
						}
					}
					
					
					if(!mapReturnMaxEventStartDate.isEmpty() && !mapReturnMinEventStartDate.isEmpty())
					{
						
						if(mapReturnMaxEventStartDate.get(eventdeatilsexl.getEventId())!=null && mapReturnMinEventStartDate.get(eventdeatilsexl.getEventId())!=null )
						{
							maxEventDate=mapReturnMaxEventStartDate.get(eventdeatilsexl.getEventId());
							
							minEventDate=mapReturnMinEventStartDate.get(eventdeatilsexl.getEventId());
						}
						
						else{
							maxEventDate=null;
							minEventDate=null;
						}
					}
								

					
					 
				
				label = new Label(2, k, total_scheduled_candidates+""); 
				excelSheet.addCell(label);
				
				
				
				label = new Label(3, k, eventdeatilsexl.getEventTypeId().getEventTypeName()); 
				excelSheet.addCell(label);
				
				
				if(minEventDate == null && maxEventDate == null)
				{
					
					label = new Label(4, k, "N/A"); 
					excelSheet.addCell(label);
					
					label = new Label(5, k, "N/A"); 
					excelSheet.addCell(label);
					
				}
				else
				{
					
					label = new Label(4, k, maxEventDate+""); 
					excelSheet.addCell(label);
					
					
					label = new Label(5, k, minEventDate+""); 
					excelSheet.addCell(label);
					
				}

				boolean statusFlagssch=false;
				boolean statusFlagPart=false;
				boolean statusFlagFac=false;
				
				
				if(eventDetailssch.containsKey(eventdeatilsexl.getEventId())){
					
					statusFlagssch=true;
				}else{
					
				}
				
						
				if(eventdeatilsexl.getEventTypeId().getEventTypeId()!=1){
					
				if(eventDetailsfac.containsKey(eventdeatilsexl.getEventId())){
					
					
					statusFlagFac=true;
				}
				else{
					
						
				}
				}else
				{
						
				}	
				
				if(eventDetailspar.containsKey(eventdeatilsexl.getEventId())){
					
					statusFlagPart=true;
						}else{
						
				}

				if(eventdeatilsexl.getEventTypeId().getEventTypeId().equals(1))
				{
					if(statusFlagssch && statusFlagPart){
						
						label = new Label(6, k, optCmplt); 
						excelSheet.addCell(label);
						
						
					}else{
						label = new Label(6, k, optIncomlete); 
						excelSheet.addCell(label);
					}
				}else{
					if(statusFlagssch && statusFlagPart && statusFlagFac){
						label = new Label(6, k, optCmplt); 
						excelSheet.addCell(label);
					}else{
						label = new Label(6, k, optIncomlete); 
						excelSheet.addCell(label);
					}
				}
			
												
			   k++; 
		 }
		}

		workbook.write();
		workbook.close();
	}catch(Exception e){
		
		e.printStackTrace();
		
	}
	
 return fileName;
}

public String displayRecordsByEntityTypePDF(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer districtId, String eventName,Integer eventTypeId,int headQuarterId,int branchId,int schoolId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	try
	{	
		String fontPath = request.getRealPath("/");
		BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else
			userMaster = (UserMaster)session.getAttribute("userMaster");

		int userId = userMaster.getUserId();

		
		String time = String.valueOf(System.currentTimeMillis()).substring(6);
		String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
		String fileName =time+"Report.pdf";

		File file = new File(basePath);
		if(!file.exists())
			file.mkdirs();

		Utility.deleteAllFileFromDir(basePath);
		
		generateCandidatePDReport( noOfRow,  pageNo, sortOrder, sortOrderType, districtId,  eventName, eventTypeId, headQuarterId, branchId, schoolId,basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
		
		return "user/"+userId+"/"+fileName;
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return "ABV";	
}
public boolean generateCandidatePDReport(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer districtId, String eventName,Integer eventTypeId,int headQuarterId,int branchId,int schoolId, String path,String realPath)
{
	Document document=null;
	FileOutputStream fos = null;
	PdfWriter writer = null;
	Font font8Green = null;
		Font font8bold = null;
		Font font10 = null;
		Font font10_10 = null;
		Font font20bold = null;
		Color bluecolor =null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		try{
			
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			HeadQuarterMaster headQuarterMaster=null;
			BranchMaster branchMaster=null;
			if (session == null || session.getAttribute("userMaster") ==null) {
			
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
				
			}else{
				
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				
				if(schoolId!=0){
					schoolMaster=new SchoolMaster();
					schoolMaster.setSchoolId(Long.parseLong(schoolId+""));
				}

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
				if(userMaster.getHeadQuarterMaster()!=null){
	            	headQuarterMaster=userMaster.getHeadQuarterMaster();
	            }
	            if(userMaster.getBranchMaster()!=null){
	            	branchMaster=userMaster.getBranchMaster();
	            	headQuarterMaster=branchMaster.getHeadQuarterMaster();
	            }
	         
				if(branchId!=0){
					branchMaster=branchMasterDAO.findById(branchId,false,false);
				}
				
			}
			int sortingcheck=0;
			String sortOrderFieldName	=	"createdDateTime";
			Order  sortOrderStrVal		=	null;
			
			if(sortOrder.equalsIgnoreCase("eventTypeId"))
				sortingcheck=1;
			
			
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
				}
			}

			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}

			if(sortOrderType.equals("")){
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
			
			
			
			List<EventDetails> eventdetails	 =	new ArrayList<EventDetails>();
			
			List<EventDetails> finalJobOrder =new ArrayList<EventDetails>();	
			
			CmsEventTypeMaster cmsEventTypeMaster =null;
			
			if(eventTypeId!=null && eventTypeId!=0)
				cmsEventTypeMaster=cmsEventTypeMasterDao.findById(eventTypeId, false,false);
			
			if(entityID==1){
			
				if(districtId!=null && districtId!=0)
					districtMaster=districtMasterDAO.findById(districtId, false,false);
				eventdetails=eventDetailsDAO.getEventDetailsByFilter(entityID,sortingcheck,sortOrderStrVal,0,0,districtMaster,eventName,cmsEventTypeMaster,null,null,null);
			}
			else if(entityID==2 || entityID==3)
			{
				
				List<UserMaster> listUserMasters=new  ArrayList<UserMaster>();
				
				if(schoolMaster!=null ){
					Criterion criterionSchool=Restrictions.eq("schoolId", schoolMaster);
					Criterion criterionEntityTYpe=Restrictions.eq("entityType",new Integer(3));
					listUserMasters=userMasterDAO.findByCriteria(criterionSchool,criterionEntityTYpe);
				}
				eventdetails=eventDetailsDAO.getEventDetailsByFilter(entityID,sortingcheck,sortOrderStrVal,0,0,districtMaster,eventName,cmsEventTypeMaster,null,null,listUserMasters);
			}
			else if(entityID==5 || entityID==6){
				eventdetails=eventDetailsDAO.getEventDetailsByFilter(entityID,sortingcheck,sortOrderStrVal,0,0,districtMaster,eventName,cmsEventTypeMaster,headQuarterMaster,branchMaster,null);
			}	
			
			
			Map<Integer, EventParticipantsList> eventDetailspar = new HashMap<Integer, EventParticipantsList>();
			Map<Integer, EventFacilitatorsList> eventDetailsfac = new HashMap<Integer, EventFacilitatorsList>();
			Map<Integer, EventSchedule> eventDetailssch = new HashMap<Integer, EventSchedule>();
		
			Map<Integer,Integer> mapCountEventIdBySlotSelction = eventDetailsDAO.countEventIdSlotSelection();
			
			Map<Integer,Integer> mapEventIdCandidateEventDetails = eventDetailsDAO.countEventIdCandidateEventDetails();
			
			Map<Integer,Date> mapReturnMaxEventStartDate = eventDetailsDAO.returnMaxEventEndDate();
			Map<Integer,Date> mapReturnMinEventStartDate = eventDetailsDAO.returnMinEventStartDate();
			
			
			if(eventdetails.size()>0){
				
				Criterion eventCri = Restrictions.in("eventDetails", eventdetails);
			    List<EventSchedule> eventSchList=eventScheduleDAO.findByCriteria(eventCri);
				List<EventFacilitatorsList> eventFacilitatorsList=eventFacilitatorsListDAO.findByCriteria(eventCri);
				List<EventParticipantsList> eventParticipantsList=eventParticipantsListDAO.findByCriteria(eventCri);
				
				for(EventSchedule eventschlist:eventSchList)
				{
					eventDetailssch.put(eventschlist.getEventDetails().getEventId(), eventschlist);	
				}	
				for(EventFacilitatorsList eventfachlist:eventFacilitatorsList)
				{
					eventDetailsfac.put(eventfachlist.getEventDetails().getEventId(), eventfachlist);	
				}	
				
				if(eventParticipantsList!=null && eventParticipantsList.size()>0){
					for(EventParticipantsList eventparlist:eventParticipantsList)
					{
						eventDetailspar.put(eventparlist.getEventDetails().getEventId(), eventparlist);	
					}	
				}
			}
		
			   
			finalJobOrder=eventdetails;
			     
		 
			 String fontPath = realPath;
			     try {
						
						BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
						BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
						font8Green = new Font(tahoma, 8);
						font8Green.setColor(Color.BLUE);
						font8bold = new Font(tahoma, 8, Font.NORMAL);


						font10 = new Font(tahoma, 10);
						
						font10_10 = new Font(tahoma, 10);
						font10_10.setColor(Color.white);
						bluecolor =  new Color(0,122,180); 
						
						font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);


					} 
					catch (DocumentException e1) 
					{
						e1.printStackTrace();
					} 
					catch (IOException e1) 
					{
						e1.printStackTrace();
					}
					
					document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
					document.addAuthor("TeacherMatch");
					document.addCreator("TeacherMatch Inc.");
					document.addSubject(Utility.getLocaleValuePropByKey("headMsgManageEvents", locale));
					document.addCreationDate();
					document.addTitle(Utility.getLocaleValuePropByKey("headMsgManageEvents", locale));

					fos = new FileOutputStream(path);
					PdfWriter.getInstance(document, fos);
				
					document.open();
					
					PdfPTable mainTable = new PdfPTable(1);
					mainTable.setWidthPercentage(90);

					Paragraph [] para = null;
					PdfPCell [] cell = null;
					
					para = new Paragraph[3];
					cell = new PdfPCell[3];
					
					para[0] = new Paragraph(" ",font20bold);
					cell[0]= new PdfPCell(para[0]);
					cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell[0].setBorder(0);
					mainTable.addCell(cell[0]);
					
					
					Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
					logo.scalePercent(75);

					
					cell[1]= new PdfPCell(logo);
					cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[1].setBorder(0);
					mainTable.addCell(cell[1]);

					document.add(new Phrase("\n"));
					
					
					para[2] = new Paragraph("",font20bold);
					cell[2]= new PdfPCell(para[2]);
					cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[2].setBorder(0);
					mainTable.addCell(cell[2]);

					document.add(mainTable);
					
					document.add(new Phrase("\n"));

					float[] tblwidthz={.15f};
					
					mainTable = new PdfPTable(tblwidthz);
					mainTable.setWidthPercentage(100);
					para = new Paragraph[1];
					cell = new PdfPCell[1];

					
					para[0] = new Paragraph(Utility.getLocaleValuePropByKey("headMsgManageEvents", locale),font20bold);
					cell[0]= new PdfPCell(para[0]);
					cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[0].setBorder(0);
					mainTable.addCell(cell[0]);
					document.add(mainTable);

					document.add(new Phrase("\n"));
					
			        float[] tblwidths={.15f,.20f};
					
					mainTable = new PdfPTable(tblwidths);
					
					mainTable.setWidthPercentage(100);
					para = new Paragraph[2];
					cell = new PdfPCell[2];

					String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
					
					para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
					cell[0]= new PdfPCell(para[0]);
					cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
					cell[0].setBorder(0);
					mainTable.addCell(cell[0]);
					
					para[1] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
					cell[1]= new PdfPCell(para[1]);
					cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell[1].setBorder(0);
					mainTable.addCell(cell[1]);
					
					document.add(mainTable);
					
					document.add(new Phrase("\n"));


					float[] tblwidth={.16f,.15f,.15f,.08f,.10f,.10f,.10f};
					
					mainTable = new PdfPTable(tblwidth);
					mainTable.setWidthPercentage(100);
					para = new Paragraph[7];
					cell = new PdfPCell[7];
					
					
					
			// header
					para[0] = new Paragraph(lblEventName,font10_10);
					cell[0]= new PdfPCell(para[0]);
					cell[0].setBackgroundColor(bluecolor);
					cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[0]);
					
					para[1] = new Paragraph(lblDistrictName,font10_10);
					cell[1]= new PdfPCell(para[1]);
					cell[1].setBackgroundColor(bluecolor);
					cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[1]);
					
					
					para[2] = new Paragraph("# Candidate",font10_10);
					cell[2]= new PdfPCell(para[2]);
					cell[2].setBackgroundColor(bluecolor);
					cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[2]);

					para[3] = new Paragraph(lblEventType,font10_10);
					cell[3]= new PdfPCell(para[3]);
					cell[3].setBackgroundColor(bluecolor);
					cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[3]);
					
					para[4] = new Paragraph("Event Date Start",font10_10);
					cell[4]= new PdfPCell(para[4]);
					cell[4].setBackgroundColor(bluecolor);
					cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[4]);
					
					para[5] = new Paragraph("Event Date End",font10_10);
					cell[5]= new PdfPCell(para[5]);
					cell[5].setBackgroundColor(bluecolor);
					cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[5]);
					
					para[6] = new Paragraph(lblStatus,font10_10);
					cell[6]= new PdfPCell(para[6]);
					cell[6].setBackgroundColor(bluecolor);
					cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[6]);
																	
					document.add(mainTable);
					
					if(finalJobOrder.size()==0){
						    float[] tblwidth11={.10f};
							
							 mainTable = new PdfPTable(tblwidth11);
							 mainTable.setWidthPercentage(100);
							 para = new Paragraph[1];
							 cell = new PdfPCell[1];
						
							 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgNorecordfound", locale),font8bold);
							 cell[0]= new PdfPCell(para[0]);
							 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[0]);
						
						document.add(mainTable);
					}
			     
					
					  if(finalJobOrder.size()>0){	
							 for(EventDetails eventdetailspdf: finalJobOrder)
							  {
						  int index=0;
						  float[] tblwidth1={.16f,.15f,.15f,.08f,.10f,.10f,.10f};							
						  mainTable = new PdfPTable(tblwidth1);
						  mainTable.setWidthPercentage(100);
						  para = new Paragraph[7];
						  cell = new PdfPCell[7];
						  
						  
                             para[index] = new Paragraph(eventdetailspdf.getEventName(),font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
							 
							 
							 if(entityID!=5 && entityID!=6){
									String districtOrSchoolName="";
									if(eventdetailspdf.getCreatedBY().getEntityType()==1){
										
										districtOrSchoolName=eventdetailspdf.getDistrictMaster().getDistrictName();
									}
									else {
										districtOrSchoolName=eventdetailspdf.getCreatedBY().getSchoolId()==null ? eventdetailspdf.getCreatedBY().getDistrictId().getDistrictName(): eventdetailspdf.getCreatedBY().getSchoolId().getSchoolName();
									}
									
									 para[index] = new Paragraph( districtOrSchoolName,font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++;

								}

								int total_scheduled_candidates=0;
								Date maxEventDate=null;
								Date minEventDate=null;
								
								if(!mapCountEventIdBySlotSelction.isEmpty() || !mapEventIdCandidateEventDetails.isEmpty())
								{
									
									
									if(mapCountEventIdBySlotSelction.get(eventdetailspdf.getEventId())!=null)
									{
										total_scheduled_candidates=mapCountEventIdBySlotSelction.get(eventdetailspdf.getEventId());
										
									}else if(mapEventIdCandidateEventDetails.get(eventdetailspdf.getEventId())!=null)
									{
										
										total_scheduled_candidates=mapEventIdCandidateEventDetails.get(eventdetailspdf.getEventId());
									}
									
									else{
										total_scheduled_candidates=0;
									}
								}
								
								
								if(!mapReturnMaxEventStartDate.isEmpty() && !mapReturnMinEventStartDate.isEmpty())
								{
									
									if(mapReturnMaxEventStartDate.get(eventdetailspdf.getEventId())!=null && mapReturnMinEventStartDate.get(eventdetailspdf.getEventId())!=null )
									{
										maxEventDate=mapReturnMaxEventStartDate.get(eventdetailspdf.getEventId());
										
										minEventDate=mapReturnMinEventStartDate.get(eventdetailspdf.getEventId());
									}
									
									else{
										maxEventDate=null;
										minEventDate=null;
									}
								}
											

								 para[index] = new Paragraph(total_scheduled_candidates+"",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
							
								 para[index] = new Paragraph(eventdetailspdf.getEventTypeId().getEventTypeName(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							
							
							if(minEventDate == null && maxEventDate == null)
							{
								
								 para[index] = new Paragraph("N/A",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								
								 para[index] = new Paragraph("N/A",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							}
							else
							{
								
								 para[index] = new Paragraph(maxEventDate+"",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								
								 para[index] = new Paragraph(minEventDate+"",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								
							
							}
							
							boolean statusFlagssch=false;
							boolean statusFlagPart=false;
							boolean statusFlagFac=false;
							
									
							if(eventDetailssch.containsKey(eventdetailspdf.getEventId())){
								
								statusFlagssch=true;
							}else{
								
							}
							
							if(eventdetailspdf.getEventTypeId().getEventTypeId()!=1){
								
							if(eventDetailsfac.containsKey(eventdetailspdf.getEventId())){
								
								
								statusFlagFac=true;
							}
							else{
								
									
							}
							}else
							{
									
							}	
							
							if(eventDetailspar.containsKey(eventdetailspdf.getEventId())){
								
								statusFlagPart=true;
									}else{
									
							}

							if(eventdetailspdf.getEventTypeId().getEventTypeId().equals(1))
							{
								if(statusFlagssch && statusFlagPart){
									
									
									 para[index] = new Paragraph(optCmplt,font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++;
									
									
								}else{
									
									
									 para[index] = new Paragraph(optIncomlete,font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++;
									
								}
							}else{
								if(statusFlagssch && statusFlagPart && statusFlagFac){
									
									
									 para[index] = new Paragraph(optCmplt,font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++;
									
								}else{
									
									 para[index] = new Paragraph(optIncomlete,font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++;
								}
							}
							 								 
							 document.add(mainTable);
			   }
			}
		}catch(Exception e){e.printStackTrace();}
		finally
		{
			if(document != null && document.isOpen())
				document.close();
			if(writer!=null){
				writer.flush();
				writer.close();
			}
		}
		
		return true;
}


public String displayRecordsByEntityTypePrintPreview(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer districtId, String eventName,Integer eventTypeId,int headQuarterId,int branchId,int schoolId)
{
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	StringBuffer tmRecords =	new StringBuffer();
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		HeadQuarterMaster headQuarterMaster=null;
		BranchMaster branchMaster=null;
		if (session == null || session.getAttribute("userMaster") ==null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			
			if(schoolId!=0){
				schoolMaster=new SchoolMaster();
				schoolMaster.setSchoolId(Long.parseLong(schoolId+""));
			}

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getHeadQuarterMaster()!=null){
            	headQuarterMaster=userMaster.getHeadQuarterMaster();
            }
            if(userMaster.getBranchMaster()!=null){
            	branchMaster=userMaster.getBranchMaster();
            	headQuarterMaster=branchMaster.getHeadQuarterMaster();
            }
         
			if(branchId!=0){
				branchMaster=branchMasterDAO.findById(branchId,false,false);
			}
			
		}
	
		int sortingcheck=1;
		
		String sortOrderFieldName	=	"createdDateTime";
		Order  sortOrderStrVal		=	null;
		
		if(sortOrder.equalsIgnoreCase("eventTypeId"))
			sortingcheck=1;
		
		
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
			}
		}

		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderStrVal=Order.desc(sortOrderFieldName);
		}

		if(sortOrderType.equals("")){
			sortOrderStrVal=Order.desc(sortOrderFieldName);
		}
		
		List<EventDetails> eventdetails	 =	new ArrayList<EventDetails>();
		
		List<EventDetails> finalJobOrder =new ArrayList<EventDetails>();	
		
		CmsEventTypeMaster cmsEventTypeMaster =null;
		
		if(eventTypeId!=null && eventTypeId!=0)
			cmsEventTypeMaster=cmsEventTypeMasterDao.findById(eventTypeId, false,false);
		
		if(entityID==1){
		
			if(districtId!=null && districtId!=0)
				districtMaster=districtMasterDAO.findById(districtId, false,false);
			eventdetails=eventDetailsDAO.getEventDetailsByFilter(entityID,sortingcheck,sortOrderStrVal,0,0,districtMaster,eventName,cmsEventTypeMaster,null,null,null);
		}
		else if(entityID==2 || entityID==3)
		{
			
			List<UserMaster> listUserMasters=new  ArrayList<UserMaster>();
			
			if(schoolMaster!=null ){
				Criterion criterionSchool=Restrictions.eq("schoolId", schoolMaster);
				Criterion criterionEntityTYpe=Restrictions.eq("entityType",new Integer(3));
				listUserMasters=userMasterDAO.findByCriteria(criterionSchool,criterionEntityTYpe);
			}
			eventdetails=eventDetailsDAO.getEventDetailsByFilter(entityID,sortingcheck,sortOrderStrVal,0,0,districtMaster,eventName,cmsEventTypeMaster,null,null,listUserMasters);
		}
		else if(entityID==5 || entityID==6){
			eventdetails=eventDetailsDAO.getEventDetailsByFilter(entityID,sortingcheck,sortOrderStrVal,0,0,districtMaster,eventName,cmsEventTypeMaster,headQuarterMaster,branchMaster,null);
		}	
		
		
		Map<Integer, EventParticipantsList> eventDetailspar = new HashMap<Integer, EventParticipantsList>();
		Map<Integer, EventFacilitatorsList> eventDetailsfac = new HashMap<Integer, EventFacilitatorsList>();
		Map<Integer, EventSchedule> eventDetailssch = new HashMap<Integer, EventSchedule>();
	
		Map<Integer,Integer> mapCountEventIdBySlotSelction = eventDetailsDAO.countEventIdSlotSelection();
		
		Map<Integer,Integer> mapEventIdCandidateEventDetails = eventDetailsDAO.countEventIdCandidateEventDetails();
		
		Map<Integer,Date> mapReturnMaxEventStartDate = eventDetailsDAO.returnMaxEventEndDate();
		Map<Integer,Date> mapReturnMinEventStartDate = eventDetailsDAO.returnMinEventStartDate();
		
		
		if(eventdetails.size()>0){
			
			Criterion eventCri = Restrictions.in("eventDetails", eventdetails);
		    List<EventSchedule> eventSchList=eventScheduleDAO.findByCriteria(eventCri);
			List<EventFacilitatorsList> eventFacilitatorsList=eventFacilitatorsListDAO.findByCriteria(eventCri);
			List<EventParticipantsList> eventParticipantsList=eventParticipantsListDAO.findByCriteria(eventCri);
			
			for(EventSchedule eventschlist:eventSchList)
			{
				eventDetailssch.put(eventschlist.getEventDetails().getEventId(), eventschlist);	
			}	
			for(EventFacilitatorsList eventfachlist:eventFacilitatorsList)
			{
				eventDetailsfac.put(eventfachlist.getEventDetails().getEventId(), eventfachlist);	
			}	
			
			if(eventParticipantsList!=null && eventParticipantsList.size()>0){
				for(EventParticipantsList eventparlist:eventParticipantsList)
				{
					eventDetailspar.put(eventparlist.getEventDetails().getEventId(), eventparlist);	
				}	
			}
		}
	
	
	     	finalJobOrder=eventdetails;
	     
	     	
	     	tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>Manage Events</div><br/>");
			
			tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");
			
			
			tmRecords.append("<table  id='eventTablePreview' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");		
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+lblEventName+"</th>");			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+lblDistrictName+"</th>");    
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'># Candidate</th>");			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+lblEventType+"</th>");			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Event Date Start</th>");			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Event Date End</th>");	
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+lblStatus+"</th>");
				
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(finalJobOrder.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+lblNoEventfound+"</td></tr>" );
			}		
			
			
			if(finalJobOrder.size()>0){	
				
				 for(EventDetails evnetsdetailsgrid: finalJobOrder)
				  {	
					 tmRecords.append("<tr>");	
					 tmRecords.append("<td style='font-size:12px;'>"+ evnetsdetailsgrid.getEventName()+"</td>");
					
					 if(entityID!=5 && entityID!=6){
							String districtOrSchoolName="";
							
							if(evnetsdetailsgrid.getCreatedBY().getEntityType()==1){
								
								districtOrSchoolName=evnetsdetailsgrid.getDistrictMaster().getDistrictName();
							}
							else {
								districtOrSchoolName=evnetsdetailsgrid.getCreatedBY().getSchoolId()==null ? evnetsdetailsgrid.getCreatedBY().getDistrictId().getDistrictName(): evnetsdetailsgrid.getCreatedBY().getSchoolId().getSchoolName();
							}
							
									
							 tmRecords.append("<td style='font-size:12px;'>"+districtOrSchoolName+"</td>");
						

						}

						int total_scheduled_candidates=0;
						Date maxEventDate=null;
						Date minEventDate=null;
						
						if(!mapCountEventIdBySlotSelction.isEmpty() || !mapEventIdCandidateEventDetails.isEmpty())
						{
							
							
							if(mapCountEventIdBySlotSelction.get(evnetsdetailsgrid.getEventId())!=null)
							{
								total_scheduled_candidates=mapCountEventIdBySlotSelction.get(evnetsdetailsgrid.getEventId());
								
							}else if(mapEventIdCandidateEventDetails.get(evnetsdetailsgrid.getEventId())!=null)
							{
								
								total_scheduled_candidates=mapEventIdCandidateEventDetails.get(evnetsdetailsgrid.getEventId());
							}
							
							else{
								total_scheduled_candidates=0;
							}
						}
						
						
						if(!mapReturnMaxEventStartDate.isEmpty() && !mapReturnMinEventStartDate.isEmpty())
						{
							
							if(mapReturnMaxEventStartDate.get(evnetsdetailsgrid.getEventId())!=null && mapReturnMinEventStartDate.get(evnetsdetailsgrid.getEventId())!=null )
							{
								maxEventDate=mapReturnMaxEventStartDate.get(evnetsdetailsgrid.getEventId());
								
								minEventDate=mapReturnMinEventStartDate.get(evnetsdetailsgrid.getEventId());
							}
							
							else{
								maxEventDate=null;
								minEventDate=null;
							}
						}
							
						 tmRecords.append("<td style='font-size:12px;'>"+total_scheduled_candidates+""+"</td>");
						 
						 tmRecords.append("<td style='font-size:12px;'>"+evnetsdetailsgrid.getEventTypeId().getEventTypeName()+"</td>");
						 
						
					
					
					if(minEventDate == null && maxEventDate == null)
					{
						
						 tmRecords.append("<td style='font-size:12px;'>N/A</td>");
						
						
						 tmRecords.append("<td style='font-size:12px;'>N/A</td>");
					
					}
					else
					{
						
						 tmRecords.append("<td style='font-size:12px;'>"+maxEventDate+"</td>");
						
						 tmRecords.append("<td style='font-size:12px;'>"+minEventDate+"</td>");
						
					
					}
					
					boolean statusFlagssch=false;
					boolean statusFlagPart=false;
					boolean statusFlagFac=false;
					
							
					if(eventDetailssch.containsKey(evnetsdetailsgrid.getEventId())){
						
						statusFlagssch=true;
					}else{
						
					}
					
					if(evnetsdetailsgrid.getEventTypeId().getEventTypeId()!=1){
						
					if(eventDetailsfac.containsKey(evnetsdetailsgrid.getEventId())){
						
						
						statusFlagFac=true;
					}
					else{
						
							
					}
					}else
					{
							
					}	
					
					if(eventDetailspar.containsKey(evnetsdetailsgrid.getEventId())){
						
						statusFlagPart=true;
							}else{
							
					}

					if(evnetsdetailsgrid.getEventTypeId().getEventTypeId().equals(1))
					{
						if(statusFlagssch && statusFlagPart){
							
							 tmRecords.append("<td style='font-size:12px;'>"+optCmplt+"</td>");
							
							
							
						}else{
							
							 tmRecords.append("<td style='font-size:12px;'>"+optIncomlete+"</td>");
							
							
						}
					}else{
						if(statusFlagssch && statusFlagPart && statusFlagFac){
							
							 tmRecords.append("<td style='font-size:12px;'>"+optCmplt+"</td>");
							
							
						}else{
							
							 tmRecords.append("<td style='font-size:12px;'>"+optIncomlete+"</td>");
						
						}
					}
																						
					   tmRecords.append("</tr>");
			   }
			}
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
}


	public String getEventType()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb	=	new StringBuffer();
		List <CmsEventTypeMaster> interactionType	= new ArrayList<CmsEventTypeMaster>();		
		try 
		{
			
		Criterion criterionActive=Restrictions.eq("status","A");
		Order  order=Order.asc("eventTypeName");
		//interactionType=cmsEventTypeMasterDao.findAll();
		interactionType=cmsEventTypeMasterDao.findByCriteria(order,criterionActive);
		//System.out.println("get Int");
			if(interactionType.size()>0)
			{	
				sb.append("<select class='form-control help-inline' id='interactionType' onclick='checkType()' >");
			
	        		sb.append("<option value=''>"+lblAllEventType+"</option>");
	        		for(CmsEventTypeMaster dtm : interactionType)
	        		{
	        			sb.append("<option value='"+dtm.getEventTypeId()+"'>"+dtm.getEventTypeName()+"</option>");
	        		}
	        	sb.append("</select>");
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	
	public String getEventFormat(int evnttype)
	{
		System.out.println("--------Getting Type-------");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb	=	new StringBuffer();
		List <CmsEventFormatMaster> interactionType	= new ArrayList<CmsEventFormatMaster>();		
		try 
		{
		CmsEventTypeMaster cmsEventTypeMaster=cmsEventTypeMasterDao.findById(evnttype,false,false);	
		Criterion crt=Restrictions.eq("cmsEventTypeMaster",cmsEventTypeMaster);	
		interactionType=cmsEventFormatMasterDAO.findByCriteria(crt);
		
			if(interactionType.size()>0)
			{	
				sb.append("<select id=\"formatEvent\" id=\"formatEvent\">");
	        		if(interactionType.size()>1)
				     sb.append("<option value=''>"+lblSelectFormat+"</option>");
	        		
	        		for(CmsEventFormatMaster dtm : interactionType)
	        		{
	        			sb.append("<option id='fmt"+dtm.getEventFormatId()+"' value='"+dtm.getEventFormatId()+"'>"+dtm.getEventFormatName()+"</option>");
	        		}
	        	
	        		
	        		
	        		sb.append("</select>");
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	public String getEventChannel(int evnttype)
	{
		System.out.println("--------Getting Type-------");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb	=	new StringBuffer();
		CmsEventTypeMaster cmsEventTypeMaster=cmsEventTypeMasterDao.findById(evnttype,false,false);	
		Criterion crt=Restrictions.eq("cmsEventTypeMaster",cmsEventTypeMaster);	
		List <CmsEventChannelMaster> interactionchannel	= new ArrayList<CmsEventChannelMaster>();		
		try 
		{
		interactionchannel=cmsEventChannelMasterDAO.findByCriteria(crt);
		System.out.println("get Int");
			if(interactionchannel.size()>0)
			{	
				
				sb.append("<select id='channel'>");
                   			
				if(interactionchannel.size()>1)	
				sb.append("<option value=''>"+lblSelectChannel+"</option>");
				
	        		for(CmsEventChannelMaster dtm : interactionchannel)
	        		{
	        			sb.append("<option value='"+dtm.getEventChannelId()+"'>"+dtm.getEventChannelName()+"</option>");
	        		}
	        	sb.append("</select>");
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return sb.toString();	
	}
	


public String getEventSchedule(int evnttype)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
	}
	StringBuffer sb	=	new StringBuffer();
	List <CmsEventScheduleMaster> interactiosch	= new ArrayList<CmsEventScheduleMaster>();		
	try 
	{
		
	CmsEventTypeMaster cmsEventTypeMaster=cmsEventTypeMasterDao.findById(evnttype,false,false);	
	System.out.println("cmsEventTypeMaster Name :: "+cmsEventTypeMaster.getEventTypeName());
	
	Criterion crt=Restrictions.eq("cmsEventTypeMaster",cmsEventTypeMaster);	
	
	Order order=Order.asc("eventScheduleName");
	interactiosch=cmsEventScheduleMasterDAO.findByCriteria(order,crt);
	
//	System.out.println(">>>> NO of Schedule ::  "+interactiosch.size());
		if(evnttype==1)
		{
			if(interactiosch.size()>0)
			{	
	        		for(CmsEventScheduleMaster dtm : interactiosch)
	        		{
	        			sb.append("<label class='radio' style='margin-top :0px; min-height: 10px;margin-bottom: 0px;'>");
	        			      sb.append("<input type='radio' name='intscheduleId' id='intschedule"+dtm.getEventScheduleId()+"' value='"+dtm.getEventScheduleId()+"' checked>");
	        			      sb.append(dtm.getEventScheduleName());
	        			sb.append("</label>");
	        		}
			}
		}
		else
		{
			if(interactiosch.size()>0)
			{	
	        		for(CmsEventScheduleMaster dtm : interactiosch)
	        		{
	        			sb.append("<label class='radio' style='margin-top :0px; min-height: 10px;margin-bottom: 0px;'>");
	        			      sb.append("<input type='radio' name='intscheduleId' id='intschedule"+dtm.getEventScheduleId()+"' value='"+dtm.getEventScheduleId()+"'>");
	        			      sb.append(dtm.getEventScheduleName());
	        			sb.append("</label>");
	        		}
			}
		}
		
	} 
	catch (Exception e){
		e.printStackTrace();
	}
	//System.out.println(sb.toString());
	return sb.toString();	
}

public String[] saveEvent(String evntId,String title,int districtId,int interactionType,/*int format,int channel,*/int intschedule,String messagetoparticipant,String description,String messagetoprinciple,String logo,int sendmessage,Integer quesId,String messtofacilitators
		,String subjectforFacilator,String subjectforParticipants,int headQuarterId,int branchId)
{
	WebContext context;
	String[] data= new String[2];
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	
	DistrictMaster districtMaster=null;
	HeadQuarterMaster headQuarterMaster=null;
	BranchMaster branchMaster=null;
	EventDetails eventDetails=null;
	int entityType;
	UserMaster userSession=null;
	
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
	throw new IllegalStateException(msgYrSesstionExp);
	}else{
		userSession	=	(UserMaster) session.getAttribute("userMaster");
		entityType=userSession.getEntityType();
	}	
	
	if(entityType!=5 && entityType!=6){
		   districtMaster=districtMasterDAO.findById(districtId,false,false);	
		}else{
			headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
			if(branchId!=0){
				branchMaster =branchMasterDAO.findById(branchId, false, false);	
				headQuarterMaster=branchMaster.getHeadQuarterMaster();
			}	
		}
	
	try{
	if(evntId!=null && !evntId.equals(""))
	{
		eventDetails=eventDetailsDAO.findById(Integer.parseInt(evntId), false,false);	
	}else
	{	
	eventDetails=new EventDetails();
	}
	List<CGStatusEventTemp> listCGStatusEventTemp=new ArrayList<CGStatusEventTemp>();
	boolean cgEventFlag=false;
	try{
		listCGStatusEventTemp= cGStatusEventTempDAO.findByCriteria(Restrictions.eq("sessionId", session.getId()));
		if(listCGStatusEventTemp!=null && listCGStatusEventTemp.size()>0){
			eventDetails.setJobOrder(listCGStatusEventTemp.get(0).getJobOrder());
			cgEventFlag=true;
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
	
	CmsEventTypeMaster  cmsEventTypeMaster=cmsEventTypeMasterDao.findById(interactionType, false,false);
	/*CmsEventFormatMaster cmsEventFormatMaster=cmsEventFormatMasterDAO.findById(format,false,false);
	CmsEventChannelMaster cmsEventChannelMaster=cmsEventChannelMasterDAO.findById(channel,false,false);*/
	CmsEventScheduleMaster cmsEventScheduleMaster=cmsEventScheduleMasterDAO.findById(intschedule,false,false);
	
	I4QuestionSets i4QuestionSets = null;
	List<I4QuestionSets> i4QuestionSetslist = new ArrayList<I4QuestionSets>();
	boolean quesInDistFlag = false;
	
	if(interactionType==1)
	{
		if(districtMaster!=null)
		{
			
			if(quesId!=null && !quesId.equals(""))
			{
				
				i4QuestionSets = i4QuestionSetsDAO.findById(quesId, false, false);
				i4QuestionSetslist = i4QuestionSetsDAO.findQuestionSetByIdAndDist(quesId, districtMaster);
				
				if(i4QuestionSetslist.size()>0)
					quesInDistFlag = true;
			}
		}
	}
	else
	{
		quesInDistFlag= true;
	}
	
	if(quesInDistFlag)
	 {
		eventDetails.setI4QuestionSets(i4QuestionSets);
		eventDetails.setEventName(title);
		eventDetails.setDistrictMaster(districtMaster);
		
		// headquarter and branch
		eventDetails.setHeadQuarterMaster(headQuarterMaster);
		eventDetails.setBranchMaster(branchMaster);
		
		eventDetails.setEventTypeId(cmsEventTypeMaster);
		/*eventDetails.setEventFormatId(cmsEventFormatMaster);
		eventDetails.setEventChannelId(cmsEventChannelMaster);*/
		eventDetails.setEventSchedulelId(cmsEventScheduleMaster);
		eventDetails.setStatus("A");
		eventDetails.setCreatedBY(userSession);
		eventDetails.setCreatedDateTime(new Date());
		eventDetails.setDescription(description);
		eventDetails.setMsgtoparticipants(messagetoparticipant);
		eventDetails.setMsgtofacilitators(messtofacilitators);
		eventDetails.setSubjectforFacilator(subjectforFacilator);
		eventDetails.setSubjectforParticipants(subjectforParticipants);
		if(logo!=null && !logo.equals(""))
		{	
		eventDetails.setLogo(logo);
		}
		
		if(entityType == 3)
		{
			if(userSession.getSchoolId()!=null)
				eventDetails.setSchoolId(userSession.getSchoolId().getSchoolId());
		}
		
		
		eventDetailsDAO.makePersistent(eventDetails);
		
		List<String> emailaddress=new ArrayList<String>();
		emailaddress.add("sonu86gupta@gmail.com");
		emailaddress.add("r.tyagi1@gmail.com");
		String messageSubject="You're Invited CMS Networking";
		String content="";
		int evntinttype=eventDetails.getEventTypeId().getEventTypeId();
		String forMated = "",eventIds = "",baseURL="";
		if(evntinttype==1 || evntinttype==3 || evntinttype==4){
			baseURL=Utility.getValueOfPropByKey("basePath");
			String eventId=Utility.encryptNo(eventDetails.getEventId());
		    eventIds = Utility.encodeInBase64(eventId);
			forMated = Utility.encodeInBase64(eventIds);
			forMated = baseURL+"cmssignup.do?id="+forMated;
			eventDetails.setEventURL(forMated);
			eventDetailsDAO.makePersistent(eventDetails);
		}
		
		data[0]=String.valueOf((eventDetails.getEventId()));
	}
	else
	{
		data[1] = "1";
	}
			
	 try {
		if(cgEventFlag)
		{
			SessionFactory sessionFactory = eventParticipantsListDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			Transaction txOpen = statelesSsession.beginTransaction();
			
			for(CGStatusEventTemp  cgStatusEventTemp : listCGStatusEventTemp){
				TeacherDetail td=cgStatusEventTemp.getTeacherDetail();
				
				EventParticipantsList eventParticipantsList=new EventParticipantsList();
				eventParticipantsList.setParticipantFirstName(td.getFirstName());
				eventParticipantsList.setParticipantLastName(td.getLastName());
				eventParticipantsList.setParticipantEmailAddress(td.getEmailAddress());
				//eventParticipantsList.setNormScore(0);
				eventParticipantsList.setDistrictMaster(eventDetails.getDistrictMaster());
				eventParticipantsList.setHeadQuarterMaster(eventDetails.getHeadQuarterMaster());
				eventParticipantsList.setBranchMaster(eventDetails.getBranchMaster());
				eventParticipantsList.setInvitationEmailSent(false);
				eventParticipantsList.setStatus("A");
				eventParticipantsList.setCreatedBY(userSession);
				eventParticipantsList.setEventDetails(eventDetails);
				eventParticipantsList.setCreatedDateTime(new Date());
				
				statelesSsession.insert(eventParticipantsList);	
			}
			txOpen.commit();
			statelesSsession.close();
			try{
				StatelessSession statelesSsession2 = sessionFactory.openStatelessSession();
				Transaction txOpen2 = statelesSsession2.beginTransaction();
				for(CGStatusEventTemp  cgStatusEventTemp : listCGStatusEventTemp){
					statelesSsession2.delete(cgStatusEventTemp);
				}
				txOpen2.commit();
				statelesSsession2.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	}catch (Exception e) {
		e.printStackTrace();
	}
return data;
}

public String deleteEvent(int evnschid)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
	throw new IllegalStateException(msgYrSesstionExp);
	}
	try	{
		EventSchedule eventSchedule=eventScheduleDAO.findById(evnschid,false,false);
		EventDetails eventDetails=eventSchedule.getEventDetails();
		Criterion crt=Restrictions.eq("eventId",eventDetails);
		eventScheduleDAO.makeTransient(eventSchedule);
		int rowcount=eventScheduleDAO.getRowCount(crt);
		if(rowcount==0)
		{
			eventDetailsDAO.makeTransient(eventDetails);
		}
	}catch(Exception e){e.printStackTrace();}
	return "success";
}
public String updateEventStatus(int evnid)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
    }
	
	try{
	EventSchedule evnsch=eventScheduleDAO.findById(evnid, false, false);
	
	String stat=evnsch.getStatus();
	System.out.println("stat is"+stat);
	if(stat.equals("A"))
	{
		evnsch.setStatus("I");
	}
	if(stat.equals("I"))
	{
		evnsch.setStatus("A");
	}
	EventDetails evndtl=evnsch.getEventDetails();
	eventScheduleDAO.makePersistent(evnsch);
	Criterion crt=Restrictions.eq("eventId", evndtl);
	}catch (Exception e) {
	System.out.println(e);
	}
	
	return "success";	
}

public boolean checkDuplicacy(String name,String evnid,int districtId)
{
boolean val=false;
try
{
int flag=0;	
DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
Criterion crt=Restrictions.eq("eventName", name);
Criterion crt1=Restrictions.eq("districtMaster", districtMaster);
int rows=eventDetailsDAO.getRowCount(crt,crt1);
if(evnid!=null && !evnid.equals(""))
{
EventDetails eventDetails=eventDetailsDAO.findById(Integer.parseInt(evnid), false,false);
if(eventDetails.getEventName().equals(name))
{
	flag=1;	
}
}

if(rows>0 && flag==0)
{
val=true;	
}
}catch(Exception e)
{
e.printStackTrace();	
}
return val;
}

//--------------------Get TemplateBY district---------
public String getTemplatesByDistrictId(int districtId,int headQuarterId ,int branchId)
{
	/* ========  For Session time Out Error =========*/
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
	}
	StringBuffer sb	=	new StringBuffer();
	try{
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		List <EventDescriptionTemplates> lstdistricttemplates	= new ArrayList<EventDescriptionTemplates>();
	    Criterion criterionHQ=null;
	    Criterion criterionBA=null;
	    HeadQuarterMaster headQuarterMaster=null;
	    BranchMaster branchMaster=null;
	    
	    Criterion criterion1 =	Restrictions.eq("status", "A");
		  if(userMaster.getEntityType()==5 || userMaster.getEntityType()==6){
			  if(branchId!=0){
				  branchMaster=branchMasterDAO.findById(branchId,false,false);
				  headQuarterMaster=branchMaster.getHeadQuarterMaster();
				  criterionHQ=Restrictions.eq("headQuarterMaster",headQuarterMaster);
				  criterionBA=Restrictions.eq("branchMaster", branchMaster);
				  lstdistricttemplates=eventDescriptionTemplatesDAO.findByCriteria(criterionHQ,criterionBA,criterion1);
			  }else {
				  headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
				  criterionHQ=Restrictions.eq("headQuarterMaster",headQuarterMaster);
				  lstdistricttemplates=eventDescriptionTemplatesDAO.findByCriteria(criterionHQ,Restrictions.isNull("branchMaster"),criterion1);
			  }
		  }else{
			    DistrictMaster districtMaster=null;
			    if(districtId!=0 && districtId>0)
			      districtMaster =districtMasterDAO.findById(districtId, false, false);	
				
			    Criterion criterion=Restrictions.eq("districtMaster", districtMaster);
				
				if(districtMaster!=null) 
					lstdistricttemplates=eventDescriptionTemplatesDAO.findByCriteria(criterion,criterion1);
				else
					lstdistricttemplates=eventDescriptionTemplatesDAO.findByCriteria(criterion1,Restrictions.isNull("headQuarterMaster"),Restrictions.isNull("branchMaster"));
		  }
	
	 if(lstdistricttemplates.size()>0)
	 {
		 sb.append("<select id='descriptiont' onchange='getDescription()'>");
			
 		sb.append("<option value=''>"+lblSelectType+"</option>");
 		for(EventDescriptionTemplates dtm : lstdistricttemplates)
 		{
 			sb.append("<option value='"+dtm.getTemplateId()+"'>"+dtm.getTemplateName()+"</option>");
 		}
 	sb.append("</select>"); 
	 }
	}catch (Exception e){
		e.printStackTrace();
	}
	return sb.toString();
}
//----------------------------------------------------
public String getTemplateDescription(int tempId)
{
StringBuffer sb	=	new StringBuffer();	
WebContext context;
context = WebContextFactory.get();
HttpServletRequest request = context.getHttpServletRequest();
HttpSession session = request.getSession(false);
if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
{
	throw new IllegalStateException(msgYrSesstionExp);
}
try{
EventDescriptionTemplates eventDescriptionTemplates=eventDescriptionTemplatesDAO.findById(tempId, false, false);
sb.append(eventDescriptionTemplates.getTemplateBody());	
}catch(Exception e){
e.printStackTrace();	
}
return sb.toString();
}


public String getEventEmailMessageTemp(Integer districtId,int headQuarterId ,int branchId)
{
	/* ========  For Session time Out Error =========*/
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
	}
	StringBuffer sb	=	new StringBuffer();
	try{
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			
	List <EventEmailMessageTemplates> lstdistricttemplates	= new ArrayList<EventEmailMessageTemplates>();		
	Criterion criterion1 =	Restrictions.eq("status", "A");
	
	  Criterion criterionHQ=null;
	  Criterion criterionBA=null;
	  HeadQuarterMaster headQuarterMaster=null;
	  BranchMaster branchMaster=null;
	  if(userMaster.getEntityType()==5 || userMaster.getEntityType()==6){
		if(branchId!=0){
			  branchMaster=branchMasterDAO.findById(branchId,false,false);
			  headQuarterMaster=branchMaster.getHeadQuarterMaster();
			  criterionHQ=Restrictions.eq("headQuarterMaster",headQuarterMaster);
			  criterionBA=Restrictions.eq("branchMaster", branchMaster);
			  lstdistricttemplates=eventEmailMessageTemplatesDAO.findByCriteria(criterionHQ,criterionBA,criterion1);
		  }else {
			  headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
			  criterionHQ=Restrictions.eq("headQuarterMaster",headQuarterMaster);
			  lstdistricttemplates=eventEmailMessageTemplatesDAO.findByCriteria(criterionHQ,criterion1,Restrictions.isNull("branchMaster"));
		  }
	  }else{
		  DistrictMaster districtMaster = null;
		  
		  
		  if(districtId!=null)
		  {
			 districtMaster=districtMasterDAO.findById(districtId, false, false);
			 Criterion criterion=Restrictions.eq("districtMaster", districtMaster);
			 lstdistricttemplates=eventEmailMessageTemplatesDAO.findByCriteria(criterion,criterion1,Restrictions.isNull("branchMaster"));
		  }
		 else
		 {
			lstdistricttemplates=eventEmailMessageTemplatesDAO.findByCriteria(criterion1,Restrictions.isNull("headQuarterMaster"),Restrictions.isNull("branchMaster"));
		 }
	 }
	
	
	 if(lstdistricttemplates!=null &&  lstdistricttemplates.size()>0)
	 {
	  sb.append("<select id='messagetoprinciple' onchange='getEmailDescription()'>");
 		sb.append("<option value=''>"+sltTemplate+"</option>");
 		for(EventEmailMessageTemplates dtm : lstdistricttemplates)
 		{
 			sb.append("<option value='"+dtm.getTemplateId()+"'>"+dtm.getTemplateName()+"</option>");
 		}
 	  sb.append("</select>"); 
	 }
	 else{
		 sb.append("");
		    /*sb.append("<select id='messagetoprinciple' onchange='getEmailDescription()'>");
	 		sb.append("<option value=''>Select Message</option>");
	 		sb.append("</select>");*/
	 }
	}catch (Exception e){
		e.printStackTrace();
	}
	return sb.toString();
}
public String getEventEmailMessageTempBody(int tempId)
{
StringBuffer sb	=	new StringBuffer();	
WebContext context;
context = WebContextFactory.get();
HttpServletRequest request = context.getHttpServletRequest();
HttpSession session = request.getSession(false);
if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
{
	throw new IllegalStateException(msgYrSesstionExp);
}
try{
	EventEmailMessageTemplates eventEmailMessageTemplates=eventEmailMessageTemplatesDAO.findById(tempId, false, false);
    sb.append(eventEmailMessageTemplates.getTemplateBody());
    sb.append("@@@@####@@@@"+eventEmailMessageTemplates.getSubjectLine());
}catch(Exception e){
e.printStackTrace();	
}
return sb.toString();
}


public Object getEventById(int eventId)
{
	StringBuffer bfr=new StringBuffer();
	StringBuffer sb=new StringBuffer();
	WebContext context;
	EventDetails eventDetails	=	null;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	
	Object obj[] =new Object[2];

	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
    }
	try{
		boolean urlAndQrCodeflag=false;
		eventDetails 	=	eventDetailsDAO.findById(eventId, false, false);
		if(eventDetails.getEventTypeId().getEventTypeId()==3)
			urlAndQrCodeflag=true;
		//*************************************************
		List <CmsEventScheduleMaster> interactiosch	= new ArrayList<CmsEventScheduleMaster>();
		CmsEventTypeMaster cmsEventTypeMaster=cmsEventTypeMasterDao.findById(eventDetails.getEventTypeId().getEventTypeId(),false,false);	
		Criterion crt=Restrictions.eq("cmsEventTypeMaster",cmsEventTypeMaster);	
		interactiosch=cmsEventScheduleMasterDAO.findByCriteria(crt);
		
			if(interactiosch.size()>0)
			{	
	        		for(CmsEventScheduleMaster dtm : interactiosch)
	        		{
	        			if(eventDetails.getEventSchedulelId().getEventScheduleId().equals(dtm.getEventScheduleId())){
	        				//sb.append("<option value='"+dtm.getEventScheduleId()+"' selected>"+dtm.getEventScheduleName()+"</option>");
	        				sb.append("<label class='radio' style='margin-top :0px; min-height: 10px;margin-bottom: 0px;'>");
	        			      sb.append("<input type='radio' name='intscheduleId' id='intschedule"+dtm.getEventScheduleId()+"' value='"+dtm.getEventScheduleId()+"' checked>");
	        			      sb.append(dtm.getEventScheduleName());
	        			sb.append("</label>");
	        			}else{
	        			 //    sb.append("<option value='"+dtm.getEventScheduleId()+"'>"+dtm.getEventScheduleName()+"</option>");
	        				sb.append("<label class='radio' style='margin-top :0px; min-height: 10px;margin-bottom: 0px;'>");
	        			      sb.append("<input type='radio' name='intscheduleId' id='intschedule"+dtm.getEventScheduleId()+"' value='"+dtm.getEventScheduleId()+"'>");
	        			      sb.append(dtm.getEventScheduleName());
	        			sb.append("</label>");
	        			}
	        		}
			}
		
		String link="";
		String qrcode="";
		if(urlAndQrCodeflag){
		   link=eventDetails.getEventURL();
		   createQRCode(link,"candqrcode"+session.getId()+".png",request);
		   String littleURL=Utility.getShortURL(link);
	       link ="<a href='"+littleURL+"' target='_blank'>"+littleURL+"</a>";
		   String path = Utility.getValueOfPropByKey("contextBasePath")+"/qrcode/"+"candqrcode"+session.getId()+".png";
	       qrcode="<img src='"+path+"'/>";
		}
	    
		bfr.append(eventDetails.getEventName()+"||||");
		bfr.append(eventDetails.getEventTypeId().getEventTypeId()+"||||");
		
		/*bfr.append(eventDetails.getEventFormatId().getEventFormatId()+"||||");
		bfr.append(eventDetails.getEventChannelId().getEventChannelId()+"||||");*/
		
		bfr.append(sb.toString()+"||||");
		if(eventDetails.getHeadQuarterMaster()==null){
			bfr.append(eventDetails.getDistrictMaster().getDistrictId()+"||||");
			bfr.append(eventDetails.getDistrictMaster().getDistrictName()+"||||");
				
		}else{
			bfr.append("||||");
			bfr.append("||||");
		}
		
		bfr.append(eventDetails.getDescription()+"||||");
		bfr.append(eventDetails.getMsgtoparticipants()+"||||");
		bfr.append(link+"||||");
		bfr.append(qrcode+"||||");
		if(eventDetails.getEventTypeId().getEventTypeId()==1)
		{
			if(eventDetails.getI4QuestionSets()!=null){
				bfr.append(eventDetails.getI4QuestionSets().getID()+"||||");
				bfr.append(eventDetails.getI4QuestionSets().getQuestionSetText());
			}
		}
		else
		{
			bfr.append(""+"||||");
			bfr.append("");
		}
		String subjectforFacilator=eventDetails.getSubjectforFacilator()==null?"":eventDetails.getSubjectforFacilator();
		String subjectforParticipants=eventDetails.getSubjectforParticipants()==null?"":eventDetails.getSubjectforParticipants();
		
		bfr.append("||||"+eventDetails.getMsgtofacilitators());
		bfr.append("||||"+subjectforFacilator);
		bfr.append("||||"+subjectforParticipants);
		

		obj[0]=bfr.toString();
		obj[1]=eventDetails;

	}catch (Exception e) {
		e.printStackTrace();
		
	}
	return obj;			

}

public String createQRCode(String url,String filename,HttpServletRequest request)
{
	String filePath="";
	 //String myCodeText = "http://Crunchify.com/";
	 // String filePath = "D:/RahulTyagi/QRCode Liberary/"+filename;
	 String root = request.getRealPath("/")+"/qrcode/";
	 File path=new File(root);
	 if (!path.exists()) {
			boolean status = path.mkdirs();
		}
	 
	 filePath = root +filename;
     int size = 125;
     String fileType = "png";
     File myFile = new File(filePath);
     try {
         Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
         hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
         QRCodeWriter qrCodeWriter = new QRCodeWriter();
         BitMatrix byteMatrix = qrCodeWriter.encode(url,BarcodeFormat.QR_CODE, size, size, hintMap);
         int CrunchifyWidth = byteMatrix.getWidth();
         BufferedImage image = new BufferedImage(CrunchifyWidth, CrunchifyWidth,
                 BufferedImage.TYPE_INT_RGB);
         image.createGraphics();

         Graphics2D graphics = (Graphics2D) image.getGraphics();
         graphics.setColor(Color.WHITE);
         graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyWidth);
         graphics.setColor(Color.BLACK);

         for (int i = 0; i < CrunchifyWidth; i++) {
             for (int j = 0; j < CrunchifyWidth; j++) {
                 if (byteMatrix.get(i, j)) {
                     graphics.fillRect(i, j, 1, 1);
                 }
             }
         }
         ImageIO.write(image, fileType, myFile);
     } catch (WriterException e) {
         e.printStackTrace();
     } catch (IOException e) {
         e.printStackTrace();
     }
    return filePath;	
}



public String printEventDetails(Integer eventId)
{
	System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ::  "+eventId);
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
    }
	StringBuffer tmRecords =	new StringBuffer();
	try{
		String strCss_lbl="padding:2px;font-size: 15px; line-height: 18px; ";
	//	List<EventSchedule> eventSchedules =new  ArrayList<EventSchedule>();
		EventDetails eventDetails	=	eventDetailsDAO.findById(eventId, false, false);
		
		/*Criterion criterion1 = Restrictions.eq("eventDetails",eventDetails);
			eventSchedules	 = eventScheduleDAO.findByCriteria(criterion1);*/
		      	   
		//LogoPath
			String  path="";
			String fileName= eventDetails.getLogo();
			if(fileName!=null)
			{
				String source="";
				String target="";
				if(eventDetails.getDistrictMaster()!=null){
					source = Utility.getValueOfPropByKey("districtRootPath")+""+eventDetails.getDistrictMaster().getDistrictId()+"/Event/"+eventDetails.getEventId()+"/"+fileName;
				}
				else if(eventDetails.getHeadQuarterMaster()!=null){
					source = Utility.getValueOfPropByKey("headQuarterRootPath")+""+eventDetails.getHeadQuarterMaster().getHeadQuarterId()+"/Event/"+eventDetails.getEventId()+"/"+fileName;
				}
				
				if(eventDetails.getDistrictMaster()!=null){
					target = context.getServletContext().getRealPath("/")+"/"+"/Event/"+eventDetails.getDistrictMaster().getDistrictId()+"/"+eventDetails.getEventId()+"/";
				}else if(eventDetails.getHeadQuarterMaster()!=null){
					target = context.getServletContext().getRealPath("/")+"/"+"/Event/"+eventDetails.getHeadQuarterMaster().getHeadQuarterId()+"/"+eventDetails.getEventId()+"/";
				}
			    
				File sourceFile = new File(source);
				File targetDir = new File(target);
				if(!targetDir.exists())
					targetDir.mkdirs();
	
				File targetFile = new File(targetDir+"/"+sourceFile.getName());
				FileUtils.copyFile(sourceFile, targetFile);
			
				if(eventDetails.getDistrictMaster()!=null){
					 path = Utility.getBaseURL(request)+"Event/"+eventDetails.getDistrictMaster().getDistrictId()+"/"+eventDetails.getEventId()+"/"+sourceFile.getName();
				}else if(eventDetails.getHeadQuarterMaster()!=null){
					 path = Utility.getBaseURL(request)+"Event/"+eventDetails.getHeadQuarterMaster().getHeadQuarterId()+"/"+eventDetails.getEventId()+"/"+sourceFile.getName();
				}
			}
			
			//QR Code Path
			String filepath="";
			String qrCodepath="";
			String scanRequestStr="";
			String target="";
			
			if(eventDetails.getEventURL()!=null)
			{
				scanRequestStr= Utility.getLocaleValuePropByKey("msgEventAjax1", locale) ;
			    filepath=createQRCode(eventDetails.getEventURL(),"candqrcode2.png",request);
			 
				if(eventDetails.getDistrictMaster()!=null){
					 target= context.getServletContext().getRealPath("/")+"/"+"/Event/"+eventDetails.getDistrictMaster().getDistrictId()+"/"+eventDetails.getEventId()+"/";
				}else if(eventDetails.getHeadQuarterMaster()!=null){
					target= context.getServletContext().getRealPath("/")+"/"+"/Event/"+eventDetails.getHeadQuarterMaster().getHeadQuarterId()+"/"+eventDetails.getEventId()+"/";
				}
			
			    File sourceFile = new File(filepath);
				File targetDir = new File(target);
				if(!targetDir.exists())
					targetDir.mkdirs();

				File targetFile = new File(targetDir+"/"+sourceFile.getName());
				FileUtils.copyFile(sourceFile, targetFile);
				
				if(eventDetails.getDistrictMaster()!=null){
					qrCodepath = Utility.getBaseURL(request)+"Event/"+eventDetails.getDistrictMaster().getDistrictId()+"/"+eventDetails.getEventId()+"/"+sourceFile.getName();
				}else if(eventDetails.getHeadQuarterMaster()!=null){
					qrCodepath = Utility.getBaseURL(request)+"Event/"+eventDetails.getHeadQuarterMaster().getHeadQuarterId()+"/"+eventDetails.getEventId()+"/"+sourceFile.getName();
				}
				
			}
			
		  tmRecords.append("<table cellpadding='2' width='100%' border='0'>");
			
			tmRecords.append("<tr>");
				tmRecords.append("<td >");
				
				tmRecords.append("</td>");
			tmRecords.append("</tr>");
			
		  
		  tmRecords.append("<tr>");
				tmRecords.append("<td>");
					tmRecords.append("<table   width='100%' border='0'>");
					    tmRecords.append("<tr>");
					    	tmRecords.append("<td>");
						    	tmRecords.append("<table>");
							    	tmRecords.append("<tr>");
								    	tmRecords.append("<td><div><img src='"+path+"' width='80px' height='80px'/></div>");
								    	tmRecords.append("</td>");
							    	tmRecords.append("</tr>");
						    	tmRecords.append("</table>");
					    	tmRecords.append("</td>");
					    tmRecords.append("</tr>");
					tmRecords.append("</table>");
				tmRecords.append("</td>");
			tmRecords.append("</tr>");
			
			tmRecords.append("<tr>");
			tmRecords.append("<td>");
			tmRecords.append("<div class=''></div>");
			tmRecords.append("</td>");
			tmRecords.append("</tr>");
			
			 tmRecords.append("<tr>");
				tmRecords.append("<td >");
				
				tmRecords.append("</td>");
				tmRecords.append("</tr><br>");
		/*if(eventSchedules!=null && eventSchedules.size()>0)
		 for(EventSchedule  eventSchedule : eventSchedules)
		 {
			String eventTime="";
			if(eventSchedule.getEventStartTime().length()>0)
			       eventTime="&nbsp;&nbsp;&nbsp;&nbsp;"+eventSchedule.getEventStartTime()+""+eventSchedule.getEventStartTimeFormat()+"-"+eventSchedule.getEventEndTime()+""+eventSchedule.getEventEndTimeFormat();
			tmRecords.append("<tr>");
			tmRecords.append("<td>");
				tmRecords.append("<table border='0'  width='100%' cellspacing='3'>");
					tmRecords.append("<tr>");
						tmRecords.append("<td width='40%' style='"+strCss_lbl+"'><font color='#007AB4'><b>Date + Time: &nbsp;&nbsp; </font></b>");
							tmRecords.append(Utility.convertDateAndTimeToUSformatOnlyDate(eventSchedule.getEventDateTime()));
							tmRecords.append(eventTime);
						tmRecords.append("</td>");
						if(eventSchedule.getLocation().length()>0){
							tmRecords.append("<td width='60%' style='"+strCss_lbl+"'><font color='#007AB4'><b>Location:&nbsp;&nbsp;</font></b>");
							   tmRecords.append(eventSchedule.getLocation());
							tmRecords.append("</td>");
		 				}else{
		 					tmRecords.append("<td width='60%' style='"+strCss_lbl+" border: 2px solid black;'>");
							tmRecords.append("</td>");
		 				}
					tmRecords.append("</tr>");
				tmRecords.append("</table>");
			tmRecords.append("</td>");
			tmRecords.append("</tr>");
		}*/
	//description/Order info 	
				
		tmRecords.append("<tr>");
			tmRecords.append("<td >");
					tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>"+eventDetails.getEventName()+"</div><br/><br/>");
			
					tmRecords.append("</td>");
		tmRecords.append("</tr>");
				
		
		
				
		tmRecords.append("<tr>");
		tmRecords.append("<td>");
			tmRecords.append("<table   width='100%' border='0'>");
				
				tmRecords.append("<tr>");
				tmRecords.append("<td style='"+strCss_lbl+"'>");
					tmRecords.append("<div class=''>");
						tmRecords.append(eventDetails.getDescription());
				    tmRecords.append("</div>");
				tmRecords.append("</td>");
				tmRecords.append("</tr>");
				
			tmRecords.append("</table>");
		tmRecords.append("</td>");
		tmRecords.append("</tr>");
		
		tmRecords.append("<tr>");
	     tmRecords.append("<td >");
			tmRecords.append("<div>----------------------------------------------------------------------------------------------------</div>");
	     tmRecords.append("</td>");
   tmRecords.append("</tr>");
		
	/*	//type
		tmRecords.append("<tr>");
		tmRecords.append("<td>");
			tmRecords.append("<table width='100%' border='0'>");
				tmRecords.append("<tr>");
					tmRecords.append("<td style='"+strCss_lbl+" width: 30%;'><font ><b>Event Description:&nbsp;&nbsp;</b></font>");
						tmRecords.append(eventDetails.getEventTypeId().getEventTypeName());
					tmRecords.append("</td>");
				tmRecords.append("</tr>");
			tmRecords.append("</table>");
		tmRecords.append("</td>");
		tmRecords.append("</tr>");*/
	
		tmRecords.append("<tr>");
		tmRecords.append("<td>");
			tmRecords.append("<table  border='0' width='100%' border='0'>");
				tmRecords.append("<tr>");
					tmRecords.append("<td style='"+strCss_lbl+" padding-top: 22px;'>");
					 tmRecords.append(msgToRegisterForThisEvent);
					tmRecords.append("</td>");
				tmRecords.append("</tr>");
				
				//QR Code Path
				
				if(eventDetails.getEventURL()!=null){
					tmRecords.append("<tr>");
					  tmRecords.append("<td style='"+strCss_lbl+"'><img src='"+qrCodepath+"'></td>");
				    tmRecords.append("</tr>");
				}
				if(eventDetails.getEventURL()!=null){
					String tinnyUrl =	Utility.getShortURL(eventDetails.getEventURL());
						tmRecords.append("<tr>");
						  tmRecords.append("<td with='100%'>"+msgToRegisterForThisEventl2+"<a href='"+eventDetails.getEventURL()+"' target='_blank'>"+tinnyUrl+"</a></td>");
					    tmRecords.append("</tr>");
			    }
			tmRecords.append("</table>");
		tmRecords.append("</td>");
		tmRecords.append("</tr>");
		tmRecords.append("</table>");
	}catch (Exception e) {
		e.printStackTrace();
	}
	return tmRecords.toString();			
}
public List<DistrictMaster> getFieldOfDistrictList(String DistrictName)
{
	
	/* ========  For Session time Out Error =========*/
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
    }
	List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
	//List<DistrictSchools> districtSchoolsList =  new ArrayList<DistrictSchools>();
	List<DistrictMaster> fieldOfDistrictList1 = null;
	List<DistrictMaster> fieldOfDistrictList2 = null;
	//List<DistrictMaster> districtMasters =new ArrayList<DistrictMaster>();
	try{
		
		Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
		Criterion criterionStatus = Restrictions.eq("status", "A");
		if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
			fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterionStatus);
			
			Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
			fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterionStatus);
			
			districtMasterList.addAll(fieldOfDistrictList1);
			districtMasterList.addAll(fieldOfDistrictList2);
			Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
			districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
			
		}else{
			fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25,criterionStatus);
			districtMasterList.addAll(fieldOfDistrictList1);
		}
		/*System.out.println("districtMasterList.size() :: "+districtMasterList.size());
	
		Criterion criteriondistrict=Restrictions.in("districtMaster", districtMasterList);
	    districtSchoolsList=districtSchoolsDAO.findByCriteria(criteriondistrict);
	
	if(districtSchoolsList!=null && districtSchoolsList.size()>0)
		for(DistrictSchools ds : districtSchoolsList){
			if(!districtMasters.contains(ds.getDistrictMaster())){
				districtMasters.add((ds.getDistrictMaster()));
			}
		}*/
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
	
	return districtMasterList;
}

public String getEmailMessageTempFacilitators(Integer districtId,int headQuarterId ,int branchId)
{
	/* ========  For Session time Out Error =========*/
	System.out.println(">>>>>>>>>districtIddistrictId  :: "+districtId);
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
	}
	StringBuffer sb	=	new StringBuffer();
	try{
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			
	List <EmailMessageTemplatesForFacilitators> lstdistricttemplates	= new ArrayList<EmailMessageTemplatesForFacilitators>();		
	Criterion criterion1 =	Restrictions.eq("status", "A");
	
	Criterion criterionHQ=null;
	Criterion criterionBA=null;
	HeadQuarterMaster headQuarterMaster=null;
	BranchMaster branchMaster=null;
	  if(userMaster.getEntityType()==5 || userMaster.getEntityType()==6){
		  if(branchId!=0){
			  branchMaster=branchMasterDAO.findById(branchId,false,false);
			  headQuarterMaster=branchMaster.getHeadQuarterMaster();
			  criterionHQ=Restrictions.eq("headQuarterMaster",headQuarterMaster);
			  criterionBA=Restrictions.eq("branchMaster", branchMaster);
			  lstdistricttemplates=emailMessageTemplatesForFacilitatorsDAO.findByCriteria(criterion1,criterionHQ,criterionBA);
		  }else{
			  headQuarterMaster=headQuarterMasterDAO.findById(headQuarterId, false, false);
			  criterionHQ=Restrictions.eq("headQuarterMaster",headQuarterMaster);
			  lstdistricttemplates=emailMessageTemplatesForFacilitatorsDAO.findByCriteria(criterion1,criterionHQ,Restrictions.isNull("branchMaster"));
		  }
	  }else{
		DistrictMaster districtMaster=null;
		if(districtId!=null && districtId!=0 && districtId>0){
			districtMaster=districtMasterDAO.findById(districtId, false, false);
			Criterion criterion=Restrictions.eq("distritMaster", districtMaster);
			lstdistricttemplates=emailMessageTemplatesForFacilitatorsDAO.findByCriteria(criterion,criterion1);
		}else{
			lstdistricttemplates=emailMessageTemplatesForFacilitatorsDAO.findByCriteria(criterion1,Restrictions.isNull("headQuarterMaster"),Restrictions.isNull("branchMaster"));
		}
	 }
	
	
	 if(lstdistricttemplates!=null &&  lstdistricttemplates.size()>0)
	 {
	  sb.append("<select id='messagetofacilitators' onchange='getEmailDescription1'>");
 		sb.append("<option value=''>"+sltTemplate+"</option>");
 		for(EmailMessageTemplatesForFacilitators dtm : lstdistricttemplates)
 		{
 			sb.append("<option value='"+dtm.getEmailMessageTemplatesForFacilitatorsId()+"'>"+dtm.getTemplateName()+"</option>");
 		}
 	  sb.append("</select>"); 
	 }
	 else{
		    sb.append("");
	 		
	 }
	}catch (Exception e){
		e.printStackTrace();
	}
	return sb.toString();
}


public String getFacilitatorsEmailMessageTempBody(int tempId)
{
	StringBuffer sb	=	new StringBuffer();	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	  if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
	   try{
		   EmailMessageTemplatesForFacilitators emailMessageTemplatesForFacilitators=emailMessageTemplatesForFacilitatorsDAO.findById(tempId, false, false);
				sb.append(emailMessageTemplatesForFacilitators.getTemplateBody());
				sb.append("@@@@####@@@@"+emailMessageTemplatesForFacilitators.getSubjectLine());
		}catch(Exception e){
		e.printStackTrace();	
		}
		return sb.toString();
}

public String[] showVirtualVideoInterview(String participantId)
{
	System.out.println(" =============== showVirtualVideoInterview ============ ");
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
	}
	
	StringBuffer sb = new StringBuffer();
	EventParticipantsList eventParticipantsList = eventParticipantsListDAO.findById(Integer.parseInt(participantId), false, false);
	
	Map<String,String> vviResponseMap = new HashMap<String,String>();
	List<VVIResponse> vviQuestionLst = new ArrayList<VVIResponse>();
	
	List<String> i4QuesIdList = new ArrayList<String>();
	List<I4QuestionPool> questionList = new ArrayList<I4QuestionPool>();
	
	/*try
	{
		if(eventParticipantsList!=null)
		{
			System.out.println("i4InterviewInvites.get(0).getVideoUrl()+ "+eventParticipantsList.getI4InterviewInvites().getVideoUrl());
			sb.append("<div class=row'>");
				sb.append("<div class='col-sm-12 col-md-12  top10 mt15'>");
					sb.append("<iframe src='"+eventParticipantsList.getI4InterviewInvites().getVideoUrl()+"' id='ifrmTrans' width='100%' height='480px'></iframe>");
				sb.append("</div>");
			sb.append("</div>");
					
		}
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}*/
	
	I4InterviewInvites i4InterviewInvites = null;
	int  maxScore = 0;
	int SliderScore = 0;
	boolean scoreflag = false;
	String[] ReturnArray = new String[2];
	try{
		if(eventParticipantsList!=null){
			if(eventParticipantsList.getI4InterviewInvites()!=null && eventParticipantsList.getI4InterviewInvites().getI4inviteid()!=null)
			{
					i4InterviewInvites = eventParticipantsList.getI4InterviewInvites();
					System.out.println(" i4InterviewInvites >>>>>>>>>>>>>>>>>>>> "+eventParticipantsList.getI4InterviewInvites().getI4inviteid());
					vviQuestionLst = vviResponseDAO.findByI4InviteId(eventParticipantsList.getI4InterviewInvites().getI4inviteid());
			}
			
			if(vviQuestionLst!=null && vviQuestionLst.size()>0)
			{
				for(VVIResponse vviResList:vviQuestionLst)
				{
					i4QuesIdList.add(vviResList.getInterviewQuestionId());
					vviResponseMap.put(vviResList.getInterviewQuestionId(), vviResList.getWebmLink());
				}
				
				questionList = i4QuestionPoolDAO.findByI4QuestionIds(i4QuesIdList);
			}
			
			String playerUrl = "";
			
			sb.append("<div class=row'>");
			sb.append("<div class='col-sm-6 col-md-6  top10 mt15'>");
				if(questionList!=null && questionList.size()>0)
				{
					if(vviResponseMap!=null && vviResponseMap.size()>0){
						for(I4QuestionPool vvir:questionList){
							if(vviResponseMap.get(vvir.getI4QuestionID()) != null){
								
								String str = vviResponseMap.get(vvir.getI4QuestionID());
								
								String[] str1 = str.split("\\?");
								String str2 = str1[0].replace("http://i4videos.s3.amazonaws.com/", "");
								
								System.out.println("  str2 >>>>>>>>>>>>>>>>>>>>>>>> "+str2);
								
								sb.append("<div id='vviUrlId' class='col-sm-12 col-md-12' style='background-color: #0078b4; color:#ffffff; border:2px; border-color:#ffffff; padding:5px; cursor:pointer' onclick='getVideoURL(\""+str2.trim()+"\")'>");
								//	sb.append("<button style='background-color: #0078b4; color:#ffffff;  onclick='getVideoURL("+str2.trim()+")'>");	
										sb.append(vvir.getQuestionText());
								//	sb.append("</button>");
								sb.append("</div>");
							}
						}
					}
				}
				
				sb.append("</div>");
				sb.append("<div class='col-sm-6 col-md-6  top10 mt15'>");
					sb.append("Please click on question for getting video interview");
					sb.append("<iframe src='"+playerUrl+"' id='ifrmTrans' width='100%' height='300px'></iframe>");
				sb.append("</div>");
			sb.append("</div>");
			
			/*if(i4InterviewInvites!=null)
			{
				if(i4InterviewInvites.getI4VideoInterviewMaxScore()!=null)
				{
					maxScore = i4InterviewInvites.getI4VideoInterviewMaxScore();
				
					if( maxScore>0)
					{
						scoreflag=true;
						int iTickInterval=maxScore/10;
						
						if(i4InterviewInvites.getI4VideoInterviewScore()!=null)
							SliderScore = i4InterviewInvites.getI4VideoInterviewScore();
					
						sb.append("<div class='row'>");
						sb.append("<div class='col-sm-9 col-md-9  top20'>");
						sb.append("<input type='hidden' id='inviteInterviewID' value='"+i4InterviewInvites.getId()+"'/>");
						sb.append("<iframe id='ifrmForVideoII'  src='slideract.do?name=normScoreFrm&tickInterval="+iTickInterval+"&max="+maxScore+"&swidth=650&svalue="+SliderScore+"' scrolling='no' frameBorder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:670px;'></iframe>");
						sb.append("</div>");
						sb.append("</div>");
					}
				}
			}*/
		}
			
		ReturnArray[0]=sb.toString();
		ReturnArray[1]=""+scoreflag;
			
	}catch(Exception e){
		e.printStackTrace();
	}

	return ReturnArray;
}


public EventDetails saveCGPhoneEvent(EventDetails eventDetails,Integer districtId, Integer intschedule,String teacherId1)
{		
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	UserMaster userMastr=null;
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
	throw new IllegalStateException(msgYrSesstionExp);
	}	
	 userMastr		=	(UserMaster) session.getAttribute("userMaster");
	try{
		
		DistrictMaster districtMaster=districtMasterDAO.findById(districtId,false, false);
		CmsEventTypeMaster  cmsEventTypeMaster=cmsEventTypeMasterDao.findById(6, false,false);
		CmsEventScheduleMaster cmsEventScheduleMaster=cmsEventScheduleMasterDAO.findById(intschedule,false,false);
		eventDetails.setDistrictMaster(districtMaster);
		eventDetails.setEventTypeId(cmsEventTypeMaster);
		eventDetails.setEventSchedulelId(cmsEventScheduleMaster);
		eventDetails.setStatus("A");
		eventDetails.setCreatedBY(userMastr);
		eventDetails.setCreatedDateTime(new Date());	
		eventDetailsDAO.makePersistent(eventDetails);
		
		
		List<Integer> teacherIdsList =  new ArrayList<Integer>();
		if(teacherId1!=null && !teacherId1.equals(""))
		{		
			for(String id : teacherId1.split(","))
			{
				try
				{ 
					teacherIdsList.add(Integer.parseInt(id));					
				}
				catch(Exception e){}
			}			
		}
		
		List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
		if(teacherIdsList!=null && teacherIdsList.size()>0)
		{
			teacherDetailList = teacherDetailDAO.findByCriteria(Restrictions.and(Restrictions.in("teacherId", teacherIdsList), Restrictions.eq("status", "A")));
		}
		if(teacherDetailList!=null && teacherDetailList.size()>0)
		{
			for(TeacherDetail teacherDetail:teacherDetailList)
			{				
				Criterion criterion1 = Restrictions.eq("participantEmailAddress", teacherDetail.getEmailAddress());
				Criterion criterion2 = Restrictions.eq("eventDetails", eventDetails);
				 List<EventParticipantsList> eventparticipantsList = eventParticipantsListDAO.findByCriteria(criterion1,criterion2);
				  if(eventparticipantsList.size()>0){
					//return "1";
				}else{
					String fName= teacherDetail.getFirstName()==null?"":teacherDetail.getFirstName();
					String lName=teacherDetail.getLastName()==null?"":teacherDetail.getLastName();
					EventParticipantsList eventParticipantsList = new EventParticipantsList();
					eventParticipantsList.setTeacherId(teacherDetail.getTeacherId());
					eventParticipantsList.setParticipantFirstName(fName);
					eventParticipantsList.setParticipantLastName(lName);
					eventParticipantsList.setParticipantEmailAddress(teacherDetail.getEmailAddress());
				//	eventParticipantsList.setNormScore(nscore);
					eventParticipantsList.setDistrictMaster(eventDetails.getDistrictMaster());
					eventParticipantsList.setInvitationEmailSent(false);
					eventParticipantsList.setStatus("A");
					eventParticipantsList.setCreatedBY(userMastr);
					eventParticipantsList.setEventDetails(eventDetails);
					eventParticipantsList.setCreatedDateTime(new Date());
					eventParticipantsListDAO.makePersistent(eventParticipantsList);
				}			
			}		
		}
		
		
		
		/*if(teacherId!=null && teacherId!=0){
		TeacherDetail teacherDetail =teacherDetailDAO.findById(teacherId, false, false);
		Criterion criterion1 = Restrictions.eq("participantEmailAddress", teacherDetail.getEmailAddress());
		Criterion criterion2 = Restrictions.eq("eventDetails", eventDetails);
		 List<EventParticipantsList> eventparticipantsList = eventParticipantsListDAO.findByCriteria(criterion1,criterion2);
		  if(eventparticipantsList.size()>0){
			//return "1";
		}else{
			String fName= teacherDetail.getFirstName()==null?"":teacherDetail.getFirstName();
			String lName=teacherDetail.getLastName()==null?"":teacherDetail.getLastName();
			EventParticipantsList eventParticipantsList = new EventParticipantsList();
			eventParticipantsList.setTeacherId(teacherId);
			eventParticipantsList.setParticipantFirstName(fName);
			eventParticipantsList.setParticipantLastName(lName);
			eventParticipantsList.setParticipantEmailAddress(teacherDetail.getEmailAddress());
		//	eventParticipantsList.setNormScore(nscore);
			eventParticipantsList.setDistrictMaster(eventDetails.getDistrictMaster());
			eventParticipantsList.setInvitationEmailSent(false);
			eventParticipantsList.setStatus("A");
			eventParticipantsList.setCreatedBY(userMastr);
			eventParticipantsList.setEventDetails(eventDetails);
			eventParticipantsList.setCreatedDateTime(new Date());
			eventParticipantsListDAO.makePersistent(eventParticipantsList);
		}
	 }*/
	if(userMastr.getEntityType()!=1){
		String userEmail = userMastr.getEmailAddress();
		if(userEmail!=null)
		{
			List<EventFacilitatorsList> EventFacilitatorsListAll = eventFacilitatorsListDAO.findEventFacilitatorsByEvent(eventDetails);
			if(EventFacilitatorsListAll.size()==0)
			{
			    SchoolMaster schoolMaster=null;
				if(userMastr.getEntityType()==3)
					schoolMaster = userMastr.getSchoolId();
				EventFacilitatorsList eventFacilitatorsList = new EventFacilitatorsList();
				eventFacilitatorsList.setCreatedBY(userMastr);
				eventFacilitatorsList.setCreatedDateTime(new Date());
				eventFacilitatorsList.setDistrictMaster(userMastr.getDistrictId());
				eventFacilitatorsList.setEventDetails(eventDetails);
				eventFacilitatorsList.setFacilitatorEmailAddress(userEmail);
				eventFacilitatorsList.setFacilitatorFirstName(userMastr.getFirstName());
				eventFacilitatorsList.setFacilitatorLastName(userMastr.getLastName());
				eventFacilitatorsList.setInvitationEmailSent(0);
				eventFacilitatorsList.setSchoolId(schoolMaster);
				eventFacilitatorsList.setStatus("A");
				eventFacilitatorsListDAO.makePersistent(eventFacilitatorsList);
			}
		}
	 }
	
	}catch (Exception e) {
		e.printStackTrace();
	}
 return eventDetails;
}

public String getDistrictUserList(Integer districtId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	StringBuffer bufferData= new StringBuffer();
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
	throw new IllegalStateException(msgYrSesstionExp);
	}	
	try{
		DistrictMaster districtMaster=districtMasterDAO.findById(districtId,false, false);
		Criterion criterionDis =Restrictions.eq("districtId", districtMaster);
		List<UserMaster> listUserMasters =new ArrayList<UserMaster>();
		listUserMasters= userMasterDAO.findByCriteria(Order.asc("firstName"),criterionDis);
		if(listUserMasters!=null && listUserMasters.size()>0){
			bufferData.append("<option value=''>"+lblSelectFacilitator+"</option>");
			for(UserMaster user :listUserMasters)
		      bufferData.append("<option value='"+user.getUserId()+"'>"+user.getFirstName()+"  "+user.getLastName()+"("+user.getEmailAddress()+")</option>");
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
 return bufferData.toString();
}


public EventDetails getEventByEventId(Integer eventId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	EventDetails eventDetails= new EventDetails()  ;
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
	}	
	
	try{
		eventDetails=eventDetailsDAO.findById(eventId, false,false);
	}catch (Exception e) {
		e.printStackTrace();
	}
 return eventDetails;
}

public boolean checkAvailFacilitator(Integer eventId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	EventDetails eventDetails= new EventDetails();
	boolean sendmailFlag=false;
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
	}	
	
	try{
		eventDetails=eventDetailsDAO.findById(eventId, false,false);
		Criterion crt=Restrictions.eq("eventDetails", eventDetails);
		List<EventFacilitatorsList> eventFacilitatorsList=eventFacilitatorsListDAO.findByCriteria(crt);
		if(eventFacilitatorsList!=null && eventFacilitatorsList.size()>0){
			sendmailFlag=true;
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
 return sendmailFlag;
}

	public String cancelEvent(Integer eventId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}	
		
		if(eventId==null)
			return "";
		
		EventDetails eventDetails= new EventDetails();
		EventSchedule eventSchedule = null;
		List<EventFacilitatorsList> eventFacilitatorsLists = new ArrayList<EventFacilitatorsList>();
		List<EventParticipantsList> eventParticipantsLists = new ArrayList<EventParticipantsList>();
		boolean isScheduling=false;
		try
		{
			eventDetails=eventDetailsDAO.findById(eventId, false,false);
			eventFacilitatorsLists  = eventFacilitatorsListDAO.findEventFacilitatorsByEvent(eventDetails);
			eventParticipantsLists = eventParticipantsListDAO.findByEventDetails(eventDetails);
			
			List<EventSchedule> eventScheduleList = eventScheduleDAO.findByEventDetail(eventDetails);
			
			if(eventScheduleList!=null && eventScheduleList.size()==1){
				eventSchedule = eventScheduleList.get(0);
				isScheduling=true;
			}else if(eventScheduleList!=null && eventScheduleList.size()>1){
				isScheduling=true;
			}
			String icalFilePath=null;
			if (isScheduling) {
				icalFilePath=createIcalFile(eventDetails.getEventName(), eventDetails.getDescription(), eventScheduleList);
			}
			if(eventFacilitatorsLists!=null && isScheduling)
			for(EventFacilitatorsList eventFacilitator : eventFacilitatorsLists)
			{
				try
				{
					String sEmailBodyText = MailText.getCancelEventMailText(eventSchedule, eventFacilitator.getFacilitatorFirstName(), eventFacilitator.getFacilitatorLastName(),eventDetails);
					String sEmailSubject="Cancel Event";
					String toEmail =eventFacilitator.getFacilitatorEmailAddress(); 
					
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("mukesh.gupta@netsutra.com");
					dsmt.setMailto(toEmail);
					dsmt.setMailsubject(sEmailSubject);
					dsmt.setMailcontent(sEmailBodyText);
					dsmt.setFilepath(icalFilePath);
					dsmt.setLstBcc(new ArrayList<String>());// To Prevent Null Pointer Exception
					try {
						dsmt.start();	
					} catch (Exception e) {}
				 }
				catch(Exception e){e.printStackTrace();}
			}
			
			if(eventParticipantsLists!=null && isScheduling)
			for(EventParticipantsList eventParticipant : eventParticipantsLists)
			{
				try
				{
					String sEmailBodyText = MailText.getCancelEventMailText(eventSchedule, eventParticipant.getParticipantFirstName(), eventParticipant.getParticipantLastName(),eventDetails);
					String sEmailSubject="Cancel Event";
					String toEmail =eventParticipant.getParticipantEmailAddress(); 
					
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("mukesh.gupta@netsutra.com");
					dsmt.setMailto(toEmail);
					dsmt.setMailsubject(sEmailSubject);
					dsmt.setMailcontent(sEmailBodyText);
					dsmt.setFilepath(icalFilePath);
					dsmt.setLstBcc(new ArrayList<String>());// To Prevent Null Pointer Exception
					try {
						dsmt.start();	
					} catch (Exception e) {}
				 }
				catch(Exception e){e.printStackTrace();}
			}
			
			eventDetails.setStatus("I");
			eventDetailsDAO.makePersistent(eventDetails);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}


	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
		List<DistrictSchools> fieldOfSchoolList1 = null;
		List<DistrictSchools> fieldOfSchoolList2 = null;
		try 
		{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);

				DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
				
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				
				if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
					
					fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
					
					Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
					
					fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
					
					schoolMasterList.addAll(fieldOfSchoolList1);
					schoolMasterList.addAll(fieldOfSchoolList2);
					
					Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
					
					schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					
				}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return schoolMasterList;
		
	}


	public void setCandidateEventDetailsDAO(CandidateEventDetailsDAO candidateEventDetailsDAO) {
		this.candidateEventDetailsDAO = candidateEventDetailsDAO;
	}


	public CandidateEventDetailsDAO getCandidateEventDetailsDAO() {
		return candidateEventDetailsDAO;
	}
	
	
	public String createIcalFile(String subject,String description,List<EventSchedule>  eventschlist) throws ParseException 
	{	
		Calendar icsCalendar = new Calendar();
		String calFileName = String.valueOf(System.currentTimeMillis()).substring(6);	
		File calFile = new File(Utility.getValueOfPropByKey("iclcalendarRootPath")+"/"+calFileName+"cal.ics");
		String email="";
		String fromdate="";
		String fromtime="";
		String enddate="";
		String endtime="";
		String googleStartTime="";
		String googleEndTime="";
		String location="";
		for(int i=0;i<eventschlist.size();i++)
		{			
			EventSchedule eventShedule=eventschlist.get(i);					
			location=eventShedule.getLocation();		
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			fromdate=dateFormat.format(eventShedule.getEventDateTime());
			enddate=dateFormat.format(eventShedule.getEventDateTime());
			fromtime=eventShedule.getEventStartTime()+" "+eventShedule.getEventStartTimeFormat();
			endtime=eventShedule.getEventEndTime()+" "+eventShedule.getEventEndTimeFormat();
			
			//convert time AM/PM to 24 hour
			SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
			SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
			Date sdate = parseFormat.parse(fromtime);
			Date edate = parseFormat.parse(endtime);
			String[] convertStartTime =displayFormat.format(sdate).split(":");
			String[] convertEndTime =displayFormat.format(edate).split(":");       
			
			/////StartTime
			int Atime=Integer.parseInt(convertStartTime[0]);
			int Btime=Integer.parseInt(convertStartTime[1]);
			
			/////EndTime
			int Ctime=Integer.parseInt(convertEndTime[0]);
			int Dtime=Integer.parseInt(convertEndTime[1]);
			
			String[] convertStartDates =fromdate.split("-");
			String[] convertEndDates =enddate.split("-");
			
			/////////Start Date       
			
			if(Integer.parseInt(convertStartDates[1])==1)
			{			
				convertStartDates[1]="12";
				convertStartDates[0]=String.valueOf((Integer.parseInt(convertStartDates[0])-1));
			}
			else
			{
				convertStartDates[1]=String.valueOf((Integer.parseInt(convertStartDates[1])-1));			
			}
			int A=(Integer.parseInt(convertStartDates[1]));
			int B=(Integer.parseInt(convertStartDates[2]));
			int C=(Integer.parseInt(convertStartDates[0]));			
			/////////////End Date		
			if(Integer.parseInt(convertEndDates[1])==1)
			{			
				convertEndDates[1]="12";
				convertEndDates[0]=String.valueOf((Integer.parseInt(convertEndDates[0])-1));
			}
			else
			{
				convertEndDates[1]=String.valueOf((Integer.parseInt(convertEndDates[1])-1));			
			}
			int D=(Integer.parseInt(convertEndDates[1]));
			int E=(Integer.parseInt(convertEndDates[2]));
			int F=(Integer.parseInt(convertEndDates[0]));		
		
			try {
			
				// Start Date is on:
				java.util.Calendar startDate = new GregorianCalendar();
				//startDate.setTimeZone(timezone);
				startDate.set(java.util.Calendar.MONTH, A);
				startDate.set(java.util.Calendar.DAY_OF_MONTH, B);
				startDate.set(java.util.Calendar.YEAR, C);
				startDate.set(java.util.Calendar.HOUR_OF_DAY, Atime);
				startDate.set(java.util.Calendar.MINUTE, Btime);
				startDate.set(java.util.Calendar.SECOND, 0);
				
				// End Date is on:
				java.util.Calendar endDate = new GregorianCalendar();
				//endDate.setTimeZone(timezone);
				endDate.set(java.util.Calendar.MONTH, D);
				endDate.set(java.util.Calendar.DAY_OF_MONTH, E);
				endDate.set(java.util.Calendar.YEAR, F);
				endDate.set(java.util.Calendar.HOUR_OF_DAY, Ctime);
				endDate.set(java.util.Calendar.MINUTE, Dtime);	
				endDate.set(java.util.Calendar.SECOND, 0);
				
				// Create the event props
				String eventName = subject;
				DateTime start = new DateTime(startDate.getTime());
				DateTime end = new DateTime(endDate.getTime());			
				googleStartTime=start.toString();
				googleEndTime=end.toString();
				// Create the event
				VEvent meeting = new VEvent(start, end, eventName);	
				
				// add timezone to vEvent
				//meeting.getProperties().add(tz.getTimeZoneId());
				if(location!=null && location!=""){
					Location loc = new Location(location);
					meeting.getProperties().add(loc);
				}
				
				Description sum = new Description(description);
				meeting.getProperties().add(sum);
				
				// generate unique identifier and add it to vEvent
				UidGenerator ug;
				ug = new UidGenerator("uidGen");
				Uid uid = ug.generateUid();			
				meeting.getProperties().add(uid);
				
				// assign props to calendar object
				icsCalendar.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
				icsCalendar.getProperties().add(CalScale.GREGORIAN);
				
				// Add the event and print
				System.out.println(meeting);
				icsCalendar.getComponents().add(meeting);
				CalendarOutputter outputter = new CalendarOutputter();
				outputter.setValidating(false);
				
				FileOutputStream fout = new FileOutputStream(calFile);
				outputter.output(icsCalendar, fout);
			}
			catch (Exception e)
			{
			e.printStackTrace();	
			}
    }
   return calFile.toString();
 }

}










