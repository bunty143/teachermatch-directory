package tm.services;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.DistrictPortfolioConfig;
import tm.bean.master.DistrictMaster;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.utility.Utility;

public class ManageDSPQConfigAjax {
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
	
	
	public List<DistrictPortfolioConfig> getDSPQConfigs(Integer districtId,Integer jobCategoryId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		System.out.println("districtId::: "+districtId);
		System.out.println("jobCategoryId::: "+jobCategoryId);
		
		DistrictMaster districtMaster = new DistrictMaster();
		districtMaster.setDistrictId(districtId);
		List<DistrictPortfolioConfig> districtPortfolioConfigs = new ArrayList<DistrictPortfolioConfig>();
		districtPortfolioConfigs = districtPortfolioConfigDAO.getPortfolioConfigs(districtMaster);
		for (DistrictPortfolioConfig districtPortfolioConfig : districtPortfolioConfigs) {
			System.out.println("Candidate Type: "+districtPortfolioConfig.getCandidateType());
		}
		
		
		return districtPortfolioConfigs;
	}

}
