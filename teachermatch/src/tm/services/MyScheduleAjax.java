package tm.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.DemoClassAttendees;
import tm.bean.master.DemoClassSchedule;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.master.DemoClassAttendeesDAO;
import tm.dao.master.DemoClassScheduleDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.Utility;

public class MyScheduleAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private DemoClassScheduleDAO demoClassScheduleDAO;
	public void setDemoClassScheduleDAO(DemoClassScheduleDAO demoClassScheduleDAO) {
		this.demoClassScheduleDAO = demoClassScheduleDAO;
	} 
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	@Autowired
	private DemoClassAttendeesDAO demoClassAttendeesDAO;
	public void setDemoClassAttendeesDAO(
			DemoClassAttendeesDAO demoClassAttendeesDAO) {
		this.demoClassAttendeesDAO = demoClassAttendeesDAO;
	}
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	
	public String displayScheduleRecordsByEntityType(boolean resultFlag,int entityID,int districtHiddenId,
			String noOfRow, String pageNo,String sortOrder,String sortOrderType,String fromDate,String toDate,int inviteeflag)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer tmRecords =	new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord = 0;
			//------------------------------------
			UserMaster userSession	=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userSession		=	(UserMaster) session.getAttribute("userMaster");
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			Date fDate=null;
			Date tDate=null;
			boolean dateFlag=false;
			try{
				if(!fromDate.equals("")){
					fDate=Utility.getCurrentDateFormart(fromDate);
					dateFlag=true;
				}
				if(!toDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(toDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					tDate=cal2.getTime();
					dateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			List<DemoClassSchedule> demoClassSchedules=null;
			List<DemoClassSchedule> demoClassSchedulesAll=null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"teacherId";
			/*
			*//**Start set dynamic sorting fieldName **/
			
			Order  sortOrderStrVal=null;
			
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				 {
					 sortOrderFieldName		=	sortOrder;
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			int totalRecord=0;	
			DistrictMaster districtMaster=districtMasterDAO.findById(districtHiddenId, false,false);
			Criterion criterion1=null;
			
			if(fDate!=null && tDate!=null){
				criterion1=Restrictions.between("demoDate",Utility.getCurrentDateFormart(fromDate),Utility.getCurrentDateFormart(toDate));
			}else if(fDate!=null && tDate==null){
				criterion1=Restrictions.ge("demoDate",Utility.getCurrentDateFormart(fromDate));
			}else if(fDate==null && tDate!=null){
				criterion1=Restrictions.le("demoDate",Utility.getCurrentDateFormart(toDate));
			}
			
			Criterion criterion2=Restrictions.eq("districtId",districtMaster);
			Criterion criterion3=Restrictions.ne("demoStatus","Cancelled");
			
			Criterion criterion4=Restrictions.and(criterion2, criterion3);
			Criterion criterion5=Restrictions.and(criterion1,criterion4);
			
			if(inviteeflag==1){
				if(fDate==null && tDate==null){
					demoClassSchedules=demoClassScheduleDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion4);
					demoClassSchedulesAll=demoClassScheduleDAO.findWithAll(sortOrderStrVal,criterion4);
				}else{
					demoClassSchedules=demoClassScheduleDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion5);
					demoClassSchedulesAll=demoClassScheduleDAO.findWithAll(sortOrderStrVal,criterion5);
				}
				totalRecord=demoClassSchedulesAll.size();
			}
			if(inviteeflag==2){
				UserMaster userMaster=userMasterDAO.findById(userSession.getUserId(), false, false);
				demoClassSchedules=demoClassAttendeesDAO.getMyDemoInviteeSchedule(userMaster,criterion1);
			}	
			
			
			if(resultFlag==false){
				totaRecord=0;
			}
			if(totalRecord<end)
				end=demoClassSchedules.size();
			
			
			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCandidateName", locale),sortOrderFieldName,"teacherId",sortOrderTypeVal,1);
			tmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDemoDate", locale),sortOrderFieldName,"demoDate",sortOrderTypeVal,1);
			tmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
		
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblLocti", locale),sortOrderFieldName,"demoClassAddress",sortOrderTypeVal,1);
			tmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderFieldName,"jobOrder",sortOrderTypeVal,1);
			tmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgHost", locale),sortOrderFieldName,"createdBy",sortOrderTypeVal,1);
			tmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			tmRecords.append("<th width='20%' valign='top'>"+Utility.getLocaleValuePropByKey("msgInvitees", locale)+"</th>");
			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
		
			if(resultFlag){
				if(demoClassSchedules.size()==0)
					tmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
				for (DemoClassSchedule democlass : demoClassSchedules) 
				{
					tmRecords.append("<tr>" );
					tmRecords.append("<td valign='top'>"+democlass.getTeacherId().getFirstName()+" "+democlass.getTeacherId().getLastName()+"</td>");
					tmRecords.append("<td valign='top'>"+Utility.convertDateAndTimeToUSformatOnlyDate(democlass.getDemoDate())+"   "+democlass.getDemoTime()+" "+democlass.getTimeFormat()+"</td>");
					tmRecords.append("<td valign='top'>"+democlass.getDemoClassAddress()+"</td>");
					tmRecords.append("<td valign='top'>"+democlass.getJobOrder().getJobTitle()+"</td>");
					tmRecords.append("<td valign='top'>"+democlass.getCreatedBy().getFirstName()+" "+democlass.getCreatedBy().getLastName()+"</td>");
					
					List<DemoClassAttendees>list=demoClassAttendeesDAO.getDemoClassAttendees(democlass);
					StringBuffer strBuffer=new StringBuffer();
					for(DemoClassAttendees classAttendees:list){
						strBuffer.append(classAttendees.getInviteeId().getFirstName()+" "+classAttendees.getInviteeId().getLastName()+", ");
					}
					if(strBuffer.length()>=2)
					strBuffer.deleteCharAt(strBuffer.length()-2); 
					
					tmRecords.append("<td valign='top'>"+strBuffer+"</td>");
					tmRecords.append("</tr>");
				}
			}else{
				/*================= Checking If Record Not Found ======================*/	
				tmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));		

		}catch (Exception e) {
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
	
}


