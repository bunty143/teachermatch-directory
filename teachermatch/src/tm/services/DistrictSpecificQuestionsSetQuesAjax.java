package tm.services;
 
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.QqQuestionsetQuestions;
import tm.bean.user.UserMaster;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.QqQuestionSetsDAO;
import tm.dao.master.QqQuestionsetQuestionsDAO;
import tm.utility.Utility;
 
public class DistrictSpecificQuestionsSetQuesAjax {
	
    String locale = Utility.getValueOfPropByKey("locale");
 
    @Autowired
    private DistrictMasterDAO districtMasterDAO;
    
    @Autowired
    private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;    
    
    @Autowired
    private QqQuestionsetQuestionsDAO qqQuestionsetQuestionsDAO;
    
    @Autowired
    private QqQuestionSetsDAO qqQuestionSetsDAO;
    
    @Autowired
    private HeadQuarterMasterDAO headQuarterMasterDAO;
    
    
    // DIsplay Questions from question pool
    public String displayQuestions(String noOfRow, String pageNo,String sortOrder,String sortOrderType,String quesSetSearchText,String quesSetId,String districtId,String headQuaterId)
    {
        System.out.println(":::::::::::::::::::: displayQuestions :::::::::::::::::::::::");
        /* ========  For Session time Out Error =========*/
        WebContext context;
        context = WebContextFactory.get();
        HttpServletRequest request = context.getHttpServletRequest();
        HttpSession session = request.getSession(false);
        if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
        {
            throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
        }
        StringBuffer dmRecords =    new StringBuffer();
        
        try{
            
            /*============= For Sorting ===========*/
            int noOfRowInPage     =     Integer.parseInt(noOfRow);
            int pgNo             =     Integer.parseInt(pageNo);
            int start             =     ((pgNo-1)*noOfRowInPage);
            int end             =     ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
            int totalRecord        =     0;
            //------------------------------------
            
            UserMaster userMaster=null;
            int roleId=0;
            if (session == null || session.getAttribute("userMaster") == null) 
            {
                return "redirect:index.jsp";
            }else{
                userMaster=    (UserMaster) session.getAttribute("userMaster");
                if(userMaster.getRoleId().getRoleId()!=null){
                    roleId=userMaster.getRoleId().getRoleId();
                }
            }
            
            boolean allData           = false;
            boolean quesTextflag= false;
            
            List<DistrictSpecificQuestions> districtSpecificRefChkQuestions    =    new ArrayList<DistrictSpecificQuestions>();
            List<DistrictSpecificQuestions> filterData                        =    new ArrayList<DistrictSpecificQuestions>();
            List<DistrictSpecificQuestions> finalDatalist                        =    new ArrayList<DistrictSpecificQuestions>();
            List<DistrictSpecificQuestions> i4QuesForQues                        =    new ArrayList<DistrictSpecificQuestions>();
            List<DistrictSpecificQuestions> repeatedQues                        =   new ArrayList<DistrictSpecificQuestions>();
            
            List<QqQuestionsetQuestions>     quesofQuesSetList    =   new ArrayList<QqQuestionsetQuestions>();
            
            /*====== set default sorting fieldName ====== **/
            String sortOrderFieldName    =    "QuestionText";
            /*====== Start set dynamic sorting fieldName **/
            Order  sortOrderStrVal        =    null;
            
            if(!sortOrder.equals("") && !sortOrder.equals(null))
            {
                sortOrderFieldName        =    sortOrder;
            }
            String sortOrderTypeVal="0";
            if(!sortOrderType.equals("") && !sortOrderType.equals(null))
            {
                if(sortOrderType.equals("0"))
                {
                    sortOrderStrVal        =    Order.asc(sortOrderFieldName);
                }
                else
                {
                    sortOrderTypeVal    =    "1";
                    sortOrderStrVal        =    Order.desc(sortOrderFieldName);
                }
            }
            else
            {
                sortOrderTypeVal        =    "0";
                sortOrderStrVal            =    Order.asc(sortOrderFieldName);
            }
            /*=============== End ======================*/
            
            QqQuestionSets qqQuestionSets = qqQuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
            
            DistrictMaster districtMaster = null;
            if(districtId!=null && !districtId.equals(""))
                districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
      
            HeadQuarterMaster headQuarterMaster = null;
            
            if(headQuaterId!=null && !headQuaterId.equals(""))
            	headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuaterId), false, false);
            	
            if(districtMaster!=null && headQuarterMaster==null){
            	districtSpecificRefChkQuestions = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(districtMaster);
	            quesofQuesSetList    = qqQuestionsetQuestionsDAO.findByQuesSetId(qqQuestionSets,districtMaster);
            }
            else if(headQuarterMaster!=null){
            	districtSpecificRefChkQuestions = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYHeadQuater(headQuarterMaster);
            	quesofQuesSetList    = qqQuestionsetQuestionsDAO.findByQuesSetByHeadQuater(qqQuestionSets,headQuarterMaster);
            }
            
            if(quesofQuesSetList!=null){
            	for(QqQuestionsetQuestions i4QSQ:quesofQuesSetList)
            	{
            		repeatedQues.add(i4QSQ.getDistrictSpecificQuestions());
            	}

            	if(quesofQuesSetList.size()>0)
            	{
            		System.out.println("call remove ");
            		districtSpecificRefChkQuestions.removeAll(repeatedQues);
            	}
            }
           
            if(districtSpecificRefChkQuestions!=null && districtSpecificRefChkQuestions.size()>0)
            {
                filterData.addAll(districtSpecificRefChkQuestions);
                allData = true;
            }
            
            if(quesSetSearchText!=null && !quesSetSearchText.equalsIgnoreCase(""))
            {
                i4QuesForQues = districtSpecificQuestionsDAO.findQuesByQues(quesSetSearchText);
                quesTextflag = true;
            }
            System.out.println(quesTextflag);
            if(quesTextflag==true)
                filterData.retainAll(i4QuesForQues);
            else
                filterData.addAll(i4QuesForQues);
            
            totalRecord = filterData.size();
            
            if(totalRecord<end)
                end=totalRecord;
            
            finalDatalist    =    filterData.subList(start,end);
            
            dmRecords.append("<table  id='refChkQuesTable' width='100%' border='0' >");
            dmRecords.append("<thead class='bg'>");
            dmRecords.append("<tr>");
 
            String responseText="";
            responseText=PaginationAndSorting.responseSortingLink("Question",sortOrderFieldName,"QuestionText",sortOrderTypeVal,pgNo);
            dmRecords.append("<th width='55%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
            
            responseText=PaginationAndSorting.responseSortingLink("Created Date",sortOrderFieldName,"DateCreated",sortOrderTypeVal,pgNo);
            dmRecords.append("<th width='15%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
            
            dmRecords.append("<th width='15%'><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</span></th>");
            dmRecords.append("</tr>");
            dmRecords.append("</thead>");
            /*================= Checking If Record Not Found ======================*/
            if(finalDatalist.size()==0)
                dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoQuestionfound1", locale)+"</td></tr>" );
            System.out.println(Utility.getLocaleValuePropByKey("msgNoOfRecords", locale)+districtSpecificRefChkQuestions.size());
 
            for (DistrictSpecificQuestions i4qp: finalDatalist) 
            {
                dmRecords.append("<tr>" );
                dmRecords.append("<td>"+i4qp.getQuestion()+"</td>");
                dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(i4qp.getCreatedDateTime())+"</td>");
                if(userMaster.getEntityType()!=6)
                dmRecords.append("<td><a href='javascript:void(0);' onclick='addQuesFromQPtoQS("+i4qp.getQuestionId()+")'>"+Utility.getLocaleValuePropByKey("lnkAddQues", locale)+"</a>"+
                " | <a href='districtspecificquestions.do?districtId="+districtMaster.getDistrictId()+"&questionId="+i4qp.getQuestionId()+"&quesSetId="+quesSetId+"' >"+Utility.getLocaleValuePropByKey("lnkEditQues", locale)+"</a>" +"</td>");
                dmRecords.append("</tr>");
            }
            dmRecords.append("</table>");
            
            dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
        }catch (Exception e) 
        {
            e.printStackTrace();
        }
        return dmRecords.toString();
    }
    
    
    // Display Questions in Question Set
    public String displayQuesSetQues(String quesSetId,String districtId,String headQuaterId)
    {
        System.out.println(" =========== displayQuesSetQues ========="+quesSetId);
        
        /* ========  For Session time Out Error =========*/
        WebContext context;
        context = WebContextFactory.get();
        HttpServletRequest request = context.getHttpServletRequest();
        HttpSession session = request.getSession(false);
        UserMaster userMaster=null;
		int roleId=0;
        if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
        {
            throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
        }else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
        }
        StringBuffer dmRecords =    new StringBuffer();
        try{
            // QqQuestionsetQuestions ReferenceQuestionSetQuestions  QqQuestionSetsDAO
            List<QqQuestionsetQuestions> qqQuestionsetQuestions = new ArrayList<QqQuestionsetQuestions>(); 
            
            QqQuestionSets qqQuestionSets = qqQuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
            System.out.println("======1 break =============");
            
           DistrictMaster districtMaster = new DistrictMaster();
            if(districtId!=null && !districtId.equals(""))
                districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
           
           HeadQuarterMaster headQuarterMaster = null;
            if(headQuaterId!=null && !headQuaterId.equals(""))
            	headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuaterId), false, false);
            
            System.out.println("====== 2 break =============");
            if(districtMaster!=null && headQuarterMaster==null)
            qqQuestionsetQuestions = qqQuestionsetQuestionsDAO.findByQuesSetId(qqQuestionSets,districtMaster);
            else if(headQuarterMaster!=null)
            qqQuestionsetQuestions = qqQuestionsetQuestionsDAO.findByQuesSetByHeadQuater(qqQuestionSets,headQuarterMaster);
            System.out.println("======3 break =============");
            dmRecords.append("<table  id='refChkQuesSetQuesTable' width='100%' border='0' >");
            dmRecords.append("<thead class='bg'>");
            dmRecords.append("<tr>");
 
            dmRecords.append("<th width='55%' valign='top'><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblQues", locale)+"</span></th>");
 
            dmRecords.append("<th width='15%'><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</span></th>");
            dmRecords.append("</tr>");
            dmRecords.append("</thead>");
 
            /*================= Checking If Record Not Found ======================*/
            if(qqQuestionsetQuestions==null || qqQuestionsetQuestions.size()==0)
                dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoQuestionfound1", locale)+"</td></tr>" );
            //System.out.println(Utility.getLocaleValuePropByKey("msgNoOfRecords", locale)+qqQuestionsetQuestions.size());
            if(qqQuestionsetQuestions!=null){
            	for (int i=0; i<qqQuestionsetQuestions.size();i++ )
            	{
            		String upButton ="";
            		String downButton ="";

            		System.out.println(" ");

            		if(i==0)
            			upButton = "Up";
            		else
            			upButton = "<a href='javascript:void(0);' disabled='disabled' onclick='moveUpQues("+qqQuestionsetQuestions.get(i).getID()+");'>"+Utility.getLocaleValuePropByKey("msgUp3", locale)+"</a>";

            		if(i==qqQuestionsetQuestions.size()-1)
            			downButton = "Down";
            		else
            			downButton = "<a href='javascript:void(0);' onclick='moveDownQues("+qqQuestionsetQuestions.get(i).getID()+");'>"+Utility.getLocaleValuePropByKey("msgDown3", locale)+"</a>";

            		dmRecords.append("<tr>" );
            		dmRecords.append("<td>"+qqQuestionsetQuestions.get(i).getDistrictSpecificQuestions().getQuestion()+"</td>");
            		dmRecords.append("<td>");
            		if(userMaster.getEntityType()!=6){
            		dmRecords.append(upButton+" | "+downButton+" | <a href='javascript:void(0);' onclick='deleteQuesFromQuesSet("+qqQuestionsetQuestions.get(i).getID()+");'>"+Utility.getLocaleValuePropByKey("lnkRemo", locale)+"</a>");
            		dmRecords.append(" | <a href='districtspecificquestions.do?districtId="+districtMaster.getDistrictId()+"&questionId="+qqQuestionsetQuestions.get(i).getDistrictSpecificQuestions().getQuestionId()+"&quesSetId="+quesSetId+"' >"+Utility.getLocaleValuePropByKey("lnkEditQues", locale)+"</a>");            		
            		dmRecords.append("</td>");
            		dmRecords.append("</tr>");
            		}
            		}
            }
            dmRecords.append("</table>");
        }catch (Exception e) 
        {
            e.printStackTrace();
        }
        return dmRecords.toString();
    }
    
    public int addQuesFromQPtoQS(String quesId,String quesSetId,String districtId,String headQuarterId)
    {
        System.out.println(" addQuesFromQPtoQS ");
        
        /* ========  For Session time Out Error =========*/
        WebContext context;
        context = WebContextFactory.get();
        HttpServletRequest request = context.getHttpServletRequest();
        HttpSession session = request.getSession(false);
        if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
        {
            throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
        }
        
        try
        {
            DistrictSpecificQuestions districtSpecificRefChkQuestions = districtSpecificQuestionsDAO.findById(Integer.parseInt(quesId), false, false);
            QqQuestionSets referenceQuestionSets = qqQuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
            DistrictMaster districtMaster = new DistrictMaster();
            if(districtId!=null && !districtId.equals(""))
                districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
            
            HeadQuarterMaster headQuarterMaster = null;
            if(headQuarterId!=null && !headQuarterId.equals(""))
            	headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
 
            QqQuestionsetQuestions referenceQuestionSetQuestions = new QqQuestionsetQuestions();
            int quesSequence =0;
            try
            {
                List<QqQuestionsetQuestions> existQuesList = new ArrayList<QqQuestionsetQuestions>();
                if(districtMaster!=null && headQuarterMaster==null)
                existQuesList = qqQuestionsetQuestionsDAO.findByQuesSetId(referenceQuestionSets,districtMaster);
                else if(headQuarterMaster!=null)
                existQuesList = qqQuestionsetQuestionsDAO.findByQuesSetByHeadQuater(referenceQuestionSets,headQuarterMaster);
                if(existQuesList!=null){
                	if(existQuesList.size()==0)
                	{
                		quesSequence=1;
                	}
                	else
                	{
                		if(existQuesList.size()>0)
                		{
                			quesSequence = existQuesList.get(existQuesList.size()-1).getQuestionSequence()+1; 
                		}
                	}
                }
                referenceQuestionSetQuestions.setQuestionSequence(quesSequence);
                
            }catch(Exception e)
            {
                e.printStackTrace();
            }
            
            referenceQuestionSetQuestions.setDistrictSpecificQuestions(districtSpecificRefChkQuestions);
            referenceQuestionSetQuestions.setQuestionSets(referenceQuestionSets);
            
            qqQuestionsetQuestionsDAO.makePersistent(referenceQuestionSetQuestions);
        
            return 1;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return 0;
    }
    
    //Delete Question from Question Set And display in Question Pool
    public int deleteQuesFromQuesSet(String quesSetQuesID)
    {
         /*========  For Session time Out Error =========*/
        WebContext context;
        context = WebContextFactory.get();
        HttpServletRequest request = context.getHttpServletRequest();
        HttpSession session = request.getSession(false);
        if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
        {
            throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
        }
        
        try
        {
            QqQuestionsetQuestions qqQuestionsetQuestions = new QqQuestionsetQuestions();
 
            qqQuestionsetQuestions = qqQuestionsetQuestionsDAO.findById(Integer.parseInt(quesSetQuesID), false, false);
            
            qqQuestionsetQuestionsDAO.makeTransient(qqQuestionsetQuestions);
            
            return 1;
            
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return 0;
    }
    
    // Question Move Up in question set
    public int moveUpQues(String quesSetQuesID,String districtId,String headQuarterId)
    {
         /*========  For Session time Out Error =========*/
        WebContext context;
        context = WebContextFactory.get();
        HttpServletRequest request = context.getHttpServletRequest();
        HttpSession session = request.getSession(false);
        if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
        {
            throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
        }
        
        try
        {
            QqQuestionsetQuestions forMoveQues = new QqQuestionsetQuestions();
            
            if(quesSetQuesID!=null && !quesSetQuesID.equals(""))
                forMoveQues = qqQuestionsetQuestionsDAO.findById(Integer.parseInt(quesSetQuesID), false, false);
                    
            QqQuestionSets qqQuestionSets = qqQuestionSetsDAO.findById(forMoveQues.getQuestionSets().getID(), false, false);
            
            DistrictMaster districtMaster = new DistrictMaster();
            if(districtId!=null && !districtId.equals(""))
                districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
            
            HeadQuarterMaster headQuarterMaster = null;
            if(headQuarterId!=null && !headQuarterId.equals(""))
            	headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
            List<QqQuestionsetQuestions> existQuesList = new ArrayList<QqQuestionsetQuestions>();
            List<QqQuestionsetQuestions> updatedObjList = new ArrayList<QqQuestionsetQuestions>();
            if(districtMaster!=null && headQuarterMaster==null)
            existQuesList = qqQuestionsetQuestionsDAO.findByQuesSetId(qqQuestionSets,districtMaster);
           else if(headQuarterMaster!=null) 
            existQuesList = qqQuestionsetQuestionsDAO.findByQuesSetByHeadQuater(qqQuestionSets,headQuarterMaster);
            
            QqQuestionsetQuestions first = new QqQuestionsetQuestions();
            QqQuestionsetQuestions second = new QqQuestionsetQuestions();
            
            int first_QS = 0;
            int second_QS = 0;
            
            if(existQuesList!=null && existQuesList.size()>0){
            	for(int i=existQuesList.size()-1; i>0;i--)
                {
                   // System.out.println(existQuesList.get(i).getID()+" -----"+qqQuestionSets.getID()+" ====================== >>>>>>>>>>>>>>> "+existQuesList.get(i).getID().equals(qqQuestionSets.getID()));
                    
                    if(existQuesList.get(i).getID().equals(forMoveQues.getID()))
                    {
                        first = existQuesList.get(i);
                        second = existQuesList.get(i-1);
                        break;
                    }
                }
            }
            first_QS = first.getQuestionSequence();
            second_QS = second.getQuestionSequence();
            
            //System.out.println("Before ::::::: first :: "+first.getQuestionSequence()+" second :: "+second.getQuestionSequence());
            
            first.setQuestionSequence(second_QS);
            second.setQuestionSequence(first_QS);
            
            //System.out.println("After ::::::: first :: "+first.getQuestionSequence()+" second :: "+second.getQuestionSequence());
            
            updatedObjList.add(first);
            updatedObjList.add(second);
            
            for(QqQuestionsetQuestions I4QSQ:updatedObjList)
            {
                qqQuestionsetQuestionsDAO.makePersistent(I4QSQ);
            }
            
            return 1;
            
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return 0;
    }
    
    // Question Move down in question set 
    public int moveDownQues(String quesSetQuesID, String districtId, String headQuarterId)
    {
        
         /*========  For Session time Out Error =========*/
        WebContext context;
        context = WebContextFactory.get();
        HttpServletRequest request = context.getHttpServletRequest();
        HttpSession session = request.getSession(false);
        if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
        {
            throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
        }
        
        try
        {
            QqQuestionsetQuestions forMoveQues = new QqQuestionsetQuestions();
            
            if(quesSetQuesID!=null && !quesSetQuesID.equals(""))
                forMoveQues = qqQuestionsetQuestionsDAO.findById(Integer.parseInt(quesSetQuesID), false, false);
                    
            QqQuestionSets qqQuestionSets = qqQuestionSetsDAO.findById(forMoveQues.getQuestionSets().getID(), false, false);
            
            DistrictMaster districtMaster = new DistrictMaster();
           if(districtId!=null && !districtId.equals(""))
                districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
           
            HeadQuarterMaster headQuarterMaster = null;
            if(headQuarterId!=null && !headQuarterId.equals(""))
            	headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
            
            
            List<QqQuestionsetQuestions> existQuesList = new ArrayList<QqQuestionsetQuestions>();
            List<QqQuestionsetQuestions> updatedObjList = new ArrayList<QqQuestionsetQuestions>();
            
             if(districtMaster!=null && headQuarterMaster==null)
            existQuesList = qqQuestionsetQuestionsDAO.findByQuesSetId(qqQuestionSets,districtMaster);
            else  if(headQuarterMaster!=null)
            existQuesList = qqQuestionsetQuestionsDAO.findByQuesSetByHeadQuater(qqQuestionSets,headQuarterMaster);
            
            QqQuestionsetQuestions first = new QqQuestionsetQuestions();
            QqQuestionsetQuestions second = new QqQuestionsetQuestions();
            
            int first_QS = 0;
            int second_QS = 0;
            if(existQuesList!=null && existQuesList.size()>0){
            	for(int i=0;i<existQuesList.size()-1;i++)
                {
                    if(existQuesList.get(i).getID().equals(forMoveQues.getID()))
                    {
                        first = existQuesList.get(i);
                        second = existQuesList.get(i+1);
                        break;
                    }
                }
            }
            
            first_QS = first.getQuestionSequence();
            second_QS = second.getQuestionSequence();
            
            System.out.println("Before ::::::: first :: "+first.getQuestionSequence()+" second :: "+second.getQuestionSequence());
            
            first.setQuestionSequence(second_QS);
            second.setQuestionSequence(first_QS);
            
            System.out.println("After ::::::: first :: "+first.getQuestionSequence()+" second :: "+second.getQuestionSequence());
            
            updatedObjList.add(first);
            updatedObjList.add(second);
            
            for(QqQuestionsetQuestions I4QSQ:updatedObjList)
            {
                qqQuestionsetQuestionsDAO.makePersistent(I4QSQ);
            }
            
            return 1;
            
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return 0;
    }
    
 
}
