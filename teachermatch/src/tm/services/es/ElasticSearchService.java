package tm.services.es;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobCertification;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.services.PaginationAndSorting;
import tm.utility.ElasticSearchConfig;
import tm.utility.Utility;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class ElasticSearchService {
	
	String locale = Utility.getValueOfPropByKey("locale");
	String kellyUrlValue = Utility.getLocaleValuePropByKey("kellyUrlValue", locale);

	Map<Integer, String> indexMap=new HashMap<Integer, String>();
	Map<Integer, String> indexMapForDistrictJobBoard=new HashMap<Integer, String>();
	Map<Integer, String> indexMapForDistrictJobOrder=new HashMap<Integer, String>();
	Map<Integer, String> indexMapForJobsOfInterestNotCandidate=new HashMap<Integer, String>();
	Map<Integer, String> indexMapForJobsOfInterestForCandidate=new HashMap<Integer, String>();
	Map<Integer, String> indexMapForSchoolJobOrder=new HashMap<Integer, String>();
	private String questJobBoardSQL="SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,"
			+ "dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId AS disStateId,"
			+ "IFNULL(scm.schoolId,0) AS schoolId,scm.schoolName,scm.zip,scm.address AS schooladdress,scm.stateId AS schStateId,"
			+ "ctm.certType,stm.stateName,ctm.certTypeId,jo.createdDateTime,geo.geoZoneId,geo.geoZoneName "
			+ "FROM joborder jo  "
			+ "LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId "
			+ "LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId "
			+ "LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId "
			+ "LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId "
			+ "LEFT JOIN jobcertification jc ON jc.jobid=jo.jobid "
			+ "LEFT JOIN certificatetypemaster ctm ON jc.certtypeid=ctm.certtypeid "
			+ "LEFT JOIN statemaster stm ON stm.stateId=dm.stateId "
			+ "left join geozonemaster geo on geo.geoZoneId=jo.geoZoneId "
			+ "WHERE jo.status='a' AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 "
			+ "AND jo.isInviteOnly!=TRUE";
	private String questJobBoardStrSQL="SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,"
			+ "dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId AS disStateId,"
			+ "IFNULL(scm.schoolId,0) AS schoolId,scm.schoolName,IFNULL(scm.zip,0) as zip,scm.address AS schooladdress,IFNULL(scm.stateId,0) AS schStateId,"
			+ "ctm.certType,stm.stateName,IFNULL(ctm.certTypeId,0) as certTypeId,jo.createdDateTime,geo.geoZoneId,geo.geoZoneName "
			+ " FROM joborder jo  "
			+ "LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId "
			+ "LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId "
			+ "LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId "
			+ "LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId "
			+ "LEFT JOIN jobcertification jc ON jc.jobid=jo.jobid "
			+ "LEFT JOIN certificatetypemaster ctm ON jc.certtypeid=ctm.certtypeid "
			+ "LEFT JOIN statemaster stm ON stm.stateId=dm.stateId "
			+ " left join geozonemaster geo on geo.geoZoneId=jo.geoZoneId "
			+ " WHERE jo.status='a' "
			+ "AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 "
			+ "AND jo.isInviteOnly!=TRUE  limit 0,0";

	String districtJobOrderSQL="SELECT jo.jobId,jo.isPoolJob,jo.jobTitle,jo.status,jo.jobEndDate,jo.districtAttachment,jo.IsInviteOnly,jo.requisitionNumber,"
			+ "dm.districtId,dm.districtName,"
			+ "sm.subjectName,"
			+ "geo.geoZoneName,geo.geoZoneId,"
			+ "geodm.districtId as geoZoneDistrictId,"
			+ "scm.schoolId,scm.schoolName  "
			+ "FROM joborder jo  "
			+ "LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId "
			+ "LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId "
			+ "LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId "
			+ "LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId "
			+ "LEFT JOIN statemaster stm ON stm.stateId=dm.stateId  "
			+ "left join geozonemaster geo on geo.geoZoneId=jo.geoZoneId "
			+ "left join districtmaster geodm on geo.districtid= geodm.districtId";
	
	String jobsOfInterestSQL="SELECT jo.jobId,jo.ipAddress,jo.createdForEntity,jo.isPoolJob,jo.jobTitle,jo.status,jo.jobEndDate,"
			+ "dm.districtId,dm.districtName,dm.displayName,dm.cityname disCityName,dm.zipCode,dm.address AS distaddress,dm.stateId AS disStateId,"
			+ "sm.subjectName,stm.statename as disStateName,"
			+ "geo.geoZoneName,geo.geoZoneId,"
			+ "geodm.districtId as geoZoneDistrictId,"
			+ "scm.schoolId,scm.schoolName,scm.address as schoolAddress,scm.zip as schoolZip,scm.cityName as schoolCityName,sstm.stateName  as schoolStateName  "
			+ "FROM joborder jo  "
			+ "LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId "
			+ "LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId "
			+ "LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId "
			+ "left join statemaster sstm on sstm.stateId=scm.stateId "
			+ "LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId "
			+ "LEFT JOIN statemaster stm ON stm.stateId=dm.stateId  "
			+ "left join geozonemaster geo on geo.geoZoneId=jo.geoZoneId "
			+ "left join districtmaster geodm on geo.districtid= geodm.districtId";
	
	String schoolJobOrderSQL="SELECT jo.isPoolJob,jo.jobTitle,jo.status,jo.jobEndDate,jo.districtAttachment,jo.IsInviteOnly,jo.requisitionNumber,"
			+ "dm.districtId,dm.districtName,"
			+ "sm.subjectName,"
			+ "geo.geoZoneName,geo.geoZoneId,"
			+ "geodm.districtId as geoZoneDistrictId,"
			+ "scm.schoolId,scm.schoolName  "
			+ "FROM joborder jo  "
			+ "LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId "
			+ "LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId "
			+ "LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId "
			+ "LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId "
			+ "LEFT JOIN statemaster stm ON stm.stateId=dm.stateId  "
			+ "left join geozonemaster geo on geo.geoZoneId=jo.geoZoneId "
			+ "left join districtmaster geodm on geo.districtid= geodm.districtId";
	

	public void delete(String index,String type,String id)
	{
		ClientConfig config = new DefaultClientConfig();
	    Client client = Client.create(config);
	    URI uri=null;
	    try 
	    {
			uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+index+"/"+type+"/"+id);
	    	
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED)
	        .delete(ClientResponse.class);
	}
	public void deletePlatformNC(String index,String type,String id)
	{
		ClientConfig config = new DefaultClientConfig();
	    Client client = Client.create(config);
	    URI uri=null;
	    try 
	    {
			uri=new URI(ElasticSearchConfig.sElasticSearchURLPlatform+"/"+index+"/"+type+"/"+id);
	    	
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED)
	        .delete(ClientResponse.class);
	}

	public void update(String index,String type,String id,String json)
	{
		ClientConfig config = new DefaultClientConfig();
	    Client client = Client.create(config);
	    URI uri=null;
	    try {
	    	uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+index+"/"+type+"/"+id+"/?pretty");
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED)
	        .put(ClientResponse.class, json);
	 }
	public void updatePlatformNC(String index,String type,String id,String json)
	{
		ClientConfig config = new DefaultClientConfig();
	    Client client = Client.create(config);
	    URI uri=null;
	    try {
	    	uri=new URI(ElasticSearchConfig.sElasticSearchURLPlatform+"/"+index+"/"+type+"/"+id+"/?pretty");
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED)
	        .put(ClientResponse.class, json);
	 }


	public void searchDocumentByJobid(int jobid)
	{
		ClientConfig config = new DefaultClientConfig();
		URI uri=null;
	    Client client = Client.create(config);
	    try {
			uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/document/_search?pretty");
	    	
		} catch (URISyntaxException e) {
			
			e.printStackTrace();
		}
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;
	    String input = "{\"query\":{\"bool\": " +
	    			"{ \"must\" : [    " +
	    			"{ \"match\": {\"jobid\":"+jobid+"} },{\"match_phrase\" : { \"serverName\" : \""+ElasticSearchConfig.serverName+"\" }}" +
	    					"]}},\"size\":30}";
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
	    //System.out.println("Form response11 " + response.getEntity(String.class));
	    String s=response.getEntity(String.class);
	   // System.out.println(s);
	    JSONObject jsonObject = new JSONObject();
		try {
			jsonObject=jsonObject.fromObject(s);
			JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
			System.out.println(hits.size());
			for(int i=0; i<hits.size(); i++)
			 {
				jsonObject=(JSONObject)hits.get(i);
				String index=(String)jsonObject.get("_index");
				String type=(String)jsonObject.get("_type");
				String id=(String)jsonObject.get("_id");
				jsonObject=(JSONObject)jsonObject.get("_source");
				delete(index, type, id);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	   

	}


	public void createJobOrderInElastic(JobOrder jobOrder,List<JobCertification> lstJobCertification,int create)
	{
		
		try
		{

					List<SchoolMaster> list=jobOrder.getSchool();
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
					ArrayList<String> dbJsonList=new ArrayList<String>();
					JSONObject obj=new JSONObject();
					
					//for deleting
					//if job is creating first time then no need to delete
					if(create==1)
					searchDocumentByJobid(jobOrder.getJobId());
					
					obj.put("jobid", jobOrder.getJobId());
					obj.put("serverName", ElasticSearchConfig.serverName);
					if(jobOrder.getDistrictMaster().getLatitude()!=null)
						obj.put("latitude", jobOrder.getDistrictMaster().getLatitude());
					else
						obj.put("latitude", "");
					if(jobOrder.getDistrictMaster().getLongitude()!=null)
						obj.put("longitude", jobOrder.getDistrictMaster().getLongitude());
					else
						obj.put("longitude", "");
					if(jobOrder.getDistrictMaster().getStateId()!=null 
							&& jobOrder.getDistrictMaster().getStateId().getCountryMaster()!=null
							&& jobOrder.getDistrictMaster().getStateId().getCountryMaster().getName()!=null)
						obj.put("countryName", jobOrder.getDistrictMaster().getStateId().getCountryMaster().getName());
					else
						obj.put("countryName", "");
					obj.put("districtid", jobOrder.getDistrictMaster().getDistrictId());
					obj.put("jobDescription", jobOrder.getJobDescription());
					obj.put("jobtitle", jobOrder.getJobTitle());
					obj.put("jobstartdate", sdf.format(jobOrder.getJobStartDate())+"T00:00:00.000+05:30");
					obj.put("jobenddate", sdf.format(jobOrder.getJobEndDate())+"T00:00:00.000+05:30");
					if(jobOrder.getSubjectMaster()!=null)
					{
						obj.put("subjectName", jobOrder.getSubjectMaster().getSubjectName());
						obj.put("subjectId", jobOrder.getSubjectMaster().getMasterSubjectId());
					}
					else
					{
						obj.put("subjectName", "");
						obj.put("subjectId", "0");
					}
					if(jobOrder.getGeoZoneMaster()!=null)
					{
						obj.put("geoZoneId", jobOrder.getGeoZoneMaster().getGeoZoneId());
						obj.put("geoZoneName", jobOrder.getGeoZoneMaster().getGeoZoneName());
						
					}
					else
					{
						obj.put("geoZoneId", 0);
						obj.put("geoZoneName", "");
					}
					
					obj.put("districtName", jobOrder.getDistrictMaster().getDistrictName());
					obj.put("cityname", jobOrder.getDistrictMaster().getCityName());
					obj.put("zipCode", jobOrder.getDistrictMaster().getZipCode());
					obj.put("distaddress", jobOrder.getDistrictMaster().getAddress());
					obj.put("stateName", jobOrder.getDistrictMaster().getStateId().getStateName());
					String createdDateTime=null;
					if(jobOrder.getCreatedDateTime()==null)
					{
						createdDateTime=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
					}
					else
					{
						createdDateTime=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(jobOrder.getCreatedDateTime());
					}
					obj.put("createdDateTime", createdDateTime.split(" ")[0]+"T"+createdDateTime.split(" ")[1].subSequence(0, 8)+".000+05:30");
					if(jobOrder.getStatus().equalsIgnoreCase("A") && jobOrder.getIsInviteOnly()!=null && !jobOrder.getIsInviteOnly() && jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive()==1 
							&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
							&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
							)
					{
						JSONArray schoolName = new JSONArray();
						JSONArray zip = new JSONArray();
						JSONArray schoolAddress = new JSONArray();
						JSONArray certType = new JSONArray();
						for(SchoolMaster school:list)
						{
							schoolName.add(school.getSchoolName());
							zip.add(school.getZip());
							schoolAddress.add(school.getAddress());
						}
						for(JobCertification certification:lstJobCertification)
						{
							
							obj.put("certType", certification.getCertificateTypeMaster().getCertType());
							certType.add(certification.getCertificateTypeMaster().getCertType()+" ["+
									certification.getCertificateTypeMaster().getStateId().getStateName()+"]");
							
						}
						obj.put("schoolName", schoolName);
						obj.put("zip", zip);
						obj.put("schoolAddress", schoolAddress);
						obj.put("certType", certType);
						//System.out.println("json String to insert==");
						//System.out.println(obj.toString());
						//update("document","jdbc",String.valueOf(jobOrder.getJobId())+"_"+ElasticSearchConfig.serverName,obj.toString());
						update("document","jdbc",String.valueOf(jobOrder.getJobId()),obj.toString());
						

					}
					

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

	
	
	public Map<Integer, String> searchByDocument(String jobtilte)
	{
		System.out.println("Calling searchByDocument");
		Map<Integer, String> map=new HashMap<Integer, String>();
		indexMap=new HashMap<Integer, String>();
		ClientConfig config = new DefaultClientConfig();
		URI uri=null;
	    Client client = Client.create(config);
	    try {
			uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/document/_search?pretty");
		} catch (URISyntaxException e) {
			
			e.printStackTrace();
		}
	    
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;
	   // String input = "{\"query\": { \"match_all\": {} },\"size\":100000}";
	    String input="{\"query\": " +
	    		"{\"bool\": " +
	    			"{ \"must\" : [   " +
	    			"{\"match_phrase\" : { \"serverName\" : \""+ElasticSearchConfig.serverName+"\" }}" +
	    			"]}},\"size\":100000}";
	    
	    //input.append("   {\"match_phrase\" : { \"branchId\" : "+iBranchId+" }},");
	    
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
	    String s=response.getEntity(String.class);
	    JSONObject jsonObject=new JSONObject();
	    jsonObject=jsonObject.fromObject(s);
	    
		try {
			
			JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
			 for(int i=0; i<hits.size(); i++)
			 {
				jsonObject=(JSONObject)hits.get(i);
				String index=(String)jsonObject.get("_index");
				String type=(String)jsonObject.get("_type");
				String id=(String)jsonObject.get("_id");
				jsonObject=(JSONObject)jsonObject.get("_source");
				map.put(Integer.parseInt(jsonObject.get("jobid").toString()), jsonObject.toString());
				indexMap.put(Integer.parseInt(jsonObject.get("jobid").toString()), index+":"+type+":"+id);
			}
			


		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("document size="+map.size());
		
		return map;
		
	}
	public Map<Integer, String> searchByDocumentPlatformNC(String jobtilte)
	{
		System.out.println("Calling searchByDocumentPlatformNC");
		Map<Integer, String> map=new HashMap<Integer, String>();
		indexMap=new HashMap<Integer, String>();
		ClientConfig config = new DefaultClientConfig();
		URI uri=null;
	    Client client = Client.create(config);
	    try {
			uri=new URI(ElasticSearchConfig.sElasticSearchURLPlatform+"/document/_search?pretty");
		} catch (URISyntaxException e) {
			
			e.printStackTrace();
		}
	    
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;
	   // String input = "{\"query\": { \"match_all\": {} },\"size\":100000}";
	    String input="{\"query\": " +
	    		"{\"bool\": " +
	    			"{ \"must\" : [   " +
	    			"{\"match_phrase\" : { \"serverName\" : \""+ElasticSearchConfig.serverName+"\" }}" +
	    			"]}},\"size\":100000}";
	    
	    //input.append("   {\"match_phrase\" : { \"branchId\" : "+iBranchId+" }},");
	    
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
	    String s=response.getEntity(String.class);
	    JSONObject jsonObject=new JSONObject();
	    jsonObject=jsonObject.fromObject(s);
	    
		try {
			
			JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
			 for(int i=0; i<hits.size(); i++)
			 {
				jsonObject=(JSONObject)hits.get(i);
				String index=(String)jsonObject.get("_index");
				String type=(String)jsonObject.get("_type");
				String id=(String)jsonObject.get("_id");
				jsonObject=(JSONObject)jsonObject.get("_source");
				map.put(Integer.parseInt(jsonObject.get("jobid").toString()), jsonObject.toString());
				indexMap.put(Integer.parseInt(jsonObject.get("jobid").toString()), index+":"+type+":"+id);
			}
			


		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("document size="+map.size());
		
		return map;
		
	}
	
	
	public Map<Integer, String> getDatabaseData()
	{
		
		System.out.println("Calling getDatabaseData");
		
		Map<Integer, String> map=new HashMap<Integer, String>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(ElasticSearchConfig.sElasticSearchDBURL,ElasticSearchConfig.sElasticSearchDBUser,ElasticSearchConfig.sElasticSearchDBPass);
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
		    String dateWithoutTime = sdf.format(new Date());
			String sql="SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,"
					+ "dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId AS disStateId,dm.latitude,dm.longitude,"
					+ "IFNULL(scm.schoolId,0) AS schoolId,scm.schoolName,scm.zip,scm.address AS schooladdress,scm.stateId AS schStateId,"
					+ "ctm.certType,stm.stateName,ctm.certTypeId,jo.createdDateTime,geo.geoZoneId,geo.geoZoneName,sm.mastersubjectid,state.stateName,name as countryName     "
					+ "FROM joborder jo  "
					+ "LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId "
					+ "LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId "
					+ "LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId "
					+ "LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId "
					+ "LEFT JOIN jobcertification jc ON jc.jobid=jo.jobid "
					+ "LEFT JOIN certificatetypemaster ctm ON jc.certtypeid=ctm.certtypeid "
					+ "LEFT JOIN statemaster state ON ctm.stateid=state.stateid "
					+ "LEFT JOIN statemaster stm ON stm.stateId=dm.stateId " 
					+ "left join countrymaster cm on cm.countryId=stm.countryId "
					+ "left join geozonemaster geo on geo.geoZoneId=jo.geoZoneId "
					+ "WHERE jo.status='a' AND jo.jobStartDate<='"+dateWithoutTime+"' AND jo.jobEndDate>='"+dateWithoutTime+"' AND jo.approvalBeforeGoLive=1 "
					+ "AND jo.isInviteOnly!=TRUE order by jo.jobid ";
			//System.out.println(sql);
			PreparedStatement psmt=con.prepareStatement(sql);
			ResultSet rset=psmt.executeQuery();
			int jobid=0;
			int i=0;
			while(rset.next())
			{
				
				int j=0;
				jobid=rset.getInt("jo.jobid");
				
				JSONObject obj=new JSONObject();
				obj.put("jobid", rset.getInt("jo.jobid"));
				obj.put("serverName", ElasticSearchConfig.serverName);
				if(rset.getString("dm.latitude")!=null)
					obj.put("latitude", rset.getString("dm.latitude"));
				else
					obj.put("latitude", "");
				if(rset.getString("dm.longitude")!=null)
					obj.put("longitude", rset.getString("dm.longitude"));
				else
					obj.put("longitude", "");
				if(rset.getString("countryName")!=null)
					obj.put("countryName", rset.getString("countryName"));
				else
					obj.put("countryName", "");
				
				if(rset.getString("jobDescription")!=null)
					obj.put("jobDescription", rset.getString("jobDescription"));
				else
					obj.put("jobDescription", "");
				obj.put("districtid", rset.getInt("jo.districtid"));
				if(rset.getString("jo.jobtitle")!=null)
					obj.put("jobtitle", rset.getString("jo.jobtitle"));
				else
					obj.put("jobtitle", "");
				obj.put("jobstartdate", rset.getString("jo.jobstartdate")+"T00:00:00.000+05:30");
				obj.put("jobenddate", rset.getString("jo.jobenddate")+"T00:00:00.000+05:30");
				if(rset.getString("sm.subjectName")!=null)
					obj.put("subjectName", rset.getString("sm.subjectName")+"");
				else
					obj.put("subjectName", "");
				if(rset.getString("dm.districtName")!=null)
					obj.put("districtName", rset.getString("dm.districtName"));
				else
					obj.put("districtName", "");
				if(rset.getString("dm.cityname")!=null)
					obj.put("cityname", rset.getString("dm.cityname"));
				else
					obj.put("cityname", "");
				if(rset.getString("dm.zipCode")!=null)
					obj.put("zipCode", rset.getString("dm.zipCode"));
				else
					obj.put("zipCode", "");
				if(rset.getString("distaddress")!=null)
					obj.put("distaddress", rset.getString("distaddress"));
				else
					obj.put("distaddress", "");
				obj.put("geoZoneId", rset.getInt("geo.geoZoneId"));
				if(rset.getString("stm.stateName")!=null)
					obj.put("stateName", rset.getString("stm.stateName"));
				else
					obj.put("stateName", "");
				if(rset.getInt("sm.mastersubjectid")!=0)
					obj.put("subjectId", rset.getString("sm.mastersubjectid"));
				else
					obj.put("subjectId", "0");
				obj.put("createdDateTime", rset.getString("jo.createdDateTime").split(" ")[0]+"T"+rset.getString("jo.createdDateTime").split(" ")[1].subSequence(0, 8)+".000+05:30");
				if(rset.getString("geo.geoZoneName")!=null)
					obj.put("geoZoneName", rset.getString("geo.geoZoneName"));
				else
					obj.put("geoZoneName", "");
				JSONArray schoolName = new JSONArray();
				JSONArray zip = new JSONArray();
				JSONArray schoolAddress = new JSONArray();
				JSONArray certType = new JSONArray();
				while(jobid==rset.getInt("jo.jobid"))
				{
					jobid=rset.getInt("jo.jobid");
					
					if(!schoolName.contains(rset.getString("scm.schoolName")))
						schoolName.add(rset.getString("scm.schoolName"));
					if(!zip.contains(rset.getString("scm.schoolName")))
						zip.add(rset.getString("scm.zip"));
					if(!schoolAddress.contains(rset.getString("scm.schoolName")))
						schoolAddress.add(rset.getString("schoolAddress"));
					if(!certType.contains(rset.getString("scm.schoolName")))
					{
						certType.add(rset.getString("certType")+" ["+rset.getString("state.stateName")+"]");
						
					}
					if(!rset.isLast())
					{
						rset.next();
					}
					else
						break;
					
							
					
				}
				obj.put("schoolName", schoolName);
				obj.put("zip", zip);
				obj.put("schoolAddress", schoolAddress);
				obj.put("certType", certType);
				if(!rset.isLast() )
					rset.previous();
				if(rset.isLast() && i!=1)
				{
					i=1;
					jobid=rset.getInt("jobid");
					rset.previous();
					if(!(rset.getInt("jobid")==jobid))
					{
						map.put(rset.getInt("jo.jobid"), obj.toString());
						
					}
					
					else
					{
						map.put(rset.getInt("jo.jobid"), obj.toString());
						break;
					}
				}
				
				map.put(rset.getInt("jo.jobid"), obj.toString());
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return map;
	}
	
	
	
	
	
	
	public void compareData(Map<Integer, String> databaseMap,Map<Integer, String> elasticMap)
	{
		System.out.println("Calling compareDataaaaaaaaaa");
		int x=0;
		int y=0;
		int z=0;
		Set<Integer> databaseSet=databaseMap.keySet();
		for(Integer i:databaseSet)
		{
			
			if(elasticMap.get(i)==null)  //if data is not present in elasticsearch then insert
			{
				x++;
				//update("document","jdbc",i.toString()+"_"+ElasticSearchConfig.serverName,databaseMap.get(i));
				//2811
				update("document","jdbc",i.toString(),databaseMap.get(i));
			}
			
			else if(!(elasticMap.get(i)).equals(databaseMap.get(i)))  //if data is present then update
			{
				y++;
				//System.out.println("***************updating*****************");
				update(indexMap.get(i).split(":")[0],indexMap.get(i).split(":")[1],indexMap.get(i).split(":")[2],databaseMap.get(i));
					
				
				
			}
			
			
			
			
		}
		int m=0;
		System.out.println(elasticMap.size()+"\t"+databaseMap.size());
		//for deleting from elasticsearch
		Set<Integer> elasticSet=elasticMap.keySet();
		for(Integer i:elasticSet)
		{
			if(databaseMap.get(i)==null)
			{
				if(m<10)
				{
					m++;
					z++;
				}
				
				delete(indexMap.get(i).split(":")[0],indexMap.get(i).split(":")[1],indexMap.get(i).split(":")[2]);
			}
				
			
			
		}
		System.out.println("databaseMap size="+databaseMap.size());
		System.out.println("elasticMap size="+elasticMap.size());
		System.out.println("indexMap size="+indexMap.size());
		System.out.println("creating x=="+x);
		System.out.println("updating y=="+y);
		System.out.println("deleting z=="+z);
	
	}
	
	public void compareDataPlatformNC(Map<Integer, String> databaseMap,Map<Integer, String> elasticMap)
	{
		System.out.println("Calling compareDataPlatformNC");
		int x=0;
		int y=0;
		int z=0;
		Set<Integer> databaseSet=databaseMap.keySet();
		for(Integer i:databaseSet)
		{
			
			if(elasticMap.get(i)==null)  //if data is not present in elasticsearch then insert
			{
				x++;
				updatePlatformNC("document","jdbc",i.toString()+"_"+ElasticSearchConfig.serverName,databaseMap.get(i));
				
			}
			
			else if(!(elasticMap.get(i)).equals(databaseMap.get(i)))  //if data is present then update
			{
				y++;
				//System.out.println("***************updating*****************");
				updatePlatformNC(indexMap.get(i).split(":")[0],indexMap.get(i).split(":")[1],indexMap.get(i).split(":")[2],databaseMap.get(i));
					
				
				
			}
			
			
			
			
		}
		int m=0;
		System.out.println(elasticMap.size()+"\t"+databaseMap.size());
		//for deleting from elasticsearch
		Set<Integer> elasticSet=elasticMap.keySet();
		for(Integer i:elasticSet)
		{
			if(databaseMap.get(i)==null)
			{
				if(m<10)
				{
					m++;
					z++;
				}
				
				deletePlatformNC(indexMap.get(i).split(":")[0],indexMap.get(i).split(":")[1],indexMap.get(i).split(":")[2]);
			}
				
			
			
		}
		System.out.println("databaseMap size="+databaseMap.size());
		System.out.println("elasticMap size="+elasticMap.size());
		System.out.println("indexMap size="+indexMap.size());
		System.out.println("creating x=="+x);
		System.out.println("updating y=="+y);
		System.out.println("deleting z=="+z);
	
	}
	
	
	
	
	
	
	//for district job board
	public void creatingStructureForDistrictJobBoard()
	{
		ClientConfig config = new DefaultClientConfig();
	    Client client = Client.create(config);
	    URI uri=null;
	    uri=getURIForStructureCr(ElasticSearchConfig.indexForhqDistrictJobBoard);    	
		WebResource service = client.resource(uri);
	    ClientResponse response = null;
	    String input = "{\"type\" : \"jdbc\"," +
	    				"\"strategy\" : \"simple\"," +
	    				  		"\"jdbc\" : " +
				    		"[" +
				    			"{\"url\" : \""+ElasticSearchConfig.sElasticSearchDBURL+"\"," +
				    			"\"user\" : \""+ElasticSearchConfig.sElasticSearchDBUser+"\",\"password\" : \""+ElasticSearchConfig.sElasticSearchDBPass+"\"," +
				    			  
				    			  
				    			  
									"\"sql\" : \"select jo.jobId,jo.headQuarterId,hq.headQuarterName,bm.branchName,jo.branchId,jo.jobTitle,dm.address,dm.zipcode,ifnull(sjm.subjectName,'') as subjectName,"
									+ "ifnull(gm.geoZoneName,'') as geoZoneName,ifnull(scm.schoolName,'') as schoolName ,dm.districtid,ifnull(scm.schoolId,0) as schoolId,"
									+ "ifnull(jo.geoZoneId,0) as geoZoneId,jo.createdForEntity,jo.isPoolJob,ifnull(dm.cityName,'') as cityName,"
									+ "ifnull(st.stateId,0) as stateId,ifnull(st.stateName,'') as stateName,jo.jobEndDate,dm.districtName,"
									+ "jm.jobCategoryName,ifnull(stm.stateName,'') as schoolStateName,ifnull(scm.cityName,'')  as schoolCityName,"
									+ "ifnull(scm.address,'') as schoolAddress,ifnull(scm.zip,'') as schoolZip "

									+ "from joborder jo "
									+ "LEFT JOIN subjectmaster sjm ON sjm.subjectId=jo.subjectId "
									+ "left join districtmaster dm on jo.districtId= dm.districtId "
									+ "left join geozonemaster gm on jo.geoZoneId=gm.geoZoneId "
									+ "left join jobcategorymaster jm on jo.jobCategoryId=jm.jobCategoryId "
									+ "LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId "
									+ "LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId "
									+ "left join statemaster stm on stm.stateId=scm.stateId "
									+ "left join statemaster st on dm.stateId=st.stateId "
									+ "where jo.jobStartDate<= now() and jo.jobEndDate >=now() and jo.isPoolJob !=1 and jo.status='A' and jo.approvalBeforeGoLive=1 "
									+ "and jo.isInviteOnly !=true limit 0,0 \"," +
									
				    			   "\"index\" : \""+ElasticSearchConfig.indexForhqDistrictJobBoard+"\"," +
				    			   //"\"autocommit\" : true," +
				    			   " \"fetchsize\" : 30" +
				    			  "}" +
				    		  "]" +
	    			  "}";
	    
	    //System.out.println(input);
	    
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
	    //System.out.println("Form response11 " + response.getEntity(String.class));
	    System.out.println("***********************************************************");
	}
	public Map<Integer, String> getDBDataForDistrictJobBoard()
	{
		
		System.out.println("Calling getDBDataForDistrictJobBoard");
		
		Map<Integer, String> map=new HashMap<Integer, String>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//Connection con=DriverManager.getConnection(ElasticSearchConfig.sElasticSearchDBURL,ElasticSearchConfig.sElasticSearchDBUserForSch,ElasticSearchConfig.sElasticSearchDBPass);
			Connection con=DriverManager.getConnection(ElasticSearchConfig.sElasticSearchDBURL,ElasticSearchConfig.sElasticSearchDBUser,ElasticSearchConfig.sElasticSearchDBPass);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
		    String dateWithoutTime = sdf.format(new Date());
			String sql="select jo.jobId,jo.headQuarterId,ifnull(hq.headQuarterName,'') as headQuarterName,ifnull(bm.branchName,'') as branchName," +
					"jo.branchId,jo.jobTitle,jo.positionStart as position ,jo.gradeLevel,dm.address,dm.zipcode,ifnull(sjm.subjectName,'') as subjectName,sjm.subjectId,"
					+ "ifnull(gm.geoZoneName,'') as geoZoneName,ifnull(scm.schoolName,'') as schoolName ,dm.districtid,ifnull(scm.schoolId,0) as schoolId,"
					+ "ifnull(jo.geoZoneId,0) as geoZoneId,jo.createdForEntity,jo.isPoolJob,ifnull(dm.cityName,'') as cityName,"
					+ "ifnull(st.stateId,0) as stateId,ifnull(st.stateName,'') as stateName,jo.jobEndDate,dm.districtName,"
					+ "jm.jobCategoryName,jm.jobCategoryId,IFNULL(jm.parentJobCategoryId,0) as parentJobCategoryId,ifnull(stm.stateName,'') as schoolStateName,ifnull(scm.cityName,'')  as schoolCityName,"
					//+ "ifnull(scm.address,'') as schoolAddress,ifnull(scm.zip,'') as schoolZip "
					+ "ifnull(scm.address,'') as schoolAddress,ifnull(scm.zip,'') as schoolZip,dm.headquarterId," 
					+ "IFNULL(dtm.timeZoneShortName,'') AS dtmTimeZoneShortName,IFNULL(jtm.timeZoneShortName,'') AS joTimeZoneShortName,IFNULL(jo.jobendtime,'') AS jobEndTime  "
					+ "from joborder jo "
					+ "LEFT JOIN subjectmaster sjm ON sjm.subjectId=jo.subjectId "
					+ "left join districtmaster dm on jo.districtId= dm.districtId "
					+ "left join geozonemaster gm on jo.geoZoneId=gm.geoZoneId "
					+ "left join jobcategorymaster jm on jo.jobCategoryId=jm.jobCategoryId "
					+ "LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId "
					+ "LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId "
					+ "left join statemaster stm on stm.stateId=scm.stateId "
					+ "left join statemaster st on dm.stateId=st.stateId "
					+ "LEFT JOIN headquartermaster hq ON jo.headquarterid=hq.headquarterid "
					+ "LEFT JOIN branchmaster bm ON jo.branchid=bm.branchid "
					+ "LEFT JOIN timezonemaster dtm ON dtm.timeZoneId=dm.timeZone "
					+ "LEFT JOIN timezonemaster jtm ON jtm.timeZoneId=jo.jobTimeZoneId "
								
					+ "where jo.jobStartDate<= '"+dateWithoutTime+"' and jo.jobEndDate >='"+dateWithoutTime+"' and jo.isPoolJob !=1 and jo.status='A' "
					//+ "and jo.approvalBeforeGoLive=1 and jo.isInviteOnly !=true AND jo.isVacancyJob !=TRUE";
					+ "and jo.approvalBeforeGoLive=1 and (jo.isInviteOnly =FALSE OR jo.isInviteOnly IS NULL) AND (jo.isVacancyJob =FALSE OR  jo.isVacancyJob IS NULL) "
					+ "and (jo.hiddenJob =FALSE OR jo.hiddenJob IS NULL)";
					/*if(jobOrder!=null && jobOrder.getJobId()!=null)
					{
						sql+=" and jo.jobId="+jobOrder.getJobId();
					}*/
					
					sql+= " order by jo.jobId";
			//System.out.println(sql);
			PreparedStatement psmt=con.prepareStatement(sql);
			ResultSet rset=psmt.executeQuery();
		//	int k=0;
			//int l=0;
			int rowCount=0;			
			int jobid=0;
			int i=0;
			
			while(rset.next())
			{//System.out.println(rowCount);
				
						
				JSONObject obj=new JSONObject();
				obj.put("jobId", rset.getInt("jo.jobId"));
				if(rset.getInt("jo.headQuarterId")!=0)
					obj.put("headQuarterId", rset.getInt("jo.headQuarterId"));
				else
					obj.put("headQuarterId", rset.getInt("dm.headQuarterId"));
				obj.put("branchId", rset.getInt("jo.branchId"));
				obj.put("headQuarterName", rset.getString("headQuarterName"));
				obj.put("branchName", rset.getString("branchName"));
				obj.put("jobTitle", rset.getString("jo.jobTitle"));
				if(rset.getString("dm.address")!=null)
					obj.put("address", rset.getString("dm.address"));
				else
					obj.put("address", "");
				if(rset.getString("dm.zipcode")!=null)
					obj.put("zipcode", rset.getString("dm.zipcode"));
				else
					obj.put("zipcode", "");
				obj.put("geoZoneId", rset.getInt("geoZoneId"));
				obj.put("createdForEntity", rset.getInt("jo.createdForEntity"));
				obj.put("isPoolJob", rset.getInt("jo.isPoolJob"));
				obj.put("stateId", rset.getInt("stateId"));//for district i.e dm
				obj.put("jobEndDate", rset.getString("jo.jobEndDate"));
				if(rset.getString("position")!=null)
					obj.put("position", rset.getString("position"));
				else
					obj.put("position", "");
				
				if(rset.getString("gradeLevel")!=null)
					obj.put("gradeLevel", rset.getString("gradeLevel"));
				else
					obj.put("gradeLevel", "");
				
				obj.put("districtid", rset.getInt("dm.districtid"));
				if(rset.getString("jm.jobCategoryName")!=null)
					obj.put("jobCategoryName", rset.getString("jm.jobCategoryName"));
				else
					obj.put("jobCategoryName", "");
				if(rset.getString("dm.districtName")!=null)
					obj.put("districtName", rset.getString("dm.districtName"));
				else
					obj.put("districtName", "");
				if(rset.getString("stateName")!=null)
					obj.put("stateName", rset.getString("stateName"));
				else
					obj.put("stateName", "");
				if(rset.getString("cityName")!=null)
					obj.put("cityName", rset.getString("cityName"));
				else
					obj.put("cityName", "");
				
				
				
				if(rset.getString("subjectName")!=null)
				{
					obj.put("subjectName", rset.getString("subjectName"));
					obj.put("subjectId", rset.getString("sjm.subjectId"));
				}
					
				else
				{
					obj.put("subjectName", "");
					obj.put("subjectId", "");
				}
					
				
				if(rset.getString("geoZoneName")!=null)
					obj.put("geoZoneName", rset.getString("geoZoneName")+"");
				else
					obj.put("geoZoneName", "");
				
				obj.put("parentJobCategoryId", rset.getInt("parentJobCategoryId"));
				obj.put("jobCategoryId", rset.getInt("jm.jobCategoryId"));
				
				if(rset.getInt("jo.jobId")==6544 || rset.getInt("jo.jobId")== 6545)
					obj.put("priority", 2);
				else
					obj.put("priority", 1);
				obj.put("dtmTimeZoneShortName", rset.getString("dtmTimeZoneShortName"));
				obj.put("joTimeZoneShortName", rset.getString("joTimeZoneShortName"));
				obj.put("jobEndTime", rset.getString("jobEndTime"));
				
				
				
				
				jobid=rset.getInt("jo.jobid");
				JSONArray schoolId = new JSONArray();
				JSONArray schoolStateName = new JSONArray();
				JSONArray schoolCityName = new JSONArray();
				JSONArray schoolAddress = new JSONArray();
				JSONArray schoolZip = new JSONArray();
				JSONArray schoolName = new JSONArray();
				while(jobid==rset.getInt("jo.jobid"))
				{rowCount++;
					jobid=rset.getInt("jo.jobid");
					schoolId.add(rset.getInt("schoolId"));
					
					if(rset.getString("schoolStateName")!=null)
						schoolStateName.add(rset.getString("schoolStateName"));
					else
						schoolStateName.add("");
					
					if(rset.getString("schoolCityName")!=null)
						schoolCityName.add(rset.getString("schoolCityName"));
					else
						schoolCityName.add("");
					
					if(rset.getString("schoolAddress")!=null)
						schoolAddress.add(rset.getString("schoolAddress"));
					else
						schoolAddress.add("");
					
					if(rset.getString("schoolZip")!=null)
						schoolZip.add(rset.getString("schoolZip"));
					else
						schoolZip.add("");
					
					if(rset.getString("schoolName")!=null)
						schoolName.add(rset.getString("schoolName"));
					else
						schoolName.add("");
					
					if(!rset.isLast())
					{
						rset.next();
					}
					else
						break;
				}
				
				
				obj.put("schoolId", schoolId);
				obj.put("schoolStateName", schoolStateName);
				obj.put("schoolCityName", schoolCityName);
				obj.put("schoolAddress", schoolAddress);
				obj.put("schoolZip", schoolZip);
				obj.put("schoolName", schoolName);
				
				
				if(!rset.isLast() )
					rset.previous();
				if(rset.isLast() && i!=1)
				{
					i=1;
					jobid=rset.getInt("jo.jobid");
					if(rowCount!=1)
						rset.previous();
					if(!(rset.getInt("jo.jobid")==jobid))
					{
						map.put(rset.getInt("jo.jobid"), obj.toString());
						//rset.next();
					}
						
					else
					{
						map.put(rset.getInt("jo.jobid"), obj.toString());
						break;
					}
						
				}
				
				
								
				
				map.put(rset.getInt("jo.jobid"), obj.toString());
				//System.out.println("map size==============="+map.size());
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} 
		//System.out.println("map size========fffffffff======="+map.size());
		return map;
	}
	
	public Map<Integer, String> esForDistrictJobBoard()
	{
		//System.out.println("Calling esForDistrictJobBoard");
		Map<Integer, String> map=new HashMap<Integer, String>();
		indexMapForDistrictJobBoard=new HashMap<Integer, String>();
		ClientConfig config = new DefaultClientConfig();
		URI uri=null;
	    Client client = Client.create(config);
	    uri=getURIForSearch(ElasticSearchConfig.indexForhqDistrictJobBoard);
		WebResource service = client.resource(uri);
	    ClientResponse response = null;
	    String input = "{\"query\": { \"match_all\": {} },\"size\":100000}";
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
	    String s=response.getEntity(String.class);
	 //   System.out.println(s);
	    
	    JSONObject jsonObject=new JSONObject();
	    try {
			
			//System.out.println(((JSONObject)jsonObject.get("hits")));
			 //System.out.println(((JSONObject)jsonObject.get("hits")).get("hits"));
	    	jsonObject=jsonObject.fromObject(s);
			 JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
			 //System.out.println("district job board hits size==="+hits.size());
			 for(int i=0; i<hits.size(); i++)
			 {
				jsonObject=(JSONObject)hits.get(i);
				String index=(String)jsonObject.get("_index");
				String type=(String)jsonObject.get("_type");
				String id=(String)jsonObject.get("_id");
				jsonObject=(JSONObject)jsonObject.get("_source");
				//System.out.println(jsonObject.toString());
				//System.out.println(jsonObject.get("jobid").toString()+"_"+jsonObject.get("schoolId")+"_"+jsonObject.get("certTypeId"));
				map.put(jsonObject.getInt("jobId"), jsonObject.toString());
				indexMapForDistrictJobBoard.put(jsonObject.getInt("jobId"), index+":"+type+":"+id);
			}
			


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//System.out.println("document size="+map.size());
		
		return map;
		
	}
	
	 public void createDistrictJobBoard(Map<Integer, String> dataBaseMap)
		{
		 try
		 {
			 System.out.println("creating createESDistrictJobBoard");
			 int x=0;
			 int y=0;
			  int j=0;
				System.out.println("*****************insertion start**************");
				Set<Integer> set=dataBaseMap.keySet();
				for(Integer i:set)
				{
					ClientConfig config = new DefaultClientConfig();
				    Client client = Client.create(config);
				    URI uri=null;
				    try {
						
				    	//uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+ElasticSearchConfig.indexForhqDistrictJobBoard+"/jdbc/"+i+"/?pretty");
						uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+ElasticSearchConfig.indexForhqDistrictJobBoard+"/jdbc/"+i+"?pretty");
						//System.out.println(ElasticSearchConfig.sElasticSearchURL+"/"+ElasticSearchConfig.indexForhqDistrictJobBoard+"/jdbc/?pretty");
				    		
				    	//System.out.println(++x+"\tmapsize="+dataBaseMap.size());
					} catch (URISyntaxException e) {
						j++;
						System.out.println("failed jobid="+i);
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    WebResource service = client.resource(uri);
				    ClientResponse response = null;
				    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).put(ClientResponse.class, dataBaseMap.get(i));
				  //  System.out.println("Form response11 " + response.getEntity(String.class));
				}
				System.out.println("Total job id fail in creating=="+j);
				System.out.println("****************insertion end************");
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
			
			
		}
	
	public void updateESForDistrictJobBoard(Map<Integer, String> databaseMap,Map<Integer, String> elasticMap)
	{
		//System.out.println("*************************************");
		System.out.println(databaseMap.size()+"\t"+elasticMap.size());
		//System.out.println("Calling compareData");
		int x=0;
		int y=0;
		int z=0;
		//System.out.println(databaseMap.get(1932));
		//System.out.println(elasticMap.get(1932));
		//System.out.println(databaseMap.get(1932).equalsIgnoreCase(databaseMap.get(1932)));
		Set<Integer> databaseSet=databaseMap.keySet();
		for(Integer i:databaseSet)
		{
			//System.out.println(elasticMap.get(i));
			//System.out.println(databaseMap.get(i));
			//System.out.println("*****************************");
			
			//if data is not present in elasticsearch then insert
			if(elasticMap.get(i)==null)
			{
				//System.out.println("**********************creating**************************"+i.toString());
				//System.out.println(databaseMap.get(i));
				x++;
				//System.out.println(i.toString());
				update(ElasticSearchConfig.indexForhqDistrictJobBoard,"jdbc",i.toString(),databaseMap.get(i));
			}
			//if data is present then update
			else if(!(elasticMap.get(i)).equals(databaseMap.get(i)))
			{
				
				y++;
				//System.out.println("***************updating*****************");
				//	System.out.println("updating key="+i);
				//	System.out.println(elasticMap.get(i));
				//	System.out.println(databaseMap.get(i));
					//System.out.println("elastic key="+indexMap.get(i));
					//break;
					//System.out.println(elasticMap.get(i).replaceAll("\n", " "));
					//System.out.println(databaseMap.get(i).replaceAll("\n", " "));
					update(indexMapForDistrictJobBoard.get(i).split(":")[0],indexMapForDistrictJobBoard.get(i).split(":")[1],indexMapForDistrictJobBoard.get(i).split(":")[2],databaseMap.get(i));
					
				
				
			}
			
			
			
			
		}
		
	//	System.out.println(elasticMap.size()+"\t"+databaseMap.size());
		//for deleting from elasticsearch
		Set<Integer> elasticSet=elasticMap.keySet();
		for(Integer i:elasticSet)
		{
			if(databaseMap.get(i)==null)
			{
				
					//System.out.println("**************************deleting********************"+i);
					//System.out.println(databaseMap.get(i));
					z++;
				
				
				delete(indexMapForDistrictJobBoard.get(i).split(":")[0],indexMapForDistrictJobBoard.get(i).split(":")[1],indexMapForDistrictJobBoard.get(i).split(":")[2]);
			}
				
			
			
		}
		//System.out.println("databaseMap size for districtjobboard="+databaseMap.size());
		//System.out.println("elasticMap size  for districtjobboard="+elasticMap.size());
		//System.out.println("indexMap size  for districtjobboard="+indexMapForDistrictJobBoard.size());
		//System.out.println("creating x  for districtjobboard=="+x);
		//System.out.println("updating y  for districtjobboard=="+y);
		//System.out.println("deleting z  for districtjobboard=="+z);
	/*	System.out.println("iterating index map");
		Set<String> set=indexMap.keySet();
		for(String s:set)
		{
			System.out.println(s);
		}*/
	}
	
	public String esForDistrictJobBoard(DistrictMaster districtMaster,int iBranchId,int iHeadQuarterId,int districtId,String districtName,String schoolName,String zipcode,String geoZoneName,String jobCategoryName,String subjectName,
			int from,HttpServletRequest request,String noOfRow,String pageNo,int pgNo,String sortOrderFieldName,String sortOrderTypeVal,String searchTerm,int stateId
			,String jobCategoryId,String jobSubCategoryId,String positionStart,String talentRef,String teacherId,String gradeLevel,String requestType)
	{
		if(searchTerm!=null)
			searchTerm=searchTerm.replaceAll("\"", "");
		StringBuffer input =new StringBuffer();
		int noOfRowInPage	=	Integer.parseInt(noOfRow);
		int start 			= 	((pgNo-1)*noOfRowInPage);
		int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		end=noOfRowInPage;
		Boolean kellyUrl = false;
		// kellyUrlValue = "localhost";
		if(request.getRequestURL().toString().contains(kellyUrlValue)){
			kellyUrl = true;
			System.out.println(" talentRef ::::::::::::: "+talentRef);
		}
		try
		{
			
			input.append("{\"query\": {\"bool\": {");
			  input.append(" \"must\" : [");
				/*if(districtName!=null && !districtName.trim().equals(""))
				{
					input.append("   {\"match_phrase\" : { \"districtName\" : \""+districtName+"\" }},");
				}*/
			  if(iBranchId!=0 )
				{
					input.append("   {\"match_phrase\" : { \"branchId\" : "+iBranchId+" }},");
				}
			  if(iHeadQuarterId!=0 )
				{
					input.append("   {\"match_phrase\" : { \"headQuarterId\" : "+iHeadQuarterId+" }},");
				}
			  	if(districtId!=0 )
				{
					input.append("   {\"match_phrase\" : { \"districtid\" : "+districtId+" }},");
				}
			  	
				if(schoolName!=null && (!schoolName.trim().equals("")) )
				{
					input.append("   {\"match_phrase\" : { \"schoolName\" : \""+schoolName+"\" }},");
				}
				if(zipcode!=null && (!zipcode.trim().equals("")))
				{
					//input.append("   {\"match_phrase\" : { \"zipcode\" : \""+zipcode+"\" }},");
					input.append("{ \"bool\" : {\"should\" : [{ \"match_phrase\" : {\"zipcode\" : \""+zipcode+"\"}},{\"match_phrase\" : {\"schoolZip\" : \""+zipcode+"\"}} ]}},");
				}
				if(geoZoneName!=null && !geoZoneName.trim().equals("") && (!geoZoneName.trim().equalsIgnoreCase("ALL")))
				{
					input.append("   {\"match_phrase\" : { \"geoZoneName\" : \""+geoZoneName+"\" }},");
				}
				
				if(jobCategoryName!=null && !jobCategoryName.trim().equals("") && (!jobCategoryName.trim().equalsIgnoreCase("ALL")))
				{
					
					if(iHeadQuarterId!=0 && iHeadQuarterId!=2 )
						input.append("   {\"match_phrase\" : { \"parentJobCategoryId\" : "+jobCategoryId+" }},");
					else
					{
						if(jobSubCategoryId.equals("0"))
						{
							input.append("{ \"bool\" : {\"should\" : [{ \"match_phrase\" : {\"jobCategoryId\" : "+jobCategoryId+"}},{\"match_phrase\" : {\"parentJobCategoryId\" : "+jobCategoryId+"}} ]}},");
						}
						else
						{
							input.append("   {\"match_phrase\" : { \"jobCategoryId\" : "+jobSubCategoryId+" }},");
						}
						//input.append("   {\"match_phrase\" : { \"jobCategoryId\" : "+jobCategoryId+" }},");
						//input.append("   {\"match_phrase\" : { \"jobCategoryName\" : \""+jobCategoryName+"\" }},");
					}
						
					//input.append("{ \"bool\" : {\"should\" : [{ \"match_phrase\" : {\"jobCategoryId\" : 250}},{\"match_phrase\" : {\"parentJobCategoryId\" : 250}} ]}},");
				}
				
				if(subjectName!=null && !subjectName.trim().equals("") && (!subjectName.trim().equalsIgnoreCase("ALL,")))
				{
					//input.append("   {\"match\" : { \"subjectName\" : \""+subjectName+"\" }},");
					input.append("   {\"match\" : { \"subjectId\" : \""+subjectName+"\" }},");
				}
				if(stateId!=0)
				{
					input.append("   {\"match_phrase\" : { \"stateId\" : "+stateId+" }},");
				}
				if(!positionStart.trim().equals(""))
				{
					input.append("   {\"match_phrase\" : { \"position\" : \""+positionStart+"\" }},");
					
				}
				
				if(!gradeLevel.trim().equals(""))
				{
					input.append("   {\"match_phrase\" : { \"gradeLevel\" : \""+gradeLevel+"\" }},");
					
				}
				
				if(input.indexOf(",")!=-1)
					input.replace(input.lastIndexOf(","), input.lastIndexOf(",")+1, "");
				input.append("],");
			  
				input.append("\"should\": [{"
						+"  \"multi_match\" : {"
	    		        +"    \"fields\" : [\"jobTitle\",\"subjectName\",\"address\",\"zipcode\",\"schoolZip\",\"schoolAddress\",\"jobCategoryName\",\"districtName\",\"stateName\",\"cityName\",\"geoZoneName\",\"schoolName\",\"headQuarterName\",\"branchName\"],"
	    		        //+"    \"fields\" : [\"jobTitle\",\"subjectName\"],"
	    		      +"      \"query\" : \""+searchTerm+"\","
	    		    +"        \"type\" : \"phrase_prefix\""
	    		  +"      }"
				  		
			  		+ "},"
			  		+ "{ \"match\": { \"jobTitle\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"subjectName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"address\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"zipcode\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"schoolZip\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"schoolAddress\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"jobCategoryName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"districtName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"stateName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"cityName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"geoZoneName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"schoolName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"headQuarterName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"branchName\":  \""+searchTerm+"\" }}"
			  		
			  //		
			  		+ "],");
				if(searchTerm.equals(""))
					input.append("\"minimum_should_match\" : \"1%\"}},\"size\":"+end+",\"from\":"+start);
				else
					input.append("\"minimum_should_match\" : 1}},\"size\":"+end+",\"from\":"+start);
						//if(searchTerm==null || searchTerm.trim().equals(""))
						{
							//input.append(",\"sort\": { \""+sortOrderFieldName+"\": { \"order\": \""+sortOrderTypeVal+"\" }}");
							if(sortOrderFieldName.equalsIgnoreCase("jobTitle") 
									|| sortOrderFieldName.equalsIgnoreCase("subjectName") 
									|| sortOrderFieldName.equalsIgnoreCase("geoZoneName")
									|| sortOrderFieldName.equalsIgnoreCase("schoolName")
									|| sortOrderFieldName.equalsIgnoreCase("districtName")
									|| sortOrderFieldName.equalsIgnoreCase("position")
									|| sortOrderFieldName.equalsIgnoreCase("gradeLevel"))
							{
								//input.append(",\"sort\": {\"_script\": {\"script\": \"_source."+sortOrderFieldName+"\",\"type\": \"string\",\"order\": \""+sortOrderTypeVal+"\"}}");
								
								input.append(",\"sort\": [" +
										"{ \"priority\":   { \"order\": \"desc\" }},");
								if(requestType.equals("1") && districtId==1200390 && sortOrderFieldName.equalsIgnoreCase("jobTitle"))
									input.append("{ \"isPoolJob\":   { \"order\": \"desc\" }},");
								input.append("{\"_script\": {\"script\": \"_source."+sortOrderFieldName+"\",\"type\": \"string\",\"order\": \""+sortOrderTypeVal+"\"}}" +
													
										"]");
							}
							else if(sortOrderFieldName.equalsIgnoreCase("address"))
							{
								/*sortOrderFieldName="schoolStateName";
								input.append(",\"sort\": {\"_script\": {" +
										
										"\"script\": \"_source."+sortOrderFieldName+"\",\"type\": \"string\",\"order\": \""+sortOrderTypeVal+"\"," +
										"\"script\": \"_source.schoolCityName\",\"type\": \"string\",\"order\": \""+sortOrderTypeVal+"\"" +
										//"\"script\": \"_source.schoolAddress\",\"type\": \"string\",\"order\": \""+sortOrderTypeVal+"\"" +
												"}" +
												"}");*/
								input.append(",\"sort\": [" +
										"{ \"priority\":   { \"order\": \"desc\" }}," +
										"{ \"schoolStateName\":   { \"order\": \""+sortOrderTypeVal+"\" }}," +
										"{ \"schoolCityName\": { \"order\": \""+sortOrderTypeVal+"\" }}]");
							}
							else
							{
								//input.append(",\"sort\": {\"_script\": {\"script\": \"_source."+sortOrderFieldName+"\",\"type\": \"string\",\"order\": \""+sortOrderTypeVal+"\"}}");
								
								input.append(",\"sort\": [" +
										"{ \"priority\":   { \"order\": \"desc\" }}," +
										"{\"_script\": {\"script\": \"_source."+sortOrderFieldName+"\",\"type\": \"string\",\"order\": \""+sortOrderTypeVal+"\"}}," +
										"{ \"isPoolJob\":   { \"order\": \"asc\" }}" +			
										"]");
							}
								
							
						}
						
						
								input.append("}");
		  
		  	
		  		//input.append("\"minimum_should_match\" : \"1%\"}},\"size\":"+end+",\"from\":"+start+",\"sort\": { \""+sortOrderFieldName+"\": { \"order\": \""+sortOrderTypeVal+"\" }}}");
			//	input.append("\"minimum_should_match\" : \"1%\"}},\"size\":100,\"from\":"+start+"}");
		  		//System.out.println(input.toString());
		  		
		  		ClientConfig config = new DefaultClientConfig();
				URI uri=null;
			    Client client = Client.create(config);
			    List<String> list=new ArrayList<String>();
			    uri=getURIForSearch(ElasticSearchConfig.indexForhqDistrictJobBoard);
				WebResource service = client.resource(uri);
			    ClientResponse response = null;
			    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input.toString());
			    String s=response.getEntity(String.class);
			    System.out.println(s);
			    JSONObject jsonObject = new JSONObject();
			    jsonObject=jsonObject.fromObject(s);
			   // System.out.println("total searchbydo=="+((JSONObject)jsonObject.get("hits")).get("total"));
			    int totalRecord=((JSONObject)jsonObject.get("hits")).getInt("total");
			    JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
			    StringBuffer sb = new StringBuffer();
			    
			   
					
					//watch later
					/*if(schoolName!=null && (!schoolName.trim().equals("")))
					{
						sb.append("<div style='padding-bottom:10px;'><label>Address</label><div style='line-height: 10px;' id='schoolAddress'></div></div>");
					}	*/		
					sb.append("<table width='100%' border='0' id='tblGrid'>");
					sb.append("<thead class='bg'>");
					sb.append("<tr>");		
					String responseText="";
		//			String sortOrderFieldName="jobId";
		//			String sortOrderTypeVal	=	"0";
					
					responseText=PaginationAndSorting.responseSortingLinkES("Job ID",sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");			

					responseText=PaginationAndSorting.responseSortingLinkES("Title",sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");			
					
					if(iBranchId ==0 && iHeadQuarterId==0)
					{
						if((districtId != 0 && districtId == 804800 || districtId == 7800047)||districtId==7800292 || (districtMaster!=null && !districtMaster.getIncludeZone())){
						   //do nothing  Swadesh
							if(districtId==7800047||districtId==7800292){
								responseText=PaginationAndSorting.responseSortingLinkES("Grade Level",sortOrderFieldName,"gradeLevel",sortOrderTypeVal,pgNo);
								sb.append("<th  valign='top'>"+responseText+"</th>");	
							}
						}else{
						responseText=PaginationAndSorting.responseSortingLinkES("Zone",sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
						sb.append("<th  valign='top'>"+responseText+"</th>");	
						}

						responseText=PaginationAndSorting.responseSortingLinkES("Subject",sortOrderFieldName,"subjectName",sortOrderTypeVal,pgNo);
						sb.append("<th  valign='top'>"+responseText+"</th>"); 

						responseText=PaginationAndSorting.responseSortingLinkES("School",sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
						sb.append("<th  valign='top'>"+responseText+"</th>");	
					}
					else
					{
						responseText=PaginationAndSorting.responseSortingLinkES("District",sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
						sb.append("<th  valign='top'>"+responseText+"</th>");
					}
							
					if(districtId!=7800292){
						responseText=PaginationAndSorting.responseSortingLinkES("Address",sortOrderFieldName,"location",sortOrderTypeVal,pgNo);
						sb.append("<th  valign='top'>"+responseText+"</th>");
					}

					responseText=PaginationAndSorting.responseSortingLinkES("End Date",sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
					//SWADESH
					if((districtId != 0 && (districtId == 7800047||districtId ==7800292)) && (districtMaster!=null ))
					{
					     responseText=PaginationAndSorting.responseSortingLinkES("Position Start Date",sortOrderFieldName,"position",sortOrderTypeVal,pgNo);
					      sb.append("<th  valign='top'>"+responseText+"</th>");
					}

					sb.append("<th valign='top'>Actions/Apply</th>");
					sb.append("</tr>");
					sb.append("</thead>");
					int rowCount = 0;

					String redirectTo = request.getParameter("redirectTo")==null?"":request.getParameter("redirectTo");			

				
				 for(int i=0; i<hits.size(); i++)
				 {
			    	
					jsonObject=(JSONObject)hits.get(i);
					String index=(String)jsonObject.get("_index");
					String type=(String)jsonObject.get("_type");
					String id=(String)jsonObject.get("_id");
					jsonObject=(JSONObject)jsonObject.get("_source");
				//	System.out.println(jsonObject);	
					
					if(i==0 && schoolName!=null && (!schoolName.trim().equals("")))
					{
						//sb.append("<script>document.getElementById('schoolAddress').text="+getSchoolAddressFromJSONArray(jsonObject, schoolName)+"</script>");
						
						
					}

					rowCount++;			
					String subName =null;
					/*if(jbOrder.getSubjectMaster()!=null){
						subName=jbOrder.getSubjectMaster().getSubjectName();
					}else{
						subName="";
					}	*/
					if(!jsonObject.getString("subjectName").equals("")){
						subName=jsonObject.getString("subjectName");
					}else{
						subName="";
					}
					sb.append("<tr>");
					sb.append("<td>"+jsonObject.get("jobId")+"</td>");
					sb.append("<td>"+jsonObject.get("jobTitle")+"</td>");

					/*if(jbOrder.getGeoZoneMaster()!=null){

						if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990){
							sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							//sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						}else if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
							//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						}else{
							sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jbOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jbOrder.getDistrictMaster().getDistrictId()+");\">"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						}
					}else
					{
						sb.append("<td></td>");
					}	*/
					
					if(iBranchId ==0 && iHeadQuarterId==0)
					{
						if(!jsonObject.getString("geoZoneName").equals("") && districtMaster!=null &&  districtMaster.getIncludeZone())
						{
							
							if(jsonObject.getInt("districtid")==4218990){
								sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jsonObject.getString("geoZoneName")+"</a></td>");
								//sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}else if(jsonObject.getInt("districtid")==1200390){
								//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jsonObject.getString("geoZoneName")+"</a></td>");
							}else{
								sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jsonObject.getString("geoZoneId")+",'"+jsonObject.getString("geoZoneName")+"',"+jsonObject.getString("districtid")+");\">"+jsonObject.getString("geoZoneName")+"</a></td>");
							}
							}else if(districtId != 0 && districtId == 804800 || districtId==7800047||districtId ==7800292){
								if(districtId==7800047||districtId ==7800292){
									
									if(!jsonObject.containsKey("gradeLevel") || jsonObject.getString("gradeLevel").trim().equals(""))
									{
										sb.append("<td style='font-size:11px;'>N/A</td>");
									}
									else
									{
										
										sb.append("<td style='font-size:11px;'>"+jsonObject.getString("gradeLevel")+"</td>");
									}
								}
							//Do Nothing
						}else if(districtMaster!=null &&  districtMaster.getIncludeZone())
						{
							
								sb.append("<td></td>");
						}	
						sb.append("<td>"+subName+"</td>");

						//String schoolListIds=getSchoolByJobIdForDistrictJobBoard(jsonObject.getInt("jobId"));
						if(jsonObject.getInt("districtid")==1200390)
						{
							sb.append("<td>&nbsp;</td>");	
						}
						else
						{
							if(Integer.valueOf(jsonObject.getInt("createdForEntity")).equals(3)){
								if(jsonObject.getInt("isPoolJob")==2){
									sb.append("<td>&nbsp;</td>");	
								}else{System.out.println(jsonObject.getJSONArray("schoolName")+"11111111111111111111"+jsonObject.getJSONArray("schoolName").getString(0));
									sb.append("<td>"+jsonObject.getJSONArray("schoolName").getString(0)+"</td>");
								}
							}
							else{
								//String schoolListIds=getSchoolByJobIdForDistrictJobBoard(jsonObject.getInt("jobId"));
								//watch later as new query for jobId will be fired
								JSONArray schoolNameArr=jsonObject.getJSONArray("schoolName");
							//	System.out.println(schoolNameArr.size()+"\t"+schoolNameArr);
								if(schoolNameArr.size()==1){
									if(jsonObject.getInt("isPoolJob")==2){
										sb.append("<td>&nbsp;</td>");	
									}else{System.out.println(jsonObject.getJSONArray("schoolName")+"22222222222222222222222"+schoolNameArr.getString(0));
										sb.append("<td>"+schoolNameArr.getString(0)+"</td>");
									}
								}else if(schoolNameArr.size()>1)
								{
									if(jsonObject.getInt("districtid")!=7800292)
										sb.append("<td> <a href='javascript:void(0);' onclick=showSchoolList('"+getSchoolIdFromJSONArray(jsonObject)+"')>Many Schools</a></td>");
									else
										sb.append("<td> <a href='javascript:void(0);' onclick=showSchoolList('"+getSchoolIdFromJSONArray(jsonObject)+"')>Multiple Schools</a></td>");
								}else
									sb.append("<td>&nbsp;</td>");
							}

						}
					}
					else
					{
						sb.append("<td>"+jsonObject.getString("districtName")+"</td>");
					}
					

					if(jsonObject.getInt("districtid")==1200390)
					{
						sb.append("<td>"+jsonObject.getString("address")+"</td>");	
					}
					else
					{
						if(jsonObject.getJSONArray("schoolName").size()>0 && !jsonObject.getJSONArray("schoolName").get(0).equals(""))
						//if(false)
						{
							//watch later as new query for jobId will be fired
							
							String schoolAddress="";
							String sAddress="";
							String sstate="";
							String scity = "";
							String szipcode="";

							// Get School address
							//if(false)
							//{
							if(jsonObject.getJSONArray("schoolName").size()==1){
								
								if(!jsonObject.getJSONArray("schoolName").getString(0).equals(""))
								{
									if(jsonObject.getJSONArray("schoolAddress").size()==1 && !jsonObject.getJSONArray("schoolAddress").getString(0).equals(""))
									{
										if(!jsonObject.getJSONArray("schoolCityName").getString(0).equals("") || !jsonObject.getJSONArray("schoolStateName").getString(0).equals(""))
										{
											schoolAddress = jsonObject.getJSONArray("schoolAddress").getString(0)+", ";
										}
										else
										{
											//schoolAddress = jsonObject.getString("schoolAddress");
											schoolAddress = jsonObject.getJSONArray("schoolAddress").getString(0);
										}
									}
									if(!jsonObject.getJSONArray("schoolCityName").getString(0).equals(""))
									{
										if(!jsonObject.getJSONArray("schoolStateName").getString(0).equals(""))
										{
											scity = jsonObject.getJSONArray("schoolCityName").getString(0)+", ";
										}else{
											scity = jsonObject.getJSONArray("schoolCityName").getString(0);
										}
									}
									if(!jsonObject.getJSONArray("schoolStateName").getString(0).equals(""))
									{
										if(!jsonObject.getJSONArray("schoolZip").getString(0).equals(""))
										{
											sstate = jsonObject.getJSONArray("schoolStateName").getString(0)+", ";
										}
										else
										{
											sstate = jsonObject.getJSONArray("schoolStateName").getString(0);
										}	
									}
									if(!jsonObject.getJSONArray("schoolZip").getString(0).equals(""))
									{
										szipcode = jsonObject.getJSONArray("schoolZip").getString(0);
									}

									if(schoolAddress !="" || scity!="" ||sstate!="" || szipcode!=""){
										sAddress = schoolAddress+scity+sstate+szipcode;
									}else{
										sAddress="";
									}
									if(jsonObject.getInt("isPoolJob")==2){
										if(jsonObject.getInt("districtid")!=7800292)
										sb.append("<td>"+getDistrictFullAddressForES(jsonObject)+"</td>");
									}else{
										if(jsonObject.getInt("districtid")!=7800292)
										sb.append("<td>"+sAddress+"</td>");
									}

								}
							}
							else
							{
								if(jsonObject.getInt("districtid")!=7800292)
									sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+getSchoolIdFromJSONArray(jsonObject)+"')>Many Address</a></td>");
							}
						}
						else
						{

							String districtAddress="";
							String address="";
							String state="";
							String city = "";
							zipcode="";

							//Get district address
							if(!jsonObject.getString("districtName").equals(""))
							{
								if(!jsonObject.getString("address").equals("null") && !jsonObject.getString("address").equals(""))
								{
									if(!jsonObject.getString("cityName").equals("") || !jsonObject.getString("stateName").equals(""))
									{
										districtAddress = jsonObject.getString("address")+", ";
									}
									else
									{
										districtAddress = jsonObject.getString("address");
									}
								}
								if(!jsonObject.getString("cityName").equals(""))
								{
									if(!jsonObject.getString("stateName").equals("null") && !jsonObject.getString("stateName").equals(""))
									{
										city = jsonObject.getString("cityName")+", ";
									}else{
										city = jsonObject.getString("cityName");
									}
								}
								if(!jsonObject.getString("stateName").equals("null") && !jsonObject.getString("stateName").equals(""))
								{
									if(!jsonObject.getString("zipcode").equals(""))
									{
										state = jsonObject.getString("stateName")+", ";
									}
									else
									{
										state = jsonObject.getString("stateName");
									}	
								}
								if(!jsonObject.getString("zipcode").equals(""))
								{
									zipcode = jsonObject.getString("zipcode");
								}

								if(districtAddress !="" || city!="" ||state!="" || zipcode!=""){
									address = districtAddress+city+state+zipcode;
								}else{
									address="";
								}
							}
							if(jsonObject.getInt("isPoolJob")==2)
							{
								//address
								if(jsonObject.getInt("districtid")!=7800292)
									sb.append("<td>"+getDistrictFullAddressForES(jsonObject)+"</td>");
							}
							else{
								if(jsonObject.getInt("districtid")!=7800292)
									sb.append("<td>"+address+"</td>");
							}
						}
					}
					//jobEndDate shreeram
					Date endDate = new Date();
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
					endDate=sdf.parse(jsonObject.getString("jobEndDate").toString().substring(0, 10));
					String time=null;
					if(Utility.convertDateAndTimeToUSformatOnlyDate(endDate).equals("Dec 25, 2099"))
						sb.append("<td style='font-size:11px;'>Until filled</td>");
					else
					{
						time=!jsonObject.getString("jobEndTime").trim().equals("")?jsonObject.getString("jobEndTime"):", 11:59 PM ";
						if(jsonObject.containsKey("joTimeZoneShortName") && !jsonObject.getString("joTimeZoneShortName").trim().equals("") )
						{
							sb.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(endDate)+" "+time+" "+jsonObject.getString("joTimeZoneShortName")+"</td>");
						}
							
						else if(jsonObject.containsKey("dtmTimeZoneShortName") && !jsonObject.getString("dtmTimeZoneShortName").trim().equals(""))
						{
							
							sb.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(endDate)+" "+time+" "+jsonObject.getString("dtmTimeZoneShortName")+"</td>");
																		
						}
						else
						{
							sb.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(endDate)+" "+time+" CST"+"</td>");
						}
					}
										
					if(jsonObject.getInt("districtid")==7800047||districtId ==7800292)
					{
						if(!jsonObject.containsKey("position") || jsonObject.getString("position").trim().equals(""))
						{
							sb.append("<td style='font-size:11px;'>N/A</td>");
						}
						else
						{
							
							sb.append("<td style='font-size:11px;'>"+jsonObject.getString("position")+"</td>");
						}
						
					  
					       
					 }

					sb.append("<td>");
					if(kellyUrl){
					if(talentRef==null || talentRef.length()==0) {
						String encBranchId = "";
							if(iBranchId!=0 ){
								encBranchId = Utility.encryptNo(iBranchId);
							}
							if(iBranchId!=0 || iHeadQuarterId!=0){
								sb.append("<a data-original-title='Apply Now' rel='tooltip' id='tpApply"+rowCount+"' href='applyteacherjob.do?jobId="+jsonObject.getString("jobId")+"&branchId="+encBranchId+"&refType=R' ><img src='images/option01.png' style='width:30px; height:25px;'></a>");
							}else{
								sb.append("<a data-original-title='Apply Now' rel='tooltip' id='tpApply"+rowCount+"' href='applyteacherjob.do?jobId="+jsonObject.getString("jobId")+"' ><img src='images/option01.png' style='width:30px; height:25px;'></a>");
							}
						}
						else if(talentRef!=null && talentRef.equalsIgnoreCase("n"))
							sb.append("<a data-original-title=' Apply Now' rel='tooltip' id='tpApply"+rowCount+"' href='applyteacherjob.do?jobId="+jsonObject.getString("jobId")+"&talentRef="+talentRef+"&teacherId="+teacherId+"'><img src='images/option01.png' style='width:30px; height:25px;'></a>");
					}
					else
						sb.append("<a data-original-title=' Apply Now' rel='tooltip' id='tpApply"+rowCount+"' href='applyteacherjob.do?jobId="+jsonObject.getString("jobId")+"' ><img src='images/option01.png' style='width:30px; height:25px;'></a>");
					//sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jsonObject.getString("jobId")+"'><img src='images/option04.png' style='width:30px; height:25px;'></a>");
					sb.append("</td>");

					sb.append("</tr>");
				 }
			    
				 if(hits.size()==0)
					{
						sb.append("<tr>");
						sb.append("<td colspan='6'>");

						sb.append("No record found.");
						sb.append("</td>");	   				
						sb.append("</tr>");
					}			

					sb.append("</table>");
					sb.append(PaginationAndSorting.getESPaginationStringForPanelAjax(request, totalRecord, noOfRow, pageNo));
					return sb.toString();
				//end copy paste
			    
			    

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		StringBuffer sb = new StringBuffer();
		sb.append("<table width='100%' border='0' id='tblGrid'>");
		sb.append("<thead class='bg'>");
		sb.append("<tr>");		
		String responseText="";
//			String sortOrderFieldName="jobId";
//			String sortOrderTypeVal	=	"0";
		
		responseText=PaginationAndSorting.responseSortingLinkES("Job ID",sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
		sb.append("<th  valign='top'>"+responseText+"</th>");			

		responseText=PaginationAndSorting.responseSortingLinkES("Title",sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
		sb.append("<th  valign='top'>"+responseText+"</th>");			
		
		if(iBranchId ==0 && iHeadQuarterId==0)
		{
			responseText=PaginationAndSorting.responseSortingLinkES("Zone",sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");			

			responseText=PaginationAndSorting.responseSortingLinkES("Subject",sortOrderFieldName,"subjectName",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>"); 

			responseText=PaginationAndSorting.responseSortingLinkES("School",sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");	
		}
		else
		{
			responseText=PaginationAndSorting.responseSortingLinkES("District",sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");
		}
				

		responseText=PaginationAndSorting.responseSortingLinkES("Address",sortOrderFieldName,"location",sortOrderTypeVal,pgNo);
		sb.append("<th  valign='top'>"+responseText+"</th>");

		responseText=PaginationAndSorting.responseSortingLinkES("End Date",sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
		sb.append("<th  valign='top'>"+responseText+"</th>");

		sb.append("<th valign='top'>Actions/Apply</th>");
		sb.append("</tr>");
		sb.append("</thead>");
		int rowCount = 0;

		sb.append("<tr>");
		sb.append("<td colspan='6'>");

		sb.append("No record found.");
		sb.append("</td>");	   				
		sb.append("</tr>");
		sb.append("</table>");
		return sb.toString(); 
	}
	
	//updating whenever joborder is changes
	public void updateESDistrictJobBoardByJobId(JobOrder jobOrder)
	{
		System.out.println("updating districtjobboard");
		searchDataAndDelete(jobOrder.getJobId(),ElasticSearchConfig.indexForhqDistrictJobBoard);
		//Map<Integer,String> map=getDBDataForDistrictJobBoard(jobOrder);
		JSONObject obj=new JSONObject();
		obj.put("jobId", jobOrder.getJobId());
		obj.put("jobTitle", jobOrder.getJobTitle());
		obj.put("jobStartTime", jobOrder.getJobStartTime());
		obj.put("jobEndTime",jobOrder.getJobEndTime());
		if(jobOrder.getHeadQuarterMaster()!=null)
		{
			obj.put("headQuarterId", jobOrder.getHeadQuarterMaster().getHeadQuarterId());
			obj.put("headQuarterName", jobOrder.getHeadQuarterMaster().getHeadQuarterName());
		}
		else
		{
			obj.put("headQuarterId", jobOrder.getDistrictMaster().getHeadQuarterMaster()==null?0:jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId());
			obj.put("headQuarterName", "");
		}
		if(jobOrder.getBranchMaster()!=null)
		{
			obj.put("branchId", jobOrder.getBranchMaster().getBranchId());
			obj.put("branchName", jobOrder.getBranchMaster().getBranchName());
		}
		else
		{
			obj.put("branchId", 0);
			obj.put("branchName", "");
		}
		if(jobOrder.getDistrictMaster()!=null)
		{
			obj.put("address", jobOrder.getDistrictMaster().getAddress());
			obj.put("zipcode", jobOrder.getDistrictMaster().getZipCode());
		}
			
		else
		{
			obj.put("address", "");
			obj.put("zipcode", "");
		}
			
		
		if(jobOrder.getGeoZoneMaster()!=null)
			obj.put("geoZoneId", jobOrder.getGeoZoneMaster().getGeoZoneId());
		else
			obj.put("geoZoneId", 0);
		obj.put("createdForEntity", jobOrder.getCreatedForEntity());
		obj.put("isPoolJob", jobOrder.getIsPoolJob());
		if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getStateId()!=null)
			obj.put("stateId", jobOrder.getDistrictMaster().getStateId().getStateId());//for district i.e dm
		else
			obj.put("stateId", 0);//for district i.e dm
		//obj.put("jobEndDate", jobOrder.getJobEndDate());
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		obj.put("jobEndDate", sdf.format(jobOrder.getJobEndDate()));
		if(jobOrder.getDistrictMaster()!=null)
			obj.put("districtid", jobOrder.getDistrictMaster().getDistrictId());
		else
			obj.put("districtid", 0);
		if(jobOrder.getJobCategoryMaster()!=null)
		{
			obj.put("jobCategoryName", jobOrder.getJobCategoryMaster().getJobCategoryName());
			obj.put("jobCategoryId", jobOrder.getJobCategoryMaster().getJobCategoryId());
		}
			
		else
		{
			obj.put("jobCategoryName", "");
			obj.put("jobCategoryId", 0);
		}
			
		if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null)
			obj.put("parentJobCategoryId", jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
		else
			obj.put("parentJobCategoryId", 0);
		
		if(jobOrder.getDistrictMaster()!=null)
			obj.put("districtName", jobOrder.getDistrictMaster().getDistrictName());
		else
			obj.put("districtName", "");
		
		if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getStateId()!=null)
			obj.put("stateName", jobOrder.getDistrictMaster().getStateId().getStateName());
		else
			obj.put("stateName", "");
		if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getCityName()!=null)
			obj.put("cityName", jobOrder.getDistrictMaster().getCityName());
		else
			obj.put("cityName", "");
		
		
		
		if(jobOrder.getSubjectMaster()!=null)
		{
			obj.put("subjectName", jobOrder.getSubjectMaster().getSubjectName());
			obj.put("subjectId", jobOrder.getSubjectMaster().getSubjectId());
		}
			
		else
		{
			obj.put("subjectName", "");
			obj.put("subjectId", "");
		}
			
		
		if(jobOrder.getGeoZoneMaster()!=null)
			obj.put("geoZoneName", jobOrder.getGeoZoneMaster().getGeoZoneName());
		else
			obj.put("geoZoneName", "");
		if(jobOrder.getJobId()==6544 || jobOrder.getJobId()== 6545)
			obj.put("priority", 2);
		else
			obj.put("priority", 1);
		
		if(jobOrder.getPositionStart()!=null)
		{
			obj.put("position", jobOrder.getPositionStart());
		}
		else
		{
			obj.put("position", "");
		}
		
		JSONArray schoolId = new JSONArray();
		JSONArray schoolStateName = new JSONArray();
		JSONArray schoolCityName = new JSONArray();
		JSONArray schoolAddress = new JSONArray();
		JSONArray schoolZip = new JSONArray();
		JSONArray schoolName = new JSONArray();
		for(SchoolMaster schoolMaster:jobOrder.getSchool())
		{
			schoolId.add(schoolMaster.getSchoolId());
			if(schoolMaster.getStateMaster()!=null)
			{
				schoolStateName.add(schoolMaster.getStateMaster().getStateName());
			}
			else
			{
				schoolStateName.add("");
			}
			if(schoolMaster.getCityName()!=null)
			{
				schoolCityName.add(schoolMaster.getCityName());
			}
			else
			{
				schoolCityName.add("");
			}
			if(schoolMaster.getAddress()!=null)
			{
				schoolAddress.add(schoolMaster.getAddress());
			}
			else
			{
				schoolAddress.add("");
			}
			if(schoolMaster.getZip()!=null)
			{
				schoolZip.add(schoolMaster.getZip());
			}
			else
			{
				schoolZip.add("");
			}
			schoolName.add(schoolMaster.getSchoolName());
		}	
		obj.put("schoolId", schoolId);
		obj.put("schoolStateName", schoolStateName);
		obj.put("schoolCityName", schoolCityName);
		obj.put("schoolAddress", schoolAddress);
		obj.put("schoolZip", schoolZip);
		obj.put("schoolName", schoolName);
		//map.put(rset.getInt("jo.jobid"), obj.toString());
		/*System.out.println(map.size());
		Set<Integer> set=map.keySet();
		for(Integer i:set)
		{
			System.out.println(i+"\tprinting job id");
		}
		System.out.println("inserting json===="+map.get(jobOrder.getJobId()));
		for(Integer i:set)
		update(ElasticSearchConfig.indexForhqDistrictJobBoard,"jdbc",jobOrder.getJobId().toString(),map.get(jobOrder.getJobId()));*/
		System.out.println("inserting json for districtjobboard"+obj.toString());
		update(ElasticSearchConfig.indexForhqDistrictJobBoard,"jdbc",jobOrder.getJobId().toString(),obj.toString());
	
	}
	
	//for searching data from elasticsearch
	public void searchDataAndDelete(int jobid,String index)
	{
		ClientConfig config = new DefaultClientConfig();
		URI uri=null;
	    Client client = Client.create(config);
	    try {
			uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+index+"/_search?pretty");
		} catch (URISyntaxException e) {
			
			e.printStackTrace();
		}
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;
	    String input = "{\"query\": { \"match\": {\"jobId\":"+jobid+"} },\"size\":1000}";
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
	    //System.out.println("Form response11 " + response.getEntity(String.class));
	    String s=response.getEntity(String.class);
	    //System.out.println(s);
	    //System.out.println("***********************************************************");
	  JSONObject jsonObject = new JSONObject();
		try {
			
			jsonObject=jsonObject.fromObject(s);
			JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
			for(int i=0; i<hits.size(); i++)
			 {
				jsonObject=(JSONObject)hits.get(i);
				String type=(String)jsonObject.get("_type");
				String id=(String)jsonObject.get("_id");
				jsonObject=(JSONObject)jsonObject.get("_source");
			//	System.out.println(jsonObject.toString());
				//update(index, type, id, json.toString());
				delete(index, type, id);
				
				

			}
			//System.out.println("outside for loop");


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   

	}

	
	//copy paste from basicController
	public String getDistrictFullAddressForES(JSONObject jsonObject)
	{
		String districtAddress="";
		String address="";
		String state="";
		String city = "";
		String zipcode="";

		if(!jsonObject.getString("districtName").equals("")){

			//Get district address
			if(!jsonObject.getString("districtName").equals(""))
			{
				if(!jsonObject.getString("address").equals("null") && !jsonObject.getString("address").equals(""))
				{
					if(!jsonObject.getString("cityName").equals(""))
					{
						districtAddress = jsonObject.getString("address")+", ";
					}
					else
					{
						districtAddress = jsonObject.getString("address");
					}
				}
				if(!jsonObject.getString("cityName").equals(""))
				{
					if(!jsonObject.getString("stateName").equals("null") && !jsonObject.getString("stateName").equals(""))
					{
						city = jsonObject.getString("cityName")+", ";
					}else{
						city = jsonObject.getString("cityName");
					}
				}
				if(jsonObject.getString("stateName").equals("null") && !jsonObject.getString("stateName").equals(""))
				{
					if(!jsonObject.getString("zipcode").equals(""))
					{
						state = jsonObject.getString("stateName")+", ";
					}
					else
					{
						state = jsonObject.getString("stateName");
					}	
				}
				if(!jsonObject.getString("zipcode").equals(""))
				{
					zipcode = jsonObject.getString("zipcode");
				}

				if(districtAddress !="" || city!="" ||state!="" || zipcode!=""){
					address = districtAddress+city+state+zipcode;
				}else{
					address="";
				}
			}
		}
		return address;
	}
	
	private String getSchoolIdFromJSONArray(JSONObject jsonObject)
	{
		JSONArray jsonArray=jsonObject.getJSONArray("schoolId");
		StringBuffer schoolId=new StringBuffer();
		for(int i=0;i<jsonArray.size();i++)
		{
			schoolId.append(jsonArray.getInt(i));
			if(i!=jsonArray.size()-1)
				schoolId.append(",");
		}
		return schoolId.toString();
	}
	
	private String getSchoolAddressFromJSONArray(JSONObject jsonObject,String schoolName)
	{
		JSONArray jsonArray=jsonObject.getJSONArray("schoolName");
		StringBuffer schoolId=new StringBuffer();
		
		for(int i=0;i<jsonArray.size();i++)
		{
			
			if(jsonArray.getString(i).equals(schoolName))
				jsonObject.getJSONArray("schoolAddress").getString(i);
			
		}
		return "";
	}


	
	
	//district job board end
	
	
	//districtjoborder start
	public void creatingStructureForDistrictJobOrder()
	{
		ClientConfig config = new DefaultClientConfig();
	    Client client = Client.create(config);
	    URI uri=null;
	    //uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+ElasticSearchConfig.indexForhqDistrictJobBoard+"/_search?pretty");
	    uri=getURIForStructureCr(ElasticSearchConfig.indexForDistrictJobOrder);    	
		WebResource service = client.resource(uri);
	    ClientResponse response = null;
	    String input = "{\"type\" : \"jdbc\"," +
	    				"\"strategy\" : \"simple\"," +
	    				  		"\"jdbc\" : " +
				    		"[" +
				    			"{\"url\" : \""+ElasticSearchConfig.sElasticSearchDBURL+"\"," +
				    			"\"user\" : \""+ElasticSearchConfig.sElasticSearchDBUser+"\",\"password\" : \""+ElasticSearchConfig.sElasticSearchDBPass+"\"," +
				    			  
				    			  
				    			  
									"\"sql\" : \""+districtJobOrderSQL+" limit 0,0 \"," +
									
				    			   "\"index\" : \""+ElasticSearchConfig.indexForDistrictJobOrder+"\"," +
				    			   //"\"autocommit\" : true," +
				    			   " \"fetchsize\" : 30" +
				    			  "}" +
				    		  "]" +
	    			  "}";
	    
	    //System.out.println(input);
	    
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
	    System.out.println("Form response11 " + response.getEntity(String.class));
	    System.out.println("****************************aaaaaaaaaaaaaaaa*******************************");

	}
	
	public Map<Integer,String> getDBDataForDistrictJobOrder(JobOrderDAO jobOrderDAO)
	{
		try
		{
			List<JobOrder> lstJobOrder	  =	 new ArrayList<JobOrder>();
			//System.out.println(jobOrderDAO);
			lstJobOrder=jobOrderDAO.findByCriteria(Order.asc("jobId"),Restrictions.eq("createdForEntity",2));
			System.out.println("districtjoborder size==============="+lstJobOrder.size());
			Map<Integer,String> map=new HashMap<Integer, String>();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			
			for(JobOrder jobOrderDetails:lstJobOrder)
			{
				JSONObject obj=new JSONObject();
				obj.put("jobId", jobOrderDetails.getJobId());
				obj.put("isPoolJob", jobOrderDetails.getIsPoolJob());
				obj.put("jobTitle", jobOrderDetails.getJobTitle());
				obj.put("districtId", jobOrderDetails.getDistrictMaster().getDistrictId());
				obj.put("districtName", jobOrderDetails.getDistrictMaster().getDistrictName());
				obj.put("status", jobOrderDetails.getStatus());
				obj.put("jobEndDate", sdf.format(jobOrderDetails.getJobEndDate()));
				if(jobOrderDetails.getDistrictAttachment()!=null)
					obj.put("districtAttachment", jobOrderDetails.getDistrictAttachment());
				else
					obj.put("districtAttachment", "");
				obj.put("IsInviteOnly", jobOrderDetails.getIsInviteOnly());
				if(jobOrderDetails.getRequisitionNumber()!=null)
				{
					obj.put("requisitionNumber", jobOrderDetails.getRequisitionNumber());
				}
				else
				{
					obj.put("requisitionNumber", "");
				}
				
				if(jobOrderDetails.getSubjectMaster()!=null)
					obj.put("subjectName", jobOrderDetails.getSubjectMaster().getSubjectName());
				else
					obj.put("subjectName", "");
				if(jobOrderDetails.getGeoZoneMaster()!=null)
				{
					obj.put("geoZoneDistrictId", jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId());
					obj.put("geoZoneName", jobOrderDetails.getGeoZoneMaster().getGeoZoneName());
					obj.put("geoZoneId", jobOrderDetails.getGeoZoneMaster().getGeoZoneId());
				}
					
				else
				{
					obj.put("geoZoneDistrictId", 0);
					obj.put("geoZoneDistrictId", 0);
					obj.put("geoZoneName", "");
					obj.put("geoZoneId", 0);
				}
				if(jobOrderDetails.getJobCategoryMaster()!=null)
				{
					obj.put("jobCategory", jobOrderDetails.getJobCategoryMaster().getJobCategoryName());
				}
				else
				{
					obj.put("jobCategory", "");
				}
				JSONArray schoolName = new JSONArray();
				JSONArray schoolId = new JSONArray();
				if(jobOrderDetails.getSchool()!=null)
				{
					for(SchoolMaster school:jobOrderDetails.getSchool())
					{
						schoolName.add(school.getSchoolName());
						schoolId.add(school.getSchoolId());
					}
					
				}
				obj.put("schoolName", schoolName);
				obj.put("schoolId", schoolId);
					
				
				//System.out.println(obj.toString());
				map.put(jobOrderDetails.getJobId(), obj.toString());
			}
			System.out.println("districtjoborder map size==="+map.size());
			return map;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	 public void createDistrictJobOrder(Map<Integer, String> dataBaseMap)
		{
		 try
		 {
			 System.out.println("creating createESDistrictJobOrder");
			 
			  int failJobNo=0;
				System.out.println("*****************districtjobboard insertion start**************");
				Set<Integer> set=dataBaseMap.keySet();
				for(Integer i:set)
				{
					ClientConfig config = new DefaultClientConfig();
				    Client client = Client.create(config);
				    URI uri=null;
				    try {
				    	
							uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+ElasticSearchConfig.indexForDistrictJobOrder+"/jdbc/"+i+"?pretty");
						} catch (URISyntaxException e) {
						failJobNo++;
						System.out.println("failed jobid="+i);
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    WebResource service = client.resource(uri);
				    ClientResponse response = null;
				    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).put(ClientResponse.class, dataBaseMap.get(i));
				  //  System.out.println("Form response11 " + response.getEntity(String.class));
				}
				System.out.println("Total job id fail in creating=="+failJobNo);
				System.out.println("****************districtjobboard insertion end************");
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
			
			
		}
	 
	 public Map<Integer, String> esForDistrictJobOrder()
		{
			//System.out.println("Calling esForDistrictJobBoard");
			Map<Integer, String> map=new HashMap<Integer, String>();
			indexMapForDistrictJobOrder=new HashMap<Integer, String>();
			ClientConfig config = new DefaultClientConfig();
			URI uri=null;
		    Client client = Client.create(config);
		    uri=getURIForSearch(ElasticSearchConfig.indexForDistrictJobOrder);
			WebResource service = client.resource(uri);
		    ClientResponse response = null;
		    String input = "{\"query\": { \"match_all\": {} },\"size\":100000}";
		    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
		    String s=response.getEntity(String.class);
		 //   System.out.println(s);
		    
		    JSONObject jsonObject=new JSONObject();
		    try {
				
				//System.out.println(((JSONObject)jsonObject.get("hits")));
				 //System.out.println(((JSONObject)jsonObject.get("hits")).get("hits"));
		    	jsonObject=jsonObject.fromObject(s);
				 JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
				// System.out.println("hits size==="+hits.size());
				 for(int i=0; i<hits.size(); i++)
				 {
					jsonObject=(JSONObject)hits.get(i);
					String index=(String)jsonObject.get("_index");
					String type=(String)jsonObject.get("_type");
					String id=(String)jsonObject.get("_id");
					jsonObject=(JSONObject)jsonObject.get("_source");
					//System.out.println(jsonObject.toString());
					//System.out.println(jsonObject.get("jobid").toString()+"_"+jsonObject.get("schoolId")+"_"+jsonObject.get("certTypeId"));
					map.put(jsonObject.getInt("jobId"), jsonObject.toString());
					indexMapForDistrictJobOrder.put(jsonObject.getInt("jobId"), index+":"+type+":"+id);
				}
				


			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//System.out.println("document size="+map.size());
			
			return map;
			
		}

	 
	 public void updateESForDistrictJobOrder(Map<Integer, String> databaseMap,Map<Integer, String> elasticMap)
		{
			System.out.println("*************************************");
			System.out.println(databaseMap.size()+"\t"+elasticMap.size());
			System.out.println("Calling compareData updateESForDistrictJobOrder");
			int x=0;
			int y=0;
			int z=0;
			//System.out.println(databaseMap.get(1932));
			//System.out.println(elasticMap.get(1932));
			//System.out.println(databaseMap.get(1932).equalsIgnoreCase(databaseMap.get(1932)));
			Set<Integer> databaseSet=databaseMap.keySet();
			for(Integer i:databaseSet)
			{
				//System.out.println(elasticMap.get(i));
				//System.out.println(databaseMap.get(i));
				//System.out.println("*****************************");
				
				//if data is not present in elasticsearch then insert
				if(elasticMap.get(i)==null)
				{
					//System.out.println("**********************creating**************************"+i.toString());
					//System.out.println(databaseMap.get(i));
					x++;
					//System.out.println(i.toString());
					update(ElasticSearchConfig.indexForDistrictJobOrder,"jdbc",i.toString(),databaseMap.get(i));
				}
				//if data is present then update
				else if(!(elasticMap.get(i)).equals(databaseMap.get(i)))
				{
					
					y++;
					//System.out.println("***************updating*****************");
					//	System.out.println("updating key="+i);
					//	System.out.println(elasticMap.get(i));
					//	System.out.println(databaseMap.get(i));
						//System.out.println("elastic key="+indexMap.get(i));
						//break;
						//System.out.println(elasticMap.get(i).replaceAll("\n", " "));
						//System.out.println(databaseMap.get(i).replaceAll("\n", " "));
						update(indexMapForDistrictJobOrder.get(i).split(":")[0],indexMapForDistrictJobOrder.get(i).split(":")[1],indexMapForDistrictJobOrder.get(i).split(":")[2],databaseMap.get(i));
						
					
					
				}
				
				
				
				
			}
			
		//	System.out.println(elasticMap.size()+"\t"+databaseMap.size());
			//for deleting from elasticsearch
			Set<Integer> elasticSet=elasticMap.keySet();
			for(Integer i:elasticSet)
			{
				if(databaseMap.get(i)==null)
				{
					
						//System.out.println("**************************deleting********************"+i);
						//System.out.println(databaseMap.get(i));
						z++;
					
					
					delete(indexMapForDistrictJobOrder.get(i).split(":")[0],indexMapForDistrictJobOrder.get(i).split(":")[1],indexMapForDistrictJobOrder.get(i).split(":")[2]);
				}
					
				
				
			}
			System.out.println("databaseMap size for districtjoborder="+databaseMap.size());
			System.out.println("elasticMap size  for districtjoborder="+elasticMap.size());
			System.out.println("indexMap size  for districtjoborder="+indexMapForDistrictJobOrder.size());
			System.out.println("creating x  for districtjoborder=="+x);
			System.out.println("updating y  for districtjoborder=="+y);
			System.out.println("deleting z  for districtjoborder=="+z);
		/*	System.out.println("iterating index map");
			Set<String> set=indexMap.keySet();
			for(String s:set)
			{
				System.out.println(s);
			}*/
		}
	
	 //jsonObject=elasticSearchService.esForDistrictJobOrder(districtOrSchoolId,schoolId,geoZonId, jobCategoryIds, 0, noOfRow, pageNo, pgNo, sortOrderFieldName, sortOrderTypeVal);
	 public JSONObject esForDistrictJobOrder(int districtId,int schoolId,int geoZonId,String jobCategoryNames,String status,String jobOrderIds,String disJobReqNo,
			 int from,String noOfRow,String pageNo,int pgNo,String sortOrderFieldName,String sortOrderTypeVal,String searchTerm)
		{
		 try
		 {
			 StringBuffer input =new StringBuffer();
				int noOfRowInPage	=	Integer.parseInt(noOfRow);
				int start 			= 	((pgNo-1)*noOfRowInPage);
				int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				
				end=noOfRowInPage;
				
					
					input.append("{\"query\": {\"bool\": {");
					  input.append(" \"must\" : [");
						/*if(districtName!=null && !districtName.trim().equals(""))
						{
							input.append("   {\"match_phrase\" : { \"districtName\" : \""+districtName+"\" }},");
						}*/
					  	if(districtId!=0 )
						{
							input.append("   {\"match_phrase\" : { \"districtId\" : "+districtId+" }},");
						}
						if(schoolId!=0 )
						{
							input.append("   {\"match_phrase\" : { \"schoolId\" : "+schoolId+" }},");
						}
						
						if(geoZonId!=0)
						{
							input.append("   {\"match_phrase\" : { \"geoZoneId\" : "+geoZonId+" }},");
						}
						if(!status.trim().equals(""))
						{
							input.append("   {\"match_phrase\" : { \"status\" : \""+status+"\" }},");
						}
						if(status.trim().equals(""))
						{
							input.append("   {\"match\" : { \"status\" : \"A,I\" }},");
						}
						if(!jobOrderIds.trim().equals(""))
						{
							input.append("   {\"match_phrase\" : { \"jobId\" : "+jobOrderIds+" }},");
						}
						
						if(jobCategoryNames!=null && !jobCategoryNames.trim().equals("") && (!jobCategoryNames.trim().equalsIgnoreCase("ALL")))
						{
							input.append("   {\"match\" : { \"jobCategory\" : \""+jobCategoryNames+"\" }},");
						}
						if(disJobReqNo!=null && !disJobReqNo.trim().equals("") )
						{
							input.append("   {\"match\" : { \"requisitionNumber\" : \""+disJobReqNo+"\" }},");
						}
						/*if(!searchTerm.trim().equals(""))
						{
							input.append("   {\"match\" : { \"jobTitle\" : \""+searchTerm+"\" }},");
						}*/
						/*if(!searchTerm.trim().equals(""))
						{
							input.append("   {\"match\" : { \"subjectName\" : \""+searchTerm+"\" }},");
						}*/
						
						if(input.indexOf(",")!=-1)
							input.replace(input.lastIndexOf(","), input.lastIndexOf(",")+1, "");
						input.append("],"
						
								+ "");
						/*input.append("\"should\": ["
					  			+ "{ \"match_phrase\": { \"jobTitle\":  \""+searchTerm+"\" }}"
					  			//+ "{ \"match_phrase\": { \"jobTitle\":  \""+searchTerm+"\" }},"
					  		//	+ "{ \"match_phrase\": { \"subjectName\": \""+searchTerm+"\"   }}"
						  		
					  		+ "],");*/
						input.append("\"should\": [{"
								+"  \"multi_match\" : {"
			    		        +"    \"fields\" : [\"jobTitle\",\"districtName\",\"districtAttachment\",\"subjectName\",\"geoZoneName\",\"jobCategory\"],"
			    		      +"      \"query\" : \""+searchTerm+"\","
			    		    +"        \"type\" : \"phrase_prefix\""
			    		  +"      }"
						  		
					  		+ "}"
					  		+ ","
					  		+ "{ \"match\": { \"jobTitle\":  \""+searchTerm+"\" }},"
					  		+ "{ \"match\": { \"districtName\":  \""+searchTerm+"\" }},"
					  		+ "{ \"match\": { \"jobCategory\":  \""+searchTerm+"\" }},"
					  		+ "{ \"match\": { \"districtAttachment\":  \""+searchTerm+"\" }},"
					  		+ "{ \"match\": { \"subjectName\":  \""+searchTerm+"\" }},"
					  		+ "{ \"match\": { \"geoZoneName\":  \""+searchTerm+"\" }}"
					  		+ "],");
					if(searchTerm.trim().equals(""))
						input.append("\"minimum_should_match\" : \"1%\"}},\"size\":"+end+",\"from\":"+start);
					else
						input.append("\"minimum_should_match\" : 1}},\"size\":"+end+",\"from\":"+start);
								if(searchTerm==null || searchTerm.trim().equals(""))
								{
									input.append(",\"sort\": { \""+sortOrderFieldName+"\": { \"order\": \""+sortOrderTypeVal+"\" }}");
								}
								
										input.append("}");
					//System.out.println(input);
				  		ClientConfig config = new DefaultClientConfig();
						URI uri=null;
					    Client client = Client.create(config);
					    List<String> list=new ArrayList<String>();
					    uri=getURIForSearch(ElasticSearchConfig.indexForDistrictJobOrder);
						WebResource service = client.resource(uri);
					    ClientResponse response = null;
					    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input.toString());
					    String s=response.getEntity(String.class);
					   // System.out.println(s);
					    JSONObject jsonObject = new JSONObject();
					    jsonObject=jsonObject.fromObject(s);
					    return jsonObject;
					   /* System.out.println("total districtjobOrder=="+((JSONObject)jsonObject.get("hits")).get("total"));
					    int totalRecord=((JSONObject)jsonObject.get("hits")).getInt("total");
					    JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
					    System.out.println("hits size===="+hits.size());
					    return hits;*/
					    /*for(int i=0; i<hits.size(); i++)
						 {
					    	
							jsonObject=(JSONObject)hits.get(i);
							String index=(String)jsonObject.get("_index");
							String type=(String)jsonObject.get("_type");
							String id=(String)jsonObject.get("_id");
							jsonObject=(JSONObject)jsonObject.get("_source");
						 }*/

		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
			return null;					
		}					 
	
	 //updating elasticsearch districtjoborder where job is created or updated
	 public void updateESDistrictJobOrderByJobId(JobOrder jobOrder)
	 {
		 System.out.println("in updateESDistrictJobOrderByJobId");
		 try
		 {
			 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				JSONObject obj=new JSONObject();
				obj.put("jobId", jobOrder.getJobId());
				obj.put("isPoolJob", jobOrder.getIsPoolJob());
				obj.put("jobTitle", jobOrder.getJobTitle());
				obj.put("districtId", jobOrder.getDistrictMaster().getDistrictId());
				obj.put("districtName", jobOrder.getDistrictMaster().getDistrictName());
				obj.put("status", jobOrder.getStatus());
				obj.put("jobEndDate", sdf.format(jobOrder.getJobEndDate()));
				if(jobOrder.getDistrictAttachment()!=null)
					obj.put("districtAttachment", jobOrder.getDistrictAttachment());
				else
					obj.put("districtAttachment", "");
				obj.put("IsInviteOnly", jobOrder.getIsInviteOnly());
				if(jobOrder.getRequisitionNumber()!=null)
				{
					obj.put("requisitionNumber", jobOrder.getRequisitionNumber());
				}
				else
				{
					obj.put("requisitionNumber", "");
				}
				
				if(jobOrder.getSubjectMaster()!=null)
					obj.put("subjectName", jobOrder.getSubjectMaster().getSubjectName());
				else
					obj.put("subjectName", "");
				if(jobOrder.getGeoZoneMaster()!=null)
				{
					obj.put("geoZoneDistrictId", jobOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId());
					obj.put("geoZoneName", jobOrder.getGeoZoneMaster().getGeoZoneName());
					obj.put("geoZoneId", jobOrder.getGeoZoneMaster().getGeoZoneId());
				}
					
				else
				{
					obj.put("geoZoneDistrictId", 0);
					obj.put("geoZoneDistrictId", 0);
					obj.put("geoZoneName", "");
					obj.put("geoZoneId", 0);
				}
				if(jobOrder.getJobCategoryMaster()!=null)
				{
					obj.put("jobCategory", jobOrder.getJobCategoryMaster().getJobCategoryName());
				}
				else
				{
					obj.put("jobCategory", "");
				}
				JSONArray schoolName = new JSONArray();
				JSONArray schoolId = new JSONArray();
				if(jobOrder.getSchool()!=null)
				{
					for(SchoolMaster school:jobOrder.getSchool())
					{
						schoolName.add(school.getSchoolName());
						schoolId.add(school.getSchoolId());
					}
					
				}
				obj.put("schoolName", schoolName);
				obj.put("schoolId", schoolId);
					
				//System.out.println("string to update in joborder");
				//System.out.println(obj.toString());
				update(ElasticSearchConfig.indexForDistrictJobOrder,"jdbc",jobOrder.getJobId().toString(),obj.toString());

		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		 			
		
	 }
	//districtjoborder  end
	 
	 
	
	//school in job order start
	 public void creatingStructureForSchoolJobOrder()
		{
			ClientConfig config = new DefaultClientConfig();
		    Client client = Client.create(config);
		    URI uri=null;
		    //uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+ElasticSearchConfig.indexForDistrictJobBoard+"/_search?pretty");
		    uri=getURIForStructureCr(ElasticSearchConfig.indexForschoolJobOrder);    	
			WebResource service = client.resource(uri);
		    ClientResponse response = null;
		    String input = "{\"type\" : \"jdbc\"," +
		    				"\"strategy\" : \"simple\"," +
		    				  		"\"jdbc\" : " +
					    		"[" +
					    			"{\"url\" : \""+ElasticSearchConfig.sElasticSearchDBURL+"\"," +
					    			"\"user\" : \""+ElasticSearchConfig.sElasticSearchDBUser+"\",\"password\" : \""+ElasticSearchConfig.sElasticSearchDBPass+"\"," +
					    			  
					    			  
					    			  
										"\"sql\" : \""+schoolJobOrderSQL+" limit 0,0 \"," +
										
					    			   "\"index\" : \""+ElasticSearchConfig.indexForschoolJobOrder+"\"," +
					    			   //"\"autocommit\" : true," +
					    			   " \"fetchsize\" : 30" +
					    			  "}" +
					    		  "]" +
		    			  "}";
		    
		    //System.out.println(input);
		    
		    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
		    //System.out.println("Form response11 " + response.getEntity(String.class));
		    System.out.println("****************************aaaaaaaaaaaaaaaa*******************************");

		}
	//----------------------------------------
	 public Map<Integer,String> getDBDataForSchoolJobOrder(JobOrderDAO jobOrderDAO,
			 	JobRequisitionNumbersDAO jobRequisitionNumbersDAO,int start)
		{
			try
			{
				
				Map<Integer, Object> hrStatusMap = new HashMap<Integer, Object>();
				List<JobOrder> allJoblist=new ArrayList<JobOrder>();
				Map<Integer,String> map=jobOrderDAO.findAllJob(hrStatusMap,start,jobRequisitionNumbersDAO);
				//Map<Integer,String> map=jobOrderDAO.findAllJobNew(hrStatusMap,start,jobRequisitionNumbersDAO);
				return map;
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return null;
		}
	 
	 public void createSchoolJobOrder(Map<Integer, String> dataBaseMap)
		{
		 try
		 {
			 System.out.println("creating createESSchoolJobOrder");
			 
			  int failJobNo=0;
				System.out.println("*****************schooljobOrder insertion start**************");
				Set<Integer> set=dataBaseMap.keySet();
				for(Integer i:set)
				{
					ClientConfig config = new DefaultClientConfig();
				    Client client = Client.create(config);
				    URI uri=null;
				    try {
				    	
							uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+ElasticSearchConfig.indexForschoolJobOrder+"/jdbc/"+i+"?pretty");
						} catch (URISyntaxException e) {
						failJobNo++;
						System.out.println("failed jobid="+i);
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    WebResource service = client.resource(uri);
				    ClientResponse response = null;
				    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).put(ClientResponse.class, dataBaseMap.get(i));
				  //  System.out.println("Form response11 " + response.getEntity(String.class));
				}
				System.out.println("Total job id fail in creating=="+failJobNo);
				System.out.println("****************schooljoborder insertion end************");
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
			
			
		}
	 public Map<Integer, String> esForSchoolJobOrder()
		{
			//System.out.println("Calling esForDistrictJobBoard");
			Map<Integer, String> map=new HashMap<Integer, String>();
			indexMapForSchoolJobOrder=new HashMap<Integer, String>();
			ClientConfig config = new DefaultClientConfig();
			URI uri=null;
		    Client client = Client.create(config);
		    uri=getURIForSearch(ElasticSearchConfig.indexForschoolJobOrder);
			WebResource service = client.resource(uri);
		    ClientResponse response = null;
		    String input = "{\"query\": { \"match_all\": {} },\"size\":10000000}";
		    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
		    String s=response.getEntity(String.class);
		    //System.out.println(s);
		    
		    JSONObject jsonObject=new JSONObject();
		    try {
				
				//System.out.println(((JSONObject)jsonObject.get("hits")));
				 //System.out.println(((JSONObject)jsonObject.get("hits")).get("hits"));
		    	jsonObject=jsonObject.fromObject(s);
				 JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
				// System.out.println("hits size==="+hits.size());
				 for(int i=0; i<hits.size(); i++)
				 {
					jsonObject=(JSONObject)hits.get(i);
					String index=(String)jsonObject.get("_index");
					String type=(String)jsonObject.get("_type");
					String id=(String)jsonObject.get("_id");
					jsonObject=(JSONObject)jsonObject.get("_source");
					//System.out.println(jsonObject.toString());
					//System.out.println(jsonObject.get("jobid").toString()+"_"+jsonObject.get("schoolId")+"_"+jsonObject.get("certTypeId"));
					map.put(jsonObject.getInt("jobId"), jsonObject.toString());
					indexMapForSchoolJobOrder.put(jsonObject.getInt("jobId"), index+":"+type+":"+id);
				}
				


			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//System.out.println("document size="+map.size());
			
			return map;
			
		}
	 public Map<Integer, String> esForSchoolJobOrderNew(String jobOrderIds)
		{
		 
		 	Map<Integer, String> map=new HashMap<Integer, String>();
			indexMapForSchoolJobOrder=new HashMap<Integer, String>();
			ClientConfig config = new DefaultClientConfig();
			URI uri=null;
		    Client client = Client.create(config);
		    uri=getURIForSearch(ElasticSearchConfig.indexForschoolJobOrder);
			WebResource service = client.resource(uri);
		    ClientResponse response = null;
		    //String input = "{\"query\": { \"match_all\": {} },\"size\":10000000}";
		    StringBuilder input=new StringBuilder();
		    input.append("{\"query\": {\"match\" : { \"jobId\" : \""+jobOrderIds+"\" }},\"size\":"+ElasticSearchConfig.rowSize+"");
		    input.append(",\"sort\": { \"jobIdOrder\": { \"order\": \"asc\" }}");
		    input.append("}");
		    //System.out.println(input.toString());
		    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input.toString());
		    String s=response.getEntity(String.class);
		    JSONObject jsonObject=new JSONObject();
		    try {
				
				jsonObject=jsonObject.fromObject(s);
				 JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
				 //System.out.println("hits size==="+hits.size());
				 for(int i=0; i<hits.size(); i++)
				 {
					jsonObject=(JSONObject)hits.get(i);
					String index=(String)jsonObject.get("_index");
					String type=(String)jsonObject.get("_type");
					String id=(String)jsonObject.get("_id");
					jsonObject=(JSONObject)jsonObject.get("_source");
					map.put(jsonObject.getInt("jobId"), jsonObject.toString());
					indexMapForSchoolJobOrder.put(jsonObject.getInt("jobId"), index+":"+type+":"+id);
				}
				


			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return map;
			
		}
	 
	 public void updateESForSchoolJobOrder(Map<Integer, String> databaseMap,Map<Integer, String> elasticMap)
		{
			//System.out.println("*************************************");
			//System.out.println(databaseMap.size()+"\t"+elasticMap.size());
			//System.out.println("Calling compareData updateESForSchoolJobOrder");
			int x=0;
			int y=0;
			int z=0;
			try
			{
				Set<Integer> databaseSet=databaseMap.keySet();
				
				for(Integer i:databaseSet)
				{
					
					//if data is not present in elasticsearch then insert
					if(elasticMap.get(i)==null)
					{
					
						x++;
						
						update(ElasticSearchConfig.indexForschoolJobOrder,"jdbc",i.toString(),databaseMap.get(i));
					}
					//if data is present then update
					else if(!(elasticMap.get(i)).equals(databaseMap.get(i)))
					{
						y++;
						//System.out.println("Elastic"+elasticMap.get(i));
						//System.out.println("Databas"+databaseMap.get(i));
						update(indexMapForSchoolJobOrder.get(i).split(":")[0],indexMapForSchoolJobOrder.get(i).split(":")[1],indexMapForSchoolJobOrder.get(i).split(":")[2],databaseMap.get(i));
					}
					
				}
				
			//	System.out.println(elasticMap.size()+"\t"+databaseMap.size());
				//for deleting from elasticsearch
				Set<Integer> elasticSet=elasticMap.keySet();
				for(Integer i:elasticSet)
				{
					if(databaseMap.get(i)==null)
					{
						
							//System.out.println("**************************deleting********************"+i);
							//System.out.println(databaseMap.get(i));
							z++;
							
						//delete(indexMapForSchoolJobOrder.get(i).split(":")[0],indexMapForSchoolJobOrder.get(i).split(":")[1],indexMapForSchoolJobOrder.get(i).split(":")[2]);
					}
						
					
					
				}
				System.out.println("databaseMap size for schooljoborder="+databaseMap.size());
				System.out.println("elasticMap size  for schooljoborder="+elasticMap.size());
				System.out.println("indexMap size  for schooljoborder="+indexMapForSchoolJobOrder.size());
				System.out.println("creating x  for schooljoborder=="+x);
				System.out.println("updating y  for schooljoborder=="+y);
				System.out.println("deleting z  for schooljoborder=="+z);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		
		}
	
	 public JSONObject esForSchoolJobOrder(String status,String jobOrderIds,String lstJobOrderIds,
			 int from,int noOfRowInPage,String sortOrderFieldName,String sortOrderTypeVal,String searchTerm,int jobOrderType)
		{
		 try
		 {
			 StringBuffer input =new StringBuffer();
				
								
					input.append("{\"query\": {\"bool\": {");
					  input.append(" \"must\" : [");
						
						
						if(status!=null && !status.trim().equals(""))
						{
							input.append("   {\"match_phrase\" : { \"status\" : \""+status+"\" }},");
						}
						if(status!=null && status.trim().equals(""))
						{
							input.append("   {\"match\" : { \"status\" : \"A,I,R\" }},");
						}
						if(!jobOrderIds.trim().equals(""))
						{
							input.append("   {\"match\" : { \"jobId\" : \""+jobOrderIds.replace(",", " ")+"\" }},");
						}
						if(!lstJobOrderIds.trim().equals(""))
						{
							input.append("   {\"match\" : { \"jobId\" : \""+lstJobOrderIds+"\" }},");
						}
						input.append("   {\"match\" : { \"createdForEntity\" : "+jobOrderType+" }},");
						
						
												
						if(input.indexOf(",")!=-1)
							input.replace(input.lastIndexOf(","), input.lastIndexOf(",")+1, "");
						input.append("],"
						
								+ "");
						/*input.append("\"should\": ["
					  			+ "{ \"match_phrase\": { \"jobTitle\":  \""+searchTerm+"\" }}"
					  			//+ "{ \"match_phrase\": { \"jobTitle\":  \""+searchTerm+"\" }},"
					  		//	+ "{ \"match_phrase\": { \"subjectName\": \""+searchTerm+"\"   }}"
						  		
					  		+ "],");*/
						input.append("\"should\": [{"
								+"  \"multi_match\" : {"
			    		        +"    \"fields\" : [\"jobTitle\",\"districtName\",\"districtAttachment\",\"subjectName\",\"geoZoneName\",\"jobCategory\""
			    		        + ",\"requisitionNumber\",\"schoolName\"],"
			    		      +"      \"query\" : \""+searchTerm+"\","
			    		    +"        \"type\" : \"phrase_prefix\""
			    		  +"      }"
						  		
					  		+ "}"
					  		+ ","
					  		+ "{ \"match\": { \"jobTitle\":  \""+searchTerm+"\" }},"
					  		+ "{ \"match\": { \"districtName\":  \""+searchTerm+"\" }},"
					  		+ "{ \"match\": { \"jobCategory\":  \""+searchTerm+"\" }},"
					  		+ "{ \"match\": { \"districtAttachment\":  \""+searchTerm+"\" }},"
					  		+ "{ \"match\": { \"subjectName\":  \""+searchTerm+"\" }},"
					  		+ "{ \"match\": { \"geoZoneName\":  \""+searchTerm+"\" }},"
					  		+ "{ \"match\": { \"requisitionNumber\":  \""+searchTerm+"\" }},"
					  		+ "{ \"match\": { \"schoolName\":  \""+searchTerm+"\" }}"
					  		+ "],");
						int start=0;
						int end=1000;
						if(sortOrderFieldName.trim().equalsIgnoreCase("jobId"))
						{
							sortOrderFieldName="jobIdOrder";
						
						}
						if(sortOrderFieldName.trim().equalsIgnoreCase("geoZoneMaster"))
						{
							sortOrderFieldName="geoZoneName";
						
						}
						
						if(sortOrderFieldName.trim().equalsIgnoreCase("subjectMaster"))
						{
							sortOrderFieldName="subjectName";
						
						}
						
						if(sortOrderTypeVal.trim().equals("0"))
							sortOrderTypeVal="desc";
						else
							sortOrderTypeVal="asc";
						
					if(searchTerm.trim().equals(""))
						input.append("\"minimum_should_match\" : \"1%\"}},\"size\":"+noOfRowInPage+",\"from\":"+from);
					else
						input.append("\"minimum_should_match\" : 1}},\"size\":"+noOfRowInPage+",\"from\":"+from);
						//if(searchTerm==null || searchTerm.trim().equals(""))
						//	{
						if(sortOrderFieldName.equalsIgnoreCase("jobTitle") || sortOrderFieldName.equalsIgnoreCase("subjectName") || sortOrderFieldName.equalsIgnoreCase("geoZoneName")
								|| sortOrderFieldName.equalsIgnoreCase("schoolName"))
							input.append(",\"sort\": {\"_script\": {\"script\": \"_source."+sortOrderFieldName+"\",\"type\": \"string\",\"order\": \""+sortOrderTypeVal+"\"}}");
							
						else
							input.append(",\"sort\": { \""+sortOrderFieldName+"\": { \"order\": \""+sortOrderTypeVal+"\" }}");
								//}
								
										input.append("}");
					//System.out.println(input);
				  		ClientConfig config = new DefaultClientConfig();
						URI uri=null;
					    Client client = Client.create(config);
					    List<String> list=new ArrayList<String>();
					    uri=getURIForSearch(ElasticSearchConfig.indexForschoolJobOrder);
						WebResource service = client.resource(uri);
					    ClientResponse response = null;
					    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input.toString());
					    String s=response.getEntity(String.class);
					 //   System.out.println(s);
					    JSONObject jsonObject = new JSONObject();
					    jsonObject=jsonObject.fromObject(s);
					
					       return jsonObject;
					   
					  /*   System.out.println("total districtjobOrder=="+((JSONObject)jsonObject.get("hits")).get("total"));
					    int totalRecord=((JSONObject)jsonObject.get("hits")).getInt("total");
					    JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
					    System.out.println("hits size===="+hits.size());
					   
					    for(int i=0; i<hits.size(); i++)
						 {
					    	
							jsonObject=(JSONObject)hits.get(i);
							String index=(String)jsonObject.get("_index");
							String type=(String)jsonObject.get("_type");
							String id=(String)jsonObject.get("_id");
							jsonObject=(JSONObject)jsonObject.get("_source");
						 }*/

		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
			return null;					
		}					 
	
	 
	 public void updateESSchoolJobOrderByJobId(JobOrder jobOrderDetails)
	 {
		 System.out.println("in updateESSchoolJobOrderByJobId");
		 try
		 {
			 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				JSONObject obj=new JSONObject();
				
				obj.put("jobIdOrder", jobOrderDetails.getJobId());
				obj.put("jobId", jobOrderDetails.getJobId().toString());
				obj.put("createdForEntity", jobOrderDetails.getCreatedForEntity());
				if(jobOrderDetails.getIsPoolJob()!=null)
					obj.put("isPoolJob", jobOrderDetails.getIsPoolJob());
				else
					obj.put("isPoolJob", 0);
				if(jobOrderDetails.getJobTitle()!=null)
					obj.put("jobTitle", jobOrderDetails.getJobTitle());
				else
					obj.put("jobTitle", "");
				if(jobOrderDetails.getDistrictMaster()!=null)
				{
					obj.put("districtName", jobOrderDetails.getDistrictMaster().getDistrictName());
					obj.put("districtId", jobOrderDetails.getDistrictMaster().getDistrictId());
				}
				else
				{
					obj.put("districtId", 0);
					obj.put("districtName", "");
				}
				obj.put("status", jobOrderDetails.getStatus());
				if(jobOrderDetails.getApprovalBeforeGoLive()!=null)
				{
					obj.put("approvalBeforeGoLive", jobOrderDetails.getApprovalBeforeGoLive());
				}
				else
					obj.put("approvalBeforeGoLive", 0);
				
				obj.put("jobEndDate", sdf.format(jobOrderDetails.getJobEndDate()));
				obj.put("jobStartDate", sdf.format(jobOrderDetails.getJobStartDate()));
				if(jobOrderDetails.getDistrictAttachment()!=null)
					obj.put("districtAttachment", jobOrderDetails.getDistrictAttachment());
				else
					obj.put("districtAttachment", "");
				if(jobOrderDetails.getIsInviteOnly()!=null)
					obj.put("IsInviteOnly", jobOrderDetails.getIsInviteOnly());
				else
					obj.put("IsInviteOnly", false);
				
				if(jobOrderDetails.getRequisitionNumber()!=null)
				{
					obj.put("requisitionNumber", jobOrderDetails.getRequisitionNumber());
				}
				else
				{
					obj.put("requisitionNumber", "");
				}
				
				if(jobOrderDetails.getSubjectMaster()!=null)
					obj.put("subjectName", jobOrderDetails.getSubjectMaster().getSubjectName());
				else
					obj.put("subjectName", "");
				if(jobOrderDetails.getGeoZoneMaster()!=null)
				{
					obj.put("geoZoneDistrictId", jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId());
					obj.put("geoZoneName", jobOrderDetails.getGeoZoneMaster().getGeoZoneName());
					obj.put("geoZoneId", jobOrderDetails.getGeoZoneMaster().getGeoZoneId());
				}
					
				else
				{
					obj.put("geoZoneDistrictId", 0);
					obj.put("geoZoneName", "");
					obj.put("geoZoneId", 0);
				}
				if(jobOrderDetails.getJobCategoryMaster()!=null)
				{
					obj.put("jobCategory", jobOrderDetails.getJobCategoryMaster().getJobCategoryName());
				}
				else
				{
					obj.put("jobCategory", "");
				}
				JSONArray schoolName = new JSONArray();
				JSONArray schoolId = new JSONArray();
				if(jobOrderDetails.getSchool()!=null)
				{
					for(SchoolMaster school:jobOrderDetails.getSchool())
					{
						schoolName.add(school.getSchoolName());
						schoolId.add(school.getSchoolId());
					}
					
				}
				obj.put("schoolName", schoolName);
				obj.put("schoolId", schoolId);
					
				//System.out.println("string to update in updateESSchoolJobOrderByJobId");
				//System.out.println(obj.toString());
				update(ElasticSearchConfig.indexForschoolJobOrder,"jdbc",jobOrderDetails.getJobId().toString(),obj.toString());

		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		 			
		
	 }
	 
	 //school in joborder end
	 
	 
	 
	 //jobsofinterest start
	 
		//not for candidate
public void creatingStructureForJobsOfInterest()
{
	ClientConfig config = new DefaultClientConfig();
 Client client = Client.create(config);
 URI uri=null;
 //uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+ElasticSearchConfig.indexForDistrictJobBoard+"/_search?pretty");
 uri=getURIForStructureCr(ElasticSearchConfig.indexForjobsOfInterest);    	
	WebResource service = client.resource(uri);
 ClientResponse response = null;
 String input = "{\"type\" : \"jdbc\"," +
 				"\"strategy\" : \"simple\"," +
 				  		"\"jdbc\" : " +
			    		"[" +
			    			"{\"url\" : \""+ElasticSearchConfig.sElasticSearchDBURL+"\"," +
			    			"\"user\" : \""+ElasticSearchConfig.sElasticSearchDBUser+"\",\"password\" : \""+ElasticSearchConfig.sElasticSearchDBPass+"\"," +
			    			  
			    			  
			    			  
								"\"sql\" : \""+jobsOfInterestSQL+" limit 0,0 \"," +
								
			    			   "\"index\" : \""+ElasticSearchConfig.indexForjobsOfInterest+"\"," +
			    			   //"\"autocommit\" : true," +
			    			   " \"fetchsize\" : 30" +
			    			  "}" +
			    		  "]" +
 			  "}";
 
 //System.out.println(input);
 
 response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
 //System.out.println("Form response11 " + response.getEntity(String.class));
 System.out.println("****************************jobsofinterest*******************************");

}

@Transactional
public Map<Integer,String> getDBDataForJobsOfInterest(JobOrderDAO jobOrderDAO)
{



	try
	{
		List<JobOrder> lstJobOrder	  =	 new ArrayList<JobOrder>();
		System.out.println(jobOrderDAO);
		Map<Integer,String> map=jobOrderDAO.findAllJobWithPoolConditionDup();
		System.out.println("=============================="+map.size());
		//lstJobOrder=jobOrderDAO.findAllJobWithPoolConditionDup();
		/*System.out.println("jobwithpoolcondition size==============="+lstJobOrder.size());
		Map<Integer,String> map=new HashMap<Integer, String>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		
		for(JobOrder jobOrderDetails:lstJobOrder)
		{
			JSONObject obj=new JSONObject();
			obj.put("jobId", jobOrderDetails.getJobId());
			obj.put("isPoolJob", jobOrderDetails.getIsPoolJob());
			obj.put("jobTitle", jobOrderDetails.getJobTitle());
			obj.put("createdForEntity", jobOrderDetails.getCreatedForEntity());
			obj.put("districtId", jobOrderDetails.getDistrictMaster().getDistrictId());
			obj.put("districtName", jobOrderDetails.getDistrictMaster().getDistrictName());
			obj.put("zipCode", jobOrderDetails.getDistrictMaster().getZipCode());
			//tommorow if null check
			if(jobOrderDetails.getDistrictMaster().getCityName()!=null)
				obj.put("disCityName", jobOrderDetails.getDistrictMaster().getCityName());
			else
				obj.put("disCityName", "");
			if(jobOrderDetails.getDistrictMaster().getAddress()!=null)
				obj.put("distaddress", jobOrderDetails.getDistrictMaster().getAddress());
			else
				obj.put("distaddress", "");
			obj.put("status", jobOrderDetails.getStatus());
			if(jobOrderDetails.getIpAddress()!=null)
				obj.put("ipAddress", jobOrderDetails.getIpAddress());
			else
				obj.put("ipAddress", "");
			obj.put("jobEndDate", sdf.format(jobOrderDetails.getJobEndDate()));
			if(jobOrderDetails.getDistrictMaster().getDisplayName()!=null)
				obj.put("displayName", jobOrderDetails.getDistrictMaster().getDisplayName());
			else
				obj.put("displayName", "");
			if(jobOrderDetails.getDistrictMaster().getStateId()!=null )
				obj.put("disStateName", jobOrderDetails.getDistrictMaster().getStateId().getStateName());
			else
				obj.put("disStateName", "");
			
			
			if(jobOrderDetails.getSubjectMaster()!=null)
				obj.put("subjectName", jobOrderDetails.getSubjectMaster().getSubjectName());
			else
				obj.put("subjectName", "");
			if(jobOrderDetails.getSubjectMaster()!=null)
				obj.put("subjectId", jobOrderDetails.getSubjectMaster().getSubjectId().toString());
			else
				obj.put("subjectId", "");
			
			
			if(jobOrderDetails.getGeoZoneMaster()!=null)
			{
				obj.put("geoZoneDistrictId", jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId());
				obj.put("geoZoneName", jobOrderDetails.getGeoZoneMaster().getGeoZoneName());
				obj.put("geoZoneId", jobOrderDetails.getGeoZoneMaster().getGeoZoneId());
			}
				
			else
			{
				obj.put("geoZoneDistrictId", 0);
				obj.put("geoZoneName", "");
				obj.put("geoZoneId", 0);
			}
		
			JSONArray schoolName = new JSONArray();
			JSONArray schoolId = new JSONArray();
			JSONArray schoolAddress = new JSONArray();
			JSONArray schoolZip = new JSONArray();
			JSONArray schoolCityName = new JSONArray();
			JSONArray schoolStateName = new JSONArray();
			JSONArray schoolGeoMapId = new JSONArray();
			JSONArray schoolSchoolTypeId = new JSONArray();
			JSONArray schoolRegionId = new JSONArray();
			if(jobOrderDetails.getSchool()!=null)
			{
				for(SchoolMaster school:jobOrderDetails.getSchool())
				{
					schoolGeoMapId.add(school.getGeoMapping().getGeoId().getGeoId()+"");
					schoolSchoolTypeId.add(school.getSchoolTypeId().getSchoolTypeId()+"");
					schoolRegionId.add(school.getRegionId().getRegionId()+"");
					
					schoolName.add(school.getSchoolName());
					schoolId.add(school.getSchoolId());
					if(school.getAddress()!=null)
						schoolAddress.add(school.getAddress());
					else
						schoolAddress.add("");
					if(school.getZip()!=null)
						schoolZip.add(school.getZip());
					else
						schoolZip.add(0);
					if(schoolCityName!=null)
						schoolCityName.add(school.getCityName());
					else
						schoolCityName.add("");
					if(school.getStateMaster()!=null)
						schoolStateName.add(school.getStateMaster().getStateName());
					else
						schoolStateName.add("");
				}
				
			}
			obj.put("schoolName", schoolName);
			obj.put("schoolId", schoolId);
			obj.put("schoolAddress", schoolAddress);
			obj.put("schoolZip", schoolZip);
			obj.put("schoolCityName", schoolCityName);
			obj.put("schoolStateName", schoolStateName);
			obj.put("schoolGeoMapId", schoolGeoMapId);
			obj.put("schoolSchoolTypeId", schoolSchoolTypeId);
			obj.put("schoolRegionId", schoolRegionId);
			
				
			
			//System.out.println(obj.toString());
			map.put(jobOrderDetails.getJobId(), obj.toString());
		}
		System.out.println("jobsofinterest map size==="+map.size());*/
		return map;
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return null;

}
public void createJobsOfInterest(Map<Integer, String> dataBaseMap)
{
try
{
	 System.out.println("creating createESDistrictJobOrder");
	 
	  int failJobNo=0;
		System.out.println("*****************jobsofinterest insertion start**************");
		Set<Integer> set=dataBaseMap.keySet();
		for(Integer i:set)
		{
			ClientConfig config = new DefaultClientConfig();
		    Client client = Client.create(config);
		    URI uri=null;
		    try {
		    	
					uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+ElasticSearchConfig.indexForjobsOfInterest+"/jdbc/"+i+"?pretty");
				} catch (URISyntaxException e) {
				failJobNo++;
				System.out.println("failed jobid="+i);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    WebResource service = client.resource(uri);
		    ClientResponse response = null;
		    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).put(ClientResponse.class, dataBaseMap.get(i));
		  //  System.out.println("Form response11 " + response.getEntity(String.class));
		}
		System.out.println("Total job id fail in creating=="+failJobNo);
		System.out.println("****************jobsofinterest insertion end************");
}
catch(Exception e)
{
	 e.printStackTrace();
}
	
	
}

public JSONObject esForJobsOfInterest(int districtId,int schoolId,int geoZonId,
	 int from,String noOfRow,String pageNo,int pgNo,String sortOrderFieldName,String sortOrderTypeVal,String searchTerm,String subjectName,String subjectId,String stateName,String cityName,boolean isImcandidate)
{
try
{
	 boolean match=true;
	 StringBuffer input =new StringBuffer();
		int noOfRowInPage	=	Integer.parseInt(noOfRow);
		int start 			= 	((pgNo-1)*noOfRowInPage);
		int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		
		end=noOfRowInPage;
		
			
			input.append("{\"query\": {\"bool\": {");
			  input.append(" \"must\" : [");
				/*if(districtName!=null && !districtName.trim().equals(""))
				{
					input.append("   {\"match_phrase\" : { \"districtName\" : \""+districtName+"\" }},");
				}*/
			  	if(districtId!=0 )
				{
			  		match=false;
					input.append("   {\"match_phrase\" : { \"districtId\" : "+districtId+" }},");
				}
				if(schoolId!=0 )
				{
					match=false;
					input.append("   {\"match_phrase\" : { \"schoolId\" : "+schoolId+" }},");
				}
				
				if(geoZonId!=-1 && geoZonId!=0)
				{
					match=false;
					input.append("   {\"match_phrase\" : { \"geoZoneId\" : "+geoZonId+" }},");
				}
				
				if(stateName!=null && !stateName.trim().equals("") && !stateName.equalsIgnoreCase("0") && !stateName.equalsIgnoreCase("ALL"))
				{
					match=false;
					input.append("   {\"match_phrase\" : { \"disStateName\" : \""+stateName+"\" }},");
				}
				if(subjectName!=null && !subjectName.trim().equals("") && !subjectName.equalsIgnoreCase("0") && !subjectName.equalsIgnoreCase("ALL"))
				{
					match=false;
					input.append("   {\"match\" : { \"subjectId\" : \""+subjectId+"\" }},");
				}
				if(cityName!=null && !cityName.trim().equals("") && !cityName.equalsIgnoreCase("0") && !cityName.equalsIgnoreCase("ALL"))
				{
					match=false;
					input.append("   {\"match_phrase\" : { \"disCityName\" : \""+cityName+"\" }},");
				}
										
					match=false;
					
					
					input.append("   {\"match_phrase\" : { \"isInviteOnly\" : "+false+" }},");
					if(!isImcandidate)
					{
						input.append("   {\"match_phrase\" : { \"isVacancyJob\" : "+false+" }},");
						input.append("   {\"match_phrase\" : { \"approvalBeforeGoLive\" : "+1+" }},");
					}
					
					//input.append("   {\"match_phrase\" : { \"isPoolJob\" : "+1+" }},");
				
				if(input.indexOf(",")!=-1)
					input.replace(input.lastIndexOf(","), input.lastIndexOf(",")+1, "");
				input.append("],"
				
						+ "");
				if(!isImcandidate)
				input.append("\"must_not\" : [{\"match_phrase\" : { \"isPoolJob\" : "+1+" }}],");
				/*input.append("\"should\": ["
			  			+ "{ \"match_phrase\": { \"jobTitle\":  \""+searchTerm+"\" }}"
			  			//+ "{ \"match_phrase\": { \"jobTitle\":  \""+searchTerm+"\" }},"
			  		//	+ "{ \"match_phrase\": { \"subjectName\": \""+searchTerm+"\"   }}"
				  		
			  		+ "],");*/
				input.append("\"should\": [{"
						+"  \"multi_match\" : {"
	    		        +"    \"fields\" : [\"jobTitle\",\"districtName\",\"subjectName\",\"geoZoneName\""
	    		        + ",\"zipCode\",\"disCityName\",\"distaddress\",\"disStateName\",\"schoolName\"],"
	    		      +"      \"query\" : \""+searchTerm+"\","
	    		    +"        \"type\" : \"phrase_prefix\""
	    		  +"      }"
				  		
			  		+ "}"
			  		+ ","
			  		+ "{ \"match\": { \"jobTitle\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"districtName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"subjectName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"geoZoneName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"zipCode\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"disCityName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"distaddress\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"disStateName\":  \""+searchTerm+"\" }},"
			  		+ "{ \"match\": { \"schoolName\":  \""+searchTerm+"\" }}"
			  		+ "],");
				end=10000;
				start=0;
			if(searchTerm.trim().equals(""))
				input.append("\"minimum_should_match\" : \"1%\"}},\"size\":"+end+",\"from\":"+start+",\"_source\": [\"jobId\"]");
			else
				input.append("\"minimum_should_match\" : 1}},\"size\":"+end+",\"from\":"+start+",\"_source\": [\"jobId\"]");
						if(searchTerm==null || searchTerm.trim().equals(""))
						{
							//input.append(",\"sort\": { \""+sortOrderFieldName+"\": { \"order\": \""+sortOrderTypeVal+"\" }}");
						}
						
								input.append("}");
				/*if(searchTerm.trim().equals("") && match)
				{
					input=new StringBuffer();
					input.append("{\"query\": { \"match_all\": {} },\"size\":"+end+",\"from\":"+start+",\"_source\": [\"jobId\"] }");
				}*/
					
			//System.out.println(input);
		  		ClientConfig config = new DefaultClientConfig();
				URI uri=null;
			    Client client = Client.create(config);
			    List<String> list=new ArrayList<String>();
			    uri=getURIForSearch(ElasticSearchConfig.indexForjobsOfInterest);
				WebResource service = client.resource(uri);
			    ClientResponse response = null;
			    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input.toString());
			    String s=response.getEntity(String.class);
			    //System.out.println(s);
			    JSONObject jsonObject = new JSONObject();
			    jsonObject=jsonObject.fromObject(s);
			    return jsonObject;
			   
}
catch(Exception e)
{
	 e.printStackTrace();
}
	return null;					
}					 

public Map<Integer, String> esForJobsOfInterestNotCandidateAll()
{
	//System.out.println("Calling esForDistrictJobBoard");
	Map<Integer, String> map=new HashMap<Integer, String>();
	indexMapForJobsOfInterestNotCandidate=new HashMap<Integer, String>();
	ClientConfig config = new DefaultClientConfig();
	URI uri=null;
 Client client = Client.create(config);
 uri=getURIForSearch(ElasticSearchConfig.indexForjobsOfInterest);
	WebResource service = client.resource(uri);
 ClientResponse response = null;
 String input = "{\"query\": { \"match_all\": {} },\"size\":100000}";
 response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
 String s=response.getEntity(String.class);
//   System.out.println(s);
 
 JSONObject jsonObject=new JSONObject();
 try {
		
		//System.out.println(((JSONObject)jsonObject.get("hits")));
		 //System.out.println(((JSONObject)jsonObject.get("hits")).get("hits"));
 	jsonObject=jsonObject.fromObject(s);
		 JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
		// System.out.println("hits size==="+hits.size());
		 for(int i=0; i<hits.size(); i++)
		 {
			jsonObject=(JSONObject)hits.get(i);
			String index=(String)jsonObject.get("_index");
			String type=(String)jsonObject.get("_type");
			String id=(String)jsonObject.get("_id");
			jsonObject=(JSONObject)jsonObject.get("_source");
			//System.out.println(jsonObject.toString());
			//System.out.println(jsonObject.get("jobid").toString()+"_"+jsonObject.get("schoolId")+"_"+jsonObject.get("certTypeId"));
			map.put(jsonObject.getInt("jobId"), jsonObject.toString());
			indexMapForJobsOfInterestNotCandidate.put(jsonObject.getInt("jobId"), index+":"+type+":"+id);
		}
		


	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	//System.out.println("document size="+map.size());
	
	return map;
	
}

public void updateESForJobsOfInterestNotCandidate(Map<Integer, String> databaseMap,Map<Integer, String> elasticMap)
{
	System.out.println("*************************************");
	System.out.println(databaseMap.size());
	System.out.println(elasticMap.size());
	System.out.println(databaseMap.size()+"\t"+elasticMap.size());
	System.out.println("Calling compareData updateESForJobsOfInterestNotCandidate");
	int x=0;
	int y=0;
	int z=0;
	Set<Integer> databaseSet=databaseMap.keySet();
	for(Integer i:databaseSet)
	{
		//System.out.println(elasticMap.get(i));
		//System.out.println(databaseMap.get(i));
		//System.out.println("*****************************");
		
		//if data is not present in elasticsearch then insert
		if(elasticMap.get(i)==null)
		{
			//System.out.println("**********************creating**************************"+i.toString());
			//System.out.println(databaseMap.get(i));
			x++;
			//System.out.println(i.toString());
			update(ElasticSearchConfig.indexForjobsOfInterest,"jdbc",i.toString(),databaseMap.get(i));
		}
		//if data is present then update
		else if(!(elasticMap.get(i)).equals(databaseMap.get(i)))
		{
			
			y++;
			update(indexMapForJobsOfInterestNotCandidate.get(i).split(":")[0],indexMapForJobsOfInterestNotCandidate.get(i).split(":")[1],indexMapForJobsOfInterestNotCandidate.get(i).split(":")[2],databaseMap.get(i));
				
			
			
		}
		
		
		
		
	}
	
//	System.out.println(elasticMap.size()+"\t"+databaseMap.size());
	//for deleting from elasticsearch
	Set<Integer> elasticSet=elasticMap.keySet();
	for(Integer i:elasticSet)
	{
		if(databaseMap.get(i)==null)
		{
			
				//System.out.println("**************************deleting********************"+i);
				//System.out.println(databaseMap.get(i));
				z++;
			
			
			delete(indexMapForJobsOfInterestNotCandidate.get(i).split(":")[0],indexMapForJobsOfInterestNotCandidate.get(i).split(":")[1],indexMapForJobsOfInterestNotCandidate.get(i).split(":")[2]);
		}
			
		
		
	}
	System.out.println("databaseMap size for JobsOfInterestNotCandidate="+databaseMap.size());
	System.out.println("elasticMap size  for JobsOfInterestNotCandidate="+elasticMap.size());
	System.out.println("indexMap size  for JobsOfInterestNotCandidate="+indexMapForJobsOfInterestNotCandidate.size());
	System.out.println("creating x  for JobsOfInterestNotCandidate=="+x);
	System.out.println("updating y  for JobsOfInterestNotCandidate=="+y);
	System.out.println("deleting z  for JobsOfInterestNotCandidate=="+z);
/*	System.out.println("iterating index map");
	Set<String> set=indexMap.keySet();
	for(String s:set)
	{
		System.out.println(s);
	}*/
}

public void updateESForJobsOfInterestNotCandidate(JobOrder jobOrderDetails)
{
	System.out.println("updating updateESForJobsOfInterestNotCandidate");
	searchDataAndDelete(jobOrderDetails.getJobId(),ElasticSearchConfig.indexForjobsOfInterest);
	
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	//Map<Integer,String> map=getDBDataForDistrictJobBoard(jobOrder);
	JSONObject obj=new JSONObject();
	obj.put("jobId", jobOrderDetails.getJobId());
	if(jobOrderDetails.getIsPoolJob()!=null)
		obj.put("isPoolJob", jobOrderDetails.getIsPoolJob());
	else
		obj.put("isPoolJob", 0);
	obj.put("jobTitle", jobOrderDetails.getJobTitle());
	obj.put("createdForEntity", jobOrderDetails.getCreatedForEntity());
	obj.put("districtId", jobOrderDetails.getDistrictMaster().getDistrictId());
	obj.put("districtName", jobOrderDetails.getDistrictMaster().getDistrictName());
	obj.put("zipCode", jobOrderDetails.getDistrictMaster().getZipCode());
	
	if(jobOrderDetails.getIsInviteOnly()!=null)
		obj.put("isInviteOnly", jobOrderDetails.getIsInviteOnly());
	else
		obj.put("isInviteOnly", false);
	
	if(jobOrderDetails.getIsVacancyJob()!=null)
		obj.put("isVacancyJob", jobOrderDetails.getIsVacancyJob());
	else
		obj.put("isVacancyJob", false);
	
	if(jobOrderDetails.getApprovalBeforeGoLive()!=null)
		obj.put("approvalBeforeGoLive", jobOrderDetails.getApprovalBeforeGoLive());
	else
		obj.put("approvalBeforeGoLive", 0);
	
	
	if(jobOrderDetails.getDistrictMaster().getCityName()!=null)
		obj.put("disCityName", jobOrderDetails.getDistrictMaster().getCityName());
	else
		obj.put("disCityName", "");
	if(jobOrderDetails.getDistrictMaster().getAddress()!=null)
		obj.put("distaddress", jobOrderDetails.getDistrictMaster().getAddress());
	else
		obj.put("distaddress", "");
	obj.put("status", jobOrderDetails.getStatus());
	if(jobOrderDetails.getIpAddress()!=null)
		obj.put("ipAddress", jobOrderDetails.getIpAddress());
	else
		obj.put("ipAddress", "");
	obj.put("jobEndDate", sdf.format(jobOrderDetails.getJobEndDate()));
	if(jobOrderDetails.getDistrictMaster().getDisplayName()!=null)
		obj.put("displayName", jobOrderDetails.getDistrictMaster().getDisplayName());
	else
		obj.put("displayName", "");
	if(jobOrderDetails.getDistrictMaster().getStateId()!=null )
		obj.put("disStateName", jobOrderDetails.getDistrictMaster().getStateId().getStateName());
	else
		obj.put("disStateName", "");
	
	
	if(jobOrderDetails.getSubjectMaster()!=null)
		obj.put("subjectName", jobOrderDetails.getSubjectMaster().getSubjectName());
	else
		obj.put("subjectName", "");
	if(jobOrderDetails.getSubjectMaster()!=null)
		obj.put("subjectId", jobOrderDetails.getSubjectMaster().getSubjectId().toString());
	else
		obj.put("subjectId", "");
	
	
	if(jobOrderDetails.getGeoZoneMaster()!=null)
	{
		obj.put("geoZoneDistrictId", jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId());
		obj.put("geoZoneName", jobOrderDetails.getGeoZoneMaster().getGeoZoneName());
		obj.put("geoZoneId", jobOrderDetails.getGeoZoneMaster().getGeoZoneId());
	}
		
	else
	{
		obj.put("geoZoneDistrictId", 0);
		obj.put("geoZoneName", "");
		obj.put("geoZoneId", 0);
	}

	JSONArray schoolName = new JSONArray();
	JSONArray schoolId = new JSONArray();
	JSONArray schoolAddress = new JSONArray();
	JSONArray schoolZip = new JSONArray();
	JSONArray schoolCityName = new JSONArray();
	JSONArray schoolStateName = new JSONArray();
	JSONArray schoolGeoMapId = new JSONArray();
	JSONArray schoolSchoolTypeId = new JSONArray();
	JSONArray schoolRegionId = new JSONArray();
	if(jobOrderDetails.getSchool()!=null)
	{
		
		for(SchoolMaster school:jobOrderDetails.getSchool())
		{
			schoolGeoMapId.add(school.getGeoMapping().getGeoId().getGeoId()+"");
			schoolSchoolTypeId.add(school.getSchoolTypeId().getSchoolTypeId()+"");
			schoolRegionId.add(school.getRegionId().getRegionId()+"");
			
			schoolName.add(school.getSchoolName());
			schoolId.add(school.getSchoolId());
			if(school.getAddress()!=null)
				schoolAddress.add(school.getAddress());
			else
				schoolAddress.add("");
			if(school.getZip()!=null)
				schoolZip.add(school.getZip());
			else
				schoolZip.add(0);
			if(schoolCityName!=null)
				schoolCityName.add(school.getCityName());
			else
				schoolCityName.add("");
			if(school.getStateMaster()!=null)
				schoolStateName.add(school.getStateMaster().getStateName());
			else
				schoolStateName.add("");
		}
		
	}
	obj.put("schoolName", schoolName);
	obj.put("schoolId", schoolId);
	obj.put("schoolAddress", schoolAddress);
	obj.put("schoolZip", schoolZip);
	obj.put("schoolCityName", schoolCityName);
	obj.put("schoolStateName", schoolStateName);
	obj.put("schoolGeoMapId", schoolGeoMapId);
	obj.put("schoolSchoolTypeId", schoolSchoolTypeId);
	obj.put("schoolRegionId", schoolRegionId);
	
		
	//System.out.println("inserting json for updateESForJobsOfInterestNotCandidate"+obj.toString());
	update(ElasticSearchConfig.indexForjobsOfInterest,"jdbc",jobOrderDetails.getJobId().toString(),obj.toString());

}

// not for candidate

// for candidate
public void creatingStructureForJobsOfInterestForCandidate()
{
ClientConfig config = new DefaultClientConfig();
Client client = Client.create(config);
URI uri=null;
//uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+ElasticSearchConfig.indexForDistrictJobBoard+"/_search?pretty");
uri=getURIForStructureCr(ElasticSearchConfig.indexForjobsOfInterestForCandidate);    	
WebResource service = client.resource(uri);
ClientResponse response = null;
String input = "{\"type\" : \"jdbc\"," +
				"\"strategy\" : \"simple\"," +
				  		"\"jdbc\" : " +
		    		"[" +
		    			"{\"url\" : \""+ElasticSearchConfig.sElasticSearchDBURL+"\"," +
		    			"\"user\" : \""+ElasticSearchConfig.sElasticSearchDBUser+"\",\"password\" : \""+ElasticSearchConfig.sElasticSearchDBPass+"\"," +
		    			  
		    			  
		    			  
							"\"sql\" : \""+jobsOfInterestSQL+" limit 0,0 \"," +
							
		    			   "\"index\" : \""+ElasticSearchConfig.indexForjobsOfInterestForCandidate+"\"," +
		    			   //"\"autocommit\" : true," +
		    			   " \"fetchsize\" : 30" +
		    			  "}" +
		    		  "]" +
			  "}";

//System.out.println(input);

response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
//System.out.println("Form response11 " + response.getEntity(String.class));
System.out.println("****************************jobsofinterest*******************************");

}

public Map<Integer,String> getDBDataForJobsOfInterestForCandidate(JobOrderDAO jobOrderDAO)
{


try
{
	List<JobOrder> lstJobOrder	  =	 new ArrayList<JobOrder>();
	//System.out.println(jobOrderDAO);
	//Map<Integer,String> map=jobOrderDAO.findActiveJobOrderbyDistrictES();
	lstJobOrder=jobOrderDAO.findActiveJobOrderbyDistrictES();
	System.out.println("getDBDataForJobsOfInterestForCandidate size==============="+lstJobOrder.size());
	Map<Integer,String> map=new HashMap<Integer, String>();
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	
	for(JobOrder jobOrderDetails:lstJobOrder)
	{
		JSONObject obj=new JSONObject();
		obj.put("jobId", jobOrderDetails.getJobId().toString());
		obj.put("isPoolJob", jobOrderDetails.getIsPoolJob());
		obj.put("jobTitle", jobOrderDetails.getJobTitle());
		obj.put("createdForEntity", jobOrderDetails.getCreatedForEntity());
		obj.put("districtId", jobOrderDetails.getDistrictMaster().getDistrictId());
		obj.put("districtName", jobOrderDetails.getDistrictMaster().getDistrictName());
		obj.put("zipCode", jobOrderDetails.getDistrictMaster().getZipCode());
		//tommorow if null check
		if(jobOrderDetails.getDistrictMaster().getCityName()!=null)
			obj.put("disCityName", jobOrderDetails.getDistrictMaster().getCityName());
		else
			obj.put("disCityName", "");
		if(jobOrderDetails.getDistrictMaster().getAddress()!=null)
			obj.put("distaddress", jobOrderDetails.getDistrictMaster().getAddress());
		else
			obj.put("distaddress", "");
		obj.put("status", jobOrderDetails.getStatus());
		if(jobOrderDetails.getIpAddress()!=null)
			obj.put("ipAddress", jobOrderDetails.getIpAddress());
		else
			obj.put("ipAddress", "");
		obj.put("jobEndDate", sdf.format(jobOrderDetails.getJobEndDate()));
		if(jobOrderDetails.getDistrictMaster().getDisplayName()!=null)
			obj.put("displayName", jobOrderDetails.getDistrictMaster().getDisplayName());
		else
			obj.put("displayName", "");
		if(jobOrderDetails.getDistrictMaster().getStateId()!=null )
			obj.put("disStateName", jobOrderDetails.getDistrictMaster().getStateId().getStateName());
		else
			obj.put("disStateName", "");
		
		
		if(jobOrderDetails.getSubjectMaster()!=null)
			obj.put("subjectName", jobOrderDetails.getSubjectMaster().getSubjectName());
		else
			obj.put("subjectName", "");
		if(jobOrderDetails.getGeoZoneMaster()!=null)
		{
			obj.put("geoZoneDistrictId", jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId());
			obj.put("geoZoneName", jobOrderDetails.getGeoZoneMaster().getGeoZoneName());
			obj.put("geoZoneId", jobOrderDetails.getGeoZoneMaster().getGeoZoneId());
		}
			
		else
		{
			obj.put("geoZoneDistrictId", 0);
			obj.put("geoZoneName", "");
			obj.put("geoZoneId", 0);
		}
		
		JSONArray schoolName = new JSONArray();
		JSONArray schoolId = new JSONArray();
		JSONArray schoolAddress = new JSONArray();
		JSONArray schoolZip = new JSONArray();
		JSONArray schoolCityName = new JSONArray();
		JSONArray schoolStateName = new JSONArray();
		JSONArray schoolGeoMapId = new JSONArray();
		JSONArray schoolSchoolTypeId = new JSONArray();
		JSONArray schoolRegionId = new JSONArray();
		if(jobOrderDetails.getSchool()!=null)
		{
			for(SchoolMaster school:jobOrderDetails.getSchool())
			{
				schoolGeoMapId.add(school.getGeoMapping().getGeoId().getGeoId()+"");
				schoolSchoolTypeId.add(school.getSchoolTypeId().getSchoolTypeId()+"");
				schoolRegionId.add(school.getRegionId().getRegionId()+"");
				
				schoolName.add(school.getSchoolName());
				schoolId.add(school.getSchoolId());
				if(school.getAddress()!=null)
					schoolAddress.add(school.getAddress());
				else
					schoolAddress.add("");
				if(school.getZip()!=null)
					schoolZip.add(school.getZip());
				else
					schoolZip.add(0);
				if(schoolCityName!=null)
					schoolCityName.add(school.getCityName());
				else
					schoolCityName.add("");
				if(school.getStateMaster()!=null)
					schoolStateName.add(school.getStateMaster().getStateName());
				else
					schoolStateName.add("");
			}
			
		}
		obj.put("schoolName", schoolName);
		obj.put("schoolId", schoolId);
		obj.put("schoolAddress", schoolAddress);
		obj.put("schoolZip", schoolZip);
		obj.put("schoolCityName", schoolCityName);
		obj.put("schoolStateName", schoolStateName);
		obj.put("schoolGeoMapId", schoolGeoMapId);
		obj.put("schoolSchoolTypeId", schoolSchoolTypeId);
		obj.put("schoolRegionId", schoolRegionId);
		
			
		
		//System.out.println(obj.toString());
		map.put(jobOrderDetails.getJobId(), obj.toString());
	}
	System.out.println("jobsofinterestforcandidate map size==="+map.size());
	return map;
}
catch(Exception e)
{
	e.printStackTrace();
}
return null;

}
public void createJobsOfInterestForCandidate(Map<Integer, String> dataBaseMap)
{
try
{
System.out.println("createJobsOfInterestForCandidate");

int failJobNo=0;
	System.out.println("*****************createJobsOfInterestForCandidate insertion start**************");
	Set<Integer> set=dataBaseMap.keySet();
	for(Integer i:set)
	{
		ClientConfig config = new DefaultClientConfig();
	    Client client = Client.create(config);
	    URI uri=null;
	    try {
	    	
				uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+ElasticSearchConfig.indexForjobsOfInterestForCandidate+"/jdbc/"+i+"?pretty");
			} catch (URISyntaxException e) {
			failJobNo++;
			System.out.println("failed jobid="+i);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).put(ClientResponse.class, dataBaseMap.get(i));
	  //  System.out.println("Form response11 " + response.getEntity(String.class));
	}
	System.out.println("Total job id fail in creating=="+failJobNo);
	System.out.println("****************createJobsOfInterestForCandidate insertion end************");
}
catch(Exception e)
{
e.printStackTrace();
}


}

public JSONObject esForJobsOfInterestForCandidate(int districtId,int schoolId,int geoZonId,
int from,String noOfRow,String pageNo,int pgNo,String sortOrderFieldName,String sortOrderTypeVal,String searchTerm,String subjectName,String subjectId,String stateName,String cityName)
{
try
{
boolean match=true;
StringBuffer input =new StringBuffer();
	int noOfRowInPage	=	Integer.parseInt(noOfRow);
	int start 			= 	((pgNo-1)*noOfRowInPage);
	int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
	
	end=noOfRowInPage;
	
		
		input.append("{\"query\": {\"bool\": {");
		  input.append(" \"must\" : [");
			/*if(districtName!=null && !districtName.trim().equals(""))
			{
				input.append("   {\"match_phrase\" : { \"districtName\" : \""+districtName+"\" }},");
			}*/
		  	if(districtId!=0 )
			{
		  		match=false;
				input.append("   {\"match_phrase\" : { \"districtId\" : "+districtId+" }},");
			}
			if(schoolId!=0 )
			{
				match=false;
				input.append("   {\"match_phrase\" : { \"schoolId\" : "+schoolId+" }},");
			}
			
			if(geoZonId!=-1 && geoZonId!=0)
			{
				match=false;
				input.append("   {\"match_phrase\" : { \"geoZoneId\" : "+geoZonId+" }},");
			}
			
			if(stateName!=null && !stateName.trim().equals("") && !stateName.equalsIgnoreCase("0") && !stateName.equalsIgnoreCase("ALL"))
			{
				match=false;
				input.append("   {\"match_phrase\" : { \"disStateName\" : \""+stateName+"\" }},");
			}
			if(subjectName!=null && !subjectName.trim().equals("") && !subjectName.equalsIgnoreCase("0") && !subjectName.equalsIgnoreCase("ALL"))
			{
				match=false;
				input.append("   {\"match\" : { \"subjectId\" : \""+subjectId+"\" }},");
			}
			if(cityName!=null && !cityName.trim().equals("") && !cityName.equalsIgnoreCase("0") && !cityName.equalsIgnoreCase("ALL"))
			{
				match=false;
				input.append("   {\"match_phrase\" : { \"disCityName\" : \""+cityName+"\" }},");
			}
			
			if(input.indexOf(",")!=-1)
				input.replace(input.lastIndexOf(","), input.lastIndexOf(",")+1, "");
			input.append("],"
			
					+ "");
			/*input.append("\"should\": ["
		  			+ "{ \"match_phrase\": { \"jobTitle\":  \""+searchTerm+"\" }}"
		  			//+ "{ \"match_phrase\": { \"jobTitle\":  \""+searchTerm+"\" }},"
		  		//	+ "{ \"match_phrase\": { \"subjectName\": \""+searchTerm+"\"   }}"
			  		
		  		+ "],");*/
			input.append("\"should\": [{"
					+"  \"multi_match\" : {"
 		        +"    \"fields\" : [\"jobTitle\",\"districtName\",\"subjectName\",\"geoZoneName\"],"
 		      +"      \"query\" : \""+searchTerm+"\","
 		    +"        \"type\" : \"phrase_prefix\""
 		  +"      }"
			  		
		  		+ "}"
		  		+ ","
		  		+ "{ \"match\": { \"jobTitle\":  \""+searchTerm+"\" }},"
		  		+ "{ \"match\": { \"districtName\":  \""+searchTerm+"\" }},"
		  		+ "{ \"match\": { \"subjectName\":  \""+searchTerm+"\" }},"
		  		+ "{ \"match\": { \"geoZoneName\":  \""+searchTerm+"\" }}"
		  		+ "],");
			end=10000;
		if(searchTerm.trim().equals(""))
			input.append("\"minimum_should_match\" : \"1%\"}},\"size\":"+end+",\"from\":"+start+",\"_source\": [\"jobId\"]");
		else
			input.append("\"minimum_should_match\" : 1}},\"size\":"+end+",\"from\":"+start+",\"_source\": [\"jobId\"]");
					if(searchTerm==null || searchTerm.trim().equals(""))
					{
						//input.append(",\"sort\": { \""+sortOrderFieldName+"\": { \"order\": \""+sortOrderTypeVal+"\" }}");
					}
					
							input.append("}");
			if(searchTerm.trim().equals("") && match)
			{
				input=new StringBuffer();
				input.append("{\"query\": { \"match_all\": {} },\"size\":"+end+",\"from\":"+start+",\"_source\": [\"jobId\"] }");
			}
				
		//System.out.println(input);
	  		ClientConfig config = new DefaultClientConfig();
			URI uri=null;
		    Client client = Client.create(config);
		    List<String> list=new ArrayList<String>();
		    uri=getURIForSearch(ElasticSearchConfig.indexForjobsOfInterestForCandidate);
			WebResource service = client.resource(uri);
		    ClientResponse response = null;
		    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input.toString());
		    String s=response.getEntity(String.class);
		    //System.out.println(s);
		    JSONObject jsonObject = new JSONObject();
		    jsonObject=jsonObject.fromObject(s);
		    return jsonObject;
		   
}
catch(Exception e)
{
e.printStackTrace();
}
return null;					
}					 

public Map<Integer, String> esForJobsOfInterestForCandidateAll()
{
	//System.out.println("Calling esForDistrictJobBoard");
	Map<Integer, String> map=new HashMap<Integer, String>();
	indexMapForJobsOfInterestForCandidate=new HashMap<Integer, String>();
	ClientConfig config = new DefaultClientConfig();
	URI uri=null;
 Client client = Client.create(config);
 uri=getURIForSearch(ElasticSearchConfig.indexForjobsOfInterestForCandidate);
	WebResource service = client.resource(uri);
 ClientResponse response = null;
 String input = "{\"query\": { \"match_all\": {} },\"size\":100000}";
 response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
 String s=response.getEntity(String.class);
//   System.out.println(s);
 
 JSONObject jsonObject=new JSONObject();
 try {
		
		//System.out.println(((JSONObject)jsonObject.get("hits")));
		 //System.out.println(((JSONObject)jsonObject.get("hits")).get("hits"));
 	jsonObject=jsonObject.fromObject(s);
		 JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
		// System.out.println("hits size==="+hits.size());
		 for(int i=0; i<hits.size(); i++)
		 {
			jsonObject=(JSONObject)hits.get(i);
			String index=(String)jsonObject.get("_index");
			String type=(String)jsonObject.get("_type");
			String id=(String)jsonObject.get("_id");
			jsonObject=(JSONObject)jsonObject.get("_source");
			//System.out.println(jsonObject.toString());
			//System.out.println(jsonObject.get("jobid").toString()+"_"+jsonObject.get("schoolId")+"_"+jsonObject.get("certTypeId"));
			map.put(jsonObject.getInt("jobId"), jsonObject.toString());
			indexMapForJobsOfInterestForCandidate.put(jsonObject.getInt("jobId"), index+":"+type+":"+id);
		}
		


	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	//System.out.println("document size="+map.size());
	
	return map;
	
}

public void updateESForJobsOfInterestForCandidate(Map<Integer, String> databaseMap,Map<Integer, String> elasticMap)
{
	System.out.println("*************************************");
	System.out.println(databaseMap.size()+"\t"+elasticMap.size());
	System.out.println("Calling compareData indexMapForJobsOfInterestForCandidate");
	int x=0;
	int y=0;
	int z=0;
	Set<Integer> databaseSet=databaseMap.keySet();
	for(Integer i:databaseSet)
	{
		//System.out.println(elasticMap.get(i));
		//System.out.println(databaseMap.get(i));
		//System.out.println("*****************************");
		
		//if data is not present in elasticsearch then insert
		if(elasticMap.get(i)==null)
		{
			//System.out.println("**********************creating**************************"+i.toString());
			//System.out.println(databaseMap.get(i));
			x++;
			//System.out.println(i.toString());
			update(ElasticSearchConfig.indexForjobsOfInterestForCandidate,"jdbc",i.toString(),databaseMap.get(i));
		}
		//if data is present then update
		else if(!(elasticMap.get(i)).equals(databaseMap.get(i)))
		{
			
			y++;
			update(indexMapForJobsOfInterestForCandidate.get(i).split(":")[0],indexMapForJobsOfInterestForCandidate.get(i).split(":")[1],indexMapForJobsOfInterestForCandidate.get(i).split(":")[2],databaseMap.get(i));
				
			
			
		}
		
		
		
		
	}
	
//	System.out.println(elasticMap.size()+"\t"+databaseMap.size());
	//for deleting from elasticsearch
	Set<Integer> elasticSet=elasticMap.keySet();
	for(Integer i:elasticSet)
	{
		if(databaseMap.get(i)==null)
		{
			
				//System.out.println("**************************deleting********************"+i);
				//System.out.println(databaseMap.get(i));
				z++;
			
			
			delete(indexMapForJobsOfInterestForCandidate.get(i).split(":")[0],indexMapForJobsOfInterestForCandidate.get(i).split(":")[1],indexMapForJobsOfInterestForCandidate.get(i).split(":")[2]);
		}
			
		
		
	}
	System.out.println("databaseMap size for JobsOfInterestForCandidate="+databaseMap.size());
	System.out.println("elasticMap size  for JobsOfInterestForCandidate="+elasticMap.size());
	System.out.println("indexMap size  for JobsOfInterestForCandidate="+indexMapForJobsOfInterestForCandidate.size());
	System.out.println("creating x  for JobsOfInterestForCandidate=="+x);
	System.out.println("updating y  for JobsOfInterestForCandidate=="+y);
	System.out.println("deleting z  for JobsOfInterestForCandidate=="+z);
/*	System.out.println("iterating index map");
	Set<String> set=indexMap.keySet();
	for(String s:set)
	{
		System.out.println(s);
	}*/
}
public void updateESForJobsOfInterestForCandidate(JobOrder jobOrderDetails)
{
	System.out.println("updating updateESForJobsOfInterestForCandidate");
	searchDataAndDelete(jobOrderDetails.getJobId(),ElasticSearchConfig.indexForjobsOfInterestForCandidate);
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	//Map<Integer,String> map=getDBDataForDistrictJobBoard(jobOrder);
	JSONObject obj=new JSONObject();
	obj.put("jobId", jobOrderDetails.getJobId());
	obj.put("isPoolJob", jobOrderDetails.getIsPoolJob());
	obj.put("jobTitle", jobOrderDetails.getJobTitle());
	obj.put("createdForEntity", jobOrderDetails.getCreatedForEntity());
	obj.put("districtId", jobOrderDetails.getDistrictMaster().getDistrictId());
	obj.put("districtName", jobOrderDetails.getDistrictMaster().getDistrictName());
	obj.put("zipCode", jobOrderDetails.getDistrictMaster().getZipCode());
	//tommorow if null check
	if(jobOrderDetails.getDistrictMaster().getCityName()!=null)
		obj.put("disCityName", jobOrderDetails.getDistrictMaster().getCityName());
	else
		obj.put("disCityName", "");
	if(jobOrderDetails.getDistrictMaster().getAddress()!=null)
		obj.put("distaddress", jobOrderDetails.getDistrictMaster().getAddress());
	else
		obj.put("distaddress", "");
	obj.put("status", jobOrderDetails.getStatus());
	if(jobOrderDetails.getIpAddress()!=null)
		obj.put("ipAddress", jobOrderDetails.getIpAddress());
	else
		obj.put("ipAddress", "");
	obj.put("jobEndDate", sdf.format(jobOrderDetails.getJobEndDate()));
	if(jobOrderDetails.getDistrictMaster().getDisplayName()!=null)
		obj.put("displayName", jobOrderDetails.getDistrictMaster().getDisplayName());
	else
		obj.put("displayName", "");
	if(jobOrderDetails.getDistrictMaster().getStateId()!=null )
		obj.put("disStateName", jobOrderDetails.getDistrictMaster().getStateId().getStateName());
	else
		obj.put("disStateName", "");
	/*if(jobOrderDetails.getDistrictAttachment()!=null)
		obj.put("districtAttachment", jobOrderDetails.getDistrictAttachment());
	else
		obj.put("districtAttachment", "");
	obj.put("IsInviteOnly", jobOrderDetails.getIsInviteOnly());
	if(jobOrderDetails.getRequisitionNumber()!=null)
	{
		obj.put("requisitionNumber", jobOrderDetails.getRequisitionNumber());
	}
	else
	{
		obj.put("requisitionNumber", "");
	}*/
	
	if(jobOrderDetails.getSubjectMaster()!=null)
		obj.put("subjectName", jobOrderDetails.getSubjectMaster().getSubjectName());
	else
		obj.put("subjectName", "");
	if(jobOrderDetails.getGeoZoneMaster()!=null)
	{
		obj.put("geoZoneDistrictId", jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId());
		obj.put("geoZoneName", jobOrderDetails.getGeoZoneMaster().getGeoZoneName());
		obj.put("geoZoneId", jobOrderDetails.getGeoZoneMaster().getGeoZoneId());
	}
		
	else
	{
		obj.put("geoZoneDistrictId", 0);
		obj.put("geoZoneName", "");
		obj.put("geoZoneId", 0);
	}
	/*if(jobOrderDetails.getJobCategoryMaster()!=null)
	{
		obj.put("jobCategory", jobOrderDetails.getJobCategoryMaster().getJobCategoryName());
	}
	else
	{
		obj.put("jobCategory", "");
	}*/
	JSONArray schoolName = new JSONArray();
	JSONArray schoolId = new JSONArray();
	JSONArray schoolAddress = new JSONArray();
	JSONArray schoolZip = new JSONArray();
	JSONArray schoolCityName = new JSONArray();
	JSONArray schoolStateName = new JSONArray();
	JSONArray schoolGeoMapId = new JSONArray();
	JSONArray schoolSchoolTypeId = new JSONArray();
	JSONArray schoolRegionId = new JSONArray();
	if(jobOrderDetails.getSchool()!=null)
	{
		for(SchoolMaster school:jobOrderDetails.getSchool())
		{
			schoolGeoMapId.add(school.getGeoMapping().getGeoId().getGeoId()+"");
			schoolSchoolTypeId.add(school.getSchoolTypeId().getSchoolTypeId()+"");
			schoolRegionId.add(school.getRegionId().getRegionId()+"");
			
			schoolName.add(school.getSchoolName());
			schoolId.add(school.getSchoolId());
			if(school.getAddress()!=null)
				schoolAddress.add(school.getAddress());
			else
				schoolAddress.add("");
			if(school.getZip()!=null)
				schoolZip.add(school.getZip());
			else
				schoolZip.add(0);
			if(schoolCityName!=null)
				schoolCityName.add(school.getCityName());
			else
				schoolCityName.add("");
			if(school.getStateMaster()!=null)
				schoolStateName.add(school.getStateMaster().getStateName());
			else
				schoolStateName.add("");
		}
		
	}
	obj.put("schoolName", schoolName);
	obj.put("schoolId", schoolId);
	obj.put("schoolAddress", schoolAddress);
	obj.put("schoolZip", schoolZip);
	obj.put("schoolCityName", schoolCityName);
	obj.put("schoolStateName", schoolStateName);
	obj.put("schoolGeoMapId", schoolGeoMapId);
	obj.put("schoolSchoolTypeId", schoolSchoolTypeId);
	obj.put("schoolRegionId", schoolRegionId);
	
		
	//System.out.println("inserting json for updateESForJobsOfInterestForCandidate"+obj.toString());
	update(ElasticSearchConfig.indexForjobsOfInterestForCandidate,"jdbc",jobOrderDetails.getJobId().toString(),obj.toString());

}
//  for candidate


public JSONObject findJobtoShow(String regionId,String schoolTypeId,String geographyId)
{

try
{
	 boolean match=true;
	 StringBuffer input =new StringBuffer();
		
			
			input.append("{\"query\": {\"bool\": {");
			input.append(" \"must\" : [");
			input.append("   {\"match\" : { \"schoolRegionId\" : \""+regionId+"\" }},");
			input.append("   {\"match\" : { \"schoolSchoolTypeId\" : \""+schoolTypeId+"\" }},");
			input.append("   {\"match\" : { \"schoolGeoMapId\" : \""+geographyId+"\" }}");
			input.append("],"
			+ "");
			int end=10000;
			int start=0;
			//if(searchTerm.trim().equals(""))
				//input.append("\"minimum_should_match\" : \"1%\"}},\"size\":"+end+",\"from\":"+start);
			//else
			input.append("\"minimum_should_match\" : 1}},\"size\":"+end+",\"from\":"+start);
						//if(searchTerm==null || searchTerm.trim().equals(""))
						//{
							//input.append(",\"sort\": { \""+sortOrderFieldName+"\": { \"order\": \""+sortOrderTypeVal+"\" }}");
						//}
						
			input.append("}");
				
					
			//System.out.println(input);
		  		ClientConfig config = new DefaultClientConfig();
				URI uri=null;
			    Client client = Client.create(config);
			    List<String> list=new ArrayList<String>();
			    uri=getURIForSearch(ElasticSearchConfig.indexForjobsOfInterest);
				WebResource service = client.resource(uri);
			    ClientResponse response = null;
			    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input.toString());
			    String s=response.getEntity(String.class);
			   // System.out.println(s);
			    JSONObject jsonObject = new JSONObject();
			    jsonObject=jsonObject.fromObject(s);
			    return jsonObject;
			   
}
catch(Exception e)
{
	 e.printStackTrace();
}
	return null;					
}					 


//jobs of interest end



	
	public static void main(String[] args) {
		//new ElasticSearchService().searchDocumentByJobid(13589);
		new ElasticSearchService().esForSchoolJobOrderNew("1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 665 10690 13373 13440 13446 13472 13484 13485 13486 13488 13489 13490 14097 14098 14099 14100 14101 14102 14103 14104 14105 14106 14107 14108 14109 14110 14111 14112 14113 14114 14115 14116 14117 14118 14119 14120 14121 14122 15144 17051 17052 17053 17054 17814 18266 18774 18775 18776 18822 18849 18867 18888 18893 18894 18895 18916 18917 18918 18923 18924 18925 18926 18927 18928 18929 18930 18931 18932 18938 18941 18942 18943 18946 18947 18948 18949 18957 18958 18959 18960 18961 18981 19033 19034 19035 19036 19037 19038 19051 19052 19053 19054 19055 19056 19057 19058 19059 19060 19061 19062 19063 19064 19065 19066 19071 19072 19073 19074 19075 19080 19081 19082 19083 19084 19085 19086 19087 19088 19089 19090 19091 19092 19093 19094 19095 19096 19107 19108 19109 19110 19111 19112 19113 19114 19115 19116 19117 19118 19119 19120 19121 19122 19123 19124 19137 19138 19139 19140 19141 19142 19143 19144 19145 19146 19147 19148 19149 19150 19151 19152 19153 19154 19155 19156 19157 19158 19159 19160 19161 19162 19163 19164 19165 19166 19167 19168 19169 19170 19171 19172 19173 19174 19190 19201 19202 19204 19205 19206 19207 19208 19209 19210 19213 19223 19365 19910 20153 20154 20530 20677 20678 20906 20909 20910 20911 20912 20913 20914 20915 20916 20917 20918 20919 20920 20921 20922 20923 20924 20925 21682 21683 21684 21685 21686 21687 21688 21693 21699 21700 21701 21702 21703 21704 21705 21706 21716 21717 21726 21727 21728 21729 21730 21731 21760 21761 21762 21763 21764 21765 21766 21767 21768 21769 21770 21771 21772 21773 21774 21775 21776 21777 21778 21779 21780 21781 21782 21783 21784 21785 21786 21787 21788 21789 21790 21791 21792 21793 21794 21795 21796 21797 21798 21799 21800 21801 21802 21803 21804 21805 21806 21807 21808 21809 21810 21811 21812 21813 21814 21815 21816 21817 21818 21819 21820 21821 21822 21823 21824 21825 21826 21827 21828 21829 21830 21831 21832 21833 21834 21835 21836 21837 21838 21839 21840 21841 21842 21843 21844 21845 21846 21847 21848 21849 21850 21851 21852 21853 21854 21855 21856 21857 21858 21859 21860 21861 21862 21863 21864 21865 21866 21867 21868 21869 21870 21871 21872 21873 21874 21875 21876 21877 21878 21879 21880 21881 21882 21883 21884 21885 21886 21887 21888 21889 21890 21891 21892 21893 21894 21895 21896 21897 21898 21899 21900 21901 21902 21903 21904 21905 21906 21907 21908 21909 21910 21911 21912 21913 21914 21915 21916 21917 21918 21919 21920 21921 21922 21923 21924 21925 21926 21927 21928 21929 21930 21931 21932 21933 21934 21935 21936 21937 21938 21939 21940 21941 21942 21943 21944 21945 21946 21947 21948 21949 21950 21951 21952 21953 21954 21955 21956 21957 21958 21959 21960 21961 21962 21963 21964 21965 21966 21967 21968 21969 21970 21971 21972 21973 21974 21975 21976 21977 21978 21979 21980 21981 21982 21983 21984 21985 21986 21987 21988 21989 21990 21991 21992 21993 21994 21995 21996 21997 21998 21999 22000 22001 22002 22003 22004 22005 22006 22007 22008 22009 22010 22011 22012 22013 22014 22015 22016 22017 22018 22019 22020 22021 22022 22023 22024 22025 22026 22027 22028 22029 22030 22031 22032 22033 22034 22035 22036 22037 22038 22039 22040 22041 22042 22043 22044 22045 22046 22047 22048 22049 22050 22051 22052 22053 22054 22055 22056 22057 22058 22059 22060 22061 22062 22063 22064 22065 22066 22067 22068 22069 22070 22071 22072 22073 22074 22075 22076 22077 22078 22079 22080 22081 22082 22083 22084 22085 22086 22087 22088 22089 22090 22091 22092 22093 22094 22095 22096 22097 22098 22099 22100 22101 22102 22103 22104 22105 22106 22107 22108 22109 22110 22111 22112 22113 22114 22115 22116 22117 22118 22119 22120 22121 22122 22123 22124 22125 22126 22127 22128 22129 22130 22131 22132 22133 22134 22135 22136 22137 22138 22139 22140 22141 22142 22143 22144 22145 22146 22147 22148 22149 22150 22151 22152 22153 22154 22155 22156 22157 22158 22159 22160 22161 22162 22163 22164 22165 22166 22167 22168 22169 22170 22171 22172 22173 22174 22175 22176 22177 22178 22179 22180 22181 22182 22183 22184 22185 22186 22187 22188 22189 22190 22191 22192 22193 22194 22195 22196 22197 22198 22199 22200 22201 22202 22203 22204 22205 22206 22207 22208 22209 22210 22211 22212 22213 22214 22215 22216 22217 22218 22219 22220 22221 22222 22223 22224 22225 22226 22227 22228 22229 22230 22231 22232 22233 22234 22235 22236 22237 22238 22239 22240 22241 22242 22243 22244 22245 22246 22247 22248 22249 22250 22251 22252 22253 22254 22255 22256 22257 22258 22259 22260 22261 22262 22263 22264 22265 22266 22267 22268 22269 22270 22271 22272 22273 22274 22275 22276 22277 22278 22279 22280 22281 22282 22283 22284 22285 22286 22287 22288 22289 22290 22291 22292 22293 22294 22295 22296 22297 22298 22299 22300 22301 22302 22303 22304 22305 22306 22307 22308 22309 22310 22311 22312 22313 22314 22315 22316 22317 22318 22319 22320 22321 22322 22323 22324 22325 22326 22327 22328 22329 22330 22331 22332 22333 22334 22335 22336 22337 22338 22339 22340 22341 22342 22343 22344 22345 22346 22347 22348 22349 22350 22351 22352 22353 22354 22355 22356 22357 22358 22359 22360 22361 22362 22363 22364 22365 22366 22367 22368 22369 22370 22371 22372 22373 22374 22375 22376 22377 22378 22379 22380 22381 22382 22383 22384 22385 22386 22387 22388 22389 22390 22391 22392 22393 22394 22395 22396 22397 22398 22399 22400 22401 22402 22403 22404 22405 22406 22407 22408 22409 22410 22411 22412 22413 22414 22415 22416 22417 22418 22419 22420 22421 22422 22423 22424 22425 22426 22427 22428 22429 22430 22431 22432 22433 22434 22435 22436 22437 22438 22439 22440 22441 22442 22443 22444 22445 22446 22447 22448 22449 22450 22451 22452 22453 22454 22455 22456 22457 22458 22459 22460 22461 22462 22463 22464");
		

	}
	
	
	
	public static URI getURIForStructureCr(String index) {
	     URI uri=null;
		    try {
			
		    	uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/_river/"+index+"_jdbc_river/_meta");
		    		    	
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    return uri;
	  }
	
	public static URI getURIForSearch(String index) {
	     URI uri=null;
		    try {
			
		    	uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+index+"/_search?pretty");
		    		    	
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    return uri;
	  }
	
	public int getJobRowCount(JobOrderDAO jobOrderDAO)
	{
		int jobCount=jobOrderDAO.getRowCount();
		return jobCount;
	}
	
	

}
