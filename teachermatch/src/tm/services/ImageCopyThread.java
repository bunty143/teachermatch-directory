package tm.services;

import java.io.File;

import org.apache.commons.io.FileUtils;

import tm.utility.Utility;

public class ImageCopyThread extends Thread 
{
	
	private String sourceFile;
	private String targetFile;
	private String fileName;
	public String getSourceFile() {
		return sourceFile;
	}
	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}
	public String getTargetFile() {
		return targetFile;
	}
	public void setTargetFile(String targetFile) {
		this.targetFile = targetFile;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public ImageCopyThread() {
		super();
	}
	
	public void run()
	{
		try{
			
				System.out.println("ImageCopyThread Run.............");
				String source = sourceFile;
				System.out.println("source:: "+source);
				String target = targetFile;
				System.out.println("target:: "+target);
				File sourceFile = new File(source);
				File targetDir = new File(target);
				if(!targetDir.exists())
					targetDir.mkdirs();
				
				System.out.println("sourceFile.getName():::::::::::: "+sourceFile.getName());
				File targetFile = new File(targetDir+"/"+sourceFile.getName());
				
				FileUtils.copyFile(sourceFile, targetFile);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
