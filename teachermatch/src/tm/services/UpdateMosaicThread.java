package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.criterion.Order;

import tm.bean.JobActionFeed;
import tm.bean.JobOrder;
import tm.bean.cgreport.PercentileZscoreTscore;
import tm.bean.cgreport.TmpPercentileWiseZScore;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.dao.JobActionFeedDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherActionFeedDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.dao.cgreport.TmpPercentileWiseZScoreDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.services.report.CGReportService;
import tm.servlet.WorkThreadServlet;


public class UpdateMosaicThread extends Thread{

	List<TmpPercentileWiseZScore> tmpPercentileWiseZScoreList = null;
	public void setTmpPercentileWiseZScoreList(
			List<TmpPercentileWiseZScore> tmpPercentileWiseZScoreList) {
		this.tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreList;
	}
	
	List<PercentileZscoreTscore> pztList = null;
	public void setPztList(List<PercentileZscoreTscore> pztList) {
		this.pztList = pztList;
	}
	
	private RawDataForDomainDAO rawDataForDomainDAO;
	public void setRawDataForDomainDAO(RawDataForDomainDAO rawDataForDomainDAO) {
		this.rawDataForDomainDAO = rawDataForDomainDAO;
	}
	
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	
	private DomainMasterDAO domainMasterDAO;
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) {
		this.domainMasterDAO = domainMasterDAO;
	}
	
	private TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO;
	public void setTmpPercentileWiseZScoreDAO(
			TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO) {
		this.tmpPercentileWiseZScoreDAO = tmpPercentileWiseZScoreDAO;
	}
	
	private TeacherNormScoreDAO teacherNormScoreDAO;
	public void setTeacherNormScoreDAO(TeacherNormScoreDAO teacherNormScoreDAO) {
		this.teacherNormScoreDAO = teacherNormScoreDAO;
	}
	
	private CGReportService cGReportService;
	public void setcGReportService(CGReportService cGReportService) {
		this.cGReportService = cGReportService;
	}
	
	private JobActionFeedDAO jobActionFeedDAO;
	public void setJobActionFeedDAO(JobActionFeedDAO jobActionFeedDAO) {
		this.jobActionFeedDAO = jobActionFeedDAO;
	}
	
	private TeacherActionFeedDAO teacherActionFeedDAO;
	public void setTeacherActionFeedDAO(
			TeacherActionFeedDAO teacherActionFeedDAO) {
		this.teacherActionFeedDAO = teacherActionFeedDAO;
	}

	public UpdateMosaicThread() {
		super();
	}
	
	public void run()
	{
		if(tmpPercentileWiseZScoreList==null)
		{
			tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreDAO.findByCriteria(Order.asc("zScore"));
			System.out.println("?????????????????????????????????????? ");
		}
		if(pztList==null)
		{
			pztList = cGReportService.populateZScoreMap(tmpPercentileWiseZScoreList);
			System.out.println("?????????????????????????????????????? ");
		}

		

		StatusMaster statusMaster = WorkThreadServlet.statusMap.get("hird");
		
		List<Integer> statusIdList = new ArrayList<Integer>();			
		String[] statusShrotName = {"hird","hide","widrw"};
		try{
			statusIdList = statusMasterDAO.findStatusIdsByStatusByShortNames(statusShrotName);
		}catch (Exception e) {
			e.printStackTrace();
		}
		int statusId = statusMaster.getStatusId();
		
		SessionFactory sessionFactory =  domainMasterDAO.getSessionFactory();
		StatelessSession session1 = sessionFactory.openStatelessSession();
		
		/*
		 List<DomainMaster> domainMasters = new ArrayList<DomainMaster>();
		domainMasters = WorkThreadServlet.domainMastersPDRList;
		
		 Stop norm score process
		TeacherDetail teacherDetail = null;
		TeacherNormScore teacherNormScore = null;
		List<TeacherDetail> teacherDetailsBlank = new ArrayList<TeacherDetail>();
		List lstRawData = new ArrayList();
		lstRawData = rawDataForDomainDAO.findTeacherEpiNormScoreByLimit(0,0,1,0,false,domainMasters,teacherDetailsBlank);
		
		double dScore = 0;
		System.out.println("lstRawData.size();;;;: "+lstRawData.size());
		teacherNormScoreDAO.truncate();*/
		
		try{
			/*
			  Stop norm score process
			  
			 for(Object oo: lstRawData){
				Object obj[] = (Object[])oo;
				teacherDetail = (TeacherDetail) obj[0];
				dScore = new Double(""+obj[1]);
				teacherNormScore = new TeacherNormScore();
				teacherNormScore.setCreatedDateTime(new Date());
				teacherNormScore.setTeacherDetail(teacherDetail);
				teacherNormScore.setTeacherNormScore((int)dScore);

				double percentile=cGReportService.getPercentile(pztList,dScore);
				//colorName =cGReportService.getColorName(percentile);
				Object[] ob =cGReportService.getDeciles(percentile);
				teacherNormScore.setDecileColor(""+ob[0]);
				//System.out.println(ob.length+" "+(Double)ob[1]+" " +(Double)ob[2]);
				teacherNormScore.setPercentile(percentile);
				teacherNormScore.setMinDecileValue((Double)ob[1]);
				teacherNormScore.setMaxDecileValue((Double)ob[2]);

				session1.insert(teacherNormScore);
				//System.out.println("dScore: "+dScore);
			}
*/
			// section 2
			teacherActionFeedDAO.truncate();
			//teacherNormScoreDAO.insertTeacherActionFeed(statusId);
			teacherNormScoreDAO.insertTeacherActionFeed(statusIdList);

			///////////////////////////////////////////////////////
			List lstJobs = jobOrderDAO.findByJobOrders();
			System.out.println("{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}} lstJobs.size(): "+lstJobs.size());
			Map<Integer,Integer> jobExpHiresMap = new HashMap<Integer,Integer>();
			
			List<JobOrder> lstJobOrders = new ArrayList<JobOrder>();

			for(Object oo: lstJobs){
				Object obj[] = (Object[])oo;
				//System.out.println(obj[0] + "  " + obj[1]+ "  " + obj[2]+ "  " + obj[3]+ "  " + obj[4]);
				JobOrder jo = new JobOrder();
				jo.setJobId((Integer)obj[0]);
				lstJobOrders.add(jo);

				if(obj[2]!=null)//sjo
				{
					if(obj[5]!=null)
					{ 
						Integer prevcnt = jobExpHiresMap.get(jo.getJobId()); 
						prevcnt = prevcnt==null?0:prevcnt;
						jobExpHiresMap.put(jo.getJobId(), prevcnt+(Integer)obj[5]);
					}
				}else //djo
				{
					jobExpHiresMap.put(jo.getJobId(), (Integer)obj[4]);
				}
			}
			System.out.println("lstJobOrders.size(): "+lstJobOrders.size());//djo
			List jftList1 = jobForTeacherDAO.countApplicantsByJobOrdersHQL(statusId, lstJobOrders);
			System.out.println("jftList1::::::::::::: "+jftList1.size());
			Map<String,JobActionFeed> jobAppliedMap = new HashMap<String, JobActionFeed>();
			
			for(Object oo: jftList1){
				Object obj[] = (Object[])oo;
				//System.out.println(obj[0] + "  " + obj[1]+ "  " + obj[2]+ "  " + obj[3]+ "  " + obj[4]+ "  " + obj[5]+ "  " + obj[6]+ "  " + obj[7]+" "+obj[8]+" "+obj[9]);
				JobActionFeed jaf = new JobActionFeed();
				jaf.setNoOfHiredCandidates(Integer.parseInt(String.valueOf(obj[2])));
				int applied = Integer.parseInt(String.valueOf(obj[1]));
				//int hired = Integer.parseInt(String.valueOf(obj[2]));
				//int ratio = ((Integer.parseInt(String.valueOf(obj[2]))/(Integer.parseInt(String.valueOf(obj[2]);
				//expectedHires;				totalApplicants;
				//System.out.println("totalApplied: "+applied);
				jaf.setTotalApplicants(applied);
				jaf.setCandidateVsOpeningRatio(applied);
				if(obj[4]!=null)
				jaf.setLastActivityOnJob(""+obj[4]);
				
				if(obj[5]!=null)
				{
					//java.sql.Timestamp ddd=((java.sql.Timestamp)obj[5]);
					//Date startDate = new Date(ddd.getTime());
					jaf.setLastActivityDate((Date)obj[5]);
				}
				if(obj[3]!=null)
					jaf.setLastActivityDate((Date)obj[3]);

				if(obj[6]!=null)
				jaf.setActivityDoneBy(""+obj[6]);

				jaf.setCriticalCandidatesWithHigherNormScore(Integer.parseInt(String.valueOf(obj[8])));
				jaf.setAttentionCandidatesWithHigherNormScore(Integer.parseInt(String.valueOf(obj[9])));

				jobAppliedMap.put(String.valueOf(obj[0]), jaf);
			}
			System.out.println("totalJobs: "+jobAppliedMap.size());
			
			
			//System.out.println(jobAppliedMap.get(""+102).getLastActivityDate());
			JobOrder jobOrder = null;
			JobActionFeed jafeed = null;
			DistrictMaster districtMaster = null;
			SchoolMaster schoolMaster = null;
			if(lstJobs.size()>0)
				jobActionFeedDAO.truncate();

			for(Object oo: lstJobs){
				Object obj[] = (Object[])oo;
				//System.out.println(obj[0] + "  " + obj[1]+ "  " + obj[2]+ "  " + obj[3]+ "  " + obj[4]);
				//System.out.println(jobAppliedMap.get(""+(Integer)obj[0]));
				jafeed = jobAppliedMap.get(""+(Integer)obj[0]);
				Integer noOfOpennings = jobExpHiresMap.get(Integer.parseInt(String.valueOf(obj[0])));
				
				jobOrder = new JobOrder();
				jobOrder.setJobId((Integer)obj[0]);
				
				if(obj[2]!=null)
				{
					schoolMaster = new SchoolMaster();
					schoolMaster.setSchoolId(Long.parseLong(""+(Integer)obj[2]));
					//List jftList2 = jobForTeacherDAO.countApplicantsByJobOrdersBySchoolHQL(statusId, jobOrder,schoolMaster);
					//System.out.println("llllllllllllllllllllllllllllllll: "+jftList2.size());
				}else
					schoolMaster=null;
				
				
				noOfOpennings = noOfOpennings==null?0:noOfOpennings;
				if(jafeed!=null)
				{	
					//jafeed.setJobactionfeedId(jobactionfeedId)
					int candidateVsOpeningRatio = 0;

					if(obj[3]!=null)
					{
						if(String.valueOf(obj[3])!="0")
							candidateVsOpeningRatio = noOfOpennings/Integer.parseInt(String.valueOf(obj[3]));
					}
					jafeed.setCandidateVsOpeningRatio(candidateVsOpeningRatio);

					if(noOfOpennings>0)
					{
						jafeed.setCriticalNormScoreVsOpeningRatio(jafeed.getCriticalCandidatesWithHigherNormScore()/noOfOpennings);
						jafeed.setAttentionNormScoreVsOpeningRatio(jafeed.getAttentionCandidatesWithHigherNormScore()/noOfOpennings);
					}
					else{
						jafeed.setCriticalNormScoreVsOpeningRatio(0);
						jafeed.setAttentionNormScoreVsOpeningRatio(0);
					}

				}else
				{
					jafeed = new JobActionFeed();
					if(obj[3]!=null)
					{
						jafeed.setCandidateVsOpeningRatio(Integer.parseInt(String.valueOf(obj[3])));
					}
					jafeed.setCandidateVsOpeningRatio(0);
					jafeed.setNoOfHiredCandidates(0);
					jafeed.setCriticalNormScoreVsOpeningRatio(0);
					jafeed.setAttentionNormScoreVsOpeningRatio(0);
					jafeed.setAttentionCandidatesWithHigherNormScore(0);
					jafeed.setCriticalCandidatesWithHigherNormScore(0);
					jafeed.setTotalApplicants(0);
				}

				jafeed.setExpectedHires(noOfOpennings);
				
				jafeed.setJobOrder(jobOrder);

				districtMaster = new DistrictMaster();
				districtMaster.setDistrictId((Integer)obj[1]);
				jafeed.setDistrictMaster(districtMaster);
				
				/*if(obj[2]!=null)
				{
					schoolMaster = new SchoolMaster();
					schoolMaster.setSchoolId(Long.parseLong(""+(Integer)obj[2]));
					jafeed.setSchoolMaster(schoolMaster);
				}*/
				
				jafeed.setSchoolMaster(schoolMaster);
				
				if(obj[1]!=null)
					jafeed.setJobActiveDays(Integer.parseInt(String.valueOf(obj[3])));

				jafeed.setCreatedDateTime(new Date());
				session1.insert(jafeed);

			}


		}catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("UpdateRecords -----------------------");
	
	}
	
}
