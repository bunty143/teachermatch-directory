package tm.services.rc.cloudfiles;

import com.google.common.io.ByteStreams;
import com.google.common.io.Closeables;
import org.jclouds.ContextBuilder;
import org.jclouds.openstack.swift.v1.domain.SwiftObject;
import org.jclouds.openstack.swift.v1.features.ObjectApi;
import org.jclouds.rackspace.cloudfiles.v1.CloudFilesApi;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
/**
 * Gets an object from a container and displays the results.
 *
 * NOTE: Run the {@link UploadObjects} example prior to running this example.
 *
 */
public class R5_GetObject implements Closeable {
   private final CloudFilesApi cloudFiles;

   /**
    * To get a username and API key see http://jclouds.apache.org/guides/rackspace/
    *
    * The first argument (args[0]) must be your username
    * The second argument (args[1]) must be your API key
    */
   public static void main(String[] args) throws IOException {
      R5_GetObject getObject = new R5_GetObject(RCConfig.RC_CLOUDFILE_USERNAME, RCConfig.RC_CLOUDFILE_APIKEY);

      try {
         SwiftObject swiftObject = getObject.getObject();
         getObject.writeObject(swiftObject);
      }
      catch (Exception e) {
         e.printStackTrace();
      }
      finally {
         getObject.close();
      }
   }

   public R5_GetObject(String username, String apiKey) {
      cloudFiles = ContextBuilder.newBuilder(RCConfig.RC_CLOUDFILE_PROVIDER)
            .credentials(username, apiKey)
            .buildApi(CloudFilesApi.class);

   }

   private SwiftObject getObject() {
      System.out.format("Get Object%n");

      ObjectApi objectApi = cloudFiles.getObjectApi(RCConfig.RC_CLOUDFILE_REGION_ORD, RCConfig.RC_CLOUDFILE_CONTAINER);
      SwiftObject swiftObject = objectApi.get("uploadObjectFromFile.txt");

      System.out.format("  %s%n", swiftObject);

      return swiftObject;
   }

   private void writeObject(SwiftObject swiftObject) throws IOException {
      System.out.format("Write Object%n");

      InputStream inputStream = swiftObject.getPayload().openStream();
      File file = File.createTempFile("uploadObjectFromFile", ".txt");
      BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));

      try {
         ByteStreams.copy(inputStream, outputStream);
      }
      finally {
         inputStream.close();
         outputStream.close();
      }

      System.out.format("  %s%n", file.getAbsolutePath());
   }

   /**
    * Always close your service when you're done with it.
    */
   public void close() throws IOException {
      Closeables.close(cloudFiles, true);
   }
}
