package tm.services.rc.cloudfiles;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.domain.Blob;
import org.jclouds.io.Payload;
import org.jclouds.io.Payloads;
import org.jclouds.openstack.swift.v1.blobstore.RegionScopedBlobStoreContext;
import org.jclouds.rackspace.cloudfiles.v1.CloudFilesApi;
import com.google.common.base.Charsets;
import com.google.common.io.ByteSource;
import com.google.common.io.Closeables;
import com.google.common.io.Files;

public class UploadObjects implements Closeable {
   private final BlobStore blobStore;
   private final CloudFilesApi cloudFiles;

   /**
    * To get a username and API key see http://jclouds.apache.org/guides/rackspace/
    *
    * The first argument (args[0]) must be your username
    * The second argument (args[1]) must be your API key
    */
   public static void main(String[] args) throws IOException {
      UploadObjects uploadContainer = new UploadObjects(RCConfig.RC_CLOUDFILE_USERNAME, RCConfig.RC_CLOUDFILE_APIKEY);

      try {
         //uploadContainer.uploadObjectFromFile();
         //uploadContainer.uploadObjectFromString();
         //uploadContainer.uploadObjectFromStringWithMetadata();
    	  
    	  uploadContainer.uploadObjectFromImageFile();
      }
      catch (IOException e) {
         e.printStackTrace();
      }
      finally {
         uploadContainer.close();
      }
   }

   public UploadObjects(String username, String apiKey) {
      ContextBuilder builder = ContextBuilder.newBuilder(RCConfig.RC_CLOUDFILE_PROVIDER)
                                  .credentials(username, apiKey);
      blobStore = builder.buildView(RegionScopedBlobStoreContext.class).getBlobStore(RCConfig.RC_CLOUDFILE_REGION_ORD);
      cloudFiles = blobStore.getContext().unwrapApi(CloudFilesApi.class);
   }

   
   /**
    * Upload an object from a File using the Swift API.
    */
   private void uploadObjectFromImageFile() throws IOException {
      System.out.format("Upload Object From File%n");

      //File largeFile = new File("E:\\test_files\\", "fme-effective-communication.pdf");
      File largeFile = new File("E:\\test_files\\", "Jellyfish.jpg");
      ByteSource source1 = Files.asByteSource(largeFile);
      Payload payload1 = Payloads.newByteSourcePayload(source1);
      String myFullFileName = largeFile.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("/") > 0) ? "/" : "/";
      int startIndex = myFullFileName.lastIndexOf(slashType);
      myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.lastIndexOf("."));
      myFileName=myFileName.replaceAll("[^\\w\\s]", "");
      String ext=myFullFileName.substring(myFullFileName.lastIndexOf("."),myFullFileName.length());
	  String fullFileName=myFileName+""+ext;
      try {
         cloudFiles.getObjectApi(RCConfig.RC_CLOUDFILE_REGION_ORD, RCConfig.RC_CLOUDFILE_CONTAINER).put("god/photo/"+fullFileName, payload1);

         System.out.format("  %s%s%n", myFileName, ext);
      } finally {
    	  //largeFile.delete();
      }
   }
   
   /**
    * Upload an object from a File using the Swift API.
    */
   private void uploadObjectFromFile() throws IOException {
      System.out.format("Upload Object From File%n");

      String filename = "2_uploadObjectFromFile";
      String suffix = ".txt";

      File tempFile = File.createTempFile(filename, suffix);

      try {
         Files.write("1 I am uploadObjectFromFile", tempFile, Charsets.UTF_8);

         ByteSource byteSource = Files.asByteSource(tempFile);
         Payload payload = Payloads.newByteSourcePayload(byteSource);

         cloudFiles.getObjectApi(RCConfig.RC_CLOUDFILE_REGION_ORD, RCConfig.RC_CLOUDFILE_CONTAINER)
            .put("shiva/textfiles/"+filename + suffix, payload);

         System.out.format("  %s%s%n", filename, suffix);
      } finally {
         tempFile.delete();
      }
   }

   /**
    * Upload an object from a String using the Swift API.
    */
   private void uploadObjectFromString() {
      System.out.format("Upload Object From String%n");

      String filename = "uploadObjectFromString.txt";

      ByteSource source = ByteSource.wrap("I am uploadObjectFromString".getBytes());
      Payload payload = Payloads.newByteSourcePayload(source);

      cloudFiles.getObjectApi(RCConfig.RC_CLOUDFILE_REGION_ORD, RCConfig.RC_CLOUDFILE_CONTAINER).put(filename, payload);

      System.out.format("  %s%n", filename);
   }

   /**
    * Upload an object from a String with metadata using the BlobStore API.
    */
   private void uploadObjectFromStringWithMetadata() {
      System.out.format("Upload Object From String With Metadata%n");

      String filename = "uploadObjectFromStringWithMetadata.txt";

      Map<String, String> userMetadata = new HashMap<String, String>();
      userMetadata.put("key1", "value1");

      ByteSource source = ByteSource.wrap("I am uploadObjectFromStringWithMetadata".getBytes());

      Blob blob = blobStore.blobBuilder(filename)
            .payload(Payloads.newByteSourcePayload(source))
            .userMetadata(userMetadata)
            .build();

      blobStore.putBlob(RCConfig.RC_CLOUDFILE_CONTAINER, blob);

      System.out.format("  %s%n", filename);
   }

   /**
    * Always close your service when you're done with it.
    */
   public void close() throws IOException {
      Closeables.close(blobStore.getContext(), true);
   }
}
