package tm.services.rc.cloudfiles;

import tm.services.rc.cloudfiles.RCConfig;
import java.io.Closeable;
import java.io.IOException;
import org.jclouds.ContextBuilder;
import org.jclouds.openstack.swift.v1.domain.Container;
import org.jclouds.rackspace.cloudfiles.v1.CloudFilesApi;
import com.google.common.io.Closeables;

public class GetContainer implements Closeable {
   private final CloudFilesApi cloudFiles;

  
   public static void main(String[] args) throws IOException {
      GetContainer getContainer = new GetContainer(RCConfig.RC_CLOUDFILE_USERNAME, RCConfig.RC_CLOUDFILE_APIKEY);

      try {
         getContainer.getContainer();
      }
      catch (Exception e) {
         e.printStackTrace();
      }
      finally {
         getContainer.close();
      }
   }

   public GetContainer(String username, String apiKey) {
      cloudFiles = ContextBuilder.newBuilder(RCConfig.RC_CLOUDFILE_PROVIDER)
            .credentials(username, apiKey)
            .buildApi(CloudFilesApi.class);
   }

   private void getContainer() {
      System.out.format("Get Container%n");

      Container container = cloudFiles.getContainerApi(RCConfig.RC_CLOUDFILE_REGION_ORD).get(RCConfig.RC_CLOUDFILE_CONTAINER);
      System.out.format("  %s%n", container);
   }

   /**
    * Always close your service when you're done with it.
    */
   public void close() throws IOException {
      Closeables.close(cloudFiles, true);
   }
}
