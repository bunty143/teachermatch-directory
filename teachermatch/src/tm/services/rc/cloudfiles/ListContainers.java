package tm.services.rc.cloudfiles;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import org.jclouds.ContextBuilder;
import org.jclouds.openstack.swift.v1.domain.Container;
import org.jclouds.rackspace.cloudfiles.v1.CloudFilesApi;
import com.google.common.io.Closeables;

public class ListContainers implements Closeable {
   private final CloudFilesApi cloudFiles;

   public static void main(String[] args) throws IOException {
      ListContainers listContainers = new ListContainers(RCConfig.RC_CLOUDFILE_USERNAME, RCConfig.RC_CLOUDFILE_APIKEY);

      try {
         listContainers.listContainers();
      }
      catch (Exception e) {
         e.printStackTrace();
      }
      finally {
         listContainers.close();
      }
   }

   public ListContainers(String username, String apiKey) {
      cloudFiles = ContextBuilder.newBuilder(RCConfig.RC_CLOUDFILE_PROVIDER)
            .credentials(username, apiKey)
            .buildApi(CloudFilesApi.class);
   }

   private void listContainers() {
      System.out.format("List Containers%n");

      List<Container> containers = cloudFiles.getContainerApi(RCConfig.RC_CLOUDFILE_REGION_ORD).list().toList();
      for (Container container: containers) {
         System.out.format("  %s%n", container);
      }
   }

   /**
    * Always close your service when you're done with it.
    */
   public void close() throws IOException {
      Closeables.close(cloudFiles, true);
   }
}
