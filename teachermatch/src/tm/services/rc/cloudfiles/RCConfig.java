package tm.services.rc.cloudfiles;

import tm.utility.Utility;

public class RCConfig 
{
	   public static String RC_CLOUDFILE_PROVIDER = System.getProperty("provider.cf", "rackspace-cloudfiles-us");
	   
	   public static String RC_CLOUDFILE_REGION_ORD = System.getProperty("region", "ORD"); // DFW ORD IAD//
	   public static String RC_CLOUDFILE_REGION_DFW = System.getProperty("region", "DFW");
	   
	 /*  public static String RC_CLOUDFILE_CONTAINER = "TM_NFS_LOCAL_CONTAINER_TEST";
	   public static String RC_CLOUDFILE_USERNAME="ramesh.bhartiya";
	   public static String RC_CLOUDFILE_APIKEY="bea3f8f2d3ed4532924148184a9befdf";
	   
	   public static boolean RC_CLOUDFILE_WRITE=true;
	   public static boolean RC_CLOUDFILE_READ=true;*/
	   
	   public static String RC_CLOUDFILE_CONTAINER=Utility.getValueOfPropByKey("RC_CLOUDFILE_CONTAINER");
	   
	   public static String RC_CLOUDFILE_USERNAME=Utility.getValueOfPropByKey("RC_CLOUDFILE_USERNAME");
	   public static String RC_CLOUDFILE_APIKEY=Utility.getValueOfPropByKey("RC_CLOUDFILE_APIKEY");
	   
	   public static boolean RC_CLOUDFILE_WRITE=Boolean.parseBoolean(Utility.getValueOfPropByKey("RC_CLOUDFILE_WRITE"));
	   public static boolean RC_CLOUDFILE_READ=Boolean.parseBoolean(Utility.getValueOfPropByKey("RC_CLOUDFILE_READ"));
	   
}
