package tm.services;

/**********************************
	 ***********************
	*  @Author Ram Nath  *
  **********************
********************************/

import static tm.services.district.GlobalServices.println;

import java.awt.Color;
import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import net.sf.json.JSONSerializer;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tm.api.UtilityAPI;
import tm.api.controller.JeffcoAPIController;
import tm.bean.DistrictApprovalGroups;
import tm.bean.DistrictSpecificApprovalFlow;
import tm.bean.DistrictWiseApprovalGroup;
import tm.bean.EligibilityStatusMaster;
import tm.bean.EligibilityVerificationHistroy;
import tm.bean.EmployeeMaster;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.SapCandidateDetails;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.EligibilityMaster;
import tm.bean.master.RaceMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictApprovalGroupsDAO;
import tm.dao.DistrictSpecificApprovalFlowDAO;
import tm.dao.DistrictWiseApprovalGroupDAO;
import tm.dao.EligibilityStatusMasterDAO;
import tm.dao.EligibilityVerificationHistroyDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.SapCandidateDetailsDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.JobForTeacherDAO.AutoInactiveJobOrderThread;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.EligibilityMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TeacherAnswerDetailsForDistrictSpecificQuestionsDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.district.GlobalServices;
import tm.services.teacher.AcceptOrDeclineMailHtml;
import tm.services.teacher.DistrictPortfolioConfigAjax;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

import edu.emory.mathcs.backport.java.util.Arrays;
@Repository
public class ONBAjaxNewImpl implements ONBAjaxNew{
	static String locale = Utility.getValueOfPropByKey("locale");
	static private Map<String,String> listStatus=new LinkedHashMap<String,String>();
	private static Map<String,String> allColorValueReturn = new LinkedHashMap<String,String>();
	private static Map<String,Boolean> allColorReturn = new LinkedHashMap<String,Boolean>();
	List<String> headerData=new LinkedList<String>();
	
	@Autowired
	private TeacherStatusHistoryForJobDAO  teacherStatusHistoryForJobDAO;
	
	@Autowired
	private StatusMasterDAO  statusMasterDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private EligibilityMasterDAO eligibilityMasterDAO;
	
	@Autowired
	private EligibilityVerificationHistroyDAO eligibilityVerificationHistroyDAO;
	
	@Autowired
	private EligibilityStatusMasterDAO eligibilityStatusMasterDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private SapCandidateDetailsDAO sapCandidateDetailsDAO;
	
	@Autowired
	private DistrictPortfolioConfigAjax districtPortfolioConfigAjax;
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private EmailerService emailerService;
	
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	
	@Autowired
	private TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	@Autowired
	private DistrictSpecificApprovalFlowDAO districtSpecificApprovalFlowDAO;
	@Autowired
	private DistrictWiseApprovalGroupDAO districtWiseApprovalGroupDAO;
	@Autowired
	private DistrictApprovalGroupsDAO districtApprovalGroupsDAO;
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;
	
	/*@Autowired
	private CityMasterDAO cityMasterDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;*/
	
	static{
		listStatus.put("A",Utility.getLocaleValuePropByKey("optAll", locale).trim());
		listStatus.put("N",Utility.getLocaleValuePropByKey("msgNew", locale).trim());
		listStatus.put("S",Utility.getLocaleValuePropByKey("lblSTPeopleSoft", locale).trim());
		listStatus.put("Q",Utility.getLocaleValuePropByKey("msgIntheQueue", locale).trim());
		listStatus.put("F",Utility.getLocaleValuePropByKey("lblFailed", locale).trim());
		
		allColorReturn.put("00882B",true);
		allColorReturn.put("FF0000",false);
		allColorReturn.put("FFFF00",true);
		allColorReturn.put("000000",false);
		allColorReturn.put("0000CC",false);
		allColorReturn.put("7F7F7F",false);
		allColorReturn.put("473F3F",false);
		allColorReturn.put("FFA500",false);
		allColorReturn.put("800080",true);
		
		allColorValueReturn.put("00882B",Utility.getLocaleValuePropByKey("lblYes", locale).trim());
		allColorValueReturn.put("FF0000",Utility.getLocaleValuePropByKey("lblNo", locale).trim());
		allColorValueReturn.put("FFFF00",Utility.getLocaleValuePropByKey("lblWaived", locale).trim());
		allColorValueReturn.put("000000",Utility.getLocaleValuePropByKey("lblReprint", locale).trim());
		allColorValueReturn.put("0000CC",Utility.getLocaleValuePropByKey("lblPending", locale).trim());
		allColorValueReturn.put("7F7F7F",Utility.getLocaleValuePropByKey("optIncomlete", locale).trim());
		allColorValueReturn.put("473F3F",Utility.getLocaleValuePropByKey("lblPrinted", locale).trim());
		allColorValueReturn.put("FFA500",Utility.getLocaleValuePropByKey("lblCHRPending", locale).trim());
		allColorValueReturn.put("800080","Provided by CDE");
	}
	
	/*ONBAjaxNewImpl(){
		headerData.add("Candidate");
		headerData.add("Position Number");
		headerData.add("Hired");
		headerData.add("Employee ID");
		headerData.add("Salary");
		headerData.add("BISI");
		headerData.add("Background");
		headerData.add("Paperwork");
		headerData.add("OS");
		headerData.add("Status");
	}*/
	
	private List<String[]> getRecords(Map<String,String> allMap)throws Exception{
		println("Ohoooooooooooooooooooooo Common service is start..............");
		WebContext context;
	    context = WebContextFactory.get();
	    HttpServletRequest request = context.getHttpServletRequest();
	    HttpSession session = request.getSession(false);
	    if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
	    allMap.put("report", "true");	
	    String status=allMap.get("status");
	    boolean reportFlag=allMap.get("report").trim().equals("true")?true:false;
	    UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
			return teacherStatusHistoryForJobDAO.hiredCandidateByDistrictId(allMap,userMaster,reportFlag,listStatus.get(status.trim()));
	}
	
	
	@SuppressWarnings("unchecked")
	public JSONObject displayONBGrid(Map<String,String> allMap) {
		println("Ohoooooooooooooooooooooo service is start..............");
		JSONObject hiredJson=new JSONObject();
		List<String[]> finalHiredTeachers=new ArrayList<String[]>();
		try{
		WebContext context;
	    context = WebContextFactory.get();
	    HttpServletRequest request = context.getHttpServletRequest();
	    HttpSession session = request.getSession(false);
	    if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
	    UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
	    Integer pageNo=allMap.get("pageNo")!=null?(allMap.get("pageNo").trim().equals("")?0:Integer.parseInt(allMap.get("pageNo"))):0;
		Integer noOfRow=allMap.get("noOfRow")!=null?(allMap.get("noOfRow").trim().equals("")?10:Integer.parseInt(allMap.get("noOfRow"))):10;
		int start=((pageNo-1)*noOfRow);
	    
		boolean reportFlag=allMap.get("report").trim().equals("true")?true:false;
		finalHiredTeachers=getRecords(allMap);
		//Map<String,String> mapThumbShowOrNot=getMapForThumbs(finalHiredTeachers);
		Map map = getMapForThumbs(finalHiredTeachers);
		Map<String,String> mapThumbShowOrNot=(Map<String, String>) map.get("job_teacherMap");
		Map<String,String> internal_teacherMap=(Map<String, String>) map.get("internal_teacherMap");
		
		long rowCount=0;
		if(reportFlag)
		hiredJson.put(rowCount, finalHiredTeachers.size()+"##"+GlobalServices.getTeacherOrUserName(userMaster)+"##"+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()));
		else{
		long totalRow=finalHiredTeachers.size();
		int last=finalHiredTeachers.size()<(start+noOfRow)?finalHiredTeachers.size():(start+noOfRow);
		finalHiredTeachers=finalHiredTeachers.subList(start, last);
		hiredJson.put(rowCount, totalRow);
		}
		if(finalHiredTeachers.size()>0){
			for (Iterator it = finalHiredTeachers.iterator(); it.hasNext();) {
	            Object[] row = (Object[]) it.next();
	            JSONArray hiredArray=new JSONArray();
	            hiredArray.add(row[0].toString());
	            hiredArray.add(row[1].toString());
	            hiredArray.add(row[2].toString());
	            hiredArray.add(row[3]!=null?row[3].toString():"");
	            hiredArray.add(Utility.convertDateAndTimeToUSformatOnlyDate((Date)(row[4]!=null?row[4]:row[5])));
	            hiredArray.add(row[6].toString());
	            hiredArray.add(row[7].toString());
	            hiredArray.add(row[8].toString());
	            hiredArray.add(row[9].toString());
	            hiredArray.add(row[10].toString());
	            hiredArray.add(row[11].toString());
	            hiredArray.add(row[12].toString());
	            hiredArray.add(row[13].toString());
	            hiredArray.add(row[14].toString());
	            hiredArray.add(row[15].toString());
	            hiredArray.add(row[16].toString());
	            hiredArray.add((mapThumbShowOrNot.get(row[8].toString()+"##"+row[7].toString())!=null?mapThumbShowOrNot.get(row[8].toString()+"##"+row[7].toString()):""));
	            hiredArray.add(internal_teacherMap.get(row[8].toString()+"##"+row[7].toString()));
	            hiredJson.put((++rowCount), hiredArray);
	        }			
		 }
		println("hiredJson========="+hiredJson.toString());
		return hiredJson;
		}catch(Exception e){e.printStackTrace();hiredJson=new JSONObject();return hiredJson;}
		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getStatusJSON(Map<String,String> allMap){
		println("Ohoooooooooooooooooooooo getStatusJSON is start..............");
		JSONObject tableStatusJson=new JSONObject();
		try{
		Long teacherStatusHistoryForJobId=Long.parseLong(allMap.get("teacherHistoryId")!=null?(allMap.get("teacherHistoryId").trim().equals("")?"0":allMap.get("teacherHistoryId")):"0");
		Integer eligibilityId=Integer.parseInt(allMap.get("eligibilityId")!=null?(allMap.get("eligibilityId").trim().equals("")?"0":allMap.get("eligibilityId")):"0");
		String empOrSalary=allMap.get("empOrSalary");
		TeacherStatusHistoryForJob TSHFJ=teacherStatusHistoryForJobDAO.findById(teacherStatusHistoryForJobId, false, false);
		System.out.println("TSHFJ.getTeacherDetail(),TSHFJ.getJobOrder()=="+TSHFJ.getTeacherDetail().getTeacherId()+"   "+TSHFJ.getJobOrder().getJobId());
		JobForTeacher jft=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(TSHFJ.getTeacherDetail(),TSHFJ.getJobOrder()).get(0);
		List<String[]> EVHList=new ArrayList<String[]>();
		EVHList=eligibilityVerificationHistroyDAO.findByEVHistory(TSHFJ,eligibilityId);
		long rowCount=0;
		String salary="";
		String employeeCode="";
		String BISICleared="";
		TeacherPersonalInfo tpi=teacherPersonalInfoDAO.findById(jft.getTeacherId().getTeacherId(), false, false);
		List<EmployeeMaster> empList=null;
		if(jft!=null && tpi!=null){
			List<Criterion> cri=new ArrayList<Criterion> ();
			cri.add(Restrictions.eq("employeeCode", tpi.getEmployeeNumber()));
			cri.add(Restrictions.eq("SSN", Utility.decodeBase64(tpi.getSSN())));
			cri.add(Restrictions.eq("dob", tpi.getDob()));
			cri.add(Restrictions.eq("districtMaster", jft.getJobId().getDistrictMaster()));
			empList=employeeMasterDAO.findByCriteria(cri.toArray(new Criterion[cri.size()]));
			if(empList!=null  && empList.size()>0){
				BISICleared=empList.get(0).getEmploymentStatus()!=null?(empList.get(0).getEmploymentStatus().trim().equalsIgnoreCase("A")?"Waived":""):"";
			}
		}
		if(jft!=null && tpi!=null && jft.getIsAffilated()!=null && jft.getIsAffilated()==1){
			employeeCode=tpi.getEmployeeNumber()!=null?tpi.getEmployeeNumber():"";
		}else if(jft!=null && tpi!=null){
			if(empList!=null  && empList.size()>0)
				employeeCode=empList.get(0).getEmployeeCode()!=null?empList.get(0).getEmployeeCode().trim():"";
		}
		
		println("EXternal Or Internal=========="+jft.getIsAffilated()+"   employeeCode========="+employeeCode);
		if(TSHFJ.getJobOrder().getJobCategoryMaster().getParentJobCategoryId()!=null)
			if(TSHFJ.getJobOrder().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryName().trim().equalsIgnoreCase("Hourly"))
				salary=TSHFJ.getJobOrder().getFsalary()!=null?TSHFJ.getJobOrder().getFsalary():"";
				else if(TSHFJ.getJobOrder().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryName().trim().equalsIgnoreCase("Administrator/Protech"))
				salary=TSHFJ.getJobOrder().getFsalaryA()!=null?TSHFJ.getJobOrder().getFsalaryA():"";
				
				if(!salary.trim().equals("")){
				Float litersOfPetrol=Float.parseFloat(salary);
				DecimalFormat df = new DecimalFormat("0.00");
				df.setMaximumFractionDigits(2);
				salary = df.format(litersOfPetrol);
				}
				
		tableStatusJson.put(rowCount, EVHList.size()+"##"+(jft.getStartDate()!=null?(new SimpleDateFormat("MM-dd-yyyy").format(jft.getStartDate())):"")+"##"+salary+"##"+employeeCode+"##"+BISICleared);
		String firstName="",lastName="";
		boolean entryDateShow=true;
		if(EVHList.size()>0){
			for (Iterator it = EVHList.iterator(); it.hasNext();) {
	            Object[] row = (Object[]) it.next();
	            JSONArray hiredArray=new JSONArray();
	            if(empOrSalary.trim().equals("emp")){
	            hiredArray.add(row[0].toString());
	            }else if(empOrSalary.trim().equals("salary")){
	            hiredArray.add(row[1]!=null?row[1].toString():"");
	            //hiredArray.add(row[2]!=null?row[2].toString():"");
	            hiredArray.add(row[3]!=null?row[3].toString():"");
	            hiredArray.add(row[10]!=null?Utility.convertDateAndTimeToUSformatOnlyDate((Date)(row[10])):"");
	            hiredArray.add(row[11]!=null?Utility.convertDateAndTimeToUSformatOnlyDate((Date)(row[11])):"");
	            entryDateShow=false;
	            }
	            if(entryDateShow)
	            hiredArray.add(Utility.convertDateAndTimeToUSformatOnlyDate((Date)(row[4])));	         
	            firstName=row[5]!=null?row[5].toString():"";
	            lastName=row[6]!=null?row[6].toString():"";
	            hiredArray.add(firstName+" "+lastName);
	            hiredArray.add(row[7].toString());
	            hiredArray.add(row[8]!=null?row[8].toString():"");
	            hiredArray.add(row[9].toString());
	            tableStatusJson.put((++rowCount), hiredArray);
	        }			
		 }
		}catch(Exception e){e.printStackTrace();}
		println("return json:::::"+tableStatusJson.toString());
		return tableStatusJson;
	}
	
	public boolean deleteStatusONB(Integer EVHId){
		println("Ohoooooooooooooooooooooo Delete Status service is start..............");
		boolean isDeleteflag=false;
		try{
			eligibilityVerificationHistroyDAO.makeTransient(eligibilityVerificationHistroyDAO.findById(EVHId, false, false));
			isDeleteflag=true;
		}catch(Exception e){e.printStackTrace();}
		return isDeleteflag;
	}
	
	public boolean saveNotesONB(Map<String,String> allMap){
		println("Ohoooooooooooooooooooooo saveNoteONB service is start..............");
		boolean isSaveflag=false;
		UserMaster userMaster=null;
		for(String s: allMap.keySet())
			println("key======"+s+" value========"+allMap.get(s));
		try{
			if(allMap.get("checkSession")==null || allMap.get("checkSession").equals("0")){
			WebContext context;
		    context = WebContextFactory.get();
		    HttpServletRequest request = context.getHttpServletRequest();
		    HttpSession session = request.getSession(false);
		    if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
		    userMaster=(UserMaster) session.getAttribute("userMaster");
			}
			String employeeId=allMap.get("employeeId");
			String salary=allMap.get("salary");
			String grade=allMap.get("grade");
			String step=allMap.get("step");
			String status=allMap.get("status")!=null?allMap.get("status").trim():allMap.get("status");
			String notes=allMap.get("notes");
			String empOrSalary=allMap.get("empOrSalary");
			String eligibilityId=allMap.get("eligibilityId");
			String teacherHistoryId=allMap.get("teacherHistoryId");
			TeacherStatusHistoryForJob objTSHFJ=teacherStatusHistoryForJobDAO.findById(new Long(teacherHistoryId), false, false);
			EligibilityVerificationHistroy newEVHOBJ=new EligibilityVerificationHistroy();
			newEVHOBJ.setTeacherDetail(objTSHFJ.getTeacherDetail());
			newEVHOBJ.setDistrictMaster(objTSHFJ.getJobOrder().getDistrictMaster());
			newEVHOBJ.setJobOrder(objTSHFJ.getJobOrder());
			EligibilityMaster em=new EligibilityMaster();
			em.setEligibilityId(new Integer(eligibilityId));
			newEVHOBJ.setEligibilityMaster(em);
			newEVHOBJ.setUserMaster(userMaster);
			newEVHOBJ.setNotes(notes);
			newEVHOBJ.setEligibilityStatusMaster(eligibilityStatusMasterDAO.getESMasterByEId_DIdAndSName(em,objTSHFJ.getJobOrder().getDistrictMaster(),status));
			if(empOrSalary.trim().equals("emp")){
			newEVHOBJ.setEmployeeId(employeeId);
			}
			if(empOrSalary.trim().equals("salary")){
				Date startDate=null;
				Date endDate=null;
				try{
					if(allMap.get("startDate")!=null && allMap.get("startDate")!="")
					 startDate=new SimpleDateFormat("MM-dd-yyyy").parse(allMap.get("startDate"));
					if(allMap.get("endDate")!=null && allMap.get("endDate")!="")
					  endDate=new SimpleDateFormat("MM-dd-yyyy").parse(allMap.get("endDate"));
					}catch(Exception e){e.printStackTrace();}
				/*newEVHOBJ.setGrade(grade);*/
				Boolean yearRound=allMap.get("yearRound").trim().equalsIgnoreCase("1")?true:false;
				String actualDays=allMap.get("actualDays");
				String totalDays=allMap.get("totalDays");
				newEVHOBJ.setYearRound(yearRound);
				newEVHOBJ.setActualDays(actualDays);
				newEVHOBJ.setTotalDays(totalDays);
				newEVHOBJ.setSalary(salary);
				newEVHOBJ.setStep(step);
				newEVHOBJ.setStartDate(startDate);
				newEVHOBJ.setEndDate(endDate);
			}
			newEVHOBJ.setCreatedDateTime(new Date());
			newEVHOBJ.setActivity("activity");
			eligibilityVerificationHistroyDAO.makePersistent(newEVHOBJ);
			isSaveflag=true;
		}catch(Exception e){e.printStackTrace();isSaveflag=false;}
		return isSaveflag;
	}
	
	public boolean sendToPeopleSoftONB(Long[] selectedTSHId){
		println("Ohoooooooooooooooooooooo sendToPeopleSoftONB service is start..............");
		boolean isSendToPeopleSoft=false;
		try{
			WebContext context;
		    context = WebContextFactory.get();
		    HttpServletRequest request = context.getHttpServletRequest();
		    HttpSession session = request.getSession(false);
		    if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
		    UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
		    
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			List<TeacherDetail> listTD=new ArrayList<TeacherDetail>();
			List<JobOrder> listJO=new ArrayList<JobOrder> ();
			List<Integer> teacherIdList=new ArrayList<Integer>();
			Map<String,JobForTeacher> JFTMap=new LinkedHashMap<String,JobForTeacher> ();
			Map<String,TeacherStatusHistoryForJob> TSHFJMap=new LinkedHashMap<String,TeacherStatusHistoryForJob> ();
			Map<String,SapCandidateDetails> SCDMap=new LinkedHashMap<String,SapCandidateDetails> ();
			Map<Integer,TeacherPersonalInfo> TPIMap=new LinkedHashMap<Integer,TeacherPersonalInfo>();
			/*StateMaster stateObj=stateMasterDAO.findByCriteria(Restrictions.eq("stateName", "No Response")).get(0);
			CityMaster cityObj=cityMasterDAO.findByCriteria(Restrictions.eq("cityName", "No Response")).get(0);*/
			println("selectedTSHId===="+Arrays.asList(selectedTSHId));
			
			List<TeacherStatusHistoryForJob> listTSHFJ=teacherStatusHistoryForJobDAO.findByCriteria(Restrictions.in("teacherStatusHistoryForJobId", Arrays.asList(selectedTSHId)));
			for(TeacherStatusHistoryForJob tshfj:listTSHFJ){
				TSHFJMap.put(tshfj.getTeacherDetail().getTeacherId()+"##"+tshfj.getJobOrder().getJobId(), tshfj);
				listTD.add(tshfj.getTeacherDetail());
				listJO.add(tshfj.getJobOrder());
				teacherIdList.add(tshfj.getTeacherDetail().getTeacherId());
			}
			List<JobForTeacher> listJFT=jobForTeacherDAO.findByCriteria(Restrictions.in("jobId", listJO),Restrictions.in("teacherId", listTD));
			for(JobForTeacher jft:listJFT){
				JFTMap.put(jft.getTeacherId().getTeacherId()+"##"+jft.getJobId().getJobId(), jft);
			}
			
			for(TeacherPersonalInfo tpi:teacherPersonalInfoDAO.findByCriteria(Restrictions.in("teacherId", teacherIdList))){
				TPIMap.put(tpi.getTeacherId(), tpi);
			}
			
			for(SapCandidateDetails scd:sapCandidateDetailsDAO.findByCriteria(Restrictions.in("teacherDetail", listTD),Restrictions.in("jobOrder", listJO))){
				SCDMap.put(scd.getTeacherDetail().getTeacherId()+"##"+scd.getJobOrder().getJobId(), scd);
			}
			
			int count =0;
			for(Map.Entry<String, TeacherStatusHistoryForJob> entry:TSHFJMap.entrySet()){
				println("key==="+entry.getKey()+" value=="+entry.getValue());
				if(count==0){
					TeacherStatusHistoryForJob tshfjObj=entry.getValue();
					JobForTeacher jftObj=JFTMap.get(tshfjObj.getTeacherDetail().getTeacherId()+"##"+tshfjObj.getJobOrder().getJobId());
					TeacherPersonalInfo tpiObj=TPIMap.get(entry.getValue().getTeacherDetail().getTeacherId());
					if(SCDMap.get(tshfjObj.getTeacherDetail().getTeacherId()+"##"+tshfjObj.getJobOrder().getJobId())==null){
					SapCandidateDetails SCDObj=new SapCandidateDetails();
					SCDObj.setTeacherDetail(tshfjObj.getTeacherDetail());
					SCDObj.setJobOrder(tshfjObj.getJobOrder());
					SCDObj.setHireDate((sdf.format(tshfjObj.getHiredByDate()!=null?tshfjObj.getHiredByDate():tshfjObj.getCreatedDateTime())));
					SCDObj.setEmployeeNumber(tpiObj.getEmployeeNumber()!=null?tpiObj.getEmployeeNumber():"");
					SCDObj.setPosition(tshfjObj.getJobOrder().getRequisitionNumber()!=null?tshfjObj.getJobOrder().getRequisitionNumber():"");
					SCDObj.setJobTitle(tshfjObj.getJobOrder().getJobTitle());
					SCDObj.setCandidateFirstName(tpiObj.getFirstName());
					SCDObj.setCandidateMiddleName(tpiObj.getMiddleName()!=null?tpiObj.getMiddleName():"");
					SCDObj.setCandidateLastName(tpiObj.getLastName());
					SCDObj.setCandidateSSN(tpiObj.getSSN()!=null?tpiObj.getSSN():"");
					
					SCDObj.setCandidateGender(tpiObj.getGenderId()!=null?tpiObj.getGenderId().getGenderId().toString():"0");
					
					SCDObj.setCandidateDOB(tpiObj.getDob()!=null?sdf.format(tpiObj.getDob()):"");
					SCDObj.setCandidateCorrespondLanguage("E");
					SCDObj.setCandidateStreet(tpiObj.getAddressLine1()!=null?tpiObj.getAddressLine1():"");
					SCDObj.setCandidateSecondAddressLine(tpiObj.getAddressLine2()!=null?tpiObj.getAddressLine2():"");
					
					
					SCDObj.setCandidateCity(tpiObj.getCityId()!=null?tpiObj.getCityId().getCityName().toString():"");
					
					SCDObj.setCandidateZip(tpiObj.getZipCode()!=null?tpiObj.getZipCode():"");
					SCDObj.setCandidateCountry(tpiObj.getCountryId()!=null?tpiObj.getCountryId().getShortCode().toString():"US");
					
					SCDObj.setCandidateState(tpiObj.getStateId()!=null?tpiObj.getStateId().getStateShortName().toString():"");
					
					SCDObj.setCandidateTelephoneNumber(tpiObj.getMobileNumber()!=null?tpiObj.getMobileNumber():"");
					SCDObj.setCandidateEthnicOrigin(tpiObj.getEthnicOriginId()!=null?tpiObj.getEthnicOriginId().getEthnicOriginCode():"09");
					//SCDObj.setCandidateTelephoneNumber2(tpiObj.get);
					//SCDObj.setCandidateMilitaryStatus(tpiObj.g);
					//SCDObj.setCandidateDisability(candidateDisability);
					SCDObj.setCandidateVeteranStatus(tpiObj.getIsVateran()!=null?(tpiObj.getIsVateran()==1?"X":""):"");
					SCDObj.setCandidateEthnicity(tpiObj.getEthnicityId()!=null?tpiObj.getEthnicityId().getEthnicityCode():"E3");
					println("tpiObj.getRaceId()::::::==="+tpiObj.getRaceId());
					setRaceCode(SCDObj, tpiObj);
					SCDObj.setNoOfTries(1);
					SCDObj.setSchoolMaster(jftObj.getSchoolMaster()!=null?jftObj.getSchoolMaster():null);
					SCDObj.setJobForTeacher(jftObj);
					SCDObj.setUserMaster(userMaster);
					SCDObj.setCreatedDateTime(new Date());
					SCDObj.setLastUpdateDateTime(new Date());
					String[] response=JeffcoAPIController.jcTMHireCandidate(jftObj,teacherPersonalInfoDAO.getSessionFactory(),tshfjObj,eligibilityVerificationHistroyDAO,teacherNormScoreDAO);
					println("response:::: "+Arrays.asList(response));	
					if(!response[0].trim().equals("")){
						try{
							net.sf.json.JSONObject jsonRequest = (net.sf.json.JSONObject) JSONSerializer.toJSON(response[0].trim().toString());
			        		String emailAddress = tshfjObj.getTeacherDetail().getEmailAddress();//jsonRequest.get("emailAddress")==null?"":UtilityAPI.parseString(jsonRequest.getString("emailAddress"));
				        	String emplid = jsonRequest.get("emplid")==null?"":UtilityAPI.parseString(jsonRequest.getString("emplid"));
				        	String status = jsonRequest.get("status")==null?"":UtilityAPI.parseString(jsonRequest.getString("status"));
				        	String message = jsonRequest.get("message")==null?"":UtilityAPI.parseString(jsonRequest.getString("message"));
				        	if(status.trim().equals("Success")){
				        		String insertDOB=tpiObj.getDob()!=null?(new SimpleDateFormat("MMddyyyy").format(tpiObj.getDob())):"";
				        		String sSN=tpiObj.getSSN()!=null?Utility.decodeBase64(tpiObj.getSSN()):"";
				        		if(emplid!=null && !emplid.trim().equalsIgnoreCase("") &&(employeeMasterDAO.findByCriteria(Restrictions.eq("districtMaster",tshfjObj.getJobOrder().getDistrictMaster()),Restrictions.eq("employeeCode", emplid.trim())).isEmpty()))
				        		districtPortfolioConfigAjax.insertOrUpdateEmployeeMaster(tshfjObj.getTeacherDetail(), emplid, tshfjObj.getJobOrder().getDistrictMaster(), insertDOB, sSN, null, null, null,emplid,null,null);
				        		SCDObj.setSapError(jsonRequest.toString());
				        		SCDObj.setSapStatus("S");
				        	}
							}catch(Exception e){
								SCDObj.setSapError(response[0].trim());
				        		SCDObj.setSapStatus("F");
							}
					}else{
						try{
						net.sf.json.JSONObject jsonRequest = (net.sf.json.JSONObject) JSONSerializer.toJSON(response[1].trim().toString());
		        		String HireFault = jsonRequest.get("HireFault")==null?"":UtilityAPI.parseString(jsonRequest.getString("HireFault"));
		        		net.sf.json.JSONObject allInfo = (net.sf.json.JSONObject) JSONSerializer.toJSON(HireFault);
			        	String fault_data = allInfo.get("fault_data")==null?"":UtilityAPI.parseString(allInfo.getString("fault_data"));
			        	SCDObj.setSapError(fault_data);
			        	SCDObj.setSapStatus("F");
						}catch ( Exception e) {
							SCDObj.setSapError(response[1].trim());
				        	SCDObj.setSapStatus("F");
						}
			        	
					}
					println("tpiObj.getAddressLine1()==="+tpiObj.getAddressLine1());
					println("hired==="+(sdf.format(tshfjObj.getHiredByDate()!=null?tshfjObj.getHiredByDate():tshfjObj.getCreatedDateTime())));
					sapCandidateDetailsDAO.makePersistent(SCDObj);
					saveCommunicationLog(SCDObj, userMaster,request);
					if(SCDObj.getSapStatus().trim().equals("S")){
						Set<UserMaster> listUserSet=getGroupWiseAllUser(SCDObj.getJobOrder(),SCDObj.getJobOrder().getSchool().get(0));
						if(listUserSet.size()>0){
							new Thread(new SendNotificationThreadSuccessSTP(listUserSet,SCDObj.getJobOrder(),SCDObj.getJobOrder().getSchool().get(0),SCDObj.getTeacherDetail(),emailerService)).start();
						}
					}
					isSendToPeopleSoft=true;
					}else{
						SapCandidateDetails SCDObj=SCDMap.get(tshfjObj.getTeacherDetail().getTeacherId()+"##"+tshfjObj.getJobOrder().getJobId());
						String[] response=JeffcoAPIController.jcTMHireCandidate(jftObj,teacherPersonalInfoDAO.getSessionFactory(),tshfjObj,eligibilityVerificationHistroyDAO,teacherNormScoreDAO);
						println("response:::: "+Arrays.asList(response));
						if(!response[0].trim().equals("")){
							try{
							net.sf.json.JSONObject jsonRequest = (net.sf.json.JSONObject) JSONSerializer.toJSON(response[0].trim().toString());
			        		String emailAddress = tshfjObj.getTeacherDetail().getEmailAddress();//jsonRequest.get("emailAddress")==null?"":UtilityAPI.parseString(jsonRequest.getString("emailAddress"));
				        	String emplid = jsonRequest.get("emplid")==null?"":UtilityAPI.parseString(jsonRequest.getString("emplid"));
				        	String status = jsonRequest.get("status")==null?"":UtilityAPI.parseString(jsonRequest.getString("status"));
				        	String message = jsonRequest.get("message")==null?"":UtilityAPI.parseString(jsonRequest.getString("message"));
				        	if(status.trim().equals("Success")){
				        		SCDObj.setSapError(jsonRequest.toString());
				        		SCDObj.setSapStatus("S");
				        		String insertDOB=tpiObj.getDob()!=null?(new SimpleDateFormat("MMddyyyy").format(tpiObj.getDob())):"";
				        		String sSN=tpiObj.getSSN()!=null?Utility.decodeBase64(tpiObj.getSSN()):"";
				        		if(emplid!=null && !emplid.trim().equalsIgnoreCase("") &&(employeeMasterDAO.findByCriteria(Restrictions.eq("districtMaster",tshfjObj.getJobOrder().getDistrictMaster()),Restrictions.eq("employeeCode", emplid.trim())).isEmpty()))
				        		districtPortfolioConfigAjax.insertOrUpdateEmployeeMaster(tshfjObj.getTeacherDetail(), emplid, tshfjObj.getJobOrder().getDistrictMaster(), insertDOB, sSN, null, null, null,emplid,null,null);
				        	}
							}catch(Exception e){
								SCDObj.setSapError(response[0].trim());
				        		SCDObj.setSapStatus("F");
							}
						}else{
							try{
								net.sf.json.JSONObject jsonRequest = (net.sf.json.JSONObject) JSONSerializer.toJSON(response[1].trim().toString());
				        		String HireFault = jsonRequest.get("HireFault")==null?"":UtilityAPI.parseString(jsonRequest.getString("HireFault"));
				        		net.sf.json.JSONObject allInfo = (net.sf.json.JSONObject) JSONSerializer.toJSON(HireFault);
					        	String fault_data = allInfo.get("fault_data")==null?"":UtilityAPI.parseString(allInfo.getString("fault_data"));
					        	SCDObj.setSapError(fault_data);
					        	SCDObj.setSapStatus("F");
								}catch(Exception e){
									SCDObj.setSapError(response[1].trim());
						        	SCDObj.setSapStatus("F");
								}
				        	
						}
						SCDObj.setUserMaster(userMaster);
						SCDObj.setLastUpdateDateTime(new Date());
						SCDObj.setNoOfTries(SCDObj.getNoOfTries()+1);
						sapCandidateDetailsDAO.makePersistent(SCDObj);
						saveCommunicationLog(SCDObj, userMaster,request);
						if(SCDObj.getSapStatus().trim().equals("S")){
							Set<UserMaster> listUserSet=getGroupWiseAllUser(SCDObj.getJobOrder(),SCDObj.getJobOrder().getSchool().get(0));
							if(listUserSet.size()>0){
								new Thread(new SendNotificationThreadSuccessSTP(listUserSet,SCDObj.getJobOrder(),SCDObj.getJobOrder().getSchool().get(0),SCDObj.getTeacherDetail(),emailerService)).start();
							}
						}
						isSendToPeopleSoft=true;
					}
				}
				count++;
			}
		}catch(Exception e){
			e.printStackTrace();
			isSendToPeopleSoft=false;
			}
		return isSendToPeopleSoft;
	}
	private void setRaceCode(SapCandidateDetails SCDObj, TeacherPersonalInfo tpiObj){
		println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>set Reace Code><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		try{
			List<RaceMaster> lstRace= null;		   	 
			lstRace = raceMasterDAO.findByCriteria(Order.asc("orderBy"),Restrictions.ne("raceId",0),Restrictions.eq("status","A"));
			Map<String,RaceMaster> raceMap = new HashMap<String, RaceMaster>();
			for (RaceMaster raceMaster : lstRace) {
				raceMap.put(raceMaster.getRaceId().toString(), raceMaster);
			}	
			
			String raceIds[] = null;
			if(tpiObj.getRaceId()!=null)
			{
				raceIds = tpiObj.getRaceId().split(",");
			}else{
			   SCDObj.setCandidateRac01("R6");
			}
			if(raceIds!=null && raceIds.length>0)
			{
				for (int i = 0; i < raceIds.length; i++) {
					RaceMaster raceMaster = raceMap.get(raceIds[i]);
					if(raceMaster!=null  && i == 0)
						SCDObj.setCandidateRac01(raceMaster.getRaceCode());
					if(raceMaster!=null  && i == 1)
						SCDObj.setCandidateRac02(raceMaster.getRaceCode());
					if(raceMaster!=null  && i == 2)
						SCDObj.setCandidateRac03(raceMaster.getRaceCode());
					if(raceMaster!=null  && i == 3)
						SCDObj.setCandidateRac04(raceMaster.getRaceCode());
					if(raceMaster!=null  && i == 4)
						SCDObj.setCandidateRac05(raceMaster.getRaceCode());
					if(raceMaster!=null  && i == 5)
						SCDObj.setCandidateRac06(raceMaster.getRaceCode());
					if(raceMaster!=null  && i == 6)
						SCDObj.setCandidateRac07(raceMaster.getRaceCode());
					if(raceMaster!=null  && i == 7)
						SCDObj.setCandidateRac08(raceMaster.getRaceCode());
					if(raceMaster!=null  && i == 8)
						SCDObj.setCandidateRac09(raceMaster.getRaceCode());
					if(raceMaster!=null  && i == 9)
						SCDObj.setCandidateRac10(raceMaster.getRaceCode());
				}
			}else{
			   SCDObj.setCandidateRac01("R6");
			}
			if(SCDObj.getCandidateRac01()==null)SCDObj.setCandidateRac01("R6");;
		}catch(Exception e){e.printStackTrace();}
		
	}
	
	public String generateOnboardingPDF(Map<String,String> allMap)
	{	
		println("Ohoooooooooooooooooooooo PDF service is start..............");
		try
		{	WebContext context;
		    context = WebContextFactory.get();
		    HttpServletRequest request = context.getHttpServletRequest();
		    HttpSession session = request.getSession(false);
		    if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
		    UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			int userId = userMaster.getUserId();
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"jeffco_onboarding.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			println("user/"+userId+"/"+fileName);

			try{
				List<String[]> finalHiredTeachers=getRecords(allMap);
				List<String> data=GlobalServices.createList();
				String candName="",teacherEmail="",postionNumber="",hiredDate="",teacherIdColor="",salaryColor="",BISI="",background="",paperwork="",statusSTP="",schoolName="";
				if(finalHiredTeachers.size()>0){
					for (Iterator it = finalHiredTeachers.iterator(); it.hasNext();) {
			            Object[] row = (Object[]) it.next();
			            candName=((row[0]!=null?row[0].toString():"")+" "+(row[1]!=null?row[1].toString():"")); 
			            teacherEmail=row[2]!=null?row[2].toString():""; 
			            postionNumber=row[3]!=null?row[3].toString():""; 
			            hiredDate=Utility.convertDateAndTimeToUSformatOnlyDate((Date)(row[4]!=null?row[4]:row[5]));
			            statusSTP=row[9]!=null?row[9].toString():"";
			            schoolName=row[11]!=null?row[11].toString():"";
			            teacherIdColor=row[12]!=null?row[12].toString():"";
			            salaryColor=row[13]!=null?row[13].toString():"";
			            BISI=row[14]!=null?row[14].toString():"";
			            background=row[15]!=null?row[15].toString():"";
			            paperwork=row[16]!=null?row[16].toString():"";
						data.add(candName+"\n("+teacherEmail+")");
						data.add(postionNumber);
						data.add(schoolName);
						data.add(hiredDate);
						data.add(allColorValueReturn.get(teacherIdColor));
						//data.add(allColorValueReturn.get(BISI));
						data.add(allColorValueReturn.get(background));
						data.add(allColorValueReturn.get(paperwork));
						data.add(allColorValueReturn.get(salaryColor));
						data.add(getShowHideAndOSValue(teacherIdColor,salaryColor,background,paperwork,1));
						data.add(statusSTP);
					}
				}
				String createdBy=Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": ";
				String onDate=Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": ";
				String imagePath=fontPath+"/images/";
				String imageName="Logo with Beta300.png";
				
				println(""+data);
				MakePDF pdf=new MakePDF();
				pdf.setBaseFont(fontPath, "fonts/tahoma.ttf");
				Document document=pdf.createDocument(null,10f,10f,50f,50f, true);
				pdf.createPdfWriter(document, basePath,fileName);
				pdf.setAuthorAndSubject(document, "TeacherMatch", "Onboarding", "Onboarding Dashboard");
				document.addCreator("TeacherMatch Inc.");
				document.addCreationDate();
				pdf.openDocument(document);
				document.add(new Phrase("\n\n"));
				pdf.addImageInPDFCell(imagePath, imageName, 75f, "center", document, null);
				document.add(new Phrase("\n"));
				PdfPTable tableHeader=pdf.createPdfTable(2, 100,new float[] {500f, 500f}, "center");
				PdfPCell headerOnboarding1=pdf.createPDFCell("Onboarding Dashboard", "center", "bold", 20, 0, 100, new Color(0,122,180), null, 2);
				PdfPCell headerOnboarding2=pdf.createPDFCell("", "center", "bold", 20, 0, 100, new Color(0,122,180), null, 2);
				PdfPCell headerOnboarding3=pdf.createPDFCell(createdBy+GlobalServices.getTeacherOrUserName(userMaster), "left", "bold", 10, 0, 100, Color.BLACK, null, null);
				PdfPCell headerOnboarding4=pdf.createPDFCell(onDate+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()), "right", "bold", 10, 0, 100, Color.BLACK, null, null);
				tableHeader.addCell(headerOnboarding1);
				tableHeader.addCell(headerOnboarding2);
				tableHeader.addCell(headerOnboarding3);
				tableHeader.addCell(headerOnboarding4);
				tableHeader.addCell(headerOnboarding2);
				pdf.addTableInDocument(document, tableHeader);
				PdfPTable table=pdf.createPdfTable(10, 100,new float[] {210f, 110f, 130f, 90f, 90f, 90f, 90f,90f, 90f, 90f, 120f}, "center");
				pdf.addCellInHeaderPDFTable(table, headerData, "center", "bold", 9, -1, Color.white,new Color(0,122,180),0);
				if(finalHiredTeachers.size()>0){
				pdf.addCellInPDFTable(table, data, "left", "normal", 9, -1,Color.black,Color.white,0);
				}else{
					PdfPCell norecords=pdf.createPDFCell(Utility.getLocaleValuePropByKey("lblNoRecord", locale), "left", "bold", 9, -1, 100, Color.black, null, 11);
					table.addCell(norecords);
				}
				pdf.closeDocument(document, table);
				println("yeeeeeeeeee");
				
					}catch(Exception e){
						e.printStackTrace();
					}
			
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}
	
	public String generateOnboardingExcel(Map<String,String> allMap){
		println("Ohoooooooooooooooooooooo Excel service is start..............");
		String fileName ="";
		WebContext context;
	    context = WebContextFactory.get();
	    HttpServletRequest request = context.getHttpServletRequest();
	    HttpSession session = request.getSession(false);
	    if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			List<String[]> finalHiredTeachers=getRecords(allMap);
					//  Excel   Exporting	
						String time = String.valueOf(System.currentTimeMillis()).substring(6);
						String basePath = request.getSession().getServletContext().getRealPath ("/")+"/joblist";
						println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
						fileName ="jeffco_onboarding"+time+".xls";
						File file = new File(basePath);
						if(!file.exists())
							file.mkdirs();
						Utility.deleteAllFileFromDir(basePath);
						file = new File(basePath+"/"+fileName);
						WorkbookSettings wbSettings = new WorkbookSettings();
						wbSettings.setLocale(new Locale("en", "EN"));
						WritableCellFormat timesBoldUnderline;
						WritableCellFormat header;
						WritableCellFormat headerBold;
						WritableCellFormat times;
						WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
						workbook.createSheet("Onboarding Dashboard", 0);
						WritableSheet excelSheet = workbook.getSheet(0);
						WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
						// Define the cell format
						times = new WritableCellFormat(times10pt);
						// Lets automatically wrap the cells
						times.setWrap(true);
				
						// Create create a bold font with unterlines
						WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
						WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);
				
						timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
						timesBoldUnderline.setAlignment(Alignment.CENTRE);
						// Lets automatically wrap the cells
						timesBoldUnderline.setWrap(true);
				
						header = new WritableCellFormat(times10ptBoldUnderline);
						headerBold = new WritableCellFormat(times10ptBoldUnderline);
						CellView cv = new CellView();
						cv.setFormat(times);
						cv.setFormat(timesBoldUnderline);
						cv.setAutosize(true);
						header.setBackground(Colour.GRAY_25);
				
						// Write a few headers
						excelSheet.mergeCells(0, 0, 10, 1);
						Label label;
						label = new Label(0, 0,  "Onboarding Dashboard" , timesBoldUnderline);
						excelSheet.addCell(label);
						excelSheet.mergeCells(0, 2, 10, 2);
						label = new Label(0, 6, "");
						excelSheet.addCell(label);
						excelSheet.getSettings().setDefaultColumnWidth(18);
						
						int k=3;
						int col=1;
						for(col=0; col<headerData.size();col++){
						label = new Label(col, k, headerData.get(col),header); 
						excelSheet.addCell(label);
						}
						
						k=k+1;
						if(finalHiredTeachers.size()==0)
						{	
							excelSheet.mergeCells(0, k, 10, k);
							label = new Label(0, k, Utility.getLocaleValuePropByKey("lblNoRecord", locale)); 
							excelSheet.addCell(label);
						}
				String sta="";
				if(finalHiredTeachers.size()>0)
				{String candName="",teacherEmail="",postionNumber="",hiredDate="",teacherIdColor="",salaryColor="",BISI="",background="",paperwork="",statusSTP="",schoolName="";
				 for (Iterator it = finalHiredTeachers.iterator(); it.hasNext();) 					            
						{
					 	Object[] row = (Object[]) it.next();
					 	candName=((row[0]!=null?row[0].toString():"")+" "+(row[1]!=null?row[1].toString():"")); 
			            teacherEmail=row[2]!=null?row[2].toString():""; 
			            postionNumber=row[3]!=null?row[3].toString():""; 
			            hiredDate=Utility.convertDateAndTimeToUSformatOnlyDate((Date)(row[4]!=null?row[4]:row[5]));
			            statusSTP=row[9]!=null?row[9].toString():"";
			            schoolName=row[11]!=null?row[11].toString():"";
			            teacherIdColor=row[12]!=null?row[12].toString():"";
			            salaryColor=row[13]!=null?row[13].toString():"";
			            BISI=row[14]!=null?row[14].toString():"";
			            background=row[15]!=null?row[15].toString():"";
			            paperwork=row[16]!=null?row[16].toString():"";
							col=0;
						    label = new Label(col, k, candName+"\n("+teacherEmail+")"); 
							excelSheet.addCell(label);
							label = new Label(++col, k,postionNumber); 
							excelSheet.addCell(label);
							label = new Label(++col, k,schoolName); 
							excelSheet.addCell(label);
							label = new Label(++col, k, hiredDate); 
							excelSheet.addCell(label);
							label = new Label(++col, k,allColorValueReturn.get(teacherIdColor)); 
							excelSheet.addCell(label);
							/*label = new Label(++col, k,allColorValueReturn.get(BISI)); 
							excelSheet.addCell(label);*/
							label = new Label(++col, k,allColorValueReturn.get(background)); 
							excelSheet.addCell(label);
							label = new Label(++col, k,allColorValueReturn.get(paperwork)); 
							excelSheet.addCell(label);
							label = new Label(++col, k,allColorValueReturn.get(salaryColor)); 
							excelSheet.addCell(label);
							label = new Label(++col, k, getShowHideAndOSValue(teacherIdColor,salaryColor,background,paperwork,1)); 
							excelSheet.addCell(label);
							label = new Label(++col, k,statusSTP); 
							excelSheet.addCell(label);	
						   k++; 
				}
				}
				workbook.write();
				workbook.close();
				}catch(Exception e){
				e.printStackTrace();
				}
				println("return=="+fileName);
				return fileName;
	}
	
	public String onBoardingSendMailToTeacher(Long teacherHistoryId)
	{
		try
		{
			println("::::::::::::::: onBoardingSendMailToTeacher ::::::::::::::::::::::");
					
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
			    throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			
			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			DistrictMaster districtMaster = userMaster.getDistrictId();
			
			println("teacherHistoryId:- "+teacherHistoryId);
			if(districtMaster!=null  && teacherHistoryId!=null )
			{
				Boolean isSendMailToTeacher=false;
				TeacherStatusHistoryForJob teacherStatusHistoryForJob =  teacherStatusHistoryForJobDAO.findById(teacherHistoryId, false, false);
					List<EligibilityVerificationHistroy>  evhList=eligibilityVerificationHistroyDAO.findByLastEntryEVHistoryByJobOrderAndTeacherDetail(teacherStatusHistoryForJob.getTeacherDetail(), teacherStatusHistoryForJob.getJobOrder());
					List<String> colorCode=new ArrayList<String>();
					System.out.println("Size===="+evhList.size());
						for(EligibilityVerificationHistroy evh:evhList){
							if(!evh.getEligibilityMaster().getEligibilityName().trim().equalsIgnoreCase("BISI")){
								colorCode.add(evh.getEligibilityStatusMaster().getStatusColorCode());
								System.out.println("evh=="+evh.getEligibilityMaster().getEligibilityName());
							}
						}
					if(colorCode.size()>3)
					isSendMailToTeacher=getShowHideAndOSValue(colorCode.get(0),colorCode.get(1),colorCode.get(2),colorCode.get(3),1).trim().equalsIgnoreCase("Complete")?true:false;	
					println("After isSendMailToTeacher:- "+isSendMailToTeacher);
					if(isSendMailToTeacher)
					{
						//Code For Sending the mail to Candidate
						println("-----------------Start Sending mail To Teacher------------------");						
						SchoolMaster schoolMaster=null;
						try
						{
							List<SchoolInJobOrder> schoolInJobOrderList = schoolInJobOrderDAO.findJobOrder(teacherStatusHistoryForJob.getJobOrder());
							if(schoolInJobOrderList!=null && schoolInJobOrderList.size()>0)
								schoolMaster = schoolInJobOrderList.get(0).getSchoolId();
						}
						catch(Exception e){ e.printStackTrace(); }
						
						String sEmailBodyText = MailText.getPNrTextMail(teacherStatusHistoryForJob.getJobOrder(),schoolMaster);
						String sEmailSubject="Invitation to Attend Jeffco New Employee Orientation";
						
						DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("shadab.ansari@netsutra.com");
						dsmt.setMailto(teacherStatusHistoryForJob.getTeacherDetail().getEmailAddress());
						dsmt.setMailsubject(sEmailSubject);
						dsmt.setMailcontent(sEmailBodyText);
						
						println("sEmailSubject:- "+sEmailSubject);
						println("sEmailBodyText:- "+sEmailBodyText);
						
						try
						{
							dsmt.start();
						}
						catch (Exception e) {e.printStackTrace();}
						
						println("-----------------End Sending mail To Teacher------------------");
					}
				
				/*
				List<EligibilityMaster> eligibilityMasterList = null;
				Map<Integer, EligibilityVerificationHistroy> eligibilityVerificationHistroyMap = new HashMap<Integer, EligibilityVerificationHistroy>();
				if(teacherStatusHistoryForJob!=null && teacherStatusHistoryForJob.getTeacherDetail()!=null && teacherStatusHistoryForJob.getJobOrder()!=null)
				{
					Criterion c1 = Restrictions.eq("teacherDetail", teacherStatusHistoryForJob.getTeacherDetail());
					Criterion c2 = Restrictions.eq("jobOrder", teacherStatusHistoryForJob.getJobOrder());
					Criterion c3 = Restrictions.eq("districtMaster", districtMaster);
					
					List<EligibilityVerificationHistroy> eligibilityVerificationHistroyList = eligibilityVerificationHistroyDAO.findByCriteria(c1,c2,c3);
					
					if(eligibilityVerificationHistroyList!=null)
					{
						Criterion criterion1 = Restrictions.eq("districtId", districtMaster.getDistrictId());
						Criterion criterion2 = Restrictions.ne("eligibilityCode", "OS");
						Criterion criterion3 = Restrictions.eq("status", "A");
						eligibilityMasterList = eligibilityMasterDAO.findByCriteria(criterion1,criterion2,criterion3);
						
						for(EligibilityVerificationHistroy eligibilityVerificationHistroy : eligibilityVerificationHistroyList)
						{
							EligibilityVerificationHistroy mapId = eligibilityVerificationHistroyMap.get(eligibilityVerificationHistroy.getEligibilityMaster().getEligibilityId());
							if(mapId==null || mapId.getEligibilityVerificationId()<eligibilityVerificationHistroy.getEligibilityVerificationId())
								eligibilityVerificationHistroyMap.put(eligibilityVerificationHistroy.getEligibilityMaster().getEligibilityId(), eligibilityVerificationHistroy);
						}
					}
				}
				
				if(eligibilityMasterList!=null && eligibilityMasterList.size()>0 && eligibilityVerificationHistroyMap.size()>0)
				{
					println("inside if");
					println("teacherDetail:- "+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
					println("jobOrder:- "+teacherStatusHistoryForJob.getJobOrder());
					
					println("Before isSendMailToTeacher:- "+isSendMailToTeacher);
					for(EligibilityMaster em : eligibilityMasterList)
					{
						EligibilityVerificationHistroy verificationHistroy = eligibilityVerificationHistroyMap.get(em.getEligibilityId());
						
						if(!verificationHistroy.getEligibilityMaster().getEligibilityName().equalsIgnoreCase("BISI")){
						if(verificationHistroy==null)
							isSendMailToTeacher=false;
						else
							isSendMailToTeacher = verificationHistroy.getEligibilityStatusMaster()!=null && verificationHistroy.getEligibilityStatusMaster().getStatusColorCode()!=null && (verificationHistroy.getEligibilityStatusMaster().getStatusColorCode().equalsIgnoreCase("00882B") || verificationHistroy.getEligibilityStatusMaster().getStatusColorCode().equalsIgnoreCase("FFFF00"));
						
						if(verificationHistroy!=null)
							println("Color Status:- "+verificationHistroy.getEligibilityStatusMaster().getStatusColorCode()+", eligibilityStatusId:- "+verificationHistroy.getEligibilityStatusMaster().getEligibilityStatusId()+", em.getEligibilityId():- "+em.getEligibilityId());
						else
							println("verificationHistroy  is null for "+em.getEligibilityId());
						
						if(!isSendMailToTeacher)
							break;
					}
					}
					
					println("After isSendMailToTeacher:- "+isSendMailToTeacher);
					if(isSendMailToTeacher)
					{
						//Code For Sending the mail to Candidate
						println("-----------------Start Sending mail To Teacher------------------");
						
						SchoolMaster schoolMaster=null;
						try
						{
							List<SchoolInJobOrder> schoolInJobOrderList = schoolInJobOrderDAO.findJobOrder(teacherStatusHistoryForJob.getJobOrder());
							if(schoolInJobOrderList!=null && schoolInJobOrderList.size()>0)
								schoolMaster = schoolInJobOrderList.get(0).getSchoolId();
						}
						catch(Exception e){ e.printStackTrace(); }
						
						String sEmailBodyText = MailText.getPNrTextMail(teacherStatusHistoryForJob.getJobOrder(),schoolMaster);
						String sEmailSubject="Invitation to Attend Jeffco New Employee Orientation";
						
						DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("shadab.ansari@netsutra.com");
						dsmt.setMailto(teacherStatusHistoryForJob.getTeacherDetail().getEmailAddress());
						dsmt.setMailsubject(sEmailSubject);
						dsmt.setMailcontent(sEmailBodyText);
						
						println("sEmailSubject:- "+sEmailSubject);
						println("sEmailBodyText:- "+sEmailBodyText);
						
						try
						{
							dsmt.start();
						}
						catch (Exception e) {e.printStackTrace();}
						
						println("-----------------End Sending mail To Teacher------------------");
					}
				}*/
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "";
		}		
		return "true";
	}
	
	public Long getJFTIdByJobIdAndTeacherId(Map<String,String> allMap){
		try{
			Integer jobId=0;
			Integer teacherId=0;
				try{
					 jobId=Integer.parseInt(allMap.get("jobId"));
					 teacherId=Integer.parseInt(allMap.get("teacherId"));
				}catch (NumberFormatException e) {e.printStackTrace();}
			return jobForTeacherDAO.findByCriteria(Restrictions.eq("jobId", jobId),Restrictions.eq("teacherId", teacherId)).get(0).getJobForTeacherId();
		}catch(Exception e){e.printStackTrace(); return 0l;}
	}
	
	private String getShowHideAndOSValue(String empColor,String salaryColor,String background,String paperwork ,int keyReturn){
		if(allColorReturn.get(empColor) && allColorReturn.get(salaryColor) && allColorReturn.get(background) && allColorReturn.get(paperwork)){
			if(keyReturn==0){
				return "block";
			}
			if(keyReturn==1){
				return Utility.getLocaleValuePropByKey("optCmplt", locale);
			}
		}else{
			if(keyReturn==0){
				return "none";
			}
			if(keyReturn==1){
				return Utility.getLocaleValuePropByKey("optIncomlete", locale);
			}
		}
		return "";
	}
	
	public List<String[]> getAllStatusByPuttingStatus(Map<String,String> allMap){
		List<String[]> jsonObj=new ArrayList<String[]>();
		String[] allInfo=null;
		try{
			
			return jsonObj;
		}catch(Exception e){e.printStackTrace();return jsonObj;}
	}
	
	public Map<String,String> getDistrictWiseColumn(Integer districtId){
		println("Ohoooooooooooooooooooooo getDistrictWiseColumn service is start..............");
		Map<String,String> mapColumn=new LinkedHashMap<String,String>();
		headerData.clear();
		int count=0;
		mapColumn.put("noSort"+(++count),"");
		mapColumn.put("firstName",Utility.getLocaleValuePropByKey("headCand", locale));
		mapColumn.put("requisitionNumber",Utility.getLocaleValuePropByKey("msgPositionNumber", locale));
		mapColumn.put("schoolName",Utility.getLocaleValuePropByKey("lblSchoolName", locale));
		mapColumn.put("hiredByDate",Utility.getLocaleValuePropByKey("msgHired1", locale));
		
		headerData.add(Utility.getLocaleValuePropByKey("headCand", locale));
		headerData.add(Utility.getLocaleValuePropByKey("msgPositionNumber", locale));
		headerData.add(Utility.getLocaleValuePropByKey("lblSchoolName", locale));
		headerData.add(Utility.getLocaleValuePropByKey("msgHired1", locale));
		
		List<String> column= eligibilityMasterDAO.getColumnByDistrict(districtId);
		for(String string:column){
			if(!string.trim().equalsIgnoreCase("BISI")){
				mapColumn.put("noSort"+(++count),string.toString());
				headerData.add(string.toString());
			}
		}
		mapColumn.put("noSort"+(++count),Utility.getLocaleValuePropByKey("lblStatus", locale));
		headerData.add(Utility.getLocaleValuePropByKey("lblStatus", locale));
		return mapColumn;
	}
	
	public List<EligibilityStatusMaster> getStatusByEidJSON(Integer eligibilityId){
		List<EligibilityStatusMaster> listESM=new ArrayList<EligibilityStatusMaster>();
		try{
			listESM=eligibilityStatusMasterDAO.findByCriteria(Restrictions.eq("eligibilityMaster",eligibilityMasterDAO.findById(eligibilityId,false, false)),Restrictions.eq("status", "A"));
		}catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("listESM-========:::"+listESM.size());
		return listESM;
	}
	
	private Map getMapForThumbs(List<String[]> list){
		
		Map returnMap=new HashMap();
		Map<String,String> job_teacherMap=new LinkedHashMap<String, String>();
		Map<String,String> internal_teacherMap=new HashMap<String, String>();
		List<Integer> teacherList=new ArrayList<Integer>();
		List<Integer> jobIdList=new ArrayList<Integer>();
		try{
		for(Object[] allInfo:list){
			try{
			teacherList.add(Integer.parseInt(allInfo[7].toString()));
			jobIdList.add(Integer.parseInt(allInfo[8].toString()));
			}catch(NumberFormatException e){e.printStackTrace();}
		}
		if(teacherList.size()==0)teacherList.add(0);
		if(jobIdList.size()==0)jobIdList.add(0);
		
		Map<String,String> answerDetails=teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTADFDQByTeaJobForONB(teacherList,jobIdList);
		Map<String,String> jftisDistrictSpecificNoteFinalizeONB=jobForTeacherDAO.findJFTByTeaJobForONB(teacherList,jobIdList);
		String isDistrictSpecificNoteFinalizeONB="0";
		for(Map.Entry<String, String> entry:answerDetails.entrySet()){
			isDistrictSpecificNoteFinalizeONB=(jftisDistrictSpecificNoteFinalizeONB.get(entry.getKey())!=null?jftisDistrictSpecificNoteFinalizeONB.get(entry.getKey()):"0");
			job_teacherMap.put(entry.getKey(), entry.getValue()+"##"+isDistrictSpecificNoteFinalizeONB);
			println("getkey---"+entry.getKey()+"job_teacherMap=="+job_teacherMap.get(entry.getKey()));
		}
		for(Map.Entry<String, String> entry:jftisDistrictSpecificNoteFinalizeONB.entrySet()){
			String isAffiliated = entry.getValue().split("##")[2];
			System.out.println("isAffiliated     "+isAffiliated);
			if(isAffiliated.equalsIgnoreCase("1")){
				internal_teacherMap.put(entry.getKey(), isAffiliated);
			}else{
				internal_teacherMap.put(entry.getKey(), "0");
			}
			
		}
		
		}catch(Exception e){e.printStackTrace();}
		
		returnMap.put("job_teacherMap",job_teacherMap);
		returnMap.put("internal_teacherMap",internal_teacherMap);
		//return job_teacherMap;
		return returnMap;
	}
	
	private boolean getAccessThumbOrNot(){
		WebContext context;
	    context = WebContextFactory.get();
	    HttpServletRequest request = context.getHttpServletRequest();
	    HttpSession session = request.getSession(false);
	    boolean thumbFlag=false;
	    try{
	    UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");
		if(userMaster.getEntityType()==3)
		{
				SchoolMaster schoolMaster = userMaster.getSchoolId();
				DistrictSchools districtSchools = districtSchoolsDAO.findBySchoolMaster(schoolMaster);
				if(districtSchools.getDistrictMaster().getResetQualificationIssuesPrivilegeToSchool()==true)
					thumbFlag = true;
		    	if(userMaster.getEntityType()==2)
		    		thumbFlag = true;
		}
	    }catch(Exception e){
	    	e.printStackTrace();
	    	return false;
	    }
	    return thumbFlag;
	   
	}
	
	public String getQQOThumb(){
		WebContext context;
	    context = WebContextFactory.get();
	    HttpServletRequest request = context.getHttpServletRequest();
	    HttpSession session = request.getSession(false);
	    if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
	    StringBuilder sb=new StringBuilder("");
	    try{
	    	UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");
	    	SchoolMaster schoolMaster=null;
	    	
	    	List<DistrictSpecificQuestions> lstdistrictSpecificQuestions = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(userMaster.getDistrictId());
			boolean schoolCheckFlag=false;
			if(userMaster.getEntityType()==3)
			{
				try
				{
					schoolMaster = userMaster.getSchoolId();
					DistrictSchools districtSchools = districtSchoolsDAO.findBySchoolMaster(schoolMaster);
					if(districtSchools.getDistrictMaster().getResetQualificationIssuesPrivilegeToSchool()==true)
					{
						schoolCheckFlag = true;
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
	    	boolean CheckForIssue = false;
	    	if(userMaster.getEntityType()==2)
	    	{
	    		CheckForIssue = true;
	    	}
	    	
	    	if(schoolCheckFlag)
	    		CheckForIssue = true;
	    	
	    	
	    }catch(Exception e){e.printStackTrace();}
	    	
	    return sb.toString();
	}
	
	private Set<UserMaster> getGroupWiseAllUser(JobOrder jobOrder,SchoolMaster schoolMaster){
		Set<UserMaster> allUserMaster=new HashSet<UserMaster>();
		Set<UserMaster> emailListDAs=new HashSet<UserMaster>();
		Set<UserMaster> emailListSAs=new HashSet<UserMaster>();
		Set<UserMaster> emailListEST=new HashSet<UserMaster>();
		Map<String,String> groupNameToShortNameList=new LinkedHashMap<String,String>();
		if(jobOrder.getEmploymentServicesTechnician()!=null){
			List<String> esmails=new ArrayList<String>();
			esmails.add(jobOrder.getEmploymentServicesTechnician().getPrimaryEmailAddress());
			esmails.add(jobOrder.getEmploymentServicesTechnician().getBackupEmailAddress());
			List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactEmailAddress", esmails));
			for(DistrictKeyContact dkc:districtKeyContactList)
				emailListEST.add(dkc.getUserMaster());
		}
		if(schoolMaster!=null){
			List<UserMaster> umListSAs=userMasterDAO.getAllUserByDistrictAndSchool(jobOrder.getDistrictMaster(), schoolMaster);
			for(UserMaster um:umListSAs)
				if(!emailListSAs.contains(um.getEmailAddress())){
					emailListSAs.add(um);
				}
		}
		
		if(jobOrder.getSpecialEdFlag()!=null){
			List<DistrictSpecificApprovalFlow> listDSAF=districtSpecificApprovalFlowDAO.findByCriteria(Restrictions.eq("specialEdFlag", jobOrder.getSpecialEdFlag()),Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()),Restrictions.eq("status", "A"));
			if(listDSAF!=null && listDSAF.size()>0){
				List<String> groupShortName=new ArrayList<String>();
				for(String group:listDSAF.get(0).getApprovalFlow().split("#")){
					if(!group.trim().equals("")){
						groupShortName.add(group.trim());
					}
				}
				if(groupShortName.size()>0){
				List<DistrictWiseApprovalGroup> listDWAG=districtWiseApprovalGroupDAO.findByCriteria(Restrictions.in("groupShortName", groupShortName),Restrictions.eq("status", "A"));
					if(listDWAG!=null && listDWAG.size()>0){
						List<String> groupName=new ArrayList<String>();
						for(DistrictWiseApprovalGroup dwag:listDWAG){
							groupName.add(dwag.getGroupName());
							groupNameToShortNameList.put(dwag.getGroupName(), dwag.getGroupShortName());
						}
						
						
						for(Map.Entry<String,String> entry:groupNameToShortNameList.entrySet())
							System.out.println(entry.getKey()+"      ============    "+entry.getValue());
						
						
						if(groupName.size()>0){
						List<DistrictApprovalGroups> dAGList=districtApprovalGroupsDAO.findByCriteria(Restrictions.in("groupName",groupName),Restrictions.isNull("jobCategoryId"),Restrictions.isNull("jobId"),Restrictions.eq("districtId", jobOrder.getDistrictMaster()));
						//System.out.println("dAGList=============="+dAGList.size());
							if(dAGList.size()>0){
								List<Integer> keyContactId=new ArrayList<Integer>();
								Map<String,DistrictKeyContact> memberToKeyContactMap=new LinkedHashMap<String,DistrictKeyContact>();
								for(DistrictApprovalGroups districtAppGroup:dAGList){
									if(districtAppGroup.getGroupMembers()!=null && !districtAppGroup.getGroupMembers().trim().equals(""))
									for(String memberKey:districtAppGroup.getGroupMembers().split("#")){
										if(!memberKey.trim().equals(""))
											try{
											keyContactId.add(Integer.parseInt(memberKey));
											//System.out.println("keyId============="+memberKey);
											}catch(NumberFormatException e){}
									}
								}
								if(keyContactId.size()>0){
									List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", keyContactId));
									for(DistrictKeyContact dkc:districtKeyContactList)
										memberToKeyContactMap.put(dkc.getKeyContactId().toString(), dkc);
								}
								
								for(DistrictApprovalGroups districtAppGroup:dAGList){
									if(districtAppGroup.getGroupMembers()!=null && !districtAppGroup.getGroupMembers().trim().equals(""))
									for(String memberKey:districtAppGroup.getGroupMembers().split("#")){
										if(!memberKey.trim().equals(""))
											try{
												if(groupNameToShortNameList.get(districtAppGroup.getGroupName()).trim().equalsIgnoreCase("SA")){
												emailListSAs.add(memberToKeyContactMap.get(memberKey).getUserMaster());
												}else{
												emailListDAs.add(memberToKeyContactMap.get(memberKey).getUserMaster());
												}
											}catch(NumberFormatException e){}
									}
								}
								/*if(keyContactId.size()>0){
									List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", keyContactId));
									for(DistrictKeyContact dkc:districtKeyContactList)
										if(!allUserEmail.contains(dkc.getUserMaster().getEmailAddress()))
											allUserEmail.add(dkc.getUserMaster().getEmailAddress());
								}*/
							}
						}
					}
				}
			}
		}
		System.out.println("********************ES*********************"+emailListEST);
		System.out.println("********************SAs********************"+emailListSAs);
		//System.out.println("********************DAs********************"+emailListDAs);
		//allUserMaster.addAll(emailListDAs);
		allUserMaster.addAll(emailListSAs);
		allUserMaster.addAll(emailListEST);
		println("umList==================================================="+allUserMaster);
		return allUserMaster;
	}
	private class SendNotificationThreadSuccessSTP implements Runnable{
		private Set<UserMaster> allUserMasterToSendMail;
		private JobOrder jobOrder;
		private SchoolMaster schoolMaster;
		private TeacherDetail teacherDetail;
		private EmailerService emailerService;
		SendNotificationThreadSuccessSTP(Set<UserMaster> allUserMasterToSendMail, JobOrder jobOrder, SchoolMaster schoolMaster, TeacherDetail teacherDetail, EmailerService emailerService){
			this.allUserMasterToSendMail=allUserMasterToSendMail;
			this.jobOrder=jobOrder;
			this.schoolMaster=schoolMaster;
			this.teacherDetail=teacherDetail;
			this.emailerService=emailerService;
		}
		@Override
		public void run() {
			for(UserMaster um:allUserMasterToSendMail){
			String templateMail=AcceptOrDeclineMailHtml.sendNotificationTOAllSAsAndESTechMail(um,jobOrder,schoolMaster,teacherDetail);
			 println("templateMail====="+templateMail+"\n user mail=="+um.getEmailAddress());
			 emailerService.sendMailAsHTMLText(um.getEmailAddress(),"Sent to Peoplesoft",templateMail);
			}
		}
		
	}
	
	public boolean sendToPeopleSoftONB(Long[] selectedTSHId,String errorMessage){
		println("Ohoooooooooooooooooooooo sendToPeopleSoftONB service is start..............");
		boolean isSendToPeopleSoft=false;
		try{			
		    UserMaster userMaster=userMasterDAO.findById(6621, false, false);		    
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			List<TeacherDetail> listTD=new ArrayList<TeacherDetail>();
			List<JobOrder> listJO=new ArrayList<JobOrder> ();
			List<Integer> teacherIdList=new ArrayList<Integer>();
			Map<String,JobForTeacher> JFTMap=new LinkedHashMap<String,JobForTeacher> ();
			Map<String,TeacherStatusHistoryForJob> TSHFJMap=new LinkedHashMap<String,TeacherStatusHistoryForJob> ();
			Map<String,SapCandidateDetails> SCDMap=new LinkedHashMap<String,SapCandidateDetails> ();
			Map<Integer,TeacherPersonalInfo> TPIMap=new LinkedHashMap<Integer,TeacherPersonalInfo>();
			/*StateMaster stateObj=stateMasterDAO.findByCriteria(Restrictions.eq("stateName", "No Response")).get(0);
			CityMaster cityObj=cityMasterDAO.findByCriteria(Restrictions.eq("cityName", "No Response")).get(0);*/
			println("selectedTSHId===="+Arrays.asList(selectedTSHId));
			
			List<TeacherStatusHistoryForJob> listTSHFJ=teacherStatusHistoryForJobDAO.findByCriteria(Restrictions.in("teacherStatusHistoryForJobId", Arrays.asList(selectedTSHId)));
			for(TeacherStatusHistoryForJob tshfj:listTSHFJ){
				TSHFJMap.put(tshfj.getTeacherDetail().getTeacherId()+"##"+tshfj.getJobOrder().getJobId(), tshfj);
				listTD.add(tshfj.getTeacherDetail());
				listJO.add(tshfj.getJobOrder());
				teacherIdList.add(tshfj.getTeacherDetail().getTeacherId());
			}
			List<JobForTeacher> listJFT=jobForTeacherDAO.findByCriteria(Restrictions.in("jobId", listJO),Restrictions.in("teacherId", listTD));
			for(JobForTeacher jft:listJFT){
				JFTMap.put(jft.getTeacherId().getTeacherId()+"##"+jft.getJobId().getJobId(), jft);
			}
			
			for(TeacherPersonalInfo tpi:teacherPersonalInfoDAO.findByCriteria(Restrictions.in("teacherId", teacherIdList))){
				TPIMap.put(tpi.getTeacherId(), tpi);
			}
			
			for(SapCandidateDetails scd:sapCandidateDetailsDAO.findByCriteria(Restrictions.in("teacherDetail", listTD),Restrictions.in("jobOrder", listJO))){
				SCDMap.put(scd.getTeacherDetail().getTeacherId()+"##"+scd.getJobOrder().getJobId(), scd);
			}
			
			int count =0;
			for(Map.Entry<String, TeacherStatusHistoryForJob> entry:TSHFJMap.entrySet()){
				println("key==="+entry.getKey()+" value=="+entry.getValue());
				if(count==0){
					TeacherStatusHistoryForJob tshfjObj=entry.getValue();
					JobForTeacher jftObj=JFTMap.get(tshfjObj.getTeacherDetail().getTeacherId()+"##"+tshfjObj.getJobOrder().getJobId());
					TeacherPersonalInfo tpiObj=TPIMap.get(entry.getValue().getTeacherDetail().getTeacherId());
					if(SCDMap.get(tshfjObj.getTeacherDetail().getTeacherId()+"##"+tshfjObj.getJobOrder().getJobId())==null){
					SapCandidateDetails SCDObj=new SapCandidateDetails();
					SCDObj.setTeacherDetail(tshfjObj.getTeacherDetail());
					SCDObj.setJobOrder(tshfjObj.getJobOrder());
					SCDObj.setHireDate((sdf.format(tshfjObj.getHiredByDate()!=null?tshfjObj.getHiredByDate():tshfjObj.getCreatedDateTime())));
					SCDObj.setEmployeeNumber(tpiObj.getEmployeeNumber()!=null?tpiObj.getEmployeeNumber():"");
					SCDObj.setPosition(tshfjObj.getJobOrder().getRequisitionNumber()!=null?tshfjObj.getJobOrder().getRequisitionNumber():"");
					SCDObj.setJobTitle(tshfjObj.getJobOrder().getJobTitle());
					SCDObj.setCandidateFirstName(tpiObj.getFirstName());
					SCDObj.setCandidateMiddleName(tpiObj.getMiddleName()!=null?tpiObj.getMiddleName():"");
					SCDObj.setCandidateLastName(tpiObj.getLastName());
					SCDObj.setCandidateSSN(tpiObj.getSSN()!=null?tpiObj.getSSN():"");
					
					SCDObj.setCandidateGender(tpiObj.getGenderId()!=null?tpiObj.getGenderId().getGenderId().toString():"0");
					
					SCDObj.setCandidateDOB(tpiObj.getDob()!=null?sdf.format(tpiObj.getDob()):"");
					SCDObj.setCandidateCorrespondLanguage("E");
					SCDObj.setCandidateStreet(tpiObj.getAddressLine1()!=null?tpiObj.getAddressLine1():"");
					SCDObj.setCandidateSecondAddressLine(tpiObj.getAddressLine2()!=null?tpiObj.getAddressLine2():"");
					
					
					SCDObj.setCandidateCity(tpiObj.getCityId()!=null?tpiObj.getCityId().getCityName().toString():"");
					
					SCDObj.setCandidateZip(tpiObj.getZipCode()!=null?tpiObj.getZipCode():"");
					SCDObj.setCandidateCountry(tpiObj.getCountryId()!=null?tpiObj.getCountryId().getShortCode().toString():"US");
					
					SCDObj.setCandidateState(tpiObj.getStateId()!=null?tpiObj.getStateId().getStateShortName().toString():"");
					
					SCDObj.setCandidateTelephoneNumber(tpiObj.getMobileNumber()!=null?tpiObj.getMobileNumber():"");
					SCDObj.setCandidateEthnicOrigin(tpiObj.getEthnicOriginId()!=null?tpiObj.getEthnicOriginId().getEthnicOriginCode():"09");
					//SCDObj.setCandidateTelephoneNumber2(tpiObj.get);
					//SCDObj.setCandidateMilitaryStatus(tpiObj.g);
					//SCDObj.setCandidateDisability(candidateDisability);
					SCDObj.setCandidateVeteranStatus(tpiObj.getIsVateran()!=null?(tpiObj.getIsVateran()==1?"X":""):"");
					SCDObj.setCandidateEthnicity(tpiObj.getEthnicityId()!=null?tpiObj.getEthnicityId().getEthnicityCode():"E3");
					println("tpiObj.getRaceId()::::::==="+tpiObj.getRaceId());
					setRaceCode(SCDObj, tpiObj);
					SCDObj.setNoOfTries(1);
					SCDObj.setSchoolMaster(jftObj.getSchoolMaster()!=null?jftObj.getSchoolMaster():null);
					SCDObj.setJobForTeacher(jftObj);
					SCDObj.setUserMaster(userMaster);
					SCDObj.setCreatedDateTime(new Date());
					SCDObj.setLastUpdateDateTime(new Date());
					String[] response=new String[]{"{\"emailAddress\":\"\",\"emplid\":\"\",\"status\":\"Success\",\"message\":\"\"}"};
					println("response:::: "+Arrays.asList(response));	
					if(!response[0].trim().equals("")){
						try{
							net.sf.json.JSONObject jsonRequest = (net.sf.json.JSONObject) JSONSerializer.toJSON(response[0].trim().toString());
			        		String emailAddress = tshfjObj.getTeacherDetail().getEmailAddress();//jsonRequest.get("emailAddress")==null?"":UtilityAPI.parseString(jsonRequest.getString("emailAddress"));
				        	String status = jsonRequest.get("status")==null?"":UtilityAPI.parseString(jsonRequest.getString("status"));
				        	if(status.trim().equals("Success")){
				        		String insertDOB=tpiObj.getDob()!=null?(new SimpleDateFormat("MMddyyyy").format(tpiObj.getDob())):"";
				        		String sSN=tpiObj.getSSN()!=null?Utility.decodeBase64(tpiObj.getSSN()):"";
				        		SCDObj.setSapError(errorMessage);
				        		SCDObj.setSapStatus("S");
				        	}
							}catch(Exception e){
								SCDObj.setSapError(response[0].trim());
				        		SCDObj.setSapStatus("F");
							}
					}
					println("tpiObj.getAddressLine1()==="+tpiObj.getAddressLine1());
					println("hired==="+(sdf.format(tshfjObj.getHiredByDate()!=null?tshfjObj.getHiredByDate():tshfjObj.getCreatedDateTime())));
					sapCandidateDetailsDAO.makePersistent(SCDObj);
					isSendToPeopleSoft=true;
					}else{
						SapCandidateDetails SCDObj=SCDMap.get(tshfjObj.getTeacherDetail().getTeacherId()+"##"+tshfjObj.getJobOrder().getJobId());
						String[] response=new String[]{"{\"emailAddress\":\"\",\"emplid\":\"\",\"status\":\"Success\",\"message\":\"\"}"};
						println("response:::: "+Arrays.asList(response));
						if(!response[0].trim().equals("")){
							try{
							net.sf.json.JSONObject jsonRequest = (net.sf.json.JSONObject) JSONSerializer.toJSON(response[0].trim().toString());
			        		String emailAddress = tshfjObj.getTeacherDetail().getEmailAddress();//jsonRequest.get("emailAddress")==null?"":UtilityAPI.parseString(jsonRequest.getString("emailAddress"));
				        	String status = jsonRequest.get("status")==null?"":UtilityAPI.parseString(jsonRequest.getString("status"));
				        	if(status.trim().equals("Success")){
				        		SCDObj.setSapError(errorMessage);
				        		SCDObj.setSapStatus("S");
				        		String insertDOB=tpiObj.getDob()!=null?(new SimpleDateFormat("MMddyyyy").format(tpiObj.getDob())):"";
				        		String sSN=tpiObj.getSSN()!=null?Utility.decodeBase64(tpiObj.getSSN()):"";
				        	}
							}catch(Exception e){
								SCDObj.setSapError(response[0].trim());
				        		SCDObj.setSapStatus("F");
							}
						}
						SCDObj.setUserMaster(userMaster);
						SCDObj.setLastUpdateDateTime(new Date());
						SCDObj.setNoOfTries(SCDObj.getNoOfTries()+1);
						sapCandidateDetailsDAO.makePersistent(SCDObj);
						isSendToPeopleSoft=true;
					}
				}
				count++;
			}
		}catch(Exception e){
			e.printStackTrace();
			isSendToPeopleSoft=false;
			}
		return isSendToPeopleSoft;
	}
	
	/*@Autowired
	private JobOrderDAO jobOrderDAO;
	@Transactional
	public void busyConnection(){
		try{
			System.out.println("connnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
			SessionFactory sessionFactory=userMasterDAO.getSessionFactory();
			StatelessSession statelessSession=sessionFactory.openStatelessSession();
			JobOrder jobOrder=(JobOrder) sessionFactory.openSession().get(JobOrder.class, 1);
			int count =0;
			while(true){
				System.out.println("111111111--------------"+count);
				if(count%2==0)
				jobOrder.setDaysWorked("9");
				else
					jobOrder.setDaysWorked("8");
				statelessSession.update(jobOrder);
				count++;
				if(count==10000)
					break;
			}
			
			
		}catch(Throwable t){t.printStackTrace();}
	}*/
	 void saveCommunicationLog(SapCandidateDetails SCDObj,UserMaster userMaster, HttpServletRequest request){

		try{
			MessageToTeacher  messageToTeacher= new MessageToTeacher();
			messageToTeacher.setTeacherId(SCDObj.getTeacherDetail());
			messageToTeacher.setJobId(SCDObj.getJobOrder());
			messageToTeacher.setTeacherEmailAddress(SCDObj.getTeacherDetail().getEmailAddress());
			messageToTeacher.setSenderId(userMaster);
			messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
			messageToTeacher.setEntityType(userMaster.getEntityType());
			messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
			messageToTeacher.setMessageSend(SCDObj.getSapError());
			messageToTeacher.setMessageSubject("Send To PeopleSoft");
			messageToTeacher.setPeaplesoft(true);
			
			messageToTeacherDAO.makePersistent(messageToTeacher);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
