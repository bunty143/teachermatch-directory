package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.BatchJobOrder;
import tm.bean.EligibilityVerificationHistroy;
import tm.bean.StafferMaster;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentQuestions;
import tm.bean.assessment.AssessmentSections;
import tm.bean.assessment.QuestionOptions;
import tm.bean.assessment.QuestionsPool;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.DomainMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.ObjectiveMaster;
import tm.bean.master.OptionsForDistrictSpecificQuestions;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.StafferMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.OptionsForDistrictSpecificQuestionsDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.district.PrintOnConsole;
import tm.utility.Utility;

public class StafferAjax {

	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	RoleAccessPermissionDAO roleAccessPermissionDAO;

	@Autowired
	DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;

	@Autowired
	OptionsForDistrictSpecificQuestionsDAO optionsForDistrictSpecificQuestionsDAO;

	@Autowired
	DistrictMasterDAO districtMasterDAO;

	@Autowired
	UserMasterDAO userMasterDAO;

	@Autowired
	SchoolMasterDAO schoolMasterDAO;

	@Autowired
	StafferMasterDAO stafferMasterDAO;

	@Autowired
	RoleMasterDAO roleMasterDAO;

	public String getStafferDetail(Integer districtId, Integer schoolId,
			String noOfRow, String pageNo, String sortOrder,
			String sortOrderType) {
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session
						.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		StringBuffer dmRecords = new StringBuffer();
		try {
			UserMaster userMaster = null;
			int roleId = 0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "redirect:index.jsp";
			} else {
				userMaster = (UserMaster) session.getAttribute("userMaster");
				if (userMaster.getRoleId().getRoleId() != null) {
					roleId = userMaster.getRoleId().getRoleId();
				}
			}

			/* ============= For Sorting =========== */
			int noOfRowInPage = 0;
			int pgNo = 0;
			int start = 0;
			int end = 0;
			int totalRecord = 0;

			try {
				noOfRowInPage = Integer.parseInt(noOfRow);
				pgNo = Integer.parseInt(pageNo);
				start = ((pgNo - 1) * noOfRowInPage);
				end = ((pgNo - 1) * noOfRowInPage) + noOfRowInPage;
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

			// ------------------------------------
			/* ====== set default sorting fieldName ====== * */
			/** set default sorting fieldName **/
			String sortOrderFieldName = "schoolMaster";
			String sortOrderNoField = "schoolMaster";
			/** Start set dynamic sorting fieldName **/
			Order sortOrderStrVal = null;
			if (sortOrder != null) {
				if (!sortOrder.equals("") && !sortOrder.equals(null)
						&& !sortOrder.equals("schoolMaster")) {
					sortOrderFieldName = sortOrder;
					sortOrderNoField = sortOrder;
				}
				if (sortOrder.equals("location")) {
					sortOrderNoField = "location";
				}
				if (sortOrder.equals("userMaster")) {
					sortOrderNoField = "userMster";
				}
				if (sortOrder.equals("districtMaster")) {
					sortOrderNoField = "districtMaster";
				}
			}

			if (!sortOrder.equals("") && !sortOrder.equals(null)) {
				sortOrderFieldName = sortOrder;
			}
			String sortOrderTypeVal = "0";
			if (!sortOrderType.equals("") && !sortOrderType.equals(null)) {
				if (sortOrderType.equals("0")) {
					sortOrderStrVal = Order.asc(sortOrderFieldName);
				} else {
					sortOrderTypeVal = "1";
					sortOrderStrVal = Order.desc(sortOrderFieldName);
				}
			} else {
				sortOrderTypeVal = "0";
				sortOrderStrVal = Order.asc(sortOrderFieldName);
			}

			List<StafferMaster> sortedStafferList = new ArrayList<StafferMaster>();
			SortedMap<String, StafferMaster> sortedMap = new TreeMap<String, StafferMaster>();
			if (sortOrderNoField.equals("schoolMaster")) {
				sortOrderFieldName = "schoolMaster";
			}
			List<StafferMaster> stafferMasterRecord = null;
			stafferMasterRecord = stafferMasterDAO.findAll();

			if (districtId != null) {
				System.out.println("===districtId " + districtId);
				Criterion criterion = Restrictions.eq("districtMaster",
						districtMasterDAO.findById(districtId, false, false));
				stafferMasterRecord = stafferMasterDAO
						.findByCriteria(criterion);
				if (schoolId != null) {
					System.out.println("===schoolId " + schoolId);
					Criterion criterion2 = Restrictions.eq("schoolMaster",
							schoolMasterDAO.findById(new Long(schoolId), false,
									false));
					stafferMasterRecord = stafferMasterDAO.findByCriteria(
							criterion, criterion2);

				}

			}

			/*
			 * if (schoolId != null) { System.out.println("===schoolId " +
			 * schoolId); Criterion criterion = Restrictions.eq("schoolMaster",
			 * schoolMasterDAO.findById(new Long(schoolId), false, false));
			 * stafferMasterRecord = stafferMasterDAO
			 * .findByCriteria(criterion);
			 * 
			 * }
			 */

			totalRecord = stafferMasterRecord.size();
			int mapFlag = 2;
			for (StafferMaster stf : stafferMasterRecord) {
				String orderFieldName = null;
				if (stf.getSchoolMaster() != null) {
					if (sortOrderFieldName.equals("schoolMaster")) {
						orderFieldName = stf.getSchoolMaster().getSchoolName()
								+ "||" + stf.getStafferId();
						sortedMap.put(orderFieldName + "||", stf);
						if (sortOrderTypeVal.equals("0")) {
							mapFlag = 0;
						} else {
							mapFlag = 1;
						}
					}
				} else {
					if (sortOrderFieldName.equals("schoolMaster")) {
						orderFieldName = "0||" + stf.getStafferId();
						sortedMap.put(orderFieldName + "||", stf);
						if (sortOrderTypeVal.equals("0")) {
							mapFlag = 0;
						} else {
							mapFlag = 1;
						}
					}
				}
				// ==================

				if (sortOrderFieldName.equals("districtMaster")) {
					orderFieldName = stf.getDistrictMaster().getDistrictName()
							+ "||" + stf.getStafferId();
					sortedMap.put(orderFieldName + "||", stf);
					if (sortOrderTypeVal.equals("0")) {
						mapFlag = 0;
					} else {
						mapFlag = 1;
					}
				} else {
					if (sortOrderFieldName.equals("districtMaster")) {
						orderFieldName = "0||" + stf.getStafferId();
						sortedMap.put(orderFieldName + "||", stf);
						if (sortOrderTypeVal.equals("0")) {
							mapFlag = 0;
						} else {
							mapFlag = 1;
						}
					}
				}

				// =============

				if (stf.getUserMaster() != null) {
					if (stf.getUserMaster().getMiddleName() != null)
						orderFieldName = stf.getUserMaster().getFirstName()
								+ " " + stf.getUserMaster().getMiddleName()
								+ " " + stf.getUserMaster().getLastName();
					else
						orderFieldName = stf.getUserMaster().getFirstName()
								+ " " + stf.getUserMaster().getLastName();
					if (sortOrderFieldName.equals("userMaster")) {
						orderFieldName = orderFieldName + "||"
								+ stf.getStafferId();
						sortedMap.put(orderFieldName + "||", stf);
						if (sortOrderTypeVal.equals("0")) {
							mapFlag = 0;
						} else {
							mapFlag = 1;
						}
					}
				} else {
					if (sortOrderFieldName.equals("schoolMaster")) {
						orderFieldName = "0||" + stf.getStafferId();
						sortedMap.put(orderFieldName + "||", stf);
						if (sortOrderTypeVal.equals("0")) {
							mapFlag = 0;
						} else {
							mapFlag = 1;
						}
					}
				}
			}
			if (mapFlag == 1) {
				NavigableSet<String> navig = ((TreeMap) sortedMap)
						.descendingKeySet();
				for (Iterator iter = navig.iterator(); iter.hasNext();) {
					Object key = iter.next();
					sortedStafferList.add((StafferMaster) sortedMap.get(key));
				}
			} else if (mapFlag == 0) {
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedStafferList.add((StafferMaster) sortedMap.get(key));
				}
			} else {
				sortedStafferList = stafferMasterRecord;
			}
			if (totalRecord < end)
				end = totalRecord;

			if (mapFlag != 2)
				sortedStafferList = sortedStafferList.subList(start, end);
			else
				sortedStafferList = sortedStafferList;

			dmRecords.append("<table  id='stafferTable'  border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText = "";

			responseText = PaginationAndSorting.responseSortingLink(
					Utility.getLocaleValuePropByKey("lblDistrictName", locale), sortOrderFieldName, "districtMaster",
					sortOrderTypeVal, pgNo);
			dmRecords.append("<th width='25%' valign='top'>" + responseText
					+ "</th>");

			responseText = PaginationAndSorting.responseSortingLink(
					Utility.getLocaleValuePropByKey("lblSchoolName", locale), sortOrderFieldName, "schoolMaster",
					sortOrderTypeVal, pgNo);
			dmRecords.append("<th width='25%' valign='top'>" + responseText
					+ "</th>");

			responseText = PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblLocti", locale),
					sortOrderFieldName, "schoolMaster", sortOrderTypeVal, pgNo);
			dmRecords.append("<th width='35%' valign='top'>" + responseText
					+ "</th>");

			responseText = PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("headStaffer", locale),
					sortOrderFieldName, "userMaster", sortOrderTypeVal, pgNo);
			dmRecords.append("<th width='35%' valign='top'>" + responseText
					+ "</th>");

			dmRecords.append("<th width='45%' valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");

			if (stafferMasterRecord.size() == 0)
				dmRecords
						.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNoStafferFound", locale)+"</td></tr>");
			for (StafferMaster allRecord : sortedStafferList) {
				String nameStr = "";
				if (allRecord.getUserMaster().getMiddleName() != null)
					nameStr = allRecord.getUserMaster().getFirstName() + " "
							+ allRecord.getUserMaster().getMiddleName() + " "
							+ allRecord.getUserMaster().getLastName();
				else
					nameStr = allRecord.getUserMaster().getFirstName() + " "
							+ allRecord.getUserMaster().getLastName();
				dmRecords.append("<tr>");
				dmRecords.append("<td>"
						+ allRecord.getDistrictMaster().getDistrictName()
						+ "</td>");
				dmRecords
						.append("<td>"
								+ allRecord.getSchoolMaster().getSchoolName()
								+ "</td>");
				dmRecords.append("<td>"
						+ allRecord.getSchoolMaster().getAddress() + "</td>");
				dmRecords.append("<td>" + nameStr + "</td>");
				dmRecords.append("<td>");
				dmRecords
						.append("<a href='javascript:void(0);' onclick='editStafferDetails("
								+ allRecord.getStafferId()
								+ "); ' id=''>"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>");
				dmRecords
						.append("| <a href='javascript:void(0);' onclick='return showDeleteModel("
								+ allRecord.getStafferId()
								+ ")' id=''>"+Utility.getLocaleValuePropByKey("lnkDlt", locale)+"</a>");
				dmRecords.append("</td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,
					totalRecord, noOfRow, pageNo));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dmRecords.toString();
	}

	@Transactional(readOnly = false)
	public void deleteStafferById(Integer stafferId) {
		if (stafferId != null || stafferId == 0) {
			StafferMaster stafferMaster = stafferMasterDAO.findById(stafferId,
					false, false);
			stafferMasterDAO.makeTransient(stafferMaster);
		}
	}

	public String getDistrictQuestions(Integer districtId) {
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session
						.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		StringBuffer sb = new StringBuffer();
		try {
			UserMaster userMaster = null;
			int roleId = 0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "redirect:index.jsp";
			} else {
				userMaster = (UserMaster) session.getAttribute("userMaster");
				if (userMaster.getRoleId().getRoleId() != null) {
					roleId = userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess = null;
			try {
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,
						22, "assessmentquestions.do", 0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			List<DistrictSpecificQuestions> districtSpecificQuestionList = null;
			DistrictMaster districtMaster = districtMasterDAO.findById(
					districtId, false, false);
			Criterion criterion = Restrictions.eq("districtMaster",
					districtMaster);
			districtSpecificQuestionList = districtSpecificQuestionsDAO
					.findByCriteria(criterion);

			int i = 0;
			List<OptionsForDistrictSpecificQuestions> questionOptionsList = null;
			String questionInstruction = null;
			String shortName = "";

			sb.append("");

			for (DistrictSpecificQuestions districtSpecificQuestion : districtSpecificQuestionList) {
				shortName = districtSpecificQuestion.getQuestionTypeMaster()
						.getQuestionTypeShortName();

				sb.append("<tr>");
				sb.append("<td width='88%'>");

				sb
						.append("Question " + (++i) + ": "
								+ districtSpecificQuestion.getQuestion()
								+ "<br/><br/>");

				questionOptionsList = districtSpecificQuestion
						.getQuestionOptions();
				if (shortName.equalsIgnoreCase("tf")
						|| shortName.equalsIgnoreCase("slsel")) {
					sb.append("<table >");
					for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
						sb
								.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt"
										+ i
										+ "' /></td><td style='border:0px;background-color: transparent;padding: 2px 3px;'>"
										+ questionOptions.getQuestionOption()
										+ "</td></tr>");
					}
					sb.append("</table>");
				}
				if (shortName.equalsIgnoreCase("et")) {
					sb.append("<table >");
					for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
						sb
								.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt"
										+ i
										+ "' /></td><td style='border:0px;background-color: transparent;padding: 2px 3px;'>"
										+ questionOptions.getQuestionOption()
										+ "</td></tr>");
					}
					sb.append("</table>");
					sb
							.append("<br>Explain:<br><br>&nbsp;&nbsp;<textarea name='opt"
									+ i
									+ "' class='span10' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\"/></textarea><br/>");
					// sb.append(Utility.genrateHTMLspace(148)+"Max Length: 5000 Characters<br/>");
				} else if (shortName.equalsIgnoreCase("ml")) {
					sb
							.append("&nbsp;&nbsp;<textarea name='opt"
									+ i
									+ "' class='span10' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\"/></textarea><br/>");
					// sb.append(Utility.genrateHTMLspace(148)+"Max Length: 5000 Characters<br/>");
				}

				questionInstruction = districtSpecificQuestion
						.getQuestionInstructions() == null ? ""
						: districtSpecificQuestion.getQuestionInstructions();
				if (!questionInstruction.equals(""))
					sb.append("<br/>"+Utility.getLocaleValuePropByKey("lblInst", locale)+": </br>" + questionInstruction);
				else
					sb.append("<br/>");
				sb.append("</td>");
				sb.append("<td width='12%'>");
				boolean pipeFlag = false;

				sb.append("<a href='districtspecificquestions.do?districtId="
						+ districtSpecificQuestion.getDistrictMaster()
								.getDistrictId() + "&questionId="
						+ districtSpecificQuestion.getQuestionId()
						+ "' >Edit</a>");
				sb.append("&nbsp;|&nbsp;");
				if (districtSpecificQuestion.getStatus().equalsIgnoreCase("A"))
					sb
							.append("<span id='Q"
									+ districtSpecificQuestion.getQuestionId()
									+ "'><a href='javascript:void(0)' onclick=\"activateDeactivateDistrictQuestion("
									+ districtSpecificQuestion.getQuestionId()
									+ ",'I')\">Deactivate</a></span>");
				else
					sb
							.append("<span id='Q"
									+ districtSpecificQuestion.getQuestionId()
									+ "'><a href='javascript:void(0)' onclick=\"activateDeactivateDistrictQuestion("
									+ districtSpecificQuestion.getQuestionId()
									+ ",'A')\">Activate</a></span>");

				sb.append("</td>");

			}
			if (districtSpecificQuestionList.size() == 0)
				sb
						.append("<tr id='tr1' ><td id='tr1' colspan=2>"+Utility.getLocaleValuePropByKey("lblNoQuestionfound1", locale)+"</td></tr>");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/*
	 * @Author: Vishwanath Kumar
	 * 
	 * @Discription: It is used to save District Question.
	 */
	// @Transactional(readOnly=false)
	public DistrictSpecificQuestions saveDistrictQuestion(
			DistrictSpecificQuestions districtSpecificQuestion,
			DistrictMaster districtMaster,
			OptionsForDistrictSpecificQuestions[] optionsForDistrictSpecificQuestions) {
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session
						.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		UserMaster user = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			// return "false";
		} else
			user = (UserMaster) session.getAttribute("userMaster");

		if (districtSpecificQuestion.getQuestionId() == null) {
			districtSpecificQuestion.setCreatedDateTime(new Date());
		}

		try {
			districtSpecificQuestion.setDistrictMaster(districtMaster);
			districtSpecificQuestion.setUserMaster(user);
			districtSpecificQuestion.setStatus("A");

			DistrictSpecificQuestions districtSpecificQuestionNew = null;
			List<OptionsForDistrictSpecificQuestions> qopt = null;
			if (districtSpecificQuestion.getQuestionId() != null) {
				districtSpecificQuestionNew = districtSpecificQuestionsDAO
						.findById(districtSpecificQuestion.getQuestionId(),
								false, false);
				qopt = districtSpecificQuestionNew.getQuestionOptions();
				districtSpecificQuestion.setStatus(districtSpecificQuestionNew
						.getStatus());
				districtSpecificQuestion
						.setCreatedDateTime(districtSpecificQuestionNew
								.getCreatedDateTime());
				// districtSpecificQuestionsDAO.clear();
			}
			districtSpecificQuestionsDAO
					.makePersistent(districtSpecificQuestion);

			Map<Integer, OptionsForDistrictSpecificQuestions> map = new HashMap<Integer, OptionsForDistrictSpecificQuestions>();
			if (qopt != null)
				for (OptionsForDistrictSpecificQuestions questionOptions2 : qopt) {
					map.put(questionOptions2.getOptionId(), questionOptions2);
				}

			for (OptionsForDistrictSpecificQuestions optionsForDistrictSpecificQuestions2 : optionsForDistrictSpecificQuestions) {
				optionsForDistrictSpecificQuestions2
						.setDistrictSpecificQuestions(districtSpecificQuestion);
				optionsForDistrictSpecificQuestions2
						.setCreatedDateTime(new Date());
				optionsForDistrictSpecificQuestions2.setUserMaster(user);
				optionsForDistrictSpecificQuestions2.setStatus("A");
				optionsForDistrictSpecificQuestionsDAO
						.makePersistent(optionsForDistrictSpecificQuestions2);

				if (qopt != null)
					map.remove(optionsForDistrictSpecificQuestions2
							.getOptionId());
				optionsForDistrictSpecificQuestions2 = null;
			}
			if (qopt != null)
				for (Integer aKey : map.keySet()) {
					optionsForDistrictSpecificQuestionsDAO.makeTransient(map
							.get(aKey));
				}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return districtSpecificQuestion;
	}

	/*
	 * @Author: Vishwanath Kumar
	 * 
	 * @Discription: It is used to change a particular district question status.
	 */
	@Transactional(readOnly = false)
	public DistrictSpecificQuestions activateDeactivateDistrictQuestion(
			int questionId, String status) {
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session
						.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		try {
			DistrictSpecificQuestions districtSpecificQuestion = districtSpecificQuestionsDAO
					.findById(questionId, false, false);

			districtSpecificQuestion.setStatus(status);
			districtSpecificQuestionsDAO
					.makePersistent(districtSpecificQuestion);
			return districtSpecificQuestion;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * @Author: Vishwanath Kumar
	 * 
	 * @Discription: It is used to get a particular District Question.
	 */
	@Transactional(readOnly = false)
	public DistrictSpecificQuestions getDistrictQuestionById(
			DistrictSpecificQuestions districtSpecificQuestion) {
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session
						.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		System.out.println(" districtSpecificQuestion "
				+ districtSpecificQuestion.getQuestionId());
		try {
			districtSpecificQuestion = districtSpecificQuestionsDAO.findById(
					districtSpecificQuestion.getQuestionId(), false, false);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return districtSpecificQuestion;
	}

	public StafferMaster getStaffer(Integer stafferId) {
		StafferMaster stafferMaster = null;
		try {
			if (stafferId != null) {
				stafferMaster = stafferMasterDAO.findById(stafferId, false,
						false);
			}
		} catch (Exception e) {
			PrintOnConsole.debugPrintln("stafferajax", "getStaffer method :"
					+ e.getMessage());
		}
		return stafferMaster;
	}

	@Transactional(readOnly = false)
	public String saveStaffer(Integer districtId, Long schoolId,
			Integer userId, Integer stafferId) {
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session
						.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		} else {
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		System.out.println("stafferid " + stafferId);
		System.out.println("districtId " + districtId);
		System.out.println(" user id, schoolid " + userId + "," + schoolId);

		try {
			DistrictMaster districtMaster = null;
			SchoolMaster schoolMaster = null;
			StafferMaster stafferMaster = null;
			if (userId != null) {
				userMaster = userMasterDAO.findById(userId, false, false);
			}

			if (districtId != null) {
				districtMaster = districtMasterDAO.findById(districtId, false,
						false);
			}
			if (schoolId != null) {
				schoolMaster = schoolMasterDAO.findById(schoolId, false, false);
			}
			if (stafferId != null) {
				stafferMaster = stafferMasterDAO.findById(stafferId, false,
						false);
				System.out.println("UPDATE STAFFER");
				if (userId == null) {
					System.out.println("no changes maded");
					userMaster = stafferMaster.getUserMaster();
				}
				// userMaster = stafferMaster.getUserMaster();
				System.out.println(userMaster + "================"
						+ userMaster.getLastName());

			} else {
				System.out.println("new staffer");
				Criterion criterion = Restrictions.eq("schoolMaster",
						schoolMaster);
				List<StafferMaster> schoolMasterLst = stafferMasterDAO
						.findByCriteria(criterion);
				if (schoolMasterLst != null && schoolMasterLst.size() == 0) {
					stafferMaster = new StafferMaster();
					stafferMaster.setStatus("A");
					stafferMaster.setCreatedDateTime(new Date());
				}

			}
			stafferMaster.setDistrictMaster(districtMaster);
			stafferMaster.setSchoolMaster(schoolMaster);
			stafferMaster.setUserMaster(userMaster);

			String result = "update";
			if (stafferId == null) {
				result = "add";
			}
			System.out.println(stafferMaster);
			stafferMasterDAO.makePersistent(stafferMaster);
			return result;

		} catch (HibernateException e) {
			e.printStackTrace();
			PrintOnConsole.debugPrintln("staffer", e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			PrintOnConsole.debugPrintln("staffer", e.getMessage());
		}
		return null;
	}

	public StafferMaster getStafferBySchoolId(Integer schoolId) {
		StafferMaster stafferMaster = null;
		try {
			System.out.println(schoolId + " School id");
			if (schoolId != null) {
				SchoolMaster schoolMaster = schoolMasterDAO.findById(new Long(
						schoolId), false, false);
				Criterion criterion = Restrictions.eq("schoolMaster",
						schoolMaster);
				if (stafferMasterDAO.findByCriteria(criterion) != null
						&& stafferMasterDAO.findByCriteria(criterion).size() > 0) {
					stafferMaster = stafferMasterDAO.findByCriteria(criterion)
							.get(0);
					System.out.println(stafferMaster.getStafferId()
							+ " <get staffer id");
				}
			}
		} catch (Exception e) {
			PrintOnConsole.debugPrintln("stafferajax.",
					"getStafferBySchoolId method :" + e.getMessage());
		}
		return stafferMaster;
	}

	public List<UserMaster> getFieldOfStafferList(int districtId,
			String stafferName) {
		/* ======== For Session time Out Error ========= */
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session
						.getAttribute("userMaster") == null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<UserMaster> userMasterList = new ArrayList<UserMaster>();
		List<UserMaster> fieldOfStafferList1 = null;
		List<UserMaster> fieldOfStafferList2 = null;
		List<UserMaster> fieldOfStafferList3 = null;
		List<UserMaster> fieldOfStafferList4 = null;
		try {
			Criterion criterionFN = Restrictions.like("firstName", stafferName,
					MatchMode.START);
			System.out.println(" districtId :: " + districtId);
			if (districtId != 0) {
				DistrictMaster districtMaster = districtMasterDAO.findById(
						districtId, false, false);
				RoleMaster roleMaster = roleMasterDAO.findById(2, false, false);

				Criterion criterion2 = Restrictions.eq("districtId",
						districtMaster);
				Criterion criterionRole = Restrictions.eq("roleId", roleMaster);

				List<UserMaster> userMasterLst = new ArrayList<UserMaster>();
				userMasterLst = userMasterDAO.findByCriteria(criterion2);

				List<Integer> userMasterIds = new ArrayList<Integer>();
				for (UserMaster um : userMasterLst) {
					userMasterIds.add(um.getUserId());
				}

				Criterion criterionUserMasterIds = Restrictions.in("userId",
						userMasterIds);
				if (userMasterIds.size() > 0) {
					if (stafferName.trim() != null
							&& stafferName.trim().length() > 0) {
						System.out.println("staffer Name :: " + stafferName);
						fieldOfStafferList1 = userMasterDAO.findWithLimit(Order
								.asc("firstName"), 0, 25, criterionFN,
								criterion2, criterionUserMasterIds);
						criterionFN = Restrictions.ilike("firstName", "% "
								+ stafferName.trim() + "%");
						fieldOfStafferList2 = userMasterDAO.findWithLimit(Order
								.asc("firstName"), 0, 25, criterionFN,
								criterion2, criterionUserMasterIds);

						userMasterList.addAll(fieldOfStafferList1);
						userMasterList.addAll(fieldOfStafferList2);

						// check if string have spaces for last name
						if (stafferName.contains(" ")) {
							System.out.println("LAST NAME SEARCH");
							String[] tmpStafferNameIndex = stafferName
									.split(" ");

							String tmpStafferName = tmpStafferNameIndex[tmpStafferNameIndex.length - 1];
							criterionFN = Restrictions.like("lastName",
									tmpStafferName.trim(), MatchMode.START);
							fieldOfStafferList3 = userMasterDAO.findWithLimit(
									Order.asc("lastName"), 0, 25, criterionFN,
									criterion2, criterionUserMasterIds);
							criterionFN = Restrictions.ilike("lastName", "% "
									+ tmpStafferName.trim() + "%");
							fieldOfStafferList4 = userMasterDAO.findWithLimit(
									Order.asc("lastName"), 0, 25, criterionFN,
									criterion2, criterionUserMasterIds);
							userMasterList.addAll(fieldOfStafferList3);
							userMasterList.addAll(fieldOfStafferList4);

						}

						Set<UserMaster> setUsers = new LinkedHashSet<UserMaster>(
								userMasterList);
						userMasterList = new ArrayList<UserMaster>(
								new LinkedHashSet<UserMaster>(setUsers));

						System.out.println("result found:: "
								+ userMasterList.size());
					} else {
						fieldOfStafferList1 = userMasterDAO.findWithLimit(Order
								.asc("firstName"), 0, 25, criterion2,
								criterionUserMasterIds);
						userMasterList.addAll(fieldOfStafferList1);
					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return userMasterList;
	}
}
