package tm.services.district;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import tm.utility.Utility;

public class JobCategorywiseJsiUploadServletForIM extends HttpServlet  
{
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println(">>>>>>> JobCategorywiseJsiUploadServletForIM  >>>>>>>>>>");
		 response.setContentType("text/html");
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
	    List uploadedItems = null;
		FileItem fileItem = null;
		String ext="",filePath="";
		String dateTime=Utility.getDateTime();
		
		try{
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			String finalFileName="";
			
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if(fileItem.getFieldName().equals("imdistrictHiddenId"))
				filePath=Utility.getValueOfPropByKey("districtRootPath")+fileItem.getString()+"/jobcategory/";
			
				File f=new File(filePath);
				if(!f.exists())
					 f.mkdirs();
				
				
				if (fileItem.isFormField() == false) 
				{
					if (fileItem.getSize() > 0)	
					{
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", fullFileName="", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.lastIndexOf("."));
						ext=myFullFileName.substring(myFullFileName.lastIndexOf("."),myFullFileName.length());
						myFileName=myFileName.replaceAll("[^\\w\\s]", "");
						fullFileName=myFileName+""+dateTime+""+ext;
						
						//System.out.println(" fullFileName "+fullFileName+"\n\n fileName :"+fileName);
						uploadedFile = new File(filePath, fullFileName);
						fileItem.write(uploadedFile);
						finalFileName=fullFileName;
					}
				}
				fileItem=null;				
			}
			
			PrintWriter pw = response.getWriter();
			response.setContentType("text/html");  
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.saveRecord(\""+finalFileName+"\");");
			pw.print("</script>");
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
