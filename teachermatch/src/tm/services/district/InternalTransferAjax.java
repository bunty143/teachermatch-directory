package tm.services.district;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.InternalTransferCandidates;
import tm.bean.JobCategoryAndSubjectTemp;
import tm.bean.JobCategoryForInternalCandidate;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.ExclusivePeriodMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SubjectMaster;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobCategoryForInternalCandidateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.ExclusivePeriodMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.services.PaginationAndSorting;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;


public class InternalTransferAjax 
{
    String locale = Utility.getValueOfPropByKey("locale");
    
    
	@Autowired
	private ExclusivePeriodMasterDAO exclusivePeriodMasterDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@Autowired
	private JobCategoryForInternalCandidateDAO jobCategoryForInternalCandidateDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService){
		this.emailerService = emailerService;
	}
	public String displayCategoryByTeacher(String teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer tmRecords =	new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord = 0;
			//------------------------------------
			if (session == null || session.getAttribute("teacherDetail") == null) 
			{
				return "redirect:index.jsp";
			}
			
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"firstName";
	
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				 {
					 sortOrderFieldName		=	sortOrder;
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			TeacherDetail teacherDetail=teacherDetailDAO.findById(Integer.parseInt(teacherId),false,false);
			List<JobCategoryForInternalCandidate> jcficList= new ArrayList<JobCategoryForInternalCandidate>(); 
			jcficList=jobCategoryForInternalCandidateDAO.getCategoryByTeacher(teacherDetail);
			
			
			
			tmRecords.append("<table  id='categoryTable' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			String responseText="";
			//responseText=PaginationAndSorting.responseSortingLink("Category",sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
			responseText="Category";
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			responseText="Subject";
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(jcficList.size()==0)
				tmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
	
			Map<Integer,JobCategoryAndSubjectTemp> categoryMap = new HashMap<Integer, JobCategoryAndSubjectTemp>();		
			
			for (JobCategoryForInternalCandidate jcficObj : jcficList){
				JobCategoryAndSubjectTemp jCAST= new JobCategoryAndSubjectTemp();
				jCAST = categoryMap.get(jcficObj.getJobCategoryMaster().getJobCategoryId());
				int categoryId=jcficObj.getJobCategoryMaster().getJobCategoryId();
				if(jCAST==null){
					jCAST= new JobCategoryAndSubjectTemp();
					jCAST.setCategoryId(categoryId);
					jCAST.setInternaljobCategoryId(jcficObj.getInternaljobCategoryId());
					jCAST.setCategoryName(jcficObj.getJobCategoryMaster().getJobCategoryName());
					if(jcficObj.getSubjectMaster()!=null){
						jCAST.setSubjectIds(jcficObj.getSubjectMaster().getSubjectId()+"");
						jCAST.setSubjectNames(jcficObj.getSubjectMaster().getSubjectName());
					}else{
						jCAST.setSubjectIds(0+"");
						jCAST.setSubjectNames("No Subject");
					}
					
					categoryMap.put(categoryId,jCAST);
				}else{
					jCAST.setCategoryId(categoryId);
					jCAST.setInternaljobCategoryId(jcficObj.getInternaljobCategoryId());
					jCAST.setCategoryName(jcficObj.getJobCategoryMaster().getJobCategoryName());
				
					
					if(jcficObj.getSubjectMaster()!=null){
						jCAST.setSubjectIds(jCAST.getSubjectIds()+"-"+jcficObj.getSubjectMaster().getSubjectId());
						jCAST.setSubjectNames(jCAST.getSubjectNames()+", "+jcficObj.getSubjectMaster().getSubjectName());
					}else{
						jCAST.setSubjectIds(jCAST.getSubjectIds()+"-"+0);
						jCAST.setSubjectNames(jCAST.getSubjectNames()+", "+"No Subject");
					}
					
					
					categoryMap.put(categoryId,jCAST);
				}
			}
			
			for (Map.Entry<Integer,JobCategoryAndSubjectTemp> entry : categoryMap.entrySet()) {
			    int key = entry.getKey();
			    JobCategoryAndSubjectTemp  jCAST = entry.getValue();
			    tmRecords.append("<tr>" );
				tmRecords.append("<td>"+jCAST.getCategoryName()+"</td>");
				tmRecords.append("<td>"+jCAST.getSubjectNames()+"</td>");
				tmRecords.append("<td><a href='javascript:void(0);' onclick=\"return editCategory("+jCAST.getCategoryId()+",'"+jCAST.getSubjectIds()+"')\">"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>|<a href='javascript:void(0);' onclick='return deleteCategory("+jCAST.getCategoryId()+")'>"+Utility.getLocaleValuePropByKey("lnkDlt", locale)+"</a>");
				tmRecords.append("</td>");
				tmRecords.append("</tr>");
			}
			
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationString(request,totaRecord,noOfRow, pageNo));
		}catch (Exception e) {
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
	@Transactional(readOnly=false)
	public String saveInternalCandidate(String optEmails,String teacherId,String districtId,String schoolId,String firstName,String lastName,String email,String phone,String categoryDivValue){
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		String returnTxt="0";
		try{
			System.out.println("::::::::Inside saveInternalCandidate:::::===:"+optEmails);
			DistrictMaster districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId), false,false);
			SchoolMaster schoolMaster=null;
			
			if(schoolId!=null && !schoolId.equals("")){
				schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId), false,false);
			}
			String rPassword=Utility.randomString(8);
			
			 SessionFactory sessionFactory=jobCategoryForInternalCandidateDAO.getSessionFactory();
			 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
        	 Transaction txOpen =statelesSsession.beginTransaction();
        	 
			 TeacherDetail teacherDetail= new TeacherDetail();
			 List <TeacherDetail> teacherList=teacherDetailDAO.findByEmail(email);
			 int exclusivePeriod=0;
			 try{
				 if(districtMaster.getExclusivePeriod()==null || districtMaster.getExclusivePeriod()==0){
					 List<ExclusivePeriodMaster> expMasterList=exclusivePeriodMasterDAO.findByCriteria();
					 if(expMasterList.size()>0){
						 exclusivePeriod=expMasterList.get(0).getExclPeriod();
					 }
				 }else{
					 exclusivePeriod=districtMaster.getExclusivePeriod();
				 }
			 }catch(Exception e){}
			 
			 if(teacherId!=null && !teacherId.equals("")&& teacherList.size()>0){
				returnTxt="2";
				teacherDetail=teacherList.get(0);
				teacherDetail.setPhoneNumber(phone);
				teacherDetail.setFirstName(firstName);
				teacherDetail.setLastName(lastName);
				statelesSsession.update(teacherDetail);
				 
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getDistrictForTeacher(teacherDetail);
				if(itcList.size()>0){
					InternalTransferCandidates itc =itcList.get(0);
					itc.setSchoolMaster(schoolMaster);
					if(optEmails!=null && optEmails.equals("true")){
						itc.setOptEmails(true);
					}else{
						itc.setOptEmails(false);
					}
					//internalTransferCandidatesDAO.updatePersistent(itc);
					statelesSsession.update(itc);
				}
			 }else{
				 if(teacherList!=null && teacherList.size()>0){
					 teacherDetail=teacherList.get(0);
					 teacherDetail.setInternalTransferCandidate(true);
					 teacherDetail.setPhoneNumber(phone);
					 //teacherDetailDAO.updatePersistent(teacherDetail);
					 statelesSsession.update(teacherDetail);
					 returnTxt="0";
					emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), Utility.getLocaleValuePropByKey("msgIntTransTegConfig", locale) ,MailText.getRegistrationMailForInternalCandidate(request,teacherDetail,districtMaster,rPassword,1));
				 }else{
					 	returnTxt="1";
						String password=MD5Encryption.toMD5(rPassword);
						int authorizationkey=(int) Math.round(Math.random() * 2000000);
						teacherDetail= new TeacherDetail();
						teacherDetail.setFbUser(false);
						teacherDetail.setQuestCandidate(0);
						teacherDetail.setEmailAddress(email);
						teacherDetail.setFirstName(firstName);
						teacherDetail.setLastName(lastName);
						teacherDetail.setPassword(password);
						teacherDetail.setExclusivePeriod(exclusivePeriod);
						teacherDetail.setUserType("R");
						teacherDetail.setFbUser(false);
						teacherDetail.setAuthenticationCode(""+authorizationkey);
						teacherDetail.setVerificationCode(""+authorizationkey);
						teacherDetail.setVerificationStatus(0);				
						teacherDetail.setNoOfLogin(0);
						teacherDetail.setStatus("A");
						teacherDetail.setSendOpportunity(false);
						teacherDetail.setCreatedDateTime(new Date());
						teacherDetail.setForgetCounter(0);
						teacherDetail.setIsResearchTeacher(false);
						teacherDetail.setInternalTransferCandidate(true);
						teacherDetail.setIsPortfolioNeeded(true);
						teacherDetail.setPhoneNumber(phone);
						teacherDetail.setIpAddress(IPAddressUtility.getIpAddress(request));
						//teacherDetailDAO.makePersistent(teacherDetail);
						statelesSsession.insert(teacherDetail);
						emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),  Utility.getLocaleValuePropByKey("lblNoRecord", locale) ,MailText.getRegistrationMailForInternalCandidate(request,teacherDetail,districtMaster,rPassword,0));
				}
				if(teacherDetail!=null){
					InternalTransferCandidates itc= new InternalTransferCandidates();
					itc.setTeacherDetail(teacherDetail);
					itc.setDistrictMaster(districtMaster);
					if(schoolMaster!=null)
					itc.setSchoolMaster(schoolMaster);
					itc.setVerificationStatus(false);
					if(optEmails!=null && optEmails.equals("true")){
						itc.setOptEmails(true);
					}else{
						itc.setOptEmails(false);
					}
					itc.setStatus("A");
					itc.setCreatedDateTime(new Date());
					//internalTransferCandidatesDAO.makePersistent(itc);
					statelesSsession.insert(itc);
					
					
					 String allTrValue=categoryDivValue.substring(198,categoryDivValue.length());
					 try{
						 for (int i = 0; (i = allTrValue.indexOf("editCategory(")) != -1; ){
							allTrValue = allTrValue.substring(allTrValue.indexOf("editCategory(")+13);
							String categoryId= allTrValue.substring(0,allTrValue.indexOf(","));
							allTrValue = allTrValue.substring(allTrValue.indexOf("'")+1);
							String subjectIdTxt= allTrValue.substring(0,allTrValue.indexOf("'"));
							if(subjectIdTxt!=null){
								String[] subjectIds=subjectIdTxt.split("-");
								for(int s=0;s<subjectIds.length;s++){
									if(subjectIds[s]!=null){
										JobCategoryForInternalCandidate jFIC= new JobCategoryForInternalCandidate();
										SubjectMaster subjectMaster=new SubjectMaster();
										JobCategoryMaster jobCategoryMaster= new JobCategoryMaster();
										jobCategoryMaster.setJobCategoryId(Integer.parseInt(categoryId));
										if(subjectIds[s].equals("0")){
											jFIC.setSubjectMaster(null);
										}else{
											subjectMaster.setSubjectId(Integer.parseInt(subjectIds[s]));
											jFIC.setSubjectMaster(subjectMaster);
										}
										jFIC.setTeacherDetail(teacherDetail);
										jFIC.setJobCategoryMaster(jobCategoryMaster);
										
										jFIC.setInternalTransferCandidates(itc);
										statelesSsession.insert(jFIC);
									}
								}
							}
						}
					 }catch(Exception e){
						 e.printStackTrace();
					 }
				}
			 }
			 txOpen.commit();
		     statelesSsession.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return returnTxt;
	}
	@Transactional(readOnly=false)
	public String saveInternalJobCategory(String teacherId,String categoryId,String subjectIds,String categoryeditid){
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		String returnTxt="0";
		try{
			System.out.println("::::::::Inside saveInternalJobCategory:::::===:"+categoryId);
			TeacherDetail teacherDetail=teacherDetailDAO.findById(Integer.parseInt(teacherId),false,false);
			JobCategoryMaster jobCategoryMaster=jobCategoryMasterDAO.findById(Integer.parseInt(categoryId),false,false);
			JobCategoryMaster jobCategoryMasterForEdit=null;
			if(categoryeditid!=null && !categoryeditid.equals("")){
				jobCategoryMasterForEdit=jobCategoryMasterDAO.findById(Integer.parseInt(categoryeditid),false,false);
			}
			if(teacherDetail!=null){
				List<JobCategoryForInternalCandidate> jcficList= new ArrayList<JobCategoryForInternalCandidate>(); 
				jcficList=jobCategoryForInternalCandidateDAO.getCategoryByTeacherWithCategory(teacherDetail,jobCategoryMaster);
				
				InternalTransferCandidates itc= new InternalTransferCandidates();
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getDistrictForTeacher(teacherDetail);
				if(itcList.size()>0){
					itc=itcList.get(0);
				}
				Map<Integer,JobCategoryForInternalCandidate> jcicMap= new HashMap<Integer, JobCategoryForInternalCandidate>();
				for(JobCategoryForInternalCandidate jcic:jcficList){
					if(jcic.getSubjectMaster()!=null){
						jcicMap.put(jcic.getSubjectMaster().getSubjectId(),jcic);
					}else{
						jcicMap.put(0,jcic);
					}
				}
				
				 String subjectIdsArr[]=subjectIds.split("-");
				 SessionFactory sessionFactory=jobCategoryForInternalCandidateDAO.getSessionFactory();
				 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	        	 Transaction txOpen =statelesSsession.beginTransaction();
	        	 
	        	 
	        	 if(jcficList.size()==0 && jobCategoryMasterForEdit!=null){
	        		List<JobCategoryForInternalCandidate> jcficDeleteList= new ArrayList<JobCategoryForInternalCandidate>(); 
	        		jcficDeleteList=jobCategoryForInternalCandidateDAO.getCategoryByTeacherWithCategory(teacherDetail,jobCategoryMasterForEdit);
	        		for(JobCategoryForInternalCandidate deleteObj:jcficDeleteList){
	        			statelesSsession.delete(deleteObj);
	        		}
				 }
	        	 
				 try{
					 int countSubject=0;
					 String subjectIdsChk="";
					 for(int i=0;i<subjectIdsArr.length;i++){
						 JobCategoryForInternalCandidate jcfic=jcicMap.get(Integer.parseInt(subjectIdsArr[i]));
						 if(jcfic==null){
						 	JobCategoryForInternalCandidate jFIC= new JobCategoryForInternalCandidate();
							SubjectMaster subjectMaster=new SubjectMaster();
							subjectMaster.setSubjectId(Integer.parseInt(subjectIdsArr[i]));
							jFIC.setTeacherDetail(teacherDetail);
							jFIC.setJobCategoryMaster(jobCategoryMaster);
							if(subjectIdsArr[i].equals("0")){
								jFIC.setSubjectMaster(null);
							}else{
								jFIC.setSubjectMaster(subjectMaster);
							}
							jFIC.setInternalTransferCandidates(itc);
							statelesSsession.insert(jFIC);
						 }else{
							 if(jcfic.getSubjectMaster()!=null){
								 subjectIdsChk+="||"+jcfic.getSubjectMaster().getSubjectId()+"||";
							 }else{
								 subjectIdsChk+="||"+0+"||";
							 }
							 countSubject++;
						 }
					 }
				 	if(jcficList.size()>countSubject){
						for(JobCategoryForInternalCandidate jcic:jcficList){
							if(jcic.getSubjectMaster()!=null){
								if(subjectIdsChk.indexOf("||"+jcic.getSubjectMaster().getSubjectId()+"||")!= -1){
								}else{
									statelesSsession.delete(jcic);
								}
							}else{
								if(subjectIdsChk.indexOf("||"+0+"||")!= -1){
								}else{
									statelesSsession.delete(jcic);
								}
							}
						}
				 	}
				 }catch(Exception e){
					 e.printStackTrace();
				 }
				txOpen.commit();
		       	statelesSsession.close();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return returnTxt;
	}
	@Transactional(readOnly=false)
	public String deleteInternalJobCategory(String teacherId,String categoryId){
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		String returnTxt="0";
		try{
			System.out.println("::::::::Inside deleteInternalJobCategory:::::===:");
			JobCategoryMaster jobCategoryMaster=jobCategoryMasterDAO.findById(Integer.parseInt(categoryId),false,false);
				try{
					jobCategoryForInternalCandidateDAO.deleteCategoryByTeacher(jobCategoryMaster);
				}catch(Exception e){
					e.printStackTrace();
				}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return returnTxt;
	}
}


