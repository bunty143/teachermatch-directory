package tm.services;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SecondaryStatusMasterDAO;
import tm.utility.Utility;

public class ManageTagsAjax
{
	 String locale = Utility.getValueOfPropByKey("locale");
	// String locale = Utility.getValueOfPropByKey("locale");
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lblTName=Utility.getLocaleValuePropByKey("lblTName", locale);
	 String lblDistrict=Utility.getLocaleValuePropByKey("lblDistrict", locale);
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	 String lblTagsIcon=Utility.getLocaleValuePropByKey("lblTagsIcon", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String lblNoTagsfound=Utility.getLocaleValuePropByKey("lblNoTagsfound", locale);
	 String optAct=Utility.getLocaleValuePropByKey("optAct", locale);
	 String optInActiv=Utility.getLocaleValuePropByKey("optInActiv", locale);
	 String lblDeactivate=Utility.getLocaleValuePropByKey("lblDeactivate", locale);
	 String lblActivate=Utility.getLocaleValuePropByKey("lblActivate", locale);
	 String lblBranch=Utility.getLocaleValuePropByKey("optBranch", locale);
	 String lblHeadQuarter=Utility.getLocaleValuePropByKey("optHeadQuarter", locale);
	 
	 
	 
	@Autowired
	private SecondaryStatusMasterDAO secondaryStatusMasterDAO;
	public void setSecondaryStatusMasterDAO(
			SecondaryStatusMasterDAO secondaryStatusMasterDAO) {
		this.secondaryStatusMasterDAO = secondaryStatusMasterDAO;
	}
	
	@Autowired 
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	public String displayTags(Boolean result,Integer searchEntityType,Integer entityType,Integer searchId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer hqId, Integer branchId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		System.out.println("result :"+result);
		System.out.println("searchId :"+searchId);
		
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			List<SecondaryStatusMaster> secondaryStatusMasters  	=	null;
			List<SecondaryStatusMaster> secondaryStatusMastersForAll  	=	null;
			String sortOrderFieldName	=	"secStatusName";
			String sortOrderNoField		=	"secStatusName";
			
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("districtMaster")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("districtMaster"))
				 {
					 sortOrderNoField="districtMaster";
				 }
			}
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			
			List<SecondaryStatusMaster>sortedStsList=new ArrayList<SecondaryStatusMaster>();
			Criterion criterion=null; 
			DistrictMaster districtMaster=null;
			if(userMaster.getEntityType()==2 || userMaster.getEntityType()==3){
				districtMaster=userMaster.getDistrictId();
				criterion=Restrictions.eq("districtMaster", districtMaster);
				secondaryStatusMasters =secondaryStatusMasterDAO.findSecStatus(sortOrderStrVal, start, noOfRowInPage,criterion);
				secondaryStatusMastersForAll=secondaryStatusMasterDAO.findByCriteria(sortOrderStrVal,criterion);
				totalRecord=secondaryStatusMastersForAll.size();
			}
			else if(userSession.getEntityType()==6)
			{
				criterion=Restrictions.eq("branchId", branchId);
				secondaryStatusMasters =secondaryStatusMasterDAO.findSecStatus(sortOrderStrVal, start, noOfRowInPage,criterion);
				secondaryStatusMastersForAll=secondaryStatusMasterDAO.findByCriteria(sortOrderStrVal,criterion);
				totalRecord=secondaryStatusMastersForAll.size();
			}
			else if(userSession.getEntityType()==5 && hqId==1){
					criterion=Restrictions.and(Restrictions.isNull("branchId"), Restrictions.eq("headQuarterId", hqId));
					secondaryStatusMasters =secondaryStatusMasterDAO.findSecStatus(sortOrderStrVal, start, noOfRowInPage,criterion);
					secondaryStatusMastersForAll=secondaryStatusMasterDAO.findByCriteria(sortOrderStrVal,criterion);
					totalRecord=secondaryStatusMastersForAll.size();
				}
			else{
					if(result){
						if(searchId!=0){
							districtMaster=districtMasterDAO.findById(searchId, false, false);
							criterion=Restrictions.eq("districtMaster", districtMaster);
							secondaryStatusMasters =secondaryStatusMasterDAO.findSecStatus(sortOrderStrVal, start, noOfRowInPage,criterion);
						}else{
							criterion=Restrictions.isNotNull("districtMaster");
							secondaryStatusMasters =secondaryStatusMasterDAO.findSecStatus(sortOrderStrVal, start, noOfRowInPage,criterion);
						}
						secondaryStatusMastersForAll=secondaryStatusMasterDAO.findByCriteria(sortOrderStrVal,criterion);
						totalRecord=secondaryStatusMastersForAll.size();
					}else{
						if(searchEntityType==2){
							districtMaster=districtMasterDAO.findById(searchId, false, false);
							session.setAttribute("districtMasterId", districtMaster.getDistrictId());
							session.setAttribute("districtMasterName", districtMaster.getDistrictName());
							criterion=Restrictions.eq("districtMaster", districtMaster);
							secondaryStatusMasters =secondaryStatusMasterDAO.findSecStatus(sortOrderStrVal, start, noOfRowInPage,criterion);
							secondaryStatusMastersForAll=secondaryStatusMasterDAO.findByCriteria(sortOrderStrVal,criterion);
						}else{
							session.setAttribute("districtMasterId", 0);
							session.setAttribute("districtMasterName", "");
							secondaryStatusMasters =secondaryStatusMasterDAO.findSecStatus(sortOrderStrVal, start, noOfRowInPage);
							secondaryStatusMastersForAll=secondaryStatusMasterDAO.findByCriteria(sortOrderStrVal);
						}
						totalRecord=secondaryStatusMastersForAll.size();
					}
				}
			
			SortedMap<String,SecondaryStatusMaster>	sortedMap = new TreeMap<String,SecondaryStatusMaster>();
			if(sortOrderNoField.equals("districtMaster"))
			{
				sortOrderFieldName	=	"districtMaster";
			}
			
			int mapFlag=2;
			for (SecondaryStatusMaster secondaryStatusMaster : secondaryStatusMastersForAll){
				String orderFieldName=null;
				if(secondaryStatusMaster.getDistrictMaster()!=null){
					if(sortOrderFieldName.equals("districtMaster")){
						orderFieldName=secondaryStatusMaster.getDistrictMaster().getDistrictName()+"||"+secondaryStatusMaster.getSecStatusId();
						sortedMap.put(orderFieldName+"||",secondaryStatusMaster);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}else{
					if(sortOrderFieldName.equals("districtMaster")){
						orderFieldName="0||"+secondaryStatusMaster.getSecStatusId();
						sortedMap.put(orderFieldName+"||",secondaryStatusMaster);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedStsList.add((SecondaryStatusMaster) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedStsList.add((SecondaryStatusMaster) sortedMap.get(key));
				}
			}else{
				sortedStsList=secondaryStatusMasters;
			}
			List<SecondaryStatusMaster>sortedSList=new ArrayList<SecondaryStatusMaster>();
			
			System.out.println("start: "+start+"end :"+end+" totalRecord :"+totalRecord);
			//totalRecord =	sortedStsList.size();
			if(totalRecord<end)
				end=totalRecord;
			
			if(mapFlag!=2)
				sortedSList=sortedStsList.subList(start,end);
			else
				sortedSList=sortedStsList;
			
			dmRecords.append("<table  id='tagsTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblTName,sortOrderFieldName,"secStatusName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'>"+responseText+"</th>");
			if(userSession.getEntityType()==5)
				responseText=PaginationAndSorting.responseSortingLink(lblHeadQuarter,sortOrderFieldName,"headQuarterId",sortOrderTypeVal,pgNo);
			else if(userSession.getEntityType()==6)
				responseText=PaginationAndSorting.responseSortingLink(lblBranch,sortOrderFieldName,"branchId",sortOrderTypeVal,pgNo);
			else
				responseText=PaginationAndSorting.responseSortingLink(lblDistrict,sortOrderFieldName,"districtMaster",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblTagsIcon,sortOrderFieldName,"pathOfTagsIcon",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			
			dmRecords.append("<th width='15%' valign='top'>"+lblAct+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			
			if(sortedStsList.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+lblNoTagsfound+"</td></tr>" );
			String branchName="";
			String headQuarterName="";
			if(userMaster.getEntityType()==5 && userMaster.getHeadQuarterMaster()!=null)
			{
				headQuarterName = userMaster.getHeadQuarterMaster().getHeadQuarterName();
			}
			else if(userMaster.getEntityType()==6 && userMaster.getBranchMaster()!=null)
			{
				branchName=userMaster.getBranchMaster().getBranchName();
			}
			for (SecondaryStatusMaster scSts : sortedSList) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+scSts.getSecStatusName()+"</td>");
				if(scSts.getDistrictMaster()!=null){
					dmRecords.append("<td>"+scSts.getDistrictMaster().getDistrictName()+"</td>");
				}
				else if(scSts.getBranchId()!=null && userMaster.getEntityType()==6)
				{
					dmRecords.append("<td>"+branchName+"</td>");
				}
				else if(scSts.getHeadQuarterId()!=null && userMaster.getEntityType()==5)
				{
					dmRecords.append("<td>"+headQuarterName+"</td>");
				}
				else{
					dmRecords.append("<td></td>");
				}
				
				dmRecords.append("<td>");
				if(scSts.getStatus().equalsIgnoreCase("A"))
					dmRecords.append(optAct);
				else
					dmRecords.append(optInActiv); 
				dmRecords.append("</td>");
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=400,height=300,toolbar=1,resizable=0'); return false;";
				dmRecords.append("<td>");
				if(scSts.getDistrictMaster()!=null){
					dmRecords.append(scSts.getPathOfTagsIcon()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view tags icon!'  id='tags"+scSts.getSecStatusId()+"' onclick=\"downloadTagsIcon('"+scSts.getSecStatusId()+"','tags"+scSts.getSecStatusId()+"');"+windowFunc+"\">"+scSts.getPathOfTagsIcon()+"</a>");
				}else{
					dmRecords.append(scSts.getPathOfTagsIcon()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view tags icon!'  id='tags"+scSts.getSecStatusId()+"' onclick=\"downloadTagsIcon('"+scSts.getSecStatusId()+"','tags"+scSts.getSecStatusId()+"');"+windowFunc+"\">"+scSts.getPathOfTagsIcon()+"</a>");
				}
					
				dmRecords.append("<script>$('#tags"+scSts.getSecStatusId()+"').tooltip();</script>");
				dmRecords.append("</td>");
				
				dmRecords.append("<td>");
				
				if(userMaster.getEntityType()!=3){
					dmRecords.append("<a href='javascript:void(0);' title='Edit' onclick='return editSecStatus("+scSts.getSecStatusId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>&nbsp;|&nbsp;");
					if(scSts.getStatus().equalsIgnoreCase("A"))
						dmRecords.append("<a href='javascript:void(0);'title='Deactivate' onclick=\"return activateDeactivateSecStatus("+scSts.getSecStatusId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>");
					else
						dmRecords.append("<a href='javascript:void(0);' title='Activate' onclick=\"return activateDeactivateSecStatus("+scSts.getSecStatusId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>");
				}else{
					boolean pipe1=false;
					if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getIsEditTags()!=null && userMaster.getDistrictId().getIsEditTags()){
						pipe1=true;
						dmRecords.append("<a href='javascript:void(0);' title='Edit' onclick='return editSecStatus("+scSts.getSecStatusId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>&nbsp;");
					}
					 
					if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getIsRemoveTags()!=null && userMaster.getDistrictId().getIsRemoveTags()){
						if(pipe1)dmRecords.append("|&nbsp;");
						if(scSts.getStatus().equalsIgnoreCase("A"))
							dmRecords.append("<a href='javascript:void(0);'title='Deactivate' onclick=\"return activateDeactivateSecStatus("+scSts.getSecStatusId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>");
						else
							dmRecords.append("<a href='javascript:void(0);' title='Activate' onclick=\"return activateDeactivateSecStatus("+scSts.getSecStatusId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>");
					}
					
				}
					
					dmRecords.append("</td>");
				
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjaxForDSPQ(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public boolean saveTags(Integer secStmId,String secStmName,Integer districtId,String fileName,Integer hqId,Integer branchId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try
		{
			DistrictMaster districtMaster=null;
			boolean duplicate=false;
			SecondaryStatusMaster secondaryStatusMaster=new SecondaryStatusMaster();
			
			if(secStmId!=null){
				secondaryStatusMaster=secondaryStatusMasterDAO.findById(secStmId, false, false);
				duplicate=secondaryStatusMasterDAO.checkDuplicateJobCategory(secondaryStatusMaster,secStmName,districtMaster,hqId,branchId);
			}else{
				duplicate=secondaryStatusMasterDAO.checkDuplicateJobCategory(null,secStmName,districtMaster,hqId,branchId);
			}
			if(districtId!=null){
				districtMaster=districtMasterDAO.findById(districtId, false, false); 
				secondaryStatusMaster.setDistrictMaster(districtMaster);
			}
			
			if(hqId!=null)
				secondaryStatusMaster.setHeadQuarterId(hqId);
			if(branchId!=null)
				secondaryStatusMaster.setBranchId(branchId);
			
			if(!fileName.equals("")){
				secondaryStatusMaster.setPathOfTagsIcon(fileName);
			}else{
				secondaryStatusMaster.setPathOfTagsIcon(null);
			}
			
			if(!duplicate){
				secondaryStatusMaster.setSecStatusName(secStmName);
				secondaryStatusMaster.setStatus("A");
				secondaryStatusMasterDAO.makePersistent(secondaryStatusMaster);
				return false;
			}	
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return true;
	}
	public Boolean findDuplicateTags(Integer secStmId,String secStmName,Integer districtId,String fileName,Integer hqId,Integer branchId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try
		{
			boolean duplicate=false;
			DistrictMaster districtMaster=null;
			SecondaryStatusMaster secondaryStatusMaster=new SecondaryStatusMaster();
			
			if(districtId!=null){
				districtMaster=districtMasterDAO.findById(districtId, false, false); 
			}
			if(secStmId!=null){
				secondaryStatusMaster.setSecStatusId(secStmId);
				duplicate=secondaryStatusMasterDAO.checkDuplicateJobCategory(secondaryStatusMaster,secStmName,districtMaster,hqId,branchId);
			}else{
				duplicate=secondaryStatusMasterDAO.checkDuplicateJobCategory(null,secStmName,districtMaster,hqId,branchId);
			}
			session.setAttribute("districtId",districtId);
			return duplicate;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	public String downloadTagsIcon(Integer secStsId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String path="";
		try 
		{
			SecondaryStatusMaster secondaryStatusMaster=secondaryStatusMasterDAO.findById(secStsId, false, false);
			String fileName= secondaryStatusMaster.getPathOfTagsIcon();
			String source =null;
			String target =null;
			if(secondaryStatusMaster.getDistrictMaster()!=null){
				source = Utility.getValueOfPropByKey("districtRootPath")+secondaryStatusMaster.getDistrictMaster().getDistrictId()+"/tags/"+fileName;
		        target = context.getServletContext().getRealPath("/")+"/"+"/district/"+secondaryStatusMaster.getDistrictMaster().getDistrictId()+"/tags/";
			}else{
				source = Utility.getValueOfPropByKey("rootPath")+"/tags/"+fileName;
		        target = context.getServletContext().getRealPath("/")+"/"+"tags/";
			}
			
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	        	if(secondaryStatusMaster.getDistrictMaster()!=null){
	        		path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+secondaryStatusMaster.getDistrictMaster().getDistrictId()+"/tags/"+sourceFile.getName();	
	        	}else{
	        		path = Utility.getValueOfPropByKey("contextBasePath")+"/tags/"+sourceFile.getName();
	        	}
	        	
	        }	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	
	}
	public boolean activateDeactivateSecStatus(Integer secStsId,String status)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try{
			SecondaryStatusMaster secondaryStatusMaster=secondaryStatusMasterDAO.findById(secStsId, false, false);
			secondaryStatusMaster.setStatus(status);
			secondaryStatusMasterDAO.makePersistent(secondaryStatusMaster);
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Transactional(readOnly=false)
	public SecondaryStatusMaster getSecStsId(Integer secStsmId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		SecondaryStatusMaster secondaryStatusMaster=new SecondaryStatusMaster();
		try{
			if(secStsmId!=null){
				secondaryStatusMaster=secondaryStatusMasterDAO.findById(secStsmId, false, false);
				session.setAttribute("previousTagsIconFile", secondaryStatusMaster.getPathOfTagsIcon());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return secondaryStatusMaster;
	}
	
}

























































/*int mapFlag=2;
for (SecondaryStatusMaster secSts : secondaryStatusMastersForAll){
	String orderFieldName=null;
	if(secSts.getDistrictMaster()!=null){
		orderFieldName=secSts.getDistrictMaster().getDistrictName();
		if(sortOrderFieldName.equals("districtMaster")){
			orderFieldName=secSts.getDistrictMaster().getDistrictName()+"||"+secSts.getSecStatusId();
			sortedMap.put(orderFieldName+"||",secSts);
			if(sortOrderTypeVal.equals("0")){
				mapFlag=1;
			}else{
				mapFlag=0;
			}
		}
	}else{
		if(sortOrderFieldName.equals("districtMaster")){
			orderFieldName=""+"||"+secSts.getSecStatusId();
			sortedMap.put(orderFieldName+"||",secSts);
			if(sortOrderTypeVal.equals("0")){
				mapFlag=1;
			}else{
				mapFlag=0;
			}
		}
	}
}
if(mapFlag==1){
	NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
	for (Iterator iter=navig.iterator();iter.hasNext();) {  
		Object key = iter.next(); 
		sortedStsList.add((SecondaryStatusMaster) sortedMap.get(key));
	} 
}else if(mapFlag==0){
	Iterator iterator = sortedMap.keySet().iterator();
	while (iterator.hasNext()) {
		Object key = iterator.next();
		sortedStsList.add((SecondaryStatusMaster) sortedMap.get(key));
	}
}else{
	sortedStsList=secondaryStatusMasters;
}*/
