package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.python.antlr.PythonParser.return_stmt_return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springmodules.cache.config.gigaspaces.GigaSpacesNamespaceHandler;

import tm.bean.DistrictJobDescriptionLibrary;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.user.UserMaster;
import tm.dao.DistrictJobDescriptionLibraryDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.PanelAttendeesDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.DistrictNameCompratorASC;
import tm.utility.DistrictNameCompratorDESC;
import tm.utility.JobTitleCompratorASC;
import tm.utility.JobTitleCompratorDESC;
import tm.utility.LastNameCompratorASC;
import tm.utility.LastNameCompratorDESC;
import tm.utility.StafferNameCompratorASC;
import tm.utility.StafferNameCompratorDESC;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;




public class DistrictJobDescriptionLibraryAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private PanelAttendeesDAO panelAttendeesDAO;
	
	
	
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;

	
	
	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}

	@Autowired
	private PanelScheduleDAO panelScheduleDAO;
	
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) {
		this.districtKeyContactDAO = districtKeyContactDAO;
	}

	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	public void setRoleAccessPermissionDAO(
			RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}

	@Autowired
	private DistrictJobDescriptionLibraryDAO districtJobDescriptionLibraryDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO; 
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}

	public void setDistrictJobDescriptionLibraryDAO(
			DistrictJobDescriptionLibraryDAO districtJobDescriptionLibraryDAO) {
		this.districtJobDescriptionLibraryDAO = districtJobDescriptionLibraryDAO;
	}

	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	
	public String displayRecordsByEntityType(Integer districtOrSchoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,Integer jobcategoryId,String status)
		{
		System.out.println("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIii    "+jobcategoryId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			boolean sortingcheck=false;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");
					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}
					entityID=userMaster.getEntityType();
				}
					int noOfRowInPage = Integer.parseInt(noOfRow);
					int pgNo = Integer.parseInt(pageNo);
					int start =((pgNo-1)*noOfRowInPage);
					int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
					int totalRecord = 0;
					//------------------------------------
				 /** set default sorting fieldName **/
					String sortOrderFieldName="districtMaster";
					String sortOrderNoField="districtMaster";

					if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("districtMaster") || sortOrder.equalsIgnoreCase("jobCategoryMaster") || sortOrder.equalsIgnoreCase("jobDescriptionName")|| sortOrder.equalsIgnoreCase("jobDescription"))
						sortingcheck=true;
					
					/**Start set dynamic sorting fieldName **/
					
					Order  sortOrderStrVal=null;

					if(sortOrder!=null){
						if(!sortOrder.equals("") && !sortOrder.equals(null)){
							sortOrderFieldName=sortOrder;
							sortOrderNoField=sortOrder;
						}
					}

					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("0")){
							sortOrderStrVal=Order.asc(sortOrderFieldName);
						}else{
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
						}
					}else{
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<DistrictJobDescriptionLibrary>	jobDescriptionLibraries    = new ArrayList<DistrictJobDescriptionLibrary>();
					
					
					
					jobDescriptionLibraries=districtJobDescriptionLibraryDAO.findShortedData(sortOrderTypeVal,sortOrderFieldName,districtOrSchoolId,jobcategoryId, status);
							      
		
			   totalRecord=jobDescriptionLibraries.size();
				if(totalRecord<end)
					end=totalRecord;
				jobDescriptionLibraries	=	jobDescriptionLibraries.subList(start,end);
			  
				boolean flagedit=false;
				int roleId=0;
				int districtmster=0;
				Criterion districtIdcriteria=null;
				List<DistrictKeyContact> districtkeycontact=null;
				if(userMaster!=null){
					ContactTypeMaster contacttypemaster=null;
					Criterion criterion=Restrictions.eq("contactType", "Job Description Library");
					List<ContactTypeMaster> contactTypeMasterList =  contactTypeMasterDAO.findByCriteria(criterion);
					if(contactTypeMasterList!=null && contactTypeMasterList.size()>0)
					{
						contacttypemaster=contactTypeMasterList.get(0);
					}
					//ContactTypeMaster contacttypemaster=contactTypeMasterDAO.findById(16, false, false);

					if(userMaster.getDistrictId()!=null){
						districtmster=userMaster.getDistrictId().getDistrictId();
						districtIdcriteria=Restrictions.eq("districtId", districtmster);
					}
					
					
					Criterion keyContactTypeIdcriteria=Restrictions.eq("keyContactTypeId", contacttypemaster);
					Criterion userMastercriteria=Restrictions.eq("keyContactEmailAddress", userMaster.getEmailAddress());
					if(districtmster!=0)
						districtkeycontact=districtKeyContactDAO.findByCriteria(userMastercriteria,keyContactTypeIdcriteria,districtIdcriteria);
					else
					districtkeycontact=districtKeyContactDAO.findByCriteria(userMastercriteria,keyContactTypeIdcriteria);
				}
				if((userMaster.getEntityType()==1) || (districtkeycontact!=null && districtkeycontact.size()>0))
					flagedit=true;
				
				String responseText="";
				tmRecords.append("<table  id='tblGridOfferReady' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey(" lblDistrictName", locale),sortOrderNoField,"districtMaster",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobCategoryName", locale),sortOrderNoField,"jobCategoryMaster",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lbljobDesctriptionname", locale),sortOrderNoField,"jobDescriptionName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lbljobdescription", locale),sortOrderNoField,"jobDescription",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblStatus", locale),sortOrderNoField,"status",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				if(flagedit)		
					tmRecords.append("<th valign='top'>Action</th>");
				
			
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				tmRecords.append("<tbody>");
				
				if(jobDescriptionLibraries.size()==0){
				 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
				}
			
				if(jobDescriptionLibraries.size()>0){
				 for(DistrictJobDescriptionLibrary disjoblibrary:jobDescriptionLibraries) 
				 {
					 /*
					  * add by pawan kumar
					  * this is to get the word wrap by java
					  * */
					 String valMain=null;
					 String discriptionMessage=((disjoblibrary.getJobDescription()).trim()).replaceAll("<[^>]*>", "");
					 char[]p=discriptionMessage.toCharArray();
					 if(p.length>150)
					 {
						 String newP=String.valueOf(p);
						 String newStringVal=newP.substring(0, 150);
						 int y = 0;
						 if(newStringVal.endsWith(">"))
						 {
							 y=newStringVal.lastIndexOf(">");
						 }
						 else if(newStringVal.endsWith("</"))
						 {
							 y=newStringVal.indexOf("</");
						 }
						 else if(newStringVal.endsWith("<"))
						 {
							 y=newStringVal.indexOf("<");
						 }
						
						 if(y!=0)
						 {
							 valMain=newStringVal.substring(0, y);
						 }
						 else
						 {
							 if(!(newStringVal.endsWith("\\s")))
							 {
								 y=newStringVal.lastIndexOf(" ");
								 valMain=newStringVal.substring(0, y+1);
							 }
							 else
							 {
								 valMain=newStringVal;
							 }
						 }
					 }
					 else
					 {
						 valMain=disjoblibrary.getJobDescription();
					 }
					 /*
					  * end
					  * add by pawan kumar
					  * this is to get the word wrap by java
					  * */
					tmRecords.append("<tr>");
					tmRecords.append("<td >"+disjoblibrary.getDistrictMaster().getDistrictName()+"</td>");
					tmRecords.append("<td>"+disjoblibrary.getJobCategoryMaster().getJobCategoryName()+"</td>");
					tmRecords.append("<td>"+disjoblibrary.getJobDescriptionName()+"</td>");
     				tmRecords.append("<td >"+valMain+"</td>");//disjoblibrary.getJobDescription();title='"+disjoblibrary.getJobDescription()+"'
     				if(disjoblibrary.getStatus().equalsIgnoreCase("A"))
     					tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optAct", locale)+"</td>");
     				else
     					tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblInActiv", locale)+"</td>");
     				
     				if(flagedit){
     					tmRecords.append("<td><a title='Edit' href='#' onclick='addnewJobDescriprion(1,"+disjoblibrary.getDistrictjobdescriptionlibraryId()+");'><i class='fa fa-pencil-square-o fa-lg'></i></a>&nbsp;|&nbsp;");
     					if(disjoblibrary.getStatus().equalsIgnoreCase("A"))
     						tmRecords.append("<a title='Deactivate' href='javascript:void(0);' onclick=\"return activateDeactivateDescription("+disjoblibrary.getDistrictjobdescriptionlibraryId()+",'I')\"><i class='fa fa-times fa-lg'></i></a></td>");
     					else
     						tmRecords.append("<a title='Activate' href='javascript:void(0);' onclick=\"return activateDeactivateDescription("+disjoblibrary.getDistrictjobdescriptionlibraryId()+",'A')\"><i class='fa fa-check fa-lg'></i></a></td>");
     				}
     				
     				//tmRecords.append("<td><a href='#' onclick='addnewJobDescriprion(1,"+disjoblibrary.getDistrictjobdescriptionlibraryId()+");'>Edit</a></td>");
     				tmRecords.append("</tr>");
				 }
				}
			tmRecords.append("</tbody>");
			tmRecords.append("</table>");
			System.out.println("totalRecord   "+totalRecord+"\tnoOfRow  "+noOfRow+"\t pageNo   "+pageNo);
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));

			}catch(Exception e){
				e.printStackTrace();
			}
			
		 return tmRecords.toString();
	  }
		
	
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	public String editDistrictJobDescriptionLibrary(String  districtjobdescriptionLibraryId)
	{
	System.out.println("in the json method>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   "+districtjobdescriptionLibraryId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictJobDescriptionLibrary jobDescriptionLibraries=null;
		StringBuffer tmRecords=new StringBuffer();
		JSONObject Jsonobj = new JSONObject();
		//  StringWriter out = new StringWriter();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			UserMaster userMaster = null;
			
				userMaster=(UserMaster)session.getAttribute("userMaster");
					jobDescriptionLibraries=districtJobDescriptionLibraryDAO.findDataById(Integer.parseInt(districtjobdescriptionLibraryId));
					if(jobDescriptionLibraries!=null){
						Jsonobj.put("districtjobdescriptionlibraryId", jobDescriptionLibraries.getDistrictjobdescriptionlibraryId());
						Jsonobj.put("districtId", jobDescriptionLibraries.getDistrictMaster().getDistrictId());
						Jsonobj.put("districtname", jobDescriptionLibraries.getDistrictMaster().getDistrictName());
						Jsonobj.put("jobcategoryId", jobDescriptionLibraries.getJobCategoryMaster().getJobCategoryId());
						Jsonobj.put("jobcategoryname", jobDescriptionLibraries.getJobCategoryMaster().getJobCategoryName());
						Jsonobj.put("jobdescriptionname", jobDescriptionLibraries.getJobDescriptionName());
						Jsonobj.put("status", jobDescriptionLibraries.getStatus());
						Jsonobj.put("jobdescription", jobDescriptionLibraries.getJobDescription());
						
					}
							      
		}catch(Exception e){e.printStackTrace();}
	 return Jsonobj.toString();
  }
			
	
	public void addEditdescriptionLibrary(Integer districtjobdescriptionId,Integer districtId,Integer jobcategoryId,String districtjobdescriptionname,String  jobdescription,String status)
	{
		System.out.println("in the json method>>>>>>>>>>>>>>>>addEditdescriptionLibrary>>>>>>>>>>>>>>districtjobdescriptionId   "+districtjobdescriptionId+"\t  jobdescription  "+jobdescription);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictJobDescriptionLibrary jobDescriptionLibraries=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			UserMaster userMaster = null;
				userMaster=(UserMaster)session.getAttribute("userMaster");
				
				DistrictMaster districtmaster=districtMasterDAO.findById(districtId, false, false);
				JobCategoryMaster jobcategorymaster=jobCategoryMasterDAO.findById(jobcategoryId, false, false);
				DistrictJobDescriptionLibrary disjobdeslibraryobj=new DistrictJobDescriptionLibrary();
				if(districtjobdescriptionId==0){
					
					disjobdeslibraryobj.setDistrictMaster(districtmaster);
					disjobdeslibraryobj.setJobCategoryMaster(jobcategorymaster);
					disjobdeslibraryobj.setJobDescriptionName(districtjobdescriptionname);
					disjobdeslibraryobj.setJobDescription(jobdescription);
					disjobdeslibraryobj.setCreatedBy(userMaster);
					disjobdeslibraryobj.setCreatedDateTime(new Date());
					disjobdeslibraryobj.setLastUpdateBy(null);
					disjobdeslibraryobj.setStatus(status);
					disjobdeslibraryobj.setUpdateDateTime(null);
					districtJobDescriptionLibraryDAO.makePersistent(disjobdeslibraryobj);
					
				}else{
					disjobdeslibraryobj=districtJobDescriptionLibraryDAO.findById(districtjobdescriptionId, false, false);
					disjobdeslibraryobj.setDistrictMaster(districtmaster);
					disjobdeslibraryobj.setJobCategoryMaster(jobcategorymaster);
					disjobdeslibraryobj.setJobDescriptionName(districtjobdescriptionname);
					disjobdeslibraryobj.setJobDescription("");
					disjobdeslibraryobj.setJobDescription(jobdescription);
					disjobdeslibraryobj.setLastUpdateBy(userMaster);
					disjobdeslibraryobj.setStatus(status);
					disjobdeslibraryobj.setUpdateDateTime(new Date());
					districtJobDescriptionLibraryDAO.updatePersistent(disjobdeslibraryobj);
		
				}
					
							      
		}catch(Exception e){e.printStackTrace();}
	 
  }
	
	
	public String jobcategoryaList(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictJobDescriptionLibrary jobDescriptionLibraries=null;
		List<JobCategoryMaster> jobcategoryList=null;
		StringBuffer tmRecords=new StringBuffer();
		DistrictMaster districtmaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			UserMaster userMaster = null;
				userMaster=(UserMaster)session.getAttribute("userMaster");
				
				if(districtId!=0){
					try{
							districtmaster=districtMasterDAO.findById(districtId, false, false);
					}catch(Exception e){e.printStackTrace();}
				Criterion districtcriteria=Restrictions.eq("districtMaster", districtmaster);
				//jobcategoryList =jobCategoryMasterDAO.findByCriteria(districtcriteria);
				jobcategoryList =jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtmaster);
				}
				
				tmRecords.append("<select id='jobcategoryId' class='form-control'>");
				tmRecords.append("<option  value='0'>Select Job Category</option>");
				if(jobcategoryList!=null && jobcategoryList.size()>0){
					
					for(JobCategoryMaster jcm:jobcategoryList){
						 tmRecords.append("<option  value='"+jcm.getJobCategoryId()+"'  >"+jcm.getJobCategoryName()+"</option>");
					}
				}
				tmRecords.append("</select>");
				System.out.println("int the class  "+tmRecords.toString());
		}catch(Exception e){e.printStackTrace();}
		return tmRecords.toString();
  }
	
	public String jobcategoryaEditList(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictJobDescriptionLibrary jobDescriptionLibraries=null;
		List<JobCategoryMaster> jobcategoryList=null;
		StringBuffer tmRecords=new StringBuffer();
		DistrictMaster districtmaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			UserMaster userMaster = null;
				userMaster=(UserMaster)session.getAttribute("userMaster");
				
				if(districtId!=0){
					try{
							districtmaster=districtMasterDAO.findById(districtId, false, false);
					}catch(Exception e){e.printStackTrace();}
				//Criterion districtcriteria=Restrictions.eq("districtMaster", districtmaster);
				jobcategoryList =jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtmaster);
				}
				
				tmRecords.append("<select id='jobcategoryfordescriptionId' class='form-control'>");
				tmRecords.append("<option  value='0'>Select Job Category</option>");
				if(jobcategoryList!=null && jobcategoryList.size()>0){
					
					for(JobCategoryMaster jcm:jobcategoryList){
						 tmRecords.append("<option  value='"+jcm.getJobCategoryId()+"'  >"+jcm.getJobCategoryName()+"</option>");
					}
				}
				tmRecords.append("</select>");
				System.out.println("int the class  "+tmRecords.toString());
		}catch(Exception e){e.printStackTrace();}
		return tmRecords.toString();
  }
	
	
	
	
	public String activeANDDeactivate(Integer descriptionid,String action)
	{
		System.out.println("in the dwre class   "+descriptionid);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictJobDescriptionLibrary jobDescriptionLibraries=null;
		
		StringBuffer tmRecords=new StringBuffer();
		DistrictJobDescriptionLibrary districtjobdesLibrary=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
				districtjobdesLibrary=districtJobDescriptionLibraryDAO.findById(descriptionid, false, false);
				if(districtjobdesLibrary!=null)
					districtjobdesLibrary.setStatus(action);
					districtJobDescriptionLibraryDAO.updatePersistent(districtjobdesLibrary);
					
						
				
		}catch(Exception e){e.printStackTrace();}
		return tmRecords.toString();
  }
}


