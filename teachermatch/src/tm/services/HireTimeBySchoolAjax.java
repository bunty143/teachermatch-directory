package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.lang.ArrayUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.utility.NormScoreforJobOrderCompratorASC;
import tm.utility.NormScoreforJobOrderCompratorDesc;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;




public class HireTimeBySchoolAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;	
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(
			JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	
	 public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	
	 public String getHireTimeSchoolCategory(Integer districtId,Long schoolId)
		{	 
			StringBuffer	sb=new StringBuffer();
			List<JobCategoryMaster> categoryMaster;
		try{
			DistrictMaster dm=districtMasterDAO.findById(districtId, false, false);
			
			System.out.println("dm........................."+dm);
			Criterion districtCriteria=Restrictions.eq("districtMaster", dm);
			//System.out.println("districtCriteria--------------------"+districtCriteria);
			 categoryMaster=jobCategoryMasterDAO.findByCriteria(districtCriteria);
			//set category
			if(categoryMaster.size()>0){
				sb.append("<option style='width:10px;' value=''>All</option>");
				for(JobCategoryMaster jcm:categoryMaster){
					System.out.println("JCCCCCCCCCCCCCCCC@@@@@@@@@@@@"+jcm.getJobCategoryName());
				sb.append("<option style='width:10px;' value='"+jcm.getJobCategoryId()+"'>"+jcm.getJobCategoryName()+"</option>");
				}
			}else{
				sb.append("<option style='width:10px;' value=''>"+Utility.getLocaleValuePropByKey("lblNotAvailable", locale)+"</option>");
			}
			}catch(Exception e){
				e.printStackTrace();
			}
			return sb.toString();
		}
	public String displayRecordsByHireTimeSchool(int districtOrSchoolId,Long schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String jobOrderId,int categoryId,boolean exportcheck)
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession();
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=0;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position

					int noOfRowInPage = Integer.parseInt(noOfRow);
					int pgNo = Integer.parseInt(pageNo);
					int start =((pgNo-1)*noOfRowInPage);
					int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
					int totalRecord = 0;
					int totalRecords=0;
					//------------------------------------
					String  sortOrderStrVal		=	null;
					String sortOrderFieldName	=	"districtName";
					
					if(!sortOrder.equals("") && !sortOrder.equals(null))
					{
						sortOrderFieldName		=	sortOrder;
					}
					
					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null))
					{
						if(sortOrderType.equals("0"))
						{
							sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
						}
						else
						{
							sortOrderTypeVal	=	"1";
							sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
						}
					}
					else
					{
						sortOrderTypeVal		=	"0";
						sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
					}
					
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<String[]> hiretimebyschool =new ArrayList<String[]>();
					boolean checkflag = false;
					try {
						hiretimebyschool=jobForTeacherDAO.hireTimeBySchool(districtOrSchoolId,schoolId,jobOrderId,categoryId, noOfRow, pageNo, sortOrder, sortOrderType, exportcheck,sortOrderFieldName,sortOrderStrVal,start,false,end);
						totalRecord=jobForTeacherDAO.hireTimeBySchool(districtOrSchoolId,schoolId,jobOrderId,categoryId, noOfRow, pageNo, sortOrder, sortOrderType, exportcheck,sortOrderFieldName,sortOrderStrVal,start,true,end).size();
						System.out.println("total records are :::"+totalRecord);
					} catch (Exception e) {
						
					}
               try{
				
				
				String responseText="";
				
				tmRecords.append("<table  id='tblGridJobList' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDistrictName", locale),sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("optSchool", locale),sortOrderFieldName,"School",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobId", locale),sortOrderFieldName,"jobid",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderFieldName,"jobtitle",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobCat", locale),sortOrderFieldName,"jobcategoryname",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblNumberOfHires", locale),sortOrderFieldName,"N_Hired",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblAverageHireDays", locale),sortOrderFieldName,"Average_Hire_Days",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				tmRecords.append("<tbody>");
				if(hiretimebyschool.size()==0){
				 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
				}
			System.out.println("Size of List---------------------"+hiretimebyschool.size());
				if(hiretimebyschool.size()>0){
					for (Iterator it = hiretimebyschool.iterator(); it.hasNext();)
	                   {
	                	   String[] row = (String[]) it.next(); 
	                	   
	                	   String  myVal1="";
	                	   String  myVal2="";
	                	  
	                	        tmRecords.append("<tr>");	
	                	        
	                            myVal1 = (row[0]==null)? "N/A" : row[0].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
	                            
			                    
			                    myVal1 = (row[1]==null)? "N/A" : row[1].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? "N/A" : row[2].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    
			                    myVal1 = (row[3]==null)? "N/A" : row[3].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[4]==null)? "N/A" : row[4].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[5]==null)? "N/A" : row[5].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[6]==null)? "N/A" : row[6].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    tmRecords.append("</tr>");
	               }               
				}
			tmRecords.append("</tbody>");
			tmRecords.append("</table>");			
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
               }catch(Exception e){
   				e.printStackTrace();
   			}
			}catch(Exception e){
				e.printStackTrace();
			}
			
		 return tmRecords.toString();
	  }
		
	public String hireTimeSchoolExportEXL(int districtOrSchoolId,Long schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String jobOrderId,int categoryId,boolean exportcheck)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		int sortingcheck=0;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				//------------------------------------
				
				 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"districtName";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<String[]> hiretimebyschool =new ArrayList<String[]>();
				
				Map<Integer,Double> mapAvgEPI = new HashMap<Integer, Double>();
				
				hiretimebyschool= jobForTeacherDAO.hireTimeBySchool(districtOrSchoolId,schoolId,jobOrderId,categoryId, sortOrderTypeVal, sortOrderTypeVal, sortOrderTypeVal, sortOrderTypeVal, exportcheck,sortOrderFieldName, sortOrderStrVal, start,false,end);
					String time = String.valueOf(System.currentTimeMillis()).substring(6);
					String basePath = request.getSession().getServletContext().getRealPath ("/")+"/Hiretimeschoollist";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="hiretimebyschooljob"+time+".xls";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

					WorkbookSettings wbSettings = new WorkbookSettings();

					wbSettings.setLocale(new Locale("en", "EN"));

					WritableCellFormat timesBoldUnderline;
					WritableCellFormat header;
					WritableCellFormat headerBold;
					WritableCellFormat times;
					WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
					workbook.createSheet("hireTimeBySchoolJob", 0);
					WritableSheet excelSheet = workbook.getSheet(0);
					WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
					// Define the cell format
					times = new WritableCellFormat(times10pt);
					// Lets automatically wrap the cells
					times.setWrap(true);

					// Create create a bold font with unterlines
					WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
					WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

					timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
					timesBoldUnderline.setAlignment(Alignment.CENTRE);
					// Lets automatically wrap the cells
					timesBoldUnderline.setWrap(true);

					header = new WritableCellFormat(times10ptBoldUnderline);
					headerBold = new WritableCellFormat(times10ptBoldUnderline);
					CellView cv = new CellView();
					cv.setFormat(times);
					cv.setFormat(timesBoldUnderline);
					cv.setAutosize(true);
					header.setBackground(Colour.GRAY_25);

					// Write a few headers
					excelSheet.mergeCells(0, 0, 6, 1);
					Label label;
					label = new Label(0, 0, ""+Utility.getLocaleValuePropByKey("lblHireTimebySchool", locale)+"", timesBoldUnderline);
					excelSheet.addCell(label);
					excelSheet.mergeCells(0, 3, 6, 3);
					label = new Label(0, 3, "");
					excelSheet.addCell(label);
					excelSheet.getSettings().setDefaultColumnWidth(18);
					
					int k=4;
					int col=1;
					label = new Label(0, k, Utility.getLocaleValuePropByKey("lblDistrictName", locale),header); 
					excelSheet.addCell(label);
					label = new Label(1, k, Utility.getLocaleValuePropByKey("optSchool", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJobId", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJoTil", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJobCat", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblNumberOfHires", locale),header); 
					excelSheet.addCell(label); 
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblAverageHireDays", locale),header); 
					excelSheet.addCell(label);
					k=k+1;
					if(hiretimebyschool.size()==0)
					{	
						excelSheet.mergeCells(0, k, 6, k);
						label = new Label(0, k, ""+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+""); 
						excelSheet.addCell(label);
					}
			
			if(hiretimebyschool.size()>0)
			 for(Iterator it = hiretimebyschool.iterator(); it.hasNext();)
			 {
				 String[] row = (String[]) it.next(); 
					 col=1;
					    label = new Label(0, k, row[0].toString()); 
						excelSheet.addCell(label);
						if(row[1].toString()!=null){
						label = new Label(1, k, row[1].toString()); 
						excelSheet.addCell(label);
						}else
						{
							label = new Label(1, k, "N/A"); 
							excelSheet.addCell(label);
						}
						label = new Label(++col, k,row[2].toString()+""); 
						excelSheet.addCell(label);
						
						label = new Label(++col, k, row[3].toString()); 
						excelSheet.addCell(label);
						if(row[4].toString()!=null){
							label = new Label(++col, k, row[4].toString()); 
							excelSheet.addCell(label);
							}else
							{
								label = new Label(++col, k, "N/A"); 
								excelSheet.addCell(label);
							}
						if(row[5].toString()!=null){
							label = new Label(++col, k, row[5].toString()); 
							excelSheet.addCell(label);
							}else
							{
								label = new Label(++col, k, "N/A"); 
								excelSheet.addCell(label);
							}
						if(row[6]==null){
						label = new Label(++col, k, "N/A"); 
						excelSheet.addCell(label);
						}else
						{
							label = new Label(++col, k,row[6].toString() ); 
							excelSheet.addCell(label);
						}
					   k++;
			}
			workbook.write();
			workbook.close();
		}catch(Exception e){}
		
	 return fileName;
  }
	
	public String hireTimeSchoolExportPDF(int districtOrSchoolId,Long schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String jobOrderId,int categoryId,boolean exportcheck)
	{
		//System.out.println("dfdfffffffffffffffffffffffffffffffffffff");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport(  noOfRow, pageNo, sortOrder, sortOrderType,basePath+"/"+fileName,context.getServletContext().getRealPath("/"),districtOrSchoolId,schoolId,jobOrderId,categoryId);
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	public boolean generateCandidatePDReport(String noOfRow,String pageNo,String sortOrder,String sortOrderType,String path,String realPath,int districtOrSchoolId,Long schoolId,String jobOrderId,int categoryId)
	{
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			int sortingcheck=1;
			try{
				
				
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				
				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"districtName";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<String[]> hiretimebyschool =new ArrayList<String[]>();
			
						 hiretimebyschool =jobForTeacherDAO.hireTimeBySchool(districtOrSchoolId,schoolId,jobOrderId,categoryId, sortOrderTypeVal, sortOrderTypeVal, sortOrderTypeVal, sortOrderTypeVal, false,sortOrderFieldName, sortOrderStrVal, start,false,end);
					  	totalRecord = hiretimebyschool.size();
					    	System.out.println("totallllllllllll========="+totalRecord);
		
				 String fontPath = realPath;
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject("Report for Hire Time by School");
						document.addCreationDate();
						document.addTitle(" Report for Hire Time by School");

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));
						
						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						
						para[0] = new Paragraph("Hire Time by School Job",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
												
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph("Created By: "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+"Created on: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						document.add(new Phrase("\n"));


						float[] tblwidth={.06f,.07f,.04f,.08f,.06f,.04f,.04f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[7];
						cell = new PdfPCell[7];
				// header
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblDistrictName", locale),font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("optSchool", locale),font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJobId", locale),font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[2]);
						
						para[3] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJoTil", locale),font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[3]);

						para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJobCat", locale),font10_10);
						cell[4]= new PdfPCell(para[4]);
						cell[4].setBackgroundColor(bluecolor);
						cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[4]);
						
						para[5] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblNumberOfHires", locale),font10_10);
						cell[5]= new PdfPCell(para[5]);
						cell[5].setBackgroundColor(bluecolor);
						cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[5]);
						
						para[6] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblAverageHireDays", locale),font10_10);
						cell[6]= new PdfPCell(para[6]);
						cell[6].setBackgroundColor(bluecolor);
						cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[6]);
						
						
						document.add(mainTable);
						
					
						
						if(hiretimebyschool.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph("No Record Found",font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
				     String sta="";
						if(hiretimebyschool.size()>0){
							for (Iterator it = hiretimebyschool.iterator(); it.hasNext();) 					            
							 {
								String[] row = (String[]) it.next();
							  int index=0;
							 									
							  mainTable = new PdfPTable(tblwidth);
							  mainTable.setWidthPercentage(100);
							  para = new Paragraph[7];
							  cell = new PdfPCell[7];
							
                              String myVal1="";
                              String myVal2="";
                              
								 myVal1 = (row[0]==null)? "N/A" : row[0].toString();
								 para[index] = new Paragraph( row[0].toString(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
															 
								 myVal1 = (row[1]==null)? "N/A" : row[1].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? "N/A" : row[2].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 								 
								 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[4]==null)? "N/A" : row[4].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[6]==null)? "N/A" : row[6].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								
								 index++;
								
								

						document.add(mainTable);
				   }
				}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	
	public String hireTimeSchoolPrintPreview(int districtOrSchoolId,Long schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String jobOrderId,int categoryId,boolean exportcheck)
	{
		System.out.println("inside Print Preview");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
		
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
				 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"districtName";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<String[]> hiretimeschool =new ArrayList<String[]>();
				int disID=0;
				hiretimeschool =jobForTeacherDAO.hireTimeBySchool(districtOrSchoolId,schoolId,jobOrderId,categoryId, sortOrderTypeVal, sortOrderTypeVal, sortOrderTypeVal, sortOrderTypeVal, exportcheck,sortOrderFieldName, sortOrderStrVal, start,false,end);
				  	totalRecord = hiretimeschool.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
			    	 					
					List<String[]> finallstNobleCandidate =new ArrayList<String[]>();
				    finallstNobleCandidate=hiretimeschool;
				
			

				    	tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>Hire Time by School Job</div><br/>");
			
				    	tmRecords.append("<div style='width:100%'>");
						tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+"Created by: "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
						tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>Date of Print: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
						tmRecords.append("<br/><br/>");
					    tmRecords.append("</div>");
			
			
						tmRecords.append("<table  id='tblGridHireTime' width='100%' border='1'>");
						tmRecords.append("<thead class='bg'>");
						tmRecords.append("<tr>");
						
						
						tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblDistrictName", locale)+"</th>");
						
						tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("optSchool", locale)+"</th>");
						
						tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJobId", locale)+"</th>");
			    
						tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoTil", locale)+"</th>");
						
						tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJobCat", locale)+"</th>");
						
						tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblNumberOfHires", locale)+"</th>");
						tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblAverageHireDays", locale)+"</th>");
						
						tmRecords.append("</tr>");
						tmRecords.append("</thead>");
						tmRecords.append("<tbody>");
			
			if(finallstNobleCandidate.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			}
		String sta="";
			if(finallstNobleCandidate.size()>0){
				  for (Iterator it = finallstNobleCandidate.iterator(); it.hasNext();)
	                   {
	                	   String[] row = (String[]) it.next(); 
	                	   
	                	   String  myVal1="";
	                	   String  myVal2="";
	                	  
	                	        tmRecords.append("<tr>");	
	                	        
	                            myVal1 = (row[0]==null)? "N/A" : row[0].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
	                            
			                    
			                    myVal1 = (row[1]==null)? "N/A" : row[1].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? "N/A" : row[2].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    
			                    myVal1 = (row[3]==null)? "N/A" : row[3].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[4]==null)? "N/A" : row[4].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[5]==null)? "N/A" : row[5].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[6]==null)? "N/A" : row[6].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    tmRecords.append("</tr>");
	               }               
	              
			}
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
  }
	
	
}


