package tm.services;

/**********************************
     ***********************
    * @Author Ram Nath   *
  **********************
******************************/

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import tm.services.district.GlobalServices;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
public class MakePDF {
	String CREATER = "TeacherMatch Inc.";
	Color blue = new Color(0, 122, 180);
	BaseFont tahoma = null;

	public void setBaseFont(String fontPath, String fontName)
			throws DocumentException, IOException {
		BaseFont.createFont(fontPath + fontName, BaseFont.IDENTITY_H,
				BaseFont.EMBEDDED);
		tahoma = BaseFont.createFont(fontPath + fontName, BaseFont.CP1252,
				BaseFont.NOT_EMBEDDED);
	}

	public Document createDocument(Rectangle pageSize, Float leftMargin,
			Float rightMargin, Float topMargin, Float bottomMargin,
			Boolean pageHorizontal) {
		Rectangle rec = null;
		if (pageSize == null) {
			rec = PageSize.A4;
		} else {
			rec = pageSize;
		}
		if (leftMargin == null) {
			leftMargin = 50f;
		}
		if (rightMargin == null) {
			rightMargin = 50f;
		}
		if (topMargin == null) {
			topMargin = 50f;
		}
		if (bottomMargin == null) {
			bottomMargin = 50f;
		}
		Document document = new Document(rec, leftMargin, rightMargin,
				topMargin, bottomMargin);
		if (pageHorizontal != null && pageHorizontal) {
			document.setPageSize(PageSize.LETTER.rotate());
		}
		return document;
	}

	public PdfWriter createPdfWriter(Document document, String pdfPath,
			String pdfName) throws FileNotFoundException, DocumentException {
		try {
			System.out.println("pdfPath+pdfName====" + pdfPath + pdfName);
			return PdfWriter.getInstance(document,
					new java.io.FileOutputStream(pdfPath + pdfName));
		} catch (FileNotFoundException e) {
			File file = new File(pdfPath);
			if (!file.exists())
				file.mkdirs();
			return PdfWriter.getInstance(document,
					new java.io.FileOutputStream(pdfPath + pdfName));
		}
	}

	public void setAuthorAndSubject(Document document, String authorName,
			String subject, String title) {
		document.addAuthor(authorName);
		document.addSubject(subject);
		document.addCreator(CREATER);
		document.addCreationDate();
		document.addTitle(title);
	}

	public void openDocument(Document document) {
		document.open();
	}

	public int getAlignment(String align) {
		int alignment = 0;
		if (align == null)
			alignment = Element.ALIGN_LEFT;
		else if (align.equals("left"))
			alignment = Element.ALIGN_LEFT;
		else if (align.equals("right"))
			alignment = Element.ALIGN_RIGHT;
		else if (align.equals("center"))
			alignment = Element.ALIGN_CENTER;
		else if (align.equals("justify"))
			alignment = Element.ALIGN_JUSTIFIED;
		return alignment;
	}

	public int getImageAlignment(String align) {
		int alignment = 0;
		if (align == null)
			alignment = Image.DEFAULT;
		else if (align.equals("left"))
			alignment = Image.LEFT;
		else if (align.equals("right"))
			alignment = Image.RIGHT;
		else if (align.equals("center"))
			alignment = Image.MIDDLE;
		else if (align.equals("justify"))
			alignment = Image.ALIGN_JUSTIFIED;
		return alignment;
	}

	public int getFontStyle(String fontStyle) {
		int fontStyle1 = Font.NORMAL;
		if (fontStyle != null) {
			if (fontStyle.equals("normal")) {
				fontStyle1 = Font.NORMAL;
			} else if (fontStyle.equals("bold")) {
				fontStyle1 = Font.BOLD;
			} else if (fontStyle.equals("italic")) {
				fontStyle1 = Font.ITALIC;
			} else if (fontStyle.equals("bolditalic")) {
				fontStyle1 = Font.BOLDITALIC;
			} else {
				fontStyle1 = Font.NORMAL;
			}
		}
		return fontStyle1;
	}

	public Font getFontTahoma(BaseFont tahoma, int fontSize, int fontStyle1,
			Color fontcolor) {
		Color setColor = Color.white;
		if (fontcolor != null) {
			setColor = fontcolor;
		}
		if (tahoma == null) {
			return FontFactory.getFont(FontFactory.TIMES_ROMAN, fontSize,
					fontStyle1, setColor);
		} else {
			return new Font(tahoma, fontSize, fontStyle1, setColor);
		}

	}

	public PdfPTable createPdfTable(int columnSize, int widthInPercentage,
			float[] columnWidth, String align) {
		PdfPTable pdfTable = null;
		try {
			int alignment = getAlignment(align);
			float[] columnWidths = columnWidth;
			pdfTable = new PdfPTable(columnSize);
			pdfTable.setHorizontalAlignment(alignment);
			pdfTable.setWidthPercentage(widthInPercentage);
			pdfTable.setWidths(columnWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return pdfTable;
	}

	public void addCellInHeaderPDFTable(PdfPTable pdfTable,
			List<String> content, String align, String fontStyle, int fontSize,
			int border, Color fontcolor, Color bgColor, Integer colspan) {
		int fontStyle1 = getFontStyle(fontStyle);
		for (String contentData : content) {
			PdfPCell cell = null;
			cell = new PdfPCell(new Paragraph(contentData + "\n",
					getFontTahoma(tahoma, fontSize, fontStyle1, fontcolor)));
			if (colspan != null)
				cell.setColspan(colspan);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(bgColor);
			if (border != -1)
				cell.setBorder(border);
			pdfTable.addCell(cell);
		}
	}

	public void addCellInPDFTable(PdfPTable pdfTable, List<String> content,
			String align, String fontStyle, int fontSize, int border,
			Color fontcolor, Color bgColor, Integer colspan) {
		int fontStyle1 = getFontStyle(fontStyle);
		int alignment = getAlignment(align);
		for (String contentData : content) {
			PdfPCell cell = null;
			cell = new PdfPCell(new Paragraph(contentData + "\n",
					getFontTahoma(tahoma, fontSize, fontStyle1, fontcolor)));
			if (colspan != null)
				cell.setColspan(colspan);
			cell.setHorizontalAlignment(alignment);
			cell.setBackgroundColor(bgColor);
			if (border != -1)
				cell.setBorder(border);
			pdfTable.addCell(cell);
		}
	}

	public PdfPCell createPDFCell(String content, String align,
			String fontStyle, int fontSize, int border, int width,
			Color fontcolor, Color bgColor, Integer colspan) {
		int fontStyle1 = getFontStyle(fontStyle);
		int alignment = getAlignment(align);
		PdfPCell cell = null;
		cell = new PdfPCell(new Paragraph(content + "\n", getFontTahoma(tahoma,
				fontSize, fontStyle1, fontcolor)));
		if (colspan != null)
			cell.setColspan(colspan);
		cell.setHorizontalAlignment(alignment);
		cell.setBackgroundColor(bgColor);
		if (border != -1)
			cell.setBorder(border);
		return cell;
	}

	public void addImageInPDFCell(String imagePath, String imageName,
			Float imageSize, String align, Document document, PdfPTable pdfTable) {
		Image image;
		try {
			int alignment = getAlignment(align);
			image = Image.getInstance(imagePath + imageName);
			image.setAlignment(getImageAlignment(align));
			image.scalePercent(imageSize);
			// para[1] = new Paragraph(" ",font20bold);
			PdfPCell cell = new PdfPCell(image);
			cell.setHorizontalAlignment(alignment);
			cell.setBorder(0);
			if (pdfTable != null)
				pdfTable.addCell(cell);
			if (document != null)
				try {
					document.add(image);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		} catch (BadElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void newLinePhrase(Document document) {
		try {
			document.add(new Phrase("\n"));
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void newLineParagraph(Document document) {
		try {
			document.add(new Paragraph("\n"));
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Paragraph getParagraph(String content, Boolean newLine,
			String fontStyle, int fontSize, Color fontcolor)
			throws DocumentException {
		Paragraph paragraph = null;
		String newLineText = "";
		if (newLine != null && newLine) {
			newLineText = "\n";
		}
		int fontStyle1 = getFontStyle(fontStyle);
		paragraph = new Paragraph(content, getFontTahoma(tahoma, fontSize,
				fontStyle1, fontcolor));
		return paragraph;
	}

	public Phrase getPhrase(String content, Boolean newLine, String fontStyle,
			int fontSize, Color fontcolor) throws DocumentException {
		Phrase phrase = null;
		String newLineText = "";
		if (newLine != null && newLine) {
			newLineText = "\n";
		}
		int fontStyle1 = getFontStyle(fontStyle);
		phrase = new Phrase(content, getFontTahoma(tahoma, fontSize,
				fontStyle1, fontcolor));
		return phrase;
	}

	public void setFooter(Document document, String footerContent,
			String align, int fontSize, String fontStyle, Color fontcolor) {
		HeaderFooter footer = null;
		int alignment = getAlignment(align);
		int fontStyle1 = getFontStyle(fontStyle);
		footer = new HeaderFooter(new Phrase(footerContent, getFontTahoma(
				tahoma, fontSize, fontStyle1, fontcolor)), false);
		footer.setAlignment(alignment);
		footer.setBorder(0);
		document.setFooter(footer);
	}

	public void addTableInDocument(Document document, PdfPTable pdfTable) {
		if (pdfTable != null) {
			try {
				document.add(pdfTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void closeDocument(Document document, PdfPTable pdfTable) {
		if (pdfTable != null) {
			try {
				document.add(pdfTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			document.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	//Example how to create pdf using this Class
	public static void main(String[] args) throws IOException {
		try{
	List<String> header=GlobalServices.createList();
	header.add("Name");
	header.add("Roll");
	header.add("Marks");
	header.add("Name");
	header.add("Roll");
	header.add("Marks");
	List<String> data=GlobalServices.createList();
	for(int i=0;i<=1000;i++){
	data.add("Sohan");
	data.add("23");
	data.add("90");
	data.add("Sohaniigggg ggggggggggg ggggggggg ggggggggggggg dsgfsdgdg sfgsd iiiiiii");
	data.add("23");
	data.add("90");
	}
	
	String imagePath="D:\\Ram\\30-03-2015 workspace\\teachermatchnowRAM\\WebRoot\\images\\";
	String imageName="logo_teachermatch_large.png";
	
	System.out.println(data);
	MakePDF pdf=new MakePDF();
	Document document=pdf.createDocument(null,50f,50f,20f,20f, false);
	pdf.createPdfWriter(document, "E://","demo.pdf");
	pdf.setAuthorAndSubject(document, "Ram Nath", "Tested", "TEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
	pdf.openDocument(document);
	pdf.addImageInPDFCell(imagePath, imageName, 20f, "center", document, null);
	document.add(new Phrase("\n\n"));
	document.add(new Phrase("\n\n"));
	PdfPTable table=pdf.createPdfTable(6, 100,new float[] {300f, 100f, 100f, 300f, 100f, 100f}, "center");
	pdf.addCellInHeaderPDFTable(table, header, "center", "bold", 15, 0, Color.white,Color.blue,0);
	pdf.addCellInPDFTable(table, data, "left", "normal", 10, 1,Color.black,Color.white,0);
	pdf.setFooter(document, "Set Here Footer", "center", 10, "bold", Color.blue);
	pdf.closeDocument(document, table);
	System.out.println("yeeeeeeeeee");
	
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}


