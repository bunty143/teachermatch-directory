package tm.services;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.util.UidGenerator;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import tm.bean.CmsEventScheduleMaster;
import tm.bean.CmsEventTypeMaster;
import tm.bean.EventDetails;
import tm.bean.EventFacilitatorsList;
import tm.bean.EventSchedule;
import tm.bean.MessageToTeacher;
import tm.bean.SlotSelection;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;
import tm.dao.CmsEventScheduleMasterDAO;
import tm.dao.CmsEventTypeMasterDao;
import tm.dao.EventDetailsDAO;
import tm.dao.EventFacilitatorsListDAO;
import tm.dao.EventScheduleDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.SlotSelectionDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class SlotSelectionAjax {
	 
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
	{
	this.districtMasterDAO = districtMasterDAO;
	}
	@Autowired
	private CmsEventTypeMasterDao cmsEventTypeMasterDao;
	public void setCmsEventTypeMasterDao(
				CmsEventTypeMasterDao cmsEventTypeMasterDao) {
			this.cmsEventTypeMasterDao = cmsEventTypeMasterDao;
		}
	@Autowired 
	private EventDetailsDAO eventDetailsDAO;
	@Autowired
	private EventScheduleDAO eventScheduleDAO;
	@Autowired
	private SlotSelectionDAO slotSelectionDAO;
	@Autowired
	private CmsEventScheduleMasterDAO cmsEventScheduleMasterDAO;
	@Autowired
	private EventFacilitatorsListDAO eventFacilitatorsListDAO;
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;
	
	public String displayEvents(String noOfRow, String pageNo,String sortOrder,String sortOrderType)	
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		
		try{
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
		
			String sortOrderFieldName	=	"eventName";
			String sortOrderNoField		=	"eventName";
			Order  sortOrderStrVal		=	null;
			
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) ){
					 sortOrderFieldName	=	sortOrder;
					 sortOrderNoField	=	sortOrder;
				 }
				 if(sortOrder.equals("districtMaster")) {
					 sortOrderNoField="districtMaster";
				 }
				 
				 if(sortOrder.equals("eventName")) {
					 sortOrderNoField="eventName";
				 }
				 if(sortOrder.equals("eventTypeId")) {
					 sortOrderNoField="eventTypeId";
				 }
				 
				 
				
			}
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			} else {
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			List<EventDetails> eventdetails	 =	null;
			TeacherDetail teacherDetail=(TeacherDetail) session.getAttribute("teacherDetail");
			totalRecord=eventDetailsDAO.getRowCount();
			
			List<Integer> eventIds= new ArrayList<Integer>();
			List evntdsch =new ArrayList();
			 evntdsch=eventScheduleDAO.listEventDetailsl();
			  
				 System.out.println("evntdsch.size()evntdsch.size()evntdsch.size() :: " +evntdsch.size());
				 for(Object  object : evntdsch ){
					 Object[] obj = (Object[]) object;
					 eventIds.add((Integer)obj[0]);
				 }			 
			 
				 if(eventIds!=null&&  eventIds.size()>0){
					 CmsEventTypeMaster cmsEventTypeMaster=cmsEventTypeMasterDao.findById(1,false,false);
					 Criterion ctttype=Restrictions.ne("eventTypeId", cmsEventTypeMaster);
					 
					 Criterion criterion2 = Restrictions.in("eventId",eventIds);
					 
					 Criterion crtschname=Restrictions.eq("eventScheduleName", "Fixed Date and Time");
					 Criterion crtschname1=Restrictions.eq("eventScheduleName", "Multiple Fixed Date and Time");
					 
					 Criterion criteriaMix=Restrictions.and(crtschname, crtschname1);
					 
					 System.out.println("sxxsa "+crtschname);
					 List<CmsEventScheduleMaster> cmsevntschmas=cmsEventScheduleMasterDAO.findByCriteria(criteriaMix);
					 
					 Criterion crtschtype=Restrictions.not(Restrictions.in("eventSchedulelId",cmsevntschmas));
					 
					 eventdetails = eventDetailsDAO.findByCriteria(ctttype,criterion2);
				 }
			Criterion crtslots=Restrictions.eq("teacherDetail",teacherDetail);
			List<SlotSelection> lstslotSelection=slotSelectionDAO.findByCriteria(crtslots);
			
			Map<Integer,EventSchedule> slotmap=new HashMap<Integer, EventSchedule>();
			//List<Integer> evntdtllist=new ArrayList<Integer>();
			for(SlotSelection slotsel:lstslotSelection)
			{
				EventSchedule eventsch=slotsel.getEventSchedule();
				int eventsId=eventsch.getEventDetails().getEventId();
				slotmap.put(eventsId, eventsch);
			}
			
			Criterion crtsch=Restrictions.in("eventDetails", eventdetails);
			List<EventSchedule> eventScheduleList=eventScheduleDAO.findByCriteria(crtsch);
			List<Integer> l5=new ArrayList<Integer>();
			for(EventSchedule evnsch:eventScheduleList)
			{
			l5.add(evnsch.getEventDetails().getEventId());	
			}	
			
			List<EventDetails> sortedeventdetails  =	 new ArrayList<EventDetails>();

			SortedMap<String,EventDetails>	sortedMap = new TreeMap<String,EventDetails>();
			if(sortOrderNoField.equals("eventName")) {
				sortOrderFieldName	=	"eventName";
			}
			if(sortOrderNoField.equals("eventTypeId")) {
				sortOrderFieldName	=	"eventTypeId";
			}

			if(sortOrderNoField.equals("districtMaster")) {
				sortOrderFieldName	=	"districtMaster";
			}

			int mapFlag=2;
			String orderFieldName = "";
			for (EventDetails eventdtl : eventdetails){

				orderFieldName=""+eventdtl.getEventId();
				if(sortOrderFieldName.equals("eventName")){
					orderFieldName=eventdtl.getEventName()+"||"+eventdtl.getEventId();
					sortedMap.put(orderFieldName+"||",eventdtl);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					} else {
						mapFlag=1;
					}
				}

				orderFieldName=""+eventdtl.getEventTypeId().getEventTypeName();
				if(sortOrderFieldName.equals("eventTypeId")){
				orderFieldName=""+eventdtl.getEventTypeId().getEventTypeName()+"||"+eventdtl.getEventId();
					sortedMap.put(orderFieldName+"||",eventdtl);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}

				orderFieldName=""+eventdtl.getDistrictMaster();
				if(sortOrderFieldName.equals("districtMaster")){
				orderFieldName=" "+eventdtl.getDistrictMaster().getDistrictName()+"||"+eventdtl.getEventId();
					sortedMap.put(orderFieldName+"||",eventdtl);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			
			}
			if(mapFlag==1) {
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet(); 
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedeventdetails.add((EventDetails) sortedMap.get(key));
				} 
			} else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedeventdetails.add((EventDetails) sortedMap.get(key));
				}
			} else {
				sortedeventdetails=eventdetails;
			}

			
			totalRecord =sortedeventdetails.size();		
			if(totalRecord<end)
			   end=totalRecord;
	        List<EventDetails> lstsortedeventDetails		=	sortedeventdetails.subList(start,end);
			
	        dmRecords.append("<table id='eventTable' border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDistrictName", locale),sortOrderFieldName,"districtMaster",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");


			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEventName", locale),sortOrderFieldName,"eventName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEventType", locale),sortOrderFieldName,"eventTypeId",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");

			dmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblLocti", locale)+"</th>");
			dmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("msgDate/Time", locale)+"</th>");
				
			dmRecords.append("<th >"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(eventdetails.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoEventfound", locale)+"</td></tr>" );
			
			//int noofrec=0;
			for (EventDetails eventdtllist :lstsortedeventDetails) 
			{
				dmRecords.append("<tr>");
				dmRecords.append("<td>"+eventdtllist.getDistrictMaster().getDistrictName()+"</td>");
				dmRecords.append("<td>"+eventdtllist.getEventName()+"</td>");
				dmRecords.append("<td>"+eventdtllist.getEventTypeId().getEventTypeName()+"</td>");
				
				if(slotmap.containsKey(eventdtllist.getEventId()))
				  {	
					EventSchedule eventSchedule=slotmap.get(eventdtllist.getEventId());	
					SimpleDateFormat formate=new SimpleDateFormat("MMM dd,  yyyy");
					Date d=eventSchedule.getEventDateTime();
					String eventdate=formate.format(d);
					String starttime=eventSchedule.getEventStartTime()+"&nbsp;"+eventSchedule.getEventStartTimeFormat();
					String endtime=eventSchedule.getEventEndTime()+"&nbsp;"+eventSchedule.getEventEndTimeFormat();
					String schedule=eventdate+"&nbsp;&nbsp;&nbsp;&nbsp;"+starttime+" - "+endtime;
				
					dmRecords.append("<td><a href='javascript:void(0);' onclick=\'return showSchedule("+eventdtllist.getEventId()+",1)'>"+eventSchedule.getLocation()+"</a></td>");
					dmRecords.append("<td><a href='javascript:void(0);' onclick=\'return showSchedule("+eventdtllist.getEventId()+",1)'>"+schedule+"</a></td>");
					dmRecords.append("<td><a href='javascript:void(0);' onclick=\'return showSchedule("+eventdtllist.getEventId()+",1)'>Select Slot</a></td>");
				 }
				else
				 {
					dmRecords.append("<td><a href='javascript:void(0);' onclick=\'return showSchedule("+eventdtllist.getEventId()+",0)'><span class='icon-calendar icon-large iconcolorhover'></span></a></td>");
					dmRecords.append("<td><a href='javascript:void(0);' onclick=\'return showSchedule("+eventdtllist.getEventId()+",0)'><span class='icon-calendar icon-large iconcolorhover'></span></a></td>");
					dmRecords.append("<td><a href='javascript:void(0);' onclick=\'return showSchedule("+eventdtllist.getEventId()+",0)'>Select Slot</a></td>");
				 }	
				dmRecords.append("</tr>");
			   }
			 				
			 dmRecords.append("</table>");
			 dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dmRecords.toString();
	}

public String showSlotGrid(int eventId,int status,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	StringBuffer dmRecords =	new StringBuffer();
	try{
		List<SlotSelection> listSlotSelections = new ArrayList<SlotSelection>();
		Map<Integer,EventSchedule> mapAlreadySelectedSlot = new HashMap<Integer, EventSchedule>();
		TeacherDetail teacherDetail=(TeacherDetail) session.getAttribute("teacherDetail");
		Criterion crtslots=Restrictions.eq("teacherDetail",teacherDetail);
		List<SlotSelection> lstslotSelection=slotSelectionDAO.findByCriteria(crtslots);
		List<Integer> lsteventsch=new ArrayList<Integer>();
	
		//Fixed Candidate on slot
		//Map<scheduleId,No. ofCandidate>	
		Map<Integer,Integer> fixedSlotWiseCandiadteMap= new HashMap<Integer, Integer>();
		boolean isFixedCandidateSlot=false;
		
		for(SlotSelection sltsl:lstslotSelection)
		{
			lsteventsch.add(sltsl.getEventSchedule().getEventScheduleId());	
		}	
	
		EventDetails eventDetails=eventDetailsDAO.findById(eventId, false,false);
		Criterion crit=Restrictions.eq("eventDetails",eventDetails);
		List<EventSchedule> eventSchedduleList=eventScheduleDAO.findByCriteria(crit);
		
		//checking already selected slot
		int noOfCandidatePerSlot=0;
		if(eventSchedduleList.get(0).getSinglebookingonly()!=null && eventSchedduleList.get(0).getSinglebookingonly()){
			listSlotSelections= slotSelectionDAO.findByEventScheduleList(eventSchedduleList);
			if(listSlotSelections!=null && listSlotSelections.size()>0){
				for(SlotSelection ss :  listSlotSelections){
					mapAlreadySelectedSlot.put(ss.getEventSchedule().getEventScheduleId(), ss.getEventSchedule());
				}
			}
		}else{
			//check for no of candidates per slot
			if(eventSchedduleList.get(0).getNoOfCandidatePerSlot()!=null && eventSchedduleList.get(0).getNoOfCandidatePerSlot()>0)
			{
				isFixedCandidateSlot=true;
				noOfCandidatePerSlot=eventSchedduleList.get(0).getNoOfCandidatePerSlot();
				fixedSlotWiseCandiadteMap=slotSelectionDAO.schedulewiseCandidateCount(eventSchedduleList);
			}
		}
		
		dmRecords.append("<table id='scheduleTable' border='0' class='table table-bordered table-striped' >");
		dmRecords.append("<thead class='bg'>");
		dmRecords.append("<tr>");
		dmRecords.append("<th></th>");
		dmRecords.append("<th>"+Utility.getLocaleValuePropByKey("msgEventDate", locale)+"</th>");
		dmRecords.append("<th>"+Utility.getLocaleValuePropByKey("msgTimeFrom", locale)+"</th>");
		dmRecords.append("<th>"+Utility.getLocaleValuePropByKey("msgTimeTo", locale)+"</th>");
		dmRecords.append("<th>"+Utility.getLocaleValuePropByKey("lblLocti", locale)+"</th>");
		dmRecords.append("</tr>");
		dmRecords.append("</thead>");
		/*================= Checking If Record Not Found ======================*/
		
		for (EventSchedule eventSch :eventSchedduleList) {
			dmRecords.append("<tr>");
            Date eventdate=eventSch.getEventDateTime();
            SimpleDateFormat formate=new SimpleDateFormat("MMM dd,  yyyy");
            String datestring=formate.format(eventdate);
			String starttime=eventSch.getEventStartTime()+"&nbsp;"+eventSch.getEventStartTimeFormat();
			String endtime=eventSch.getEventEndTime()+"&nbsp;"+eventSch.getEventEndTimeFormat();
			String disablestr="";
			String checked="";
			if(lsteventsch.contains(eventSch.getEventScheduleId()))
			{
				checked="checked=''";	
			}
			if(status==1)
			{
				disablestr="disabled='disabled'";	
			}	
			
			if(mapAlreadySelectedSlot.get(eventSch.getEventScheduleId())!=null){
				disablestr="disabled='disabled'";	
			}
			
			if(isFixedCandidateSlot){
				if(fixedSlotWiseCandiadteMap.get(eventSch.getEventScheduleId())!=null && ( fixedSlotWiseCandiadteMap.get(eventSch.getEventScheduleId()).equals(noOfCandidatePerSlot) || fixedSlotWiseCandiadteMap.get(eventSch.getEventScheduleId())>noOfCandidatePerSlot)){
					disablestr="disabled='disabled'";
				}
			}
			
			String sLocation="";
			if(eventSch.getLocation()!=null)
				sLocation=eventSch.getLocation();
			
			String radio="<input type='radio' name='chooseoption' id='chooseoption' value='"+eventSch.getEventScheduleId()+"'"+disablestr+" "+checked+">";
			dmRecords.append("<td>"+radio+"</td>");
			dmRecords.append("<td>"+datestring+"</td>");
			dmRecords.append("<td>"+starttime+"</td>");
			dmRecords.append("<td>"+endtime+"</td>");
			dmRecords.append("<td>"+sLocation+"</td>");
			dmRecords.append("</tr>");		
		
		}
		dmRecords.append("</table>");
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	return dmRecords.toString();	
}

public String saveSlots(int eventScheduleId)
{
	System.out.println("eventScheduleId"+eventScheduleId);
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	  if(session == null ||(session.getAttribute("teacherDetail")==null)) 
	   {
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
       }
	  //check in flag for one candidate on one slot. Singlebookingonly()
	  boolean unlockedSlotFlag=true;
	  String returnvalue="";
	  List<SlotSelection> listSlotSelections =new  ArrayList<SlotSelection>();
	  SlotSelection slotSelection=new SlotSelection();
      TeacherDetail teacherDetail=(TeacherDetail) session.getAttribute("teacherDetail");
      EventSchedule eventSchedule=eventScheduleDAO.findById(eventScheduleId,false,false);
	    
      if(eventSchedule.getEventDetails().getStatus().equalsIgnoreCase("I")){
    	  return "I";
      }
      
	   try {
		 if(eventSchedule.getSinglebookingonly()!=null && eventSchedule.getSinglebookingonly()){
			   listSlotSelections =  slotSelectionDAO.findByEventScheduleId(eventSchedule);
			   if(listSlotSelections!=null && listSlotSelections.size()>0){
				   unlockedSlotFlag=false;
			   }
		   }else{
			   if(eventSchedule.getNoOfCandidatePerSlot()!=null && eventSchedule.getNoOfCandidatePerSlot()>0){
				   listSlotSelections =  slotSelectionDAO.findByEventScheduleId(eventSchedule);
				   if(listSlotSelections!=null && listSlotSelections.size()>0)
				   {
					   if(listSlotSelections.size()>=eventSchedule.getNoOfCandidatePerSlot()){
						   unlockedSlotFlag=false;
					   }
				   }
			   }
		   }
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		try
		{			
		  if(unlockedSlotFlag)
		  {
			returnvalue="success";
			slotSelection.setTeacherDetail(teacherDetail);
		    slotSelection.setEventSchedule(eventSchedule);
			slotSelectionDAO.makePersistent(slotSelection);
			/*int eventId=eventSchedule.getEventDetails().getEventId();
			System.out.println("eventId ..."+ eventId);
			EventDetails eventdetails=eventDetailsDAO.findById(eventId,false,false);*/
			EventDetails eventdetails=eventSchedule.getEventDetails();
			String districtName="";
			if(eventdetails.getDistrictMaster()!=null)
			   districtName=eventdetails.getDistrictMaster().getDistrictName();
			else if(eventdetails.getBranchMaster()!=null)
				districtName=eventdetails.getBranchMaster().getBranchName();
			else if(eventdetails.getHeadQuarterMaster()!=null)
				districtName=eventdetails.getHeadQuarterMaster().getHeadQuarterName();
			
			
			Criterion crt=Restrictions.eq("eventDetails", eventdetails);		
			List<EventFacilitatorsList> eventFacilitatorsList=eventFacilitatorsListDAO.findByCriteria(crt);	
		
			String mailid="";			
			String content="";		
			
			// String 
			String[] glink = null;
			String calFileName="";
			String calfile="";
			//Create ICS File 			
			String eventSubject=eventdetails.getEventName();				
			String desc=eventdetails.getDescription();			
			calFileName= createIcalFile(eventSubject, desc,eventSchedule);	
			String location=eventSchedule.getLocation();
			String location1="";
			String location2="";
			String location3="";
			if(location!=null && location!=""){
				location1=" at "+eventSchedule.getLocation();
				location2="<br>"+Utility.getLocaleValuePropByKey("lblLocti", locale)+": "+eventSchedule.getLocation();
				location3="<p>"+Utility.getLocaleValuePropByKey("msgTheLocationofthe", locale)+" "+eventdetails.getEventTypeId().getEventTypeName()+" is "+eventSchedule.getLocation()+". </p>";				
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
			SimpleDateFormat formday=new SimpleDateFormat("EEEE");
			String dayofweek=formday.format(eventSchedule.getEventDateTime());			
			String slotTime=eventSchedule.getEventStartTime()+" "+eventSchedule.getEventStartTimeFormat()+Utility.getLocaleValuePropByKey("lblTo", locale)+ " "+eventSchedule.getEventEndTime()+" "+eventSchedule.getEventEndTimeFormat()+" "+eventSchedule.getTimeZoneMaster().getTimeZoneShortName();
			String slotDate=dayofweek+", "+dateFormat.format(eventSchedule.getEventDateTime()).toString();	
			String eventName=eventdetails.getEventName();
			String participantsName=teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
			String eventSubjectforFacilator=""+participantsName+Utility.getLocaleValuePropByKey("msgSelectedTimeSlot", locale)+" "+slotTime +" "+Utility.getLocaleValuePropByKey("msgon3", locale) +" "+slotDate+" "+Utility.getLocaleValuePropByKey("msgfor3", locale)+" "+eventName;
			String eventSubjectforParticipants=Utility.getLocaleValuePropByKey("msgConfirmationForYour", locale)+eventName +": "+ slotTime+" "+Utility.getLocaleValuePropByKey("msgon3", locale) +" "+slotDate+" "+location1 ;
			//String eventSubjectforParticipants="Time Slot "+ slotTime +" on " +slotDate+ " is selected for "+eventName;
			String msgToParticipants="<p>"+Utility.getLocaleValuePropByKey("lblDear", locale)+" "+participantsName+",</p> <p>"+Utility.getLocaleValuePropByKey("msgConfirmingYourSlot", locale)+" "+eventName+". "+Utility.getLocaleValuePropByKey("msgDetailsRreBelow", locale)+":<br><br>"+Utility.getLocaleValuePropByKey("lblDate", locale)+":  "+slotDate+"<br>"+Utility.getLocaleValuePropByKey("msgTime3", locale)+":"+slotTime+location2+" "+"<br></p><p>"+Utility.getLocaleValuePropByKey("msgRegards", locale)+" "+"<br>"+districtName+"</p>";			
			
			System.out.println(calfile);
				 
				for(EventFacilitatorsList sender:eventFacilitatorsList)
				{
					try{		
						String facilatorName=sender.getFacilitatorFirstName()+" "+sender.getFacilitatorLastName();
						String msgToFacilator="<p>"+Utility.getLocaleValuePropByKey("lblDear", locale)+" "+facilatorName+",</p><p>"+Utility.getLocaleValuePropByKey("msgApplicant", locale)+" "+participantsName+" "+Utility.getLocaleValuePropByKey("msgSelectedTimeSlot", locale)+" "+slotTime+Utility.getLocaleValuePropByKey("msgon3", locale)+" "+slotDate+Utility.getLocaleValuePropByKey("msgfor3", locale)+" "+eventName+".</p>"+location3+" "+"<p>Regards<br>"+districtName+"</p>";
						content=msgToFacilator;		
						String email=sender.getFacilitatorEmailAddress();
						System.out.println(content);
						emailerService.sendMailWithAttachments(email, eventSubjectforFacilator, "noreply@teachermatch.net", content,calFileName.toString());
					}catch(Exception e)
					{e.printStackTrace();}
				}
				
					mailid=teacherDetail.getEmailAddress();				
					
					content=msgToParticipants;
					System.out.println("content     "+content);
					emailerService.sendMailWithAttachments(mailid, eventSubjectforParticipants, "noreply@teachermatch.net", content,calFileName.toString());
					
					//Saving Logs
					if(teacherDetail!=null)
					{
						UserMaster userMaster=eventSchedule.getEventDetails().getCreatedBY();
						MessageToTeacher  messageToTeacher= new MessageToTeacher();
						messageToTeacher.setTeacherId(teacherDetail);
						//messageToTeacher.setJobId(jobOrder);
						messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
						messageToTeacher.setSenderId(userMaster);
						messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
						messageToTeacher.setEntityType(userMaster.getEntityType());
						messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
						messageToTeacher.setMessageSubject(eventSubjectforParticipants);
						messageToTeacher.setMessageSend(content);
						
						try {
							messageToTeacherDAO.makePersistent(messageToTeacher);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					return "success";
		  }	else{
			  returnvalue=Utility.getLocaleValuePropByKey("msgSlotAlreadyBookedSomeone", locale);
		  }		
		}catch(Exception e)
		{
			e.printStackTrace();
			//System.out.println(e);
		}		
		
	return returnvalue;	
 }
	public String createIcalFile(String subject,String description,EventSchedule eventShedule) throws ParseException 
			{	
		Calendar icsCalendar = new Calendar();
		String calFileName = String.valueOf(System.currentTimeMillis()).substring(6);
		File calFile = new File(Utility.getValueOfPropByKey("iclcalendarRootPath")+"/"+calFileName+"cal.ics");
		String email="";
		String fromdate="";
		String fromtime="";
		String enddate="";
		String endtime="";
		String googleStartTime="";
		String googleEndTime="";
		String location="";
		//EventSchedule eventShedule=eventschlist.get(0);					
								
			location=eventShedule.getLocation();		
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			fromdate=dateFormat.format(eventShedule.getEventDateTime());
			enddate=dateFormat.format(eventShedule.getEventDateTime());
			fromtime=eventShedule.getEventStartTime()+" "+eventShedule.getEventStartTimeFormat();
			endtime=eventShedule.getEventEndTime()+" "+eventShedule.getEventEndTimeFormat();
			
			//convert time AM/PM to 24 hour
			SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
			SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
			Date sdate = parseFormat.parse(fromtime);
			Date edate = parseFormat.parse(endtime);
			String[] convertStartTime =displayFormat.format(sdate).split(":");
			String[] convertEndTime =displayFormat.format(edate).split(":");       
			
			/////StartTime
			int Atime=Integer.parseInt(convertStartTime[0]);
			int Btime=Integer.parseInt(convertStartTime[1]);
			
			/////EndTime
			int Ctime=Integer.parseInt(convertEndTime[0]);
			int Dtime=Integer.parseInt(convertEndTime[1]);
			
			String[] convertStartDates =fromdate.split("-");
			String[] convertEndDates =enddate.split("-");
			
			/////////Start Date       
			
			if(Integer.parseInt(convertStartDates[1])==1)
			{			
				convertStartDates[1]="12";
				convertStartDates[0]=String.valueOf((Integer.parseInt(convertStartDates[0])-1));
			}
			else
			{
				convertStartDates[1]=String.valueOf((Integer.parseInt(convertStartDates[1])-1));			
			}
			int A=(Integer.parseInt(convertStartDates[1]));
			int B=(Integer.parseInt(convertStartDates[2]));
			int C=(Integer.parseInt(convertStartDates[0]));			
			/////////////End Date		
			if(Integer.parseInt(convertEndDates[1])==1)
			{			
				convertEndDates[1]="12";
				convertEndDates[0]=String.valueOf((Integer.parseInt(convertEndDates[0])-1));
			}
			else
			{
				convertEndDates[1]=String.valueOf((Integer.parseInt(convertEndDates[1])-1));			
			}
			int D=(Integer.parseInt(convertEndDates[1]));
			int E=(Integer.parseInt(convertEndDates[2]));
			int F=(Integer.parseInt(convertEndDates[0]));		
		
			try {
			
			// Create a TimeZone
			/*TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
			TimeZone timezone = registry.getTimeZone("Asia/Calcutta");
			VTimeZone tz = ((net.fortuna.ical4j.model.TimeZone) timezone).getVTimeZone();*/
			
			// Start Date is on:
			java.util.Calendar startDate = new GregorianCalendar();
			//startDate.setTimeZone(timezone);
			startDate.set(java.util.Calendar.MONTH, A);
			startDate.set(java.util.Calendar.DAY_OF_MONTH, B);
			startDate.set(java.util.Calendar.YEAR, C);
			startDate.set(java.util.Calendar.HOUR_OF_DAY, Atime);
			startDate.set(java.util.Calendar.MINUTE, Btime);
			startDate.set(java.util.Calendar.SECOND, 0);
			
			// End Date is on:
			java.util.Calendar endDate = new GregorianCalendar();
			//endDate.setTimeZone(timezone);
			endDate.set(java.util.Calendar.MONTH, D);
			endDate.set(java.util.Calendar.DAY_OF_MONTH, E);
			endDate.set(java.util.Calendar.YEAR, F);
			endDate.set(java.util.Calendar.HOUR_OF_DAY, Ctime);
			endDate.set(java.util.Calendar.MINUTE, Dtime);	
			endDate.set(java.util.Calendar.SECOND, 0);
			
			// Create the event props
			String eventName = subject;
			DateTime start = new DateTime(startDate.getTime());
			DateTime end = new DateTime(endDate.getTime());			
			googleStartTime=start.toString();
			googleEndTime=end.toString();
			// Create the event
			VEvent meeting = new VEvent(start, end, eventName);	
			
			// add timezone to vEvent
			//meeting.getProperties().add(tz.getTimeZoneId());
			if(location!=null && location!=""){
				Location loc = new Location(location);
				meeting.getProperties().add(loc);
			}
			
			Description sum = new Description(description);
			meeting.getProperties().add(sum);
			
			// generate unique identifier and add it to vEvent
			UidGenerator ug;
			ug = new UidGenerator("uidGen");
			Uid uid = ug.generateUid();			
			meeting.getProperties().add(uid);
			
			// add attendees..
			/*Attendee dev1 = new Attendee(URI.create("someone@something"));
			dev1.getParameters().add(Role.REQ_PARTICIPANT);
			dev1.getParameters().add(new Cn("Developer 1"));
			meeting.getProperties().add(dev1);*/
			
			// assign props to calendar object
			icsCalendar.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
			icsCalendar.getProperties().add(CalScale.GREGORIAN);
			
			// Add the event and print
			System.out.println(meeting);
			icsCalendar.getComponents().add(meeting);
			CalendarOutputter outputter = new CalendarOutputter();
			outputter.setValidating(false);
			
			FileOutputStream fout = new FileOutputStream(calFile);
			outputter.output(icsCalendar, fout);
			}
			catch (Exception e)
			{
			System.err.println("Error in method icalfile() " + e);		
			}
			/*
			String microsoftStartDate=C+"-"+A+"-"+B+"t"+Atime+":"+Btime+":00z";
			String microsoftEndDate=F+"-"+D+"-"+E+"t"+Ctime+":"+Dtime+":00z";
			String microsoftCalendar="";
			microsoftCalendar="https://outlook.office365.com/api/v1.0/me/calendarview?startdatetime="+microsoftStartDate+"&enddatetime="+microsoftEndDate+"&location="+location.replace(" ", "+")+"";
			System.out.println("DDDDDDDDDDDDDD "+Utility.getValueOfPropByKey("iclcalendarRootPath"));
			String contents = googleLink(fromdate, fromtime, enddate, endtime, location,description,subject);
				
			contents="Google Calendar<br><br>"+contents+"\n<br><br><br>Microsoft Calendar<br><br>"+microsoftCalendar;
			System.out.println("contents::: "+contents);*/
			//  EmailerService emailerService=new EmailerService();	
			//String googleUrl= "Google Invitaion Calendar\n<br><br>"+ "<a href='https://www.google.com/calendar/render?action=TEMPLATE&details="+description.replace(" ", "+")+"&text="+subject.replace(" ", "+")+"&location="+location.replace(" ", "+")+"&dates="+(googleStartTime)+"%2F"+(googleEndTime)+"&calendar-label=Outgoing#f'>Google Invitation Calander Link</a>";			 	
			
			
			//emailerService.sendMailWithAttachments(email, subject, "noreply@teachermatch.net", contents,calFile.toString());
			System.out.println("execute complete");
			return calFile.toString();
			}	

 public String displayEventsNew(Integer eventId,String email,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
 { 
      	System.out.println(email);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer dmRecords =	new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<EventDetails> listEventDetails =new ArrayList<EventDetails>();
		Criterion criterionEmail=Restrictions.eq("emailAddress", email);
		List<TeacherDetail> listteacherDetails=teacherDetailDAO.findByCriteria(criterionEmail);
		
		TeacherDetail tDetail=listteacherDetails.get(0);
		try
		{
		EventDetails eventDetail =eventDetailsDAO.findById(eventId,false, false);		
		listEventDetails.add(eventDetail);
		Criterion criterionevent=Restrictions.eq("eventDetails", eventDetail);
		List<EventSchedule> listEventSchedules =eventScheduleDAO.findByCriteria(criterionevent);
		
		List<SlotSelection> listSlotSelections=new ArrayList<SlotSelection>();
		if(listEventSchedules!=null && listEventSchedules.size() >0)
		{
			Criterion criterion21=Restrictions.not(Restrictions.in("eventSchedule", listEventSchedules));
			listSlotSelections =slotSelectionDAO.findByCriteria(Restrictions.eq("teacherDetail", tDetail),criterion21);
		}
		else
		{
			listSlotSelections =slotSelectionDAO.findByCriteria(Restrictions.eq("teacherDetail", tDetail));
		}
		
		if(listSlotSelections!=null && listSlotSelections.size()>0)
		{
			for(SlotSelection sl: listSlotSelections)
			  listEventDetails.add(sl.getEventSchedule().getEventDetails());
		}
		
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
		
			String sortOrderFieldName	=	"eventName";
			String sortOrderNoField		=	"eventName";
			Order  sortOrderStrVal		=	null;
			
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) ){
					 sortOrderFieldName	=	sortOrder;
					 sortOrderNoField	=	sortOrder;
				 }
				 if(sortOrder.equals("districtMaster")) {
					 sortOrderNoField="districtMaster";
				 }
				 
				 if(sortOrder.equals("eventName")) {
					 sortOrderNoField="eventName";
				 }
				 if(sortOrder.equals("eventTypeId")) {
					 sortOrderNoField="eventTypeId";
				 }
				 
				 
				
			}
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			} else {
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			List<EventDetails> eventdetails	 =	null;
			TeacherDetail teacherDetail=(TeacherDetail) session.getAttribute("teacherDetail");
			String uu=teacherDetail.getFirstName();
			totalRecord=eventDetailsDAO.getRowCount();
			
			List<Integer> eventIds= new ArrayList<Integer>();
			List evntdsch =new ArrayList();
			 evntdsch=eventScheduleDAO.listEventDetailsl();
			  
				 System.out.println("evntdsch.size()evntdsch.size()evntdsch.size() :: " +evntdsch.size());
				 for(Object  object : evntdsch ){
					 Object[] obj = (Object[]) object;
					 eventIds.add((Integer)obj[0]);
				 }			 
			 
				 if(eventIds!=null&&  eventIds.size()>0){
					 CmsEventTypeMaster cmsEventTypeMaster=cmsEventTypeMasterDao.findById(1,false,false);					
					 Criterion ctttype=Restrictions.ne("eventTypeId", cmsEventTypeMaster);
					 
					 Criterion criterion2 = Restrictions.in("eventId",eventIds);
					 
					 Criterion crtschname=Restrictions.eq("eventScheduleName", "Fixed Date and Time");
					 Criterion crtschname1=Restrictions.eq("eventScheduleName", "Multiple Fixed Date and Time");
					 
					 Criterion criteriaMix=Restrictions.and(crtschname, crtschname1);
					 
					 System.out.println("sxxsa "+crtschname);
					 List<CmsEventScheduleMaster> cmsevntschmas=cmsEventScheduleMasterDAO.findByCriteria(criteriaMix);
					 
					 Criterion crtschtype=Restrictions.not(Restrictions.in("eventSchedulelId",cmsevntschmas));
					 
					 eventdetails = eventDetailsDAO.findByCriteria(ctttype,criterion2);
				 }
			Criterion crtslots=Restrictions.eq("teacherDetail",teacherDetail);
			List<SlotSelection> lstslotSelection=slotSelectionDAO.findByCriteria(crtslots);
			
			Map<Integer,EventSchedule> slotmap=new HashMap<Integer, EventSchedule>();
			//List<Integer> evntdtllist=new ArrayList<Integer>();
			for(SlotSelection slotsel:lstslotSelection)
			{
				EventSchedule eventsch=slotsel.getEventSchedule();
				int eventsId=eventsch.getEventDetails().getEventId();
				slotmap.put(eventsId, eventsch);
			}
			
			Criterion crtsch=Restrictions.in("eventDetails", eventdetails);
			List<EventSchedule> eventScheduleList=eventScheduleDAO.findByCriteria(crtsch);
			List<Integer> l5=new ArrayList<Integer>();
			for(EventSchedule evnsch:eventScheduleList)
			{
			l5.add(evnsch.getEventDetails().getEventId());	
			}	
			
			List<EventDetails> sortedeventdetails  =	 new ArrayList<EventDetails>();

			SortedMap<String,EventDetails>	sortedMap = new TreeMap<String,EventDetails>();
			if(sortOrderNoField.equals("eventName")) {
				sortOrderFieldName	=	"eventName";
			}
			if(sortOrderNoField.equals("eventTypeId")) {
				sortOrderFieldName	=	"eventTypeId";
			}

			if(sortOrderNoField.equals("districtMaster")) {
				sortOrderFieldName	=	"districtMaster";
			}

			int mapFlag=2;
			String orderFieldName = "";
			for (EventDetails eventdtl : eventdetails){

				orderFieldName=""+eventdtl.getEventId();
				if(sortOrderFieldName.equals("eventName")){
					orderFieldName=eventdtl.getEventName()+"||"+eventdtl.getEventId();
					sortedMap.put(orderFieldName+"||",eventdtl);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					} else {
						mapFlag=1;
					}
				}

				orderFieldName=""+eventdtl.getEventTypeId().getEventTypeName();
				if(sortOrderFieldName.equals("eventTypeId")){
				orderFieldName=""+eventdtl.getEventTypeId().getEventTypeName()+"||"+eventdtl.getEventId();
					sortedMap.put(orderFieldName+"||",eventdtl);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}

				/*orderFieldName=""+eventdtl.getDistrictMaster();
				if(sortOrderFieldName.equals("districtMaster")){
				orderFieldName=" "+eventdtl.getDistrictMaster().getDistrictName()+"||"+eventdtl.getEventId();
					sortedMap.put(orderFieldName+"||",eventdtl);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}*/
			
			}
			if(mapFlag==1) {
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet(); 
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedeventdetails.add((EventDetails) sortedMap.get(key));
				} 
			} else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedeventdetails.add((EventDetails) sortedMap.get(key));
				}
			} else {
				sortedeventdetails=eventdetails;
			}

			
			totalRecord =sortedeventdetails.size();		
			if(totalRecord<end)
			   end=totalRecord;
	       // List<EventDetails> lstsortedeventDetails		=	sortedeventdetails.subList(start,end);
			
	        dmRecords.append("<table id='eventTable' border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			
			/*responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDistrictName", locale),sortOrderFieldName,"districtMaster",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");*/
			
			dmRecords.append("<th  valign='top'>District/Branch/Headquarter Name</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEventName", locale),sortOrderFieldName,"eventName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEventType", locale),sortOrderFieldName,"eventTypeId",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");

			dmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblLocti", locale)+"</th>");
			dmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("msgDate_time", locale)+"</th>");
				
			dmRecords.append("<th >"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/				
			
			if(eventdetails.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoEventfound", locale)+"</td></tr>" );
			
			//int noofrec=0;
			for (EventDetails eventdtllist :listEventDetails) 
			{
				dmRecords.append("<tr>");
				
				if(eventdtllist.getDistrictMaster()!=null){
					 dmRecords.append("<td>"+eventdtllist.getDistrictMaster().getDistrictName()+"</td>");	
				}
				else if(eventdtllist.getHeadQuarterMaster()!=null)
				{
					 if(eventdtllist.getBranchMaster()!=null){
						 dmRecords.append("<td>"+eventdtllist.getBranchMaster().getBranchName()+"</td>");	 
					 }else{
						 dmRecords.append("<td>"+eventdtllist.getHeadQuarterMaster().getHeadQuarterName()+"</td>");
					 }
					 
				}
				dmRecords.append("<td>"+eventdtllist.getEventName()+"</td>");
				dmRecords.append("<td>"+eventdtllist.getEventTypeId().getEventTypeName()+"</td>");
				
				if(slotmap.containsKey(eventdtllist.getEventId()))
				  {	
					EventSchedule eventSchedule=slotmap.get(eventdtllist.getEventId());	
					SimpleDateFormat formate=new SimpleDateFormat("MMM dd,  yyyy");
					Date d=eventSchedule.getEventDateTime();
					String eventdate=formate.format(d);
					String starttime=eventSchedule.getEventStartTime()+"&nbsp;"+eventSchedule.getEventStartTimeFormat();
					String endtime=eventSchedule.getEventEndTime()+"&nbsp;"+eventSchedule.getEventEndTimeFormat();
					String schedule=eventdate+"&nbsp;&nbsp;&nbsp;&nbsp;"+starttime+" - "+endtime;
				
					if(eventSchedule.getLocation()!=null)
						dmRecords.append("<td><a href='javascript:void(0);' onclick=\'return showSchedule("+eventdtllist.getEventId()+",1)'>"+eventSchedule.getLocation()+"</a></td>");
					else
						dmRecords.append("<td><a href='javascript:void(0);'>&nbsp;</td>");
					
					dmRecords.append("<td><a href='javascript:void(0);' onclick=\'return showSchedule("+eventdtllist.getEventId()+",1)'>"+schedule+"</a></td>");
					dmRecords.append("<td><a href='javascript:void(0);' onclick=\'return showSchedule("+eventdtllist.getEventId()+",1)'>"+Utility.getLocaleValuePropByKey("msgSlotConfirmed", locale)+"</a></td>");
				 }
				else
				 {
					dmRecords.append("<td><a href='javascript:void(0);' onclick=\'return showSchedule("+eventdtllist.getEventId()+",0)'><span class='icon-calendar icon-large iconcolorhover'></span></a></td>");
					dmRecords.append("<td><a href='javascript:void(0);' onclick=\'return showSchedule("+eventdtllist.getEventId()+",0)'><span class='icon-calendar icon-large iconcolorhover'></span></a></td>");
					dmRecords.append("<td><a href='javascript:void(0);' onclick=\'return showSchedule("+eventdtllist.getEventId()+",0)'>"+Utility.getLocaleValuePropByKey("msgSelectSlot", locale)+"</a></td>");
				 }	
				dmRecords.append("</tr>");
			   }
			 				
			 dmRecords.append("</table>");
			// dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dmRecords.toString();
	
	}

}




