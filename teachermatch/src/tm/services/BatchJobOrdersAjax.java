package tm.services;


import java.io.File;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;


import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.BatchJobCertification;
import tm.bean.BatchJobOrder;
import tm.bean.JobCertification;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInBatchJob;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAssessmentAttempt;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.UserLoginHistory;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.assessment.AssessmentSections;
import tm.bean.master.CertificateNameMaster;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictNotes;
import tm.bean.master.DistrictSchools;
import tm.bean.master.ExclusivePeriodMaster;
import tm.bean.master.FieldOfStudyMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.menu.MenuMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.bean.user.UserMaster;
import tm.dao.BatchJobCertificationDAO;
import tm.dao.BatchJobOrderDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInBatchJobDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAssessmentAttemptDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.social.SocialService;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

import jsx3.net.Request;


public class BatchJobOrdersAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	public void setAssessmentDetailDAO(AssessmentDetailDAO assessmentDetailDAO) {
		this.assessmentDetailDAO = assessmentDetailDAO;
	}
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	
	@Autowired
	private TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO;
	public void setTeacherAssessmentAttemptDAO(
			TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO) {
		this.teacherAssessmentAttemptDAO = teacherAssessmentAttemptDAO;
	}
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	public void setJobCertificationDAO(JobCertificationDAO jobCertificationDAO) {
		this.jobCertificationDAO = jobCertificationDAO;
	}
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	@Autowired
	private BatchJobOrderDAO batchJobOrderDAO;
	public void setBatchJobOrderDAO(BatchJobOrderDAO batchJobOrderDAO) {
		this.batchJobOrderDAO = batchJobOrderDAO;
	}
	@Autowired
	private SchoolInBatchJobDAO schoolInBatchJobDAO;
	public void setSchoolInBatchJobDAO(SchoolInBatchJobDAO schoolInBatchJobDAO) {
		this.schoolInBatchJobDAO = schoolInBatchJobDAO;
	}
	@Autowired
	private BatchJobCertificationDAO batchJobCertificationDAO;
	public void setBatchJobCertificationDAO(BatchJobCertificationDAO batchJobCertificationDAO) {
		this.batchJobCertificationDAO = batchJobCertificationDAO;
	}
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private SocialService socialService;
	public void setSocialService(SocialService socialService) {
		this.socialService= socialService;
	}
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
	
	@Autowired 
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
	public void setCertificateTypeMasterDAO(
			CertificateTypeMasterDAO certificateTypeMasterDAO) {
		this.certificateTypeMasterDAO = certificateTypeMasterDAO;
	}
	public String  displaySchool(int batchJobId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String locale = Utility.getValueOfPropByKey("locale");
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgSessionExpired", locale));
	    }
		StringBuffer tmRecords =	new StringBuffer();
		try{
			UserMaster userMaster = null;
			SchoolMaster schoolMaster=null;
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,37,"addeditbatchjoborder.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			BatchJobOrder batchJobOrder =batchJobOrderDAO.findById(batchJobId,false,false);
			List<SchoolInBatchJob> listSchoolInBatchJob	= schoolInBatchJobDAO.findBatchJobOrder(batchJobOrder);
			
			if(listSchoolInBatchJob.size()!=0)
				tmRecords.append("<table  id='tblGridschool_list' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");					
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th>");
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblAddedOn", locale)+"</th>");
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblOfExpHi", locale)+"</th>");
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblActions", locale)+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			
			for (SchoolInBatchJob schoolInBatchJob : listSchoolInBatchJob) 
			{
				int noExpHires=0;
				if(schoolInBatchJob.getNoOfSchoolExpHires()!=null){
					noExpHires=schoolInBatchJob.getNoOfSchoolExpHires();
				}
				tmRecords.append("<tr>" );
				tmRecords.append("<td  nowrap >"+schoolInBatchJob.getSchoolId().getSchoolName()+"</td>");
				tmRecords.append("<td  >"+Utility.convertDateAndTimeToUSformatOnlyDate(schoolInBatchJob.getAcceptedDateTime())+"</td>");
				tmRecords.append("<td  >"+noExpHires+"</td>");
				if(schoolInBatchJob.isJobAccepted()==false){
					if(roleAccess.indexOf("|3|")!=-1){
						tmRecords.append("<td  nowrap><a href='javascript:void(0);' onclick='return deleteSchoolInBatchJob("+schoolInBatchJob.getBatchJobId().getBatchJobId()+","+schoolInBatchJob.getSchoolId().getSchoolId()+")'>"+Utility.getLocaleValuePropByKey("lblDelete", locale)+"</a></td>");
					}else{
						tmRecords.append("<td >&nbsp;</td>");
					}
				}else{
					tmRecords.append("<td  >&nbsp;</td>");
				}
				tmRecords.append("</tr>" );
			}
			if(listSchoolInBatchJob.size()!=0)
			tmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tmRecords.toString();
	}
	public String  displayCertification(int batchJobId){
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer tmRecords =	new StringBuffer();
		try{
			UserMaster userMaster = null;
			SchoolMaster schoolMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,37,"addeditbatchjoborder.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			BatchJobOrder batchJobOrder =batchJobOrderDAO.findById(batchJobId,false,false);
			List<BatchJobCertification> lstBatchJobCertification= null;
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			lstBatchJobCertification = batchJobCertificationDAO.findByCriteria(criterion1);		
			
			int certificationDivCount=0;
			int i=0;
			for (BatchJobCertification batchJobCertification : lstBatchJobCertification) 
			{	
				certificationDivCount++;
				//tmRecords.append("<div class=\"span4 inline\" id=\"CertificationGrid"+certificationDivCount+"\" nowrap><input type=\"hidden\" name=\"certificateTypeIds\" id=\"certificateTypeId"+i+"\" value=\""+batchJobCertification.getBatchJobCertificationId()+"\" ><label>"+batchJobCertification.getCertificateTypeMaster().getCertType()+" ");
				tmRecords.append("<div class=\"\" id=\"CertificationGrid"+i+"\" nowrap><input type=\"hidden\" name=\"certificateTypeIds\" id=\"certificateTypeId"+i+"\" value=\""+batchJobCertification.getBatchJobCertificationId()+"\" ><label name=\"certificateTypeText\">"+batchJobCertification.getCertificateTypeMaster().getCertType()+"" );
				if(roleAccess.indexOf("|3|")!=-1){
					tmRecords.append("<a href=\"javascript:void(0);\" onclick='return deleteCertification("+certificationDivCount+","+batchJobCertification.getBatchJobId().getBatchJobId()+","+batchJobCertification.getBatchJobCertificationId()+")'><img width=\"15\" height=\"15\"  class=\"can\" src=\"images/can-icon.png\"></a>" );
				}
				i++;
				tmRecords.append("</label></div>");
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tmRecords.toString();
	}
	public String showFile(String districtORSchoool,int districtORSchooolId,String docFileName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String path="";
		String source="";
		String target="";
		try 
		{
			if(districtORSchoool.equalsIgnoreCase("district")){
				source = Utility.getValueOfPropByKey("districtRootPath")+districtORSchooolId+"/"+docFileName;
				target = context.getServletContext().getRealPath("/")+"/"+"/district/"+districtORSchooolId+"/";
			}else{
				source = Utility.getValueOfPropByKey("schoolRootPath")+districtORSchooolId+"/"+docFileName;
				target = context.getServletContext().getRealPath("/")+"/"+"/school/"+districtORSchooolId+"/";
			}
		    File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        
	        FileUtils.copyFile(sourceFile, targetFile);
	  
	        if(districtORSchoool.equalsIgnoreCase("district")){
	        	  path = Utility.getBaseURL(request)+"/district/"+districtORSchooolId+"/"+docFileName; 	
	        }else{
	        	  path = Utility.getBaseURL(request)+"/school/"+districtORSchooolId+"/"+docFileName;
	        }
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return path;
	}
	public String displayRecordsByEntityType(boolean resultFlag,int JobOrderType,int districtId,int schoolId,String certifications,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }

		String locale = Utility.getValueOfPropByKey("locale");
		StringBuffer tmRecords =	new StringBuffer();
		try
		{
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------
			
			/** set default sorting fieldName **/
			String sortOrderFieldName="createdDateTime";
			String sortOrderNoField="batchJobId";
			boolean deafultFlag=false;
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("certName") && !sortOrder.equals("schoolJobOrder")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
					 deafultFlag=false;
				 }else{
					 deafultFlag=true;
				 }
				 if(sortOrder.equals("certName")){
					 deafultFlag=false;
					 sortOrderNoField="certName";
				 }
				 if(sortOrder.equals("schoolJobOrder")){
					 deafultFlag=false;
					 sortOrderNoField="schoolJobOrder";
				 }
			}
			String sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
			if(sortOrderType!=null)
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("1")){
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}
			
			/**End ------------------------------------**/
			
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				
				if(userMaster.getEntityType()==3){
					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
				}
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				entityID=userMaster.getEntityType();
			}
			
			String roleAccess=null;
			try{
				if(JobOrderType==3 && entityID==1){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,40,"batchschooljoborder.do",0);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,26,"batchjoborder.do",0);
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
		
			List<BatchJobOrder> lstBatchJobOrder	  =	 new ArrayList<BatchJobOrder>();
			
			if(schoolId!=0){
				schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
			}
			
			if(districtId!=0){
				districtMaster=districtMasterDAO.findById(districtId,false,false);
				Criterion criterion2= Restrictions.eq("districtMaster",districtMaster);
				if(schoolId!=0){
					if(entityID==3 || JobOrderType==3){
						Criterion criterion4= Restrictions.eq("jobStatus","T");
							lstBatchJobOrder = batchJobOrderDAO.findByCriteria(sortOrderStrVal,criterion2,criterion4);
					}else{
							lstBatchJobOrder = batchJobOrderDAO.findByCriteria(sortOrderStrVal,criterion2);
					}
				}else{
					if(entityID==3 || JobOrderType==3){
						Criterion criterion4= Restrictions.eq("jobStatus","T");
						lstBatchJobOrder = batchJobOrderDAO.findByCriteria(sortOrderStrVal,criterion2,criterion4);
					}else{
						lstBatchJobOrder = batchJobOrderDAO.findByCriteria(sortOrderStrVal,criterion2);
					}
				}
			}else{
				lstBatchJobOrder = batchJobOrderDAO.findByCriteria(sortOrderStrVal);
			}
			Map<Integer,SchoolInBatchJob> schoolInBatchJobList = schoolInBatchJobDAO.findSchoolInJobBySchoolAndJob();
			Map<Integer,BatchJobCertification> batchJobCertificationList = batchJobCertificationDAO.findCertificationTypeListByJob();
			
			
			String jcRecords="";
			List<BatchJobOrder> lstBatchJob=new ArrayList<BatchJobOrder>();
			if(sortOrderNoField.equals("certName")){
				sortOrderFieldName="certName";
			}
			if(sortOrderNoField.equals("schoolJobOrder")){
				sortOrderFieldName="schoolJobOrder";
			}
			lstBatchJob=getBatchJobList(sortOrderTypeVal,lstBatchJobOrder,sortOrder,sortOrderFieldName,schoolInBatchJobList,batchJobCertificationList,certifications,schoolMaster,JobOrderType,entityID,schoolId);
				
			totalRecord =lstBatchJob.size();
			System.out.println("totalRecord::::::::::::"+totalRecord);
				if(totalRecord<end)
					end=totalRecord;
				List<BatchJobOrder> batchJobLst=lstBatchJob.subList(start,end); 
				if(deafultFlag){
					sortOrderTypeVal="1";
					sortOrderNoField="createdDateTime";
				}
				tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				String responseText="";
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobOrder", locale),sortOrderNoField,"batchJobId",sortOrderTypeVal,pgNo);
				tmRecords.append("<th width='10%' valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCreatedOn", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
				tmRecords.append("<th width='12%' valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTitle", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
				tmRecords.append("<th width='23%' valign='top'>"+responseText+"</th>");
				
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCertificationName ", locale),sortOrderNoField,"certName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th width='15%'  valign='top'>"+responseText+"</th>");
				if(entityID==3 || JobOrderType==3){
					responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("optSchJoOtr", locale),sortOrderNoField,"schoolJobOrder",sortOrderTypeVal,pgNo);
					tmRecords.append("<th width='10%'  valign='top'>"+responseText+"</th>");
				}
				//tmRecords.append("<th  width='10%'  valign='top'>Accepted By</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblStatus", locale),sortOrderFieldName,"jobStatus",sortOrderTypeVal,pgNo);
				tmRecords.append("<th width='10%' valign='top'>"+responseText+"</th>");
				tmRecords.append(" <th  width='20%' valign='top'>"+Utility.getLocaleValuePropByKey("lblActions", locale)+"</th>");
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				if(resultFlag==false){
					totalRecord=0;
				}
				if(resultFlag){	
					if(batchJobLst.size()==0)
						tmRecords.append("<tr><td colspan='9' align='center'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
					
					for (BatchJobOrder batchJobOrder : batchJobLst){
						tmRecords.append("<tr>" );
						tmRecords.append("<td  width='10%' nowrap >"+batchJobOrder.getBatchJobId()+"</td>");
						tmRecords.append("<td  width='12%'>"+Utility.convertDateAndTimeToUSformatOnlyDate(batchJobOrder.getCreatedDateTime())+"</td>");
						tmRecords.append("<td  width='23%' nowrap >"+batchJobOrder.getJobTitle()+"</td>");
						
						//temporary value get Certification Type
						if(batchJobOrder.getExitURL()=="" || batchJobOrder.getExitURL()==null){
							tmRecords.append("<td  width='15%' nowrap >&nbsp;</td>");
						}else{
							tmRecords.append("<td nowrap  width='15%' >"+batchJobOrder.getExitURL()+"</td>");
						}
						if(entityID==3 || JobOrderType==3){
							tmRecords.append("<td  width='10%' style='text-align:center;'>");
							if(batchJobOrder.getExitMessage()!=null){
								//temporary value get CorrespondingSchoolJobOrder 
								tmRecords.append(batchJobOrder.getExitMessage());
							}else{
								tmRecords.append("");
							}
							tmRecords.append("&nbsp;</td>");
						}
						int isJobAcceptedCount =0;
						int schoolJobCont =0;
						try{
							isJobAcceptedCount=findJobAcceptedORHasSchool(schoolInBatchJobList,batchJobOrder,1);
							schoolJobCont=findJobAcceptedORHasSchool(schoolInBatchJobList,batchJobOrder,2);
						}catch(Exception e){}
						
						/*tmRecords.append("<td  width='10%' nowrap>");
						if(isJobAcceptedCount==0){
							tmRecords.append("0");
						}else{
							tmRecords.append("<a href='batchschoollist.do?batchJobId="+batchJobOrder.getBatchJobId()+"&type=1&JobOrderType=2'>"+isJobAcceptedCount+"</a>");
						}
						if(schoolJobCont==0){
							tmRecords.append(" : 0");
						}else{
							tmRecords.append(" : <a href='batchschoollist.do?batchJobId="+batchJobOrder.getBatchJobId()+"&type=2&JobOrderType=2'>"+schoolJobCont+"</a>");
						}
						tmRecords.append("</td>");*/
						
						boolean sentFlag=false;
						tmRecords.append("<td  width='10%'>");
						if(entityID==3 || JobOrderType==3){
							//temporary value get isAcceptStatus 
							if(batchJobOrder.getJobStatus().equalsIgnoreCase("P")){
								tmRecords.append(Utility.getLocaleValuePropByKey("lblPending", locale));
							}else if(batchJobOrder.getJobStatus().equalsIgnoreCase("a")){
								tmRecords.append(Utility.getLocaleValuePropByKey("lblAccepted", locale)); 
							}
						}else{
							if(batchJobOrder.getJobStatus().equalsIgnoreCase("s")){
								tmRecords.append(Utility.getLocaleValuePropByKey("lblSaved", locale));
							}else if(batchJobOrder.getJobStatus().equalsIgnoreCase("t")){
								tmRecords.append(Utility.getLocaleValuePropByKey("lblSent", locale)); sentFlag=true;
							}
						}
						tmRecords.append("</td>");
						tmRecords.append("<td  width='20%' nowrap>");
						boolean pipeFlag=false;
						if(entityID==3 || JobOrderType==3){
							if(!sentFlag && roleAccess.indexOf("|6|")!=-1){
								String AcceptView=Utility.getLocaleValuePropByKey("lblView", locale);
								if(batchJobOrder.getJobStatus().equalsIgnoreCase("P")){
									AcceptView=Utility.getLocaleValuePropByKey("lblAccept", locale);
								}
								//get temporary SchoolId through IpAddress
								tmRecords.append("<a href='editbatchjoborder.do?JobOrderType=3&batchJobId="+batchJobOrder.getBatchJobId()+"&schoolId="+batchJobOrder.getIpAddress()+"&entityID="+entityID+"'>"+AcceptView+"</a>");
								pipeFlag=true;
							}	
						}else{
							if(!sentFlag && roleAccess.indexOf("|2|")!=-1){
								tmRecords.append("<a href='editbatchjoborder.do?JobOrderType=2&batchJobId="+batchJobOrder.getBatchJobId()+"'>"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>");
								pipeFlag=true;
							}
						}
						if(JobOrderType==2 && entityID!=3 && roleAccess.indexOf("|12|")!=-1){
							if(pipeFlag)tmRecords.append(" | ");
							tmRecords.append("<a href='copybatchjoborder.do?JobOrderType=2&batchJobId="+batchJobOrder.getBatchJobId()+"'>"+Utility.getLocaleValuePropByKey("lnkCpy", locale)+"</a>");
						}
						
						if(JobOrderType==2 && !sentFlag && isJobAcceptedCount==0 && entityID!=3){
							if(roleAccess.indexOf("|3|")!=-1){
								if(pipeFlag)tmRecords.append(" | ");
								tmRecords.append("<a href='javascript:void(0);' onclick='return deleteBatchJobOrder("+batchJobOrder.getBatchJobId()+","+1+")'>"+Utility.getLocaleValuePropByKey("lblDelete", locale)+"</a>");
							}
						}
						tmRecords.append("</td>");
						tmRecords.append("</tr>" );
						
					}
				}else{
					tmRecords.append("<tr><td colspan='9' align='center'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
				}
				tmRecords.append("</table>");
				System.out.println("totalRecord"+totalRecord);
				tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
				try{
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Search Batch Job Order");
				}catch(Exception e){
					e.printStackTrace();
				}
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tmRecords.toString();
	}
	public List<BatchJobOrder> getBatchJobList(String sortOrderTypeVal,List<BatchJobOrder> lstBatchJobOrder,String sortOrder,String sortOrderFieldName,Map<Integer,SchoolInBatchJob> schoolInBatchJobList,Map<Integer,BatchJobCertification> batchJobCertificationList,String certifications,SchoolMaster schoolMaster,int JobOrderType, int entityID,int schoolId){
		List<BatchJobOrder> lstBatchJob=new ArrayList<BatchJobOrder>();
		String	jcRecords=""; 
		int mapFlag=1;
		int mapFlagForCart=0;
		Map<String,BatchJobOrder> map = new LinkedHashMap<String,BatchJobOrder>();
		SortedMap<String,BatchJobOrder>	sortedMap = new TreeMap<String,BatchJobOrder>();
		SortedMap<Integer,BatchJobOrder>	sortedIntMap = new TreeMap<Integer,BatchJobOrder>();
		SortedMap	sortedMapCommon=null; 
		
		if(sortOrder!=null)
		if(!sortOrder.equals("") && !sortOrder.equals(null)){
			mapFlag=0;
		}else{
			mapFlag=1;
		}
		int sortingDynaVal=99999;
		int orderFieldInt=0;
		try{
			for (BatchJobOrder batchJobOrder : lstBatchJobOrder){
				
				jcRecords=""; 
				SchoolInBatchJob schoolInBatchJob =findSchoolInJobBySchoolAndJob(schoolInBatchJobList,batchJobOrder,schoolMaster);
				jcRecords =findCertificationTypeListByJob(batchJobCertificationList,batchJobOrder, certifications);
				String schoolJobOrderCount="";
				if(schoolInBatchJob!=null){
					if(schoolInBatchJob.getCorrespondingSchoolJobOrder()!=null)
						schoolJobOrderCount=schoolInBatchJob.getCorrespondingSchoolJobOrder()+"";
				}
				String orderFieldName=batchJobOrder.getCreatedDateTime()+"||";
				if(sortOrder!=null){
					if(sortOrderFieldName.equals("batchJobId"))
						orderFieldName=batchJobOrder.getBatchJobId()+"";
					
					if(sortOrderFieldName.equals("schoolJobOrder")){
						if(entityID==3 || JobOrderType==3){
							sortingDynaVal--;
							orderFieldInt=Integer.parseInt(schoolJobOrderCount+""+sortingDynaVal);
						}else{
							orderFieldInt=Integer.parseInt(sortingDynaVal+"");
						}
						mapFlagForCart=1;
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}else if(sortOrderFieldName.equals("certName")){
						orderFieldName=jcRecords.toUpperCase()+orderFieldName;
						mapFlagForCart=1;
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}else if(sortOrderFieldName.equals("createdDateTime"))
						orderFieldName=batchJobOrder.getCreatedDateTime()+"";
					
					if(sortOrderFieldName.equals("jobTitle"))
						orderFieldName=batchJobOrder.getJobTitle();
					
					if(sortOrderFieldName.equals("jobStatus")){
						orderFieldName+=batchJobOrder.getJobStatus();
						if(sortOrderTypeVal.equals("0")){
							mapFlag=1;
						}
					}
				}
					if(!((jcRecords==null || jcRecords=="") && !certifications.equals(""))){
						
					//temporary value set Certification Type
					if(jcRecords!=""){
						batchJobOrder.setExitURL(jcRecords);
					}else{
						batchJobOrder.setExitURL(null);
					}
					
					if(schoolInBatchJob==null){
						if(JobOrderType==2 && entityID!=3 && schoolId==0){
							if(batchJobOrder.getJobStatus().equalsIgnoreCase("s")){
								if(mapFlagForCart==1){
									if(sortingDynaVal<99999){
										sortedIntMap.put(orderFieldInt, batchJobOrder);
									}else{
										sortedMap.put(orderFieldName, batchJobOrder);
									}
								}else if(mapFlag==0)
									map.put("1||"+orderFieldName, batchJobOrder);
								else 
									sortedMap.put("1||"+orderFieldName, batchJobOrder);
							}else if(batchJobOrder.getJobStatus().equalsIgnoreCase("t")){
								if(mapFlagForCart==1){
									if(sortingDynaVal<99999){
										sortedIntMap.put(orderFieldInt, batchJobOrder);
									}else{
										sortedMap.put(orderFieldName, batchJobOrder);
									}
								}else if(mapFlag==0)
									map.put("0||"+orderFieldName, batchJobOrder);
								else
									sortedMap.put("0||"+orderFieldName, batchJobOrder);
							}
						}
					}else{
						//temporary value set SchoolId
						batchJobOrder.setIpAddress(schoolInBatchJob.getSchoolId().getSchoolId()+"");
						if(entityID==3 || JobOrderType==3){
							if(schoolInBatchJob!=null){
								//temporary value set CorrespondingSchoolJobOrder
								batchJobOrder.setExitMessage(schoolJobOrderCount+"");
							}else{
								batchJobOrder.setExitMessage(null);
							}
						}
						
						if(schoolInBatchJob.isAcceptStatus()){
							if(batchJobOrder.getJobStatus().equalsIgnoreCase("s")){
								if(entityID==3 || JobOrderType==3){
									//temporary value set isAcceptStatus
									batchJobOrder.setJobStatus("A");
								}
								if(mapFlagForCart==1){
									if(sortingDynaVal<99999){
										sortedIntMap.put(orderFieldInt, batchJobOrder);
									}else{
										sortedMap.put(orderFieldName, batchJobOrder);
									}
								}else if(mapFlag==0)
									map.put("1||"+0+"||+"+orderFieldName, batchJobOrder);
								else 
									sortedMap.put("1||"+0+"||+"+orderFieldName, batchJobOrder);
							}else if(batchJobOrder.getJobStatus().equalsIgnoreCase("t")){
								if(entityID==3 || JobOrderType==3){
									//temporary value set isAcceptStatus
									batchJobOrder.setJobStatus("A");
								}
								if(mapFlagForCart==1){
									if(sortingDynaVal<99999){
										sortedIntMap.put(orderFieldInt, batchJobOrder);
									}else{
										sortedMap.put(orderFieldName, batchJobOrder);
									}
								}else if(mapFlag==0)
									map.put("0||"+0+"||+"+orderFieldName, batchJobOrder);
								else 
									sortedMap.put("0||"+0+"||+"+orderFieldName, batchJobOrder);
							}
						}else{
							if(batchJobOrder.getJobStatus().equalsIgnoreCase("s")){
								if(entityID==3 || JobOrderType==3){
									//temporary value set isAcceptStatus
									batchJobOrder.setJobStatus("P");
								}
								if(mapFlagForCart==1){
									if(sortingDynaVal<99999){
										sortedIntMap.put(orderFieldInt, batchJobOrder);
									}else{
										sortedMap.put(orderFieldName, batchJobOrder);
									}
								}else if(mapFlag==0)
									map.put("1||"+1+"||+"+orderFieldName, batchJobOrder);
								else
									sortedMap.put("1||"+1+"||+"+orderFieldName, batchJobOrder);	
							}else if(batchJobOrder.getJobStatus().equalsIgnoreCase("t")){
								if(entityID==3 || JobOrderType==3){
									//temporary value set isAcceptStatus
									batchJobOrder.setJobStatus("P");
								}
								if(mapFlagForCart==1){
									if(sortingDynaVal<99999){
										sortedIntMap.put(orderFieldInt, batchJobOrder);
									}else{
										sortedMap.put(orderFieldName, batchJobOrder);
									}
								}else if(mapFlag==0)
									map.put("0||"+1+"||+"+orderFieldName, batchJobOrder);
								else 
									sortedMap.put("0||"+1+"||+"+orderFieldName, batchJobOrder);
							}
							
						}
					}
				}
			}
			if(sortingDynaVal<99999){
				sortedMapCommon=sortedIntMap;
			}else{
				sortedMapCommon=sortedMap;
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMapCommon ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
				    lstBatchJob.add((BatchJobOrder) sortedMapCommon.get(key));
				} 
			}else if(mapFlagForCart==1){
				Iterator iterator = sortedMapCommon.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					lstBatchJob.add((BatchJobOrder) sortedMapCommon.get(key));
				}
			}else{
				Iterator iterator = map.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					lstBatchJob.add((BatchJobOrder) map.get(key));
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return lstBatchJob;

	}
	public String displaySchoolList(int batchJobId,int type)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer tmRecords =	new StringBuffer();
		System.out.println("displaySchoolList::::::::");
		try
		{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
		
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				entityID=userMaster.getEntityType();
			}
			BatchJobOrder batchJobOrder=batchJobOrderDAO.findById(batchJobId,false,false);
			List<SchoolMaster> schoolMasterlst=new ArrayList<SchoolMaster>();
			SortedMap map = new TreeMap();
			try{
				List<SchoolMaster> schoolMasters = schoolInBatchJobDAO.findJobAcceptedORHasSchool(batchJobOrder,type);
				for(int i=0;i<schoolMasters.size();i++){
					map.put(schoolMasters.get(i).getSchoolName(),schoolMasters.get(i));
				}
				Iterator iterator = map.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					schoolMasterlst.add((SchoolMaster) map.get(key));
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
				tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				tmRecords.append("<th width='25%'>"+Utility.getLocaleValuePropByKey("lblNameOfSchool", locale)+"</th>");
				tmRecords.append("<th width='25%'>"+Utility.getLocaleValuePropByKey("optDistrict", locale)+"</th>");
				tmRecords.append("<th width='50%'>"+Utility.getLocaleValuePropByKey("lblAdd", locale)+"</th>");
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				
				
				if(schoolMasterlst.size()==0){
						tmRecords.append("<tr><td colspan='9' align='center'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
				}
				for (SchoolMaster schoolMaster : schoolMasterlst){
					tmRecords.append("<tr>" );
					tmRecords.append("<td  width='25%' nowrap >"+schoolMaster.getSchoolName()+"</td>");
					tmRecords.append("<td  width='25%'>"+schoolMaster.getDistrictId().getDistrictName()+"</td>");
					tmRecords.append("<td  width='50%' nowrap >"+schoolMaster.getAddress()+"</td>");
					tmRecords.append("</tr>" );
				}
				tmRecords.append("</table>");
				tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
				tmRecords.append("<tr><td colspan='3'><a href=\"batchjoborder.do\"><b>Back</b></a></td></tr>" );
				tmRecords.append("</table>");
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tmRecords.toString();
	}
	public int findJobAcceptedORHasSchool(Map<Integer,SchoolInBatchJob> schoolInBatchJobList,BatchJobOrder batchJobOrder,int sizeType) 
	{
		int schoolJobCount=0;
		for(int i=0;i<schoolInBatchJobList.size();i++){
			if(sizeType==1){
				if(schoolInBatchJobList.get(i).isAcceptStatus()==true && schoolInBatchJobList.get(i).getBatchJobId().getBatchJobId()==batchJobOrder.getBatchJobId()){
					schoolJobCount++;
				}
			}else{
				if(schoolInBatchJobList.get(i).getBatchJobId().getBatchJobId()==batchJobOrder.getBatchJobId()){
					schoolJobCount++;
				}
			}
		}
		return schoolJobCount;
	}
	public SchoolInBatchJob findSchoolInJobBySchoolAndJob(Map<Integer,SchoolInBatchJob> schoolInBatchJobList,BatchJobOrder batchJobOrder, SchoolMaster schoolMaster) 
	{
		SchoolInBatchJob schoolInBatchJob=null;
		for(int i=0;i<schoolInBatchJobList.size();i++){
			if(schoolInBatchJobList.get(i).getSchoolId().equals(schoolMaster) && schoolInBatchJobList.get(i).getBatchJobId().getBatchJobId()==batchJobOrder.getBatchJobId()){
				schoolInBatchJob=schoolInBatchJobList.get(i);
			}
		}
		return schoolInBatchJob;
	}
	
	public String findCertificationTypeListByJob(Map<Integer,BatchJobCertification> batchJobCertificationList,BatchJobOrder batchJobOrder, String CertificationType) 
	{
		if(CertificationType!=null && !CertificationType.equals(""))
		CertificationType= (CertificationType.substring(0,CertificationType.lastIndexOf("["))).trim();
		//System.out.println(" kkkkkkkkkkkk CertificationType: "+CertificationType);
		BatchJobCertification batchJobCertification=null;
		boolean CertificationTypeFlag=false;
		String  jcRecords= "";
		int iCertification=0;
		if(!CertificationType.equals("")){
			CertificationTypeFlag=true;
		}
/*		for(int i=0;i<batchJobCertificationList.size();i++){
			if(CertificationTypeFlag && batchJobCertificationList.get(i).getBatchJobId().getBatchJobId()==batchJobOrder.getBatchJobId()){
				if(batchJobCertificationList.get(i).getCertType().indexOf(CertificationType.trim())!=-1){
					batchJobCertification=batchJobCertificationList.get(i);
					if(iCertification!=0){
						jcRecords+=", ";
					}
					jcRecords+=batchJobCertification.getCertType();
					iCertification++;
				}
			}else if(CertificationTypeFlag==false && batchJobCertificationList.get(i).getBatchJobId().getBatchJobId()==batchJobOrder.getBatchJobId()){
				batchJobCertification=batchJobCertificationList.get(i);
				if(iCertification!=0){
					jcRecords+=", ";
				}
				jcRecords+=batchJobCertification.getCertType();
				iCertification++;
			}
		}*/
		
		for(int i=0;i<batchJobCertificationList.size();i++){
			if(CertificationTypeFlag && batchJobCertificationList.get(i).getBatchJobId().getBatchJobId()==batchJobOrder.getBatchJobId()){
				CertificateTypeMaster certificateTypeMaster = batchJobCertificationList.get(i).getCertificateTypeMaster();
				if(certificateTypeMaster!=null && certificateTypeMaster.getCertType().indexOf(CertificationType.trim())!=-1){
					batchJobCertification=batchJobCertificationList.get(i);
					if(iCertification!=0){
						jcRecords+=", ";
					}
					if(batchJobCertification.getCertificateTypeMaster()!=null)
					jcRecords+=batchJobCertification.getCertificateTypeMaster().getCertType();
					iCertification++;
				}
			}else if(CertificationTypeFlag==false && batchJobCertificationList.get(i).getBatchJobId().getBatchJobId()==batchJobOrder.getBatchJobId()){
				batchJobCertification=batchJobCertificationList.get(i);
				if(iCertification!=0){
					jcRecords+=",";
				}
				if(batchJobCertification.getCertificateTypeMaster()!=null)
				jcRecords+=batchJobCertification.getCertificateTypeMaster().getCertType();
				iCertification++;
			}
		}
		return jcRecords;
	}
		
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int checkDuplicateCertification(int batchJobId,String certType) 
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<BatchJobCertification> lstBatchJobCertification= null;
		try{
			BatchJobOrder  batchJobOrder = batchJobOrderDAO.findById(batchJobId, false, false);
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			//Criterion criterion2 = Restrictions.eq("certType",certType.trim());
			
			CertificateTypeMaster certificateTypeMaster = new CertificateTypeMaster();
			certificateTypeMaster.setCertTypeId(Integer.parseInt(certType));
			Criterion criterion2 = Restrictions.eq("certificateTypeMaster",certificateTypeMaster);
			lstBatchJobCertification = batchJobCertificationDAO.findByCriteria(criterion1,criterion2);	
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		return lstBatchJobCertification.size();

	}
	public int addCertification(int jobId,String certType,Long stateId )
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		int exitFlag=0;
		try{
			exitFlag=checkDuplicateCertification(jobId,certType);
			if(exitFlag==0){
				//System.out.println("add Batch Certification:::");
				BatchJobCertification batchJobCertification =new BatchJobCertification();
				BatchJobOrder  batchJobOrder = batchJobOrderDAO.findById(jobId, false, false);
				StateMaster stateMaster=stateMasterDAO.findById(stateId, false, false);
				batchJobCertification.setBatchJobId(batchJobOrder);
				//batchJobCertification.setCertType(certType);
				System.out.println("stateId :"+stateMaster.getStateName());
				System.out.println("certType :"+certType);
				CertificateTypeMaster certificateTypeMaster = new CertificateTypeMaster();
				certificateTypeMaster.setCertTypeId(Integer.parseInt(certType));
				batchJobCertification.setCertificateTypeMaster(certificateTypeMaster);
				batchJobCertification.setStateMaster(stateMaster);
				batchJobCertificationDAO.makePersistent(batchJobCertification);
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
			return 0;
		}

		return exitFlag;
	}
	@Transactional(readOnly=false)
	public String addSchoolJobOrder(int jobCategoryId,String schoolBatchId,boolean  pkOffered,boolean  kgOffered,boolean  g01Offered,boolean  g02Offered,boolean  g03Offered,boolean  g04Offered,boolean  g05Offered,boolean  g06Offered,boolean  g07Offered,boolean  g08Offered,boolean  g09Offered,boolean  g10Offered,boolean  g11Offered,boolean  g12Offered,int btnVal,int batchJobId,int districtHiddenId,int exitURLMessageVal,String exitMessage,String exitURL,String certificationType,String schoolVal,int JobOrderType, int districtOrSchooHiddenlId,String  jobTitle,String  jobDescription,String  jobQualification,int  noOfExpHires,String  jobStartDate,String  jobEndDate,int  allSchoolGradeDistrictVal,int  isJobAssessment,int  attachJobAssessment,String  assessmentDocument) 
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String JobPostingURL="";
		try{
			UserMaster userMaster = null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			int entityID=0;
			int createdBy=0;
			int returnJobId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				entityID=userMaster.getEntityType();
				createdBy=userMaster.getUserId();
			}
			if(districtHiddenId!=0){
				districtMaster =districtMasterDAO.findById(districtHiddenId, false, false);
			}
			if(entityID==3){
				if(userMaster.getSchoolId()!=null){
					schoolMaster =schoolMasterDAO.findById(Long.parseLong(userMaster.getSchoolId().getSchoolId()+""), false, false);
				}
			}
			if(entityID==1 && JobOrderType==3){
				if(schoolBatchId!=null){
					schoolMaster =schoolMasterDAO.findById(Long.parseLong(schoolBatchId), false, false);
				}
			}
			
			int jobId=0;
			JobOrder jobOrder=null;
			jobOrder=new JobOrder();
			
			
			jobOrder.setDistrictMaster(districtMaster);
			
			if(jobTitle!=null)
			jobOrder.setJobTitle(jobTitle);
			
			if(jobStartDate!=null)
				jobOrder.setJobStartDate(Utility.getCurrentDateFormart(jobStartDate));
			
			if(jobEndDate!=null)
				jobOrder.setJobEndDate(Utility.getCurrentDateFormart(jobEndDate));
			
			if(jobDescription!=null)
			jobOrder.setJobDescription(jobDescription);
			
			if(jobQualification!=null)
				jobOrder.setJobQualification(jobQualification);
			
			if(allSchoolGradeDistrictVal==1){
				jobOrder.setAllSchoolsInDistrict(1);
				jobOrder.setAllGrades(2);
				jobOrder.setPkOffered(2);
				jobOrder.setKgOffered(2);
				jobOrder.setG01Offered(2);
				jobOrder.setG02Offered(2);
				jobOrder.setG03Offered(2);
				jobOrder.setG04Offered(2);
				jobOrder.setG05Offered(2);
				jobOrder.setG06Offered(2);
				jobOrder.setG07Offered(2);
				jobOrder.setG08Offered(2);
				jobOrder.setG09Offered(2);
				jobOrder.setG10Offered(2);
				jobOrder.setG11Offered(2);
				jobOrder.setG12Offered(2);
				jobOrder.setSelectedSchoolsInDistrict(2);
				jobOrder.setNoSchoolAttach(2);
				
			}
			if(allSchoolGradeDistrictVal==2){
				jobOrder.setAllGrades(1);
				if(pkOffered)
					jobOrder.setPkOffered(1);
				else
					jobOrder.setPkOffered(2);
				
				if(kgOffered)
					jobOrder.setKgOffered(1);
				else
					jobOrder.setKgOffered(2);
				
				if(g01Offered)
					jobOrder.setG01Offered(1);
				else
					jobOrder.setG01Offered(2);
				
				if(g02Offered)
					jobOrder.setG02Offered(1);
				else 
					jobOrder.setG02Offered(2);
				
				if(g03Offered)
					jobOrder.setG03Offered(1);
				else 
					jobOrder.setG03Offered(2);
				
				if(g04Offered)
					jobOrder.setG04Offered(1);
				else 
					jobOrder.setG04Offered(2);
	
				if(g05Offered)
					jobOrder.setG05Offered(1);
				else 
					jobOrder.setG05Offered(2);
				
				if(g06Offered)
					jobOrder.setG06Offered(1);
				else 
					jobOrder.setG06Offered(2);
				
				if(g07Offered)
					jobOrder.setG07Offered(1);
				else 
					jobOrder.setG07Offered(2);
				
				if(g08Offered)
					jobOrder.setG08Offered(1);
				else 
					jobOrder.setG08Offered(2);
				
				if(g09Offered)
					jobOrder.setG09Offered(1);
				else 
					jobOrder.setG09Offered(2);
				
				if(g10Offered)
					jobOrder.setG10Offered(1);
				else 
					jobOrder.setG10Offered(2);
				
				if(g11Offered)
					jobOrder.setG11Offered(1);
				else 
					jobOrder.setG11Offered(2);
				
				if(g12Offered)
					jobOrder.setG12Offered(1);
				else 
					jobOrder.setG12Offered(2);
				
				jobOrder.setSelectedSchoolsInDistrict(2);
				jobOrder.setAllSchoolsInDistrict(2);
				jobOrder.setNoSchoolAttach(2);
			}
			if(allSchoolGradeDistrictVal==3){
				jobOrder.setAllGrades(2);
				jobOrder.setAllSchoolsInDistrict(2);
				jobOrder.setAllGrades(2);
				jobOrder.setPkOffered(2);
				jobOrder.setKgOffered(2);
				jobOrder.setG01Offered(2);
				jobOrder.setG02Offered(2);
				jobOrder.setG03Offered(2);
				jobOrder.setG04Offered(2);
				jobOrder.setG05Offered(2);
				jobOrder.setG06Offered(2);
				jobOrder.setG07Offered(2);
				jobOrder.setG08Offered(2);
				jobOrder.setG09Offered(2);
				jobOrder.setG10Offered(2);
				jobOrder.setG11Offered(2);
				jobOrder.setG12Offered(2);
				jobOrder.setSelectedSchoolsInDistrict(1);
				jobOrder.setNoSchoolAttach(2);
				jobOrder.setNoOfExpHires(null);
			}
				
			if(isJobAssessment==1){
				jobOrder.setIsJobAssessment(true);
			}else{
				jobOrder.setIsJobAssessment(false);
			}
			if(isJobAssessment==1 && attachJobAssessment==2){
				
				if(!assessmentDocument.equals(""))
				jobOrder.setAssessmentDocument(assessmentDocument);
				
				jobOrder.setAttachNewPillar(1);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultSchoolPillar(2);
			}else if(isJobAssessment==1 && attachJobAssessment==1){
				jobOrder.setAssessmentDocument(null);
				jobOrder.setAttachNewPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(1);
				jobOrder.setAttachDefaultSchoolPillar(2);
			}else if(isJobAssessment==1 && attachJobAssessment==3){
				jobOrder.setAssessmentDocument(null);
				jobOrder.setAttachNewPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultSchoolPillar(1);
			}else if(isJobAssessment==2){
				jobOrder.setAttachNewPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultSchoolPillar(2);
			}
			
			if(exitURLMessageVal==1){
				jobOrder.setExitURL(exitURL);
				jobOrder.setFlagForURL(1);
				jobOrder.setFlagForMessage(2);
			}else if(exitURLMessageVal==2){
				jobOrder.setExitMessage(exitMessage);
				jobOrder.setFlagForMessage(1);
				jobOrder.setFlagForURL(2);
			}
			try{
				JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				jobOrder.setJobCategoryMaster(jobCategoryMaster);
			}catch (Exception e) {
				// TODO: handle exception
				JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(1, false, false);
				jobOrder.setJobCategoryMaster(jobCategoryMaster);
			}
			
			jobOrder.setJobStatus("O");
			jobOrder.setStatus("A");
			jobOrder.setCreatedBy(createdBy);
			jobOrder.setCreatedByEntity(entityID);
			jobOrder.setCreatedForEntity(JobOrderType);
			jobOrder.setBatchJobId(batchJobId);
			
			if(schoolMaster.getIsPortfolioNeeded())
				jobOrder.setIsPortfolioNeeded(true);
			else
				jobOrder.setIsPortfolioNeeded(false);
			
			
			jobOrder.setNotificationToschool(true);
			jobOrderDAO.makePersistent(jobOrder);
			returnJobId=jobOrder.getJobId();
			
			//*********Check and Insert AssessmentJobRelation ********//  
			AssessmentJobRelation  assessmentJobRelation=null;
			assessmentJobRelation=assessmentJobRelationDAO.findRelationExistByJobOrder(jobOrder);
			if(attachJobAssessment!=2  && isJobAssessment==1){
				AssessmentDetail assessmentDetail=null;
				if(attachJobAssessment==1){
					assessmentDetail=assessmentDetailDAO.checkIsDefault(districtMaster, schoolMaster,1,null);
				}else{
					assessmentDetail=assessmentDetailDAO.checkIsDefault(districtMaster, schoolMaster,2,null);
				}
				if(assessmentDetail!=null){
					if(assessmentJobRelation==null){
						assessmentJobRelation= new AssessmentJobRelation();
						String ipAddress = IPAddressUtility.getIpAddress(request);
						assessmentJobRelation.setJobId(jobOrder);
						assessmentJobRelation.setCreatedBy(userMaster);
						assessmentJobRelation.setAssessmentId(assessmentDetail);
						assessmentJobRelation.setStatus("A");
						assessmentJobRelation.setIpaddress(ipAddress);
						assessmentJobRelationDAO.makePersistent(assessmentJobRelation);
					}else{
						String ipAddress = IPAddressUtility.getIpAddress(request);
						assessmentJobRelation.setJobId(jobOrder);
						assessmentJobRelation.setCreatedBy(userMaster);
						assessmentJobRelation.setAssessmentId(assessmentDetail);
						assessmentJobRelation.setStatus("A");
						assessmentJobRelation.setIpaddress(ipAddress);
						assessmentJobRelationDAO.updatePersistent(assessmentJobRelation);
					}
				}
			}else{
				if(assessmentJobRelation!=null){
					try{
						assessmentJobRelationDAO.makeTransient(assessmentJobRelation);
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			
			if(returnJobId!=0){
				JobPostingURL=Utility.getShortURL(Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+returnJobId);
			}
			if(certificationType!="" && !certificationType.equalsIgnoreCase("")){
				try{
					String certificationTypes[]=certificationType.split(",");
					for(int i=0;i<certificationTypes.length;i++){
						JobCertification jobCertification= new JobCertification();
						String certificationTypeVal="";
							if(certificationTypes[i].contains(",")){
								certificationTypeVal=certificationTypes[i].replace("<|>",",");
							}else{
								certificationTypeVal=certificationTypes[i];
							}
							jobCertification.setJobId(jobOrder);
							jobCertification.setCertType(certificationTypeVal);
							jobCertificationDAO.makePersistent(jobCertification);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			try{
				BatchJobOrder batchJobOrder=batchJobOrderDAO.findById(batchJobId,false, false);
				Criterion criterion1=Restrictions.eq("batchJobId",batchJobOrder);
				Criterion criterion2=Restrictions.eq("schoolId",schoolMaster);
				try{
					List<SchoolInBatchJob> schoolInBatchJob = schoolInBatchJobDAO.findByCriteria(criterion1,criterion2);
					schoolInBatchJob.get(0).setAcceptStatus(true);
					schoolInBatchJob.get(0).setCorrespondingSchoolJobOrder(returnJobId);
					schoolInBatchJobDAO.updatePersistent(schoolInBatchJob.get(0));
				}catch(Exception e){
					e.printStackTrace();
				}

				List<SchoolInBatchJob> schoolInBatchJob=schoolInBatchJobDAO.findByCriteria(criterion1,criterion2);
				SchoolInJobOrder schoolInJobOrder= new SchoolInJobOrder();
				schoolInJobOrder.setJobId(jobOrder);
				schoolInJobOrder.setSchoolId(schoolMaster);
				if(noOfExpHires!=0){
					schoolInJobOrder.setNoOfSchoolExpHires(noOfExpHires);
				}else{
					schoolInJobOrder.setNoOfSchoolExpHires(schoolInBatchJob.get(0).getNoOfSchoolExpHires());
				}
				schoolInJobOrder.setCreatedDateTime(new Date());
				schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			boolean jsaFlag=false;
			String nameOfEntity=null;
			String aUploadUrl=null;
			if(jobOrder.getIsJobAssessment()){
				jsaFlag=true;
			}
			if(jobOrder.getIsJobAssessment()){
				jsaFlag=true;
			}
			List<SchoolMaster> schoolIds=schoolInJobOrderDAO.findSchoolIds(jobOrder);
			if(isJobAssessment==1){
				if(attachJobAssessment==1){
					try{
						nameOfEntity=jobOrder.getDistrictMaster().getDistrictName();
					}catch(Exception e){}
					try{
						aUploadUrl=Utility.getBaseURL(request)+"showFile.do?type=2&id="+jobOrder.getDistrictMaster().getDistrictId()+"&file="+jobOrder.getDistrictMaster().getAssessmentUploadURL();
					}catch(Exception e){}
				}else if(attachJobAssessment==3){
					try{
						nameOfEntity=schoolMaster.getSchoolName();
					}catch(Exception e){}
					try{
						aUploadUrl=Utility.getBaseURL(request)+"showFile.do?type=3&id="+schoolMaster.getSchoolId()+"&file="+schoolMaster.getAssessmentUploadURL();
					}catch(Exception e){}
				}else if(attachJobAssessment==2){
					if(JobOrderType==2){
						try{
							nameOfEntity=jobOrder.getDistrictMaster().getDistrictName();
						}catch(Exception e){}
						try{
							aUploadUrl=Utility.getBaseURL(request)+"showFile.do?type=2&id="+jobOrder.getDistrictMaster().getDistrictId()+"&file="+jobOrder.getAssessmentDocument();
						}catch(Exception e){}
					}else{
						try{
							nameOfEntity=schoolMaster.getSchoolName();
						}catch(Exception e){}
						try{
							aUploadUrl=Utility.getBaseURL(request)+"showFile.do?type=3&id="+schoolMaster.getSchoolId()+"&file="+jobOrder.getAssessmentDocument();
						}catch(Exception e){}
					}
				}
			}
			List<SchoolInJobOrder> schoolJobOrderLst= null;
			schoolJobOrderLst = schoolInJobOrderDAO.findJobOrder(jobOrder);		
			int ischoolJobOrder=0;
			String schoolsRecords ="";
			for (SchoolInJobOrder schoolInJobOrder : schoolJobOrderLst){
				if(ischoolJobOrder!=0){
					schoolsRecords+=", ";
				}
				schoolsRecords+=schoolInJobOrder.getSchoolId().getSchoolName();
				ischoolJobOrder++;
			}
			if(jobId==0){
				
				String subject="";
				if(userMaster.getEntityType()==1 && JobOrderType==2){
					subject= Utility.getLocaleValuePropByKey("msgBatchJobOrder1", locale)+" ( "+userMaster.getFirstName()+" ) "+userMaster.getEmailAddress();		
				}else if(userMaster.getEntityType()==1 && JobOrderType==3){
					subject= Utility.getLocaleValuePropByKey("msgBatchJobOrder2", locale)+" ( "+userMaster.getFirstName()+" ) "+userMaster.getEmailAddress();	
				}else if(userMaster.getEntityType()==2){
					subject= Utility.getLocaleValuePropByKey("msgBatchJobOrder3", locale)+" ( "+userMaster.getFirstName()+" ) "+userMaster.getEmailAddress();	
				}else if(userMaster.getEntityType()==3){
					subject= Utility.getLocaleValuePropByKey("msgBatchJobOrder4", locale)+" ( "+userMaster.getFirstName()+" ) "+userMaster.getEmailAddress();	
				}
				List<UserMaster> lstUserMaster= null;
				if(noOfExpHires!=0){
					jobOrder.setNoOfExpHires(noOfExpHires);
				}
				if(jsaFlag){
					String sSupport= Utility.getLocaleValuePropByKey("msgBatchJobOrder5", locale)+""+jobOrder.getJobId()+". ";
					Properties  properties = new Properties();    		
		    		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("smtpmail.properties");
		    		properties.load(inputStream);
		    		mailSendByThread(JobOrderType,nameOfEntity,schoolsRecords,aUploadUrl,3,userMaster,properties.getProperty("smtphost.clientservices"),sSupport,request, jobOrder);
				}
			}
			///post On Social Network
			ManageJobOrdersAjax mJobAjax= new ManageJobOrdersAjax();
			if(Utility.getIndiaFormatDate(jobOrder.getJobStartDate()).equals(Utility.getIndiaFormatDate(new Date()))){
				mJobAjax.postOnSocialNetwork(socialService,JobOrderType,districtMaster,schoolMaster,Utility.getBaseURL(request),jobOrder,JobPostingURL);
			}
			try{
				UserLoginHistory userLoginHistory = new UserLoginHistory();
				userLoginHistory.setUserMaster(userMaster);
				userLoginHistory.setSessionId(session.getId());
				userLoginHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
				userLoginHistory.setLogTime(new Date());
				if(JobOrderType==2){
					if(districtMaster!=null)
					userLoginHistory.setDistrictId(districtMaster);
					
					if(jobId==0)
						userLoginHistory.setLogType("Created District Job Order");
					else
						userLoginHistory.setLogType("Edited District Job Order");
				}else{
					if(schoolMaster!=null)
					userLoginHistory.setSchoolId(schoolMaster);
					
					if(jobId==0)
						userLoginHistory.setLogType("Created School Job Order");
					else
						userLoginHistory.setLogType("Edited School Job Order");
				}
				userLoginHistoryDAO.makePersistent(userLoginHistory);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			try{
				String logType="";
				if(JobOrderType==2){
					if(districtMaster!=null)
					userMaster.setDistrictId(districtMaster);
					
					if(jobId==0)
						logType="Created District Job Order";
					else
						logType="Edited District Job Order";
				}else{
					if(districtMaster!=null)
						userMaster.setDistrictId(districtMaster);
					
					/*if(schoolMaster!=null)
					userMaster.setSchoolId(schoolMaster);*/
					
					if(jobId==0)
						logType="Created School Job Order";
					else
						logType="Edited School Job Order";
				}
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				e.printStackTrace();
			}
	
		}catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}

		return JobPostingURL;
	}
	public void mailSendByThread(int mailType,int noOfSchoolExpHires,UserMaster userSession,String to,String subject,HttpServletRequest request,JobOrder jobOrder,String jcRecords, String text){
		MailSend mst =new MailSend(mailType,noOfSchoolExpHires,userSession,to,subject,request,jobOrder,jcRecords,text);
		Thread currentThread = new Thread(mst);
		currentThread.start(); 
	}
	public void mailSendByThread(int jobOrderType,String nameOfEntity,String Schools,String aUploadUrl,int mailType,UserMaster userSession,String to,String subject,HttpServletRequest request,JobOrder jobOrder){
		MailSend mst =new MailSend(jobOrderType,nameOfEntity,Schools,aUploadUrl,mailType,userSession,to,subject,request,jobOrder);
		Thread currentThread = new Thread(mst);
		currentThread.start(); 
	}
	public void mailSendByThreadForBatchJobOrder(int mailType,UserMaster userSession,String to,String subject,HttpServletRequest request,BatchJobOrder batchJobOrder,String jcRecords, String text){
			MailSend mst =new MailSend(mailType,userSession,to,subject,request,batchJobOrder,jcRecords,text);
		Thread currentThread = new Thread(mst);
		currentThread.start(); 
	}
	public class MailSend  extends Thread{
		int mailTypeTrdVal=0;
		int noOfSchoolExpHiresTrdVal=0;
		UserMaster userSessionTrdVal=null;
		String toTrdVal="";
		String subjectTrdVal="";
		String baseURL=null;
		JobOrder jobOrderTrdVal=null;
		String jcRecordsTrdVal="";
		String nameTrdVal="";
		BatchJobOrder batchJobOrderTrdVal=null;
		int jobOrderTypeTrdVal=0;
		String nameOfEntityTrdVal=null;
		String SchoolsTrdVal=null;
		String aUploadUrlTrdVal=null;
		int noOfExpHiresTrdVal=0;
		public MailSend(int mailType,int noOfSchoolExpHires,UserMaster userSession, String to,String subject,HttpServletRequest request,JobOrder jobOrder,String jcRecords, String name){
			  mailTypeTrdVal=mailType;
			  noOfSchoolExpHiresTrdVal=noOfSchoolExpHires;
			  userSessionTrdVal=userSession;
			  toTrdVal=to;
			  subjectTrdVal=subject;
			  baseURL=Utility.getBaseURL(request);
			  jobOrderTrdVal=jobOrder;
			  jcRecordsTrdVal=jcRecords;
			  nameTrdVal=name;
		 }
		public MailSend(int jobOrderType,String nameOfEntity,String Schools,String aUploadUrl,int mailType,UserMaster userSession, String to,String subject,HttpServletRequest request,JobOrder jobOrder){
			  mailTypeTrdVal=mailType;
			  userSessionTrdVal=userSession;
			  toTrdVal=to;
			  subjectTrdVal=subject;
			  baseURL=Utility.getBaseURL(request);
			  jobOrderTrdVal=jobOrder;
			  jobOrderTypeTrdVal=jobOrderType;
			  nameOfEntityTrdVal=nameOfEntity;
			  SchoolsTrdVal=Schools;
			  aUploadUrlTrdVal=aUploadUrl;
		 }
		public MailSend(int mailType,UserMaster userSession, String to,String subject,HttpServletRequest request,BatchJobOrder batchJobOrder,String jcRecords, String name){
			  mailTypeTrdVal=mailType;
			  userSessionTrdVal=userSession;
			  toTrdVal=to;
			  subjectTrdVal=subject;
			  baseURL=Utility.getBaseURL(request);
			  batchJobOrderTrdVal=batchJobOrder;
			  jcRecordsTrdVal=jcRecords;
			  nameTrdVal=name;
			  noOfExpHiresTrdVal=batchJobOrder.getNoOfExpHires();
		 }
		 public void run() {
				try {
					Thread.sleep(1000);
					if(mailTypeTrdVal==1){
						emailerService.sendMailAsHTMLText(toTrdVal,subjectTrdVal,MailText.createJobOrderForDistrict(noOfSchoolExpHiresTrdVal,userSessionTrdVal,baseURL, jobOrderTrdVal, jcRecordsTrdVal,nameTrdVal));
					}else if(mailTypeTrdVal==2){
						emailerService.sendMailAsHTMLText(toTrdVal,subjectTrdVal,MailText.createBatchJobOrderForSchool(noOfExpHiresTrdVal,userSessionTrdVal,baseURL, batchJobOrderTrdVal, jcRecordsTrdVal,nameTrdVal));
					}else if(mailTypeTrdVal==3){
						emailerService.sendMailAsHTMLText(toTrdVal,subjectTrdVal,MailText.jobOrderSupport(jobOrderTypeTrdVal,nameOfEntityTrdVal,SchoolsTrdVal,aUploadUrlTrdVal,userSessionTrdVal,baseURL, jobOrderTrdVal));
					}
				}catch (NullPointerException en) {
					en.printStackTrace();
				}catch (InterruptedException e) {
					e.printStackTrace();
				}
		  }

	}
	@Transactional(readOnly=false)
	public String addEditBatchJobOrder(boolean  pkOffered,boolean  kgOffered,boolean  g01Offered,boolean  g02Offered,boolean  g03Offered,boolean  g04Offered,boolean  g05Offered,boolean  g06Offered,boolean  g07Offered,boolean  g08Offered,boolean  g09Offered,boolean  g10Offered,boolean  g11Offered,boolean  g12Offered,int btnVal,int batchJobId,int districtHiddenId,int exitURLMessageVal,String exitMessage,String exitURL,String certificationType,String schoolVal,int JobOrderType, int districtOrSchooHiddenlId,String  jobTitle,String  jobDescription,String  jobQualification,int  noOfExpHires,String  jobStartDate,String  jobEndDate,int  allSchoolGradeDistrictVal,int  isJobAssessment,int  attachJobAssessment,String  assessmentDocument) 
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String existJobTitle="";
		try{
			UserMaster userMaster = null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			int entityID=0;
			int createdBy=0;
			boolean isJobAcceptedFlag =false; 
			
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				entityID=userMaster.getEntityType();
				createdBy=userMaster.getUserId();
			}
			int eJobTitleFlag=0;
			if(districtHiddenId!=0){
				districtMaster =districtMasterDAO.findById(districtHiddenId, false, false);
			}
				
			BatchJobOrder batchJobOrder=null;
			if(batchJobId==0){
				batchJobOrder=new BatchJobOrder();
			}else{
				batchJobOrder = batchJobOrderDAO.findById(batchJobId, false, false);
				isJobAcceptedFlag=schoolInBatchJobDAO.findJobAccepted(batchJobOrder);
			}
			
			batchJobOrder.setDistrictMaster(districtMaster);
			
			if(jobTitle!=null)
			batchJobOrder.setJobTitle(jobTitle);
			
			if(jobDescription!=null)
			batchJobOrder.setJobDescription(jobDescription);
			
			if(jobQualification!=null)
				batchJobOrder.setJobQualification(jobQualification);
			
			
			if(allSchoolGradeDistrictVal==1 && JobOrderType==2 && isJobAcceptedFlag==false){
				batchJobOrder.setAllSchoolsInDistrict(1);
				batchJobOrder.setAllGrades(2);
				batchJobOrder.setPkOffered(2);
				batchJobOrder.setKgOffered(2);
				batchJobOrder.setG01Offered(2);
				batchJobOrder.setG02Offered(2);
				batchJobOrder.setG03Offered(2);
				batchJobOrder.setG04Offered(2);
				batchJobOrder.setG05Offered(2);
				batchJobOrder.setG06Offered(2);
				batchJobOrder.setG07Offered(2);
				batchJobOrder.setG08Offered(2);
				batchJobOrder.setG09Offered(2);
				batchJobOrder.setG10Offered(2);
				batchJobOrder.setG11Offered(2);
				batchJobOrder.setG12Offered(2);
				batchJobOrder.setSelectedSchoolsInDistrict(2);
				
			}
			if(allSchoolGradeDistrictVal==2 && JobOrderType==2  && isJobAcceptedFlag==false){
				batchJobOrder.setAllGrades(1);
				if(pkOffered)
					batchJobOrder.setPkOffered(1);
				else
					batchJobOrder.setPkOffered(2);
				
				if(kgOffered)
					batchJobOrder.setKgOffered(1);
				else
					batchJobOrder.setKgOffered(2);
				
				if(g01Offered)
					batchJobOrder.setG01Offered(1);
				else
					batchJobOrder.setG01Offered(2);
				
				if(g02Offered)
					batchJobOrder.setG02Offered(1);
				else 
					batchJobOrder.setG02Offered(2);
				
				if(g03Offered)
					batchJobOrder.setG03Offered(1);
				else 
					batchJobOrder.setG03Offered(2);
				
				if(g04Offered)
					batchJobOrder.setG04Offered(1);
				else 
					batchJobOrder.setG04Offered(2);
	
				if(g05Offered)
					batchJobOrder.setG05Offered(1);
				else 
					batchJobOrder.setG05Offered(2);
				
				if(g06Offered)
					batchJobOrder.setG06Offered(1);
				else 
					batchJobOrder.setG06Offered(2);
				
				if(g07Offered)
					batchJobOrder.setG07Offered(1);
				else 
					batchJobOrder.setG07Offered(2);
				
				if(g08Offered)
					batchJobOrder.setG08Offered(1);
				else 
					batchJobOrder.setG08Offered(2);
				
				if(g09Offered)
					batchJobOrder.setG09Offered(1);
				else 
					batchJobOrder.setG09Offered(2);
				
				if(g10Offered)
					batchJobOrder.setG10Offered(1);
				else 
					batchJobOrder.setG10Offered(2);
				
				if(g11Offered)
					batchJobOrder.setG11Offered(1);
				else 
					batchJobOrder.setG11Offered(2);
				
				if(g12Offered)
					batchJobOrder.setG12Offered(1);
				else 
					batchJobOrder.setG12Offered(2);
				batchJobOrder.setSelectedSchoolsInDistrict(2);
				batchJobOrder.setAllSchoolsInDistrict(2);
			}
			
			if((allSchoolGradeDistrictVal==3 && JobOrderType==2  && isJobAcceptedFlag==false) || (JobOrderType==3  && isJobAcceptedFlag==false)){
				batchJobOrder.setAllGrades(2);
				batchJobOrder.setAllSchoolsInDistrict(2);
				batchJobOrder.setPkOffered(2);
				batchJobOrder.setKgOffered(2);
				batchJobOrder.setG01Offered(2);
				batchJobOrder.setG02Offered(2);
				batchJobOrder.setG03Offered(2);
				batchJobOrder.setG04Offered(2);
				batchJobOrder.setG05Offered(2);
				batchJobOrder.setG06Offered(2);
				batchJobOrder.setG07Offered(2);
				batchJobOrder.setG08Offered(2);
				batchJobOrder.setG09Offered(2);
				batchJobOrder.setG10Offered(2);
				batchJobOrder.setG11Offered(2);
				batchJobOrder.setG12Offered(2);
				batchJobOrder.setSelectedSchoolsInDistrict(1);
				if(JobOrderType==3){
					if(noOfExpHires!=0)
						batchJobOrder.setNoOfExpHires(noOfExpHires);
				}else{
					batchJobOrder.setNoOfExpHires(null);
				}
			}
				
			if(isJobAssessment==1){
				batchJobOrder.setIsJobAssessment(true);
			}else{
				batchJobOrder.setIsJobAssessment(false);
			}
			if(isJobAssessment==1 && attachJobAssessment==2){
				
				if(!assessmentDocument.equals(""))
				batchJobOrder.setAssessmentDocument(assessmentDocument);
				
				batchJobOrder.setAttachNewPillar(1);
				batchJobOrder.setAttachDefaultDistrictPillar(2);
				batchJobOrder.setAttachDefaultSchoolPillar(2);
			}else if(isJobAssessment==1 && attachJobAssessment==1){
				batchJobOrder.setAssessmentDocument(null);
				batchJobOrder.setAttachNewPillar(2);
				batchJobOrder.setAttachDefaultDistrictPillar(1);
				batchJobOrder.setAttachDefaultSchoolPillar(2);
			}else if(isJobAssessment==1 && attachJobAssessment==3){
				batchJobOrder.setAssessmentDocument(null);
				batchJobOrder.setAttachNewPillar(2);
				batchJobOrder.setAttachDefaultDistrictPillar(2);
				batchJobOrder.setAttachDefaultSchoolPillar(1);
			}else if(isJobAssessment==2){
				batchJobOrder.setAttachNewPillar(2);
				batchJobOrder.setAttachDefaultDistrictPillar(2);
				batchJobOrder.setAttachDefaultSchoolPillar(2);
			}
			
			if(btnVal==1){
				batchJobOrder.setJobStatus("S");
				batchJobOrder.setStatus("I");
			}else{
				batchJobOrder.setJobStatus("T");
				batchJobOrder.setStatus("A");
			}
			batchJobOrder.setCreatedBy(createdBy);
			batchJobOrder.setCreatedByEntity(entityID);
			batchJobOrder.setCreatedForEntity(JobOrderType);
			
			
			/******Add District School******/
			if(batchJobId==0){
				batchJobOrderDAO.makePersistent(batchJobOrder);
			}else{
				batchJobOrderDAO.updatePersistent(batchJobOrder);
			}
			if(allSchoolGradeDistrictVal==1 && JobOrderType==2  && isJobAcceptedFlag==false){
				deleteSchoolInBatchJob(batchJobOrder.getBatchJobId(),0);
				List<DistrictSchools> lstDistrictSchools		=	null;
				Criterion criterionDSchool = Restrictions.eq("districtMaster",districtMaster);
				lstDistrictSchools =	districtSchoolsDAO.findByCriteria(criterionDSchool);
				for(DistrictSchools districtSchools: lstDistrictSchools){
					SchoolInBatchJob schoolInBatchJob= new SchoolInBatchJob();
					schoolInBatchJob.setBatchJobId(batchJobOrder);
					schoolInBatchJob.setSchoolId(districtSchools.getSchoolMaster());
					if(noOfExpHires!=0){
						schoolInBatchJob.setNoOfSchoolExpHires(noOfExpHires);
					}else{
						schoolInBatchJob.setNoOfSchoolExpHires(null);
					}
					schoolInBatchJob.setAcceptedDateTime(new Date());
					schoolInBatchJobDAO.makePersistent(schoolInBatchJob);
				}
			}
			if(allSchoolGradeDistrictVal==2 && JobOrderType==2  && isJobAcceptedFlag==false){
				deleteSchoolInBatchJob(batchJobOrder.getBatchJobId(),0);
				Criterion criterion1=null;
				Criterion criterion2=null;
				Criterion criterion3=null;
				Criterion criterion4=null;
				Criterion criterion5=null;
				Criterion criterion6=null;
				Criterion criterion7=null;
				Criterion criterion8=null;
				Criterion criterion9=null;
				Criterion criterion10=null;
				Criterion criterion11=null;
				Criterion criterion12=null;
				Criterion criterion13=null;
				Criterion criterion14=null;
			
				List<DistrictSchools> lstDistrictSchools		=	null;
				List<SchoolMaster> lstSchoolMaster		=	null;
				
				Disjunction disjunction = Restrictions.disjunction();
				
				if(batchJobOrder.getPkOffered()!=null)
				if(batchJobOrder.getPkOffered().equals(1)){
					criterion1 = Restrictions.eq("pkOffered",batchJobOrder.getPkOffered());
					disjunction.add(criterion1);
				}
				
				if(batchJobOrder.getKgOffered()!=null)
				if(batchJobOrder.getKgOffered().equals(1)){
					criterion2 = Restrictions.eq("kgOffered",batchJobOrder.getKgOffered());
					disjunction.add(criterion2);
				}
				
				if(batchJobOrder.getG01Offered()!=null)
				if(batchJobOrder.getG01Offered().equals(1) && batchJobOrder.getG01Offered()!=null){
					criterion3 = Restrictions.eq("g01Offered",batchJobOrder.getG01Offered());
					disjunction.add(criterion3);
				}
				
				if(batchJobOrder.getG02Offered()!=null)
				if(batchJobOrder.getG02Offered().equals(1)){
					criterion4 = Restrictions.eq("g02Offered",batchJobOrder.getG02Offered());
					disjunction.add(criterion4);
				}
				
				if(batchJobOrder.getG03Offered()!=null)
				if(batchJobOrder.getG03Offered().equals(1)){
					criterion5 = Restrictions.eq("g03Offered",batchJobOrder.getG03Offered());
					disjunction.add(criterion5);
				}
				if(batchJobOrder.getG04Offered()!=null)
				if(batchJobOrder.getG04Offered().equals(1)){
					criterion6 = Restrictions.eq("g04Offered",batchJobOrder.getG04Offered());
					disjunction.add(criterion6);
				}
				
				if(batchJobOrder.getG05Offered()!=null)
				if(batchJobOrder.getG05Offered().equals(1)){
					criterion7 = Restrictions.eq("g05Offered",batchJobOrder.getG05Offered());
					disjunction.add(criterion7);
				}
				
				if(batchJobOrder.getG06Offered()!=null)
				if(batchJobOrder.getG06Offered().equals(1)){
					criterion8 = Restrictions.eq("g06Offered",batchJobOrder.getG06Offered());
					disjunction.add(criterion8);
				}
				
				if(batchJobOrder.getG07Offered()!=null)
				if(batchJobOrder.getG07Offered().equals(1)){
					criterion9 = Restrictions.eq("g07Offered",batchJobOrder.getG07Offered());
					disjunction.add(criterion9);
				}
				
				if(batchJobOrder.getG08Offered()!=null)
				if(batchJobOrder.getG08Offered().equals(1)){
					criterion10 = Restrictions.eq("g08Offered",batchJobOrder.getG08Offered());
					disjunction.add(criterion10);
				}
				
				if(batchJobOrder.getG09Offered()!=null)
				if(batchJobOrder.getG09Offered().equals(1)){
					criterion11 = Restrictions.eq("g09Offered",batchJobOrder.getG09Offered());
					disjunction.add(criterion11);
				}
				
				if(batchJobOrder.getG10Offered()!=null)
				if(batchJobOrder.getG10Offered().equals(1)){
					criterion12 = Restrictions.eq("g10Offered",batchJobOrder.getG10Offered());
					disjunction.add(criterion12);
				}
				
				if(batchJobOrder.getG11Offered()!=null)
				if(batchJobOrder.getG11Offered().equals(1)){
					criterion13 = Restrictions.eq("g11Offered",batchJobOrder.getG11Offered());
					disjunction.add(criterion13);
				}
				
				if(batchJobOrder.getG12Offered()!=null)
				if(batchJobOrder.getG12Offered().equals(1)){
					criterion14 = Restrictions.eq("g12Offered",batchJobOrder.getG12Offered());
					disjunction.add(criterion14);
				}
				try{
					Criterion criterionDistrict = Restrictions.eq("districtId",districtMaster);
					lstSchoolMaster	=	schoolMasterDAO.findByCriteria(criterionDistrict,disjunction);
					if(lstSchoolMaster.size()>0){
						lstDistrictSchools =	districtSchoolsDAO.findSchoolId(lstSchoolMaster);
						 for(DistrictSchools districtSchools: lstDistrictSchools){
								SchoolInBatchJob schoolInBatchJob= new SchoolInBatchJob();
								schoolInBatchJob.setBatchJobId(batchJobOrder);
								schoolInBatchJob.setSchoolId(districtSchools.getSchoolMaster());
								if(noOfExpHires!=0){
									schoolInBatchJob.setNoOfSchoolExpHires(noOfExpHires);
								}else{
									schoolInBatchJob.setNoOfSchoolExpHires(null);
								}
								schoolInBatchJob.setAcceptedDateTime(new Date());
								schoolInBatchJobDAO.makePersistent(schoolInBatchJob);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(JobOrderType==2 && batchJobId==0){
				if(schoolVal!="" && !schoolVal.equalsIgnoreCase("") && allSchoolGradeDistrictVal==3){
					try{
						String idAndHires[]=schoolVal.split(",");
						for(int i=0;i<idAndHires.length;i++){
							SchoolInBatchJob schoolInBatchJob= new SchoolInBatchJob();
							String ids[]=idAndHires[i].split("-");
							schoolInBatchJob.setBatchJobId(batchJobOrder);
							schoolMaster = schoolMasterDAO.findById(Long.parseLong(ids[0]), false, false);
							schoolInBatchJob.setSchoolId(schoolMaster);
							schoolInBatchJob.setNoOfSchoolExpHires(Integer.parseInt(ids[1]));
							schoolInBatchJob.setAcceptedDateTime(new Date());
							schoolInBatchJobDAO.makePersistent(schoolInBatchJob);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}else if(batchJobId==0){
				try{
					String idAndHires[]=schoolVal.split(",");
					for(int i=0;i<idAndHires.length;i++){
						SchoolInBatchJob schoolInBatchJob= new SchoolInBatchJob();
						schoolInBatchJob.setBatchJobId(batchJobOrder);
						schoolMaster = schoolMasterDAO.findById(Long.parseLong(districtOrSchooHiddenlId+""), false, false);
						schoolInBatchJob.setSchoolId(schoolMaster);
						schoolInBatchJob.setAcceptedDateTime(new Date());
						schoolInBatchJobDAO.makePersistent(schoolInBatchJob);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if(btnVal==2){
					String subject="";
					
					subject= Utility.getLocaleValuePropByKey("msgBatchJobOrder6", locale) + userMaster.getFirstName()+" "+userMaster.getLastName()+" ( "+userMaster.getEmailAddress()+" ) to Activate It";
					
					List<SchoolMaster> schoolIds=schoolInBatchJobDAO.findSchoolIds(batchJobOrder);
					try{
						if(schoolIds.size()>0){
							List<UserMaster> lstUserMaster= null;
							Criterion criterionUser1 = Restrictions.eq("entityType",3);
							Criterion criterionUser2 = Restrictions.eq("districtId",districtMaster);
							Criterion criterionUser3 = Restrictions.in("schoolId",schoolIds);
							Criterion criterionUser4 = Restrictions.eq("status","A");
							
							lstUserMaster = userMasterDAO.findByCriteria(criterionUser1,criterionUser2,criterionUser3,criterionUser4);
							String newCertiId=null; 
							newCertiId=certificateTypeMasterDAO.getCertificateNameByIds(certificationType);
							
							//System.out.println("certificationType :"+certificationType);
							//System.out.println("newCertiId :"+newCertiId);
							
							for (UserMaster userMasterDetails : lstUserMaster){
								int noOfSchoolExpHires=0;
								if(noOfExpHires!=0){
									batchJobOrder.setNoOfExpHires(noOfExpHires);
								}else if(userMasterDetails.getEntityType()==3){
									noOfSchoolExpHires=schoolInBatchJobDAO.getNoOfSchoolExpHiresBySchool(batchJobOrder,userMasterDetails.getSchoolId());
									batchJobOrder.setNoOfExpHires(noOfSchoolExpHires);
									noOfSchoolExpHires=0;
								}
								
								mailSendByThreadForBatchJobOrder(2,userMaster,userMasterDetails.getEmailAddress(),subject,request, batchJobOrder, newCertiId,userMasterDetails.getFirstName());
							}

						}
					}catch(Exception e){
						e.printStackTrace();
					}
			}
				
			if(certificationType!="" && !certificationType.equalsIgnoreCase("") && batchJobId==0){
				try{
					String certificationTypes[]=certificationType.split(",");
					for(int i=0;i<certificationTypes.length;i++){
						BatchJobCertification jobCertification= new BatchJobCertification();
						//String certificationTypeVal=certificationTypes[i].replace("<|>",",");
						String certificationTypeVal=certificationTypes[i];
						CertificateTypeMaster certificateTypeMaster = new CertificateTypeMaster();
						certificateTypeMaster.setCertTypeId(Integer.parseInt(certificationTypeVal));
						jobCertification.setCertificateTypeMaster(certificateTypeMaster);
						jobCertification.setBatchJobId(batchJobOrder);
						//jobCertification.setCertType(certificationTypeVal);
						batchJobCertificationDAO.makePersistent(jobCertification);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			try{
				String logType="";
				if(JobOrderType==2){
					if(districtMaster!=null)
					userMaster.setDistrictId(districtMaster);
					
					if(batchJobId==0)
						logType="Created Batch Job Order";
					else
						logType="Edited Batch Job Order";
				}else{
					if(districtMaster!=null)
						userMaster.setDistrictId(districtMaster);
					
					/*if(schoolMaster!=null)
					userMaster.setSchoolId(schoolMaster);*/
					
					if(batchJobId==0)
						logType="Created Batch Job Order";
					else
						logType="Edited Batch Job Order";
				}
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				e.printStackTrace();
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}

		return existJobTitle;
	}
	public int copyToClipBoard(String strVal)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try
		{
			if (session == null || session.getAttribute("userMaster") == null)
			{
				return 0;
			}
			else
			{
				Toolkit toolkit = Toolkit.getDefaultToolkit();
				Clipboard clipboard = toolkit.getSystemClipboard();
				StringSelection strSel = new StringSelection(strVal);
				clipboard.setContents(strSel, null);
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return 1;
	}
	/* @Author: Sekhar 
	 * @Discription: It is used to delete DistrictSchools   .
	 */
	@Transactional(readOnly=false)
	public boolean deleteDistrictSchools(DistrictMaster districtMaster){
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			DistrictNotes districtNotes			=	null;
			List<DistrictSchools> lstDistrictSchools		=	null;
			Criterion criterionDSchool = Restrictions.eq("districtMaster",districtMaster);
			lstDistrictSchools						=	districtSchoolsDAO.findByCriteria(criterionDSchool);
			for(DistrictSchools districtSchools: lstDistrictSchools){
				districtSchoolsDAO.makeTransient(districtSchools);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int checkDuplicateSchool(int batchJobId,int schoolId) 
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<SchoolInBatchJob> lstSchoolInBatchJob= null;
		try{
			BatchJobOrder  batchJobOrder = batchJobOrderDAO.findById(batchJobId, false, false);
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			SchoolMaster  schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId+""), false, false);
			Criterion criterion2 = Restrictions.eq("schoolId",schoolMaster);
			lstSchoolInBatchJob = schoolInBatchJobDAO.findByCriteria(criterion1,criterion2);	
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		return lstSchoolInBatchJob.size();

	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int checkDuplicateJobTitle(String jobTitle,int districtId) 
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<BatchJobOrder> lstBatchJobOrder= null;
		try{
			Criterion criterion1 = Restrictions.eq("jobTitle",jobTitle.trim());
			DistrictMaster  districtMaster = districtMasterDAO.findById(districtId, false, false);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			lstBatchJobOrder = batchJobOrderDAO.findByCriteria(criterion1,criterion2);	
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		return lstBatchJobOrder.size();

	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int checkSchoolExist(int districtId) 
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictSchools> lstDistrictSchools= null;
		try{
			DistrictMaster  districtMaster = districtMasterDAO.findById(districtId, false, false);
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			lstDistrictSchools = districtSchoolsDAO.findByCriteria(criterion);	
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		return lstDistrictSchools.size();

	}	
	public int addSchoolInBatchJob(int batchJobId,int schoolId,int noOfSchoolExpHires){
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		int existFlag=0;
		try{
			existFlag=checkDuplicateSchool(batchJobId,schoolId);
			if(existFlag==0){
				SchoolInBatchJob schoolInBatchJob= new SchoolInBatchJob();
				BatchJobOrder  batchJobOrder = batchJobOrderDAO.findById(batchJobId, false, false);
				schoolInBatchJob.setBatchJobId(batchJobOrder);
				SchoolMaster  schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId+""), false, false);
				schoolInBatchJob.setSchoolId(schoolMaster);
				schoolInBatchJob.setNoOfSchoolExpHires(noOfSchoolExpHires);
				schoolInBatchJob.setAcceptedDateTime(new Date());
				schoolInBatchJobDAO.makePersistent(schoolInBatchJob);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		return existFlag;
	}
	/* @Author: Sudhandu
	 * @Discription: It is used to delete a particular School from database.
	 */
	//@Transactional(readOnly=false)
	public boolean deleteSchoolInBatchJob(int batchJobId,int schoolId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			List<SchoolInBatchJob> lstschoolInBatchJob=null; 
			BatchJobOrder  batchJobOrder = batchJobOrderDAO.findById(batchJobId, false, false);
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			lstschoolInBatchJob =schoolInBatchJobDAO.findByCriteria(criterion1);
			
			for(SchoolInBatchJob schoolInBatchJob: lstschoolInBatchJob){
				if(schoolId==0){
					schoolInBatchJobDAO.makeTransient(schoolInBatchJob);
				}else{
					if(schoolInBatchJob.getSchoolId().getSchoolId()==schoolId){
						schoolInBatchJobDAO.makeTransient(schoolInBatchJob);
					}
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	//@Transactional(readOnly=false)
	public boolean deleteCertification(int batchJobId,int batchJobCertificationId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			List<BatchJobCertification> lstBatchJobCertification=null; 
			BatchJobOrder  batchJobOrder = batchJobOrderDAO.findById(batchJobId, false, false);
			Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
			lstBatchJobCertification =batchJobCertificationDAO.findByCriteria(criterion1);
			for(BatchJobCertification batchJobCertification: lstBatchJobCertification){
				if(batchJobCertificationId==0){
					batchJobCertificationDAO.makeTransient(batchJobCertification);
				}else{
					if(batchJobCertification.getBatchJobCertificationId()==batchJobCertificationId){
						batchJobCertificationDAO.makeTransient(batchJobCertification);
					}
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	@Transactional(readOnly=false)
	public Boolean deleteAssessment(int jobId,int JobOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try 
		{
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false,false);
			String filePath="";
			if(JobOrderType==2){
				filePath=Utility.getValueOfPropByKey("districtRootPath")+jobOrder.getDistrictMaster().getDistrictId()+"/";
			}else{
				filePath=Utility.getValueOfPropByKey("schoolRootPath")+jobOrder.getSchool().get(0).getSchoolId()+"/";
			}
			try{
				File file = new File(filePath+"\\"+jobOrder.getAssessmentDocument());
				if(file.delete()){
	    			System.out.println(file.getName() + " is deleted!");
	    		}else{
	    			System.out.println("Delete operation is failed.");
	    		}
			}catch(Exception e){
				e.printStackTrace();
			}
			attachFlagUpdate(jobId);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
	@Transactional(readOnly=false)
	public boolean attachFlagUpdate(int jobId){
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
			jobOrder.setAttachNewPillar(2);
			jobOrder.setAssessmentDocument("");
			jobOrderDAO.updatePersistent(jobOrder);
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public List<SchoolMaster> getFieldOfSchoolList(String fieldOfStudy)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<SchoolMaster> fieldOfSchoolList = null;
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", fieldOfStudy,MatchMode.ANYWHERE);
			if(fieldOfStudy.trim()!=null && fieldOfStudy.trim().length()>0){
				fieldOfSchoolList = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion);
			}else{
				fieldOfSchoolList = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return fieldOfSchoolList;
	}
	/* ===============      delete Batch JobOrder      =========================*/
	@Transactional(readOnly=false)
	public boolean deleteBatchJobOrder(int batchJobId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
	    	userMaster=	(UserMaster) session.getAttribute("userMaster");
	    }
		try{
			BatchJobOrder  batchJobOrder = batchJobOrderDAO.findById(batchJobId, false, false);
			deleteSchoolInBatchJob(batchJobId,0);
			deleteCertification(batchJobId,0);
			batchJobOrderDAO.makeTransient(batchJobOrder);
			try{
				if(batchJobOrder.getDistrictMaster()!=null)
					userMaster.setDistrictId(batchJobOrder.getDistrictMaster());
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Deleted Batch Job Order");
			}catch(Exception e){
				e.printStackTrace();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public List<DistrictMaster> getFieldOfDistrictList(String DistrictName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		try{
			
			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "A");
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);
				
				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);
				
				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
				
			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25,criterion1);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return districtMasterList;
	}
	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
		List<DistrictSchools> fieldOfSchoolList1 = null;
		List<DistrictSchools> fieldOfSchoolList2 = null;
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
			if(districtIdForSchool==0){
				if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
					fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
					Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
					fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
					schoolMasterList.addAll(fieldOfSchoolList1);
					schoolMasterList.addAll(fieldOfSchoolList2);
					Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
					schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
				}else{
					fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
					schoolMasterList.addAll(fieldOfSchoolList1);
				}
			}else{
				DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				
				if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
					fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
					Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
					fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
					
					schoolMasterList.addAll(fieldOfSchoolList1);
					schoolMasterList.addAll(fieldOfSchoolList2);
					Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
					schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
				}else{
					fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
					schoolMasterList.addAll(fieldOfSchoolList1);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return schoolMasterList;
		
	}
	
	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName,String all)
	{
		System.out.println(districtIdForSchool+":::::getFieldOfSchoolList:::::::::::::::::::"+SchoolName);
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
		List<DistrictSchools> fieldOfSchoolList1 = null;
		List<DistrictSchools> fieldOfSchoolList2 = null;
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
			
				if(districtIdForSchool==0){
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
						Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					}else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}else{
					DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
					Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
					
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
						Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
						
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					}else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return schoolMasterList;
		
	}
		
	/*=================== GAgan : getDistrictObjByDistrictId ==============*/
	public DistrictMaster getDistrictObjByDistrictId(int districtId)
	{
		//System.out.println("\n\n\n GAgan : getDistrictObjByDistrictId  "+districtId);
		DistrictMaster districtMaster	=	null;
		try 
		{
			if(""+districtId!="")
			districtMaster	=	districtMasterDAO.findById(districtId, false, false);
			
			
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return districtMaster;
	}
	
		
		
}


