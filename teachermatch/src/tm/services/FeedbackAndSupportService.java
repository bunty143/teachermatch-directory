package tm.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;
import tm.dao.UserFeedbackDAO;
import tm.dao.UserSupportDAO;
import tm.bean.UserSupport;
import tm.bean.UserFeedback;

public class FeedbackAndSupportService {

	@Autowired
	private UserFeedbackDAO userFeedbackDAO;
	public void setUserFeedbackDAO(UserFeedbackDAO userFeedbackDAO) {
		this.userFeedbackDAO = userFeedbackDAO;
	}

	@Autowired
	private UserSupportDAO userSupportDAO;
	public void setUserSupportDAO(UserSupportDAO userSupportDAO) {
		this.userSupportDAO = userSupportDAO;
	}
	
	public boolean saveFeedbackOrSupport(String reqType,UserMaster userMaster,TeacherDetail teacherDetail,String visitedPageURL,String uploadedFileURL,String subject,String msgContent,String ipAddress,String starRating,String phone,String visitedPageTitle)
	{
		String userEmail		=	"";
		String userFirstName	=	"";
		String userLastName		=	"";
		String userRole			=	"";
		String userType			=	"";
		
		try {
			if(userMaster!=null) {
				userEmail 		= 	userMaster.getEmailAddress();
				userFirstName 	= 	userMaster.getFirstName();
				userLastName 	= 	userMaster.getLastName();
				userRole	 	= 	userMaster.getRoleId().getRoleName();
				if(userMaster.getEntityType()==1)
					userType="TeacherMatch";
				else if(userMaster.getEntityType()==2)
					userType="District";
				else if(userMaster.getEntityType()==3)
					userType="School";
			} else {
				userEmail 		= 	teacherDetail.getEmailAddress();
				userFirstName 	= 	teacherDetail.getFirstName();
				userLastName 	= 	teacherDetail.getLastName();
				userType		=	"Teacher";
				userRole		=	"Teacher";
			}
			
			if(reqType.equalsIgnoreCase("1"))
			{
				UserSupport userSupport = new UserSupport();
				userSupport.setCreatedDateTime(new Date());
				userSupport.setIpAddress(ipAddress);
				userSupport.setMessage(msgContent);
				userSupport.setSubject(subject);
				userSupport.setUploadedFileURL(uploadedFileURL);
				userSupport.setVisitedPageURL(visitedPageURL);
				userSupport.setUserEmail(userEmail);
				userSupport.setUserFirstName(userFirstName);
				userSupport.setUserLastName(userLastName);
				userSupport.setUserRole(userRole);
				userSupport.setUserType(userType);
				userSupport.setUserContact(phone);
				userSupport.setVisitedPageTitle(visitedPageTitle);

				userSupportDAO.makePersistent(userSupport);

			} else {
				UserFeedback userFeedback = new UserFeedback();
				userFeedback.setFeedback(msgContent);
				userFeedback.setCreatedDateTime(new Date());
				userFeedback.setIpAddress(ipAddress);
				userFeedback.setVisitedPageURL(visitedPageURL);
				userFeedback.setUserEmail(userEmail);
				userFeedback.setUserFirstName(userFirstName);
				userFeedback.setUserLastName(userLastName);
				userFeedback.setUserRole(userRole);
				userFeedback.setUserType(userType);
				userFeedback.setStarRating(Integer.parseInt(starRating));
				userFeedback.setUserContact(phone);
				userFeedback.setVisitedPageTitle(visitedPageTitle);

				userFeedbackDAO.makePersistent(userFeedback);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean saveFeedbackOrSupportBeforeLogin(String reqType,String email,String inputFirstName,String inputLastName,String visitedPageURL,String uploadedFileURL,String subject,String msgContent,String ipAddress,String starRating,String phone,String visitedPageTitle)
	{
		String userEmail	=	"";
		String userFirstName=	"";
		String userLastName	=	"";
		String userType		=	"";

		try {
			if(!email.equals("")) {
				userEmail 		= 	email;
				userFirstName 	= 	inputFirstName;
				userLastName 	= 	inputLastName;
				userType		=	"District";
			} else {
				userType		=	"District";
			}

			if(!email.equals("")){
				System.out.println("Save Support");
				UserSupport userSupport = new UserSupport();
				userSupport.setCreatedDateTime(new Date());
				userSupport.setIpAddress(ipAddress);
				userSupport.setMessage(msgContent);
				userSupport.setSubject(subject);
				userSupport.setUploadedFileURL(uploadedFileURL);
				userSupport.setVisitedPageURL(visitedPageURL);
				userSupport.setUserEmail(userEmail);
				userSupport.setUserFirstName(userFirstName);
				userSupport.setUserLastName(userLastName);
				userSupport.setUserType(userType);
				userSupport.setUserContact(phone);
				userSupport.setVisitedPageTitle(visitedPageTitle);

				userSupportDAO.makePersistent(userSupport);
			} else {
				System.out.println("Save FeedBack");
				UserFeedback userFeedback = new UserFeedback();
				userFeedback.setFeedback(msgContent);
				userFeedback.setCreatedDateTime(new Date());
				userFeedback.setIpAddress(ipAddress);
				userFeedback.setVisitedPageURL(visitedPageURL);
				userFeedback.setUserEmail(userEmail);
				userFeedback.setUserFirstName(userFirstName);
				userFeedback.setUserLastName(userLastName);
				userFeedback.setUserType(userType);
				userFeedback.setStarRating(Integer.parseInt(starRating));
				userFeedback.setUserContact(phone);
				userFeedback.setVisitedPageTitle(visitedPageTitle);

				userFeedbackDAO.makePersistent(userFeedback);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean saveFeedbackOrSupportNew(String reqType,String checkFeedback,UserMaster userMaster,TeacherDetail teacherDetail,String visitedPageURL,String uploadedFileURL,String subject,String msgContent,String ipAddress,String starRating,String phone,String visitedPageTitle)
	{
		String userEmail	=	"";
		String userFirstName=	"";
		String userLastName	=	"";
		String userRole		=	"";
		String userType		=	"";

		try {
			if(userMaster!=null) {
				userEmail 		= 	userMaster.getEmailAddress();
				userFirstName 	= 	userMaster.getFirstName();
				userLastName 	= 	userMaster.getLastName();
				userRole 		= 	userMaster.getRoleId().getRoleName();
				if(userMaster.getEntityType()==1)
					userType="TeacherMatch";
				else if(userMaster.getEntityType()==2)
					userType="District";
				else if(userMaster.getEntityType()==3)
					userType="School";
			} else {
				userEmail 		= 	teacherDetail.getEmailAddress();
				userFirstName 	= 	teacherDetail.getFirstName();
				userLastName 	= 	teacherDetail.getLastName();
				userType		=	"Teacher";
				userRole		=	"Teacher";
			}

			if(checkFeedback.equals("-1")) {

				UserFeedback userFeedback = new UserFeedback();
				userFeedback.setFeedback(msgContent);
				userFeedback.setCreatedDateTime(new Date());
				userFeedback.setIpAddress(ipAddress);
				userFeedback.setVisitedPageURL(visitedPageURL);
				userFeedback.setUserEmail(userEmail);
				userFeedback.setUserFirstName(userFirstName);
				userFeedback.setUserLastName(userLastName);
				userFeedback.setUserRole(userRole);
				userFeedback.setUserType(userType);
				userFeedback.setStarRating(Integer.parseInt(starRating));
				userFeedback.setUserContact(phone);
				userFeedback.setVisitedPageTitle(visitedPageTitle);

				userFeedbackDAO.makePersistent(userFeedback);

			} else {
				
				UserSupport userSupport = new UserSupport();
				userSupport.setCreatedDateTime(new Date());
				userSupport.setIpAddress(ipAddress);
				userSupport.setMessage(msgContent);
				userSupport.setSubject(subject);
				userSupport.setUploadedFileURL(uploadedFileURL);
				userSupport.setVisitedPageURL(visitedPageURL);
				userSupport.setUserEmail(userEmail);
				userSupport.setUserFirstName(userFirstName);
				userSupport.setUserLastName(userLastName);
				userSupport.setUserRole(userRole);
				userSupport.setUserType(userType);
				userSupport.setUserContact(phone);
				userSupport.setVisitedPageTitle(visitedPageTitle);

				userSupportDAO.makePersistent(userSupport);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
