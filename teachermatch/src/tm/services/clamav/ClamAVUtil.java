package tm.services.clamav;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import net.taldius.clamav.ClamAVScanner;
import net.taldius.clamav.ClamAVScannerFactory;
import tm.utility.Utility;

public class ClamAVUtil {
	
   
	
       // Host where 'clamd' process is running
       private static String clamdHost;
      
       // Port on which 'clamd' process is listening
       private static String clamdPort;
      
       // Connection time out to connect 'clamd' process
       private static String connTimeOut;
      
       private static ClamAVScanner scanner;
       
       private static Boolean clamInstall;//=true;

       public Boolean getClamInstall() {
		return clamInstall;
       }
       
       public void setClamInstall(Boolean clamInstall) {
    	   ClamAVUtil.clamInstall = clamInstall;
       }
       
       public void setClamdHost(String clamdHost){
    	   ClamAVUtil.clamdHost = clamdHost;
       	}
      
       public String getClamdHost(){
              return ClamAVUtil.clamdHost;
       }
      
       public void setClamdPort(String clamdPort){
    	   ClamAVUtil.clamdPort = clamdPort;
       }
      
       public String getClamdPort(){
              return ClamAVUtil.clamdPort;
       }
      
       public void setConnTimeOut(String connTimeOut){
    	   ClamAVUtil.connTimeOut = connTimeOut;
       }
      
       public String getConnTimeOut(){
              return ClamAVUtil.connTimeOut;
       }
      
       /**
        * Method to initialize clamAV scanner
        */
       public void initScanner(){
             
              ClamAVScannerFactory.setClamdHost(clamdHost);

              ClamAVScannerFactory.setClamdPort(Integer.parseInt(clamdPort));

              int connectionTimeOut = Integer.parseInt(connTimeOut);
             
              if (connectionTimeOut > 0) {
                     ClamAVScannerFactory.setConnectionTimeout(connectionTimeOut);
              }
              this.scanner = ClamAVScannerFactory.getScanner();
       }

       public ClamAVScanner getClamAVScanner() {
              return scanner;
       }

       /**
        * Method scans files to check whether file is virus infected
        *
        * @param destFilePath file path
        * @return
        * @throws Exception
        */
       public static boolean fileScanner(String destFilePath) throws Exception  {

              return fileScanner(new FileInputStream(destFilePath));
       }

       /**
        * Method scans files to check whether file is virus infected
        *
        * @param fileInputStream
        * @return
        * @throws Exception
        */
       public static boolean fileScanner(InputStream fileInputStream) throws Exception {

              boolean resScan = false;

              if (fileInputStream != null) {

                     resScan = scanner.performScan(fileInputStream);

              } else {

                     throw new Exception();
              }
              return resScan;
       }
       
       //scanAndRemove
       public static String scanAndRemove(String destFilePath) throws Exception
       {
    	   String locale = Utility.getValueOfPropByKey("locale");
    	   
    	   String msg="";
    	   if(clamInstall)
    	   {
    		   System.out.println(">>>>>>>>>>>>>>>>>>>>>>>> Start to scan File >>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    		   System.out.println("destFilePath:- "+destFilePath);
    		       		   
    		   try
    		   {
    			   if(destFilePath==null)
    				   return msg;
    			   
    			   FileInputStream fileInputStream = new FileInputStream(new File(destFilePath));
    			   
    			   ClamAVScannerFactory.setClamdHost(clamdHost);
    			   ClamAVScannerFactory.setClamdPort(Integer.parseInt(clamdPort));
    			   ClamAVScannerFactory.setConnectionTimeout(Integer.parseInt(connTimeOut));
    			   
    			   boolean noVirus = ClamAVScannerFactory.getScanner().performScan(fileInputStream);
    			   //boolean noVirus = false;
    			   fileInputStream.close();
    			   
    			   System.out.println("noVirus:- "+noVirus);
    			   if( !noVirus )
    			   {
    				   boolean isFileDeleted= new File(destFilePath).delete();
    				   System.out.println("Is File Deleted:- "+isFileDeleted);
    				   msg = msg + Utility.getLocaleValuePropByKey("msgFileHaveVirus", locale);
    				   return msg;
    			   }
    		   }
    		   
    		   catch(Exception e)
    		   {
    			   System.out.println("Could not scan File:- "+e.getMessage());
    		   }
    		   
    		   finally
    		   {
    			   System.out.println("msg:- "+msg);
    			   System.out.println(">>>>>>>>>>>>>>>>>>>>>>>> End of File Scaning >>>>>>>>>>>>>>>>>>>>>>>>");
    		   }
    		   
    		   return msg;
    	   }
    	   return msg;
       }
}

/* False:- Virus Detected.
 * if(noVirus != true)
 * 		System.out.println("Warning !! Virus detected");
 * */