package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
public class HiredEeocReportAjax 
{
	
    String locale = Utility.getValueOfPropByKey("locale");
    
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	

	public void setTeacherPersonalInfoDAO(
			TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}

	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	
	public String displayRecordsByEntityType(String districtName,String jobStatus,int districtOrSchoolId,String fJobStatus,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate,String jobendstartDate,String jobendendDate)
	 {
			System.out.println(jobStatus+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			System.out.println("job ststus="+jobStatus+"startDate="+startDate+"endDate="+endDate);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=1;
			try{
				
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				//System.out.println("schoolMaster="+schoolMaster+"districtMaster="+districtMaster+"entityID="+entityID);
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position


				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
			 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"districtName";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<String[]> lstHiredEeoc =new ArrayList<String[]>();
				
				if(entityID==2||entityID==3)
				{
					lstHiredEeoc =teacherPersonalInfoDAO.hiredEeoc(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
			    	totalRecord = lstHiredEeoc.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
			    	 
				}
				else if(entityID==1) 
				{
					System.out.println("indside admin ");
					if(districtOrSchoolId!=0)
					{
						lstHiredEeoc =teacherPersonalInfoDAO.hiredEeoc(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
				    	totalRecord = lstHiredEeoc.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
					}
					else
					{
					lstHiredEeoc =teacherPersonalInfoDAO.hiredEeocForAdmin(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
			    	totalRecord = lstHiredEeoc.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
					}
			    	
				}
				
				List<String[]> finallstHiredEeoc =new ArrayList<String[]>();
			     if(totalRecord<end)
						end=totalRecord;
			     finallstHiredEeoc=lstHiredEeoc.subList(start,end);
							
				String responseText="";
				
				tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
								
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDistrictName", locale),sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoStatus", locale),sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgNoApplicant", locale),sortOrderFieldName,"N",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgResponded", locale),sortOrderFieldName,"Responded",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgNoofResponded", locale),sortOrderFieldName,"N_Responded",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgBlack", locale),sortOrderFieldName,"Black",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgAsian", locale),sortOrderFieldName,"Asian",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgHispanic", locale),sortOrderFieldName,"Hispanic",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgWhite", locale),sortOrderFieldName,"White",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgAmricanIndian", locale),sortOrderFieldName,"AmricanIndian",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgHawaiian", locale),sortOrderFieldName,"Hawaiian",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgMulti", locale),sortOrderFieldName,"Multi",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
			
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				if(finallstHiredEeoc.size()==0){
					 tmRecords.append("<tr><td colspan='9' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
					}
				
				String sta="";
				  if(finallstHiredEeoc.size()>0){
	                   for (Iterator it = finallstHiredEeoc.iterator(); it.hasNext();)
	                   {
	                	   String[] row = (String[]) it.next(); 
	                	   
	                	   String  myVal1="";
	                	   String  myVal2="";
	                	  
	                	        tmRecords.append("<tr>");	
	                	        
	                           /* myVal1 = (row[0]==null)? "N/A" : row[0].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");*/
	                            
			                    
			                    myVal1 = (row[1]==null)? "N/A" : row[1].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? "N/A" : row[2].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    if(row[13].toString().equalsIgnoreCase("A"))
								{
									sta=Utility.getLocaleValuePropByKey("optAct", locale);
								}
								else
								{
									sta=Utility.getLocaleValuePropByKey("lblInActiv", locale);
								}
			                    
			                    //myVal1 = (row[13]==null)? "N/A" : row[13].toString();
			                    tmRecords.append("<td>"+sta+"</td>");
			                    
			                    myVal1 = (row[3]==null)? "N/A" : row[3].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[4]==null)? "N/A" : row[4].toString();
			                    tmRecords.append("<td>"+myVal1+"%"+"</td>");
			                    
			                    myVal1 = (row[5]==null)? "N/A" : row[5].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString()+"%";
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString()+"%";
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString()+"%";
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString()+"%";
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString()+"%";
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[11]==null)||(row[11].toString().length()==0))? " " : row[11].toString()+"%";
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[12]==null)||(row[12].toString().length()==0))? " " : row[12].toString()+"%";
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    tmRecords.append("</tr>");
	               }               
	              }
				
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			

			}catch(Exception e){
				e.printStackTrace();
			}
			
		 return tmRecords.toString();
	  }
		
	public String displayRecordsByEntityTypeEXL(String districtName,String jobStatus,int districtOrSchoolId,String fJobStatus,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate,String jobendstartDate,String jobendendDate)
	{
		System.out.println(jobStatus+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		int sortingcheck=1;
		try{
			
			
			
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
		}
		
		//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start =((pgNo-1)*noOfRowInPage);
		int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		
		 /** set default sorting fieldName **/
		String  sortOrderStrVal		=	null;
		String sortOrderFieldName	=	"districtName";
		
		if(!sortOrder.equals("") && !sortOrder.equals(null))
		{
			sortOrderFieldName		=	sortOrder;
		}
		
		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null))
		{
			if(sortOrderType.equals("0"))
			{
				sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
			}
			else
			{
				sortOrderTypeVal	=	"1";
				sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
			}
		}
		else
		{
			sortOrderTypeVal		=	"0";
			sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
		}
		
		//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
			List<String[]> lstHiredEeoc =new ArrayList<String[]>();
			
			
				if(entityID==2||entityID==3)
				{
					//System.out.println("inside entity id=2");
					lstHiredEeoc =teacherPersonalInfoDAO.hiredEeoc(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
			    	totalRecord = lstHiredEeoc.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
			    	 
				}
				else if(entityID==1) 
				{
					System.out.println("indside admin ");
					if(districtOrSchoolId!=0)
					{
						lstHiredEeoc =teacherPersonalInfoDAO.hiredEeoc(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
				    	totalRecord = lstHiredEeoc.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
					}
					else
					{
					lstHiredEeoc =teacherPersonalInfoDAO.hiredEeocForAdmin(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
			    	totalRecord = lstHiredEeoc.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
					}
			    	
				}
				
				List<String[]> finallstHiredEeoc =new ArrayList<String[]>();
			     if(totalRecord<end)
						end=totalRecord;
			     finallstHiredEeoc=lstHiredEeoc;


		 
		 
			 //  Excel   Exporting	
					
					String time = String.valueOf(System.currentTimeMillis()).substring(6);
					//String basePath = request.getRealPath("/")+"/candidate";
					String basePath = request.getSession().getServletContext().getRealPath ("/")+"/hiredapplicants";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="hiredeeoc"+time+".xls";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

					WorkbookSettings wbSettings = new WorkbookSettings();

					wbSettings.setLocale(new Locale("en", "EN"));

					WritableCellFormat timesBoldUnderline;
					WritableCellFormat header;
					WritableCellFormat headerBold;
					WritableCellFormat times;

					WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
					workbook.createSheet("hiredeeoc", 0);
					WritableSheet excelSheet = workbook.getSheet(0);

					WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
					// Define the cell format
					times = new WritableCellFormat(times10pt);
					// Lets automatically wrap the cells
					times.setWrap(true);

					// Create create a bold font with unterlines
					WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
					WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

					timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
					timesBoldUnderline.setAlignment(Alignment.CENTRE);
					// Lets automatically wrap the cells
					timesBoldUnderline.setWrap(true);

					header = new WritableCellFormat(times10ptBoldUnderline);
					headerBold = new WritableCellFormat(times10ptBoldUnderline);
					CellView cv = new CellView();
					cv.setFormat(times);
					cv.setFormat(timesBoldUnderline);
					cv.setAutosize(true);

					header.setBackground(Colour.GRAY_25);

					// Write a few headers
					excelSheet.mergeCells(0, 0, 12, 1);
					Label label;
					label = new Label(0, 0, Utility.getLocaleValuePropByKey("msgHiredEEOC", locale), timesBoldUnderline);
					excelSheet.addCell(label);
					excelSheet.mergeCells(0, 3, 12, 3);
					label = new Label(0, 3, "");
					excelSheet.addCell(label);
					excelSheet.getSettings().setDefaultColumnWidth(18);
					
					int k=4;
					int col=1;
					label = new Label(0, k, Utility.getLocaleValuePropByKey("lblDistrictName", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(1, k, Utility.getLocaleValuePropByKey("lblJoTil", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJoStatus", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgNoApplicant", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgResponded", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgNoofResponded", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgBlack", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgAsian", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgHispanic", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgWhite", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgAmricanIndian", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgHawaiian", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgMulti", locale),header); 
					excelSheet.addCell(label);
					 
					
					k=k+1;
					if(finallstHiredEeoc.size()==0)
					{	
						excelSheet.mergeCells(0, k, 9, k);
						label = new Label(0, k, Utility.getLocaleValuePropByKey("lblNoRecord", locale)); 
						excelSheet.addCell(label);
					}
					String sta="";
			if(finallstHiredEeoc.size()>0){
				 for (Iterator it = finallstHiredEeoc.iterator(); it.hasNext();) 					            
					{
					String[] row = (String[]) it.next();
				col=1;
			
						label = new Label(0, k,row[1].toString()); 
						excelSheet.addCell(label);
				
						label = new Label(1, k, row[2].toString()); 
						excelSheet.addCell(label);
				
						
						if(row[13].toString().equalsIgnoreCase("A"))
						{
							sta=Utility.getLocaleValuePropByKey("optAct", locale);
						}
						else
						{
							sta=Utility.getLocaleValuePropByKey("lblInActiv", locale);
						}
						
						 //String  myVal1 = (row[3]==null)? "N/A" : row[3].toString();
							label = new Label(++col, k, sta); 
							excelSheet.addCell(label);
				 
					 String  myVal1 = (row[3]==null)? "N/A" : row[3].toString();
					label = new Label(++col, k, myVal1); 
					excelSheet.addCell(label);
					
					 String  myVal2 = ((row[4]==null)||(row[4].toString().length()==0))? "0.00" : row[4].toString();
					label = new Label(++col, k, myVal2+"%"); 
					excelSheet.addCell(label);
					
					 String  myVal3 = ((row[5]==null)||(row[5].toString().length()==0))? "N/A" : row[5].toString();
					label = new Label(++col, k, myVal3); 
					excelSheet.addCell(label);
					
					 String  myVal4 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString()+"%";
					label = new Label(++col, k, myVal4); 
					excelSheet.addCell(label);
					
					String  myVal7 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString()+"%";
					label = new Label(++col, k, myVal7); 
					excelSheet.addCell(label);
					
					String  myVal8 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString()+"%";
					label = new Label(++col, k, myVal8); 
					excelSheet.addCell(label);
					
					String  myVal9 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString()+"%";
					label = new Label(++col, k, myVal9); 
					excelSheet.addCell(label);
					
					String  myVal10 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString()+"%";
					label = new Label(++col, k, myVal10); 
					excelSheet.addCell(label);
					
					String  myVal11 = ((row[11]==null)||(row[11].toString().length()==0))? " " : row[11].toString()+"%";
					label = new Label(++col, k, myVal11); 
					excelSheet.addCell(label);
					
					String  myVal12 = ((row[12]==null)||(row[12].toString().length()==0))? " " : row[12].toString()+"%";
					label = new Label(++col, k, myVal12); 
					excelSheet.addCell(label);
					
									
								
				   k++; 
			 }
			}
	
			workbook.write();
			workbook.close();
		}catch(Exception e){}
		
	 return fileName;
  }
	
  
	
	public String displayRecordsByEntityTypePDF(String districtName,String jobStatus,int districtOrSchoolId,String fJobStatus,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate,String jobendstartDate,String jobendendDate)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport( districtName, jobStatus, districtOrSchoolId, fJobStatus, noOfRow, pageNo, sortOrder, sortOrderType,startDate,endDate,jobendstartDate,jobendendDate,basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	public boolean generateCandidatePDReport(String districtName,String jobStatus,int districtOrSchoolId,String fJobStatus,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate,String jobendstartDate,String jobendendDate,String path,String realPath)
	{
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			System.out.println(jobStatus+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			int sortingcheck=1;
			try{
				
				
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				
				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"districtName";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<String[]> lstHiredEeoc =new ArrayList<String[]>();
					
					
						if(entityID==2||entityID==3)
						{
							//System.out.println("inside entity id=2");
							lstHiredEeoc =teacherPersonalInfoDAO.hiredEeoc(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
					    	totalRecord = lstHiredEeoc.size();
					    	System.out.println("totallllllllllll========="+totalRecord);
					    	 
						}
						else if(entityID==1) 
						{
							System.out.println("indside admin ");
							if(districtOrSchoolId!=0)
							{
								lstHiredEeoc =teacherPersonalInfoDAO.hiredEeoc(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
						    	totalRecord = lstHiredEeoc.size();
						    	System.out.println("totallllllllllll========="+totalRecord);
							}
							else
							{
							lstHiredEeoc =teacherPersonalInfoDAO.hiredEeocForAdmin(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
					    	totalRecord = lstHiredEeoc.size();
					    	System.out.println("totallllllllllll========="+totalRecord);
							}
					    	
						}
						
						
						List<String[]> finallstHiredEeoc =new ArrayList<String[]>();
					     if(totalRecord<end)
								end=totalRecord;
					     finallstHiredEeoc=lstHiredEeoc;

								
			 // System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
				 String fontPath = realPath;
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject(Utility.getLocaleValuePropByKey("msgHiredEEOC", locale));
						document.addCreationDate();
						document.addTitle(Utility.getLocaleValuePropByKey("msgHiredEEOC", locale));

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						

						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgHiredEEOC", locale),font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						
						
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph( Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						document.add(new Phrase("\n"));


						float[] tblwidth={.15f,.15f,.05f,.06f,.08f,.08f,.05f,.05f,.06f,.05f,.06f,.06f,.04f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[13];
						cell = new PdfPCell[13];
				// header
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblDistrictName", locale),font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJoTil", locale),font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJoStatus", locale),font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[2]);
						
						para[3] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgNoApplicant", locale),font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[3]);

						para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgResponded", locale),font10_10);
						cell[4]= new PdfPCell(para[4]);
						cell[4].setBackgroundColor(bluecolor);
						cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[4]);
						
						para[5] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgNoofResponded", locale),font10_10);
						cell[5]= new PdfPCell(para[5]);
						cell[5].setBackgroundColor(bluecolor);
						cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[5]);
						
						para[6] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgBlack", locale),font10_10);
						cell[6]= new PdfPCell(para[6]);
						cell[6].setBackgroundColor(bluecolor);
						cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[6]);
						
						para[7] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgAsian", locale),font10_10);
						cell[7]= new PdfPCell(para[7]);
						cell[7].setBackgroundColor(bluecolor);
						cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[7]);
						
						para[8] = new Paragraph(""+ Utility.getLocaleValuePropByKey("msgHispanic", locale),font10_10);
						cell[8]= new PdfPCell(para[8]);
						cell[8].setBackgroundColor(bluecolor);
						cell[8].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[8]);
						
						para[9] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgWhite", locale),font10_10);
						cell[9]= new PdfPCell(para[9]);
						cell[9].setBackgroundColor(bluecolor);
						cell[9].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[9]);
						
						para[10] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgAmricanIndian", locale),font10_10);
						cell[10]= new PdfPCell(para[10]);
						cell[10].setBackgroundColor(bluecolor);
						cell[10].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[10]);
												
						
						para[11] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgHawaiian", locale),font10_10);
						cell[11]= new PdfPCell(para[11]);
						cell[11].setBackgroundColor(bluecolor);
						cell[11].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[11]);
						document.add(mainTable);
						
						para[12] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgMulti", locale),font10_10);
						cell[12]= new PdfPCell(para[12]);
						cell[12].setBackgroundColor(bluecolor);
						cell[12].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[12]);
						document.add(mainTable);
						
						if(finallstHiredEeoc.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblNoRecord", locale),font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
				     String sta="";
						if(finallstHiredEeoc.size()>0){
							for (Iterator it = finallstHiredEeoc.iterator(); it.hasNext();) 					            
							 {
								String[] row = (String[]) it.next();
							  int index=0;
							 									
							  mainTable = new PdfPTable(tblwidth);
							  mainTable.setWidthPercentage(100);
							  para = new Paragraph[13];
							  cell = new PdfPCell[13];
							
                              String myVal1="";
                              String myVal2="";
                              
                                                           
							  
                            //System.out.println("row[0]"+row[0]);
								 
								 /*myVal1 = (row[0]==null)? "N/A" : row[0].toString();
								 para[index] = new Paragraph( row[0].toString(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 System.out.println("mayval1111"+myVal1);*/
								
								 
								 myVal1 = (row[1]==null)? "N/A" : row[1].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? "N/A" : row[2].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 if(row[13].toString().equalsIgnoreCase("A"))
									{
										sta= Utility.getLocaleValuePropByKey("optAct", locale);
									}
									else
									{
										sta= Utility.getLocaleValuePropByKey("optInActiv", locale) ;
									}
								 
								 //myVal1 = (row[3]==null)? "N/A" : row[3].toString();
								 para[index] = new Paragraph(sta,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 
								 
								 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[4]==null)? "N/A" : row[4].toString();
								 para[index] = new Paragraph(myVal1+"%",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString()+"%";
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString()+"%";
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString()+"%";
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString()+"%";
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								
								 myVal1 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString()+"%";
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[11]==null)||(row[11].toString().length()==0))? " " : row[11].toString()+"%";
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = ((row[12]==null)||(row[12].toString().length()==0))? " " : row[12].toString()+"%";
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 
								

						document.add(mainTable);
				   }
				}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	
	public String displayRecordsByEntityTypePrintPreview(String districtName,String jobStatus,int districtOrSchoolId,String fJobStatus,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate,String jobendstartDate,String jobendendDate)
	{
		System.out.println(jobStatus+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
		
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"districtName";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<String[]> lstHiredEeoc =new ArrayList<String[]>();
				
				
					if(entityID==2||entityID==3)
					{
						//System.out.println("inside entity id=2");
						lstHiredEeoc =teacherPersonalInfoDAO.hiredEeoc(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,false,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
				    	totalRecord = lstHiredEeoc.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
				    	 
					}
					else if(entityID==1) 
					{
						System.out.println("indside admin ");
						if(districtOrSchoolId!=0)
						{
							lstHiredEeoc =teacherPersonalInfoDAO.hiredEeoc(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
					    	totalRecord = lstHiredEeoc.size();
					    	System.out.println("totallllllllllll========="+totalRecord);
						}
						else
						{
						lstHiredEeoc =teacherPersonalInfoDAO.hiredEeocForAdmin(districtOrSchoolId,jobStatus,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
				    	totalRecord = lstHiredEeoc.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
						}
				    	
					}
					
					
					
					List<String[]> finallstHiredEeoc =new ArrayList<String[]>();
				     if(totalRecord<end)
							end=totalRecord;
				     finallstHiredEeoc=lstHiredEeoc;
				
			

	        tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>"+Utility.getLocaleValuePropByKey("msgHiredEEOC", locale)+"</div><br/>");
			
			tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+ Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");
			
			
			tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblDistrictName", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoTil", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoStatus", locale)+"</th>");
    
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgNoApplicant", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgResponded", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgNoofResponded", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgBlack", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgAsian", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgHispanic", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgWhite", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgAmricanIndian", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgHawaiian", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgMulti", locale)+"</th>");
			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(finallstHiredEeoc.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			}
		String sta="";
			if(finallstHiredEeoc.size()>0){
				 for (Iterator it = finallstHiredEeoc.iterator(); it.hasNext();) 					            
					{
					String[] row = (String[]) it.next();
					
					 String myVal1="";
                     String myVal2="";
					  
                   
					 tmRecords.append("<tr>");
					 
					 	
					 
					 
					 /*myVal1 = (row[0]==null)? "N/A" : row[0].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");*/
					 
					
						
						 myVal1 = (row[1]==null)? "N/A" : row[1].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = (row[2]==null)? "N/A" : row[2].toString();
							tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
							
							if(row[13].toString().equalsIgnoreCase("A"))
							{
								sta="Active";
							}
							else
							{
								sta="Inactive";
							}
							
							// myVal1 = (row[3]==null)? "N/A" : row[3].toString();
								tmRecords.append("<td style='font-size:12px;'>"+sta+"</td>");
							
							 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
								tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = ((row[4]==null)||(row[4].toString().length()==0))? "N/A" : row[4].toString();
			      		tmRecords.append("<td style='font-size:12px;'>"+ myVal1+"%"+"</td>");	
			      		
			      		 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString()+"%";
							tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
							
							 myVal1 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString()+"%";
								tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
								
								 myVal1 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString()+"%";
									tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
									
									 myVal1 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString()+"%";
										tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
										
										 myVal1 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString()+"%";
											tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
											
											 myVal1 = ((row[11]==null)||(row[11].toString().length()==0))? " " : row[11].toString()+"%";
												tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
												
												 myVal1 = ((row[12]==null)||(row[12].toString().length()==0))? " " : row[12].toString()+"%";
													tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
													
					   tmRecords.append("</tr>");
			   }
			}
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
  }
	
}


