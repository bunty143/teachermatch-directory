
package tm.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.EmployeeMaster;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.CityMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.textfile.License;
import tm.bean.textfile.LicenseArea;
import tm.bean.textfile.LicenseAreaDomain;
import tm.bean.textfile.LicenseClassLevelDomain;
import tm.bean.textfile.LicenseStatusDomain;
import tm.bean.textfile.LicensureNBPTS;
import tm.bean.textfile.NBPTSAreaDomain;
import tm.bean.user.UserMaster;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.textfile.LicenseAreaDAO;
import tm.dao.textfile.LicenseAreaDomainDAO;
import tm.dao.textfile.LicenseClassLevelDomailDAO;
import tm.dao.textfile.LicenseDAO;
import tm.dao.textfile.LicenseStatusDomainDAO;
import tm.dao.textfile.LicensureNBPTSDao;
import tm.dao.textfile.NBPTSAreaDomainDAO;
import tm.services.teacher.PFLicenseEducation;
import tm.utility.Utility;

public class HrmsLicenseAjax 
{		
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private LicenseDAO licenseDAO;
	public void setLicenseDAO(LicenseDAO licenseDAO) {
		this.licenseDAO = licenseDAO;
	}

	@Autowired
	private StateMasterDAO stateMasterDAO;	
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	@Autowired
	private LicenseAreaDAO licenseAreaDAO;	
	public void setLicenseAreaDAO(LicenseAreaDAO licenseAreaDAO) {
		this.licenseAreaDAO = licenseAreaDAO;
	}
	@Autowired
	private LicenseAreaDomainDAO licenseAreaDomainDAO;
	public void setLicenseAreaDomainDAO(LicenseAreaDomainDAO licenseAreaDomainDAO) {
		this.licenseAreaDomainDAO = licenseAreaDomainDAO;
	}
	@Autowired
	private LicenseStatusDomainDAO licenseStatusDomainDAO;
	public void setLicenseStatusDomainDAO(LicenseStatusDomainDAO licenseStatusDomainDAO) {
		this.licenseStatusDomainDAO = licenseStatusDomainDAO;
	}

	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	public void setTeacherCertificateDAO(TeacherCertificateDAO teacherCertificateDAO) {
		this.teacherCertificateDAO = teacherCertificateDAO;
	}

	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
	public void setCertificateTypeMasterDAO(CertificateTypeMasterDAO certificateTypeMasterDAO) {
		this.certificateTypeMasterDAO = certificateTypeMasterDAO;
	}
	
	@Autowired LicensureNBPTSDao licensureNBPTSDao;
	
	@Autowired 
	private EmployeeMasterDAO employeeMasterDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	
	@Autowired
	private LicenseClassLevelDomailDAO licenseClassLevelDomailDAO;
	
	@Autowired
	private NBPTSAreaDomainDAO nbptsAreaDomainDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;

	public String displayRecordsBySSN(String ssn,String affCheck,String jobId)
	{
				
		StringBuffer sb=new StringBuffer();		

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try
		{
		JobOrder jobOrder=null;
		List<EmployeeMaster> emplMaster=null;
		if((session.getAttribute("Currnrt")==null || session.getAttribute("Currnrt")=="") && (affCheck.equalsIgnoreCase("true") || affCheck.equalsIgnoreCase("1") ))
		{
		    if(jobId!=null && jobId!="")
			jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
		    if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && ssn!=null && !ssn.equals("") )
		    emplMaster=employeeMasterDAO.getTeacherInfoListBySsn(ssn,jobOrder.getDistrictMaster());
		    if(emplMaster!=null && emplMaster.size()>0)
		    {
		    	for(EmployeeMaster empmaster:emplMaster)
		    	{
		    		if(empmaster.getEmploymentStatus().equalsIgnoreCase("c"))
		    		{			    			
		    			session.setAttribute("Currnrt", empmaster.getEmploymentStatus());			    			
		    		}
		    		else if(empmaster.getEmploymentStatus().equalsIgnoreCase("x"))
		    		{			    			
		    			session.setAttribute("Currnrt", empmaster.getEmploymentStatus());			    			
		    		}			    		
		    	}
		    }
		    else
		    {
		    	session.setAttribute("Currnrt","");
		    }
			
		}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		
		TeacherDetail teacherDetail = null;			
		TeacherPersonalInfo teacherPersonalInfo=null;
		if (session == null || session.getAttribute("teacherDetail") == null) {
			return "false";
		}else{
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		}

		if(teacherDetail!=null)
		{
			teacherPersonalInfo=teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		}
		
		if(teacherPersonalInfo!=null)
		{			
			try {				
				ssn=Utility.decodeBase64(teacherPersonalInfo.getSSN());
			} catch (IOException e) {				
				e.printStackTrace();
			}
		}
		

		List<License> ssnList=new ArrayList<License>();
		List<StateMaster> stateFullName = null;
		List<LicenseArea> licenseAreaList=null;
		List<LicenseAreaDomain> licenseAreaDomainList=null;
		List<LicenseStatusDomain> licenseStatusDomainList=null;
		List<CertificateTypeMaster> lstcertificatetypemaster=null;
		try{	
									
			String passYear="";
			String finalDate="";
			String certName="";
			String certStatus="";
			String stateName="";

			sb.append("<table border='0'  id='divLicneseGridCertifications' class='table  table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			
			sb.append("<th width='30%' valign='top'>Name</th>");
			sb.append("<th width='30%' valign='top'>State</th>");
			sb.append("<th width='20%' valign='top'>Year received</th>");
			sb.append("<th width='20%' valign='top'>Status</th>");


			sb.append("</tr>");
			sb.append("</thead>");

						//get license area code on the basis of ssn for get certificate name
			Criterion statusArea = Restrictions.eq("status", "A");
			Criterion ssnArea = Restrictions.eq("SSN", ssn);
			licenseAreaList=licenseAreaDAO.findByCriteria(statusArea,ssnArea);

			//get certification name on the basis of license area code
			if((licenseAreaList!=null)&&(licenseAreaList.size()>0))
			{
				
				//get state full name
				Criterion active = Restrictions.eq("status", "A");
				Criterion name = Restrictions.eq("stateShortName", "NC");
				stateFullName = stateMasterDAO.findByCriteria(active,name);
				stateName=stateFullName.get(0).getStateName();	
				
				
				
				Criterion statusAreaDomain = Restrictions.eq("status", "A");
				Criterion ssnAreaDomain = Restrictions.eq("licenseAreaCode", licenseAreaList.get(0).getLicenseArea());
				licenseAreaDomainList=licenseAreaDomainDAO.findByCriteria(statusAreaDomain,ssnAreaDomain);
				if(licenseAreaDomainList!=null)
					certName=licenseAreaDomainList.get(0).getLicenseAreaDescription()==null?"":licenseAreaDomainList.get(0).getLicenseAreaDescription();
			

			//get list of certification status
			if((licenseAreaList!=null)&&(licenseAreaList.size()>0))
			{
				Criterion LicenseStatusDomain = Restrictions.eq("status", "A");
				Criterion statusAreaStatus = Restrictions.eq("programStatusCode", licenseAreaList.get(0).getLicenseProgramStatusCode());
				licenseStatusDomainList=licenseStatusDomainDAO.findByCriteria(LicenseStatusDomain,statusAreaStatus);
				if(licenseStatusDomainList!=null)
					certStatus=licenseStatusDomainList.get(0).getProgramStatusDescription()==null?"":licenseStatusDomainList.get(0).getProgramStatusDescription();
			}

			if((licenseAreaList!=null)&&(licenseAreaList.size()>0))
			{
				if(licenseAreaList.get(0).getLicenseAreaEffectiveDate()!=null)
				{
					passYear=licenseAreaList.get(0).getLicenseAreaEffectiveDate().toString();
					finalDate=passYear.substring(0, 4);
				}
			}
			
			sb.append("<tr >");
			sb.append("<td>");
			sb.append(certName);
			sb.append("</td>");

			sb.append("<td>");
			sb.append(stateName);
			sb.append("</td>");

			sb.append("<td>");
			sb.append(finalDate);
			sb.append("</td>");

			sb.append("<td>");
			sb.append(certStatus);
			sb.append("</td>");

			sb.append("</tr>");
			}
		
		if((licenseAreaList==null)||(licenseAreaList.size()==0))
		{
			sb.append("<tr>");
			sb.append("<td colspan='4'>");
			sb.append(Utility.getLocaleValuePropByKey("NcdpinoLicense", locale));
			sb.append("</td>");
			sb.append("</tr>");
			
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();	
	}

	public String getNBCLicense(String ssn){
		String result="";
		StringBuffer sb=new StringBuffer();		

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		TeacherDetail teacherDetail = null;			
		TeacherPersonalInfo teacherPersonalInfo=null;
		if (session == null || session.getAttribute("teacherDetail") == null) {
			return "false";
		}else{
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		}

		if(teacherDetail!=null)
		{
			teacherPersonalInfo=teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		}
		
		if(teacherPersonalInfo!=null)
		{			
			try {				
				ssn=Utility.decodeBase64(teacherPersonalInfo.getSSN());
			} catch (IOException e) {				
				e.printStackTrace();
			}
		}
		
		try{
			
			List<String> ssnList = new ArrayList<String>();
			List<NBPTSAreaDomain> nbptsAreaDomains=new ArrayList<NBPTSAreaDomain>();
			ssnList.add(Utility.encodeInBase64(ssn));
			
			List<LicensureNBPTS> teacherLicense = licensureNBPTSDao.getDataBySsnList(ssnList);
						
			sb.append("<table border='0'  id='divLicneseNBPTSCertifications' class='table  table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");		
			sb.append("<th width='30%' valign='top'>Name</th>");
			sb.append("<th width='30%' valign='top'>Effective Date</th>");
			sb.append("<th width='20%' valign='top'>Expiration Date</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			if(teacherLicense!=null && teacherLicense.size()>0){
				
				if(teacherLicense.get(0).getNbotsAreaCode()!=null)
				nbptsAreaDomains=nbptsAreaDomainDAO.getDataByNBPTSAreaCode(teacherLicense.get(0).getNbotsAreaCode());
								
				for(LicensureNBPTS listNBPTS:teacherLicense)
				{					
					sb.append("<tr >");
					sb.append("<td>");
					if(nbptsAreaDomains!=null && nbptsAreaDomains.size()>0 )
					{
						for(NBPTSAreaDomain nad:nbptsAreaDomains)
						{
						sb.append(nad.getNbptsAreaDescription());
						}
					}
					else
					{
						sb.append("");	
					}
					
					sb.append("</td>");

					sb.append("<td>");
					if(listNBPTS.getEffectiveDate()!=null && !listNBPTS.getEffectiveDate().equals(""))
					{
					sb.append(Utility.convertDateAndTimeToDatabaseformatOnlyDate(listNBPTS.getEffectiveDate()));
					}
					else
					{
						sb.append("N/A");	
					}
					sb.append("</td>");

					sb.append("<td>");
					if(listNBPTS.getExpirationDate()!=null && !listNBPTS.getExpirationDate().equals(""))
					{
					sb.append(Utility.convertDateAndTimeToDatabaseformatOnlyDate(listNBPTS.getExpirationDate()));
					}
					else
					{
						sb.append("N/A");
					}
					sb.append("</td>");					
				}				
			}
			if((teacherLicense==null)||(teacherLicense.size()==0))
			{
				sb.append("<tr>");
				sb.append("<td colspan='3'>");
				sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
				sb.append("</td>");
				sb.append("</tr>");
				
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	
	////////////
	public List<EmployeeMaster> displayTeacherInfoSSN(String ssn,Integer DistrictIdd)
	{
		System.out.println("Hrms License Ajax Calll freom teacher info::::::::::::::::"+ssn+"And District Iddddd:===="+DistrictIdd);

		StringBuffer sb=new StringBuffer();		

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		List<EmployeeMaster> employeeMasterList=null;
		DistrictMaster districtMaster=null;
		StateMaster stateMaster=null;
		List<CityMaster> cityMasterlist=null;
		CityMaster cityMaster = null;
		
		try{	
			if(DistrictIdd!=null)
			districtMaster=districtMasterDAO.findById(DistrictIdd, false, false);
						
			if(districtMaster!=null)
			employeeMasterList = employeeMasterDAO.getTeacherInfoListBySsn(ssn,districtMaster);
			
			
			
			if(employeeMasterList!=null && employeeMasterList.size()>0)
			{				
				stateMaster=employeeMasterList.get(0).getStateMaster();
								
				if(stateMaster!=null)
				{
					String cityName=employeeMasterList.get(0).getCityName();
					cityMasterlist=cityMasterDAO.findCityByNameAndState(cityName,stateMaster);
					if(cityMasterlist!=null && cityMasterlist.size()>0)
					{
					cityMaster = cityMasterlist.get(0);
					employeeMasterList.get(0).setCityName(cityMaster.getCityName());
					}
									
				}
						
			}
			
		    }catch(Exception e){
			e.printStackTrace();
	    	}
		if(employeeMasterList!=null && employeeMasterList.size()>0)
		{
		return employeeMasterList;	
		}
		else
		{
			return null;
		}
	}
	
	public String displayRecordsBySSNForDate(String ssn)
	{
		
		StringBuffer sb=new StringBuffer();		

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		TeacherDetail teacherDetail = null;			
		TeacherPersonalInfo teacherPersonalInfo=null;
		if (session == null || session.getAttribute("teacherDetail") == null) {
			return "false";
		}else{
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		}

		if(teacherDetail!=null)
		{
			teacherPersonalInfo=teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		}
		
		if(teacherPersonalInfo!=null)
		{			
			try {				
				ssn=Utility.decodeBase64(teacherPersonalInfo.getSSN());
			} catch (IOException e) {				
				e.printStackTrace();
			}
		}
		

		List<License> ssnList=new ArrayList<License>();
		List<StateMaster> stateFullName = null;
		List<License> licenseList=null;
		List<LicenseAreaDomain> licenseAreaDomainList=null;
		List<LicenseStatusDomain> licenseStatusDomainList=null;
		List<CertificateTypeMaster> lstcertificatetypemaster=null;
		try{	

					
			String stateName="";
			String issueDate="";
			String effectiveDate="";
			String expirationDate="";			

			sb.append("<table border='0'  id='divLicneseGridCertificationsGridD' class='table  table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			
			sb.append("<th width='33%'  valign='top'>State</th>");
			sb.append("<th width='33%'  valign='top'>Effective Date</th>");
			sb.append("<th width='34%'  valign='top'>Expiration Date</th>");


			sb.append("</tr>");
			sb.append("</thead>");

				
			Criterion statusArea = Restrictions.eq("status", "A");
			Criterion ssnArea = Restrictions.eq("SSN", ssn);
			licenseList=licenseDAO.findByCriteria(statusArea,ssnArea);

		
			if((licenseList!=null)&&(licenseList.size()>0))
			{				
				//get state full name
				Criterion active = Restrictions.eq("status", "A");
				Criterion name = Restrictions.eq("stateShortName", "NC");
				stateFullName = stateMasterDAO.findByCriteria(active,name);
				stateName=stateFullName.get(0).getStateName();	
				
				if(licenseList.get(0).getLicenseEffectiveDate()!=null)
				{
				effectiveDate=Utility.convertDateAndTimeToDatabaseformatOnlyDate(licenseList.get(0).getLicenseEffectiveDate()).toString();
				}
				else
				{
					effectiveDate="N/A";
				}
				
				if(licenseList.get(0).getLicenseExpireDate()!=null)
				{
				expirationDate=Utility.convertDateAndTimeToDatabaseformatOnlyDate(licenseList.get(0).getLicenseExpireDate()).toString();
				}
				else
				{
					expirationDate="N/A";
				}
			sb.append("<tr >");
			sb.append("<td>");
			sb.append(stateName);
			sb.append("</td>");

			sb.append("<td>");
			sb.append(effectiveDate);
			sb.append("</td>");

			sb.append("<td>");
			sb.append(expirationDate);
			sb.append("</td>");

			sb.append("</tr>");
			}
		
		if((licenseList==null)||(licenseList.size()==0))
		{
			sb.append("<tr>");
			sb.append("<td colspan='3'>");
			sb.append(Utility.getLocaleValuePropByKey("NcdpinoLicense", locale));			
			sb.append("</td>");
			sb.append("</tr>");
			
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	public String displayRecordsBySSNForArea(String ssn)
	{
		
		StringBuffer sb=new StringBuffer();		

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		TeacherDetail teacherDetail = null;			
		TeacherPersonalInfo teacherPersonalInfo=null;
		if (session == null || session.getAttribute("teacherDetail") == null) {
			return "false";
		}else{
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		}

		if(teacherDetail!=null)
		{
			teacherPersonalInfo=teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		}
		
		if(teacherPersonalInfo!=null)
		{			
			try {				
				ssn=Utility.decodeBase64(teacherPersonalInfo.getSSN());
			} catch (IOException e) {				
				e.printStackTrace();
			}
		}

		List<License> ssnList=new ArrayList<License>();
		List<StateMaster> stateFullName = null;
		List<LicenseArea> licenseAreaList=null;
		List<LicenseAreaDomain> licenseAreaDomainList=null;
		List<LicenseStatusDomain> licenseStatusDomainList=null;
		List<CertificateTypeMaster> lstcertificatetypemaster=null;
		List<LicenseClassLevelDomain> lstClassLevelDomains=null;
		try{			
			String effectivedate="";
			String yearExp="";
			String certName="";
			String certShortName="";
			String name="";

			sb.append("<table border='0'  id='divLicneseGridCertificationsGridA' class='table  table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			
			sb.append("<th width='30%' valign='top'>Name</th>");
			sb.append("<th width='30%' valign='top'>Effective Date</th>");
			sb.append("<th width='20%' valign='top'>Class</th>");
			sb.append("<th width='20%' valign='top'>Year Exp.</th>");


			sb.append("</tr>");
			sb.append("</thead>");

						//get license area code on the basis of ssn for get certificate name
			Criterion statusArea = Restrictions.eq("status", "A");
			Criterion ssnArea = Restrictions.eq("SSN", ssn);
			licenseAreaList=licenseAreaDAO.findByCriteria(statusArea,ssnArea);

			//get certification name on the basis of license area code
			if((licenseAreaList!=null)&&(licenseAreaList.size()>0))
			{
				for(LicenseArea listArea:licenseAreaList)
				{
				//get certificate name
				Criterion statusAreaDomain = Restrictions.eq("status", "A");
				Criterion ssnAreaDomain = Restrictions.eq("licenseAreaCode",listArea.getLicenseArea());
				licenseAreaDomainList=licenseAreaDomainDAO.findByCriteria(statusAreaDomain,ssnAreaDomain);
				if(licenseAreaDomainList!=null && licenseAreaDomainList.size()>0 )
					certName=licenseAreaDomainList.get(0).getLicenseAreaDescription()==null?"":licenseAreaDomainList.get(0).getLicenseAreaDescription();
				
				
				if(listArea.getLicenseAreaEffectiveDate()!=null)
				{
				effectivedate=Utility.convertDateAndTimeToDatabaseformatOnlyDate(listArea.getLicenseAreaEffectiveDate()).toString();
				}
				else
				{
					effectivedate="N/A";
				}
				
			
				certShortName=listArea.getLicenseClassLevelCode();				
				lstClassLevelDomains=licenseClassLevelDomailDAO.getClassName(certShortName);
				if(lstClassLevelDomains!=null && lstClassLevelDomains.size()>0)
				{
					name=lstClassLevelDomains.get(0).getClassCodeDescription();
				}
				
				yearExp=listArea.getYearsOfExperience();
				
				
			
			sb.append("<tr >");
			sb.append("<td>");
			sb.append(certName);
			sb.append("</td>");

			sb.append("<td>");
			sb.append(effectivedate);
			sb.append("</td>");

			sb.append("<td>");
			sb.append(name);
			sb.append("</td>");

			sb.append("<td>");
			sb.append(yearExp);
			sb.append("</td>");

			sb.append("</tr>");
			}
				sb.append("</table>");
				sb.append("<div>");
				sb.append("If you have an NC license that information is provided by NCDPI. If any licensure data is missing, click Add Certification/Licensure and add it below.");
				sb.append("If any of the NC license data provided by NCDPI is inaccurate, please contact NCDPI Licensure M-F 8-5 EST at 1.800.577.7994 within North Carolina or 1.919.807.3310 outside of North Carolina.");
				sb.append("</div>");
			}
			
		if((licenseAreaList==null)||(licenseAreaList.size()==0))
		{
			sb.append("<tr>");
			sb.append("<td colspan='4'>");
			sb.append(Utility.getLocaleValuePropByKey("NcdpinoLicense", locale));
			sb.append("<input type='hidden' id='ncCertificationCheck' name='ncCertificationCheck' value='1'>");
			sb.append("</td>");
			sb.append("</tr>");
			
		}	
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();	
	}
	public String saveSSNOnSubmitButton(String ssn,String isAffilatedCheck,String jobId)
	{			
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		TeacherDetail teacherDetail = null;		
		if (session == null || session.getAttribute("teacherDetail") == null) {
			return "false";
		  }
		  else
		 {
			  teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");			
		 }
		
		TeacherPersonalInfo teacherPersonalInfo=null;	
		JobOrder jobOrder=null;
		String employeestatus="";
		List<EmployeeMaster> emplMaster=null;
		try
		{
			if(isAffilatedCheck.equalsIgnoreCase("true") || isAffilatedCheck.equalsIgnoreCase("1") )
			{
			    if(jobId!=null && jobId!="")
				jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);			   
			    emplMaster=employeeMasterDAO.getTeacherInfoListBySsn(ssn,jobOrder.getDistrictMaster());
			    if(emplMaster!=null && emplMaster.size()>0)
			    {
			    	for(EmployeeMaster empmaster:emplMaster)
			    	{
			    		if(empmaster.getEmploymentStatus()!=null && empmaster.getEmploymentStatus().equalsIgnoreCase("c"))
			    		{			    			
			    			session.setAttribute("Currnrt", empmaster.getEmploymentStatus());
			    			employeestatus=empmaster.getEmploymentStatus();
			    		}
			    		else if(empmaster.getEmploymentStatus()!=null && empmaster.getEmploymentStatus().equalsIgnoreCase("x"))
			    		{			    			
			    			session.setAttribute("Currnrt", empmaster.getEmploymentStatus());
			    			employeestatus=empmaster.getEmploymentStatus();
			    		}
			    		else
			    		{
			    			employeestatus="NoStatus";
			    		}
			    	}
			    }
			    else
			    {
			    	employeestatus="NoRecordFound";
			    }
				
			}
			
		if(teacherDetail!=null)
		teacherPersonalInfo=teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);		
		if(teacherPersonalInfo!=null)
		{
			teacherPersonalInfo.setSSN(Utility.encodeInBase64(ssn));
			teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
			
		}
		}catch(Exception e)
		{
			
		}
			
		return employeestatus;
	}
	
	public String verifyCandidate(String ssn,String jobId)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		TeacherDetail teacherDetail = null;		
		if (session == null || session.getAttribute("teacherDetail") == null) {
			return "false";
		  }
		  else
		 {
			  teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");			
		 }
		
		TeacherPersonalInfo teacherPersonalInfo=null;	
		JobOrder jobOrder=null;
		String employeestatus="";
		List<EmployeeMaster> emplMaster=null;
		try
		{		
			    if(jobId!=null && jobId!="")
				jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);			   
			    emplMaster=employeeMasterDAO.getTeacherInfoListBySsn(ssn,jobOrder.getDistrictMaster());
			    if(emplMaster!=null && emplMaster.size()>0)
			    {
			    	for(EmployeeMaster empmaster:emplMaster)
			    	{
			    		if(empmaster.getEmploymentStatus()!=null && empmaster.getEmploymentStatus().equalsIgnoreCase("c"))
			    		{			    			
			    			session.setAttribute("Currnrt", empmaster.getEmploymentStatus());
			    			session.setAttribute("Currentforinternal", empmaster.getEmploymentStatus());
			    			employeestatus=empmaster.getEmploymentStatus();
			    		}
			    		else if(empmaster.getEmploymentStatus()!=null && empmaster.getEmploymentStatus().equalsIgnoreCase("x"))
			    		{			    			
			    			session.setAttribute("Currnrt", empmaster.getEmploymentStatus());
			    			employeestatus=empmaster.getEmploymentStatus();
			    		}
			    		else
			    		{
			    			employeestatus="NoStatus";
			    		}
			    	}
			    }
			    else
			    {
			    	employeestatus="NoRecordFound";
			    }
			    if(teacherDetail!=null)
					teacherPersonalInfo=teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
			    if(teacherPersonalInfo==null && teacherDetail!=null && employeestatus.equalsIgnoreCase("c"))
			    {
			    	TeacherPersonalInfo	teacherPersonalInfoNew=new TeacherPersonalInfo();
			    	teacherPersonalInfoNew.setTeacherId(teacherDetail.getTeacherId());
			    	teacherPersonalInfoNew.setSSN(Utility.encodeInBase64(ssn));
			    	teacherPersonalInfoNew.setFirstName(teacherDetail.getFirstName());
			    	teacherPersonalInfoNew.setLastName(teacherDetail.getLastName());
			    	teacherPersonalInfoNew.setIsDone(false);
			    	teacherPersonalInfoNew.setCreatedDateTime(new Date());
			    	teacherPersonalInfoDAO.makePersistent(teacherPersonalInfoNew);
			    
			    } 
			    else if(teacherPersonalInfo!=null && employeestatus.equalsIgnoreCase("c"))
					{
						teacherPersonalInfo.setSSN(Utility.encodeInBase64(ssn));						
						teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
						
					}
		
		}catch(Exception e)
		{
			e.printStackTrace();
		}
			
		return employeestatus;
	
		
	}	
	public String displayRecordsBySSNForSupervisor(String ssn,String jobId)
	{
		
		StringBuffer sb=new StringBuffer();		

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		TeacherDetail teacherDetail = null;			
		if (session == null || session.getAttribute("teacherDetail") == null) {
			return "false";
		}else{
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		}		
		try{	
			JobOrder jobOrder=null;		
			List<EmployeeMaster> emplMaster=null;
			
			 if(jobId!=null && jobId!="")
					jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);			   
				    emplMaster=employeeMasterDAO.getTeacherInfoListBySsn(ssn,jobOrder.getDistrictMaster());			

			sb.append("<table border='0'  id='divSupervisorInfo' class='table  table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");			
			sb.append("<th width='17%'  valign='top'>Name</th>");
			sb.append("<th width='17%'  valign='top'>Email</th>");
			sb.append("<th width='17%'  valign='top'>Phone</th>");
			sb.append("<th width='17%'  valign='top'>Date</th>");
			sb.append("<th width='17%'  valign='top'>Comment</th>");
			sb.append("<th width='17%'  valign='top'>Action</th>");
			sb.append("</tr>");
			sb.append("</thead>");		
			if(emplMaster!=null && emplMaster.size()>0)
			{
				for(EmployeeMaster emp:emplMaster)
				{
					if(emp.getSupervisorFirstName()!=null)
					{
			sb.append("<tr >");
			sb.append("<td>");
			sb.append(emp.getSupervisorFirstName()+" "+emp.getSupervisorLastName());
			sb.append("</td>");

			sb.append("<td>");
			if(emp.getSupervisorEmail()!=null)
			{
				sb.append(emp.getSupervisorEmail());	
			}
			else
			{
				sb.append("N/A");
			}			
			sb.append("</td>");

			sb.append("<td>");
			if(emp.getSupervisorPhone()!=null)
			{
				sb.append(emp.getSupervisorPhone());
			}
			else
			{
				sb.append("N/A");
			}			
			sb.append("</td>");
			
			sb.append("<td>");
			sb.append(Utility.getDateWithoutTime(emp.getSupervisorAvailDate()));
			sb.append("</td>");
			
			sb.append("<td>");
			if(emp.getSuperVisorComment()!=null)
			{
			sb.append(emp.getSuperVisorComment());
			}
			else
			{
				sb.append("");
			}
			sb.append("</td>");			
			
			sb.append("<td>");
			sb.append("<a href='#' onclick=\"return editSupervisorForm("+emp.getSSN()+")\" >Edit</a>");
			
			sb.append("</td>");

			sb.append("</tr>");
				}
					else
					{
						sb.append("<tr>");
						sb.append("<td colspan='6'>");
						sb.append("No Record Found");			
						sb.append("</td>");
						sb.append("</tr>");	
					}
				}
			}
		
		if(emplMaster==null || emplMaster.size()==0)
		{	sb.append("<tr>");
			sb.append("<td colspan='6'>");
			sb.append("No Record Found");			
			sb.append("</td>");
			sb.append("</tr>");			
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	public String internalCandidateForExternalLink(String ssn , String jobId)
	{

		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);		
		JobOrder jobOrder=null;
		String employeestatus="";
		List<EmployeeMaster> emplMaster=null;
		try
		{		
			    if(jobId!=null && jobId!="")
				jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);			   
			    emplMaster=employeeMasterDAO.getTeacherInfoListBySsn(ssn,jobOrder.getDistrictMaster());
			    if(emplMaster!=null && emplMaster.size()>0)
			    {
			    	for(EmployeeMaster empmaster:emplMaster)
			    	{
			    		if(empmaster.getEmploymentStatus()!=null && empmaster.getEmploymentStatus().equalsIgnoreCase("c"))
			    		{			    			
			    			session.setAttribute("Currnrt", empmaster.getEmploymentStatus());
			    			employeestatus=empmaster.getEmploymentStatus();
			    			session.setAttribute("ssnNcInternal", ssn);
			    		}			    		
			    		else
			    		{
			    			employeestatus="NoStatus";
			    		}
			    	}
			    }
			    else
			    {
			    	employeestatus="NoRecordFound";
			    }		   
		
		}catch(Exception e)
		{
			
		}
			
		return employeestatus;	
	
	}
	public String addSupervisor(String ssn,String jobId,String fname,String lname,String email,String phone,String comment,String date)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		TeacherDetail teacherDetail = null;			
		TeacherPersonalInfo teacherPersonalInfo=null;
		if (session == null || session.getAttribute("teacherDetail") == null) {
			return "false";
		}else{
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		}
		JobOrder jobOrder=null;		
		EmployeeMaster emplMaster=null;
		
		 if(jobId!=null && jobId!="")
				jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);			   
			    emplMaster=employeeMasterDAO.getTeacherInfoListBySsn(ssn,jobOrder.getDistrictMaster()).get(0);			   
			    if(emplMaster!=null)
			    {
			    	try
			    	{
			    	emplMaster.setEmployeeId(emplMaster.getEmployeeId());
			    	emplMaster.setSupervisorFirstName(fname);			    	
			    	emplMaster.setSupervisorLastName(lname);			    				    	
			    	emplMaster.setSupervisorEmail(email);			    	
			    	emplMaster.setSupervisorPhone(phone);			    	
			    	emplMaster.setSupervisorAvailDate(Utility.getCurrentDateFormart(date));			    	
			    	emplMaster.setSuperVisorComment(comment);
			    	employeeMasterDAO.makePersistent(emplMaster);
			    	}catch (Exception e) {
						e.printStackTrace();
					}
			    	
			    }
		return null;
	}
	
	public String displayRecordsBySSNForEmployee(String ssn,String jobId)
	{
		
		StringBuffer sb=new StringBuffer();		

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		TeacherDetail teacherDetail = null;			
		if (session == null || session.getAttribute("teacherDetail") == null) {
			return "false";
		}else{
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		}		
		try{	
			JobOrder jobOrder=null;		
			List<EmployeeMaster> emplMaster=null;
			
			 if(jobId!=null && jobId!="")
					jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);			   
				    emplMaster=employeeMasterDAO.getTeacherInfoListBySsn(ssn,jobOrder.getDistrictMaster());			

			sb.append("<table border='0'  id='divEmployeeInfo' class='table  table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");			
			sb.append("<th width='25%'  valign='top'>Phone Number</th>");
			sb.append("<th width='25%'  valign='top'>Email Address</th>");
			sb.append("<th width='25%'  valign='top'>Current Location Site</th>");
			sb.append("<th width='25%'  valign='top'>Current Postion Title</th>");			
			sb.append("</tr>");
			sb.append("</thead>");		
			if(emplMaster!=null && emplMaster.size()>0)
			{
				for(EmployeeMaster emp:emplMaster)
				{
			sb.append("<tr >");
			sb.append("<td>");
			if(emp.getPhone()!=null)
			{
				sb.append(emp.getPhone());	
			}
			else
			{
				sb.append("");
			}
			
			sb.append("</td>");

			sb.append("<td>");
			if(emp.getEmailAddress()!=null)
			{
				sb.append(emp.getEmailAddress());	
			}
			else
			{
				sb.append(" ");
			}			
			sb.append("</td>");

			sb.append("<td>");			
			sb.append(" ");						
			sb.append("</td>");	
			
			
			sb.append("<td>");
			if(emp.getCurrentPositionName()!=null)
			{
			sb.append(emp.getCurrentPositionName());
			}
			else
			{
				sb.append(" ");
			}
			sb.append("</td>");	
			
			sb.append("</tr>");
				}
			}
		
		if(emplMaster==null || emplMaster.size()==0)
		{
			sb.append("<tr>");
			sb.append("<td colspan='4'>");
			sb.append("No Record Found");			
			sb.append("</td>");
			sb.append("</tr>");
			
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();	
	}
	public EmployeeMaster editSupervisor(String ssn,String jobId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		EmployeeMaster emplMaster=null;
		try{	
			JobOrder jobOrder=null;				
			 if(jobId!=null && jobId!="")
					jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);			   
				    emplMaster=employeeMasterDAO.getTeacherInfoListBySsn(ssn,jobOrder.getDistrictMaster()).get(0);
		}catch (Exception e) {			
		}
		return emplMaster;
	}
}


