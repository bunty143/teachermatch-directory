package tm.services;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.StatusSpecificEmailTemplates;
import tm.bean.StatusWiseEmailSection;
import tm.bean.StatusWiseEmailSubSection;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.cgreport.DistrictMaxFitScore;
import tm.bean.cgreport.JobCategoryWiseStatusPrivilege;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.cgreport.TeacherStatusScores;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusSpecificQuestions;
import tm.bean.master.StatusSpecificScore;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.StatusSpecificEmailTemplatesDAO;
import tm.dao.StatusWiseEmailSectionDAO;
import tm.dao.StatusWiseEmailSubSectionDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.cgreport.DistrictMaxFitScoreDAO;
import tm.dao.cgreport.JobCategoryWiseStatusPrivilegeDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.cgreport.TeacherStatusScoresDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.StatusSpecificQuestionsDAO;
import tm.dao.master.StatusSpecificScoreDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.services.district.PrintOnConsole;
import tm.services.report.CandidateGridService;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

public class MassStatusUpdateService {
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO;
	
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	private PanelScheduleDAO panelScheduleDAO;
	
	@Autowired
	private StatusSpecificQuestionsDAO statusSpecificQuestionsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private StatusSpecificEmailTemplatesDAO statusSpecificEmailTemplatesDAO;
	
	@Autowired
	private TeacherStatusNotesDAO teacherStatusNotesDAO;
	
	@Autowired
	private TeacherStatusScoresDAO teacherStatusScoresDAO;
	
	@Autowired
	private JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO;
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	
	@Autowired
	private StatusSpecificScoreDAO statusSpecificScoreDAO;
	
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	
	@Autowired
	private DistrictMaxFitScoreDAO districtMaxFitScoreDAO;
	
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	
	@Autowired
	private StatusWiseEmailSectionDAO statusWiseEmailSectionDAO;
	
	@Autowired
	private StatusWiseEmailSubSectionDAO statusWiseEmailSubSectionDAO;
	
	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	public String getJobCategoryAndSubjectListBox(int teacherId)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		int entityID=0;

		String locale = Utility.getValueOfPropByKey("locale");
		if(session == null || session.getAttribute("userMaster")==null)
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			
			if(userMaster.getDistrictId()!=null)
				districtMaster=userMaster.getDistrictId();
			
			entityID = userMaster.getEntityType();
		}
		
		StringBuffer sb=new StringBuffer();
		
		TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
		if(teacherDetail!=null)
		{
			String sFullName="";
			if(teacherDetail.getFirstName()!=null && !teacherDetail.getFirstName().equalsIgnoreCase(""))
			{
				sFullName=teacherDetail.getFirstName();
			}
			if(teacherDetail.getLastName()!=null && !teacherDetail.getLastName().equalsIgnoreCase(""))
			{
				sFullName=sFullName+" "+teacherDetail.getLastName();
			}
			sFullName=sFullName.trim();
			sb.append("<input type='hidden' id='teacherFullName' value='"+sFullName+"'>");
			
		}
		
		
		if(entityID==2)
		{
			try 
			{
				
				List<JobCategoryMaster> jobCategoryMastersList=new ArrayList<JobCategoryMaster>();
				List<SubjectMaster> subjectMastersList=new ArrayList<SubjectMaster>();
				
				jobCategoryMastersList = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
				subjectMastersList = subjectMasterDAO.findActiveSubjectByDistrict(districtMaster);
// zone dropdown added by mukesh
					sb.append("<div class='row left1'>");
					sb.append("<div class='col-sm-4 col-md-4' style='max-width:325px;'>");
					sb.append("<span class=''><label class=''>"+Utility.getLocaleValuePropByKey("lblZone", locale)+"</label>");
						sb.append("<select id='zoneMM' name='zoneMM' class='form-control'>");
					  if(districtMaster!=null && districtMaster.getIsZoneRequired()==1)
						 {
						   	 Criterion criterion=Restrictions.eq("districtMaster",districtMaster);
                             sb.append("<option selected value='"+(-1)+"'>"+""+Utility.getLocaleValuePropByKey("optAll", locale)+""+"</option>");
							 List<GeoZoneMaster> list=geoZoneMasterDAO.findByCriteria(criterion);
								for(GeoZoneMaster g:list)
								sb.append("<option value='"+g.getGeoZoneId()+"'>"+g.getGeoZoneName()+"</option>");
						 }
						System.out.println(">>>>>>>>>>>>>>>>>>>> "+sb);
					sb.append("</select>");
					sb.append("</span>");
			     	sb.append("</div>");
		    	sb.append("</div>");
			
				
				
				//Job Category 
				sb.append("<div class='col-sm-4 col-md-4'>");
					sb.append("<span class=''><label class=''>"+Utility.getLocaleValuePropByKey("lblJoCatN", locale)+"<span class='required'>*</span><a href='#' id='jobCategoryToolTip' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgJobCategoryFilterJobs", locale)+"'><img width='15' height='15' alt='' src='images/qua-icon.png'></a></label>");
						sb.append("<select id='jobCategoryId_msu' name='jobCategoryId_msu' class='form-control'>");
							sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("lblAllJobCategories ", locale)+"</option>");
							if(jobCategoryMastersList!=null && jobCategoryMastersList.size()>0)
								for(JobCategoryMaster jb: jobCategoryMastersList)
									sb.append("<option value='"+jb.getJobCategoryId()+"'>"+jb.getJobCategoryName()+"</option>");
						sb.append("</select>");
					sb.append("</span>");
				sb.append("</div>");
				
				//Subject
				sb.append("<div class='col-sm-4 col-md-4'>");
					sb.append("<span class=''><label class=''>"+Utility.getLocaleValuePropByKey("lblSubN ", locale)+"</label>");
						sb.append("<select id='subjectId_msu' name='subjectId_msu' class='form-control'>");
							sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("lblAllSubjects", locale)+"</option>");
							if(subjectMastersList!=null && subjectMastersList.size()>0)
								for(SubjectMaster sm: subjectMastersList)
									sb.append("<option value='"+sm.getSubjectId()+"'>"+sm.getSubjectName()+"</option>");
						sb.append("</select>");
					sb.append("</span>");
				sb.append("</div>");
				
				//Search Button
				sb.append("<div class='col-sm-2 col-md-2' style='width:155px;'>");
					sb.append("<span class=''><label class=''>&nbsp;</label>");
						sb.append("<button class='btn btn-primary top25-sm2' type='button' onclick='getJobOrderList_msu("+teacherId+")'>Search<i class='icon'></i></button>");
					sb.append("</span>");
				sb.append("</div>");
				
				
				sb.append("<div class='span3'>");
				sb.append("<span class=''><label class=''>&nbsp;</label>");
					sb.append("<button class='btn btn-primary top25-sm2' type='button' id='btnChangeStatus' onclick='displayMassStatus_msu("+teacherId+")'>"+Utility.getLocaleValuePropByKey("btnChngStatus", locale)+" <i class='icon'></i></button>");
				sb.append("</span>");
				sb.append("</div>");
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		sb.append("");

		return sb.toString();
	}
	
	public String displayAllStatusByTIDAndJID(String teacherId,String jobIds,String isOverrideForAllJob_msu)
	{
		System.out.println("Calling displayAllStatusByTIDAndJID teacherId "+teacherId +" jobIds "+jobIds+" isOverrideForAllJob_msu "+isOverrideForAllJob_msu);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userSession=(UserMaster) session.getAttribute("userMaster");
		
		int iJobCount=0;
		
		StringBuffer rdiv =	new StringBuffer();
		
		TeacherDetail teacherDetail=null;
		teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId),false,false);
		String sTeacherName="";
		if(teacherDetail!=null)
		{
			if(teacherDetail.getFirstName()!=null && teacherDetail.getFirstName()!="")
				sTeacherName=teacherDetail.getFirstName();
			if(teacherDetail.getLastName()!=null && teacherDetail.getLastName()!="")
				sTeacherName=sTeacherName+" "+teacherDetail.getLastName();
		}
		
		
		
		/*List<Integer> jobIdsLst=new ArrayList<Integer>();
		if(jobIds!=null && jobIds.length() > 0)
		{
			jobIds=jobIds.substring(0, jobIds.length()-1);
			String jobIdsArray[]=jobIds.split(",");
			for(int i=0;i < jobIdsArray.length; i++)
				jobIdsLst.add(Integer.parseInt(jobIdsArray[i]));
		}
		List<JobOrder> jobOrdersLst=new ArrayList<JobOrder>();
		jobOrdersLst=jobOrderDAO.getJobOrdersByJobIds_msu(jobIdsLst);*/
		
		List<JobOrder> jobOrdersLst=new ArrayList<JobOrder>();
		jobOrdersLst=getJobList_msu(jobIds);
		
		iJobCount=jobOrdersLst.size();
		JobOrder jobOrder=jobOrdersLst.get(0); 
		
		List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob= new ArrayList<TeacherStatusHistoryForJob>();
		if(jobOrdersLst.size() > 0)
			lstTeacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.getTeacherStatusHistoryByJobList(teacherDetail,jobOrdersLst);
		
		//System.out.println("lstTeacherStatusHistoryForJob Size "+lstTeacherStatusHistoryForJob.size());
		//System.out.println("Select * from teacherstatushistoryforjob where teacherId="+teacherId+" and status='s' and jobId in ("+jobIds+")");
		
		Map<Integer, Map<String,TeacherStatusHistoryForJob>> mapTeacherStatusHistoryForJob=new HashMap<Integer, Map<String,TeacherStatusHistoryForJob>>();
		mapTeacherStatusHistoryForJob=getMapTeacherStatusHistoryForJob(lstTeacherStatusHistoryForJob);
		
		List<StatusMaster> lstStatusMaster= new ArrayList<StatusMaster>();
		List<SecondaryStatus> lstTreeStructure=	new ArrayList<SecondaryStatus>();
		
		lstStatusMaster=WorkThreadServlet.statusMasters;
		
		
		Map<String, StatusMaster> mapStatusMaster=new HashMap<String, StatusMaster>();
		for(StatusMaster statusMaster:lstStatusMaster)
			if(statusMaster!=null)
				mapStatusMaster.put(statusMaster.getStatusShortName(), statusMaster);
		
		//Set Status counter 0
		Map<String, Integer> mapSMAndSSCountByJob=new HashMap<String, Integer>();
		Map<String, Integer> mapSMAndSSCountByJobForPanel=new HashMap<String, Integer>();
		String tempArray[]={"hird","dcln","rem"};
		for(int i=0;i<tempArray.length;i++)
		{
			StatusMaster temp=(StatusMaster)mapStatusMaster.get(tempArray[i]);
			mapSMAndSSCountByJob.put("SM"+temp.getStatusId(), 0);
			mapSMAndSSCountByJobForPanel.put("SM"+temp.getStatusId(), 0);
		}
		
		try 
		{
			try
			{
				if(jobOrder!=null)
				{
					 List<SecondaryStatus> lstSecondaryStatus=secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
					 if(lstSecondaryStatus.size()==0)
						 secondaryStatusDAO.copyDataFromDistrictToJobCategoryBanchAndHead(jobOrder.getHeadQuarterMaster(),jobOrder.getBranchMaster(),jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster(),userSession);
                    	//secondaryStatusDAO.copyDataFromDistrictToJobCategory(jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster(),userSession);
	                lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
					
				}
			}
			catch(Exception e)
			{
					e.printStackTrace();
			}
			//Set Status and SecondaryStatus counter 0
			mapSMAndSSCountByJob=setMapForSecondaryStatus(lstTreeStructure, mapSMAndSSCountByJob);
			mapSMAndSSCountByJobForPanel=setMapForSecondaryStatus(lstTreeStructure, mapSMAndSSCountByJobForPanel);
			
			//Set Status and SecondaryStatus update counter value according to TeacherStatusHistoryForJob
			mapSMAndSSCountByJob=getMapForSMAndSSCounterUpdateByJob(mapSMAndSSCountByJob, jobOrdersLst, mapTeacherStatusHistoryForJob);
			
			//Get Panel Data
			List<PanelSchedule> panelScheduleLst= new ArrayList<PanelSchedule>();
			panelScheduleLst=panelScheduleDAO.getPanelSchedulesByTIDAndJIDs_msu(teacherDetail, jobOrdersLst);
			
			//System.out.println("panelScheduleLst "+panelScheduleLst.size());
			//System.out.println("Select * from panelschedule where teacherId="+teacherId+" and jobId in ("+jobIds+")");
			//System.out.println("SELECT * FROM `jobwisepanelstatus` WHERE `jobPanelStatusId` in (Select jobPanelStatusId from panelschedule where teacherId="+teacherId+" and jobId in ("+jobIds+")) ");
			
			//Update Status and SecondaryStatus panel counter value
			mapSMAndSSCountByJobForPanel=getMapForSMAndSSCounterUpdateForPanel(panelScheduleLst, mapSMAndSSCountByJobForPanel);
			
			rdiv.append("<table border=0 bordercolor='red'>");
			rdiv.append("<tr>");
			for(SecondaryStatus tree: lstTreeStructure)
			{
				if(tree.getSecondaryStatus()==null)
				{
					rdiv.append("<td style='vertical-align:bottom;'> ");
					if(tree.getChildren().size()>0)
					{
						rdiv.append(getSecondaryStatus_msu(tree.getChildren(),mapSMAndSSCountByJob,mapSMAndSSCountByJobForPanel,iJobCount,sTeacherName,teacherId,jobIds,isOverrideForAllJob_msu));
					}
					rdiv.append("</td>");
				}
			}
			
			rdiv.append(getStatusMaster_msu(mapStatusMaster, mapSMAndSSCountByJob, mapSMAndSSCountByJobForPanel, iJobCount,sTeacherName,teacherId,jobIds,isOverrideForAllJob_msu));
			
			rdiv.append("</tr>");
			rdiv.append("</table>");
		}catch (Exception e) {
			e.printStackTrace();
		}	
		return rdiv.toString();
	}
	
	public String getSecondaryStatus_msu(List<SecondaryStatus> lstSecondaryStatus,Map<String, Integer> mapSMAndSSCountByJob,Map<String, Integer> mapSMAndSSCountByJobForPanel,int iJobCount,String sTeacherName,String teacherId,String jobOrderIds,String isOverrideForAllJob_msu)
	{
		StringBuffer sb=new StringBuffer();
		Collections.sort(lstSecondaryStatus,SecondaryStatus.secondaryStatusOrder);
		sb.append("<table  border=0 bordercolor='green'>");
		int banchmarkStatus=0;
		
		for(SecondaryStatus secondaryStatus: lstSecondaryStatus )
		{
			if(secondaryStatus.getStatus().equalsIgnoreCase("A"))
			{
				if(secondaryStatus.getStatusMaster()!=null)
				{
					if(secondaryStatus.getStatusMaster().getBanchmarkStatus()!=null)
					{
						banchmarkStatus=secondaryStatus.getStatusMaster().getBanchmarkStatus();
					}
				}
				if(banchmarkStatus==0)
				{	
					if(secondaryStatus.getStatusMaster()!=null)
					{
						if(secondaryStatus.getStatusMaster().getStatusShortName().equals("apl"))
						{
							sb.append("<tr><td style='text-align:center;height:100px;'>");
							sb.append("<img src=\"images/green.png\"/><br/>"+secondaryStatus.getSecondaryStatusName());
							sb.append("</td></tr>");
						}
					}
				}
				else if(secondaryStatus.getStatusMaster()==null)
				{
					String sStatusCode="SS"+secondaryStatus.getSecondaryStatusId();
					int iShowNextProcess=showStatusDetails(sStatusCode, mapSMAndSSCountByJob, mapSMAndSSCountByJobForPanel, iJobCount);
					int iStatusId=secondaryStatus.getSecondaryStatusId();
					String sStatusName=secondaryStatus.getSecondaryStatusName();
					
					if(sStatusName!=null && !sStatusName.equalsIgnoreCase("JSI"))
					{
						sb.append("<tr><td style='text-align:center;height:100px;'>");
						sb.append("<a href='#' onclick=\"showStatusDetails_msu('"+iShowNextProcess+"','"+sTeacherName.replace("'", "\\'")+"','"+sStatusName+"',"+ null +",'"+iStatusId+"','"+teacherId+"','"+jobOrderIds+"',"+null+",'"+isOverrideForAllJob_msu+"'); \">");
						sb.append("<img src=\"images/grey.png\"/>"); //green.png greengrey.png greygreen.png
						sb.append("</a>");
						//sb.append("<br/>["+iShowNextProcess+"]"+sStatusName);
						sb.append("<br/>"+sStatusName);
						sb.append("</td></tr>");
					}
					else
					{
						sb.append("<tr><td style='text-align:center;height:100px;'>");
						sb.append("<img src=\"images/grey.png\"/>");
						sb.append("<br/>"+sStatusName);
						sb.append("</td></tr>");
					}
					
				}
			}
		}

		for(SecondaryStatus secondaryStatus: lstSecondaryStatus)
		{
			if(secondaryStatus.getStatus().equalsIgnoreCase("A"))
			{
				if(secondaryStatus.getStatusMaster()!=null)
				{
					if(secondaryStatus.getStatusMaster().getStatusShortName().equals("vcomp") || secondaryStatus.getStatusMaster().getStatusShortName().equals("ecomp") || secondaryStatus.getStatusMaster().getStatusShortName().equals("scomp"))
					{
						
						String sStatusCode="SM"+secondaryStatus.getStatusMaster().getStatusId();
						int iShowNextProcess=showStatusDetails(sStatusCode, mapSMAndSSCountByJob, mapSMAndSSCountByJobForPanel, iJobCount);
						int iStatusId=secondaryStatus.getSecondaryStatusId();
						String sStatusName=secondaryStatus.getSecondaryStatusName();
						
						sb.append("<tr><td align='center' style='height:100px;width:150px;'>");
						//sb.append("<a href='#' onclick=\"showStatusDetails_msu('"+iShowNextProcess+"','"+sTeacherName.replace("'", "\\'")+"','"+sStatusName+"',"+ null +",'"+iStatusId+"','"+teacherId+"','"+jobOrderIds+"',"+null+",'"+isOverrideForAllJob_msu+"'); \">["+iShowNextProcess+"]");
						sb.append("<a href='#' onclick=\"showStatusDetails_msu('"+iShowNextProcess+"','"+sTeacherName.replace("'", "\\'")+"','"+sStatusName+"',"+ null +",'"+iStatusId+"','"+teacherId+"','"+jobOrderIds+"',"+null+",'"+isOverrideForAllJob_msu+"'); \">");
						
						sb.append("<table><tr><td class='txtbgroundstatus'>"+secondaryStatus.getSecondaryStatusName()+"</td></tr></table>");
						
						sb.append("</a>");	
						sb.append("</td></tr>");		
					}
				}
			}
		}
		sb.append("</table>");
		return sb.toString();
	}
	
	
	public String getStatusMaster_msu(Map<String, StatusMaster> mapStatusMaster,Map<String, Integer> mapSMAndSSCountByJob,Map<String, Integer> mapSMAndSSCountByJobForPanel,int iJobCount,String sTeacherName,String teacherId,String jobOrderIds,String isOverrideForAllJob_msu)
	{
		StringBuffer sb=new StringBuffer();
		
		StatusMaster statusHired=mapStatusMaster.get("hird"); 
		StatusMaster statusDeclined=mapStatusMaster.get("dcln"); 
		StatusMaster statusRemoved=mapStatusMaster.get("rem"); 
		
		boolean hiredFlag=false,declineFlag=false,rejectFlag=false;
		
		sb.append("<td style='vertical-align:top;'>");
		
		sb.append("<table  border=0 bordercolor='green'>");
		sb.append("<tr><td style='text-align:center;height:100px;'>");
		if(!hiredFlag)
		{
			sb.append("<img src=\"images/hire.png\"/>"); //rdiv.append("<img src=\"images/hired.png\"/>");
		}
		sb.append("<br/>Hired");
		sb.append("</td></tr>");
		sb.append("</table>");
		
		sb.append("<table  border=0 bordercolor='green'>");
		sb.append("<tr><td style='text-align:center;height:100px;'>");
		if(!declineFlag)
		{
			
			int iStatusId=statusDeclined.getStatusId();
			sb.append("<a href='#' onclick=\"showStatusDetails_msu('1','"+sTeacherName.replace("'", "\\'")+"','Declined',"+ iStatusId +",'"+null+"','"+teacherId+"','"+jobOrderIds+"','dcln','"+isOverrideForAllJob_msu+"'); \">");
			sb.append("<img src=\"images/decline.png\"/>");//rdiv.append("<img src=\"images/declined.png\"/>");
			sb.append("</a>");
			
			//sb.append("<img src=\"images/decline.png\"/>");
			
		}
		sb.append("<br/>Declined");
		sb.append("</td></tr>");
		sb.append("</table>");
		
		
		sb.append("<table  border=0 bordercolor='green'>");
		sb.append("<tr><td style='text-align:center;height:100px;'>");
		
		String sStatusName=statusRemoved.getStatus();
		
		if(!rejectFlag)
		{
			int iStatusId=statusRemoved.getStatusId();
			sb.append("<a href='#' onclick=\"showStatusDetails_msu('1','"+sTeacherName.replace("'", "\\'")+"','"+sStatusName+"',"+ iStatusId +",'"+null+"','"+teacherId+"','"+jobOrderIds+"','rem','"+isOverrideForAllJob_msu+"'); \">");
			sb.append("<img src=\"images/remove.png\"/>");//rdiv.append("<img src=\"images/removed.png\"/>");
			sb.append("</a>");
			//sb.append("<img src=\"images/remove.png\"/>");
		}
		sb.append("<br/>"+statusRemoved.getStatus());
		sb.append("</td></tr>");
		sb.append("</table>");
		
		sb.append("<table  border=0 bordercolor='green'>");
		sb.append("<tr><td style='text-align:center;height:100px;'>");
		sb.append("<img src=\"images/withdrew.png\"/><br/>Withdrawn");	
		sb.append("</td></tr>");
		sb.append("</table>");
		
		sb.append("</td>");
		return sb.toString();
	}
	
	public Map<String, Integer> setMapForSecondaryStatus(List<SecondaryStatus> lstTreeStructure,Map<String, Integer> mapSMAndSSCountByJob)
	{
		for(SecondaryStatus tree: lstTreeStructure)
		{
			if(tree.getSecondaryStatus()==null)
			{
				if(tree.getChildren().size()>0)
				{
					List<SecondaryStatus> lstSecondaryStatus=tree.getChildren();
					Collections.sort(lstSecondaryStatus,SecondaryStatus.secondaryStatusOrder);
					for(SecondaryStatus secondaryStatus: lstSecondaryStatus )
					{
						if(secondaryStatus.getStatus().equalsIgnoreCase("A"))
						{
							if(secondaryStatus.getStatusMaster()==null)
							{
								mapSMAndSSCountByJob.put("SS"+secondaryStatus.getSecondaryStatusId(), 0);
							}
							else if(secondaryStatus.getStatusMaster()!=null)
							{
								if(secondaryStatus.getStatusMaster().getStatusShortName().equals("vcomp") || secondaryStatus.getStatusMaster().getStatusShortName().equals("ecomp") || secondaryStatus.getStatusMaster().getStatusShortName().equals("scomp"))
								{
									//mapSMAndSSCountByJob.put("SS"+secondaryStatus.getSecondaryStatusId(), 0);
									mapSMAndSSCountByJob.put("SM"+secondaryStatus.getStatusMaster().getStatusId(), 0);
								}
							}
						}
					}
				}
			}
		}
		
		return mapSMAndSSCountByJob;
	}
	
	public Map<Integer, Map<String,TeacherStatusHistoryForJob>> getMapTeacherStatusHistoryForJob(List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob)
	{
		Map<Integer, Map<String,TeacherStatusHistoryForJob>> mapTeacherStatusHistoryForJob=new HashMap<Integer, Map<String,TeacherStatusHistoryForJob>>();
		Map<String,TeacherStatusHistoryForJob> mapTemp=null;
		
		if(lstTeacherStatusHistoryForJob.size() > 0)
		{
			for(TeacherStatusHistoryForJob teacherStatusHistoryForJob:lstTeacherStatusHistoryForJob)
			{
				if(teacherStatusHistoryForJob!=null)
				{
					mapTemp=new HashMap<String, TeacherStatusHistoryForJob>();
					String tempValue="";
					if(teacherStatusHistoryForJob.getStatusMaster()!=null)
						tempValue="SM"+teacherStatusHistoryForJob.getStatusMaster().getStatusId();
					else 
						tempValue="SS"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId();
					
					if(teacherStatusHistoryForJob.getJobOrder()!=null && mapTeacherStatusHistoryForJob.get(teacherStatusHistoryForJob.getJobOrder().getJobId())!=null)
					{
						mapTemp=mapTeacherStatusHistoryForJob.get(teacherStatusHistoryForJob.getJobOrder().getJobId());
						mapTemp.put(tempValue,teacherStatusHistoryForJob);
						mapTeacherStatusHistoryForJob.put(teacherStatusHistoryForJob.getJobOrder().getJobId(), mapTemp);
					}
					else
					{
						mapTemp.put(tempValue,teacherStatusHistoryForJob);
						mapTeacherStatusHistoryForJob.put(teacherStatusHistoryForJob.getJobOrder().getJobId(), mapTemp);
					}
				}
			}
		}
		
		return mapTeacherStatusHistoryForJob;
	}
	
	
	public Map<String, Integer> getMapForSMAndSSCounterUpdateByJob(Map<String, Integer> mapSMAndSSCountByJob,List<JobOrder> jobOrdersLst,Map<Integer, Map<String,TeacherStatusHistoryForJob>> mapTeacherStatusHistoryForJob)
	{
		if(jobOrdersLst!=null && jobOrdersLst.size() > 0)
		{
			for(JobOrder jobOrder2:jobOrdersLst)
			{
				if(jobOrder2!=null)
				{
					if(mapTeacherStatusHistoryForJob.get(jobOrder2.getJobId())!=null)
					{
						Map<String,TeacherStatusHistoryForJob> mapTemp=mapTeacherStatusHistoryForJob.get(jobOrder2.getJobId());
						Iterator iter1 = mapTemp.entrySet().iterator();
						while (iter1.hasNext())
						{
							Map.Entry mEntry1 = (Map.Entry) iter1.next();
							String tempKey=(String)mEntry1.getKey();
							if(mapSMAndSSCountByJob.get(tempKey)!=null)
							{
								int tempCount=mapSMAndSSCountByJob.get(tempKey);
								tempCount=tempCount+1;
								mapSMAndSSCountByJob.put(tempKey, tempCount);
							}
						}
					}
				}
			}
		}
		return mapSMAndSSCountByJob;
	}
	
	
	public Map<String, Integer> getMapForSMAndSSCounterUpdateForPanel(List<PanelSchedule> panelScheduleLst,Map<String, Integer> mapSMAndSSCountByJobForPanel)
	{
		if(panelScheduleLst!=null && panelScheduleLst.size() > 0)
		{
			for(PanelSchedule panelSchedule:panelScheduleLst)
			{
				if(panelSchedule!=null && panelSchedule.getJobWisePanelStatus()!=null && panelSchedule.getJobWisePanelStatus().getPanelStatus())
				{
					String tempKey="";
					if(panelSchedule.getJobWisePanelStatus().getStatusMaster()!=null)
						tempKey="SM"+panelSchedule.getJobWisePanelStatus().getStatusMaster().getStatusId();
					else if(panelSchedule.getJobWisePanelStatus().getSecondaryStatus()!=null)
						tempKey="SS"+panelSchedule.getJobWisePanelStatus().getSecondaryStatus().getSecondaryStatusId();
					
					if(tempKey!="" && mapSMAndSSCountByJobForPanel.get(tempKey)!=null)
					{
						int iTempValue=mapSMAndSSCountByJobForPanel.get(tempKey);
						iTempValue=iTempValue+1;
						mapSMAndSSCountByJobForPanel.put(tempKey, iTempValue);
					}
				}
			}
		}
		return mapSMAndSSCountByJobForPanel;
	}
	
	public int showStatusDetails(String sStatusCode,Map<String, Integer> mapSMAndSSCountByJob,Map<String, Integer> mapSMAndSSCountByJobForPanel,int iJobCount)
	{
		int iReturnValue=0;
		if(iJobCount > 0)
		{
			//int iStatusCount=0;
			int iStatusPanelCount=0;
			if(sStatusCode!=null && sStatusCode!="")
			{
				/*if(mapSMAndSSCountByJob.get(sStatusCode)!=null)
					iStatusCount=mapSMAndSSCountByJob.get(sStatusCode);*/

				if(mapSMAndSSCountByJobForPanel.get(sStatusCode)!=null)
					iStatusPanelCount=mapSMAndSSCountByJobForPanel.get(sStatusCode);
				
				if(iJobCount==1)
					iReturnValue=1;
				else if(iJobCount > 1 &&  iJobCount==iStatusPanelCount)
					iReturnValue=1;
				else if(iJobCount > 1 &&  iStatusPanelCount > 0 && iJobCount > iStatusPanelCount)
					iReturnValue=0;
				else
					iReturnValue=1;
			}
		}
		return iReturnValue;
	}
	
	
	//Use Later
	public String getStatusColor(String sStatusCode,Map<String, Integer> mapSMAndSSCountByJob,Map<String, Integer> mapSMAndSSCountByJobForPanel,int iJobCount)
	{
		String sReturnValue="";
		if(iJobCount > 0)
		{
			int iStatusCount=0;
			int iStatusPanelCount=0;
			if(sStatusCode!=null && sStatusCode!="")
			{
				if(mapSMAndSSCountByJob.get(sStatusCode)!=null)
					iStatusCount=mapSMAndSSCountByJob.get(sStatusCode);

				if(mapSMAndSSCountByJobForPanel.get(sStatusCode)!=null)
					iStatusPanelCount=mapSMAndSSCountByJobForPanel.get(sStatusCode);
				
				if(iStatusCount==0 && iStatusPanelCount==0)
					sReturnValue="Grey";
				else if(iStatusCount==iJobCount && iStatusPanelCount==0)
					sReturnValue="Green";
				else if(iJobCount < iStatusCount && iStatusPanelCount==0)
					sReturnValue="No_Panel_Partial_Green";
				
			}
		}
		return sReturnValue;
	}
	
	public List<JobOrder> getJobList_msu(String jobIds)
	{
		
		List<Integer> jobIdsLst=new ArrayList<Integer>();
		if(jobIds!=null && jobIds.length() > 0)
		{
			jobIds=jobIds.substring(0, jobIds.length()-1);
			String jobIdsArray[]=jobIds.split(",");
			for(int i=0;i < jobIdsArray.length; i++)
				jobIdsLst.add(Integer.parseInt(jobIdsArray[i]));
		}
		List<JobOrder> jobOrdersLst=new ArrayList<JobOrder>();
		jobOrdersLst=jobOrderDAO.getJobOrdersByJobIds_msu(jobIdsLst);
		
		return jobOrdersLst;
	}
	
	public String OnlyShowInstructionAttacheFileNameWithOutSlider(boolean bShowInstructionAttacheFileName,String strShowInstructionAttacheFileName,List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges)
	{
		StringBuffer sb=new StringBuffer();
		if(bShowInstructionAttacheFileName)
		{
			sb.append("<tr><td align=left bgcolor=white>");
			sb.append("<table border=0><tr><td style='vertical-align:top;width:580px;'>&nbsp;</td>");
			sb.append("<td style='padding-left:5px;vertical-align:bottom;width:42px;'>&nbsp;</td>");
			sb.append("<td valign=top style='padding-top:4px;' align='right'>");
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			String fileName=strShowInstructionAttacheFileName;
			String filePath=""+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusId();
			sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewInst", locale)+"' rel='tooltip' id='attachInstructionFileName' href='javascript:void(0);' onclick=\"downloadAttachInstructionFileName_msu('"+filePath+"','"+fileName+"','"+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusName()+"');"+windowFunc+"\"><span class='icon-info-sign icon-large iconcolor'></span></a>");
			sb.append("</td></tr>");
			sb.append("</table>");
			sb.append("</td></tr>");
		}
		else
		{
			sb.append("&nbsp;");
		}
		return sb.toString();
	}
	
	
	public String downloadAttachInstructionFile_msu(String filePath,String fileName)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String path="";

		try 
		{
			String source = Utility.getValueOfPropByKey("statusInstructionRootPath")+filePath+"/"+fileName;
			String target = context.getServletContext().getRealPath("/")+"/statusInstruction/"+filePath+"/";
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+"/statusInstruction/"+filePath+"/"+sourceFile.getName();
		} 
		catch (Exception e) 
		{
			System.out.println("Exception raise in downloadAttachInstructionFile method and Class Name is ManageStatusAjax and Error is "+e.getMessage());
			System.out.println(new Date() +" filePath "+filePath +" fileName "+fileName);
			path="";
		}
		return path;
	}
	
	
	public Double getSliderInterval(Double iMaxSliderValue)
	{
		Double interval=10.0;
		if(iMaxSliderValue%10==0)
			interval=10.0;
		else if(iMaxSliderValue%5==0)
			interval=5.0;
		else if(iMaxSliderValue%3==0)
			interval=3.0;
		else if(iMaxSliderValue%2==0)
			interval=2.0;
		return interval; 
	}
	
	public String[] getStatusDetailsInfo_msu(String teacherId,String jobIds,String dclnORrem,String statusId,String secondaryStatusId,String isOverrideForAllJob_msu)
	{
		String sTempArrary[]=new String[4];
		StringBuffer sb = new StringBuffer("");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		try 
		{
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			DistrictMaster districtMaster     = null;
			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
				districtMaster  = userMaster.getDistrictId();
				
			sb.append("<input type='hidden' name='teacherId_msu' id='teacherId_msu' value="+teacherId+">");
			sb.append("<input type='hidden' name='jobIds_msu' id='jobIds_msu' value="+jobIds+">");
			sb.append("<input type='hidden' name='statusId_msu' id='statusId_msu' value="+statusId+">");
			sb.append("<input type='hidden' name='secondaryStatusId_msu' id='secondaryStatusId_msu' value="+secondaryStatusId+">");
			sb.append("<input type='hidden' name='dclnORrem_msu' id='dclnORrem_msu' value="+dclnORrem+">");
			sb.append("<input type='hidden' name='isOverrideForAllJob_msu' id='isOverrideForAllJob_msu' value="+isOverrideForAllJob_msu+">");
			sb.append("<input type='hidden' name='districtId' id='districtId' value="+districtMaster.getDistrictId()+">");
			
			//sb.append("teacherId "+teacherId +" jobIds "+jobIds +" statusId "+statusId +" secondaryStatusId "+secondaryStatusId +" dclnORrem "+dclnORrem +" isOverrideForAllJob_msu "+isOverrideForAllJob_msu +"<BR>");
			
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			//System.out.println("0.1");
			StatusMaster statusMaster=null;
			SecondaryStatus secondaryStatus=null;
			String sStatusName_msu="";
			String sStatusShortName_msu="";
			if(statusId!=null && !statusId.equals("0"))
			{
				statusMaster=WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
				sStatusName_msu=statusMaster.getStatus();
				sStatusShortName_msu=statusMaster.getStatusShortName();
				
			}
			else if(secondaryStatusId!=null && !secondaryStatusId.equals("0"))
			{
				secondaryStatus=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
				sStatusName_msu=secondaryStatus.getSecondaryStatusName();
			}
			
			sb.append("<input type='hidden' name='sStatusName_msu' id='sStatusName_msu' value='"+sStatusName_msu+"'>");
			sb.append("<input type='hidden' name='sStatusShortName_msu' id='sStatusShortName_msu' value='"+sStatusShortName_msu+"'>");
			
			//System.out.println("0.2");
			List<JobOrder> jobOrdersLst=new ArrayList<JobOrder>();
			jobOrdersLst=getJobList_msu(jobIds);
			//System.out.println("0.3");
			List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
			List<DistrictMaxFitScore> lstDistrictMaxFitScores= new ArrayList<DistrictMaxFitScore>();
			JobOrder jobOrder=null;
			JobCategoryMaster jobCategoryMaster=null;
			if(jobOrdersLst.size() > 0)
			{
				jobOrder=jobOrdersLst.get(0);
				if(jobOrder!=null)
				{
					districtMaster=jobOrder.getDistrictMaster();
					jobCategoryMaster=jobOrder.getJobCategoryMaster();
				}
			}

			jobCategoryWiseStatusPrivileges=jobCategoryWiseStatusPrivilegeDAO.getJobCWSP(jobOrder, secondaryStatus,statusMaster);
			
			
			SecondaryStatus temp_secStatus=null;
			StatusMaster temp_status=null;
			
			if(secondaryStatus!=null)
			{
				System.out.println("1 SSID "+secondaryStatus.getSecondaryStatusId());
				if(secondaryStatus.getStatusMaster()!=null)
				{
					temp_status=secondaryStatus.getStatusMaster();
					System.out.println("2");
				}
				else if(secondaryStatus.getSecondaryStatus_copy()!=null)
				{
					temp_secStatus=secondaryStatus.getSecondaryStatus_copy();
					System.out.println("3");
				}
				else
				{
					temp_secStatus=secondaryStatus;
					System.out.println("4");
				}
			}
			else if(statusMaster!=null)
			{
				temp_status=statusMaster;
				System.out.println("5 SID "+statusMaster.getStatusId());
			}
			
			
			lstDistrictMaxFitScores=districtMaxFitScoreDAO.getMaxFitScoreByDID_SM_SS_msu(districtMaster, temp_status, temp_secStatus);
			
			/*if(secondaryStatus!=null && secondaryStatus.getStatusMaster()!=null)
			{
				lstDistrictMaxFitScores=districtMaxFitScoreDAO.getMaxFitScoreByDID_SM_SS_msu(districtMaster, secondaryStatus.getStatusMaster(), null);
			}
			else
			{
				lstDistrictMaxFitScores=districtMaxFitScoreDAO.getMaxFitScoreByDID_SM_SS_msu(districtMaster, statusMaster, secondaryStatus);
			}*/
			

			int iTopSliderDisable=1;
			boolean isDispalyTopSlider=false;
			Double iMaxFitScore=0.0;
			if(lstDistrictMaxFitScores.size()==1)
			{
				iMaxFitScore=lstDistrictMaxFitScores.get(0).getMaxFitScore();
				if(iMaxFitScore > 0)
					isDispalyTopSlider=true;
				else
					iTopSliderDisable=0;
			}
			else
			{
				iTopSliderDisable=0;	
			}
			
			//ShowInstructionAttacheFileName 
			boolean bShowInstructionAttacheFileName=false;
			String strShowInstructionAttacheFileName="";
			
			if(jobCategoryWiseStatusPrivileges.size()>0)
			{
				strShowInstructionAttacheFileName=jobCategoryWiseStatusPrivileges.get(0).getInstructionAttachFileName();
				if(userMaster.getEntityType()!=1 && strShowInstructionAttacheFileName!=null && !strShowInstructionAttacheFileName.equalsIgnoreCase(""))
					bShowInstructionAttacheFileName=true;
			}
			
			// Start ... Copy Question in statusspecificscore table
			List<StatusSpecificQuestions> lstStatusSpecificQuestions = new ArrayList<StatusSpecificQuestions>();
			lstStatusSpecificQuestions=statusSpecificQuestionsDAO.findQuestions_msu(districtMaster, jobCategoryMaster, secondaryStatus, statusMaster);
			System.out.println("5.1 lstStatusSpecificQuestions "+lstStatusSpecificQuestions.size());
			
			List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
			Map<JobOrder,List<StatusSpecificScore>> mapStatusScoreDetailsByJobOrder= new HashMap<JobOrder,List<StatusSpecificScore>>();
			
			if(lstStatusSpecificQuestions.size() > 0)
			{
				lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList_msu(districtMaster,jobCategoryMaster,jobOrdersLst,statusMaster,secondaryStatus,teacherDetail,userMaster);
				System.out.println("5.2 lstStatusSpecificScore "+lstStatusSpecificScore.size() +" JID Size "+jobOrdersLst.size());
				if(lstStatusSpecificScore.size() == 0)
				{
					statusSpecificScoreDAO.copyQuestionAnswersForAllJobLst_msu(jobOrdersLst, statusMaster, secondaryStatus, userMaster, teacherDetail);
					//lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList_msu(districtMaster,jobCategoryMaster,jobOrdersLst,statusMaster,secondaryStatus,teacherDetail,userMaster);
					System.out.println("5.3 Copied all");
				}
				else
				{
					mapStatusScoreDetailsByJobOrder=getStatusScoreDetailsByJobOrder(lstStatusSpecificScore);
					
					List<JobOrder> jobOrdersLst_Partial=new ArrayList<JobOrder>();
					jobOrdersLst_Partial=getJobOrderListWithNoQuestion(jobOrdersLst, mapStatusScoreDetailsByJobOrder);
					
					if(jobOrdersLst_Partial.size() > 0)
					{
						statusSpecificScoreDAO.copyQuestionAnswersForAllJobLst_msu(jobOrdersLst_Partial, statusMaster, secondaryStatus, userMaster, teacherDetail);
						//lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList_msu(districtMaster,jobCategoryMaster,jobOrdersLst,statusMaster,secondaryStatus,teacherDetail,userMaster);
						System.out.println("5.4 Copied partial , Job Lst Size "+jobOrdersLst_Partial.size());
					}
				}
				
			}

			lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList_msu(districtMaster,jobCategoryMaster,jobOrdersLst,statusMaster,secondaryStatus,teacherDetail,userMaster);
			System.out.println("5.5 lstStatusSpecificScore "+lstStatusSpecificScore.size());
			mapStatusScoreDetailsByJobOrder=getStatusScoreDetailsByJobOrder(lstStatusSpecificScore);
			Map<JobOrder, Integer> jobOrdersCount_Output=new HashMap<JobOrder, Integer>();
			jobOrdersCount_Output=getStatusQuestionCountByJobOrder(jobOrdersLst, mapStatusScoreDetailsByJobOrder);
			
			if(validateStatusQuestionForAllJob(jobOrdersCount_Output))
			{
				if(getQuestionSetForAJob(mapStatusScoreDetailsByJobOrder,jobOrdersLst))
				{
			
				}
				else
				{
					sTempArrary[2]="NoofQ_Match_Q_Diff";
				}
				
			}
			else
			{
				// Do not go forward
				sTempArrary[0]="NoofQ_NoMatch";
			}
			
			//Get List for display
			if(lstStatusSpecificScore.size() > 0)
			{
				List<JobOrder> tempJobOrders=new ArrayList<JobOrder>();
				tempJobOrders.add(lstStatusSpecificScore.get(lstStatusSpecificScore.size()-1).getJobOrder());
				lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList_msu(districtMaster,jobCategoryMaster,tempJobOrders,statusMaster,secondaryStatus,teacherDetail,userMaster);
				System.out.println("5.6 lstStatusSpecificScore "+lstStatusSpecificScore.size());
				System.out.println("5.7 "+lstStatusSpecificScore.get(0).getCreatedDateTime() +" :: "+lstStatusSpecificScore.get(lstStatusSpecificScore.size()-1).getCreatedDateTime() +" JID "+lstStatusSpecificScore.get(lstStatusSpecificScore.size()-1).getJobOrder().getJobId());
			}
			
			int iScoreSum=0;
			Double iScoreMaxSum=0.0;
			int answerSliderCount=0;
			for(StatusSpecificScore pojo:lstStatusSpecificScore)
			{
				if(pojo!=null)
				{
					if(pojo.getScoreProvided()!=null)
					{
						iScoreSum=iScoreSum+pojo.getScoreProvided();
					}
					if(pojo.getMaxScore()!=null && pojo.getMaxScore()>0){
						iScoreMaxSum=iScoreMaxSum+pojo.getMaxScore();
						answerSliderCount++;
					}
						
				}
			}
			
			// End ... Copy Question in statusspecificscore table
			
			
			sb.append("<table border='0' style='width:670px;'>");
			
			int iScoreProvided=0;
			Double iTopSliderMaxValue=0.0;
			Double iSliderInterval=10.0;
			
			if(answerSliderCount > 0)
			{
				isDispalyTopSlider=true;
				iTopSliderMaxValue=iScoreMaxSum;
				//iScoreProvided=iScoreSum;
				iTopSliderDisable=0;
			}
			else
			{
				iTopSliderMaxValue=iMaxFitScore;
			}
			iSliderInterval=getSliderInterval(iTopSliderMaxValue);
			
			sb.append("<input type='hidden' name='iMaxFitScore_msu' id='iMaxFitScore_msu' value="+iMaxFitScore+">");
			
			if(isDispalyTopSlider)
			{
					sb.append("<tr><td align=left id='sliderStatusNote'>");
					sb.append("<table border=0><tr><td style='vertical-align:center;height:33px;padding-left:8px'><label >Score </label></td>");
					sb.append("<td style='padding-left:5px;vertical-align:bottom;height:33px;'>&nbsp;<iframe id=\"ifrmTopSlider_msu\"  src=\"slideract.do?name=topSlider_msu&tickInterval="+iSliderInterval+"&max="+iTopSliderMaxValue+"&swidth=575&dslider="+iTopSliderDisable+"&svalue="+iScoreProvided+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:620px;margin-top:-11px;\"></iframe></td>");
					
					sb.append("<td valign=top style='padding-top:4px;'>");					
					
					if(bShowInstructionAttacheFileName)
					{
						String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
						String fileName=strShowInstructionAttacheFileName;
						String filePath=""+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusId();
						sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("lblClickToViewInst", locale)+"' rel='tooltip' id='attachInstructionFileName' href='javascript:void(0);' onclick=\"downloadAttachInstructionFileName_msu('"+filePath+"','"+fileName+"','"+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusName()+"');"+windowFunc+"\"><span class='icon-info-sign icon-large iconcolor'></span></a>");
					}
					
					sb.append("</td></tr>");
					sb.append("</table>");
					sb.append("</td></tr>");
			}
			else
			{
				sb.append(OnlyShowInstructionAttacheFileNameWithOutSlider(bShowInstructionAttacheFileName, strShowInstructionAttacheFileName, jobCategoryWiseStatusPrivileges));
			}
			
			
			
			//************* Start ... Display Question and List
			int iQuestionSliderJobId=0;
			int iQsliderDisable=1;
			int iCountScoreSlider=0;
			if(userMaster.getEntityType()==2)
			{
				iCountScoreSlider=0;
				if(lstStatusSpecificScore.size()>0)
				{
					sb.append("<tr><td align=left>");
					int questionNumber=1;
					
					if(lstStatusSpecificScore.size() > 0)
					{
						/*if(lstStatusSpecificScore.get(0).getFinalizeStatus()==1)
							iQsliderDisable=0;
						else
							iQsliderDisable=1;*/
						
						iQuestionSliderJobId=lstStatusSpecificScore.get(0).getJobOrder().getJobId();
					}
					
					int iQuetionScoreProvided=0;
					for(StatusSpecificScore score:lstStatusSpecificScore)
					{
						if(score.getQuestion()!=null && !score.getQuestion().equalsIgnoreCase(""))
							sb.append("<div class='row question_padding_left '><span><label class='labletxt'><B>Question:</B> "+score.getQuestion()+"</label></span></div>");
							
						if(score.getMaxScore()!=null)
							iTopSliderMaxValue=score.getMaxScore();
							
						iSliderInterval=getSliderInterval(iTopSliderMaxValue);
							
						if(score.getSkillAttributesMaster()!=null)
							sb.append("<div class='row question_padding_left'><span><label  class='lableAtxt'><B>Attribute:</B> "+score.getSkillAttributesMaster().getSkillName()+"</label></span></div>");
							
						if(score.getMaxScore()!=null && score.getMaxScore()>0)
						{
							iCountScoreSlider++;
							sb.append("<div class='row mt10' style='padding-left: 22px;'>");
							sb.append("<div class='span5 slider_question'><iframe id=\"ifrmQuestion_msu_"+iCountScoreSlider+"\" src=\"slideract.do?name=questionFrm_msu&tickInterval="+iSliderInterval+"&max="+score.getMaxScore()+"&dslider="+iQsliderDisable+"&swidth=575&step=1&answerId_msu="+score.getAnswerId()+"&svalue="+iQuetionScoreProvided+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:615px;margin-top:-11px;\"></iframe></div>");
							sb.append("</div>");
						}
						questionNumber++;	
					}
					
					if(questionNumber>1)
						sb.append("<div class='row mt10'></div>");
					
					sb.append("</td></tr>");
				}
				else
				{
					iQsliderDisable=0;
				}
			}
			sb.append("<input type='hidden' name='topSliderDisable' id='topSliderDisable' value="+iTopSliderDisable+">");
			sb.append("<input type='hidden' name='questionScoreSliderDisable' id='questionScoreSliderDisable' value="+iQsliderDisable+">");
			sb.append("<input type='hidden' name='countQuestionSlider' id='countQuestionSlider' value="+iCountScoreSlider+">");
			sb.append("<input type='hidden' name='iQuestionSliderJobId_msu' id='iQuestionSliderJobId_msu' value="+iQuestionSliderJobId+">");
			
			//sb.append("Top Slider "+iTopSliderDisable +" , Q Slider "+iQsliderDisable +" , Q SL Count  "+iCountScoreSlider);
			
			// End ... Display Question list from statusSpecificScore
			
			sb.append("</table>");
			
			
			
			// Hr Re De
			if(statusMaster!=null)
			{
				List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryActiveByStatus_msu(teacherDetail,jobOrdersLst,statusMaster);
				
				System.out.println("lstTeacherStatusHistoryForJob Size "+lstTeacherStatusHistoryForJob.size() +" for SId "+statusMaster.getStatusId()+" TID "+teacherDetail.getTeacherId());
				
				Map<JobOrder, StatusMaster> mapStatusMaster=new HashMap<JobOrder, StatusMaster>();
				if(lstTeacherStatusHistoryForJob.size() > 0)
				{
					for(TeacherStatusHistoryForJob historyForJob:lstTeacherStatusHistoryForJob)
					{
						if(historyForJob!=null)
						{
							if(mapStatusMaster.get(historyForJob.getJobOrder())==null)
							{
								mapStatusMaster.put(historyForJob.getJobOrder(), historyForJob.getStatusMaster());
								//System.out.println(" map add job "+historyForJob.getJobOrder().getJobId());
							}
						}
					}
				}
				
				int iStatusCount=0;
				if(jobOrdersLst.size() > 0)
				{
					for(JobOrder order:jobOrdersLst)
					{
						if(order!=null)
							if(mapStatusMaster.get(order)!=null)
								iStatusCount++;
					}
				}
				if(iStatusCount > 0 && jobOrdersLst.size() > 0 && jobOrdersLst.size()==iStatusCount)
				{
					if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
						sTempArrary[3]="dcln";
					else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem"))
						sTempArrary[3]="rem";
				}
				else
					sTempArrary[3]="";

				System.out.println("Status Name "+statusMaster.getStatus() +" , J Size "+jobOrdersLst.size() +" , Status Count "+iStatusCount +" , sTempArrary[3] "+sTempArrary[3]);
				
			}
		}
		catch (Exception e) 
		{
				// TODO: handle exception
		}
		sTempArrary[1]=sb.toString();
		return sTempArrary;
	}

	
	public Map<JobOrder,List<StatusSpecificScore>> getStatusScoreDetailsByJobOrder(List<StatusSpecificScore> lstStatusSpecificScore)
	{
		System.out.println("Calling method getStatusScoreDetailsByJobLst "+lstStatusSpecificScore.size());
		
		Map<JobOrder,List<StatusSpecificScore>> mapStatusScore= new HashMap<JobOrder,List<StatusSpecificScore>>();
		if(lstStatusSpecificScore.size() > 0)
		{
			for(StatusSpecificScore specificScore:lstStatusSpecificScore)
			{
				if(specificScore!=null)
				{
					if(mapStatusScore.get(specificScore.getJobOrder())==null)
					{
						List<StatusSpecificScore> tempLst=new ArrayList<StatusSpecificScore>();
						tempLst.add(specificScore);
						mapStatusScore.put(specificScore.getJobOrder(),tempLst);
					}
					else
					{
						List<StatusSpecificScore> tempLst=new ArrayList<StatusSpecificScore>();
						tempLst=mapStatusScore.get(specificScore.getJobOrder());
						tempLst.add(specificScore);
						mapStatusScore.put(specificScore.getJobOrder(), tempLst);
					}
				}
			}
		}
		return mapStatusScore;
	}
	
	
	public List<JobOrder> getJobOrderListWithNoQuestion(List<JobOrder> jobOrdersLst_Input,Map<JobOrder,List<StatusSpecificScore>> mapJobOrder)
	{
		List<JobOrder> jobOrders_Output=new ArrayList<JobOrder>();
		for(JobOrder jobOrder:jobOrdersLst_Input)
		{
			if(jobOrder!=null)
			{
				if(mapJobOrder.get(jobOrder)!=null)
				{
					if(mapJobOrder.get(jobOrder).size() == 0)
					{
						jobOrders_Output.add(jobOrder);
					}
				}
				else
				{
					jobOrders_Output.add(jobOrder);
				}
				
			}
			
		}
		return jobOrders_Output;
	}
	
	public Map<JobOrder,Integer> getStatusQuestionCountByJobOrder(List<JobOrder> jobOrdersLst_Input,Map<JobOrder,List<StatusSpecificScore>> mapJobOrder)
	{
		Map<JobOrder, Integer> jobOrdersCount_Output=new HashMap<JobOrder, Integer>();
		for(JobOrder jobOrder:jobOrdersLst_Input)
		{
			if(jobOrder!=null)
			{
				if(mapJobOrder.get(jobOrder)!=null)
				{
					jobOrdersCount_Output.put(jobOrder, mapJobOrder.get(jobOrder).size());
				}
				else
				{
					jobOrdersCount_Output.put(jobOrder, new Integer(0));
				}
			}
			
		}
		return jobOrdersCount_Output;
	}
	
	
	public boolean validateStatusQuestionForAllJob(Map<JobOrder, Integer> jobOrdersCount)
	{
		
		boolean bReturnValue=true;
		
		int iMasterValue=0;
		int i=0;
		Iterator iter1 = jobOrdersCount.entrySet().iterator();
		while (iter1.hasNext())
		{
			Map.Entry mEntry1 = (Map.Entry) iter1.next();
			int temp=(Integer)mEntry1.getValue();
			
			JobOrder jobOrder=(JobOrder)mEntry1.getKey();
			
			System.out.println(i+" time(s) for Job Id "+jobOrder.getJobId() +" and No of Question is " +temp);
			
			i++;
			
			if(i==1 && temp==iMasterValue)
			{

			}
			else if(i==1 && temp!=iMasterValue)
			{
				iMasterValue=temp;
			}
			else
			{
				if(i > 1 && temp!=iMasterValue)
				{
					bReturnValue=false;
				}
			}
		}
		
		return bReturnValue;
	}
	
	public boolean getQuestionSetForAJob(Map<JobOrder,List<StatusSpecificScore>> mapStatusScoreDetailsByJobOrder,List<JobOrder> jobOrderLst)
	{
		boolean bReturnValue=true;
		Map<StatusSpecificQuestions, Boolean> mapQuestionSet=new HashMap<StatusSpecificQuestions, Boolean>();
		List<StatusSpecificScore> statusSpecificScores=new ArrayList<StatusSpecificScore>();
		
		// Part 1
		int i=0;
		if(jobOrderLst.size() > 0)
		{
			for(JobOrder jobOrder:jobOrderLst)
			{
				i++;
				
				if(mapStatusScoreDetailsByJobOrder.get(jobOrder)!=null)
				{
					statusSpecificScores=mapStatusScoreDetailsByJobOrder.get(jobOrder);
					if(statusSpecificScores.size() > 0)
						for(StatusSpecificScore statusSpecificScore:statusSpecificScores)
							if(statusSpecificScore.getStatusSpecificQuestions()!=null)
								if(i==1)
									mapQuestionSet.put(statusSpecificScore.getStatusSpecificQuestions(), true);
								else if(i > 1)
								{
									if(mapQuestionSet.get(statusSpecificScore.getStatusSpecificQuestions())==null)
									{
										bReturnValue=false;
										System.out.println("6.1 Did not match QID "+statusSpecificScore.getQuestion());
									}
								}
				}
			}
		}
		return bReturnValue;
	}
	
	
	public StatusSpecificEmailTemplates getStatusWiseEmailForAdmin_msu(String statusName,String statusShortName,String jobIds,String teacherId)
	{
		UserMaster userMaster =null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		else{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		try{
			List<JobOrder> jobOrdersLst=new ArrayList<JobOrder>();
			jobOrdersLst=getJobList_msu(jobIds);
			//JobForTeacher jbforteacher	=	jobForTeacherDAO.findById(jftIdForSNote, false, false);
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			List<JobForTeacher> jobForTeachers=new ArrayList<JobForTeacher>();
			jobForTeachers=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrdersLst.get(0));
			JobForTeacher jbforteacher=jobForTeachers.get(0);
			String[] arrHrDetail = getHrDetailToTeacher(request,jobOrdersLst.get(0));
			StringBuffer sb 					= 	new StringBuffer();
			String hrContactFirstName = arrHrDetail[0];
			String hrContactLastName = arrHrDetail[1];
			String hrContactEmailAddress = arrHrDetail[2];
			String phoneNumber			 = arrHrDetail[3];
			String title		 = arrHrDetail[4];
			int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
			String schoolDistrictName =	arrHrDetail[6];
			String dmName 			 = arrHrDetail[7];
			String dmEmailAddress = arrHrDetail[8];
			String dmPhoneNumber	 = arrHrDetail[9];
			if(userMaster.getDistrictId().getDistrictId().equals(1201470)){
				dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
			}
			sb.append("<table>");
				sb.append("<tr>");
				sb.append("<td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>");
				if(isHrContactExist==0)
					sb.append(Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal11", locale)+"<br/>"+schoolDistrictName+" "+Utility.getLocaleValuePropByKey("msgRecruitmentSelectionTeam", locale)+" <br/><br/>");
				else 
					if(isHrContactExist==1)
					{
						sb.append(""+Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal11", locale)+"<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
						if(title!="")
						sb.append(title+"<br/>");
						if(phoneNumber!="")
							sb.append(phoneNumber+"<br/>");
						sb.append(schoolDistrictName+"<br/><br/>");
					}
					else
					{
						if(isHrContactExist==2)
						{
							sb.append(""+Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal11", locale)+"<br/>"+dmName+"<br/>");
							if(dmPhoneNumber!="")
								sb.append(dmPhoneNumber+"<br/>");
							sb.append(schoolDistrictName+"<br/><br/>");
						}
					}
				sb.append("</td>");
				sb.append("</tr>");
			sb.append("</table>");
			
			String url = Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jbforteacher.getJobId().getJobId()+"&JobOrderType="+jbforteacher.getJobId().getCreatedForEntity();
			System.out.println(" \n CG Url "+url+"\n statusName : "+statusName);
			
			Criterion criterion2				=	Restrictions.eq("statusShortName",statusShortName);
			List<StatusSpecificEmailTemplates> lstStatusSpecificEmailTemplates	=	statusSpecificEmailTemplatesDAO.findByCriteria(criterion2);
			
			String mailBody	="";
			
			if(lstStatusSpecificEmailTemplates.size()>0)
			{
				mailBody	=	lstStatusSpecificEmailTemplates.get(0).getTemplateBody();
				if(mailBody.contains("&lt; Teacher Name &gt;") || mailBody.contains("&lt; User Name &gt;") || mailBody.contains("&lt; Job Title &gt;")|| mailBody.contains("&lt; Status Name &gt;") || mailBody.contains("&lt; Previous Status Name &gt;"))
				{
					System.out.println("\n\n =================================================================================================");
					mailBody	=	mailBody.replaceAll("&lt; Teacher Name &gt;",jbforteacher.getTeacherId().getFirstName()+" "+jbforteacher.getTeacherId().getLastName());
					mailBody	=	mailBody.replaceAll("&lt; User Name &gt;",userMaster.getFirstName()+" "+userMaster.getLastName());
					mailBody	=	mailBody.replaceAll("&lt; Job Title &gt;",jbforteacher.getJobId().getJobTitle());
					mailBody	=	mailBody.replaceAll("&lt; Status Name &gt;",statusName);
					mailBody	=	mailBody.replaceAll("&lt; URL &gt;",url);
					mailBody	=	mailBody.replaceAll("&lt; Hr Contact &gt;",sb.toString());
					mailBody	=	mailBody.replaceAll("&lt; User EmailAddress &gt;",userMaster.getEmailAddress());
					mailBody	=	mailBody.replaceAll("from  &lt; Previous Status Name &gt;","");
					mailBody	=	mailBody.replaceAll("&lt; DA/SA Name &gt;","");
				}
				lstStatusSpecificEmailTemplates.get(0).setTemplateBody(mailBody);
				return lstStatusSpecificEmailTemplates.get(0);
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	
	}
	
	/*public StatusSpecificEmailTemplates getStatusWiseEmailForTeacher_msu(String statusName,String statusShortName,String jobIds,String teacherId)
	{
		UserMaster userMaster =null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		else{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		try{
			
			List<JobOrder> jobOrdersLst=new ArrayList<JobOrder>();
			jobOrdersLst=getJobList_msu(jobIds);
			
			//JobForTeacher jbforteacher	=	jobForTeacherDAO.findById(jftIdForSNote, false, false);
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			
			List<JobForTeacher> jobForTeachers=new ArrayList<JobForTeacher>();
			jobForTeachers=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrdersLst.get(0));
			JobForTeacher jbforteacher=jobForTeachers.get(0);
			
			String[] arrHrDetail = getHrDetailToTeacher(request,jobOrdersLst.get(0));
			String fromUserName	=	userMaster.getFirstName()+" "+userMaster.getLastName();
			String teacherName	=	jbforteacher.getTeacherId().getFirstName();
			String jobTitle		=	jbforteacher.getJobId().getJobTitle();
			
			StatusSpecificEmailTemplates sstObj	=	new StatusSpecificEmailTemplates();
			String url = Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jbforteacher.getJobId().getJobId()+"&JobOrderType="+jbforteacher.getJobId().getCreatedForEntity();
			System.out.println(" \n CG Url "+url+"\n statusName : "+statusName);
			
			String mailBody	=MailText.statusNoteSendToCandidate(userMaster,url,fromUserName,statusShortName,statusName,teacherName,jobTitle,arrHrDetail);
			
			sstObj.setSubjectLine("Status Changed for "+jbforteacher.getJobId().getJobTitle()+" Position You Had Applied For" );
			sstObj.setTemplateBody(mailBody);
			
			return sstObj;
			
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		//return districtMaster;
		return null;
	
	}*/
	
	public String[] getHrDetailToTeacher(HttpServletRequest request,JobOrder jobOrder)
	{
		System.out.println("\n Method getHrDetailToTeacher in Manage Status Ajax ");
		//HttpSession session = request.getSession(false);
		String schoolDistrictName =	"";
		int isHrContactExist = 0;
		String hrContactFirstName = "";
		String hrContactLastName = "";
		String hrContactEmailAddress = "";
		String title 			 = "";
		String phoneNumber		 = "";
		String dmName 			 = "";
		String dmEmailAddress = "";
		String dmPhoneNumber	 = "";
		String[] arrHrDetail	= new String[15]; 
		
		List<DistrictKeyContact> dkclist = new ArrayList<DistrictKeyContact>();
		//List<SchoolKeyContact> skclist = new ArrayList<SchoolKeyContact>();
		
		ContactTypeMaster ctm 	= new ContactTypeMaster();
		
		try{
			ctm	=	contactTypeMasterDAO.findById(1, false, false);
			Criterion criterion1 = Restrictions.eq("keyContactTypeId", ctm);
			Criterion criterion2 = null;
			
			
			if(jobOrder.getCreatedForEntity()==2)
			{
				schoolDistrictName		=	jobOrder.getDistrictMaster().getDistrictName()==null?"":jobOrder.getDistrictMaster().getDistrictName();
				criterion2				=	Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
				dkclist					=	districtKeyContactDAO.findByCriteria(Order.asc("keyContactId"),criterion1,criterion2);
				
				if(dkclist.size()>0)
				{
					isHrContactExist = 1;
					if(dkclist.get(0).getKeyContactFirstName()!=null)
						hrContactFirstName = dkclist.get(0).getKeyContactFirstName();
					
					if(dkclist.get(0).getKeyContactLastName()!=null)
						hrContactLastName = dkclist.get(0).getKeyContactLastName();
					
					if(dkclist.get(0).getKeyContactEmailAddress()!=null)
						hrContactEmailAddress = dkclist.get(0).getKeyContactEmailAddress();
					
					if(dkclist.get(0).getKeyContactPhoneNumber()!=null)
						phoneNumber	= dkclist.get(0).getKeyContactPhoneNumber();
					
					if(dkclist.get(0).getKeyContactTitle()!=null)
						title = dkclist.get(0).getKeyContactTitle();
						
				}
				else
				{
					System.out.println(" [ ---- Manage Status Ajax ---- ] DA Final Decision Maker Information [ If Hr is not Available ]");
					if(jobOrder.getDistrictMaster().getDmName()!=null)
					{
						isHrContactExist=2;
						dmName	=	jobOrder.getDistrictMaster().getDmName();
						
						if(jobOrder.getDistrictMaster().getDmEmailAddress()!=null)
							dmEmailAddress	=	jobOrder.getDistrictMaster().getDmEmailAddress();
						
						if(jobOrder.getDistrictMaster().getDmPhoneNumber()!=null)
							dmPhoneNumber	=	jobOrder.getDistrictMaster().getDmPhoneNumber();
					}
				}
			}
			else
			{
				if(jobOrder.getCreatedForEntity()==3)
				{/*
					schoolDistrictName		=	jobOrder.getSchool().get(0).getSchoolName()==null?"":jobOrder.getSchool().get(0).getSchoolName();
					criterion2				=	Restrictions.eq("schoolId", jobOrder.getSchool().get(0).getSchoolId());
					skclist					=	schoolKeyContactDAO.findByCriteria(Order.asc("keyContactId"),criterion1,criterion2);
					
					if(skclist.size()>0)
					{
						isHrContactExist = 1;
						if(skclist.get(0).getKeyContactFirstName()!=null)
							hrContactFirstName = skclist.get(0).getKeyContactFirstName();
						
						if(skclist.get(0).getKeyContactLastName()!=null)
							hrContactLastName = skclist.get(0).getKeyContactLastName();
						
						if(dkclist.get(0).getKeyContactEmailAddress()!=null)
							hrContactEmailAddress = dkclist.get(0).getKeyContactEmailAddress();
						
						if(skclist.get(0).getKeyContactPhoneNumber()!=null)
							phoneNumber	= skclist.get(0).getKeyContactPhoneNumber();
						
						if(skclist.get(0).getKeyContactTitle()!=null)
							title = skclist.get(0).getKeyContactTitle();
					}
					else
					{
						System.out.println(" [Manage Status Ajax  SA Final Decision Maker Information [ If Hr is not Available ]");
						if(jobOrder.getSchool().get(0).getDmName()!=null)
						{
							isHrContactExist=2;
							dmName	=	jobOrder.getSchool().get(0).getDmName();
							
							if(jobOrder.getSchool().get(0).getDmEmailAddress()!=null)
								dmEmailAddress	=	jobOrder.getSchool().get(0).getDmEmailAddress();
							
							if(jobOrder.getSchool().get(0).getDmPhoneNumber()!=null)
								dmPhoneNumber	=	jobOrder.getSchool().get(0).getDmPhoneNumber();
						}
					}
				*/}
			}
			
			arrHrDetail[0]	= hrContactFirstName;
			arrHrDetail[1]	= hrContactLastName;
			arrHrDetail[2]	= hrContactEmailAddress;	
			arrHrDetail[3]	= phoneNumber;
			arrHrDetail[4]	= title;
			arrHrDetail[5]	= ""+isHrContactExist;
			arrHrDetail[6]	= schoolDistrictName;
			arrHrDetail[7]	= dmName;
			arrHrDetail[8]	= dmEmailAddress;
			arrHrDetail[9]	= dmPhoneNumber;
			String JobBoardURL="";
			try{
				JobBoardURL=Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?districtId="+Utility.encryptNo(jobOrder.getDistrictMaster().getDistrictId()));
			}catch (Exception e) {
				// TODO: handle exception
			}
			arrHrDetail[12]	= JobBoardURL;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return arrHrDetail;
	}
	
	@Transactional(readOnly=false)
	public String[] saveStatusNotesAndSliderInfo_msu(String finalizeStatusFlag,String isOverrideForAllJob,String teacherId,String jobIds,String statusId,String secondaryStatusId,String dclnORrem,String statusNotes,String statusNoteFileName,String topSliderValue,Integer[] answerId_array,Integer[] score_array,boolean bEmailToDASA,boolean bEmailToCA,int iQuestionSliderJobId_msu,Double iMaxFitScore_msu)
	{
		boolean bOfclTransVeteranPref=false;
		
		String sTempArrary[]=new String[2];
		sTempArrary[0]="0";
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		
		try 
		{
			HttpSession session = request.getSession();
			UserMaster userMaster =null;
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}else{
				userMaster = (UserMaster) session.getAttribute("userMaster");
			}
			
			boolean bFinalizeStatusFlag=false;
			int iFinalizeStatusFlag=0;
			if(finalizeStatusFlag!=null && ( finalizeStatusFlag.equalsIgnoreCase("1") || finalizeStatusFlag.equalsIgnoreCase("3") || finalizeStatusFlag.equalsIgnoreCase("4") ))
			{
				bFinalizeStatusFlag=true;
				iFinalizeStatusFlag=1;
			}
			
			CandidateGridService cgService=new CandidateGridService();
			
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			
			List<JobOrder> jobOrdersLst=new ArrayList<JobOrder>();
			jobOrdersLst=getJobList_msu(jobIds);
			
			DistrictMaster districtMaster =null;
			JobCategoryMaster jobCategoryMaster=null;
			JobOrder jobOrder_Question=null;
			if(jobOrdersLst.size() > 0)
			{
				for(JobOrder jobOrder:jobOrdersLst)
					if(iQuestionSliderJobId_msu > 0 && jobOrder!=null && jobOrder.getJobId()==iQuestionSliderJobId_msu)
						jobOrder_Question=jobOrder;
				
				districtMaster=jobOrdersLst.get(0).getDistrictMaster();
				jobCategoryMaster=jobOrdersLst.get(0).getJobCategoryMaster();
			}
			System.out.println("districtMaster "+districtMaster.getDistrictId());
			String sDistrictName=districtMaster.getDistrictName();
			/*if(jobOrder2!=null && jobOrder2.getCreatedForEntity()==2)
			{
				try{
					districtMaster=jobOrder2.getDistrictMaster();
				}catch(Exception e){}
			}*/
			
			String statusShortName="soth";
			
			boolean bMSU_OverrideForAllJob=false;
			if(isOverrideForAllJob.equalsIgnoreCase("1"))
				bMSU_OverrideForAllJob=true;
				
			boolean bBottomStatusMaster=false;
			boolean bStatusMaster=false;
			boolean bSecondaryStatus=false;
			
			StatusMaster statusMaster=null;
			StatusMaster statusMaster_MasterCopy=null;
			SecondaryStatus secondaryStatus=null;
			
			StatusMaster statusMaster_temp=null;
			SecondaryStatus secondaryStatus_temp=null;
			
			if(!statusId.equalsIgnoreCase("null") && !statusId.equalsIgnoreCase("") && !statusId.equals("0"))
			{
				statusMaster=WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
				bStatusMaster=true;
				statusMaster_temp=statusMaster;
				statusMaster_MasterCopy=statusMaster;
				
			}

			if(!secondaryStatusId.equalsIgnoreCase("null") && !secondaryStatusId.equalsIgnoreCase("") && !secondaryStatusId.equals("0"))
			{
				 secondaryStatus=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
				 bSecondaryStatus=true;
				 if(secondaryStatus.getStatusMaster()!=null)
				 {
					 bBottomStatusMaster=true;
					 statusMaster_temp=secondaryStatus.getStatusMaster();
				 }
				 else
				 {
					 secondaryStatus_temp=secondaryStatus;
				 }
			}
			
			Map<JobOrder, JobForTeacher> mapJobForTeacher=new HashMap<JobOrder, JobForTeacher>();
			List<JobForTeacher> jobForTeachers=new ArrayList<JobForTeacher>();
			jobForTeachers=jobForTeacherDAO.getJobForTeacherByJIDsAndTID_msu(teacherDetail, jobOrdersLst);
			mapJobForTeacher=getMapJobForTeacher(jobForTeachers);
			
			System.out.println(" bStatusMaster "+bStatusMaster+" bSecondaryStatus "+bSecondaryStatus+" bBottomStatusMaster "+bBottomStatusMaster);
			////////////////////////Add Zone Wise Rejected//////////////////////////////////
			List<JobForTeacher> jobForTeacherList= new ArrayList<JobForTeacher>();
			String queryString="";
			int count=0;
			try{
				if(districtMaster!=null && districtMaster.getDistrictId().equals(1200390)){
					for(JobForTeacher jobForTeacher: jobForTeachers){
						try{
							String jobTitle=jobForTeacher.getJobId().getJobTitle();
							if(jobTitle.indexOf("(")>0){
								jobTitle=jobTitle.substring(0,jobTitle.lastIndexOf("(")).trim();
								System.out.println("jobTitle:::::::::"+jobTitle);
							}
							System.out.println("jobTitle::::::"+jobTitle);
							if(count==1){
								count=0;
								queryString+=" or ";	
							}
							queryString+="( jo.districtId="+districtMaster.getDistrictId()+" and jo.jobTitle like '%"+jobTitle+"%' and jo.jobCategoryId="+jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryId()+" and jft.jobForTeacherId not in ("+jobForTeacher.getJobForTeacherId()+") and jft.teacherId="+jobForTeacher.getTeacherId().getTeacherId()+")";
							count++;
						}catch(Exception e){
							e.printStackTrace();
						}
						System.out.println("queryString::::::"+queryString);
					}
					jobForTeacherList= jobForTeacherDAO.findJobsByTeacherListAndJobTitles(queryString);
				}else{
					jobForTeacherList=jobForTeachers;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			List<JobOrder> joblist=new ArrayList<JobOrder>();
			List<TeacherDetail> teacherList =new ArrayList<TeacherDetail>();
			System.out.println("jobForTeacherList:::>>>Mass>>::"+jobForTeacherList.size());
			try{
				for (JobForTeacher jobForTeacher : jobForTeacherList) {
					joblist.add(jobForTeacher.getJobId());
					teacherList.add(jobForTeacher.getTeacherId());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			List<TeacherStatusHistoryForJob> forJobs=teacherStatusHistoryForJobDAO.findByTeachersAndJobsStatus(teacherList, joblist);
			
			Map<String,TeacherStatusHistoryForJob> mapTSHFJ=new HashMap<String, TeacherStatusHistoryForJob>();
			try{
				if(forJobs!=null && forJobs.size() > 0)
				{
					for(TeacherStatusHistoryForJob tSHObj : forJobs){
						mapTSHFJ.put(tSHObj.getTeacherDetail().getTeacherId()+"#"+tSHObj.getJobOrder().getJobId(),tSHObj);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			SessionFactory sessionFactory=jobOrderDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			Transaction txOpen =statelesSsession.beginTransaction();
			
			////////////////////////End Zone Wise Rejected//////////////////////////////////
			TeacherStatusNotes teacherStatusNotes=null;
			
			Map<Integer, Integer> mapQuestionSliderScore=new HashMap<Integer, Integer>();
			Map<Integer, Integer> mapAnswerSliderScore=new HashMap<Integer, Integer>();
			List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
			Map<JobOrder,List<StatusSpecificScore>> mapStatusScoreDetailsByJobOrder= new HashMap<JobOrder,List<StatusSpecificScore>>();
			lstStatusSpecificScore=statusSpecificScoreDAO.getQuestionList_msu(districtMaster,jobCategoryMaster,jobOrdersLst,statusMaster,secondaryStatus,teacherDetail,userMaster);
			mapStatusScoreDetailsByJobOrder=getStatusScoreDetailsByJobOrder(lstStatusSpecificScore);
			
			if(answerId_array!=null && answerId_array.length>0)
				for(int i=0; i<answerId_array.length;i++)
					mapAnswerSliderScore.put(answerId_array[i], score_array[i]);
			
			List<StatusSpecificScore> statusSpecificScores_QS=new ArrayList<StatusSpecificScore>();
			if(jobOrder_Question!=null)
			{
				if(mapStatusScoreDetailsByJobOrder.get(jobOrder_Question)!=null)
				{
					statusSpecificScores_QS=mapStatusScoreDetailsByJobOrder.get(jobOrder_Question);
					if(statusSpecificScores_QS!=null && statusSpecificScores_QS.size() > 0)
					{
						for(StatusSpecificScore score:statusSpecificScores_QS)
						{
							if(score!=null)
							{
								if(mapAnswerSliderScore.get(score.getAnswerId())!=null)
									mapQuestionSliderScore.put(score.getStatusSpecificQuestions().getQuestionId(), mapAnswerSliderScore.get(score.getAnswerId()));
								else
									mapQuestionSliderScore.put(score.getStatusSpecificQuestions().getQuestionId(), new Integer(0));
							}
						}
					}
				}
			}
		
			List<TeacherStatusScores> teacherStatusScoresList=new ArrayList<TeacherStatusScores>();
			if(bBottomStatusMaster)
				teacherStatusScoresList= teacherStatusScoresDAO.getStatusScoreByJobList(teacherDetail,jobOrdersLst,secondaryStatus.getStatusMaster(),null,userMaster);
			else
				teacherStatusScoresList= teacherStatusScoresDAO.getStatusScoreByJobList(teacherDetail,jobOrdersLst,statusMaster,secondaryStatus,userMaster);
			
			
			Map<JobOrder, List<TeacherStatusHistoryForJob>> mapTeacherStatusHistoryForJob=new HashMap<JobOrder, List<TeacherStatusHistoryForJob>>();
			List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJobLst=new ArrayList<TeacherStatusHistoryForJob>();
			lstTeacherStatusHistoryForJobLst=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryActive_msu(teacherDetail,jobOrdersLst);
			mapTeacherStatusHistoryForJob=getMapTeacherStatusHistoryForJob_HRD(lstTeacherStatusHistoryForJobLst);
			
			List<TeacherStatusHistoryForJob> lstTSHJob_msu=new ArrayList<TeacherStatusHistoryForJob>();
			lstTSHJob_msu =teacherStatusHistoryForJobDAO.findByTeacherStatusHistorySelected_msu(teacherDetail,jobOrdersLst);
			Map<JobOrder, List<TeacherStatusHistoryForJob>> mapTeacherStatusHistoryForJobSelected=new HashMap<JobOrder, List<TeacherStatusHistoryForJob>>();
			mapTeacherStatusHistoryForJobSelected=getMapTeacherStatusHistoryForJob_HRD(lstTSHJob_msu);
			
			List<SecondaryStatus> secondaryStatusLst=new ArrayList<SecondaryStatus>();
			if(bBottomStatusMaster)
				secondaryStatusLst = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithStatus_msu(jobCategoryMaster, districtMaster, secondaryStatus.getStatusMaster());
			else
				secondaryStatusLst = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithStatus_msu(jobCategoryMaster, districtMaster, statusMaster);
			
			Map<JobOrder, String> mapDclnRejected=new HashMap<JobOrder, String>();
			boolean bStatusUpdateHDR=false;
			
			Map<JobOrder, TeacherStatusScores> mapTeacherStatusScores=new HashMap<JobOrder, TeacherStatusScores>(); 
			mapTeacherStatusScores=getMapTeacherStatusScores(teacherStatusScoresList);
			
			Map<JobOrder, TeacherStatusHistoryForJob> mapStatusHistory=new HashMap<JobOrder, TeacherStatusHistoryForJob>();
			List<TeacherStatusHistoryForJob> lstStatusHistory=new ArrayList<TeacherStatusHistoryForJob>();
			lstStatusHistory=teacherStatusHistoryForJobDAO.findByTeacherStatus_msu(teacherDetail,jobOrdersLst,statusMaster_temp,secondaryStatus_temp);
			mapStatusHistory=getMapStatusHistory(lstStatusHistory);
			
			String errorFlag="1";
			
			List<StatusSpecificScore> lstStatusSpecificScores_massUpdate=null;
			for(JobOrder jobOrder:jobOrdersLst)
			{
				System.out.println("0.0 Start process for Job Id "+jobOrder.getJobId());
				
				if(jobOrder!=null)
				{
					statusMaster=new StatusMaster();
					statusMaster=statusMaster_MasterCopy;
					
					statusShortName="soth";
					
					if(statusNotes!=null && !statusNotes.equalsIgnoreCase(""))
					{
						//SessionFactory sessionFactory1=teacherStatusNotesDAO.getSessionFactory();
						//StatelessSession statelesSsession1 = sessionFactory1.openStatelessSession();
						//Transaction txOpen1 =statelesSsession1.beginTransaction();
						
						teacherStatusNotes = new TeacherStatusNotes();
						
						teacherStatusNotes.setTeacherDetail(teacherDetail);
						teacherStatusNotes.setUserMaster(userMaster);
						teacherStatusNotes.setJobOrder(jobOrder);
						teacherStatusNotes.setDistrictId(districtMaster.getDistrictId());
						teacherStatusNotes.setSchoolId(null);	
						teacherStatusNotes.setEmailSentTo(0);
						teacherStatusNotes.setFinalizeStatus(bFinalizeStatusFlag);
						
						if(bBottomStatusMaster)
						{
							teacherStatusNotes.setStatusMaster(secondaryStatus.getStatusMaster());
							teacherStatusNotes.setSecondaryStatus(null);
						}
						else
						{
							if(bStatusMaster)
								teacherStatusNotes.setStatusMaster(statusMaster);
		
							if(bSecondaryStatus)
								teacherStatusNotes.setSecondaryStatus(secondaryStatus);
						}
						
						if(statusNotes!=null)
							teacherStatusNotes.setStatusNotes(statusNotes);
						
						if(statusNoteFileName!=null)
							teacherStatusNotes.setStatusNoteFileName(jobOrder.getJobId()+"_"+statusNoteFileName);
						
						try {
							/*statelesSsession1.insert(teacherStatusNotes);
							txOpen1.commit();
					       	statelesSsession1.close();
					       	sessionFactory1.close();*/
							teacherStatusNotesDAO.makePersistent(teacherStatusNotes);
						} catch (Exception e) {
							System.out.println("Error in Save Notes ");
							e.printStackTrace();
						}
						
						System.out.println("Note Saved...");
					}
					
					// Start Update Answer Score
					Double iQuestionScoreSum=0.0;
					Double iQuestionMaxScoreSum=0.0;
					boolean bUpdateTopStatusScore=false;
					boolean bUpdateQuestionSliderValue=false;
					
					if(jobOrder_Question!=null)
					{
						if(mapStatusScoreDetailsByJobOrder.get(jobOrder_Question)!=null)
						{
							lstStatusSpecificScores_massUpdate=mapStatusScoreDetailsByJobOrder.get(jobOrder);
							if(lstStatusSpecificScores_massUpdate!=null && lstStatusSpecificScores_massUpdate.size() > 0)
							{
								boolean bAllScoreValueZero=false;
								int iAllScoreValue=0;
								boolean bCGInprocessOrFinalizeFlag=false;
								int temp=0;
								for(StatusSpecificScore score:lstStatusSpecificScores_massUpdate)
								{
									temp++;
									if(score!=null)
										iAllScoreValue=iAllScoreValue+score.getScoreProvided();
									
									if(score.getFinalizeStatus()==1 && temp==1)
										bCGInprocessOrFinalizeFlag=true;
								}
								
								if(iAllScoreValue==0)
									bAllScoreValueZero=true;
								
								for(StatusSpecificScore score:lstStatusSpecificScores_massUpdate)
								{
									if(score!=null)
									{
										
										bUpdateQuestionSliderValue=isUpdateSliderValue(bMSU_OverrideForAllJob, bAllScoreValueZero, bFinalizeStatusFlag, bCGInprocessOrFinalizeFlag);
										
										if(mapQuestionSliderScore.get(score.getStatusSpecificQuestions().getQuestionId())!=null)
										{
											int iSliderScoreValue=mapQuestionSliderScore.get(score.getStatusSpecificQuestions().getQuestionId());
											if(bUpdateQuestionSliderValue && iSliderScoreValue > 0)
											{
												/*SessionFactory sessionFactory2=teacherStatusNotesDAO.getSessionFactory();
												StatelessSession statelesSsession2 = sessionFactory2.openStatelessSession();
												Transaction txOpen2 =statelesSsession2.beginTransaction();*/
												
												score.setScoreProvided(iSliderScoreValue);
												score.setFinalizeStatus(iFinalizeStatusFlag);
													
												try 
												{
													/*statelesSsession2.update(score);
													
													txOpen2.commit();
											       	statelesSsession2.close();
											       	sessionFactory2.close();*/
											       	
											       	statusSpecificScoreDAO.makePersistent(score);
												} 
												catch (Exception e) 
												{
													System.out.println("Error in Update Score");
													e.printStackTrace();
												}
											}
											System.out.println("Score Saved...");
										}
										iQuestionScoreSum=iQuestionScoreSum+score.getScoreProvided();
										iQuestionMaxScoreSum=iQuestionMaxScoreSum+score.getMaxScore();
										System.out.println("QSum "+score.getScoreProvided() +" Q Max Sum "+score.getMaxScore());
									}
								}
								
								// Update Status Specific Score
								
							}
						}
					}
					
					Double itopSliderValue=0.0;
					try {
						itopSliderValue = Double.parseDouble(topSliderValue);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					//System.out.println("Final QSum "+iQuestionScoreSum +" Max Sum "+iQuestionMaxScoreSum +" Q bUpdateStatusScore "+bUpdateStatusScore +" itopSliderValue "+itopSliderValue);
					
					if(bUpdateQuestionSliderValue || itopSliderValue > 0)
					{
						// UpDate/Save teacherStatusScore
						
						TeacherStatusScores teacherStatusScores_temp=null;
						if(mapTeacherStatusScores.get(jobOrder)!=null)
						{
							teacherStatusScores_temp=mapTeacherStatusScores.get(jobOrder);
							
							boolean bTopSliderScoreValueZero=false;
							boolean bCGTopSliderScoreInprocessOrFinalizeFlag=false;
							if(teacherStatusScores_temp!=null)
							{
								if(teacherStatusScores_temp.isFinalizeStatus())
									bCGTopSliderScoreInprocessOrFinalizeFlag=true;

								if(teacherStatusScores_temp.getScoreProvided()==0)
									bTopSliderScoreValueZero=true;
							}
							
							bUpdateTopStatusScore=isUpdateSliderValue(bMSU_OverrideForAllJob, bTopSliderScoreValueZero, bFinalizeStatusFlag, bCGTopSliderScoreInprocessOrFinalizeFlag);;
							
							if(bUpdateQuestionSliderValue)
							{
								teacherStatusScores_temp.setScoreProvided(iQuestionScoreSum);
								teacherStatusScores_temp.setMaxScore(iQuestionMaxScoreSum);
							}
							else if(bUpdateTopStatusScore)
							{
								teacherStatusScores_temp.setScoreProvided(itopSliderValue);
								teacherStatusScores_temp.setMaxScore(iMaxFitScore_msu);
							}
							teacherStatusScores_temp.setFinalizeStatus(bFinalizeStatusFlag);
							
							if(bUpdateQuestionSliderValue || bUpdateTopStatusScore)
							{
								try {
									//statelesSsession.update(teacherStatusScores_temp);
									/*SessionFactory sessionFactory3=teacherStatusNotesDAO.getSessionFactory();
									StatelessSession statelesSsession3 = sessionFactory3.openStatelessSession();
									Transaction txOpen3 =statelesSsession3.beginTransaction();*/

									/*statelesSsession3.update(teacherStatusScores_temp);
									txOpen3.commit();
									statelesSsession3.close();
									sessionFactory3.close();*/
									
									teacherStatusScoresDAO.makePersistent(teacherStatusScores_temp);
									
								} catch (Exception e) {
									System.out.println("Error in Update teacherStatusScores_temp");
									e.printStackTrace();
								}
							}
							System.out.println("Update teacherStatusScores");
							
						}
						else
						{
							teacherStatusScores_temp=new TeacherStatusScores();
							teacherStatusScores_temp.setTeacherDetail(teacherDetail);
							teacherStatusScores_temp.setUserMaster(userMaster);
							teacherStatusScores_temp.setJobOrder(jobOrder);
							teacherStatusScores_temp.setDistrictId(districtMaster.getDistrictId());
							
							if(bBottomStatusMaster)
							{
								teacherStatusScores_temp.setStatusMaster(secondaryStatus.getStatusMaster());
								teacherStatusScores_temp.setSecondaryStatus(null);
							}
							else
							{
								if(bStatusMaster)
									teacherStatusScores_temp.setStatusMaster(statusMaster);
			
								if(bSecondaryStatus)
									teacherStatusScores_temp.setSecondaryStatus(secondaryStatus);
							}
							
							teacherStatusScores_temp.setSchoolId(null);	
							teacherStatusScores_temp.setEmailSentTo(0);
							
							if(iQuestionScoreSum > 0)
							{
								teacherStatusScores_temp.setScoreProvided(iQuestionScoreSum);
								teacherStatusScores_temp.setMaxScore(iQuestionMaxScoreSum);
							}
							else if(itopSliderValue > 0)
							{
								teacherStatusScores_temp.setScoreProvided(itopSliderValue);
								teacherStatusScores_temp.setMaxScore(iMaxFitScore_msu);
							}
							
							
							teacherStatusScores_temp.setFinalizeStatus(bFinalizeStatusFlag);
							
							if(iQuestionScoreSum > 0 || itopSliderValue > 0)
							{
								try {
									//statelesSsession.insert(teacherStatusScores_temp);
									/*SessionFactory sessionFactory4=teacherStatusNotesDAO.getSessionFactory();
									StatelessSession statelesSsession4 = sessionFactory4.openStatelessSession();
									Transaction txOpen4 =statelesSsession4.beginTransaction();*/

									/*statelesSsession4.insert(teacherStatusScores_temp);
									txOpen4.commit();
									statelesSsession4.close();
									sessionFactory4.close();*/
									
									teacherStatusScoresDAO.makePersistent(teacherStatusScores_temp);
									
								} catch (Exception e) {
									System.out.println("Error in Insert teacherStatusScores_temp");
									e.printStackTrace();
								}
							}
							
							System.out.println("Save teacherStatusScores");
						}
					}
					
					// End Update Answer Score
					
					List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob=new ArrayList<TeacherStatusHistoryForJob>();
					lstTeacherStatusHistoryForJob=mapTeacherStatusHistoryForJob.get(jobOrder);
					
					////////////////////////
					if(statusMaster!=null && bFinalizeStatusFlag)
					{
						
						if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
						{
							if(getPrimaryStatus(lstTeacherStatusHistoryForJob)!=null && !getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("ED"))
							{
								if(getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("EH"))
								{
									//sError_dcln_Msg="Please remove the Hired status of the Candidate first and then Decline";
									bStatusUpdateHDR=true;
									mapDclnRejected.put(jobOrder, "Hired");
								}
								else if(getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("ER"))
								{
									//sError_rem_Msg="Please remove the Rejected status of the Candidate first and then Decline";
									bStatusUpdateHDR=true;
									mapDclnRejected.put(jobOrder, "Rejected");
								}
							}
						}
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem"))
						{
							if(getPrimaryStatus(lstTeacherStatusHistoryForJob)!=null && !getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("ER"))
							{
								if(getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("ED"))
								{
									//sError_dcln_Msg="Please remove the Declined status of the Candidate first and then Rejected";
									bStatusUpdateHDR=true;
									mapDclnRejected.put(jobOrder, "Declined");
								}
								else if(getPrimaryStatus(lstTeacherStatusHistoryForJob).equals("EH"))
								{
									//sError_rem_Msg="Please remove the Hired status of the Candidate first and then Rejected";
									bStatusUpdateHDR=true;
									mapDclnRejected.put(jobOrder, "Hired");
								}
							}
						}
						
						
						
						// Need to UnHire , UnDecline , UnRejected
						if(bStatusUpdateHDR)
						{
							if(bFinalizeStatusFlag)
							{
								//////////////////////////////
								JobForTeacher jobForTeacherObj_UnDeRe=new JobForTeacher();
								if(mapJobForTeacher.get(jobOrder)!=null)
									jobForTeacherObj_UnDeRe=mapJobForTeacher.get(jobOrder);
								boolean isUpdateStatus_UnDeRe=isUpdateStatus(jobForTeacherObj_UnDeRe,userMaster,null);

								if(isUpdateStatus_UnDeRe && statusMaster!=null && statusMaster.getStatusShortName().equals("rem") && finalizeStatusFlag.equals("4"))
								{
									//JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
									JobForTeacher jobForTeacher=jobForTeacherObj_UnDeRe;
									
									TeacherStatusHistoryForJob lastSelectedObj=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryLastSelected(teacherDetail,jobOrder);
									boolean isAssesmetnStatus = jobForTeacher.getJobId().getIsJobAssessment();
									if(isAssesmetnStatus){	
										try{
											AssessmentJobRelation assessmentJobRelation = assessmentJobRelationDAO.getAssessmentJobRelationByJobOrder(jobOrder);
											if(assessmentJobRelation!=null){
												AssessmentDetail assessmentDetail = assessmentJobRelation.getAssessmentId();
												List<TeacherAssessmentStatus> lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentByTeacher(jobForTeacher.getTeacherId(), assessmentDetail);
												if(lstTeacherAssessmentStatus.size()>0){						
													TeacherAssessmentStatus teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
													statusMaster = teacherAssessmentStatus.getStatusMaster();
												}else{
													statusMaster  = WorkThreadServlet.statusMap.get("icomp");
												}	
											}else{
												statusMaster  = WorkThreadServlet.statusMap.get("icomp");
											}	
										}catch(Exception e){
											e.printStackTrace();
										}
									}else{
										statusMaster  = WorkThreadServlet.statusMap.get("comp");
									}
									if(lastSelectedObj!=null){
										statusMaster=lastSelectedObj.getStatusMaster();
									}
									if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
										//errorFlag="3";
										try{
											TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
											StatusMaster statusMasterObj  = WorkThreadServlet.statusMap.get("rem");
											tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterObj,"A");
											if(tSHJ!=null){
												tSHJ.setStatus("I");
												tSHJ.setUpdatedDateTime(new Date());
												tSHJ.setUserMaster(userMaster);
												teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
											}
										}catch(Exception e){
											e.printStackTrace();
										}
										jobForTeacher.setStatus(statusMaster);
										if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
											jobForTeacher.setStatusMaster(null);
										}else{
											jobForTeacher.setStatusMaster(statusMaster);
										}
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity("UnRejected");
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);
										if(statusMaster!=null){
											jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
										}
										jobForTeacherDAO.makePersistent(jobForTeacher);
									}
								}
								else if(isUpdateStatus_UnDeRe && statusMaster!=null && statusMaster.getStatusShortName().equals("dcln") && finalizeStatusFlag.equals("3"))
								{
									//JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
									JobForTeacher jobForTeacher=jobForTeacherObj_UnDeRe;
									TeacherStatusHistoryForJob lastSelectedObj=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryLastSelected(teacherDetail,jobOrder);
									
									boolean isAssesmetnStatus = jobForTeacher.getJobId().getIsJobAssessment();
									if(isAssesmetnStatus){	
										try{
											AssessmentJobRelation assessmentJobRelation = assessmentJobRelationDAO.getAssessmentJobRelationByJobOrder(jobOrder);
											if(assessmentJobRelation!=null){
												AssessmentDetail assessmentDetail = assessmentJobRelation.getAssessmentId();
												List<TeacherAssessmentStatus> lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentByTeacher(jobForTeacher.getTeacherId(), assessmentDetail);
												if(lstTeacherAssessmentStatus.size()>0){						
													TeacherAssessmentStatus teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
													statusMaster = teacherAssessmentStatus.getStatusMaster();
												}else{
													statusMaster  = WorkThreadServlet.statusMap.get("icomp");
												}	
											}else{
												statusMaster  = WorkThreadServlet.statusMap.get("icomp");
											}	
										}catch(Exception e){
											e.printStackTrace();
										}
									}else{
										statusMaster  = WorkThreadServlet.statusMap.get("comp");
									}
									if(lastSelectedObj!=null){
										statusMaster=lastSelectedObj.getStatusMaster();
									}
									if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
										//errorFlag="3";
										try{
											TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
											StatusMaster statusMasterObj  = WorkThreadServlet.statusMap.get("dcln");
											tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterObj,"A");
											if(tSHJ!=null){
												tSHJ.setStatus("I");
												tSHJ.setUpdatedDateTime(new Date());
												tSHJ.setUserMaster(userMaster);
												teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
											}
										}catch(Exception e){
											e.printStackTrace();
										}
										jobForTeacher.setStatus(statusMaster);
										if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
											jobForTeacher.setStatusMaster(null);
										}else{
											jobForTeacher.setStatusMaster(statusMaster);
										}
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity("UnDecline");
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setUserMaster(userMaster);
										if(statusMaster!=null){
											jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
										}
										jobForTeacherDAO.makePersistent(jobForTeacher);
									}
								}
							
								
								/////////////////////////////
							}
						}
						
						
						String secondaryStatusNames="";
						String sStatusName="";
						errorFlag="1";
						
						if(bBottomStatusMaster)
							sStatusName=secondaryStatus.getStatusMaster().getStatusShortName();
						
						if(bBottomStatusMaster && bFinalizeStatusFlag && jobOrder.getDistrictMaster()!=null && (sStatusName.equalsIgnoreCase("scomp") || sStatusName.equalsIgnoreCase("ecomp") || sStatusName.equalsIgnoreCase("vcomp")))
						{
							if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()!=null)
							{
								if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints())
								{
									String accessDPoints="";
									if(jobOrder.getDistrictMaster().getAccessDPoints()!=null)
									accessDPoints=jobOrder.getDistrictMaster().getAccessDPoints();
									
									if(accessDPoints.contains(""+statusMaster.getStatusId()))
									{
										/*List<TeacherStatusHistoryForJob> lstTSHJob =teacherStatusHistoryForJobDAO.findByTeacherStatusHistorySelected(teacherDetail,jobOrder);
										List<SecondaryStatus> listSecondaryStatus = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithStatus(jobOrder,statusMaster);*/
										
										List<TeacherStatusHistoryForJob> lstTSHJob_temp=new ArrayList<TeacherStatusHistoryForJob>();
										if(mapTeacherStatusHistoryForJobSelected.get(jobOrder)!=null)
											lstTSHJob_temp=mapTeacherStatusHistoryForJobSelected.get(jobOrder);
										
										Map<Integer,SecondaryStatus> secMap=new HashMap<Integer, SecondaryStatus>();
										for(TeacherStatusHistoryForJob tSHObj : lstTSHJob_temp)
										{
											if(tSHObj.getSecondaryStatus()!=null)
											secMap.put(tSHObj.getSecondaryStatus().getSecondaryStatusId(),tSHObj.getSecondaryStatus());
										}
										if(secondaryStatusLst.size()>0)
										{
											int counter=0;
											for(SecondaryStatus  obj : secondaryStatusLst.get(0).getChildren()){
												if(obj.getStatus().equalsIgnoreCase("A") && obj.getStatusMaster()==null)
												{
													SecondaryStatus sec=secMap.get(obj.getSecondaryStatusId());
													if(sec==null)
													{
														boolean isJobAssessment=false;
														if(jobOrder.getIsJobAssessment()!=null)
														{
															if(jobOrder.getIsJobAssessment())
															{
																isJobAssessment=true;
															}
														}
														if((isJobAssessment && obj.getSecondaryStatusName().equalsIgnoreCase("JSI"))|| !obj.getSecondaryStatusName().equalsIgnoreCase("JSI"))
														{
															if(counter==1)
															{
																secondaryStatusNames+=", ";
															}
															counter=0;
															secondaryStatusNames+=obj.getSecondaryStatusName();
															counter++;
														}
													}
												}
											}
										}
										if(!secondaryStatusNames.equals("")){
											errorFlag="5";
										}
									}
								}
							}
						}
						
					}
					
					
					
					//////////////////////
					
					if(!errorFlag.equals("5"))
					{
						JobForTeacher jobForTeacherObj=new JobForTeacher();
						if(mapJobForTeacher.get(jobOrder)!=null)
							jobForTeacherObj=mapJobForTeacher.get(jobOrder);
						boolean isUpdateStatus=isUpdateStatus(jobForTeacherObj,userMaster,null);
						
						TeacherStatusHistoryForJob statusHistory=null;
						if(mapStatusHistory.get(jobOrder)!=null)
							statusHistory=mapStatusHistory.get(jobOrder);
						
						
						boolean selectedstatus=false;
						boolean selectedSecondaryStatus=false;
						try
						{
							if(jobForTeacherObj.getStatus()!=null && statusMaster_temp!=null)
							{
								selectedstatus=cgService.selectedStatusCheck(statusMaster_temp,jobForTeacherObj.getStatus());
							}
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
						
						try
						{
							if(jobForTeacherObj.getStatus()!=null && bBottomStatusMaster)
							{
								// && (jobForTeacherObj.getStatus().getStatusShortName().equals("scomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("ecomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("vcomp"))
								System.out.println("::::::::: 1st Section ::::::::");
								if(jobForTeacherObj.getSecondaryStatus()!=null)
								{
									selectedSecondaryStatus=cgService.selectedPriSecondaryStatusCheck(secondaryStatus,jobForTeacherObj.getSecondaryStatus());
									if(selectedSecondaryStatus==false){
										selectedstatus=false;
									}
								}
							}
							else if(jobForTeacherObj.getSecondaryStatus()!=null && secondaryStatus!=null && statusMaster==null)
							{
								System.out.println("::::::::: 2nd Section ::::::::");
								if(jobForTeacherObj.getStatus()!=null){
									if(cgService.priSecondaryStatusCheck(jobForTeacherObj.getStatus(),secondaryStatus)){
										selectedSecondaryStatus=cgService.selectedNotPriSecondaryStatusCheck(secondaryStatus,jobForTeacherObj.getSecondaryStatus());
									}
								}
							}else if(jobForTeacherObj.getSecondaryStatus()==null && secondaryStatus!=null && statusMaster==null){
								System.out.println("::::::::: 3rd Section ::::::::");
								if(jobForTeacherObj.getStatus()!=null){
									if(cgService.priSecondaryStatusCheck(jobForTeacherObj.getStatus(),secondaryStatus)){
										selectedSecondaryStatus=true;
									}
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						
						System.out.println("::::::selectedstatus::::::::;;;>>"+selectedstatus);
						System.out.println(":::::::;;selectedSecondaryStatus::::::::;;;>>"+selectedSecondaryStatus);
						
						
						////
						
						
						//List<TeacherStatusScores> teacherStatusScoresList_temp=new ArrayList<TeacherStatusScores>();
						//List<TeacherStatusNotes> teacherStatusNotesList_temp=new ArrayList<TeacherStatusNotes>();
						
						//teacherStatusScoresList_temp= teacherStatusScoresDAO.getFinalizeStatusScore(teacherDetail,jobOrder,statusMaster_temp,secondaryStatus_temp);
						//teacherStatusNotesList_temp= teacherStatusNotesDAO.getFinalizeStatusScore(teacherDetail,jobOrder,statusMaster_temp,secondaryStatus_temp);
						//TeacherStatusHistoryForJob canNotHireObj= new TeacherStatusHistoryForJob();
						boolean noInsert=true;
						if(statusMaster_temp!=null && (statusMaster_temp.getStatusShortName().equals("hird")|| statusMaster_temp.getStatusShortName().equals("rem")|| statusMaster_temp.getStatusShortName().equals("dcln")))
						{
							noInsert=false;
						}
						
						if(noInsert && (selectedstatus || selectedSecondaryStatus) && bFinalizeStatusFlag )
						{
							try
							{
								Calendar c = Calendar.getInstance();
								c.add(Calendar.SECOND,01);
								Date date = c.getTime();
								TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
								tSHJ.setTeacherDetail(teacherDetail);
								tSHJ.setJobOrder(jobOrder);
								if(statusMaster_temp!=null)
								{
									tSHJ.setStatusMaster(statusMaster_temp);
								}
								if(secondaryStatus_temp!=null)
								{
									tSHJ.setSecondaryStatus(secondaryStatus_temp);
									
									if(jobOrder.getDistrictMaster().getDistrictId()==1200390)
										if(secondaryStatus_temp.getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref"))
											bOfclTransVeteranPref=true;
								}
								tSHJ.setStatus("S");
								tSHJ.setCreatedDateTime(date);
								tSHJ.setUserMaster(userMaster);
								/*if(chkOverridetSatus){
									tSHJ.setOverride(chkOverridetSatus);
									tSHJ.setOverrideBy(userMaster);
									debugPrintln("Override entry first time");
								}*/
								teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
								
							}catch(Exception e){
								e.printStackTrace();
							}
							if(jobForTeacherObj!=null)
							{
								try{
									if(jobForTeacherObj!=null && secondaryStatusId!=null && !secondaryStatusId.equals("0")){
										jobForTeacherObj.setSecondaryStatus(secondaryStatus);
										jobForTeacherObj.setStatusMaster(null);
										jobForTeacherObj.setUpdatedBy(userMaster);
										jobForTeacherObj.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacherObj.setUpdatedDate(new Date());
										jobForTeacherObj.setLastActivity(secondaryStatus.getSecondaryStatusName());
										jobForTeacherObj.setLastActivityDate(new Date());
										jobForTeacherObj.setUserMaster(userMaster);
										jobForTeacherDAO.makePersistent(jobForTeacherObj);
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
							
						}
						else if(statusHistory==null && noInsert && (selectedstatus==false|| selectedSecondaryStatus==false) && bFinalizeStatusFlag)
						{
							try{
								Calendar c = Calendar.getInstance();
								c.add(Calendar.SECOND,01);
								Date date = c.getTime();
								TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
								tSHJ.setTeacherDetail(teacherDetail);
								tSHJ.setJobOrder(jobOrder);
								if(statusId!=null && !statusId.equals("0")){
									tSHJ.setStatusMaster(statusMaster);
								}
								if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
									tSHJ.setSecondaryStatus(secondaryStatus);
									
									if(jobOrder.getDistrictMaster().getDistrictId()==1200390)
										if(secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref"))
											bOfclTransVeteranPref=true;
								}
								tSHJ.setStatus("S");
								tSHJ.setCreatedDateTime(date);
								tSHJ.setUserMaster(userMaster);
								/*if(chkOverridetSatus){
									tSHJ.setOverride(chkOverridetSatus);
									tSHJ.setOverrideBy(userMaster);
									debugPrintln("Override entry first time");
								}*/
								teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
								if(secondaryStatus!=null)
								{
									if(jobForTeacherObj.getSecondaryStatus()!=null)
									{
										if(cgService.selectedNotPriSecondaryStatusCheck(secondaryStatus,jobForTeacherObj.getSecondaryStatus()))
										{
											if(jobForTeacherObj!=null)
											{
												try{
													//JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
													if(jobForTeacherObj!=null && secondaryStatusId!=null && !secondaryStatusId.equals("0")){
														jobForTeacherObj.setSecondaryStatus(secondaryStatus);
														jobForTeacherObj.setUpdatedBy(userMaster);
														jobForTeacherObj.setUpdatedByEntity(userMaster.getEntityType());		
														jobForTeacherObj.setUpdatedDate(new Date());
														jobForTeacherObj.setLastActivity(secondaryStatus.getSecondaryStatusName());
														jobForTeacherObj.setLastActivityDate(new Date());
														jobForTeacherObj.setUserMaster(userMaster);
														jobForTeacherDAO.makePersistent(jobForTeacherObj);
													}
												}catch(Exception e){
													e.printStackTrace();
												}
											}
											
										}
									}
									else
									{
										if(jobForTeacherObj!=null)
										{
											try{
												//JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
												if(jobForTeacherObj!=null && secondaryStatusId!=null && !secondaryStatusId.equals("0")){
													jobForTeacherObj.setSecondaryStatus(secondaryStatus);
													jobForTeacherObj.setUpdatedBy(userMaster);
													jobForTeacherObj.setUpdatedByEntity(userMaster.getEntityType());		
													jobForTeacherObj.setUpdatedDate(new Date());
													jobForTeacherObj.setLastActivity(secondaryStatus.getSecondaryStatusName());
													jobForTeacherObj.setLastActivityDate(new Date());
													jobForTeacherObj.setUserMaster(userMaster);
													jobForTeacherDAO.makePersistent(jobForTeacherObj);
												}
											}catch(Exception e){
												e.printStackTrace();
											}
										}
										
										
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
						
						
						// Start ... Update jobWiseConsolidatedTeacherScore
						
						/*List<TeacherStatusScores> teacherStatusScoresAll= new ArrayList<TeacherStatusScores>();
						teacherStatusScoresAll= teacherStatusScoresDAO.getTeacherStatusScores_msu(teacherDetail,jobOrder);*/
						
						Double jobWiseConsolidatedScore=0.0;
						Double jobWiseMaxScore=0.0;
						
						try
						{
							Double[] teacherStatusScoresAvg= teacherStatusScoresDAO.getAllFinalizeStatusScoreAvg(teacherDetail,jobOrder);
							jobWiseConsolidatedScore=teacherStatusScoresAvg[0];
							jobWiseMaxScore=teacherStatusScoresAvg[1];
						}
						catch(Exception e)
						{ 
							e.printStackTrace();
						}
						
						System.out.println("JID "+jobOrder.getJobId()+" TID "+teacherDetail.getTeacherId() +" Score "+jobWiseConsolidatedScore +" Max "+jobWiseMaxScore);
						try
						{
							if(jobWiseConsolidatedScore > 0 && jobWiseMaxScore > 0)
							{
								//System.out.println("2.1 In");
								JobWiseConsolidatedTeacherScore jWScore=jobWiseConsolidatedTeacherScoreDAO.getJWCTScore(teacherDetail, jobOrder);
								/*SessionFactory sessionFactory6=jobWiseConsolidatedTeacherScoreDAO.getSessionFactory();
								StatelessSession statelesSsession6 = sessionFactory6.openStatelessSession();
								Transaction txOpen6 =statelesSsession6.beginTransaction();*/
								
								if(jWScore==null)
								{
									jWScore=new JobWiseConsolidatedTeacherScore();
									jWScore.setTeacherDetail(teacherDetail);
									jWScore.setJobOrder(jobOrder);
									jWScore.setJobWiseConsolidatedScore(jobWiseConsolidatedScore);
									jWScore.setJobWiseMaxScore(jobWiseMaxScore);
									//statelesSsession6.insert(jWScore);
									jobWiseConsolidatedTeacherScoreDAO.makePersistent(jWScore);
									//System.out.println("2.2 Insert");
								}
								else
								{
									jWScore.setJobWiseConsolidatedScore(jobWiseConsolidatedScore);
									jWScore.setJobWiseMaxScore(jobWiseMaxScore);
									//statelesSsession6.update(jWScore);
									jobWiseConsolidatedTeacherScoreDAO.makePersistent(jWScore);
									//System.out.println("2.3 Update");
								}
								
								/*txOpen6.commit();
								statelesSsession6.close();
								sessionFactory6.close();*/
							}
						}
						catch(Exception e)
						{ 
							e.printStackTrace();
						}
						// End ... Update jobWiseConsolidatedTeacherScore
						
						
						// Start Other *************************************************************
						
						if(!bStatusUpdateHDR && bFinalizeStatusFlag)
						{
							errorFlag="3";
						}
						boolean mailSend=false;
						//************ UnDecline and UnRejected **************************
						
						System.out.println("************** Start UnRejected and UnDeclined ****************** "+bFinalizeStatusFlag);
						//System.out.println("0.1 isUpdateStatus "+isUpdateStatus+" ShortCode "+statusMaster.getStatusShortName()+" finalizeStatusFlag "+finalizeStatusFlag);
						
						if(bFinalizeStatusFlag)
						{
							System.out.println("0.2");
							if(isUpdateStatus && statusMaster!=null && statusMaster.getStatusShortName().equals("rem") && finalizeStatusFlag.equals("4"))
							{
								System.out.println("0.3");
								//JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
								JobForTeacher jobForTeacher=jobForTeacherObj;
								
								TeacherStatusHistoryForJob lastSelectedObj=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryLastSelected(teacherDetail,jobOrder);
								boolean isAssesmetnStatus = jobForTeacher.getJobId().getIsJobAssessment();
								if(isAssesmetnStatus){	
									try{
										AssessmentJobRelation assessmentJobRelation = assessmentJobRelationDAO.getAssessmentJobRelationByJobOrder(jobOrder);
										if(assessmentJobRelation!=null){
											AssessmentDetail assessmentDetail = assessmentJobRelation.getAssessmentId();
											List<TeacherAssessmentStatus> lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentByTeacher(jobForTeacher.getTeacherId(), assessmentDetail);
											if(lstTeacherAssessmentStatus.size()>0){						
												TeacherAssessmentStatus teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
												statusMaster = teacherAssessmentStatus.getStatusMaster();
											}else{
												statusMaster  = WorkThreadServlet.statusMap.get("icomp");
											}	
										}else{
											statusMaster  = WorkThreadServlet.statusMap.get("icomp");
										}	
									}catch(Exception e){
										e.printStackTrace();
									}
								}else{
									statusMaster  = WorkThreadServlet.statusMap.get("comp");
								}
								if(lastSelectedObj!=null){
									statusMaster=lastSelectedObj.getStatusMaster();
								}
								if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
									errorFlag="3";
									try{
										TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
										StatusMaster statusMasterObj  = WorkThreadServlet.statusMap.get("rem");
										tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterObj,"A");
										if(tSHJ!=null){
											tSHJ.setStatus("I");
											tSHJ.setUpdatedDateTime(new Date());
											tSHJ.setUserMaster(userMaster);
											teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									jobForTeacher.setStatus(statusMaster);
									if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
										jobForTeacher.setStatusMaster(null);
									}else{
										jobForTeacher.setStatusMaster(statusMaster);
									}
									if(statusMaster!=null){
										jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
									}
									jobForTeacher.setUpdatedBy(userMaster);
									jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
									jobForTeacher.setUpdatedDate(new Date());
									jobForTeacher.setLastActivity("UnRejected");
									jobForTeacher.setLastActivityDate(new Date());
									jobForTeacher.setUserMaster(userMaster);
									jobForTeacherDAO.makePersistent(jobForTeacher);
								}
							}
							else if(isUpdateStatus && statusMaster!=null && statusMaster.getStatusShortName().equals("dcln") && finalizeStatusFlag.equals("3"))
							{
								System.out.println("0.4");
								//JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
								JobForTeacher jobForTeacher=jobForTeacherObj;
								TeacherStatusHistoryForJob lastSelectedObj=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryLastSelected(teacherDetail,jobOrder);
								
								boolean isAssesmetnStatus = jobForTeacher.getJobId().getIsJobAssessment();
								if(isAssesmetnStatus){	
									try{
										AssessmentJobRelation assessmentJobRelation = assessmentJobRelationDAO.getAssessmentJobRelationByJobOrder(jobOrder);
										if(assessmentJobRelation!=null){
											AssessmentDetail assessmentDetail = assessmentJobRelation.getAssessmentId();
											List<TeacherAssessmentStatus> lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentByTeacher(jobForTeacher.getTeacherId(), assessmentDetail);
											if(lstTeacherAssessmentStatus.size()>0){						
												TeacherAssessmentStatus teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
												statusMaster = teacherAssessmentStatus.getStatusMaster();
											}else{
												statusMaster  = WorkThreadServlet.statusMap.get("icomp");
											}	
										}else{
											statusMaster  = WorkThreadServlet.statusMap.get("icomp");
										}	
									}catch(Exception e){
										e.printStackTrace();
									}
								}else{
									statusMaster  = WorkThreadServlet.statusMap.get("comp");
								}
								if(lastSelectedObj!=null){
									statusMaster=lastSelectedObj.getStatusMaster();
								}
								if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId())){
									errorFlag="3";
									try{
										TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
										StatusMaster statusMasterObj  = WorkThreadServlet.statusMap.get("dcln");
										tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterObj,"A");
										if(tSHJ!=null){
											tSHJ.setStatus("I");
											tSHJ.setUpdatedDateTime(new Date());
											tSHJ.setUserMaster(userMaster);
											teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									jobForTeacher.setStatus(statusMaster);
									if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
										jobForTeacher.setStatusMaster(null);
									}else{
										jobForTeacher.setStatusMaster(statusMaster);
									}
									if(statusMaster!=null){
										jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
									}
									jobForTeacher.setUpdatedBy(userMaster);
									jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
									jobForTeacher.setUpdatedDate(new Date());
									jobForTeacher.setLastActivity("UnDecline");
									jobForTeacher.setLastActivityDate(new Date());
									jobForTeacher.setUserMaster(userMaster);
									jobForTeacherDAO.makePersistent(jobForTeacher);
								}
							}
						}
						
						
						System.out.println("************** End UnRejected and UnDeclined ****************** ");
						
						System.out.println("************** Start Rejected and Declined ****************** ");
						
						if(!bStatusUpdateHDR && isUpdateStatus && selectedstatus)
						{
							if(selectedstatus && statusMaster!=null && statusMaster.getStatusShortName().equals("rem") && bFinalizeStatusFlag)
							{
								//JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
								JobForTeacher jobForTeacher=jobForTeacherObj;
								if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId()))
								{
									jobForTeacher.setStatus(statusMaster);
									jobForTeacher.setStatusMaster(statusMaster);
									jobForTeacher.setUpdatedBy(userMaster);
									jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
									jobForTeacher.setUpdatedDate(new Date());
									jobForTeacher.setLastActivity(statusMaster.getStatus());
									jobForTeacher.setLastActivityDate(new Date());
									jobForTeacher.setUserMaster(userMaster);
									if(statusMaster!=null){
										jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
									}
									jobForTeacherDAO.makePersistent(jobForTeacher);
									errorFlag="3";
									mailSend=true;
									statusShortName=statusMaster.getStatusShortName();
									////////////////////////Add Zone Wise Rejected//////////////////////////////////
									try{
										if(jobForTeacherList.size()>0){
											TeacherStatusNotes noteObj=teacherStatusNotesDAO.getFinalizeNotesByOrder(teacherDetail, jobOrder, statusMaster, secondaryStatus, userMaster);
											
											for (JobForTeacher jobForObj : jobForTeacherList){
												System.out.println("::::::::::::::Rejected Now Continue:::::::::::::::::::");
												TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
												String sTID_JID=jobForObj.getTeacherId().getTeacherId()+"#"+jobForObj.getJobId().getJobId();
												try{
													if(mapTSHFJ.get(sTID_JID)!=null){
															teacherStatusHistoryForJob=mapTSHFJ.get(sTID_JID);
													}
													if(teacherStatusHistoryForJob!=null)
													{
														System.out.println("::::::::::::::Inactive Function:::::::::::::;");
														teacherStatusHistoryForJob.setStatus("I");
														teacherStatusHistoryForJob.setUpdatedDateTime(new Date());
														teacherStatusHistoryForJob.setUserMaster(userMaster);
														
														txOpen =statelesSsession.beginTransaction();
														statelesSsession.update(teacherStatusHistoryForJob);
														txOpen.commit();
													}
												}catch(Exception e){
													e.printStackTrace();
												}
												try{
														TeacherStatusHistoryForJob forJob=new TeacherStatusHistoryForJob();
														forJob.setTeacherDetail(jobForObj.getTeacherId());
														forJob.setJobOrder(jobForObj.getJobId());
														forJob.setStatusMaster(statusMaster);
														forJob.setStatus("A");
														forJob.setUserMaster(userMaster);
														forJob.setCreatedDateTime(new Date());
														//teacherStatusHistoryForJobDAO.makePersistent(forJob);
														txOpen =statelesSsession.beginTransaction();
														statelesSsession.insert(forJob);
														txOpen.commit();
												}catch(Exception e){
													e.printStackTrace();
												}	
												if(noteObj!=null){
													try{
														TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
														teacherStatusNoteObj.setTeacherDetail(noteObj.getTeacherDetail());
														teacherStatusNoteObj.setJobOrder(jobForObj.getJobId());
														if(noteObj.getStatusMaster()!=null)
														teacherStatusNoteObj.setStatusMaster(noteObj.getStatusMaster());
														if(noteObj.getSecondaryStatus()!=null){
															teacherStatusNoteObj.setSecondaryStatus(noteObj.getSecondaryStatus());
														}
														teacherStatusNoteObj.setUserMaster(noteObj.getUserMaster());
														teacherStatusNoteObj.setDistrictId(noteObj.getDistrictId());
														teacherStatusNoteObj.setStatusNotes(noteObj.getStatusNotes());
														teacherStatusNoteObj.setStatusNoteFileName(noteObj.getStatusNoteFileName());
														teacherStatusNoteObj.setEmailSentTo(noteObj.getEmailSentTo());
														teacherStatusNoteObj.setFinalizeStatus(noteObj.isFinalizeStatus());
														//teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
														txOpen =statelesSsession.beginTransaction();
														statelesSsession.insert(teacherStatusNoteObj);
														txOpen.commit();
														System.out.println(teacherStatusNoteObj.getTeacherStatusNoteId()+":::::::::AfterInsert Note::::::::"+noteObj.getTeacherStatusNoteId());
													}catch(Exception ee){
														ee.printStackTrace();
													}
												}else{
													try{
														TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
														teacherStatusNoteObj.setTeacherDetail(jobForObj.getTeacherId());
														teacherStatusNoteObj.setJobOrder(jobForObj.getJobId());
														teacherStatusNoteObj.setStatusMaster(statusMaster);
														teacherStatusNoteObj.setUserMaster(userMaster);
														teacherStatusNoteObj.setDistrictId(districtMaster.getDistrictId());
														try{
															if(statusNotes!=null){
																teacherStatusNoteObj.setStatusNotes(statusNotes);
															}else{
																teacherStatusNoteObj.setStatusNotes("Rejected");
															}
														}catch(Exception e){
															teacherStatusNoteObj.setStatusNotes("Rejected");
															e.printStackTrace();
														}
														teacherStatusNoteObj.setEmailSentTo(1);
														teacherStatusNoteObj.setFinalizeStatus(true);
														//teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
														txOpen =statelesSsession.beginTransaction();
														statelesSsession.insert(teacherStatusNoteObj);
														txOpen.commit();
													}catch(Exception ee){
														ee.printStackTrace();
													}
												}
												jobForObj.setOfferReady(null);
												jobForObj.setOfferAccepted(null);
												jobForObj.setRequisitionNumber(null);
												jobForObj.setStatus(statusMaster);
												jobForObj.setStatusMaster(statusMaster);
												jobForObj.setUpdatedBy(userMaster);
												jobForObj.setUpdatedByEntity(userMaster.getEntityType());		
												jobForObj.setUpdatedDate(new Date());
												jobForObj.setLastActivity("Rejected");
												jobForObj.setLastActivityDate(new Date());
												jobForObj.setUserMaster(userMaster);
												//jobForTeacherDAO.makePersistent(jobForObj);
												txOpen =statelesSsession.beginTransaction();
												statelesSsession.update(jobForObj);
												txOpen.commit();
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
								////////////////////////End Zone Wise Rejected//////////////////////////////////
								}
							}
							else if(selectedstatus && statusMaster!=null && statusMaster.getStatusShortName().equals("dcln") && bFinalizeStatusFlag)
							{
								//JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
								JobForTeacher jobForTeacher=jobForTeacherObj;
								if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster.getStatusId()))
								{
									jobForTeacher.setStatus(statusMaster);
									jobForTeacher.setStatusMaster(statusMaster);
									jobForTeacher.setUpdatedBy(userMaster);
									jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
									jobForTeacher.setUpdatedDate(new Date());
									jobForTeacher.setLastActivity("Declined");
									jobForTeacher.setLastActivityDate(new Date());
									jobForTeacher.setUserMaster(userMaster);
									if(statusMaster!=null){
										jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
									}
									jobForTeacherDAO.makePersistent(jobForTeacher);
									errorFlag="3";
									mailSend=true;
									statusShortName="soth";
								}
							}
							else
							{

								if(selectedstatus)
								{
									 if(statusMaster_temp!=null && statusMaster_temp.getStatusShortName().equals("scomp")&& bFinalizeStatusFlag)
									 {
										String statuss ="|hird|dcln|rem|vcomp|ecomp|";
										//JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
										JobForTeacher jobForTeacher=jobForTeacherObj;
										if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster_temp.getStatusId())&& !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|")){
											jobForTeacher.setStatus(statusMaster_temp);
											jobForTeacher.setStatusMaster(statusMaster_temp);
											jobForTeacher.setUpdatedBy(userMaster);
											jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
											jobForTeacher.setUpdatedDate(new Date());
											jobForTeacher.setLastActivity(statusMaster_temp.getStatus());
											jobForTeacher.setLastActivityDate(new Date());
											jobForTeacher.setUserMaster(userMaster);
											if(statusMaster!=null){
												jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
											}
											jobForTeacherDAO.makePersistent(jobForTeacher);
											errorFlag="3";
											mailSend=true;
											statusShortName="soth";
										}
									}
									 else if(statusMaster_temp!=null && statusMaster_temp.getStatusShortName().equals("ecomp")&& bFinalizeStatusFlag)
									 {
										String statuss = "|hird|dcln|rem|vcomp|";
										
										//JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
										JobForTeacher jobForTeacher=jobForTeacherObj;
										if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster_temp.getStatusId())&& !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|"))
										{
											jobForTeacher.setStatus(statusMaster_temp);
											jobForTeacher.setStatusMaster(statusMaster_temp);
											jobForTeacher.setUpdatedBy(userMaster);
											jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
											jobForTeacher.setUpdatedDate(new Date());
											jobForTeacher.setLastActivity(statusMaster_temp.getStatus());
											jobForTeacher.setLastActivityDate(new Date());
											jobForTeacher.setUserMaster(userMaster);
											if(statusMaster!=null){
												jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
											}
											jobForTeacherDAO.makePersistent(jobForTeacher);
											errorFlag="3";
											mailSend=true;
											statusShortName="soth";
										}
									}
									 else if(statusMaster_temp!=null && statusMaster_temp.getStatusShortName().equals("vcomp")&& bFinalizeStatusFlag)
									 {
										String statuss = "|hird|dcln|rem|";
										//JobForTeacher jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
										JobForTeacher jobForTeacher=jobForTeacherObj;
										if(!jobForTeacher.getStatus().getStatusId().equals(statusMaster_temp.getStatusId()) && !statuss.contains("|"+jobForTeacher.getStatus().getStatusShortName()+"|"))
										{
											jobForTeacher.setStatus(statusMaster_temp);
											jobForTeacher.setStatusMaster(statusMaster_temp);
											jobForTeacher.setUpdatedBy(userMaster);
											jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
											jobForTeacher.setUpdatedDate(new Date());
											jobForTeacher.setLastActivity(statusMaster_temp.getStatus());
											jobForTeacher.setLastActivityDate(new Date());
											jobForTeacher.setUserMaster(userMaster);
											if(statusMaster!=null){
												jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
											}
											jobForTeacherDAO.makePersistent(jobForTeacher);
											errorFlag="3";
											mailSend=true;
											statusShortName="soth";
										}
									}
								}
							
							} // end else
						}
						
						if(selectedstatus && isUpdateStatus && isUpdateStatus && !bStatusUpdateHDR && !errorFlag.equals("2") && !errorFlag.equals("4"))
						{
							if(statusMaster!=null && (statusMaster.getStatusShortName().equals("hird")|| statusMaster.getStatusShortName().equals("rem")||statusMaster.getStatusShortName().equals("dcln")))
							{
								if(bFinalizeStatusFlag && lstTeacherStatusHistoryForJob==null)
								{
									try{
										Calendar c = Calendar.getInstance();
										c.add(Calendar.SECOND,01);
										Date date = c.getTime();
										TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
										tSHJ.setTeacherDetail(teacherDetail);
										tSHJ.setJobOrder(jobOrder);
										if(statusId!=null && !statusId.equals("0"))
										{
											tSHJ.setStatusMaster(statusMaster);
										}
										if(secondaryStatusId!=null && !secondaryStatusId.equals("0") && secondaryStatus!=null)
										{
											tSHJ.setSecondaryStatus(secondaryStatus);
											
											if(jobOrder.getDistrictMaster().getDistrictId()==1200390)
												if(secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref"))
													bOfclTransVeteranPref=true;
										}
										tSHJ.setStatus("A");
										tSHJ.setCreatedDateTime(date);
										tSHJ.setUserMaster(userMaster);
										teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
										//canNotHireObj=tSHJ;
										
										System.out.println("Save teacherStatusHistoryForJobDAO for "+statusMaster.getStatus() +" SID "+statusMaster.getStatusId() +" :: "+statusMaster.getStatusShortName());
										
									}catch(Exception e){
										e.printStackTrace();
									}
								}
							}
						}
						System.out.println("************** End Rejected and Declined ****************** ");
						// End Other *************************************************************
					}
				}
			}
			
			/*txOpen.commit();
	       	statelesSsession.close();*/
			
			
			try{ 
				if(bOfclTransVeteranPref)
					sendEmailOfficialTranscripts_Mass(teacherDetail, sDistrictName);
				} 
			catch (Exception e) 
			{}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return sTempArrary;
	}
	
	public Map<JobOrder, TeacherStatusScores> getMapTeacherStatusScores(List<TeacherStatusScores> teacherStatusScoresList)
	{
		Map<JobOrder, TeacherStatusScores> mapTeacherStatusScores=new HashMap<JobOrder, TeacherStatusScores>();
		if(teacherStatusScoresList.size() > 0)
		{
			for(TeacherStatusScores teacherStatusScores:teacherStatusScoresList)
			{
				if(mapTeacherStatusScores.get(teacherStatusScores.getJobOrder())==null)
				{
					mapTeacherStatusScores.put(teacherStatusScores.getJobOrder(), teacherStatusScores);
				}
			}
		}
		return mapTeacherStatusScores;
	}
	
	public Map<JobOrder, List<TeacherStatusHistoryForJob>> getMapTeacherStatusHistoryForJob_HRD(List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJobLst)
	{
		Map<JobOrder, List<TeacherStatusHistoryForJob>> mapTeacherStatusHistoryForJob=new HashMap<JobOrder, List<TeacherStatusHistoryForJob>>();
		if(lstTeacherStatusHistoryForJobLst.size() > 0)
		{
			for(TeacherStatusHistoryForJob teacherStatusHistoryForJob:lstTeacherStatusHistoryForJobLst)
			{
				if(teacherStatusHistoryForJob!=null)
				{
					List<TeacherStatusHistoryForJob> historyForJobs=new ArrayList<TeacherStatusHistoryForJob>();
					if(mapTeacherStatusHistoryForJob.get(teacherStatusHistoryForJob.getJobOrder())!=null)
					{
						historyForJobs=mapTeacherStatusHistoryForJob.get(teacherStatusHistoryForJob.getJobOrder());
						historyForJobs.add(teacherStatusHistoryForJob);
						mapTeacherStatusHistoryForJob.put(teacherStatusHistoryForJob.getJobOrder(), historyForJobs);
					}
					else
					{
						historyForJobs.add(teacherStatusHistoryForJob);
						mapTeacherStatusHistoryForJob.put(teacherStatusHistoryForJob.getJobOrder(), historyForJobs);
					}
				}
			}
		}
				
		return mapTeacherStatusHistoryForJob;
	}
	
	public String getPrimaryStatus(List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob)
	{
		String status=null;
		try {
			if(lstTeacherStatusHistoryForJob!=null)
			for(TeacherStatusHistoryForJob tSH: lstTeacherStatusHistoryForJob){
				if(tSH.getStatusMaster()!=null){
					if(tSH.getStatusMaster().getStatusShortName().equalsIgnoreCase("hird")){
						status="EH";
					}else if(tSH.getStatusMaster().getStatusShortName().equalsIgnoreCase("dcln")){
						status="ED";
					}else if(tSH.getStatusMaster().getStatusShortName().equalsIgnoreCase("rem")){
						status="ER";
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public Map<JobOrder, JobForTeacher> getMapJobForTeacher(List<JobForTeacher> jobForTeachers)
	{
		Map<JobOrder, JobForTeacher> mapJobForTeacher=new HashMap<JobOrder, JobForTeacher>();
		if(jobForTeachers.size() > 0)
		{
			for(JobForTeacher jobForTeacher:jobForTeachers)
			{
				if(jobForTeacher!=null)
				{
					if(mapJobForTeacher.get(jobForTeacher.getJobId())==null)
					{
						mapJobForTeacher.put(jobForTeacher.getJobId(), jobForTeacher);
					}
				}
			}
		}
		return mapJobForTeacher;
	}
	
	
	public boolean  isUpdateStatus(JobForTeacher jobForTeacher,UserMaster userMaster,SchoolInJobOrder schoolInJobOrder){
		boolean doChange = false;
		JobOrder jobOrder =null;
		if(jobForTeacher!=null){
			jobOrder =jobForTeacher.getJobId();
		}
		try{
			boolean isSchoolIn=false;
			if(jobOrder.getSelectedSchoolsInDistrict()!=null){
				if(jobOrder.getSelectedSchoolsInDistrict()==1){
					isSchoolIn=true;
				}
			}
			boolean isDistrict=false;
			if(userMaster.getDistrictId()!=null &&  jobOrder.getDistrictMaster().equals(userMaster.getDistrictId())){
				isDistrict=true;
			}
			boolean isSchool=false;
			if(schoolInJobOrder!=null)
			if(userMaster.getSchoolId()!=null &&  schoolInJobOrder.getSchoolId().equals(userMaster.getSchoolId())){
				isSchool=true;
			}
			
			if(isDistrict && jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2 && isSchoolIn==false){	
				doChange=true;
			}else if(isDistrict && jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2 && isSchoolIn){
				doChange=true;
			}else if(isDistrict && isSchool && jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==3){
				doChange=true;
			}else if(userMaster.getEntityType()==3){
				if(schoolInJobOrder==null){					
					doChange = false;
				}else{
					doChange=true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return doChange;
	}
	
	public Map<JobOrder, TeacherStatusHistoryForJob> getMapStatusHistory(List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob)
	{
		Map<JobOrder, TeacherStatusHistoryForJob> mapStatusHistory=new HashMap<JobOrder, TeacherStatusHistoryForJob>();
		if(lstTeacherStatusHistoryForJob.size() > 0)
			for (TeacherStatusHistoryForJob forJob:lstTeacherStatusHistoryForJob) 
				if(forJob!=null)
					if(mapStatusHistory.get(forJob.getJobOrder())==null)
						mapStatusHistory.put(forJob.getJobOrder(), forJob);
		return mapStatusHistory;
	}
	
	public boolean isUpdateSliderValue(boolean bMSU_OverrideForAllJob,boolean bAllScoreValueZero,boolean bFinalizeStatusFlag,boolean bCGInprocessOrFinalizeFlag)
	{
		boolean bReturnValue=false;
		if(!bMSU_OverrideForAllJob && bAllScoreValueZero && !bFinalizeStatusFlag && !bCGInprocessOrFinalizeFlag)
		{
			bReturnValue=true;
		}
		else if(!bMSU_OverrideForAllJob && bFinalizeStatusFlag && !bCGInprocessOrFinalizeFlag)
		{
			bReturnValue=true;
		}
		else if(bMSU_OverrideForAllJob)
		{
			if(!bFinalizeStatusFlag && bCGInprocessOrFinalizeFlag)
				bReturnValue=false;
			else
				bReturnValue=true;
		}
		return bReturnValue;
	}
	
	/*======= Gourav :It is used to get Status Wise Templates List ===============  */
	public String getSectionWiseTemaplteslist(Integer sectionEmailId){
		try{
		StatusWiseEmailSection statusWiseEmailSection = statusWiseEmailSectionDAO.findById(sectionEmailId, false, false);
		Criterion criteria				=	Restrictions.eq("statusWiseEmailSection",statusWiseEmailSection);
		List<StatusWiseEmailSubSection> sectionWiseTemaplteslist = statusWiseEmailSubSectionDAO.findByCriteria(Order.asc("subSectionName"),criteria);
		
		String statuswisesectiontemplateslist="<select class='form-control' id='statuswisesectiontemplateslist' onchange='setTemplateByLIstId(this.value)'><option value='0'>"+Utility.getLocaleValuePropByKey("lblSelectSubTemplate1", locale)+"</option>";
		
		for (StatusWiseEmailSubSection statusWiseEmailSubSection : sectionWiseTemaplteslist) {
			statuswisesectiontemplateslist+="<option value="+statusWiseEmailSubSection.getEmailSubSectionId()+">"+statusWiseEmailSubSection.getSubSectionName()+"</option>";
			/*if(statusWiseEmailSubSection.getSubSectionName().length()>55){
				String templateName =   statusWiseEmailSubSection.getSubSectionName().substring(0, 54);
				statuswisesectiontemplateslist+="<a id='iconpophover4' rel='tooltip'><option value='"+statusWiseEmailSubSection.getEmailSubSectionId()+"' title='"+statusWiseEmailSubSection.getSubSectionName()+"'>"+templateName+"....</option></a>";
			}else{
				statuswisesectiontemplateslist+="<option value="+statusWiseEmailSubSection.getEmailSubSectionId()+">"+statusWiseEmailSubSection.getSubSectionName()+"</option>";
			}*/
			
		}
		
		statuswisesectiontemplateslist+="</select>";
		return statuswisesectiontemplateslist;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	} 
	
	public String getTemplateByLIst(Integer subSectionEmailId){
		try {
			StatusWiseEmailSubSection statusWiseEmailSubSection = statusWiseEmailSubSectionDAO.findById(subSectionEmailId, false, false);
			String templateBody = statusWiseEmailSubSection.getTemplateBody();
			return templateBody;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void sendEmailOfficialTranscripts_Mass(TeacherDetail teacherDetail,String sDistrictName)
	{
		PrintOnConsole.debugPrintln(":::::::::::::::::::::::::::::::::Mass ... Copies of your official transcripts::::::::::::::::::::::");
		if(teacherDetail!=null)
		{
			List<TeacherAcademics> lstTeacherAcademics=teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
			//PrintOnConsole.debugPrintln("Non Verify lstTeacherAcademics "+lstTeacherAcademics.size());
			if(lstTeacherAcademics!=null && lstTeacherAcademics.size()>0)
			{
				for(TeacherAcademics academics:lstTeacherAcademics)
				{
					if(academics!=null)
					{
						academics.setStatus("V");
						teacherAcademicsDAO.makePersistent(academics);
						//PrintOnConsole.debugPrintln("TeacherAcademics is updated Status V for TAIV "+academics.getAcademicId() +" TID "+academics.getTeacherId().getTeacherId());
					}
				}
			}

			String sTo=teacherDetail.getEmailAddress();
			DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
			String mailContent = MailText.OfficialTranscripts(teacherDetail,sDistrictName);
			dsmt.setEmailerService(emailerService);
			dsmt.setMailfrom("admin@netsutra.com");
			dsmt.setMailto(sTo);
			dsmt.setMailsubject(Utility.getLocaleValuePropByKey("lblCopiesOfficialTran", locale));
			dsmt.setMailcontent(mailContent);

			PrintOnConsole.debugPrintln("*************** SendEmailOfficialTranscripts ************");
			PrintOnConsole.debugPrintln("Try to send email for Email "+sTo +" Subject Copies of your official transcripts");
			//PrintOnConsole.debugPrintln("mailContent "+mailContent);

			dsmt.start();
		}
	}
	
}

