package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DistrictDomain;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.master.DistrictDomainDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.utility.Utility;

public class DistrictDomainAjax {
	
	String locale = Utility.getValueOfPropByKey("locale");

	
	@Autowired
	private DistrictDomainDAO districtDomainDAO;
	public void setDistrictDomainDAO(DistrictDomainDAO districtDomainDAO) {
		this.districtDomainDAO = districtDomainDAO;
	}
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	public String getDistrictDomains(Integer districtId ,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		StringBuffer dmRecords =	new StringBuffer();
		try{
			String locale = Utility.getValueOfPropByKey("locale");
			String lblEdit  = Utility.getLocaleValuePropByKey("lblEdit", locale);
			String lblDelete   = Utility.getLocaleValuePropByKey("lblDelete", locale);
			String lblDomainName    = Utility.getLocaleValuePropByKey("lblDomainName ", locale);
			String lblNoDomain    = Utility.getLocaleValuePropByKey("lblNoDomain ", locale);
			String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
			
			
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			List<DistrictDomain>   districtDomains	=	null;
			List<DistrictDomain>   districtDomainsAll	=	null;
			String sortOrderFieldName	=	"domainName";
			String sortOrderNoField		=	"domainName";
			
			Order  sortOrderStrVal		=	null;
			
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("domainName")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			
			List<DistrictDomain>sortedStsList=new ArrayList<DistrictDomain>();
			Criterion criterion=null; 
			DistrictMaster districtMaster=null;
			if(districtId!=null)
				districtMaster=districtMasterDAO.findById(districtId, false, false);
			
			criterion=Restrictions.eq("districtMaster", districtMaster);
			System.out.println("noOfRow "+noOfRow+"\tnoOfRowInPage "+noOfRowInPage );
			districtDomains =districtDomainDAO.findDistrictDomains(sortOrderStrVal, start, noOfRowInPage,criterion,Restrictions.eq("status", "A"));
			districtDomainsAll=districtDomainDAO.findByCriteria(sortOrderStrVal,criterion);
			totalRecord=districtDomainsAll.size();
			
			SortedMap<String,DistrictDomain>	sortedMap = new TreeMap<String,DistrictDomain>();
			
			int mapFlag=2;
			for (DistrictDomain districtDomain : districtDomainsAll){
				String orderFieldName=null;
				if(!districtDomain.getDomainName().equals("")){
					if(sortOrderFieldName.equals("domainName")){
						orderFieldName=districtDomain.getDomainName()+"||"+districtDomain.getDistrictDomainId();
						sortedMap.put(orderFieldName+"||",districtDomain);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedStsList.add((DistrictDomain) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedStsList.add((DistrictDomain) sortedMap.get(key));
				}
			}else{
				sortedStsList=districtDomains;
			}
			List<DistrictDomain>sortedSList=new ArrayList<DistrictDomain>();
			
			if(totalRecord<end)
				end=totalRecord;
			
			if(mapFlag!=2)
				sortedSList=sortedStsList.subList(start,end);
			else
				sortedSList=sortedStsList;
			
			dmRecords.append("<table  id='domainTable' width='100%' border='0'>");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			
			String responseText="";
			
			
			responseText=PaginationAndSorting.responseSortingLink(lblDomainName,sortOrderFieldName,"domainName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='350' valign='top'>"+responseText+"</th>");
			dmRecords.append(" <th  width='150' valign='top'>"+lblActions+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			
			if(sortedStsList.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+lblNoDomain+"</td></tr>" );
			for (DistrictDomain dmn : sortedSList) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+dmn.getDomainName()+"</td>");
				
				dmRecords.append("<td>");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return editDomain("+dmn.getDistrictDomainId()+")\">"+lblEdit+"</a>&nbsp;|&nbsp;");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return deleteDomain("+dmn.getDistrictDomainId()+")\">"+lblDelete+"");
				dmRecords.append("</td>");
				
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationTrippleGrid(request, totalRecord, noOfRow, pageNo)); 
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	@Transactional(readOnly=false)
	public int saveDistrictDomain(Integer districtDomainId, Integer districtiId,String domainName){
		try{
			if(districtiId!=0 && !domainName.equals("")){
				DistrictMaster districtMaster=districtMasterDAO.findById(districtiId, false, false);
				DistrictDomain districtDomain=null;
				if(districtDomainId!=null){
					districtDomain=districtDomainDAO.findById(districtDomainId, false, false);
				}else{
					districtDomain=new DistrictDomain();
					districtDomain.setCreatedDateTime(new Date());
					districtDomain.setStatus("A");
				}
				districtDomain.setDistrictMaster(districtMaster);
				districtDomain.setDomainName(domainName);
				
				boolean result=districtDomainDAO.checkDuplicateDistrictDomain(districtDomainId,districtMaster,domainName);
				if(result){
					return 1;
				}else{
					districtDomainDAO.makePersistent(districtDomain);
				}	
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	@Transactional(readOnly=false)
	public DistrictDomain editDistrictDomain(Integer districtDomainId){
		DistrictDomain districtDomain=null;
		try{
			if(districtDomainId!=0){
				districtDomain=districtDomainDAO.findById(districtDomainId, false, false);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtDomain;
	}
	
	@Transactional(readOnly=false)
	public void deleteDistrictDomain(Integer districtDomainId){
		DistrictDomain districtDomain=null;
		try{
			if(districtDomainId!=0){
				districtDomain=districtDomainDAO.findById(districtDomainId, false, false);
				districtDomainDAO.makeTransient(districtDomain);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
