package tm.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.utility.Utility;

public class RequisitionAjax {
 
	
	 String locale = Utility.getValueOfPropByKey("locale");
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lblDistrictName=Utility.getLocaleValuePropByKey("lblDistrictName", locale);
	 String lblRequisitionNumber=Utility.getLocaleValuePropByKey("lblRequisitionNumber", locale);
	 String lblPostionT=Utility.getLocaleValuePropByKey("lblPostionT", locale);
	 String lblAddedOn =Utility.getLocaleValuePropByKey("lblAddedOn", locale);
	 String lblUsed=Utility.getLocaleValuePropByKey("lblUsed", locale);
	 String lblHired=Utility.getLocaleValuePropByKey("lblHired", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String msgNoRequisitionNofound=Utility.getLocaleValuePropByKey("msgNoRequisitionNofound", locale);
	 String optPTime=Utility.getLocaleValuePropByKey("optPTime", locale);
	 String optFTime=Utility.getLocaleValuePropByKey("optFTime", locale);
	 String optY=Utility.getLocaleValuePropByKey("optY", locale);
	 String optN=Utility.getLocaleValuePropByKey("optN", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String lnkDlt=Utility.getLocaleValuePropByKey("lnkDlt", locale);
	 String msgDisplaysWhetherRequisitionNumIsAttach=Utility.getLocaleValuePropByKey("msgDisplaysWhetherRequisitionNumIsAttach", locale);
	 String msgDisplaysWhetherRequisitionNumIsAttach2=Utility.getLocaleValuePropByKey("msgDisplaysWhetherRequisitionNumIsAttach2", locale);
	 
	 
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	
	public String DisplayRequisitionNos(Integer entityType,Integer searchId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String requisitionId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			List<DistrictRequisitionNumbers> districtRequisitionNumbers  	=	null;
			List<DistrictRequisitionNumbers> districtRequisitionNumbersAll  	=	null;
			String sortOrderFieldName	=	"districtMaster";
			String sortOrderNoField		=	"districtMaster";
			
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("districtMaster") && !sortOrder.equals("isHired")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("requisitionNumber"))
				 {
					 sortOrderNoField="requisitionNumber";
				 }
				 if(sortOrder.equals("isHired")){
						sortOrderNoField="isHired";
				 }
				 
			}
			 System.out.println("sortOrderNoField::::::::"+sortOrderNoField);
			 System.out.println("sortOrderFieldName::::::::"+sortOrderFieldName);
			 
			 
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			
			List<DistrictRequisitionNumbers>sortedStsList=new ArrayList<DistrictRequisitionNumbers>();
			Criterion criterion=null; 
		
			Criterion criterion1 =null; 
			//if(requisitionId=="" || requisitionId==null)
				
			criterion1=Restrictions.ilike("requisitionNumber","%"+requisitionId.trim()+"%" );
			DistrictMaster districtMaster=null;
			
			if(userMaster.getEntityType()==2){
				districtMaster=userMaster.getDistrictId();
				criterion=Restrictions.eq("districtMaster", districtMaster);
				
				districtRequisitionNumbers = districtRequisitionNumbersDAO.finddistrictReqList(sortOrderStrVal, start, noOfRowInPage,criterion,criterion1);
				districtRequisitionNumbersAll = districtRequisitionNumbersDAO.findByCriteria(sortOrderStrVal,criterion,criterion1);
							
				totalRecord=districtRequisitionNumbersAll.size();			
			}else{
					if(searchId!=0){
						districtMaster=districtMasterDAO.findById(searchId, false, false);
						criterion=Restrictions.eq("districtMaster", districtMaster);
						districtRequisitionNumbers = districtRequisitionNumbersDAO.finddistrictReqList(sortOrderStrVal, start, noOfRowInPage,criterion,criterion1);
						districtRequisitionNumbersAll = districtRequisitionNumbersDAO.findByCriteria(sortOrderStrVal,criterion,criterion1);
						totalRecord=districtRequisitionNumbersAll.size();
					}else{
						criterion=Restrictions.isNotNull("districtMaster");
						districtRequisitionNumbers = districtRequisitionNumbersDAO.finddistrictReqList(sortOrderStrVal, start, noOfRowInPage,criterion,criterion1);
						districtRequisitionNumbersAll = districtRequisitionNumbersDAO.findByCriteria(sortOrderStrVal,criterion,criterion1);
						totalRecord=districtRequisitionNumbersAll.size();
					}
					
				}
						
			List<JobOrder> jobIdList = jobOrderDAO.findByCriteria(criterion);			
			List<String> reqList = new ArrayList<String>(); 
			List<JobRequisitionNumbers> JRNLIst=new ArrayList<JobRequisitionNumbers>();
			if(districtRequisitionNumbersAll.size()>0){
				Criterion criterion2 = Restrictions.eq("status", 1);
				Criterion criterion3 = Restrictions.in("districtRequisitionNumbers",districtRequisitionNumbersAll);
				JRNLIst = jobRequisitionNumbersDAO.findByCriteria(criterion2,criterion3);
			}
			Map<Integer, Boolean> JRStauas = new HashMap<Integer, Boolean>();
			if(JRNLIst.size()>0){
				
				for (JobRequisitionNumbers JRNLIst2 : JRNLIst) {
					List L1 = Arrays.asList(JRNLIst2);
					
					System.out.println(L1);
					reqList.add(JRNLIst2.getDistrictRequisitionNumbers().getRequisitionNumber());
					if(JRNLIst2.getStatus().equals(1)){
						JRStauas.put(JRNLIst2.getDistrictRequisitionNumbers().getDistrictRequisitionId(), true);
					}else{
						JRStauas.put(JRNLIst2.getDistrictRequisitionNumbers().getDistrictRequisitionId(), false);
					}
				}
			}	
			List<JobForTeacher> reqInJFTList = new ArrayList<JobForTeacher>();
			if(jobIdList.size()>0){
				reqInJFTList = jobForTeacherDAO.getHiredRequisitionNumbers(reqList,jobIdList);
			}
			Map<String, String> reqIsHired = new HashMap<String, String>();
			
			 if(reqInJFTList!=null && reqInJFTList.size()>0){
				 for(JobForTeacher reqInJFT:reqInJFTList){					 
					 if(reqInJFT.getStatus().getStatusId().equals(6) && reqInJFT.getStatusMaster().getStatusId().equals(6)){
						 						 
						 String jobName="";
						 String schoolName="";
						 String jobSchoolName="";
						 if(reqInJFT.getJobId()!=null){
							 jobName 	= "<tr><td style='padding:0px;margin:0px;width:100%;'>Job title: "+reqInJFT.getJobId().getJobTitle()+"</td></tr>";
						 }
						 
						 if(reqInJFT.getSchoolMaster()!=null){
							 schoolName = "<tr><td  style='padding:0px;margin:0px;width:100%;'>School name: "+reqInJFT.getSchoolMaster().getSchoolName()+"</td></tr>";
						 }
						 
						 jobSchoolName=jobName+schoolName;
						 
						 if(reqIsHired.containsKey(reqInJFT.getRequisitionNumber()+"##"+reqInJFT.getJobId().getDistrictMaster().getDistrictId())){
							 jobSchoolName=reqIsHired.get(reqInJFT.getRequisitionNumber()+"##"+reqInJFT.getJobId().getDistrictMaster().getDistrictId())+jobSchoolName;
							 reqIsHired.put(reqInJFT.getRequisitionNumber()+"##"+reqInJFT.getJobId().getDistrictMaster().getDistrictId(), jobSchoolName);
						 }{
							 reqIsHired.put(reqInJFT.getRequisitionNumber()+"##"+reqInJFT.getJobId().getDistrictMaster().getDistrictId(), jobSchoolName);
						 }
					 }
				 }
			 }
			
			SortedMap<String,DistrictRequisitionNumbers>	sortedMap = new TreeMap<String,DistrictRequisitionNumbers>();
			if(sortOrderNoField.equals("districtMaster"))
			{
				sortOrderFieldName	=	"districtMaster";
			}
			if(sortOrderNoField.equals("isHired"))
			{
					sortOrderFieldName	=	"isHired";
			}
			
			int mapFlag=2;
			for (DistrictRequisitionNumbers diNumbers : districtRequisitionNumbersAll){
				
				if(reqIsHired.containsKey(diNumbers.getRequisitionNumber()+"##"+diNumbers.getDistrictMaster().getDistrictId())){
					//reqIsHired.get(diNumbersWithIsHired.getRequisitionNumber()+"##"+diNumbersWithIsHired.getDistrictMaster().getDistrictId())
					diNumbers.setIsHired("Yes");
				}else{
					diNumbers.setIsHired("No");
				}
				
				String orderFieldName=null;
				if(diNumbers.getDistrictMaster()!=null){
					if(sortOrderFieldName.equals("districtMaster")){
						orderFieldName=diNumbers.getDistrictMaster().getDistrictName()+"||"+diNumbers.getDistrictRequisitionId();
						sortedMap.put(orderFieldName+"||",diNumbers);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
					if(sortOrderFieldName.equals("isHired")){
						if(reqIsHired.containsKey(diNumbers.getRequisitionNumber()+"##"+diNumbers.getDistrictMaster().getDistrictId())){
							orderFieldName="Yes||"+diNumbers.getDistrictRequisitionId();
						}else{
							orderFieldName="No||"+diNumbers.getDistrictRequisitionId();
						}
						sortedMap.put(orderFieldName+"||",diNumbers);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}else{
					if(sortOrderFieldName.equals("districtMaster")){
						orderFieldName="0||"+diNumbers.getDistrictRequisitionId();
						sortedMap.put(orderFieldName+"||",diNumbers);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
					if(sortOrderFieldName.equals("isHired")){
						if(reqIsHired.containsKey(diNumbers.getRequisitionNumber()+"##"+diNumbers.getDistrictMaster().getDistrictId())){
							orderFieldName="Yes||"+diNumbers.getDistrictRequisitionId();
						}else{
							orderFieldName="No||"+diNumbers.getDistrictRequisitionId();
						}
						sortedMap.put(orderFieldName+"||",diNumbers);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedStsList.add((DistrictRequisitionNumbers) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedStsList.add((DistrictRequisitionNumbers) sortedMap.get(key));
				}
			}else{
				sortedStsList=districtRequisitionNumbers;
			}
			List<DistrictRequisitionNumbers>sortedSList=new ArrayList<DistrictRequisitionNumbers>();
			
			if(totalRecord<end)
				end=totalRecord;
			
			if(mapFlag!=2)
				sortedSList=sortedStsList.subList(start,end);
			else
				sortedSList=sortedStsList;
			
			dmRecords.append("<table  id='reqNoTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblDistrictName,sortOrderFieldName,"districtMaster",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblRequisitionNumber,sortOrderFieldName,"requisitionNumber",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblPostionT,sortOrderFieldName,"posType",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			//shriram
			responseText=PaginationAndSorting.responseSortingLink(lblAddedOn,sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			//shriram
			responseText=PaginationAndSorting.responseSortingLink(lblUsed+"<a href='#' id='iconpophover1' class='net-header-text ' rel='tooltip' data-original-title='"+msgDisplaysWhetherRequisitionNumIsAttach+"'><span class='icon-question-sign'></span></a>",sortOrderFieldName,"isUsed",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			dmRecords.append("<script type='text/javascript'>$('#iconpophover1').tooltip();</script>");
			
			responseText=PaginationAndSorting.responseSortingLink( lblHired+"<a href='#' id='iconpophover2' class='net-header-text ' rel='tooltip' data-original-title='"+msgDisplaysWhetherRequisitionNumIsAttach2+"'><span class='icon-question-sign'></span></a>",sortOrderFieldName,"isHired",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			dmRecords.append("<script type='text/javascript'>$('#iconpophover2').tooltip();</script>");
			
			dmRecords.append("<th width='15%' valign='top'>"+lblAct+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			
			if(sortedSList.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+msgNoRequisitionNofound+"</td></tr>" );
			int numOfrecords=0;;
			for (DistrictRequisitionNumbers dNumbers : sortedSList) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+dNumbers.getDistrictMaster().getDistrictName()+"</td>");
				dmRecords.append("<td>"+dNumbers.getRequisitionNumber()+"</td>");
				if(dNumbers.getPosType()!=null && dNumbers.getPosType().equalsIgnoreCase("P"))
					dmRecords.append("<td>"+optPTime+"</td>");
				else
					dmRecords.append("<td>"+optFTime+"</td>");
				
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(dNumbers.getCreatedDateTime())+"</td>");
				if(dNumbers.getIsUsed())
					dmRecords.append("<td> <a href='managejoborders.do?requisitionNumber="+dNumbers.getRequisitionNumber()+"' target='_blank'>"+optY+"</a> </td>");
				else
					dmRecords.append("<td>"+optN+"</td>");
				
				
				if(JRStauas.get(dNumbers.getDistrictRequisitionId())!=null && JRStauas.get(dNumbers.getDistrictRequisitionId()) && reqIsHired.containsKey(dNumbers.getRequisitionNumber()+"##"+dNumbers.getDistrictMaster().getDistrictId())){
					String toolTipContent = "<table width='100%' border='0'>"+reqIsHired.get(dNumbers.getRequisitionNumber()+"##"+dNumbers.getDistrictMaster().getDistrictId())+"</table>";
					dmRecords.append("<td><a data-html='true' id='tooltipPreview"+numOfrecords+"' href='javascript:void(0)' rel='tooltip' data-original-title=\""+toolTipContent+"\" data-placement='top'  ");
					dmRecords.append(">"+optY+"</a></td>");
					dmRecords.append("<script type='text/javascript'>$('#tooltipPreview"+numOfrecords+"').tooltip();</script>");
				}else{
					dmRecords.append("<td>"+optN+"</td>");
				}
				
				/*if(dNumbers.getIsHired().equalsIgnoreCase("Yes")){
					String toolTipContent = "<table>"+reqIsHired.get(dNumbers.getRequisitionNumber()+"##"+dNumbers.getDistrictMaster().getDistrictId())+"</table>";
					dmRecords.append("<td><a data-html='true' id='tooltipPreview"+numOfrecords+"' href='' rel='tooltip' data-original-title=\""+toolTipContent+"\" data-placement='top'  ");
					dmRecords.append(">"+dNumbers.getIsHired()+"</a></td>");
					dmRecords.append("<script type='text/javascript'>$('#tooltipPreview"+numOfrecords+"').tooltip();</script>");
				}else{
					dmRecords.append("<td>"+dNumbers.getIsHired()+"</td>");
				}*/
				
				
				dmRecords.append("<td>");
					if(!dNumbers.getIsUsed()){
						dmRecords.append("<a href='javascript:void(0);' onclick='return getRequisitionNo("+dNumbers.getDistrictRequisitionId()+")'>"+lblEdit+"</a>&nbsp;|&nbsp;");
						dmRecords.append("<a href='javascript:void(0);' onclick=\"return deleteRequisitionNo("+dNumbers.getDistrictRequisitionId()+",'I')\">"+lnkDlt+"");
					}
				dmRecords.append("</td>");
				dmRecords.append("</tr>");
				numOfrecords++;
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public int saveRequisitionNo(Integer districtRequisitionId,Integer districtId,String requisitionNumber,String posType){
		try{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(msgYrSesstionExp);
		    }
			UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
			DistrictRequisitionNumbers districtRequisitionNumbers=null;
			DistrictMaster districtMaster=null;
			if(districtId!=null){
				districtMaster=districtMasterDAO.findById(districtId, false, false);
			}
			
			if(districtRequisitionId!=null){
				districtRequisitionNumbers=districtRequisitionNumbersDAO.findById(districtRequisitionId, false, false);
			}else{
				districtRequisitionNumbers=new DistrictRequisitionNumbers();
				districtRequisitionNumbers.setCreatedDateTime(new Date());
				districtRequisitionNumbers.setDistrictMaster(districtMaster);
				districtRequisitionNumbers.setIsUsed(false);
				districtRequisitionNumbers.setUserMaster(userMaster);
			}
			districtRequisitionNumbers.setPosType(posType);
			districtRequisitionNumbers.setRequisitionNumber(requisitionNumber);
			boolean result=districtRequisitionNumbersDAO.checkDuplicateRequisitionNO(districtRequisitionId,districtMaster,requisitionNumber);
			if(result){
				return 1;
			}else{
				districtRequisitionNumbersDAO.makePersistent(districtRequisitionNumbers);
			}	
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public DistrictRequisitionNumbers getRequisitionNo(Integer districtRequisitionId){
		DistrictRequisitionNumbers districtRequisitionNumbers=null;
		try{
			if(districtRequisitionId!=null){
				districtRequisitionNumbers=districtRequisitionNumbersDAO.findById(districtRequisitionId, false, false);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtRequisitionNumbers;
	}
	public int deleteRequisitionNo(Integer districtRequisitionId){
		DistrictRequisitionNumbers districtRequisitionNumbers=null;
		try{
			if(districtRequisitionId!=null){
				districtRequisitionNumbers=districtRequisitionNumbersDAO.findById(districtRequisitionId, false, false);
				districtRequisitionNumbersDAO.makeTransient(districtRequisitionNumbers);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}
