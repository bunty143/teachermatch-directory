var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	ShowDistrict();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	ShowDistrict();
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
var txtBgColor="#F5E7E1";
function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function isEmailAddress(str) 
{	

	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	return emailPattern.test(str);
	
}
function checkSession(responseText)
{
	if(responseText=='100001')
	{
		alert(resourceJSON.oops)
		window.location.href="quest.do";
	}	
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function chkPasswordExist(pwd)
{
	var res;
	createXMLHttpRequest();  
	queryString = "userchkpwdexist.do?password="+pwd+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{	
				checkSession(xmlHttp.responseText)
				res = xmlHttp.responseText;		
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;

}
function chkPwdPtrn(str)
{
	var pat1 = /[^a-zA-Z]/g; //If any spcl char find it return true otherwise false
	var pat2 = /[a-zA-Z]/g;  //If any alphabet found it return true otherwise false
	//alert(pat1.test(str) && pat2.test(str))
	return pat1.test(str) && pat2.test(str);
}
function checkEmail(emailAddress)
{
	var res;
	createXMLHttpRequest();  
	queryString = "chkemail.do?emailAddress="+emailAddress+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{			
				res = xmlHttp.responseText;				
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
}
function getQuestUserJob(districtId)
{  var div = d3.select("#thin-donut-chart").append("div").attr("class", "toolTip");
	var id="";   
	id=districtId;    
	try
	{
		   $('#loadingDiv').show();
		    UserHomeAjax.getJob(id,{ 
			async: false,
			callback: function(data){	
			$('#loadingDiv').hide();				
			var setcolor;		
			setcolor= d3.scale.ordinal().range(["#008000", "#60a7ee"]);	
			
			  (function () {
			    var chartConfig = {
			        width: 300,
			        height: 300,
			       // hasBottomLabel: true,
			       // bottomMarginForLabel: 30,
			        //color: d3.scale.category10(),        
			        color :setcolor,			        
			        renderHalfDonut: false,
			        // See margin convention: http://bl.ocks.org/mbostock/3019563
			        margin: {
			            top: 25,
			            right: 5,
			            bottom: 5,
			            left: 5
			        },
			    states: {
                    hover: {
                        enabled: true
                    }
                }
			    };
			    var fromValue=1000-data;
			    function makeRandomData() {
			        return {
			            centerLabel: data,
			            bottomLabel: 'world',
			            values: [
			                {label: 'x', value: data},
			                {label: 'y', value: fromValue}
			            ]
			        };
			    }
			    function init() {
			        var chartData = makeRandomData(),
			            chartId = 'thin-donut-chart',
			            d3ChartEl = d3.select('#' + chartId);
			        chartConfig.width = parseInt(d3ChartEl.style('width')) || chartConfig.width;
			        chartConfig.height = parseInt(d3ChartEl.style('height')) || chartConfig.height;
			        drawChart(chartId, chartData, chartConfig);
			    }

			    function drawChart(chartId, chartData, chartConfig) {
			        var width = chartConfig.width,
			            height = chartConfig.height,
			            margin = chartConfig.margin,
			            radius;

			        // Add a bottom margin if there is a label for the bottom
			        if (!!chartConfig.hasBottomLabel) {
			            margin.bottom = chartConfig.bottomMarginForLabel;
			        }

			        // Adjust for margins
			        width = width - margin.left - margin.right;
			        height = height - margin.top - margin.bottom;

			        if (chartConfig.renderHalfDonut) {
			            radius = Math.min(width / 2, height);
			        } else {
			            radius = Math.min(width, height) / 2;
			        }

			        var thickness = chartConfig.thickness || Math.floor(radius / 3);

			        var arc = d3.svg.arc()			        			            
			            .innerRadius(radius - thickness)
			            .outerRadius(radius);

			        var pieFn = d3.layout.pie()			      
			            .sort(null)			           
			            .value(function (d) {
			                return d.value;
			            });

			        if (chartConfig.renderHalfDonut) {
			            pieFn.startAngle(-Math.PI / 2).endAngle(Math.PI / 2);
			        }

			        var centerLabel = (!!chartData.centerLabel) ? chartData.centerLabel : '',
			            bottomLabel = (!!chartData.bottomLabel) ? chartData.bottomLabel : '';

			        var d3ChartEl = d3.select('#' + chartId);

			        // Clear any previous chart
			        d3ChartEl.select('svg').remove();

			        var gRoot = d3ChartEl.append('svg')
			            .attr('width', width + margin.left + margin.right)
			            .attr('height', height + margin.top + margin.bottom)
			            .append('g');

			        if (chartConfig.renderHalfDonut) {
			            gRoot.attr('class', 'half-donut');
			            gRoot.attr('transform', 'translate(' + (width / 2 + margin.left) + ',' + (height + margin.top) + ')');
			        } else {
			            gRoot.attr('transform', 'translate(' + (width / 2 + margin.left) + ',' + (height / 2 + margin.top) + ')');
			        }

			        var middleCircle = gRoot.append('svg:circle')
			            .attr('cx', 0)
			            .attr('cy', 0)
			            .attr('r', radius)
			            .style('fill', '#fff');

			        var g = gRoot.selectAll('g.arc')
			            .data(pieFn(chartData.values))
			            .enter()
			            .append('g')
			            .attr('class', 'arc');

			        g.append('svg:path')			        
			            .attr('d', arc)
			            .style("stroke", "green")
			            .style("stroke-width", "0.5px")			            
			            .style('fill', function (d) {
			                return chartConfig.color(d.data.label);
			            })
			            .attr('data-arc-data', function (d) {
			                return d.data.value;
			            });
			       
			        
			        try
			        {			        	
			        	 //adding tooltip
			        	g.on("mousemove", function(d){
			   			    div.style("left", (d3.event.pageX+15)+"px");
			   			    div.style("top", (d3.event.pageY-10)+"px");
			   				div.style("display", "inline-block");			   				
			   				if(data > 0)
			   				{			   					
			   					div.html(resourceJSON.msgPostedJobs+": "+d.data.value);
			   				}
			   				else
			   				{			   					
			   					//div.html("Posted Jobs: "+d.data.value);
			   					div.html(resourceJSON.msgNoJobPosted);
			   				}
			   		});			        
			        	g.on("mouseout", function(d){
			   		    div.style("display", "none");
			   		});
			        	

			        	
			        }catch(e){alert(e)}

			        gRoot.append('svg:text')
			            .attr('class', 'center-label')			           
			            .text(centerLabel);

			       if (chartConfig.hasBottomLabel) {
			            gRoot.append('svg:text')
			                .attr('class', 'bottom-label')
			                .attr('transform', function (d) {
			                    return 'translate(0, ' + (height / 2 + margin.bottom / 2) + ')';
			                })
			                .text(bottomLabel);
			        }
			    }
			    init();
			})();			
		},
		errorHandler:handleError
		});	
	}
	catch(e)
	{alert(e)}
}
function editDistrict(districtId)
{
	document.getElementById("district").value="";
	document.getElementById("email").value="";
	document.getElementById("fname").value="";
	document.getElementById("lname").value="";
	document.getElementById("password").value="";
	document.getElementById("savepassword").value="";
	$("#editdistrictDiv").show();	
	UserHomeAjax.getDistrictRecords(districtId,{ 
		async: true,
		callback: function(data)
		{		 
			var array=data.split("::");			
			document.getElementById("district").value=array[0];
			document.getElementById("email").value=array[1];
			document.getElementById("fname").value=array[2];
			document.getElementById("lname").value=array[3];
			document.getElementById("savepassword").value=array[4];
			document.getElementById("password").value=array[4].substring(0,8);		
		},
		errorHandler:handleError 
	});
	
}

function showChangePWD()
{
	document.getElementById("oldpassword").value="";
	document.getElementById("newpassword").value="";
	document.getElementById("repassword").value="";
	$('#errordivPwd').empty();
	$('#divServerError').css("background-color","");
	$('#oldpassword').css("background-color","");
	$('#newpassword').css("background-color","");
	$('#repassword').css("background-color","");
	$('#myModal').modal('show');
	document.getElementById("oldpassword").focus();
	return false;
}


function chkPassword()
{
	var oldpassword = document.getElementById("oldpassword");
	var newpassword = document.getElementById("newpassword");
	var repassword = document.getElementById("repassword");


	var cnt=0;
	var focs=0;	
	$('#errordivPwd').empty();
	$('#oldpassword').css("background-color","");
	$('#newpassword').css("background-color","");
	$('#repassword').css("background-color","");


	if(trim(oldpassword.value)=="")
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgcurrentpsw+" <br>");
		if(focs==0)
			$('#oldpassword').focus();

		$('#oldpassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!chkPasswordExist(oldpassword.value))
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgCurrentPassworddoesnotexis+" <br>");
		if(focs==0)
			$('#oldpassword').focus();

		$('#oldpassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(trim(newpassword.value)=="")
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgPleaseenterNewPassword+" <br>");
		if(focs==0)
			$('#newpassword').focus();

		$('#newpassword').css("background-color",txtBgColor);
		cnt++;focs++;
	} 
	else if(!chkPwdPtrn(newpassword.value) || newpassword.value.length<6 )
	{		
		$('#errordivPwd').append("&#149; "+resourceJSON.msgpasswordcombination+" <br>");
		if(focs==0)
			$('#newpassword').focus();

		$('#newpassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(repassword.value)=="")
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgReenterPassword+"<br>");
		if(focs==0)
			$('#repassword').focus();

		$('#repassword').css("background-color",txtBgColor);
		cnt++;focs++;
	} 
	if(trim(newpassword.value)!="" && trim(repassword.value)!="" && !(trim(newpassword.value)==trim(repassword.value)))
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgrepasswordnotmatch+" <br>");
		if(focs==0)
			$('#newpassword').focus();

		$('#newpassword').css("background-color",txtBgColor);
		$('#repassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(cnt==0)
	{
		$('#myModal').modal('hide');
		$('#loadingDiv').show();
		document.getElementById("password").value=newpassword.value;
		$('#errordivPwd').empty();
		$('#divServerError').css("background-color","");
		$('#oldpassword').css("background-color","");
		$('#newpassword').css("background-color","");
		$('#repassword').css("background-color","");



		createXMLHttpRequest();  
		queryString = "changeusrpwd.do?password="+newpassword.value+"&dt="+new Date().getTime();
		xmlHttp.open("POST", queryString, true);
		xmlHttp.onreadystatechange = function()
		{
			$('#loadingDiv').hide();
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{			
					checkSession(xmlHttp.responseText)					
					$('#myModalUpdateMsg').modal('show');
					document.getElementById("messageText").innerHTML=resourceJSON.msgSuccessfullychangepass;
				}			
				else
				{
					alert(resourceJSON.msgServerErr);
				}
			}
		}
		xmlHttp.send(null);	
		return true;
	}
	else
	{
		$('#errordivPwd').show();
		return false;
	}

}




function updateUser(oldDistrictEmail)
{
try
{
    var reqType=document.querySelector('input[name="reqType"]:checked').value;	
    var firstName=document.getElementById("fname");
    var lastName=document.getElementById("lname");
	var emailAddress = document.getElementById("email");
	//var oldpassword = document.getElementById("oldpassword");
	//var newpassword = document.getElementById("newpassword");
	//var repassword = document.getElementById("repassword");	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	
	$('#fname').css("background-color","");
	$('#lname').css("background-color","");
	$('#email').css("background-color","");
	//$('#oldpassword').css("background-color","");
	//$('#newpassword').css("background-color","");
	//$('#repassword').css("background-color","");	
	if(trim(firstName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#fname').focus();
		
		$('#fname').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(lastName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#lname').focus();
		
		$('#lname').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#email').focus();
		
		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!isEmailAddress(emailAddress.value))
	{		
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#email').focus();
		
		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(emailAddress.value!=oldDistrictEmail)
	{
		if(checkEmail(emailAddress.value))
		{
		$('#errordiv').append("&#149; "+resourceJSON.msgaladyemail+"<br>");
		if(focs==0)
			$('#email').focus();
		
		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
		}
	}	
	if(cnt==0)
	{	
		    UserHomeAjax.editDistrict(trim(firstName.value),trim(lastName.value),trim(emailAddress.value),oldDistrictEmail,reqType,{ 
			async: false,
			callback: function(data){		
			if(data==1)
			{
				$('#myModalUpdateMsg').modal('show');
				document.getElementById("messageText").innerHTML=resourceJSON.msgSuccessfullyUpdatedRecord;
			
			}
			else
			{
				alert(resourceJSON.msgServerErr);
				
				return false;
			}
			
			},errorHandler:handleError 
		});	
		return true;
	}
	else
	{
		$('#errordiv').show();
		return false;
	}
	}
	catch(e){alert(e)}
}

function cancelUpdateUserr()		
{
	
	    $('#editdistrictDiv').hide();
		$('#errordiv').empty();
}
function hidemessageDiv()
{
	    $('#editdistrictDiv').hide();
		$('#errordiv').empty();
}
function onLoadDisplayHideSearchBox(entity_type){	
	if(entity_type ==2){
		$("#Searchbox").hide();
		$('#SearchTextboxDiv').hide();
	}else if(entity_type ==1){
		$("#Searchbox").fadeIn();
		$('#SearchTextboxDiv').fadeIn();
		document.getElementById("districtName").focus();
		document.getElementById("MU_EntityType").value=2;
	}else{
		$("#Searchbox").fadeIn();
		$('#SearchTextboxDiv').hide();
	}
	ShowDistrict();
}

/*========  Show District ===============*/
function searchDistrict()
{
	page = 1;
	ShowDistrict();
}
function ShowDistrict()
{
	$('#loadingDiv').fadeIn();
	var entityID	=	document.getElementById("MU_EntityType").value;
	
	var searchText	="";
	try{
		searchText=document.getElementById("districtName").value;
	}catch(err){
		searchText="";
	}
	delay(1000);
	UserHomeAjax.SearchDistrictRecords(entityID,searchText,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{
			$('#divMain').html(data);
			applyScrollOnTbl();
			$('#loadingDiv').hide();
		},
		errorHandler:handleError 
	});

}
function delay(sec){
	var starttime = new Date().getTime();
	starttime = starttime+sec;
	while(true){
		if(starttime< new Date().getTime()){
			break;
		}
	}
}
