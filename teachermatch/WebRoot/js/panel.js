/* @Author: Gagan 
 * @Discription: view of edit Panel js.
*/
/*========  For Sorting  ===============*/
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayPanel();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayPanel();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*======= Save Panel on Press Enter Key ========= */
function chkForEnterSavePanel(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		savePanel();
	}	
}

function searchData()
{
	//alert($('#districtId').val());
	$('#districtName').css("background-color", "");
	if($('#districtId').val()==0)
	{
		//alert(" Hi ");
		$('#errordiv').html("&#149; "+resourceJSON.PlzEtrAnyDistrictName+"<br>");
		$('#districtName').focus();
		$('#districtName').css("background-color", "#F5E7E1");
		$('#errordiv').show();
		return;
	}
	
	$('#searchItem').fadeOut(1000);
	$('#sa').fadeIn(1200);
	$('#closePan').html("<a style='padding-right: 6px;'   onclick='hideSearchPan();' href='javascript:void(0);' ><i class='icon-remove icon-large' ></i></a>");
	showTMDefaultView();
	
	$('#loadingCounter').val();
	$('#jobLoadingCounter').val();
	$('#yellowJobLoadingCounter').val();
	$('#loadingFlag').val(1);
	
	$(".items").empty();
	
	if(dwr.util.getValue("districtId")!=0)
		districtMaster={districtId:dwr.util.getValue("districtId")};
	
	if(dwr.util.getValue("schoolId")!="" && dwr.util.getValue("schoolId")!=0)
		schoolMaster={schoolId:dwr.util.getValue("schoolId")};
	else 
		schoolMaster=null;
	
	//alert(districtMaster.districtId+"districtMaster "+districtMaster.districtName+" schoolMaster "+schoolMaster);
	
	$('#panelDiv').fadeIn();
	var districtId	=	document.getElementById("districtId").value;
	var districtName	=	document.getElementById("districtName").value;
	$("#distName").val(districtName);
	//alert("districtName -----"+districtName+" "+$("districtName").val()+" --------------- "+$("districtId").val()+" districtId "+districtId);
	displayPanel();
}
function hideSearchPan()
{
	$('#districtName').css("background-color", "");
	$('#searchItem').fadeOut(1000);
	$('#sa').fadeIn(1200);
}
function getSearchPan()
{
	$('#errordiv').hide();
	$('#districtName').css("background-color", "");
	//$('#districtName').focus();
	$('#searchItem').fadeIn(1000);
	$('#sa').fadeOut(1200);
	$('#panelDiv').fadeOut(1200);
	clearPanel();
	//setTMDefaultView();
}

function showTMDefaultView()
{
	$('#feed').show();
	$('#sticker').show();
}




/*========  displayPanel ===============*/
function displayPanel()
{
	$('#panelDiv').fadeIn();
	var districtId	=	$("#districtId").val();
	//alert(" districtId "+districtId);
	PanelAjax.displayPanel(districtId,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data){
		$('#panelGrid').html(data);
		applyScrollOnTbl();
	},
	errorHandler:handleError  
	});
}
/*========  Add New Panel===============*/
function addNewPanel()
{
	//var districtName= $("districtName").val();
	//alert(districtName+"  DDADUTADAD "+$("districtName").val());
	//$("distName").val($("districtName").val());
	dwr.util.setValues({ panelId:null,panelName:null,multiplier:null,panelUId:null});
	document.getElementById("divPanelForm").style.display	=	"block";
	$('#panelName').focus();
	$('#errorpaneldiv').hide();
	$('#panelName').css("background-color", "");
	
	//$('#displayInPDReport').attr('checked', false); // will uncheck the checkbox with id displayInPDReport
	document.getElementById("divDone").style.display		=	"block";
	document.getElementById("divManage").style.display		=	"none";
	return false;
}
/*========  Clear Panel Fields ===============*/
function clearPanel()
{
	document.getElementById("divPanelForm").style.display	=	"none";
	document.getElementById("divDone").style.display		=	"none";
	document.getElementById("divManage").style.display		=	"none";
	document.getElementById("panelName").value				=	"";
	//document.getElementsByName("panelStatus")[0].checked	=	true;
}
/*========  Save Panel  ===============*/
function savePanel()
{
	
	var panelId			=	trim(document.getElementById("panelId").value);
	var districtId		=	trim(document.getElementById("districtId").value);
	var panelName		=	trim(document.getElementById("panelName").value);

	//var panelUId			=	trim(document.getElementById("panelUId").value);
	//var multiplier			=	trim(document.getElementById("multiplier").value);
	//var displayInPDReport	=	document.getElementById("displayInPDReport");
	//var panelStatus		=	document.getElementsByName("panelStatus");
	//alert(" panelName "+panelName);
	
	if (panelName=="")
	{
		document.getElementById('errorpaneldiv').innerHTML		=	'&#149;'+resourceJSON.PlzEtrPanelName;
		$('#errorpaneldiv').show();
		$('#panelName').css("background-color", "#F5E7E1");
		document.getElementById("panelName").focus();
		return false;
	}

	dwr.engine.beginBatch();		
	PanelAjax.savePanel(panelId,districtId,panelName,{ 
		async: true,
		callback: function(data)
		{
			//alert(" data "+data);
			if(data==3)
			{
				document.getElementById('errorpaneldiv').innerHTML	=	'&#149; '+resourceJSON.PlzEtrUniquePanelName;
				$('#errorpaneldiv').show();
				$('#panelName').css("background-color", "#F5E7E1");
				$('#panelName').focus();
			}else
			{
				displayPanel();
				clearPanel();
			}
		},
		errorHandler:handleError 
	});
	dwr.engine.endBatch();
}
/*========  deletePanel ===============*/
function deletePanel(panelId)
{
	//alert(" delete Panel : panelId : "+panelId);
	$("#panelId").val(panelId);
		$('#deletePanelDiv').modal('show');
				
}
function deleteconfirm()
{
	var panelId=$("#panelId").val();
	
	PanelAjax.deletePanel(panelId, { 
		async: true,
		callback: function(data)
		{
			$('#deletePanelDiv').modal('hide');
			displayPanel();
			clearPanel();
		},
		errorHandler:handleError 
	});
				
}




/*========  Edit Panel ===============*/
function editPanel(panelId)
{
	//alert(" panelId "+panelId);
	$('#errorpaneldiv').hide();
	$('#panelName').css("background-color", "");
	//var displayInPDReport									=	document.getElementById("displayInPDReport");
	document.getElementById("divDone").style.display		=	"none";
	document.getElementById("divPanelForm").style.display	=	"block";
	//var panelStatus	=	document.getElementsByName("panelStatus");
	
	$('#panelName').focus();
		PanelAjax.editPanel(panelId, { 
			async: true,
			callback: function(data)
			{
				dwr.util.setValues(data);
			},
		errorHandler:handleError 
		});
	
	document.getElementById("divManage").style.display="block";
	return false;
}


/* =====================================   Panel Member js Method [ END ] =========================================*/

///////////////////////////////////////////////////////////////////////////////////////////////
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
		BatchJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(document.getElementById("districtName").value==""){
		document.getElementById('schoolName').readOnly=true;
		document.getElementById('schoolName').value="";
		document.getElementById('schoolId').value="0";
	}
	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
			document.getElementById('schoolName').readOnly=false;
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtId").value;
		BatchJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}




var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}



