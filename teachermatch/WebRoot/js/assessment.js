var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
var lastGroupValue="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}

	noOfRows = document.getElementById("pageSize").value;
	getAssessmentGrid();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	var pageIdNo	=	document.getElementById("pageIdNo").value;
	if(pageIdNo==1)
		getAssessmentGrid();
	else
		getAssessmentSectionGrid();
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}
function init() {
	  dwr.engine.setActiveReverseAjax(true);
}
/*======= Show AssessmentGrid on Press Enter Key ========= */
function chkForEnterGetAssessmentGrid(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		getAssessmentGrid();
	}	
}
/*======= Save Assessment on Press Enter Key ========= */
function chkForEnterSaveAssessment(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	//alert(evt.srcElement.id);
	/*if((charCode==13) && (evt.srcElement.id!='assessmentDescription'))
	{
		saveAssessment();
	}	*/
	if((charCode==13) && (evt.srcElement.className!='jqte_editor'))
	{
		saveAssessment();
	}
}
/*======= Save AssessmentSection on Press Enter Key ========= */
function chkForEnterSaveAssessmentSection(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;

	//alert(evt.srcElement.className);

	//alert(evt.srcElement.id);
	/*if((charCode==13) && (evt.srcElement.id!='sectionDescription' ))
	{
		if(evt.srcElement.id!='sectionInstructions')
		saveAssessmentSection();
	}	*/

	if((charCode==13))
	{
		if(evt.srcElement.className!='jqte_editor')
			saveAssessmentSection();
	}	
}

/*======= Save AssessmentSection Question on Press Enter Key ========= */
function chkForEnterSaveAssessmentQuestion(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	//alert(evt.srcElement.id);
	/*if((charCode==13) && (evt.srcElement.id!='question' ))
	{
		if(evt.srcElement.id!='questionInstruction')
			$('#saveQues').click();
	}	*/
	if((charCode==13))
	{
		if(evt.srcElement.className!='jqte_editor')
			$('#saveQues').click();
	}	
}

/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}
function searchInventory()
{
	$('#loadingDiv').fadeIn();
	page = 1;
	getAssessmentGrid();
}
function getAssessmentGrid()
{
	$('#loadingDiv').show();
	var assessmentType=document.getElementById("assessmentType1").value;
	var assessmentStatus=document.getElementById("assessmentStatus1").value;
	var districtName=document.getElementById("districtName").value.trim();
	var districtId=document.getElementById("districtHiddenlId").value;
	if(districtName!='' && districtId=='')
		districtId=null;
	AssessmentAjax.getAssessment(assessmentType,assessmentStatus,districtId,noOfRows,page,sortOrderStr,sortOrderType, { 
		async: true,
		callback: function(data){
		$('#divMain').html(data);
		$('#loadingDiv').hide();
		applyScrollOnTbl();
	},
	errorHandler:handleError  
	});
}

function addAssessment()
{
	getJobOrderDistricts();
	$('#divAssessmentRow').show();
	$('#assessmentName').focus();
	$('#errordiv').hide();
	$('#divDone').show();
	$('#divManage').hide();
	$('#jobs').html("");
	$('#jobs').hide();
	$('#def3').hide();
	$('#def4').hide();	
	$('#new_gp').hide();
	$('#sel_gp').hide();
	$('#gp1').hide();
	$('#gp2').hide();
	dwr.util.removeAllOptions("schoolMaster");
	$('#schoolMaster').hide();
	$('#schoolLbl').hide();
	dwr.util.removeAllOptions("jobOrders");
	$('#jobOrders').hide();
	$('#infoData').hide();
	$('#isDefaultEPIGroupDiv1').hide();
	$('#isDefaultEPIGroupDiv2').hide();
	document.getElementById("defaultEPIGroupId").value = '';
	$("#defaultEPIGroupId").attr("name", "");
	$('#jsjo1').show();
	$('#jsjo2').show();
	$('#jot1').show();
	$('#jot2').show();
	$('#def1').show();
	$('#def2').show();
	$('#jobOrderType').attr("disabled",false);
	//$('#isDefault').attr("disabled",false);
	$('#schoolMaster').attr("disabled",false);
	$('#districtMaster').attr("disabled",false);
	/*var tnl = document.getElementById("jobOrders");  

	for(i=0;i<tnl.length;i++){  
		if(tnl[i].selected == true){  
			tnl[i].selected= false;
		}
	}*/
	//$('#jobOrders').show();
	$('#instruction1').hide();
	$('#instruction2').hide();
	checkDefaultPDPAssociation();
	clearData();
	return false;
}
function deselectJobs()
{
	var tnl = document.getElementById("jobOrders");  

	for(i=0;i<tnl.length;i++){  
		if(tnl[i].selected == true){  
			tnl[i].selected= false;
		}
	}
}
function getJobOrders(assessmentId)
{

	var assessmentDetail = {assessmentId:assessmentId};
	var districtMaster = {districtId:dwr.util.getValue("districtMaster")};
	var schoolMaster = {schoolId:(dwr.util.getValue("schoolMaster")=='Select School'?0:dwr.util.getValue("schoolMaster"))};
	// alert(schoolMaster.schoolId);
	AssessmentAjax.getJobOrders(assessmentDetail,districtMaster,schoolMaster,{ 
		async: false,
		callback: function(data){
		//alert(data);
		dwr.util.removeAllOptions("jobOrders");
		for (var key in data) {
			  if (data.hasOwnProperty(key)) {
			    //alert(key + " -> " + data[key].jobId);
				  
				  var x = document.getElementById("jobOrders");
					var option = document.createElement("option");
					option.value = data[key].jobId;
					option.text = data[key].jobTitle+" ("+data[key].jobId+")";
					x.add(option);
			  }
			}
	},
	errorHandler:handleError  
	});	
}
function checkedUnchecked(dis)
{
	//alert(dis.id);
	if(dis.checked == true)
		dis.checked = false;
	else
		dis.checked = true;
}

function UncheckJobs()
{
	//alert('dd');
	var jobOrders = document.getElementById("jobOrders");
	for ( var i = 0; i < jobOrders.length; i++) {
		jobOrders[i].checked = false;
		//jobOrders[i].checked = true;
	}
}

function getJobOrderDistricts()
{
	var createdForEntity=2;
	createdForEntity=dwr.util.getValue("jobOrderType")=='2'?3:2;
	AssessmentAjax.getJobOrderDistricts(createdForEntity,{ 
		async: false,
		callback: function(data){
		dwr.util.removeAllOptions("districtMaster");
		dwr.util.addOptions("districtMaster",data,"districtId","districtName");
	},
	errorHandler:handleError  
	});	
}
function getJobOrderTypeData(districtId)
{
	
	if(document.getElementById("jobOrderType").value==2)
	{
		getJobOrderSchools(districtId);
		$('#schoolMaster').show();
		$('#schoolLbl').show();
	}
	else
	{
		$('#jobOrders').show();
		$('#infoData').show();
		getJobOrders(0);
		dwr.util.removeAllOptions("schoolMaster");
		dwr.util.addOptions("schoolMaster",["Select School"]);
	}
	if(districtId==0)
	{
		dwr.util.removeAllOptions("jobOrders");
	}else
	{
		displayJobCategoryByDistrict(districtId,"0");
	}
}
function displayJobCategoryByDistrict(districtId,jobCatId)
{
	AssessmentAjax.displayJobCategoryByDistrictForJSI(districtId,jobCatId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("jobCategoryMaster").innerHTML=data;
			$('#jobCategoryMaster').show();
			$('#jobCategoryLbl').show();
		}
	});
}

function getJobOrderTypeDataSOJ()
{
	$('#jobOrders').show();
	$('#infoData').show();
	//alert(dwr.util.getValue("schoolMaster"));
	if(document.getElementById("jobOrderType").value==2 && dwr.util.getValue("schoolMaster")>0)
	{
		getJobOrders(0);
	}else
		dwr.util.removeAllOptions("jobOrders");
	//getJobOrders(0);
}
function clearJobOrderTypeData()
{
	if(document.getElementById("jobOrderType").value==1)
	{
		dwr.util.removeAllOptions("schoolMaster");
		dwr.util.addOptions("schoolMaster",["Select School"]);

		if(dwr.util.getValue("districtMaster")>0)
			getJobOrderTypeData(dwr.util.getValue("districtMaster"))
			//$('#jobOrders').hide();
			//$('#infoData').hide();
			dwr.util.removeAllOptions("jobOrders");
		$('#schoolMaster').hide();
		$('#schoolLbl').hide();
	}else
	{
		dwr.util.removeAllOptions("jobOrders");
		if(dwr.util.getValue("districtMaster")>0)
		{
			$('#schoolMaster').show();
			$('#schoolLbl').show();
			getJobOrderSchools(dwr.util.getValue("districtMaster"));
		}
	}
}
function getJobOrderSchools(districtId)
{
	//alert("districtId: "+districtId);
	var districtMaster = {districtId:districtId};
	AssessmentAjax.getJobOrderSchools(districtMaster,{ 
		async: false,
		callback: function(data){
		//alert(data);
		dwr.util.removeAllOptions("schoolMaster");
		dwr.util.addOptions("schoolMaster",data,"schoolId","schoolName");
	},
	errorHandler:handleError  
	});	
}
function clearAssessment()
{
	dwr.util.setValues({ assessmentId:null,assessmentName:null,assessmentDescription:null,jobOrderType:null,isDefault:null,isResearchEPI:null,status:null,assessmentSessionTime:null,questionSessionTime:null,questionRandomization:false,optionRandomization:false,splashInstruction1:null,splashInstruction2:null});

	//document.getElementsByName("assessmentType")[1].checked=true;
	$("select[name='assessmentType']").val(1);
	$('#divAssessmentRow').hide();
	$('#divDone').hide();
	$('#divManage').hide();
	$('#jobOrders').hide();

	$('#assessmentName').css("background-color", "");
	//$('#assessmentDescription').css("background-color", "");
	$('#assDescription').find(".jqte_editor").css("background-color", "");
	$('#assDescription').find(".jqte_editor").html("");
	$('#jobOrders').css("background-color", "");
	$('#assessmentGroupName').css("background-color", "");
	$('#assessmentGroupDetails').css("background-color", "");
	$('#jobs').html("");
	$('#jobs').hide();
	$('#def3').hide();
	$('#def4').hide();
	$('#isResearchEPI').attr("disabled",false);
	$('#noOfExperimentQuestions').val(0);
	$('#noOfExperimentQuestions').css("background-color", "");
	$('#scoreLookupMaster').css("background-color", "");
	document.getElementById("scoreDiv").style.display="none";
	
	lastGroupValue='';
	
	$('#instruction1').find(".jqte_editor").css("background-color", "");
	$('#instruction1').find(".jqte_editor").html("");
	$('#instruction2').find(".jqte_editor").css("background-color", "");
	$('#instruction2').find(".jqte_editor").html("");
}
function clearData()
{

	dwr.util.setValues({ assessmentId:null,assessmentName:null,assessmentDescription:null,jobOrderType:null,isDefault:null,isResearchEPI:null,status:null,assessmentSessionTime:null,questionSessionTime:null,questionRandomization:false,optionRandomization:false,splashInstruction1:null,splashInstruction2:null});

	//document.getElementsByName("assessmentType")[1].checked=true;
	$("select[name='assessmentType']").val(2);
	$('#assessmentName').attr("disabled",false);
	$("select[name='assessmentType']").prop("disabled",false);
	/*
	var Atype=document.getElementsByName("assessmentType");
	for(i=0;i<Atype.length;i++)
		Atype[i].disabled=false; */
	$('#questionSessionTime').attr("disabled",false);
	$('#assessmentSessionTime').attr("disabled",false);
	$('#questionRandomization').attr("disabled",false);
	$('#optionRandomization').attr("disabled",false);
	$('#jobOrderType').attr("disabled",false);
	//$('#isDefault').attr("disabled",false);

	$('#assessmentName').css("background-color", "");
	//$('#assessmentDescription').css("background-color", "");
	$('#assDescription').find(".jqte_editor").css("background-color", "");
	$('#assDescription').find(".jqte_editor").html("");
	$('#jobOrders').css("background-color", "");
	$('#jobs').html("");
	$('#jobs').hide();
	$('#isResearchEPI').attr("disabled",false);

	dwr.util.removeAllOptions("schoolMaster");
	$('#schoolLbl').hide();
	$('#schoolMaster').hide();
	$('#jobCategoryLbl').hide();
	$('#jobCategoryMaster').hide();
	$('#assessmentGroupName').val('');
	$('#assessmentGroupName').css("background-color", "");
	$('#assessmentGroupDetails').css("background-color", "");
	$('#noOfExperimentQuestions').val(0);
	$('#noOfExperimentQuestions').css("background-color", "");
	$('#scoreLookupMaster').css("background-color", "");
	document.getElementById("scoreDiv").style.display="none";
	
	lastGroupValue='';
	
	$('#instruction1').find(".jqte_editor").css("background-color", "");
	$('#instruction1').find(".jqte_editor").html("");
	$('#instruction2').find(".jqte_editor").css("background-color", "");
	$('#instruction2').find(".jqte_editor").html("");
}
function editAssessment(assessmentId,noAttempts)
{
	$('#loadingDiv').show();
	$('#errordiv').hide();
	$('#divDone').hide();
	$('#assessmentName').css("background-color", "");
	//$('#assessmentDescription').css("background-color", "");
	$('#assDescription').find(".jqte_editor").css("background-color", "");
	$('#jobOrders').css("background-color", "");
	$('#divAssessmentRow').show();
	$('#assessmentName').focus();
	$('#jobs').html("");
	$('#jobCategoryMaster').hide();
	$('#jobCategoryLbl').hide();
	$('#assessmentGroupName').css("background-color", "");
	$('#assessmentGroupDetails').css("background-color", "");
	$('#noOfExperimentQuestions').css("background-color", "");
	$('#scoreLookupMaster').css("background-color", "");
	//$('#assessmentName').attr("disabled","disabled");
	//$('#jobOrders').attr("disabled","disabled");
	
	if(noAttempts>0)
		$('#isResearchEPI').attr("disabled","disabled");
	else
		$('#isResearchEPI').attr("disabled",false);
	
	$("select[name='assessmentType']").prop("disabled",true);
	
	$('#instruction1').find(".jqte_editor").css("background-color", "");
	$('#instruction1').find(".jqte_editor").html("");
	$('#instruction2').find(".jqte_editor").css("background-color", "");
	$('#instruction2').find(".jqte_editor").html("");
	/*
	var Atype=document.getElementsByName("assessmentType");
	for(i=0;i<Atype.length;i++)
		Atype[i].disabled=true; */
	//$('#questionSessionTime').attr("disabled","disabled");
	//$('#assessmentSessionTime').attr("disabled","disabled");
	//$('#questionRandomization').attr("disabled","disabled");
	//$('#optionRandomization').attr("disabled","disabled");

	AssessmentAjax.getAssessmentById(assessmentId, { 
		async: true,
		errorHandler:handleError,
		callback:  function(data)
		{	
			$('#assDescription').find(".jqte_editor").html(data.assessmentDescription);
			$('#instruction1').find(".jqte_editor").html(data.splashInstruction1);
			$('#instruction2').find(".jqte_editor").html(data.splashInstruction2);
	
			if(data.districtMaster!=null)
			{
				$("#districtMaster").html("<option value="+data.districtMaster.districtId+">"+data.districtMaster.districtName+"</option>");
				$('#districtMaster').attr("disabled","disabled");
				$("#districtMaster").show();
				var jobCategoryId =0;
				
				if(data.jobCategoryMaster!=null)
					jobCategoryId = data.jobCategoryMaster.jobCategoryId;
				
				displayJobCategoryByDistrict(data.districtMaster.districtId,jobCategoryId);
				
			}else
				getJobOrderDistricts();
	
			if(data.schoolMaster!=null)
			{
				$("#schoolLbl").show();
				$("#schoolMaster").show();
				$('#schoolMaster').attr("disabled","disabled");
				$("#schoolMaster").html("<option value="+data.schoolMaster.schoolId+">"+data.schoolMaster.schoolName+"</option>");
			}else
			{
				dwr.util.removeAllOptions("schoolMaster");
				$("#schoolLbl").hide();
				$("#schoolMaster").hide();
				$('#schoolMaster').attr("disabled",false);
			}
	
			//getting jobOrders
			//if(document.getElementsByName("assessmentType")[1].checked == true)
			//getJobOrders(assessmentId);
	
			$('#assessmentName').focus();
			dwr.util.setValues(data);
			var obj=data.assessmentJobRelations;
			var len=obj.length;
			//alert(len);
			if(len!=0)
			{
				for(i=0;i<len;i++)
				{
					//document.getElementById(obj[i].jobId.jobId).selected=true;
					//$("#jobOrders").find('option:selected').attr("readonly",true);
					//alert(obj[i].jobId.jobId);
					$('#'+obj[i].jobId.jobId).attr("disabled",true);
					//$('#'+obj[i].jobId.jobId).attr('readonly', true);
				}
	
			}
			var pdpReportType=data.pdpreporttype;
			if(pdpReportType==null)
				pdpReportType=0;
			if($("select[name='assessmentType']").val() == "1")
			{
				$("#jot1").hide();
				$("#jot2").hide();
				$("#def1").hide();
				$("#def2").hide();
				$('#jobOrderType').attr("disabled",false);
				//$('#isDefault').attr("disabled",false);
				$('#districtMaster').attr("disabled",false);
				$('#schoolMaster').attr("disabled",false);
				$("#jobOrders").hide();
				$("#infoData").hide();
				$("#jsjo1").hide();
				$("#jsjo2").hide();
				$('#def3').show();
				$('#new_gp').show();
				$('#sel_gp').show();
				$('#gp1').show();
				$('#gp2').show();
				$('#assessmentGroupName').val('');
				$('#assessmentGroupName').prop('disabled', true);
				$('#assessmentGroupDetails').prop('disabled', false);
				getGroupsByAssessmentType($("select[name='assessmentType']").val(), 1);
				lastGroupValue = data.assessmentGroupDetails.assessmentGroupId;
				dwr.util.setValue("assessmentGroupDetails", data.assessmentGroupDetails.assessmentGroupId);
				$("input[name='rec_group']").eq(1).prop('checked',"checked");
				
				$("#PDPassociatedoptyes").attr('checked', true);// ADD PDP Configuration...
				$("#PDPselect").prop('selectedIndex', pdpReportType);
				$("#PDPselect").prop('disabled',false);
				$("#PDPassociatedoptno").prop('disabled',false);
				$("#PDPassociatedoptyes").prop('disabled',false);
				
				document.getElementById("scoreDiv").style.display="block";
				if(data.scoreLookupMaster==null){
					$("#score2").prop('checked',"checked");
					document.getElementById("selectScoreDiv").style.display="none";
				}
				else{
					$("#score1").prop('checked',"checked");
					getScoringMethods($("select[name='assessmentType']").val());
					document.getElementById("selectScoreDiv").style.display="block";
					$('#scoreLookupMaster').val(data.scoreLookupMaster.lookupId);
				}
				if(data.assessmentGroupDetails.isDefault==null || data.assessmentGroupDetails.isDefault==0)
					$("#isDefaultEPI").prop('checked',false);
				else if(data.assessmentGroupDetails.isDefault==1)
					$("#isDefaultEPI").prop('checked',true);
				
			} else if($("select[name='assessmentType']").val() == "3") {
				$("#jot1").hide();
				$("#jot2").hide();
				$("#def1").hide();
				$("#def2").hide();
				$('#jobOrderType').attr("disabled",false);
				//$('#isDefault').attr("disabled",false);
				$('#districtMaster').attr("disabled",false);
				$('#schoolMaster').attr("disabled",false);
				$("#jobOrders").hide();
				$("#infoData").hide();
				$("#jsjo1").hide();
				$("#jsjo2").hide();
				$('#def3').hide();
				$('#def4').hide();
				$('#new_gp').show();
				$('#sel_gp').show();
				$('#gp1').show();
				$('#gp2').show();
				$('#assessmentGroupName').val('');
				$('#assessmentGroupName').prop('disabled', true);
				$('#assessmentGroupDetails').prop('disabled', false);
				getGroupsByAssessmentType($("select[name='assessmentType']").val(), 1);
				lastGroupValue = data.assessmentGroupDetails.assessmentGroupId;
				dwr.util.setValue("assessmentGroupDetails", data.assessmentGroupDetails.assessmentGroupId);
				$("input[name='rec_group']").eq(1).prop('checked',"checked");
				
				$("#PDPassociatedoptyes").attr('checked', true);// ADD PDP Configuration...
				$("#PDPselect").prop('selectedIndex', pdpReportType);
				$("#PDPselect").prop('disabled',false);
				$("#PDPassociatedoptno").prop('disabled',false);
				$("#PDPassociatedoptyes").prop('disabled',false);
				
				document.getElementById("scoreDiv").style.display="block";
				if(data.scoreLookupMaster==null){
					$("#score2").prop('checked',"checked");
					document.getElementById("selectScoreDiv").style.display="none";
				}
				else{
					$("#score1").prop('checked',"checked");
					getScoringMethods($("select[name='assessmentType']").val());
					document.getElementById("selectScoreDiv").style.display="block";
					$('#scoreLookupMaster').val(data.scoreLookupMaster.lookupId);
				}
			} else if($("select[name='assessmentType']").val() == "4") {
				$("#jot1").hide();
				$("#jot2").hide();
				$("#def1").hide();
				$("#def2").hide();
				$('#jobOrderType').attr("disabled",false);
				//$('#isDefault').attr("disabled",false);
				$('#districtMaster').attr("disabled",false);
				$('#schoolMaster').attr("disabled",false);
				$("#jobOrders").hide();
				$("#infoData").hide();
				$("#jsjo1").hide();
				$("#jsjo2").hide();
				$('#def3').hide();
				$('#def4').hide();
				$('#new_gp').show();
				$('#sel_gp').show();
				$('#gp1').show();
				$('#gp2').show();
				$('#assessmentGroupName').val('');
				$('#assessmentGroupName').prop('disabled', true);
				$('#assessmentGroupDetails').prop('disabled', false);
				getGroupsByAssessmentType($("select[name='assessmentType']").val(), 1);
				lastGroupValue = data.assessmentGroupDetails.assessmentGroupId;
				dwr.util.setValue("assessmentGroupDetails", data.assessmentGroupDetails.assessmentGroupId);
				$("input[name='rec_group']").eq(1).prop('checked',"checked");
				
				$("#PDPassociatedoptyes").attr('checked', true);// ADD PDP Configuration...
				$("#PDPselect").prop('selectedIndex', pdpReportType);
				$("#PDPselect").prop('disabled',false);
				$("#PDPassociatedoptno").prop('disabled',false);
				$("#PDPassociatedoptyes").prop('disabled',false);
				
				document.getElementById("scoreDiv").style.display="block";
				if(data.scoreLookupMaster==null){
					$("#score2").prop('checked',"checked");
					document.getElementById("selectScoreDiv").style.display="none";
				}
				else{
					$("#score1").prop('checked',"checked");
					getScoringMethods($("select[name='assessmentType']").val());
					document.getElementById("selectScoreDiv").style.display="block";
					$('#scoreLookupMaster').val(data.scoreLookupMaster.lookupId);
				}
			}
			else
			{
				$("#jot1").show();
				$("#jot2").show();
				$("#def1").show();
				$("#def2").show();
				$('#jobOrderType').attr("disabled","disabled");
				//$('#isDefault').attr("disabled","disabled");
				$('#districtMaster').attr("disabled",true);
				$('#schoolMaster').attr("disabled",true);
				$("#jobOrders").show();
				$("#infoData").show();
				$("#jsjo1").show();
				$("#jsjo2").show();
				$('#def3').hide();
				$('#def4').hide();
				$('#new_gp').hide();
				$('#sel_gp').hide();
				$('#gp1').hide();
				$('#gp2').hide();
				// selected jobs
				$("#PDPassociatedoptno").attr('checked', true);// ADD PDP Configuration...
				$("#PDPselect").prop('selectedIndex', 0);
				$("#PDPselect").prop('disabled',true);
				$("#PDPassociatedoptno").prop('disabled',true);
				$("#PDPassociatedoptyes").prop('disabled',true);
				
				getSelectedJobs(assessmentId);
				document.getElementById("scoreDiv").style.display="none";
			}
	
			getJobOrders(0);
			$('#loadingDiv').hide();
		}
	});

	document.getElementById("divManage").style.display="block";

	return false;
}
function validateAssessment()
{
	$('#errordiv').empty();
	$('#assessmentName').css("background-color", "");
	//$('#assessmentDescription').css("background-color", "");
	$('#assDescription').find(".jqte_editor").css("background-color", "");
	$('#jobOrders').css("background-color", "");
	$('#districtMaster').css("background-color", "");
	$('#schoolMaster').css("background-color", "");
	$('#assessmentGroupName').css("background-color", "");
	$('#assessmentGroupDetails').css("background-color", "");
	$('#noOfExperimentQuestions').css("background-color", "");
	$('#scoreLookupMaster').css("background-color", "");
	$('#instruction1').find(".jqte_editor").css("background-color", "");
	$('#instruction2').find(".jqte_editor").css("background-color", "");
	var cnt=0;
	var focs=0;

	if (trim(document.getElementById("assessmentName").value)==""){
		$('#errordiv').append("&#149; "+resourceJSON.msgInventoryName+"<br>");
		if(focs==0)
			$('#assessmentName').focus();
		$('#assessmentName').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}/*if (trim(document.getElementById("assessmentDescription").value)==""){
		$('#errordiv').append("&#149; Please enter Inventory Description<br>");
		if(focs==0)
			$('#assessmentDescription').focus();
		$('#assessmentDescription').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}*/
	//if ($('#assDescription').find(".jqte_editor").html()==""){
	//alert($('#assDescription').find(".jqte_editor").text());
	if ($('#assDescription').find(".jqte_editor").text().trim()==""){
		$('#errordiv').append("&#149; "+resourceJSON.msgInventoryDescription+"<br>");
		if(focs==0)
			//$('#assessmentDescription').focus();
			$('#assDescription').find(".jqte_editor").focus();
		//$('#assessmentDescription').css("background-color", "#F5E7E1");
		$('#assDescription').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}/*else
	{
		var charCount=trim($('#assDescription').find(".jqte_editor").text());
		var count = charCount.length;
		//alert(count);
		if(count>750)
		{
			$('#errordiv').append("&#149; Inventory Description length cannot exceed 750 characters<br>");
			if(focs==0)
				$('#assDescription').find(".jqte_editor").focus();
			$('#assDescription').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}*/
	if ($('#instruction1').is(':visible') && $('#instruction1').find(".jqte_editor").text().trim()==""){
		$('#errordiv').append("&#149; "+resourceJSON.msgSplashScreenInstruction1+"<br>");
		if(focs==0)
			$('#instruction1').find(".jqte_editor").focus();
		$('#instruction1').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if ($('#instruction2').is(':visible') && $('#instruction2').find(".jqte_editor").text().trim()==""){
		$('#errordiv').append("&#149; "+resourceJSON.msgSplashScreenInstruction2+"<br>");
		if(focs==0)
			$('#instruction2').find(".jqte_editor").focus();
		$('#instruction2').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if($("select[name='assessmentType']").val() == "2")
	{
		if ($('#districtMaster').val()==0){
			$('#errordiv').append("&#149; "+resourceJSON.msgSelectDistrict+"<br>");
			if(focs==0)
				$('#districtMaster').focus();
			$('#districtMaster').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		if ($('#districtMaster').val()!=0 && $('#jobOrderType').val()==2 && ($('#schoolMaster').val()=='Select School'?0:$('#schoolMaster').val())==0){
			$('#errordiv').append("&#149; "+resourceJSON.msgPleaseselectSchool+"<br>");
			if(focs==0)
				$('#schoolMaster').focus();
			$('#schoolMaster').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}
	if($("select[name='assessmentType']").val() == "1" || $("select[name='assessmentType']").val() == "3" || $("select[name='assessmentType']").val() == "4") {
	    var rec_group = $("input[name='rec_group']:checked").val();
	    if($('input[name=PDPassociatedopt]:checked').val() == "1" && $("select[name='PDPselect']").val() == "0" ){
	    	$('#errordiv').append("&#149; Please Select PDP<br>");
	    	cnt++;
	    }
	    if(rec_group == '' || rec_group == undefined) {
		   $('#errordiv').append("&#149; "+resourceJSON.msgNewGroupName+"<br>");
		   $("input[name='rec_group']").eq(0).focus();
		   cnt++;focs++;
	    }
	    else {
	    	if(rec_group == "1"){
	    		if($('#assessmentGroupName').val() == '') {
			    	$('#errordiv').append("&#149; "+resourceJSON.msgGroupName+"<br>");
			    	if(focs==0)
			    		$('#assessmentGroupName').focus();
			    	$('#assessmentGroupName').css("background-color", "#F5E7E1");
					cnt++;focs++;
			    }
	    		if($("select[name='assessmentType']").val() == "1") {
	    			if($('#isDefaultEPI').is(":checked")){
	    				if($('#defaultEPIGroupId').val()!="" && $('#defaultEPIGroupId').val()!=null){
	    					$('#errordiv').append('&#149; We already have a default EPI Group '+'"'+$('#defaultEPIGroupId').attr("name")+'".'+' Please make sure that we have only one default EPI Group.<br>');
	    			    	if(focs==0)
	    			    		$('#assessmentGroupName').focus();
	    			    	$('#assessmentGroupName').css("background-color", "#F5E7E1");
	    					cnt++;focs++;
	    				}
	    			}
			    }
	    	}
	    	else if(rec_group == "2") {
	    		if($('#assessmentGroupDetails').val() == 'Select Group') {
			    	$('#errordiv').append("&#149; "+resourceJSON.msgSelectGroup+"<br>");
			    	if(focs==0)
			    		$('#assessmentGroupDetails').focus();
			    	$('#assessmentGroupDetails').css("background-color", "#F5E7E1");
					cnt++;focs++;
			    }
	    		if($("select[name='assessmentType']").val() == "1" && $('#assessmentGroupDetails').val() != 'Select Group' && $("#assessmentGroupDetails :selected").text() != $('#defaultEPIGroupId').attr("name")) {
	    			if($('#isDefaultEPI').is(":checked")){
	    				if($('#defaultEPIGroupId').val()!="" && $('#defaultEPIGroupId').val()!=null){
	    					$('#errordiv').append('&#149; We already have a default EPI Group '+'"'+$('#defaultEPIGroupId').attr("name")+'".'+' Please make sure that we have only one default EPI Group.<br>');
	    			    	if(focs==0)
	    			    		$('#assessmentGroupDetails').focus();
	    			    	$('#assessmentGroupDetails').css("background-color", "#F5E7E1");
	    					cnt++;focs++;
	    				}
	    			}
			    }
	    	}
	    }
	}
	if($("select[name='assessmentType']").val() == "1" || $("select[name='assessmentType']").val() == "3" || $("select[name='assessmentType']").val() == "4") {
		if (trim(document.getElementById("noOfExperimentQuestions").value)==""){
			$('#errordiv').append("&#149; Please enter No Of Experiment Questions<br>");
			if(focs==0)
				$('#noOfExperimentQuestions').focus();
			$('#noOfExperimentQuestions').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		var score = $("input[name='score']:checked").val();
		if(score == '' || score == undefined){
			$('#errordiv').append("&#149; Please select Want To Score<br>");
			if(focs==0)
				$("input[name='score']").eq(0).focus();
			cnt++;focs++;
		}
		else{
			if(score=="1"){
				if(document.getElementById("scoreLookupMaster").length>1){
					if(document.getElementById("scoreLookupMaster").value=="Select Method"){
						$('#errordiv').append("&#149; Please select Scoring Method<br>");
						if(focs==0)
							$('#scoreLookupMaster').focus();
						$('#scoreLookupMaster').css("background-color", "#F5E7E1");
						cnt++;focs++;
					}
				}
			}
		}
	}
	if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		return false;
	}
}

function saveAssessment()
{
	if(!validateAssessment())
		return;

	var assessment = { assessmentId:null,assessmentName:null,assessmentDescription:null,assessmentSessionTime:null,status:null,questionSessionTime:null,questionRandomization:false,optionRandomization:false,noOfExperimentQuestions:null,splashInstruction1:null,splashInstruction2:null,pdpreporttype:null };

	var districtMaster = {districtId:dwr.util.getValue("districtMaster")};
	var schoolMaster = {schoolId:(dwr.util.getValue("schoolMaster")=='Select School'?0:dwr.util.getValue("schoolMaster"))};
	//var jobCategoryMaster = {jobCategoryId:(dwr.util.getValue("jobCategoryMaster")=='Select Job Category'?0:dwr.util.getValue("jobCategoryMaster"))};
	//alert("schoolMaster.schoolId: "+schoolMaster.schoolId);
	dwr.util.getValues(assessment);
	dwr.engine.beginBatch();
	assessment.assessmentType=1;
	assessment.assessmentType = $("select[name='assessmentType']").val();
	
	/*
	var Atype=document.getElementsByName("assessmentType");
	for(i=0;i<Atype.length;i++)
	{
		if(Atype[i].checked==true)
		{
			assessment.assessmentType=Atype[i].value;
			break;
		}
	}*/

	if($("select[name='assessmentType']").val() == "1" || $("select[name='assessmentType']").val() == "3" || $("select[name='assessmentType']").val() == "4") {
		var score = $("input[name='score']:checked").val();
		
		if(score == "1") {
			var scoreLookupMaster = {lookupId:dwr.util.getValue("scoreLookupMaster")};
			assessment.scoreLookupMaster = scoreLookupMaster;
		} else if(score == "2") {
			var scoreLookupMaster = {lookupId : 0,name : $("input[name='scoreLookupMaster']").val()};
		    assessment.scoreLookupMaster = scoreLookupMaster;
		}
	}
	if($("select[name='assessmentType']").val() == "2")	{
		if(districtMaster.districtId>0)
			assessment.districtMaster=districtMaster;
		if(schoolMaster.schoolId>0)
			assessment.schoolMaster=schoolMaster;

		assessment.jobOrderType=dwr.util.getValue("jobOrderType");
		assessment.isDefault=dwr.util.getValue("isDefault");
		assessment.isResearchEPI=false;
		var jobCategoryMaster = {jobCategoryId:(dwr.util.getValue("jobCategoryMaster")=='Select Job Category'?0:dwr.util.getValue("jobCategoryMaster"))};
		if(jobCategoryMaster.jobCategoryId!=0)
		assessment.jobCategoryMaster=jobCategoryMaster;
	}
	else if($("select[name='assessmentType']").val() == "1" || $("select[name='assessmentType']").val() == "3" || $("select[name='assessmentType']").val() == "4") {
		var rec_group = $("input[name='rec_group']:checked").val();
		if(rec_group == "1") {
		    var assessmentGroupDetails = {
		    		assessmentGroupId : 0,
		    		assessmentGroupName : $("input[name='assessmentGroupName']").val()
		    };
		    assessment.assessmentGroupDetails = assessmentGroupDetails;
		} else if(rec_group == "2") {
			var assessmentGroupDetails = {assessmentGroupId:dwr.util.getValue("assessmentGroupDetails")};
			assessment.assessmentGroupDetails = assessmentGroupDetails;		
		}		
	}
	if($("select[name='assessmentType']").val() == "1")
		assessment.isResearchEPI=dwr.util.getValue("isResearchEPI");
	
	var selectedValue="";
	var tnl = document.getElementById("jobOrders");  

	for(i=0;i<tnl.length;i++){
		if(tnl[i].selected == true || tnl[i].disabled == true){
			selectedValue+=tnl[i].value+",";
		}  
	}  

	var tn2 = document.getElementsByName("o_jobs");  

	for(i=0;i<tn2.length;i++){
		selectedValue+=tn2[i].value+",";
	}  

	var defaultEPIGroupId = null;
	if($("select[name='assessmentType']").val() == "1" && $('#isDefaultEPI').is(':visible')){
		if($('#defaultEPIGroupId').val()!=null && $('#defaultEPIGroupId').val()!="" && $('#modalDefaultEPIGroupId').val()!=null && $('#modalDefaultEPIGroupId').val()!="" && $('#defaultEPIGroupId').val()!=$('#modalDefaultEPIGroupId').val()){
			defaultEPIGroupId = $('#modalDefaultEPIGroupId').val();
		}
	}
	// ADD PDP Configuration
	if($('input[name=PDPassociatedopt]:checked').val() == "2")
	{
		assessment.pdpreporttype=0;
	}else
	{
		assessment.pdpreporttype=dwr.util.getValue("PDPselect");
	}
	AssessmentAjax.saveAssessment(assessment,selectedValue,defaultEPIGroupId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data==4)
			{
				document.getElementById('errordiv').innerHTML="&#149; "+resourceJSON.msgUniqueGroupName;
				$('#errordiv').show();
				$('#assessmentGroupName').css("background-color", "#F5E7E1");
				$('#assessmentGroupName').focus();
			}
			else if(data==3)
			{
				document.getElementById('errordiv').innerHTML="&#149; "+resourceJSON.msgUniqueInventoryName;
				$('#errordiv').show();
				$('#assessmentName').css("background-color", "#F5E7E1");
				$('#assessmentName').focus();
			}
			else if(data==1 || data==2)
			{
				getAssessmentGrid();
				clearAssessment();
			}
			else
			{
				//alert(data);
				if($('#jobCategoryMaster').val()!=0)
					document.getElementById('errordiv').innerHTML='&#149; '+resourceJSON.msgDefaultInventory+' "'+data+'" '+resourceJSON.msgfor+' "'+$('#districtMaster').find("option:selected").text()+'" in "'+$('#jobCategoryMaster').find("option:selected").text()+'". '+resourceJSON.msgDefaultInventoryJobCategory;
				else if($('#jobOrderType').val()==1)
					document.getElementById('errordiv').innerHTML='&#149; '+resourceJSON.msgDefaultInventory+' "'+data+'" '+resourceJSON.msgfor+' "'+$('#districtMaster').find("option:selected").text()+'". '+resourceJSON.msgDefaultInventoryDistrict;
				else if($('#jobOrderType').val()==2)
					document.getElementById('errordiv').innerHTML='&#149; '+resourceJSON.msgDefaultInventory+' "'+data+'" '+resourceJSON.msgfor+' "'+$('#schoolMaster').find("option:selected").text()+'". '+resourceJSON.msgDefaultInventorySchool;
				
				$('#errordiv').show();
				$('#isDefault').css("background-color", "#F5E7E1");
				$('#isDefault').focus();
			}
		}
	});
	dwr.engine.endBatch();

}
function deleteAssessment(assessmentId)
{
	if (confirm(resourceJSON.msgDeletethisInventory)) {
		var assessmentDetail = {assessmentId:assessmentId};
		//alert(assessmentDetail.assessmentId);
		AssessmentAjax.deleteAssessment(assessmentDetail,"",{ 
			async: true,
			errorHandler:handleError, 
			callback: function(data)
			{
			getAssessmentGrid();
			clearAssessment();
			}
		});

	}
}
function deleteJob(assessmentJobRelationId,assessmentId)
{
	if (confirm(resourceJSON.msgRemovethisJobInventory)) {
		var assessmentJobRelation = {assessmentJobRelationId:assessmentJobRelationId};

		AssessmentAjax.deleteJob(assessmentJobRelation,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			//alert(data);
			getSelectedJobs(assessmentId);
			getJobOrders(0);
		}
		});	

	}
}
function getSelectedJobs(assessmentId)
{

	var assessmentDetail = {assessmentId:assessmentId};

	AssessmentAjax.getSelectedJobs(assessmentDetail,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		//alert(data);
		$('#jobs').html(data);
		$('#jobs').show();
	}
	});	


}
function activateDeactivateAssessment(assessmentId,status)
{
	AssessmentAjax.activateDeactivateAssessment(assessmentId,status, { 
		async: true,
		errorHandler:handleError,
		callback:  function(data)
		{
		// no active base checking 
		/*if(data!=null)
		{
			alert("We already have active base inventory ("+data.assessmentName+"). Please deactivate it first. ");
			return;
		}*/
		getAssessmentGrid();
		clearAssessment();
		}
	});
}

function getJobs(flag)
{
	if(flag==1)
	{
		$('#jobOrders').hide();
		$('#infoData').hide();
		$('#jsjo1').hide();
		$('#jsjo2').hide();
		$('#isDefaultEPIGroupDiv1').show();
		$('#isDefaultEPIGroupDiv2').show();
		document.getElementById("defaultEPIGroupId").value = '';
		$("#defaultEPIGroupId").attr("name", "");
		getGroupsByAssessmentType(1,1);
		$('#jot1').hide();
		$('#jot2').hide();
		$('#def1').hide();
		$('#def2').hide();
		$('#def3').show();
		$('#def4').show();
//		$('#new_gp').hide();
//		$('#sel_gp').hide();
//		$('#gp1').hide();
//		$('#gp2').hide();
		$('#new_gp').show();
		$('#sel_gp').show();
		$('#gp1').show();
		$('#gp2').show();
		$('#rec1').prop('checked', true);
		$('#assessmentGroupDetails').prop('disabled', true);
		$('#assessmentGroupName').prop('disabled', false);
		$('#assessmentGroupName').focus();
		$('#score1').prop('checked', false);
		$('#score2').prop('checked', false);
		$('#noOfExperimentQuestions').val(0);
		document.getElementById("scoreDiv").style.display="block";
		document.getElementById("selectScoreDiv").style.display="none";
		$('#instruction1').show();
		$('#instruction2').show();
		$("#PDPassociatedoptyes").attr('checked', true);// ADD PDP Configuration...
		$("#PDPselect").prop('selectedIndex', 1);
		$("#PDPselect").prop('disabled',false);
		$("#PDPassociatedoptno").prop('disabled',false);
		$("#PDPassociatedoptyes").prop('disabled',false);
		
	} else if(flag==3)
	{
		$('#jobOrders').hide();
		$('#infoData').hide();
		$('#jsjo1').hide();
		$('#jsjo2').hide();
		$('#jot1').hide();
		$('#jot2').hide();
		$('#schoolLbl').hide();
		$('#schoolMaster').hide();
		$('#jobCategoryLbl').hide();
		$('#jobCategoryMaster').hide();
		$('#isDefaultEPIGroupDiv1').hide();
		$('#isDefaultEPIGroupDiv2').hide();
		document.getElementById("defaultEPIGroupId").value = '';
		$("#defaultEPIGroupId").attr("name", "");
		$('#def1').hide();
		$('#def2').hide();
		$('#def3').hide();
		$('#def4').hide();
		$('#new_gp').show();
		$('#sel_gp').show();
		$('#gp1').show();
		$('#gp2').show();
		$('#rec1').prop('checked', true);
		$('#assessmentGroupDetails').prop('disabled', true);
		$('#assessmentGroupName').prop('disabled', false);
		$('#assessmentGroupName').focus();
		$('#noOfExperimentQuestions').val(0);
		$('#score1').prop('checked', false);
		$('#score2').prop('checked', false);
		document.getElementById("scoreDiv").style.display="block";
		document.getElementById("selectScoreDiv").style.display="none";
		$('#instruction1').show();
		$('#instruction2').show();
		$("#PDPassociatedoptyes").attr('checked', true);// ADD PDP Configuration...
		$("#PDPselect").prop('selectedIndex', 1);
		$("#PDPselect").prop('disabled',false);
		$("#PDPassociatedoptno").prop('disabled',false);
		$("#PDPassociatedoptyes").prop('disabled',false);
	} else if(flag==4)
	{
		$('#jobOrders').hide();
		$('#infoData').hide();
		$('#jsjo1').hide();
		$('#jsjo2').hide();
		$('#schoolLbl').hide();
		$('#schoolMaster').hide();
		$('#jobCategoryLbl').hide();
		$('#jobCategoryMaster').hide();
		$('#isDefaultEPIGroupDiv1').hide();
		$('#isDefaultEPIGroupDiv2').hide();
		document.getElementById("defaultEPIGroupId").value = '';
		$("#defaultEPIGroupId").attr("name", "");
		$('#jot1').hide();
		$('#jot2').hide();
		$('#def1').hide();
		$('#def2').hide();
		$('#def3').hide();
		$('#def4').hide();
		$('#new_gp').show();
		$('#sel_gp').show();
		$('#gp1').show();
		$('#gp2').show();
		$('#rec1').prop('checked', true);
		$('#assessmentGroupDetails').prop('disabled', true);
		$('#assessmentGroupName').prop('disabled', false);
		$('#assessmentGroupName').focus();
		$('#noOfExperimentQuestions').val(0);
		$('#score1').prop('checked', false);
		$('#score2').prop('checked', false);
		document.getElementById("scoreDiv").style.display="block";
		document.getElementById("selectScoreDiv").style.display="none";
		$('#instruction1').show();
		$('#instruction2').show();
		$("#PDPassociatedoptyes").attr('checked', true);// ADD PDP Configuration...
		$("#PDPselect").prop('selectedIndex', 2);
		$("#PDPselect").prop('disabled',false);
		$("#PDPassociatedoptno").prop('disabled',false);
		$("#PDPassociatedoptyes").prop('disabled',false);
	} 
	else
	{
		$('#jobOrders').show();
		$('#infoData').show();
		$('#jsjo1').show();
		$('#jsjo2').show();
		$('#isDefaultEPIGroupDiv1').hide();
		$('#isDefaultEPIGroupDiv2').hide();
		document.getElementById("defaultEPIGroupId").value = '';
		$("#defaultEPIGroupId").attr("name", "");
		$('#jot1').show();
		$('#jot2').show();
		$('#def1').show();
		$('#def2').show();
		$('#def3').hide();
		$('#def4').hide();
		$('#new_gp').hide();
		$('#sel_gp').hide();
		$('#gp1').hide();
		$('#gp2').hide();
		document.getElementById("scoreDiv").style.display="none";
		$('#instruction1').hide();
		$('#instruction2').hide();
		$("#PDPassociatedoptno").attr('checked', true);// ADD PDP Configuration...
		$("#PDPselect").prop('selectedIndex', 0);
		$("#PDPselect").prop('disabled',true);
		$("#PDPassociatedoptno").prop('disabled',true);
		$("#PDPassociatedoptyes").prop('disabled',true);
	}
}

//amit
function radioChange()
{
	if ($('#rec1').prop("checked"))
	{
		$('#assessmentGroupDetails').prop('disabled', true);
		$('#assessmentGroupName').prop('disabled', false);
		$('#assessmentGroupName').focus();
	}
	if($('#rec2').prop("checked"))
	{
		getGroupsByAssessmentType($("select[name='assessmentType']").val(), 1);
		$('#assessmentGroupName').prop('disabled', true);
		$('#assessmentGroupDetails').prop('disabled', false);
		$('#assessmentGroupDetails').focus();
	}
}

function showAndHideScoringMethods(flag)
{
	if(flag=="1"){
		getScoringMethods($("select[name='assessmentType']").val());
		document.getElementById("selectScoreDiv").style.display="block";
	}
	else if(flag=="2"){
		dwr.util.removeAllOptions("scoreLookupMaster");
		document.getElementById("selectScoreDiv").style.display="none";
	}
}

function getScoringMethods(assessmentType)
{
	dwr.util.removeAllOptions("scoreLookupMaster");
	AssessmentAjax.getScoringMethods(assessmentType,{
		async: false,
		callback: function(data){
//			console.log(data);
			if(data.length==1){
				dwr.util.addOptions("scoreLookupMaster",data,"lookupId","name");
				$('#scoreLookupMaster').prop('disabled', true);
			}
			else if(data.length>1){
				dwr.util.addOptions("scoreLookupMaster",["Select Method"]);
				dwr.util.addOptions("scoreLookupMaster",data,"lookupId","name");
				$('#scoreLookupMaster').prop('disabled', false);
			}
	    },errorHandler:handleError
	});
}
/////////////////////////////////////Assessment Section ////////////////////////////////////////////////


function getAssessmentSectionGrid()
{
	//alert(dwr.util.getValue("assessmentId"));
	AssessmentAjax.getAssessmentSections(dwr.util.getValue("assessmentId"),noOfRows,page,sortOrderStr,sortOrderType,{  
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
		$('#divMain').html(data);
		applyScrollOnTbl();		
		}
	});

}

function addAssessmentSection()
{
	document.getElementById("divAssessmentRow").style.display="block";
	$('#sectionName').focus();
	$('#errordiv').hide();
	$('#sectionName').css("background-color", "");
	document.getElementById("divDone").style.display="block";
	document.getElementById("divManage").style.display="none";
	dwr.util.setValues({ sectionId:null,sectionName:null,sectionDescription:null, sectionInstructions:null});
	//clearData();
	$('#secDescription').find(".jqte_editor").html("");
	$('#secInstruction').find(".jqte_editor").html("");
	$('#secDescription').find(".jqte_editor").css("background-color", "");
	$('#secInstruction').find(".jqte_editor").css("background-color", "");
	return false;
}
function validateAssessmentSection()
{
	$('#errordiv').empty();
	$('#sectionName').css("background-color", "");
	$('#secDescription').find(".jqte_editor").css("background-color", "");
	$('#secInstruction').find(".jqte_editor").css("background-color", "");
	var cnt=0;
	var focs=0;

	if (trim(document.getElementById("sectionName").value)==""){

		$('#errordiv').append('&#149; '+resourceJSON.msgSectionName+'<br>');
		if(focs==0)
			$('#sectionName').focus();
		$('#sectionName').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	var charCount=trim($('#secDescription').find(".jqte_editor").text());
	var count = charCount.length;
	if(count>750)
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgSectionDescriptionlength750char+"<br>");
		if(focs==0)
			$('#secDescription').find(".jqte_editor").focus();
		$('#secDescription').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}

	charCount=trim($('#secInstruction').find(".jqte_editor").text());
	count = charCount.length;
	if(count>2500)
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgSectionInstructionslength2500char+"<br>");
		if(focs==0)
			$('#secInstruction').find(".jqte_editor").focus();
		$('#secInstruction').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}


	if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		return false;
	}

}
function saveAssessmentSection()
{
	if(!validateAssessmentSection())
		return;

	var assessmentSection = {sectionId:null,sectionName:null,sectionDescription:null,sectionInstructions:null,sectionVideoUrl:null};

	dwr.util.getValues(assessmentSection);
	dwr.engine.beginBatch();

	AssessmentAjax.saveAssessmentSection(assessmentSection,dwr.util.getValue("assessmentId"), { 
		async: true,
		errorHandler:handleError,  
		callback:function(data)
		{
			if(data==3)
			{
				document.getElementById('errordiv').innerHTML='&#149; '+resourceJSON.msgUniqueSectionName;
				$('#errordiv').show();
				$('#sectionName').css("background-color", "#F5E7E1");
				$('#sectionName').focus();
			}else
			{
				getAssessmentSectionGrid();
				clearAssessmentSection();
			}
		}
	});

	dwr.engine.endBatch();

}
function editAssessmentSection(sectionId)
{
	dwr.util.setValues({ sectionId:null,sectionName:null,sectionDescription:null, sectionInstructions:null,sectionVideoUrl:null});
	$('#errordiv').hide();
	$('#sectionName').css("background-color", "");
	$('#secDescription').find(".jqte_editor").css("background-color", "");
	$('#secInstruction').find(".jqte_editor").css("background-color", "");

	document.getElementById("divDone").style.display="none";
	document.getElementById("divAssessmentRow").style.display="block";

	$('#sectionName').focus();

	AssessmentAjax.getAssessmentSectionById(sectionId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			dwr.util.setValues(data);
			//alert(data.sectionDescription);
			$('#secDescription').find(".jqte_editor").html(data.sectionDescription);
			$('#secInstruction').find(".jqte_editor").html(data.sectionInstructions);
		}
	});

	document.getElementById("divManage").style.display="block";

	return false;
}
function clearAssessmentSection()
{
	dwr.util.setValues({ sectionId:null,sectionName:null,sectionDescription:null, sectionInstructions:null,sectionVideoUrl:null});

	document.getElementById("divAssessmentRow").style.display="none";
	document.getElementById("divDone").style.display="none";
	document.getElementById("divManage").style.display="none";

	$('#sectionName').css("background-color", "");
	$('#secDescription').find(".jqte_editor").html("");
	$('#secInstruction').find(".jqte_editor").html("");
}
function deleteAssessmentSection(sectionId)
{
	if (confirm(resourceJSON.msgDeleteSection)) {

		var assessmentSection = {sectionId:sectionId};
		AssessmentAjax.deleteAssessmentSection(assessmentSection, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			getAssessmentSectionGrid();
			clearAssessmentSection();
			}
		});

	}
}
function displayVideo(url)
{
	//alert("url "+url);
	if((url!=null && url.length>0))
	{
			AssessmentAjax.displayVideo(url, { 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
				//alert("data "+data);
				//getAssessmentSectionQuestionsGrid();
				$('#myModal2').modal('show');
				$('#message2show').html(data);
				//$('#dv').empty();
				//$('#dv').append(data);
				}
			});
	}
	else
	{
		$('#myModal2').modal('show');
		$('#message2show').html(resourceJSON.msgVideoCorrespondingQuestion);
	}
}

function getCompetencies(domainId)
{
	//alert(domainId);
	AssessmentAjax.getCompetencies(domainId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		dwr.util.removeAllOptions("competencyMaster");
		dwr.util.addOptions("competencyMaster",data,"competencyId","competencyName");
		dwr.util.removeAllOptions("objectiveMaster");
		dwr.util.addOptions("objectiveMaster",["Select Objective"]);
	}
	});	
}

function getObjectives(competencyId)
{
	//alert(competencyId);

	AssessmentAjax.getObjectives(competencyId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		dwr.util.removeAllOptions("objectiveMaster");
		dwr.util.addOptions("objectiveMaster",data,"objectiveId","objectiveName");
	}
	});	
}


function getQuestionOptions(questionTypeId)
{
	$('#imageType').hide();
	var qType=findSelected(questionTypeId);
	if(questionTypeId==0 || qType=='sl' || qType=='ml')
	{
		for(i=0;i<=5;i++)
			$('#row'+i).hide();

		$('#questionWeightage').attr("disabled","disabled");
		return;
	}
	else
	{
		for(i=0;i<=5;i++)
			$('#row'+i).show();

		$('#questionWeightage').attr("disabled",false);
	}


	if(qType=='tf')
	{
		/*$('#row2').hide();
		$('#row3').hide();
		$('#row4').hide();
		$('#row5').hide();*/
		
		for(i=2;i<=5;i++)
			$('#row'+i).hide();
		
		for(i=1;i<=4;i++)
			$('#rank'+i).attr("disabled","disabled");

		//clearOptions();

		if($('#opt1').val()=="")
			$('#opt1').attr("value","True");
		if($('#opt2').val()=="")
			$('#opt2').attr("value","False");

	}else if(qType=='slsel')
	{
		/*$('#row2').show();
		$('#row3').show();*/
		
		for(i=2;i<=5;i++)
			$('#row'+i).show();
		
		for(i=1;i<=10;i++)
			$('#rank'+i).attr("disabled","disabled");

		//clearOptions();
	}else if(qType=='lkts')
	{
		/*$('#row2').show();
		$('#row3').show();
		$('#row4').show();
		$('#row5').show();*/
		
		for(i=2;i<=5;i++)
			$('#row'+i).show();
		
		for(i=1;i<=10;i++)
			$('#rank'+i).attr("disabled","disabled");

		//clearOptions();
	}
	else if(qType=='rt')
	{
		for(i=2;i<=5;i++)
			$('#row'+i).show();
		
		/*$('#row2').show();
		$('#row3').show();
		$('#row4').show();
		$('#row5').show();*/
		
		for(i=1;i<=10;i++)
			$('#rank'+i).attr("disabled",false);

		//clearOptions();
	}else if(qType=='it')
	{
		for(i=0;i<=5;i++)
			$('#row'+i).hide();

		$('#imageType').show();

		return;
	}
	else if(qType=='mlsel') {
		for(i=1;i<=10;i++) {

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	} else if(qType=='mloet') {
		for(i=1;i<=10;i++) {

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	} else if(qType=='sloet') {
		for(i=1;i<=10;i++) {

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	}

}
function clearOptions()
{
	for(i=1;i<=6;i++)
	{
		$('#opt'+i).attr("value","");
		$('#score'+i).attr("value","");
		$('#rank'+i).attr("value","");
	}
}
function redirectTo(redirectURL)
{
	var url = redirectURL+"&dt="+new Date().getTime();
	window.location.href=url;
}

function validateAssessmentSectionQuestions()
{
	$('#errordiv').empty();
	$('#errordiv1').empty();
	$('#domainMaster').css("background-color", "");
	$('#competencyMaster').css("background-color", "");
	$('#objectiveMaster').css("background-color", "");
	$('#stageOneStatus').css("background-color", "");
	$('#stageTwoStatus').css("background-color", "");
	$('#stageThreeStatus').css("background-color", "");
	//$('#question').css("background-color", "");
	$('#assQuestion').find(".jqte_editor").css("background-color", "");
	$('#assInstruction').find(".jqte_editor").css("background-color", "");
	$('#questionTypeMaster').css("background-color", "");
	$('#opt1').css("background-color", "");

	for(i=1;i<=10;i++)
	{
		$('#rank'+i).css("background-color", "");
		$('#score'+i).css("background-color", "");
	}

	var cnt=0;
	var focs=0;

	if (document.getElementById("domainMaster") && trim(document.getElementById("domainMaster").value)==0){

		$('#errordiv').append("&#149; "+resourceJSON.msgDomainName+"<br>");
		if(focs==0)
			$('#domainMaster').focus();
		$('#domainMaster').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if (document.getElementById("competencyMaster") && trim(document.getElementById("competencyMaster").value)==0){

		$('#errordiv').append("&#149; "+resourceJSON.msgCompetency+"<br>");
		if(focs==0)
			$('#competencyMaster').focus();
		$('#competencyMaster').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	//alert(document.getElementById("objectiveMaster").value);
	if(document.getElementById("objectiveMaster"))
		if (trim(document.getElementById("objectiveMaster").value)==resourceJSON.msgSelectObjective || trim(document.getElementById("objectiveMaster").value)==0){

			$('#errordiv').append("&#149; "+resourceJSON.msgObjective+"<br>");
			if(focs==0)
				$('#objectiveMaster').focus();
			$('#objectiveMaster').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	/* validation For Stage1,2,3*/
	if(document.getElementById("stageOneStatus"))
		if (trim(document.getElementById("stageOneStatus").value)==resourceJSON.msgSelectStageOne || trim(document.getElementById("stageOneStatus").value)==0){

			$('#errordiv').append("&#149; "+resourceJSON.msgStageOne+"<br>");
			if(focs==0)
				$('#stageOneStatus').focus();
			$('#stageOneStatus').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	
	if(document.getElementById("stageTwoStatus"))
		if (trim(document.getElementById("stageTwoStatus").value)==resourceJSON.msgSelectStageOne || trim(document.getElementById("stageTwoStatus").value)==0){

			$('#errordiv').append("&#149; "+resourceJSON.msgStageTwo+"<br>");
			if(focs==0)
				$('#stageTwoStatus').focus();
			$('#stageTwoStatus').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	
	if(document.getElementById("stageThreeStatus"))
		if (trim(document.getElementById("stageThreeStatus").value)==resourceJSON.msgSelectStageOne || trim(document.getElementById("stageThreeStatus").value)==0){

			$('#errordiv').append("&#149; "+resourceJSON.msgStageThree+"<br>");
			if(focs==0)
				$('#stageThreeStatus').focus();
			$('#stageThreeStatus').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	/* End Stage Validation */
	
	if(document.getElementById("isExperimental")){
		if($('#isExperimental').is(":checked") && $('#questionWeightage').val()!=null && $('#questionWeightage').val()!="" && $('#questionWeightage').val()!=0){
			$('#errordiv').append("&#149; "+resourceJSON.msgIsExperimental+"<br>");
			if(focs==0)
				$('#questionWeightage').focus();
			$('#questionWeightage').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}
	
	if($('#assQuestion').find(".jqte_editor").text().trim()==""){

		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrQuest+"<br>");
		if(focs==0)
			$('#assQuestion').find(".jqte_editor").focus();
		$('#assQuestion').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}else
	{
		var charCount=trim($('#assQuestion').find(".jqte_editor").text());
		var count = charCount.length;
		//alert(count);
		if(count>5000)
		{
			$('#errordiv').append("&#149; "+resourceJSON.MsgQuestionLength+"<br>");
			if(focs==0)
				$('#assQuestion').find(".jqte_editor").focus();
			$('#assQuestion').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}
	if (trim(document.getElementById("questionTypeMaster").value)==0){

		$('#errordiv').append("&#149; "+resourceJSON.PlzSelectQuestionType+"<br>");
		if(focs==0)
			$('#questionTypeMaster').focus();
		$('#questionTypeMaster').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}

	var qType=findSelected(dwr.util.getValue("questionTypeMaster"));
	var c=0;

	if(!(qType=='sl' || qType=='ml' || qType=='it'))
		if(document.getElementById("opt1"))
		{
			for(i=1;i<=10;i++)
			{
				if(trim(document.getElementById("opt"+i).value)=="")
					c++;
			}
			if(c==9 || c==10)
			{
				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrAtLeastTwoOptions+"<br>");
				if(focs==0)
				{
					if(c==10)
						$('#opt1').focus();
					else
						$('#opt2').focus();
				}

				$('#opt1').css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}

	if(qType=='it' && document.getElementById("opt100"))
	{
		c=0;
		for(i=100;i<=105;i++)
		{
			if(trim(document.getElementById("opt"+i).value)=="")
				c++;
		}
		if(c==9 || c==10)
		{
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrAtLeastTwoOptions+"<br>");
			$('#errordiv').show();
			return;
		}
	}

	var arr =[];
/////////////////// for rank type question validation
	if(qType=='rt')
	{

		for(i=1;i<=10;i++)
		{
			if($('#rank'+i).val()!="")
				arr.push($('#rank'+i).val());
		}


		if(arr.length==0)
		{
			// rank validation
			/*$('#errordiv').append("&#149; Please enter Rank of corresponding option<br>");
			$('#rank1').css("background-color", "#F5E7E1");
			if(focs==0)
			{
				$('#rank1').focus();
			}
			cnt++;focs++;*/
		}else
		{

			var uniqueNames = [];
			var dup = [];
			var focArr = [];
			$.each(arr, function(j, el){

				if($.inArray(el, uniqueNames) === -1)
				{
					uniqueNames.push(el);
				}else
				{
					dup.push(el);
					focArr.push(j+1);
				}
			});

			if(dup.length>0)
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgUniqueRank+"<br>");
				$('#rank'+focArr[0]).css("background-color", "#F5E7E1");
				if(focs==0)
				{
					$('#rank'+focArr[0]).focus();
				}
				cnt++;focs++;
			}
		}


		// rank validation
		var arr2 = [1,2,3,4,5,6];
		for(i=1;i<=10;i++)
		{
			if($('#opt'+i).val()!="" && $('#rank'+i).val()=="")
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgRankCorrespondingOption);
				$('#rank'+i).css("background-color", "#F5E7E1");
				if(focs==0)
				{
					$('#rank'+i).focus();
				}
				cnt++;focs++;
				break;
			}
		}

		if($('#questionWeightage').val()!=0)
		{
			for(i=1;i<=10;i++)
			{
				if($('#opt'+i).val()!="" && $('#score'+i).val()=="")
				{
					$('#errordiv').append("&#149; "+resourceJSON.msgScoreCorrespondingOption);
					$('#score'+i).css("background-color", "#F5E7E1");
					if(focs==0)
					{
						$('#score'+i).focus();
					}
					cnt++;focs++;
					break;
				}
			}
		}
	}

///////////////////////	

	var charCount=trim($('#assInstruction').find(".jqte_editor").text());
	var count = charCount.length;
	//alert(count);
	if(count>2500)
	{
		$('#errordiv').append("&#149; "+resourceJSON.MsgInstructionsCannotExceed+"<br>");
		if(focs==0)
			$('#assInstruction').find(".jqte_editor").focus();
		$('#assInstruction').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}

	if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		return false;
	}

}
function saveQuestion(assessmentId,sectionId,status) {

	if(!validateAssessmentSectionQuestions())
		return;

	var questionType={questionTypeId:dwr.util.getValue("questionTypeMaster")};

	if(document.getElementById("domainMaster"))	{
		var domain		=	{domainId:dwr.util.getValue("domainMaster")};
		var competency	=	{competencyId:dwr.util.getValue("competencyMaster")};
		var objective	=	{objectiveId:dwr.util.getValue("objectiveMaster")};
		var stageOne	=	{stage1Id:dwr.util.getValue("stageOneStatus")};
		var stageTwo	=	{stage2Id:dwr.util.getValue("stageTwoStatus")};
		var stageThree	=	{stage3Id:dwr.util.getValue("stageThreeStatus")};
		//var itemCode	=	{itemCode:dwr.util.getValue("itemCode")};

		var questionsPool = {questionId:dwr.util.getValue("questionId"),domainMaster:domain,competencyMaster:competency,objectiveMaster:objective,stageOneStatus:stageOne,stageTwoStatus:stageTwo,stageThreeStatus:stageThree,question:null,questionTypeMaster:questionType,questionWeightage:null,questionInstruction:null,itemCode:itemCode};
	} else {
		var questionsPool = {questionId:dwr.util.getValue("questionId"),question:null,questionTypeMaster:questionType,questionWeightage:null,questionInstruction:null,itemCode:itemCode};
	}

	var assessmentDetail 	= 	{assessmentId:assessmentId};
	var assessmentSections 	= 	{sectionId:sectionId};
	var assessmentQuestion	=	{assessmentQuestionId:dwr.util.getValue("assessmentQuestionId")};
	var qType				=	findSelected(dwr.util.getValue("questionTypeMaster"));
	var weightage 			= 	$('#questionWeightage').val()==""?0:$('#questionWeightage').val();
	var jjon				=	"{id:1}";
	var questionoptions		=	{};
	var arr 				=	[];
	var max 				= 	0;
	var itemCode 			= 	$('#itemCode').val();
	if(qType=='tf')	{
		var maxArr =[];

		for(i=1;i<=2;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : 0
				});

				if($('#score'+i).val()!="") {
					maxArr.push($('#score'+i).val());
				}
			}
		}

		if(maxArr.length>0)
			max =  weightage*Math.max.apply(Math, maxArr);
			document.getElementById("maxMarks").value = max;

	} else if(qType=='slsel') {
		var maxArr =[];

		for(i=1;i<=10;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : 0
				});

				if($('#score'+i).val()!="") {
					maxArr.push($('#score'+i).val());
				}
			}
		}

		if(maxArr.length>0)
			max =  weightage*Math.max.apply(Math, maxArr);
			document.getElementById("maxMarks").value = max;

	} else if(qType=='lkts') {
		var maxArr =[];

		for(i=1;i<=10;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : 0
				});

				if($('#score'+i).val()!="") {
					maxArr.push($('#score'+i).val());
				}
			}
		}
		if(maxArr.length>0)
			max =  weightage*Math.max.apply(Math, maxArr);
			document.getElementById("maxMarks").value = max;
	} else if(qType=='it') {
		var maxArr =[];
		for(i=100;i<=105;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : 0
				});

				if($('#score'+i).val()!="")
				{
					maxArr.push($('#score'+i).val());
				}
			}
		}

		if(maxArr.length>0)
			max =  weightage*Math.max.apply(Math, maxArr);
			document.getElementById("maxMarks").value = max;
	} else if(qType=='rt') {
		var v;
		var total=0;
		for(i=1;i<=10;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : $('#rank'+i).val()==""?"0":$('#rank'+i).val()
				});

				if($('#score'+i).val()!="") {
					v = parseFloat($('#score'+i).val());
					if (!isNaN(v)) total += v;
				}
			}
		}

		max =  weightage*total;
		document.getElementById("maxMarks").value = max;
	}
	else if(qType=='mlsel') { 
		var maxArr =[];

		for(i=1;i<=10;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : 0
				});

				if($('#score'+i).val()!="") {
					maxArr.push($('#score'+i).val());
				}
			}
		}

		if(maxArr.length>0)
			max =  weightage*Math.max.apply(Math, maxArr);
			document.getElementById("maxMarks").value = max;

	}
	else if(qType=='mloet') {
		var maxArr =[];

		for(i=1;i<=10;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : 0
				});

				if($('#score'+i).val()!="") {
					maxArr.push($('#score'+i).val());
				}
			}
		}

		if(maxArr.length>0)
			max =  weightage*Math.max.apply(Math, maxArr);
			document.getElementById("maxMarks").value = max;

	}
	else if(qType=='sloet') {
		var maxArr =[];

		for(i=1;i<=10;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : 0
				});

				if($('#score'+i).val()!="") {
					maxArr.push($('#score'+i).val());
				}
			}
		}

		if(maxArr.length>0)
			max =  weightage*Math.max.apply(Math, maxArr);
			document.getElementById("maxMarks").value = max;

	}

	dwr.util.getValues(questionsPool);
	dwr.engine.beginBatch();

	if(qType=='sl' || qType=='ml')
		questionsPool.questionWeightage=0;

	questionsPool.maxMarks = max;
	
	var isExperimental = false;
	if($('#isExperimental').is(':visible')){
		if($("#isExperimental").is(":checked")){
			isExperimental = true;
		}
	}
	
	AssessmentAjax.saveAssessmentSectionQuestion(questionsPool,assessmentDetail,assessmentSections,jjon,arr,assessmentQuestion,weightage,itemCode,isExperimental,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{

		var redirectURL = "";
		if(status=="save")
			redirectURL = "assessmentquestions.do?assessmentId="+assessmentId+"&sectionId="+sectionId;
		else
			redirectURL = "assessmentsectionquestions.do?sectionId="+sectionId;

			redirectTo(redirectURL);

		}
	});

	dwr.engine.endBatch();

}
function cancelQuestion(assessmentId,sectionId)
{
	var redirectURL = "assessmentquestions.do?assessmentId="+assessmentId+"&sectionId="+sectionId;
	redirectTo(redirectURL);
}
function getAssessmentSectionQuestionsGrid()
{
	var assessmentDetail = {assessmentId:dwr.util.getValue("assessmentId")};
	var assessmentSections = {sectionId:dwr.util.getValue("sectionId")};
	var domain={domainId:dwr.util.getValue("domainMaster")};
	var competency={competencyId:dwr.util.getValue("competencyMaster")};
	var objective={objectiveId:dwr.util.getValue("objectiveMaster")=="Select Objective"?"0":dwr.util.getValue("objectiveMaster")};

	AssessmentAjax.getAssessmentSectionQuestions(assessmentDetail,assessmentSections,domain,competency,objective,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#tblGrid').html(data);
		}
	});
}
function deleteAssessmentQuestion(assessmentQuestionId)
{
	if (confirm(resourceJSON.msgRemoveQuestionInventory)) {
		var assessmentQuestions = {assessmentQuestionId:assessmentQuestionId};

		AssessmentAjax.deleteAssessmentQuestion(assessmentQuestions, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			getAssessmentSectionQuestionsGrid();
			}
		});

	}
}
function setAssessmentQuestion(assessmentQuestionId) {

	if(assessmentQuestionId>0) {
		var assessmentQuestions = {assessmentQuestionId:assessmentQuestionId};

		AssessmentAjax.getAssessmentQuestionById(assessmentQuestions,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){

			var asseessmentType=data.assessmentDetail.assessmentType;
			if(asseessmentType==1 || asseessmentType==3 || asseessmentType==4) {
				getCompetencies(data.questionsPool.domainMaster.domainId);
				getObjectives(data.questionsPool.competencyMaster.competencyId);
			}

			dwr.util.setValues(data.questionsPool);

			$('#assQuestion').find(".jqte_editor").html(data.questionsPool.question)
			$('#assInstruction').find(".jqte_editor").html(data.questionsPool.questionInstruction)
			$('#itemCode').val(data.questionsPool.itemCode);
			
			if(asseessmentType!=2 && data.isExperimental!=null && data.isExperimental!=""){
				if(data.isExperimental==0)
					$("#isExperimental").prop('checked',false);
				else if(data.isExperimental==1)
					$("#isExperimental").prop('checked',true);
			}
            
			if(asseessmentType==1 || asseessmentType==3 || asseessmentType==4) {
				dwr.util.setValue("domainMaster",data.questionsPool.domainMaster.domainId);
				dwr.util.setValue("competencyMaster",data.questionsPool.competencyMaster.competencyId);
				dwr.util.setValue("objectiveMaster",data.questionsPool.objectiveMaster.objectiveId);
				dwr.util.setValue("stageOneStatus",data.questionsPool.stageOneStatus.stage1Id);
				dwr.util.setValue("stageTwoStatus",data.questionsPool.stageTwoStatus.stage2Id);
				dwr.util.setValue("stageThreeStatus",data.questionsPool.stageThreeStatus.stage3Id);
				dwr.util.setValue("questionWeightage",data.questionWeightage);
			}
			dwr.util.setValue("questionTypeMaster",data.questionsPool.questionTypeMaster.questionTypeId);
			getQuestionOptions(data.questionsPool.questionTypeMaster.questionTypeId);
			var cnnt=1;
			if(findSelected(data.questionsPool.questionTypeMaster.questionTypeId)=='it') {

				cnnt=7;

				jQuery.each(assessQuesOpts, function (i, jsonSelectedOptions) {								

					$('#fileDiv'+(cnnt-6)).hide();
					$('#imgTp'+(cnnt-6)).hide();
					$('#rm'+(cnnt-6)).show();
					$('#img'+(cnnt-6)).html("<a target='_blank' href='showImage?image=ques_images/"+jsonSelectedOptions.questionOption+"'><img src='showImage?image=ques_images/"+jsonSelectedOptions.questionOption+"' height='100' width='180'></a>");
					$('#img'+(cnnt-6)).show();

					$('#hid'+cnnt).attr("value",jsonSelectedOptions.optionId);
					$('#opt'+cnnt).attr("value",jsonSelectedOptions.questionOption);
					$('#score'+cnnt).attr("value",jsonSelectedOptions.score);
					$('#rank'+cnnt).attr("value",jsonSelectedOptions.rank==0?"":jsonSelectedOptions.rank);
					cnnt++;
				});

			} else {
				jQuery.each(assessQuesOpts, function (i, jsonSelectedOptions) {								
					$('#hid'+cnnt).attr("value",jsonSelectedOptions.optionId);
					$('#opt'+cnnt).attr("value",jsonSelectedOptions.questionOption);
					$('#score'+cnnt).attr("value",jsonSelectedOptions.score);
					$('#rank'+cnnt).attr("value",jsonSelectedOptions.rank==0?"":jsonSelectedOptions.rank);
					cnnt++;
				});
			}
		  }
		});	
	}
}
function getSectionQuestions(dis)
{
	var	redirectURL = "assessmentquestions.do?assessmentId="+$('#assessmentId').val()+"&sectionId="+dis.value;
	redirectTo(redirectURL);
}

function importQuestion(){
	
	$('.importQuestionDiv').modal('show');
}

function validateTeacherFile()
{
	var teacherfile	=	document.getElementById("teacherfile").value;
	var errorCount=0;
	var aDoc="";
	$('#errordivQuestionUpload').empty();
	
	if(teacherfile==""){
		$('#errordivQuestionUpload').show();
		$('#errordivQuestionUpload').append("&#149; "+resourceJSON.msgXlsXlsxFile+"<br>");
		errorCount++;
		aDoc="1";
	}
	else if(teacherfile!="")
	{
		var ext = teacherfile.substr(teacherfile.lastIndexOf('.') + 1).toLowerCase();	
		
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("teacherfile").files[0]!=undefined)
			{
				fileSize = document.getElementById("teacherfile").files[0].size;
			}
		}
		
		if(!(ext=='xlsx' || ext=='xls'))
		{
			$('#errordivQuestionUpload').show();
			$('#errordivQuestionUpload').append("&#149; "+resourceJSON.msgXlsXlsxFileOnly+"<br>");
			errorCount++;
			aDoc="1";
		}
		else if(fileSize>=10485760)
		{
			$('#errordivQuestionUpload').show();
			$('#errordivQuestionUpload').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			errorCount++;
			aDoc="1";	
		}
	}

	if(aDoc==1){
		$('#teacherfile').focus();
		$('#teacherfile').css("background-color", "#F5E7E1");
	}
	
	if(errorCount==0){
		try{
			if(teacherfile!=""){
					document.getElementById("questionUploadServlet").submit();
			}
		}catch(err){}

	}else{ 
		$('#errordivQuestionUpload').show();
		return false;
	}
}


function uploadDataQuestion(fileName,sessionId){
	$('#loadingDiv').fadeIn();
	TeacherUploadTempAjax.saveTeacherTemp(fileName,sessionId,{
		async: true,
		callback: function(data){
			if(data=='1'){
				window.location.href="candidatetemplist.do";
			}else{
				$('#loadingDiv').hide();
				$('#errordivQuestionUpload').show();
				$('#errordivQuestionUpload').append("&#149; "+resourceJSON.msgField+" "+data+" "+resourceJSON.msgDoesNotMatch+"<br>");
			}
		},
		errorHandler:handleError 
	});
}

/*search with district Name*/
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
	/*==== Gagan : District Auto Complete will show for each case ========*/
	ManageJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	
	return searchArray;
}
var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="") {
			var onclick="onclick=\"$('#divTxtShowData').css('display','none');\"";
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;

				if(count==10)
					break;
			}
		} else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}

		scrolButtom();
	}catch (err){}
}
var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}
function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if($('#districtName').val().trim()!=''){
	document.getElementById("districtHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(showDataArray && showDataArray[index]) {
			dis.value=showDataArray[index];
			document.getElementById("districtHiddenlId").value=hiddenDataArray[index];
		}
		
	} else {
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}

	if(document.getElementById(divId)) {
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
	}else{
		document.getElementById("districtHiddenlId").value='';
	}
}
function inputOnEnter(e) {
    var key;

    if (window.event)
        key = window.event.keyCode; //IE
    else
        key = e.which; //Firefox & others

    if(key == 13)
        return false;
}
/*
AssessmentAjax.getAllAssessmentGroupDetails({ 
	async: true,
	callback: function(data){
	console.log(data);
	//dwr.util.removeAllOptions("assessmentGroupDetails");
	dwr.util.addOptions("assessmentGroupDetails",data,"assessmentGroupId","assessmentGroupName");
	
	  $("select[name='assessmentGroupGetails']").html("<option value=''>Select Group</option>");
	  $(data).each(function(index, data) {
		  $("select[name='assessmentGroupGetails']").append("<option value='"+data.assessmentGroupId+"'>"+data.assessmentGroupName+"</option>");  
	  });
	
    },
    errorHandler:handleError  
});
*/

//amit
function getGroupsByAssessmentType(assessmentType, from)
{
	//alert('assessmentType=='+assessmentType);
	dwr.util.removeAllOptions("assessmentGroupDetails");
	dwr.util.addOptions("assessmentGroupDetails",["Select Group"]);
	AssessmentAjax.getAllAssessmentGroupDetailsList(assessmentType, from,{
		async: false,
		callback: function(data){
//			console.log(data);
			if(assessmentType=='1'){
				for(i=0;i<data.length;i++){
					if(data[i].isDefault!=null && data[i].isDefault==true){
						document.getElementById("defaultEPIGroupId").value = data[i].assessmentGroupId;
						$("#defaultEPIGroupId").attr("name", data[i].assessmentGroupName);
						$("select[name='assessmentGroupDetails']").append("<option value='"+data[i].assessmentGroupId+"' isdefault='"+data[i].isDefault+"'>"+data[i].assessmentGroupName+"</option>");
					}
					else{
						$("select[name='assessmentGroupDetails']").append("<option value='"+data[i].assessmentGroupId+"' isdefault='false'>"+data[i].assessmentGroupName+"</option>");
					}
				}
			}
			else{
				dwr.util.addOptions("assessmentGroupDetails",data,"assessmentGroupId","assessmentGroupName");
				document.getElementById("defaultEPIGroupId").value = '';
				$("#defaultEPIGroupId").attr("name", "");
			}
	    },errorHandler:handleError
	});
	if(lastGroupValue!="")
	{
		//alert('lastGroupValue=='+lastGroupValue);
		$('#assessmentGroupDetails').val(lastGroupValue);
	}
}

function checkDefaultEPIGroup(){
	if($("select[name='assessmentType']").val() == "1" && $('#assessmentGroupDetails').val() != 'Select Group' && $("#assessmentGroupDetails :selected").text() == $('#defaultEPIGroupId').attr("name")) {
		$("#isDefaultEPI").prop('checked',true);
    }
	else if($("select[name='assessmentType']").val() == "1" && $("#assessmentGroupDetails :selected").text() != $('#defaultEPIGroupId').attr("name")) {
		$("#isDefaultEPI").prop('checked',false);
    }
}

// ADD PDP Configuration 
function checkPDPAssociation(){
	if($('input[name=PDPassociatedopt]:checked').val() == "2" ) {
		$("#PDPselect").prop('selectedIndex', 0);
		$("#PDPselect").prop('disabled',true);
    }
	else if($('input[name=PDPassociatedopt]:checked').val() == "1") {
		$("#PDPselect").prop('selectedIndex', 0);
		$("#PDPselect").prop('disabled',false);
    }
}
function checkDefaultPDPAssociation(){
	$("#PDPassociatedoptno").attr('checked', true);// ADD PDP Configuration...
	$("#PDPselect").prop('selectedIndex', 0);
	$("#PDPselect").prop('disabled',true);
	$("#PDPassociatedoptno").prop('disabled',true);
	$("#PDPassociatedoptyes").prop('disabled',true);
}

$(document).ready(function(){
	$('#isDefaultEPI').change(function() {
	    //'checked' event code
		if($(this).is(":checked")) {
//			alert('checked');
		}
		//'unchecked' event code
		else{
//			alert('unchecked');
			if($("select[name='assessmentType']").val() == "1" && $('#assessmentGroupDetails').is(':enabled') && $('#assessmentGroupDetails').val() != 'Select Group' && $("#assessmentGroupDetails :selected").text() == $('#defaultEPIGroupId').attr("name")) {
				$('#modalDefaultEPIGroup').modal('show');
				$('#errorDivDefaultEPIGroup').empty();
				$('#modalAssessmentGroupDetails').empty();
				$('#modalAssessmentGroupDetails').css("background-color", "");
				var $options = $("#assessmentGroupDetails > option").clone();
				$('#modalAssessmentGroupDetails').append($options);
			}
		}
	})
});

function updateDefaultEPIGroup(){
	$('#errorDivDefaultEPIGroup').empty();
	$('#modalAssessmentGroupDetails').css("background-color", "");
	if($('#modalAssessmentGroupDetails').val() == 'Select Group'){
		$('#errorDivDefaultEPIGroup').show();
		$('#errorDivDefaultEPIGroup').append("&#149; "+resourceJSON.msgSelectGroup+"<br>");
    	$('#modalAssessmentGroupDetails').focus();
    	$('#modalAssessmentGroupDetails').css("background-color", "#F5E7E1");
		return false;
	}
	else{
		document.getElementById("modalDefaultEPIGroupId").value = $('#modalAssessmentGroupDetails').val();
		$("#modalDefaultEPIGroupId").attr("name", $("#modalAssessmentGroupDetails :selected").text());
		if($('#modalDefaultEPIGroupId').val() == $('#defaultEPIGroupId').val()){
			$('#errorDivDefaultEPIGroup').empty();
			$('#modalAssessmentGroupDetails').css("background-color", "");
			$('#errorDivDefaultEPIGroup').show();
			$('#errorDivDefaultEPIGroup').append("&#149; This Group is already a Default EPI Group.<br>");
	    	$('#modalAssessmentGroupDetails').focus();
	    	$('#modalAssessmentGroupDetails').css("background-color", "#F5E7E1");
	    	return false;
		}
		else{
			$("#isDefaultEPI").prop('checked',false);
			$('#modalDefaultEPIGroup').modal('hide');
		}
	}
}

function closeDefaultEPIGroup(){
	$("#isDefaultEPI").prop('checked',true);
	$('#modalDefaultEPIGroup').modal('hide');
}
