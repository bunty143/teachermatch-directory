function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

/********************************** district auto completer ***********************************/

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(districtNameFilter==1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("districtName").focus();
		
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	QuestAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{	
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];			
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];		
				
		}
		
	}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}
function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}


function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function isEmailAddress(str) 
{	

	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	return emailPattern.test(str);
	
}

function chkPwdPtrn(str)
{
	var pat1 = /[^a-zA-Z]/g; //If any spcl char find it return true otherwise false
	var pat2 = /[a-zA-Z]/g;  //If any alphabet found it return true otherwise false
	//alert(pat1.test(str) && pat2.test(str))
	return pat1.test(str) && pat2.test(str);
}
function checkEmail(emailAddress)
{
	var res;
	createXMLHttpRequest();  
	queryString = "chkemail.do?emailAddress="+emailAddress+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{			
				res = xmlHttp.responseText;				
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}

function PasswordValue(){  
   var control = document.getElementById("signupPassword");
   var myString= control.value; 
   var Stringlen = myString.length;
   var ValidateDigits = /[^0-9]/g;
   var ValidateSpChar = /[a-zA-Z0-9]/g;
   var ValidateChar = /[^a-zA-Z]/g;
   var digitString = myString.replace(ValidateDigits , "");
   var specialString = myString.replace(ValidateSpChar, "");
   var charString = myString.replace(ValidateChar, "");


   if(!specialString < 1 && !digitString < 1 && !charString < 1 && Stringlen>=8 ){
 	  
  	   document.getElementById('d8').style.display='none';
       document.getElementById('d9').style.display='none';
       document.getElementById('d4').style.backgroundColor = 'lightgreen';
       document.getElementById('d5').style.backgroundColor = 'lightgreen';
       document.getElementById('d6').style.display='inline';
       document.getElementById('d7').style.display='inline';
	   document.getElementById('d10').style.display='inline'; 
	   control.focus();
   }else if(!charString  < 1 && !digitString < 1 &&Stringlen>=6 ||!charString  < 1  &&!specialString < 1 && Stringlen>=6){
 	   
       document.getElementById('d6').style.display='none';
       document.getElementById('d7').style.display='none';
       document.getElementById('d10').style.display='none';       
       document.getElementById('d8').style.display='none';
	   document.getElementById('d4').style.display='inline';
       document.getElementById('d9').style.display='inline';
       document.getElementById('d5').style.display='inline';
	   document.getElementById('d4').style.backgroundColor = 'yellow';
       document.getElementById('d5').style.backgroundColor = 'yellow';       
   
   }else if( !specialString < 1 || !digitString < 1 || !charString < 1 || !charString && !digitString < 1 && Stringlen<6   ||!charString &&!specialString < 1 && Stringlen<6 ){
   	 
   	   document.getElementById('d5').style.display='none';
  	   document.getElementById('d6').style.display='none';
       document.getElementById('d7').style.display='none';
       document.getElementById('d9').style.display='none';
       document.getElementById('d10').style.display='none';       
	   document.getElementById('d4').style.display='inline';
       document.getElementById('d4').style.backgroundColor = 'red';
       document.getElementById('d8').style.display='inline';
	   control.focus();
   }else if(Stringlen<1){
	   document.getElementById('d4').style.display='none';
	   document.getElementById('d5').style.display='none';
  	   document.getElementById('d6').style.display='none';
       document.getElementById('d7').style.display='none';
  	   document.getElementById('d8').style.display='none';
       document.getElementById('d9').style.display='none';
       document.getElementById('d10').style.display='none';       
      
   }
 }





var txtBgColor="#F5E7E1";
function SignInDiv()
{	var video = document.getElementById("Video1");	    
    video.pause();	
    $('#errordiv').empty();
	$("#divServerError").empty();
	$("#screenData").hide();					
	$("#teacherModal").modal("show");
	document.getElementById("emailAddress").value="";
	document.getElementById("password").value="";	
	$('#emailAddress').css("background-color","");
    $('#password').css("background-color","");
	$( "#emailAddress").focus();
	
	
}		
function hideDiv()
{
    var video = document.getElementById("Video1");	    
    video.play();		
	$("#screenData").show();					
	$("#teacherModal").modal("hide");
}

function getPowerTracker(teacherId)
{  
	var a = 0;
	var b = 0;
	var c = 0;
	var d = 0;
	var id="";
	id = teacherId;
	var category ='';
	try
	{		
		$('#loadingDiv').show();
		QuestAjax.powerTracker(id,{
			async: false,
			callback: function(data){
			$('#loadingDiv').hide();
			var setcolor;
			if(data<=25)
			{
				a = data;
				//setcolor= d3.scale.ordinal().range(["#FF1919", "#D0D0D0"]);
				category='Aspiring';
				document.getElementById("title").innerHTML = "<a href='powerprofile.do' target='_blank' style='color:white;'>"+category+"<br/>Engagement</a>";
				document.getElementById("hidea").style.display = "block";
				document.getElementById("show").style.display = "none";
				document.getElementById("jobseekertext").style.display = "block";
	//  khan 
				
				if(resourceJSON.locale=="fr")
				{
				
					document.getElementById("statusimage").innerHTML="<a href='javascript:void(0)' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				else
				{
					
				  document.getElementById("statusimage").innerHTML="<a href='powerprofile.do' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				
				document.getElementById("statusText").innerHTML =""+resourceJSON.MsgAnAspiringEngagement+"";
				imghover1();
			}
			else if(data<=50)
			{
				a =25;
				b = data - 25;
				//setcolor= d3.scale.ordinal().range(["#FFD700", "#D0D0D0"]);
				category='Skilled';
				document.getElementById("title").innerHTML = "<a href='powerprofile.do' target='_blank' style='color:white;'>"+category+"<br/>Engagement</a>";
				document.getElementById("hidea").style.display = "block";
				document.getElementById("show").style.display = "none";
				document.getElementById("jobseekertext").style.display = "block";
	//  khan 
				
				if(resourceJSON.locale=="fr")
				{
				
					document.getElementById("statusimage").innerHTML="<a href='javascript:void(0)' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				else
				{
					
				  document.getElementById("statusimage").innerHTML="<a href='powerprofile.do' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				
				document.getElementById("statusText").innerHTML =""+resourceJSON.MsgASkilledEngagement+"";
				imghover1();
			}
			else if(data<=75)
			{
				a =25;
				b = 25;
				c = data - 50;				
				//setcolor= d3.scale.ordinal().range(["#008000", "#D0D0D0"]);
				category='Advanced';
				document.getElementById("title").innerHTML = "<a href='powerprofile.do' target='_blank' style='color:white;'>"+category+"<br/>"+resourceJSON.MsgEngagement+"</a>";
				document.getElementById("hidea").style.display = "block";
				document.getElementById("show").style.display = "none";
				document.getElementById("jobseekertext").style.display = "block";
	//  khan 
				
				if(resourceJSON.locale=="fr")
				{
				
					document.getElementById("statusimage").innerHTML="<a href='javascript:void(0)' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				else
				{
					
				  document.getElementById("statusimage").innerHTML="<a href='powerprofile.do' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				
				document.getElementById("statusText").innerHTML =""+resourceJSON.MsgAnAdvancedEngagement+"";
				imghover1();
			}
			else
			{
				a = 25;
				b = 25;
				c = 25;
				d = data - 75;
				//setcolor= d3.scale.ordinal().range(["#6495ED", "#D0D0D0"]);
				category='Expert';
				document.getElementById("title").innerHTML = "<a href='powerprofile.do' target='_blank' style='color:white;'>"+category+"<br/>Engagement</a>";
				document.getElementById("hidea").style.display = "none";
				document.getElementById("show").style.display = "block";
				document.getElementById("jobseekertext").style.display = "none";
				
				
				//  khan 
				
				if(resourceJSON.locale=="fr")
				{
				
					document.getElementById("statusimage").innerHTML="<a href='javascript:void(0)' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				else
				{
					
				  document.getElementById("statusimage").innerHTML="<a href='powerprofile.do' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				
				
				document.getElementById("statusText").innerHTML =""+resourceJSON.MsgAnExpertEngagement+"";
				imghover2();
			}	
			  (function () {
			    var chartConfig = {
			        width:290,
			        height:87.5,			      
			       // hasBottomLabel: true,
			      //  bottomMarginForLabel: 30,
			      //  color: d3.scale.category10(), 
			        //label:category,
			        color :d3.scale.ordinal().range(["#ED2028","#FECD49","#A9D489","#288ACA", "#434955"]),			     
			        renderHalfDonut: true,
			        margin: {
			            top: 33, 
			            right: 5,
			            bottom: 5,
			            left: 5
			        }
			    };
			    var fromValue = 100-data;
			    function makeRandomData() {
			        return {
			            centerLabel: data,
			            bottomLabel: 'world',
			            values: [
			               // {label: 'x', value: data},
			               // {label: 'y', value: fromValue}
			                {label: 'a', value:a},
			                {label: 'b', value: b},
							{label: 'c', value: c},
							{label: 'd', value: d},
							{label: 'e', value: fromValue}
			            ]
			        };
			    }
			    function init() {

			        var chartData = makeRandomData(),
			            chartId = 'thin-donut-chart',
			            d3ChartEl = d3.select('#' + chartId);
			        chartConfig.width = parseInt(d3ChartEl.style('width')) || chartConfig.width;
			        chartConfig.height = parseInt(d3ChartEl.style('height')) || chartConfig.height;
			        drawChart(chartId, chartData, chartConfig);
			    }
			    function drawChart(chartId, chartData, chartConfig) {
			        var width = chartConfig.width,
			            height = chartConfig.height,
			            margin = chartConfig.margin,
			            radius;
			        // Add a bottom margin if there is a label for the bottom
			        if (!!chartConfig.hasBottomLabel) {
			            margin.bottom = chartConfig.bottomMarginForLabel;
			        }
			        // Adjust for margins
			        width = width - margin.left - margin.right;
			        height = height - margin.top - margin.bottom;
			        if (chartConfig.renderHalfDonut) {
			            radius = Math.min(width / 2, height);			            
			        } else {
			            radius = Math.min(width, height) / 2;			            
			        }
			        var thickness = chartConfig.thickness || Math.floor(radius / 4); 
			        var arc = d3.svg.arc()
			            .outerRadius(radius)
			            .innerRadius(radius - thickness);
			        var pieFn = d3.layout.pie()
			            .sort(null)
			            .value(function (d) {
			                return d.value;
			            });
			        if (chartConfig.renderHalfDonut) {			        	
			            pieFn.startAngle(-Math.PI / 2).endAngle(Math.PI / 2);
			        }
			        var centerLabel = (!!chartData.centerLabel) ? chartData.centerLabel : '',
			            bottomLabel = (!!chartData.bottomLabel) ? chartData.bottomLabel : '';
			        var d3ChartEl = d3.select('#' + chartId);
			        // Clear any previous chart
			        d3ChartEl.select('svg').remove();
			        var gRoot = d3ChartEl.append('svg')
			            .attr('width', width + margin.left + margin.right)
			            .attr('height', height + margin.top + margin.bottom)
			            .append('g');
			        if (chartConfig.renderHalfDonut) {
			            gRoot.attr('class', 'half-donut');
			            gRoot.attr('transform', 'translate(' + (width / 2 + margin.left) + ',' + (height + margin.top) + ')');
			        } else {
			            gRoot.attr('transform', 'translate(' + (width / 2 + margin.left) + ',' + (height / 2 + margin.top) + ')');
			        }
			        var middleCircle = gRoot.append('svg:circle')
			            .attr('cx', 0)
			            .attr('cy', 0)
			            .attr('r', radius)			            
			            .style('fill', '#221f1f')
			            .attr('title',category);
			        var g = gRoot.selectAll('g.arc')
			            .data(pieFn(chartData.values))
			            .enter()
			            .append('g')
			            .attr('class', 'arc');
			        g.append('svg:path')
			            .attr('d', arc)
			            .style('fill', function (d) {
			                return chartConfig.color(d.data.label);
			            })
			            .attr('data-arc-data', function (d) {
			                return d.data.value;
			            });
			        gRoot.append('svg:text')
			            .attr('class', 'center-label')
			            .style('fill', 'white')
			            .text(centerLabel);		     

			       if (chartConfig.hasBottomLabel) {
			            gRoot.append('svg:text')
			                .attr('class', 'bottom-label')
			                .attr('transform', function (d) {
			                    return 'translate(0, ' + (height / 2 + margin.bottom / 2) + ')';
			                })
			                .text(bottomLabel);
			        }
			    }
			    init();
			})();			
		},
		errorHandler:handleError
		});	
	}
	catch(e)
	{alert(e)}
}

function teacherValidate()
{
	var emailAddress = document.getElementById("emailAddress");
	var password = document.getElementById("password");	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	
	document.getElementById("divServerError").style.display="none";	
	$('#emailAddress').css("background-color","");
	$('#password').css("background-color","");	
	
	if(trim(emailAddress.value) == '')
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value) != '' && !isEmailAddress(trim(emailAddress.value)))
	{		
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		$('#emailAddress').css("background-color", txtBgColor);
		cnt++;focs++;
	}
	if(trim(password.value) == '')
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgPassword+"<br>");
		if(focs==0)
			$('#password').focus();
		$('#password').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(cnt==0)
	{
		return true;		
	}
	else
	{
		$('#errordiv').show();
		return false;
	}
}
function userValidate()
{	
	var emailAddress = document.getElementById("emailAddress");
	var password = document.getElementById("password");	
	var cnt=0;
	var focs=0;	
	$('#usererrordiv').empty();
	
	document.getElementById("divServerError").style.display="none";		
	$('#emailAddress').css("background-color","");
	$('#password').css("background-color","");	
	
	if(emailAddress.value == '')
	{		
		$('#usererrordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value) != '' && !isEmailAddress(trim(emailAddress.value)))
	{				
		$('#usererrordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		$('#emailAddress').css("background-color", txtBgColor);
		cnt++;focs++;
	}
	if(trim(password.value) == '')
	{
		$('#usererrordiv').append("&#149; "+resourceJSON.msgPassword+"<br>");
		if(focs==0)
			$('#password').focus();
		$('#password').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(cnt==0)
	{		
		return true;		
	}
	else
	{		
		$('#usererrordiv').show();		
		return false;
	}
}
function loginTeacher()
{	
	var emailAddress = document.getElementById("emailAddress");
	var password = document.getElementById("password");	
	var txtRememberme="off";
	
	if(document.getElementById("teacherTxtRememberme").checked)
			{
		txtRememberme="on";
			}
	if(!teacherValidate())
		return;	
	try
	{		
		QuestAjax.teacherLogin(trim(emailAddress.value),trim(password.value),{ 
			async: false,
			callback: function(data){			
			if(data=="")
			{
				
				document.getElementById('frmpowertracker').action="teacherhome.do?emailAddress="+trim(emailAddress.value)+"&password="+trim(password.value)+"&txtRememberme="+txtRememberme;
				$('#frmpowertracker').submit();				
			}
			else
			{
				$("#divServerError").html(data);
				$("#divServerError").show();
				return false;
			}
			
			},errorHandler:handleError 
		});	
	}catch(e){alert(e)}	
}
function loginUser()
{	
	try
	{
	var emailAddress = document.getElementById("emailAddress");
	var password = document.getElementById("password");	
	if(!userValidate())
		return false;
	var password = document.getElementById("password");
	if(!userValidate())
		return false;
	QuestAjax.userLogin(trim(emailAddress.value),trim(password.value),{ 
			async: false,
			callback: function(data){	
		//alert("Return :: "+data);
			if(data=="")
			{
				$("#divServerError").hide();			
				//document.getElementById('userform').action="userhome.do?emailAddress="+trim(emailAddress.value)+"&password="+trim(password.value)+"&txtRememberme="+txtRememberme;
				$('#userform').submit();
				//$('#userform').submit();
			}
			else if(data.indexOf("isType3User")>=0)	//If server return a JSON
			{
				var myresult = jQuery.parseJSON(''+data+'');
				$('#selectSchoolName').find('option').remove();//remove the previous element.
  				$("#modalErrorMsg").text("");//remove the error message.
  				$('#selectSchoolName').append("<option value='none'>"+resourceJSON.MsgSelectSchool+"</option>");
  				$.each(myresult.schoolsDetails, function(index, data)
  				{
  					$('#selectSchoolName').append("<option value="+data.schoolId+">"+data.schoolName+"</option>");
  				});
  				$("#selectSchoolDiv").modal("show");
  			}
			else
			{
				$("#divServerError").html(data);
				$("#divServerError").show();
				return false;
			}
			
			},errorHandler:handleError 
		});	
	}catch(e){alert(e)}	

}
function afterLogin(checkCandidate)
{
	//alert(checkCandidate)
	if(checkCandidate==1)
	{
		document.location.href = "userdashboard.do";
	}
	else
	{
		document.location.href = "userdashboard.do";
	}
	
	
}
function afterUserLogin(checkDistrict)
{
	if(checkDistrict==1)
	{
		document.location.href = "userhome.do";
	}
	else
	{
		document.location.href = "tmdashboard.do";
	}
}
function afterSignUp()
{	
	document.location.href = "quest.do?success=1";
}

function afterSignUpNonClient()
{
	document.location.href = "quest.do?success=0";
}
function postJob()
{
	document.location.href = "employer-sign-in.do";	
}
var status="";
/*function questsignup(user)
{	

	if(user==1)
	{
		document.location.href = "questsignup.do?user=1";
	}
	else if(user==0)
	{
		document.location.href = "questsignup.do?user=0";
	}
}*/
function signUpUser(status)
{ 
	var otherdistrictName=document.getElementById("otherdistrictName");	
	var districtId="";	
	if(status==1)
	{		
		if(otherdistrictNamestatus!=1)
		{
			
			districtId = document.getElementById("districtId").value;
		}

	}
	/*if(user==1)
	{
		alert(444444)
		document.location.href = "questsignup.do?user=1";

	}*/
	var districtName=document.getElementById("districtName");	
	var other =0;
	var firstName = document.getElementById("fname");	
	var lastName = document.getElementById("lname");
	var emailAddress = document.getElementById("signupEmail");
	var password = document.getElementById("signupPassword");	

try
{
	var cnt=0;
	var focs=0;	
	var checkvalue=0;
	$('#signUpErrordiv').empty();
	$('#signUpServerError').css("background-color","");		
	$('#districtName').css("background-color","");
	$('#fname').css("background-color","");
	$('#lname').css("background-color","");
	$('#signupEmail').css("background-color","");
	$('#signupPassword').css("background-color","");
	$('#sumOfCaptchaText').css("background-color","");	
	$('#otherdistrictName').css("background-color","");
	
	if(status==1)
	{	
		if(otherdistrictNamestatus == 1)
		{				
			if(trim(otherdistrictName.value).length<1)
			{				
				$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
				if(focs==0)
					$('#otherdistrictName').focus();				
				$('#otherdistrictName').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			other=1;
		}
		else
		{
			other=0;
			if(trim(districtName.value)=="")
			{
				$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
				if(focs==0)
					$('#districtName').focus();
				
				$('#districtName').css("background-color",txtBgColor);
				cnt++;focs++;				
			}
			else if(districtId=="")
			{					
					$('#signUpErrordiv').append("&#149; "+resourceJSON.msgvaliddistrict+"<br>");
					if(focs==0)
						$('#districtName').focus();
					
					$('#districtName').css("background-color",txtBgColor);
					cnt++;focs++;				
			}
		}			
	}	
	if(trim(firstName.value)=="")
	{		
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focs==0)
			$('#fname').focus();
		
		$('#fname').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(lastName.value)=="")
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#lname').focus();
		
		$('#lname').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value)=="")
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#signupEmail').focus();
		
		$('#signupEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!isEmailAddress(emailAddress.value))
	{		
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#signupEmail').focus();
		
		$('#signupEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(checkEmail(emailAddress.value))
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.MsgForDifferentEmailAddress+"<br>");
		if(focs==0)
			$('#signupEmail').focus();
		
		$('#signupEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(password.value)=="") 
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.msgPassword+"<br>");
		if(focs==0)
			$('#signupPassword').focus();
		
		$('#signupPassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!chkPwdPtrn(password.value) || password.value.length<8 )
	{		
		$('#signUpErrordiv').append("&#149; "+resourceJSON.MsgForPasswordLength+"<br>");
		if(focs==0)
			$('#signupPassword').focus();
		
		$('#signupPassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(document.getElementById("captcharesponse").value=="failed" || document.getElementById("captcharesponse").value=='')
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzSelectCheckbox+" <br>");		
		cnt++;focs++;
	}		
	if(cnt==0)
	{	
		$('#loadingDiv').show();
		try
		{	
			var district ="";
			if(other==1)
			{
				district = document.getElementById("otherdistrictName").value;
				QuestAjax.questSignUp(district,trim(firstName.value),trim(lastName.value),trim(emailAddress.value),trim(password.value),0,1,{ 
					async: true,
					callback: function(data){
					if(data=="")
					{							
						$('#loadingDiv').hide();	
						window.location.href="employer-thank-you.do";						
					}
					else
					{						
						$("#signUpServerError").html(data);
						$("#divServerError").show();
						return false;
					}				
					},errorHandler:handleError 
				});
			}
			else if(districtId != "")
			{					
			QuestAjax.questSignUp(districtId,trim(firstName.value),trim(lastName.value),trim(emailAddress.value),trim(password.value),0,other,{ 
				async: true,
				callback: function(data){					
				try
				{
				if(data=="")
				{
					$('#loadingDiv').hide();					
					window.location.href="employer-thank-you.do";
				}
				else
				{					
					$('#loadingDiv').hide();
					$("#signUpServerError").html(data);
					$("#divServerError").show();
					return false;
				}	
				}catch(e){alert(e)}	
				},errorHandler:handleError 
			});
		}	
			else
			{					
			QuestAjax.questSignUp(districtId,trim(firstName.value),trim(lastName.value),trim(emailAddress.value),trim(password.value),0,other,{ 
				async: true,
				callback: function(data){					
				try
				{
				if(data=="")
				{
					$('#loadingDiv').hide();					
					window.location.href="candidate-thank-you.do";
				}
				else
				{					
					$('#loadingDiv').hide();
					$("#signUpServerError").html(data);
					$("#divServerError").show();
					return false;
				}	
				}catch(e){alert(e)}	
				},errorHandler:handleError 
			});
		}
		}catch(e){alert(e)}	
	}
	else
	{
		//changeCaptcha(null);
		$('#signUpErrordiv').show();
		return false;
	}
}

	

//}



/*function signUpUser()
{  	
	var districtName=document.getElementById("districtName");	
	var districtId = document.getElementById("districtId");	
=======
function signUpUser(status)
{ 	
	var districtId="";	
	if(status==1)
	{
		if(otherdistrictNamestatus!=1)
		{
			districtId = document.getElementById("districtId").value;
		}
	}
	var districtName=document.getElementById("districtName");
>>>>>>> 1.15
	var otherdistrictName=document.getElementById("otherdistrictName");	
	var other =0;
	var firstName = document.getElementById("fname");	
	var lastName = document.getElementById("lname");
	var emailAddress = document.getElementById("signupEmail");
	var password = document.getElementById("signupPassword");	
	var sumOfCaptchaText = document.getElementById("sumOfCaptchaText");
	
try
{
	var cnt=0;
	var focs=0;	
	var checkvalue=0;
	$('#signUpErrordiv').empty();
	$('#signUpServerError').css("background-color","");		
	$('#districtName').css("background-color","");
	$('#fname').css("background-color","");
	$('#lname').css("background-color","");
	$('#signupEmail').css("background-color","");
	$('#signupPassword').css("background-color","");
	$('#sumOfCaptchaText').css("background-color","");	
	$('#otherdistrictName').css("background-color","");
	
	if(status==1)
	{	
		if(otherdistrictNamestatus == 1)
		{				
			if(trim(otherdistrictName.value).length<1)
			{				
				$('#signUpErrordiv').append("&#149; Please enter District<br>");
				if(focs==0)
					$('#otherdistrictName').focus();				
				$('#otherdistrictName').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			//alert(" districtId :: "+districtId);
			other=1;
		}
		else
		{
			other=0;
			if(trim(districtName.value)=="")
			{
				$('#signUpErrordiv').append("&#149; Please enter District<br>");
				if(focs==0)
					$('#districtName').focus();
				
				$('#districtName').css("background-color",txtBgColor);
				cnt++;focs++;				
			}
			if(districtId=="")
			{					
					$('#signUpErrordiv').append("&#149; Please enter valid District<br>");
					if(focs==0)
						$('#districtName').focus();
					
					$('#districtName').css("background-color",txtBgColor);
					cnt++;focs++;				
			}
		}			
	}	
	if(trim(firstName.value)=="")
	{		
		$('#signUpErrordiv').append("&#149; Please enter First Name<br>");
		if(focs==0)
			$('#fname').focus();
		
		$('#fname').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(lastName.value)=="")
	{
		$('#signUpErrordiv').append("&#149; Please enter Last Name<br>");
		if(focs==0)
			$('#lname').focus();
		
		$('#lname').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value)=="")
	{
		$('#signUpErrordiv').append("&#149; Please enter Email<br>");
		if(focs==0)
			$('#signupEmail').focus();
		
		$('#signupEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!isEmailAddress(emailAddress.value))
	{		
		$('#signUpErrordiv').append("&#149; Please enter valid Email<br>");
		if(focs==0)
			$('#signupEmail').focus();
		
		$('#signupEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(checkEmail(emailAddress.value))
	{
		$('#signUpErrordiv').append("&#149; The email address you have provided is already registered with us, please use a different email address<br>");
		if(focs==0)
			$('#signupEmail').focus();
		
		$('#signupEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(password.value)=="") 
	{
		$('#signUpErrordiv').append("&#149; Please enter Password<br>");
		if(focs==0)
			$('#signupPassword').focus();
		
		$('#signupPassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!chkPwdPtrn(password.value) || password.value.length<6 )
	{		
		$('#signUpErrordiv').append("&#149; Password should be a combination of digits and characters or it should be a combination of digits and special symbols and Password length must be a minimum of 6 characters and a maximum of 12 characters.<br>");
		if(focs==0)
			$('#signupPassword').focus();
		
		$('#signupPassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(sumOfCaptchaText.value)=="")
	{
		$('#signUpErrordiv').append("&#149; Please enter sum of two numbers<br>");
		if(focs==0)
			$('#sumOfCaptchaText').focus();
		
		$('#sumOfCaptchaText').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(chkCaptcha())
	{
		$('#signUpErrordiv').append("&#149; Sum of both the numbers is wrong, please enter again<br>");
		if(focs==0)
			$('#sumOfCaptchaText').focus();
		
		$('#sumOfCaptchaText').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(cnt==0)
	{			
		try
		{	
			$("#loadingDiv").css({"z-index":"9000"});
<<<<<<< quest.js
			$('#loadingDiv').show();	
			var district ="";
			if(otherdistrictNamestatus==1)
			{
				district = document.getElementById("otherdistrictName").value;
				//alert(" other :: 1 :: "+otherdistrictNamestatus);
				QuestAjax.questSignUp(district,trim(firstName.value),trim(lastName.value),trim(emailAddress.value),trim(password.value),trim(sumOfCaptchaText.value),1,{ 
					async: true,
					callback: function(data){					
					if(data=="")
					{
						$('#loadingDiv').hide();
						$('#nonclientModal').modal('show');
						//window.location.href="questsignup.do?nonclient=0";
						afterSignUpNonClient();
						//document.getElementById('signupform').action="questsignup.do?nonclient=1";					
						//$('#signupform').submit();	
					}
					else
					{
						$("#signUpServerError").html(data);
						$("#divServerError").show();
						return false;
					}				
					},errorHandler:handleError 
				});
			}
			else if(otherdistrictNamestatus==0)
			{
				district = document.getElementById("districtId").value;
				//alert(" other :: 0 :: "+otherdistrictNamestatus);
				QuestAjax.questSignUp(district,trim(firstName.value),trim(lastName.value),trim(emailAddress.value),trim(password.value),trim(sumOfCaptchaText.value),0,{ 
					async: true,
					callback: function(data){				
					$('#loadingDiv').hide();
					if(data=="")
					{
						$('#messageModal').modal('show');
						afterSignUp();
						document.getElementById('signupform').action="questsignup.do?nonclient=0";					
						$('#signupform').submit();	
					}
					else
					{
						$("#signUpServerError").html(data);
						$("#divServerError").show();
						return false;
					}				
					},errorHandler:handleError 
				});
			}
			

			$('#loadingDiv').show();			
			QuestAjax.questSignUp(districtId,trim(firstName.value),trim(lastName.value),trim(emailAddress.value),trim(password.value),trim(sumOfCaptchaText.value),other,{ 
				async: true,
				callback: function(data){					
				try
				{
				if(data=="")
				{
					$('#loadingDiv').hide();
					window.location.href="quest.do";
					/*document.getElementById("#signupform").action="questsignup.do";					                          
                    $('#signupform').submit();  
			}
				else
				{					
					$('#loadingDiv').hide();
					$("#signUpServerError").html(data);
					$("#divServerError").show();
					return false;
				}	
				}catch(e){alert(e)}	
				},errorHandler:handleError 
			});	

		}catch(e){alert(e)}	
	}
	else
	{
		changeCaptcha(null);
		$('#signUpErrordiv').show();
		return false;
	}
}
catch(e){alert(e);}
}*/
catch(e){alert(e);}
}


function hideDiv()
{	
	$("#messageModal").hide()
	document.location.href = "quest.do";
}
function allowToEnterDistrict()
{
	
}

var otherdistrictNamestatus=0;

function showforotherDistrict()
{
	try
	{
		$('#districtName').hide();
		$('#otherdistrictName').show();	
		otherdistrictNamestatus=1;
	}
	catch(e)
	{alert(e)}
   return false;
}

function showHideJbApld(showhide)
{
	document.getElementById("jobAppliedFromDate").value="";
	document.getElementById("jobAppliedToDate").value="";	
	if(showhide==0)
	{
		document.getElementById("jaFrom").style.display="none";
		document.getElementById("jaTo").style.display="none";
	}else
	{
		document.getElementById("jaFrom").style.display="block";
		document.getElementById("jaTo").style.display="block";
	}
}

/*=======  searchTeacher on Press Enter Key ========= */
function chkForEnterSearchTeacher(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;	
	if(charCode==13)
	{
		searchTeacher();
	}	
}
/*========  display grid after Filter ===============*/
function searchTeacher()
{
	
	page=1;
	displayTeacherGrid();
}
function getPagingForJobboard(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayTeacherGrid();
}
function getPaging(pageno)
{	
		if(document.getElementById("pageSize")!=''){
			noOfRows = parseInt(document.getElementById("pageSize").value);
		}else{
			noOfRows=10;
		}
		if(page!='')
		{
			page = pageno;
		}
		else
		{
			page = 1;
		}
		displayTeacherGrid();	
}
function displayTeacherGrid()
{
	
	exportIconsDiv=true;   
	
	currentPageFlag="";
	
	$('#advanceSearchDiv').hide();
	
	$('#searchLinkDiv').fadeIn();	

	$('#loadingDiv').show();
	var firstName		=	trim(document.getElementById("firstName").value);	
	var lastName		=	trim(document.getElementById("lastName").value);	
	var emailAddress	=	trim(document.getElementById("emailAddress").value);	
	/*var fromDate	=	trim(document.getElementById("fromDate").value);
	var toDate		=	trim(document.getElementById("toDate").value); */
	var jobAppliedFromDate	=	trim(document.getElementById("jobAppliedFromDate").value);	
	var jobAppliedToDate	=	trim(document.getElementById("jobAppliedToDate").value); 
	
	var daysVal = $("input[name=jobApplied]:radio:checked").val();	
	//alert("firstName ::"+firstName+"lastName ::"+lastName+"emailAddress ::"+emailAddress+"jobAppliedFromDate ::"+jobAppliedFromDate+"jobAppliedToDate ::"+jobAppliedToDate+"daysVal ::"+daysVal+" noofRows :: "+noOfRows+" page :: "+page+" sortOrderStr :: "+sortOrderStr+" sortOrderType :: "+sortOrderType)
	try
	{
		var url =window.location.href;
		  if(url.indexOf("questcandidatesnew.do") > -1){
	QuestAjax.displayTeacherGrid_OP(firstName,lastName,emailAddress,noOfRows,page,sortOrderStr,sortOrderType,jobAppliedFromDate,jobAppliedToDate,daysVal,{ 
		async: true,
		callback: function(data)
		{
		//alert(data);
		 //   $('#exportIconsDiv').show();
			$('#loadingDiv').hide();			
			$('#teacherGrid').html(data);			
			applyScrollOnTbl();			
		/*	========== for showing Tool tip on Images ============*/
			//tpJbIDisable();
			//tpJbIEnable();
		},
		errorHandler:handleError 
	});
}else{
	QuestAjax.displayTeacherGrid(firstName,lastName,emailAddress,noOfRows,page,sortOrderStr,sortOrderType,jobAppliedFromDate,jobAppliedToDate,daysVal,{ 
		async: true,
		callback: function(data)
		{
		//alert(data);
		 //   $('#exportIconsDiv').show();
			$('#loadingDiv').hide();			
			$('#teacherGrid').html(data);			
			applyScrollOnTbl();			
		/*	========== for showing Tool tip on Images ============*/
			//tpJbIDisable();
			//tpJbIEnable();
		},
		errorHandler:handleError 
	});
}
	}catch(e){alert(e)}	
	
}



function defaultTeacherGrid(){
	page = 1;
	noOfRows = 20;
	sortOrderStr="";
	sortOrderType="";
	currentPageFlag="";
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(currentPageFlag=="tran" || currentPageFlag=="cert"  || currentPageFlag=="job"){
		if(pageno!=''){
			pageForTC=pageno;	
		}else{
			pageForTC=1;
		}
		sortOrderStrForTC	=	sortOrder;
		sortOrderTypeForTC	=	sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRowsTranCert = document.getElementById("pageSize").value;
		}else{
			noOfRowsTranCert=100;
		}
		if(currentPageFlag=="job"){
			getJobOrderList();
		}else if(currentPageFlag=="tran"){
			getTranscriptGrid();
		}else{
			getCertificationGrid();
		}
	}
	else if(currentPageFlag=="epiJsi")
	{
		if(pageno!=''){
			pageEJ=pageno;	
		}else{
			pageEJ=1;
		}
		sortOrderStrEJ=sortOrder;
		sortOrderTypeEJ=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRowsEJ = document.getElementById("pageSize").value;
		}else{
			noOfRowsEJ=10;
		}
		showEpiAndJsi1();
	}
	else if(currentPageFlag=="stt"){
		if(pageno!=''){
			pageSTT=pageno;	
		}else{
			pageSTT=1;
		}
		sortOrderStrSTT	=	sortOrder;
		sortOrderTypeSTT	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsSTT = document.getElementById("pageSize3").value;
		}else{
			noOfRowsSTT=10;
		}
		var folderId = document.getElementById("folderId").value;
		var checkboxshowHideFlag= document.getElementById("checkboxshowHideFlag").value;
		displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
	}else if(currentPageFlag=="us"){
		if(pageno!=''){
			pageUS=pageno;	
		}else{
			pageUS=1;
		}
		sortOrderStrUS	=	sortOrder;
		sortOrderTypeUS	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsUS = document.getElementById("pageSize3").value;
		}else{
			noOfRowsUS=10;
		}
		var userFPS=document.getElementById("userFPS").value;
		var userFPD=document.getElementById("userFPD").value;
		var userCPS=document.getElementById("userCPS").value;
		var userCPD=document.getElementById("userCPD").value;
		if(userFPS==1){
			searchUser(0);
		}else if(userFPD==1){
			displayUsergrid(0,1);
		}else if(userCPS==1){
			searchUserthroughPopUp(0);
		}else if(userCPD==1){
			displayShareFolder();
		}
	}
	else if(currentPageFlag=="workExp"){
		if(pageno!=''){
			pageworkExp=pageno;	
		}else{
			pageworkExp=1;
		}
		sortOrderStrworkExp	=	sortOrder;
		sortOrderTypeworkExp	=	sortOrderTyp;
		if(document.getElementById("pageSize11")!=null){
			noOfRowsworkExp = document.getElementById("pageSize11").value;
		}else{
			noOfRowsworkExp=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getPFEmploymentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="References"){
		if(pageno!=''){
			pageReferences=pageno;	
		}else{
			pageReferences=1;
		}
		sortOrderStrReferences	=	sortOrder;
		sortOrderTypeReferences	=	sortOrderTyp;
		if(document.getElementById("pageSize12")!=null){
			noOfRowsReferences = document.getElementById("pageSize12").value;
		}else{
			noOfRowsReferences=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="videoLink"){
		if(pageno!=''){
			pagevideoLink=pageno;	
		}else{
			pagevideoLink=1;
		}
		sortOrderStrvideoLink	=	sortOrder;
		sortOrderTypevideoLink	=	sortOrderTyp;
		if(document.getElementById("pageSize13")!=null){
			noOfRowsvideoLink = document.getElementById("pageSize13").value;
		}else{
			noOfRowsvideoLink=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getVideoLinksGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="document"){
		if(pageno!=''){
			pageaddDoc=pageno;	
		}else{
			pageaddDoc=1;
		}
		sortOrderStraddDoc	=	sortOrder;
		sortOrderTypeaddDoc	=	sortOrderTyp;
		if(document.getElementById("pageSize15")!=null){
			noOfRowsaddDoc = document.getElementById("pageSize15").value;
		}else{
			noOfRowsaddDoc=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;		
		getadddocumentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="tpvh"){
		if(pageno!=''){
			pagetpvh=pageno;	
		}else{
			pagetpvh=1;
		}
		sortOrderStrtpvh	=	sortOrder;
		sortOrderTypetpvh	=	sortOrderTyp;
		if(document.getElementById("pageSize14")!=null){
			noOfRowstpvh = document.getElementById("pageSize14").value;
		}else{
			noOfRowstpvh=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherProfileVisitHistoryShow(teacherIdForprofileGrid);
	}else if(currentPageFlag=="teacherAce"){
		if(pageno!=''){
			pageteacherAce=pageno;	
		}else{
			pageteacherAce=1;
		}
		sortOrderStrteacherAce	=	sortOrder;
		sortOrderTypeteacherAce	=	sortOrderTyp;
		if(document.getElementById("pageSize10")!=null){
			noOfRowsteacherAce = document.getElementById("pageSize10").value;
		}else{
			noOfRowsteacherAce=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAcademicsGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="teacherCerti"){
		if(pageno!=''){
			pageteacherCerti=pageno;	
		}else{
			pageteacherCerti=1;
		}
		sortOrderStrteacherCerti	=	sortOrder;
		sortOrderTypeteacherCerti	=	sortOrderTyp;
		if(document.getElementById("pageSize10")!=null){
			noOfRowsteacherCerti = document.getElementById("pageSize9").value;
		}else{
			noOfRowsteacherCerti=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherCertificationsGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="jobDetail")
	{
		if(pageno!=''){
			pageJobDetail=pageno;	
		}else{
			pageJobDetail=1;
		}
		sortOrderStrJobDetail	=	sortOrder;
		sortOrderTypeJobDetail	=	sortOrderTyp;
		if(document.getElementById("pageSize12")!=null){
			noOfRowsJobDetail = document.getElementById("pageSize12").value;
		}else{
			noOfRowsJobDetail=10;
		}
		
		getJobDetailList($("#tId4Job").val());
	}else if(currentPageFlag=="zone")
	{	
		if(pageno!=''){
			pageZS=pageno;	
		}else{
			pageZS=1;
		}
		sortOrderStrZS=sortOrder;
		sortOrderTypeZS=sortOrderTyp;
		if(document.getElementById("pageSizeZoneSchool")!=null){
			noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		}else{
			noOfRowsZS=100;
		}
		displayZoneSchoolList();
	}	
	else
	{
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr=sortOrder;
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRows = document.getElementById("pageSize").value;
		}else{
			noOfRows=100;
		}
		displayTeacherGrid();
	}
}

function getPPIDetails()
{
	window.open(
			"powerprofile.do",
			  '_blank' // <- This is what makes it open in a new window.
			);
	//document.location.href = "powerprofile.do";
}

function displayPowerProfile() {
    
    try
    {           
          $('#loadingDivs').show();
          QuestAjax.displayPowerProfile({
          async: true,
          callback: function(data)
          {
        	  $('#loadingDivs').hide();
               $('#dispPowerProfileGrid').html(data);          
                
          },        
    });
}
catch(e)
{
    alert(e);
}    
}


function callepi()
{
checkInventory(0,null);      
}
function imghover1()
{

                $("#himg1").hide();
                $("#himg2").hide();
                $("#himg3").hide();
                $("#himg4").hide();
                $("#himg5").hide();                
                $("#hdiv1").hover(
        function()
        {
                        $("#hb1").hide();
                        $("#himg1").show();
                        
                        
        },
        function()
        {
                        $("#himg1").hide();
                        $("#hb1").show();
                                
                }
);
                
                $("#hdiv2").hover(
                function()
                {
                                $("#hb2").hide();
                                $("#himg2").show();      
                },
                function()
                {
                                $("#himg2").hide();
                                $("#hb2").show();
                }
            );             
                $("#hdiv3").hover(
            function()
            {
                             $("#hb3").hide();
                             $("#himg3").show();     
            },
            function()
            {
                            $("#himg3").hide();
                            $("#hb3").show();
                }
);
                $("#hdiv4").hover(
            function()
            {
                             $("#hb4").hide();
                             $("#himg4").show();     
            },
            function()
            {
                            $("#himg4").hide();
                            $("#hb4").show();
            }
                                );
                $("#hdiv5").hover(
            function()
            {
                             $("#hb5").hide();
                             $("#himg5").show();     
            },
            function()
            {
                            $("#himg5").hide();
                            $("#hb5").show();
            }
                                );
}
//Amit Kumar
function imghover2()
{
                $("#simg2").hide();
                $("#sdiv2").hover(
                function()
                {
                                $("#sb2").hide();
                                $("#simg2").show();       
                },
                function()
                {
                                $("#simg2").hide();
                                $("#sb2").show();
                }
);
}


function getEPI(teacherId)
{ 
	var id="";
	id = teacherId;	
	try
	{		
		
		QuestAjax.powerTracker(id,{
			async: false,
			callback: function(data){			
			if(data<=75)
			{	
				$(document).ready(function() {
			         $("#hidea").show(); 
			         $("#show").hide();
			         imghover1();
			       });
				
			}
			else
			{
				$(document).ready(function() {
			         $("#hidea").hide(); 
			         $("#show").show();
			         imghover2();
			       });							
				
			}				 		
		},
		errorHandler:handleError
		});	
	}
	catch(e)
	{alert(e)}
	
}

function getCandidateListOfAllJobs()
{
exportIconsDiv=true;   
	
	currentPageFlag="";
	
	$('#advanceSearchDiv').hide();
	
	$('#searchLinkDiv').fadeIn();	

	$('#loadingDiv').show();
	var firstName		=	trim(document.getElementById("firstName").value);	
	var lastName		=	trim(document.getElementById("lastName").value);	
	var emailAddress	=	trim(document.getElementById("emailAddress").value);	
	var jobAppliedFromDate	=	"";	
	var jobAppliedToDate	=	""; 

	var daysVal = 5;	
	//alert("firstName ::"+firstName+"lastName ::"+lastName+"emailAddress ::"+emailAddress+"jobAppliedFromDate ::"+jobAppliedFromDate+"jobAppliedToDate ::"+jobAppliedToDate+"daysVal ::"+daysVal+" noofRows :: "+noOfRows+" page :: "+page+" sortOrderStr :: "+sortOrderStr+" sortOrderType :: "+sortOrderType)
	try
	{
		var url =window.location.href;
		if(url.indexOf("questcandidatesnew.do") > -1){
			QuestAjax.displayTeacherGridForExcel_OP(firstName,lastName,emailAddress,noOfRows,page,sortOrderStr,sortOrderType,jobAppliedFromDate,jobAppliedToDate,daysVal,{ 
				async: true,
				callback: function(data)
				{
				try{
					document.getElementById('ifrmTrans2').src = "candidatelistAppiledforjob/"+data+"";
					$('#loadingDiv').hide();
				}catch(e){alert(e);
				}
				},
				errorHandler:handleError 
			});
		}else{
			QuestAjax.displayTeacherGridForExcel(firstName,lastName,emailAddress,noOfRows,page,sortOrderStr,sortOrderType,jobAppliedFromDate,jobAppliedToDate,daysVal,{ 
				async: true,
				callback: function(data)
				{
				try{
					document.getElementById('ifrmTrans2').src = "candidatelistAppiledforjob/"+data+"";
					$('#loadingDiv').hide();
				}catch(e){alert(e);
				}
				},
				errorHandler:handleError 
			});
		}
	}catch(e){alert(e)}	
}


/////PDF FOR CANDIDATE LIST
function getOhioCandidateListPdf()
{
exportIconsDiv=true;   
	
	currentPageFlag="";
	
	$('#advanceSearchDiv').hide();
	
	$('#searchLinkDiv').fadeIn();	

	$('#loadingDiv').show();
	var firstName		=	trim(document.getElementById("firstName").value);	
	var lastName		=	trim(document.getElementById("lastName").value);	
	var emailAddress	=	trim(document.getElementById("emailAddress").value);	
	var jobAppliedFromDate	=	"";	
	var jobAppliedToDate	=	""; 

	var daysVal = 5;	

$('#loadingDiv').fadeIn();
var url =window.location.href;
if(url.indexOf("questcandidatesnew.do") > -1){
	QuestAjax.displayOhioCandidateListPdf_OP(firstName,lastName,emailAddress,noOfRows,page,sortOrderStr,sortOrderType,jobAppliedFromDate,jobAppliedToDate,daysVal,{
	async: true,
	callback: function(data)
	{
	$('#loadingDiv').hide();	
	if(deviceType || deviceTypeAndroid)
	{
		window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
	}		
	else
	{
		$('#modalDownloadOhioCandidatesList').modal('hide');
		document.getElementById('ifrmCJS1').src = ""+data+"";
		try{
			$('#modalDownloadOhioCandidatesList').modal('show');
		}catch(err)
		{}		
     }		
     return false;
}});
}else{
	QuestAjax.displayOhioCandidateListPdf(firstName,lastName,emailAddress,noOfRows,page,sortOrderStr,sortOrderType,jobAppliedFromDate,jobAppliedToDate,daysVal,{
		async: true,
		callback: function(data)
		{
		$('#loadingDiv').hide();	
		if(deviceType || deviceTypeAndroid)
		{
			window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
		}		
		else
		{
			$('#modalDownloadOhioCandidatesList').modal('hide');
			document.getElementById('ifrmCJS1').src = ""+data+"";
			try{
				$('#modalDownloadOhioCandidatesList').modal('show');
			}catch(err)
			{}		
	     }		
	     return false;
	}});
}
}



//Ohio Candidate List Print Preview
function generateOhioCandidatListPrintPre()
{
	exportIconsDiv=true;   

currentPageFlag="";

$('#advanceSearchDiv').hide();

$('#searchLinkDiv').fadeIn();	

$('#loadingDiv').show();
var firstName		=	trim(document.getElementById("firstName").value);	
var lastName		=	trim(document.getElementById("lastName").value);	
var emailAddress	=	trim(document.getElementById("emailAddress").value);	
var jobAppliedFromDate	=	"";	
var jobAppliedToDate	=	""; 

var daysVal = 5;	

$('#loadingDiv').fadeIn();
var url =window.location.href;
if(url.indexOf("questcandidatesnew.do") > -1){
	QuestAjax.displayOhioCandidateListPrintPreview_OP(firstName,lastName,emailAddress,noOfRows,page,sortOrderStr,sortOrderType,jobAppliedFromDate,jobAppliedToDate,daysVal,{
		async: true,
		callback: function(data)
		{	$('#loadingDiv').hide();
		try{ 
			if (isSafari && !deviceType)
			    {
			    	window.document.write(data);
					window.print();
			    }else
			    {
			    	var newWindow = window.open();
			    	newWindow.document.write(data);	
			    	newWindow.print();
				 } 
			}catch (e) 
			{
				$('#printmessageOhio').modal('show');							 
			}
		},
	errorHandler:handleError
	});
}else{
	QuestAjax.displayOhioCandidateListPrintPreview(firstName,lastName,emailAddress,noOfRows,page,sortOrderStr,sortOrderType,jobAppliedFromDate,jobAppliedToDate,daysVal,{
		async: true,
		callback: function(data)
		{	$('#loadingDiv').hide();
		try{ 
			if (isSafari && !deviceType)
			    {
			    	window.document.write(data);
					window.print();
			    }else
			    {
			    	var newWindow = window.open();
			    	newWindow.document.write(data);	
			    	newWindow.print();
				 } 
			}catch (e) 
			{
				$('#printmessageOhio').modal('show');							 
			}
		},
	errorHandler:handleError
	});
}	
}





function getPowerTracker_Op(teacherId)
{  
	var a = 0;
	var b = 0;
	var c = 0;
	var d = 0;
	var id="";
	id = teacherId;
	var category ='';
	try
	{		
		$('#loadingDiv').show();
		QuestAjax.powerTracker_Op(id,{
			async: false,
			callback: function(data){
			$('#loadingDiv').hide();
			var setcolor;
			if(data<=25)
			{
				a = data;
				//setcolor= d3.scale.ordinal().range(["#FF1919", "#D0D0D0"]);
				category='Aspiring';
				document.getElementById("title").innerHTML = "<a href='powerprofile.do' target='_blank' style='color:white;'>"+category+"<br/>Engagement</a>";
				document.getElementById("hidea").style.display = "block";
				document.getElementById("show").style.display = "none";
				document.getElementById("jobseekertext").style.display = "block";
	//  khan 
				
				if(resourceJSON.locale=="fr")
				{
				
					document.getElementById("statusimage").innerHTML="<a href='javascript:void(0)' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				else
				{
					
				  document.getElementById("statusimage").innerHTML="<a href='powerprofile.do' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				
				document.getElementById("statusText").innerHTML =""+resourceJSON.MsgAnAspiringEngagement+"";
				imghover1();
			}
			else if(data<=50)
			{
				a =25;
				b = data - 25;
				//setcolor= d3.scale.ordinal().range(["#FFD700", "#D0D0D0"]);
				category='Skilled';
				document.getElementById("title").innerHTML = "<a href='powerprofile.do' target='_blank' style='color:white;'>"+category+"<br/>Engagement</a>";
				document.getElementById("hidea").style.display = "block";
				document.getElementById("show").style.display = "none";
				document.getElementById("jobseekertext").style.display = "block";
	//  khan 
				
				if(resourceJSON.locale=="fr")
				{
				
					document.getElementById("statusimage").innerHTML="<a href='javascript:void(0)' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				else
				{
					
				  document.getElementById("statusimage").innerHTML="<a href='powerprofile.do' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				
				document.getElementById("statusText").innerHTML =""+resourceJSON.MsgASkilledEngagement+"";
				imghover1();
			}
			else if(data<=75)
			{
				a =25;
				b = 25;
				c = data - 50;				
				//setcolor= d3.scale.ordinal().range(["#008000", "#D0D0D0"]);
				category='Advanced';
				document.getElementById("title").innerHTML = "<a href='powerprofile.do' target='_blank' style='color:white;'>"+category+"<br/>"+resourceJSON.MsgEngagement+"</a>";
				document.getElementById("hidea").style.display = "block";
				document.getElementById("show").style.display = "none";
				document.getElementById("jobseekertext").style.display = "block";
	//  khan 
				
				if(resourceJSON.locale=="fr")
				{
				
					document.getElementById("statusimage").innerHTML="<a href='javascript:void(0)' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				else
				{
					
				  document.getElementById("statusimage").innerHTML="<a href='powerprofile.do' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				
				document.getElementById("statusText").innerHTML =""+resourceJSON.MsgAnAdvancedEngagement+"";
				imghover1();
			}
			else
			{
				a = 25;
				b = 25;
				c = 25;
				d = data - 75;
				//setcolor= d3.scale.ordinal().range(["#6495ED", "#D0D0D0"]);
				category='Expert';
				document.getElementById("title").innerHTML = "<a href='powerprofile.do' target='_blank' style='color:white;'>"+category+"<br/>Engagement</a>";
				document.getElementById("hidea").style.display = "none";
				document.getElementById("show").style.display = "block";
				document.getElementById("jobseekertext").style.display = "none";
				
				
				//  khan 
				
				if(resourceJSON.locale=="fr")
				{
				
					document.getElementById("statusimage").innerHTML="<a href='javascript:void(0)' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				else
				{
					
				  document.getElementById("statusimage").innerHTML="<a href='powerprofile.do' target='_blank' data-toggle='tooltip' data-original-title='"+resourceJSON.MsgToIncreasePowerProfile+"' id='search222Tooltip'><img  src='images/ppi4.png' style='width:150px;'></a>";
				}
				
				
				document.getElementById("statusText").innerHTML =""+resourceJSON.MsgAnExpertEngagement+"";
				imghover2();
			}	
			  (function () {
			    var chartConfig = {
			        width:290,
			        height:87.5,			      
			       // hasBottomLabel: true,
			      //  bottomMarginForLabel: 30,
			      //  color: d3.scale.category10(), 
			        //label:category,
			        color :d3.scale.ordinal().range(["#ED2028","#FECD49","#A9D489","#288ACA", "#434955"]),			     
			        renderHalfDonut: true,
			        margin: {
			            top: 33, 
			            right: 5,
			            bottom: 5,
			            left: 5
			        }
			    };
			    var fromValue = 100-data;
			    function makeRandomData() {
			        return {
			            centerLabel: data,
			            bottomLabel: 'world',
			            values: [
			               // {label: 'x', value: data},
			               // {label: 'y', value: fromValue}
			                {label: 'a', value:a},
			                {label: 'b', value: b},
							{label: 'c', value: c},
							{label: 'd', value: d},
							{label: 'e', value: fromValue}
			            ]
			        };
			    }
			    function init() {

			        var chartData = makeRandomData(),
			            chartId = 'thin-donut-chart',
			            d3ChartEl = d3.select('#' + chartId);
			        chartConfig.width = parseInt(d3ChartEl.style('width')) || chartConfig.width;
			        chartConfig.height = parseInt(d3ChartEl.style('height')) || chartConfig.height;
			        drawChart(chartId, chartData, chartConfig);
			    }
			    function drawChart(chartId, chartData, chartConfig) {
			        var width = chartConfig.width,
			            height = chartConfig.height,
			            margin = chartConfig.margin,
			            radius;
			        // Add a bottom margin if there is a label for the bottom
			        if (!!chartConfig.hasBottomLabel) {
			            margin.bottom = chartConfig.bottomMarginForLabel;
			        }
			        // Adjust for margins
			        width = width - margin.left - margin.right;
			        height = height - margin.top - margin.bottom;
			        if (chartConfig.renderHalfDonut) {
			            radius = Math.min(width / 2, height);			            
			        } else {
			            radius = Math.min(width, height) / 2;			            
			        }
			        var thickness = chartConfig.thickness || Math.floor(radius / 4); 
			        var arc = d3.svg.arc()
			            .outerRadius(radius)
			            .innerRadius(radius - thickness);
			        var pieFn = d3.layout.pie()
			            .sort(null)
			            .value(function (d) {
			                return d.value;
			            });
			        if (chartConfig.renderHalfDonut) {			        	
			            pieFn.startAngle(-Math.PI / 2).endAngle(Math.PI / 2);
			        }
			        var centerLabel = (!!chartData.centerLabel) ? chartData.centerLabel : '',
			            bottomLabel = (!!chartData.bottomLabel) ? chartData.bottomLabel : '';
			        var d3ChartEl = d3.select('#' + chartId);
			        // Clear any previous chart
			        d3ChartEl.select('svg').remove();
			        var gRoot = d3ChartEl.append('svg')
			            .attr('width', width + margin.left + margin.right)
			            .attr('height', height + margin.top + margin.bottom)
			            .append('g');
			        if (chartConfig.renderHalfDonut) {
			            gRoot.attr('class', 'half-donut');
			            gRoot.attr('transform', 'translate(' + (width / 2 + margin.left) + ',' + (height + margin.top) + ')');
			        } else {
			            gRoot.attr('transform', 'translate(' + (width / 2 + margin.left) + ',' + (height / 2 + margin.top) + ')');
			        }
			        var middleCircle = gRoot.append('svg:circle')
			            .attr('cx', 0)
			            .attr('cy', 0)
			            .attr('r', radius)			            
			            .style('fill', '#221f1f')
			            .attr('title',category);
			        var g = gRoot.selectAll('g.arc')
			            .data(pieFn(chartData.values))
			            .enter()
			            .append('g')
			            .attr('class', 'arc');
			        g.append('svg:path')
			            .attr('d', arc)
			            .style('fill', function (d) {
			                return chartConfig.color(d.data.label);
			            })
			            .attr('data-arc-data', function (d) {
			                return d.data.value;
			            });
			        gRoot.append('svg:text')
			            .attr('class', 'center-label')
			            .style('fill', 'white')
			            .text(centerLabel);		     

			       if (chartConfig.hasBottomLabel) {
			            gRoot.append('svg:text')
			                .attr('class', 'bottom-label')
			                .attr('transform', function (d) {
			                    return 'translate(0, ' + (height / 2 + margin.bottom / 2) + ')';
			                })
			                .text(bottomLabel);
			        }
			    }
			    init();
			})();			
		},
		errorHandler:handleError
		});	
	}
	catch(e)
	{alert(e)}
}

