function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'myassessments.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
var rdURL = "myassessments.do"; 
var totalAttempted=0;
var jOrder;
var vData;
var teacherDetail;
var districtAssessmentDetail;
function checkDistrictInventory(teacherId,jobId,districtAssessmentId)
{
	districtAssessmentDetail={districtAssessmentId:districtAssessmentId}
	teacherDetail={teacherId:teacherId};
	var jobOrder={jobId:jobId};
	DistrictAssessmentCampaignAjax.checkDistrictInventory(teacherDetail,jobOrder,districtAssessmentDetail,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){

		var remodel= resourceJSON.msgAssessmentthisjob ;

		if(data.districtAssessmentName==null && data.status=='8')
		{
			$('#message2show').html(  resourceJSON.msgInvalidAssessment );
			$('#myModal2').modal('show');
		}else if(data.districtAssessmentName==null && data.status=='1')
		{
			$('#message2show').html(remodel+ resourceJSON.msgFinalizedCompleteApplication);
			$('#myModal2').modal('show');
		}else if(data.districtAssessmentName==null && data.status=='2')
		{
			$('#message2show').html(remodel  + resourceJSON.msgFinalizedCompleteApplication );
			$('#myModal2').modal('show');
		}
		else if(data.districtAssessmentName==null && data.status=='3')
		{
			if(jobId>0)
				getTeacherJobDone(jobId);
			else
			{
				$('#message2show').html(resourceJSON.msgAlreadyComplted);
				$('#myModal2').modal('show');
			}
		}else if(data.districtAssessmentName==null && data.status=='4')
		{
			$('#message2show').html(resourceJSON.msgAssessmentexpired );
			$('#myModal2').modal('show');
		}
		else if(data.districtAssessmentName==null && data.status=='5')
		{
			if(jobId>0)
				getTeacherJobDone(jobId);
			else
			{
				$('#message2show').html(resourceJSON.msgAssessmentTimedOut);
				$('#myModal2').modal('show');
			}
		}
		else if(data.districtAssessmentName==null && data.status=='6')
		{
			$('#message2show').html(resourceJSON.msgActiveTeacherMatchAdministrator);
			$('#myModal2').modal('show');
		}
		else if(data.districtAssessmentName==null && data.status=='7')
		{
			$('#message2show').html(resourceJSON.msgjobExpiredTeacherMatchAdministrator);
			$('#myModal2').modal('show');
		}
		else
		{
			if(data.status.charAt(2)>=3)
			{
				//$("#myModalvk").css({ top: '60%' });
				$('#warningImg1k').html("<img src='images/stop.png' align='top'>");
				$('#message2showConfirm1k').html(resourceJSON.msgDidntAnswithin3Attempt+" clientservices@teachermatch.net "+resourceJSON.msgor+" (888) 312-7231 .");
				$('#nextMsgk').html("");
				$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' >"+resourceJSON.btnOk+"</button>");
				$('#vcloseBtnk').html("<span>x</span>");
				$('#myModalvk').modal('show');
				if(document.getElementById("bStatus"))
				{
					$('#bStatus').html(resourceJSON.msgtimeout1);
					$('#iconpophover3').trigger('mouseout');
					$('#bClick').html("");
				}
			}else
			{
				//var st_re = data.status.charAt(2)==''?0:data.status.charAt(2);

				$('#warningImg1').html("<img src='images/info.png' align='top'>");

				if(jobId>0)
				{
					$('#message2showConfirm1').html(data.districtAssessmentDescription);
					$('#myModalLabelId').html(resourceJSON.msgAdditionalQuestions);
				}

				$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"startDistrictInventory()\">"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
				$('#vcloseBtn').html("<span>x</span>");
				$('#myModalv').modal('show');
				//$("#myModalv").css({ top: '45%' });

				jOrder=jobOrder;
				vData=data;

			}
		}
	}
	});	
}

function startDistrictInventory()
{
	var data = vData;
	var jobOrder = jOrder;
	var loadingShow = true;
	if(data.status.indexOf("#")==-1)
	{
		$('#loadingDivInventory').fadeIn();
		loadDistrictAssessmentQuestions(data,jobOrder);

	}else
	{
		var newJobId = jobOrder.jobId;
		var dd = data.status;
		var jId = dd.split("#")[2];

		var msg = "";
		if(jId!=0)
		{
			jobOrder.jobId=jId;
			jOrder.jobId=jId;
			msg = "Job Specific Exam";
		}

		if(data.status.charAt(2)==0 || data.status.charAt(2)==1)
		{
			data.status=data.status.charAt(0);
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='images/warn.png' align='top'>");
			$('#message2showConfirm1k').html("<b>"+resourceJSON.msgPleaseReadCarefully+"</b>");
			$('#nextMsgk').html( resourceJSON.msgexceededtimelimit1+" <font color='red'>"+resourceJSON.msgSECOND+"</font> "+resourceJSON.msgexceededtimelimit3);
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"runDistrictInventory(1,"+newJobId+");\">"+resourceJSON.btnOk+"</button>");
			$('#vcloseBtnk').html("<span onclick=\"runDistrictInventory(1,"+newJobId+");\">x</span>");
			$('#myModalvk').modal('show');

		}else if(data.status.charAt(2)==2)
		{
			data.status=data.status.charAt(0);
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='images/warn.png' align='top'>");
			$('#message2showConfirm1k').html("<b>"+resourceJSON.msgReadCarefully+"</b>");
			var nextMsg="<p>"+resourceJSON.msgexceededtimelimitforsec1+""+msg+". "+resourceJSON.msgexceededtimelimitforsec2+" "+msg+" "+resourceJSON.msgitems+".</p>";

			if(jId==0)
				nextMsg += "<p>"+resourceJSON.msgTMStatRemain1+"</p><p>"+resourceJSON.msgTMStatRemain2+"</p>";

			//nextMsg += "<p>"+resourceJSON.msgTMPolicy1+"<a href='javascript:void(0);'>here</a>.</p><p> "+resourceJSON.msgTMPolicy2+" (888) 312-7231 "+resourceJSON.msgTMPolicy3+" <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.</p> <p>"+resourceJSON.msgTMPolicy4+"</p>";
			nextMsg += "<p>If you would like to review TeacherMatch's policy regarding this issue in greater depth please click <a href='javascript:void(0);'>here</a>.</p><p> If you have any further questions regarding this matter, please call us at (888) 312-7231 or email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.</p> <p>Thank you and best of luck in your job search.</p>";

			$('#nextMsgk').html(nextMsg);
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"runDistrictInventory(1,"+newJobId+");\">"+resourceJSON.btnOk+"</button>");
			$('#vcloseBtnk').html("<span onclick=\"runDistrictInventory(1,"+newJobId+");\">x</span>");
			$('#myModalvk').modal('show');

		}

	}
}
function runDistrictInventory(flg,newJobId)
{
	var data = vData;
	var jobOrder = jOrder;
	runDistrictAssessment(data,jobOrder,1,newJobId);
}
function loadDistrictAssessmentQuestions(districtAssessmentDetail,jobOrder)
{
	DistrictAssessmentCampaignAjax.loadDistrictAssessmentQuestions(districtAssessmentDetail,jobOrder,teacherDetail,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==1)
			runDistrictAssessment(districtAssessmentDetail,jobOrder,0,0);
		else if(data=='vlt')
		{
			$('#loadingDivInventory').hide();
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='images/warn.png' align='top'>");
			$('#message2showConfirm1k').html(resourceJSON.msgTimeutInvt);
			$('#nextMsgk').html("");
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#vcloseBtnk').html("<span>x</span>");
			$('#myModalvk').modal('show');
		}else if(data=='comp')
		{
			try{$('#loadingDivInventory').hide();
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='images/info.png' align='top'>");
			$('#message2showConfirm1k').html(resourceJSON.msgAlreadyCompletedInventory+".");
			$('#nextMsgk').html("");
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#vcloseBtnk').html("<span>x</span>");
			$('#myModalvk').modal('show');
			}catch(err)
			{
			}
		}else if(data=='icomp')
		{
			$('#loadingDivInventory').hide();
		}
	}
	});	

}

function runDistrictAssessment(districtAssessmentDetail,jobOrder,doInsert,newJobId)
{
	$('#loadingDivInventory').fadeIn();
	if(doInsert==1)
	{
		insertTeacherDistrictAssessmentAttempt(districtAssessmentDetail,jobOrder);
		// last Attempt insertion
		insertDistrictLastAttempt(jobOrder);
	}

	var redirectURL = "runDistrictAssessment.do?distAssessmentId="+districtAssessmentDetail.districtAssessmentId+"&jobId="+jobOrder.jobId+"&newJobId="+newJobId+"&td="+teacherDetail.teacherId;
	var url = redirectURL+"&dt="+new Date().getTime();
	window.location.href=url;
}
function insertTeacherDistrictAssessmentAttempt(districtAssessmentDetail,jobOrder)
{
	DistrictAssessmentCampaignAjax.insertTeacherDistrictAssessmentAttempt(districtAssessmentDetail,jobOrder,teacherDetail,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
	}
	});	
}
function insertDistrictLastAttempt(jobOrder)
{
	DistrictAssessmentCampaignAjax.insertDistrictLastAttempt(jobOrder,teacherDetail,districtAssessmentDetail,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
	}
	});	
}
function getDistrictAssessmentSectionCampaignQuestionsGrid(whoClicked)
{
	try
	{
		var districtAssessmentDetail = {districtAssessmentId:dwr.util.getValue("districtAssessmentId")};
		var teacherDistrictAssessmentDetail = {teacherDistrictAssessmentId:dwr.util.getValue("teacherDistrictAssessmentId")};
		$('#loadingDivInventory').fadeIn();
		DistrictAssessmentCampaignAjax.getDistrictAssessmentSectionCampaignQuestions(districtAssessmentDetail,teacherDistrictAssessmentDetail,{ 
			async: true,
			errorHandler:handleError,
			callback: function(data){

			$('#tblGrid').html(data);
			setAccordion();
			$('#loadingDivInventory').fadeOut();
		}
		});	
	}
	catch(e)
	{alert("e====="+e)}
}
function setAccordion()
{
	$(function ($)
	{
		
			var v = $("#collapseIdsText").val().toString();
		    var val2= $(v);
		     // var val2= $('#id0,#id1,#id2,#id3,#id4,#id5,#id6,#id7,#id8,#id9,#id10,#id11,#id12');
		    val2.on('show.bs.collapse hidden.bs.collapse', function () {
		        $(this).prev().find('.accordion-toggle').toggleClass('plus minus');
		    })
		});

			$('.accordion').collapse();
			$('.accordion').on('show', function (e) {
			$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
			});
			$('.accordion').on('hide', function (e) {
			$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
	});
}
function finalizeAnswers(flag)
{

	var teacherDistrictAssessmentQuestion;
	var arr =[];
	var questionIds = document.getElementsByName("teacherDistrictAssessmentQuestionId");
	var questionImages = document.getElementsByName("questionImage");
	var questionWeightages = document.getElementsByName("questionWeightage");
	var questionTypeIds = document.getElementsByName("questionTypeId");
	var questionTypeShortNames = document.getElementsByName("questionTypeShortName");
	var isError = false;
	var j=0;
	var errorMsg = "";
	var sectionIds = document.getElementsByName("sectionId");
	var answered=0;
	var unanswered=0;
	$('#myModalInner').empty();
	$('#totalquestion').empty();
	$('#answered').empty();
	$('#unanswered').empty();
	
	
	for (var i = 0; i < questionIds.length; i++) {
		
		j++;
		$('#q'+j+'validate').hide();
		
		teacherDistrictAssessmentQuestion = {teacherDistrictAssessmentQuestionId:questionIds[i].value,questionImage:questionImages[j]};
		var opts=document.getElementsByName("q"+j+"opt");

		var questionTypeMaster = {questionTypeId:questionTypeIds[i].value};
		var qType = questionTypeShortNames[i].value;
		var questionWeightage = questionWeightages[i].value;

		var o_maxMarks = dwr.util.getValue("q"+j+"o_maxMarks");
		//alert("j:: "+j+" qType: "+qType);
		if(qType=='tf' || qType=='slsel' || qType=='lkts' || qType=='it' || qType=='itq')
		{

			var optId="";
			var score=0;
			if($('input[name=q'+j+'opt]:radio:checked').length > 0 )
			{
				answered++;
				optId=$('input[name=q'+j+'opt]:radio:checked').val();
				var scores = document.getElementsByName("q"+j+"score");
				for (var k = 0; k < opts.length; k++) {
					if(optId==opts[k].value)
					{
						score = scores[k].value;
						break;
					}
				}
			}
			
			if(optId=='')
			{
				unanswered++;
				//alert(i+" "+qType+" Question"+j+" is answered");
				errorMsg+= "Question "+j+" is answered\n";
				var secNo="";
				var secValue="";
				for(var s=1;s<=sectionIds.length;s++)
				{
					secValue=document.getElementById("q"+j+"section").value;
					if(s==secValue)
						secNo=secValue;
				}
				if(secNo!="")
				{
					//alert("secNo::  "+secNo+"\nsecName::  "+sectionIds[(secNo-1)].value+"\nquestionNo::  "+j);
					if($("#errordiv"+secNo).length==0)
					{
						$('#myModalInner').append(resourceJSON.msgAnsFollowingQn+"  <b>&#34; <font color='#007AB4'>"+sectionIds[(secNo-1)].value+"</font> &#34;</b><br>");
						$('#myModalInner').append("<div class='divErrorMsg' id='errordiv"+secNo+"' style='display: block;'></div><br>");
						$("#errordiv"+secNo).append(j);
						//alert("errordiv"+secNo+"\nques"+j);
					}
					else
					{
						$("#errordiv"+secNo).append(", "+j);
						//alert("errordiv"+secNo+"\nques"+j);
					}
				}
				isError = true;
				$('#q'+j+'validate').show();
			}
			
			if(optId!='')
			{
				var totalScore = questionWeightage*score;
				
				arr.push({ 
					"selectedOptions"  : optId,
					"optionScore"      : score,
					"totalScore"       : totalScore,
					"questionWeightage" : questionWeightage,
					"questionTypeMaster" : questionTypeMaster,
					"maxMarks" :o_maxMarks,
					"teacherDistrictAssessmentQuestion":teacherDistrictAssessmentQuestion
				});
			}

		}else if(qType=='rt')
		{
			var optId="";
			var score="";
			var rank="";
			var scoreRank=0;
			var scores = document.getElementsByName("q"+j+"score");
			var ranks = document.getElementsByName("q"+j+"rank");
			var o_ranks = document.getElementsByName("q"+j+"o_rank");
			var tt=0;
			var uniqueflag=false;
			for(var k = 0; k < opts.length; k++) {
				optId += opts[k].value+"|";
				score += scores[k].value+"|";
				rank += ranks[k].value+"|";

				if(checkUniqueRank(ranks[i]))
				{
					uniqueflag=true;
					break;
				}

				if(ranks[k].value==o_ranks[k].value)
				{
					scoreRank+=parseInt(scores[k].value);
					//alert(scoreRank);
				}
				if(ranks[k].value=="")
				{
					tt++;
				}
			}
			/////////////////////////////////////
			if(uniqueflag)
				return;

			if(tt!=0)
				optId=""; 

			if(optId=="")
			{
				unanswered++;
				//alert(i+" "+qType+" Question"+j+" is answered");
				errorMsg+= "Question "+j+" is answered\n";
				var secNo="";
				var secValue="";
				for(var s=1;s<=sectionIds.length;s++)
				{
					secValue=document.getElementById("q"+j+"section").value;
					if(s==secValue)
						secNo=secValue;
				}
				if(secNo!="")
				{
					//alert("secNo::  "+secNo+"\nsecName::  "+sectionIds[(secNo-1)].value+"\nquestionNo::  "+j);
					if($("#errordiv"+secNo).length==0)
					{
						$('#myModalInner').append(""+resourceJSON.msgAnsFollowingQn+"  <b>&#34; <font color='#007AB4'>"+sectionIds[(secNo-1)].value+"</font> &#34;</b><br>");
						$('#myModalInner').append("<div class='divErrorMsg' id='errordiv"+secNo+"' style='display: block;'></div><br>");
						$("#errordiv"+secNo).append(j);
						//alert("errordiv"+secNo+"\nques"+j);
					}
					else
					{
						$("#errordiv"+secNo).append(", "+j);
						//alert("errordiv"+secNo+"\nques"+j);
					}
				}
				isError = true;
				$('#q'+j+'validate').show();
			}else
				answered++;
			
			if(optId!='')
			{
				var totalScore = questionWeightage*scoreRank;
				
				arr.push({ 
					"selectedOptions"  : optId,
					"optionScore"      : score,
					"totalScore"       : totalScore,
					"insertedRanks"    : rank,
					"questionWeightage" : questionWeightage,
					"questionTypeMaster" : questionTypeMaster,
					"maxMarks" :o_maxMarks,
					"teacherDistrictAssessmentQuestion":teacherDistrictAssessmentQuestion
				});
			}

		}else if(qType=='sl' || qType=='ml')
		{
			var opts=document.getElementsByName("q"+j+"opt");
			var scores = document.getElementsByName("q"+j+"score");
			var insertedText = opts[0].value;
			if(insertedText=='')
			{

				unanswered++;
				//alert(i+" "+qType+" Question"+j+" is answered");
				errorMsg+= "Question "+j+" is answered\n";
				var secNo="";
				var secValue="";
				for(var s=1;s<=sectionIds.length;s++)
				{
					secValue=document.getElementById("q"+j+"section").value;
					if(s==secValue)
						secNo=secValue;
				}
				if(secNo!="")
				{
					//alert("secNo::  "+secNo+"\nsecName::  "+sectionIds[(secNo-1)].value+"\nquestionNo::  "+j);
					if($("#errordiv"+secNo).length==0)
					{
						$('#myModalInner').append(""+resourceJSON.msgAnsFollowingQn+"  <b>&#34; <font color='#007AB4'>"+sectionIds[(secNo-1)].value+"</font> &#34;</b><br>");
						$('#myModalInner').append("<div class='divErrorMsg' id='errordiv"+secNo+"' style='display: block;'></div><br>");
						$("#errordiv"+secNo).append(j);
						//alert("errordiv"+secNo+"\nques"+j);
					}
					else
					{
						$("#errordiv"+secNo).append(", "+j);
						//alert("errordiv"+secNo+"\nques"+j);
					}
				}
				isError = true;
				$('#q'+j+'validate').show();
			}else
				answered++;
			
			if(insertedText!='')
			{
				//alert("swss: "+scores[0].value);
				var score = 0;
				 try{score = scores[0].value==null?0:scores[0].value}catch(e){};
				
				arr.push({ 
					"insertedText"    : insertedText,
					"questionTypeMaster" : questionTypeMaster,
					"totalScore"       : null,
					"questionWeightage" : 0,
					//"maxMarks" :o_maxMarks,
					"maxMarks" :0,
					"teacherDistrictAssessmentQuestion":teacherDistrictAssessmentQuestion
				});
			}
		}else
		{

			arr.push({ 
				"questionWeightage" : questionWeightage,
				"questionTypeMaster" : questionTypeMaster,
				"totalScore" : 0,
				"maxMarks" :o_maxMarks,
				"teacherDistrictAssessmentQuestion":teacherDistrictAssessmentQuestion
			});
		}

	}
	if(isError && flag)
	{
		//alert(""+isError);
		$('#totalquestion').append(""+j);
		if(answered>0)
		{
			document.getElementById("answeredDiv").style.display = 'block';
			$('#answered').append(answered);
		}
		$('#unanswered').append(unanswered);
		$('#myModal').modal('show');
		
	}else
	{
		try
		{
			var districtAssessmentDetail = {districtAssessmentId:dwr.util.getValue("districtAssessmentId")};
			var teacherDistrictAssessmentDetail = {teacherDistrictAssessmentId:dwr.util.getValue("teacherDistrictAssessmentId")};
			var attemptId = dwr.util.getValue("attemptId");
			$('#loadingDivInventory').fadeIn();
			$('#spnMpro').html( resourceJSON.msgAnsSaved+"...");
			DistrictAssessmentCampaignAjax.finalizeDistrictAssessmentQuestions(districtAssessmentDetail,teacherDistrictAssessmentDetail,arr,attemptId,{ 
				async: true,
				errorHandler:handleError,
				callback: function(data){
				//alert("data:: "+data);
				var msg = resourceJSON.msgComplAssmnt ;
				var logo = "info.png";
				if(data==0)
				{
					msg =  resourceJSON.msgSesTimeOut ; 
					logo = "warn.png";
				}
				$('#loadingDivInventory').fadeOut();
				$('#warningImg1').html("<img src='images/"+logo+"' align='top'>");
				$('#message2showConfirm1').html(msg);
				$('#myModalLabelId').html(resourceJSON.msgThankYou);

				$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"location.href='myassessments.do'\">"+resourceJSON.btnOk+"</button>");
				$('#vcloseBtn').html("<span onclick=\"location.href='myassessments.do'>x</span>");
				$('#myModalv').modal('show');
				
				window.onbeforeunload = function() {};
				document.getElementById('sbmtbtn').style.display='none'
				setTimeout(function() {
				window.location.href='myassessments.do';
				}, 10000)
			}
			});
		}
		catch(e)
		{alert("e====="+e)}
	}

}

function updateAttempt()
{
	try
	{
		var attemptId = dwr.util.getValue("attemptId");

		DistrictAssessmentCampaignAjax.updateAttempt(attemptId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			//alert(data);
			//window.clearInterval(intervalID);
			//$('#tblGrid1').html(data);
		}
		});
	}
	catch(e)
	{alert("e====="+e)}
}
function getDistrictAssessmentQuestions()
{
	try
	{

		var districtAssessmentDetail = {districtAssessmentId:dwr.util.getValue("districtAssessmentId")};
		var teacherDistrictAssessmentDetail = {teacherDistrictAssessmentId:dwr.util.getValue("teacherDistrictAssessmentId")};

		DistrictAssessmentCampaignAjax.getDistrictAssessmentQuestions(districtAssessmentDetail,teacherDistrictAssessmentDetail,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			$('#tblGrid1').html(data);
		}
		});
	}
	catch(e)
	{alert("e====="+e)}
}	
function checkStrikes(questionSessionTime,whoClicked)
{	
	if(questionSessionTime==0)
	{
		//$("#myModalv").css({ top: '60%' });
		$('#warningImg1').html("<img src='images/info.png' align='top'>");
		$('#message2showConfirm1').html(resourceJSON.msgAllQMand);
		$('#nextMsg').html("");
		$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
		$('#vcloseBtn').html("<span>x</span>");
		$('#myModalv').modal('show');
	}else
	{
		if(whoClicked==1)
		{

			//$("#myModalv").css({ top: '60%' });@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

			$('#warningImg1').html("<img src='images/info.png' align='top'>");

			$('#message2showConfirm1').html(resourceJSON.msgEveryAnsb4zero);		
			$('#nextMsg').html("");			
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");	
			$('#vcloseBtn').html("<span>x</span>");					
			$('#myModalv').modal('show');



			var remSec = parseInt($('.min').html()==null?0:$('.min').html())*60 +parseInt($('.sec').html());
			if(remSec>0)
			{
				return;
			}


		}else
		{
			strikeCheck(document.getElementById("teacherAssessmentQuestionId").value,whoClicked);
		}
	}
}
function checkUniqueRank(dis)
{
	if(isNaN(dis.value))
	{
		dis.value="";
		dis.focus();
		return;
	}
	var arr =[];
	var ranks = document.getElementsByName("rank");
	for(i=0;i<ranks.length;i++)
	{
		if(ranks[i].value!="" && ranks[i]!=dis)
		{
			arr.push(ranks[i].value);
		}
	}
	var o_rank = document.getElementsByName("o_rank");

	var arr2 =[];
	for(i=0;i<o_rank.length;i++)
		arr2.push(o_rank[i].value);


	if(dis.value!="")
		if($.inArray(dis.value, arr2)==-1)
		{
			dis.value="";
			$('#warningImg1').html("<img src='images/info.png' align='top'>");
			$('#message2showConfirm1').html(resourceJSON.msgValidRank);
			$('#nextMsg').html("");
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#myModalv').modal('show');
			//$("#myModalv").css({ top: '45%' });
			$('#vcloseBtn').html("<span>x</span>");

			dis.focus();
			return false;
		}


	$.each(arr, function(j, el){
		if(parseInt(el)==parseInt(dis.value))
		{
			dis.value="";
			$('#warningImg1').html("<img src='images/info.png' align='top'>");
			$('#message2showConfirm1').html(resourceJSON.msgUniqueRank);
			$('#nextMsg').html("");
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#myModalv').modal('show');
			//$("#myModalv").css({ top: '45%' });
			$('#vcloseBtn').html("<span>x</span>");
			dis.focus();

			return false;
		}
	});
}
function redirectToDashboard(teacherAssessmentType)
{
	window.onbeforeunload = function() {
	};	
	var attemptId = dwr.util.getValue("attemptId");

	DistrictAssessmentCampaignAjax.setAssessmentForced(attemptId,{ 
		async: false,
		callback: function(data){
		if(epiStandalone)
			window.location.href="epiboard.do?w="+teacherAssessmentType;
		else
			window.location.href=rdURL+"?w="+teacherAssessmentType;
	},
	errorHandler:handleError  
	});	
}
function stopTimer()
{
	clearTimeout(timeout);
}
function strikeCheck(teacherAssessmentQuestionId,whoClicked)
{
	var teacherAssessmentType = dwr.util.getValue("teacherAssessmentType");
	var moreMsg = ".";
	if(teacherAssessmentType==1)
		moreMsg = ".";

	$('#tm-root').html("");
	var questionSessionTime = 0;
	if(document.getElementById("questionSessionTime"))
		questionSessionTime = dwr.util.getValue("questionSessionTime")==null?0:dwr.util.getValue("questionSessionTime");

	var totalChances = parseInt(totalAttempted) + totalStrikes;
	if(totalChances==1  && (whoClicked!=2))
	{
		var onclick = "";
		onclick = "onclick=\"stopTimer(),tmTimer("+questionSessionTime+",'1')\"";

		//$("#myModalv").css({ top: '60%' });
		$('#warningImg1').html("<img src='images/warn.png' align='top'>");
		$('#message2showConfirm1').html(resourceJSON.msgTimedOutthestipulatedtime);
		$('#nextMsg').html(resourceJSON.msgreadytoStrt1+" <font color='red'>SECOND</font> "+resourceJSON.msgreadytoStrt2 + moreMsg);
		$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' "+onclick+" aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
		$('#vcloseBtn').html("<span "+onclick+" >x</span>");
		$('#myModalv').modal('show');

		if(questionSessionTime>0 && whoClicked==0)
		{
			window.clearInterval(intervalID);
			saveDistrictStrike(teacherAssessmentQuestionId);
			timeout =  setTimeout('redirectToDashboard('+teacherAssessmentType+')', 10000);
		}
		return;
	}
	if(totalChances==2  && (whoClicked!=2))
	{
		var onclick = "";
		onclick = "onclick=\"stopTimer(),tmTimer("+questionSessionTime+",'1')\"";

		//$("#myModalv").css({ top: '60%' });
		$('#warningImg1').html("<img src='images/warn.png' align='top'>");
		$('#message2showConfirm1').html(resourceJSON.msgTimedOutthestipulatedtime2);
		$('#nextMsg').html( resourceJSON.msgreadytoStrt1+"<font color='red'>"+resourceJSON.msgFINAL+"</font> "+resourceJSON.msgreadytoStrt2  +  moreMsg);
		$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' "+onclick+" aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
		$('#vcloseBtn').html("<span "+onclick+" >x</span>");
		$('#myModalv').modal('show');

		if(questionSessionTime>0 && whoClicked==0)
		{
			window.clearInterval(intervalID);
			saveDistrictStrike(teacherAssessmentQuestionId);
			timeout = setTimeout('redirectToDashboard('+teacherAssessmentType+')', 10000);

		}
		return;
	}else if((totalAttempted>=1 && totalStrikes>=1) || (totalAttempted>=2 && totalStrikes==0)) //if(totalChances>=3)
	{

		if(whoClicked==0)
		{
			window.clearInterval(intervalID);
		}

		finishDistrictAssessment(0);
		return;
	}

}
function redirectToURL(redirectURL)
{
	var url = redirectURL;
	window.location.href=url;
}
function redirectTo()
{
	if(document.getElementById('epiStandAlone') && $('#epiStandAlone').val()==false || $('#epiStandAlone').val()=="false")
	{
		//$(location).attr('href','epiboard.do');
		//window.location.href='epiboard.do';

		if(document.getElementById('rURL'))
		{
			var url = $('#rURL').val();
			window.location.href=url;
		}else
		{
			window.location.href='portalboard.do';
		}

		//window.open(url,"_tab","TeacherMatch","toolbar=yes,location=yes,scrollbars=yes,menubar=yes");
	}else
	{
		//window.location.href='userdashboard.do';
		if(document.getElementById('rURL'))
		{
			var url = $('#rURL').val();
			window.location.href=url;
		}else
		{
			window.location.href=rdURL;
		}

		//window.open(url,"_tab","TeacherMatch","toolbar=yes,location=yes,scrollbars=yes,menubar=yes");
	}
}
var isPortal = false;
function showInfoAndRedirect(sts,msg,nextmsg,url,epiFlag)
{
	//$("#myModalv").css({ top: '60%' });
	var img="info";
	if(sts==1)
		img = "warn";
	else if(sts==2)
		img = "stop";

	$('#warningImg1').html("<img src='images/"+img+".png' align='top'>");
	$('#message2showConfirm1').html(msg);
	$('#nextMsg').html(nextmsg);

	var onclick = "";
	if(url!=""){
		if(epiFlag==1){
			onclick = "onclick='redirectTo()'";
		}else{
			//onclick = "onclick=checkPopup('"+url+"')";
			onclick = "onclick=redirectToPage('"+url+"')";
		}
	}


	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">"+resourceJSON.btnOk+"</button>");
	$('#vcloseBtn').html("<span "+onclick+" >x</span>");
	$('#myModalv').modal('show');
}
function redirectToPage(url)
{
	//alert(url);
	checkPopup(url);
	if(isPortal)
		window.location.href="portaldashboard.do";
	else
		window.location.href=rdURL;
}
function clearDialog(divId)
{
	$("#"+divId ).dialog( "close" );
	$("#"+divId ).hide();
}
function checkPopup(url) {
	var openWin = window.open(url,"directories=no,height=400,width=600,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
	if (!openWin) {
		window.location.href=url;
	} else {
		window.open(url,"directories=no,height=400,width=600,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
	}
}
function saveDistrictStrike(teacherDistrictAssessmentQuestionId)
{
	if(teacherDistrictAssessmentQuestionId==0)
		return;

	var teacherDistrictAssessmentQuestion = {teacherDistrictAssessmentQuestionId:dwr.util.getValue("teacherDistrictAssessmentQuestionId")};
	var teacherDistrictAssessmentDetail = {teacherDistrictAssessmentId:dwr.util.getValue("teacherDistrictAssessmentId")};
	DistrictAssessmentCampaignAjax.saveDistrictStrike(teacherDistrictAssessmentDetail,teacherDistrictAssessmentQuestion,teacherDetail,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		totalStrikes++;
	}
	});	
}


function finishDistrictAssessment(sessionCheck)
{

	$('#myModalvk').modal('hide');
	$('#myModalv').modal('hide');

	var newJobId = document.getElementById("newJobId").value;

	DistrictAssessmentCampaignAjax.finishDistrictAssessment(teacherDistrictAssessmentdetail,jobOrder,newJobId,sessionCheck,teacherDetail,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#tm-root').html(data);
	}
	});	
}

function checkDistrictAssessmentDone(teacherDistrictAssessmentId)
{

	var teacherDistrictAssessmentDetail = {teacherDistrictAssessmentId:teacherDistrictAssessmentId};
	DistrictAssessmentCampaignAjax.checkDistrictAssessmentDone(teacherDistrictAssessmentDetail,teacherDetail,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==0)
		{
			//getDistrictAssessmentQuestions();
			getDistrictAssessmentSectionCampaignQuestionsGrid(0);
		}
		else
			finishDistrictAssessment(0)
	}
	});	
}
function portfolioURL(){
	var portfolioAction= document.getElementById("portfolioAction").value; 
	window.location.href=portfolioAction;
}