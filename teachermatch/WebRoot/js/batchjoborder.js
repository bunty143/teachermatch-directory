/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	searchRecordsByEntityType();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	searchRecordsByEntityType();
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr + exception.javaClassName);}
}
/*======= Batch Batch Job Orders on Press Enter Key ========= */
function chkForEnterSearchBatchJobOrder(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		searchBatchJobOrder();
	}	
}
/*========== Add Batch Job Order Functionality ============*/
	function addBatchJobOrder()
	{
		$("#addBatchJobOrderDiv").fadeIn();
	}
	
	function clearBatchJobOrder()
	{
			$("#addBatchJobOrderDiv").hide();
	}
	
	/*========== Add Batch Job Order Functionality ============*/
	function addCertifiction()
	{
		$("#addCertifictionDiv").fadeIn();
	}
	
	function clearCertifiction()
	{
		$("#addCertifictionDiv").hide();
	}
	
	/*========== Add SchoolJobOrder Functionality ============*/
	function addSchoolJobOrder()
	{
		$("#addSchoolJobOrderDiv").fadeIn();
	}
	
	function clearSchoolJobOrder()
	{
		$("#addSchoolJobOrderDiv").hide();
	}
	/* Display SchoolList */
	function displaySchoolList(batchJobId,type){
		BatchJobOrdersAjax.displaySchoolList(batchJobId,type,{ 
			async: true,
			callback: function(data)
			{
				$('#divMain').html(data);
				applyScrollOnTbl();	
			},
			errorHandler:handleError 
		});
	}
	
	function searchBatchJobOrder()
	{
		page = 1;
		searchRecordsByEntityType();
	}

	/*========  SearchDistrictOrSchool ===============*/
	function searchRecordsByEntityType()
	{
		var JobOrderType=document.getElementById("JobOrderType").value;
		var districtId	=	document.getElementById("districtlId").value;
		var certifications="";
		var addBatchJobFlag=document.getElementById("addBatchJobFlag").value;
		if(document.getElementById("certType").value!=''){
			certifications=trim(document.getElementById("certType").value);
		}
		
		var schoolId =document.getElementById("schoolId").value;
		var resultFlag=true;		
		if(document.getElementById("districtORSchoolName").value!="" && districtId==0){
			resultFlag=false;
		}
		if(document.getElementById("schoolName").value!="" && schoolId==0){
			resultFlag=false;
		}
		$('#errordiv').empty();
		if(addBatchJobFlag=="true" && JobOrderType==3){
			if(districtId==""){
				$('#errordiv').show();
				$('#errordiv').append("&#149;"+resourceJSON.msgValidDistrictName+"<br>");
				$('#districtORSchoolName').focus();
				$('#districtORSchoolName').css("background-color", "#F5E7E1");
			}else if(districtId=="0"){
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgEnterDistName+"<br>");
				$('#districtORSchoolName').focus();
				$('#districtORSchoolName').css("background-color", "#F5E7E1");
			}else if(schoolId=="0" || document.getElementById("schoolName").value==''){
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgPleaseenterSchoolName+"<br>");
				$('#schoolName').focus();
				$('#schoolName').css("background-color", "#F5E7E1");
			}else  if(schoolId==""){
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgentervalidSchoolName+"<br>");
				$('#schoolName').focus();
				$('#schoolName').css("background-color", "#F5E7E1");
			}
		}
		$('#loadingDiv').fadeIn();
		delay(1000);
		BatchJobOrdersAjax.displayRecordsByEntityType(resultFlag,JobOrderType,districtId,schoolId,certifications,noOfRows,page,sortOrderStr,sortOrderType, { 
			async: true,
			callback: function(data)
			{
				$('#divMain').html(data);
				applyScrollOnTbl();	
				$('#loadingDiv').hide();
			},
			errorHandler:handleError 
		});
	}
	/*========  displayJobRecords ===============*/
	function displayBatchJobRecords()
	{
		$('#loadingDiv').fadeIn();
		var JobOrderType	=null;	
		var addBatchJobFlag=document.getElementById("addBatchJobFlag").value;
		try{
			JobOrderType=document.getElementById("JobOrderType").value;
		}catch(err){
			JobOrderType=0;
		}
		var districtId	=0;
		
		if(document.getElementById("districtlId").value!=0){
			districtId=document.getElementById("districtlId").value;
		}else{
			document.getElementById('schoolName').readOnly=true;
		}
		var displayFlag=true;
		
		if(addBatchJobFlag=="true" && JobOrderType==3){
			displayFlag=false;
		}
		delay(1000);
		BatchJobOrdersAjax.displayRecordsByEntityType(displayFlag,JobOrderType,districtId,0,"",noOfRows,page,sortOrderStr,sortOrderType, { 
			async: true,
			callback: function(data)
			{
				$('#divMain').html(data);
				applyScrollOnTbl();	
				$('#loadingDiv').hide();
			},
			errorHandler:handleError
		});
	}
	function delay(sec){
		var starttime = new Date().getTime();
		starttime = starttime+sec;
		while(true){
			if(starttime< new Date().getTime()){
				break;
			}
		}
	}
	/*========  deleteBatchJobOrder ===============*/
	function deleteBatchJobOrder(batchJobId)
	{
		if (confirm(resourceJSON.msgDeleteBAtchJobOrder)) {
			BatchJobOrdersAjax.deleteBatchJobOrder(batchJobId, { 
				async: true,
				callback: function(data)
				{
					displayBatchJobRecords();
				},
				errorHandler:handleError
			});
		}
	}


	var count=0;
	var index=-1;
	var length=0;
	var divid='';
	var txtid='';
	var hiddenDataArray = new Array();
	var showDataArray = new Array();
	var degreeTypeArray = new Array();
	var hiddenId="";
	function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
	{
		hiddenId=hiddenId;
		divid=txtdivid;
		txtid=txtSearch.id;
		if(event.keyCode==40){
			downArrowKey(txtdivid);
		} 
		else if(event.keyCode==38) //up key
		{
			upArrowKey(txtdivid);
		} 
		else if(event.keyCode==13) // RETURN
		{
			if(document.getElementById(divid))
				document.getElementById(divid).style.display='block';
			
			document.getElementById("districtORSchoolName").focus();
			
		} 
		else if(event.keyCode==9) // Tab
		{
			
		}
		else if(txtSearch.value!='')
		{
			index=-1;
			length=0;
			document.getElementById(divid).style.display='block';
			searchArray = getDistrictORSchoolArray(txtSearch.value);
			fatchData(txtSearch,searchArray,txtId,txtdivid);
			
		}
		else if(txtSearch.value==""){
			document.getElementById(divid).style.display='none';
		}
	}

	function getDistrictORSchoolArray(districtOrSchoolName){
		var searchArray = new Array();
		var JobOrderType	=	document.getElementById("JobOrderType").value;
	//	if(JobOrderType==2){
			BatchJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
				async: false,
				callback: function(data){
				hiddenDataArray = new Array();
				showDataArray = new Array();
				for(i=0;i<data.length;i++){
					searchArray[i]=data[i].districtName;
					showDataArray[i]=data[i].districtName;
					hiddenDataArray[i]=data[i].districtId;
				}
			},
			errorHandler:handleError
			});	
		//}
		
		

		return searchArray;
	}


	var selectFirst="";

	var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
		var result = document.getElementById(txtdivid);
		try{
			result.style.display='block';
			result.innerHTML = '';
			var items='';
			count=0;
			var len=searchArray.length;
			if(document.getElementById(txtId).value!="")
			{
				
				for(var i=0;i<len;i++){
						items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
						searchArray[i].toUpperCase() + "</div>";
						count++;
						length++;
						
					if(count==10)
						break;
					
				}
				
			}
			else {
				
			}
			if(count!=0)
				result.innerHTML = items;
			else{
				result.style.display='none';
				selectFirst="";
			}
			if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
			{
				document.getElementById('divResult'+txtdivid+0).className='over';
				selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
			}
			
			scrolButtom();
		}catch (err){}
	}
	function hideDistrictMasterDiv(dis,hiddenId,divId)
	{
		document.getElementById("districtlId").value="";
		if(parseInt(length)>0){
			if(index==-1){
				index=0;
			}
			if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
				document.getElementById(hiddenId).value=hiddenDataArray[index];
			}
			if(dis.value==""){
				document.getElementById(hiddenId).value="";
			}
			else if(showDataArray && showDataArray[index]){
				dis.value=showDataArray[index];
				document.getElementById("districtlId").value=hiddenDataArray[index];
				document.getElementById('schoolName').readOnly=false;
				document.getElementById('schoolName').value="";
				document.getElementById('schoolId').value="0";
			}
			
		}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
		}
		
		if(document.getElementById(divId))
		{
			document.getElementById(divId).style.display="none";
		}
		index = -1;
		length = 0;
	}

	
	count=0;
	index=-1;
	length=0;
	divid='';
	txtid='';
	hiddenDataArray = new Array();
	showDataArray = new Array();
	degreeTypeArray = new Array();
	hiddenId="";

	function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
	{
		hiddenId=hiddenId;
		divid=txtdivid;
		txtid=txtSearch.id;
		if(event.keyCode==40){
			downArrowKey(txtdivid);
		} 
		else if(event.keyCode==38) //up key
		{
			upArrowKey(txtdivid);
		} 
		else if(event.keyCode==13) // RETURN
		{
			if(document.getElementById(divid))
				document.getElementById(divid).style.display='block';
			
			document.getElementById("schoolName").focus();
			
		} 
		else if(event.keyCode==9) // Tab
		{
			
		}
		else if(txtSearch.value!='')
		{
			index=-1;
			length=0;
			document.getElementById(divid).style.display='block';
			searchArray = getSchoolArray(txtSearch.value);
			fatchData2(txtSearch,searchArray,txtId,txtdivid);
			
		}
		else if(txtSearch.value==""){
			document.getElementById(divid).style.display='none';
		}
	}

	function getSchoolArray(schoolName){
		var searchArray = new Array();
		var JobOrderType	=	document.getElementById("JobOrderType").value;
		var districtId	=	document.getElementById("districtlId").value;
			BatchJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
				async: false,
				callback: function(data){
				hiddenDataArray = new Array();
				showDataArray = new Array();
				for(i=0;i<data.length;i++){
					searchArray[i]=data[i].schoolName;
					showDataArray[i]=data[i].schoolName;
					hiddenDataArray[i]=data[i].schoolMaster.schoolId;
				}
			},
			errorHandler:handleError
			});	
		return searchArray;
	}


	selectFirst="";

	var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

		var result = document.getElementById(txtdivid);
		try{
			result.style.display='block';
			result.innerHTML = '';
			var items='';
			count=0;
			var len=searchArray.length;
			if(document.getElementById(txtId).value!="")
			{
				
				for(var i=0;i<len;i++){
						items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
						searchArray[i].toUpperCase() + "</div>";
						count++;
						length++;
						
					if(count==10)
						break;
					
				}
				
			}
			else {
				
			}
			if(count!=0)
				result.innerHTML = items;
			else{
				result.style.display='none';
				selectFirst="";
			}
			if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
			{
				document.getElementById('divResult'+txtdivid+0).className='over';
				selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
			}
			
			scrolButtom();
		}catch (err){}
	}
	function hideSchoolMasterDiv(dis,hiddenId,divId)
	{
		document.getElementById("schoolId").value="";
		if(parseInt(length)>0){
			if(index==-1){
				index=0;
			}
			if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
				document.getElementById(hiddenId).value=hiddenDataArray[index];
			}
			if(dis.value==""){
				document.getElementById(hiddenId).value="";
			}
			else if(showDataArray && showDataArray[index]){
				dis.value=showDataArray[index];
				document.getElementById("schoolId").value=hiddenDataArray[index];
			}
			
		}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
		}
		
		if(document.getElementById(divId))
		{
			document.getElementById(divId).style.display="none";
		}
		index = -1;
		length = 0;
	}
	
	
	

	var downArrowKey = function(txtdivid){
		if(txtdivid)
		{
			if(index<length-1){
				for(var i=0;i<10;i++){
					{
						if(document.getElementById('divResult'+txtdivid+i))
							var div_id=document.getElementById('divResult'+txtdivid+i);
						if(div_id)
						{
							if(div_id.className=='over')
								index=div_id.id.split('divResult'+txtdivid)[1];
							div_id.className='normal';
						}
					}
				}
				index++;
				if(document.getElementById('divResult'+txtdivid+index))
				{
					var div_id=document.getElementById('divResult'+txtdivid+index);
					div_id.className='over';
					document.getElementById(txtid).value = div_id.innerHTML;
					selectFirst=div_id.innerHTML;
				}
			}
		}
	}

	var upArrowKey = function(txtdivid){
		if(txtdivid)
		{
			if(index>0){
				for(var i=0;i<length;i++){

					var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
				index--;
				if(document.getElementById('divResult'+txtdivid+index))
					document.getElementById('divResult'+txtdivid+index).className='over';
				if(txtid && document.getElementById('divResult'+txtdivid+index))
				{
					document.getElementById(txtid).value=
						document.getElementById('divResult'+txtdivid+index).innerHTML;
					selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
				}
			}
		}
	}

	var overText=function (div_value,txtdivid) {

		for(var i=0;i<length;i++)
		{
			if(document.getElementById('divResult'+i))
			{
				var div_id=document.getElementById('divResult'+i);

				if(div_id.className=='over')
					index=div_id.id.split(txtdivid)[1];
				div_id.className='normal';
			}
		}
		div_value.className = 'over';
		document.getElementById(txtid).value= div_value.innerHTML;
	}
	function trim(s)
	{
		while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
			s = s.substring(1,s.length);
		}
		while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
			s = s.substring(0,s.length-1);
		}
		return s;
	}
	function checkForInt(evt) {

		var charCode = ( evt.which ) ? evt.which : event.keyCode;
		if(charCode==8)
			return true;

		return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
	}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}